﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit MainData;

interface

uses
  SysUtils, Classes, Controls, Forms, Db, ADODB, Wwintl, isEnv,
  NativeXml, AdvMenuStylers, AdvMenus, ImgList, isLogin;

type
  TDataMain = class(TDataModule)
    Conn: TADOConnection;
    ImageMark: TImageList;
    ImageSort: TImageList;
    ImageSmall: TImageList;
    wwIntl: TwwIntl;
    ImageOrg: TImageList;
    QrSql: TADOQuery;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    isEnv1: TisEnv;
    ImageNavi: TImageList;
    isLogon: TisLogon;
    procedure ConnWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure DataModuleCreate(Sender: TObject);
  private
    mEximPath: String;
  public
    function  ListTables(pType: Integer; ls: TStrings): Boolean;
    procedure CopyData(qrSource, qrDest: TADOQuery);

    function Logon(): Boolean;

    function CreateXml: TNativeXml;

    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer); overload;
    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer); overload;

    procedure XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
    procedure XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);

    procedure XmlCreateNode(pXml: TNativeXml; tabname: String);
    procedure ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String); overload;

    procedure ImportDataXml(pXml: TNativeXml; pTable: Integer); overload;
    procedure ImportDataXml(pXml: TNativeXml; pTable: string); overload;
  end;

var
  DataMain: TDataMain;

implementation

uses
	isLib, isMsg, isDb, Variants, ExCommon, isStr, isEnCoding, isCommon;

{$R *.DFM}

const
    EXCEPT_FIELDS: String = 'TABLE,';
    SELECT_SQL: String = 'select * from ';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ConnWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
	ConnectionString := GetConnectionString(isNameServer, isUserServer, isPasswordServer, isDatabaseServer);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleCreate(Sender: TObject);
begin
	CloseDataSets(Conn);
    Conn.Connected := False;
	wwIntl.Connected := True;

    GetLocaleFormatSettings(0, fmt);
    fmt.DateSeparator := '/';
    fmt.ShortDateFormat := 'mm/dd/yyyy';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ListTables(pType: Integer; ls: TStrings): Boolean;
begin
    Result := False;

    if (pType and 1024) <> 0 then
        if TableExists('SYS_LCT') then
            ls.Add('SYS_LCT');

    // OtherX
    if (pType and 512) <> 0 then
        if TableExists('DM_HOTRO') then
            ls.Add('DM_HOTRO');

    if (pType and 256) <> 0 then
        if TableExists('DM_HOTRO_NHOM') then
            ls.Add('DM_HOTRO_NHOM');

    if (pType and 128) <> 0 then
        if TableExists('DM_KHAC2') then
            ls.Add('DM_KHAC2');

    if (pType and 64) <> 0 then
        if TableExists('DM_KHACX') then
            ls.Add('DM_KHACX');

    // Dbe
    if (pType and 32) <> 0 then
    begin
        ls.Add('SYS_DBE_OBJ');
        ls.Add('SYS_DBE_MSG');
    end;

    // Reports
    if (pType and 16) <> 0 then
        ls.Add('SYS_REPORT');

    // Functions
    if (pType and 8) <> 0 then
        ls.Add('SYS_FUNC');

    // Flexible
    if (pType and 4) <> 0 then
    begin
        ls.Add('SYS_FLEXCONFIG');
    end;

    // Dictionary
    if (pType and 2) <> 0 then
        ls.Add('SYS_DICTIONARY');

    // Grid
    if (pType and 1) <> 0 then
        ls.Add('SYS_GRID');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.Logon: Boolean;
begin
	Result := False;
    DataMain.Conn.Close;

  // Logon
    with isLogon do
    begin
        if not Logon2('SZ') then
        begin
            Exit;
        end;
    end;

    sysDbConnection := Conn;

	wwIntl.Connected := not sysEnglish;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CopyData(qrSource, qrDest: TADOQuery);
var
    i, n: Integer;
begin
    // Export
    with qrDest do
    begin
        n := qrSource.FieldCount - 1;
        while not qrSource.Eof do
        begin
            Append;
            for i := 0 to n do
                if Fields[i].DataType <> ftAutoInc then
                begin
                    FieldByName(qrSource.Fields[i].FullName).Value := qrSource.Fields[i].Value;
                end;
            Post;
            qrSource.Next;
        end;
        Close;
    end;
    qrSource.Close;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

(*==============================================================================
** Init XML
**------------------------------------------------------------------------------
*)
function TDataMain.CreateXml: TNativeXml;
begin
	Result := TNativeXml.CreateName('ApplicationConfigs');
	with Result do
	begin
		EncodingString := 'Utf-8';
		Root.AttributeAdd('Exportby', ExtractFileName(Application.ExeName));
        Root.AttributeAdd('Version', VersionString);
        Root.AttributeAdd('Source', isNameServer + '.' + isDatabaseServer);
	end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlCreateNode(pXml: TNativeXml; tabname: String);
var
    i, n: Integer;
    sField: String;
    nodeR: TXmlNode;
begin
    // Table
    pXml.Root.NodeNew(tabname);

    // Column & Data
    with QrSql do
    begin
        if tabname = 'DM_KHAC2' then
            SQL.Text := 'select	b.* from	DM_KHACX a join DM_KHAC2 b on a.LOAI = b.LOAI where	a.[SYS] = 2'
        else if tabname = 'DM_HOTRO' then
            SQL.Text := 'select	b.* from	DM_HOTRO_NHOM a join DM_HOTRO b on a.MANHOM_HOTRO = b.MANHOM_HOTRO where	a.[SYS] = 0'
        else
            SQL.Text := SELECT_SQL + tabname;
        Open;
        n := FieldCount - 1;
        while not Eof  do
        begin
            nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
            for i := 0 to n do
                if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                begin
                    sField := Fields[i].FullName;
                    nodeR.NodeNew(sField).ValueAsString :=FieldByName(sField).AsString;
                end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String);
var
    i: Integer;
    ls: TStrings;

    (*
    **
    *)
    function TableKey(pTabname: string): string;
    begin
        if (pTabname = 'SYS_GRID') then
            Result := 'NAME'
        else
        if (pTabname = 'SYS_DICTIONARY') then
            Result := 'TABLE;FIELD'
        else
        if (pTabname = 'SYS_FLEXCONFIG') then
            Result := 'Section;Item'
        else
        if (pTabname = 'SYS_FUNC') then
            Result := 'FUNC_CODE'
        else
        if (pTabname = 'SYS_REPORT') then
            Result := 'REP_NAME'
        else
        if (pTabname = 'SYS_DBE_MSG') then
            Result := 'ERROR'
        else
        if (pTabname = 'SYS_DBE_OBJ') then
            Result := 'KEY'
        else
        if (pTabname = 'DM_KHACX')then
            Result := 'LOAI'
        else
        if (pTabname = 'DM_KHAC2')then
            Result := 'LOAI;MA'
        else
        if (pTabname =  'DM_HOTRO_NHOM')  then
            Result := 'MANHOM_HOTRO'
        else
        if (pTabname =  'DM_HOTRO')  then
            Result := 'MANHOM_HOTRO;MA_HOTRO'
        else
            Result := '';
    end;

    procedure CreateNode(pXml: TNativeXml; tabname: String);
    var
        i, n: Integer;
        sField: String;
        nodeR: TXmlNode;
    begin
        // Table
        pXml.Root.NodeNew(tabname);
        if (tabname = 'DM_KHACX') or (tabname = 'DM_HOTRO_NHOM') or
            (tabname = 'DM_KHAC2') or (tabname = 'DM_HOTRO')
        then
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 0)
        else
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 1);

        pXml.Root.NodeByName(tabname).AttributeAdd('Key', TableKey(tabname));

        // Column & Data
        with QrSql do
        begin
            if tabname = 'DM_KHAC2' then
                SQL.Text := 'select	b.* from	DM_KHACX a join DM_KHAC2 b on a.LOAI = b.LOAI where	a.[SYS] = 1'
            else if tabname = 'DM_HOTRO' then
                SQL.Text := 'select	b.* from	DM_HOTRO_NHOM a join DM_HOTRO b on a.MANHOM_HOTRO = b.MANHOM_HOTRO where	a.[SYS] = 0'
            else
                SQL.Text := SELECT_SQL + tabname;

            Open;
            n := FieldCount - 1;

            while not Eof  do
            begin
                nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
                for i := 0 to n do
                    if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                    begin
                        sField := Fields[i].FullName;
                        nodeR.NodeNew(sField).ValueAsString := (FieldByName(sField).AsString);
//                        nodeR.NodeNew(sField).ValueAsString := Encrypt6(FieldByName(sField).AsString);
                    end;
                Next;
            end;
        end;
    end;

begin
    mEximPath := pFile;
    Screen.Cursor := crSQLWait;
    ls := TStringList.Create;

    ListTables(pTable, ls);
    for i := 0 to ls.Count - 1 do
        CreateNode(pXml, ls[i]);

    ls.Free;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer) overload;
var
    i: Integer;
    data: string;
begin
    with pData do
    begin
        Append;
        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer) overload;
var
    i: Integer;
    data: string;
    sK1, sK2, sV1, sV2: string;
begin
    i := Pos(';', pKey);
    if i > 0 then
    begin
        sK1 := isLeft(pKey, i-1);
        sK2 := isRight(pKey, Length(pKey)- i);
    end else
        sK1 := pKey;

    sV1 := pNode.FindNode(sK1).ValueAsUnicodeString;
    if i > 0 then
        sV2 := pNode.FindNode(sK2).ValueAsUnicodeString;

    with pData do
    begin
        if i > 0 then
        begin
//            if Locate(pKey, VarArrayOf([Decrypt6(sV1), Decrypt6(sV2)]),[]) then
            if Locate(pKey, VarArrayOf([(sV1), (sV2)]),[]) then
                SetEditState(pData)
            else
                Append;
        end else
        begin
//            if Locate(pKey, Decrypt6(sV1), []) then
            if Locate(pKey, (sV1), []) then
                SetEditState(pData)
            else
                Append;
        end;

        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;

    NodeName := isStrReplace(pField.FieldName,' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := node.ValueAsString;
    ftWideString, ftWideMemo:
        pField.AsString := node.ValueAsUnicodeString;
    ftBoolean:
        pField.AsBoolean := node.ValueAsBool;
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := node.ValueAsInteger;
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := node.ValueAsFloat;
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(node.ValueAsString, fmt);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
    data: string;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;
    NodeName := isStrReplace(pField.FieldName, ' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
//    data := Decrypt6(node.ValueAsUnicodeString);
    data := (node.ValueAsUnicodeString);
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := data;
    ftWideString, ftWideMemo:
        pField.AsString := data;
    ftBoolean:
        pField.AsBoolean := StrToBoolDef(data, False);
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := StrToIntDef(data, 0)
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := StrToFloatDef(data, 0.0);
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(data, fmt);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: Integer);
var
    lsTB: TStrings;
    i, j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
    bFull: Boolean;
    sKey: string;
begin
    lsTB := TStringList.Create;
    LsNode := TXmlNodeList.Create;
    ListTables(pTable, lsTB);

    StartProgress(lsTB.Count, 'Import');

    //No check constraint
    Conn.Execute(
        'alter table SYS_RIGHT nocheck constraint all'#13 +
        'alter table SYS_RIGHT_REP nocheck constraint all');

    // Import
    for i := 0 to lsTB.Count - 1 do
    begin
        SetProgressDesc(lsTB[i]);
        with QrSql do
        begin
            SQL.Text := SELECT_SQL + lsTB[i];
            Open;
        end;
        node := pXml.Root.FindNode(lsTB[i]);
        if node <> nil then
        begin
            bFull := StrToBoolDef(node.AttributeByName['Full'], False);
            if SameText(lsTB[i], 'SYS_CONFIG') then
            begin
                if bFull then
                    with QrSql do
                    begin
                        node := pXml.Root.NodeByName(lsTB[i]).FindNode('ROW').FindNode('FLEXIBLE');
                        if node <> nil then
                        begin
                            Edit;
//                            FieldByName('FLEXIBLE').Value := Decrypt6(node.ValueAsString);
                            FieldByName('FLEXIBLE').Value := (node.ValueAsString);
                            Post;
                        end;
                        Close;
                    end
                else
                    Continue;
            end
            else
            begin
                if bFull then
                begin
                    Conn.Execute('delete ' + lsTB[i]);
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, 0);
                        end;
                    end;
                end else
                begin
                    sKey := node.AttributeByName['Key'];
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, sKey, 0);
                        end;
                    end;
                end;
            end;
        end;
    end;

    // Check constraint
    Conn.Execute(
        'alter table SYS_RIGHT check constraint all'#13 +
        'alter table SYS_RIGHT_REP check constraint all');

    // CLean up
    Conn.Execute('delete SYS_RIGHT where FUNC_CODE not in (select FUNC_CODE from SYS_FUNC)');
    Conn.Execute('delete SYS_RIGHT_REP where REP_CODE not in (select REP_CODE from SYS_REPORT)');

    lsTB.Free;
    pXml.Free;
	StopProgress;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: string);
var
    j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
begin
    LsNode := TXmlNodeList.Create;
    // Import
    with QrSql do
    begin
        SQL.Text := SELECT_SQL + pTable;
        Open;
    end;

    node := pXml.Root.FindNode(pTable);
    if node <> nil then
    begin
        Conn.Execute('delete ' + pTable);
        with QrSql do
        begin
            pXml.Root.NodeByName(pTable).FindNodes('ROW', LsNode);
            for j := 0 to LsNode.Count - 1 do
                xmlNodeToDs(LsNode.Items[j], QrSql, 0);
        end;
    end;
    LsNode.Free;
end;

end.
