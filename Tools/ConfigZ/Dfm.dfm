object FrmDfm: TFrmDfm
  Left = 0
  Top = 0
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  BorderWidth = 5
  Caption = 'Ch'#7881'nh S'#7917'a Forms'
  ClientHeight = 196
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    502
    196)
  PixelsPerInch = 96
  TextHeight = 16
  object SpeedButton1: TSpeedButton
    Left = 340
    Top = 142
    Width = 78
    Height = 24
    Cursor = 1
    Action = CmdRun
    Anchors = [akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FF66CCCCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC0099CCFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF66CCCC0099CCFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99
      FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF66CCCC0099CC0099CC0099CC66FFFF0099CCFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99FFFF66FFFF66
      FFFF66FFFF66FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF66CCCC99FFFF66FFFF0099CC66CCCC66CCCC66CCCCFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99FFFF66
      FFFF66FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      66CCCC0099CC0099CC0099CC0099CC99FFFF66FFFF66FFFF0099CCFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99FFFF99FFFF66FFFF66FFFF66
      FFFF66FFFF66FFFF66FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FF66CCCC99FFFF99FFFF66FFFF66FFFF0099CC66CCCC66CCCC66CCCCFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99FFFF99FFFF99FFFF66
      FFFF66FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF66CCCC99FFFF99FFFF99FFFF66FFFF66FFFF0099CCFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC99FFFF99FFFF99
      FFFF66FFFF66FFFF66FFFF0099CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF66CCCC99FFFF99FFFF99FFFF66FFFF66FFFF66FFFF0099
      CCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF66CCCC66CCCC66
      CCCC66CCCC66CCCC66CCCC66CCCC66CCCC66CCCCFF00FFFF00FF}
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object SpeedButton2: TSpeedButton
    Left = 424
    Top = 142
    Width = 78
    Height = 24
    Cursor = 1
    Action = CmdClose
    Anchors = [akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFE2EFF1FF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE2EFF1E5E5E5CCCCCCE5E5E5E2
      EFF1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE2EFF1
      E5E5E5B2B2B2CC9999996666996666B2B2B2CCCCCCE5E5E5E2EFF1FF00FFFF00
      FFFF00FFFF00FFFF00FFE5E5E5CC9999996666CC9999CC9999FFFFFF99666699
      9999999999B2B2B2E5E5E5FF00FFFF00FFFF00FFFF00FFFF00FF996666CC9999
      FFCC99FFCC99FFCCCCFFFFFF996666336699336699336699E2EFF1FF00FFFF00
      FFFF00FFFF00FFFF00FF996666FFCC99FFCC99FFCC99FFCCCCFFFFFF99666666
      CCCC66CCCC0099CCFFFFFFFFCCCCFF00FFFF00FFFF00FFFF00FF996666FFCC99
      FFCC99FFCC99FFCCCCFFFFFF99666666CCCC66CCFF3399CCFFCCCCCC6600FF00
      FFFF00FFFF00FFFF00FF996666FFCC99CC9999CC9966FFCCCCFFFFFF99666699
      CCCC99CCFFB2B2B2FF6600CC6600FF00FFFF00FFFF00FFFF00FF996666FFCC99
      996666FFFFFFFFCCCCFFFFFF99666699CCCCC0C0C0CC6600CC6600CC6600CC66
      00CC6600CC6600FF00FF996666FFCC99CC9999996666FFCCCCFFFFFF996666FF
      00FFCC6600CC6600CC6600CC6600CC6600CC6600CC6600FF00FF996666FFCC99
      FFCC99FFCC99FFCCCCFFFFFF996666FF00FFCC9999CC6600CC6600CC6600CC66
      00CC6600CC6600FF00FF996666FFCC99FFCC99FFCC99FFCCCCFFFFFF996666CC
      CCCCE2EFF1CC9999FF6600CC6600FF00FFFF00FFFF00FFFF00FF996666FFCC99
      FFCC99FFCC99FFCCCCFFFFFF99666699CCCCFF00FF99CCCCFFCC99CC6600FF00
      FFFF00FFFF00FFFF00FF996666CC9999FFCC99FFCC99FFCCCCFFFFFF996666CC
      CCCCFF00FF3399CCFF00FFFFCC99FF00FFFF00FFFF00FFFF00FFFF00FFC0C0C0
      CC9966CC9999CCCC99FFFFFF9966660099CC0099CC0099CCFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCCCCCCCC9999996666996666FF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 173
    Width = 502
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '100'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    SizeGrip = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 502
    Height = 134
    Align = alTop
    TabOrder = 0
    object TntLabel1: TLabel
      Left = 71
      Top = 23
      Width = 36
      Height = 16
      Alignment = taRightJustify
      Caption = 'Folder'
    end
    object btSelectFile: TSpeedButton
      Left = 464
      Top = 20
      Width = 25
      Height = 24
      Cursor = 1
      Action = CmdSelectDir
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FF00000000000000000000000000000000000000000000
        00000000000000840029CE00299C00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF31EFFF00CEFF00BDEF00CEFF00ADDE00BDEF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF63FFFF00CEFF31EFFF00CEFF00BDEF00
        CEFF00ADDE0000845ACEFF0063FF00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF63FFFF00CEFF31EFFF00CEFF00BDEF00CEFF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF9CFFFF00CEFF63FFFF00CEFF31EFFF00
        CEFF00BDEF0000845ACEFF0063FF00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF9CFFFF00BDEF63FFFF00CEFF31EFFF00CEFF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF9CFFFF9CFFFF9CFFFF9CFFFF9CFFFF9C
        FFFF9CFFFF0000849CFFFF0063FF00299C000000FF00FFFF00FF006363006363
        006363006363006363006363006363006363006363000084CEFFFF5ACEFF0029
        9C000000FF00FFFF00FFFF00FF006363F7FFFF9CFFFF9CFFFFADCECE000000FF
        00FFFF00FF008484000084000084000084FF00FFFF00FFFF00FFFF00FFFF00FF
        006363006363006363000000FF00FFFF00FFFF00FFFF00FF424242F7CEA50000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF73
        7373393939FF00FF424242F7CEA5000000FF00FF000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF737373FFEFCE3939394242427B7B7BF7CEA59C29
        290000009C2929000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF737373FF
        FFFFCECECECECECECECECEFFFFFFCECECECECECEF7CEA5000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF737373FFFFFFF7CEA5000000FFFFFFFFFFFFFFEF
        CEFFEFCE000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF73
        7373737373E7DEE7737373737373737373737373FF00FFFF00FF}
      ParentFont = False
    end
    object LbAciton: TLabel
      Left = 301
      Top = 51
      Width = 42
      Height = 16
      Alignment = taRightJustify
      Caption = 'Actinon'
    end
    object LbName: TLabel
      Left = 41
      Top = 77
      Width = 66
      Height = 16
      Alignment = taRightJustify
      Caption = 'Class name'
    end
    object LbProperty: TLabel
      Left = 295
      Top = 77
      Width = 48
      Height = 16
      Alignment = taRightJustify
      Caption = 'Property'
    end
    object LbValue: TLabel
      Left = 48
      Top = 102
      Width = 59
      Height = 16
      Alignment = taRightJustify
      Caption = 'New value'
    end
    object LbFileExt: TLabel
      Left = 66
      Top = 51
      Width = 41
      Height = 16
      Alignment = taRightJustify
      Caption = 'File Ext'
    end
    object EdDir: TEdit
      Left = 113
      Top = 20
      Width = 348
      Height = 24
      TabStop = False
      Color = clInfoBk
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object CbAciton: TComboBox
      Left = 349
      Top = 46
      Width = 140
      Height = 24
      ItemIndex = 0
      TabOrder = 2
      Text = 'Utf8'
      OnChange = CbAcitonChange
      Items.Strings = (
        'Utf8'
        'Setval'
        'Fsetval'
        'Setval2'
        'Fsetval2')
    end
    object EdValue: TEdit
      Left = 113
      Top = 98
      Width = 376
      Height = 24
      TabOrder = 5
    end
    object CbFileExt: TComboBox
      Left = 113
      Top = 46
      Width = 140
      Height = 24
      TabOrder = 1
      Text = '*.dfm'
      OnChange = CbFileExtChange
      Items.Strings = (
        '*.dfm'
        '*.bak'
        '*.pas'
        '*.txt'
        '*.*')
    end
    object EdName: TEdit
      Left = 113
      Top = 72
      Width = 140
      Height = 24
      TabOrder = 3
      Text = '*'
    end
    object EdProperty: TEdit
      Left = 349
      Top = 72
      Width = 140
      Height = 24
      TabOrder = 4
      Text = 'Caption'
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 113
    Top = 139
    object CmdSelectDir: TAction
      Hint = 'Select folder'
      ShortCut = 118
      OnExecute = CmdSelectDirExecute
    end
    object CmdRun: TAction
      Caption = 'Run'
      Hint = 'Run'
      ShortCut = 120
      OnExecute = CmdRunExecute
    end
    object CmdClose: TAction
      Caption = 'Close'
      Hint = 'Close form'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
  end
end
