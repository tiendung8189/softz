﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit RepConfig;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  Wwdbgrid, Db, ADODB, ComCtrls, ActnList, Menus, Windows,
  Wwdbcomb, AppEvnts, wwDBGrid2, AdvMenus, StdCtrls, Mask, wwdbedit, Wwdotdot,
  Grids, Wwdbigrd, Vcl.DBCtrls, Vcl.ExtCtrls, RzPanel, RzSplit, isPanel,
  Vcl.ToolWin, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmRepConfig = class(TForm)
    Status: TStatusBar;
    QrRep: TADOQuery;
    DsRep: TDataSource;
    ActionList1: TActionList;
    CmdClose: TAction;
    PopFunc: TAdvPopupMenu;
    CmdEx: TAction;
    CmdIm: TAction;
    SaveDlg: TSaveDialog;
    OpenDlg: TOpenDialog;
    ApplicationEvents1: TApplicationEvents;
    SYNC_REP_RIGHTS: TADOCommand;
    Import1: TMenuItem;
    N1: TMenuItem;
    Export1: TMenuItem;
    CmdShow: TAction;
    tcboco1: TMenuItem;
    CmdSearch: TAction;
    N3: TMenuItem;
    CmdReRead: TAction;
    CmdEmpty: TAction;
    N4: TMenuItem;
    Xatonbboco1: TMenuItem;
    RzSizePanel1: TRzSizePanel;
    isPanel1: TisPanel;
    ToolBar1: TToolBar;
    ToolButton3: TToolButton;
    ToolButton1: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    CmdSave: TAction;
    CmdDel: TAction;
    CmdCancel: TAction;
    EdGridWeb: TDBMemo;
    QrSysFuncCat: TADOQuery;
    DsSysFuncCat: TDataSource;
    QrRepCAT: TIntegerField;
    QrRepGRID_WEB: TWideMemoField;
    GrList: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
	procedure CmdCloseExecute(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure CmdExExecute(Sender: TObject);
	procedure CmdImExecute(Sender: TObject);
	procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
	procedure QrRepBeforePost(DataSet: TDataSet);
	procedure FormShow(Sender: TObject);
	procedure QrRepAfterPost(DataSet: TDataSet);
	procedure QrRepBeforeDelete(DataSet: TDataSet);
	procedure CmdShowExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure QrRepREP_NAMEChange(Sender: TField);
    procedure QrSysFuncCatBeforeOpen(DataSet: TDataSet);
  private
	mRet, mTrigger: Boolean;
    mCaption: String;
  public
	function  Execute(pTag: Integer = 0): Boolean;
  end;

var
  FrmRepConfig: TFrmRepConfig;

implementation

uses
	islib, isMsg, isDb, ExCommon, MainData, isCommon, isType, isStr;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRepConfig.Execute;
begin
	// Report cat.
	mRet := False;
	ShowModal;
	Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    AddAllFields(QrRep, 'SYS_REPORT');
	mTrigger := False;
	mCaption := Caption;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.FormShow(Sender: TObject);
var
	it: TMenuItem;
begin
	// Report group names
     OpenDataSets([QrRep, QrSysFuncCat]);

     with QrSysFuncCat do
     if not IsEmpty then
     begin
         First;
         while not Eof do
         begin;
             it := TMenuItem.Create(Self);
             with it do
             begin
                 Caption := FieldByName('VALUE').AsString;
                 Tag := FieldByName('KEY').AsInteger;
                 RadioItem := True;
                 Hint := FieldByName('KEY').AsString;
                 OnClick := CmdShowExecute;
                 PopFunc.Items.Add(it);
             end;
             Next;
         end;
     end;
     buildGriEh('SYS_REPORT', GrList, True);
     CmdShow.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	// Save grid state
	//GrList.SaveToIniFile;

	// Save change
	with QrRep do
	begin
		CheckBrowseMode;
		Close;
	end;

	// Synchro rights
	if mRet then
	begin
		Wait(PREPARING);
		SYNC_REP_RIGHTS.Execute;
		ClearWait;
	end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdCancelExecute(Sender: TObject);
begin
   QrRep.Cancel;
end;

procedure TFrmRepConfig.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_EMPTY    = 'Xóa toàn bộ báo cáo. Tiếp tục?';

procedure TFrmRepConfig.CmdEmptyExecute(Sender: TObject);
begin
    if not YesNo(RS_EMPTY, 1) then
        Exit;
    QrRep.DisableControls;
    EmptyDataset(QrRep);
    QrRep.EnableControls;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdExExecute(Sender: TObject);
begin
	if not SaveDlg.Execute then
		Exit;

	Wait(PROCESSING);
//	exXmlExport(QrREP, 'SYS_REPORT', SaveDlg.FileName);
	ClearWait;
	MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIM_IM        = 'Import từ file đã chọn. Tiếp tục?';

procedure TFrmRepConfig.CmdImExecute(Sender: TObject);
begin
	// Get file name
	if not OpenDlg.Execute then
		Exit;

	// Confirm
	if not YesNo(RS_CONFIM_IM, 1) then
		Exit;

	// Delete current list
	//Data.Conn.Execute('delete from SYS_REPORT');

	// Import
	with QrREP do
	begin
		//Requery;
		DisableControls;
		mTrigger := True;
//		exXmlImport(QrRep, OpenDlg.FileName, 'SYS_REPORT', 'REP_CODE');
		mTrigger := False;
		EnableControls;
	end;

	MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdReReadExecute(Sender: TObject);
begin
    Screen.Cursor := crSQLWait;
	with QrRep do
	begin
		Close;
		Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdSaveExecute(Sender: TObject);
begin
   QrRep.Post;
end;

procedure TFrmRepConfig.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsRep);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdShowExecute(Sender: TObject);
var
	n: Integer;
begin
	with QrRep do
	begin
        n := (Sender as TComponent).Tag;
		if (n < 0) then
		begin
			Filter := '';
			Caption := mCaption;
            tcboco1.Checked := True;
		end
		else
		begin
            Filter := Format('CAT=%d', [n]);
			Caption := mCaption + ' * ' + (Sender as TMenuItem).Caption;
            (Sender as TMenuItem).Checked := True;
		end;
        Filtered := Filter <> '';
        First;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
    b, b1: Boolean;
begin
    with QrRep do
    begin
        if not Active then
        	Exit;

        b  := not (State in [dsBrowse]);
        b1 := not IsEmpty;
    end;

    CmdSave.Enabled := b;
    CmdCancel.Enabled := b;
    CmdDel.Enabled := b1;
    CmdSearch.Enabled := not b;
end;

procedure TFrmRepConfig.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrRep);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.QrRepBeforePost(DataSet: TDataSet);
begin
	with (QrRep) do
	begin
		if BlankConfirm(QrRep, ['SORT']) then
			Abort;

		if FieldByName('REP_CODE').AsInteger = 0 then
			FieldByName('REP_CODE').AsInteger := FieldByName('SORT').AsInteger;

		if BlankConfirm(QrRep, ['REP_CODE']) then
			Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
	end;
end;




procedure TFrmRepConfig.QrRepREP_NAMEChange(Sender: TField);
begin
    with QrRep do
    begin
        if FieldByName('REP_NAME_WEB').AsString = '' then
                FieldByName('REP_NAME_WEB').AsString := FieldByName('REP_NAME').AsString;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.QrSysFuncCatBeforeOpen(DataSet: TDataSet);
begin
    QrSysFuncCat.Parameters[0].Value := Iif(sysEnglish, 'Report Categoriess EN', 'Report Categories');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.QrRepAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.QrRepBeforeDelete(DataSet: TDataSet);
begin
	if mTrigger then
		Exit;

	if not DeleteConfirm then
		Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepConfig.CmdDelExecute(Sender: TObject);
begin
    QrRep.Delete;
end;


end.
