object FrmDBGridCorrection: TFrmDBGridCorrection
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'wwDBGrid correcting'
  ClientHeight = 150
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 12
    Top = 4
    Width = 361
    Height = 97
    Caption = ' Options '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Ck1: TCheckBox
      Left = 27
      Top = 23
      Width = 97
      Height = 17
      Caption = 'Show cell hint'
      TabOrder = 0
    end
    object Ck2: TCheckBox
      Left = 27
      Top = 46
      Width = 177
      Height = 17
      Caption = 'Double click column sizing'
      TabOrder = 1
    end
    object Ck3: TCheckBox
      Left = 27
      Top = 68
      Width = 313
      Height = 17
      Caption = 'Cleanup missed match picture masks, control types'
      TabOrder = 2
    end
  end
  object btContinue: TBitBtn
    Left = 168
    Top = 112
    Width = 101
    Height = 25
    Cursor = 1
    Caption = 'Ti'#7871'p t'#7909'c'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
  end
  object btCancel: TBitBtn
    Left = 272
    Top = 112
    Width = 101
    Height = 25
    Cursor = 1
    Cancel = True
    Caption = 'Tho'#225't'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
  end
end
