﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExecuteSQL;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ActnList, StdCtrls, Buttons, ExtCtrls, Vcl.ComCtrls, RzEdit;

type
  TFrmExecuteSQL = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdFile: TEdit;
    SpeedButton1: TSpeedButton;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    CmdExecute: TAction;
    BitBtn3: TBitBtn;
    CmdBrowse: TAction;
    EdText: TRzRichEdit;
    procedure CmdExecuteExecute(Sender: TObject);
    procedure CmdBrowseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
	procedure ActionList1Update(Action: TBasicAction;
	  var Handled: Boolean);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

	procedure Proc;
  public
  end;

var
  FrmExecuteSQL: TFrmExecuteSQL;

implementation

{$R *.DFM}

uses
	isFile, isMsg, MainData;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.CmdExecuteExecute(Sender: TObject);
var
	i: Integer;
begin
	if not YesNo('Xác nhận Execute. Tiếp tục?', 1) then
		Exit;

    Proc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.CmdBrowseExecute(Sender: TObject);
var
	pFile: String;
    bs: TStrings;
begin
	pFile := isGetOpenFileName('SQL');
	if pFile <> '' then
		with EdFile do
		begin
			Text := pFile;
			SelectAll;
			SetFocus;
		end;

    bs := TStringList.Create;
    bs.LoadFromFile(pFile);

    EdText.Text := bs.Text;
    bs.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.FormShow(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.Proc;
var
    s: String;
begin
    s := EdText.Text;

    try
        DataMain.Conn.Execute(s);
        MsgDone;
    except on e: Exception do
        ErrMsg(e.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExecuteSQL.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	b := Trim(EdText.Text) <> '';
	CmdExecute.Enabled := b;
end;

end.
