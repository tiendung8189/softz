(*==============================================================================
**------------------------------------------------------------------------------
*)
unit RepEncode;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ActnList, StdCtrls, Buttons, ExtCtrls;

type
  TFrmRepEncode = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdFolder: TEdit;
    SpeedButton1: TSpeedButton;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    CmdEncrypt: TAction;
    CmdDecrypt: TAction;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    CmdBrowse: TAction;
    procedure CmdEncryptExecute(Sender: TObject);
    procedure CmdDecryptExecute(Sender: TObject);
    procedure CmdBrowseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
	procedure ActionList1Update(Action: TBasicAction;
	  var Handled: Boolean);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	mList: TStrings;

	procedure List(pExt: String);
	procedure Proc(pName: String; pEncode: Boolean);
  public
  end;

var
  FrmRepEncode: TFrmRepEncode;

implementation

{$R *.DFM}

uses
	isFile, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.CmdEncryptExecute(Sender: TObject);
var
	i: Integer;
begin
	if not YesNo('All of reports on this folder will be encoded. Continue?', 1) then
		Exit;

	List('rpt');
	StartProgress(mList.Count);
	for i := 0 to mList.Count - 1 do
	begin
		Proc(mList.Strings[i], True);
		IncProgress;
	end;
	StopProgress;

	if YesNo('Delete all of original files?', 1) then
		for i := 0 to mList.Count - 1 do
			DeleteFile(mList.Strings[i]);

	EdFolder.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.CmdDecryptExecute(Sender: TObject);
var
	i: Integer;
begin
	if not YesNo('All of encoded reports on this folder will be dencoded. Continue?', 1) then
		Exit;

	List('szr');
	StartProgress(mList.Count);
	for i := 0 to mList.Count - 1 do
	begin
		Proc(mList.Strings[i], False);
		IncProgress;
	end;
	StopProgress;

	if YesNo('Delete all of original files?', 1) then
		for i := 0 to mList.Count - 1 do
			DeleteFile(mList.Strings[i]);

	EdFolder.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.CmdBrowseExecute(Sender: TObject);
var
	s: String;
begin
	s := isGetDir('', '');
	if s <> '' then
		with EdFolder do
		begin
			Text := s;
			SelectAll;
			SetFocus;
		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.FormShow(Sender: TObject);
begin
	EdFolder.Text := GetCurrentDir;
	mList := TStringList.Create;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	mList.Free;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.List(pExt: String);
var
	dta: TSearchRec;
	n: Integer;
	s: String;
begin
	mList.Clear;
	s := SysUtils.IncludeTrailingPathDelimiter(Trim(EdFolder.Text));
	n := FindFirst(s + '*.' + pExt, faAnyFile, dta);
	while n = 0 do
    begin
		mList.Add(s + dta.Name);
		n := FindNext(dta);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.Proc;
var
    fp: TMemoryStream;
    s: String;
    msk: Byte;
    p: PByte;
    i: Integer;
begin
	fp := TMemoryStream.Create;
    fp.LoadFromFile(pName);

    msk := 0;
    p := fp.Memory;
    for i := 1 to fp.Size do
    begin
        msk := (msk + 1) mod 256;
        p^ := p^ xor msk;
        Inc(p);
    end;

    if pEncode then
    	s := ChangeFileExt(pName, '.szr')
    else
    	s := ChangeFileExt(pName, '.rpt');
    fp.SaveToFile(s);
    fp.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepEncode.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	b := Trim(EdFolder.Text) <> '';
	CmdEncrypt.Enabled := b;
	CmdDecrypt.Enabled := b;
end;

end.
