object FrmDbeSetup: TFrmDbeSetup
  Left = 0
  Top = 0
  Caption = 'C'#225'c Th'#244'ng B'#225'o L'#7895'i - Database Error Messages'
  ClientHeight = 573
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel2: TBevel
    Left = 0
    Top = 36
    Width = 1016
    Height = 1
    Align = alTop
    Shape = bsSpacer
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 1016
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object PgMain: TPageControl
    Left = 0
    Top = 37
    Width = 1016
    Height = 513
    Cursor = 1
    ActivePage = TabSheet2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet1: TTabSheet
      Caption = ' Th'#244'ng b'#225'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
      object GrMsgs: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 1008
        Height = 485
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'ERROR'#9'31'#9'M'#227' l'#7895'i'#9'F'
          'DESCRIPTION'#9'53'#9'Ti'#7871'ng Vi'#7879't'#9'F'
          'DESCRIPTION1'#9'53'#9'Ti'#7871'ng Anh'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        Align = alClient
        Color = clWhite
        DataSource = DsMsgs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
        ParentFont = False
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrMsgsCalcCellColors
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 14474460
      end
    end
    object TabSheet2: TTabSheet
      Caption = #272#7889'i t'#432#7907'ng'
      ImageIndex = 1
      object GrObjs: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 1008
        Height = 485
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'KEY'#9'31'#9'Kh'#243'a'#9'F'
          'DESCRIPTION'#9'53'#9'Ti'#7871'ng Vi'#7879't'#9'F'
          'DESCRIPTION1'#9'53'#9'Ti'#7871'ng Anh'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        Align = alClient
        DataSource = DsObjs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
        ParentFont = False
        PopupMenu = TntPopupMenu1
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrObjsCalcCellColors
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 14474460
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1016
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 63
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdGetObject
      ImageIndex = 9
    end
    object ToolButton4: TToolButton
      Left = 63
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 71
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 208
    Top = 176
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdGetObject: TAction
      Caption = #272#7889'i t'#432#7907'ng'
      Hint = 'L'#7845'y t'#7845't c'#7843' kh'#243'a trong database'
      OnExecute = CmdGetObjectExecute
    end
  end
  object QrMsgs: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrMsgsBeforePost
    OnDeleteError = QrMsgsPostError
    OnEditError = QrMsgsPostError
    OnPostError = QrMsgsPostError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from'#9'SYS_DBE_MSG')
    Left = 94
    Top = 177
    object QrMsgsERROR: TStringField
      FieldName = 'ERROR'
      Size = 50
    end
    object QrMsgsDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 250
    end
    object QrMsgsDESCRIPTION1: TWideStringField
      FieldName = 'DESCRIPTION1'
      Size = 250
    end
  end
  object DsMsgs: TDataSource
    DataSet = QrMsgs
    Left = 94
    Top = 205
  end
  object QrObjs: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrObjsBeforePost
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from'#9'SYS_DBE_OBJ')
    Left = 122
    Top = 177
    object QrObjsKEY: TStringField
      DisplayLabel = 'Kh'#243'a'
      FieldName = 'KEY'
      Size = 50
    end
    object QrObjsDESCRIPTION: TWideStringField
      DisplayLabel = 'Ti'#7871'ng vi'#7879't'
      FieldName = 'DESCRIPTION'
      Size = 250
    end
    object QrObjsDESCRIPTION1: TWideStringField
      DisplayLabel = 'Ti'#7871'ng anh'
      FieldName = 'DESCRIPTION1'
      Size = 250
    end
  end
  object DsObjs: TDataSource
    DataSet = QrObjs
    Left = 122
    Top = 205
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 248
    Top = 175
  end
  object TntPopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 300
    Top = 182
    object mmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object OpenDlg: TOpenDialog
    OnShow = OpenDlgShow
    Filter = 'System Setting (*.xml, *.txt)|*.xml;*.txt|All Files (*.*)|*.*'
    Left = 364
    Top = 174
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.xml'
    Filter = 'System Setting (*.xml)|*.xml|All Files (*.*)|*.*'
    Left = 400
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsObjs
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'KEY'
      'DESCRIPTION'
      'DESCRIPTION1')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 436
    Top = 178
  end
end
