object FrmVersionUpdate: TFrmVersionUpdate
  Left = 176
  Top = 108
  Caption = 'Phi'#234'n B'#7843'n Ph'#7847'n M'#7873'm - Version'
  ClientHeight = 573
  ClientWidth = 1116
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 369
    Top = 36
    Width = 2
    Height = 514
    OnMoved = Splitter1Moved
    ExplicitLeft = 221
    ExplicitTop = 37
    ExplicitHeight = 517
  end
  object GrMaster: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 369
    Height = 514
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'VersionDate'#9'18'#9'Ng'#224'y c'#7853'p nh'#7853't'#9'F'
      'Version'#9'15'#9'Phi'#234'n b'#7843'n c'#7853'p nh'#7853't'#9'F'
      'VersionKey'#9'12'#9'File ch'#237'nh'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alLeft
    Color = clWhite
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = TntPopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1116
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton2: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDelete
      ImageIndex = 3
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdQuit
      ImageIndex = 5
    end
  end
  object PaDetail: TisPanel
    Left = 371
    Top = 36
    Width = 745
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    Color = 16119285
    ParentBackground = False
    TabOrder = 2
    HeaderCaption = ' :: '
    HeaderColor = 8404992
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindow
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object spl1: TSplitter
      Left = 0
      Top = 368
      Width = 745
      Height = 2
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 367
      ExplicitWidth = 569
    end
    object GrDetail: TwwDBGrid2
      Left = 0
      Top = 16
      Width = 745
      Height = 352
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'VISIBLE;CheckBox;True;False'
        'Visible;CheckBox;True;False'
        'IMG1;ImageIndex;Original Size')
      Selected.Strings = (
        'FileName'#9'25'#9'FileName'#9'T'
        'IMG1'#9'4'#9#9'T'
        'FileSubFolder'#9'15'#9'FileSubFolder'#9'F'
        'FileVersion'#9'25'#9'FileVersion'#9'T'
        'FileChecksum'#9'25'#9'FileChecksum'#9'T'
        'Idx'#9'38'#9'Idx'#9'T')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 2
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopRight
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      ImageList = DataMain.ImageMark
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 16119285
    end
    object vlPanel1: TisPanel
      Left = 0
      Top = 370
      Width = 745
      Height = 144
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'x'
      Color = 16119285
      ParentBackground = False
      TabOrder = 2
      HeaderCaption = ' :: Comment'
      HeaderColor = 7617536
      ImageSet = 4
      RealHeight = 0
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindow
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object EdComment: TDBMemo
        Left = 0
        Top = 16
        Width = 745
        Height = 128
        Align = alClient
        DataField = 'Comment'
        DataSource = DsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 1116
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object QrDetail: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = QrDetailBeforeOpen
    BeforeInsert = QrDetailBeforeInsert
    AfterInsert = QrDetailAfterInsert
    BeforePost = QrDetailBeforePost
    BeforeDelete = QrDetailBeforeDelete
    OnCalcFields = QrDetailCalcFields
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'SYS_VERSION')
    Left = 415
    Top = 71
    object QrDetailIdx: TGuidField
      FieldName = 'Idx'
      FixedChar = True
      Size = 38
    end
    object QrDetailCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDetailUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDetailCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDetailUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDetailROWGUID: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object QrDetailVersion: TWideStringField
      FieldName = 'Version'
      Size = 50
    end
    object QrDetailVersionDate: TDateTimeField
      FieldName = 'VersionDate'
    end
    object QrDetailVersionType: TIntegerField
      FieldName = 'VersionType'
    end
    object QrDetailFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrDetailFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrDetailFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrDetailFileSubFolder: TWideStringField
      FieldName = 'FileSubFolder'
      Size = 50
    end
    object QrDetailFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrDetailFileVersion: TWideStringField
      FieldName = 'FileVersion'
      Size = 50
    end
    object QrDetailComment: TWideMemoField
      FieldName = 'Comment'
      BlobType = ftWideMemo
    end
    object QrDetailIMG1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG1'
      Calculated = True
    end
    object QrDetailVersionKey: TWideStringField
      FieldName = 'VersionKey'
      Size = 50
    end
    object QrDetailVersionFolder: TWideStringField
      FieldName = 'VersionFolder'
      Size = 50
    end
    object QrDetailFileType: TIntegerField
      FieldName = 'FileType'
    end
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = QrMasterAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'select distinct [Version], [VersionDate], [VersionKey], [Version' +
        'Folder]'
      '  from '#9'SYS_VERSION'
      'order by '#9'[VersionDate] desc, [Version] desc')
    Left = 387
    Top = 71
    object QrMasterVersion: TWideStringField
      FieldName = 'Version'
      Size = 50
    end
    object QrMasterVersionDate: TDateTimeField
      FieldName = 'VersionDate'
    end
    object QrMasterVersionKey: TWideStringField
      FieldName = 'VersionKey'
      Size = 50
    end
    object QrMasterVersionFolder: TWideStringField
      FieldName = 'VersionFolder'
      Size = 50
    end
  end
  object DsDetail: TDataSource
    DataSet = QrDetail
    Left = 415
    Top = 99
  end
  object DsMaster: TDataSource
    DataSet = QrMaster
    Left = 387
    Top = 99
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 120
    Top = 200
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDelete: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDeleteExecute
    end
    object CmdVisibleFields: TAction
      Tag = 10
      Category = 'DETAIL'
      Caption = 'Visible Fields'
    end
    object CmdSearchM: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchMExecute
    end
    object CmdSearchD: TAction
      Category = 'DETAIL'
      Caption = 'T'#236'm m'#7851'u tin...'
      OnExecute = CmdSearchDExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u...'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdQuit: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdReload: TAction
      Caption = #272#7885'c l'#7841'i t'#7915' database'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdFilteredFields: TAction
      Tag = 11
      Category = 'DETAIL'
      Caption = 'Filter Fields'
    end
    object CmdNative: TAction
      Tag = 12
      Category = 'DETAIL'
      Caption = 'Native Description'
    end
    object Cmd2nd: TAction
      Tag = 13
      Category = 'DETAIL'
      Caption = '2nd Description'
    end
    object CmdFormatedFields: TAction
      Tag = 14
      Category = 'DETAIL'
      Caption = 'Formated Fields'
    end
    object CmdClearAll: TAction
      Category = 'DETAIL'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearAllExecute
    end
    object CmdSpecialFilter: TAction
      Category = 'DETAIL'
      Caption = 'L'#7885'c '#273#7863'c bi'#7879't'
    end
    object CmdSwitch: TAction
      Hint = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm Section'
      OnExecute = CmdNewExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDetail
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'FIELD'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'FIELD'
      'COMMENT'
      'DESCRIPTION'
      'DESCRIPTION1'
      'FILTER'
      'FORMAT')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 64
    Top = 200
  end
  object PopRight: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 92
    Top = 200
    object Tm1: TMenuItem
      Action = CmdSearchD
      ImageIndex = 31
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ClearAllFiltered1: TMenuItem
      Action = CmdClearAll
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 456
    Top = 243
  end
  object TntPopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 92
    Top = 228
    object Search1: TMenuItem
      Action = CmdSearchM
      ImageIndex = 31
    end
  end
end
