(*==============================================================================**------------------------------------------------------------------------------
*)
unit ExCommon;

interface
uses
    SysUtils, Classes, Forms, Db, ADODB, DBGridEh;

const
	ES_INVALID_PROJECT = 'Invalid Project. Function Abort.';

var
    sysIni: String;
    fmt: TFormatSettings;
    (*
    **
    *)
    function  exGetConnInfo: String;
    function FandR (pFile, pF, pR: String; pCaseInsensity: Boolean) : Boolean;
    procedure GetFileList(pls: Tstrings; pFolder, pExt: string);
    procedure exInitCommonVariables;

    function  exReadFuncCat(const sys: Integer = 0): String;
    function  exReadRepCat(const sys: Integer = 0): String;
    function buildGriEh(GridName: string;Grid: TDBGridEh; bColumnExists: Boolean = False) : Boolean;
    function buildColumnEh(ls: TStrings; Grid: TDBGridEh; bTitleButton, bColumnExists: Boolean) : Boolean;
implementation

uses
    isCommon, isFile, MainData, isStr, isLib, isDb, isType;

    (*
    ** Other
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exInitCommonVariables;
begin
    sysAppPath := ExtractFilePath(Application.ExeName);
    sysRepPath := sysAppPath + 'Rps\';

    sysTempPath := isGetTempDir;

    sysAppData := IncludeTrailingPathDelimiter(isGetSpecialFolder($001a)) +
    	'Softz Solutions\' + ExtractFileName(sysRegPath) + '\';
    ForceDirectories(sysAppData);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function  exGetConnInfo: String;
begin
    if DataMain.Conn.Connected then
        Result := '  [' + isNameServer + '.' + isDatabaseServer + ']'
    else
        result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure GetFileList(pls: Tstrings; pFolder, pExt: string);
var
	dta: TSearchRec;
    n: Integer;
begin
	pls.Clear;
	n := FindFirst(pFolder + pExt, faAnyFile, dta);
    while n = 0 do
    begin
    	if dta.Attr and faDirectory <> 0 then
        else
        	pls.Add(pFolder + dta.Name);
		n := FindNext(dta);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function FandR (pFile, pF, pR: String; pCaseInsensity: Boolean) : Boolean;
    var
    	n, nSize, nR, nF : Integer;
    	fp : File;
		s : String;
        pData, p : PChar;
    begin
    	Result := False;
        nF := length(pF);
        nR := length(pR);

        {$I-}
        AssignFile(fp, pFile);
        Reset(fp, 1);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        nSize := FileSize(fp);
        try
	        GetMem(pData, nSize);
        except
        	CloseFile(fp);
            Exit;
        end;

        BlockRead(fp, pData^, nSize);
        CloseFile(fp);

        n := Pos(pF, pData);
            if n <= 0 then
            	if pCaseInsensity then
		        	n := Pos(UpperCase(pF), UpperCase(pData));

        if n <= 0 then
        begin
        	FreeMem(pData);
            Exit;
        end;

        // Do replace
        DeleteFile(ChangeFileExt(s, '.bak'));
        RenameFile(s, ChangeFileExt(s, '.bak'));

        {$I-}
        AssignFile(fp, s);
        Rewrite(fp, 1);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        p := pData;
        while True do
        begin
        	n := Pos(pF, p);
            if n <= 0 then
            	if pCaseInsensity then
		        	n := Pos(UpperCase(pF), UpperCase(p));

            if n <= 0 then
            	Break;
	    	Result := True;

            BlockWrite(fp, p^, n - 1);
            nSize := nSize - (n - 1);
            p := p + n - 1;

            BlockWrite(fp, (@pR[1])^, nR);
            nSize := nSize - nF;
            p := p + nF;
        end;
		BlockWrite(fp, p^, nSize);
        CloseFile(fp);
		FreeMem(pData);
    end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exReadFuncCat(const sys: Integer): String;
var
	s: String;
begin
	s := Iif(sysEnglish, 'Function English Categories', 'Function Categories');
    if sys > 0 then
    	s := s + IntToStr(sys);
    Result := isStrReplace(FlexConfigString('Admin', s), ';', #13);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exReadRepCat(const sys: Integer): String;
var
	s: String;
begin
	s := Iif(sysEnglish, 'Report English Categories', 'Report Categories');
    if sys > 0 then
    	s := s + IntToStr(sys);
    Result := isStrReplace(FlexConfigString('Admin', s), ';', #13);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function buildGriEh(GridName: string; Grid: TDBGridEh; bColumnExists: Boolean = False) : Boolean;
var
    bEnable, bTitleButtons: Boolean;
	pSelected: TStrings;
	nOptions, nTitleLines, nFixedCols: Integer;
begin
    pSelected := Nil;	// Avoid warning
    with TADOQuery.Create(Nil) do
    begin
    	Connection := sysDbConnection;
        LockType := ltReadOnly;
        SQL.Text := 'select * from SYS_GRID where [NAME]=''' + GridName + '''';
        Open;
        // 1st time
        if IsEmpty then
        begin
        	Close;
            Free;
            Exit;
        end;

        // Options
		bEnable := FieldByName('ENABLE').AsBoolean;
        nFixedCols := FieldByName('FIXCOLS').AsInteger;
        nOptions := FieldByName('OPTIONS').AsInteger;
        bTitleButtons := FieldByName('TITLE_BUTTON').AsBoolean;

        if bEnable then
        begin
			pSelected := TStringList.Create;
	        if mvlLanguage = VIETNAMESE then
		        pSelected.Text := FieldByName('SELECTED').AsString
        	else
	        	pSelected.Text := FieldByName('SELECTED1').AsString;
		end;

        Close;
        Free;
    end;

    if not bEnable then
    	Exit;

    nTitleLines := 1;
    // Exists customize info.
    if pSelected.Count > 0 then
    with Grid do
    begin
        buildColumnEh(pSelected, Grid, bTitleButtons, bColumnExists);
        FrozenCols := nFixedCols;		// Num of fixed columns
        TitleLines := nTitleLines;	    // Num of title lines
    end;

    pSelected.Free;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function buildColumnEh(ls: TStrings; Grid: TDBGridEh; bTitleButton, bColumnExists: Boolean) : Boolean;
var
	i, j, k, n, width: Integer;
    s, s2, s3, sKey, sCaption: String;
    bCheckBox, bReadOnly: Boolean;
    line: TStrings;
    column: TColumnEh;
begin
    if not bColumnExists then
    begin
       Grid.Columns.Clear;
    end;

	i := 0;
    while i < ls.Count do
    begin
    	// Empty row
        if Trim(ls[i]) = '' then
        begin
            ls.Delete(i);
        	Continue;
        end;

        // Remarked row
    	if (ls[i][1] = '@') or (ls[i][1] = '#') then
        begin
        	ls.Delete(i);
            Continue;
        end;

        bCheckBox := False;
        bReadOnly := False;
        sKey := '';
        sCaption := '';
        s := '';
        s2 := '';
        width := 0;
        column := nil;
        line := TStringList.Create;
        isStrBreak(ls[i], ';', line);

        try
            if line.Count > 0 then
            begin
                 k := 0;
                 while k < line.Count do
                 begin
                      s := line[k];
                      if s <> '' then
                      begin
                          if k > 0 then
                          begin
                              if (s <> 'T') and (s <> 'F') then
                             begin
                                 if sCaption <> '' then
                                     sCaption := '|' + sCaption;

                                 sCaption := s + sCaption;
                             end
                             else
                             begin
                                 bReadOnly := (s = 'T');
                             end;
                          end
                          else
                          begin
                             j := Pos('=', s);
                             sKey := Copy(s, 1, j - 1);
                             s2 := Copy(s, j + 1, Length(s));
                             if (s2 <> '') and (width = 0) then
                             begin
                                width := StrToInt64(s2);
                                width :=  width * 8;
                             end;
                          end;
                      end;
                      Inc(k);
                 end;
                 if (sKey <> '') then
                 begin
                     if (sCaption <>  '') then
                        sCaption := sKey;

                     column := Grid.Columns.Grid.FindFieldColumn(sKey);
                     if bColumnExists and (column <> Nil) then
                         column.Index := i
                     else
                     begin
                        column := Grid.Columns.Add;
                        if bColumnExists then
                            column.Index := i;
                     end;

                     if (Grid.DataSource.DataSet.FindField(sKey) <> Nil) then
                     begin
                        bCheckBox := (Grid.DataSource.DataSet.FieldByName(sKey).DataType = ftBoolean);
                     end;

                     column.AutoFitColWidth := False;
                     column.Width := width;
                     column.FieldName := sKey;
                     column.Title.Caption := sCaption;
                     column.Title.TitleButton := bTitleButton;
                     column.ReadOnly := bReadOnly;
                     if not bCheckBox then
                     begin
                        column.TextEditing := True;
                        column.WordWrap := True;
                     end;
                 end;
            end;
        finally
            line.Free;
        end;
        Inc(i);
    end;
    Result := True;
end;

initialization
    sysIni := ExtractFilePath(Application.ExeName) + 'ConfigZ.ini';
end.

