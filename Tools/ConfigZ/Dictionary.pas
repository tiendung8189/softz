(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dictionary;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid2, Db, ADODB, ActnList,
  Menus, ComCtrls, wwfltdlg, Wwdbgrid, wwFltDlg2, AppEvnts,
  isPanel, DBCtrls, fcStatusBar, AdvMenus, wwDialog, StdCtrls, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmDictionary = class(TForm)
    Splitter1: TSplitter;
    QrDetail: TADOQuery;
    QrMaster: TADOQuery;
    DsDetail: TDataSource;
    DsMaster: TDataSource;
    QrMasterTABLE: TStringField;
    ActionList: TActionList;
    CmdSearchD: TAction;
    CmdQuit: TAction;
    Filter: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdDelete: TAction;
    QrDetailVISIBLE: TBooleanField;
    QrDetailFILTER: TIntegerField;
    CmdReload: TAction;
    PopRight: TAdvPopupMenu;
    QrDetailDESCRIPTION: TWideStringField;
    QrDetailDESCRIPTION1: TWideStringField;
    Tm1: TMenuItem;
    CmdVisibleFields: TAction;
    CmdFilteredFields: TAction;
    N3: TMenuItem;
    VisibleFieldsOnly1: TMenuItem;
    FilterFieldsOnly1: TMenuItem;
    CmdNative: TAction;
    Cmd2nd: TAction;
    CmdClearAll: TAction;
    TranslatedtoVietnamese1: TMenuItem;
    TranslatedtoEnglish1: TMenuItem;
    ClearAllFiltered1: TMenuItem;
    N4: TMenuItem;
    QrDetailFORMAT: TStringField;
    QrDetailTABLE: TStringField;
    QrDetailFIELD: TStringField;
    CmdFormatedFields: TAction;
    FormatedFields1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    PaDetail: TisPanel;
    TntPopupMenu1: TAdvPopupMenu;
    CmdSearchM: TAction;
    Search1: TMenuItem;
    CmdSpecialFilter: TAction;
    SpecialFilter1: TMenuItem;
    QrDetailCOMMENT: TWideMemoField;
    vlPanel1: TisPanel;
    EdComment: TDBMemo;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    Status: TfcStatusBar;
    spl1: TSplitter;
    GrMaster: TDBGridEh;
    GrDetail: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdSearchDExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrDetailCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdVisibleFieldsExecute(Sender: TObject);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure CmdClearAllExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure QrMasterAfterScroll(DataSet: TDataSet);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdSearchMExecute(Sender: TObject);
    procedure CmdSpecialFilterExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDetailBeforePost(DataSet: TDataSet);
  private
    mDetailSQL, mFilterStr, mMasterSQL, mCurrentFilter: String;
    mListFilter: TStringList;
    mFiltered: Boolean;
  public
    { Public declarations }
  end;

var
  FrmDictionary: TFrmDictionary;

implementation

uses
	MainData, islib, isMsg, isDb, ExCommon;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;
//    if GetSystemMetrics(SM_CXSCREEN) > 800 then
//    	GrDetail.Options := GrDetail.Options + [dgProportionalColResize];

    AddAllFields(QrDetail, 'SYS_DICTIONARY', 0);

    //ResetRowColor([GrMaster, GrDetail]);
    mDetailSQL := QrDetail.SQL.Text;
    mMasterSQL := QrMaster.SQL.Text;
    mCurrentFilter := '';
    mFilterStr := '';
    mListFilter := TStringList.Create;
    with mListFilter do
    begin
        Add(' and isnull(VISIBLE, 0) <> 0               = 0');
        Add(' and isnull(FILTER, 0) > 0                 = 0');
        Add(' and isnull(DESCRIPTION, '''') <> ''''     = 0');
        Add(' and isnull(DESCRIPTION1, '''') <> ''''    = 0');
        Add(' and isnull(FORMAT, '''') <> ''''          = 0');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.FormShow(Sender: TObject);
begin
    QrMaster.SQL.Text := Format(mMasterSQL, [#13' where 1=1 '#13]);
	OpenDataSets([QrMaster, QrDetail]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    mListFilter.Free;
    Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.QrMasterAfterScroll(DataSet: TDataSet);
begin
    PaDetail.HeaderCaption := ' :: ' + QrMaster.FieldByName('TABLE').AsString;
    if not QrMaster.IsEmpty then
    begin
       GrDetail.SumList.RecalcAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.QrDetailBeforeOpen(DataSet: TDataSet);
begin
    QrDetail.SQL.Text := mDetailSQL + mFilterStr;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.QrDetailBeforePost(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*
    **  Grid
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.GrDetailCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'FIELD' then
    begin
    	AFont.Size := 10;
        if Field.AsString = mCurrentFilter then
        begin
            AFont.Style := [fsBold];
            AFont.Color :=  clRed;
        end
        else
        begin
            AFont.Style := [];
            AFont.Color :=  clBlack;
        end;
    end;
end;
(*
    **  ApplicationEvents
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
//    Status.Panels[0].Text := isDb.RecordCount(QrMaster);
//    Status.Panels[1].Text := isdb.RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse: Boolean;
begin
    bBrowse := QrDetail.State in [dsBrowse];

    with QrMaster do
    begin
        CmdDelete.Enabled := not IsEmpty;
        CmdSearchM.Enabled := bBrowse;
    end;

    CmdSearchD.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdClearAll.Enabled := mFiltered;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrMaster then
        GrDetail.SetFocus
    else if ActiveControl = GrDetail then
        EdComment.SetFocus
    else
        GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdSearchMExecute(Sender: TObject);
begin
    exSearch(Name + '_M', DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdSearchDExecute(Sender: TObject);
begin
    exSearch(Name + '_D', DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdFilterExecute(Sender: TObject);
begin
    //Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdClearFilterExecute(Sender: TObject);
begin
    Filter.ClearFilter
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdSaveExecute(Sender: TObject);
begin
    QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdCancelExecute(Sender: TObject);
begin
    QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdDeleteExecute(Sender: TObject);
var
    s, s1: String;
begin
    s := QrMaster.FieldByName('TABLE').AsString;
    if not YesNo(Format('Clear All Information of "%s" Table In Dictionary?', [s]), 1) then
        Exit;

    with QrMaster do
    begin
        if Eof then
            s1 := ''
        else
        begin
            DisableControls;
            Next;
            s1 := FieldByName('TABLE').AsString;
        end;
    end;

    DataMain.Conn.Execute(Format('delete SYS_DICTIONARY where [TABLE]=''%s''', [s]));
    with QrMaster do
    begin
        Requery;
        if s1 <> '' then
            if not Locate('TABLE', s1, []) then
                First;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdReloadExecute(Sender: TObject);
begin
	QrMaster.Requery;
    QrDetail.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdVisibleFieldsExecute(Sender: TObject);
var
    n, i, nCount: Integer;
begin
    with (Sender as TAction) do
    begin
        mFiltered := not Checked;
        Checked := mFiltered;
        n := Tag;
    end;

    with mListFilter do
    begin
        if mFiltered then
            Values[Names[n - 10]] := '1'
        else
            Values[Names[n - 10]] := '0';

        mFilterStr := '';
        nCount := Count - 1;
        for i := 0 to nCount do
            if Values[Names[i]] = '1' then
                mFilterStr := mFilterStr + Names[i];
    end;

    with QrDetail do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdClearAllExecute(Sender: TObject);
var
    n, i: Integer;
begin

    with mListFilter do
    begin
        mFilterStr := '';
        n := Count - 1;
        for i := 0 to n do
            Values[Names[i]] := '0';
    end;

    mFiltered := False;
    CmdVisibleFields.Checked := False;
    CmdFilteredFields.Checked := False;
    CmdNative.Checked := False;
    Cmd2nd.Checked := False;
    CmdFormatedFields.Checked := False;

    with QrDetail do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDictionary.CmdSpecialFilterExecute(Sender: TObject);
var
    s: String;
    b: Boolean;
begin
    with CmdSpecialFilter do
    begin
        b := not Checked;
        Checked := b;
    end;

    if b then
    begin
        mCurrentFilter := QrDetail.FieldByName('FIELD').AsString;
        s := #13' where [TABLE] in ('#13 +
            #9'select [TABLE] from SYS_DICTIONARY where [FIELD] = ' +
            QuotedStr(mCurrentFilter) + ')'#13;
    end
    else
    begin
        mCurrentFilter := '';
        s := #13' where 1=1 '#13;
    end;
    with QrMaster do
    begin
        Close;
        SQL.Text := Format(mMasterSQL, [s]);
        Open;
    end;
end;

end.
