object FrmPrinterFont: TFrmPrinterFont
  Left = 661
  Top = 403
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Fonts M'#225'y In - Printer Supported Fonts'
  ClientHeight = 372
  ClientWidth = 487
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 12
    Top = 8
    Width = 82
    Height = 16
    Caption = 'List of Printers'
  end
  object Label2: TLabel
    Left = 232
    Top = 8
    Width = 132
    Height = 16
    Caption = 'List of Supported Fonts'
  end
  object LbPrinters: TListBox
    Left = 12
    Top = 28
    Width = 217
    Height = 309
    ItemHeight = 16
    TabOrder = 0
    OnDblClick = LbPrintersDblClick
  end
  object LbFonts: TListBox
    Left = 232
    Top = 28
    Width = 241
    Height = 309
    ItemHeight = 16
    TabOrder = 1
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 351
    Width = 487
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Width = 300
      end>
    UseSystemFont = False
    ExplicitTop = 352
    ExplicitWidth = 442
  end
end
