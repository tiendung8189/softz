﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Main;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, ActnList, Menus, ExtCtrls,
  RzGroupBar, StdCtrls, RzSplit, fcStatusBar, NativeXml, AdvMenus,
  StrUtils, AppEvnts, ImgList, RzPanel, isType;

type
  TFrmMain = class(TForm)
    MyActionList: TActionList;
    ImgLarge: TImageList;
    PopFunc: TAdvPopupMenu;
    ItemCat: TMenuItem;
    Outlook2: TMenuItem;
    TaskList2: TMenuItem;
    N1: TMenuItem;
    ItemCloseAll: TMenuItem;
    Panel1: TPanel;
    RzSizePanel1: TRzSizePanel;
    GbFunc: TRzGroupBar;
    GrSetup: TRzGroup;
    GrTool: TRzGroup;
    GrCode: TRzGroup;
    PaBkGr: TPanel;
    PaDesc: TPanel;
    Bevel3: TBevel;
    Bevel2: TBevel;
    Status: TfcStatusBar;
    CmdGrid: TAction;
    CmdDictionary: TAction;
    CmdRetailReceipt: TAction;
    CmdReportEncrypt: TAction;
    CmdPrinterFonts: TAction;
    CmdMemoEdit: TAction;
    CmdDba: TAction;
    CmdExport: TAction;
    CmdImport: TAction;
    CmdClose: TAction;
    CmdDeactQueries: TAction;
    CmdReconnect: TAction;
    CmdStringReplace: TAction;
    CmdwwDBGrid: TAction;
    CmdNonTextDfm: TAction;
    CmdCrpe: TAction;
    CmdDbe: TAction;
    CmdFormCorrect: TAction;
    CmdOtherX: TAction;
    CmdCrpeConn: TAction;
    CmdCrpeContent: TAction;
    ApplicationEvents1: TApplicationEvents;
    CmdHotroX: TAction;
    CmdFuncAdmin: TAction;
    CmdRepAdmin: TAction;
    CmdFmtRouding: TAction;
    CmdLOC: TAction;
    CmdDocFile: TAction;
    CmdRunSQL: TAction;
    CmdJob: TAction;
    CmdFlexConfig: TAction;
    CmdVersion: TAction;
    CmdResource: TAction;
    CmdSysMenu: TAction;
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdSetDateTimeExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ItemCloseAllClick(Sender: TObject);
    procedure ItemCatClick(Sender: TObject);
    procedure PopFuncPopup(Sender: TObject);
    procedure TntFormResize(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure CmdGridExecute(Sender: TObject);
    procedure CmdReportEncryptExecute(Sender: TObject);
    procedure CmdPrinterFontsExecute(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure CmdDbaExecute(Sender: TObject);
    procedure CmdDictionaryExecute(Sender: TObject);
    procedure CmdMemoEditExecute(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdRetailReceiptExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdwwDBGridExecute(Sender: TObject);
    procedure CmdStringReplaceExecute(Sender: TObject);
    procedure CmdDeactQueriesExecute(Sender: TObject);
    procedure CmdNonTextDfmExecute(Sender: TObject);
    procedure CmdCrpeExecute(Sender: TObject);
    procedure CmdDbeExecute(Sender: TObject);
    procedure CmdFormCorrectExecute(Sender: TObject);
    procedure CmdOtherXExecute(Sender: TObject);
    procedure CmdCrpeConnExecute(Sender: TObject);
    procedure CmdCrpeContentExecute(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure CmdHotroXExecute(Sender: TObject);
    procedure CmdFuncAdminExecute(Sender: TObject);
    procedure CmdRepAdminExecute(Sender: TObject);
    procedure CmdFmtRoudingExecute(Sender: TObject);
    procedure CmdLOCExecute(Sender: TObject);
    procedure CmdDocFileExecute(Sender: TObject);
    procedure CmdRunSQLExecute(Sender: TObject);
    procedure CmdJobExecute(Sender: TObject);
    procedure CmdFlexConfigExecute(Sender: TObject);
    procedure CmdVersionExecute(Sender: TObject);
    procedure CmdResourceExecute(Sender: TObject);
    procedure CmdSysMenuExecute(Sender: TObject);

  private
    mList: TStringList;
    mDir: String;
	sF, sR: String;
    mFixLogon: Boolean;
    caseInsensity: Boolean;
    function GetConnection: Integer;
    procedure CheckConn;
    procedure CheckFixLogon;
    // Dephi
  	function ListFiles (wc: String = '*.*') : String;

    // Xml
    function  CreateXml: TNativeXml;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

uses
	MainData, isMsg, isLib, ExCommon, isFile, isDb, isCommon, ShellAPI,
    CustomizeGrid, Dictionary, DbeSetup, RetailReceipt, FlexEdit,
  OtherX, PrinterFont, ExtractBlob, Dialogs, Dfm, FindReplace, DBGridCorrection,
  EximOption, RepEncode, HotroX, FuncConfig, RepConfig, FormatAndRouding,
  DmHotro, DmKho2, DocumentFile, ExecuteSQL, Job, FlexConfig, VersionUpdate, Resource,
  SysMenu;

{$R *.DFM}
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.CheckConn();
begin
    if not DataMain.Conn.Connected then
    begin
        ErrMsg('Lỗi kết nối CSDL.');
        Abort;
    end
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormCreate(Sender: TObject);
var
    i: Integer;
begin
    mList := TStringList.Create;
	sF := '';
    sR := '';
    mDir := '.';

    // Load GroupBar state
    with GbFunc do
    begin
        Style := TRzGroupBarStyle(RegRead(Self.Name, 'RzGroupStyle', 0));
        for i := 0 to GroupCount - 1 do
            Groups[i].Opened := RegReadBool(Self.Name, Groups[i].Name, True);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
    i: Integer;
begin
    // Save GroupBar state
    with GbFunc do
        for i := 0 to GroupCount - 1 do
            RegWrite(Self.Name, Groups[i].Name, Groups[i].Opened);

    DbeFinal;
	mList.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormShow(Sender: TObject);
begin
    with Status do
    begin
        if GetConnection = 2 then
            ErrMsg('Error connect to database server.')
        else
            Panels[4].Text := 'DB: ' + isNameServer + '.' + isDatabaseServer;
	    Panels[6].Text := 'Build ' + GetModuleVersion;
    end;

	TMyForm(Self).Init2;
	with GbFunc do
	begin
		ScrollPosition := 0;
		Groups[0].Reposition;
    end;
    DbeInitial;
    mFixLogon := False;
    sysLogonUID := 1;
    exInitCommonVariables;
end;

(*==============================================================================
** Init XML
**------------------------------------------------------------------------------
*)
function TFrmMain.CreateXml: TNativeXml;
begin
	Result := TNativeXml.CreateName('ApplicationConfigs');
	with Result do
	begin
		EncodingString := 'Utf-8';
		Root.AttributeAdd('Exportby', ExtractFileName(Application.ExeName));
        Root.AttributeAdd('Version', VersionString);
        Root.AttributeAdd('Source', isNameServer + '.' + isDatabaseServer);
	end;
end;

(*==============================================================================
** Return Value:
**	0:	OK
**	1:	Cancel
**	2:	Error
**------------------------------------------------------------------------------
*)
function TFrmMain.GetConnection: Integer;
var
    Return: Integer;
begin
    Return := 0;
    // Get logon info.

    if Return = 0 then
    with DataMain do
    begin
	   	// Db connect
		Conn.Connected := False;

    	Wait(CONNECTING);
        try
	    	Conn.Connected := True;
        except
            Return := 2;
        end;
        ClearWait;
    end;

    if Return = 0 then
        Caption := Caption + exGetConnInfo;

    Result := Return;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
 function TFrmMain.ListFiles;
var
	s : String;
    n : Integer;
    dta : TSearchRec;
begin
	s := isGetDir('Source Folder', mDir);
    if s = '' then
    begin
    	Result := '';
    	Exit;
    end;
    mDir := s;

	s := '';
    n := FindFirst(mDir + '\' + wc, faAnyFile, dta);

    while n = 0 do
    begin
    	if (dta.Name = '.') or (dta.Name = '..') then
        else
        begin
        	if s <> '' then
		    	s := s + #13;
	    	s := s + dta.Name;
        end;

    	n := FindNext(dta);
    end;

    FindClose(dta);
	Result := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	if Action.Tag = 99 then
    begin
		Handled := False;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPrinterFontsExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmPrinterFont, FrmPrinterFont);
	FrmPrinterFont.ShowModal
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDbeExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_DBE_MSG') or not TableExists('SYS_DBE_OBJ') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;
//
    Application.CreateForm(TFrmDbeSetup, FrmDbeSetup);
    FrmDbeSetup.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdExportExecute(Sender: TObject);
var
    s: String;
    n: Integer;
    Xml : TNativeXml;
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmEximOption, FrmEximOption);
    n := FrmEximOption.Execute(1, s);
    if n < 0 then
        Exit;

    Xml := CreateXml;

    // Dataset -> Xml
    DataMain.ExportDataXlm(xml, n, s);
    xml.SaveToFile(s);
    MsgDone;
    xml.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGridExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_GRID') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

	Application.CreateForm(TFrmCustomizeGrid, FrmCustomizeGrid);
	FrmCustomizeGrid.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHotroXExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('DM_HOTRO') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

	Application.CreateForm(TFrmHotroX, FrmHotroX);
	FrmHotroX.Execute();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdImportExecute(Sender: TObject);
var
    s: String;
    n: Integer;
    Xml: TNativeXml;
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmEximOption, FrmEximOption);
    n := FrmEximOption.Execute(0, s);
    if n < 0 then
        Exit;

    Xml := CreateXml;
    Xml.LoadFromFile(s);
    DataMain.ImportDataXml(Xml, n);
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdJobExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

	Application.CreateForm(TFrmJob, FrmJob);
	FrmJob.Execute();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLOCExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmDmKho2, FrmDmKho2);
    FrmDmKho2.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CheckFixLogon;
begin
    if not mFixLogon then
    begin
		if not FixLogon then
			Abort;
        mFixLogon := True;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCrpeConnExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmRepConnect, FrmRepConnect);
//    FrmRepConnect.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCrpeContentExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmRepEdit, FrmRepEdit);
//    FrmRepEdit.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCrpeExecute(Sender: TObject);
begin
//    if not (DebugHook  <> 0) then
//        Exit;
//
//    Application.CreateForm(TFrmRepDebug, FrmRepDebug);
//    FrmRepDebug.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDbaExecute(Sender: TObject);
begin
	if GetConnection = 2 then
    begin
        ErrMsg('Error connect to database server.');
        Status.Panels[4].Text := '';
    end
    else
        Status.Panels[4].Text := 'DB: ' + isNameServer + '.' + isDatabaseServer;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDeactQueriesExecute(Sender: TObject);
var
	s : String;
    n : Integer;

    	(*
        **
        *)
    function One (sFile : String) : Boolean;
    var
    	fp, gp : TextFile;
        sSource, sTarget, ts, s : String;
        b, b1 : Boolean;
    begin
    	Result := False;
        with Status.Panels[0] do
        begin
            Style := psTextOnly;
    	    Text := sFile;
        end;
    	sSource := mDir + '\' + sFile;
    	sTarget := ChangeFileExt(sSource, '.bak');
    	if not CopyFile(PChar(sSource), PChar(sTarget), False) then
			Exit;

        {$I-}
        AssignFile(fp, sTarget);
        Reset(fp);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        Readln(fp, s);
        s := Trim(s);
        if Copy(s, 1, 6) <> 'object' then
        begin
	        CloseFile(fp);
            Exit;
        end;
        Reset(fp);


        {$I-}
        AssignFile(gp, sSource);
        Rewrite(gp);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        b := False;
        b1 := False;
        while not Eof(fp) do
        begin
            Readln(fp, s);
            ts := Trim(s);

            // Query & StoreProc
            if (Pos('object', ts) > 0) and
				((Pos('TADOQuery', ts) > 0) or
                 (Pos('TADOStoredProc', ts) > 0)) then
            	b := True;

            if b and (ts = 'end') then
            	b := False;

            if b and (ts = 'Active = True') then
            	Continue;

            // Connection
            if (Pos('object', ts) > 0) and
				(Pos('TADOConnection', ts) > 0)  then
            	b1 := True;

            if b1 and (ts = 'end') then
            	b1 := False;

            if ts = 'Connected = True' then
            	Continue;

           	Writeln(gp, s);
        end;

        CloseFile(fp);
        CloseFile(gp);
    	Result := True;
    end;

begin
	s := ListFiles('*.dfm');
    if s = '' then
    	Exit;

    mList.Text := s;
    for n := 0 to mList.Count - 1 do
    	One (mList.Strings[n]);

    Msg(Format('-== %d form(s) patched. ==-', [mList.Count]));
    Status.Panels[0].Style := psHintContainerOnly;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFormCorrectExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmDfm, FrmDfm);
    FrmDfm.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFuncAdminExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmFuncConfig, FrmFuncConfig);
	FrmFuncConfig.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDictionaryExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_DICTIONARY') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

	Application.CreateForm(TFrmDictionary, FrmDictionary);
    FrmDictionary.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDocFileExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_DOCUMENT') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

    Application.CreateForm(TFrmDocumentFile, FrmDocumentFile);
    FrmDocumentFile.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdRetailReceiptExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('PRINTERS') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

    Application.CreateForm(TFrmRetailReceipt, FrmRetailReceipt);
    FrmRetailReceipt.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdRunSQLExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmExecuteSQL, FrmExecuteSQL);
	FrmExecuteSQL.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
//	 DbConnect;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdStringReplaceExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmReplaceDialog, FrmReplaceDialog);
    FrmReplaceDialog.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSysMenuExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_MENU') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

    Application.CreateForm(TFrmSysMenu, FrmSysMenu);
    FrmSysMenu.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdVersionExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_VERSION') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

    Application.CreateForm(TFrmVersionUpdate, FrmVersionUpdate);
    FrmVersionUpdate.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdRepAdminExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmRepConfig, FrmRepConfig);
	FrmRepConfig.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReportEncryptExecute(Sender: TObject);
begin
    CheckFixLogon;

	Application.CreateForm(TFrmRepEncode, FrmRepEncode);
	FrmRepEncode.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdResourceExecute(Sender: TObject);
begin
    CheckFixLogon;

    Application.CreateForm(TFrmResource, FrmResource);
	FrmResource.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetDateTimeExecute(Sender: TObject);
begin
	WinExec('control timedate.cpl', SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdwwDBGridExecute(Sender: TObject);
var
	s: String;
    n: Integer;

    	(*
        **
        *)

{$REGION '<< Sub procedures >>'}
    function One (sFile : String) : Boolean;
    var
    	fp, gp: TextFile;
        sSource, sTarget, ts, s: String;
        b: Boolean;
    begin
    	Result := False;

        with Status.Panels[0] do
        begin
            Style := psTextOnly;
    	    Text := sFile;
        end;

    	sSource := mDir + '\' + sFile;
    	sTarget := ChangeFileExt(sSource, '.bak');
    	if not CopyFile(PChar(sSource), PChar(sTarget), False) then
			Exit;

        {$I-}
        AssignFile(fp, sTarget);
        Reset(fp);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        Readln(fp, s);
        s := Trim(s);
        if Copy(s, 1, 6) <> 'object' then
        begin
	        CloseFile(fp);
            Exit;
        end;
        Reset(fp);


        {$I-}
        AssignFile(gp, sSource);
        Rewrite(gp);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        b := False;
        while not Eof(fp) do
        begin
            Readln(fp, s);
            ts := Trim(s);

            // wwGrid
            if (Pos('object', ts) = 1) and (Pos(': TwwDBGrid', ts) > 0) then
            	b := True;

            if b and (ts = 'end') then
            	b := False;

            if b and (Pos('Options = [', ts) = 1) then
            begin
                if FrmDBGridCorrection.Ck1.Checked then
				    if Pos('dgShowCellHint', s) <= 0 then
                	    s := Copy(s, 1, Length(s) - 1) + ', dgShowCellHint]';

                if FrmDBGridCorrection.Ck2.Checked then
				    if Pos('dgDblClickColSizing', s) <= 0 then
                	    s := Copy(s, 1, Length(s) - 1) + ', dgDblClickColSizing]';
            end;

           	Writeln(gp, s);
        end;

        CloseFile(fp);
        CloseFile(gp);
    	Result := True;
    end;

    procedure WWFix(pFile: String);
    var
        bs: TStrings;
        i, iP, iC, iG: Integer;
        b: Boolean;
        s: String;

        // Dọn dẹp PictureMask!
        procedure Proc1();
        var
            pP, pG :Integer;
            bb: Boolean;
            sF: string;
        begin
            pP := iP;
            Inc(pP);
            while True do
            begin
                bb := False;
                pG := iG;
                sF := bs.Strings[pP];
                sF := RightStr(sF, Length(sf) - Pos('''', sF));
                sF := '''' + LeftStr(sF, Pos('''', sF));

                while True do
                begin
                    if Pos(sF, bs.Strings[pG]) > 0 then
                    begin
                        bb := True;
                        Break;
                    end;

                    if Pos(')', bs.Strings[pG]) > 0 then
                        Break;

                    Inc(pG);
                end;

                // PictureMask là rác rến -> Del.
                if not bb then
                begin
                    if not (Pos(')', bs.Strings[pP]) > 0) then
                        bs.Delete(pP)
                    else
                    begin
                        bs.Delete(pP);
                        Dec(pP);
                        bs.Strings[pP] := bs.Strings[pP] + ')';
                    end;

                    //sau khi xóa các vị trí có thẻ thay đổi
                    if iC > iP then
                        Dec(iC);
                    if iG > iP then
                        Dec(iG);

                    // Không còn gì  -> xóa luôn.
                    if Pos('()', bs.Strings[iP])  > 0then
                    begin
                        bs.Delete(iP);
                        b := True;

                        if iC > iP then
                            Dec(iC);
                        if iG > iP then
                            Dec(iG);

                        Break;
                    end;

                    // Đánh dấu có chỉnh sửa.
                    b := True;
                end;
                if Pos(')', bs.Strings[pP]) > 0 then
                    if not bb then
                        Continue
                    else
                        Break;

                if bb then
                    Inc(pP);
            end;
        end;

        // Dọn dẹp ControlType!
        procedure Proc2();
        var
            pC, pG :Integer;
            bb: Boolean;
            sF: string;
        begin
            pC := iC;
            Inc(pC);
            while True do
            begin
                bb := False;
                pG := iG + 1;
                sF := bs.Strings[pC];
                sF := RightStr(sF, Length(sf) - Pos('''', sF));
                sF := '''' + LeftStr(sF, Pos(';', sF) -1) + '''';
                while True do
                begin
                    if Pos(sF, bs.Strings[pG]) > 0 then
                    begin
                        bb := True;
                        Break;
                    end;

                    if Pos(')', bs.Strings[pG]) > 0 then
                        Break;

                    Inc(pG);
                end;

                // ControlType là rác rến -> Del.
                if not bb then
                begin
                    if not (Pos(')', bs.Strings[pC]) > 0) then
                        bs.Delete(pC)
                    else
                    begin
                        bs.Delete(pC);
                        Dec(pC);
                        bs.Strings[pC] := bs.Strings[pC] + ')';
                    end;

                    //sau khi xóa các vị trí có thẻ thay đổi
                    if iP > iC then
                        Dec(iP);
                    if iG > iC then
                        Dec(iG);

                    // Không còn gì  -> xóa luôn.
                    if Pos('()', bs.Strings[iC])  > 0 then
                    begin
                        bs.Delete(iC);
                        b := True;

                        if iP > iC then
                            Dec(iP);
                        if iG > iC then
                            Dec(iG);
                        Break;
                    end;

                    // Đánh dấu có chỉnh sửa.
                    b := True;
                end;

                if Pos(')', bs.Strings[pC]) > 0 then
                    if not bb then
                        Continue
                    else
                        Break;

                if bb then
                    Inc(pC);
            end;
        end;
    begin
        bs := TStringList.Create;
        bs.LoadFromFile(pFile);
        bs.NameValueSeparator := ':';

        i := 0;
        b := False;
        while i < bs.Count do
        begin
            // Go to ww Grid
            bs.NameValueSeparator := ':';
            if not SameText(Trim(bs.ValueFromIndex[i]), 'TwwDBGrid2') then
            begin
                Inc(i);
                Continue;
            end;
            Inc(i);

            // Search for property
            iC := -1;
            iP := -1;
            iG := -1;
            bs.NameValueSeparator := '=';
            while i < bs.Count do
            begin
                // Hết Object này hoạc qua Object mới thì ngưng.
                if SameText(Trim(bs[i]), 'end') or
                   SameText(Copy(Trim(bs[i]), 1, 6), 'object') then
                begin
                    Inc(i);
                    Break;
                end;

                // Find PictureMasks Strings.
                if iP = -1 then
                if SameText(Trim(bs.Names[i]), 'PictureMasks.Strings') then
                begin
                    iP := i;
                    Inc(i);
                end;

                // Find ControlType Strings.
                if iC  = -1 then
                if SameText(Trim(bs.Names[i]), 'ControlType.Strings') then
                begin
                    iC := i;
                    Inc(i);
                end;

                // Find Column Strings.
                if iG = -1 then
                if SameText(Trim(bs.Names[i]), 'Selected.Strings') then
                begin
                    iG := i;
                    Inc(i);
                end;

                // Thường thì tìm thấy Selected.Strings là hết rồi :)
                if iG <> -1 then
                    Break;

                Inc(i);
            end;

            // Có PictureMasks
            if (iP <> -1) and (iG <> -1) then
                Proc1;

            // Có ControlType
            if (iC <> -1) and (iG <> -1) then
                Proc2;

            Inc(i);
        end;

        if b then
        begin
            s := ChangeFileExt(pFile, '.bak');
            DeleteFile(s);
            RenameFile(pFile, s);
            bs.SaveToFile(pFile);
        end;
        bs.Free;
    end;
{$ENDREGION}

begin
	s := ListFiles('*.dfm');
    if s = '' then
    	Exit;

    mList.Text := s;
   Application.CreateForm(TFrmDBGridCorrection, FrmDBGridCorrection);
    while True do
    begin
        if not FrmDBGridCorrection.Execute then
            Break;

        for n := 0 to mList.Count - 1 do
        begin
    	    One (mList.Strings[n]);
            if FrmDBGridCorrection.Ck3.Checked then
                WWFix(mDir + '\' + mList.Strings[n]);
        end;
        Msg(Format('-== %d form(s) patched. ==-', [mList.Count]));
    end;
    FrmDBGridCorrection.Free;
   	Status.Panels[0].Style := psHintContainerOnly;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFlexConfigExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('SYS_FLEXCONFIG') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

	Application.CreateForm(TFrmFlexConfig, FrmFlexConfig);
    FrmFlexConfig.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFmtRoudingExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    Application.CreateForm(TFrmFormatAndRouding, FrmFormatAndRouding);
	FrmFormatAndRouding.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNonTextDfmExecute(Sender: TObject);
var
	s : String;
    n : Integer;
    Ls : TStrings;
    	(*
        **
        *)
    procedure One (sFile : String);
    var
    	fp : TextFile;
        s : String;
    begin
        with Status.Panels[0] do
        begin
            Style := psTextOnly;
    	    Text := sFile;
        end;
        {$I-}
        AssignFile(fp, mDir + '\' + sFile);
        Reset(fp);
        {$I+}
        if IOResult <> 0 then
        	Exit;

        Readln(fp, s);
        s := Trim(s);

        if Copy(s, 1, 6) <> 'object' then
        	Ls.Add(sFile);

        CloseFile(fp);
    end;

begin
	s := ListFiles('*.dfm');
    if s = '' then
    	Exit;
    Ls := TStringList.Create;
    mList.Text := s;
    for n := 0 to mList.Count - 1 do
    	One (mList.Strings[n]);

   	Status.Panels[0].Style := psHintContainerOnly;

    if Ls.Count > 0 then
    begin
        s:= sysTempPath + '$non-textform.txt';
        Ls.SaveToFile(s);
        ShellExecute(Handle, 'Open', PChar(s), nil, nil, SW_NORMAL);
    end;
    Ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdOtherXExecute(Sender: TObject);
begin
    CheckConn;
    CheckFixLogon;

    if not TableExists('DM_KHACX') then
    begin
        ErrMsg(ES_INVALID_PROJECT);
        Exit;
    end;

	Application.CreateForm(TFrmOtherX, FrmOtherX);
	FrmOtherX.Execute();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdMemoEditExecute(Sender: TObject);
begin
    CheckConn;

	Application.CreateForm(TFrmExtractBlob, FrmExtractBlob);
    FrmExtractBlob.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCloseAllClick(Sender: TObject);
var
	i: Integer;
begin
	with GbFunc do
   		for i := 0 to GroupCount - 1 do
        	Groups[i].Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCatClick(Sender: TObject);
begin
	case (Sender as TComponent).Tag of
    0:
    	with GbFunc do
        	if Style <> gbsCategoryView then
            begin
	        	Style := gbsCategoryView;
                ItemCloseAll.OnClick(Nil);
            end;
    1:
    	with GbFunc do
        	if Style <> gbsOutlook then
	        	Style := gbsOutlook;
    2:
    	with GbFunc do
        	if Style <> gbsTaskList then
	        	Style := gbsTaskList;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PopFuncPopup(Sender: TObject);
begin
	with PopFunc, GbFunc do
    begin
    	Items[0].Checked := Style = gbsCategoryView;
    	Items[1].Checked := Style = gbsOutlook;
    	Items[2].Checked := Style = gbsTasklist;
    end;
    ItemCloseAll.Enabled := ItemCat.Checked;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status)
end;

initialization
	{$IFDEF _MULTILANG_}
    sysEnglish := GetArg('E');
    if sysEnglish = False then
    else if not	SetNewResourceDll(LANG_ENGLISH) then
    {$ENDIF}
    	sysEnglish := False;
    if sysEnglish then
        DataMain.isEnv1.Language := ENGLISH;
end.

