object FrmReplaceDialog: TFrmReplaceDialog
  Left = 192
  Top = 107
  BorderStyle = bsDialog
  Caption = 'Thay Th'#7871
  ClientHeight = 225
  ClientWidth = 409
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    409
    225)
  PixelsPerInch = 96
  TextHeight = 16
  object BitBtn2: TBitBtn
    Left = 297
    Top = 182
    Width = 101
    Height = 29
    Anchors = [akRight, akBottom]
    Caption = 'Tho'#225't'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Kind = bkCancel
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 193
    Top = 182
    Width = 101
    Height = 29
    Anchors = [akRight, akBottom]
    Caption = 'Th'#7921'c hi'#7879'n'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object pnl1: TPanel
    Left = 12
    Top = 12
    Width = 385
    Height = 163
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object lbl1: TLabel
      Left = 24
      Top = 73
      Width = 22
      Height = 16
      Caption = 'T'#236'm'
    end
    object lbl2: TLabel
      Left = 18
      Top = 101
      Width = 28
      Height = 16
      Caption = 'Thay'
    end
    object TntLabel1: TLabel
      Left = 10
      Top = 22
      Width = 36
      Height = 16
      Alignment = taRightJustify
      Caption = 'Folder'
    end
    object btSelectFile: TSpeedButton
      Left = 348
      Top = 16
      Width = 25
      Height = 24
      Cursor = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FF00000000000000000000000000000000000000000000
        00000000000000840029CE00299C00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF31EFFF00CEFF00BDEF00CEFF00ADDE00BDEF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF63FFFF00CEFF31EFFF00CEFF00BDEF00
        CEFF00ADDE0000845ACEFF0063FF00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF63FFFF00CEFF31EFFF00CEFF00BDEF00CEFF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF9CFFFF00CEFF63FFFF00CEFF31EFFF00
        CEFF00BDEF0000845ACEFF0063FF00299C000000FF00FFFF00FF0063639CFFFF
        00CEFF9CFFFF00BDEF63FFFF00CEFF31EFFF00CEFF0000845ACEFF0063FF0029
        9C000000FF00FFFF00FF0063639CFFFF9CFFFF9CFFFF9CFFFF9CFFFF9CFFFF9C
        FFFF9CFFFF0000849CFFFF0063FF00299C000000FF00FFFF00FF006363006363
        006363006363006363006363006363006363006363000084CEFFFF5ACEFF0029
        9C000000FF00FFFF00FFFF00FF006363F7FFFF9CFFFF9CFFFFADCECE000000FF
        00FFFF00FF008484000084000084000084FF00FFFF00FFFF00FFFF00FFFF00FF
        006363006363006363000000FF00FFFF00FFFF00FFFF00FF424242F7CEA50000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF73
        7373393939FF00FF424242F7CEA5000000FF00FF000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF737373FFEFCE3939394242427B7B7BF7CEA59C29
        290000009C2929000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF737373FF
        FFFFCECECECECECECECECEFFFFFFCECECECECECEF7CEA5000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF737373FFFFFFF7CEA5000000FFFFFFFFFFFFFFEF
        CEFFEFCE000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF73
        7373737373E7DEE7737373737373737373737373FF00FFFF00FF}
      ParentFont = False
      OnClick = btSelectFileClick
    end
    object LbFileExt: TLabel
      Left = 5
      Top = 48
      Width = 41
      Height = 16
      Alignment = taRightJustify
      Caption = 'File Ext'
    end
    object EdSearch: TEdit
      Left = 52
      Top = 69
      Width = 321
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object EdReplace: TEdit
      Left = 52
      Top = 97
      Width = 321
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      Text = 'Edit1'
    end
    object CkCase: TCheckBox
      Left = 100
      Top = 132
      Width = 192
      Height = 17
      Caption = 'Kh'#244'ng ph'#226'n bi'#7879't hoa / th'#432#7901'ng'
      TabOrder = 4
    end
    object EdDir: TEdit
      Left = 52
      Top = 16
      Width = 292
      Height = 24
      TabStop = False
      Color = clInfoBk
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object CbFileExt: TComboBox
      Left = 52
      Top = 43
      Width = 140
      Height = 24
      TabOrder = 1
      Text = '*.dfm'
      Items.Strings = (
        '*.dfm'
        '*.bak'
        '*.pas'
        '*.txt'
        '*.*')
    end
  end
end
