(*==============================================================================
**------------------------------------------------------------------------------
*)
unit GridOption;

interface

uses
  Windows, Classes, Controls, Forms,
  CheckLst, Buttons, StdCtrls;

type
  TFrmGridOption = class(TForm)
    ckList: TCheckListBox;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
  public
    function Execute(var pValue: DWORD): Boolean;
  end;

var
  FrmGridOption: TFrmGridOption;

implementation

{$R *.dfm}

uses
    isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGridOption.Execute;
var
	i, n: DWORD;
begin
    for i := 0 to ckList.Items.Count - 1 do
        ckList.Checked[i] := (pValue and (1 shl i)) <> 0;

	Result := ShowModal = mrOK;
    if Result then
    begin
	    n := 0;
	    for i := 0 to ckList.Items.Count - 1 do
			if ckList.Checked[i] then
        	    n := n + (1 shl i);

		Result := pValue <> n;
        if Result then
        	pValue := n;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGridOption.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGridOption.FormShow(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGridOption.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGridOption.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
