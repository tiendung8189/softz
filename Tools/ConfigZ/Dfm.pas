﻿unit Dfm;

interface

uses
  Windows, SysUtils, Variants, Classes, Controls, Forms,
  ActnList, fcStatusBar, StdCtrls, Buttons;

type
  TFrmDfm = class(TForm)
    Status: TfcStatusBar;
    Action: TActionList;
    CmdSelectDir: TAction;
    CmdClose: TAction;
    CmdRun: TAction;
    GroupBox1: TGroupBox;
    TntLabel1: TLabel;
    btSelectFile: TSpeedButton;
    EdDir: TEdit;
    CbAciton: TComboBox;
    LbAciton: TLabel;
    LbName: TLabel;
    LbProperty: TLabel;
    EdValue: TEdit;
    LbValue: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    CbFileExt: TComboBox;
    LbFileExt: TLabel;
    EdName: TEdit;
    EdProperty: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSelectDirExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CbAcitonChange(Sender: TObject);
    procedure CmdRunExecute(Sender: TObject);
    procedure CbFileExtChange(Sender: TObject);

  private
    ls: TStrings;
    sPath: string;

    procedure GetFileList(const pPath: string);
    procedure ToUtf8(const pClass, pProp: String);
    function ToPlainUtf8(const s: String): String;
    procedure SetPropValueByClass(const pClass, pProp, pValue: String;
        const force: Boolean = False);
    procedure SetPropValueByComponent(const pComName, pProp, pValue: String;
	    force: Boolean = False);
  public

  end;

var
  FrmDfm: TFrmDfm;

implementation

uses
    isLib, isFile, isStr, isMsg;

const
	MapSet1: array [160..255] of Integer = (
        $0000, $0102, $00C2, $00CA, $00D4, $01A0, $0000, $0110,
        $0103, $00E2, $00EA, $00F4, $01A1, $01B0, $0111, $0000,
        $0000, $0000, $0000, $0000, $0000, $00E0, $1EA3, $00E3,
        $00E1, $1EA1, $0000, $1EB1, $1EB3, $1EB5, $1EAF, $0000,
        $0000, $0000, $0000, $0000, $0000, $0000, $1EB7, $1EA7,
        $1EA9, $1EAB, $1EA5, $1EAD, $00E8, $0000, $1EBB, $1EBD,
        $00E9, $1EB9, $1EC1, $1EC3, $1EC5, $1EBF, $1EC7, $00EC,
        $1EC9, $0000, $0000, $0000, $0129, $00ED, $1ECB, $00F2,
        $0000, $1ECF, $00F5, $00F3, $1ECD, $1ED3, $1ED5, $1ED7,
        $1ED1, $1ED9, $1EDD, $1EDF, $1EE1, $1EDB, $1EE3, $00F9,
        $0000, $1EE7, $0169, $00FA, $1EE5, $1EEB, $1EED, $1EEF,
        $1EE9, $1EF1, $1EF3, $1EF7, $1EF9, $00FD, $1EF5, $0000
    );	// Normal

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.ActionUpdate(Action: TBasicAction; var Handled: Boolean);
var
    bDir, bAct, bName, bPro, bVal: Boolean;
    sAct: string;
begin
    bAct := CbAciton.Text <> '';
    bName := EdName.Text <> '';
    bPro := EdProperty.Text <> '';
    bVal := EdValue.Text <> '';
    sAct := CbAciton.Text;

    bDir := ls.Text <> '';
    CbAciton.Enabled := bDir;
    EdValue.Visible := sAct <> 'Utf8';
    LbValue.Visible := sAct <> 'Utf8';
    EdName.Enabled := bDir;
    EdProperty.Enabled := bDir;
    CbFileExt.Enabled := EdDir.Text <> '';

    if sAct <> 'Utf8' then
        CmdRun.Enabled := bDir and bAct and bName and bPro and bVal
    else
        CmdRun.Enabled := bDir and bAct and bName and bPro;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.CbAcitonChange(Sender: TObject);
begin
    with Sender as TComboBox do
    begin
        if (Text = 'Setval2') or (Text = 'Fsetval2') then
        begin
            LbName.Caption := 'Component name';
        end
        else
        begin
            LbName.Caption := 'Class name';
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.CbFileExtChange(Sender: TObject);
begin
    GetFileList(sPath + '\' + CbFileExt.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.CmdRunExecute(Sender: TObject);
var
    sAct, sName, sPro, sVal: string;
begin
    sAct := CbAciton.Text;
    sName := EdName.Text;
    sPro := EdProperty.Text;
    sVal := EdValue.Text;
    try
    // Convert UTF8
    if sAct = 'Utf8' then
        ToUtf8(sName, sPro);

    // Set property by class
    if sAct = 'Setval' then
        SetPropValueByClass(sName, sPro, sVal);

    // Set property by class with force add new if not existing
    if sAct = 'Fsetval' then
        SetPropValueByClass(sName, sPro, sVal, True);

    // Set property by component name
    if sAct = 'Setval2' then
        SetPropValueByComponent(sName, sPro, sVal);

    // Set property by component name with force add new if not existing
    if sAct = 'Fsetval2' then
        SetPropValueByComponent(sName, sPro, sVal, True);
    except
        on E : Exception do ErrMsg(e.Message)
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.CmdSelectDirExecute(Sender: TObject);
begin
    sPath := isGetDir('Chọn thư mục', RegRead(Self.Name, 'LastFolder', ''));
    if sPath = '' then
        Exit;
    RegWrite(Self.Name, 'LastFolder', sPath);
    EdDir.Text := sPath;
    GetFileList(sPath + '\' + CbFileExt.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ls.Free;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    ls := TStringList.Create;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.GetFileList;
var
	dta: TSearchRec;
    n: Integer;
    sPath :string;
begin
    ls.Clear;
    sPath := ExtractFileDir(pPath);
	n := FindFirst(pPath, faAnyFile, dta);
    while n = 0 do
    begin
    	if dta.Attr and faDirectory <> 0 then
        else
        	ls.Add(sPath + '\' + dta.Name);
		n := FindNext(dta);
    end;
    Status.Panels[0].Text := IntToStr(ls.Count) + ' files';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDfm.ToPlainUtf8(const s: String): String;
var
	rs, ns: String;
    i, n, k: Integer;
begin
	i := 1;
    n := Length(s) + 1;
    rs := '';
    while i < n do
    begin
    	if s[i] <> '#' then
        begin
        	rs := rs + s[i];
	        Inc(i);
            Continue;
        end;

        ns := '';
        Inc(i);
        while i < n do
        begin
            if (s[i] >= '0') and (s[i] <= '9') then
                ns := ns + s[i]
            else
                Break;
            Inc(i);
        end;

		rs := rs + '#';
		if ns <> '' then
        begin
        	k := StrToInt(ns);
            if (k < 160) or (MapSet1[k] = 0) then
            	rs := rs + ns
            else
				rs := rs + IntToStr(MapSet1[k]);
        end;
    end;
    Result := rs;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.ToUtf8(const pClass, pProp: String);
var
	i: Integer;

	procedure Proc(pFile: String);
    var
    	bs: TStrings;
        i: Integer;
        b, b1: Boolean;
        s: String;
    begin
    	bs := TStringList.Create;
        bs.LoadFromFile(pFile);
		bs.NameValueSeparator := ':';

        i := 0;
        b := False;
        while i < bs.Count do
        begin
        	// Search for class
	        bs.NameValueSeparator := ':';
            if pClass <> '*' then
            begin
				if not SameText(Trim(bs.ValueFromIndex[i]), pClass) then
    	        begin
        	        Inc(i);
            	    Continue;
	            end;
    	        Inc(i);
        	end;

        	// Search for property
            b1 := False;
	        bs.NameValueSeparator := '=';
            while i < bs.Count do
            begin
            	// End of structure
	        	if SameText(Trim(bs[i]), 'end') or
                   SameText(Trim(bs[i]), 'object') then
                begin
	                Inc(i);
                	Break;
                end;

                // Skip
	        	if not SameText(Trim(bs.Names[i]), pProp) then
                begin
	                Inc(i);
    	            Continue;
                end;

                // Found
				b1 := True;
                Break;
			end;

            if not b1 then
            	Continue;

            // Processing
			if isRight(bs[i], 1) = '(' then
            begin
            	Inc(i);
	            while i < bs.Count do
    	        begin
	            	s := ToPlainUtf8(bs[i]);
	            	if s <> bs[i] then
                    begin
	            		bs[i] := s;
                        b := True;
                    end;

        	    	if isRight(bs[i], 1) = ')' then
                    	Break;
        	    	if Trim(bs[i]) = 'end' then
                    	Break;
                    Inc(i);
				end
			end
            else
            begin
                s := ToPlainUtf8(bs[i]);
                if s <> bs[i] then
                begin
                    bs[i] := s;
                    b := True;
                end;
	            Inc(i);
			end;
        end;

        if b then
        begin
        	s := ChangeFileExt(pFile, '.bak');
        	DeleteFile(s);
        	RenameFile(pFile, s);
            bs.SaveToFile(pFile);
        end;
        bs.Free;
    end;

begin
	for i := 0 to ls.Count - 1 do
    begin
    	Status.Panels[1].Text := (ls[i]);
    	Proc(ls[i]);
    end;
    Status.Panels[1].Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.SetPropValueByClass(const pClass, pProp, pValue: String;
	const force: Boolean = False);
var
	i: Integer;

	procedure Proc(pFile: String);
    var
    	bs: TStrings;
        i: Integer;
        b, b1: Boolean;
        s: String;
    begin
    	bs := TStringList.Create;
        bs.LoadFromFile(pFile);
		bs.NameValueSeparator := ':';

        i := 0;
        b := False;
        while i < bs.Count do
        begin
        	// Search for class
	        bs.NameValueSeparator := ':';
            if pClass <> '*' then
            begin
				if not SameText(Trim(bs.ValueFromIndex[i]), pClass) then
    	        begin
        	        Inc(i);
            	    Continue;
	            end;
    	        Inc(i);
        	end;

        	// Search for property
            b1 := False;
	        bs.NameValueSeparator := '=';
            while i < bs.Count do
            begin
            	// End of structure or begin of new object
	        	if SameText(Trim(bs[i]), 'end') or
                   SameText(Copy(Trim(bs[i]), 1, 6), 'object') then
                begin
                	if force then
                    begin
                    	bs.Insert(i, pProp + '=' + pValue);
	                	b := True;
                    end;
	                Inc(i);
                	Break;
                end;

            	// Skip
	        	if not SameText(Trim(bs.Names[i]), pProp) then
                begin
	                Inc(i);
    	            Continue;
                end;

                // Found
	            b1 := True;
				Break;
			end;

            if not b1 then
            	Continue;

            // Processing
            if bs.ValueFromIndex[i] <> pValue then
            begin
                bs.ValueFromIndex[i] := pValue;
                b := True;
            end;
            Inc(i);
        end;

        if b then
        begin
        	s := ChangeFileExt(pFile, '.bak');
        	DeleteFile(s);
        	RenameFile(pFile, s);
            bs.SaveToFile(pFile);
        end;
        bs.Free;
    end;

begin
	for i := 0 to ls.Count - 1 do
    begin
    	Status.Panels[1].Text := (ls[i]);
    	Proc(ls[i]);
    end;
    Status.Panels[1].Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDfm.SetPropValueByComponent(const pComName, pProp, pValue: String;
	force: Boolean = False);
var
	i: Integer;

	procedure Proc(pFile: String);
    var
    	bs: TStrings;
        i: Integer;
        b, b1: Boolean;
        s: String;
    begin
    	bs := TStringList.Create;
        bs.LoadFromFile(pFile);
		bs.NameValueSeparator := ':';

        i := 0;
        b := False;
        s := 'object ' + pComName;
        while i < bs.Count do
        begin
        	// Search for class
	        bs.NameValueSeparator := ':';
            if pComName <> '*' then
            begin
				if not SameText(Trim(bs.Names[i]), s) then
    	        begin
        	        Inc(i);
            	    Continue;
	            end;
    	        Inc(i);
        	end;

        	// Search for property
            b1 := False;
	        bs.NameValueSeparator := '=';
            while i < bs.Count do
            begin
            	// End of structure
	        	if SameText(Trim(bs[i]), 'end') or
                   SameText(Copy(Trim(bs[i]), 1, 6), 'object') then
                begin
                	if force then
                    begin
                    	bs.Insert(i, pProp + '=' + pValue);
	                	b := True;
                    end;
	                Inc(i);
                	Break;
                end;

                // Skip
	        	if not SameText(Trim(bs.Names[i]), pProp) then
                begin
	                Inc(i);
    	            Continue;
                end;

				// Found
	            b1 := True;
                Break;
			end;

            if not b1 then
            	Continue;

            // Processing
            if bs.ValueFromIndex[i] <> pValue then
            begin
                bs.ValueFromIndex[i] := pValue;
                b := True;
            end;
            Inc(i);
        end;

        if b then
        begin
        	s := ChangeFileExt(pFile, '.bak');
        	DeleteFile(s);
        	RenameFile(pFile, s);
            bs.SaveToFile(pFile);
        end;
        bs.Free;
    end;

begin
	for i := 0 to ls.Count - 1 do
    begin
    	Status.Panels[1].Text := (ls[i]);
    	Proc(ls[i]);
    end;
    Status.Panels[1].Text := '';
end;
end.
