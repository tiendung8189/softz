object FrmJob: TFrmJob
  Left = 467
  Top = 239
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch Job And Sheduler'
  ClientHeight = 562
  ClientWidth = 1023
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 541
    Width = 1023
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 44
    Width = 1023
    Height = 497
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'VISIBLE;CheckBox;True;False'
      'SYS;CustomEdit;CbType;F'
      'SZ;CheckBox;True;False'
      'HR;CheckBox;True;False'
      'KT;CheckBox;True;False'
      'SX;CheckBox;True;False'
      'OTHER;CheckBox;True;False')
    PictureMasks.Strings = (
      'MAMAU'#9'*!'#9'T'#9'T')
    Selected.Strings = (
      'job_id'#9'38'#9'job_id'#9'F'
      'job_name'#9'35'#9'job_name'#9'F'
      'job_description'#9'30'#9'job_description'#9'F'
      'job_enable'#9'6'#9'job_enable'#9'F'
      'cat_name'#9'25'#9'cat_name'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = DsDanhmuc
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgDblClickColSizing]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 14474460
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1023
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 60
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 2
    object ToolButton4: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
    end
    object ToolButton5: TToolButton
      Left = 60
      Top = 0
      Cursor = 1
      Action = CmdCancel
    end
    object ToolButton6: TToolButton
      Left = 120
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdDel
    end
    object ToolButton8: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Caption = 'Chi ti'#7871't'
      DropdownMenu = PopDetail
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = ToolButton2Click
    end
    object ToolButton9: TToolButton
      Left = 271
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 279
      Top = 0
      Cursor = 1
      Action = CmdClose
    end
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnEditError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      
        'SELECT job_id, job.name job_name, job.enabled job_enable, job.de' +
        'scription job_description, job.category_id'
      'FROM msdb.dbo.sysjobs job'
      'ORDER BY date_created')
    Left = 28
    Top = 136
    object QrDanhmucjob_id: TGuidField
      FieldName = 'job_id'
      FixedChar = True
      Size = 38
    end
    object QrDanhmucjob_name: TWideStringField
      FieldName = 'job_name'
      Size = 128
    end
    object QrDanhmucjob_enable: TWordField
      FieldName = 'job_enable'
    end
    object QrDanhmucjob_description: TWideStringField
      FieldName = 'job_description'
      Size = 512
    end
    object QrDanhmuccategory_id: TIntegerField
      FieldName = 'category_id'
    end
    object QrDanhmuccat_name: TWideStringField
      FieldKind = fkLookup
      FieldName = 'cat_name'
      LookupDataSet = QrGroup
      LookupKeyFields = 'category_id'
      LookupResultField = 'name'
      KeyFields = 'category_id'
      Size = 200
      Lookup = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 28
    Top = 168
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 60
    Top = 168
    object Tmmutin1: TMenuItem
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnClick = CmdSearchExecute
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 136
  end
  object ActionList: TActionList
    Images = DataMain.ImageNavi
    OnUpdate = ActionListUpdate
    Left = 56
    Top = 136
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 1
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ImageIndex = 3
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 5
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdChitiet: TAction
      Caption = 'Chi ti'#7871't'
    end
    object CmdJob: TAction
      Category = 'POPUP'
      Caption = 'Job'
      OnExecute = CmdJobExecute
    end
    object CmdJobDrc: TAction
      Category = 'POPUP'
      Caption = 'Create DRC (Truy'#7873'n d'#7919' li'#7879'u)'
      OnExecute = CmdJobDrcExecute
    end
  end
  object Filter1: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'KEY'
      'DESCRIPTION'
      'DESCRIPTION1')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 172
    Top = 138
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 172
    Top = 224
    object CreateAutoCleanLogile1: TMenuItem
      Caption = 'Create Auto Clean Logfile'
      OnClick = CmdJobExecute
    end
    object CreateAutoBakup1: TMenuItem
      Tag = 1
      Caption = 'Create Auto Backup'
      OnClick = CmdJobExecute
    end
    object CreateAutoCleanBackup1: TMenuItem
      Tag = 2
      Caption = 'Create Auto Clean Backup'
      OnClick = CmdJobExecute
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object CreateDRCTruyndliu1: TMenuItem
      Action = CmdJobDrc
    end
    object CreateAutoCleanRAW1: TMenuItem
      Tag = 3
      Caption = 'Create Auto Clean RAW'
      OnClick = CmdJobExecute
    end
  end
  object QrGroup: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnEditError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM msdb.dbo.syscategories'
      'WHERE name in ('#39'BACKUP_CLEAN'#39', '#39'[DRC]'#39') AND category_class=1')
    Left = 340
    Top = 264
  end
end
