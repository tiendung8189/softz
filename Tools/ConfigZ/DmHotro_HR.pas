﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmHotro_HR;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids, StdCtrls, Db,
  Wwdbgrid2, ADODb, wwdblook,
  AppEvnts, Menus, AdvMenus, DBCtrls, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmHotro_HR = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    QrDmkhac: TADOQuery;
    DsDmkhac: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdAdmin: TAction;
    QrDmkhacMANHOM_HOTRO: TWideStringField;
    QrDmkhacMA_HOTRO: TWideStringField;
    QrDmkhacTEN_HOTRO: TWideStringField;
    QrDmkhacCREATE_BY: TIntegerField;
    QrDmkhacUPDATE_BY: TIntegerField;
    QrDmkhacCREATE_DATE: TDateTimeField;
    QrDmkhacUPDATE_DATE: TDateTimeField;
    QrDmkhacMA: TWideStringField;
    QrDmkhacGHICHU: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDmkhacBeforePost(DataSet: TDataSet);
    procedure QrDmkhacBeforeDelete(DataSet: TDataSet);
    procedure QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbKhacNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrDmkhacAfterInsert(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    mLoai: String;
  public
  	procedure Execute(loai: String);
  end;

var
  FrmDmHotro_HR: TFrmDmHotro_HR;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.Execute (loai: String);
begin
    mLoai := loai;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDmkhac, 'DM_HOTRO', 0);
    SetDictionary(QrDmkhac, 'DM_HOTRO');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdNewExecute(Sender: TObject);
begin
	QrDmKhac.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdSaveExecute(Sender: TObject);
begin
	QrDmKhac.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdCancelExecute(Sender: TObject);
begin
	QrDmKhac.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdDelExecute(Sender: TObject);
begin
	QrDmKhac.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDmKhac]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.FormShow(Sender: TObject);
var
    i: Integer;
begin
	with QrDmkhac do
    begin
        Close;

        Parameters[0].Value := mLoai;
        Open;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmKhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.QrDmkhacAfterInsert(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
        FieldByName('MANHOM_HOTRO').AsString := mLoai;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.QrDmkhacBeforePost(DataSet: TDataSet);
var
    s: string;
begin
    with QrDmKhac do
    begin
        if BlankConfirm(QrDmKhac, ['MA', 'TEN_HOTRO']) then
            Abort;

        if BlankConfirm(QrDmKhac, ['TEN_HOTRO']) then
            Abort;

        s := QrDmKhac.FieldByName('MANHOM_HOTRO').AsString;
        FieldByName('MANHOM_HOTRO').AsString := s;
        FieldByName('MA_HOTRO').AsString := s + '.' + FieldByName('MA').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.QrDmkhacBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDmkhac);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDMKHAC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.CbKhacNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro_HR.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

end.
