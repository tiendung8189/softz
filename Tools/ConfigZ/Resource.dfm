object FrmResource: TFrmResource
  Left = 176
  Top = 108
  Caption = 'D'#7883'ch ng'#244'n ng'#7919' - Resource'
  ClientHeight = 573
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 0
    Top = 36
    Width = 2
    Height = 514
    OnMoved = Splitter1Moved
    ExplicitLeft = 221
    ExplicitTop = 37
    ExplicitHeight = 517
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton2: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDelete
      ImageIndex = 3
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdQuit
      ImageIndex = 5
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 792
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object GrMaster: TDBGridEh
    Left = 2
    Top = 36
    Width = 790
    Height = 514
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataGrouping.Active = True
    DataGrouping.FootersDefValues.ShowFunctionName = True
    DataGrouping.FootersDefValues.RunTimeCustomizable = True
    DataSource = DsMaster
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterRowCount = 1
    GridLineParams.GridBoundaries = True
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = 13360356
    IndicatorTitle.UseGlobalMenu = False
    OddRowColor = 16119285
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    PopupMenu = PopCat
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    STFilter.InstantApply = True
    STFilter.Local = True
    SumList.Active = True
    TabOrder = 2
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    OnDrawColumnCell = GrMasterDrawColumnCell
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ResourceName'
        Footer.DisplayFormat = '#,##0 m'#7851'u tin'
        Footer.FieldName = 'Section'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = [fsBold]
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = 'Resource Name'
        Width = 300
      end
      item
        AutoDropDown = True
        CellButtons = <>
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            FieldName = 'VALUE'
            Width = 210
          end
          item
            FieldName = 'KEY'
            Width = 70
          end>
        DropDownBox.ListFieldNames = 'KEY;VALUE'
        DropDownBox.ListSource = DsResourceCAT
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoSortMarkingEh, dlgMultiSortMarkingEh]
        DropDownWidth = 300
        DynProps = <>
        EditButtons = <>
        FieldName = 'ResourceCAT'
        Footers = <>
        LookupParams.KeyFieldNames = 'ResourceCAT'
        LookupParams.LookupDataSet = QrResourceCAT
        LookupParams.LookupDisplayFieldName = 'VALUE'
        LookupParams.LookupKeyFieldNames = 'KEY'
        Title.Caption = 'Resource CAT'
      end
      item
        CellButtons = <>
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownRows = 15
        DropDownSizing = True
        DropDownSpecRow.Value = ''
        DynProps = <>
        EditButtons = <>
        FieldName = 'ResourceType'
        Footers = <>
        LookupParams.KeyFieldNames = 'ResourceType'
        LookupParams.LookupCache = False
        LookupParams.LookupDataSet = QrResourceType
        LookupParams.LookupDisplayFieldName = 'Value'
        LookupParams.LookupKeyFieldNames = 'Key'
        Title.Caption = 'Resource Type'
        Width = 110
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ResourceValue'
        Footers = <>
        Title.Caption = 'Vietnam'
        Width = 350
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ResourceValue_EN'
        Footers = <>
        Title.Caption = 'English'
        Width = 350
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from '#9'SYS_RESOURCE'
      'order by  ResourceName')
    Left = 387
    Top = 71
    object QrMasterResourceType: TWideStringField
      FieldName = 'ResourceType'
      Size = 10
    end
    object QrMasterResourceName: TWideStringField
      FieldName = 'ResourceName'
      Size = 100
    end
    object QrMasterResourceValue: TWideStringField
      FieldName = 'ResourceValue'
      Size = 250
    end
    object QrMasterResourceValue_EN: TWideStringField
      FieldName = 'ResourceValue_EN'
      Size = 250
    end
    object QrMasterResourceCAT: TStringField
      FieldName = 'ResourceCAT'
    end
  end
  object DsMaster: TDataSource
    DataSet = QrMaster
    Left = 387
    Top = 99
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 120
    Top = 200
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDelete: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDeleteExecute
    end
    object CmdVisibleFields: TAction
      Tag = 10
      Category = 'DETAIL'
      Caption = 'Visible Fields'
    end
    object CmdSearchM: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
    end
    object CmdSearchD: TAction
      Category = 'DETAIL'
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
    end
    object CmdQuit: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdReload: TAction
      Caption = #272#7885'c l'#7841'i t'#7915' database'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdFilteredFields: TAction
      Tag = 11
      Category = 'DETAIL'
      Caption = 'Filter Fields'
    end
    object CmdNative: TAction
      Tag = 12
      Category = 'DETAIL'
      Caption = 'Native Description'
    end
    object Cmd2nd: TAction
      Tag = 13
      Category = 'DETAIL'
      Caption = '2nd Description'
    end
    object CmdFormatedFields: TAction
      Tag = 14
      Category = 'DETAIL'
      Caption = 'Formated Fields'
    end
    object CmdClearAll: TAction
      Category = 'DETAIL'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearAllExecute
    end
    object CmdSpecialFilter: TAction
      Category = 'DETAIL'
      Caption = 'L'#7885'c '#273#7863'c bi'#7879't'
    end
    object CmdSwitch: TAction
      Hint = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm Section'
      OnExecute = CmdNewExecute
    end
    object CmdShow: TAction
      Caption = 'T'#7845't c'#7843' ch'#7913'c n'#259'ng'
      OnExecute = CmdShowExecute
    end
  end
  object Filter: TwwFilterDialog2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'FIELD'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'FIELD'
      'COMMENT'
      'DESCRIPTION'
      'DESCRIPTION1'
      'FILTER'
      'FORMAT')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 64
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 456
    Top = 243
  end
  object QrResourceType: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '#39'Text'#39' [Key], '#39'Text'#39' [Value]'
      '  union all'
      '  select '#39'Message'#39' [Key], '#39'Message'#39' [Value]')
    Left = 291
    Top = 303
    object QrResourceTypeKey: TWideStringField
      FieldName = 'Key'
    end
    object QrResourceTypeValue: TWideStringField
      FieldName = 'Value'
    end
  end
  object DsResourceType: TDataSource
    DataSet = QrResourceType
    Left = 291
    Top = 331
  end
  object QrResourceCAT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrResourceCATBeforeOpen
    Parameters = <
      item
        Name = 'Item'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, *'
      
        '         from fnS_Table_By_Comma ((select Value from SYS_FLEXCON' +
        'FIG where Section = '#39'Admin'#39' and Item = :Item), '#39'='#39')')
    Left = 184
    Top = 304
  end
  object DsResourceCAT: TDataSource
    DataSet = QrResourceCAT
    Left = 184
    Top = 332
  end
  object PopCat: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Version = '2.6.6.0'
    Left = 284
    Top = 135
    object tcboco1: TMenuItem
      Tag = -1
      Action = CmdShow
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'T'#7845't c'#7843
      Checked = True
      RadioItem = True
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
end
