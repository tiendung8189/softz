(*==============================================================================
**------------------------------------------------------------------------------
*)
unit RetailReceipt;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, ComCtrls, ActnList, Grids, Db,
  Wwdbgrid, ADODb, fcStatusBar, AppEvnts, ToolWin, Wwdbigrd;

type
  TFrmRetailReceipt = class(TForm)
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    GrList: TwwDBGrid;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    QrPRINTER: TADOQuery;
    DsPRINTER: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    ToolButton9: TToolButton;
    Status: TfcStatusBar;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrPRINTERBeforePost(DataSet: TDataSet);
    procedure QrPRINTERBeforeDelete(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrListMemoOpen(Grid: TwwDBGrid; MemoDialog: TwwMemoDialog);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntFormCreate(Sender: TObject);
  private
  public
  end;

var
  FrmRetailReceipt: TFrmRetailReceipt;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.TntFormCreate(Sender: TObject);
begin
    Caption := Caption + exGetConnInfo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    QrPRINTER.Open;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrPRINTER, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.CmdNewExecute(Sender: TObject);
begin
	QrPRINTER.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.CmdSaveExecute(Sender: TObject);
begin
	QrPRINTER.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.CmdCancelExecute(Sender: TObject);
begin
	QrPRINTER.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.CmdDelExecute(Sender: TObject);
begin
	QrPRINTER.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
	with QrPRINTER do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and (not bEmpty);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.QrPRINTERBeforePost(DataSet: TDataSet);
begin
	with (QrPRINTER) do
		if BlankConfirm(QrPRINTER, ['PRINTER']) then
    		Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.QrPRINTERBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

	(*
    ** Others
    *)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if CompareText(Field.FullName, 'PRINTER') = 0 then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrPRINTER);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailReceipt.GrListMemoOpen(Grid: TwwDBGrid;
  MemoDialog: TwwMemoDialog);
begin
    with MemoDialog.Font do
    begin
        Name := 'Courier New';
        Size := 10;
        Color:= $00804000;
    end;
end;

end.
