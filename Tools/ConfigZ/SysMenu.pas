(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SysMenu;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid2, Db, ADODB, ActnList,
  Menus, ComCtrls, wwfltdlg, Wwdbgrid, wwFltDlg2, AppEvnts,
  isPanel, DBCtrls, fcStatusBar, AdvMenus, wwDialog, StdCtrls, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, System.StrUtils;

type
  TFrmSysMenu = class(TForm)
    Splitter1: TSplitter;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    ActionList: TActionList;
    CmdSearchD: TAction;
    CmdQuit: TAction;
    Filter: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdDelete: TAction;
    CmdReload: TAction;
    CmdVisibleFields: TAction;
    CmdFilteredFields: TAction;
    CmdNative: TAction;
    Cmd2nd: TAction;
    CmdClearAll: TAction;
    CmdFormatedFields: TAction;
    ApplicationEvents1: TApplicationEvents;
    CmdSearchM: TAction;
    CmdSpecialFilter: TAction;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    Status: TfcStatusBar;
    CmdNew: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    GrMaster: TDBGridEh;
    QrSysFunc: TADOQuery;
    DsSysFunc: TDataSource;
    QrMasterID: TAutoIncField;
    QrMasterSort: TIntegerField;
    QrMasterFUNC_CODE: TWideStringField;
    QrMasterTag: TWideStringField;
    QrMasterTitle: TWideStringField;
    QrMasterIcon: TWideStringField;
    QrMasterIconCss: TWideStringField;
    QrMasterHref: TWideStringField;
    QrMasterStatus: TWideStringField;
    QrMasterVisible: TBooleanField;
    QrMasterParent_Id: TIntegerField;
    QrMasterHorizontal: TBooleanField;
    QrMasterVertical: TBooleanField;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    QrMasterLK_Enable: TBooleanField;
    QrMasterLK_Description: TWideStringField;
    QrMasterIconColor: TWideStringField;
    QrMasterSZ: TBooleanField;
    QrMasterHR: TBooleanField;
    QrMasterFB: TBooleanField;
    QrMasterEMP: TBooleanField;
    QrMasterOTHER: TBooleanField;
    QrIconColor: TADOQuery;
    DsIconColor: TDataSource;
    QrMasterLK_SZ: TBooleanField;
    QrMasterLK_HR: TBooleanField;
    QrMasterLK_FB: TBooleanField;
    QrMasterLK_EMP: TBooleanField;
    QrMasterLK_OTHER: TBooleanField;
    QrMasterLK_ACC: TBooleanField;
    QrMasterLK_POM: TBooleanField;
    QrMasterACC: TBooleanField;
    QrMasterPOM: TBooleanField;
    QrMasterASS: TBooleanField;
    QrMasterLK_ACTION_TYPE: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdClearAllExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure GrMasterDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrMasterFUNC_CODEChange(Sender: TField);
    procedure GrMasterKeyPress(Sender: TObject; var Key: Char);
    procedure GrMasterColumns13GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns5GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns6GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns8GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns9GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns10GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns12GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns11GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
    procedure GrMasterColumns7GetCellParams(Sender: TObject; EditMode: Boolean;
      Params: TColCellParamsEh);
  private
    mFilterStr, mMasterSQL, mCurrentFilter: String;
    mFiltered: Boolean;
  public
      procedure DisableColumnByFieldName (EditMode: Boolean; Params: TColCellParamsEh; FieldName: String = '');
  end;

var
  FrmSysMenu: TFrmSysMenu;

implementation

uses
	MainData, islib, isMsg, isDb, ExCommon, NewString, GuidEx, ResourceForm;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;
    AddAllFields(QrMaster, 'SYS_MENU', 0);
    mMasterSQL := QrMaster.SQL.Text;
    mCurrentFilter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.FormShow(Sender: TObject);
begin
    QrMaster.SQL.Text := mMasterSQL;
	OpenDataSets([QrMaster, QrSysFunc, QrIconColor]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns10GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'ASS');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns11GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'EMP');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmSysMenu.GrMasterColumns12GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'OTHER');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns13GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'Title');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns5GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'SZ');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns6GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'POM');
end;
procedure TFrmSysMenu.GrMasterColumns7GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'FB');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterColumns8GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'HR');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmSysMenu.GrMasterColumns9GetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
     DisableColumnByFieldName(EditMode, Params, 'ACC');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
     if Column.ReadOnly then
     begin
        Column.Color := clBtnFace;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.GrMasterKeyPress(Sender: TObject; var Key: Char);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrMaster, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Visible').AsBoolean := True;
        FieldByName('Horizontal').AsBoolean := True;
        FieldByName('Vertical').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.QrMasterBeforeDelete(DataSet: TDataSet);
begin
     if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Sort', 'Title']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);


    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.QrMasterFUNC_CODEChange(Sender: TField);
begin
    with QrMaster do
    begin
        if Sender.AsString <> '' then
        begin
            FieldByName('Visible').AsBoolean :=  FieldByName('LK_Enable').AsBoolean;
            FieldByName('Title').AsString := FieldByName('FUNC_CODE').AsString;
            FieldByName('SZ').AsBoolean :=  FieldByName('LK_SZ').AsBoolean;
            FieldByName('HR').AsBoolean :=  FieldByName('LK_HR').AsBoolean;
            FieldByName('ACC').AsBoolean :=  FieldByName('LK_ACC').AsBoolean;
            FieldByName('POM').AsBoolean :=  FieldByName('LK_POM').AsBoolean;
            FieldByName('FB').AsBoolean :=  FieldByName('LK_FB').AsBoolean;
            FieldByName('EMP').AsBoolean :=  FieldByName('LK_EMP').AsBoolean;
            FieldByName('OTHER').AsBoolean :=  FieldByName('LK_OTHER').AsBoolean;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
//    Status.Panels[0].Text := isDb.RecordCount(QrMaster);
//    Status.Panels[1].Text := isdb.RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse: Boolean;
begin
    with QrMaster do
    begin
         bBrowse := State in [dsBrowse];
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDelete.Enabled := bBrowse;
    CmdSearchD.Enabled := bBrowse;
    CmdClearAll.Enabled := mFiltered;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdSwitchExecute(Sender: TObject);
begin
    GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdNewExecute(Sender: TObject);
var
    bm: TBytes;
begin
//    Application.CreateForm(TFrmSysMenuForm, FrmSysMenuForm);
//    FrmSysMenuForm.ShowModal;
	with QrMaster do
    begin
    	DisableControls;
        bm := BookMark;
        CmdReload.Execute;
        BookMark := bm;
    	EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdSaveExecute(Sender: TObject);
begin
    QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdCancelExecute(Sender: TObject);
begin
    QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdDeleteExecute(Sender: TObject);
begin
    QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdReloadExecute(Sender: TObject);
begin
	QrMaster.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.CmdClearAllExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSysMenu.DisableColumnByFieldName (EditMode: Boolean; Params: TColCellParamsEh; FieldName: String = '');
begin
     with QrMaster do
     begin
         if ((FieldByName('FUNC_CODE').AsString <> '')
                and (FieldByName('LK_ACTION_TYPE').AsInteger > 0)
                and AnsiMatchStr(FieldName, ['SZ', 'POM', 'FB', 'HR', 'ACC', 'ASS', 'EMP', 'OTHER', 'Title'])) then
         begin
            Params.Background := clBtnFace;
            Params.ReadOnly := True;
         end
         else
         begin
            Params.ReadOnly := False;
         end;
     end;
end;


end.
