(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FlexConfig;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid2, Db, ADODB, ActnList,
  Menus, ComCtrls, wwfltdlg, Wwdbgrid, wwFltDlg2, AppEvnts,
  isPanel, DBCtrls, fcStatusBar, AdvMenus, wwDialog, StdCtrls, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmFlexConfig = class(TForm)
    Splitter1: TSplitter;
    QrDetail: TADOQuery;
    QrMaster: TADOQuery;
    DsDetail: TDataSource;
    DsMaster: TDataSource;
    ActionList: TActionList;
    CmdSearchD: TAction;
    CmdQuit: TAction;
    Filter: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdDelete: TAction;
    CmdReload: TAction;
    PopRight: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdVisibleFields: TAction;
    CmdFilteredFields: TAction;
    N3: TMenuItem;
    CmdNative: TAction;
    Cmd2nd: TAction;
    CmdClearAll: TAction;
    ClearAllFiltered1: TMenuItem;
    CmdFormatedFields: TAction;
    ApplicationEvents1: TApplicationEvents;
    PaDetail: TisPanel;
    TntPopupMenu1: TAdvPopupMenu;
    CmdSearchM: TAction;
    Search1: TMenuItem;
    CmdSpecialFilter: TAction;
    vlPanel1: TisPanel;
    EdComment: TDBMemo;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    Status: TfcStatusBar;
    spl1: TSplitter;
    QrMasterSection: TWideStringField;
    QrDetailIdx: TGuidField;
    QrDetailSection: TWideStringField;
    QrDetailItem: TWideStringField;
    QrDetailValue: TWideStringField;
    QrDetailVisible: TBooleanField;
    QrDetailComment: TWideMemoField;
    QrDetailCREATE_BY: TIntegerField;
    QrDetailUPDATE_BY: TIntegerField;
    QrDetailCREATE_DATE: TDateTimeField;
    QrDetailUPDATE_DATE: TDateTimeField;
    CmdNew: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    GrDetail: TDBGridEh;
    GrMaster: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdSearchDExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure CmdClearAllExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure QrMasterAfterScroll(DataSet: TDataSet);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdSearchMExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure CmdNewExecute(Sender: TObject);
    procedure QrDetailAfterInsert(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
  private
    mDetailSQL, mFilterStr, mMasterSQL, mCurrentFilter: String;
    mFiltered: Boolean;
  public
    { Public declarations }
  end;

var
  FrmFlexConfig: TFrmFlexConfig;

implementation

uses
	MainData, islib, isMsg, isDb, ExCommon, NewString, GuidEx;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;
//    if GetSystemMetrics(SM_CXSCREEN) > 800 then
//    	GrDetail.Options := GrDetail.Options + [dgProportionalColResize];

    AddAllFields(QrDetail, 'SYS_FLEXCONFIG', 0);

//    ResetRowColor([GrMaster, GrDetail]);
    mDetailSQL := QrDetail.SQL.Text;
    mMasterSQL := QrMaster.SQL.Text;
    mCurrentFilter := '';
    mFilterStr := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.FormShow(Sender: TObject);
begin
    QrMaster.SQL.Text := mMasterSQL;
	OpenDataSets([QrMaster, QrDetail]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.QrMasterAfterScroll(DataSet: TDataSet);
begin
    PaDetail.HeaderCaption := ' :: ' + QrMaster.FieldByName('Section').AsString;
    if not QrMaster.IsEmpty then
    begin
       GrDetail.SumList.RecalcAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.QrDetailAfterInsert(DataSet: TDataSet);
begin
    with QrDetail do
    begin
        FieldByName('Visible').AsBoolean := True;
        TGuidField(FieldByName('Idx')).AsGuid := TGuidEx.NewGuid;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.QrDetailBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.QrDetailBeforeOpen(DataSet: TDataSet);
begin
    QrDetail.SQL.Text := mDetailSQL + mFilterStr;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.QrDetailBeforePost(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if BlankConfirm(QrDetail, ['Item']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*
    **  Grid
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
//    Status.Panels[0].Text := isDb.RecordCount(QrMaster);
//    Status.Panels[1].Text := isdb.RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse: Boolean;
begin
    bBrowse := QrDetail.State in [dsBrowse];

    with QrMaster do
    begin
        CmdDelete.Enabled := not IsEmpty;
        CmdSearchM.Enabled := bBrowse;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;

    CmdSearchD.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdClearAll.Enabled := mFiltered;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrMaster then
        GrDetail.SetFocus
    else if ActiveControl = GrDetail then
        EdComment.SetFocus
    else
        GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdSearchMExecute(Sender: TObject);
begin
    exSearch(Name + '_M', DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdSearchDExecute(Sender: TObject);
begin
    exSearch(Name + '_D', DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdFilterExecute(Sender: TObject);
begin
    //Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdNewExecute(Sender: TObject);
var
    newSection: String;
begin
    newSection := FrmNewString.GetString;
    if newSection = '' then
        Exit;

    with QrDetail do
    begin
        Append;
        FieldByName('Section').AsString := newSection;
        FieldByName('Item').AsString := newSection;
        Post;
    end;
    with QrMaster do
    begin
        Requery;

        if not Locate('Section', newSection, []) then
            First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdClearFilterExecute(Sender: TObject);
begin
    Filter.ClearFilter
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdSaveExecute(Sender: TObject);
begin
    QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdCancelExecute(Sender: TObject);
begin
    QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdDeleteExecute(Sender: TObject);
var
    s, s1: String;
begin
    QrDetail.Delete;

    with QrMaster do
    begin
        Requery;
        if s1 <> '' then
            if not Locate('Section', s1, []) then
                First;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdReloadExecute(Sender: TObject);
begin
	QrMaster.Requery;
    QrDetail.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexConfig.CmdClearAllExecute(Sender: TObject);
var
    n, i: Integer;
begin
    mFiltered := False;
    CmdVisibleFields.Checked := False;
    CmdFilteredFields.Checked := False;
    CmdNative.Checked := False;
    Cmd2nd.Checked := False;
    CmdFormatedFields.Checked := False;

    with QrDetail do
    begin
        Close;
        Open;
    end;
end;
end.
