object FrmFlexEdit: TFrmFlexEdit
  Left = 322
  Top = 106
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'T'#249'y Bi'#7871'n - Flexible Settings'
  ClientHeight = 573
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = TntFormCreate
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 221
    Top = 36
    Width = 2
    Height = 514
    OnMoved = Splitter1Moved
    ExplicitLeft = 223
    ExplicitTop = 37
    ExplicitHeight = 517
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton4: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 54
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 108
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object GrMaster: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 221
    Height = 514
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'Section'#9'28'#9'Section'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alLeft
    Color = clWhite
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgTabExitsOnLastCol]
    ParentFont = False
    PopupMenu = PopLeft
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnExit = GrMasterExit
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object PaDetail: TisPanel
    Left = 223
    Top = 36
    Width = 569
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    Color = 16119285
    ParentBackground = False
    TabOrder = 2
    HeaderCaption = ' :: '
    HeaderColor = 8404992
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object GrDetail: TwwDBGrid2
      Left = 0
      Top = 16
      Width = 569
      Height = 498
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'Item'#9'20'#9'Item'#9'F'
        'Value'#9'56'#9'Value'#9'F')
      MemoAttributes = [mSizeable, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgTabExitsOnLastCol, dgProportionalColResize]
      ParentFont = False
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 16119285
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 792
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 60
    Top = 136
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdReload: TAction
      Caption = #272#7885'c l'#7841'i t'#7915' database'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdSwitch: TAction
      Hint = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdCopy: TAction
      Caption = 'Nh'#226'n b'#7843'n m'#7851'u tin'
      OnExecute = CmdCopyExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 136
  end
  object QrParams: TADOQuery
    Connection = DataMain.Conn
    Parameters = <>
    SQL.Strings = (
      'select'#9'FLEXIBLE'
      '  from'#9'SYS_CONFIG')
    Left = 306
    Top = 81
  end
  object DsDetail: TDataSource
    DataSet = tbDetail
    Left = 333
    Top = 212
  end
  object DsMaster: TDataSource
    DataSet = tbMaster
    Left = 302
    Top = 213
  end
  object PopLeft: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 60
    Top = 166
    object Search1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Copy1: TMenuItem
      Action = CmdCopy
    end
  end
  object tbMaster: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    AfterScroll = QrMasterAfterScroll
    Left = 302
    Top = 185
    object tbMasterSection: TWideStringField
      FieldName = 'Section'
      OnValidate = QrMasterSectionValidate
      Size = 50
    end
    object tbMasterDescription: TWideStringField
      FieldName = 'Description'
      Visible = False
      Size = 200
    end
    object tbMasterIDX: TIntegerField
      FieldName = 'IDX'
    end
    object tbMasterEnable: TByteField
      FieldName = 'Enable'
    end
    object tbMasterFill: TByteField
      FieldName = 'Fill'
    end
    object tbMasterContent: TMemoField
      FieldName = 'Content'
      BlobType = ftMemo
    end
  end
  object tbDetail: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    MasterFields = 'IDX'
    DetailFields = 'IDX'
    MasterSource = DsMaster
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    BeforeInsert = QrDetailBeforeInsert
    AfterInsert = QrDetailAfterInsert
    BeforePost = QrDetailBeforePost
    AfterPost = QrDetailAfterPost
    AfterDelete = QrDetailAfterInsert
    Left = 332
    Top = 184
    object tbDetailSection: TWideStringField
      FieldName = 'Section'
      Size = 50
    end
    object tbDetailIDX: TIntegerField
      FieldName = 'IDX'
    end
    object tbDetailItem: TWideStringField
      FieldName = 'Item'
      Size = 50
    end
    object tbDetailValue: TWideStringField
      FieldName = 'Value'
      Size = 255
    end
    object tbDetailValue2: TWideStringField
      FieldName = 'Value2'
      Size = 255
    end
    object tbDetailValue3: TWideStringField
      FieldName = 'Value3'
      Size = 255
    end
    object tbDetailValue4: TWideStringField
      FieldName = 'Value4'
      Size = 255
    end
    object tbDetailValue5: TWideStringField
      FieldName = 'Value5'
      Size = 255
    end
    object tbDetailValue6: TWideStringField
      FieldName = 'Value6'
      Size = 255
    end
    object tbDetailValue7: TWideStringField
      FieldName = 'Value7'
      Size = 255
    end
    object tbDetailValue8: TWideStringField
      FieldName = 'Value8'
      Size = 255
    end
    object tbDetailValue9: TWideStringField
      FieldName = 'Value9'
      Size = 255
    end
    object tbDetailDisable: TByteField
      FieldName = 'Disable'
    end
  end
end
