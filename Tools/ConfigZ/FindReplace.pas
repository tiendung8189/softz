﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FindReplace;

interface

uses
  Classes, Controls, Forms, SysUtils,
  StdCtrls, Buttons, ExtCtrls;

type
  TFrmReplaceDialog = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    pnl1: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    EdSearch: TEdit;
    EdReplace: TEdit;
    CkCase: TCheckBox;
    TntLabel1: TLabel;
    btSelectFile: TSpeedButton;
    EdDir: TEdit;
    LbFileExt: TLabel;
    CbFileExt: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btSelectFileClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    ls: TStrings;
    sPath: string;
  public
  	function GetStr (var sF, sR : String; var ci : Boolean) : Boolean;
    procedure GetFileList(const pPath: string);
  end;

var
  FrmReplaceDialog: TFrmReplaceDialog;

implementation
uses
    isLib, isFile, isMsg;
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReplaceDialog.GetFileList(const pPath: string);
var
	dta: TSearchRec;
    n: Integer;
    sPath :string;
begin
    ls.Clear;
    sPath := ExtractFileDir(pPath);
	n := FindFirst(pPath, faAnyFile, dta);
    while n = 0 do
    begin
    	if dta.Attr and faDirectory <> 0 then
        else
        	ls.Add(sPath + '\' + dta.Name);
		n := FindNext(dta);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmReplaceDialog.GetStr;
begin
	EdSearch.Text := sF;
	EdReplace.Text := sR;
	CkCase.Checked := ci;

    Result := ShowModal = mrOK;

    sF := EdSearch.Text;
    sR := EdReplace.Text;
	ci := CkCase.Checked;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReplaceDialog.BitBtn1Click(Sender: TObject);
var
    i, n: Integer;
    s1, s2: String;
    function One (sF, sR, sFile: String) : Boolean;
    var
        sSource: String;
        sTex: TStrings;
    begin
    	Result := False;

        //File goc
    	sSource := sFile;

        sTex := TStringList.Create;
        with sTex do
        begin
            LoadFromFile(sSource);
            if Pos(UpperCase(sF), UpperCase(sTex.Text)) > 0 then
            begin
                Text := StringReplace(sTex.Text, sF, sR, [rfReplaceAll, rfIgnoreCase]);
                SaveToFile(sSource, TEncoding.UTF8);
                Inc(n);
            end;
            Free;
        end;
    end;
begin
    GetFileList(EdDir.Text + '\' + CbFileExt.Text);
    if ls.Count = 0 then
    begin
        Msg(Format('Không tìm thấy file "%s"', [CbFileExt.Text]));
        Exit;
    end;

    n := 0;
    try
        s1 := EdSearch.Text;
        s2 := EdReplace.Text;
        for i := 0 to ls.Count - 1 do
            One (s1, s2, ls.Strings[i]);

    except
        on E: Exception do
            ErrMsg('Lỗi: ' + E.Message);
    end;

    Msg(Format('-== %d form(s) patched. ==-', [n]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReplaceDialog.btSelectFileClick(Sender: TObject);
begin
    sPath := isGetDir('Chọn thư mục', RegRead(Self.Name, 'LastFolder', ''));
    if sPath = '' then
        Exit;
    RegWrite(Self.Name, 'LastFolder', sPath);
    EdDir.Text := sPath;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReplaceDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    ls.Free;
end;

procedure TFrmReplaceDialog.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReplaceDialog.FormShow(Sender: TObject);
begin
	EdSearch.SetFocus;
    ls := TStringList.Create;
end;

end.
