object FrmHotroX: TFrmHotroX
  Left = 467
  Top = 239
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Lo'#7841'i Danh M'#7909'c H'#7895' Tr'#7907
  ClientHeight = 562
  ClientWidth = 932
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 541
    Width = 932
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 932
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
    end
    object ToolButton8: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdChitiet
      ImageIndex = 8
    end
    object ToolButton9: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
    end
  end
  object CbType: TwwDBComboBox
    Left = 456
    Top = 144
    Width = 121
    Height = 24
    ShowButton = True
    Style = csDropDown
    MapList = True
    AllowClearKey = False
    DataField = 'SYS'
    DataSource = DsDanhmuc
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      '1.Danh m'#7909'c h'#7895' tr'#7907#9'1'
      '0.H'#7879' th'#7889'ng'#9'0')
    Sorted = False
    TabOrder = 2
    UnboundDataType = wwDefault
  end
  object GrMaster: TDBGridEh
    Left = 0
    Top = 36
    Width = 932
    Height = 505
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataGrouping.Active = True
    DataGrouping.FootersDefValues.ShowFunctionName = True
    DataGrouping.FootersDefValues.RunTimeCustomizable = True
    DataSource = DsDanhmuc
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterRowCount = 1
    GridLineParams.GridBoundaries = True
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = 13360356
    IndicatorTitle.UseGlobalMenu = False
    OddRowColor = 16119285
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    STFilter.InstantApply = True
    STFilter.Local = True
    SumList.Active = True
    TabOrder = 3
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    OnGetCellParams = GrMasterGetCellParams
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'MANHOM_HOTRO'
        Footers = <>
        Width = 250
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'TENNHOM_HOTRO'
        Footers = <>
        Width = 280
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'VISIBLE'
        Footers = <>
        Width = 55
      end
      item
        AutoDropDown = True
        CaseInsensitiveTextSearch = False
        CellButtons = <>
        DropDownBox.Columns = <
          item
            FieldName = 'TEN_HOTRO'
          end>
        DropDownBox.ListFieldNames = 'MA_HOTRO;TEN_HOTRO'
        DropDownBox.ListSource = DsType
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoSortMarkingEh, dlgMultiSortMarkingEh]
        DynProps = <>
        EditButtons = <>
        FieldName = 'SYS'
        Footers = <>
        LimitTextToListValues = True
        LookupParams.KeyFieldNames = 'SYS'
        LookupParams.LookupDataSet = QrType
        LookupParams.LookupDisplayFieldName = 'TEN_HOTRO'
        LookupParams.LookupKeyFieldNames = 'MA_HOTRO'
        Width = 122
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'SORT'
        Footers = <>
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = 'PROJECT|SZ'
        Width = 55
        WordWrap = True
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'HR'
        Footers = <>
        Title.Caption = 'PROJECT|HR'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FB'
        Footers = <>
        Title.Caption = 'PROJECT|FB'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'POM'
        Footers = <>
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ACC'
        Footers = <>
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ASS'
        Footers = <>
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'OTHER'
        Footers = <>
        Title.Caption = 'PROJECT|OTHER'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'GHICHU'
        Footers = <>
        Width = 280
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnEditError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_HOTRO_NHOM')
    Left = 28
    Top = 136
    object QrDanhmucMANHOM_HOTRO: TWideStringField
      FieldName = 'MANHOM_HOTRO'
      Size = 50
    end
    object QrDanhmucTENNHOM_HOTRO: TWideStringField
      FieldName = 'TENNHOM_HOTRO'
      Size = 200
    end
    object QrDanhmucVISIBLE: TBooleanField
      FieldName = 'VISIBLE'
    end
    object QrDanhmucSYS: TIntegerField
      FieldName = 'SYS'
    end
    object QrDanhmucSORT: TIntegerField
      FieldName = 'SORT'
    end
    object QrDanhmucGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDanhmucSZ: TBooleanField
      FieldName = 'SZ'
    end
    object QrDanhmucHR: TBooleanField
      FieldName = 'HR'
    end
    object QrDanhmucOTHER: TBooleanField
      FieldName = 'OTHER'
    end
    object QrDanhmucFB: TBooleanField
      FieldName = 'FB'
    end
    object QrDanhmucACC: TBooleanField
      FieldName = 'ACC'
    end
    object QrDanhmucPOM: TBooleanField
      FieldName = 'POM'
    end
    object QrDanhmucASS: TBooleanField
      FieldName = 'ASS'
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 28
    Top = 168
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 60
    Top = 168
    object Tmmutin1: TMenuItem
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnClick = CmdSearchExecute
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 136
  end
  object ActionList: TActionList
    Images = DataMain.ImageNavi
    OnUpdate = ActionListUpdate
    Left = 56
    Top = 136
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 1
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ImageIndex = 3
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 5
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdChitiet: TAction
      Caption = 'Chi ti'#7871't'
      OnExecute = CmdChitietExecute
    end
  end
  object Filter1: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'KEY'
      'DESCRIPTION'
      'DESCRIPTION1')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 172
    Top = 138
  end
  object QrType: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnEditError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      'select 1 MA_HOTRO, N'#39'Danh m'#7909'c h'#7895' tr'#7907#39' TEN_HOTRO'
      'union all'
      'select 0 MA_HOTRO, N'#39'H'#7879' th'#7889'ng'#39' TEN_HOTRO')
    Left = 220
    Top = 176
    object QrTypeMA_HOTRO: TIntegerField
      FieldName = 'MA_HOTRO'
      ReadOnly = True
    end
    object QrTypeTEN_HOTRO: TWideStringField
      FieldName = 'TEN_HOTRO'
      ReadOnly = True
      Size = 15
    end
  end
  object DsType: TDataSource
    DataSet = QrType
    Left = 228
    Top = 216
  end
end
