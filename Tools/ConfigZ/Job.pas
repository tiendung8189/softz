(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Job;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus,
  AppEvnts, Wwdbgrid, Wwdbigrd, AdvMenus, wwfltdlg, wwFltDlg2,
  Wwdbcomb, wwDialog, StdCtrls, Mask, wwdbedit, Wwdotdot, ToolWin;

type
  TFrmJob = class(TForm)
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    N1: TMenuItem;
    Filter1: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    CmdChitiet: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    QrDanhmucjob_id: TGuidField;
    QrDanhmucjob_name: TWideStringField;
    QrDanhmucjob_enable: TWordField;
    QrDanhmucjob_description: TWideStringField;
    PopDetail: TAdvPopupMenu;
    CmdJob: TAction;
    CreateAutoBakup1: TMenuItem;
    CreateAutoCleanBackup1: TMenuItem;
    CreateAutoCleanLogile1: TMenuItem;
    N2: TMenuItem;
    CreateDRCTruyndliu1: TMenuItem;
    CmdJobDrc: TAction;
    QrGroup: TADOQuery;
    QrDanhmuccategory_id: TIntegerField;
    QrDanhmuccat_name: TWideStringField;
    CreateAutoCleanRAW1: TMenuItem;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdJobExecute(Sender: TObject);
    procedure CmdJobDrcExecute(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
  public
  	procedure Execute;
  end;

var
  FrmJob: TFrmJob;

implementation

uses
	isDb, isMsg, ExCommon, isLib, DmHotro, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.Execute;
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    Caption := Caption + exGetConnInfo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.FormShow(Sender: TObject);
var
    i: Integer;
    sName: string;
begin
    with QrDanhmuc do
    begin
	    Open;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;
	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter1 do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdDelExecute(Sender: TObject);
var
    s: String;
begin

    with QrDanhmuc do
    begin
        s := FieldByName('job_name').AsString;
        DataMain.Conn.Execute(Format('delete msdb.dbo.sysjobschedules where schedule_id in (select schedule_id from msdb.dbo.sysschedules where name = ''%s'')', [s]));
        DataMain.Conn.Execute(Format('delete msdb.dbo.sysschedules where name = ''%s''', [s]));

    	Delete;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdFilterExecute(Sender: TObject);
begin
	Filter1.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdJobDrcExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdJobExecute(Sender: TObject);
var
    storeName: String;
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        1: storeName := 'SYS_JOB_BACKUP;1';
        2: storeName := 'SYS_JOB_CLEAN;1';
        3: storeName := 'SYS_JOB_CLEAN_RAW;1'
    else
        storeName := 'SYS_JOB_CLEAN_LOG;1'
    end;

    with TADOCommand.Create(nil) do
    begin
    	Connection := DataMain.Conn;
        CommandTimeout := 1200;
        CommandText :=  storeName;
        try
            Prepared := True;
            Execute;
        except
            on E: Exception do
                raise Exception.Create('Error: ' + E.Message);
        end;
        Free;
    end;

    MsgDone;
    QrDanhmuc.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.QrDanhmucBeforePost(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.QrDanhmucBeforeDelete(DataSet: TDataSet);
var
    s: String;
begin
	if not DeleteConfirm then
    	Abort;

    with QrDanhmuc do
    begin
        s := FieldByName('job_name').AsString;
        DataMain.Conn.Execute(Format('delete msdb.dbo.sysjobschedules where schedule_id in (select schedule_id from msdb.dbo.sysschedules where name = ''%s'')', [s]));
        DataMain.Conn.Execute(Format('delete msdb.dbo.sysschedules where name = ''%s''', [s]));
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.ToolButton2Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and (not bEmpty);

    CmdChitiet.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmJob.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

end.
