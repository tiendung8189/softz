object FrmSysMenu: TFrmSysMenu
  Left = 176
  Top = 108
  Caption = 'Danh S'#225'ch Menu Website - Menu Website'
  ClientHeight = 573
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 0
    Top = 36
    Width = 2
    Height = 514
    OnMoved = Splitter1Moved
    ExplicitLeft = 221
    ExplicitTop = 37
    ExplicitHeight = 517
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton2: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDelete
      ImageIndex = 3
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdQuit
      ImageIndex = 5
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 792
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object GrMaster: TDBGridEh
    Left = 2
    Top = 36
    Width = 790
    Height = 514
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataGrouping.Active = True
    DataGrouping.FootersDefValues.ShowFunctionName = True
    DataGrouping.FootersDefValues.RunTimeCustomizable = True
    DataSource = DsMaster
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterRowCount = 1
    GridLineParams.GridBoundaries = True
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = 13360356
    IndicatorTitle.UseGlobalMenu = False
    OddRowColor = clWindow
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    STFilter.InstantApply = True
    STFilter.Local = True
    SumList.Active = True
    TabOrder = 2
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    OnDrawColumnCell = GrMasterDrawColumnCell
    OnKeyPress = GrMasterKeyPress
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ID'
        Footers = <>
        Width = 50
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Sort'
        Footers = <>
      end
      item
        AutoDropDown = True
        CellButtons = <>
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            FieldName = 'DESCRIPTION'
            Width = 350
          end
          item
            FieldName = 'FUNC_CODE'
            Width = 300
          end>
        DropDownBox.ListFieldNames = 'FUNC_CODE;DESCRIPTION'
        DropDownBox.ListSource = DsSysFunc
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoSortMarkingEh, dlgMultiSortMarkingEh]
        DropDownRows = 15
        DropDownWidth = 670
        DynProps = <>
        EditButtons = <>
        FieldName = 'FUNC_CODE'
        Footers = <>
        LimitTextToListValues = False
        LookupParams.KeyFieldNames = 'FUNC_CODE'
        LookupParams.LookupDataSet = QrSysFunc
        LookupParams.LookupDisplayFieldName = 'DESCRIPTION'
        LookupParams.LookupKeyFieldNames = 'FUNC_CODE'
        STFilter.DataField = 'FUNC_CODE'
        STFilter.KeyField = 'FUNC_CODE'
        STFilter.ListField = 'DESCRIPTION'
        STFilter.ListSource = DsSysFunc
        Title.Caption = 'FUNC_CODE|Name'
        Width = 300
      end
      item
        CellButtons = <>
        Color = cl3DLight
        DynProps = <>
        EditButtons = <>
        FieldName = 'LK_Enable'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'FUNC_CODE|Enable'
        Width = 45
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Visible'
        Footers = <>
        Width = 45
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        OnGetCellParams = GrMasterColumns5GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'POM'
        Footers = <>
        OnGetCellParams = GrMasterColumns6GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FB'
        Footers = <>
        OnGetCellParams = GrMasterColumns7GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'HR'
        Footers = <>
        OnGetCellParams = GrMasterColumns8GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ACC'
        Footers = <>
        OnGetCellParams = GrMasterColumns9GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ASS'
        Footers = <>
        OnGetCellParams = GrMasterColumns10GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'EMP'
        Footers = <>
        OnGetCellParams = GrMasterColumns11GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'OTHER'
        Footers = <>
        OnGetCellParams = GrMasterColumns12GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Title'
        Footers = <>
        Title.Caption = 'Title | (FUNC_CODE/ Resource Name)'
        Width = 200
        OnGetCellParams = GrMasterColumns13GetCellParams
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Parent_Id'
        Footers = <>
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Horizontal'
        Footers = <>
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Vertical'
        Footers = <>
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Tag'
        Footers = <>
        Width = 100
      end
      item
        AutoDropDown = True
        ButtonStyle = cbsDropDown
        CellButtons = <>
        DisplayFormat = 'Value'
        DropDownBox.ColumnDefValues.AutoDropDown = True
        DropDownBox.ListFieldNames = 'Value'
        DropDownBox.ListSource = DsIconColor
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DynProps = <>
        EditButton.Visible = True
        EditButtons = <>
        FieldName = 'IconColor'
        Footers = <>
        LimitTextToListValues = True
        LookupParams.KeyFieldNames = 'IconColor'
        LookupParams.LookupDataSet = QrIconColor
        LookupParams.LookupDisplayFieldName = 'Value'
        LookupParams.LookupKeyFieldNames = 'Value'
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Icon'
        Footers = <>
        Width = 100
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'IconCss'
        Footers = <>
        Width = 200
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Href'
        Footers = <>
        Width = 250
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Status'
        Footers = <>
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from '#9'SYS_MENU'
      'order by  Sort')
    Left = 387
    Top = 71
    object QrMasterID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QrMasterSort: TIntegerField
      FieldName = 'Sort'
    end
    object QrMasterFUNC_CODE: TWideStringField
      FieldName = 'FUNC_CODE'
      OnChange = QrMasterFUNC_CODEChange
      Size = 100
    end
    object QrMasterTag: TWideStringField
      FieldName = 'Tag'
      Size = 100
    end
    object QrMasterTitle: TWideStringField
      FieldName = 'Title'
      Size = 100
    end
    object QrMasterIcon: TWideStringField
      FieldName = 'Icon'
      Size = 100
    end
    object QrMasterIconCss: TWideStringField
      FieldName = 'IconCss'
      Size = 200
    end
    object QrMasterHref: TWideStringField
      FieldName = 'Href'
      Size = 500
    end
    object QrMasterStatus: TWideStringField
      FieldName = 'Status'
      Size = 50
    end
    object QrMasterVisible: TBooleanField
      FieldName = 'Visible'
    end
    object QrMasterParent_Id: TIntegerField
      FieldName = 'Parent_Id'
    end
    object QrMasterHorizontal: TBooleanField
      FieldName = 'Horizontal'
    end
    object QrMasterVertical: TBooleanField
      FieldName = 'Vertical'
    end
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterLK_Enable: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Enable'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'ENABLED'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_Description: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Description'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'FUNC_CODE'
      Size = 200
      Lookup = True
    end
    object QrMasterIconColor: TWideStringField
      FieldName = 'IconColor'
    end
    object QrMasterSZ: TBooleanField
      FieldName = 'SZ'
    end
    object QrMasterHR: TBooleanField
      FieldName = 'HR'
    end
    object QrMasterFB: TBooleanField
      FieldName = 'FB'
    end
    object QrMasterEMP: TBooleanField
      FieldName = 'EMP'
    end
    object QrMasterOTHER: TBooleanField
      FieldName = 'OTHER'
    end
    object QrMasterLK_SZ: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_SZ'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'SZ'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_HR: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_HR'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'HR'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_FB: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_FB'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'FB'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_EMP: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_EMP'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'EMP'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_OTHER: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_OTHER'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'OTHER'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_ACC: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_ACC'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'ACC'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterLK_POM: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_POM'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'POM'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
    object QrMasterACC: TBooleanField
      FieldName = 'ACC'
    end
    object QrMasterPOM: TBooleanField
      FieldName = 'POM'
    end
    object QrMasterASS: TBooleanField
      FieldName = 'ASS'
    end
    object QrMasterLK_ACTION_TYPE: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_ACTION_TYPE'
      LookupDataSet = QrSysFunc
      LookupKeyFields = 'FUNC_CODE'
      LookupResultField = 'ACTION_TYPE'
      KeyFields = 'FUNC_CODE'
      Lookup = True
    end
  end
  object DsMaster: TDataSource
    DataSet = QrMaster
    Left = 387
    Top = 99
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 120
    Top = 200
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDelete: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDeleteExecute
    end
    object CmdVisibleFields: TAction
      Tag = 10
      Category = 'DETAIL'
      Caption = 'Visible Fields'
    end
    object CmdSearchM: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
    end
    object CmdSearchD: TAction
      Category = 'DETAIL'
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
    end
    object CmdQuit: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdReload: TAction
      Caption = #272#7885'c l'#7841'i t'#7915' database'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdFilteredFields: TAction
      Tag = 11
      Category = 'DETAIL'
      Caption = 'Filter Fields'
    end
    object CmdNative: TAction
      Tag = 12
      Category = 'DETAIL'
      Caption = 'Native Description'
    end
    object Cmd2nd: TAction
      Tag = 13
      Category = 'DETAIL'
      Caption = '2nd Description'
    end
    object CmdFormatedFields: TAction
      Tag = 14
      Category = 'DETAIL'
      Caption = 'Formated Fields'
    end
    object CmdClearAll: TAction
      Category = 'DETAIL'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearAllExecute
    end
    object CmdSpecialFilter: TAction
      Category = 'DETAIL'
      Caption = 'L'#7885'c '#273#7863'c bi'#7879't'
    end
    object CmdSwitch: TAction
      Hint = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm Section'
      OnExecute = CmdNewExecute
    end
  end
  object Filter: TwwFilterDialog2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'FIELD'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'FIELD'
      'COMMENT'
      'DESCRIPTION'
      'DESCRIPTION1'
      'FILTER'
      'FORMAT')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 64
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 456
    Top = 243
  end
  object QrSysFunc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from SYS_FUNC'
      'order by Sort')
    Left = 291
    Top = 303
  end
  object DsSysFunc: TDataSource
    DataSet = QrSysFunc
    Left = 291
    Top = 331
  end
  object QrIconColor: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '#39'solid'#39' as Value'
      'union '
      'select '#39'regular'#39' as Value'
      'union '
      'select '#39'light'#39' as Value'
      'union '
      'select '#39'duotone'#39' as Value'
      'union '
      'select '#39'thin'#39' as Value'
      'union '
      'select '#39'brands'#39' as Value')
    Left = 427
    Top = 303
  end
  object DsIconColor: TDataSource
    DataSet = QrIconColor
    Left = 427
    Top = 331
  end
end
