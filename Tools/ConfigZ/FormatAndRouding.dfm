object FrmFormatAndRouding: TFrmFormatAndRouding
  Left = 179
  Top = 201
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Lo'#7841'i Ch'#7913'ng T'#7915', '#272#7883'nh D'#7841'ng V'#224' L'#224'm Tr'#242'n - LCT, Format And Rouding '
  ClientHeight = 356
  ClientWidth = 752
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = TntFormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 752
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 333
    Width = 752
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    ParentFont = False
    SimplePanel = True
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object GrMaster: TDBGridEh
    Left = 0
    Top = 36
    Width = 752
    Height = 297
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataGrouping.Active = True
    DataGrouping.FootersDefValues.ShowFunctionName = True
    DataGrouping.FootersDefValues.RunTimeCustomizable = True
    DataSource = DsList
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterRowCount = 1
    GridLineParams.GridBoundaries = True
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = 13360356
    IndicatorTitle.UseGlobalMenu = False
    OddRowColor = 16119285
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    STFilter.InstantApply = True
    STFilter.Local = True
    SumList.Active = True
    TabOrder = 2
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    OnGetCellParams = GrMasterGetCellParams
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'LCT'
        Footers = <>
        Width = 100
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'TABLENAME'
        Footers = <>
        Width = 150
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'PREFIX'
        Footers = <>
        Width = 80
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FMT_CUR'
        Footers = <>
        Title.Caption = 'FORMAT|CUR'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FMT_PRICE'
        Footers = <>
        Title.Caption = 'FORMAT|PRICE'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FMT_QTY'
        Footers = <>
        Title.Caption = 'FORMAT|QTY'
        Width = 130
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FMT_OTHER'
        Footers = <>
        Title.Caption = 'FORMAT|OTHER'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_FMT_CUR'
        Footers = <>
        Title.Caption = 'FORMAT|RP_CUR'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_FMT_PRICE'
        Footers = <>
        Title.Caption = 'FORMAT|RP_PRICE'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_FMT_QTY'
        Footers = <>
        Title.Caption = 'FORMAT|RP_QTY'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_FMT_OTHER'
        Footers = <>
        Title.Caption = 'FORMAT|RP_OTHER'
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ROU_CUR'
        Footers = <>
        Title.Caption = 'ROUDING|CUR'
        Width = 50
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ROU_CUR_NUM'
        Footers = <>
        Title.Caption = 'ROUDING|CUR_NUM'
        Width = 60
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ROU_PRICE'
        Footers = <>
        Title.Caption = 'ROUDING|PRICE'
        Width = 50
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ROU_QTY'
        Footers = <>
        Title.Caption = 'ROUDING|QTY'
        Width = 50
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ROU_OTHER'
        Footers = <>
        Title.Caption = 'ROUDING|OTHER'
        Width = 50
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_ROU_CUR'
        Footers = <>
        Title.Caption = 'ROUDING|RP_CUR'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_ROU_PRICE'
        Footers = <>
        Title.Caption = 'ROUDING|RP_PRICE'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_ROU_QTY'
        Footers = <>
        Title.Caption = 'ROUDING|RP_QTY'
        Width = 55
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'RP_ROU_OTHER'
        Footers = <>
        Title.Caption = 'ROUDING|RP_OTHER'
        Width = 55
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 216
    Top = 166
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrListBeforePost
    BeforeDelete = QrListBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'SYS_LCT')
    Left = 28
    Top = 88
    object QrListLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrListPREFIX: TWideStringField
      FieldName = 'PREFIX'
      Size = 200
    end
    object QrListTABLENAME: TWideStringField
      FieldName = 'TABLENAME'
      Size = 200
    end
    object QrListFMT_QTY: TWideStringField
      FieldName = 'FMT_QTY'
      Size = 50
    end
    object QrListFMT_PRICE: TWideStringField
      FieldName = 'FMT_PRICE'
      Size = 50
    end
    object QrListFMT_CUR: TWideStringField
      FieldName = 'FMT_CUR'
      Size = 50
    end
    object QrListFMT_OTHER: TWideStringField
      FieldName = 'FMT_OTHER'
      Size = 50
    end
    object QrListROU_QTY: TIntegerField
      FieldName = 'ROU_QTY'
    end
    object QrListROU_PRICE: TIntegerField
      FieldName = 'ROU_PRICE'
    end
    object QrListROU_CUR: TIntegerField
      FieldName = 'ROU_CUR'
    end
    object QrListROU_CUR_NUM: TIntegerField
      FieldName = 'ROU_CUR_NUM'
    end
    object QrListROU_OTHER: TIntegerField
      FieldName = 'ROU_OTHER'
    end
    object QrListRP_FMT_QTY: TIntegerField
      FieldName = 'RP_FMT_QTY'
    end
    object QrListRP_FMT_PRICE: TIntegerField
      FieldName = 'RP_FMT_PRICE'
    end
    object QrListRP_FMT_CUR: TIntegerField
      FieldName = 'RP_FMT_CUR'
    end
    object QrListRP_FMT_OTHER: TIntegerField
      FieldName = 'RP_FMT_OTHER'
    end
    object QrListRP_ROU_QTY: TIntegerField
      FieldName = 'RP_ROU_QTY'
    end
    object QrListRP_ROU_PRICE: TIntegerField
      FieldName = 'RP_ROU_PRICE'
    end
    object QrListRP_ROU_CUR: TIntegerField
      FieldName = 'RP_ROU_CUR'
    end
    object QrListRP_ROU_OTHER: TIntegerField
      FieldName = 'RP_ROU_OTHER'
    end
    object QrListDGIAI: TWideStringField
      FieldName = 'DGIAI'
      Size = 200
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 28
    Top = 124
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 248
    Top = 166
  end
end
