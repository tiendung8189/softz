(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HotroX;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus,
  AppEvnts, Wwdbgrid, Wwdbigrd, AdvMenus, wwfltdlg, wwFltDlg2,
  Wwdbcomb, wwDialog, StdCtrls, Mask, wwdbedit, Wwdotdot, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmHotroX = class(TForm)
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    N1: TMenuItem;
    Filter1: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    CbType: TwwDBComboBox;
    QrDanhmucMANHOM_HOTRO: TWideStringField;
    QrDanhmucTENNHOM_HOTRO: TWideStringField;
    QrDanhmucVISIBLE: TBooleanField;
    QrDanhmucSYS: TIntegerField;
    QrDanhmucSORT: TIntegerField;
    QrDanhmucGHICHU: TWideStringField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucSZ: TBooleanField;
    QrDanhmucHR: TBooleanField;
    QrDanhmucOTHER: TBooleanField;
    CmdChitiet: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    QrDanhmucFB: TBooleanField;
    QrDanhmucACC: TBooleanField;
    QrDanhmucPOM: TBooleanField;
    QrDanhmucASS: TBooleanField;
    GrMaster: TDBGridEh;
    QrType: TADOQuery;
    DsType: TDataSource;
    QrTypeMA_HOTRO: TIntegerField;
    QrTypeTEN_HOTRO: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdChitietExecute(Sender: TObject);
    procedure GrMasterGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
  public
  	procedure Execute;
  end;

var
  FrmHotroX: TFrmHotroX;

implementation

uses
	isDb, isMsg, ExCommon, isLib, DmHotro, DmHotro_HR;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.Execute;
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    Caption := Caption + exGetConnInfo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.FormShow(Sender: TObject);
var
    i: Integer;
    sName: string;
begin
    OpenDataSets([QrType, QrDanhmuc]);
    GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;
	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdChitietExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(QrDanhmuc.FieldByName('MANHOM_HOTRO').AsString);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter1 do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdFilterExecute(Sender: TObject);
begin
	Filter1.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with (QrDanhmuc) do
    begin
        if BlankConfirm(QrDanhmuc, ['MANHOM_HOTRO']) then
            Abort;
        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MANHOM_HOTRO') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

procedure TFrmHotroX.GrMasterGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
    if (Column.FieldName = 'MANHOM_HOTRO') then
    begin
        AFont.Style := [fsBold];
        AFont.Color := clPurple
    end;
end;

procedure TFrmHotroX.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and (not bEmpty);

    CmdChitiet.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHotroX.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

end.
