﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit NewString;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls;

type
  TFrmNewString = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Ed1: TEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
  public
  	function GetString: String;
  end;

var
  FrmNewString: TFrmNewString;

implementation

uses
	isLib;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNewString.FormKeyPress(Sender: TObject; var Key: Char);
begin
	case Key of
    #13:
        try
	    	ModalResult := mrOk;
        except
        end;
    #27:
    	ModalResult := mrCancel;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNewString.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmNewString.GetString : String;
begin
	Ed1.Text := '';
	if ShowModal = mrOK then
    	Result := Ed1.Text
    else
    	Result := '';
end;


end.
