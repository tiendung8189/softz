program ConfigZ;

uses
  Forms,
  MainData in 'MainData.pas' {DataMain: TDataModule},
  Main in 'Main.pas' {FrmMain},
  ExCommon in 'ExCommon.pas',
  CustomizeGrid in 'CustomizeGrid.pas' {FrmCustomizeGrid},
  Dictionary in 'Dictionary.pas' {FrmDictionary},
  DbeSetup in 'DbeSetup.pas' {FrmDbeSetup},
  FlexEdit in 'FlexEdit.pas' {FrmFlexEdit},
  OtherX in 'OtherX.pas' {FrmOtherX},
  RetailReceipt in 'RetailReceipt.pas' {FrmRetailReceipt},
  ExtractBlob in 'ExtractBlob.pas' {FrmExtractBlob},
  PrinterFont in 'PrinterFont.pas' {FrmPrinterFont},
  Dfm in 'Dfm.pas' {FrmDfm},
  GridOption in 'GridOption.pas' {FrmGridOption},
  DBGridCorrection in 'DBGridCorrection.pas' {FrmDBGridCorrection},
  EximOption in 'EximOption.pas' {FrmEximOption},
  RepEncode in 'RepEncode.pas' {FrmRepEncode},
  HotroX in 'HotroX.pas' {FrmHotroX},
  RepConfig in 'RepConfig.pas' {FrmRepConfig},
  FuncConfig in 'FuncConfig.pas' {FrmFuncConfig},
  FormatAndRouding in 'FormatAndRouding.pas' {FrmFormatAndRouding},
  FindReplace in 'FindReplace.pas' {FrmReplaceDialog},
  DmHotro in 'DmHotro.pas' {FrmDmHotro},
  DmKho2 in 'DmKho2.pas' {FrmDmKho2},
  DocumentFile in 'DocumentFile.pas' {FrmDocumentFile},
  ExecuteSQL in 'ExecuteSQL.pas' {FrmExecuteSQL},
  Job in 'Job.pas' {FrmJob},
  FlexConfig in 'FlexConfig.pas' {FrmFlexConfig},
  NewString in 'NewString.pas' {FrmNewString},
  VersionUpdate in 'VersionUpdate.pas' {FrmVersionUpdate},
  DmHotro_HR in 'DmHotro_HR.pas' {FrmDmHotro_HR},
  Resource in 'Resource.pas' {FrmResource},
  ResourceForm in 'ResourceForm.pas' {FrmResourceForm},
  SysMenu in 'SysMenu.pas' {FrmSysMenu};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Config Panel';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TFrmReplaceDialog, FrmReplaceDialog);
  Application.CreateForm(TFrmExecuteSQL, FrmExecuteSQL);
  Application.CreateForm(TFrmNewString, FrmNewString);
  if not DataMain.Logon then
  begin
      Application.Terminate;
      Exit;
  end;
  Application.Run;
end.
