(*==============================================================================
**------------------------------------------------------------------------------
*)
unit OtherX;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus,
  AppEvnts, Wwdbgrid, Wwdbigrd, AdvMenus, wwfltdlg, wwFltDlg2,
  Wwdbcomb, wwDialog, StdCtrls, Mask, wwdbedit, Wwdotdot, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmOtherX = class(TForm)
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    N1: TMenuItem;
    Filter1: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    CbType: TwwDBComboBox;
    QrType: TADOQuery;
    QrTypeMA_HOTRO: TIntegerField;
    QrTypeTEN_HOTRO: TWideStringField;
    DsType: TDataSource;
    GrMaster: TDBGridEh;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure GrMasterGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
  public
  	procedure Execute();
  end;

var
  FrmOtherX: TFrmOtherX;

implementation

uses
	isDb, isMsg, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.Execute;
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    Caption := Caption + exGetConnInfo;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.FormShow(Sender: TObject);
var
    i: Integer;
    sName: string;
begin
    OpenDataSets([QrType, QrDanhmuc]);

//    with QrDanhmuc do
//    begin
//	    Open;
//        for i := 0 to FieldCount - 1 do
//        begin
//            sName := Fields[i].DisplayName;
//        if ('CREATE_BY' = sName) or ('UPDATE_BY' = sName) or
//            ('DELETE_BY' = sName) or ('CREATE_DATE' = sName) or
//            ('UPDATE_DATE' = sName) or ('DELETE_DATE' = sName) then
//            Continue;
//            GrMaster.AddField(Fields[i].DisplayName, i);
//            Filter1.SelectedFields.Add(Fields[i].DisplayName);
//        end;
//        GrMaster.SetFocus;
//    end;
//
//    with GrMaster do
//        for i := 0 to GetColCount - 1 do
//            if Columns[i].DisplayWidth > 40 then
//                Columns[i].DisplayWidth := 40;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;
	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter1 do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdFilterExecute(Sender: TObject);
begin
	Filter1.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with (QrDanhmuc) do
    begin
        if BlankConfirm(QrDanhmuc, ['LOAI']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'LOAI') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.GrMasterGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
    if (Column.FieldName = 'LOAI') then
    begin
        AFont.Style := [fsBold];
        AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmOtherX.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

end.
