(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PrinterFont;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, ComCtrls;

type
  TFrmPrinterFont = class(TForm)
    Label1: TLabel;
    LbPrinters: TListBox;
    Label2: TLabel;
    LbFonts: TListBox;
    StatusBar: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure LbPrintersDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
  public
	{ Public declarations }
  end;

var
  FrmPrinterFont: TFrmPrinterFont;

implementation

{$R *.DFM}

uses
	Printers;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinterFont.FormShow(Sender: TObject);
begin
	with Printer do
	begin
		LbPrinters.Items.Text := Printers.Text;
		StatusBar.Panels[0].Text  := 'Selected: ' + Printers.Strings[PrinterIndex];
		LbFonts.Items.Text := Fonts.Text;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinterFont.LbPrintersDblClick(Sender: TObject);
begin
	with Printer do
	begin
		PrinterIndex := LbPrinters.ItemIndex;
		StatusBar.Panels[0].Text  := 'Selected: ' + Printers.Strings[PrinterIndex];
		LbFonts.Items.Text := Fonts.Text;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinterFont.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
		ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinterFont.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
