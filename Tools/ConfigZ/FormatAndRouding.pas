(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FormatAndRouding;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, ComCtrls, ActnList, Grids, Db,
  Wwdbgrid, ADODb, fcStatusBar, AppEvnts, ToolWin, Wwdbigrd, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh;

type
  TFrmFormatAndRouding = class(TForm)
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    QrList: TADOQuery;
    DsList: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    ToolButton9: TToolButton;
    Status: TfcStatusBar;
    QrListLCT: TWideStringField;
    QrListPREFIX: TWideStringField;
    QrListTABLENAME: TWideStringField;
    QrListFMT_QTY: TWideStringField;
    QrListFMT_PRICE: TWideStringField;
    QrListFMT_CUR: TWideStringField;
    QrListFMT_OTHER: TWideStringField;
    QrListROU_QTY: TIntegerField;
    QrListROU_PRICE: TIntegerField;
    QrListROU_CUR: TIntegerField;
    QrListROU_CUR_NUM: TIntegerField;
    QrListROU_OTHER: TIntegerField;
    QrListRP_FMT_QTY: TIntegerField;
    QrListRP_FMT_PRICE: TIntegerField;
    QrListRP_FMT_CUR: TIntegerField;
    QrListRP_FMT_OTHER: TIntegerField;
    QrListRP_ROU_QTY: TIntegerField;
    QrListRP_ROU_PRICE: TIntegerField;
    QrListRP_ROU_CUR: TIntegerField;
    QrListRP_ROU_OTHER: TIntegerField;
    QrListDGIAI: TWideStringField;
    GrMaster: TDBGridEh;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrListMemoOpen(Grid: TwwDBGrid; MemoDialog: TwwMemoDialog);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntFormCreate(Sender: TObject);
    procedure GrMasterGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
  public
  end;

var
  FrmFormatAndRouding: TFrmFormatAndRouding;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.TntFormCreate(Sender: TObject);
begin
    Caption := Caption + exGetConnInfo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

    SetDisplayFormat(QrList, 'SYS_LCT');
    QrList.Open;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrList, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.CmdNewExecute(Sender: TObject);
begin
	QrList.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.CmdSaveExecute(Sender: TObject);
begin
	QrList.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.CmdCancelExecute(Sender: TObject);
begin
	QrList.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.CmdDelExecute(Sender: TObject);
begin
	QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
	with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and (not bEmpty);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.QrListBeforePost(DataSet: TDataSet);
begin
	with (QrList) do
		if BlankConfirm(QrList, ['LCT']) then
    		Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

	(*
    ** Others
    *)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if CompareText(Field.FullName, 'LCT') = 0 then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.GrListMemoOpen(Grid: TwwDBGrid;
  MemoDialog: TwwMemoDialog);
begin
    with MemoDialog.Font do
    begin
        Name := 'Courier New';
        Size := 10;
        Color:= $00804000;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFormatAndRouding.GrMasterGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
    if CompareText(Column.FieldName, 'LCT') = 0 then
    begin
        AFont.Style := [fsBold];
        AFont.Color := clPurple
    end;
end;

end.
