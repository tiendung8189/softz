﻿unit DbeSetup;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ActnList,
  ComCtrls, fcStatusBar, ExtCtrls, Grids, wwDBGrid2, DB, ADODB, AppEvnts, Menus,
  AdvMenus, wwfltdlg,
  wwFltDlg2, wwDialog, ToolWin, Wwdbigrd, Wwdbgrid;

type
  TFrmDbeSetup = class(TForm)
    ActionList: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Status: TfcStatusBar;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Bevel2: TBevel;
    GrMsgs: TwwDBGrid2;
    GrObjs: TwwDBGrid2;
    QrMsgs: TADOQuery;
    DsMsgs: TDataSource;
    QrObjs: TADOQuery;
    DsObjs: TDataSource;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    QrMsgsERROR: TStringField;
    QrMsgsDESCRIPTION: TWideStringField;
    QrMsgsDESCRIPTION1: TWideStringField;
    QrObjsKEY: TStringField;
    QrObjsDESCRIPTION: TWideStringField;
    QrObjsDESCRIPTION1: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    TntPopupMenu1: TAdvPopupMenu;
    ToolButton4: TToolButton;
    OpenDlg: TOpenDialog;
    SaveDlg: TSaveDialog;
    ToolButton1: TToolButton;
    CmdGetObject: TAction;
    mmutin1: TMenuItem;
    Lcdliu1: TMenuItem;
    N1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    Filter: TwwFilterDialog2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrMsgsBeforePost(DataSet: TDataSet);
    procedure QrObjsBeforePost(DataSet: TDataSet);
    procedure QrMsgsPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure GrMsgsCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure GrObjsCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure OpenDlgShow(Sender: TObject);
    procedure CmdGetObjectExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
  private
    mQuery: TADOQuery;
    mDs: TDataSource;
    mF: string;
  public
    procedure Execute();
  end;

var
  FrmDbeSetup: TFrmDbeSetup;

implementation

uses
    MainData, isLib, isDb, ExCommon, isMsg, isCommon;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    with QrObjs do
    begin
        if not Active then
            Exit;
        CmdGetObject.Enabled := (State in [dsBrowse]) and
            (PgMain.ActivePageIndex = 1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := RecordCount(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.CmdGetObjectExecute(Sender: TObject);
var
    s: string;
begin
    s :=
        ' declare @S	varchar(50)' +
        ' declare tam cursor for' +
        ' select	name' +
        ' from	sysobjects a' +
        ' where	a.xtype = ''PK''' +
        ' order by name' +
        ' open tam' +
        ' fetch next from tam into @S' +
        ' while @@FETCH_STATUS = 0' +
        ' begin' +
	        ' if not exists (select 1 from SYS_DBE_OBJ where [KEY] = @S)' +
		    ' insert into SYS_DBE_OBJ ([KEY]) values (@S)' +
	        ' fetch next from tam into @S' +
        ' end' +
        ' close tam' +
        ' deallocate tam';

    Wait('Đang thực hiện...');
    DataMain.Conn.Execute(s);

    QrObjs.Close;
    QrObjs.Open;

    ClearWait;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)procedure TFrmDbeSetup.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsObjs)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.Execute;
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataset(mQuery, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.FormCreate(Sender: TObject);
begin
    Caption := Caption + exGetConnInfo;

    AddAllFields(QrMsgs, 'SYS_DBE_MSG', 0);
    AddAllFields(QrObjs, 'SYS_DBE_OBJ', 0);

    mQuery := QrMsgs;
    mDs := DsMsgs;
    mF:= 'ERROR';
    TMyForm(Self).Init2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.FormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMsgs.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.FormShow(Sender: TObject);
begin
    OpenDataSets([QrMsgs, QrObjs]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.GrMsgsCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'ERROR' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.GrObjsCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'KEY' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.OpenDlgShow(Sender: TObject);
begin
	OpenDlg.InitialDir := sysAppPath;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrMsgs;
            mDs := DsMsgs;
            mF := 'ERROR';
//            mFilter := FilterQuocgia;
            GrMsgs.SetFocus;
	    end;
    1:
	    begin
    	    mQuery := QrObjs;
            mDs := DsObjs;
            mF := 'KEY';
//            mFilter := FilterTinh;
            GrObjs.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.QrMsgsBeforePost(DataSet: TDataSet);
begin
    with QrMsgs do
    begin
        if BlankConfirm(QrMsgs, ['ERROR']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.QrMsgsPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDbeSetup.QrObjsBeforePost(DataSet: TDataSet);
begin
    with QrObjs do
    begin
        if BlankConfirm(QrObjs, ['KEY']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

end.
