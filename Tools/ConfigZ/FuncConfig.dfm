object FrmFuncConfig: TFrmFuncConfig
  Left = 138
  Top = 158
  Caption = 'Danh S'#225'ch Ch'#7913'c N'#259'ng'
  ClientHeight = 438
  ClientWidth = 1361
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object CbGroup: TwwDBComboBox
    Left = 180
    Top = 88
    Width = 121
    Height = 24
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    DropDownCount = 8
    ItemHeight = 0
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
  end
  object Status: TStatusBar
    Left = 0
    Top = 417
    Width = 1361
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TDBGridEh
    Left = 0
    Top = 0
    Width = 1361
    Height = 417
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataSource = DsFUNC
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    OddRowColor = 16119285
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    PopupMenu = PopFunc
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    SumList.Active = True
    TabOrder = 0
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            FieldName = 'VALUE'
            Width = 210
          end
          item
            FieldName = 'KEY'
            Width = 70
          end>
        DropDownBox.ListFieldNames = 'KEY;VALUE'
        DropDownBox.ListSource = DsSysFuncCat
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoSortMarkingEh, dlgMultiSortMarkingEh]
        DropDownWidth = 300
        DynProps = <>
        EditButtons = <>
        FieldName = 'CAT'
        Footers = <>
        LookupParams.KeyFieldNames = 'FUNC_CAT'
        LookupParams.LookupDataSet = QrSysFuncCat
        LookupParams.LookupDisplayFieldName = 'VALUE'
        LookupParams.LookupKeyFieldNames = 'KEY'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Action: TActionList
    Left = 312
    Top = 135
    object CmdClose: TAction
      Caption = 'Close'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdFilter: TAction
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdExFunction: TAction
      Caption = 'L'#432'u ra file'
      OnExecute = CmdExFunctionExecute
    end
    object CmdImFunction: TAction
      Caption = 'L'#7845'y t'#7915' file'
      OnExecute = CmdImFunctionExecute
    end
    object CmdShow: TAction
      Tag = -1
      Caption = 'T'#7845't c'#7843' b'#225'o c'#225'o'
      OnExecute = CmdShowExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm'
      OnExecute = CmdSearchExecute
    end
    object CmdReRead: TAction
      Caption = 'ReRead'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdEmpty: TAction
      Caption = 'X'#243'a to'#224'n b'#7897' ch'#7913'c n'#259'ng'
      OnExecute = CmdEmptyExecute
    end
  end
  object QrFunc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrFuncBeforePost
    AfterPost = QrFuncAfterPost
    BeforeDelete = QrFuncBeforeDelete
    AfterDelete = QrFuncAfterPost
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from SYS_FUNC'
      'order by [SORT]')
    Left = 160
    Top = 168
    object QrFuncCAT: TIntegerField
      FieldName = 'CAT'
      OnChange = QrFuncCATChange
    end
    object QrFuncFUNC_CAT: TWideStringField
      FieldName = 'FUNC_CAT'
      OnChange = QrFuncFUNC_CATChange
    end
  end
  object DsFUNC: TDataSource
    DataSet = QrFunc
    Left = 160
    Top = 196
  end
  object Filter: TwwFilterDialog
    DataSource = DsFUNC
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 256
    Top = 135
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '*.xlm'
    Filter = 'Comma Separated Value (*.xml)|*.xml'
    FilterIndex = 0
    Left = 368
    Top = 135
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.xml'
    Filter = 'Comma Separated Value (*.xml)|*.xml'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 396
    Top = 135
  end
  object PopFunc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Version = '2.6.6.0'
    Left = 284
    Top = 135
    object Import1: TMenuItem
      Action = CmdImFunction
      GroupIndex = 4
      ImageIndex = 15
    end
    object Export1: TMenuItem
      Action = CmdExFunction
      GroupIndex = 4
      ImageIndex = 16
    end
    object N4: TMenuItem
      Caption = '-'
      GroupIndex = 4
    end
    object Xatonbchcnng1: TMenuItem
      Action = CmdEmpty
      GroupIndex = 4
    end
    object N2: TMenuItem
      Caption = '-'
      GroupIndex = 4
    end
    object tcboco1: TMenuItem
      Tag = -1
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'T'#7845't c'#7843' ch'#7913'c n'#259'ng'
      Checked = True
      GroupIndex = 4
      RadioItem = True
      OnClick = CmdShowExecute
    end
    object N3: TMenuItem
      Caption = '-'
      GroupIndex = 4
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 340
    Top = 135
  end
  object SYNC_RIGHTS: TADOCommand
    CommandText = 'SYS_SYNC_RIGHTS;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 188
    Top = 168
  end
  object DsResource: TDataSource
    DataSet = QrResource
    Left = 291
    Top = 259
  end
  object QrResource: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from '#9'SYS_RESOURCE'
      'where ResourceCAT not in ('#39'OTHER'#39')'
      'order by  ResourceName')
    Left = 291
    Top = 231
    object QrResourceResourceType: TWideStringField
      FieldName = 'ResourceType'
      Size = 10
    end
    object QrResourceResourceName: TWideStringField
      FieldName = 'ResourceName'
      Size = 100
    end
    object QrResourceResourceValue: TWideStringField
      FieldName = 'ResourceValue'
      Size = 250
    end
    object QrResourceResourceValue_EN: TWideStringField
      FieldName = 'ResourceValue_EN'
      Size = 250
    end
  end
  object QrSysFuncCat: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrSysFuncCatBeforeOpen
    Parameters = <
      item
        Name = 'Item'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select (ROW_NUMBER() OVER (ORDER BY (SELECT 1)) - 1) AS ID, *'
      
        '         from fnS_Table_By_Comma ((select Value from SYS_FLEXCON' +
        'FIG where Section = '#39'Admin'#39' and Item = :Item), '#39'='#39')')
    Left = 456
    Top = 200
  end
  object DsSysFuncCat: TDataSource
    DataSet = QrSysFuncCat
    Left = 456
    Top = 228
  end
end
