﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmHotro;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids, StdCtrls, Db,
  Wwdbgrid2, ADODb, wwdblook,
  AppEvnts, Menus, AdvMenus, DBCtrls, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmHotro = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    QrDmkhac: TADOQuery;
    DsDmkhac: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdAdmin: TAction;
    QrDmkhacMANHOM_HOTRO: TWideStringField;
    QrDmkhacMA_HOTRO: TWideStringField;
    QrDmkhacTEN_HOTRO: TWideStringField;
    QrDmkhacCREATE_BY: TIntegerField;
    QrDmkhacUPDATE_BY: TIntegerField;
    QrDmkhacCREATE_DATE: TDateTimeField;
    QrDmkhacUPDATE_DATE: TDateTimeField;
    QrDmkhacROWGUID: TGuidField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDmkhacBeforePost(DataSet: TDataSet);
    procedure QrDmkhacBeforeDelete(DataSet: TDataSet);
    procedure QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbKhacNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrDmkhacAfterInsert(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    mLoai: String;
  public
  	procedure Execute(loai: String);
  end;

var
  FrmDmHotro: TFrmDmHotro;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.Execute (loai: String);
begin
    mLoai := loai;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDmkhac, 'DM_HOTRO', 0);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdNewExecute(Sender: TObject);
begin
	QrDmKhac.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdSaveExecute(Sender: TObject);
begin
	QrDmKhac.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdCancelExecute(Sender: TObject);
begin
	QrDmKhac.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdDelExecute(Sender: TObject);
begin
	QrDmKhac.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDmKhac]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormShow(Sender: TObject);
var
    i: Integer;
begin
	with QrDmkhac do
    begin
        Close;

        Parameters[0].Value := mLoai;
        Open;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmKhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacAfterInsert(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
        FieldByName('MANHOM_HOTRO').AsString := mLoai;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacBeforePost(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
		if BlankConfirm(QrDmKhac, ['MA_HOTRO', 'TEN_HOTRO']) then
    		Abort;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDmkhac);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MA_HOTRO') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDMKHAC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CbKhacNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

end.
