﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExtractBlob;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  ActnList, StdCtrls, Buttons, ExtCtrls, Db, DBCtrls, ADODB;

type
  TFrmExtractBlob = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdTable: TEdit;
    Label2: TLabel;
    EdField: TEdit;
    btContinue: TBitBtn;
    btCancel: TBitBtn;
    ActionList1: TActionList;
    CmdProc: TAction;
    Label3: TLabel;
    EdCriteria: TEdit;
    CmdLoad: TAction;
    EdBlob: TDBMemo;
    QrBlob: TADOQuery;
    DsBlob: TDataSource;
    QrBlobVALUE: TWideMemoField;
    DBNavigator1: TDBNavigator;
    BitBtn1: TBitBtn;
    CmdClose: TAction;
    procedure CmdProcExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdLoadExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
  private
  public
  end;

var
  FrmExtractBlob: TFrmExtractBlob;

implementation

uses
	isLib, MainData, isMsg, ExCommon, isDb;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.CmdLoadExecute(Sender: TObject);
var
	s1, s2, s3: String;
begin
	s1 := EdTable.Text;
    s2 := EdField.Text;
    s3 := Trim(EdCriteria.Text);
    if s3 = '' then
    	s3 := '1=1';

	with QrBlob do
    begin
    	SQL.Text := Format('select %s as "VALUE" from %s where %s', [s2, s1, s3]);
        try
	        Open;
        except
        	ErrMsg(DataMain.Conn.Errors[0].Description);
            Close;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.CmdProcExecute(Sender: TObject);
begin
    QrBlob.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    with QrBlob do
    begin
        if not Active then
            Exit;

        if (UpdateStatus = usUnmodified) and (State in [dsBrowse]) then
            Exit;

//        case YesNoCancel('Lưu các thay đổi, tiếp tục?', 'Thông báo') of
//            6: UpdateBatch;
//            7: CancelBatch;
//            2: CanClose := False;
//        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.FormCreate(Sender: TObject);
begin
    Caption := Caption + exGetConnInfo;
    TMyForm(Self).Init2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExtractBlob.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
    with QrBlob do
    begin
        if not Active then
        begin
            CmdProc.Enabled := False;
            Exit;
        end;
        CmdProc.Enabled := (UpdateStatus <> usUnmodified) or not (State in [dsBrowse])
    end;
	CmdLoad.Enabled := (EdTable.Text <> '') and (EdField.Text <> '');
end;

end.
