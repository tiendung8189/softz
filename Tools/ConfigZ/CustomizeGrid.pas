﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CustomizeGrid;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, StdCtrls, ComCtrls, ActnList, Db, ADODB, Menus,
  isPanel, Grids, Wwdbgrid, wwDBGrid2, AppEvnts,
  wwfltdlg, wwFltDlg2, DBCtrls, fcStatusBar,
  AdvMenus, wwDialog, Wwdbigrd, ToolWin, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmCustomizeGrid = class(TForm)
    Splitter1: TSplitter;
    ActionList: TActionList;
    CmdSave: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    CmdReload: TAction;
    PaDetail: TisPanel;
    PopLeft: TAdvPopupMenu;
    QrCustom: TADOQuery;
    DsGridCustom: TDataSource;
    ToolButton3: TToolButton;
    ToolButton6: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ItemAll: TMenuItem;
    ItemDisable: TMenuItem;
    ItemEnable: TMenuItem;
    QrCustomNAME: TWideStringField;
    QrCustomFIXCOLS: TIntegerField;
    QrCustomENABLE: TBooleanField;
    CmdSearch: TAction;
    N2: TMenuItem;
    Search1: TMenuItem;
    PgDetail: TPageControl;
    TntTabSheet1: TTabSheet;
    TntTabSheet2: TTabSheet;
    Ed2ndDetail: TDBMemo;
    QrCustomCOMMENT: TWideMemoField;
    vlPanel1: TisPanel;
    EdComment: TDBMemo;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    QrCustomOPTIONS: TIntegerField;
    QrCustomTITLE_BUTTON: TBooleanField;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    Filter1: TMenuItem;
    CmdClearFilter: TAction;
    ClearFilter1: TMenuItem;
    EdNativeDetails: TDBMemo;
    QrCustomSELECTED: TWideMemoField;
    QrCustomSELECTED1: TWideMemoField;
    CmdClearList: TAction;
    N1: TMenuItem;
    ClearFiltered1: TMenuItem;
    Status: TfcStatusBar;
    Bevel1: TBevel;
    spl1: TSplitter;
    DBGridEh1: TDBGridEh;
    GrList: TDBGridEh;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrCustomBeforeDelete(DataSet: TDataSet);
    procedure QrCustomAfterScroll(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure ItemEnableClick(Sender: TObject);
    procedure PopLeftPopup(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure GrListDblClick(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure EdNativeDetailsEnter(Sender: TObject);
    procedure CmdClearListExecute(Sender: TObject);
    procedure QrCustomBeforePost(DataSet: TDataSet);
    procedure GrListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure GrListCellClick(Column: TColumnEh);

   
    
  private
  	mShow: Integer;
	mTrigger: Boolean;
    fieldName: string;
  public
  	procedure Execute;
  end;

var
  FrmCustomizeGrid: TFrmCustomizeGrid;

implementation

uses
	isLib, isMsg, MainData, Excommon, isDb, GridOption, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.Execute;
begin
	mShow := 0;
	ShowModal;
end;

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;

    AddAllFields(QrCustom, 'SYS_GRID', 0);

    mTrigger := False;

    //ResetRowColor([GrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.FormShow(Sender: TObject);
begin
    QrCustom.Open;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrList.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrCustom, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
	Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.QrCustomBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.QrCustomBeforePost(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.QrCustomAfterScroll(DataSet: TDataSet);
begin
    PaDetail.HeaderCaption := ' :: ' + QrCustom.FieldByName('NAME').AsString;
end;

    (*
    **  Grid + AppEvent
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if Field.FullName = 'NAME' then
    begin
    	AFont.Size := 10;
		AFont.Style := [];
        AFont.Color := clBlack;
    end;

    if not QrCustom.FieldByName('ENABLE').AsBoolean then
        AFont.Color := clGray;
end;


procedure TFrmCustomizeGrid.GrListCellClick(Column: TColumnEh);
begin
    fieldName := Column.FieldName;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.ApplicationEvents1Hint(Sender: TObject);
begin
    Status.Panels[1].Text := GetLongHint(Application.Hint);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.GrListDblClick(Sender: TObject);
var
    n: DWORD;
begin
	if SameText(fieldName, 'COMMENT') then
    	Exit;

    Application.CreateForm(TFrmGridOption, FrmGridOption);
    n := QrCustom.FieldByName('OPTIONS').AsInteger;
    if FrmGridOption.Execute(n) then
    begin
        SetEditState(QrCustom);
        QrCustom.FieldByName('OPTIONS').AsInteger := n;
    end;
end;

(*
========================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.GrListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
    if SameText(Column.FieldName, 'NAME') then
    begin
    	AFont.Size := 10;
		AFont.Style := [];
        AFont.Color := clBlack;
    end;

    if not QrCustom.FieldByName('ENABLE').AsBoolean then
        AFont.Color := clGray;
end;

(*
========================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    //Status.Panels[0].Text := RecordCount(QrCustom);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    b, b1: Boolean;
begin
    with QrCustom do
    begin
        if not Active then
        	Exit;

        b  := not (State in [dsBrowse]);
        b1 := not IsEmpty;
    end;

    CmdSave.Enabled := b;
    CmdCancel.Enabled := b;
    CmdDel.Enabled := b1;
    CmdSearch.Enabled := not b;
    CmdFilter.Enabled := not b;
    CmdClearFilter.Enabled := not b and (Filter.FieldInfo.Count> 0 );
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrList then
        case PgDetail.ActivePageIndex of
        0:
            EdNativeDetails.SetFocus;
        1:
            Ed2ndDetail.SetFocus;
        end
    else if (ActiveControl = PgDetail) or (ActiveControl.Parent = PgDetail) then
        EdComment.SetFocus
    else
        GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdDelExecute(Sender: TObject);
begin
    QrCustom.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdSaveExecute(Sender: TObject);
begin
    QrCustom.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdCancelExecute(Sender: TObject);
begin
    QrCustom.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdReloadExecute(Sender: TObject);
begin
	QrCustom.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsGridCustom);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.CmdClearListExecute(Sender: TObject);
begin
	if not YesNo('Xóa toàn bộ cấu hình lưới. Tiếp tục?', 1) then
    	Exit;

    mTrigger := True;
    EmptyDataSet(QrCustom);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.ItemEnableClick(Sender: TObject);
begin
	mShow := (Sender as TMenuItem).Tag;
    with QrCustom do
    begin
        case mShow of
        1:
        	begin
	            Filter := 'ENABLE = 1';
//                ItemEnable.Checked := True;
            end;
        2:
        	begin
	            Filter := 'ENABLE = 0';
//                ItemDisable.Checked := True;
            end;
        else
        	begin
	            Filter := '';
//                ItemAll.Checked := True;
            end;
        end;
        First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.PopLeftPopup(Sender: TObject);
begin
	ItemEnable.Checked := mShow = 1;
	ItemDisable.Checked := mShow = 2;
	ItemAll.Checked := mShow = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCustomizeGrid.EdNativeDetailsEnter(Sender: TObject);
var
    fInfo: TwwFieldInfo;
    i, n: Integer;
    s: String;
    EdText: TDBMemo;
begin
    case PgDetail.ActivePageIndex of
    0:
        begin
            s := 'SELECTED';
            EdText := EdNativeDetails
        end;
    1:
        begin
            s := 'SELECTED1';
            EdText := Ed2ndDetail;
        end;
    end;
    s := QrCustom.FieldByName(s).DisplayLabel;

    with Filter do
    begin
        n := FieldInfo.Count - 1;
        if n < 0 then
            Exit;

        for i:= 0 to n do
        begin
            fInfo:= TwwFieldInfo(FieldInfo[i]);
            if CompareText(fInfo.DisplayLabel, s) = 0 then
                Break
            else
                fInfo := Nil;
        end;
    end;

    if not Assigned(fInfo) then
        Exit;

    s := fInfo.FilterValue;
    with EdText do
    begin
        SelStart := Pos(UpperCase(s), UpperCase(Text)) - 1;
        SelLength := Length(s);
    end
end;

end.
