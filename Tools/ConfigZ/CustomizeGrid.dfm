object FrmCustomizeGrid: TFrmCustomizeGrid
  Left = 120
  Top = 61
  Caption = 'C'#7845'u H'#236'nh L'#432#7899'i wwDBGrid - Customize wwDBGrids'
  ClientHeight = 573
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 722
    Top = 36
    Width = 2
    Height = 514
    OnMoved = TntFormResize
    ExplicitLeft = 400
    ExplicitTop = 37
    ExplicitHeight = 517
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1016
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton3: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 54
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 108
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PaDetail: TisPanel
    Left = 724
    Top = 36
    Width = 292
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    HeaderCaption = ' :: '
    HeaderColor = 8404992
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWhite
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object Bevel1: TBevel
      Left = 0
      Top = 16
      Width = 292
      Height = 2
      Align = alTop
      Shape = bsSpacer
      ExplicitLeft = 92
      ExplicitTop = 32
      ExplicitWidth = 50
    end
    object spl1: TSplitter
      Left = 0
      Top = 367
      Width = 292
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 18
      ExplicitWidth = 352
    end
    object PgDetail: TPageControl
      Left = 0
      Top = 18
      Width = 292
      Height = 349
      Cursor = 1
      ActivePage = TntTabSheet1
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      HotTrack = True
      ParentFont = False
      TabOrder = 1
      object TntTabSheet1: TTabSheet
        Cursor = 1
        Caption = 'Vietnam'
        object EdNativeDetails: TDBMemo
          Left = 0
          Top = 0
          Width = 284
          Height = 321
          Align = alClient
          DataField = 'SELECTED'
          DataSource = DsGridCustom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
      object TntTabSheet2: TTabSheet
        Cursor = 1
        Caption = 'English'
        object Ed2ndDetail: TDBMemo
          Left = 0
          Top = 0
          Width = 284
          Height = 321
          Align = alClient
          DataField = 'SELECTED1'
          DataSource = DsGridCustom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
          OnEnter = EdNativeDetailsEnter
        end
      end
    end
    object vlPanel1: TisPanel
      Left = 0
      Top = 370
      Width = 292
      Height = 144
      Align = alBottom
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 2
      HeaderCaption = ' :: Ghi ch'#250
      HeaderColor = 7617536
      ImageSet = 4
      RealHeight = 0
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object EdComment: TDBMemo
        Left = 0
        Top = 16
        Width = 292
        Height = 128
        Align = alClient
        DataField = 'COMMENT'
        DataSource = DsGridCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 550
    Width = 1016
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object DBGridEh1: TDBGridEh
    Left = 178
    Top = 104
    Width = 465
    Height = 185
    DataSource = DsGridCustom
    DynProps = <>
    PopupMenu = PopLeft
    STFilter.Local = True
    TabOrder = 3
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Width = 326
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ENABLE'
        Footers = <>
        Width = 61
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'TITLE_BUTTON'
        Footers = <>
        Title.Caption = 'Title Btn.'
        Width = 77
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FIXCOLS'
        Footers = <>
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'COMMENT'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GrList: TDBGridEh
    Left = 0
    Top = 36
    Width = 722
    Height = 514
    ParentCustomHint = False
    Align = alLeft
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataGrouping.Active = True
    DataGrouping.FootersDefValues.ShowFunctionName = True
    DataGrouping.FootersDefValues.RunTimeCustomizable = True
    DataSource = DsGridCustom
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterRowCount = 1
    GridLineParams.GridBoundaries = True
    IndicatorTitle.UseGlobalMenu = False
    OddRowColor = clWindow
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    PopupMenu = PopLeft
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    ShowHint = True
    SortLocal = True
    STFilter.InstantApply = True
    STFilter.Local = True
    SumList.Active = True
    TabOrder = 4
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    OnCellClick = GrListCellClick
    OnDblClick = GrListDblClick
    OnGetCellParams = GrListGetCellParams
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'NAME'
        Footer.DisplayFormat = '#,##0 m'#7851'u tin'
        Footer.FieldName = 'NAME'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = [fsBold]
        Footer.ValueType = fvtCount
        Footers = <>
        Width = 311
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ENABLE'
        Footers = <>
        STFilter.Visible = False
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'TITLE_BUTTON'
        Footers = <>
        STFilter.Visible = False
        Title.Caption = 'Title Btn.'
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'FIXCOLS'
        Footers = <>
        STFilter.Visible = False
        Title.Caption = 'Fixed Cols'
        Width = 39
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'COMMENT'
        Footers = <>
        Width = 255
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 184
    Top = 200
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdReload: TAction
      Caption = #272#7885'c l'#7841'i t'#7915' database'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdSwitch: TAction
      Hint = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdClearList: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a t'#7845't c'#7843
      OnExecute = CmdClearListExecute
    end
  end
  object PopLeft: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopLeftPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 140
    Top = 200
    object Search1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Filter1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object ClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItemEnable: TMenuItem
      Tag = 1
      Caption = 'Hi'#7879'u l'#7921'c'
      GroupIndex = 1
      RadioItem = True
      OnClick = ItemEnableClick
    end
    object ItemDisable: TMenuItem
      Tag = 2
      Caption = 'Kh'#244'ng hi'#7879'u l'#7921'c'
      GroupIndex = 1
      RadioItem = True
      OnClick = ItemEnableClick
    end
    object ItemAll: TMenuItem
      Caption = 'T'#7845't c'#7843
      GroupIndex = 1
      RadioItem = True
      OnClick = ItemEnableClick
    end
    object N1: TMenuItem
      Caption = '-'
      GroupIndex = 1
    end
    object ClearFiltered1: TMenuItem
      Action = CmdClearList
      Caption = 'X'#243'a to'#224'n b'#7897
      GroupIndex = 1
    end
  end
  object QrCustom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrCustomBeforePost
    BeforeDelete = QrCustomBeforeDelete
    AfterScroll = QrCustomAfterScroll
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from'#9'SYS_GRID')
    Left = 134
    Top = 81
    object QrCustomNAME: TWideStringField
      DisplayLabel = 'T'#234'n l'#432#7899'i'
      FieldName = 'NAME'
      Size = 50
    end
    object QrCustomENABLE: TBooleanField
      DisplayLabel = 'Hi'#7879'u l'#7921'c'
      FieldName = 'ENABLE'
      Visible = False
    end
    object QrCustomTITLE_BUTTON: TBooleanField
      DisplayLabel = 'Title button'
      FieldName = 'TITLE_BUTTON'
      Visible = False
    end
    object QrCustomFIXCOLS: TIntegerField
      DisplayLabel = 'Fixed cols'
      FieldName = 'FIXCOLS'
      Visible = False
    end
    object QrCustomCOMMENT: TWideMemoField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'COMMENT'
      BlobType = ftWideMemo
    end
    object QrCustomOPTIONS: TIntegerField
      DisplayLabel = 'Options'
      FieldName = 'OPTIONS'
      Visible = False
    end
    object QrCustomSELECTED: TWideMemoField
      DisplayLabel = 'N'#7897'i dung'
      FieldName = 'SELECTED'
      BlobType = ftWideMemo
    end
    object QrCustomSELECTED1: TWideMemoField
      DisplayLabel = 'N'#7897'i dung * English'
      FieldName = 'SELECTED1'
      BlobType = ftWideMemo
    end
  end
  object DsGridCustom: TDataSource
    DataSet = QrCustom
    Left = 142
    Top = 129
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 224
    Top = 200
  end
  object Filter: TwwFilterDialog2
    DataSource = DsGridCustom
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'FIELD'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NAME'
      'ENABLE'
      'TITLE_BUTTON'
      'FIXCOLS'
      'SELECTED1'
      'SELECTED1'
      'COMMENT')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 100
    Top = 200
  end
end
