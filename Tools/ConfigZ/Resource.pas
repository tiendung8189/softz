(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Resource;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid2, Db, ADODB, ActnList,
  Menus, ComCtrls, wwfltdlg, Wwdbgrid, wwFltDlg2, AppEvnts,
  isPanel, DBCtrls, fcStatusBar, AdvMenus, wwDialog, StdCtrls, ToolWin,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmResource = class(TForm)
    Splitter1: TSplitter;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    ActionList: TActionList;
    CmdSearchD: TAction;
    CmdQuit: TAction;
    Filter: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdDelete: TAction;
    CmdReload: TAction;
    CmdVisibleFields: TAction;
    CmdFilteredFields: TAction;
    CmdNative: TAction;
    Cmd2nd: TAction;
    CmdClearAll: TAction;
    CmdFormatedFields: TAction;
    ApplicationEvents1: TApplicationEvents;
    CmdSearchM: TAction;
    CmdSpecialFilter: TAction;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    Status: TfcStatusBar;
    CmdNew: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    GrMaster: TDBGridEh;
    QrResourceType: TADOQuery;
    DsResourceType: TDataSource;
    QrResourceTypeKey: TWideStringField;
    QrResourceTypeValue: TWideStringField;
    QrMasterResourceType: TWideStringField;
    QrMasterResourceName: TWideStringField;
    QrMasterResourceValue: TWideStringField;
    QrMasterResourceValue_EN: TWideStringField;
    QrMasterResourceCAT: TStringField;
    QrResourceCAT: TADOQuery;
    DsResourceCAT: TDataSource;
    PopCat: TAdvPopupMenu;
    tcboco1: TMenuItem;
    N3: TMenuItem;
    CmdShow: TAction;
    procedure FormCreate(Sender: TObject);
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdClearAllExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure GrMasterDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure QrResourceCATBeforeOpen(DataSet: TDataSet);
    procedure CmdShowExecute(Sender: TObject);
  private
    mCaption, mFilterStr, mMasterSQL, mCurrentFilter: String;
    mFiltered: Boolean;
  public
  end;

var
  FrmResource: TFrmResource;

implementation

uses
	MainData, islib, isMsg, isDb, ExCommon, NewString, GuidEx, ResourceForm, isCommon;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;
    AddAllFields(QrMaster, 'SYS_RESOURCE', 0);
    mMasterSQL := QrMaster.SQL.Text;
    mCurrentFilter := '';
    mCaption := Caption;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.FormShow(Sender: TObject);
var
    it: TMenuItem;
begin
    QrMaster.SQL.Text := mMasterSQL;
	OpenDataSets([QrMaster, QrResourceType, QrResourceCAT]);

    with QrResourceCAT do
    if not IsEmpty then
    begin
        First;
        while not Eof do
        begin
            it := TMenuItem.Create(Self);
			with it do
			begin
				Caption := FieldByName('VALUE').AsString;
				Tag := FieldByName('ID').AsInteger;
				RadioItem := true;
                Hint := FieldByName('KEY').AsString;
				OnClick := CmdShowExecute;
                PopCat.Items.Add(it);
			end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.GrMasterDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
     if Column.ReadOnly then
     begin
        Column.Color := clBtnFace;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrMaster, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.QrMasterBeforeDelete(DataSet: TDataSet);
begin
     if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['ResourceName', 'ResourceType']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

procedure TFrmResource.QrResourceCATBeforeOpen(DataSet: TDataSet);
begin
    QrResourceCAT.Parameters[0].Value := Iif(sysEnglish, 'Resource Categories EN', 'Resource Categories');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
//    Status.Panels[0].Text := isDb.RecordCount(QrMaster);
//    Status.Panels[1].Text := isdb.RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse: Boolean;
begin
    with QrMaster do
    begin
         bBrowse := State in [dsBrowse];
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDelete.Enabled := bBrowse;
    CmdSearchD.Enabled := bBrowse;
    CmdClearAll.Enabled := mFiltered;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdSwitchExecute(Sender: TObject);
begin
    GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdNewExecute(Sender: TObject);
var
    bm: TBytes;
begin
    Application.CreateForm(TFrmResourceForm, FrmResourceForm);
    FrmResourceForm.ShowModal;
	with QrMaster do
    begin
    	DisableControls;
        bm := BookMark;
        CmdReload.Execute;
        BookMark := bm;
    	EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdSaveExecute(Sender: TObject);
begin
    QrMaster.Post;
end;

procedure TFrmResource.CmdShowExecute(Sender: TObject);
var
	n: Integer;
    key: string;
begin
	with QrMaster do
	begin
		n := (Sender as TComponent).Tag;
		if (n < 1) then
		begin
			Filter := '';
			Caption := mCaption;
            tcboco1.Checked := True;
		end
		else
		begin
            key := (Sender as TMenuItem).Hint;
            Filter := 'ResourceCAT=''' + key + '''';
			Caption := mCaption + ' * ' + (Sender as TMenuItem).Caption;
            (Sender as TMenuItem).Checked := True;
		end;
        Filtered := Filter <> '';
        First;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdCancelExecute(Sender: TObject);
begin
    QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdDeleteExecute(Sender: TObject);
begin
    QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdReloadExecute(Sender: TObject);
begin
	QrMaster.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResource.CmdClearAllExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;
end.
