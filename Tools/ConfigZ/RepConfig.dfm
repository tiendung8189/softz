object FrmRepConfig: TFrmRepConfig
  Left = 323
  Top = 101
  Caption = 'Danh S'#225'ch B'#225'o C'#225'o'
  ClientHeight = 385
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 364
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Width = 650
      end
      item
        Width = 50
      end>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 784
    Top = 36
    Width = 8
    Height = 328
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 1
    HotSpotClosed = True
    HotSpotPosition = 400
    object isPanel1: TisPanel
      Left = 8
      Top = 0
      Width = 0
      Height = 328
      Align = alClient
      Color = 16119285
      ParentBackground = False
      ShowCaption = False
      TabOrder = 0
      HeaderCaption = 'GRID_WEB'
      HeaderColor = clBtnFace
      ImageSet = 4
      RealHeight = 364
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object EdGridWeb: TDBMemo
        Left = 1
        Top = 17
        Width = 390
        Height = 310
        Align = alClient
        DataField = 'GRID_WEB'
        DataSource = DsRep
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 2
    object ToolButton3: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 54
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 108
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object GrList: TDBGridEh
    Left = 0
    Top = 36
    Width = 784
    Height = 328
    ParentCustomHint = False
    Align = alClient
    BiDiMode = bdLeftToRight
    Ctl3D = True
    DataSource = DsRep
    DrawGraphicData = True
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = clWindow
    FixedColor = 13360356
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    OddRowColor = 16119285
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    PopupMenu = PopFunc
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    SearchPanel.OptionsPopupMenuItems = []
    SearchPanel.PreferSearchToEdit = True
    ShowHint = True
    SortLocal = True
    SumList.Active = True
    TabOrder = 3
    TitleParams.Color = 13360356
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = 8404992
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            FieldName = 'VALUE'
            Width = 210
          end
          item
            FieldName = 'KEY'
            Width = 70
          end>
        DropDownBox.ListFieldNames = 'KEY;VALUE'
        DropDownBox.ListSource = DsSysFuncCat
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoSortMarkingEh, dlgMultiSortMarkingEh]
        DropDownWidth = 300
        DynProps = <>
        EditButtons = <>
        FieldName = 'CAT'
        Footers = <>
        LookupParams.KeyFieldNames = 'CAT'
        LookupParams.LookupDataSet = QrSysFuncCat
        LookupParams.LookupDisplayFieldName = 'VALUE'
        LookupParams.LookupKeyFieldNames = 'ID'
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <
          item
            Style = ebsEllipsisEh
            Width = 20
          end>
        FieldName = 'GRID_WEB'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object QrRep: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrRepBeforePost
    AfterPost = QrRepAfterPost
    BeforeDelete = QrRepBeforeDelete
    AfterDelete = QrRepAfterPost
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'SYS_REPORT'
      'order by [SORT]'
      ' ')
    Left = 404
    Top = 120
    object QrRepCAT: TIntegerField
      FieldName = 'CAT'
    end
    object QrRepGRID_WEB: TWideMemoField
      FieldName = 'GRID_WEB'
      BlobType = ftWideMemo
    end
  end
  object DsRep: TDataSource
    DataSet = QrRep
    Left = 404
    Top = 152
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 216
    Top = 148
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdEx: TAction
      Caption = 'L'#432'u ra file        '
      OnExecute = CmdExExecute
    end
    object CmdIm: TAction
      Caption = 'L'#7845'y t'#7915' file'
      OnExecute = CmdImExecute
    end
    object CmdShow: TAction
      Tag = -1
      Caption = 'T'#7845't c'#7843' b'#225'o c'#225'o'
      OnExecute = CmdShowExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm'
      OnExecute = CmdSearchExecute
    end
    object CmdReRead: TAction
      Caption = 'ReRead'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdEmpty: TAction
      Caption = 'X'#243'a to'#224'n b'#7897' b'#225'o c'#225'o'
      OnExecute = CmdEmptyExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
  end
  object PopFunc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Version = '2.6.6.0'
    Left = 244
    Top = 148
    object Import1: TMenuItem
      Action = CmdIm
      GroupIndex = 4
      ImageIndex = 15
      Visible = False
    end
    object Export1: TMenuItem
      Action = CmdEx
      GroupIndex = 4
      ImageIndex = 16
      Visible = False
    end
    object N4: TMenuItem
      Caption = '-'
      GroupIndex = 4
      Visible = False
    end
    object Xatonbboco1: TMenuItem
      Action = CmdEmpty
      GroupIndex = 4
    end
    object N3: TMenuItem
      Caption = '-'
      GroupIndex = 4
    end
    object tcboco1: TMenuItem
      Tag = -1
      Caption = 'T'#7845't c'#7843' b'#225'o c'#225'o'
      Checked = True
      GroupIndex = 4
      RadioItem = True
      OnClick = CmdShowExecute
    end
    object N1: TMenuItem
      Caption = '-'
      GroupIndex = 4
    end
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '*.xml'
    Filter = 'Comma Separated Value (*.xml)|*.xml|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Ch'#7885'n file'
    Left = 300
    Top = 148
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '*.xml'
    Filter = 'Comma Separated Value (*.xml)|*.xml|All Files (*.*)|*.*'
    Title = 'Ch'#7885'n file'
    Left = 272
    Top = 148
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 188
    Top = 148
  end
  object SYNC_REP_RIGHTS: TADOCommand
    CommandText = 'SYS_SYNC_REP_RIGHTS;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 475
    Top = 152
  end
  object QrSysFuncCat: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrSysFuncCatBeforeOpen
    Parameters = <
      item
        Name = 'Item'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      
        'select  cast([KEY] as int) ID, *  from fnS_Table_By_Comma ((sele' +
        'ct Value from SYS_FLEXCONFIG where Section = '#39'Admin'#39' and Item = ' +
        ':Item), '#39'='#39')')
    Left = 376
    Top = 216
  end
  object DsSysFuncCat: TDataSource
    DataSet = QrSysFuncCat
    Left = 376
    Top = 244
  end
end
