﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FuncConfig;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  wwfltdlg, Db, ADODB, Menus, ActnList,
  Wwdbgrid, AppEvnts, Wwdbcomb, ComCtrls,
  wwDBGrid2, AdvMenus, wwDialog, StdCtrls, Mask, wwdbedit, Wwdotdot, Grids,
  Wwdbigrd, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, DBCtrlsEh;

type
  TFrmFuncConfig = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    QrFunc: TADOQuery;
    DsFUNC: TDataSource;
    Filter: TwwFilterDialog;
    CmdFilter: TAction;
    CmdExFunction: TAction;
    CmdImFunction: TAction;
    OpenDlg: TOpenDialog;
    SaveDlg: TSaveDialog;
    PopFunc: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CbGroup: TwwDBComboBox;
    Status: TStatusBar;
    SYNC_RIGHTS: TADOCommand;
    Import1: TMenuItem;
    Export1: TMenuItem;
    CmdShow: TAction;
    tcboco1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    CmdSearch: TAction;
    CmdReRead: TAction;
    CmdEmpty: TAction;
    N4: TMenuItem;
    Xatonbchcnng1: TMenuItem;
    GrList: TDBGridEh;
    DsResource: TDataSource;
    QrResource: TADOQuery;
    QrResourceResourceType: TWideStringField;
    QrResourceResourceName: TWideStringField;
    QrResourceResourceValue: TWideStringField;
    QrResourceResourceValue_EN: TWideStringField;
    QrSysFuncCat: TADOQuery;
    DsSysFuncCat: TDataSource;
    QrFuncCAT: TIntegerField;
    QrFuncFUNC_CAT: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdExFunctionExecute(Sender: TObject);
    procedure CmdImFunctionExecute(Sender: TObject);
    procedure QrFuncAfterPost(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrFuncBeforeDelete(DataSet: TDataSet);
	procedure CmdShowExecute(Sender: TObject);
	procedure CmdSearchExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure QrFuncBeforePost(DataSet: TDataSet);
    procedure QrSysFuncCatBeforeOpen(DataSet: TDataSet);
    procedure QrFuncFUNC_CATChange(Sender: TField);
    procedure QrFuncCATChange(Sender: TField);
  private
	mRet, mTrigger: Boolean;
    mCaption: String;
  public
	function Execute: Boolean;
  end;

var
  FrmFuncConfig: TFrmFuncConfig;

implementation

{$R *.DFM}

uses
	isLib, isMsg, isDb, ExCommon, MainData, isCommon, isStr;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFuncConfig.Execute: Boolean;
begin
	mRet := False;
	ShowModal;
	Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    AddAllFields(QrFunc, 'SYS_FUNC');
	mTrigger := False;
    mCaption := Caption;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.FormShow(Sender: TObject);
var
	i: Integer;
	it: TMenuItem;
begin
     OpenDataSets([QrFunc, QrResource, QrSysFuncCat]);
     // Function group names
     with QrSysFuncCat do
     if not IsEmpty then
     begin
         First;
         while not Eof do
         begin
             it := TMenuItem.Create(Self);
             with it do
             begin
                 Caption := FieldByName('VALUE').AsString;
                 Tag := FieldByName('ID').AsInteger;
                 RadioItem := true;
                 Hint := FieldByName('KEY').AsString;
                 OnClick := CmdShowExecute;
                 PopFunc.Items.Add(it);
             end;
             Next;
         end;
     end;

     buildGriEh('SYS_FUNC', GrList, True);
     CmdShow.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//	GrList.SaveToIniFile;

	with QrFunc do
	begin
		CheckBrowseMode;
		Close;
	end;

	if mRet then
	begin
		// Synchro rights
		Wait(PREPARING);
		SYNC_RIGHTS.Execute;
		ClearWait;
	end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_EMPTY    = 'Xóa toàn bộ chức năng. Tiếp tục?';

procedure TFrmFuncConfig.CmdEmptyExecute(Sender: TObject);
begin
    if not YesNo(RS_EMPTY, 1) then
        Exit;
    QrFunc.DisableControls;
    EmptyDataset(QrFunc);
    QrFunc.EnableControls;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdExFunctionExecute(Sender: TObject);
begin
	if not SaveDlg.Execute then
		Exit;
//	exXmlExport(QrFunc, 'SYS_FUNC', SaveDlg.FileName);
	MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIM_IM        = 'Import từ file đã chọn. Tiếp tục?';

procedure TFrmFuncConfig.CmdImFunctionExecute(Sender: TObject);
begin
	// Get file name
	if not OpenDlg.Execute then
		Exit;

	// Confirm
	if not YesNo(RS_CONFIM_IM) then
		Exit;

	Wait(PROCESSING);

	//Import
	with QrFunc do
	begin
		DisableControls;
		mTrigger := True;
//		exXmlImport(QrFunc, OpenDlg.FileName, 'SYS_FUNC', 'FUNC_CODE');
		mTrigger := False;
		EnableControls;
	end;

	ClearWait;
	MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdReReadExecute(Sender: TObject);
begin
    Screen.Cursor := crSQLWait;
	with QrFunc do
	begin
		Close;
		Open;
    end;
    Screen.Cursor := crDefault
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsFUNC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.CmdShowExecute(Sender: TObject);
var
	n: Integer;
    key: string;
begin
	with QrFunc do
	begin
        n := (Sender as TComponent).Tag;
		if (n < 0) then
		begin
			Filter := '';
			Caption := mCaption;
            tcboco1.Checked := True;
		end
		else
		begin
            key := (Sender as TMenuItem).Hint;
            Filter := Format('FUNC_CAT=''%s''', [key]);
			Caption := mCaption + ' * ' + (Sender as TMenuItem).Caption;
            (Sender as TMenuItem).Checked := True;
		end;
        Filtered := Filter <> '';
        First;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.QrFuncAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrFunc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.QrFuncBeforeDelete(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.QrFuncBeforePost(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

procedure TFrmFuncConfig.QrFuncCATChange(Sender: TField);
var
    s : string;
    n : Integer;
    bTrigger: Boolean;
begin
    if mTrigger then
    	Exit;

    bTrigger := mTrigger;
    mTrigger := True;

    s := '';
    n := Sender.AsInteger;
    with QrFunc do
	begin
        with QrSysFuncCat do
        if (n >= 0) and Locate('ID', n, []) then
        begin
           s := FieldByName('KEY').AsString;
        end;
        FieldByName('FUNC_CAT').AsString := s;
    end;

    mTrigger := bTrigger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.QrFuncFUNC_CATChange(Sender: TField);
var
    s : string;
    n : Integer;
    bTrigger: Boolean;
begin
    if mTrigger then
    	Exit;

    bTrigger := mTrigger;
    mTrigger := True;

    n := -1;
    s := Sender.AsString;
    with QrFunc do
	begin
        with QrSysFuncCat do
        if (s <> '') and Locate('KEY', s, []) then
        begin
           n := FieldByName('ID').AsInteger;
        end;
        FieldByName('CAT').AsInteger := n;
    end;
    mTrigger := bTrigger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncConfig.QrSysFuncCatBeforeOpen(DataSet: TDataSet);
begin
   QrSysFuncCat.Parameters[0].Value := Iif(sysEnglish, 'Function Categories EN', 'Function Categories');
end;

end.
