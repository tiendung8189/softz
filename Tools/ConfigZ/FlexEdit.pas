(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FlexEdit;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Db,
  ADODb, Menus, AppEvnts, fcStatusBar,
  wwDBGrid2, IniFiles, kbmMemTable,
  AdvMenus, Grids, Wwdbigrd, Wwdbgrid, ToolWin, isPanel;

type
  TFrmFlexEdit = class(TForm)
    Action: TActionList;
    CmdSave: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    GrMaster: TwwDBGrid2;
    PaDetail: TisPanel;
    GrDetail: TwwDBGrid2;
    Splitter1: TSplitter;
    QrParams: TADOQuery;
    DsDetail: TDataSource;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSearch: TAction;
    ToolButton2: TToolButton;
    PopLeft: TAdvPopupMenu;
    Search1: TMenuItem;
    CmdSwitch: TAction;
    ToolButton5: TToolButton;
    CmdCancel: TAction;
    CmdCopy: TAction;
    N1: TMenuItem;
    Copy1: TMenuItem;
    Status: TfcStatusBar;
    tbMaster: TkbmMemTable;
    tbMasterSection: TWideStringField;
    tbMasterDescription: TWideStringField;
    tbMasterIDX: TIntegerField;
    tbMasterEnable: TByteField;
    tbMasterFill: TByteField;
    tbMasterContent: TMemoField;
    tbDetail: TkbmMemTable;
    tbDetailSection: TWideStringField;
    tbDetailIDX: TIntegerField;
    tbDetailItem: TWideStringField;
    tbDetailValue: TWideStringField;
    tbDetailValue2: TWideStringField;
    tbDetailValue3: TWideStringField;
    tbDetailValue4: TWideStringField;
    tbDetailValue5: TWideStringField;
    tbDetailValue6: TWideStringField;
    tbDetailValue7: TWideStringField;
    tbDetailValue8: TWideStringField;
    tbDetailValue9: TWideStringField;
    tbDetailDisable: TByteField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdSaveExecute(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
    procedure TntFormResize(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrMasterAfterScroll(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDetailAfterInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrDetailAfterPost(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure GrMasterExit(Sender: TObject);
    procedure QrDetailBeforeInsert(DataSet: TDataSet);
    procedure QrMasterSectionValidate(Sender: TField);
    procedure CmdCopyExecute(Sender: TObject);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
  private
    mTrigger: Boolean;
    mID: Integer;
    
    procedure LoadData;
    procedure SaveData;
  public
  	procedure Execute;
  end;

var
  FrmFlexEdit: TFrmFlexEdit;

implementation

uses
    MainData, isDb, isMsg, ExCommon, islib, isFile;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.Execute;
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.LoadData;
var
	i, k, n, m: Integer;
	sFile, sSec, sItem: String;
    lSec, lItem: TStrings;
begin
    Screen.Cursor := crHourGlass;

	// Ini file preparation
    sFile := isGetTempFileName;
    with QrParams do
    begin
    	if Active then
            Close;
        Open;
	    TBlobField(FieldByName('FLEXIBLE')).SaveToFile(sFile);
    end;

    // Initial
    with tbDetail do
    begin
    	Close;
        Open;
        DisableControls;
    end;

    with tbMaster do
    begin
    	Close;
        Open;
        DisableControls;
    end;
    mTrigger := True;

	// Load7 into TStrings
    lSec := TStringList.Create;
    lItem := TStringList.Create;

    with TIniFile.Create(sFile) do
    begin
    	ReadSections(lSec);

        // List of sections
		n := lSec.Count - 1;
        mID := n + 1;

    	for i := 0 to n do
        begin
            sSec := lSec[i];

            // Section
            with tbMaster do
            begin
                Append;
	            FieldByName('IDX').AsInteger := i + 1;
                FieldByName('Section').AsString := sSec;
                Post;
            end;

            // List of Items
            lItem.Clear;
		    ReadSectionValues(sSec, lItem);
	        m := lItem.Count - 1;
	        for k := 0 to m do
	        begin
    	        sItem := lItem.Names[k];
		        with tbDetail do
        		begin
                	Append;
		            FieldByName('IDX').AsInteger := i + 1;
		            FieldByName('Section').AsString := sSec;
        		    FieldByName('Item').AsString := sItem;
					FieldByName('Value').AsString := lItem.Values[lItem.Names[k]];
        		    Post;
        		end;
		    end;
        end;
        Free;
    end;
	DeleteFile(sFile);

    tbDetail.EnableControls;
    with tbMaster do
    begin
        First;
        EnableControls;
    end;

    lSec.Free;
    lItem.Free;
    mTrigger := False;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.SaveData;
var
	ls: TStrings;
	ini: TMemIniFile;
    s, sFile, sSec: String;
    bm: TBytes;
begin
	// Data file preparation
	sFile := isGetTempFileName;
	ini := TMemIniFile.Create(sFile);

    // Scan and save
    with tbMaster do
    begin
        CheckBrowseMode;
        bm := Bookmark;
        First;

        while not Eof do
        begin
            sSec := FieldByName('Section').AsString;
            ini.EraseSection(sSec);

            // write all value to section
            with tbDetail do
            begin
                First;
                while not Eof do
                begin
                    s := FieldByName('Item').AsString;
					ini.WriteString(sSec, s, FieldByName('Value').AsWideString);
                    Next;
                end;
            end;
            Next;
        end;
        Bookmark := bm;
    end;

    ls := TStringList.Create;
    ini.GetStrings(ls);
    ini.Free;
	DeleteFile(sFile);

    SetEditState(QrParams);
	with QrParams do
    begin
		FieldByName('FLEXIBLE').Value := ls.Text;
        Post;
    end;
    ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.TntFormCreate(Sender: TObject);
begin
    Caption := Caption;
    TMyForm(Self).Init2;
    ResetRowColor([GrMaster, GrDetail]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.FormShow(Sender: TObject);
begin
    LoadData;
    SetDictionary(tbMaster, 'LIST_FLEXIBLE', nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrParams, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	b := QrParams.State in [dsBrowse];
    CmdSave.Enabled := not b;
    CmdCancel.Enabled := not b;
    CmdSearch.Enabled := b;
	CmdCopy.Enabled := tbMaster.FieldByName('Section').AsString <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrMaster then
        GrDetail.SetFocus
    else
        GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdSaveExecute(Sender: TObject);
begin
	SaveData;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdCancelExecute(Sender: TObject);
begin
    LoadData;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdReloadExecute(Sender: TObject);
begin
	if QrParams.State in [dsBrowse] then
    else if YesNo('Save Changed?', 1) then
    	SaveData;
	LoadData;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrMasterAfterScroll(DataSet: TDataSet);
begin
    with tbMaster do
        PaDetail.HeaderCaption := ' :: ' + FieldByName('Section').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrMasterBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    tbDetail.CheckBrowseMode;
	SetEditState(QrParams);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrDetailAfterInsert(DataSet: TDataSet);
begin
    with tbDetail do
    begin
        FieldByName('IDX').AsInteger := tbMaster.FieldByName('IDX').AsInteger;
        FieldByName('Section').AsString := tbMaster.FieldByName('Section').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrDetailBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(tbDetail, ['Item']) then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrDetailAfterPost(DataSet: TDataSet);
begin
    if not mTrigger then
        SetEditState(QrParams);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := RecordCount(tbMaster);
    Status.Panels[1].Text := RecordCount(tbDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.GrMasterExit(Sender: TObject);
begin
    tbMaster.CheckBrowseMode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrDetailBeforeInsert(DataSet: TDataSet);
begin
	tbMaster.CheckBrowseMode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrMasterSectionValidate(Sender: TField);
begin
	if Sender.AsString = '' then
    begin
    	ErrMsg('Invalid Input.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.CmdCopyExecute(Sender: TObject);
var
	sec: String;
    i: Integer;
    ls: TStrings;
begin
	// Allocate new section name
	with tbMaster do
    begin
    	CheckBrowseMode;
    	DisableControls;
		sec := FieldByName('Section').AsString;
        i := 1;
        while Locate('Section', sec + IntToStr(i), []) do
        	Inc(i);
        EnableControls;
    end;

    // Query to list
    ls := TStringList.Create;
    with tbDetail do
    begin
    	First;
        while not Eof do
        begin
        	ls.Add(FieldByName('Item').AsString + '=' + FieldByName('Value').AsString);
            Next;
        end;
    end;

    // Clone
    with tbMaster do
    begin
    	Append;
        FieldByName('Section').AsString := sec + IntToStr(i);
    end;

    with tbDetail do
    	for i := 0 to ls.Count - 1 do
        begin
        	Append;
            FieldByName('Item').AsString := ls.Names[i];
            FieldByName('Value').AsString := ls.ValueFromIndex[i];
            Post;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexEdit.QrMasterAfterInsert(DataSet: TDataSet);
begin
	with tbMaster do
    begin
        FieldByName('IDX').AsInteger := mID;
        Inc(mID);
    end;
end;

end.
