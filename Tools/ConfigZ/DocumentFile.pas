﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DocumentFile;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  ADODb, Db,
  Menus , AppEvnts,
  Wwdbgrid, RzDBBnEd,
  AdvMenus, StdCtrls, Mask, RzEdit, RzDBEdit, Grids, Wwdbigrd, ToolWin;

type
  TFrmDocumentFile = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    Tmmutin1: TMenuItem;
    PaMaster: TPanel;
    BtImport: TRzDBButtonEdit;
    BtClearFile: TRzDBButtonEdit;
    BtView: TRzDBButtonEdit;
    QrListNAME: TWideStringField;
    QrListFILENAME: TWideStringField;
    QrListCONTENT: TBlobField;
    QrListEXT: TWideStringField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrListF1: TIntegerField;
    QrListF2: TIntegerField;
    QrListF3: TIntegerField;
    QrListCAL_STT: TIntegerField;
    QrListCAL_EXT: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure BtImportButtonClick(Sender: TObject);
    procedure BtClearFileButtonClick(Sender: TObject);
    procedure BtViewButtonClick(Sender: TObject);
  private
    mKhoa: Integer;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  end;

var
  FrmDocumentFile: TFrmDocumentFile;

implementation

uses
	ExCommon,  isDb, islib, isMsg, Rights, isStr, isFile,
    ShellAPI, isCommon;

{$R *.DFM}

const
    FORM_CODE: String   = 'SYS_DOCUMENT';

procedure TFrmDocumentFile.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.FormShow(Sender: TObject);
begin
    ForceDirectories(sysAppData);
    QrList.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrList do
    begin
    	Edit;
		TBlobField(FieldByName('CONTENT')).LoadFromFile(s);
	    if Trim(FieldByName('FILENAME').AsString) = '' then
        	FieldByName('FILENAME').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('EXT').AsString := ExtractFileExt(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.BtClearFileButtonClick(Sender: TObject);
begin
    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.BtImportButtonClick(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.BtViewButtonClick(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.Dettach();
begin
     with QrList do
     begin
        Edit;
        FieldByName('GHICHU').Clear;
        FieldByName('CONTENT').Clear;
        FieldByName('EXT').Clear;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.ViewAttachment();
var
	s, sn: String;
begin
	with QrList do
    begin
    	sn := isStripToneMark(FieldByName('FILENAME').AsString);
        s := sysAppData + sn + FieldByName('EXT').AsString;
        try
	        TBlobField(FieldByName('CONTENT')).SaveToFile(s);
        except
        	ErrMsg('Lỗi trích xuất nội dung.');
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([QrList]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrList, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdNewExecute(Sender: TObject);
begin
    QrList.Append;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdSaveExecute(Sender: TObject);
begin
    QrList.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdCancelExecute(Sender: TObject);
begin
    QrList.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsList)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrList, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.QrListBeforePost(DataSet: TDataSet);
begin
	with (DataSet) do
    begin
        if BlankConfirm(QrList,['FILENAME']) then
            Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.QrListCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrList do
    begin
        // File extension
        s := FieldByName('EXT').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('CAL_EXT').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDocumentFile.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrList);
end;

end.
