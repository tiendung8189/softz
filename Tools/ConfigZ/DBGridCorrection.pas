unit DBGridCorrection;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons;

type
  TFrmDBGridCorrection = class(TForm)
    GroupBox1: TGroupBox;
    Ck1: TCheckBox;
    Ck2: TCheckBox;
    Ck3: TCheckBox;
    btContinue: TBitBtn;
    btCancel: TBitBtn;
  private
  public
  function Execute(): Boolean;
  end;

var
  FrmDBGridCorrection: TFrmDBGridCorrection;

implementation

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDBGridCorrection.Execute(): Boolean;
begin
    Result := ShowModal = mrOk;
end;

end.
