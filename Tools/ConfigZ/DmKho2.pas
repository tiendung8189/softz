(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmKho2;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2, ExtCtrls,
  ADODb, Db, Menus, AdvMenus,
  AppEvnts, Wwdbgrid,
  fcdbtreeview, RzSplit, StdCtrls, DBCtrls,
  isPanel, Mask, Wwdbigrd, RzPanel, ToolWin, wwdbedit;

type
  TFrmDmKho2 = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    QrKho: TADOQuery;
    DsKho: TDataSource;
    QrChinhanh: TADOQuery;
    DsChinhanh: TDataSource;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    Tm1: TMenuItem;
    CmdAudit: TAction;
    QrChinhanhDIACHI: TWideStringField;
    QrChinhanhDTHOAI: TWideStringField;
    QrChinhanhFAX: TWideStringField;
    QrChinhanhGHICHU: TWideMemoField;
    QrChinhanhCREATE_BY: TIntegerField;
    QrChinhanhUPDATE_BY: TIntegerField;
    QrChinhanhCREATE_DATE: TDateTimeField;
    QrChinhanhUPDATE_DATE: TDateTimeField;
    QrKhoMAKHO: TWideStringField;
    QrKhoTENKHO: TWideStringField;
    QrKhoDIACHI: TWideStringField;
    QrKhoDTHOAI: TWideStringField;
    QrKhoFAX: TWideStringField;
    QrKhoTHUKHO: TWideStringField;
    QrKhoGHICHU: TWideMemoField;
    QrKhoNGAYTT: TDateTimeField;
    QrKhoCREATE_BY: TIntegerField;
    QrKhoUPDATE_BY: TIntegerField;
    QrKhoCREATE_DATE: TDateTimeField;
    QrKhoUPDATE_DATE: TDateTimeField;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    GrChiNhanh: TwwDBGrid2;
    TabSheet2: TTabSheet;
    GrKho: TwwDBGrid2;
    RzSizePanel2: TRzSizePanel;
    PD1: TisPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    EdMAKHO: TwwDBEdit;
    EdTENKHO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit4: TwwDBEdit;
    DBEdit6: TwwDBEdit;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    QrChinhanhLOC: TWideStringField;
    QrChinhanhTEN: TWideStringField;
    QrKhoLOC: TWideStringField;
    CmdThelenh: TAction;
    CmdThelenh2: TAction;
    QrChinhanhTHELENH: TWideStringField;
    ToolButton10: TToolButton;
    QrChinhanhIDX: TIntegerField;
    QrChinhanhNGUNG_SUDUNG: TBooleanField;
    QrChinhanhIS_DRC: TBooleanField;
    QrChinhanhHEADER1: TWideStringField;
    QrChinhanhHEADER2: TWideStringField;
    QrChinhanhHEADER3: TWideStringField;
    QrChinhanhHEADER4: TWideStringField;
    QrChinhanhHEADER5: TWideStringField;
    QrChinhanhFOOTER1: TWideStringField;
    QrChinhanhFOOTER2: TWideStringField;
    QrChinhanhFOOTER3: TWideStringField;
    QrChinhanhFOOTER4: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrKhoCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrKhoBeforePost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure PgMainChange(Sender: TObject);
    procedure QrKhoBeforeInsert(DataSet: TDataSet);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrKhoBeforeDelete(DataSet: TDataSet);
    procedure TreeViewCalcNodeAttributes(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode);
    procedure QrChinhanhBeforeInsert(DataSet: TDataSet);
    procedure BtthelenhClick(Sender: TObject);
    procedure QrKhoBeforeEdit(DataSet: TDataSet);
    procedure QrChinhanhBeforePost(DataSet: TDataSet);
    procedure QrChinhanhAfterInsert(DataSet: TDataSet);
    procedure QrChinhanhBeforeDelete(DataSet: TDataSet);
  private
    mCanEdit, fixCode, fixCode1: Boolean;
    mGrid: TwwDBGrid2;
    mQuery: TADOQuery;
    mDs: TDataSource;
    fIDX: Integer;
  public
  	procedure Execute();
  end;

var
  FrmDmKho2: TFrmDmKho2;

implementation

uses
	ExCommon, MainData, isDb, isMsg, Rights, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.Execute;
begin
	mCanEdit := True;
    ShowModal;
end;

    (*
    **  Forms
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE1: String   = 'ADMIN_DM_LOCATION';
    FORM_CODE: String   = 'ADMIN_DM_KHO';

procedure TFrmDmKho2.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

    fixCode := SetCodeLength(FORM_CODE, QrKho.FieldByName('MAKHO'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.FormShow(Sender: TObject);
begin
	GrChiNhanh.ReadOnly    := not mCanEdit;
	GrKho.ReadOnly     := not mCanEdit;

    if RegReadBool(Name, 'PDKHO', True) then
        RzSizePanel2.CloseHotSpot
    else
        RzSizePanel2.RestoreHotSpot;

    SetDictionary([QrChinhanh, QrKho],
    	[FORM_CODE1, FORM_CODE], []);

    SetCustomGrid(
        [FORM_CODE1, FORM_CODE],
        [GrChiNhanh, GrKho]);

    OpenDataSets([QrChinhanh, QrKho]);
    PgMain.OnChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(mQuery, True)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    RegWrite(Name, 'PDKHO', RzSizePanel2.HotSpotClosed);
	HideAudit;
	CloseDataSets(DataMain.Conn);
	Action := caFree;
end;

    (*
    ** Page cotrol
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.PgMainChange(Sender: TObject);

begin
	case PgMain.ActivePageIndex of
    0:
    	begin
        	mQuery := QrChinhanh;
            mDs := DsChinhanh;
            mGrid  := GrChiNhanh;
        end;
    1:
    	begin
            mQuery := QrKho;
            mDs := DsKho;
            mGrid  := GrKho;
        end;
    end;

	HideAudit;
    mGrid.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

	(*
    ** Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
    mGrid.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name + IntToStr(PgMain.ActivePageIndex), mGrid.DataSource);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, mQuery, mCanEdit);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.OnDbError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrChinhanhAfterInsert(DataSet: TDataSet);
begin
    with QrChinhanh do
    begin
        FieldByName('IDX').AsInteger := fIDX + 1;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrChinhanhBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrChinhanhBeforeInsert(DataSet: TDataSet);
begin
    with QrChinhanh do
    begin
        Last;
        fIDX := FieldByName('IDX').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrChinhanhBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrChinhanh, ['LOC', 'TEN']) then
        Abort;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrKhoBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrKhoBeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrKhoBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    if QrKho.RecordCount > 2 then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.QrKhoBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrKho, ['MAKHO', 'TENKHO']) then
        Abort;

    if fixCode then
        if LengthConfirm(QrKho.FieldByName('MAKHO')) then
            Abort;

	with QrKho.FieldByName('LOC') do
        if IsNull then
            AsString  := QrChinhanh.FieldByName('LOC').AsString;

    SetAudit(DataSet);
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if mQuery = Nil then
    	Exit;
    Status.SimpleText := RecordCount(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.BtthelenhClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.GrKhoCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'LOC') or (Field.FullName = 'MAKHO') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKho2.TreeViewCalcNodeAttributes(
  TreeView: TfcDBCustomTreeView; Node: TfcDBTreeNode);
begin
	with Node do
    	ImageIndex := Level
end;

end.
