(*==============================================================================
**------------------------------------------------------------------------------
*)
unit VersionUpdate;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, System.StrUtils,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid2, Db, ADODB, ActnList,
  Menus, ComCtrls, wwfltdlg, Wwdbgrid, wwFltDlg2, AppEvnts,
  isPanel, DBCtrls, fcStatusBar, AdvMenus, wwDialog, StdCtrls, ToolWin;

type
  TFrmVersionUpdate = class(TForm)
    Splitter1: TSplitter;
    GrMaster: TwwDBGrid2;
    QrDetail: TADOQuery;
    QrMaster: TADOQuery;
    DsDetail: TDataSource;
    DsMaster: TDataSource;
    ActionList: TActionList;
    CmdSearchD: TAction;
    CmdQuit: TAction;
    Filter: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdDelete: TAction;
    CmdReload: TAction;
    PopRight: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdVisibleFields: TAction;
    CmdFilteredFields: TAction;
    N3: TMenuItem;
    CmdNative: TAction;
    Cmd2nd: TAction;
    CmdClearAll: TAction;
    ClearAllFiltered1: TMenuItem;
    CmdFormatedFields: TAction;
    ApplicationEvents1: TApplicationEvents;
    PaDetail: TisPanel;
    GrDetail: TwwDBGrid2;
    TntPopupMenu1: TAdvPopupMenu;
    CmdSearchM: TAction;
    Search1: TMenuItem;
    CmdSpecialFilter: TAction;
    vlPanel1: TisPanel;
    EdComment: TDBMemo;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdSwitch: TAction;
    Status: TfcStatusBar;
    spl1: TSplitter;
    QrDetailIdx: TGuidField;
    QrDetailCREATE_BY: TIntegerField;
    QrDetailUPDATE_BY: TIntegerField;
    QrDetailCREATE_DATE: TDateTimeField;
    QrDetailUPDATE_DATE: TDateTimeField;
    QrDetailROWGUID: TGuidField;
    CmdNew: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrMasterVersion: TWideStringField;
    QrMasterVersionDate: TDateTimeField;
    QrDetailVersion: TWideStringField;
    QrDetailVersionDate: TDateTimeField;
    QrDetailVersionType: TIntegerField;
    QrDetailFileName: TWideStringField;
    QrDetailFileContent: TBlobField;
    QrDetailFileExt: TWideStringField;
    QrDetailFileSubFolder: TWideStringField;
    QrDetailFileChecksum: TWideStringField;
    QrDetailFileVersion: TWideStringField;
    QrDetailComment: TWideMemoField;
    QrDetailIMG1: TIntegerField;
    QrMasterVersionKey: TWideStringField;
    QrDetailVersionKey: TWideStringField;
    QrDetailVersionFolder: TWideStringField;
    QrDetailFileType: TIntegerField;
    QrMasterVersionFolder: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdSearchDExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDeleteExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure CmdClearAllExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure QrMasterAfterScroll(DataSet: TDataSet);
    procedure Splitter1Moved(Sender: TObject);
    procedure CmdSearchMExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure CmdNewExecute(Sender: TObject);
    procedure QrDetailAfterInsert(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailCalcFields(DataSet: TDataSet);
    procedure QrDetailBeforeInsert(DataSet: TDataSet);
  private
    mDetailSQL, mFilterStr, mMasterSQL, mCurrentFilter: String;
    mFiltered, mNew: Boolean;
  public
    { Public declarations }
  end;

var
  FrmVersionUpdate: TFrmVersionUpdate;

implementation

uses
	MainData, islib, isMsg, isDb, ExCommon, NewString, isFile, isSystem, isStr, GuidEx;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.FormCreate(Sender: TObject);
begin
   	TMyForm(Self).Init2;
    AddAllFields(QrDetail, 'SYS_VERSION', 0);

    ResetRowColor([GrMaster, GrDetail]);
    mDetailSQL := QrDetail.SQL.Text;
    mMasterSQL := QrMaster.SQL.Text;
    mNew := False;
    mCurrentFilter := '';
    mFilterStr := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.FormShow(Sender: TObject);
begin
    QrMaster.SQL.Text := mMasterSQL;
	OpenDataSets([QrMaster, QrDetail]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.TntFormResize(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

    (*
    **  DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrMasterAfterScroll(DataSet: TDataSet);
var
    s, s2: String;
begin
    s := QrMaster.FieldByName('Version').AsString;
    s2 := QrMaster.FieldByName('VersionKey').AsString;
    with QrDetail do
    begin
        Filter := 'Version=''' + s +'''' + ' and VersionKey=''' + s2 +'''';
        PaDetail.HeaderCaption := ' :: ' + Filter;
    end;
    GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailAfterInsert(DataSet: TDataSet);
var
    s, sVer, sName, sDir: String;
    ls: TStrings;
begin
    // Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    ls := TStringList.Create;
    GetVersionInfos(s, ls);

    sVer := ls.Values['FileVersion'];
    sName := ExtractFileName(s);
    sDir := ExtractFileDir(s);

    with QrDetail do
    begin
        TGuidField(FieldByName('Idx')).AsGuid := GuidEx.TGuidEx.NewGuid;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
        FieldByName('FileName').AsString := sName;
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
        FieldByName('FileVersion').AsString := sVer;

        if mNew then
        begin
            FieldByName('Version').AsString := sVer;
            FieldByName('VersionDate').AsDateTime := Now;
            FieldByName('VersionKey').AsString := sName;
            FieldByName('VersionFolder').AsString := sDir;
            FieldByName('FileSubFolder').AsString := '';
        end else
        begin
            FieldByName('Version').AsString := QrMaster.FieldByName('Version').AsString;
            FieldByName('VersionDate').AsDateTime := QrMaster.FieldByName('VersionDate').AsDateTime;
            FieldByName('VersionKey').AsString := QrMaster.FieldByName('VersionKey').AsString;
            FieldByName('VersionFolder').AsString := QrMaster.FieldByName('VersionFolder').AsString;
            FieldByName('FileSubFolder').AsString := isStr.isRight(sDir, Length(sDir) - Length(QrMaster.FieldByName('VersionFolder').AsString));
        end;

        FieldByName('VersionType').AsInteger := 0;
    end;
    ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailBeforeDelete(DataSet: TDataSet);
begin
//    with QrDetail do
//    begin
//        if FieldByName('VersionKey').AsString = FieldByName('FileName').AsString then
//            Abort;
//    end;

    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailBeforeInsert(DataSet: TDataSet);
begin
    if (not mNew) and QrMaster.IsEmpty then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailBeforeOpen(DataSet: TDataSet);
begin
//    QrDetail.SQL.Text := mDetailSQL + mFilterStr;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailBeforePost(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if BlankConfirm(QrDetail, ['FileName']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.QrDetailCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
        case IndexStr(FieldByName('FileExt').AsString,
                ['.exe', '.xls', '.xlsx', '.doc', '.docx', '.rpt', '.rpz', '.pdf',
                 '.ini', '.inf', '.bat', '.zip', '.rar', '.dll', '.json', '.xml', '.bak']) of
            0: FieldByName('IMG1').AsInteger := 3;
            1,2: FieldByName('IMG1').AsInteger := 4;
            3,4: FieldByName('IMG1').AsInteger := 5;
            5,6: FieldByName('IMG1').AsInteger := 6;
            7: FieldByName('IMG1').AsInteger := 7;
            8,9: FieldByName('IMG1').AsInteger := 8;
            10: FieldByName('IMG1').AsInteger := 9;
            11,12: FieldByName('IMG1').AsInteger := 10;
            13: FieldByName('IMG1').AsInteger := 11;
            14: FieldByName('IMG1').AsInteger := 12;
            15: FieldByName('IMG1').AsInteger := 13;
            16: FieldByName('IMG1').AsInteger := 14;
        else
            FieldByName('IMG1').AsInteger := 2;
        end;
    end;
end;

(*
    **  Grid
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := isDb.RecordCount(QrMaster);
    Status.Panels[1].Text := isdb.RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.Splitter1Moved(Sender: TObject);
begin
    Status.Panels[0].Width := IntToStr(GrMaster.Width);
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse: Boolean;
begin
    bBrowse := QrDetail.State in [dsBrowse];

    with QrMaster do
    begin
        CmdDelete.Enabled := not IsEmpty;
        CmdSearchM.Enabled := bBrowse;
    end;

    CmdNew.Enabled := bBrowse;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;

    CmdSearchD.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdClearAll.Enabled := mFiltered;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrMaster then
        GrDetail.SetFocus
    else if ActiveControl = GrDetail then
        EdComment.SetFocus
    else
        GrMaster.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdSearchMExecute(Sender: TObject);
begin
    exSearch(Name + '_M', DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdSearchDExecute(Sender: TObject);
begin
    exSearch(Name + '_D', DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdFilterExecute(Sender: TObject);
begin
    //Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdNewExecute(Sender: TObject);

begin
    with QrDetail do
    begin
        mNew := True;
        Append;
        FieldByName('VersionDate').AsDateTime := Now;
        Post;
        mNew := False;
    end;

    with QrMaster do
    begin
        Requery;
        First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdClearFilterExecute(Sender: TObject);
begin
    Filter.ClearFilter
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdSaveExecute(Sender: TObject);
begin
    QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdCancelExecute(Sender: TObject);
begin
    QrDetail.Cancel;
    mNew := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdDeleteExecute(Sender: TObject);
var
    s, s1: String;
begin
    QrDetail.Delete;

    with QrMaster do
    begin
        Requery;
        if s1 <> '' then
            if not Locate('Version', s1, []) then
                First;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdReloadExecute(Sender: TObject);
begin
	QrMaster.Requery;
    QrDetail.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVersionUpdate.CmdClearAllExecute(Sender: TObject);
var
    n, i: Integer;
begin
    mFiltered := False;
    CmdVisibleFields.Checked := False;
    CmdFilteredFields.Checked := False;
    CmdNative.Checked := False;
    Cmd2nd.Checked := False;
    CmdFormatedFields.Checked := False;

    with QrDetail do
    begin
        Close;
        Open;
    end;
end;
end.
