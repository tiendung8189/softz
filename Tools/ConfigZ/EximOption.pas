﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit EximOption;

interface

uses
  Classes, Controls, Forms, Dialogs, StdCtrls, Buttons, ActnList, SysUtils;

type
  TFrmEximOption = class(TForm)
    OpenDlg: TOpenDialog;
    SaveDlg: TSaveDialog;
    TntActionList1: TActionList;
    CmdContinue: TAction;
    CmdClose: TAction;
    GroupBox1: TGroupBox;
    lblTntLabel1: TLabel;
    btnSelectFile: TSpeedButton;
    CkGrid: TCheckBox;
    CkDic: TCheckBox;
    CkFlex: TCheckBox;
    EdFile: TEdit;
    CkFunc: TCheckBox;
    CkRep: TCheckBox;
    CkDbe: TCheckBox;
    CkOther: TCheckBox;
    btContinue: TBitBtn;
    btCancel: TBitBtn;
    CkLCT: TCheckBox;
    CkHotro: TCheckBox;
    CkOther2: TCheckBox;
    CkHotroCT: TCheckBox;
    procedure btnSelectFileClick(Sender: TObject);
    procedure CkGridClick(Sender: TObject);
    procedure CmdContinueExecute(Sender: TObject);
    procedure TntActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure TntFormCreate(Sender: TObject);
    procedure OpenDlgShow(Sender: TObject);
    procedure SaveDlgShow(Sender: TObject);
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure EdFileChange(Sender: TObject);
  private
    mType, mOption: Integer;
  public
    function Execute(pType: Integer; var pFile: String): Integer;
  end;

var
  FrmEximOption: TFrmEximOption;

implementation

{$R *.DFM}

uses
    isLib, ExCommon, isDb, isMsg, NativeXml, MainData, isCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.EdFileChange(Sender: TObject);
var
    Xml: TNativeXml;
    i: Integer;
    sName: string;
begin
    if mType <> 0 then
        Exit;
    if not FileExists(EdFile.Text) then
        Exit;
    if LowerCase(ExtractFileExt(EdFile.Text)) <> '.xml' then
        Exit;

    Xml := DataMain.CreateXml;
    try
        Xml.LoadFromFile(EdFile.Text);

        ckGrid.Checked := False;
        CkDic.Checked := False;
        ckFlex.Checked := False;
        ckFunc.Checked := False;
        ckRep.Checked := False;
        ckDbe.Checked := False;
        CkOther.Checked := False;
        CkOther2.Checked := False;
        CkHotro.Checked := False;
        CkHotroCT.Checked := False;
        CkLCT.Checked := False;
        for i := 0 to Xml.Root.NodeCount - 1 do
        begin
            sName := Xml.Root.Nodes[i].Name;

            if (sName =  'SYS_GRID') then
                ckGrid.Checked := True;
            if (sName =  'SYS_DICTIONARY') then
                CkDic.Checked := True;
            if (sName =  'SYS_FLEXCONFIG') then
                ckFlex.Checked := True;
            if (sName =  'SYS_FUNC') then
                ckFunc.Checked := True;
            if (sName =  'SYS_REPORT') then
                ckRep.Checked := True;
            if (sName =  'SYS_DBE_MSG') or (sName =  'SYS_DBE_OBJ') then
                ckDbe.Checked := True;
            if (sName =  'DM_KHACX') then
                CkOther.Checked := True;
            if (sName =  'DM_KHAC2') then
                CkOther2.Checked := True;
            if (sName =  'DM_HOTRO_NHOM') then
                CkHotro.Checked := True;
            if (sName =  'DM_HOTRO') then
                CkHotroCT.Checked := True;
            if (sName =  'SYS_LCT') then
                CkLCT.Checked := True;
        end;
    finally
        Xml.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmEximOption.Execute(pType: Integer; var pFile: String): Integer;
begin
    ckGrid.Enabled := TableExists('SYS_GRID');
    ckDic.Enabled := TableExists('SYS_DICTIONARY');
    ckFlex.Enabled := FieldExists('SYS_CONFIG', 'FLEXIBLE') or TableExists('SYS_FLEXCONFIG');
    ckFunc.Enabled := TableExists('SYS_FUNC');
    ckRep.Enabled := TableExists('SYS_REPORT');
    ckDbe.Enabled := TableExists('SYS_DBE_MSG') and TableExists('SYS_DBE_OBJ');
    CkOther.Enabled := TableExists('DM_KHACX');
    CkOther2.Enabled := TableExists('DM_KHAC2');
    CkHotro.Enabled := TableExists('DM_HOTRO_NHOM');
    CkHotroCT.Enabled := TableExists('DM_HOTRO');
    CkLCT.Enabled := TableExists('SYS_LCT');

    mType := pType;
    if mType = 0 then
    	Caption := 'Import'
    else
    	Caption := 'Export';
    Caption := Caption;

    mOption :=
        Iif(ckGrid.Checked,     ckGrid.Tag, 0)      +
    	Iif(ckDic.Checked,      ckDic.Tag, 0)       +
        Iif(ckFlex.Checked,     ckFlex.Tag, 0)      +
        Iif(ckFunc.Checked,     ckFunc.Tag, 0)      +
        Iif(ckRep.Checked,      ckRep.Tag, 0)       +
        Iif(ckDbe.Checked,      ckDbe.Tag, 0)       +
        Iif(ckOther.Checked,    ckOther.Tag, 0)     +
        Iif(ckOther2.Checked,    ckOther2.Tag, 0)   +
        Iif(CkHotro.Checked,    CkHotro.Tag, 0)     +
        Iif(CkHotroCT.Checked,    CkHotroCT.Tag, 0) +
        Iif(CkLCT.Checked,      CkLCT.Tag, 0);

    EdFile.Text := sysAppPath + isDatabaseServer + '.xml';

    if ShowModal = mrOk then
    begin
        pFile := EdFile.Text;
        Result := mOption;
    end
    else
        Result := -1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.btnSelectFileClick(Sender: TObject);
begin
    case mType of
    0:
        begin
            if not OpenDlg.Execute then
                Exit;
            EdFile.Text := OpenDlg.FileName;
        end;
    1:
        begin
            if not SaveDlg.Execute then
                Exit;
            EdFile.Text := SaveDlg.FileName;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.CkGridClick(Sender: TObject);
begin
    with Sender as TCheckBox do
        if Checked then
            mOption := mOption + Tag
        else
            mOption := mOption - Tag;
            
    if mOption < 0 then
        mOption := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.CmdContinueExecute(Sender: TObject);
var
    PartF: string;
begin
    PartF := EdFile.Text;
    case mType of
    0:
        if not FileExists(PartF) then
        begin
            ErrMsg('Không tìm thấy file');
            Exit;
        end;
    1:
    begin
        if not DirectoryExists( ExtractFileDir(PartF)) then
        begin
            ErrMsg('Đường dẫn không hợp lệ');
            Exit;
        end;

        if (ExtractFileName(PartF) = '') or (ExtractFileExt(PartF) = '') then
        begin
            ErrMsg('Tên file không hợp lệ');
            Exit;
        end;
    end;
    end;

    if (mOption > 0) and (PartF <> '')  then
        ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.TntActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	CmdContinue.Enabled := (EdFile.Text <> '') and
    	(ckGrid.Checked or ckDic.Checked or ckFlex.Checked
        or ckFunc.Checked or ckRep.Checked or ckDbe.Checked
        or CkOther.Checked or CkOther2.Checked or CkHotro.Checked or CkHotroCT.Checked
        or CkLCT.Checked);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.TntFormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.OpenDlgShow(Sender: TObject);
begin
	OpenDlg.FileName := sysAppPath;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.SaveDlgShow(Sender: TObject);
begin
	SaveDlg.FileName := EdFile.Text
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmEximOption.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

end.
