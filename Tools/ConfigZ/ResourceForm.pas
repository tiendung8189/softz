﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ResourceForm;

interface

uses
  Windows, Classes, Controls, Forms, SysUtils, Dialogs,
  CheckLst, Buttons, StdCtrls, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh,
  DbLookupComboboxEh2, Data.DB, Data.Win.ADODB, GuidEx;
type
  TFrmResourceForm = class(TForm)
    btOk: TBitBtn;
    btCancel: TBitBtn;
    GroupBox1: TGroupBox;
    EdResourceName: TDBEditEh;
    EdResourceValue: TDBMemoEh;
    EdResourceValue_EN: TDBMemoEh;
    CbResourceType: TDbLookupComboboxEh2;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    QrMasterResourceType: TWideStringField;
    QrMasterResourceName: TWideStringField;
    QrMasterResourceValue: TWideStringField;
    QrMasterResourceValue_EN: TWideStringField;
    CbResourceCAT: TDbLookupComboboxEh2;
    QrMasterResourceCAT: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterAfterPost(DataSet: TDataSet);
    procedure QrMasterBeforeOpen(DataSet: TDataSet);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
  private
  public

  end;

var
  FrmResourceForm: TFrmResourceForm;

implementation

{$R *.dfm}

uses
    isLib, isDb, isMsg, Resource;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.FormShow(Sender: TObject);
begin
    OpenDataSets([QrMaster]);
    QrMaster.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_SAVE    = 'Thêm dữ liệu thành công';
procedure TFrmResourceForm.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('ResourceCAT').AsString := 'FUNC';
        FieldByName('ResourceType').AsString := 'Text';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.QrMasterAfterPost(DataSet: TDataSet);
begin
    QrMaster.Append;
    Msg(RS_SAVE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.QrMasterBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(TGuidEx.NewGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['ResourceName', 'ResourceCAT', 'ResourceType']) then
            Abort;

        if (FindField('CREATE_BY') <> nil) and (FindField('UPDATE_BY') <> nil) then
            SetAudit(DataSet);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.btCancelClick(Sender: TObject);
begin
    QrMaster.Cancel;
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.btOkClick(Sender: TObject);
begin
   QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmResourceForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
