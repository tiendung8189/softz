﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Nhaptrabl;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, isDb,
  frameNavi,
  frameKho, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, wwdbedit, Wwdotdot,
  Wwdbcomb;

type
  TFrmNhaptrabl = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label10: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrDMVT: TADOQuery;
    QrCTRSTT: TIntegerField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    GrDetail: TwwDBGrid2;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdDetail: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXCREATE_BY: TIntegerField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_BY: TIntegerField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXIMG: TIntegerField;
    QrNXXOA: TWideStringField;
    QrCTSTT: TIntegerField;
    DBEdit1: TDBMemo;
    QrCTTENVT: TWideStringField;
    Panel2: TPanel;
    Label6: TLabel;
    DBEdit5: TwwDBEdit;
    Label16: TLabel;
    DBEdit14: TwwDBEdit;
    vlTotal: TisTotal;
    Bevel1: TBevel;
    frNavi: TfrNavi;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    QrNXSOLUONG: TFloatField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTLK_DVT: TWideStringField;
    QrNXCHIETKHAU: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTLK_DONGIA: TFloatField;
    QrNXLK_THUNGAN: TWideStringField;
    TntLabel2: TLabel;
    EdSoBill: TwwDBEdit;
    Label9: TLabel;
    CbMAQUAY: TwwDBLookupCombo;
    TntLabel3: TLabel;
    CbMANV: TwwDBLookupCombo;
    CbTENNV: TwwDBLookupCombo;
    QrDMQUAY: TADOQuery;
    CmdUpdateDetail: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    CmdDelDetail: TAction;
    N3: TMenuItem;
    Xachitit1: TMenuItem;
    CmdUpdateQty: TAction;
    N4: TMenuItem;
    Cpnhtchititmthng1: TMenuItem;
    QrUSER: TADOQuery;
    QrNXSCT2: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXTHUNGAN: TWideStringField;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrNXQUAY: TWideStringField;
    QrNXLK_QUAY: TWideStringField;
    DBEdit2: TwwDBEdit;
    QrNXLCT: TWideStringField;
    CmdAudit: TAction;
    QrNXDGIAI: TWideMemoField;
    ImgTotal: TImage;
    DBText2: TDBText;
    QrNXTL_CK: TFloatField;
    TntLabel6: TLabel;
    QrCTGHICHU: TWideStringField;
    QrCTTHUE_SUAT: TFloatField;
    TntLabel1: TLabel;
    DBEdit4: TwwDBEdit;
    QrCTSOTIEN1: TFloatField;
    QrNXSOTIEN1: TFloatField;
    QrCHECKBILL: TADOQuery;
    QrCT2: TADOQuery;
    QrNXDRC_STATUS: TWideStringField;
    CmdListRefesh: TAction;
    frKHO: TfrKHO;
    QrCTTL_CK2: TFloatField;
    QrCTTL_CK3: TFloatField;
    QrCTTL_CK4: TFloatField;
    QrCTTL_CK5: TFloatField;
    QrCTTL_CK6: TFloatField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    LbDrcStatus: TLabel;
    CbDrcStatus: TwwDBComboBox;
    cxGroupBox1: TGroupBox;
    QrNXTHUE: TFloatField;
    QrCTLOAITHUE: TWideStringField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLOC: TWideStringField;
    QrNXKHOA2: TGuidField;
    QrNXMAVIP: TWideStringField;
    QrCTTL_CK7: TFloatField;
    QrCTTL_CK_MAX: TFloatField;
    QrCTTL_CK_THEM: TFloatField;
    QrCTTL_CK_THEM2: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrNXSCT2Validate(Sender: TField);
    procedure CmdUpdateDetailExecute(Sender: TObject);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure CmdUpdateQtyExecute(Sender: TObject);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure QrNXMAKHOChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
  private
    mLCT: String;
	mCanEdit, mReadOnly: Boolean;
   	fTungay, fDenngay: TDateTime;
    fKho: String;
    mKhoa: TGUID;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmNhaptrabl: TFrmNhaptrabl;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    ChonDsma, isLib;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAPTRA_BL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.Execute;
begin
    mLCT := 'NTBL';
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled  := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
//    ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;

    // Initial
    fType := 3;
    fStr := '';
    fSQL := QrNX.SQL.Text;

    mKhoa := TGuidEx.EmptyGuid;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.FormShow(Sender: TObject);
begin
	// Open Database
    OpenDataSets([QrDMVT, QrDMQUAY, DataMain.QrDMTT_NHAPTRABL, DataMain.QrDMKHO]);
    frKHO.Init2(Date - sysLateDay, Date, CmdRefresh);

    CbNGAY.ReadOnly := not sysIsCentral;

    with QrNX do
    begin
    	SetDisplayFormat(QrNX, sysCurFmt);
	    SetDisplayFormat(QrNX, ['SOLUONG'], sysQtyFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG2', 'SOLUONG'], sysQtyFmt);
	    SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);
    end;

    // Customize
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exHideDrc;
    try
	    CloseDataSets([QrDMVT, QrCT, QrNX]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        
        with QrUSER do
        begin
        	Close;
            Open;
        end;

        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
        exShowDrc(DsNX);
        mKhoa := TGuidEx.EmptyGuid;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
        exHideDrc;
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frKHO.edFrom.Date <> fTungay) or
	   (frKHO.edTo.Date   <> fDenngay) or
       (frKHO.CbMaKho.Text <> fKho) then
    begin
		fTungay  := frKHO.EdFrom.Date;
        fDenngay := frKHO.EdTo.Date;
        fKho 	 := frKHO.CbMaKho.Text;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			if fKho <> '' then
            	SQL.Add(' and MAKHO = ''' + fKho + '''');
            //  Loc theo mat hang
            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from TRAHANG_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from TRAHANG_CT a, DM_VT_FULL b where a.KHOA = TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from TRAHANG_CT where KHOA = TRAHANG.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMVT.Requery;
    QrDMQUAY.Requery;
    QrUSER.Requery;
    DataMain.QrDMTT_NHAPTRABL.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;

    // Xoa cac dong thua
	mTrigger := True;
    with QrCT do
    begin
        DisableControls;
        First;
        while not Eof do
            if FieldByName('SOLUONG').AsFloat = 0 then
                Delete
            else
                Next;
        First;
        EnableControls;
    end;
    mTrigger := False;

    // Đánh số lại và lưu
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdTotalExecute(Sender: TObject);
begin
    vlTotal.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrNX);

    CmdDetail.Enabled := not bEmpty;

    CmdPrint.Enabled  := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    //CmdSearch.Enabled := bBrowse and (n = 0);
    CmdFilter.Enabled := bBrowse and (n = 0);
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    mReadOnly := not exCanEditVoucher(QrNX, 'MAKHO', False);
    CmdUpdateDetail.Enabled := mCanEdit and (not mReadOnly);

    bEmpty := QrCT.IsEmpty;
    CmdDelDetail.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);
    CmdUpdateQty.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);

    // State Components
    CbMAQUAY.ReadOnly       := QrNX.FieldByName('SCT2').AsString <> '';
    CbMAQUAY.AllowClearKey  := QrNX.FieldByName('SCT2').AsString = '';
//    CbTENQUAY.ReadOnly      := QrNX.FieldByName('SCT2').AsString <> '';
//    CbTENQUAY.AllowClearKey := QrNX.FieldByName('SCT2').AsString = '';
    EdSoBill.ReadOnly       := not bEmpty;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

	(*
    ** Master events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXAfterInsert(DataSet: TDataSet);
var
	mKho: String;
begin
	mKho := iIf(frKHO.CbMaKho.LookupValue = '', sysDefKho, frKHO.CbMaKho.LookupValue);
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := Now;
		FieldByName('LCT').AsString := mLCT;
        FieldByName('MAKHO').AsString := mKho;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('THUNGAN').AsString := sysLogonUser;
        FieldByName('DRC_STATUS').AsString := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'SCT2', 'THUNGAN']) then
	    	Abort;

        if not sysIsCentral then
            if BlankConfirm(QrNX, ['QUAY']) then
    	    	Abort;

	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;

    if QrCT.IsEmpty then
    begin
        Msg('Chưa nhập chi tiết hàng hóa trả lại.');
        Abort;
    end;

    exDrcValidate(QrNX);
    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
	exCanEditVoucher(QrNX);
	if mTrigger then
    	Exit;
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
        FieldByName('THANHTOAN').AsFloat :=
			FieldByName('SOTIEN').AsFloat -
            FieldByName('CHIETKHAU').AsFloat -
            FieldByName('CHIETKHAU_MH').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXSCT2Validate(Sender: TField);
var
    sct, mavip: String;
    ngay: TDateTime;
    sogio: Double;
begin
    sct := QrNX.FieldByName('SCT2').AsString;
    if sct = '' then
        Exit;

    with QrCHECKBILL do
    begin
        Parameters[0].Value := QrNX.FieldByName('MAKHO').AsString;
//        Parameters[1].Value := QrNX.FieldByName('QUAY').AsString;
        Parameters[1].Value := sct;
        Open;
        if IsEmpty then
        begin
            ErrMsg('Lỗi nhập liệu. Số bill không hơp lệ.');
            Close;
            Abort;
        end;

        sogio := DataMain.GetSysParam('BL_SOGIO_TRAHANG');
        ngay := Fields[3].AsDateTime;

        if (QrNX.FieldByName('NGAY').AsDateTime - ngay) * 24 > sogio then
        begin
            ErrMsg('Đã quá thời gian cho phép trả hàng.');
            Abort;
        end;

        mKhoa := TGuidField(Fields[0]).AsGuid;
        mavip := Fields[1].AsString;
        TGuidField(QrNX.FieldByName('KHOA2')).AsGuid := mKhoa;

        if mavip <> '' then
    		QrNX.FieldByName('MAVIP').AsString := mavip;
        Close;
    end;

    if QrCT.IsEmpty then
    	CmdUpdateDetail.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrNXMAKHOChange(Sender: TField);
var
    s: String;
begin
    s := Trim(QrNX.FieldByName('MAKHO').AsString);

    with QrDMQUAY do
    begin
        if s = '' then
            Filter := ''
        else
            Filter := 'MAKHO = ' + QuotedStr(s);
        First;
    end;
end;

	(*
    ** Detail events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTSOLUONGValidate(Sender: TField);
begin
    with QrCT do
        if FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG2').AsFloat then
        begin
            ErrMsg('Lỗi nhập liệu. Số lượng trả không hợp lệ.');
            Abort;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTSOLUONGChange(Sender: TField);
var
	sotien, ckmh, mTs, mSotien, mCk, mThue: Double;
begin
	with QrCT do
    begin
    	sotien := exVNDRound(
        	FieldByName('SOLUONG').AsFloat *
        	FieldByName('DONGIA').AsFloat);

		ckmh := exVNDRound(
        	sotien * FieldByName('TL_CK').AsFloat / 100);

    	FieldByName('SOTIEN').AsFloat := sotien;
    	FieldByName('CHIETKHAU').AsFloat := ckmh;

		(* Khong tinh CKHD cho cac mat hang co CKMH roi
		** Dung field SOTIEN1 de tinh tong tien tham gia CKHD
        *)

        mTs := FieldByName('THUE_SUAT').AsFloat;
        mCk := exVNDRound(sotien * FieldByName('TL_CK').AsFloat / 100.0);

        mSotien := sotien - mCk;

        // Thue
            if mTs = 0 then
                mThue := 0
            else
                mThue := exVNDRound(mSotien / (100/mTs + 1));

        FieldByName('TIEN_THUE').AsFloat := mThue;
        FieldByName('SOTIEN1').AsFloat := mSotien;
	end;
    vlTotal.Update;
    GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
        
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTBeforeDelete(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTAfterCancel(DataSet: TDataSet);
begin
    vlTotal.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTAfterDelete(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);       
    vlTotal.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdDetailExecute(Sender: TObject);
begin
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdDelDetailExecute(Sender: TObject);
begin
    if not YesNo('Xóa toàn bộ chi tiết mặt hàng. Tiếp tục?') then
        Exit;
    EmptyDataSet(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdUpdateDetailExecute(Sender: TObject);
begin
//    mKhoa := QrNX.FieldByName('KHOA').AsInteger;

    if TGuidEx.IsEmptyGuid(mKhoa) then
    begin
    	QrNXSCT2Validate(QrNX.FieldByName('SCT2'));
    end;

    // Clean up
    EmptyDataSet(QrCT);

    Screen.Cursor := crSqlWait;
    with QrCT2 do
    begin
        Parameters[0].Value := TGuidEx.ToString(mKhoa);
        Open;
        QrCT.DisableControls;
        while not Eof do
        begin
            QrCT.Append;
            QrCT.FieldByName('MAVT').AsString     := FieldByName('MAVT').AsString;
            QrCT.FieldByName('SOLUONG2').AsFloat   := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('DONGIA').AsFloat    := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('TL_CK').AsFloat     := FieldByName('TL_CK').AsFloat;
            QrCT.FieldByName('THUE_SUAT').AsFloat := FieldByName('THUE_SUAT').AsFloat;
            QrCT.FieldByName('TL_CK7').AsFloat 	  := FieldByName('TL_CK7').AsFloat;
            QrCT.FieldByName('TL_CK6').AsFloat 	  := FieldByName('TL_CK6').AsFloat;
            QrCT.FieldByName('TL_CK5').AsFloat 	  := FieldByName('TL_CK5').AsFloat;
            QrCT.FieldByName('TL_CK4').AsFloat 	  := FieldByName('TL_CK4').AsFloat;
            QrCT.FieldByName('TL_CK3').AsFloat 	  := FieldByName('TL_CK3').AsFloat;
            QrCT.FieldByName('TL_CK2').AsFloat 	  := FieldByName('TL_CK2').AsFloat;
            QrCT.FieldByName('TL_CK_MAX').AsFloat 	  := FieldByName('TL_CK_MAX').AsFloat;
            QrCT.FieldByName('TL_CK_THEM').AsFloat 	  := FieldByName('TL_CK_THEM').AsFloat;
            QrCT.FieldByName('TL_CK_THEM2').AsFloat 	  := FieldByName('TL_CK_THEM2').AsFloat;
            QrCT.Post;

            Next;
        end;
        Close;
        QrCT.EnableControls;
    end;
    QrCT.First;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdUpdateQtyExecute(Sender: TObject);
begin
    with QrCT do
    begin
        CheckBrowseMode;
        DisableControls;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG2').AsFloat;
            Post;
            Next;
        end;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
