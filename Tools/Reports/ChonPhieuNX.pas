(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieuNX;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmChonPhieuNX = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNX: TDataSource;
    QrPHIEUNX: TADOQuery;
    QrPHIEUNXNGAY: TDateTimeField;
    QrPHIEUNXSCT: TWideStringField;
    QrPHIEUNXMADT: TWideStringField;
    QrPHIEUNXMAKHO: TWideStringField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbDV: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMaDV: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrPHIEUNXTENKHO: TWideStringField;
    QrPHIEUNXTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrPHIEUNXNG_GIAO: TWideStringField;
    QrPHIEUNXNG_NHAN: TWideStringField;
    QrPHIEUNXTHUE_SUAT: TFloatField;
    QrPHIEUNXHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNXTHANHTOAN: TFloatField;
    QrPHIEUNXDGIAI: TWideMemoField;
    QrPHIEUNXHOADON_SO: TWideStringField;
    QrPHIEUNXHOADON_NGAY: TDateTimeField;
    QrPHIEUNXSOLUONG: TFloatField;
    QrPHIEUNXPHIEUGIAOHANG: TWideStringField;
    QrPHIEUNXKHOA: TGuidField;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVChange(Sender: TObject);
    procedure CbMaDVExit(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
    procedure QrPHIEUNXBeforeOpen(DataSet: TDataSet);
  private
	mMaDV, mKHO, mSQL, mLCT: String;
    mTungay, mDenngay: TDateTime;
    mLoai: Integer; //0: Khach hang; 1: NCC
  public
  	function Execute(pFix: Boolean; pMaDV, pKho: String;
    	pLCT: String = 'NMUA'; pLoai: Integer = 1): TGUID;
  end;

var
  FrmChonPhieuNX: TFrmChonPhieuNX;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieuNX.Execute;
begin
	mMaDV := pMaDV;
    mKho := pKho;
    CbMaDV.Enabled := not pFix;
    CbDV.Enabled := not pFix;
    mLCT := pLCT;
    mLoai := pLoai;

	if ShowModal = mrOK then
        Result := TGuidField(QrPHIEUNX.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNX, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_PHIEUNHAP', GrBrowse);

    SetDisplayFormat(QrPHIEUNX, sysCurFmt);
    SetDisplayFormat(QrPHIEUNX, ['NGAY'], DateTimeFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrPHIEUNX, QrDM_KH_NCC, QrDMKHO]);
	mSQL := QrPHIEUNX.SQL.Text;
    CbMaDV.LookupValue := mMaDV;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mMaDV = '' then
    	try
    		CbMaDV.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mMaDV := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CmdRefreshExecute(Sender: TObject);
var
	s, donvi, Kho: String;
    Tungay, Denngay: TDateTime;
begin
    donvi     := CbMaDV.LookupValue;
    Kho     := CbMAKHO.LookupValue;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mMaDV <> donvi)        or (mKHO <> Kho)         then
	begin
   	    mMaDV := donvi;
        mKHO := Kho;
        mTungay := Tungay;
        mDenngay := Denngay;

        with QrPHIEUNX do
        begin
			Close;

            s := mSQL;
            if mMaDV <> '' then
                s := s + ' and a.MADT=''' + mMaDV + '''';

			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
                
            s := s + ' order by a.NGAY, a.SCT';
            
            SQL.Text := s;
			Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CbMaDVChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbDV.LookupValue := s;
    // Ten NCC
    1:
        CbMaDV.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CbMaDVExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
begin
	QrDM_KH_NCC.Parameters[0].Value := mLoai;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.QrPHIEUNXBeforeOpen(DataSet: TDataSet);
begin
	with QrPHIEUNX do
    begin
    	Parameters[0].Value := mLCT;
        Parameters[1].Value := mTungay;
        Parameters[2].Value := mDenngay;
    end;
end;

end.
