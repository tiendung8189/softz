﻿program POSz;

uses
  Forms,
  MainData in 'MainData.pas' {DataMain: TDataModule},
  ExCommon in 'ExCommon.pas',
  CkMa in 'CkMa.pas' {FrmCkma},
  SoBill in 'SoBill.pas' {FrmSoBill},
  PosCommoInfo in 'PosCommoInfo.pas' {FrmCommoInfo},
  Printer in 'Printer.pas' {FrmPrinter},
  TempReceipt in 'TempReceipt.pas' {FrmTempReceipt},
  ReceiptDesc in 'ReceiptDesc.pas' {FrmReceiptDesc},
  exResStr in 'exResStr.pas',
  PosMain in 'PosMain.pas' {FrmMain},
  PosVIP in 'PosVIP.pas' {FrmPosVIP},
  CkBill in 'CkBill.pas' {FrmChietkhau},
  Retailer in 'Retailer.pas' {FrmRetailer},
  Payment in 'Payment.pas' {FrmPayment},
  CongnoBanle in 'CongnoBanle.pas' {FrmCongnoBanle},
  frameNavi in 'frameNavi.pas' {frNavi: TFrame},
  TheLenh in 'TheLenh.pas' {FrmThelenh},
  CR208U in 'CR208U.pas',
  exCOMPort in 'exCOMPort.pas',
  SystemCriticalU in 'SystemCriticalU.pas',
  ThongtinCN in 'ThongtinCN.pas' {FrmThongtinCN},
  exPrintBill in 'exPrintBill.pas',
  ThongtinCN2 in 'ThongtinCN2.pas' {FrmThongtinCN2},
  AddList in '..\Softz.Admin\AddList.pas' {FrmAddList},
  AdminData in '..\Softz.Admin\AdminData.pas' {Data: TDataModule},
  CustomizeGrid in '..\Softz.Admin\CustomizeGrid.pas' {FrmCustomizeGrid},
  Dictionary in '..\Softz.Admin\Dictionary.pas' {FrmDictionary},
  ExAdminCommon in '..\Softz.Admin\ExAdminCommon.pas',
  FlexEdit in '..\Softz.Admin\FlexEdit.pas' {FrmFlexEdit},
  FunctionAccess in '..\Softz.Admin\FunctionAccess.pas' {FrmFuncAccess},
  FunctionAdmin in '..\Softz.Admin\FunctionAdmin.pas' {FrmFuncAdmin},
  GridOption in '..\Softz.Admin\GridOption.pas' {FrmGridOption},
  Groups in '..\Softz.Admin\Groups.pas' {FrmGroup},
  ListGroup in '..\Softz.Admin\ListGroup.pas' {FrmListGroup},
  ListUser in '..\Softz.Admin\ListUser.pas' {FrmListUser},
  RepConfig in '..\Softz.Admin\RepConfig.pas' {FrmRepConfig},
  ReportAccess in '..\Softz.Admin\ReportAccess.pas' {FrmRepAccess},
  Users in '..\Softz.Admin\Users.pas' {FrmUser},
  crCommon in '..\Softz.Admin\crFrames\crCommon.pas' {CrFrame: TFrame},
  crSAMS_FUNC in '..\Softz.Admin\crFrames\crSAMS_FUNC.pas' {frameSAMS_FUNC: TFrame},
  crSAMS_GROUP in '..\Softz.Admin\crFrames\crSAMS_GROUP.pas' {frameSAMS_GROUP: TFrame},
  crSAMS_REP in '..\Softz.Admin\crFrames\crSAMS_REP.pas' {frameSAMS_REP: TFrame},
  crSAMS_USER in '..\Softz.Admin\crFrames\crSAMS_USER.pas' {frameSAMS_USER: TFrame},
  RepEngine in 'RepEngine.pas' {FrmRep},
  PosChitiet in 'PosChitiet.pas' {FrmPosChitiet},
  exPrintBill2 in 'exPrintBill2.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Bán lẻ';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TFrmAddList, FrmAddList);
  Application.CreateForm(TData, Data);
  Application.CreateForm(TFrmRep, FrmRep);
  if not DataMain.Logon(1) then
  begin
      Application.Terminate;
      Exit;
  end;

  Application.Run;
end.
