object FrmDmLoaiVipCT: TFrmDmLoaiVipCT
  Left = 175
  Top = 122
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Chi'#7871't Kh'#7845'u Theo Lo'#7841'i Th'#7867' VIP'
  ClientHeight = 367
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 346
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 85
    Width = 792
    Height = 261
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'TENMAY;CustomEdit;CbTenmay;F'
      'PRINTER;CustomEdit;CbPrinter;F'
      'TENKHO;CustomEdit;CbKho;F')
    PictureMasks.Strings = (
      'TENMAY'#9'*!'#9'T'#9'T'
      'QUAY'#9'*!'#9'T'#9'T')
    Selected.Strings = (
      'MANHOM'#9'11'#9'M'#227' nh'#243'm'#9'F'
      'LK_TENNHOM'#9'40'#9'T'#234'n nh'#243'm'#9'T'
      'LK_TENNGANH'#9'41'#9'T'#234'n ng'#224'nh'#9'T'
      'TL_CK'#9'13'#9'% Chi'#7871't kh'#7845'u'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDmChietkhau
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton8: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 792
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object LbKHO: TLabel
      Left = 186
      Top = 16
      Width = 45
      Height = 16
      Alignment = taRightJustify
      Caption = 'Lo'#7841'i th'#7867
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbTen: TwwDBLookupCombo
      Left = 296
      Top = 12
      Width = 333
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'DGIAI'#9'35'#9'DGIAI'#9'F'#9
        'MA'#9'5'#9'MA'#9'F'#9)
      LookupTable = QrLOAI_THE_VIP
      LookupField = 'MA'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnNotInList = CbMaNotInList
ButtonEffects.Transparent=True
    end
    object CbMa: TwwDBLookupCombo
      Left = 240
      Top = 12
      Width = 53
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'5'#9'MA'#9'F'
        'DGIAI'#9'35'#9'DGIAI'#9'F')
      LookupTable = QrLOAI_THE_VIP
      LookupField = 'MA'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnNotInList = CbMaNotInList
ButtonEffects.Transparent=True
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 124
    Top = 136
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin     '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdChonNhom: TAction
      Caption = 'Ch'#7885'n nh'#243'm h'#224'ng'
      OnExecute = CmdChonNhomExecute
    end
  end
  object QrNhom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'b.MANHOM, b.TENNHOM, b.MANGANH, c.TENNGANH'
      '  from '#9'DM_NHOM b, DM_NGANH c'
      'where'#9'b.MANGANH = c.MANGANH')
    Left = 157
    Top = 136
    object QrNhomMANHOM: TWideStringField
      FieldName = 'MANHOM'
      Size = 4
    end
    object QrNhomTENNHOM: TWideStringField
      FieldName = 'TENNHOM'
      Size = 100
    end
    object QrNhomMANGANH: TWideStringField
      FieldName = 'MANGANH'
      Size = 2
    end
    object QrNhomTENNGANH: TWideStringField
      FieldName = 'TENNGANH'
      Size = 100
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDmChietkhau
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MA'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'TENMAY'
      'QUAY'
      'TENKHO'
      'PRINTER'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 309
    Top = 222
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 246
    Top = 222
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 278
    Top = 222
    object Chnnhmhng1: TMenuItem
      Action = CmdChonNhom
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
  end
  object QrDmChietkhau: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDmChietkhauBeforeInsert
    AfterInsert = QrDmChietkhauAfterInsert
    BeforePost = QrDmChietkhauBeforePost
    BeforeDelete = QrDmChietkhauBeforeDelete
    OnPostError = QrDmChietkhauPostError
    DataSource = DsLOAI_THE_VIP
    Parameters = <
      item
        Name = 'MA'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_LOAIVIP_CT'
      'where '#9'MA=:MA')
    Left = 56
    Top = 136
    object QrDmChietkhauMA: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MA'
      Size = 15
    end
    object QrDmChietkhauTL_CK: TFloatField
      DisplayLabel = '% C.K'
      FieldName = 'TL_CK'
    end
    object QrDmChietkhauLK_TENNHOM: TWideStringField
      DisplayLabel = 'T'#234'n nh'#243'm'
      FieldKind = fkLookup
      FieldName = 'LK_TENNHOM'
      LookupDataSet = QrNhom
      LookupKeyFields = 'MANHOM'
      LookupResultField = 'TENNHOM'
      KeyFields = 'MANHOM'
      Lookup = True
    end
    object QrDmChietkhauLK_TENNGANH: TWideStringField
      DisplayLabel = 'T'#234'n ng'#224'nh'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'LK_TENNGANH'
      LookupDataSet = QrNhom
      LookupKeyFields = 'MANHOM'
      LookupResultField = 'TENNGANH'
      KeyFields = 'MANHOM'
      Lookup = True
    end
    object QrDmChietkhauCREATE_BY: TWideStringField
      FieldName = 'CREATE_BY'
      Visible = False
      Size = 15
    end
    object QrDmChietkhauUPDATE_BY: TWideStringField
      FieldName = 'UPDATE_BY'
      Visible = False
      Size = 15
    end
    object QrDmChietkhauCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDmChietkhauUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDmChietkhauMANHOM: TWideStringField
      DisplayLabel = 'M'#227' nh'#243'm'
      FieldName = 'MANHOM'
      OnChange = QrDmChietkhauMANHOMChange
      Size = 4
    end
  end
  object DsDmChietkhau: TDataSource
    DataSet = QrDmChietkhau
    Left = 56
    Top = 168
  end
  object QrLOAI_THE_VIP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrLOAI_THE_VIPBeforeOpen
    Parameters = <
      item
        Name = 'LOAI'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_LOAIVIP'
      'where LOAI = :LOAI')
    Left = 88
    Top = 136
  end
  object DsLOAI_THE_VIP: TDataSource
    DataSet = QrLOAI_THE_VIP
    Left = 88
    Top = 168
  end
  object QrTemp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'b.MANHOM, b.TENNHOM, b.MANGANH, c.TENNGANH'
      '  from '#9'DM_NHOM b, DM_NGANH c'
      'where'#9'b.MANGANH = c.MANGANH')
    Left = 368
    Top = 147
  end
end
