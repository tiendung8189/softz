﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Tonkho;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin;

type
  TFrmTonkho = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    Panel1: TPanel;
    Status: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    QrTONKHO: TADOQuery;
    DsTONKHO: TDataSource;
    QrDMVT: TADOQuery;
    QrTONKHOKY: TIntegerField;
    QrTONKHONAM: TIntegerField;
    QrTONKHOMAVT: TWideStringField;
    QrTONKHOTENVT: TWideStringField;
    QrTONKHODVT: TWideStringField;
    CmdRefresh: TAction;
    Label3: TLabel;
    QrDMVTMAVT: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTTENNHOM: TWideStringField;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    CbMA: TwwDBLookupCombo;
    CbTEN: TwwDBLookupCombo;
    EdKy: TComboBox;
    EdNam: TComboBox;
    QrDMVTMANHOM: TWideStringField;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    QrTONKHOMAKHO: TWideStringField;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    QrTONKHOLG_DAU: TFloatField;
    QrTONKHOTIEN_DAU: TFloatField;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTGIANHAPVAT: TFloatField;
    QrTONKHOGIANHAP: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    QrDMVTMADT: TWideStringField;
    QrDMVTTENTAT: TWideStringField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTGIASI: TFloatField;
    QrDMVTGIAVON_TK: TFloatField;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTTENLT: TWideStringField;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    TntToolButton4: TToolButton;
    QrTONKHORSTT: TIntegerField;
    QrTONKHOLG_NHAP: TFloatField;
    QrTONKHOTIEN_NHAP: TFloatField;
    QrTONKHOLG_NHAPTRA: TFloatField;
    QrTONKHOTIEN_NHAPTRA: TFloatField;
    QrTONKHOLG_NHAPTRA_LE: TFloatField;
    QrTONKHOTIEN_NHAPTRA_LE: TFloatField;
    QrTONKHOLG_NHAPKHAC: TFloatField;
    QrTONKHOTIEN_NHAPKHAC: TFloatField;
    QrTONKHOLG_NHAPVC: TFloatField;
    QrTONKHOTIEN_NHAPVC: TFloatField;
    QrTONKHOLG_NHAPDC: TFloatField;
    QrTONKHOTIEN_NHAPDC: TFloatField;
    QrTONKHOLG_XUAT: TFloatField;
    QrTONKHOTIEN_XUAT: TFloatField;
    QrTONKHOLG_BANLE: TFloatField;
    QrTONKHOTIEN_BANLE: TFloatField;
    QrTONKHOLG_XUATTRA: TFloatField;
    QrTONKHOTIEN_XUATTRA: TFloatField;
    QrTONKHOLG_XUATKHAC: TFloatField;
    QrTONKHOTIEN_XUATKHAC: TFloatField;
    QrTONKHOLG_XUATVC: TFloatField;
    QrTONKHOTIEN_XUATVC: TFloatField;
    QrTONKHOLG_XUATDC: TFloatField;
    QrTONKHOTIEN_XUATDC: TFloatField;
    QrTONKHOLG_CUOI: TFloatField;
    QrTONKHOTIEN_CUOI: TFloatField;
    QrTONKHOKHOA: TGuidField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTVAT_VAO: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrTONKHOBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrTONKHOBeforePost(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrTONKHOPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrTONKHOBeforeDelete(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CbMAChange(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTONKHOLG_DAUChange(Sender: TField);
    procedure QrTONKHOTIEN_DAUChange(Sender: TField);
    procedure QrTONKHOBeforeEdit(DataSet: TDataSet);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrTONKHOCalcFields(DataSet: TDataSet);
  private
  	mCanEdit, mBookClosed: Boolean;
  	mKy, mNam: Integer;
    mKho: String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmTonkho: TFrmTonkho;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'TONKHO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.Execute;
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

	isMonthList(EdKy);
    isYearList(EdNam);
  	mTrigger := False;

    SetDisplayFormat(QrTONKHO, sysQtyFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrTONKHO, FORM_CODE, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOBeforeOpen(DataSet: TDataSet);
begin
    with QrTONKHO do
    begin
        Parameters[0].Value := mKy;
        Parameters[1].Value := mNam;
        Parameters[2].Value := mKho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DATA_ARISE = 'Có dữ liệu tồn kho trước tháng bắt đầu sử dụng chương trình.' +
            	    #13 + 'Xin liên hệ với người quản trị hệ thống để hiệu chỉnh.';

procedure TFrmTonkho.FormShow(Sender: TObject);
begin
	// Open database
    OpenDataSets([QrDMVT, DataMain.QrDMKHO]);
	CbMA.LookupValue := sysDefKho;

    // Start to use checking
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;

		SQL.Text := Format('select 1 from TONKHO where KY+12*NAM<%d',
        	[sysBegMon + 12 * sysBegYear]);
        Open;

        if not IsEmpty then
            Msg(RS_DATA_ARISE);

        Close;
        Free;
    end;
    DataMain.CalcStock(Date);
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MAVT']) then
    		Abort;
    	FieldByName('KY').AsInteger := mKy;
    	FieldByName('NAM').AsInteger := mNam;
    	FieldByName('MAKHO').AsString := mKho;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_MONTH = 'Tháng làm việc không hợp lệ. Tháng bắt đầu sử dụng chương trình là %d/%d.';

procedure TFrmTonkho.CmdRefreshExecute(Sender: TObject);
var
    s: String;
	k, n: Integer;
    b: Boolean;
    mDate: TDateTime;
begin
    k := StrToInt(EdKy.Text);
    n := StrToInt(EdNam.Text);

	if (mKy <> k) or (mNam <> n) or (CbMA.LookupValue <> mKho) then
    begin
    	mKy := k;
        mNam := n;
        mKho := CbMA.LookupValue;

    	with QrTONKHO do
        begin
        	s := Sort;
        	Close;
            Open;
            if s <> '' then
            	Sort := s;
        end;

        // Check valid month
        b := mKy + 12 * mNam >= sysBegMon + 12 * sysBegYear;

        if not b then
            Msg(Format(RS_INVALID_MONTH, [sysBegMon, sysBegYear]));
            
        // Check BookClose.
		mDate := EncodeDate(mNam, mKy, 1);
    	mBookClosed := mDate <= sysCloseHH;

		DsTONKHO.AutoEdit := b;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOBeforeDelete(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdSaveExecute(Sender: TObject);
begin
	QrTONKHO.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdCancelExecute(Sender: TObject);
begin
	QrTONKHO.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrTONKHO do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not QrTONKHO.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsTONKHO);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CbMAChange(Sender: TObject);
var
	s : String;
begin
	if mTrigger then
    	Exit;

    mTrigger := True;

	if (Sender as TwwDbLookupCombo).Text = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    0:		// Ma
	    CbTEN.LookupValue := s;
    1:		// Ten
	    CbMA.LookupValue := s;
    end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrTONKHO, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOLG_DAUChange(Sender: TField);
begin
  	if mTrigger then
    	Exit;
  	mTrigger := True;

	with QrTONKHO do
    	FieldByName('TIEN_DAU').AsFloat :=
	    	FieldByName('DG_DAU').AsFloat *
    		FieldByName('LG_DAU').AsFloat;

  	mTrigger := False;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOTIEN_DAUChange(Sender: TField);
begin
  	if mTrigger then
    	Exit;
  	mTrigger := True;

	with QrTONKHO do
		if FieldByName('LG_DAU').AsFloat = 0.0 then
        else
	    	FieldByName('DG_DAU').AsFloat :=
		    	FieldByName('TIEN_DAU').AsFloat /
    			FieldByName('LG_DAU').AsFloat;

  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdReReadExecute(Sender: TObject);
begin
	mKy := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Xác nhận tính tồn kho đến ngày %s. Tiếp tục?';

procedure TFrmTonkho.CmdCalcExecute(Sender: TObject);
var
    mDate: TDateTime;
begin
    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Exit;
    end;

    // Make sure the params are correct
    CmdRefresh.Execute;
    mDate := Date;

    // Confirm
	if not YesNo(Format(RS_CONFIRM, [DateToStr(mDate)]), 1) then
    	Exit;

    //Calc
    DataMain.CalcStock(mDate);

    //
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTONKHO);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, mKy, mNam, mKho]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrTONKHOCalcFields(DataSet: TDataSet);
begin
   with QrTONKHO do
   	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

end.
