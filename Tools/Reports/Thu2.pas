﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Thu2;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  wwidlg, AdvMenus, AppEvnts, isPanel, wwfltdlg, Wwdbgrid, frameNgay, frameNavi,
  isDb, wwDialog, Grids, ToolWin, Buttons;

type
  TFrmThu2 = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    QrTCLK_TENDT: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCLK_TENKHO: TWideStringField;
    Bevel1: TBevel;
    PaMaster: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    LbDV: TLabel;
    Label10: TLabel;
    Label25: TLabel;
    Label18: TLabel;
    Label31: TLabel;
    DBText1: TDBText;
    EdSCT: TwwDBEdit;
    CbPTTT: TwwDBLookupCombo;
    CbNgay: TwwDBDateTimePicker;
    EdSOTIEN: TwwDBEdit;
    CbMakho: TwwDBLookupCombo;
    CbTenkho: TwwDBLookupCombo;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    EdTenDT: TwwDBEdit;
    EdMaDT: TwwDBEdit;
    QrTCLK_PTTT: TWideStringField;
    QrCHITIET: TADOQuery;
    DsCT: TDataSource;
    CmdSwitch: TAction;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrCHITIETCALC_STT: TIntegerField;
    QrCHITIETLK_PTTT: TWideStringField;
    QrNX: TADOQuery;
    QrCHITIETSODU: TFloatField;
    QrCHITIETSOTIEN: TFloatField;
    QrCHITIETLK_NGAY: TDateTimeField;
    QrCHITIETLK_SCT: TWideStringField;
    QrCHITIETLK_THANHTOAN: TFloatField;
    QrCHITIETCALC_CONLAI: TFloatField;
    QrCHITIETLK_HOADON_SO: TWideStringField;
    QrCHITIETLK_HOADON_SERI: TWideStringField;
    QrCHITIETLK_HOADON_NGAY: TDateTimeField;
    CmdAudit: TAction;
    PaTra: TisPanel;
    GrNhaptra: TwwDBGrid2;
    PopNT: TAdvPopupMenu;
    QrCHITIET2: TADOQuery;
    DsCT2: TDataSource;
    QrCHITIET2CALC_STT: TIntegerField;
    CmdChonCT2: TAction;
    Chnphiunhptr1: TMenuItem;
    QrTRAHANG2: TADOQuery;
    TntLabel1: TLabel;
    wwDBEdit1: TwwDBEdit;
    TntLabel2: TLabel;
    wwDBEdit2: TwwDBEdit;
    QrTCCALC_SOTIEN: TFloatField;
    QrTRAHANG: TADOQuery;
    QrCHITIET2LK_NGAY: TDateTimeField;
    QrCHITIET2LK_SCT: TWideStringField;
    QrCHITIET2LK_THANHTOAN: TFloatField;
    QrCHITIET2LK_DGIAI: TWideStringField;
    PhieuCT2: TwwSearchDialog;
    QrNX2: TADOStoredProc;
    QrCHITIETLK_DGIAI: TWideStringField;
    CmdChonCT: TAction;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    PopNX: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    QrTRAHANG2NGAY: TDateTimeField;
    QrTRAHANG2SCT: TWideStringField;
    QrTRAHANG2THANHTOAN: TFloatField;
    QrTRAHANG2DGIAI: TWideMemoField;
    CmdTotal: TAction;
    vlTotal1: TisTotal;
    QrTCCALC_CONLAI: TFloatField;
    QrCHITIET2SOTIEN: TFloatField;
    vlTotal2: TisTotal;
    CmdEmpty: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    N3: TMenuItem;
    Xachitit2: TMenuItem;
    edMaKH: TwwDBEdit;
    QrTCLCT: TWideStringField;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMAKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    QrTCMST: TWideStringField;
    QrTCNGUOI: TWideStringField;
    QrTCSOTIEN: TFloatField;
    QrTCSOTIEN2: TFloatField;
    QrTCDGIAI: TWideMemoField;
    QrTCSODU: TFloatField;
    QrTCTHANHTOAN: TFloatField;
    QrTCLYDO: TWideStringField;
    QrTCSOTIEN3: TFloatField;
    QrTCMADT: TWideStringField;
    QrCHITIETNGAYTT: TDateTimeField;
    QrCHITIETSCT2: TWideStringField;
    QrCHITIETGHICHU: TWideStringField;
    QrCHITIETLK_TC_SOTIEN: TFloatField;
    QrCHITIETTHANHTOAN_NX: TFloatField;
    QrCHITIETTC_SOTIEN_NX: TFloatField;
    QrCHITIETLK_PHIEUGIAOHANG: TWideStringField;
    QrTCKHOA: TGuidField;
    QrCHITIETKHOACT: TGuidField;
    QrCHITIETKHOA: TGuidField;
    QrCHITIETKHOANX: TGuidField;
    QrCHITIET2KHOACT: TGuidField;
    QrCHITIET2KHOA: TGuidField;
    QrCHITIET2KHOANX: TGuidField;
    QrTRAHANG2KHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrTCDRC_STATUS: TWideStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    QrTCMATK: TWideStringField;
    PaTK: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    CbSTK: TwwDBLookupCombo;
    CbTenTK: TwwDBLookupCombo;
    EdNganhang: TwwDBEdit;
    EdChinhanh: TwwDBEdit;
    DBMemo1: TDBMemo;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTRAHANG2HOADON_SERI: TWideStringField;
    QrTRAHANG2HOADON_SO: TWideStringField;
    QrTRAHANG2HOADON_NGAY: TDateTimeField;
    QrCHITIET2LK_HOADON_SERI: TWideStringField;
    QrCHITIET2LK_HOADON_SO: TWideStringField;
    QrCHITIET2LK_HOADON_NGAY: TDateTimeField;
    Label6: TLabel;
    wwDBEdit3: TwwDBEdit;
    QrCHITIETLOC: TWideStringField;
    QrCHITIET2LOC: TWideStringField;
    BtCongno: TSpeedButton;
    QrTCLK_MANH: TWideStringField;
    QrTCLK_MACN: TWideStringField;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMakhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCMADTChange(Sender: TField);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure QrCHITIETBeforeOpen(DataSet: TDataSet);
    procedure QrCHITIETBeforeEdit(DataSet: TDataSet);
    procedure QrCHITIETCalcFields(DataSet: TDataSet);
    procedure QrTCMADTValidate(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdChonCT2Execute(Sender: TObject);
    procedure QrCHITIET2CalcFields(DataSet: TDataSet);
    procedure QrCHITIET2BeforeOpen(DataSet: TDataSet);
    procedure QrCHITIET2BeforeDelete(DataSet: TDataSet);
    procedure PhieuCT2InitDialog(Dialog: TwwLookupDlg);
    procedure CmdChonCTExecute(Sender: TObject);
    procedure QrCHITIETSOTIENValidate(Sender: TField);
    procedure QrNX2AfterOpen(DataSet: TDataSet);
    procedure CmdTotalExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrCHITIETAfterCancel(DataSet: TDataSet);
    procedure QrCHITIETAfterDelete(DataSet: TDataSet);
    procedure QrCHITIETAfterEdit(DataSet: TDataSet);
    procedure QrCHITIETBeforePost(DataSet: TDataSet);
    procedure QrCHITIETBeforeDelete(DataSet: TDataSet);
    procedure QrCHITIETSOTIENChange(Sender: TField);
    procedure QrCHITIETSODUChange(Sender: TField);
    procedure QrCHITIET2KHOANXChange(Sender: TField);
    procedure QrCHITIET2AfterCancel(DataSet: TDataSet);
    procedure QrCHITIET2AfterDelete(DataSet: TDataSet);
    procedure QrCHITIET2AfterEdit(DataSet: TDataSet);
    procedure QrCHITIET2SOTIENChange(Sender: TField);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure CbMakhoBeforeDropDown(Sender: TObject);
    procedure CbMakhoCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure BtCongnoClick(Sender: TObject);
    procedure CbSTKBeforeDropDown(Sender: TObject);
  private
    mPTTT, mLCT, mKho: String;
	mCanEdit, mFuncTK: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;

    procedure OpenDetail;
    function  Subtract: Boolean;
    procedure AllowEditSomeFields;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmThu2: TFrmThu2;

implementation

uses
	isMsg,  ExCommon, MainData, RepEngine, Rights, Variants, Wwstr, isLib, ChonDsPX,
    isCommon, GuidEx, CongnoKH;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    mLCT := 'THU2';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE: String = 'PHIEU_THU2';

procedure TFrmThu2.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetDisplayFormat(QrCHITIET, sysCurFmt);
    SetShortDateFormat(QrCHITIET);
    SetDisplayFormat(QrCHITIET, ['LK_NGAY'], DateTimeFmt);

    SetDisplayFormat(QrCHITIET2, sysCurFmt);
    SetShortDateFormat(QrCHITIET2);
    SetDisplayFormat(QrCHITIET2, ['LK_NGAY'], DateTimeFmt);

    SetDisplayFormat(QrTRAHANG2, sysCurFmt);
    SetShortDateFormat(QrTRAHANG2);
    SetDisplayFormat(QrTRAHANG2, ['NGAY'], DateTimeFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_TH', FORM_CODE + '_CT'], [GrBrowse, GrNhaptra, GrDetail]);
    SetDictionary([QrTC, QrCHITIET, QrCHITIET2], [FORM_CODE, FORM_CODE + '_TH', FORM_CODE + '_CT'], [Filter, nil, nil]);

    mFuncTK := DataMain.GetFuncState('SZ_TAIKHOAN');

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
    end;

    PaTra.Collapsed := True;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.FormShow(Sender: TObject);
begin
    with DataMain do
    begin
        OpenDataSets([QrDMKH, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

	mPTTT := DataMain.GetSysParam('DEFAULT_PTTT');
    mKho := RegReadString(Name, 'Warehouse', sysDefKho);
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;

	CloseDataSets(DataMain.Conn);
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.OpenDetail;
begin
    Screen.Cursor := crSQLWait;

    // Reopen
    with QrCHITIET do
    begin
        Close;
        Open;
    end;
    with QrCHITIET2 do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.PgMainChange(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
    begin
        OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
            GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
    if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
	        if s <> '' then
    	        Sort := s

        end;
        RefreshAudit;
	    AllowEditSomeFields;
        if PgMain.ActivePageIndex = 0 then
            GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdNewExecute(Sender: TObject);
begin
	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Chứng từ sẽ không thể chỉnh sửa sau khi lưu. Tiếp tục?';

procedure TFrmThu2.CmdSaveExecute(Sender: TObject);
begin
	{
    // Strongly confirm
    if not YesNo(RS_CONFIRM, 1) then
        Exit;
	}

    CmdTotal.Execute;
    if not Subtract then
        Exit;

	QrCHITIET.CheckBrowseMode;
	QrCHITIET2.CheckBrowseMode;

    // Delete meanless rows
    with QrCHITIET do
    begin
        DisableControls;
        First;
        DeleteConfirm(False);
        while not Eof do
        begin
            if FieldByName('SOTIEN').AsFloat = 0.0 then
                Delete
            else
                Next;
        end;
        DeleteConfirm(True);
        CheckBrowseMode;

        First;
        EnableControls;
    end;
    // Saving
	QrTC.Post;

    QrCHITIET.UpdateBatch;
    QrCHITIET2.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    QrCHITIET.CancelBatch;
    QrCHITIET2.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM1 = 'Phiếu bị xóa sẽ không phục hồi được. Tiếp tục?';

procedure TFrmThu2.CmdDelExecute(Sender: TObject);
var
	khoa: String;
begin
    with QrTC do
    begin
    	exValidClosing(FieldByName('NGAY').AsDateTime);
        if not YesNo(RS_CONFIRM1, 1) then
            Exit;
    end;

	khoa := TGuidEx.ToString(QrTC.FieldByName('KHOA'));
    DataMain.Conn.Execute(Format('delete THUCHI_CT where KHOA=%s', [QuotedStr(khoa)]));
    DataMain.Conn.Execute(Format('delete THUCHI_TRAHANG where KHOA=%s', [QuotedStr(khoa)]));
    DataMain.Conn.Execute(Format('update THUCHI set SODU=0, SOTIEN2=0, SOTIEN3=0, SOTIEN=0, THANHTOAN=0 where KHOA=%s', [QuotedStr(khoa)]));

    mTrigger := True;
    MarkDataSet(QrTC);
    mTrigger := False;
    OpenDetail;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdEmptyExecute(Sender: TObject);
begin
    exEmptyDetails(QrCHITIET, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdPrintExecute(Sender: TObject);
begin
    CmdSave.Execute;
    ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrDetail then
        try
            CbNgay.SetFocus;
        except
        end
    else
        GrDetail.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdTotalExecute(Sender: TObject);
begin
    vlTotal2.Sum;
    vlTotal1.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsTC)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bDeleted, bInsert, bEmpty: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := (not bBrowse) and (not QrCHITIET.IsEmpty);
    CmdCancel.Enabled := not bBrowse;

    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and
        (n = 1) and not bDeleted and exCheckLoc(QrTC, False);

    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdSwitch.Enabled := n = 1;
    CmdReRead.Enabled := bBrowse;
    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdTotal.Enabled := (n = 1) and not bDeleted;

    CmdChonCT2.Enabled := mCanEdit and not bDeleted;
    CmdChonCT.Enabled := mCanEdit and not bDeleted;
    CmdEmpty.Enabled := mCanEdit and bInsert;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCAfterInsert(DataSet: TDataSet);
begin
    AllowEditSomeFields;

	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('NGAY').AsDateTime  := Now;
        FieldByName('LCT').AsString     := mLCT;
        FieldByName('MAKHO').AsString   := mKho;
        FieldByName('LOC').AsString     := sysLoc;
		FieldByName('PTTT').AsString    := mPTTT;
        FieldByName('DRC_STATUS').AsString    := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_MSG = 'Số tiền không hợp lệ.';
    RS_MSG3 = 'Số tiền hỗ trợ không hợp lệ.';

procedure TFrmThu2.QrTCBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
        
	// Required fields validate
	if BlankConfirm(QrTC, ['NGAY', 'MADT', 'MAKHO']) then
    	Abort;

    // Closed validate
    exValidClosing(QrTC.FieldByName('NGAY').AsDateTime);

    // Others validate
    with QrTC do
    begin
        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

    	if (FieldByName('SOTIEN').AsFloat < 0) then
        begin
        	ErrMsg(RS_MSG);
            EdSOTIEN.SetFocus;
        	Abort;
        end;

        if FieldByName('SOTIEN3').AsFloat < 0 then
        begin
        	ErrMsg(RS_MSG3);
            EdSOTIEN.SetFocus;
        	Abort;
        end;
    end;

    // Alloc receipt no.
    DataMain.AllocSCT(mLCT, QrTC);

	// Passed constrainsts
    SetNull(QrTC, ['PTTT', 'LYDO']);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
	if mTrigger then
        Exit;

    exIsChecked(QrTC);

    if not mCanEdit then
        Abort;

    if QrTC.FieldByName('DELETE_BY').AsInteger <> 0 then
        Abort;
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
    with QrTC do
    begin
//        FieldByName('CALC_SOTIEN').AsFloat :=
//            FieldByName('SOTIEN2').AsFloat +
//            FieldByName('SOTIEN3').AsFloat +
//            FieldByName('SOTIEN').AsFloat;

//        FieldByName('CALC_CONLAI').AsFloat :=
//           FieldByName('SODU').AsFloat - FieldByName('THANHTOAN').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCAfterPost(DataSet: TDataSet);
begin
    mKho := QrTC.FieldByName('MAKHO').AsString;
    AllowEditSomeFields;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCMADTValidate(Sender: TField);
begin
    if QrTC.State in [dsInsert] then
    begin
        if not QrCHITIET2.IsEmpty then
            Abort;
    end
    else
        Abort;
end;

procedure TFrmThu2.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('THANHTOAN').AsFloat :=
            FieldByName('SOTIEN').AsFloat -
            FieldByName('SOTIEN2').AsFloat -
            FieldByName('SOTIEN3').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrTCMADTChange(Sender: TField);
begin
    exDotMakh(DataMain.QrDMKH, Sender);
    EdTenDT.Text := EdTenDT.Field.AsString;
    OpenDetail;
end;

(*==============================================================================
**  Details
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETBeforeOpen(DataSet: TDataSet);
var
    khoa: TGUID;
begin
    with QrNX do
        if Active then
            Close;

    khoa := TGuidField(QrTC.FieldByName('KHOA')).AsGuid;
    with QrCHITIET do
        Parameters[0].Value := TGuidEx.ToString(khoa);

    with QrNX do
    begin
        Parameters[0].Value := TGuidEx.ToString(khoa);
        Parameters[1].Value := QrTC.FieldByName('MADT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETBeforePost(DataSet: TDataSet);
begin
    with DataSet do
        if State in [dsInsert] then
        begin
            TGuidField(FieldByName('KHOA')).AsGuid := TGuidField(QrTC.FieldByName('KHOA')).AsGuid;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETBeforeDelete(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

//    if not (QrTC.State in [dsInsert]) then
//        Abort;

    if not DeleteConfirm then
        Abort;

    SetEditState(QrTC);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    SetEditState(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETAfterCancel(DataSet: TDataSet);
begin
    vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETCalcFields(DataSet: TDataSet);
begin
    with QrCHITIET do
    begin
        FieldByName('CALC_STT').AsInteger := Abs(RecNo);
        FieldByName('CALC_CONLAI').AsFloat :=
            FieldByName('SODU').AsFloat -
            FieldByName('SOTIEN').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETSODUChange(Sender: TField);
begin
    vlTotal1.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIETSOTIENValidate(Sender: TField);
begin
        with QrCHITIET do
        if FieldByName('SOTIEN').AsFloat > FieldByName('SODU').AsFloat then
        begin
            InvalidMsg(Sender);
            Abort;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrNX2AfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrNX2, sysCurFmt);
    SetShortDateFormat(QrNX2);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.BtCongnoClick(Sender: TObject);
begin
    if DataMain.GetRights('SZ_CONGNO_KH') = R_DENY then
        Exit;

    with QrTC do
    begin
        Application.CreateForm(TFrmCongnoKH, FrmCongnoKH);
        FrmCongnoKH.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrTC do
    begin
        ColumnByName('SODU').FooterValue := FormatFloat(sysCurFmt, FieldByName('SODU').AsFloat);
        ColumnByName('SOTIEN').FooterValue := FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('CALC_CONLAI').FooterValue := FormatFloat(sysCurFmt, FieldByName('CALC_CONLAI').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CbMakhoBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CbMakhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CbMakhoNotInList(Sender: TObject; LookupTable: TDataSet;
    NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_ERROR    = 'Số tiền không hợp lệ.';
    RS_INVALID_RECEIPT = 'Phiếu không hợp lệ.';

function TFrmThu2.Subtract;
begin
    Result := QrTC.FieldByName('THANHTOAN').AsFloat > 0.0;

    if not Result then
        ErrMsg(RS_INVALID_RECEIPT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdAuditExecute(Sender: TObject);
begin
    ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_TRAHANG =
        'KHOA'#9'10'#9''#13 +
        'NGAY'#9'10'#9'Ngày'#13 +
        'SCT'#9'18'#9'Số phiếu'#13 +
        'THANHTOAN'#9'13'#9'Số tiền'#13 +
        'DGIAI'#9'40'#9'Diễn giải';

procedure TFrmThu2.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdChonCT2Execute(Sender: TObject);
var
	khoa: TGUID;
begin
    with QrTRAHANG2 do
    begin
        Close;
        Parameters[0].Value := QrTC.FieldByName('MADT').AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;
    end;

    if PhieuCT2.Execute then
    begin
    	khoa := TGuidField(QrTRAHANG2.FieldByName('KHOA')).AsGuid;

        with QrCHITIET2 do
        	if Locate('KHOANX', TGuidEx.ToString(khoa), []) then		// Duplicate
            	Exit
            else
            begin
		        SetEditState(QrTC);
	            Append;
    	        TGuidField(FieldByName('KHOANX')).AsGuid := khoa;
        	    Post;
	        end;
    end;
    QrTRAHANG2.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CmdChonCTExecute(Sender: TObject);
var
	khoa: TGUID;
    procedure AppendDetail(pKhoa: TGUID);
    var
        mSodu: Double;
    begin
        with QrCHITIET do
        begin
            if Locate('KHOANX', TGuidEx.ToString(pkhoa), []) then		// Duplicate
            begin
                if FieldByName('SOTIEN').OldValue = null then
                    mSodu := 0
                else
                    mSodu := FieldByName('SOTIEN').OldValue;

                mSodu := mSodu + QrNX2.FieldByName('SODU').AsFloat;
                if (mSodu <> FieldByName('SOTIEN').AsFloat) and (mSodu <= FieldByName('SODU').AsFloat) then
                begin
                    SetEditState(QrCHITIET);
                    FieldByName('SOTIEN').AsFloat := mSodu;
                    Post;
                end;
            end
            else
            begin
                mSodu := QrNX2.FieldByName('SODU').AsFloat;
                Append;
                TGuidField(FieldByName('KHOANX')).AsGuid := pkhoa;
                FieldByName('NGAYTT').AsDateTime := QrNX2.FieldByName('NGAYTT').AsDateTime;
                FieldByName('SODU').AsFloat := mSodu;
                FieldByName('SOTIEN').AsFloat := mSodu;
                FieldByName('THANHTOAN_NX').AsFloat := FieldByName('LK_THANHTOAN').AsFloat;
                FieldByName('TC_SOTIEN_NX').AsFloat := FieldByName('LK_TC_SOTIEN').AsFloat;
                Post;
            end;
        end;
    end;
begin
    if BlankConfirm(QrTC, ['MADT']) then
        Exit;

    Application.CreateForm(TFrmChonDsPX, FrmChonDsPX);
    if not FrmChonDsPX.Execute(QrTC.FieldByName('MADT').AsString) then
        Exit;

    with QrNX2 do
    begin
        Filter := 'SELECTED=1';
            Filtered := True;
            if RecordCount > 0 then
            begin
                SetEditState(QrTC);
                First;
                while not eof do
                begin
                    khoa := TGuidField(FieldByName('KHOA')).AsGuid;
                    AppendDetail(khoa);
                    Next;
                end;
            end;
            Filter := '';
    end;

    QrNX2.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2CalcFields(DataSet: TDataSet);
begin
    with DataSet do
        FieldByName('CALC_STT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2KHOANXChange(Sender: TField);
begin
    with QrCHITIET2 do
        FieldByName('SOTIEN').AsFloat := FieldByName('LK_THANHTOAN').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2SOTIENChange(Sender: TField);
begin
    vlTotal2.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2BeforeOpen(DataSet: TDataSet);
var
    khoa: TGUID;
begin
    with QrTRAHANG do
        if Active then
            Close;

    khoa := TGuidField(QrTC.FieldByName('KHOA')).AsGuid;
    with QrCHITIET2 do
        Parameters[0].Value := TGuidEx.ToString(khoa);

    with QrTRAHANG do
    begin
        Parameters[0].Value := TGuidEx.ToString(khoa);
        Parameters[1].Value := QrTC.FieldByName('MADT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2AfterCancel(DataSet: TDataSet);
begin
    vlTotal2.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2AfterDelete(DataSet: TDataSet);
begin
    vlTotal2.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2AfterEdit(DataSet: TDataSet);
begin
    vlTotal2.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.QrCHITIET2BeforeDelete(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    if not (QrTC.State in [dsInsert]) then
        Abort;

    if not DeleteConfirm then
        Abort;

    SetEditState(QrTC);
    vlTotal2.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.PhieuCT2InitDialog(Dialog: TwwLookupDlg);
begin
    InitSearchDialog(Dialog);
    Dialog.wwIncrementalSearch1.SetSearchField('SCT');
    with Dialog.wwDBGrid1 do
    begin
        KeyOptions := KeyOptions - [dgAllowInsert, dgAllowDelete];
        EditControlOptions := EditControlOptions + [ecoCheckboxSingleClick];
    end;
end;

(*==============================================================================
** Chi cho chinh mot so fields
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.AllowEditSomeFields;
var
    b: Boolean;
begin
	with QrTC do
    begin
    	b := not (State in [dsInsert]);

        with EdMADT do
        begin
        	ReadOnly := b;
            TabStop := not b;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThu2.CbSTKBeforeDropDown(Sender: TObject);
begin
    exComboBeforeDropDownTK(Sender as TwwDBLookupCombo);
end;

end.

