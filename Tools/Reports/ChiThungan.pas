(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChiThungan;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwdotdot, Wwfltdlg2,
  wwidlg, AdvMenus, AppEvnts, isPanel, wwfltdlg,
  frameNgay, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid, ToolWin, isDb;

type
  TFrmChiThungan = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMST: TWideStringField;
    QrTCNGUOI: TWideStringField;
    QrTCSOTIEN: TFloatField;
    QrTCDGIAI: TWideMemoField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    QrTCLK_LYDO: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCLYDO: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    Bevel1: TBevel;
    PaMaster: TisPanel;
    Label1: TLabel;
    Label2: TLabel;
    LbNHAPCUA: TLabel;
    Label10: TLabel;
    Label18: TLabel;
    Label31: TLabel;
    DBText1: TDBText;
    EdSCT: TwwDBEdit;
    CbNgay: TwwDBDateTimePicker;
    DBMemo1: TDBMemo;
    EdSOTIEN: TwwDBEdit;
    CbMakho: TwwDBLookupCombo;
    Cbtenkho: TwwDBLookupCombo;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    QrTCLCT: TWideStringField;
    QrTCPTTT2: TWideStringField;
    CmdAudit: TAction;
    QrTCTC_XONG: TBooleanField;
    EdFULLNAME: TwwDBEdit;
    QrTCTHUNGAN: TIntegerField;
    QrTHUNGAN: TADOQuery;
    QrTCLK_USERNAME: TWideStringField;
    QrTCLK_TENTHUNGAN: TWideStringField;
    EdTHUNGAN: TwwDBComboDlg;
    ChonThungan: TwwSearchDialog;
    QrTHUNGAN2: TADOQuery;
    QrTCDRC_STATUS: TWideStringField;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrCT: TADOQuery;
    QrCTCALC_STT: TIntegerField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    DsCT: TDataSource;
    GrDetail: TwwDBGrid2;
    QrCTMENHGIA: TIntegerField;
    QrCTSOLUONG: TIntegerField;
    QrCTGHICHU: TWideStringField;
    vlTotal1: TisTotal;
    QrCTSOTIEN: TFloatField;
    PopPrint: TAdvPopupMenu;
    ItemsTN: TMenuItem;
    ItemsTX: TMenuItem;
    ItemBangke: TMenuItem;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTCTHANHTOAN: TFloatField;
    QrCTLOC: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMakhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrTCMAKHOChange(Sender: TField);
    procedure EdTHUNGANCustomDlg(Sender: TObject);
    procedure ChonThunganInitDialog(Dialog: TwwLookupDlg);
    procedure EdTHUNGANKeyPress(Sender: TObject; var Key: Char);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure CbMakhoBeforeDropDown(Sender: TObject);
    procedure CbMakhoCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure ItemBangkeClick(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
  private
    mLydo, mLCT, mMakho, mPTTT: String;
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;
  public
	procedure Execute (r: WORD);
  end;

var
  FrmChiThungan: TFrmChiThungan;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    mLCT := 'PCTN';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE: String = 'PHIEU_CHI_TN';

procedure TFrmChiThungan.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrDMKHO, QrTHUNGAN2]);

    mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := RegReadString(Name, 'Lydo', DataMain.GetSysParam('DEFAULT_LYDO_CHI'));
    mPTTT := DataMain.GetSysParam('DEFAULT_PTTT');
    
    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetDisplayFormat(QrCT, sysCurFmt);
    SetDisplayFormat(QrCT, ['MENHGIA', 'SOLUONG'], sysIntFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrTC, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	RegWrite(Name, ['Makho', 'Lydo'], [mMakho, mLydo]);
    HideAudit;
    exHideDrc;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
    begin
        with QrCT do
        begin
            Close;
            Open;
        end;

    	exShowDrc(DsTC);
    	try
   	    	CbNgay.SetFocus;
        except
        end;
    end
	else
    begin
    	exHideDrc;
    	GrBrowse.SetFocus;
    end;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
    if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;

	        if s <> '' then
    	        Sort := s;
        end;
		if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    DataMain.QrDMKHO.Requery;
	QrTHUNGAN2.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdNewExecute(Sender: TObject);
begin
    QrTC.Append;
    ActiveSheet(PgMain, 1);

    with DataMain.QrDMMENHGIA do
    begin
        if Active then
            Close;
        Open;

        if IsEmpty then
            Exit;

        First;
        while not Eof do
        begin
            QrCT.Append;
            QrCT.FieldByName('MENHGIA').AsInteger := FieldByName('MENHGIA').AsInteger;

            Next;
        end;
        QrCT.CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdSaveExecute(Sender: TObject);
begin
    vlTotal1.Sum;

	QrTC.Post;
    QrCT.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    QrCT.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime);

    mTrigger := True;       // Skip QrTC.BeforePost
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrTC);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsTC)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := (QrTC.FieldByName('SOTIEN').AsFloat <> 0) and not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdReRead.Enabled := bBrowse;
    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCAfterInsert(DataSet: TDataSet);
begin
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime  	:= Now;
		FieldByName('LCT').AsString     	:= mLCT;
		FieldByName('MAKHO').AsString   	:= mMakho;
        FieldByName('LOC').AsString           := sysLoc;
		FieldByName('LYDO').AsString    	:= mLydo;
        FieldByName('PTTT').AsString    	:= mPTTT;
        FieldByName('DRC_STATUS').AsString 	:= '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    with QrTC do
    begin
		if BlankConfirm(QrTC, ['NGAY', 'THUNGAN', 'MAKHO', 'SOTIEN']) then
	    	Abort;
		exValidClosing(FieldByName('NGAY').AsDateTime, 2);
    	SetNull(QrTC, ['PTTT', 'LYDO']);
    end;

    exDrcValidate(QrTC);
    DataMain.AllocSCT(mLCT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exCanEditVoucher(QrTC);
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTAfterCancel(DataSet: TDataSet);
begin
    vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTBeforeEdit(DataSet: TDataSet);
begin
    SetEditState(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTBeforeOpen(DataSet: TDataSet);
begin
    with QrCT do
        Parameters[0].Value := TGuidEx.ToString(QrTC.FieldByName('KHOA'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTBeforePost(DataSet: TDataSet);
begin
    with DataSet do
        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrTC.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTCalcFields(DataSet: TDataSet);
begin
    with QrCT do
    begin
        FieldByName('CALC_STT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTSOLUONGChange(Sender: TField);
begin
    with QrCT do
        FieldByName('SOTIEN').AsFloat :=
            FieldByName('MENHGIA').AsInteger * FieldByName('SOLUONG').AsInteger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCAfterPost(DataSet: TDataSet);
begin
	with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
        mLydo := FieldByName('LYDO').AsString; 
    end;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.ItemBangkeClick(Sender: TObject);
begin
    CmdSave.Execute;
	ShowReport(Caption, FORM_CODE + '_BK', [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CbMakhoBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CbMakhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CbMakhoNotInList(Sender: TObject; LookupTable: TDataSet;
    NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.CmdAuditExecute(Sender: TObject);
begin
    ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.EdTHUNGANCustomDlg(Sender: TObject);
var
	makho: String;
begin
	makho := QrTC.FieldByName('MAKHO').AsString;
    //QrTHUNGAN2.Filter := 'MAKHO=''' + makho + '''';
    if not ChonThungan.Execute then
    	Exit;

    with QrTC do
    begin
		if State in [dsBrowse] then
            Edit;
        FieldByName('THUNGAN').AsInteger := QrTHUNGAN2.FieldByName('UID').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCMAKHOChange(Sender: TField);
begin
  	with QrTC do
		FieldByName('THUNGAN').Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrTC.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.ChonThunganInitDialog(Dialog: TwwLookupDlg);
begin
	InitSearchDialog(Dialog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChiThungan.EdTHUNGANKeyPress(Sender: TObject; var Key: Char);
begin
	if CharInSet(Key, ['.', '>', '?', '/']) then
    	EdTHUNGAN.DropDown;
end;



end.
