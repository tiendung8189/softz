object FrmChikhac: TFrmChikhac
  Left = 273
  Top = 259
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Phi'#7871'u Chi Kh'#225'c'
  ClientHeight = 573
  ClientWidth = 822
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    822
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel2: TBevel
    Left = 0
    Top = 38
    Width = 822
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 822
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object SepChecked: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton5: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 822
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 814
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 814
        Height = 483
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GrBrowse: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 814
          Height = 434
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'IMG;ImageIndex;Original Size'
            'IMG2;ImageIndex;Original Size')
          Selected.Strings = (
            'IMG'#9'3'#9#9'F'
            'IMG2'#9'3'#9#9'F'
            'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
            'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7913'ng t'#7915
            'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho'
            'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho'
            'SOTIEN'#9'15'#9'S'#7889' ti'#7873'n'#9'F'
            'LK_LYDO'#9'20'#9'L'#253' do'#9'F'
            'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgAllowInsert]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopMaster
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnDblClick = GrBrowseDblClick
          OnEnter = CmdRefreshExecute
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
        inline frDate: TfrNGAY
          Left = 0
          Top = 0
          Width = 814
          Height = 49
          Align = alTop
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 814
          inherited Panel1: TPanel
            Width = 814
            ParentColor = False
            ExplicitWidth = 814
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaMaster: TisPanel
        Left = 0
        Top = 0
        Width = 814
        Height = 178
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        HeaderCaption = ' :: Chi kh'#225'c'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 58
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Panel1: TPanel
          Left = 2
          Top = 18
          Width = 810
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object DBText1: TDBText
            Left = 6
            Top = 3
            Width = 21
            Height = 17
            DataField = 'XOA'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 77
            Top = 7
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object Label2: TLabel
            Left = 283
            Top = 7
            Width = 50
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' phi'#7871'u'
          end
          object Label3: TLabel
            Left = 546
            Top = 31
            Width = 54
            Height = 16
            Alignment = taRightJustify
            Caption = 'H'#236'nh th'#7913'c'
          end
          object Label31: TLabel
            Left = 52
            Top = 31
            Width = 53
            Height = 16
            Alignment = taRightJustify
            Caption = 'Kho h'#224'ng'
          end
          object TntLabel1: TLabel
            Left = 569
            Top = 7
            Width = 30
            Height = 16
            Alignment = taRightJustify
            Caption = 'L'#253' do'
          end
          object CbHinhThuc: TwwDBLookupCombo
            Left = 606
            Top = 27
            Width = 185
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DGIAI'#9'27'#9'H'#236'nh th'#7913'c'#9'F'
              'MA'#9'2'#9#9'F')
            DataField = 'PTTT'
            DataSource = DsTC
            LookupTable = DataMain.QrPTTT
            LookupField = 'MA'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
ButtonEffects.Transparent=True
          end
          object CbKHO: TwwDBLookupCombo
            Left = 168
            Top = 27
            Width = 357
            Height = 22
            TabStop = False
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'TENKHO'#9'40'#9'T'#234'n'#9'F'
              'MAKHO'#9'6'#9'M'#227#9'F')
            DataField = 'MAKHO'
            DataSource = DsTC
            LookupTable = DataMain.QrDMKHO
            LookupField = 'MAKHO'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
          object CbMAKHO: TwwDBLookupCombo
            Left = 112
            Top = 27
            Width = 53
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'MAKHO'#9'6'#9'M'#227#9'F'
              'TENKHO'#9'40'#9'T'#234'n'#9'F')
            DataField = 'MAKHO'
            DataSource = DsTC
            LookupTable = DataMain.QrDMKHO
            LookupField = 'MAKHO'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            OnBeforeDropDown = CbMAKHOBeforeDropDown
            OnCloseUp = CbMAKHOCloseUp
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
          object CbNGAY: TwwDBDateTimePicker
            Left = 112
            Top = 3
            Width = 165
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NGAY'
            DataSource = DsTC
            Epoch = 1950
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ShowButton = True
            TabOrder = 0
          end
          object EdSCT: TwwDBEdit
            Left = 340
            Top = 3
            Width = 185
            Height = 22
            TabStop = False
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'SCT'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBLookupCombo1: TwwDBLookupCombo
            Left = 606
            Top = 3
            Width = 185
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DGIAI'#9'28'#9'Lo'#7841'i thu'#9'F'
              'MA'#9'2'#9#9'F')
            DataField = 'LYDO'
            DataSource = DsTC
            LookupTable = DataMain.QrLYDO_CK
            LookupField = 'MA'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
ButtonEffects.Transparent=True
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 116
          Width = 810
          Height = 60
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label10: TLabel
            Left = 56
            Top = 29
            Width = 49
            Height = 16
            Alignment = taRightJustify
            Caption = 'Di'#7877'n gi'#7843'i'
          end
          object Label18: TLabel
            Left = 559
            Top = 5
            Width = 40
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' ti'#7873'n'
          end
          object Label5: TLabel
            Left = 40
            Top = 5
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#432#7901'i nh'#7853'n'
          end
          object DBMemo1: TDBMemo
            Left = 112
            Top = 25
            Width = 679
            Height = 22
            Ctl3D = False
            DataField = 'DGIAI'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
          end
          object EdNGUOIGIAO: TwwDBEdit
            Left = 112
            Top = 1
            Width = 237
            Height = 22
            Ctl3D = False
            DataField = 'NGUOI'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdSOTIEN: TwwDBEdit
            Left = 606
            Top = 1
            Width = 121
            Height = 22
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'SOTIEN'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaTK: TPanel
          Left = 2
          Top = 68
          Width = 810
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label4: TLabel
            Left = 49
            Top = 4
            Width = 56
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#224'i kho'#7843'n'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 44
            Top = 28
            Width = 61
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#226'n h'#224'ng'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object CbSTK: TwwDBLookupCombo
            Left = 112
            Top = 1
            Width = 165
            Height = 22
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'MATK'#9'23'#9'MATK'#9#9
              'TENTK'#9'33'#9'TENTK'#9'F')
            DataField = 'MATK'
            DataSource = DsTC
            LookupTable = DataMain.QrDMTK_NB
            LookupField = 'MATK'
            Options = [loColLines]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
ButtonEffects.Transparent=True
          end
          object CbTenTK: TwwDBLookupCombo
            Left = 280
            Top = 1
            Width = 245
            Height = 22
            TabStop = False
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taRightJustify
            Selected.Strings = (
              'TENTK'#9'35'#9'TENTK'#9'F'
              'MATK'#9'23'#9'MATK'#9#9)
            DataField = 'MATK'
            DataSource = DsTC
            LookupTable = DataMain.QrDMTK_NB
            LookupField = 'MATK'
            Options = [loColLines]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
ButtonEffects.Transparent=True
          end
          object EdNganhang: TwwDBEdit
            Left = 112
            Top = 25
            Width = 237
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'LK_NGANHANG'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdChinhanh: TwwDBEdit
            Left = 352
            Top = 25
            Width = 173
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'LK_CHINHANH'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 680
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 680
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 364
    Top = 316
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      OnExecute = CmdReReadExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTC
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MAKHO'
      'NGUOI'
      'SOTIEN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 336
    Top = 316
  end
  object QrTC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTCBeforeOpen
    BeforeInsert = QrTCBeforeInsert
    AfterInsert = QrTCAfterInsert
    BeforeEdit = QrTCBeforeEdit
    BeforePost = QrTCBeforePost
    AfterPost = QrTCAfterPost
    AfterCancel = QrTCAfterCancel
    AfterScroll = QrTCAfterScroll
    OnCalcFields = QrTCCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'THUCHI'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 420
    Top = 316
    object QrTCIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrTCCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrTCNGAY: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrTCNGAYValidate
    end
    object QrTCSCT: TWideStringField
      DisplayLabel = 'S'#232' phi'#213'u'
      DisplayWidth = 20
      FieldName = 'SCT'
    end
    object QrTCSOTIEN: TFloatField
      DisplayLabel = 'S'#232' ti'#210'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrTCSOTIENChange
    end
    object QrTCLK_TENKHO: TWideStringField
      DisplayLabel = 'Kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrTCNGUOI: TWideStringField
      DisplayLabel = 'Ng'#173#234'i'
      DisplayWidth = 30
      FieldName = 'NGUOI'
      Visible = False
      Size = 30
    end
    object QrTCDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      Visible = False
      BlobType = ftWideMemo
    end
    object QrTCCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTCUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTCDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrTCXOA: TWideStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrTCCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrTCUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrTCDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrTCMAKHO: TWideStringField
      DisplayLabel = 'M'#183' kho'
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrTCIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrTCLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrTCTC_XONG: TBooleanField
      FieldName = 'TC_XONG'
    end
    object QrTCLYDO: TWideStringField
      FieldName = 'LYDO'
      FixedChar = True
      Size = 2
    end
    object QrTCLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_CK
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Lookup = True
    end
    object QrTCPTTT: TWideStringField
      FieldName = 'PTTT'
      OnChange = QrTCPTTTChange
      FixedChar = True
      Size = 2
    end
    object QrTCLK_PTTT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = DataMain.QrPTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Size = 200
      Lookup = True
    end
    object QrTCDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      FixedChar = True
      Size = 1
    end
    object QrTCKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTCMATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'MATK'
      Size = 50
    end
    object QrTCLK_TENTK: TWideStringField
      DisplayLabel = 'Ch'#7911' t'#224'i kho'#7843'n'
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_MANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'MANH'
      KeyFields = 'MATK'
      Size = 100
      Lookup = True
    end
    object QrTCLK_MACN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MACN'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'MACN'
      KeyFields = 'MATK'
      Size = 100
      Lookup = True
    end
    object QrTCLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'LK_MANH'
      Size = 200
      Lookup = True
    end
    object QrTCLK_CHINHANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CHINHANH'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MACN'
      LookupResultField = 'TENCN'
      KeyFields = 'LK_MACN'
      Size = 200
      Lookup = True
    end
    object QrTCLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrTCTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
  end
  object DsTC: TDataSource
    DataSet = QrTC
    Left = 420
    Top = 344
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 121
    Top = 302
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 153
    Top = 302
  end
end
