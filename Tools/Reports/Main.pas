(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Main;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ActnList,
  Menus, ComCtrls, ExtCtrls, isEnv, AdvMenus, Jpeg, ExtDlgs, RzGroupBar, StdCtrls,
  RzSplit, fcStatusBar, ADODb, ImgList, fcImager, RzPanel, ToolWin, SystemCriticalU,
  AdvToolBar, NativeXml;

type
  TFrmMain = class(TForm)
    MyActionList: TActionList;
    CmdQuit: TAction;
    CmdSetpass: TAction;
    CmdAbout: TAction;
    CmdParams: TAction;
    CmdTonkhoDK: TAction;
    CmdDmnhom: TAction;
    CmdDmvt: TAction;
    CmdNhap: TAction;
    CmdXuat: TAction;
    CmdDmncc: TAction;
    CmdDmkhac: TAction;
    CmdKhoaso: TAction;
    CmdDmkh: TAction;
    CmdThu: TAction;
    CmdChi: TAction;
    CmdBc: TAction;
    CmdBanle: TAction;
    CmdNhapkhac: TAction;
    CmdXuatkhac: TAction;
    CmdDmkhac3: TAction;
    CmdForceDropDown: TAction;
    CmdNhaptra: TAction;
    CmdXuattra: TAction;
    CmdChuyenkho: TAction;
    CmdDmkho: TAction;
    CmdInlabel: TAction;
    CmdChikhac: TAction;
    ImgLarge: TImageList;
    CmdHelp: TAction;
    CmdSetBkgr: TAction;
    CmdClearBkgr: TAction;
    CmdBkgrOption: TAction;
    PicDlg: TOpenPictureDialog;
    CmdKiemke: TAction;
    CmdSetDateTime: TAction;
    CmdDmVIP: TAction;
    CmdDmmau: TAction;
    CmdDmsize: TAction;
    CmdDdhNCC: TAction;
    CmdCounter: TAction;
    CmdLock: TAction;
    CmdFaq: TAction;
    CmdMyCustomer: TAction;
    PopBkGr: TAdvPopupMenu;
    Gnhnhnn1: TMenuItem;
    Xahnhnn1: TMenuItem;
    N30: TMenuItem;
    Canhchnh2: TMenuItem;
    PopFunc: TAdvPopupMenu;
    ItemCat: TMenuItem;
    Outlook2: TMenuItem;
    TaskList2: TMenuItem;
    N1: TMenuItem;
    ItemCloseAll: TMenuItem;
    PopHanghoa: TAdvPopupMenu;
    PopDanhmuc: TAdvPopupMenu;
    PopCongno: TAdvPopupMenu;
    Nhmhngha1: TMenuItem;
    Danhmchngha2: TMenuItem;
    N6: TMenuItem;
    Danhschnhcungcp1: TMenuItem;
    N8: TMenuItem;
    Khchhngthnthit1: TMenuItem;
    N10: TMenuItem;
    Danhmckhohng1: TMenuItem;
    N14: TMenuItem;
    Danhmcmu2: TMenuItem;
    Danhmcsize2: TMenuItem;
    N15: TMenuItem;
    Ccquyc2: TMenuItem;
    Danhmckhc2: TMenuItem;
    Phiuthu1: TMenuItem;
    Phiuchi1: TMenuItem;
    N2: TMenuItem;
    Chiph2: TMenuItem;
    CmdDmdialy: TAction;
    N18: TMenuItem;
    Vtral1: TMenuItem;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton3: TToolButton;
    ToolBar2: TToolBar;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton10: TToolButton;
    BtBaocao: TToolButton;
    ToolButton2: TToolButton;
    Panel1: TPanel;
    RzSizePanel1: TRzSizePanel;
    GbFunc: TRzGroupBar;
    GrHanghoa: TRzGroup;
    GrCongno: TRzGroup;
    GrDanhmuc: TRzGroup;
    GrHethong: TRzGroup;
    PaBkGr: TPanel;
    BkGr: TfcImager;
    PaDesc: TPanel;
    LbDesc2: TLabel;
    Bevel3: TBevel;
    LbDesc1: TLabel;
    Bevel2: TBevel;
    PopDulieu: TAdvPopupMenu;
    PopHethong: TAdvPopupMenu;
    PopTrogiup: TAdvPopupMenu;
    Tnkhouk1: TMenuItem;
    N16: TMenuItem;
    Ktthc2: TMenuItem;
    Danhschquythungn2: TMenuItem;
    N17: TMenuItem;
    imtkhu2: TMenuItem;
    Hngdnsdng2: TMenuItem;
    N21: TMenuItem;
    Cchipthnggp1: TMenuItem;
    Danhschkhchhng1: TMenuItem;
    N31: TMenuItem;
    Giithiu2: TMenuItem;
    Status: TfcStatusBar;
    N22: TMenuItem;
    CmdTygia: TAction;
    N23: TMenuItem;
    Tgi1: TMenuItem;
    CmdDondhKH: TAction;
    Kimkkho2: TMenuItem;
    N13: TMenuItem;
    nthngkhchhng1: TMenuItem;
    nthng2: TMenuItem;
    N11: TMenuItem;
    Vnchuynnib1: TMenuItem;
    Xutkhc2: TMenuItem;
    Xuttr2: TMenuItem;
    Phiuxut2: TMenuItem;
    N4: TMenuItem;
    Nhpkhc2: TMenuItem;
    Nhptr2: TMenuItem;
    Phiunhp2: TMenuItem;
    CmdNhapDC: TAction;
    CmdXuatDC: TAction;
    N20: TMenuItem;
    iuchnhphiunhp1: TMenuItem;
    iuchnhphiuxut1: TMenuItem;
    CmdDmvtBo: TAction;
    CmdNhaptrabl: TAction;
    CmdHoadonGTGT: TAction;
    N25: TMenuItem;
    CmdThuHT: TAction;
    CmdChiHT: TAction;
    CmdThuThungan: TAction;
    CmdChiThungan: TAction;
    CmdKetchuyen: TAction;
    Phiuthu2: TMenuItem;
    Phiuchi2: TMenuItem;
    hutmng1: TMenuItem;
    N7: TMenuItem;
    Chitmng1: TMenuItem;
    Ktchuyntin1: TMenuItem;
    CmdThukhac: TAction;
    hukhc1: TMenuItem;
    GrBanle: TRzGroup;
    CmdKhuyenmai: TAction;
    CmdDmKhuyenmai: TAction;
    CmdReconnect: TAction;
    CmdTonkho: TAction;
    BtInBarcode: TToolButton;
    CmdExport: TAction;
    CmdKhuyenmai3: TAction;
    CmdNhaptrabl2: TAction;
    CmdLoaiVipCT: TAction;
    ChitkhutheoloithVIP1: TMenuItem;
    CmdDmNxb: TAction;
    CmdDmTacgia: TAction;
    N24: TMenuItem;
    Nhcungcp1: TMenuItem;
    cgi1: TMenuItem;
    CmdDmTaikhoan: TAction;
    ikhoncngty1: TMenuItem;
    CmdThu2: TAction;
    PhiuthutinkhchhngTheophiu1: TMenuItem;
    CmdChi2: TAction;
    PhiuchitinnhcungcpTheophiu1: TMenuItem;
    N26: TMenuItem;
    XutExcel1: TMenuItem;
    ToolButton8: TToolButton;
    PopBanle: TAdvPopupMenu;
    MenuItem19: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    N28: TMenuItem;
    Khas1: TMenuItem;
    GrPhanQuyen: TRzGroup;
    CmdListUser: TAction;
    CmdListGroup: TAction;
    CmdFunctionRight: TAction;
    CmdReportRight: TAction;
    CmdFlexsible: TAction;
    GrAdmin: TRzGroup;
    CmdCustomGrid: TAction;
    CmdDictionary: TAction;
    CmdFuncAdmin: TAction;
    CmdRepAdmin: TAction;
    CmdAdmin: TAction;
    CmdDmKhNcc: TAction;
    CmdLoaiVIP: TAction;
    LoithVIP1: TMenuItem;
    CmdLoaiVip2: TAction;
    CmdLoaiVipCt2: TAction;
    LoithVIPS1: TMenuItem;
    ChitkhutheoloithVIPS1: TMenuItem;
    CmdDmQuayke: TAction;
    Quyk1: TMenuItem;
    Khchhng1: TMenuItem;
    Nhcungcp2: TMenuItem;
    CmdReportEncrypt: TAction;
    Inmvch2: TMenuItem;
    N5: TMenuItem;
    Chitkhukhuynmi1: TMenuItem;
    heodiCKkhuynmi1: TMenuItem;
    N9: TMenuItem;
    Khuynmitheohnghab1: TMenuItem;
    Khuynmitheohanhngha1: TMenuItem;
    CmdExOption: TAction;
    CmdImOption: TAction;
    CmdConfig: TAction;
    Cuhnhhthng1: TMenuItem;
    N12: TMenuItem;
    CmdDmNganhang: TAction;
    N19: TMenuItem;
    Ngnhng1: TMenuItem;
    procedure CmdQuitExecute(Sender: TObject);
    procedure CmdSetpassExecute(Sender: TObject);
    procedure CmdAboutExecute(Sender: TObject);
    procedure CmdParamsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdTonkhoDKExecute(Sender: TObject);
    procedure CmdDmnhomExecute(Sender: TObject);
    procedure CmdXuatExecute(Sender: TObject);
    procedure CmdNhapExecute(Sender: TObject);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure CmdDmkhacExecute(Sender: TObject);
    procedure CmdDmkhExecute(Sender: TObject);
    procedure CmdThuExecute(Sender: TObject);
    procedure CmdChiExecute(Sender: TObject);
    procedure CmdBcExecute(Sender: TObject);
    procedure CmdKhoasoExecute(Sender: TObject);
    procedure CmdBanleExecute(Sender: TObject);
    procedure CmdNhapkhacExecute(Sender: TObject);
    procedure CmdXuatkhacExecute(Sender: TObject);
    procedure CmdDmkhac3Execute(Sender: TObject);
    procedure CmdForceDropDownExecute(Sender: TObject);
    procedure CmdNhaptraExecute(Sender: TObject);
    procedure CmdXuattraExecute(Sender: TObject);
    procedure CmdChuyenkhoExecute(Sender: TObject);
    procedure CmdDmkhoExecute(Sender: TObject);
    procedure CmdInlabelExecute(Sender: TObject);
    procedure CmdChikhacExecute(Sender: TObject);
    procedure CmdHelpExecute(Sender: TObject);
    procedure CmdSetBkgrExecute(Sender: TObject);
    procedure CmdClearBkgrExecute(Sender: TObject);
    procedure CmdBkgrOptionExecute(Sender: TObject);
    procedure CmdKiemkeExecute(Sender: TObject);
    procedure CmdSetDateTimeExecute(Sender: TObject);
    procedure CmdDmVIPExecute(Sender: TObject);
    procedure CmdDmmauExecute(Sender: TObject);
    procedure CmdDmsizeExecute(Sender: TObject);
    procedure CmdDdhNCCExecute(Sender: TObject);
    procedure PopBkGrPopup(Sender: TObject);
    procedure CmdCounterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdLockExecute(Sender: TObject);
    procedure CmdFaqExecute(Sender: TObject);
    procedure CmdMyCustomerExecute(Sender: TObject);
    procedure ItemCloseAllClick(Sender: TObject);
    procedure ItemCatClick(Sender: TObject);
    procedure PopFuncPopup(Sender: TObject);
    procedure CmdDmdialyExecute(Sender: TObject);
    procedure TntFormResize(Sender: TObject);
    procedure CmdTygiaExecute(Sender: TObject);
    procedure CmdDondhKHExecute(Sender: TObject);
    procedure CmdNhapDCExecute(Sender: TObject);
    procedure CmdXuatDCExecute(Sender: TObject);
    procedure CmdDmvtBoExecute(Sender: TObject);
    procedure CmdNhaptrablExecute(Sender: TObject);
    procedure CmdHoadonGTGTExecute(Sender: TObject);
    procedure CmdThuHTExecute(Sender: TObject);
    procedure CmdChiHTExecute(Sender: TObject);
    procedure CmdThuThunganExecute(Sender: TObject);
    procedure CmdChiThunganExecute(Sender: TObject);
    procedure CmdKetchuyenExecute(Sender: TObject);
    procedure CmdThukhacExecute(Sender: TObject);
    procedure CmdKhuyenmaiExecute(Sender: TObject);
    procedure CmdChietkhauBanleExecute(Sender: TObject);
    procedure CmdDmKhuyenmaiExecute(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure CmdTonkhoExecute(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure CmdKhuyenmai3Execute(Sender: TObject);
    procedure CmdNhaptrabl2Execute(Sender: TObject);
    procedure CmdLoaiVipCTExecute(Sender: TObject);
    procedure CmdDmNxbExecute(Sender: TObject);
    procedure CmdDmTacgiaExecute(Sender: TObject);
    procedure CmdDmTaikhoanExecute(Sender: TObject);
    procedure CmdThu2Execute(Sender: TObject);
    procedure CmdChi2Execute(Sender: TObject);
    procedure CmdListUserExecute(Sender: TObject);
    procedure CmdListGroupExecute(Sender: TObject);
    procedure CmdFunctionRightExecute(Sender: TObject);
    procedure CmdReportRightExecute(Sender: TObject);
    procedure CmdFlexsibleExecute(Sender: TObject);
    procedure CmdCustomGridExecute(Sender: TObject);
    procedure CmdDictionaryExecute(Sender: TObject);
    procedure CmdFuncAdminExecute(Sender: TObject);
    procedure CmdRepAdminExecute(Sender: TObject);
    procedure CmdAdminExecute(Sender: TObject);
    procedure CmdDmKhNccExecute(Sender: TObject);
    procedure CmdLoaiVIPExecute(Sender: TObject);
    procedure CmdLoaiVip2Execute(Sender: TObject);
    procedure CmdLoaiVipCt2Execute(Sender: TObject);
    procedure CmdDmQuaykeExecute(Sender: TObject);
    procedure CmdReportEncryptExecute(Sender: TObject);
    procedure CmdExOptionExecute(Sender: TObject);
    procedure CmdImOptionExecute(Sender: TObject);
    procedure CmdConfigExecute(Sender: TObject);
    procedure Ngnhng1Click(Sender: TObject);
    procedure CmdDmNganhangExecute(Sender: TObject);
  private
	r: WORD;
    mFixLogon: Boolean;
	procedure LoadBkgrImage;
    procedure SetBkgrText;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

uses
	{$IFDEF _MULTILANG_}
	// Multilizer
    IvI18N,
    IvResDll,
    IvResLangD,
    {$ENDIF}

	MainData, ExCommon, Dmvt, Tonkho, Params, Rights, GmsRep, Nhap, Xuat,
    Dmkhac, Dmncc, Dmkh, isMsg, Banle, Thu, Chi, NXKhac, Dmkhac3, Khoaso, isDb,
    ChuyenKho, Dmkho, isType, BkgrOption, Scan, Kiemke, isStr, DmVIP, Dmmau,
    Dmsize, isDba, DondhNCC, DondhKH, isLib, Dmdl, Dmquaytn, Tygia, DmhhNhom,
    DieuchinhNhap, DieuchinhXuat, DmvtBo, Nhaptrabl, HoadonGTGT, NhapTra,
    XuatTra, Inlabel, Ketchuyentien, ThuThungan, ChiThungan, Chikhac, Thukhac,
    Khuyenmai, DmKhuyenmai, TonkhoDK, FlexRep, Khuyenmai3,
    Nhaptrabl2,  ChiHT, ThuHT, DmLoaiVipCT, isCommon, Dmtacgia, DmNXB,
    DmTK, Thu2, Chi2, DmKho2, isOdbc, isLogin, ListUser, ListGroup,
  FunctionAccess, ReportAccess, AdminData, FlexEdit, CustomizeGrid, Dictionary,
  FunctionAdmin, RepConfig, DmKhNcc, isResourceString, DmLoaiVIP, DmPhongban,
    Dmnv, DmQuayke, RepEncode, EximOption, Params2, DmNganhang;

{$R *.DFM}
{$R exRes.res}

{$REGION '<< Some of reusable procedures >>'}
(*==============================================================================
** Load background image
**------------------------------------------------------------------------------
*)
procedure TFrmMain.LoadBkgrImage;
const
	cDrawStyle: array[0..5] of TfcImagerDrawStyle = (dsCenter, dsNormal,
        dsProportional, dsProportionalCenter, dsStretch, dsTile);
var
	s: String;
	n: Integer;
begin
    // Set background image
    s := RegRead('Background', 'FileName', '');
    if FileExists(s) then
    begin
        BkGr.Visible := True;
        try
            BkGr.Picture.LoadFromFile(s);
        except;
        end;
    end;

    with BkGr do
		if Visible then
	    begin
            Transparent := RegRead('Transparent', BKGR_TRANSPARENT);
            TransparentColor := RegRead('Transparent Color', BKGR_TRANSPARENT_COLOR);
            n := RegRead('Draw Style', BKGR_DRAW_STYLE);
            DrawStyle := cDrawStyle[n];
        end;

    // Background text
    PaDesc.Visible := RegRead('Show Text', BKGR_SHOWTEXT);
    if PaDesc.Visible then
    begin
        // Text
        n := RegRead('Text Color', BKGR_TEXT_COLOR);
        LbDesc1.Font.Color := n;
        LbDesc2.Font.Color := n;
    end;
end;

(*==============================================================================
** Set background text
**------------------------------------------------------------------------------
*)
procedure TFrmMain.SetBkgrText;
begin
    if PaDesc.Visible then
    begin
	    LbDesc1.Caption := '';//UpperCase(isDoubleChr(sysDesc1), loUserLocale);
    	LbDesc2.Caption := '';//UpperCase(isDoubleChr(sysDesc2), loUserLocale);
    end;
end;
{$ENDREGION}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	Handled := MyActionList.Tag = 0;
//    if Handled then //NTD
//        TestDbConnection;
end;

procedure TFrmMain.Ngnhng1Click(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormActivate(Sender: TObject);
begin
	if Tag = 1 then
    begin
	    with GbFunc do
    	begin
	    	ScrollPosition := 0;
	        Groups[0].Reposition;
    	end;

		Tag := 2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
    i: Integer;
begin
    // Save GroupBar state
    with GbFunc do
        for i := 0 to GroupCount - 1 do
            RegWrite(Self.Name, Groups[i].Name, Groups[i].Opened);

    try
//        DataMain.isEnv1.Logoff; //NTD
    except
    end;
    DbeFinal;

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormShow(Sender: TObject);
var
    i: Integer;
begin
	TMyForm(Self).Init2;
	LoadCustomIcon;
//    Status.Panels[4].Text := 'DB: ' + isNameServer + '.' + isDatabaseServer;
//    Status.Panels[6].Text := 'Build ' + GetModuleVersion;
    DbeInitial;

	// Initial
    FrmScan := Nil;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);

    // Enable system functions
    SetApplicationVersion;
    SetApplicationFunctions(Self);

    GrPhanQuyen.Visible := sysIsAdmin;
    GrAdmin.Visible := False;
    BtBaocao.Visible := FlexConfigBool('FRMMAIN', 'Bao cao', False) and (not sysIsAdmin);
//    ToolButton16.Visible := BtBaocao.Visible;
    BtInBarcode.Visible := FlexConfigBool('FRMMAIN', 'In Barcode', False);
//    ToolButton13.Visible := BtInBarcode.Visible;

    // Desktop
	SetBkgrText;
    LoadBkgrImage;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');


    // Load GroupBar state
    with GbFunc do
    begin
        Style := TRzGroupBarStyle(RegRead(Self.Name, 'RzGroupStyle', 0));
        for i := 0 to GroupCount - 1 do
            Groups[i].Opened := RegReadBool(Self.Name, Groups[i].Name, True);
	    ScrollPosition := 0;
        Groups[0].Reposition;
    end;
    MyActionList.Tag := 1;
    SystemCritical.IsCritical := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetpassExecute(Sender: TObject);
begin
    DataMain.isLogon.ResetPassword; //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdAboutExecute(Sender: TObject);
begin
//	Application.CreateForm(TFrmAbout, FrmAbout);
//    FrmAbout.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdParamsExecute(Sender: TObject);
var
	n: Integer;
begin
//	r := DataMain.GetRights('SZ_TSO');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmParams, FrmParams);
    n := FrmParams.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhoasoExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_KHOASO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhoaso, FrmKhoaso);
    FrmKhoaso.Execute(r);
    DataMain.InitParams;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTonkhoDKExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_TONKHO_DK');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTonkhoDK, FrmTonkhoDK);
    FrmTonkhoDK.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTonkhoExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_TONKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTonkho, FrmTonkho);
    FrmTonkho.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnhomExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NGANH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmhhNhom, FrmDmhhNhom);
    FrmDmhhNhom.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmvtExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_MAVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvt, FrmDmvt);
    FrmDmvt.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmvtBoExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_MAVT_BO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvtBo, FrmDmvtBo);
    FrmDmvtBo.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhoExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_KHO');
    if r = R_DENY then
    	Exit;

    if sysIsDrc then
    begin
        Application.CreateForm(TFrmDmkho2, FrmDmkho2);
        FrmDmkho2.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmkho, FrmDmkho);
        FrmDmkho.Execute(r);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmVIPExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_VIP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmVIP, FrmDmVIP);
    FrmDmVIP.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NHAP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhap, FrmNhap);
    FrmNhap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapDCExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NHAP_DC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDieuchinhNhap, FrmDieuchinhNhap);
    FrmDieuchinhNhap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_XUAT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmXuat, FrmXuat);
    FrmXuat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatDCExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_XUAT_DC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDieuchinhXuat, FrmDieuchinhXuat);
    FrmDieuchinhXuat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptraExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NHAPTRA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptra, FrmNhaptra);
    FrmNhaptra.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuattraExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_XUATTRA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmXuattra, FrmXuattra);
    FrmXuattra.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapkhacExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NHAPKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNXKhac, FrmNXKhac);
    FrmNXKhac.Execute('N', r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatkhacExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_XUATKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNXKhac, FrmNXKhac);
    FrmNXKhac.Execute('X', r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChuyenkhoExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_CKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChuyenKho, FrmChuyenKho);
    FrmChuyenKho.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKiemkeExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_KIEMKE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKiemke, FrmKiemke);
    FrmKiemke.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnccExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmncc, FrmDmncc);
    FrmDmncc.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNganhangExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_NGANHANG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNganhang, FrmDmNganhang);
    FrmDmNganhang.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_KHACHHANG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmkh, FrmDmkh);
    FrmDmkh.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmKhNccExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_KH_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhNcc, FrmDmkhNcc);
    FrmDmkhNcc.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhacExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhac, FrmDmkhac);
    FrmDmkhac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhac3Execute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_DMKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhac3, FrmDmkhac3);
    FrmDmkhac3.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThu2Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_THU_PHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThu2, FrmThu2);
    FrmThu2.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_THU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThu, FrmThu);
    FrmThu.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuHTExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_THU_PHIEUHT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThuHT, FrmThuHT);
    FrmThuHT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuThunganExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_THU_THUNGAN');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmThuThungan, FrmThuThungan);
    FrmThuThungan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_CHI');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChi, FrmChi);
    FrmChi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiHTExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_CHI_PHIEUHT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChiHT, FrmChiHT);
    FrmChiHT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiThunganExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_CHI_THUNGAN');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChiThungan, FrmChiThungan);
    FrmChiThungan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKetchuyenExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_KETCHUYEN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKetchuyentien, FrmKetchuyentien);
    FrmKetchuyentien.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThukhacExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_THU_KHAC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmThukhac, FrmThukhac);
    FrmThukhac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChikhacExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_CHI_KHAC');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChikhac, FrmChikhac);
    FrmChikhac.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute(1) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdAdminExecute(Sender: TObject);
begin
    if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
        GrAdmin.Visible := mFixLogon;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBanleExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_SUABILL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBanle, FrmBanle);
    FrmBanle.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdForceDropDownExecute(Sender: TObject);
begin
   (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFuncAdminExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFuncAdmin, FrmFuncAdmin);
	FrmFuncAdmin.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFunctionRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFuncAccess, FrmFuncAccess);
    FrmFuncAccess.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdImOptionExecute(Sender: TObject);
var
    s: String;
    n: Integer;
    Xml: TNativeXml;
begin
//    CheckConn;

    Application.CreateForm(TFrmEximOption, FrmEximOption);
    n := FrmEximOption.Execute(0, s);
    if n < 0 then
        Exit;

    Xml := DataMain.CreateXml;
    Xml.LoadFromFile(s);
    DataMain.ImportDataXml(Xml, n);
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdInlabelExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_STAMP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmInlabel, FrmInlabel);
    FrmInlabel.ShowModal
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHelpExecute(Sender: TObject);
begin
	Application.HelpCommand(HH_DISPLAY_TOC, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetBkgrExecute(Sender: TObject);
begin
	// Get file name
	with PicDlg do
		if not Execute then
        	Exit;

    // Save to registry
    RegWrite('Background', 'FileName', PicDlg.FileName);

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdClearBkgrExecute(Sender: TObject);
begin
	// Delete file name
    RegDelete('Background', 'FileName');

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBkgrOptionExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmBkgrOption, FrmBkgrOption);
    with FrmBkgrOption do
    	if Execute then
	       LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
//     DbConnect; //NTD
    if not ConnectToServer(isNameServer, isUserServer, isPasswordServer, isDatabaseServer) then
    begin
      ErrMsg(StrFailedToConnectToDatabaseServer);
      Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdRepAdminExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmRepConfig, FrmRepConfig);
	FrmRepConfig.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReportEncryptExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmRepEncode, FrmRepEncode);
	FrmRepEncode.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReportRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmRepAccess, FrmRepAccess);
    FrmRepAccess.Execute(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetDateTimeExecute(Sender: TObject);
begin
	WinExec('control timedate.cpl', SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmmauExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_MAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmmau, FrmDmmau);
    FrmDmmau.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmsizeExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_SIZE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmsize, FrmDmsize);
    FrmDmsize.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmTacgiaExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_TACGIA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmTacgia, FrmDmTacgia);
    FrmDmTacgia.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmTaikhoanExecute(Sender: TObject);
var
    s: string;
begin
    r := DataMain.GetRights('SZ_TAIKHOAN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmTK, FrmDmTK);
    FrmDmTK.Execute(r, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmdialyExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDdhNCCExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_DDH_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDondhNCC, FrmDondhNCC);
    FrmDondhNCC.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDictionaryExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDictionary, FrmDictionary);
    FrmDictionary.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDondhKHExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_DDH_KH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDondhKH, FrmDondhKH);
    FrmDondhKH.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdExOptionExecute(Sender: TObject);
var
    s: String;
    n: Integer;
    Xml : TNativeXml;
begin
//    CheckConn;

    Application.CreateForm(TFrmEximOption, FrmEximOption);
    n := FrmEximOption.Execute(1, s);
    if n < 0 then
        Exit;

    Xml := DataMain.CreateXml;

    // Dataset -> Xml
    DataMain.ExportDataXlm(xml, n, s);
    xml.SaveToFile(s);
    MsgDone;
    xml.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdExportExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmFlexRep, FrmFlexRep);
    if not FrmFlexRep.Execute(1) then
    begin
    	DenyMsg;
    	FrmFlexRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PopBkGrPopup(Sender: TObject);
begin
	PopBkGr.Items[0].Caption := CmdSetBkGr.Caption +
    	Format(' (%dx%d)', [PaBkGr.Width, PaBkGr.Height]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdConfigExecute(Sender: TObject);
var
    n: Integer;
begin
    Application.CreateForm(TFrmParams2, FrmParams2);
    n := FrmParams2.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCounterExecute(Sender: TObject);
begin
	r := DataMain.GetRights('SZ_QUAYTN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmquaytn, FrmDmquaytn);
    FrmDmquaytn.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCustomGridExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmCustomizeGrid, FrmCustomizeGrid);
	FrmCustomizeGrid.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdListGroupExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListGroup, FrmListGroup);
    FrmListGroup.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdListUserExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListUser, FrmListUser);
    FrmListUser.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVip2Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_LOAIVIP2');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmloaiVIP, FrmDmloaiVIP);
    FrmDmloaiVIP.Execute(r, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVipCt2Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_LOAIVIP_CT2');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLoaiVipCT, FrmDmLoaiVipCT);
    FrmDmLoaiVipCT.Execute(r, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVipCTExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_LOAIVIP_CT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLoaiVipCT, FrmDmLoaiVipCT);
    FrmDmLoaiVipCT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVIPExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_LOAIVIP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmloaiVIP, FrmDmloaiVIP);
    FrmDmloaiVIP.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLockExecute(Sender: TObject);
begin
//    DataMain.isEnv1.Lock //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFaqExecute(Sender: TObject);
begin
	Faqs;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFlexsibleExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFlexEdit, FrmFlexEdit);
    FrmFlexEdit.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdMyCustomerExecute(Sender: TObject);
begin
	Customers;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCloseAllClick(Sender: TObject);
var
	i: Integer;
begin
	with GbFunc do
   		for i := 0 to GroupCount - 1 do
        	Groups[i].Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCatClick(Sender: TObject);
begin
	case (Sender as TComponent).Tag of
    0:
    	with GbFunc do
        	if Style <> gbsCategoryView then
	        	Style := gbsCategoryView;
    1:
    	with GbFunc do
        	if Style <> gbsOutlook then
	        	Style := gbsOutlook;
    2:
    	with GbFunc do
        	if Style <> gbsTaskList then
	        	Style := gbsTaskList;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PopFuncPopup(Sender: TObject);
begin
	with PopFunc, GbFunc do
    begin
    	Items[0].Checked := Style = gbsCategoryView;
    	Items[1].Checked := Style = gbsOutlook;
    	Items[2].Checked := Style = gbsTasklist;
    	Items[4].Enabled := Items[0].Checked;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTygiaExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_TYGIA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTygia, FrmTygia);
    FrmTygia.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptrabl2Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_NHAPTRA_BL2');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptrabl2, FrmNhaptrabl2);
    FrmNhaptrabl2.Execute(r);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptrablExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_NHAPTRA_BL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptrabl, FrmNhaptrabl);
    FrmNhaptrabl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHoadonGTGTExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_HOADON_GTGT');
    if r = R_DENY then
    	Exit;

    if not exDefaultWarehouse then
    	Exit;

	Application.CreateForm(TFrmHoadonGTGT, FrmHoadonGTGT);
    FrmHoadonGTGT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNxbExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_NXB');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmNxb, FrmDmNxb);
    FrmDmNxb.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmQuaykeExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_QUAYKE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmQuayke, FrmDmQuayke);
    FrmDmQuayke.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmai3Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_KHUYENMAI3');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhuyenmai3, FrmKhuyenmai3);
    FrmKhuyenmai3.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmaiExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_KHUYENMAI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhuyenmai, FrmKhuyenmai);
    FrmKhuyenmai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmKhuyenmaiExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_DM_KHUYENMAI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmKhuyenmai, FrmDmKhuyenmai);
    FrmDmKhuyenmai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChi2Execute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_CHI_PHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChi2, FrmChi2);
    FrmChi2.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChietkhauBanleExecute(Sender: TObject);
begin
    r := DataMain.GetRights('SZ_CHIETKHAU_BANLE');
    if r = R_DENY then
    	Exit;
end;

initialization
	{$IFDEF _MULTILANG_}
    sysEnglish := GetArg('E');
    if sysEnglish = False then
    else if not	SetNewResourceDll(LANG_ENGLISH) then
    {$ENDIF}
    	sysEnglish := False;
    if sysEnglish then
        DataMain.isEnv1.Language := ENGLISH;
end.

