object FrmImportExcel: TFrmImportExcel
  Left = 629
  Top = 257
  BorderIcons = [biSystemMenu]
  Caption = 'Ch'#7885'n Fields'
  ClientHeight = 302
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Padding.Left = 6
  Padding.Right = 6
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    522
    302)
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 8
    Top = 64
    Width = 505
    Height = 229
    DisableThemes = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    CaptionColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    DataSource = dsFields
    Items = <
      item
        DataSource = dsFields
        DataField = 'Daily Allowance'
        Caption = 'F1'
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end
      item
        DataSource = dsFields
        WordWrap = False
      end>
    CaptionWidth = 197
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
  end
  object cbFields: TComboBox
    Left = 184
    Top = 114
    Width = 297
    Height = 24
    Ctl3D = False
    ItemHeight = 0
    ParentCtl3D = False
    TabOrder = 2
    TabStop = False
    Visible = False
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 9
    Top = 3
    Width = 504
    Height = 60
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label3: TLabel
      Left = 14
      Top = 23
      Width = 44
      Height = 16
      Caption = 'Sheets:'
    end
    object cbSheet: TComboBox
      Left = 64
      Top = 18
      Width = 166
      Height = 24
      ItemHeight = 0
      TabOrder = 2
    end
    object BitBtn1: TBitBtn
      Left = 240
      Top = 17
      Width = 113
      Height = 25
      Cursor = 1
      Action = CmdRefresh
      Caption = 'Refresh'
      DoubleBuffered = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 376
      Top = 17
      Width = 106
      Height = 25
      Cursor = 1
      Action = CmdImport
      Caption = 'Import'
      Default = True
      DoubleBuffered = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 52
    Top = 124
    object CmdRefresh: TAction
      Caption = 'Refresh'
      Hint = 'T'#7843'i l'#7841'i danh s'#225'ch c'#7897't'
      OnExecute = CmdRefreshExecute
    end
    object CmdImport: TAction
      Caption = 'Import'
      Hint = 'L'#7845'y d'#7919' li'#7879'u v'#224'o'
      OnExecute = CmdImportExecute
    end
    object CmdReview: TAction
      Caption = 'Xem d'#7919' li'#7879'u Excel'
      Hint = 'Xem d'#7919' li'#7879'u Excel'
      ShortCut = 114
      OnExecute = CmdReviewExecute
    end
    object CmdClose: TAction
      Caption = 'Close'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
  end
  object Conn: TADOConnection
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 136
    Top = 84
  end
  object QrExcel: TADOQuery
    Connection = Conn
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 164
    Top = 84
  end
  object QrFields: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 256
    Top = 160
  end
  object dsFields: TDataSource
    DataSet = QrFields
    Left = 256
    Top = 192
  end
  object dsExcel: TDataSource
    DataSet = QrExcel
    Left = 163
    Top = 112
  end
  object QrIMPORT: TADOQuery
    Connection = DataMain.Conn
    Parameters = <>
    Left = 164
    Top = 196
  end
  object SP_PROCESS: TADOCommand
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 165
    Top = 169
  end
  object ExToExcel: TSMExportToExcel
    AnimatedStatus = False
    DataFormats.DateOrder = doDMY
    DataFormats.DateSeparator = '/'
    DataFormats.TimeSeparator = ':'
    DataFormats.FourDigitYear = True
    DataFormats.LeadingZerosInDate = True
    DataFormats.ThousandSeparator = ','
    DataFormats.DecimalSeparator = '.'
    DataFormats.CurrencyString = '$'
    DataFormats.BooleanTrue = 'True'
    DataFormats.BooleanFalse = 'False'
    DataFormats.UseRegionalSettings = True
    KeyGenerator = 'SMExport 4.99'
    Options = [soWaitCursor, soDisableControls, soUseFieldNameAsCaption, soColorsFonts]
    TitleStatus = 'Exporting...'
    ExportIfEmpty = False
    OnProgress = ExToExcelProgress
    Columns = <>
    Bands = <>
    DataSet = QrIMPORT
    ColumnSource = csDataSet
    FileName = 'SMExport.XLS'
    AddTitle = True
    CharacterSet = csANSI_WINDOWS
    RowsPerFile = 5000
    DetailSources = <>
    OnGetCellParams = ExToExcelGetCellParams
    AutoFitColumns = True
    ExportStyle.Style = esNormal
    ExportStyle.OddColor = clBlack
    ExportStyle.EvenColor = clBlack
    Left = 124
    Top = 172
  end
  object XLSReadWriteII41: TXLSReadWriteII4
    Version = xvExcel97
    Sheets = <
      item
        Name = 'Sheet1'
        DefaultColWidth = 8
        DefaultRowHeight = 255
        PrintSettings.Copies = 0
        PrintSettings.FooterMargin = 0.500000000000000000
        PrintSettings.FooterMarginCm = 1.270000000000000000
        PrintSettings.HeaderMargin = 0.500000000000000000
        PrintSettings.HeaderMarginCm = 1.270000000000000000
        PrintSettings.MarginBottom = 1.000000000000000000
        PrintSettings.MarginLeft = 0.750000000000000000
        PrintSettings.MarginRight = 0.750000000000000000
        PrintSettings.MarginTop = 1.000000000000000000
        PrintSettings.MarginBottomCm = 2.540000000000000000
        PrintSettings.MarginLeftCm = 1.905000000000000000
        PrintSettings.MarginRightCm = 1.905000000000000000
        PrintSettings.MarginTopCm = 2.540000000000000000
        PrintSettings.Options = [psoPortrait]
        PrintSettings.PaperSize = psLegal
        PrintSettings.ScalingFactor = 100
        PrintSettings.StartingPage = 1
        PrintSettings.HorizPagebreaks = <>
        PrintSettings.VertPagebreaks = <>
        PrintSettings.Resolution = 600
        PrintSettings.FitWidth = 1
        PrintSettings.FitHeight = 1
        MergedCells = <>
        Options = [soGridlines, soRowColHeadings, soShowZeros]
        WorkspaceOptions = [woShowAutoBreaks, woRowSumsBelow, woColSumsRight, woOutlineSymbols]
        SheetProtection = [spEditObjects, spEditScenarios, spEditCellFormatting, spEditColumnFormatting, spEditRowFormatting, spInsertColumns, spInsertRows, spInsertHyperlinks, spDeleteColumns, spDeleteRows, spSelectLockedCells, spSortCellRange, spEditAutoFileters, spEditPivotTables, spSelectUnlockedCells]
        Zoom = 0
        ZoomPreview = 0
        RecalcFormulas = True
        Hidden = hsVisible
        Validations = <>
        DrawingObjects.Texts = <>
        DrawingObjects.Notes = <>
        DrawingObjects.Basics = <>
        DrawingObjects.AutoShapes = <>
        DrawingObjects.Pictures = <>
        ControlsObjects.ListBoxes = <>
        ControlsObjects.ComboBoxes = <>
        ControlsObjects.Buttons = <>
        ControlsObjects.CheckBoxes = <>
        ControlsObjects.RadioButtons = <>
        Hyperlinks = <>
        ConditionalFormats = <>
      end>
    Workbook.Left = 100
    Workbook.Top = 100
    Workbook.Width = 10000
    Workbook.Height = 7000
    Workbook.SelectedTab = 0
    Workbook.Options = [woHScroll, woVScroll, woTabs]
    OptionsDialog.SaveExtLinkVal = False
    OptionsDialog.CalcCount = 100
    OptionsDialog.CalcMode = cmAutomatic
    OptionsDialog.Delta = 0.001000000000000000
    OptionsDialog.ShowObjects = soShowAll
    OptionsDialog.Iteration = False
    OptionsDialog.PrecisionAsDisplayed = True
    OptionsDialog.R1C1Mode = False
    OptionsDialog.RecalcBeforeSave = False
    OptionsDialog.Uncalced = False
    OptionsDialog.SaveRecalc = True
    BookProtected = False
    Backup = False
    RefreshAll = False
    StrTRUE = 'TRUE'
    StrFALSE = 'FALSE'
    ShowFormulas = False
    IsMac = False
    PreserveMacros = True
    ComponentVersion = '4.00.21a'
    MSOPictures = <>
    RecomendReadOnly = False
    TempFileMode = tfmOnDisk
    Left = 40
    Top = 200
  end
  object XLSDbRead41: TXLSDbRead4
    Column = 0
    Dataset = QrIMPORT
    IncludeFieldnames = True
    IndentDetailTables = True
    ReadDetailTables = True
    FormatCells = True
    Row = 0
    Sheet = 0
    XLS = XLSReadWriteII41
    Left = 40
    Top = 240
  end
end
