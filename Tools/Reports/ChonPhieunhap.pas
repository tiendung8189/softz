(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieunhap;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid;

type
  TFrmChonPhieunhap = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNHAP: TDataSource;
    QrPHIEUNHAP: TADOQuery;
    QrPHIEUNHAPNGAY: TDateTimeField;
    QrPHIEUNHAPSCT: TWideStringField;
    QrPHIEUNHAPMADT: TWideStringField;
    QrPHIEUNHAPMAKHO: TWideStringField;
    QrPHIEUNHAPDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbNCC: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMADT: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMNCC: TADOQuery;
    QrPHIEUNHAPTENKHO: TWideStringField;
    QrPHIEUNHAPTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrPHIEUNHAPNG_GIAO: TWideStringField;
    QrPHIEUNHAPNG_NHAN: TWideStringField;
    QrPHIEUNHAPTHUE_SUAT: TFloatField;
    QrPHIEUNHAPHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNHAPHOADON_SO: TWideStringField;
    QrPHIEUNHAPHOADON_NGAY: TDateTimeField;
    QrPHIEUNHAPSOLUONG: TFloatField;
    QrPHIEUNHAPTHANHTOAN: TFloatField;
    QrPHIEUNHAPPHIEUGIAOHANG: TWideStringField;
    QrPHIEUNHAPKHOA: TGuidField;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMADTChange(Sender: TObject);
    procedure CbMADTExit(Sender: TObject);
    procedure CbMADTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pFix: Boolean; pNcc, pKho: String): TGUID;
  end;

var
  FrmChonPhieunhap: TFrmChonPhieunhap;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieunhap.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    CbMADT.Enabled := not pFix;
    CbNCC.Enabled := not pFix;
	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUNHAP.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;

    SetDisplayFormat(QrPHIEUNHAP, sysCurFmt);
    SetDisplayFormat(QrPHIEUNHAP, ['NGAY'], DateTimeFmt);

    SetCustomGrid('CHON_PHIEUNHAP', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormShow(Sender: TObject);
begin
	mSQL := QrPHIEUNHAP.SQL.Text;

	OpenDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);
    CbMADT.LookupValue := mNCC;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mNCC = '' then
    	try
    		CbMADT.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    EdTungay.Date := Date - sysLateDay;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNHAP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CmdRefreshExecute(Sender: TObject);
var
	s, Ncc, Kho: String;
    Tungay, Denngay: TDateTime;
begin
    Ncc     := CbMADT.LookupValue;
    Kho     := CbMAKHO.LookupValue;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mNCC <> Ncc)        or (mKHO <> Kho)         then
	begin
   	    mNCC := Ncc;
        mKHO := Kho;
        mTungay := Tungay;
        mDenngay := Denngay;

        with QrPHIEUNHAP do
        begin
			Close;

            s := mSQL;
            if mNCC <> '' then
            	s := s + ' and a.MADT=''' + mNCC + '''';
			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
            SQL.Text := s;
            SQL.Add(' order by NGAY, SCT');
			Open;
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
begin
    with QrPHIEUNHAP do
    begin
    	Parameters[0].Value := mTungay;
        Parameters[1].Value := mDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CbMADTChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbNCC.LookupValue := s;
    // Ten NCC
    1:
        CbMADT.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CbMADTExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CbMADTNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;


end.
