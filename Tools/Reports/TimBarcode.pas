﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TimBarcode;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, Db, ADODB;

type
  TFrmTimBarcode = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdBarcode: TEdit;
    QrBarcode: TADOQuery;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntFormShow(Sender: TObject);
  private
    mQuery: TADOQuery;
  public
  	procedure Execute(DataSet: TADOQuery);
  end;

var
  FrmTimBarcode: TFrmTimBarcode;

implementation

uses
	isMsg, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTimBarcode.Execute;
begin
    mQuery := DataSet;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTimBarcode.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTimBarcode.FormKeyPress(Sender: TObject; var Key: Char);
var
	s: String;
begin
	case Key of
    #13:
    begin
    	s := EdBarcode.Text;
        if s = '' then
            Exit;

        with QrBarcode do
        begin
        	Parameters[0].Value := s;
            Open;
            if IsEmpty then
            begin
            	Beep;
                ErrMsg('Không tìm thấy barcode ' + s);
            end
            else if not mQuery.Locate('MAVT', Fields[0].AsString, []) then
            begin
                Beep;
				Msg('Mặt hàng này thuộc ngành nhóm khác.'#13'Không có mặt trên lưới dữ liệu.');
            end;
			Close;
		end;
		EdBarcode.SelectAll;
		EdBarcode.SetFocus;
    end;
    #27:
    	Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTimBarcode.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTimBarcode.TntFormShow(Sender: TObject);
begin
	EdBarcode.SetFocus;
end;

end.
