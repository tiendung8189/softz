object FrmHoadonGTGTGetBill: TFrmHoadonGTGTGetBill
  Left = 594
  Top = 286
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Th'#234'm Danh S'#225'ch Bill'
  ClientHeight = 419
  ClientWidth = 705
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 705
    Height = 324
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GrBill: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 383
      Height = 324
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'SCT'#9'10'#9'S'#7889' bill'#9'F'
        'SOTIEN'#9'13'#9'S'#7889' ti'#7873'n'#9'F'
        'THUNGAN'#9'25'#9'Thu ng'#226'n'#9'F'
        'QUAY'#9'15'#9'Qu'#7847'y'#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alLeft
      DataSource = DsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgMultiSelect, dgShowCellHint]
      ParentFont = False
      ParentShowHint = False
      PopupMenu = PopList
      ShowHint = True
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnDblClick = CmdInsExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 16119285
    end
    object Panel2: TPanel
      Left = 383
      Top = 0
      Width = 40
      Height = 324
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object SpeedButton1: TSpeedButton
        Left = 8
        Top = 140
        Width = 23
        Height = 22
        Cursor = 1
        Action = CmdIns
        Flat = True
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00848484008484840084848400848484008484840084848400848484008484
          840084848400848484008484840084848400FFFFFF00FFFFFF00FFFFFF008484
          8400000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000084848400FFFFFF00848484000000
          0000008484009CCEFF009CCEFF009CCEFF00009CCE009CCEFF009CCEFF009CCE
          FF009CCEFF009CCEFF009CCEFF000084840000000000FFFFFF00848484000000
          00009CCEFF009CCEFF009CCEFF009CCEFF00009CCE0000000000009CCE00FFFF
          FF009CCEFF009CCEFF009CCEFF009CCEFF0000000000FFFFFF00848484000000
          00009CCEFF0063CEFF00009CCE00009CCE00009CCE000000000000000000009C
          CE00FFFFFF0063CEFF009CCEFF009CCEFF0000000000FFFFFF00848484000000
          000063CEFF000000000000000000000000000000000031FF3100009C31000000
          0000009CCE0063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF000000000031FF310031FF310031FF310031FF310031FF3100009C
          31000000000063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF000000000000000000000000000000000031FF3100009C31000000
          000063CEFF0063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF00FFFFFF0063CEFF0063CEFF0063CEFF00000000000000000063CE
          FF0063CEFF0063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF00FFFFFF0063CEFF0063CEFF0063CEFF000000000063CEFF0063CE
          FF0063CEFF0063CEFF0063CEFF000084840000000000FFFFFF00FFFFFF000000
          0000C6D6EF009CFFFF0063CEFF0063CEFF0063CEFF0063CEFF00319C9C000000
          000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
          FF0000000000C6D6EF009CFFFF0063CEFF0063CEFF009CCECE0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      end
      object SpeedButton2: TSpeedButton
        Left = 8
        Top = 172
        Width = 23
        Height = 22
        Cursor = 1
        Action = CmdDel
        Flat = True
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00848484008484840084848400848484008484840084848400848484008484
          840084848400848484008484840084848400FFFFFF00FFFFFF00FFFFFF008484
          8400000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000084848400FFFFFF00848484000000
          0000008484009CCEFF009CCEFF009CCEFF00009CCE009CCEFF009CCEFF009CCE
          FF009CCEFF009CCEFF009CCEFF000084840000000000FFFFFF00848484000000
          00009CCEFF009CCEFF00FFFFFF00009CCE0000000000009CCE009CCEFF009CCE
          FF009CCEFF009CCEFF009CCEFF009CCEFF0000000000FFFFFF00848484000000
          00009CCEFF00FFFFFF00009CCE000000000000000000009CCE00009CCE00009C
          CE0063CEFF0063CEFF009CCEFF009CCEFF0000000000FFFFFF00848484000000
          000063CEFF00009CCE0000000000009C310031FF310000000000000000000000
          00000000000063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF0000000000009C310031FF310031FF310031FF310031FF310031FF
          31000000000063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF00FFFFFF0000000000009C310031FF310000000000000000000000
          00000000000063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF00FFFFFF0063CEFF00000000000000000063CEFF0063CEFF0063CE
          FF0063CEFF0063CEFF0063CEFF0063CEFF0000000000FFFFFF00848484000000
          000063CEFF00FFFFFF0063CEFF0063CEFF000000000063CEFF0063CEFF0063CE
          FF0063CEFF0063CEFF0063CEFF000084840000000000FFFFFF00FFFFFF000000
          00009CFFFF009CFFFF0063CEFF0063CEFF0063CEFF0063CEFF00319C9C000000
          000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00000000009CFFFF009CFFFF0063CEFF0063CEFF009CCECE0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      end
    end
    object GrList: TStringGrid
      Left = 423
      Top = 0
      Width = 282
      Height = 324
      Hint = 'X'#243'a danh s'#225'ch (F3)'
      Align = alClient
      ColCount = 3
      DefaultColWidth = 70
      DefaultRowHeight = 19
      FixedCols = 0
      RowCount = 100
      FixedRows = 0
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 2
      OnDblClick = CmdDelExecute
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 705
    Height = 48
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object TntLabel16: TLabel
      Left = 437
      Top = 16
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TntLabel8: TLabel
      Left = 20
      Top = 16
      Width = 29
      Height = 16
      Alignment = taRightJustify
      Caption = 'Qu'#7847'y'
    end
    object CbNgay: TwwDBDateTimePicker
      Left = 472
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 2
    end
    object TntBitBtn1: TBitBtn
      Left = 596
      Top = 12
      Width = 93
      Height = 25
      Cursor = 1
      Hint = 'C'#7853'p nh'#7853't danh s'#225'ch'
      Action = CmdGetList
      Caption = 'C'#7853'p nh'#7853't'
      DoubleBuffered = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000808000808000
        808000AFA59FB0A6A1B0A6A1B2A8A3B2A8A3B2A8A3B8AEA9B8AEA9B8AEA9B8AE
        A9808000808000808000808000808000A5978E7D6A62806D658471698471698F
        7C748F7C74927F7896837C99867F9D8A83AA9C96808000808000808000808000
        B8A89EE8DED6E7DDD6BDB4C8E7DDD6E8DED6E8DED6E8DED6E8DED6E8DED6E8DE
        D6BAABA4808000808000808000808000BFB0A6F3ECE5D2CCE52120F6A39DD0F3
        ECE5F3ECE5F3ECE5F3ECE5F3ECE5F3ECE5C0B1AC808000808000808000808000
        C0B1ACF6F0EB908DEF3837FC5554E9EAE4E0F6F0EBF6F0EBF6F0EBF6F0EBF6F0
        EBC0B1AC808000808000808000808000C0B1ACE7E3EB6161FAB2AFEC6A6AFBB0
        ACD8FAF4F1FAF4F1FAF4F1FAF4F1FAF4F1C5B9B2808000808000808000808000
        C8BAB49F9EF59C99F5FAF4F1D5D2F78180F0E2DEE1FDF7F5FDF7F5FDF7F5FDF7
        F5C5B9B2808000808000808000808000C8BAB4FDF9F8FDF9F8FDF9F8FDF7F5BB
        BAFA9998E3F2EEECFDF7F5FDF9F8FDF9F8C5B9B2808000808000808000808000
        C8BAB4FDF9F8FDF7F5FDF9F8FDF9F8FDF7F5A09FFAA19ED9FAF4F1FDF9F8FDF7
        F5C5B9B2808000808000808000808000CEC0B8FDF7F5FDF7F5FDF7F5FDF7F5FD
        F7F5F7F1F58A89F8A19ED9FAF4F1FDF7F5C4B6B1808000808000808000808000
        CEC0B8FAF4F1FAF4F1FAF4F1FAF4F1FAF4F1FAF4F1F6EFF28180F08A86D8F2EE
        ECBFB0AD808000808000808000808000CEC0B8F6F0EBFAF4F1F6F0EBFAF4F1F6
        F0EBFAF4F1FAF4F1F7EFEF9C99F56663DEADA0A5808000808000808000808000
        C8BAB4E7DDD6C0B1ACC0B1ACC0B1ACC0B1ACC0B1ACC0B1ACC0B1ACB8AEA9A095
        B2ADA0A5808000808000808000808000C0B1ACB09D97A08881987F73987F7398
        7F73967D71987F73A1887EA99189927F78A69895808000808000808000808000
        808000808000808000B8AEA9A99189AE9589BBA196B9A39CC4B6B18080008080
        00808000808000808000808000808000808000808000808000808000808000B0
        A6A1B4A6A2808000808000808000808000808000808000808000}
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 3
    end
    object CbMAQUAY: TwwDBLookupCombo
      Left = 56
      Top = 12
      Width = 91
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'QUAY'#9'12'#9'QUAY'#9'F'
        'GHICHU'#9'32'#9'GHICHU'#9'F')
      LookupTable = FrmHoadonGTGT.QrDmQuay
      LookupField = 'QUAY'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbMAQUAYChange
      OnExit = CbMAQUAYChange
      OnNotInList = CbMAQUAYNotInList
ButtonEffects.Transparent=True
    end
    object CbTENQUAY: TwwDBLookupCombo
      Tag = 1
      Left = 148
      Top = 12
      Width = 267
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'GHICHU'#9'32'#9'GHICHU'#9'F'
        'QUAY'#9'12'#9'QUAY'#9'F')
      LookupTable = FrmHoadonGTGT.QrDmQuay
      LookupField = 'QUAY'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbMAQUAYChange
      OnExit = CbMAQUAYChange
      OnNotInList = CbMAQUAYNotInList
ButtonEffects.Transparent=True
    end
  end
  object CmdReturn: TBitBtn
    Left = 230
    Top = 384
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdOK
    Caption = 'Ti'#7871'p t'#7909'c'
    DoubleBuffered = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
  end
  object BtnCancel: TBitBtn
    Left = 374
    Top = 384
    Width = 101
    Height = 25
    Cursor = 1
    Caption = 'B'#7887' qua'
    DoubleBuffered = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Kind = bkCancel
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 3
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 128
    Top = 124
    object CmdIns: TAction
      Hint = 'Add Item'
      ShortCut = 113
      OnExecute = CmdInsExecute
    end
    object CmdDel: TAction
      Hint = 'Remove Item'
      ShortCut = 114
      OnExecute = CmdDelExecute
    end
    object CmdClear: TAction
      Caption = 'Clear All'
      ShortCut = 119
      OnExecute = CmdClearExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin      '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'Filter'
      ShortCut = 16460
    end
    object CmdClearFilter: TAction
      Caption = 'Clear Filtered'
    end
    object CmdOK: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      OnExecute = CmdOKExecute
    end
    object CmdGetList: TAction
      Caption = 'C'#7853'p nh'#7853't'
      Hint = 'C'#7853'p nh'#7853't danh s'#7887'ch'
      OnExecute = CmdGetListExecute
    end
  end
  object PopList: TAdvPopupMenu
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 176
    Top = 140
    object Search1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
  end
  object LIST_BILL: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'HOADONGTGT_LISTBILL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@QUAY'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 162
    Top = 182
    object LIST_BILLSCT: TWideStringField
      DisplayLabel = 'S'#232' bill'
      FieldName = 'SCT'
    end
    object LIST_BILLSOTIEN: TFloatField
      DisplayLabel = 'Th'#181'nh ti'#210'n'
      FieldName = 'SOTIEN'
    end
    object LIST_BILLTHUNGAN: TWideStringField
      DisplayLabel = 'Thu ng'#169'n'
      FieldKind = fkLookup
      FieldName = 'THUNGAN'
      LookupDataSet = QrUSER
      LookupKeyFields = 'FULLNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CREATE_BY'
      Size = 50
      Lookup = True
    end
    object LIST_BILLQUAY: TWideStringField
      DisplayWidth = 2
      FieldName = 'QUAY'
      Size = 2
    end
    object LIST_BILLCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object LIST_BILLKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object QrUSER: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'SYS_USER'
      ' where'#9'USERNAME <> '#39'ADMIN'#39
      '   and'#9'isnull(IS_GROUP, 0) = 0'
      'order by USERNAME'
      '')
    Left = 246
    Top = 187
  end
  object DsList: TDataSource
    DataSet = LIST_BILL
    Left = 162
    Top = 211
  end
end
