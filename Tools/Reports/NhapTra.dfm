object FrmNhaptra: TFrmNhaptra
  Left = 308
  Top = 68
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Nh'#7853'p Tr'#7843
  ClientHeight = 573
  ClientWidth = 1016
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1016
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 1016
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1016
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 69
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 77
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 146
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 215
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 223
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 292
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn2: TToolButton
      Left = 300
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object SepChecked: TToolButton
      Left = 369
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 377
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton10: TToolButton
      Left = 446
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 454
      Top = 0
      Cursor = 1
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object ToolButton5: TToolButton
      Left = 538
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 546
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 1016
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 1008
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1008
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Thanh to'#225'n'#9'F'
          'MADT'#9'10'#9'M'#227#9'F'#9'Kh'#225'ch h'#224'ng'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 1008
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 1008
        inherited Panel1: TPanel
          Width = 1008
          ExplicitWidth = 1008
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 139
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 287
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object Label4: TLabel
          Left = 619
          Top = 38
          Width = 30
          Height = 16
          Alignment = taRightJustify
          Caption = 'L'#253' do'
        end
        object Label5: TLabel
          Left = 588
          Top = 62
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
        end
        object LbNHAPCUA: TLabel
          Left = 39
          Top = 62
          Width = 66
          Height = 16
          Alignment = taRightJustify
          Caption = 'Kh'#225'ch h'#224'ng'
        end
        object Label31: TLabel
          Left = 52
          Top = 38
          Width = 53
          Height = 16
          Alignment = taRightJustify
          Caption = 'Kho h'#224'ng'
        end
        object Label8: TLabel
          Left = 584
          Top = 86
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
        end
        object TntLabel4: TLabel
          Left = 248
          Top = 86
          Width = 93
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y thanh to'#225'n'
        end
        object TntLabel5: TLabel
          Left = 172
          Top = 86
          Width = 37
          Height = 16
          Caption = '(ng'#224'y)'
        end
        object TntLabel6: TLabel
          Left = 18
          Top = 86
          Width = 87
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#7841'n thanh to'#225'n'
        end
        object TntLabel9: TLabel
          Left = 56
          Top = 110
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object Label7: TLabel
          Left = 541
          Top = 14
          Width = 108
          Height = 16
          Alignment = taRightJustify
          Caption = 'Phi'#7871'u giao h'#224'ng s'#7889
        end
        object LbDrcStatus: TLabel
          Left = 555
          Top = 110
          Width = 94
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#236'nh tr'#7841'ng phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object BtCongno: TSpeedButton
          Left = 506
          Top = 58
          Width = 23
          Height = 22
          Cursor = 1
          Hint = 'Xem c'#244'ng n'#7907' kh'#225'ch h'#224'ng'
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000F0C575FFF0C5
            75FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C5
            75FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFF3F9200FFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFF3F92
            00FFFFFFFFFF3F9200FFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FF3F9200FFFFFF
            FFFFFFFFFFFFC37C00FF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFC37C00FFFFFF
            FFFFC37C00FFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFC37C
            00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFF0C575FFFFFF
            FFFF1D25DAFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFF1D25DAFF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFF3F9200FF1D25DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFF3F9200FFFFFFFFFFFFFFFFFF1D25DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D25DAFF1D25DAFF1D25DAFF1D25
            DAFF1D25DAFF1D25DAFF1D25DAFF1D25DAFF1D25DAFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          OnClick = BtCongnoClick
        end
        object EdSCT: TwwDBEdit
          Left = 344
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object wwDBLookupCombo1: TwwDBLookupCombo
          Left = 656
          Top = 34
          Width = 181
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DGIAI'#9'25'#9'DGIAI'#9'F'
            'MA'#9'0'#9'MA'#9'F')
          DataField = 'LYDO'
          DataSource = DsNX
          LookupTable = DataMain.QrPTNHAPTRA
          LookupField = 'MA'
          Options = [loColLines]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 656
          Top = 58
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_GIAO'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbKHO: TwwDBLookupCombo
          Left = 168
          Top = 34
          Width = 361
          Height = 22
          TabStop = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENKHO'#9'40'#9'T'#234'n'#9'F'
            'MAKHO'#9'6'#9'M'#227#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
ButtonEffects.Transparent=True
        end
        object CbMAKHO: TwwDBLookupCombo
          Left = 112
          Top = 34
          Width = 53
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MAKHO'#9'6'#9'M'#227#9'F'
            'TENKHO'#9'40'#9'T'#234'n'#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          OnBeforeDropDown = CbMAKHOBeforeDropDown
          OnCloseUp = CbMAKHOCloseUp
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object CbMADT: TwwDBEdit
          Left = 112
          Top = 58
          Width = 101
          Height = 22
          Ctl3D = False
          DataField = 'MADT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 656
          Top = 82
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_NHAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit7: TwwDBEdit
          Left = 348
          Top = 82
          Width = 77
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'CALC_NGAY_TTOAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit3: TwwDBEdit
          Left = 112
          Top = 82
          Width = 53
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'HAN_TTOAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBMemo2: TDBMemo
          Left = 112
          Top = 106
          Width = 417
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
        end
        object EdTENDT: TwwDBEdit
          Left = 216
          Top = 58
          Width = 289
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'LK_TENDT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit4: TwwDBEdit
          Left = 656
          Top = 10
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'PHIEUGIAOHANG'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbDrcStatus: TwwDBComboBox
          Left = 656
          Top = 106
          Width = 181
          Height = 22
          TabStop = False
          ShowButton = False
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          Color = 15794175
          DataField = 'DRC_STATUS'
          DataSource = DsNX
          DropDownCount = 8
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ItemHeight = 0
          Items.Strings = (
            '1. '#272'ang nh'#7853'p li'#7879'u'#9'1'
            '2. Ho'#224'n t'#7845't, ch'#7901' chuy'#7875'n '#273'i'#9'2'
            '3. '#272#227' chuy'#7875'n '#273'i'#9'3')
          ParentFont = False
          ReadOnly = True
          Sorted = False
          TabOrder = 13
          UnboundDataType = wwDefault
        end
      end
      object PaHoadon: TisPanel
        Left = 0
        Top = 139
        Width = 1008
        Height = 57
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' .: H'#243'a '#273#417'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PaHoadon2: TPanel
          Left = 2
          Top = 18
          Width = 1004
          Height = 37
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object TntLabel2: TLabel
            Left = 38
            Top = 10
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' h'#243'a '#273#417'n'
          end
          object TntLabel3: TLabel
            Left = 371
            Top = 10
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object TntLabel8: TLabel
            Left = 224
            Top = 10
            Width = 41
            Height = 16
            Alignment = taRightJustify
            Caption = 'K'#253' hi'#7879'u'
          end
          object LbGia: TLabel
            Left = 624
            Top = 12
            Width = 54
            Height = 16
            Alignment = taRightJustify
            Caption = 'H'#236'nh th'#7913'c'
          end
          object EdSOHD: TwwDBEdit
            Left = 110
            Top = 6
            Width = 101
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SO'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit2: TwwDBEdit
            Left = 272
            Top = 6
            Width = 89
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SERI'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 406
            Top = 6
            Width = 93
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = 15794175
            DataField = 'HOADON_NGAY'
            DataSource = DsNX
            Epoch = 1950
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 2
          end
          object CbGia: TwwDBLookupCombo
            Left = 685
            Top = 6
            Width = 150
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DGIAI'#9'0'#9'DGIAI'#9'F')
            DataField = 'HINHTHUC_GIA'
            DataSource = DsNX
            LookupTable = DataMain.QrHINHTHUC_GIA
            LookupField = 'MA'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = False
            ShowMatchText = True
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 196
        Width = 1008
        Height = 253
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 1008
          Height = 237
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'B1'#9'7'#9'In tem'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'TENVT'#9'30'#9'T'#234'n h'#224'ng'#9'T'
            'LK_DVT1'#9'8'#9'BOX'#9'T'
            'LK_QD1'#9'3'#9'='#9'T'
            'DVT'#9'8'#9'SKU'#9'T'
            'SOLUONG2'#9'10'#9'BOX'#9'F'#9'S'#7889' l'#432#7907'ng'
            'SOLUONG'#9'10'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
            'DONGIA2'#9'10'#9'BOX'#9'F'#9#272#417'n gi'#225
            'DONGIA'#9'10'#9'SKU'#9'F'#9#272#417'n gi'#225
            'TL_CK'#9'8'#9'%C.K'#9'F'
            'SOTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'T'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
      object PaTotal: TPanel
        Left = 0
        Top = 449
        Width = 1008
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 3
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object PaTotal1: TPanel
          Left = 2
          Top = 2
          Width = 579
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            579
            43)
          object Label11: TLabel
            Left = 468
            Top = 2
            Width = 78
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' sau C.K'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 422
            Top = 2
            Width = 33
            Height = 13
            Anchors = [akTop, akRight]
            Caption = '% C.K'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdTLCK: TwwDBEdit
            Left = 422
            Top = 17
            Width = 41
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'TL_CK_HD'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit1: TwwDBEdit
            Left = 466
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SOTIEN2'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaTotal2: TPanel
          Left = 581
          Top = 2
          Width = 249
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            249
            43)
          object BtThue: TSpeedButton
            Left = 111
            Top = 17
            Width = 23
            Height = 23
            Cursor = 1
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = BtThueClick
            ExplicitLeft = 287
          end
          object Label13: TLabel
            Left = 136
            Top = 2
            Width = 87
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' sau thu'#7871
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label19: TLabel
            Left = 1
            Top = 2
            Width = 78
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Ti'#7873'n thu'#7871' VAT'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdTienVAT: TwwDBEdit
            Left = 1
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'THUE'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit6: TwwDBEdit
            Left = 136
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'THANHTIEN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaTotal3: TPanel
          Left = 830
          Top = 2
          Width = 176
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          DesignSize = (
            176
            43)
          object Label24: TLabel
            Left = 45
            Top = 2
            Width = 100
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
            FocusControl = EdTriGiaTT
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 1
            Top = 2
            Width = 33
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'C.l'#7879'ch'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdCL: TwwDBEdit
            Left = 1
            Top = 17
            Width = 41
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'CL_THUE'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdTriGiaTT: TwwDBEdit
            Left = 45
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'THANHTOAN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 874
    Top = 46
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 874
    ExplicitTop = 46
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 354
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Hint = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdFromOrder: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' phi'#7871'u xu'#7845't'
      OnExecute = CmdFromOrderExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdThamkhaoGia: TAction
      Category = 'DETAIL'
      Caption = 'Tham kh'#7843'o gi'#225
      OnExecute = CmdThamkhaoGiaExecute
    end
    object CmdDmkh: TAction
      Caption = 'Kh'#225'ch h'#224'ng'
      OnExecute = CmdDmkhExecute
    end
    object CmdDmvt: TAction
      Caption = 'H'#224'ng h'#243'a'
      OnExecute = CmdDmvtExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 124
    Top = 354
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    AfterEdit = QrNXAfterEdit
    BeforePost = QrNXBeforePost
    AfterPost = QrNXAfterPost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'CHUNGTU'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY < :NGAYC + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 724
    Top = 372
    object QrNXXOA: TWideStringField
      DisplayLabel = 'H'#361'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 20
      FieldName = 'SCT'
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Thanh to'#225'n'
      DisplayWidth = 16
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 2
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXTU_MAKHO: TWideStringField
      DisplayWidth = 2
      FieldName = 'TU_MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrNXSODDH: TWideStringField
      DisplayWidth = 20
      FieldName = 'SODDH'
      Visible = False
      FixedChar = True
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXTL_CK_HD: TFloatField
      DisplayWidth = 10
      FieldName = 'TL_CK_HD'
      Visible = False
      OnChange = QrNXTL_CK_HDChange
    end
    object QrNXCHIETKHAU_HD: TFloatField
      DisplayWidth = 10
      FieldName = 'CHIETKHAU_HD'
      Visible = False
    end
    object QrNXTHUE: TFloatField
      DisplayWidth = 10
      FieldName = 'THUE'
      Visible = False
    end
    object QrNXTHUE_5: TFloatField
      FieldName = 'THUE_5'
    end
    object QrNXTHUE_10: TFloatField
      FieldName = 'THUE_10'
    end
    object QrNXTHUE_OR: TFloatField
      FieldName = 'THUE_OR'
    end
    object QrNXCL_THUE: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_THUE'
      Visible = False
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXSOTIEN: TFloatField
      DisplayLabel = 'Ch'#432'a thu'#7871
      DisplayWidth = 10
      FieldName = 'SOTIEN'
      Visible = False
    end
    object QrNXCL_SOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_SOTIEN'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXPTTT: TWideStringField
      FieldName = 'PTTT'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' kh'#225'ch h'#224'ng'
      FieldName = 'MADT'
      OnChange = QrNXMADTChange
      Size = 15
    end
    object QrNXLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n kh'#225'ch h'#224'ng'
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMKH
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 50
      Lookup = True
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXHAN_TTOAN: TIntegerField
      FieldName = 'HAN_TTOAN'
      Visible = False
    end
    object QrNXDA_TTOAN: TBooleanField
      FieldName = 'DA_TTOAN'
      Visible = False
    end
    object QrNXLK_HAN_TTOAN: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_HAN_TTOAN'
      LookupDataSet = DataMain.QrDMKH
      LookupKeyFields = 'MADT'
      LookupResultField = 'HAN_TTOAN'
      KeyFields = 'MADT'
      Visible = False
      Lookup = True
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXTC_SOTIEN: TFloatField
      FieldName = 'TC_SOTIEN'
      Visible = False
    end
    object QrNXHOADON_SO: TWideStringField
      DisplayLabel = 'S'#232' ch'#248'ng t'#245
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrNXHOADON_SERI: TWideStringField
      DisplayLabel = 'S'#170'ri'
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrNXHOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
    object CALC_NGAY_TTOAN: TDateTimeField
      DisplayLabel = 'Ng'#224'y thanh to'#225'n'
      FieldKind = fkCalculated
      FieldName = 'CALC_NGAY_TTOAN'
      Calculated = True
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
    end
    object QrNXHINHTHUC_GIA: TWideStringField
      FieldName = 'HINHTHUC_GIA'
      OnChange = QrNXHINHTHUC_GIAChange
      OnValidate = QrNXHINHTHUC_GIAValidate
      Size = 15
    end
    object QrNXSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrNXSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
      Visible = False
    end
    object QrNXTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
      Visible = False
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXPHIEUGIAOHANG: TWideStringField
      FieldName = 'PHIEUGIAOHANG'
      Size = 30
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXCALC_SOTIEN_SAUCK_MH: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN_SAUCK_MH'
      Calculated = True
    end
    object QrNXKHOA2: TGuidField
      FieldName = 'KHOA2'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      Size = 1
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 10
    end
    object QrNXLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrPTNHAPTRA
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from CHUNGTU_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 756
    Top = 372
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      OnValidate = QrCTMAVTValidate
      FixedChar = True
      Size = 15
    end
    object QrCTLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 46
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTLK_DVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTLK_QD1: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_QD1'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'QD1'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DVT1: TWideStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'LK_DVT1'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT1'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIATK: TFloatField
      DisplayLabel = 'Gi'#225' g'#7889'c'
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'LK_GIANHAP'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAP'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIANHAPVAT: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIANHAPVAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAPVAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIASI: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIASI'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIASI'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIASIVAT: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIASIVAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIASIVAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIASI'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_LOAITHUE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LOAITHUE'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'LOAITHUE'
      KeyFields = 'MAVT'
      Size = 5
      Lookup = True
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTDONGIA_REF: TFloatField
      FieldName = 'DONGIA_REF'
    end
    object QrCTDONGIA_REF2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DONGIA_REF2'
      Calculated = True
    end
    object QrCTDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 10
      FieldName = 'DONGIA'
      OnChange = QrCTDONGIAChange
    end
    object QrCTDONGIA2: TFloatField
      FieldName = 'DONGIA2'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTSOLUONG2: TFloatField
      FieldName = 'SOLUONG2'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrCTSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
    end
    object QrCTTL_CK: TFloatField
      DisplayLabel = '%C.K'
      DisplayWidth = 7
      FieldName = 'TL_CK'
      OnChange = QrCTSOTIENChange
      OnValidate = QrCTTL_CKValidate
    end
    object QrCTTL_CK_HD: TFloatField
      FieldName = 'TL_CK_HD'
      OnChange = QrCTSOTIENChange
    end
    object QrCTCHIETKHAU: TFloatField
      DisplayLabel = 'Chi'#7871't kh'#7845'u'
      DisplayWidth = 12
      FieldName = 'CHIETKHAU'
      OnChange = QrCTSOTIENChange
      OnValidate = QrCTTL_CKValidate
    end
    object QrCTCHIETKHAU_HD: TFloatField
      FieldName = 'CHIETKHAU_HD'
    end
    object QrCTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      OnChange = QrCTLOAITHUEChange
      Size = 15
    end
    object QrCTLK_TENTHUE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTHUE'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'TENLT'
      KeyFields = 'LOAITHUE'
      Size = 200
      Lookup = True
    end
    object QrCTLK_VAT_RA: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_RA'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_RA'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTLK_VAT_VAO: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_VAO'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_VAO'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
      Visible = False
      OnChange = QrCTSOTIENChange
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
      Visible = False
    end
    object QrCTTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrCTTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrCTTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrCTTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrCTLK_TENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 25
      Lookup = True
    end
    object QrCTEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TINHTRANG'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 724
    Top = 424
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 756
    Top = 424
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 290
    Top = 312
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 354
    Top = 312
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 382
    Top = 428
    object MenuItem1: TMenuItem
      Action = CmdFromOrder
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Splithtmthng1: TMenuItem
      Action = CmdSapthutu
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object hamkhoginhp1: TMenuItem
      Action = CmdThamkhaoGia
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'CHIETKHAU_MH'
      'CHIETKHAU_HD'
      'THUE_5'
      'THUE_10'
      'THUE_OR'
      'THUE'
      'SOTIEN'
      'SOTIEN1'
      'SOTIEN2'
      'THANHTIEN')
    DetailFields.Strings = (
      'SOLUONG'
      'CHIETKHAU'
      'CHIETKHAU_HD'
      'TIEN_THUE_5'
      'TIEN_THUE_10'
      'TIEN_THUE_OR'
      'TIEN_THUE'
      'SOTIEN'
      'SOTIEN1'
      'SOTIEN2'
      'THANHTIEN')
    Left = 418
    Top = 312
  end
  object CHUNGTU_LAYPHIEU: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'CHUNGTU_LAYPHIEU;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    Left = 678
    Top = 340
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 176
    Top = 408
    object MenuItem3: TMenuItem
      Tag = 1
      Action = CmdDmvt
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Tag = 2
      Action = CmdDmkh
      AutoHotkeys = maManual
      AutoLineReduction = maManual
    end
  end
end
