﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit GmsRep;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ActnList, StdCtrls, Buttons, ComCtrls, ExtCtrls, Grids,
  Wwdbgrid2, Db, ADODB,
  Shellapi, crCommon, Wwdbigrd, Wwdbgrid, ImgList;

type
  TFrmGmsRep = class(TForm)
    ActionList1: TActionList;
    CmdRun: TAction;
    DsRep: TDataSource;
    Panel4: TPanel;
    PgGroup: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet11: TTabSheet;
    GrList: TwwDBGrid2;
    Bevel4: TBevel;
    Panel21: TPanel;
    PaParam: TPanel;
    Bevel8: TBevel;
    Bevel1: TBevel;
    PaCMD: TPanel;
    Image1: TImage;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BtnCalc: TBitBtn;
    CmdCalc: TAction;
    TabSheet1: TTabSheet;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    CmdChonDsma: TAction;
    QrREP: TADOStoredProc;
    CmdReread: TAction;
    CmdRuntimeEdit: TAction;
    CmdChonDsNCC: TAction;
    CmdChonDsKH: TAction;
    CmdTestAll: TAction;
    cmdChonDsHH: TAction;
    CmdChonDsNgNh: TAction;
    CmdChonDsKho: TAction;
    ImageList1: TImageList;
    procedure CmdRunExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure QrREPAfterScroll(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure PgGroupChange(Sender: TObject);
    procedure CmdCalcExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdRereadExecute(Sender: TObject);
    procedure CmdRuntimeEditExecute(Sender: TObject);
    procedure CmdTestAllExecute(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    mCats: string;
    mGroup: Integer;
  	mStockControl, mMustCalc, mFixLogon, mIsTemplate: Boolean;
    mFields: TStrings;			// List of criteria fields
    mFrames: array of TCrFrame;	// List of criteria frames

    procedure OpenReports;
    function  Proc(fs: String = ''): Boolean;
  public
  	function  Execute(group: Integer = 0): Boolean;
    function  IsStockControl: Boolean;
    procedure SetMustCalc(pValue: Boolean);

  end;

var
  FrmGmsRep: TFrmGmsRep;

implementation

{$R *.DFM}

uses
	MainData, isDb, RepEngine, ExCommon, isStr, isMsg, Rights,
    isLib, isFile,
    isCommon,crKho, crDs_Kho, crTuden, crTon, crUID,
    crNgay, crQuy, crNam, crThoigian, crMANCC, crMAKH, crDS_NCC, crMAVIP,
    crLOAI_HD, crDS_KH, crCT_TH, crTOP, crMAVT, crSILE, crDS_NHOMVT, crMAHH,
    crNGANHNHOM, crTHANGNAM, OfficeData, crDS_LYDO_NK, crDS_LYDO_XK, crDS_USER,
    crDS_LOC, crDS_NV, crSOTHANG, isOdbc, crLOC;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGmsRep.Execute;
var
	i, n, m: Integer;
begin
  	mTrigger := False;
    mFixLogon := False;
    mCats := FlexConfigString('Sams', 'Rp Group' + Iif(group > 0, ' ' + IntToStr(group), ''));

    OpenReports;
    if QrREP.IsEmpty then
	begin
    	Result := False;
//		CloseDataSets(DataMain.Conn);
    end
    else
    begin
    	Result := True;
        // List of criteria fields
        mFields := TStringList.Create;
        mFields.Text :=
            'LOC'#13 +
            'DS_LOC'#13 +
            'KHO'#13 +
            'DS_KHO'#13 +
            'NGAY'#13 +
            'TUNGAY'#13 +
            'GIA'#13 +
            'THANGNAM'#13 +
            'NAM'#13 +
            'QUY'#13 +
            'THOIGIAN'#13 +
            'MANCC'#13 +
            'MAKH'#13 +
            'DS_NCC'#13 +
            'DS_KH'#13 +
            'DS_NV'#13 +
            'THUE'#13 +
            'SILE'#13+
            'CT_TH'#13 +
            'TOP10'#13 +
            'NHOMVT'#13 +
            'DS_NHOMVT'#13 +
            'MAVIP'#13 +
            'DS_LYDO_NK'#13 +
            'DS_LYDO_XK'#13 +
            'DS_USER'#13 +
            'SOTHANG'#13 +
            'MAVT'#13 +
            'MAHH'#13;

        // Create criteria frames
        n := mFields.Count;

        SetLength(mFrames, n);
        mFrames[mFields.IndexOf('LOC')]     := TframeCrLOC.Create(Self);
        mFrames[mFields.IndexOf('DS_LOC')]  := TframeDS_LOC.Create(Self);
        mFrames[mFields.IndexOf('KHO')]     := TframeCRKho.Create(Self);
        mFrames[mFields.IndexOf('DS_KHO')]  := TframeDs_Kho.Create(Self);
        mFrames[mFields.IndexOf('NGAY')]    := TframeCrNgay.Create(Self);
        mFrames[mFields.IndexOf('TUNGAY')]  := TframeTuden.Create(Self);
        mFrames[mFields.IndexOf('GIA')]     := TframeTON.Create(Self);
        mFrames[mFields.IndexOf('THANGNAM')]:= TframeTHANGNAM.Create(Self);
        mFrames[mFields.IndexOf('NAM')]     := TframeNam.Create(Self);
        mFrames[mFields.IndexOf('QUY')]     := TframeQuy.Create(Self);
        mFrames[mFields.IndexOf('THOIGIAN')]:= TframeThoigian.Create(Self);
        mFrames[mFields.IndexOf('MANCC')]   := TframeMANCC.Create(Self);
        mFrames[mFields.IndexOf('MAKH')]    := TframeMAKH.Create(Self);
        mFrames[mFields.IndexOf('DS_NCC')]  := TframeDS_NCC.Create(Self);
        mFrames[mFields.IndexOf('DS_KH')]   := TframeDS_KH.Create(Self);
        mFrames[mFields.IndexOf('DS_NV')]   := TframeDS_NV.Create(Self);
        mFrames[mFields.IndexOf('THUE')]    := TframeLOAI_HD.Create(Self);
        mFrames[mFields.IndexOf('SILE')]    := TframeSILE.Create(Self);
        mFrames[mFields.IndexOf('CT_TH')]   := TframeCT_TH.Create(Self);
        mFrames[mFields.IndexOf('TOP10')]   := TframeTOP.Create(Self);
        mFrames[mFields.IndexOf('NHOMVT')]  := TframeNGANHNHOM.Create(Self);
        mFrames[mFields.IndexOf('DS_NHOMVT')]:= TframeDS_NHOMVT.Create(Self);
        mFrames[mFields.IndexOf('MAVIP')]    := TframeMAVIP.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_NK')]    := TframeDS_LYDO_NK.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_XK')]    := TframeDS_LYDO_XK.Create(Self);
        mFrames[mFields.IndexOf('DS_USER')]    := TframeDS_USER.Create(Self);
        mFrames[mFields.IndexOf('SOTHANG')]    := TframeSOTHANG.Create(Self);
        mFrames[mFields.IndexOf('MAVT')]    := TframeMAVT.Create(Self);
        mFrames[mFields.IndexOf('MAHH')]    := TframeMAHH.Create(Self);



        n := Length(mFrames) - 1;
        m := PaParam.Width;
        for i := 0 to n do
            with mFrames[i] do
            begin
                Visible := False;	// Make it surely
                Parent := PaParam;
                Left := 0;
                Width := m;
                Anchors := Anchors + [akLeft, akRight];
            end;
        mIsTemplate := QrREP.FindField('TEMPLATE') <> nil;
        //ShowModal;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.SetMustCalc(pValue: Boolean);
begin
    mMustCalc := pValue;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGmsRep.IsStockControl: Boolean;
begin
    Result := mStockControl;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	// Final
    RegWrite('Rps\Rp Group ' + IntToStr(mGroup), ['Page', 'Id'],
    	[PgGroup.ActivePageIndex, QrRep.FieldByName('IDX').AsInteger]);

	// Done
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    mFields.Free;
    SetLength(mFrames, 0);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    ResetRowColor([GrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.OpenReports;
begin
    with QrREP do
    begin
    	Close;
    	Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := mCats;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormShow(Sender: TObject);
var
	m, n, i: Integer;
    txt, ls: TStrings;
begin
    TMyForm(Self).Init2;
	LoadCustomIcon;

    DbeInitial;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');

    // Stock control
	mStockControl := rCanEdit(DataMain.GetRights('SZ_TONTT', False));

    Execute(1);

    // Show / hide groups
    txt := TStringList.Create;
    if sysEnglish then
	    txt.Text := isStrReplace(FlexConfigString('Sams', 'Report English Categories'), ';', #13)
    else
	    txt.Text := isStrReplace(FlexConfigString('Sams', 'Report Categories'), ';', #13);

    ls := TStringList.Create;
    if mCats <> '' then
		ls.Text := isStrReplace(mCats, ';', #13)
	else
        for i := 0 to txt.Count - 1 do
			ls.Add(txt.Names[i]);

    n := 0;
    with QrRep, PgGroup do
    begin
    	for i := 0 to ls.Count - 1 do
        begin
        	m := StrToInt(ls[i]);
		    if Locate('CAT', m, []) and (n < PageCount) then
            begin
	            Pages[n].Tag := m;
	            Pages[n].Caption := txt.Values[ls[i]];
                Inc(n);
            end;
        end;
        txt.Free;
	    ls.Free;

		while n < PageCount do
	    begin
		    Pages[n].TabVisible := False;
	    	Inc(n);
    	end;
    end;
    ClearWait;

    // Seek to the last called report
    m := RegRead('Rps\Rp Group ' + IntToStr(mGroup) , 'Page', 0);
    n := RegRead('Id', -1);

    with PgGroup do
    begin
	    if PageCount <= m then
	    	m := 0;

//		while (m < PageCount) and (not Pages[m].TabVisible) do
//        	Inc(m);
        if not Pages[m].TabVisible then
            m := 0;

		ActivePageIndex := m;
	end;
    PgGroup.OnChange(Nil);

    with QrRep do
    begin
    	Tag := 1;
    	if not Locate('IDX', n, []) then
        	First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.QrREPAfterScroll(DataSet: TDataSet);
var
    i, n1, n2, y: Integer;
begin
	if QrRep.Tag = 0 then
    	Exit;

	n1 := Length(mFrames) - 1;
    n2 := mFields.Count - 1;
    y := 0;
	with QrRep do
    begin
    	if FieldByName('SYS').AsBoolean then
        begin
	    	for i := 0 to n1 do
                mFrames[i].Visible := False;
        	Exit;
        end;

    	for i := 0 to n2 do
        	if i <= n1 then
                with mFrames[i] do
                begin
                    Visible := FieldByName(mFields[i]).AsBoolean;
                    if Visible then
                    begin
                        Top := y;
                        Inc(y, Height);
                    end;
                end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if QrRep.FieldByName('SYS').AsBoolean then
    	with AFont do
        begin
            Style := [fsBold];
            if Highlight then
	        	Color := clWhite
            else
    	    	Color := clGreen;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_THESAME_MONTH = 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME = 'Thời gian báo cáo không hợp lệ.';

function TFrmGmsRep.Proc(fs: String = ''): Boolean;
var
	sRep, sCap, mTemplate: String;
    i, n, def: Integer;
    mVar: array of Variant;
begin
    // Prepare
	with QrRep do
    begin
    	sRep := Trim(FieldByName('REP_NAME').AsString);
        if fs = '' then
	    	//sCap := isProperCase(isStripToneMark(Trim(FieldByName('DESC').AsString)));
            sCap := '';//((Trim(FieldByName('DESC').AsString)));

	    // Within month
        if FieldByName('TRONGTHANG').AsBoolean then
        begin
            if FieldByName('TUNGAY').AsBoolean then
                if not mFrames[mFields.IndexOf('TUNGAY')].Validate then
                    Abort;

            if FieldByName('THOIGIAN').AsBoolean then
                if not mFrames[mFields.IndexOf('THOIGIAN')].Validate then
                    Abort;
        end;
    end;

    if sRep = '' then
    begin
    	GrList.SetFocus;
        Result := False;
    	Exit;
    end;

    // Stock report
    if QrRep.FieldByName('GIA').AsBoolean then
    begin
		if mMustCalc then
    	    CmdCalc.Execute;
	end;

    // Get Criteria
    n := Length(mFrames) - 1;
    SetLength(mVar, 2);
    mVar[0] := sysLogonUID;
    mVar[1] := QrRep.FieldByName('LCT').AsString;
    for i := 0 to n do
        with mFrames[i] do
        if Visible then
        begin
            SetLength(mVar, Length(mVar) + GetParamNo);
            GetCriteria(mVar);
        end;

    // Report template
    if not mIsTemplate then
        mTemplate := ''
    else
	    mTemplate := Trim(QrRep.FieldByName('TEMPLATE').AsString);
    if mTemplate  <> '' then
    begin
		Result := DataOffice.CreateReport2(mTemplate, mVar, sRep);
        Exit;
    end;

    // Proc
    if fs = '' then		// To window
        Result := ShowReport(sCap, sRep, mVar)
//    else				// Export for testing
//	    Result := ExportReport(fs, sRep, 'RPT', mVar)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRunExecute(Sender: TObject);
begin
	Proc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.PgGroupChange(Sender: TObject);
begin
    with QrRep do
    begin
    	Filter := Format('CAT=%d', [PgGroup.ActivePage.Tag]);
        while not Eof do
        	if FieldByName('SYS').AsBoolean then
	        	Next
            else
            	Break;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.BitBtn2Click(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdCalcExecute(Sender: TObject);
begin
    mFrames[mFields.IndexOf('GIA')].Action(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	with QrRep do
    begin
		CmdCalc.Enabled := FieldByName('GIA').AsBoolean and mStockControl;
    	CmdRun.Enabled := not FieldByName('SYS').AsBoolean;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRereadExecute(Sender: TObject);
var
	n: Integer;
begin
	n := QrRep.FieldByName('IDX').AsInteger;
    OpenReports;
    QrRep.Locate('IDX', n, []);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRuntimeEditExecute(Sender: TObject);
var
	mPath, mName: String;
begin
	if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
	end;

    with QrRep do
    begin
		if FieldByName('SYS').AsBoolean then
        	Exit;
		mName := Trim(FieldByName('REP_NAME').AsString);
        mName := Iif(sysEnglish, 'EN_' + mName, mName);
    end;
//    mPath := GetReportFullPath(mName);
    FrmRep.BuildFullPath(mName, mPath);
    if mPath = '' then
    	Exit;

    //  Open target report for editing
    ShellExecute(Handle, 'Open', PChar(mPath), nil, nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdTestAllExecute(Sender: TObject);
var
	s, fs: String;
    ls: TStrings;
begin
	if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
	end;

	if not YesNo('Chạy kiểm tra các báo cáo trong danh sách hiện tại. Tiếp tục?') then
    	Exit;
	fs := isGetTempFileName;
    ls := TStringList.Create;

    with QrREP do
    begin
    	First;
        while not Eof do
        begin
        	if not FieldByName('SYS').AsBoolean then
            begin
        		s := Trim(FieldByName('REP_NAME').AsString);
	            if s <> '' then
					if not Proc(fs) then
        	        	ls.Add(FieldByName('DESC').AsString + #9#9 + s);
            end;

            Next;
        end;
    end;
    DeleteFile(fs);

    // Done
    if ls.Count > 0 then
    begin
    	ls.Insert(0, '---------------------');
    	ls.Insert(0, 'List of error reports');
        ls.SaveToFile(sysTempPath + 'errors.log');
	    ShellExecute(Handle, 'Open', PChar(sysTempPath + 'errors.log'), nil, nil, SW_NORMAL);
    end
    else
    	Msg('Không có lỗi nào được phát hiện.');
    ls.Free;
end;

end.
