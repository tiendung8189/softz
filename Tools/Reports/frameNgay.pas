(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameNgay;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, wwdblook;

type
  TfrNGAY = class(TFrame)
    Panel1: TPanel;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    Label1: TLabel;
    CbLoc: TwwDBLookupCombo;
  private
  public
  	procedure Init; overload;
  	procedure Init(const d1, d2: TDateTime); overload;
  end;

implementation

uses
	ExCommon, MainData, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNGAY.Init;
begin
    Init(Date - sysLateDay, Date)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNGAY.Init(const d1, d2: TDateTime);
begin
    DataMain.QrLOC.Open;
	EdFrom.Date := d1;
	EdTo.Date := d2;
    CbLoc.LookupValue := sysLoc;
    CbLoc.Enabled := sysIsCentral;
    CbLoc.Visible := sysIsDrc;
    Label1.Visible := sysIsDrc;
end;

end.
