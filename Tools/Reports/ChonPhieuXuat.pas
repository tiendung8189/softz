(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieuXuat;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmChonPhieuXuat = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsKHACHHANG: TDataSource;
    QrPHIEUXUAT: TADOQuery;
    QrPHIEUXUATNGAY: TDateTimeField;
    QrPHIEUXUATSCT: TWideStringField;
    QrPHIEUXUATMAKHO: TWideStringField;
    QrPHIEUXUATDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbKH: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMAKH: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMKH: TADOQuery;
    QrPHIEUXUATTENKHO: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrPHIEUXUATNG_GIAO: TWideStringField;
    QrPHIEUXUATNG_NHAN: TWideStringField;
    QrPHIEUXUATHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUXUATMAKH: TWideStringField;
    QrPHIEUXUATTENDT: TWideStringField;
    QrPHIEUXUATKHOA: TGuidField;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMAKHChange(Sender: TObject);
    procedure CbMAKHExit(Sender: TObject);
    procedure CbMAKHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
	mKH, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pFix: Boolean; pKH, pKho: String): TGUID;
  end;

var
  FrmChonPhieuXuat: TFrmChonPhieuXuat;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieuXuat.Execute;
begin
	mKH := pKH;
    mKho := pKho;
    CbMAKH.Enabled := not pFix;
    CbKH.Enabled := not pFix;

	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUXUAT.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUXUAT, QrDMKH, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetDisplayFormat(QrPHIEUXUAT, sysCurFmt);
    SetDisplayFormat(QrPHIEUXUAT, ['NGAY'], DateTimeFmt);

    SetCustomGrid('CHON_PHIEUXUAT', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
	mSQL := QrPHIEUXUAT.SQL.Text;

    OpenDataSets([QrPHIEUXUAT, QrDMKH, QrDMKHO]);
//    SetShortDateFormat(QrPHIEUXUAT);

    CbMAKH.LookupValue := mKH;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mKH = '' then
    	CbMAKH.SetFocus
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mKH := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsKHACHHANG);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CmdRefreshExecute(Sender: TObject);
var
    s, Kh, Kho: String;
    Tungay, Denngay: TDateTime;
begin
    Kh      := CbMAKH.LookupValue;
    Kho     := CbMAKHO.LookupValue;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mKH <> Kh)          or (mKHO <> Kho)         then
	begin
   	    mKH := Kh;
        mKHO := Kho;
        mTungay := Tungay;
        mDenngay := Denngay;

        with QrPHIEUXUAT do
        begin
			Close;

            s := mSQL;
            if mKH <> '' then
            	s := s + ' and a.MADT=''' + mKH + '''';
			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
            SQL.Text := s;
            SQL.Add(' order by a.NGAY, a.SCT');
            Parameters[0].Value := mTungay;
            Parameters[1].Value := mDenngay;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CbMAKHChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbKH.LookupValue := s;
    // Ten NCC
    1:
        CbMAKH.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CbMAKHExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CbMAKHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

end.
