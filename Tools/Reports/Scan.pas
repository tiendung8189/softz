(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Scan;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs, ExtDlgs, Menus, ActnList, Db,
  ADODB, ExtCtrls, AdvMenus, ImgList, fcImager;

type
  TFrmScan = class(TForm)
    Action: TActionList;
    CmdAssign: TAction;
    CmdClear: TAction;
    OpenDlg: TOpenPictureDialog;
    CmdGray: TAction;
    ImageList1: TImageList;
    QrHINH: TADOQuery;
    DsHINH: TDataSource;
    Gnhnh1: TMenuItem;
    Xahnh1: TMenuItem;
    N1: TMenuItem;
    GrayScale1: TMenuItem;
    PaPic: TPanel;
    Img: TfcDBImager;
    CmdExport: TAction;
    Export1: TMenuItem;
    N2: TMenuItem;
    SaveDlg: TSaveDialog;
    PopPic: TAdvPopupMenu;
    ItemDrawStyle: TMenuItem;
    ItemDraw0: TMenuItem;
    ItemDraw1: TMenuItem;
    ItemDraw2: TMenuItem;
    ItemDraw3: TMenuItem;
    ItemDraw4: TMenuItem;
    ItemDraw5: TMenuItem;
    N3: TMenuItem;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAssignExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ImgCalcPictureType(ImageControl: TfcDBImager;
      var PictureType: TfcImagerPictureType; var GraphicClassName: String);
    procedure FormCreate(Sender: TObject);
    procedure CmdGrayExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure PopPicPopup(Sender: TObject);
    procedure ItemDraw0Click(Sender: TObject);
  private
  	nDrawStyle: Integer;
  public
  	procedure Execute(master: TDataSource; ma: String = 'MAVT');
  end;

var
  FrmScan: TFrmScan;

implementation

{$R *.DFM}

uses
	jpeg, isLib;

const
  	cDrawStyle: array[0..5] of TfcImagerDrawStyle = (dsCenter, dsNormal, dsProportional, dsProportionalCenter, dsStretch, dsTile);

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.Execute;
begin
    with QrHINH do
    begin
    	SQL.Text := Format(
			'select MAVT, HINH, EXT' +
			'  from DM_VT_HINH' +
			' where %s = :%s', [ma, ma]);

    	DataSource := master;
		Open;
    end;

    // Restore saved parameters
    Left   := RegRead('GoodsPhoto', Left);
    Top    := RegRead('GoodsPhoto', Top);
    Width  := RegRead('GoodsPhoto', Width);
    Height := RegRead('GoodsPhoto', Height);

	nDrawStyle := RegRead('DrawStyle', 1);
	Img.DrawStyle := cDrawStyle[nDrawStyle];

    Show;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdAssignExecute(Sender: TObject);
var
    ext: String;
begin
   	FormStyle := fsNormal;
	if not OpenDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
        Exit;
	end;

    ext := UpperCase(ExtractFileExt(OpenDlg.FileName));
    if ext = '.JPEG' then
    	ext := '.JPG';

	with QrHINH do
    begin
    	if IsEmpty then
        	Append
		else
	        Edit;

		TBlobField(FieldByName('HINH')).LoadFromFile(OpenDlg.FileName);
        FieldByName('EXT').AsString := ext;
        Post;
	end;

   	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdClearExecute(Sender: TObject);
begin
	QrHINH.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.ImgCalcPictureType(ImageControl: TfcDBImager;
  var PictureType: TfcImagerPictureType; var GraphicClassName: String);
var
	ext : String;
begin
	ext := QrHINH.FieldByName('EXT').AsString;
    if ext = '.JPG' then
		PictureType := fcptJpg
    else if ext = '.BMP' then
		PictureType := fcptBitmap
    else if ext = '.WMF' then
		PictureType := fcptMetafile
    else
		PictureType := fcptBitmap;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdGrayExecute(Sender: TObject);
begin
	with Img do
    begin
    	BitMapOptions.GrayScale := not BitMapOptions.GrayScale;
        Invalidate;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	b := QrHINH.IsEmpty;
	CmdClear.Enabled := not b;
	CmdExport.Enabled := not b;
    CmdAssign.Enabled := not (QrHINH.DataSource.DataSet.State in [dsInsert]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    RegWrite('GoodsPhoto', ['Left', 'Top', 'Width', 'Height', 'DrawStyle'],
    	[Left, Top, Width, Height, nDrawStyle]);
    try
	    QrHINH.Close;
    finally
    end;
    FrmScan := Nil;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormResize(Sender: TObject);
begin
	Img.Repaint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdExportExecute(Sender: TObject);
begin
    FormStyle := fsNormal;
	if not SaveDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
    	Exit;
    end;

    with QrHINH do
    begin
	    if FieldByName('HINH').IsNull then
        begin
	    	FormStyle := fsStayOnTop;
    		Exit;
        end;

	    TBlobField(FieldByName('HINH')).SaveToFile(
        	ChangeFileExt(SaveDlg.FileName, FieldByName('EXT').AsString));
    end;
	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.ItemDraw0Click(Sender: TObject);
begin
	if nDrawStyle <> (Sender as TComponent).Tag then
    begin
		nDrawStyle := (Sender as TComponent).Tag;
		Img.DrawStyle := cDrawStyle[nDrawStyle];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.PopPicPopup(Sender: TObject);
var
	i: Integer;
begin
	CmdGray.Checked := Img.BitMapOptions.GrayScale;
    with ItemDrawStyle do
	    for i := 0 to Count - 1 do
        begin
    		Items[i].Checked := nDrawStyle = Items[i].Tag;
            if Items[i].Checked then
            	System.Break;
        end;
end;

end.
