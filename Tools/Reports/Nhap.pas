﻿(*==============================================================================
**	- Full optimize
**------------------------------------------------------------------------------
*)
unit Nhap;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,Variants,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Messages, Windows,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Grids, Wwdbgrid, ToolWin,
  Wwdotdot, Wwdbcomb, Buttons;

type
  TFrmNhap = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    PaTotal: TPanel;
    Label24: TLabel;
    EdTriGiaTT: TwwDBEdit;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_DVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXTL_CK_HD: TFloatField;
    QrNXCHIETKHAU_HD: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    ImgTotal: TImage;
    EdCL: TwwDBEdit;
    Label3: TLabel;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXNG_NHAN: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaMaster: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    QrNXPTTT: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    DMVT_CAPNHAT_GIANHAP: TADOCommand;
    CmdExBarcode: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    LAYDONDH: TADOStoredProc;
    QrCTEX_DATE: TDateTimeField;
    CHECK_NCC: TADOCommand;
    QrNXHAN_TTOAN: TIntegerField;
    QrNXDA_TTOAN: TBooleanField;
    QrNXLK_HAN_TTOAN: TIntegerField;
    Bevel1: TBevel;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrNXTC_SOTIEN: TFloatField;
    QrCTLK_LOAITHUE: TWideStringField;
    QrCTLOAITHUE: TWideStringField;
    PaHoadon: TisPanel;
    PaHoadon2: TPanel;
    TntLabel2: TLabel;
    TntLabel3: TLabel;
    TntLabel8: TLabel;
    CbLOAITHUE: TwwDBLookupCombo;
    EdSOHD: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    DBText2: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    wwDBLookupCombo1: TwwDBLookupCombo;
    Label5: TLabel;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    CbKHO: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    CbMADT: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    TntLabel6: TLabel;
    TntLabel9: TLabel;
    DBEdit7: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    DBMemo2: TDBMemo;
    EdTENDT: TwwDBEdit;
    ckDatt: TDBCheckBox;
    QrNXHOADON_SO: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_NGAY: TDateTimeField;
    CALC_NGAY_TTOAN: TDateTimeField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrCTLK_VAT_VAO: TFloatField;
    QrNXLCT: TWideStringField;
    QrCTDONGIA_REF: TFloatField;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    btnDmhh: TToolButton;
    btn2: TToolButton;
    CmdDmvt: TAction;
    N4: TMenuItem;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    N5: TMenuItem;
    CmdThamkhaoGia: TAction;
    hamkhoginhp1: TMenuItem;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrCTLK_DVT1: TWideStringField;
    QrCTLK_QD1: TIntegerField;
    QrNXSCT2: TWideStringField;
    spUpdateDDH: TADOCommand;
    QrNXHINHTHUC_GIA: TWideStringField;
    CbGia: TwwDBLookupCombo;
    LbGia: TLabel;
    QrCTSOTIEN1: TFloatField;
    QrCTQD1: TIntegerField;
    QrCTLK_GIABAN: TFloatField;
    QrNXSOTIEN1: TFloatField;
    QrCTDONGIA_REF2: TFloatField;
    QrNXPHIEUGIAOHANG: TWideStringField;
    Label7: TLabel;
    wwDBEdit4: TwwDBEdit;
    QrNXKHOA: TGuidField;
    QrNXKHOA2: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXCALC_SOTIEN_SAUCK_MH: TFloatField;
    QrNXLOC: TWideStringField;
    CbDrcStatus: TwwDBComboBox;
    LbDrcStatus: TLabel;
    QrNXDRC_STATUS: TWideStringField;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    CmdDmncc: TAction;
    SepChecked: TToolButton;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIASI: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    BtnIn2: TToolButton;
    QrCTLK_VAT_RA: TFloatField;
    QrNXLYDO: TWideStringField;
    QrNXLK_LYDO: TWideStringField;
    QrCTLK_TENTHUE: TWideStringField;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    QrCTTIEN_THUE_5: TFloatField;
    QrCTTIEN_THUE_10: TFloatField;
    QrCTTIEN_THUE_OR: TFloatField;
    QrNXTHUE_5: TFloatField;
    QrNXTHUE_10: TFloatField;
    QrNXTHUE_OR: TFloatField;
    BtThue: TSpeedButton;
    QrCTGIAVON: TFloatField;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    QrCTLK_GIAVON: TFloatField;
    QrCTTL_CK_HD: TFloatField;
    QrCTCHIETKHAU_HD: TFloatField;
    Label12: TLabel;
    Label11: TLabel;
    EdTLCK: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    QrCTSOTIEN2: TFloatField;
    QrCTTHANHTIEN: TFloatField;
    QrNXSOTIEN2: TFloatField;
    QrNXTHANHTIEN: TFloatField;
    Label13: TLabel;
    wwDBEdit6: TwwDBEdit;
    QrCTLOC: TWideStringField;
    BtCongno: TSpeedButton;
    PaTotal1: TPanel;
    PaTotal2: TPanel;
    PaTotal3: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure CmdFromOrderExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrNXMADTChange(Sender: TField);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdThamkhaoGiaExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAValidate(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure QrCTLOAITHUEChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure BtThueClick(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrNXTL_CK_HDChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure BtCongnoClick(Sender: TObject);
    procedure QrNXCHIETKHAU_HDChange(Sender: TField);
    procedure QrCTTL_CKValidate(Sender: TField);

  private
	mLCT: String;
	mCanEdit, b1Ncc, mLuugianhap, bDuplicate: Boolean;
	mLydo, mMakho, mPrefix: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;
    fHTGia: String;

	    (*
    	** Functions
	    *)
    procedure CapnhatGianhap;
    procedure UpdateDDH;
    procedure escapeKey(pSleepTime: Variant);
  public
	procedure Execute(r: WORD);
  end;

var
  FrmNhap: TFrmNhap;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, Dmvt, Sapthutu, GuidEx, isCommon,
    ChonDsma, isLib, ChonDondh2, TheodoiGia, exThread, Dmncc, DmKhNcc, isFile,
    ImportExcel, Tienthue, CongnoNCC;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAP';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.Execute;
begin
    mLCT := 'NMUA';

    // Audit setting
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    PaTotal.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrNX;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

	// Params
    b1Ncc := FlexConfigBool(FORM_CODE, '1 nha cung cap');
    ckDatt.Visible := FlexConfigBool(FORM_CODE, 'Checkbox "Da thanh toan"');
    mLuuGianhap := FlexConfigBool(FORM_CODE, 'Cap nhat gia');
    CmdFromOrder.Visible := FlexConfigBool(FORM_CODE, 'Lay tu DDH');
    mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');

	mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := RegReadString(Name, 'Lydo', DataMain.GetSysParam('DEFAULT_PTNHAP'));

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    bDuplicate := FlexConfigBool(FORM_CODE, 'Duplicate Mavt');

    //Lay gia tri mac dinh cho HTGia
    fHTGia := sysHTGia;

    if fHTGia <> '' then
    begin
        cbGia.Visible := False;
        LbGia.Visible := False;
    end;

    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;

    // Initial
  	mTrigger := False;
    mTriggerCK := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2('_IDI_RECEIPT');

    // Open database
	Wait(PREPARING);

	with DataMain do
    begin
		OpenDataSets([QrDMVT, QrPTNHAP, QrDMLOAITHUE, QrDMNCC, QrDMKHO, QrHINHTHUC_GIA]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TL_LAI'], sysPerFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetDisplayFormat(QrNX, ['TL_CK_HD'], sysPerFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
	    SetDisplayFormat(QrCT, ['TL_CK', 'TL_CK_HD'], sysPerFmt);
        SetDisplayFormat(QrCT, ['THUE_SUAT'], sysTaxFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsThue then
    begin
        PaTotal2.Visible := sysIsThue;
        grRemoveFields(GrBrowse, ['THUE']);
        grRemoveFields(GrDetail, ['LK_TENTHUE', 'THUE_SUAT', 'TIEN_THUE', 'THANHTIEN']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Lydo'], [mMakho, mLydo]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
	** Actions
	*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

	with DataMain do
    begin
    	QrDMVT.Requery;
        QrPTNHAP.Requery;
        QrDMNCC.Requery;
        QrDMKHO.Requery;
    end;
            
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
    	    if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSapthutuExecute(Sender: TObject);
begin
	CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
    UpdateDDH;
    CapnhatGianhap;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDelExecute(Sender: TObject);
begin
    exCheckLoc(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDmnccExecute(Sender: TObject);
var
    r: WORD;
    mRet: Boolean;
begin
    if DataMain.GetFuncState('SZ_KH_NCC') then
    begin
        r := DataMain.GetRights('SZ_KH_NCC');
        if r = R_DENY then
            Exit;

        Application.CreateForm(TFrmDmKhNcc, FrmDmKhNcc);
        mRet := FrmDmKhNcc.Execute(r, False);
    end else
    begin
        r := DataMain.GetRights('SZ_NCC');
        if r = R_DENY then
            Exit;

        Application.CreateForm(TFrmDmncc, FrmDmncc);
        mRet := FrmDmncc.Execute(r, False);
    end;

    if mRet then
        DataMain.QrDmncc.Requery
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := DataMain.GetRights('SZ_MAVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvt, FrmDmvt);
    mRet := FrmDmvt.Execute(r, False);

    if mRet then
    	DataMain.QrDMVT.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		EdCL.SetFocus;
        except
        end
    else if (ActiveControl = PaTotal) or (ActiveControl.Parent = PaTotal) then
		try
    		CbNgay.SetFocus;
        except
        end
    else if (ActiveControl = PaHoadon2) or (ActiveControl.Parent = PaHoadon2) then
        GrDetail.SetFocus
    else
    	try
    		EdSOHD.SetFocus;
        except
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsNX)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdThamkhaoGiaExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmTheodoiGia, FrmTheodoiGia);
    with QrNX do
	    FrmTheodoiGia.Execute(mLCT,
            QrCT.FieldByName('MAVT').AsString,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Đơn đặt hàng có mặt hàng sai mã. Tiếp tục?';

procedure TFrmNhap.CmdFromOrderExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue: String;
    mSoluong, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmChonDondh2, FrmChonDondh2);
    with QrNX do
	    n := FrmChonDondh2.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 'DM_NCC','DHN');
	FrmChonDondh2.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with LAYDONDH do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        // Cap nhat nha cung cap
        mNcc := FieldByName('MADT').AsString;
        mKho := FieldByName('MAKHO').AsString;
        mSCT2 := FieldByName('SCT').AsString;
        mHTGia := FieldByName('HINHTHUC_GIA').AsString;
        mTlckHD := FieldByName('TL_CK_HD').AsFloat;

        if (QrNX.FieldByName('MADT').AsString <> mNcc) or
           (QrNX.FieldByName('MAKHO').AsString <> mKho) or
           (QrNX.FieldByName('SCT2').AsString <> mSCT2) then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('MADT').AsString := mNcc;
	        QrNX.FieldByName('MAKHO').AsString := mKho;
            QrNX.FieldByName('HINHTHUC_GIA').AsString := mHTGia;
            QrNX.FieldByName('TL_CK_HD').AsFloat := mTlckHD;
            QrNX.FieldByName('SCT2').AsString := mSCT2;
            TGuidField(QrNX.FieldByName('KHOA2')).AsGuid := n;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mTlck := FieldByName('TL_CK').AsFloat;
            mLThue := FieldByName('LOAITHUE').AsString;

            with QrCT do
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := mDongia;
                FieldByName('SOLUONG').AsFloat := mSoluong;
                FieldByName('TL_CK').AsFloat := mTlck;
                FieldByName('LOAITHUE').AsString := mLThue;
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

    // Get file name
    mFile := isGetOpenFileName('XLS;XLSX', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        if bDuplicate then
                        begin
                            QrCT.Append;
                            QrCT.FieldByName('MAVT').AsString := s
                        end else
                        begin
                            if (not QrCT.Locate('MAVT', s, []))  then
                            begin
                                QrCT.Append;
                                QrCT.FieldByName('MAVT').AsString := s
                            end
                            else
                                SetEditState(QrCT);
                        end;

                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
    DataMain.LabelExport(TGuidEx.ToString(QrNX.FieldByName('KHOA')));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	// Master
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
	CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdTotal.Enabled  := n = 1;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

	// Detail
    with QrCT do
    begin
    	if Active then
        	bEmpty := IsEmpty
        else
            bEmpty := False;
    end;

    if b1Ncc then
	    CbMADT.ReadOnly := not bEmpty;
    CmdEmptyDetail.Enabled := not bEmpty;
    CmdThamkhaoGia.Enabled := not bEmpty;
    CbGia.ReadOnly := not bEmpty;
    CmdFromOrder.Enabled := bEmpty;
end;

    (*
    **  Master DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('LYDO').AsString          := mLydo;
		FieldByName('MAKHO').AsString         := mMakho;
        FieldByName('LOC').AsString           := sysLoc;
		FieldByName('DA_TTOAN').AsBoolean     := False;
        FieldByName('HINHTHUC_GIA').AsString  := Iif(fHTGia = '', '03', fHTGia);
        FieldByName('DRC_STATUS').AsString    := '1';
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['NGAY', 'MAKHO', 'MADT']) then
        	Abort;
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;

 	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
		mMakho := FieldByName('MAKHO').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	with DataSet do
    begin
		// Validate: da co phieu thu / chi
    	if FieldByName('TC_SOTIEN').AsFloat <> 0 then
	    begin
    	    ErrMsg(RS_DA_THUCHI);
        	Abort;
	    end;

    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
    with QrNX do
    begin
        FieldByName('CALC_NGAY_TTOAN').AsDateTime :=
            FieldByName('NGAY').AsDateTime +
            FieldByName('HAN_TTOAN').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXCHIETKHAU_HDChange(Sender: TField);
var
    tlckhd, ckhd, Sotien1: Double;
begin
//    with QrNX do
//    begin
//        Sotien1 := FieldByName('SOTIEN1').AsFloat;
//        Ckhd := FieldByName('CHIETKHAU_HD').AsFloat;
//
//        tlckhd := Iif(Ckhd=0, 0, 100 * (SafeDiv(Ckhd, Sotien1)));
//        FieldByName('TL_CK_HD').AsFloat := tlckhd;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    mThanhtien, mThanhtoan: Double;
    mHinhthuc: String;
begin
    with Sender.DataSet do
    begin
        mHinhthuc := FieldByName('HINHTHUC_GIA').AsString;
        mThanhtien := FieldByName('THANHTIEN').AsFloat;

//        if mHinhthuc = '03' then     // Nguoc
//            mThanhtoan := mThanhtien - FieldByName('CL_THUE').AsFloat
//        else // Xuoi va truc tiep
            mThanhtoan := mThanhtien + FieldByName('CL_THUE').AsFloat;

        FieldByName('THANHTOAN').AsFloat := mThanhtoan;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXHINHTHUC_GIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if QrCT.IsEmpty then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.UpdateDDH;
begin
    if QrNX.FieldByName('SCT2').AsString = '' then
        Exit;

    with spUpdateDDH do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA2'));
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXMADTChange(Sender: TField);
begin
	exDotMadt(DataMain.QrDMNCC, Sender);
    with QrNX do
    begin
    	// Update TENDT
		EdTENDT.Text := EdTENDT.Field.AsString;

        // Default han thanh toan
        if not FieldByName('DA_TTOAN').AsBoolean then
            FieldByName('HAN_TTOAN').AsInteger := FieldByName('LK_HAN_TTOAN').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXNGAYValidate(Sender: TField);
begin
	exValidRecordDate(QrNX.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXTL_CK_HDChange(Sender: TField);
begin
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('TL_CK_HD').AsFloat := Sender.AsFloat;

            Next;
        end;

        CheckBrowseMode;
    end;
end;


(*
    ** End: Master DB
    *)

    (*
    **  Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
   		Abort;
    SetEditState(QrNX);
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := RoundUp(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger))
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := RoundUp(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTLOAITHUEChange(Sender: TField);
begin
    with QrCT do
    begin
        if FieldByName('LOAITHUE').AsString = 'TTT' then
            FieldByName('THUE_SUAT').Clear
        else
            FieldByName('THUE_SUAT').AsFloat := FieldByName('LK_VAT_VAO').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                d := FieldByName('SOLUONG').AsFloat /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

                if d = Trunc(d) then
                    FieldByName('SOLUONG2').AsFloat := d
                else
                    FieldByName('SOLUONG2').Clear;
            end
            else if Sender.FullName = 'SOLUONG2' then
            begin
                FieldByName('SOLUONG').AsFloat :=
                    FieldByName('SOLUONG2').AsFloat *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat);

        // Tinh tri gia tren BOX
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTSOTIENChange(Sender: TField);
var
    mHinhthuc: String;
    mSotien, tlck, tlckhd, mCk, mCkhd, mThue, mTs, mGiavon, mSl, mThanhtien,
        mSotien1, //Sau chiết khấu mặt hàng
        mSotien2: Double; //Sau chiết khấu hóa đơn
begin
    if mTriggerCK then
        Exit;

	with QrCT do
    begin
        mHinhthuc := QrNX.FieldByName('HINHTHUC_GIA').AsString;
        mSotien := FieldByName('SOTIEN').AsFloat;
        mTs := FieldByName('THUE_SUAT').AsFloat;
        mSl := FieldByName('SOLUONG').AsFloat;

        if Sender.FieldName = 'TL_CK' then
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            mCk := exVNDRound(mSotien * tlck / 100.0)
        end
        else
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            tlck := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end;
        mSotien1 := mSotien -  mCk;

//        if Sender.FieldName = 'TL_CK_HD' then
//        begin
//            tlckhd := FieldByName('TL_CK_HD').AsFloat;
//            mCkhd := exVNDRound(mSotien1 * tlckhd / 100.0)
//        end
//        else
//        begin
//            mCkhd := FieldByName('CHIETKHAU_HD').AsFloat;
//            tlckhd := Iif(mCkhd=0, 0, 100 * (SafeDiv(mCkhd, mSotien1)))
//        end;
        mCkhd := exVNDRound( mSotien1 * FieldByName('TL_CK_HD').AsFloat / 100.0);

        mSotien2 := mSotien1 -  mCkhd;
        mGiavon := 0;

        if mHinhthuc = '02' then          // Xuoi
        begin
        	// Thue
            mThue := exVNDRound(mSotien2 * mTs / 100.0);
            mThanhtien := mSotien2 + mThue;

            if mSl <> 0 then
                mGiavon := mSotien2 / mSl;
        end
        else if mHinhthuc = '03' then     // Nguoc
        begin
            // Thue
            if mTs = 0 then
                mThue := 0
            else
                mThue := exVNDRound(mSotien2 / (100/mTs + 1));
            mThanhtien := mSotien2;

            if mSl <> 0 then
                mGiavon := (mSotien2 - mThue) / mSl;
        end;

        FieldByName('GIAVON').AsFloat := mGiavon;

    	mTriggerCK := True;
        FieldByName('TL_CK').AsFloat := tlck;
    	FieldByName('CHIETKHAU').AsFloat := mCk;

//        FieldByName('TL_CK_HD').AsFloat := tlckhd;
    	FieldByName('CHIETKHAU_HD').AsFloat := mCkhd;
        mTriggerCK := False;

//        FieldByName('CHIETKHAU_HD').AsFloat := mCkhd;
        if mTs = 5 then
        begin
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_5').AsFloat := mThue;
        end
        else if mTs = 10 then
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := mThue;
        end else
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := mThue;
        end;

        FieldByName('TIEN_THUE').AsFloat := mThue;
        FieldByName('SOTIEN1').AsFloat := mSotien1;
        FieldByName('SOTIEN2').AsFloat := mSotien2;
        FieldByName('THANHTIEN').AsFloat := mThanhtien;
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTTL_CKValidate(Sender: TField);
var
    b: Boolean;
begin
    if Sender.AsFloat = 0 then
        Exit;

    b := False;
    with QrCT do
    begin
        if (Sender.AsFloat < 0) then
            b := True
        else
        if ((Sender.FieldName = 'TL_CK') and (Sender.AsFloat > 100)) or
            ((Sender.FieldName = 'CHIETKHAU') and (Sender.AsFloat > FieldByName('SOTIEN').AsFloat))  then
            b := True
    end;

    if b then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTMAVTValidate(Sender: TField);
var
	s: String;
    bm: TBytes;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
	if s = '' then
    	Exit;

    if not bDuplicate then
    //Trung mat hang
    with QrCT do
    begin
        if IsDuplicateCode2(QrCT, Sender, bm) then
        begin
            //if YesNo(RS_DUPLICATE_CODE) then
            begin
                try
                    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
                    Abort;
                finally
                    Cancel;
                    GotoBookmark(bm);
                    Edit;
                    mgMyThread := TExThread.Create(escapeKey, 50);
                end;
            end;
        end;
    end;

	// Nhieu nha cung cap
	if not b1Ncc then
    	Exit;

	// Validate MADT
    if IsDotSelect(s) = 0 then
        with CHECK_NCC do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrNX.FieldByName('MADT').AsString;
            if Execute.RecordCount < 1 then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL2);
                Abort;
            end;
        end;


end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTMAVTChange(Sender: TField);
var
	gia, ck: Double;
begin
    if b1Ncc then
		exDotMavt(1, Sender, 'MADT=''' + QrNX.FieldByName('MADT').AsString + '''')
    else
		exDotMavt(1, Sender);

    // Update referenced fields
    with QrCT do
    begin
        FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;
        FieldByName('LOAITHUE').AsString := FieldByName('LK_LOAITHUE').AsString;

        ck := 0;
        gia := 0;
        DataMain.DmvtGetRef(Sender.AsString, mLCT, QrNX.FieldByName('MADT').AsString,
                QrNX.FieldByName('HINHTHUC_GIA').AsString, ck, gia);

        FieldByName('GIAVON').AsFloat := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA_REF').AsFloat := gia;
        FieldByName('DONGIA').AsFloat := gia;
        FieldByName('TL_CK').AsFloat := ck;
        FieldByName('TL_CK_HD').AsFloat := QrNX.FieldByName('TL_CK_HD').AsFloat;

        if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;
	end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CapnhatGianhap;
begin
	if not mLuuGianhap then
    	Exit;

    with DMVT_CAPNHAT_GIANHAP do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));;
        Execute;
    end;
end;
	(*
    ** End: Detail DB
    *)

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('CHIETKHAU').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('CHIETKHAU_MH').AsFloat);
        ColumnByName('CHIETKHAU_HD').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('CHIETKHAU_HD').AsFloat);
		ColumnByName('TIEN_THUE').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THUE').AsFloat);
        ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN1').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN1').AsFloat);
        ColumnByName('SOTIEN2').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN2').AsFloat);
        ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THANHTIEN').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.BtCongnoClick(Sender: TObject);
begin
    if DataMain.GetRights('SZ_CONGNO_NCC') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.BtThueClick(Sender: TObject);
begin
    Application.CreateForm(TFrmTienthue, FrmTienthue);
    FrmTienthue.Execute(DsNX)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbMAKHOBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

end.