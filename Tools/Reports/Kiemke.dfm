object FrmKiemke: TFrmKiemke
  Left = 78
  Top = 243
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Ki'#7875'm Kho'
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001000800680500001600000028000000100000002000
    0000010008000000000040010000000000000000000000010000000000000402
    0400F48E0400FC02FC0004868400AC420400C4620C009CCECC00B4662C009CFE
    FC00F4DECC00DC760400AC724C0064CEFC00AC6E3C00848684009C5A3400BC66
    2400C4D6EC00C4966C00C45E0400CC6A0400BC6E3C00B4DEEC00B44E04009CCE
    DC009CCEFC00A4BEBC00B4E2F400E48E3400349E9C00AC460400DC660400AC66
    3400E47E0400B4723C00BC967400D46A0400C4723C00ACDAE400A4DAEC00C4C2
    C400BCE2F4000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000E0E0E0E0E0E0E0E0E0E0E0E0000000E
    0000000000000000000000000E000E00031818180F1919191919190300000E00
    19272709041519191919191900000E001916160704170D0C0C0C191900000E00
    0C1B0B040D10131F0C0C0C0C00000E000C1B072228231314010C0C0C00000E00
    0C1B1A26161612140A0C0C0C00000E000C0C0C0C0C0C0C1C0A0C0C0C00000E00
    0C0C0C0C0C0C0C0C0A0C0C030000000011080C0C0C0C1D000000000000000000
    0011080C0C060000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    0000C00300008001000000010000000100000001000000010000000100000001
    0000000100000001000080030000C07F0000E0FF0000FFFF0000FFFF0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    792
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel2: TBevel
    Left = 0
    Top = 46
    Width = 792
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 224
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 232
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 304
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 312
      Top = 0
      Cursor = 1
      Hint = 'In phi'#7871'u'
      Caption = 'In'
      DropdownMenu = PopupMenu2
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtImportClick
    end
    object SepChecked: TToolButton
      Left = 399
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 407
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton6: TToolButton
      Left = 479
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object BtImport: TToolButton
      Left = 487
      Top = 0
      Cursor = 1
      Hint = 'L'#7845'y d'#7919' li'#7879'u ki'#7875'm kho t'#7915' t'#7853'p tin ngo'#224'i'
      Caption = 'Import'
      DropdownMenu = PopIm
      ImageIndex = 7
      Style = tbsDropDown
      OnClick = BtImportClick
    end
    object ToolButton11: TToolButton
      Left = 574
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 582
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 784
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 784
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'SOTIEN'#9'15'#9'Tr'#7883' gi'#225#9'F'
          'MAKHO'#9'7'#9'M'#227#9'F'#9'Kho'
          'DGIAI'#9'50'#9'Ng'#432#7901'i ki'#7875'm k'#234#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 784
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 784
        inherited Panel1: TPanel
          Width = 784
          ParentColor = False
          ExplicitWidth = 784
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 163
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 306
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LbKHO: TLabel
          Left = 84
          Top = 38
          Width = 21
          Height = 16
          Alignment = taRightJustify
          Caption = 'Kho'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 55
          Top = 62
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'Qu'#7847'y, k'#7879
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 24
          Top = 86
          Width = 81
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i ki'#7875'm k'#234
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 299
          Top = 63
          Width = 97
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y gi'#7901' ki'#7875'm k'#234
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdSCT: TwwDBEdit
          Left = 363
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object CbKHO: TwwDBLookupCombo
          Left = 168
          Top = 34
          Width = 380
          Height = 22
          TabStop = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENKHO'#9'40'#9'T'#234'n'#9'F'
            'MAKHO'#9'6'#9'M'#227#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
ButtonEffects.Transparent=True
        end
        object CbMAKHO: TwwDBLookupCombo
          Left = 112
          Top = 34
          Width = 53
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MAKHO'#9'6'#9'M'#227#9'F'
            'TENKHO'#9'40'#9'T'#234'n'#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          OnBeforeDropDown = CbMAKHOBeforeDropDown
          OnCloseUp = CbMAKHOCloseUp
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object DBMemo1: TDBMemo
          Left = 112
          Top = 82
          Width = 657
          Height = 71
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 6
        end
        object wwDBLookupCombo3: TwwDBLookupCombo
          Left = 112
          Top = 58
          Width = 165
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TENQUAY'#9'35'#9'DGIAI'#9'F')
          DataField = 'QUAYKE'
          DataSource = DsNX
          LookupTable = QrDM_QUAYKE
          LookupField = 'MAQUAY'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 403
          Top = 59
          Width = 145
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY2'
          DataSource = DsNX
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 163
        Width = 784
        Height = 286
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 784
          Height = 270
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'MAVT'#9'18'#9'M'#227' h'#224'ng'#9'F'
            'DONGIA_REF'#9'10'#9'Gi'#225' v'#7889'n'#9'T'
            'DONGIA'#9'10'#9#272#417'n gi'#225#9'F'
            'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'
            'SOTIEN'#9'12'#9'Th'#224'nh ti'#7873'n'#9'T'
            'GHICHU'#9'50'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
      object PaTotal: TPanel
        Left = 0
        Top = 449
        Width = 784
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        Visible = False
        DesignSize = (
          784
          47)
        object Label24: TLabel
          Left = 656
          Top = 3
          Width = 66
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'T'#7893'ng Tr'#7883' gi'#225
          FocusControl = EdTriGiaTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object TntLabel9: TLabel
          Left = 544
          Top = 3
          Width = 79
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'T'#7893'ng s'#7889' l'#432#7907'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdTriGiaTT: TwwDBEdit
          Left = 656
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdQtyTotal: TwwDBEdit
          Left = 544
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOLUONG'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 650
    Top = 46
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 650
    ExplicitTop = 46
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 58
    Top = 260
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'KIEMKE'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 532
    Top = 306
    object QrNXXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 20
      FieldName = 'SCT'
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225
      DisplayWidth = 18
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrNXSODDH: TWideStringField
      DisplayWidth = 20
      FieldName = 'SODDH'
      Visible = False
      FixedChar = True
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXSOTIEN: TFloatField
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#170'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      DisplayWidth = 53
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXQUAYKE: TWideStringField
      FieldName = 'QUAYKE'
      Visible = False
      Size = 100
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrNXDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      FixedChar = True
      Size = 1
    end
    object QrNXNGAY2: TDateTimeField
      FieldName = 'NGAY2'
    end
    object QrNXSOTIEN_SI: TFloatField
      FieldName = 'SOTIEN_SI'
    end
    object QrNXSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 10
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterInsert
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from KIEMKE_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 560
    Top = 306
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      FixedChar = True
      Size = 15
    end
    object QrCTLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 46
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTLK_DVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTLK_QD1: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_QD1'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'QD1'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DVT1: TWideStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'LK_DVT1'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT1'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIANHAP: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIANHAP'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAP'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIANHAPVAT: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIANHAPVAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAPVAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIASI: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIASI'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIASI'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIASIVAT: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIASIVAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIASIVAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIAVON: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIAVON'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIAVON'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTDONGIA_REF: TFloatField
      FieldName = 'DONGIA_REF'
    end
    object QrCTDONGIA_REF2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DONGIA_REF2'
      Calculated = True
    end
    object QrCTDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 10
      FieldName = 'DONGIA'
      OnChange = QrCTDONGIAChange
    end
    object QrCTDONGIA2: TFloatField
      FieldName = 'DONGIA2'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 10
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTSOLUONG2: TFloatField
      FieldName = 'SOLUONG2'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTDONGIA_SI: TFloatField
      FieldName = 'DONGIA_SI'
    end
    object QrCTSOTIEN_SI: TFloatField
      FieldName = 'SOTIEN_SI'
    end
    object QrCTDONGIA_LE: TFloatField
      FieldName = 'DONGIA_LE'
    end
    object QrCTSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
      Visible = False
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
      Visible = False
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TINHTRANG'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrCTEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 532
    Top = 334
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 560
    Top = 334
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 400
    Top = 316
    object Tm1: TMenuItem
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnClick = CmdSearchExecute
    end
    object Lc1: TMenuItem
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnClick = CmdFilterExecute
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnClick = CmdClearFilterExecute
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnClick = CmdFilterComExecute
    end
  end
  object QrDM_QUAYKE: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_QUAYKE')
    Left = 644
    Top = 306
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 432
    Top = 316
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'SOTIEN'
      'SOTIEN_SI'
      'SOTIEN_LE')
    DetailFields.Strings = (
      'SOLUONG'
      'SOTIEN'
      'SOTIEN_SI'
      'SOTIEN_LE')
    Left = 356
    Top = 304
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 94
    Top = 260
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Caption = 'CmdSwitch'
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdImportExcel: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
      Hint = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdReRead: TAction
      Caption = 'CmdReRead'
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdImport2: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' thi'#7871't b'#7883' ki'#7875'm kho'
      OnExecute = CmdImport2Execute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdImportTxt: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Text'
      OnExecute = CmdImportTxtExecute
    end
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 273
    Top = 340
    object LydliutfileExcel1: TMenuItem
      Action = CmdImportExcel
    end
    object Lydliutfiletext1: TMenuItem
      Action = CmdImportTxt
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xachititchngt1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object MenuItem1: TMenuItem
      Action = CmdSapthutu
    end
  end
  object RzLauncher: TRzLauncher
    Action = 'Open'
    Timeout = -1
    WaitUntilFinished = True
    Left = 420
    Top = 396
  end
  object PopIm: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 384
    Top = 396
    object Lydliutthitbkimkho1: TMenuItem
      Action = CmdImport2
    end
  end
  object PopupMenu2: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 232
    Top = 304
    object Phiuvnchuynnib1: TMenuItem
      Tag = 1
      Caption = 'Theo gi'#225' b'#225'n'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      OnClick = CmdPrintExecute
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object Phiucginhp1: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'Theo gi'#225' v'#7889'n'
      Hint = 'In phi'#7871'u'
      OnClick = CmdPrintExecute
    end
  end
  object QrCsv: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=SOFTZ' +
      '_TEXT'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'"Daily.csv"'
      ' ')
    Left = 172
    Top = 324
  end
end
