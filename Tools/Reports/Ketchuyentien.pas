﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Ketchuyentien;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, isPanel, wwfltdlg,
  frameNgay, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid, ToolWin,
  RzPanel;

type
  TFrmKetchuyentien = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMST: TWideStringField;
    QrTCNGUOI: TWideStringField;
    QrTCSOTIEN: TFloatField;
    QrTCDGIAI: TWideMemoField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCMADT: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    Bevel1: TBevel;
    PA1: TisPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    Label18: TLabel;
    Label3: TLabel;
    DBText1: TDBText;
    EdSCT: TwwDBEdit;
    EdNguoi: TwwDBEdit;
    CbNgay: TwwDBDateTimePicker;
    DBMemo1: TDBMemo;
    EdSOTIEN: TwwDBEdit;
    CbPTTT: TwwDBLookupCombo;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    QrTCLCT: TWideStringField;
    QrTCLYDO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    CmdAudit: TAction;
    LbKHO: TLabel;
    CbMAKHO: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    QrTCDRC_STATUS: TWideStringField;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrTCLK_TENDT: TWideStringField;
    GrTk1: TRzGroupBox;
    QrTCMATK: TWideStringField;
    QrTCMATK2: TWideStringField;
    QrTCLK_TENTK: TWideStringField;
    QrTCLK_TENTK2: TWideStringField;
    TntLabel1: TLabel;
    Label4: TLabel;
    CbSTK: TwwDBLookupCombo;
    CbTenTK: TwwDBLookupCombo;
    EdNganhang: TwwDBEdit;
    EdChinhanh: TwwDBEdit;
    GrTk2: TRzGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    CbSTK2: TwwDBLookupCombo;
    CbTenTK2: TwwDBLookupCombo;
    EdNganhang2: TwwDBEdit;
    EdChinhanh2: TwwDBEdit;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTCTHANHTOAN: TFloatField;
    QrTCLK_MANH: TWideStringField;
    QrTCLK_MACN: TWideStringField;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    QrTCLK_MANH2: TWideStringField;
    QrTCLK_MACN2: TWideStringField;
    QrTCLK_NGANHANG2: TWideStringField;
    QrTCLK_CHINHANH2: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbPTTTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
  private
    mPTTT, mLCT: String;
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;
  public
	procedure Execute (r: WORD);
  end;

var
  FrmKetchuyentien: TFrmKetchuyentien;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.Execute;
begin
	mCanEdit := rCanEdit(r);
    if not mCanEdit then
    	QrTC.LockType := ltReadOnly;
    mLCT := 'KC';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE: String = 'PHIEU_KETCHUYEN';

procedure TFrmKetchuyentien.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.FormShow(Sender: TObject);
begin
	with DataMain do
    	OpenDataSets([QrDMKHO, QrPTKETCHUYEN, QrDMTK_NB, QrNganhang, QrNganhangCN]);
	mPTTT := DataMain.GetSysParam('DEFAULT_PTTT');

    with QrTC do
    begin
	    SetDisplayFormat(QrTC, sysCurFmt);
		SetShortDateFormat(QrTC);
        SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);
    end;

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exHideDrc;
    try
        with DataMain do
    	    CloseDataSets([QrPTTT]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
    begin
    	exShowDrc(DsTC);
        try
    		CbNgay.SetFocus;
        except
        end;
    end
	else
    begin
    	exHideDrc;
    	GrBrowse.SetFocus;
    end;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
    if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
    	        Sort := s;
        end;

    	if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdListRefeshExecute(Sender: TObject);
begin
 	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
    	QrDMKHO.Requery;
        QrPTTT.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdNewExecute(Sender: TObject);
begin
	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
    mTrigger := True;       // Skip BeforePost
   	MarkDataSet(QrTC);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsTC)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and exCheckLoc(QrTC, False);
    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    GrTk1.Enabled := QrTC.FieldByName('PTTT').AsString <> '01';
    GrTk2.Enabled := QrTC.FieldByName('PTTT').AsString <> '02';

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime  	:= d;
		FieldByName('LCT').AsString     	:= mLCT;
		FieldByName('PTTT').AsString    	:= mPTTT;
        FieldByName('MAKHO').AsString       := sysDefKho;
        FieldByName('LOC').AsString         := sysLoc;
        FieldByName('DRC_STATUS').AsString 	:= '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCBeforePost(DataSet: TDataSet);
var
    s: String;
begin
    if mTrigger then
        Exit;

    with QrTC do
    begin
		if BlankConfirm(QrTC, ['NGAY', 'PTTT', 'SOTIEN']) then
	    	Abort;

        s := FieldByName('PTTT').AsString;
        if s = '01' then
        begin
            if BlankConfirm(QrTC, ['MATK2']) then
	    	    Abort;
        end
        else if s = '02' then
        begin
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;
        end else
            if BlankConfirm(QrTC, ['MATK', 'MATK2']) then
	    	    Abort;

        if FieldByName('MATK').AsString = FieldByName('MATK2').AsString then
        begin
            ErrMsg('Số tài khoản nhận không được trùng với tài khoản chuyển.');
            Abort;
        end;

        exValidClosing(FieldByName('NGAY').AsDateTime, 2);

	    SetNull(QrTC, ['LYDO', 'MAKHO', 'MADT']);
    end;

    exDrcValidate(QrTC);
    DataMain.AllocSCT(mLCT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exCanEditVoucher(QrTC);
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrTC.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCPTTTChange(Sender: TField);
begin
    with QrTC do
    begin
        if Sender.AsString <> '01' then
        begin
            FieldByName('MATK2').Clear;
            EdChinhanh2.Text := EdChinhanh2.Field.AsString;
            EdNganhang2.Text := EdNganhang2.Field.AsString;
        end
        else
        begin
            FieldByName('MATK').Clear;
            EdChinhanh.Text := EdChinhanh.Field.AsString;
            EdNganhang.Text := EdNganhang.Field.AsString;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CbPTTTNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CmdAuditExecute(Sender: TObject);
begin
    ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetchuyentien.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;



end.
