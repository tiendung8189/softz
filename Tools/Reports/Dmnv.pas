﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmnv;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Grids, Wwdbigrd,
  Wwdbgrid, Mask, RzPanel, ToolWin, wwdblook;

type
  TFrmDmnv = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMNV: TADOQuery;
    DsDMNV: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton10: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    EdTen: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    EdMa: TwwDBEdit;
    PD5: TisPanel;
    EdGhichu: TDBMemo;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    CmdReload: TAction;
    QrDMNVMANV: TWideStringField;
    QrDMNVTENNV: TWideStringField;
    QrDMNVDTHOAI: TWideStringField;
    QrDMNVDCHI: TWideStringField;
    QrDMNVEMAIL: TWideStringField;
    QrDMNVTHOIVIEC: TBooleanField;
    QrDMNVCREATE_BY: TIntegerField;
    QrDMNVUPDATE_BY: TIntegerField;
    QrDMNVCREATE_DATE: TDateTimeField;
    QrDMNVUPDATE_DATE: TDateTimeField;
    QrDMNVGHICHU: TWideMemoField;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit2: TwwDBEdit;
    QrDMNVCHUCVU: TWideStringField;
    QrDMNVDIDONG: TWideStringField;
    ALLOC_MANV: TADOCommand;
    N1: TMenuItem;
    mnObsolete: TMenuItem;
    PD2: TisPanel;
    wwDBEdit3: TwwDBEdit;
    Label7: TLabel;
    QrDMNVLUONG: TFloatField;
    Label9: TLabel;
    QrDMNVTHOIVIEC_NGAY: TDateTimeField;
    DBCheckBox1: TDBCheckBox;
    CbNGAY: TwwDBDateTimePicker;
    QrDMNVCHU_CUAHANG: TBooleanField;
    CkbManager: TDBCheckBox;
    QrDMNVMAPB: TWideStringField;
    Label30: TLabel;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMNVBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMNVBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMNVBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMNVAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDMNVAfterInsert(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure mnObsoleteClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure QrDMNVMANVChange(Sender: TField);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mObsolete: Boolean;
    mSQL: String;

    function  AllocMANV(pLen: Integer): String;
  public
  	function Execute(r: WORD): String;
  end;

var

  FrmDmnv: TFrmDmnv;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, RepEngine, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_NHANVIEN';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
function TFrmDmnv.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsDMNV.AutoEdit := mCanEdit;

	EdGhichu.ReadOnly := not mCanEdit;

    mRet := False;
    ShowModal;
    Result := QrDMNV.FieldByName('MANV').AsString;
    CloseDataSets([QrDMNV]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    mTrigger := False;
    SetShortDateFormat(QrDMNV);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMNV, FORM_CODE, Filter);

    PaDetail.VertScrollBar.Position := 0;

    PD5.Collapsed := RegReadBool(name, 'PD5');
    PD2.Collapsed := RegReadBool(name, 'PD2');
    mObsolete     := RegReadBool('Obsolete');

    mAutoCode := FlexReadBool(FORM_CODE, 'Auto Code');
    fixCode := SetCodeLength(FORM_CODE, QrDMNV.FieldByName('MANV'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormShow(Sender: TObject);
begin
    EdMa.ReadOnly := mAutoCode;
	mSQL := QrDMNV.SQL.Text;

    with DataMain do
        OpenDataSets([QrDMPB]);

    CmdReload.Execute;

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.mnObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;

	//Save state panel
    RegWrite(Name, ['PD5','PD2', 'Obsolete'],
        [PD5.Collapsed,PD2.Collapsed, mObsolete]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMNV, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdNewExecute(Sender: TObject);
begin
    QrDMNV.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdSaveExecute(Sender: TObject);
begin
    QrDMNV.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdCancelExecute(Sender: TObject);
begin
    QrDMNV.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdDelExecute(Sender: TObject);
begin
    QrDMNV.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMNV)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrDMNV, Filter, mCanEdit);

	with QrDMNV do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdReload.Enabled := bBrowse;
    CmdContact.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMNV);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.PopupMenu1Popup(Sender: TObject);
begin
    mnObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVBeforePost(DataSet: TDataSet);
begin
	with QrDMNV do
    begin
    	if BlankConfirm(DataSet, ['MAPB', 'TENNV']) then
        	Abort;

    	if not mAutoCode then
        begin
            if BlankConfirm(DataSet, ['MANV']) then
        	    Abort;

            if fixCode then
            	if LengthConfirm(FieldByName('MANV')) then
                	Abort;
        end
        else
    		if State in [dsInsert] then
            	FieldByName('MANV').AsString :=
                	AllocMANV(FieldByName('MANV').DisplayWidth);

        if FieldByName('THOIVIEC').AsBoolean then
        begin
            if BlankConfirm(DataSet, ['THOIVIEC_NGAY']) then
                Abort;
        end
        else if not FieldByName('THOIVIEC_NGAY').IsNull then
                   FieldByName('THOIVIEC_NGAY').Clear;
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVMANVChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMNV, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CbNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmnv.AllocMANV(pLen: Integer): String;
begin
    with ALLOC_MANV do
    begin
        Prepared := True;
        Parameters[1].Value := pLen;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdRefreshExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdReloadExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;
	with QrDMNV do
    begin
    	Close;
        if not mObsolete then
            sSQL := sSQL + ' where (THOIVIEC = 0 or THOIVIEC = Null)';
        SQL.Text := sSQL;
        SQL.Add(' order by 	MANV');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVAfterInsert(DataSet: TDataSet);
begin
	with QrDMNV do
	begin
        FieldByName('THOIVIEC').AsBoolean := False;
    end
end;

end.
