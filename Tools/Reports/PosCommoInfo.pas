(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosCommoInfo;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Db, ADODB,
  wwDataInspector, Grids;

type
  TFrmCommoInfo = class(TForm)
    EdMA: TEdit;
    Inspect: TwwDataInspector;
    DsDMVT: TDataSource;
    QrDMVT: TADOQuery;
    QrDMVTTENVT: TWideStringField;
    QrDMVTTENTAT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTMAVT: TWideStringField;
    QrDMVTDVT1: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTGIASIVAT: TFloatField;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  	RefDmvt: TADOQuery;
  public
  	procedure Execute(Sender: TADOQuery);
  end;

var
  FrmCommoInfo: TFrmCommoInfo;

implementation

uses
	ExCommon, isDb, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.Execute;
begin
    RefDmvt := Sender;
	with RefDmvt do
        if not Active then
            Open;
	ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormShow(Sender: TObject);
begin
    SetDisplayFormat(QrDMVT, sysCurFmt);
    SetDisplayFormat(RefDmvt, sysCurFmt);
	EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	QrDMVT.Close;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.EdMAKeyPress(Sender: TObject; var Key: Char);
var
	n: Integer;
	s: String;
begin
		// Is CRLF keystroke
	if Key <> #13 then
    	Exit;

    // Is empty
    s := Trim(EdMA.Text);
    if s = '' then
        Exit;

    Key := #0;

    // Quick select
    n := IsDotSelect(s);
    if n <> 0 then
    begin
        // Show dialog
        if not exDotMavt(1, RefDmvt, s) then
        begin
            EdMA.Text := '';
            Exit;
        end;
        EdMA.Text := s;
    end;

    with QrDMVT do
    begin
        Close;
        Parameters[0].Value := edMA.Text;
        Parameters[1].Value := edMA.Text;
        Open;
    end;
    EdMa.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

end.
