﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosMain;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, ActnList, Menus, ExtCtrls,
  StdCtrls, Wwdbigrd, Wwdbgrid2, Mask, Db, ADODB, wwdblook, isEnv, AppEvnts,
  AdvMenus, fcStatusBar, RzPanel, Controls, wwdbedit, HTMLabel,
  RzButton, isDb, ImgList, Grids, Wwdbgrid, pngimage, isOneInstance, exPrintBill2,
  AdvGlassButton, AdvGlowButton, AdvSmoothButton, AdvMetroButton, AdvSelectors,
  AdvOfficeSelectors, wwdbdatetimepicker;

type
  TFrmMain = class(TForm)
    MyActionList: TActionList;
    CmdQuit: TAction;
    CmdSetpass: TAction;
    ImgLarge: TImageList;
    CmdSave: TAction;
    Bevel1: TBevel;
    QrBH: TADOQuery;
    DsBH: TDataSource;
    QrCTBH: TADOQuery;
    DsCTBH: TDataSource;
    QrCTBHMAVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHLK_TENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    CmdReprint: TAction;
    CmdDiscount: TAction;
    QrTAM: TADOQuery;
    QrINLAI: TADOQuery;
    CmdScanReturnQty: TAction;
    QrCTBHSTT: TIntegerField;
    Bevel4: TBevel;
    QrCTBHTHUE_SUAT: TFloatField;
    QrCTBHTRA_DATE: TDateTimeField;
    QrBHSPTTT: TWideStringField;
    CmdPrint: TAction;
    QrCTBHTL_CK: TFloatField;
    QrCTBHCHIETKHAU: TFloatField;
    QrBHCALC_TONGCK: TFloatField;
    QrCTBHTENTAT: TWideStringField;
    Popup: TAdvPopupMenu;
    CmdScanVIP: TAction;
    POS_GIABAN: TADOCommand;
    POS_VIP: TADOCommand;
    N1: TMenuItem;
    imtkhu1: TMenuItem;
    Hthng1: TMenuItem;
    CmdLock: TAction;
    CmdCommInfo: TAction;
    QrCTBHSOTIEN: TFloatField;
    CmdSetPrinter: TAction;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    CmdSave2: TAction;
    POS_CLEANUP: TADOCommand;
    CmdDoanhso: TAction;
    QrBHCALC_TIENTHOI: TFloatField;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHRSTT: TIntegerField;
    QrCTBHCALC_SOTIEN_SAUCK: TFloatField;
    PaHeader: TPanel;
    HTMLabel15: THTMLabel;
    EdCounter: TwwDBEdit;
    HTMLabel17: THTMLabel;
    EdCashier: TwwDBEdit;
    LbCode: TLabel;
    Image4: TImage;
    EdCode: TwwDBEdit;
    HTMLabel3: THTMLabel;
    HTMLabel6: THTMLabel;
    HTMLabel8: THTMLabel;
    wwDBEdit13: TwwDBEdit;
    wwDBEdit27: TwwDBEdit;
    wwDBEdit28: TwwDBEdit;
    Status: TfcStatusBar;
    ImageList1: TImageList;
    HTMLabel13: THTMLabel;
    EdCus: TwwDBEdit;
    CmdScanSku: TAction;
    CmdScanQty: TAction;
    QrCTBHTL_CK2: TFloatField;
    QrCTBHTIEN_THUE: TFloatField;
    QrCTBHTL_CK3: TFloatField;
    QrCTBHTL_CK5: TFloatField;
    QrCTBHTL_CK6: TFloatField;
    HTMLabel1: THTMLabel;
    EdRno: TwwDBEdit;
    QrBHLK_TENKHO: TWideStringField;
    CmdChangeRetailer: TAction;
    QrBHLCT: TWideStringField;
    QrBHPTTT: TWideStringField;
    QrBHNGAY: TDateTimeField;
    QrBHQUAY: TWideStringField;
    QrBHCA: TWideStringField;
    QrBHSCT: TWideStringField;
    QrBHMAVIP: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    QrBHCK_BY: TIntegerField;
    QrBHTL_CK: TFloatField;
    QrBHCHIETKHAU: TFloatField;
    QrBHTHUE: TFloatField;
    QrBHSOTIEN: TFloatField;
    QrBHSOTIEN1: TFloatField;
    QrBHSOLUONG: TFloatField;
    QrBHCHUATHOI: TFloatField;
    QrBHTHANHTOAN: TFloatField;
    QrBHQUAYKE: TWideStringField;
    QrBHDGIAI: TWideMemoField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_BY: TIntegerField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHPTTT2: TWideStringField;
    QrBHTL_CK2: TFloatField;
    QrBHTTOAN1: TFloatField;
    QrBHTTOAN2: TFloatField;
    QrCTBHLOAITHUE: TWideStringField;
    QrCTBHCK_BY: TIntegerField;
    QrCTBHTRA_BY: TIntegerField;
    QrCTBHMABO: TWideStringField;
    POS_BARCODE_VALID: TADOCommand;
    QrDMVT_BO: TADOQuery;
    QrCTBHTL_CK4: TFloatField;
    HTMLabel4: THTMLabel;
    CmdReconnect: TAction;
    Ktnicsdliu1: TMenuItem;
    POS_PAID: TADOCommand;
    QrDMVT: TADOQuery;
    CmdReLoad: TAction;
    GetUPDATE_DATE: TADOCommand;
    QrBHTL_CK_TAM: TFloatField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrBHTTTT: TWideStringField;
    CmdCongnobl: TAction;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHLK_TENTK: TWideStringField;
    QrBHLK_DAIDIEN: TWideStringField;
    QrBHLK_NGANHANG: TWideStringField;
    QrCTBHTL_CK7: TFloatField;
    CmdDiscount1: TAction;
    isOneInstance1: TisOneInstance;
    QrBHPRINT_NO: TIntegerField;
    QrBHLOC: TWideStringField;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    CmdChitiet: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    AdvGlowButton12: TAdvGlowButton;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton18: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    AdvGlowButton20: TAdvGlowButton;
    Panel11: TPanel;
    Panel12: TPanel;
    QrBHSO_CHUANCHI: TWideStringField;
    HtmlCurSoluong: THTMLabel;
    LbCurSoluong: TLabel;
    QrCTBHLK_TENVT_MARK: TWideStringField;
    HTMLabel2: THTMLabel;
    HTMLabel5: THTMLabel;
    EdLocation: TwwDBLookupCombo;
    EdDate: TwwDBDateTimePicker;
    QrDMKHO: TADOQuery;
    CmdBanle: TAction;
    CmdTrahang: TAction;
    N2: TMenuItem;
    rhng1: TMenuItem;
    Hanbnl1: TMenuItem;
    QrCTBHTL_CK_MAX: TFloatField;
    QrCTBHTL_CK_THEM: TFloatField;
    QrCTBHTENVT: TWideStringField;
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdSetpassExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdReprintExecute(Sender: TObject);
    procedure QrBHAfterPost(DataSet: TDataSet);
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrBHAfterInsert(DataSet: TDataSet);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure QrCTBHAfterPost(DataSet: TDataSet);
    procedure CmdDiscountExecute(Sender: TObject);
    procedure QrDBError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure CmdScanReturnQtyExecute(Sender: TObject);
    procedure QrBHBeforeOpen(DataSet: TDataSet);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure CmdHelpExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
      FooterCellRect: TRect; Field: TField; FooterText: String;
      var DefaultDrawing: Boolean);
    procedure GrListUpdateFooter(Sender: TObject);
    procedure GrListKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAboutExecute(Sender: TObject);
    procedure CmdScanVIPExecute(Sender: TObject);
    procedure CmdLockExecute(Sender: TObject);
    procedure CmdCommInfoExecute(Sender: TObject);
    procedure CmdFaqExecute(Sender: TObject);
    procedure QrCTBHTL_CKChange(Sender: TField);
    procedure CmdSetPrinterExecute(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure CmdSave2Execute(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDoanhsoExecute(Sender: TObject);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure QrCTBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHAfterDelete(DataSet: TDataSet);
    procedure EdCodeEnter(Sender: TObject);
    procedure EdCodeKeyPress(Sender: TObject; var Key: Char);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdScanSkuExecute(Sender: TObject);
    procedure CmdScanQtyExecute(Sender: TObject);
    procedure QrTAMBeforeOpen(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdCodeExit(Sender: TObject);
    procedure QrBHTTOAN1Change(Sender: TField);
    procedure QrCTBHTL_CK2Change(Sender: TField);
    procedure QrBHMAKHOChange(Sender: TField);
    procedure CmdChangeRetailerExecute(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure CmdReLoadExecute(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCongnoblExecute(Sender: TObject);
    procedure CmdDiscount1Execute(Sender: TObject);
    procedure QrBHCN_DIACHI_HDChange(Sender: TField);
    procedure CmdChitietExecute(Sender: TObject);
    procedure QrCT1CalcFields(DataSet: TDataSet);
    procedure CmdBanleExecute(Sender: TObject);
    procedure CmdTrahangExecute(Sender: TObject);
    procedure QrCTBHTL_CK_MAXChange(Sender: TField);
    procedure QrCTBHMAVTChange(Sender: TField);
  private
    mPTTT: String;
  	mVATMode: Integer;
    mLastDate, mLastUpDate: TDateTime;
    mDefSoluong, mNhanTT: Double;
    mNhacnhoTT: Boolean;

    mPrinter: TexPrintBill;

    // Hóa đơn hiện tại
    curKhoa: TGUID;

    // Thông tin mặt hàng scan sau cùng
    curStt: Integer;
    curSoluong, curGiaban, curTlck7, curTlck3, curThsuat, curSlBo, curTlckBo: Double;
    curLThue: String;

    function  IsEnableFunc(const pFunc: String): Boolean;
    procedure OpenQueries;
    procedure Total(pReOrder: Boolean= False);
    function  GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
    procedure GetCustomerName;

	procedure InvalidAction(msg: String = '');
    procedure Cleanup;
	procedure ScanType;

    function  ValidBarcode(pma:String; var bo: Boolean): Boolean;

    procedure ItemAddNew(const ma: String; const pMabo: String = '');
    procedure ItemSetQty(const pSoluong: Double);

    procedure ProcScanCode;
    procedure ProcScanQty;
    procedure ProcScanVIP;
    procedure ProcScanRetQty;

    function IsChangeDMHH: Boolean;
  public
    dThanhtoan, ePttt2 : Boolean;
    procedure GoLast;
    function  SaveBill(pPrinted: Boolean; pDesc: String = ''): Boolean;
    procedure PrintBill(pKhoa: TGUID);
    Function exThelenh: Boolean;
  end;

var
  FrmMain: TFrmMain;

implementation

uses
	MainData, ExCommon, isMsg, isStr, SoBill, Rights, CkBill, isDba, isOdbc,
    isLib, PosCommoInfo, RepEngine, Printer, TempReceipt, CkMa,
    ReceiptDesc, isCommon, exResStr, PosVIP, Retailer, Payment,
    Variants, ADOInt, GuidEx, CongnoBanle, TheLenh, exCOMPort, SystemCriticalU,
    PosChitiet, Banle, Nhaptrabl;

{$R *.DFM}

const
	FORM_CODE = 'POS';

{$REGION '<< Form events >>'}
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormCreate(Sender: TObject);
const
	imgext: array[1..3] of String = ('png', 'jpg', 'bmp');
var
	i: Integer;
begin

    // Smart load logo
//    for i := 1 to Length(imgext) do
//    begin
//	    s := sysAppPath + 'System\POSz.' + imgext[i];
//	    if FileExists(s) then
//        begin
//    	    Image.Picture.LoadFromFile(s);
//            Break;
//        end;
//    end;

//    GrList.PaintOptions.AlternatingRowColor := clNone;
	{Form Show
    // Other init
	SetDefButton(0);
    ShortTimeFormat := 'hh:nn';
	mTrigger := False;
    mLastDate := Date;
	}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

procedure TFrmMain.FormShow(Sender: TObject);
const
	imgext: array[1..3] of String = ('png', 'jpg', 'bmp');
var
	i, h: Integer;
	s: String;
begin
	TMyForm(Self).Init2;
    LoadCustomIcon;
//    Status.Panels[5].Text := 'Build ' + GetModuleVersion;
    DbeInitial;
    FlexInitial;

    // Smart load logo
    for i := 1 to Length(imgext) do
    begin
	    s := sysAppPath + 'System\POSz.' + imgext[i];
	    if FileExists(s) then
        begin
//    	    Image.Picture.LoadFromFile(s);
//            Break;
        end;
    end;

    // Other init
    SetDefButton(0);

    ShortTimeFormat := 'hh:nn';
	mTrigger := False;
    mLastDate := Now;

	    (*
    	** Optional settings
	    *)
    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexReadInteger(FORM_CODE, 'VAT Form');

    dThanhtoan := FlexReadBool(FORM_CODE, 'Defaul Thanh Toan', True);
    ePttt2 := FlexReadBool(FORM_CODE, 'Enable Pttt2', True);
    mNhanTT := DataMain.GetSysParam('BL_DONVI_TIENTHOI');
    mNhacnhoTT := DataMain.GetSysParam('BL_NHACNHO_TIENTHOI');

    // Init bill printer
    exBillInitial(mPrinter);

    CmdChangeRetailer.Visible := IsEnableFunc('POS_CHANGE_RETAILER');
    CmdScanVIP.Enabled := IsEnableFunc('SZ_VIP');

//    EdDate.ReadOnly := not sysIsCentral;
//    EdDate.ShowButton := sysIsCentral;
//    EdLocation.ReadOnly := not sysIsCentral;
//    EdLocation.ShowButton := sysIsCentral;
//
    // Tuy bien luoi
	SetCustomGrid(FORM_CODE, GrList);

	// Cleanup empty receipt
	Cleanup;

    // Open database
    with DataMain do
    begin
    	OpenDatasets([QrDMKHO, QrPTTT_BILL, QrUser, QrDMTK]);
		mPTTT := QrPTTT_BILL.FieldByName('MA').AsString;
    end;
    OpenDataSets([QrDMKHO]);

    // OpenDataSets([QrDMVT]);
    IsChangeDMHH;
	OpenQueries;

    // Display format
	SetDisplayFormat(QrBH, sysCurFmt);
    SetDisplayFormat(QrBH,['SOLUONG'], sysQtyFmt);
    SetShortDateFormat(QrBH);

    SetDisplayFormat(QrDmvt, sysCurFmt);
    SetDisplayFormat(QrDmvt, ['TL_LAI'], sysPerFmt);

    with QrCTBH do
    begin
		SetDisplayFormat(QrCTBH, sysCurFmt);
		SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
		SetDisplayFormat(QrCTBH, ['TL_CK', 'TL_CK2', 'TL_CK3', 'TL_CK4',
                        'TL_CK5', 'TL_CK6', 'TL_CK7'], sysPerFmt);
        SetDisplayFormat(QrCTBH, ['THUE_SUAT'], sysTaxFmt);
    end;

 	// Form initial
    SetApplicationVersion;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');

    EdCashier.Text := sysLogonFullName;
	MyActionList.Tag := 1;
    TExComPort.IcdComCreate(DataMain.GetSysParam('ICD_STR1'), DataMain.GetSysParam('ICD_STR2'));
    SystemCritical.IsCritical := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
	b: Boolean;
begin
    // Cho close nếu thấy connection failured
	b := False;
	{try
		GetDbServerDate;
    except
		b := True;
    end;   }

    if b and (DataMain.Conn.Errors.Count > 0) then
    begin
    	CanClose := True;
        Exit;
    end;

    // Confirm for bill complete
    CanClose := QrCTBH.IsEmpty;
    if not CanClose then
    begin
    	ErrMsg('Phải hoàn tất hóa đơn.');
        Exit;
    end;

	SaveBill(False);

    // Clean
	Cleanup;

	with QrTAM do
    begin
		Close;
    	Open;
        CanClose := IsEmpty;
    end;

    if not CanClose then
        ErrMsg('Phải giải quyết tất cả hóa đơn còn treo lại.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    exBillFinal(mPrinter);

    FlexFinal;
    try
        DataMain.isLogon.Logoff;
    except
    end;

    DbeFinal;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if ActiveControl = EdCode then
    	Exit;
	Enter2Tab(Self, Key);
end;
{$ENDREGION}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetpassExecute(Sender: TObject);
begin
	DataMain.isLogon.ResetPassword;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLockExecute(Sender: TObject);
begin
	DataMain.isLogon.Lock;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHelpExecute(Sender: TObject);
begin
	ShowHelp;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdAboutExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmAbout, FrmAbout);
//    FrmAbout.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBanleExecute(Sender: TObject);
var
    r: WORD;
begin
    r := DataMain.GetRights('SZ_SUABILL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBanle, FrmBanle);
    FrmBanle.Execute(r, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
    if not YesNo(RS_RECONNECT) then
        Exit;
//    DbConnect;
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReLoadExecute(Sender: TObject);
begin
    Wait(DATAREADING);
    with QrDMVT do
        if Active then
            Requery
        else
            Open;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	with QrCTBH do
		b := not IsEmpty;

    CmdChangeRetailer.Enabled := not b;
    CmdScanReturnQty.Enabled := b;
    CmdReprint.Enabled := not b;
    CmdSetPrinter.Enabled := mPrinter.PrintType = 0;
    CmdSetPrinter.Visible := mPrinter.PrintType = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChangeRetailerExecute(Sender: TObject);
var
    n, mUser: Integer;
begin
    with QrTAM do
    begin
		Close;
    	Open;
        n := RecordCount;
        if (n > 1) or ((n = 1) and
            (not TGuidEx.EqualGuids(
            	TGuidField(FieldByName('KHOA')).AsGuid,
            	TGuidField(QrBH.FieldByName('KHOA')).AsGuid))) then
        begin
            ErrMsg('Phải giải quyết tất cả hóa đơn còn treo lại.');
            Exit;
        end;
    end;

    if DataMain.GetRights('POS_CHANGE_RETAILER', False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if DataMain.GetRights(mUser, 'POS_CHANGE_RETAILER') = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    Application.CreateForm(TFrmRetailer, FrmRetailer);
    if FrmRetailer.execute(posMakho, posQuay, posQuay0) then
    with QrBH do
    begin
        FieldByName('MAKHO').AsString := posMakho;
        FieldByName('QUAY').AsString := posQuay;
        posSct := DataMain.AllocRetailBillNumber(posMakho, posQuay, Date);
        FieldByName('SCT').AsString := posSct;
        posSct := isSmartInc(posSct, 2);
    end;
    FrmRetailer.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCommInfoExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmCommoInfo, FrmCommoInfo);
    FrmCommoInfo.Execute(DataMain.QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongnoblExecute(Sender: TObject);
begin
//    r := DataMain.GetRights('SZ_SUABILL');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmCongnoBanle, FrmCongnoBanle);
    FrmCongnoBanle.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSaveExecute(Sender: TObject);
begin
    if QrCTBH.IsEmpty then
    	Exit;

    if not YesNo('Chỉ lưu và không in hóa đơn. Tiếp tục?') then
        Exit;

    with QrBH do
    begin
        SetEditState(QrBH);
        mTrigger := True;
        if (FieldByName('PTTT').AsString = mPTTT) and (FieldByName('PTTT2').AsString = '') then
            FieldByName('TTOAN1').AsFloat := FieldByName('THANHTOAN').AsFloat;
        mTrigger := False;
    end;

    SaveBill(True);
    curKhoa := TGuidEx.EmptyGuid;
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanQtyExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 1;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanSkuExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 0;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PrintBill(pKhoa: TGUID);
begin
    try
        if mPrinter.BillPrint(pKhoa) then
            DataMain.UpdatePrintNo(pKhoa);
    except
        on E: Exception do
            ErrMsg(E.Message, 'Lỗi in Bill');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPrintExecute(Sender: TObject);
begin
    if QrCTBH.IsEmpty then
    	Exit;

    Application.CreateForm(TFrmPayment, FrmPayment);
    if not FrmPayment.Execute(1) then
    	Exit;

	PrintBill(curKhoa);
    curKhoa := TGuidEx.EmptyGuid;
    OpenQueries;
    EdCode.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_RECEIPT_NO = 'Số hóa đơn không đúng.';

procedure TFrmMain.CmdReprintExecute(Sender: TObject);
var
    s: String;
    m, y: Integer;
    dd, mm, yy: WORD;
    k: TGUID;
begin
	EdCode.SetFocus;
   	DecodeDate(Date, yy, mm, dd);
    m := mm;
    y := yy;
	s := lastSct;

    Application.CreateForm(TFrmSoBill, FrmSoBill);
	if not FrmSoBill.GetBillNo(m, y, s) then
        Exit;

	// Lay khoa chung tu
    with QrINLAI do
    begin
    	Parameters[0].Value := m;
    	Parameters[1].Value := y;
    	Parameters[2].Value := posQuay;
        Parameters[3].Value := posMakho;
    	Parameters[4].Value := s;
    	Open;
        if IsEmpty then
        begin
        	Msg(RS_RECEIPT_NO);
            Close;
            Exit;
        end;
        k := TGuidField(FieldByName('KHOA')).AsGuid;
        Close;
    end;

    // In lai
    PrintBill(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscount1Execute(Sender: TObject);
var
	mMa: String;
    mCk: Double;
    bm: TBytes;
    mUser: Integer;
begin
	if QrCTBH.FieldByName('DONGIA').AsFloat = 0.0 then
		Exit;

    if DataMain.GetRights('POS_CHIETKHAU_MH', False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if DataMain.GetRights(mUser, 'POS_CHIETKHAU_MH') = R_DENY then
        begin
            SetDefButton(1);
            DenyMsg;
            SetDefButton(0);
            Exit;
        end;
    end;

	with QrCTBH do
    begin
    	if IsEmpty then
        	Exit;

        mCk := FieldByName('TL_CK6').AsFloat;

        Application.CreateForm(TFrmCkma, FrmCkma);
        if not FrmCkma.GetDiscount(FieldByName('TENVT').AsString, mCk) then
        	Exit;

        mMa := FieldByName('MAVT').AsString;
        bm := BookMark;
        DisableControls;

        First;
        while not Eof do
        begin
        	if (FieldByName('MAVT').AsString = mMa) and
               (FieldByName('DONGIA').AsFloat <> 0.0) then
            begin
		        Edit;
				FieldByName('TL_CK6').AsFloat := mCk;
				FieldByName('CK_BY').AsInteger := mUser;
            end;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;
	end;
    EdCode.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscountExecute(Sender: TObject);
var
	mUser: Integer;
    dgiai: String;
    k: Double;
begin
    EdCode.SetFocus;
    if DataMain.GetRights('POS_CHIETKHAU', False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if DataMain.GetRights(mUser, 'POS_CHIETKHAU') = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    with QrBH do
    begin
        k := FieldByName('TL_CK_TAM').AsFloat;
        dgiai := FieldByName('DGIAI').AsString;
    end;
    // Get discount info.
    Application.CreateForm(TFrmChietkhau, FrmChietkhau);
    if not FrmChietkhau.Get(k, dgiai) then
    	Exit;

    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('TL_CK_TAM').AsFloat := k;
        FieldByName('CK_BY').AsInteger := mUser;
        FieldByName('DGIAI').AsString := dgiai;
    end;
    Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanReturnQtyExecute(Sender: TObject);
var
	mUser: Integer;
begin
	EdCode.SetFocus;
    if DataMain.GetRights('POS_TRAHANG', False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
        if DataMain.GetPassCode(posMakho) <> '' then  //test. Will remove
        begin
            if not exThelenh then
                Exit;
            mUser := sysLogonUID;
        end
        else
        begin
            // Logon
            if not DataMain.isLogon.Logon3(mUser) then
                Exit;

            if DataMain.GetRights(mUser, 'POS_TRAHANG') = R_DENY then
            begin
                DenyMsg;
                Exit;
            end;
        end;
    end;

	with EdCode do
    begin
    	Tag := 3;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_VIP = 'Sai mã khách hàng thân thiết.';

procedure TFrmMain.CmdScanVIPExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 2;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;
	(*
    ** Functions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.SaveBill(pPrinted: Boolean; pDesc: String = ''): Boolean;
begin
   	Result := False;
	with QrCTBH do
    begin
    	CheckBrowseMode;
    	if IsEmpty then
            Exit;
	end;

    Total(True);
    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('PRINTED').AsBoolean := pPrinted;
		{
        if pPrinted then		// Thanh toan xong
			FieldByName('TINHTRANG').AsString := '3';
		}
        if pDesc <> '' then		// Luu tam
        begin
	        FieldByName('DGIAI').AsString := pDesc;
//            FieldByName('SCT').AsString := '00000';
        end;

    	Post;
    end;

    if pPrinted then
        with POS_PAID do
        begin
            Prepared := True;
            Parameters[1].Value := TGuidEx.ToString(QrBH.FieldByName('KHOA'));
            try
                Execute;
            except
                if DataMain.Conn.Errors.Count > 0 then
                    ErrMsg(DataMain.Conn.Errors[0].Description + #13 + RS_ADMIN_CONTACT);
                Exit;
            end;
        end;

	{
    // Tính doanh số VIP
	if pPrinted then		// Thanh toan xong
        with POS_VIP_DOANHSO do
        begin
            Prepared := True;
            Parameters[1].Value := QrBH.FieldByName('KHOA').AsInteger;
            Execute;
        end;
	}

    // Done
   	Result := True;
    icdComPort.IcdThanks;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh: Double;
	mSotien, mSoluong, mCkmh, mTlck5, mTlck2, mThue: Double;
begin
	if mTrigger then
    	Exit;

	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mThue := 0;
    mTlck5 := QrBH.FieldByName('TL_CK_TAM').AsFloat;
    mTlck2 := QrBH.FieldByName('TL_CK2').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
            if pReOrder and (FieldByName('STT').AsInteger <> FieldByName('RSTT').AsInteger)then
            begin
                try
                    mTrigger := True;
                    SetEditState(QrCTBH);
                    FieldByName('STT').AsInteger := FieldByName('RSTT').AsInteger;
                    Post;
                finally
                    mTrigger := False;
                end;
            end;

            if FieldByName('TL_CK5').AsFloat <> mTlck5 then
            begin
                SetEditState(QrCTBH);
            	FieldByName('TL_CK5').AsFloat := mTlck5;
            end;

            // CK VIP theo danh muc VIP
        	if FieldByName('TL_CK2').AsFloat <> mTlck2 then
            begin
                SetEditState(QrCTBH);
            	FieldByName('TL_CK2').AsFloat := mTlck2;
            end;

            // Xoa chiet khau khi xoa VIP  - theo ck nhom hang
            if (QrBH.FieldByName('MAVIP').AsString = '') and (FieldByName('TL_CK3').AsFloat > 0) then
            begin
                SetEditState(QrCTBH);
                FieldByName('TL_CK3').Clear;
            end;

        	xSotien := FieldByName('SOTIEN').AsFloat;			// Thanh tien chua chiet khau
            xThsuat := FieldByName('THUE_SUAT').AsFloat;		// Thue suat
            xCkmh := FieldByName('CHIETKHAU').AsFloat;			// Tien CKMH

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
	        mCkmh := mCkmh + xCkmh;

            mThue := mThue + FieldByName('TIEN_THUE').AsFloat;

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
    	FieldByName('CHIETKHAU_MH').AsFloat := mCkmh;
    	FieldByName('THUE').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mSotien - mCkmh;

        CheckBrowseMode;
    end;

    GrListUpdateFooter(GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.ValidBarcode(pma: String; var bo: Boolean): Boolean;
begin
    with POS_BARCODE_VALID do
    begin
        Prepared := True;
        Parameters[1].Value := pma;
        Parameters[2].Value := Now;
        try
            Execute;
            bo := Parameters[3].Value;
        except
        end;
        if DataMain.Conn.Errors.Count > 0 then
            InvalidAction(DataMain.Conn.Errors[0].Description);

        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.OpenQueries;
begin
	CloseDataSets([QrBH, QrCTBH]);
    OpenDataSets([QrBH, QrCTBH]);

    with QrBH do
    begin
	    if IsEmpty then
		    Append
        else
        begin
            with QrCTBH do
            begin
            	curStt := RecordCount;
                Last;
            end;
            Edit;
        end;
    end;

    mDefSoluong := 1;
    GetCustomerName;
    EdCode.SetFocus;
end;

(*==============================================================================
** Last record
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GoLast;
begin
    with QrCTBH do
    begin
        Last;
        curSoluong := FieldByName('SOLUONG').AsFloat;
    end;
end;

(*==============================================================================
	@MAVT		nvarchar(15),

	@GIA		float out,	-- Gia ban
	@TL_CK		float out,	-- Chiet khau
	@VAT		float out
**------------------------------------------------------------------------------
*)
function TFrmMain.GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
begin
	with POS_GIABAN do
    begin
    	Prepared := True;
        Parameters[1].Value := QrBH.FieldByName('MAKHO').AsString;
        Parameters[2].Value := QrBH.FieldByName('NGAY').AsDateTime;
        Parameters[3].Value := mavt;
        Parameters[4].Value := QrBH.FieldByName('MAVIP').AsString;
        Parameters[10].Value := mabo;
        Execute;
        Result := Parameters[0].Value = 0;
        if Result then
        begin
            curGiaban := Parameters[5].Value;
            curTlck7   := Parameters[6].Value;
            curTlck3  := Parameters[7].Value;
            curThsuat := Parameters[8].Value;
            curLThue  := Parameters[9].Value;
            curTlckBo := Parameters[11].Value;
            curSlBo   := Parameters[12].Value;

            if QrDMVT.Locate('MAVT', mavt, []) then
            begin
                if QrDMVT.FieldByName('UPDATE_DATE').Value <> Parameters[13].Value  then
                begin
                    exReSyncRecord(QrDMVT);
                    if mLastUpDate < QrDMVT.FieldByName('UPDATE_DATE').AsDateTime then
                        mLastUpDate := QrDMVT.FieldByName('UPDATE_DATE').AsDateTime;
                end;
            end else if curGiaban <> 0 then
                QrDMVT.Requery;
    	end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GetCustomerName;
var
	mavip: String;
begin
	mavip := QrBH.FieldByName('MAVIP').AsString;
    if mavip = '' then
        EdCus.Text := ''
    else
        with DataMain.QrTEMP do
        begin
            SQL.Text := 'select HO, TEN from DM_VIP where MAVIP=''' + mavip + '''';
            Open;
            EdCus.Text := Trim(FieldByName('HO').AsString) + ' ' +
                Trim(FieldByName('TEN').AsString);
            Close;
        end;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterPost(DataSet: TDataSet);
begin
	with QrBH do
		lastSct := FieldByName('SCT').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterInsert(DataSet: TDataSet);
begin
    curStt := 0;
	curKhoa := DataMain.GetNewGuid;

	with QrBH do
    begin
		TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
		FieldByName('NGAY').AsDateTime := Now;
		FieldByName('SCT').AsString := posSct;
		FieldByName('PTTT').AsString := mPTTT;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
		FieldByName('CREATE_BY').AsInteger := sysLogonUID;

        FieldByName('LCT').AsString := 'BLE';
		FieldByName('QUAY').AsString := posQuay;
		FieldByName('CA').AsString := '1';
		FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
		posSct := isSmartInc(posSct, 2);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.QrBHBeforePost(DataSet: TDataSet);
var
	mDate: TDateTime;
begin
	mDate := Now;
    if (Trunc(mDate) <> Trunc(mLastDate)) or (mLastDate > mDate) then
    begin
		ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
    	Abort;
    end;

    with DataSet do
    begin
    	// Kiểm tra tiền khách trả

    	if FieldByName('PRINTED').AsBoolean then
        begin
            if FieldByName('CHUATHOI').AsFloat < FieldByName('THANHTOAN').AsFloat then
            begin
                ErrMsg(RS_POS_INVALID_PAID, 'Thông báo',1);

                if (FieldByName('CHUATHOI').AsFloat = 0.0) then
				    FieldByName('TTOAN1').AsFloat :=
                        FieldByName('THANHTOAN').AsFloat;
                Abort;
            end;

            if ((FieldByName('PTTT').AsString = '02') or
                (FieldByName('PTTT').AsString = '03') or
                (FieldByName('PTTT2').AsString = '02') or
                (FieldByName('PTTT2').AsString = '03'))
                and (FieldByName('CHUATHOI').AsFloat <> FieldByName('THANHTOAN').AsFloat) then
            begin
               ErrMsg(RS_POS_INVALID_PAID, 'Thông báo',1);
               Abort;
            end
        end;

        if FieldByName('NGAY').IsNull then //
		    FieldByName('NGAY').AsDateTime := mDate;
		FieldByName('CREATE_DATE').AsDateTime := mDate;
        mLastDate := mDate;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrDBError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    // For TEST Hoang phuc Mail: 2012-08-02
    with DataMain.Conn.Errors[0] do
        if (NativeError = 32) and (Pos('CANNOT BE LOCATED', UpperCase(Description)) < 1) then
        begin
            ErrMsg(Description);
            Action := daAbort;
        end
        else
            Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN1Change(Sender: TField);
begin
    with QrBH do
    begin
        if not mTrigger then
        begin
            if mNhacnhoTT and (Sender.AsFloat <> 0) then
                if YesNo(RS_NHACNHO_THANHTOAN) then
                begin
                    mTrigger := True;
                    if Sender.FieldName = 'TTOAN1' then
                        FieldByName('TTOAN1').AsFloat := Sender.AsFloat * mNhanTT
                    else
                        FieldByName('TTOAN2').AsFloat := Sender.AsFloat * mNhanTT;
                    mTrigger := False;
                end;
        end;
        if Sender.FieldName = 'TTOAN2' then
            if Sender.AsFloat = 0 then
                FieldByName('SO_CHUANCHI').Clear;

    	FieldByName('CHUATHOI').AsFloat :=
	    	FieldByName('TTOAN1').AsFloat  + FieldByName('TTOAN2').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCT1CalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
  		FieldByName('CALC_SOTIEN_SAUCK').AsFloat :=
			FieldByName('SOTIEN').AsFloat -
			FieldByName('CHIETKHAU').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterDelete(DataSet: TDataSet);
begin
	Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	with QrCTBH do
    begin
    	Inc(curStt);
    	FieldByName('STT').AsInteger := curStt;
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
    end;

    // lay ngay gio de tinh chiet khau, khuyen mai.
    if Is1Record(QrCTBH) and (not sysIsCentral) then
    begin
        SetEditState(QrBH);
        QrBH.FieldByName('NGAY').AsDateTime := Now;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHBeforeOpen(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHCalcFields(DataSet: TDataSet);
var
    f: Double;
begin
	with DataSet do
    begin
    	FieldByName('CALC_TONGCK').AsFloat :=
    		FieldByName('CHIETKHAU').AsFloat +
    		FieldByName('CHIETKHAU_MH').AsFloat;

        if FieldByName('CHUATHOI').IsNull then
            FieldByName('CALC_TIENTHOI').AsFloat := 0
        else
        begin
            f := 0;
            if (FieldByName('PTTT').AsString = '04') and (FieldByName('TTOAN1').AsFloat > FieldByName('THANHTOAN').AsFloat) then
                f := FieldByName('TTOAN1').AsFloat - FieldByName('THANHTOAN').AsFloat;

            FieldByName('CALC_TIENTHOI').AsFloat :=
	    	    FieldByName('CHUATHOI').AsFloat - f - FieldByName('THANHTOAN').AsFloat;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHCN_DIACHI_HDChange(Sender: TField);
var
    s: String;
begin
    with QrBH do
    begin
        s := Sender.AsString;
        if s <> '' then
            if YesNo('Mặc định địa chỉ giao hàng?') then
                FieldByName('CN_DIACHI').AsString := s
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHMAKHOChange(Sender: TField);
begin
    with QrBH do
    begin
//        if FieldByName('MAKHO').AsString <> sysDefKho then
//            EdRetailer.Font.Color := clRed
//        else
//            EdRetailer.Font.Color := clPurple;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterPost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	Total;

    // Reread DMVT
    with QrCTBH do
    begin
        if FieldByName('TENVT').AsString = '' then
        begin
            QrDMVT.Requery;
            FieldByName('TENVT').RefreshLookupList;
            FieldByName('LK_TENVT_MARK').RefreshLookupList;
        end;

        icdComPort.IcdScan(FieldByName('LK_TENVT_MARK').AsString
            ,FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat)
            ,FormatFloat(sysCurFmt, QrBH.FieldByName('SOTIEN').AsFloat) + ' VND');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrBH do
    	if State in [dsInsert] then
    	begin
        	Post;
            Edit;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);

		FieldByName('CALC_SOTIEN_SAUCK').AsFloat :=
			FieldByName('SOTIEN').AsFloat -
			FieldByName('CHIETKHAU').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHMAVTChange(Sender: TField);
begin
    with QrCTBH do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CK2Change(Sender: TField);
begin
    with QrCTBH do
		FieldByName('TL_CK_MAX').AsFloat :=
                max(max(FieldByName('TL_CK4').AsFloat,
                    max(FieldByName('TL_CK2').AsFloat, FieldByName('TL_CK3').AsFloat)),
                    max(FieldByName('TL_CK5').AsFloat, Max(FieldByName('TL_CK6').AsFloat, FieldByName('TL_CK7').AsFloat))
                );
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CKChange(Sender: TField);
var
	sl, ck, dg, st, ts, thue: Double;
begin
	with QrCTBH do
    begin
    	dg  := FieldByName('DONGIA').AsFloat;
    	sl  := FieldByName('SOLUONG').AsFloat;
		//ck  := FieldByName('TL_CK').AsFloat;
        ts  := FieldByName('THUE_SUAT').AsFloat;

        st := exVNDRound(sl*dg);
        ck := exVNDRound( st * FieldByName('TL_CK').AsFloat / 100.0);
        if ts = 0 then
            Thue := 0
        else
            Thue := exVNDRound((st - ck) / (100/Ts + 1));

        FieldByName('SOTIEN').AsFloat := st;
    	FieldByName('CHIETKHAU').AsFloat := ck;
        FieldByName('TIEN_THUE').AsFloat := thue;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CK_MAXChange(Sender: TField);
begin
    with QrCTBH do
		FieldByName('TL_CK').AsFloat :=
                    FieldByName('TL_CK_MAX').AsFloat + FieldByName('TL_CK_THEM').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrTAMBeforeOpen(DataSet: TDataSet);
begin
	with QrTAM do
    begin
    	Parameters[0].Value := posQuay;
        Parameters[1].Value := posMakho;
    end;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeEnter(Sender: TObject);
begin
	EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeExit(Sender: TObject);
begin
	with EdCode do
	begin
    	if Text <> '' then
        	Exit;
		Tag := 0;
	end;
	ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_ACTION = 'Thao tác không hợp lệ.';
	RS_POS_CODE_FAIL  = 'Nhập sai mã hàng hóa.';
    RS_CONFIRM_PRICE  = 'Hàng có giá bán là %s, giá đã được giảm là %s. Giảm giá?';
    RS_CUT_PRICE_QTY  = 'Chỉ còn lại %f suất giảm giá.';
    RS_POS_NOT_PRICE  = 'Mặt hàng chưa có giá bán.';

procedure TFrmMain.InvalidAction;
begin
    Beep;
    if msg = '' then
		ErrMsg(RS_INVALID_ACTION)
    else
		ErrMsg(msg);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.IsChangeDMHH: Boolean;
begin
    Result := False;
    with GetUPDATE_DATE do
    begin
        Prepared := True;
        try
            with Execute do
            begin
                Result := mLastUpDate < Fields[0].Value;
                if Result then
                begin
                    mLastUpDate := Fields[0].Value;
                    CmdReLoad.Execute;
                end;
            end;
        except
            DbeMsg;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.IsEnableFunc(const pFunc: String): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := format('select FUNC_CODE from SYS_FUNC where isnull(ENABLED, 0)=1 and FUNC_CODE = %s',
            [Quotedstr(pFunc)]);
        Open;
        Result := not IsEmpty;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanCode;
var
	s: String;
    gio: TDateTime;
    bo: Boolean;
begin
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Quick select
    if IsDotSelect(s) <> 0 then
    begin
        IsChangeDMHH;

    	if not exDotMavt(4, QrDmvt, s) then
        	Exit;
		EdCode.Text := s;
	end;

    gio := Now;
    if (Trunc(gio) <> Trunc(mLastDate)) or (mLastDate > gio) then
    begin
        ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
        Exit;
    end;

    if not ValidBarcode(s, bo) then
        Abort;
    if bo then
    begin
        with QrDMVT_BO do
        begin
            Close;
            Parameters[0].Value := s;
            Open;
            while not eof do
            begin
                if GetSalingPrice(FieldByName('MABH').AsString, s) then
                begin
                    if QrCTBH.Locate('MAVT;MABO',
                        VarArrayOf([FieldByName('MABH').AsString, s]), []) then
                        if mDefSoluong > 0 then
                            ItemSetQty(QrCTBH.FieldByName('SOLUONG').AsFloat + mDefSoluong * curSlBo)
                        else
                            ItemSetQty(QrCTBH.FieldByName('SOLUONG').AsFloat + curSlBo)
                    else
                        ItemAddNew(FieldByName('MABH').AsString, s);
                end;
                Next;
            end;
        end;
    end
    else
    begin
        GoLast;
        with QrCTBH do
        begin
            // Mã cũ
            if Locate('MAVT;MABO', VarArrayOf([s, Null]), []) then
            begin
                GetSalingPrice(s);

                // Resync GIABAN
//				if FieldByName('DONGIA').AsFloat <> curGiaban then
//                begin
//                    QrRefDMVT.Locate('MAVT', s, []);
//                    ReSyncRecord(QrRefDMVT);
//                end;
                // End: resync

                if mDefSoluong > 0 then
                    ItemSetQty(FieldByName('SOLUONG').AsFloat + mDefSoluong)
                else
                    ItemSetQty(FieldByName('SOLUONG').AsFloat + 1)
            end
            // Mã mới
            else
            begin
                // Sai ma
                if not GetSalingPrice(s) then
                begin
                    InvalidAction(RS_POS_CODE_FAIL);
                    Exit;
                end;

                // Chua co gia ban
                if curGiaban = 0 then
                begin
                    Beep;
                    ErrMsg(RS_POS_NOT_PRICE);
                    Exit;
                end;

                ItemAddNew(s);
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanQty;
var
	b: Boolean;
    s: String;
begin
    s := Trim(EdCode.Text);
    b := Length(s) > 10;
    if not b then
        try
            mDefSoluong := StrToFloat(s);
        except
            mDefSoluong := 1;
            b := True;
        end;

    if b then
        ErrMsg(RS_POS_ERROR_QTY);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanVIP;
var
	ma, ten: String;
    ck: Double;
    bm: TBytes;
begin
	ma := Trim(EdCode.Text);

    // Xóa mã VIP
    if ma = '' then
    begin
	    SetEditState(QrBH);
    	with QrBH do
	    begin
    	    FieldByName('MAVIP').Clear;
	        FieldByName('TL_CK2').Clear;
            EdCus.Text := '';
            Total;
	    end;
        Exit;
    end;

    // Lấy thông tin VIP
    with POS_VIP do
    begin
    	Prepared := True;
        Parameters[1].Value := ma;
        Execute;

        if Parameters[0].Value = 0 then
        begin
            ma := Parameters[1].Value;
            ck := Parameters[3].Value;
            ten := Trim(Parameters[2].Value);
            if ten = '' then
            begin
                Application.CreateForm(TFrmPosVIP, FrmPosVIP);
            	ten := FrmPosVIP.Execute(ma, 0);
            end;
            if ten = '' then
	            Exit;
            EdCus.Text := ten;
        end
        else
        begin
            ErrMsg(RS_INVALID_VIP);
            Exit;
        end;
	end;

    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('MAVIP').AsString := ma;
        FieldByName('TL_CK2').AsFloat := ck;
    end;
    with QrCTBH do
    begin
        DisableControls;
        bm := Bookmark;
        First;
        while not eof do
        begin
            GetSalingPrice(FieldByName('MAVT').AsString);
            SetEditState(QrCTBH);
            FieldByName('TL_CK3').AsFloat := curTlck3;
            Next;
        end;
        Bookmark := bm;
        EnableControls;
    end;
    Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanRetQty;
var
	s: String;
    soluong: Double;
begin
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Lấy số lượng trả
    try
        soluong := StrToFloat(s);
    except
        soluong := -1;
    end;

	with QrCTBH do
    begin
        // Kiểm tra số lượng
		if (soluong <= 0) or (soluong > FieldByName('SOLUONG').AsFloat) then
        begin
            InvalidAction('Số lượng không hợp lệ.');
			EdCode.SetFocus;
            Exit;
        end;

		if soluong = FieldByName('SOLUONG').AsFloat then
        	Delete
        else
    	begin
	        Edit;
    	    FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG').AsFloat - soluong;
            Post;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeKeyPress(Sender: TObject; var Key: Char);
begin
	// Is CRLF keystroke
	if Key <> #13 then
    	Exit;

    Key := #0;

	case EdCode.Tag of
    // Mã hàng
    0:
    	ProcScanCode;

	// Số lượng
    1:
    	ProcScanQty;

	// VIP
    2:
    	ProcScanVIP;

	// Trả hàng
    3:
    	ProcScanRetQty;
    end;

    if EdCode.Tag <> 1 then
		mDefSoluong := 1;

    with EdCode do
    begin
    	Tag := 0;
	    Text := '';
		SelectAll;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.exThelenh: Boolean;
begin
    Application.CreateForm(TFrmTheLenh, FrmTheLenh);
    Result := FrmTheLenh.Execute(0, posMakho);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
  FooterCellRect: TRect; Field: TField; FooterText: String;
  var DefaultDrawing: Boolean);
begin
	with Canvas.Font do
    begin
    	Size := 10;
		Style := [fsBold];

        if (Field.FullName = 'MAVT') or (Field.FullName = 'TENVT') then
			Color := $00804000
        else
			Color := clPurple;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListUpdateFooter(Sender: TObject);
begin
	with GrList, QrBH do
    begin
    	ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
    	ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
    	ColumnByName('CALC_SOTIEN_SAUCK').FooterValue :=
        	FormatFloat(sysCurFmt,
            	FieldByName('SOTIEN').AsFloat -
            	FieldByName('CHIETKHAU_MH').AsFloat);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    begin
    	Key := #0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.ItemAddNew(const ma: String; const pMabo: String);
begin
    with QrCTBH do
    begin
	    Append;
        FieldByName('MAVT').AsString := ma;
        if pMabo <> '' then
        begin
            FieldByName('MABO').AsString := pMabo;
            FieldByName('TL_CK4').AsFloat := curTlckBo;
            FieldByName('SOLUONG').AsFloat := mDefSoluong * curSlBo;
        end
        else
            FieldByName('SOLUONG').AsFloat := mDefSoluong;

        FieldByName('DONGIA').AsFloat := curGiaban;

        FieldByName('LOAITHUE').AsString := curLThue;
        FieldByName('THUE_SUAT').AsFloat := curThsuat;

        FieldByName('TL_CK7').AsFloat := curTlck7;
        FieldByName('TL_CK3').AsFloat := curTlck3;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemSetQty(const pSoluong: Double);
begin
    with QrCTBH do
    begin
        if State in [dsBrowse] then
            Edit;
		FieldByName('DONGIA').AsFloat := curGiaban;

        FieldByName('LOAITHUE').AsString := curLThue;
        FieldByName('THUE_SUAT').AsFloat := curThsuat;

        FieldByName('TL_CK7').AsFloat := curTlck7;
        FieldByName('TL_CK3').AsFloat := curTlck3;

        // CK mặt hàng thuộc bộ!
        FieldByName('TL_CK4').AsFloat := curTlckBo;

		FieldByName('SOLUONG').AsFloat := pSoluong;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFaqExecute(Sender: TObject);
begin
	Faqs
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetPrinterExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPrinter, FrmPrinter);
    if FrmPrinter.Execute then
    begin
    	exBillFinal(mPrinter);
		exBillInitial(mPrinter);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTrahangExecute(Sender: TObject);
var
    r: WORD;
begin
    r := DataMain.GetRights('SZ_NHAPTRA_BL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptrabl, FrmNhaptrabl);
    FrmNhaptrabl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChitietExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPosChitiet, FrmPosChitiet);
    FrmPosChitiet.Execute(QrCTBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	Handled := (MyActionList.Tag = 0) and Self.Active;
//    if Handled then
//    	TestDbConnection; //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSave2Execute(Sender: TObject);
var
	b: Boolean;
    k: TGUID;
begin
	if QrCTBH.IsEmpty then		// Restore saved receipt
    begin
        with QrBH do //Truong hop xoa chi tiet phieu o luu tam
            if (State in [dsEdit]) and (FieldByName('THANHTOAN').OldValue <> FieldByName('THANHTOAN').Value) then
                CheckBrowseMode;

        // Exist temp. receipt?
        with QrTAM do
        begin
            Close;
            Open;
            b := IsEmpty;
            if RecordCount = 1 then
                k := TGuidField(FieldByName('KHOA')).AsGuid
            else
                k := TGuidEx.EmptyGuid;
        end;

        // Smart action
        if b then				// No temp.
            Exit
        else if TGuidEx.IsEmptyGuid(k) then		// More receipts
        begin
            Application.CreateForm(TFrmTempReceipt, FrmTempReceipt);
            curKhoa := FrmTempReceipt.Execute(posQuay);
        end
        else					// Only 1 saved receipt
        	curKhoa := k;
    end
    (*
    ** Temporarily save receipt
    *)
    else
    begin
	    // Temporarily save
    	if QrBH.FieldByName('DGIAI').AsString = '' then
        begin
            Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
	    	SaveBill(False, FrmReceiptDesc.Execute)
        end
        else
	    	SaveBill(False, QrBH.FieldByName('DGIAI').AsString);

        // New receipt
        curKhoa := TGuidEx.EmptyGuid;
    end;
    posSct := DataMain.AllocRetailBillNumber(posMakho, posQuay, Date);
	OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
	s: String;
begin
	if mDefSoluong > 1 then
		s := FloatToStr(mDefSoluong)
	else
		s := '';

    if LbCurSoluong.Caption <> s then
    begin
	    LbCurSoluong.Caption := s;
        if s = '' then
	        HtmlCurSoluong.HTMLText.Text := ''
        else
            HtmlCurSoluong.HTMLText.Text := Format(
                '<P align="left"><FONT face="Tahoma" size="13" color="#004080"><b>(SL)</b>  ' +
                '<FONT size="18"><b>%s</b></FONT></FONT></P>',
                [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.Cleanup;
begin
	with POS_CLEANUP do
    begin
    	Prepared := True;
        Parameters[1].Value := posMakho;
        Parameters[2].Value := posQuay;
        Execute;
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDoanhsoExecute(Sender: TObject);
begin
	with QrBH do
    	if FieldByName('MAVIP').AsString <> '' then
        begin
            Application.CreateForm(TFrmPosVIP, FrmPosVIP);
			EdCus.Text := FrmPosVIP.Execute(FieldByName('MAVIP').AsString, 1);

            FrmPosVIP.Free;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ScanType;
begin
	case EdCode.Tag of
    0:
        begin
            LbCode.Caption := 'NHẬP MÃ HÀNG';
            LbCode.Font.Color := RGB(0, 64, 128);
            EdCode.Font.Color := clGray;
        end;
    1:
        begin
            LbCode.Caption := 'NHẬP SỐ LƯỢNG';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    2:
        begin
            LbCode.Caption := 'NHẬP THẺ VIP';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    3:
        begin
            LbCode.Caption := 'NHẬP SỐ LƯỢNG TRẢ';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    end;
end;

initialization
end.
