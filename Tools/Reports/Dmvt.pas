﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmvt;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons;

type
  TFrmDmvt = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTTL_LAI: TFloatField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTGIANHAPVAT: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTDVT1: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTQUAYKE: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTSTAMP: TIntegerField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    QrDM_QUAYKE: TADOQuery;
    ItmNCC: TMenuItem;
    ItmNHOM: TMenuItem;
    N1: TMenuItem;
    CmdPhoto: TAction;
    N2: TMenuItem;
    Xemhnh1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N4: TMenuItem;
    QrDMVTGIASI: TFloatField;
    QrDMVTTON_MIN: TFloatField;
    PopIn: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    CmdChangeGroup: TAction;
    N5: TMenuItem;
    ingnhnhm1: TMenuItem;
    Xl1: TMenuItem;
    QrDMVTDAT_HANG: TFloatField;
    QrDMVTMAMAU: TWideStringField;
    QrDMVTMASIZE: TWideStringField;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    QrDMVTPLU: TWideStringField;
    QrDMVTPLU_KEY: TIntegerField;
    ALLOC_PLU: TADOCommand;
    QrDMVTIS_PLU: TBooleanField;
    CmdExport: TAction;
    BtExport: TToolButton;
    ToolButton12: TToolButton;
    DMVT_EXPORT: TADOStoredProc;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD3: TisPanel;
    LbTLLAI1: TLabel;
    LbGIALE: TLabel;
    EdTL_LAI: TwwDBEdit;
    EdGIABANLE: TwwDBEdit;
    PD1: TisPanel;
    Label14: TLabel;
    pluLb1: TLabel;
    pluLb2: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label12: TLabel;
    LbDVT1: TLabel;
    LbDVT2: TLabel;
    EdMA: TwwDBEdit;
    EdPLU: TwwDBEdit;
    EdPLUHotkey: TwwDBEdit;
    EdTen: TwwDBEdit;
    DBEdit11: TwwDBEdit;
    CbQD1: TwwDBEdit;
    CbDVT: TwwDBLookupCombo;
    CbDVT1: TwwDBLookupCombo;
    PD2: TisPanel;
    Label18: TLabel;
    Label28: TLabel;
    EdVAT_VAO: TwwDBEdit;
    DBEdit17: TwwDBEdit;
    PD5: TisPanel;
    EdGHICHU: TDBMemo;
    PD6: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    Label10: TLabel;
    wwDBLookupCombo6: TwwDBLookupCombo;
    Label3: TLabel;
    wwDBLookupCombo3: TwwDBLookupCombo;
    DBEdit8: TwwDBEdit;
    Label21: TLabel;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    QrDMVTMANHOM2: TWideStringField;
    QrDMVTMANHOM3: TWideStringField;
    QrDMVTMANHOM4: TWideStringField;
    Label26: TLabel;
    wwDBLookupCombo4: TwwDBLookupCombo;
    wwDBLookupCombo5: TwwDBLookupCombo;
    CmdSwitch: TAction;
    QrDMVTBO: TBooleanField;
    QrDMVTLOAITHUE: TWideStringField;
    TntLabel6: TLabel;
    CbLoaithue: TwwDBLookupCombo;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    TntLabel7: TLabel;
    TntLabel8: TLabel;
    LbTLLAI2: TLabel;
    CmdPLU: TAction;
    QrDMVTIS_PLU2: TBooleanField;
    CmdAudit: TAction;
    CmdTimBarcode: TAction;
    N3: TMenuItem;
    mtheobarcode1: TMenuItem;
    lbDVT3: TLabel;
    EdDVT1: TwwDBEdit;
    EdMinBox: TwwDBEdit;
    QrDMVTCAL_TONMIN_BOX: TFloatField;
    DMVT_EXPORT2: TADOStoredProc;
    Label1: TLabel;
    lbDVT4: TLabel;
    wwDBEdit1: TwwDBEdit;
    EdDVT2: TwwDBEdit;
    EdMaxBox: TwwDBEdit;
    QrDMVTCAL_TONMAX_BOX: TFloatField;
    QrDMVTTON_MAX: TFloatField;
    QrDMVTNAMXB: TIntegerField;
    QrDMVTTRONGLUONG: TFloatField;
    QrDMVTSOTRANG: TIntegerField;
    QrDMVTDAI: TFloatField;
    QrDMVTRONG: TFloatField;
    QrDMVTMATG: TWideStringField;
    QrDMVTMANXB: TWideStringField;
    QrDMVTLAN_TAIBAN: TIntegerField;
    PaSach: TisPanel;
    Label24: TLabel;
    Label30: TLabel;
    wwDBLookupCombo9: TwwDBLookupCombo;
    wwDBLookupCombo10: TwwDBLookupCombo;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    QrDMVTTL_LAI_SI: TFloatField;
    QrDMVTCAO: TFloatField;
    QrDMVTKHAC_NCC: TBooleanField;
    ChbKHAC_NCC: TDBCheckBox;
    CmdImport: TAction;
    ToolButton9: TToolButton;
    ToolButton13: TToolButton;
    CmdExport2: TAction;
    RzLauncher: TRzLauncher;
    PopEx: TAdvPopupMenu;
    Export1: TMenuItem;
    N7: TMenuItem;
    Xutdliuchothitbkimkho1: TMenuItem;
    CmdTimBarcode2: TAction;
    CmdDmdvt: TAction;
    SpeedButton1: TSpeedButton;
    CmdDmvtCt: TAction;
    ToolButton8: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    Label2: TLabel;
    CbMADT: TwwDBEdit;
    EdTENDT: TwwDBEdit;
    QrDMVTMADT: TWideStringField;
    QrDMVTLK_TENDT: TWideStringField;
    N6: TMenuItem;
    PopTinhtrang1: TMenuItem;
    QrDMVTGIASIVAT: TFloatField;
    QrDMVTTL_CK_NCC: TFloatField;
    PD7: TisPanel;
    LbGianhap: TLabel;
    LbGianhapVat: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    wwDBEdit10: TwwDBEdit;
    EdGianhapVat: TwwDBEdit;
    wwDBEdit16: TwwDBEdit;
    PD8: TisPanel;
    LbGiasi: TLabel;
    LbGiasiVat: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    wwDBEdit17: TwwDBEdit;
    EdGiaSiVat: TwwDBEdit;
    wwDBEdit22: TwwDBEdit;
    PaThuoctinh: TisPanel;
    Label5: TLabel;
    Label11: TLabel;
    wwDBEdit9: TwwDBEdit;
    wwDBEdit13: TwwDBEdit;
    wwDBEdit14: TwwDBEdit;
    wwDBEdit19: TwwDBEdit;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTNL: TBooleanField;
    QrDMVTVL: TBooleanField;
    QrDMVTCB: TBooleanField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    wwDBEdit2: TwwDBEdit;
    Label4: TLabel;
    wwDBEdit3: TwwDBEdit;
    wwDBEdit4: TwwDBEdit;
    Label7: TLabel;
    wwDBEdit5: TwwDBEdit;
    Label9: TLabel;
    wwDBEdit6: TwwDBEdit;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang: TMenuItem;
    PopTinhtrang2: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure EdTL_LAIExit(Sender: TObject);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure ItmNCCClick(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CmdPLUExecute(Sender: TObject);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdTimBarcodeExecute(Sender: TObject);
    procedure QrDMVTCalcFields(DataSet: TDataSet);
    procedure QrDMVTTON_MINChange(Sender: TField);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdExport2Execute(Sender: TObject);
    procedure CmdTimBarcode2Execute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure CmdDmvtCtExecute(Sender: TObject);
    procedure QrDMVTMADTChange(Sender: TField);
    procedure PopTinhtrang1Click(Sender: TObject);
    procedure PopMainPopup(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDMVTTL_LAIValidate(Sender: TField);
    procedure QrDMVTDVTChange(Sender: TField);
  private
  	mCanEdit, mEANConfirm, mPLU: Boolean;
    mGia: Double;
    mObsolete : Integer;
    mRet : Boolean;

    sCodeLen, defDvt, defTinhtrang, defThue: String;
  	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4, mPrefix: String;

    function  AllocPLU(nhom: String): String;
  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmDmvt: TFrmDmvt;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, Scan, Variants,
    CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
  TimBarcode2, Dmkhac, DmvtCT;

{$R *.DFM}

const
	FORM_CODE = 'DM_HH';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmDmvt.Execute;
begin
	mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    if closeDs then
        CloseDataSets(DataMain.Conn)
    else
    begin
        CloseDataSets([QrDM_QUAYKE]);
        with DataMain do
        CloseDataSets([QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG]);
    end;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvt.AllocPLU;
begin
    with ALLOC_PLU do
    begin
    	Prepared := True;
        Parameters[1].Value := nhom;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormCreate(Sender: TObject);
var
	b, bLe, bSi: Boolean;
    x: Integer;
begin
    TMyForm(Self).Init2;

    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
    	SetDisplayFormat(QrDMVT, ['TON_MIN', 'TON_MAX', 'DAT_HANG'], sysQtyFmt);
	    SetDisplayFormat(QrDMVT, ['TL_LAI', 'TL_LAI_SI', 'TL_CK_NCC'], sysPerFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
    	SetShortDateFormat(QrDMVT);
    end;

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Don vi tinh quy doi
	b := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;
    LbDVT1.Visible := b;
    CbQD1.Visible := b;
    LbDVT2.Visible := b;
    CbDVT1.Visible := b;

    lbDVT3.Visible := b;
    lbDVT4.Visible := b;
    EdMinBox.Visible := b;
    EdMaxBox.Visible := b;
    EdDVT1.Visible := b;
    EdDVT2.Visible := b;

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE, 'EAN Confirm');
	sCodeLen := FlexConfigString(FORM_CODE, 'Code Length');
    mPLU := FlexConfigBool(FORM_CODE, 'PLU');

    ChbKHAC_NCC.Visible := FlexConfigBool(FORM_CODE, 'Khong phan biet NCC');
    PaSach.Visible := FlexConfigBool(FORM_CODE, 'Thong tin sach');

    PD8.Visible := FlexConfigBool(FORM_CODE, 'Gia si');
    PD3.Visible := FlexConfigBool(FORM_CODE, 'Gia le');

    PaThuoctinh.Visible := FlexConfigBool(FORM_CODE,'Thuoc tinh');

    CmdDmvtCt.Visible := FlexConfigBool(FORM_CODE,'Chi tiet');
    ToolButton12.Visible := CmdDmvtCt.Visible;

    // Default unit
	defDvt := DataMain.GetSysParam('DEFAULT_DVT');
    defThue := DataMain.GetSysParam('DEFAULT_LOAITHUE');

    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    // PLU
    pluLb1.Visible := mPLU;
    pluLb2.Visible := mPLU;
    EdPLU.Visible  := mPLU;
    EdPLUHotkey.Visible := mPLU;
    CmdPLU.Visible := mPLU;

    mObsolete     := 0;

    // Tree Combo
    FlexGroupCombo(CbNhomhang);

    // Thue
    if not sysIsThue then
    begin
        PD2.Visible := False;

        LbGianhap.Caption := 'Giá nhập';
        LbGianhapVat.Visible := False;
        EdGianhapVat.Visible := False;

        LbGiasi.Caption := 'Giá bán sỉ';
        LbGiasiVat.Visible := False;
        EdGiaSiVat.Visible := False;

        LbGIALE.Caption := 'Giá bán lẻ';
    end;

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PaSach.Collapsed := RegReadBool('PaSach');
    PaThuoctinh.Collapsed := RegReadBool('PaThuoctinh');
    PD3.Collapsed := RegReadBool('PD3');
    PD5.Collapsed := RegReadBool('PD5');
    PD6.Collapsed := RegReadBool('PD6');
    PD7.Collapsed := RegReadBool('PD7');
    PD8.Collapsed := RegReadBool('PD8');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormShow(Sender: TObject);
begin
//	DsDMVT.AutoEdit := mCanEdit;
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    QrDM_QUAYKE.Open;
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG,
            QrDMTACGIA, QrDMNXB, QrDMLOAITHUE]);
        defTinhtrang := QrDMVT_TINHTRANG.FieldByName('MA').AsString;
    end;

    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('TINHTRANG').AsString <> '01' then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    RegWrite(Name, ['PD2', 'PD3', 'PD5', 'PD6', 'PD7', 'PD8', 'PaSach', 'PaThuoctinh'],
    	[PD2.Collapsed, PD3.Collapsed, PD5.Collapsed, PD6.Collapsed,
            PD7.Collapsed, PD8.Collapsed, PaSach.Collapsed, PaThuoctinh.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdNewExecute(Sender: TObject);
begin
	QrDMVT.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSaveExecute(Sender: TObject);
begin
    QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDelExecute(Sender: TObject);
begin
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := DataMain.GetRights('SZ_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhac, FrmDmkhac);
    FrmDmkhac.Execute(r, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDmvtCtExecute(Sender: TObject);
begin
    with QrDMVT do
    begin
        Application.CreateForm(TFrmDmvtCT, FrmDmvtCT);
        FrmDmvtCT.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString,
            FieldByName('MANHOM').AsString, mPrefix);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ItmNCCClick(Sender: TObject);
var
    n: Integer;
    REP_NAME: String;
begin
    n := (Sender as TComponent).Tag;
    if n = 1 then
        REP_NAME := FORM_CODE + '_DT'
    else
        REP_NAME := FORM_CODE + '_NN';

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPrintExecute(Sender: TObject);
begin
   BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, b: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
	end;

    CmdNew.Enabled := bBrowse and mCanEdit and (mNhom <> '') and sysIsCentral;
    CmdEdit.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;

    if FrmScan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec;
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and (not FrmScan.Visible);
        CmdFilter.Enabled := not FrmScan.Visible;
        CmdSearch.Enabled := not FrmScan.Visible;
    end;

    CmdSearch.Enabled := CmdSearch.Enabled and bBrowse;
    CmdFilter.Enabled := CmdFilter.Enabled and bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdChangeGroup.Enabled := mCanEdit and bBrowse and bHasRec;

    CmdDmvtCt.Enabled := bBrowse;

	    (*
    	** PLU
	    *)
    if mPLU then
    	with QrDMVT do
    		if Active then
		    begin
        		b := FieldByName('IS_PLU').AsBoolean or
		        	FieldByName('IS_PLU2').AsBoolean;

		        EdPLU.ReadOnly := not b;
		        EdPLUHotkey.ReadOnly := not b;

		        EdPLU.TabStop := b;
		        EdPLUHotkey.TabStop := b;
                CmdPLU.Enabled := b;
		    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.PopMainPopup(Sender: TObject);
begin
    PopTinhtrang.Checked := mObsolete = 0;
    PopTinhtrang1.Checked := mObsolete = 1;
    PopTinhtrang2.Checked := mObsolete = 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.PopTinhtrang1Click(Sender: TObject);
begin
    mObsolete :=  (Sender as TComponent).Tag;
    mNganh := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mNhom2 := sNhom2;
        mNhom3 := sNhom3;
        mNhom4 := sNhom4;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''', [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            case mObsolete of
            0:
                sSql := sSql + ' and TINHTRANG in (''01'')';
            1:
                sSql := sSql + ' and TINHTRANG in (''02'')';
            end;

            SQL.Text := sSQL;
            Open;
            First;
        end;

//        with Filter do
//        begin
//            RefreshOriginalSQL; //NTD
//            ApplyFilter;
//        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
		FieldByName('MANHOM2').AsString := mNhom2;
		FieldByName('MANHOM3').AsString := mNhom3;
		FieldByName('MANHOM4').AsString := mNhom4;
        FieldByName('LOAITHUE').AsString := defThue;
//        FieldByName('GIABAN').AsFloat := mGia;
        FieldByName('DVT').AsString := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
		FieldByName('BO').AsBoolean := False;
        FieldByName('KHAC_NCC').AsBoolean := False;
        FieldByName('NL').AsBoolean := False;
        FieldByName('VL').AsBoolean := False;
        FieldByName('CB').AsBoolean := False;
		FieldByName('MAVT').AsString := DataMain.AllocEAN(mPrefix, mNhom);

        if FieldByName('IS_PLU').AsBoolean or
        	FieldByName('IS_PLU2').AsBoolean then
			FieldByName('PLU').AsString := AllocPLU(mNhom);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';

procedure TFrmDmvt.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['TENVT', 'MADT', 'LOAITHUE']) then
			Abort;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MAVT').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;

	    SetNull(DataSet, ['MAMAU' , 'MASIZE', 'MANHOM2', 'MANHOM3', 'MANHOM4']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTGIANHAPChange(Sender: TField);
var
	mGianhap, mLai, mGiaban, mGiasi, mGiasivat, mLaisi, mGianhapvat: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDmvt do
    begin
		mGianhap := FieldByName('GIANHAP').AsFloat;
        mGianhapvat := FieldByName('GIANHAPVAT').AsFloat;
        mLai := FieldByName('TL_LAI').AsFloat;
        mLaisi := FieldByName('TL_LAI_SI').AsFloat;
        mGiaban := FieldByName('GIABAN').AsFloat;
        mGiasi := FieldByName('GIASI').AsFloat;
        mGiasivat := FieldByName('GIASIVAT').AsFloat;
        mLoaithue := FieldByName('LOAITHUE').AsString;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := mLoaithue;
        Parameters[2].Value := Sender.FieldName;
    	Parameters[3].Value := mGianhap;
        Parameters[4].Value := mGianhapvat;
    	Parameters[5].Value := mLaisi;
    	Parameters[6].Value := mGiasi;
    	Parameters[7].Value := mGiasivat;
    	Parameters[8].Value := mLai;
        Parameters[9].Value := mGiaban;
        Execute;
    	mGianhap := Parameters[3].Value;
        mGianhapvat := Parameters[4].Value;
    	mLaisi := Parameters[5].Value;
        mGiasi := Parameters[6].Value;
    	mGiasivat := Parameters[7].Value;
    	mLai := Parameters[8].Value;
        mGiaban := Parameters[9].Value;
    end;

    mTrigger := True;
	with QrDmvt do
    begin
        FieldByName('GIANHAP').AsFloat := mGianhap;
        FieldByName('GIANHAPVAT').AsFloat := mGianhapvat;
        FieldByName('TL_LAI').AsFloat := mLai;
        FieldByName('TL_LAI_SI').AsFloat := mLaisi;
        FieldByName('GIABAN').AsFloat := mGiaban;
        FieldByName('GIASI').AsFloat  := mGiasi;
        FieldByName('GIASIVAT').AsFloat  := mGiasivat;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPhotoExecute(Sender: TObject);
begin
 	Application.CreateForm(TFrmScan, FrmScan);
	FrmScan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmDmvt.QrDMVTTENVTChange(Sender: TField);
var
	b: Boolean;
begin
	with QrDMVT do
    begin
		b := FieldByName('TENTAT').AsString = '';
    	if not b then
        	b := YesNo(RS_NAME_DEF);
		if b then
			FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);
	end;
end;

procedure TFrmDmvt.QrDMVTTL_LAIValidate(Sender: TField);
begin
    if sysTinhlai = 1 then
        begin
            if (Sender.AsFloat >= 100) then
            begin
                ErrMsg(RS_INVALID_INTEREST);
                Abort;
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTTON_MINChange(Sender: TField);
begin
	with QrDMVT do
    begin
      	if FieldByName('TON_MAX').AsFloat < FieldByName('TON_MIN').AsFloat then
            FieldByName('TON_MAX').AsFloat := FieldByName('TON_MIN').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if FieldByName('LOAITHUE').AsString = 'TTT' then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTMADTChange(Sender: TField);
begin
    exDotMadt(DataMain.QrDMNCC, Sender);
    EdTENDT.Text := EdTENDT.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmDmvt.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTAfterPost(DataSet: TDataSet);
begin
//	with QrDMVT do
//		mGia := FieldByName('GIABAN').AsFloat;
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';

procedure TFrmDmvt.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom, nhom2, nhom3, nhom4: String;
begin
	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
        nhom2 := FieldByName('MANHOM2').AsString;
        nhom3 := FieldByName('MANHOM3').AsString;
        nhom4 := FieldByName('MANHOM4').AsString;
    end;
	Application.CreateForm(TFrmCayNganhNhom, FrmCayNganhNhom);
    b := FrmCayNganhNhom.Execute(nganh, nhom, nhom2, nhom3, nhom4);
    FrmCayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;
            FieldByName('MANHOM2').AsString  := nhom2;
            FieldByName('MANHOM3').AsString  := nhom3;
            FieldByName('MANHOM4').AsString  := nhom4;
            FieldByName('PLU').AsString := AllocPLU(nhom);

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdExport2Execute(Sender: TObject);
var
	s, sNganh, sNhom: String;
    n, fLevel: Integer;
begin
    with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
        end;

    // Export to file
    s := sysTempPath + 'download.txt';

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	Prepared := True;
    	Open;
        TextExport(DMVT_EXPORT, s);
        Close;
    end;

	Cursor := crDefault;
	if not YesNo('Thiết bị phải đang ở tình trạng nhận danh mục. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'Softz.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Dn.exe';
        // [filename],[COM port],[Baud rate],[Interface],[Show dialog]
        Parameters := s + ',' + IntToStr(n) + ',1,2,0';
        Launch;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdExportExecute(Sender: TObject);
var
	s: String;
    sNganh, sNhom, sNhom2, sNhom3, sNhom4: String;
    fLevel: Integer;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
        with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT2 do
    begin
        Parameters[1].Value := 0;  // Bo = 0
        Parameters[2].Value := sNganh;
        Parameters[3].Value := sNhom;
        Parameters[4].Value := sNhom2;
        Parameters[5].Value := sNhom3;
        Parameters[6].Value := sNhom4;
        Parameters[7].Value := fLevel;

    	ExecProc;
        Active := True;
//        TextExport(DMVT_EXPORT2, s, TEncoding.UTF8);
        TextExport(DMVT_EXPORT2, s);
        Active := False
    end;
	Cursor := crDefault;

    MsgDone();

        {
	s := vlGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	ExecProc;
        Active := True;
        TextExport(DMVT_EXPORT, s);
        Active := False
    end;
	Cursor := crDefault;
    }
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.EdTL_LAIExit(Sender: TObject);
begin
//    if QrDMVT.State in [dsBrowse] then
//    else
//        QrDMVTGIANHAPChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPLUExecute(Sender: TObject);
begin
	with QrDMVT do
	begin
		if FieldByName('IS_PLU').AsBoolean or FieldByName('IS_PLU2').AsBoolean then
        else	// Khong phai PLU
        	Exit;

    	if IsEmpty or Eof or (State in [dsInsert]) then	// Safety
        	Exit;

		if State in [dsBrowse] then	// Force edit
        	Edit;

        // Alloc
		FieldByName('PLU').AsString := AllocPLU(FieldByName('MANHOM').AsString);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdImportExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
begin
	sFile := isGetOpenFileName('XLS');
    if sFile = '' then
        Exit;

    b := not YesNo('Không cập nhật mặt hàng bị trùng mã. Tiếp tục?', 0);
    DataExcel.ExcelImport('IMP_' + FORM_CODE, sFile, 'IMP_DM_HH',
        'PROCESS_IMPORT_DM_HH;1', 'MAVT', [sysLogonUID, mPrefix, b], 0);
    CmdReRead.Execute;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdTimBarcode2Execute(Sender: TObject);
begin
    Application.CreateForm(TFrmTimBarcode2, FrmTimBarcode2);
    FrmTimBarcode2.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdTimBarcodeExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmTimBarcode, FrmTimBarcode);
    FrmTimBarcode.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTCalcFields(DataSet: TDataSet);
var
	Qd: Double;
begin
	with QrDMVT do
    begin
    	Qd := FieldByName('QD1').AsFloat;

    	if Qd <> 0 then
        begin
    		FieldByName('CAL_TONMIN_BOX').AsFloat :=
        		RoundUp(FieldByName('TON_MIN').AsFloat / Qd);

    		FieldByName('CAL_TONMAX_BOX').AsFloat :=
        		RoundUp(FieldByName('TON_MAX').AsFloat / Qd);
        end
        else
        begin
        	FieldByName('CAL_TONMIN_BOX').Clear;
            FieldByName('CAL_TONMAX_BOX').Clear
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTDVTChange(Sender: TField);
begin
    with QrDMVT do
    begin
        FieldByName('DVT1').AsString := FieldByName('DVT').AsString;
    end;
end;

end.