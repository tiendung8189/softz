﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Payment;

interface

uses
  Classes, Controls, Forms,
  DB, ADODB, wwdblook,
  ExtCtrls, wwdbedit, ActnList, HTMLabel, RzBckgnd,
  RzPanel, StdCtrls, RzButton, Mask, pngimage, dxGDIPlusClasses;

type
  TFrmPayment = class(TForm)
    PaPay: TRzGroupBox;
    RzSeparator4: TRzSeparator;
    Image2: TImage;
    HTMLabel9: THTMLabel;
    HTMLabel7: THTMLabel;
    EdChange: TwwDBEdit;
    EdSotien1: TwwDBEdit;
    wwDBEdit23: TwwDBEdit;
    EdSotien2: TwwDBEdit;
    ActionList1: TActionList;
    CmdReturn: TAction;
    HTMLabel2: THTMLabel;
    wwDBEdit2: TwwDBEdit;
    BtReturn: TRzBitBtn;
    BtClose: TRzBitBtn;
    CmdClose: TAction;
    HTMLabel3: THTMLabel;
    HTMLabel4: THTMLabel;
    EdChuanchi: TwwDBEdit;
    HTMLabel1: THTMLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdReturnExecute(Sender: TObject);
    procedure EdSotien2Exit(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure EdSotien1Enter(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
  private
  public
  	function  Execute(pLoai: Integer): Boolean;
    function  NhapCongno(pPTTT: String = '02'): Boolean;
  end;

var
  FrmPayment: TFrmPayment;

implementation

uses
	isLib, PosMain, isMsg, exResStr, isDb, ExCommon, ThongtinCN;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.EdSotien1Enter(Sender: TObject);
begin
    EdSotien1.SelectAll;
end;

procedure TFrmPayment.EdSotien2Exit(Sender: TObject);
begin
    with FrmMain.QrBH do
    begin
//        if FieldByName('CHUATHOI').AsFloat < FieldByName('THANHTOAN').AsFloat then
//            ErrMsg(RS_POS_INVALID_PAID, 'Thông báo', 1);
//        if FrmMain.dThanhtoan then
//            if (FieldByName('CHUATHOI').AsFloat = 0.0) then
//            begin
//                SetEditState(FrmMain.QrBH);
//                FieldByName('TTOAN1').AsFloat := FieldByName('THANHTOAN').AsFloat;
//            end;
    end;
end;

(*==============================================================================
** 0: Save!
** 1: Save an Print!
**------------------------------------------------------------------------------
*)
function TFrmPayment.Execute(pLoai: Integer): Boolean;
begin
    case pLoai of
    0:
    begin
        CmdReturn.ShortCut := 16467;
        BtReturn.Caption := 'Lưu'#13'(F7)';
        BtReturn.Hint := 'Lưu bill nhưng không in';
    end;
    1:
    begin
        CmdReturn.ShortCut := 115;
        BtReturn.Caption := 'In'#13'(F4)';
        BtReturn.Hint := 'Lưu và in bill';
    end;
    end;
	Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.FormShow(Sender: TObject);
begin
    if FrmMain.dThanhtoan then
        with FrmMain.QrBH do
        begin
            SetEditState(FrmMain.QrBH);
            mTrigger := True;
            FieldByName('TTOAN1').AsFloat := FieldByName('THANHTOAN').AsFloat;
            mTrigger := False;
        end;

    EdSotien1.SetFocus;
    EdSotien1.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPayment.NhapCongno(pPTTT: String): Boolean;
begin
    Application.CreateForm(TFrmThongtinCN, FrmThongtinCN);
    FrmThongtinCN.Execute(pPTTT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
    bTt2: Boolean;
begin
    with FrmMain.QrBH do
    begin
        if not Active then
            Exit;
        bTt2 := FieldByName('TTOAN2').AsFloat <> 0;

        EdChuanchi.ReadOnly := not bTt2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdReturnExecute(Sender: TObject);
begin
    BtReturn.SetFocus;
    with FrmMain.QrBH do
    begin
        if (FieldByName('TTOAN2').AsFloat > FieldByName('THANHTOAN').AsFloat) or
           (FieldByName('CHUATHOI').AsFloat < FieldByName('THANHTOAN').AsFloat)  then
        begin
            ErrMsg('Số tiền không hợp lệ.', 'Thanh toán',1);
            if FieldByName('TTOAN1').AsFloat <> 0 then
                EdSotien1.SetFocus
            else
                EdSotien2.SetFocus;
            Exit;
        end;

        if FieldByName('TTOAN2').AsFloat <> 0 then
            if BlankConfirm(FrmMain.QrBH, ['SO_CHUANCHI'], EdChuanchi) then
                Exit;
    end;

	if not FrmMain.SaveBill(True) then
    	Exit;
    ModalResult := mrOk;
end;

end.
