object FrmChonDondh: TFrmChonDondh
  Left = 269
  Top = 191
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n '#208#417'n '#208#7863't H'#224'ng'
  ClientHeight = 386
  ClientWidth = 1070
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 1070
    Height = 77
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbNHAPCUA: TLabel
      Left = 114
      Top = 17
      Width = 77
      Height = 16
      Alignment = taRightJustify
      Caption = 'Nh'#224' cung c'#7845'p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label31: TLabel
      Left = 124
      Top = 45
      Width = 67
      Height = 16
      Alignment = taRightJustify
      Caption = 'Giao t'#7841'i kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbNCC: TwwDBLookupCombo
      Tag = 1
      Left = 324
      Top = 12
      Width = 285
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENDT'#9'40'#9'T'#234'n'#9#9
        'MADT'#9'12'#9'M'#227#9#9)
      LookupTable = QrDMNCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
ButtonEffects.Transparent=True
    end
    object CbKHO: TwwDBLookupCombo
      Tag = 3
      Left = 260
      Top = 40
      Width = 349
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENKHO'#9'40'#9'T'#234'n'#9'F'
        'MAKHO'#9'6'#9'M'#227#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
ButtonEffects.Transparent=True
    end
    object CbMADT: TwwDBLookupCombo
      Left = 200
      Top = 12
      Width = 121
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MADT'#9'12'#9'M'#227#9'F'
        'TENDT'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDMNCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
      OnNotInList = CbMADTNotInList
ButtonEffects.Transparent=True
    end
    object CbMAKHO: TwwDBLookupCombo
      Tag = 2
      Left = 200
      Top = 40
      Width = 57
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'6'#9'M'#227#9'F'
        'TENKHO'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
      OnNotInList = CbMADTNotInList
ButtonEffects.Transparent=True
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 77
    Width = 1070
    Height = 288
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'18'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7913'ng t'#7915
      'MADT'#9'13'#9'M'#227#9'F'#9'Nh'#224' cung c'#7845'p'
      'TENDT'#9'23'#9'T'#234'n'#9'F'#9'Nh'#224' cung c'#7845'p'
      'MAKHO'#9'6'#9'M'#227#9'F'#9'Kho'
      'TENKHO'#9'23'#9'T'#234'n'#9'F'#9'Kho'
      'SOLUONG'#9'7'#9'S'#7889' l'#432#7907'ng'#9'F'
      'THANHTOAN'#9'12'#9'Thanh to'#225'n'#9'F'
      'NG_DATHANG'#9'30'#9'Ng'#432#7901'i '#273#7863't h'#224'ng'#9'F'
      'NGAY_GIAO'#9'12'#9'Ng'#224'y giao'#9'F'
      'DGIAI'#9'30'#9'Di'#7877'n gi'#7843'i'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDONDH
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object Status: TStatusBar
    Left = 0
    Top = 365
    Width = 1070
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object DsDONDH: TDataSource
    DataSet = QrDONDH
    Left = 172
    Top = 196
  end
  object QrDONDH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.*, b.TENKHO, c.TENDT'
      '  from'#9'DONDH a left join DM_KHO b on a.MAKHO = b.MAKHO '
      #9'left join DM_NCC c  on a.MADT = c.MADT '
      'where'#9'a.LCT = '#39'DHN'#39
      '   and '#9'a.TINHTRANG in ('#39'02'#39')')
    Left = 144
    Top = 196
    object QrDONDHSCT: TWideStringField
      DisplayLabel = 'S'#232' phi'#213'u'
      FieldName = 'SCT'
    end
    object QrDONDHNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
    end
    object QrDONDHMADT: TWideStringField
      DisplayLabel = 'M'#227' '#272'V'
      FieldName = 'MADT'
      Size = 15
    end
    object QrDONDHTENDT: TWideStringField
      DisplayLabel = 'T'#234'n '#272'V'
      FieldName = 'TENDT'
      Size = 100
    end
    object QrDONDHTENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldName = 'TENKHO'
      Size = 100
    end
    object QrDONDHMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDONDHNG_DATHANG: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i '#273#7863't h'#224'ng'
      FieldName = 'NG_DATHANG'
      Size = 200
    end
    object QrDONDHTHANHTOAN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225' thanh to'#225'n'
      FieldName = 'THANHTOAN'
    end
    object QrDONDHDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrDONDHNGAY_GIAO: TDateTimeField
      FieldName = 'NGAY_GIAO'
    end
    object QrDONDHCHIETKHAU_HD: TFloatField
      FieldName = 'CHIETKHAU_HD'
    end
    object QrDONDHSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrDONDHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdChose: TAction
      Caption = 'CmdChose'
    end
    object CmdClose: TAction
      Caption = 'CmdClose'
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin    '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 172
    Top = 224
    object Tm1: TMenuItem
      Action = CmdSearch
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 234
    Top = 196
  end
  object QrDMNCC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MADT, TENDT'
      '  from DM_NCC'
      'order by TENDT'
      ' ')
    Left = 206
    Top = 196
  end
end
