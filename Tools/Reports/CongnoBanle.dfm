object FrmCongnoBanle: TFrmCongnoBanle
  Left = 214
  Top = 131
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch H'#243'a '#272#417'n C'#244'ng N'#7907
  ClientHeight = 573
  ClientWidth = 1045
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000000000000680500001600000028000000100000002000
    0000010008000000000040010000000000000000000000000000000000006666
    66004242A600B2423200E6825200CE623E00DA724600C6523A00E6A23A00EEAE
    5A00BE4A3600DA6A4200E27E5200D2D2D200BA463200D66A4200E2724600F2BA
    6E00CA5A3E00EEAE5600FEFEFE00F2BA72008E8E8E00B6463200D2623E00DE72
    4600C6563A00E6A23E00BE4E3600DE6E4600FEDEBE00BA463600D26E4200E676
    4600F6BA6E000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000002222
    2222151522222222221515222222222200150C0C15000000150C0C1522222222
    22000C0C00222222000C0C000022222222220000222222222200002200222222
    2222222222222222222222220022222222220202020202020202020202222222
    2206020202020202020202020222222222060207020702070207020702222222
    0A0606060606060606060606062222220A080408170817081708170806222205
    0A0A0A0A0A0A0A0A0A0A0A0A172222050510051005100F10051405100A220F0F
    0F0F0F0F0F0F0F0F0F0F0F0F0A220F1D1D1D1D1D1D1D1D1D1D1D1D1D0B220F0F
    0F0F0F0F0F0F0F0F0F0F0F03010122222222222222222222222222220101F3E7
    0000C0030000E1C10000F3E50000FFFD0000F0010000E0010000E0010000C001
    0000C00100008001000080010000000100000001000000000000FFFC0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1045
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 1045
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1045
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton3: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 60
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton9: TToolButton
      Left = 120
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton4: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton11: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdThongtinCN2
      ImageIndex = 27
    end
    object ToolButton6: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 28
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 1045
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 1037
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1037
        Height = 434
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'15'#9'Ng'#224'y'#9'F'
          'QUAY'#9'4'#9'M'#227#9'F'#9'Qu'#7847'y thu ng'#226'n'
          'LK_TENQUAY'#9'20'#9'T'#234'n'#9'F'#9'Qu'#7847'y thu ng'#226'n'
          'SCT'#9'10'#9'S'#7889' bill'#9'F'
          'THANHTOAN'#9'14'#9'Ph'#7843'i thu'#9'F'
          'LK_USERNAME'#9'12'#9'M'#227#9'F'#9'Thu ng'#226'n'
          'THUNGAN'#9'22'#9'T'#234'n'#9'F'#9'Thu ng'#226'n'
          'NG_CK'#9'22'#9'Duy'#7879't chi'#7871't kh'#7845'u'#9'F'
          'NG_HUY'#9'22'#9'Ng'#432#7901'i h'#7911'y h'#243'a '#273#417'n'#9'F'
          'DELETE_DATE'#9'12'#9'Ng'#224'y h'#361'y'#9'F'
          'LK_PTTT'#9'20'#9'PTTT'#9'F'
          'LK_TTTT'#9'20'#9'thanh to'#225'n'#9'F'#9'T'#236'nh tr'#7841'ng')
        MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1037
        Height = 49
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object TntLabel1: TLabel
          Left = 151
          Top = 16
          Width = 87
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7875'm b'#225'n h'#224'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbMaKho: TwwDBLookupCombo
          Tag = 3
          Left = 248
          Top = 12
          Width = 53
          Height = 24
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MAKHO'#9'5'#9'MAKHO'#9'F'
            'TENKHO'#9'35'#9'TENKHO'#9'F')
          LookupTable = QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Navigator = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = CbMAQUAYChange
          OnBeforeDropDown = CbMaKhoBeforeDropDown
          OnCloseUp = CbMaKhoCloseUp
          OnExit = CbMAQUAYChange
          OnNotInList = CbMaKhoNotInList
ButtonEffects.Transparent=True
        end
        object CbTenKho: TwwDBLookupCombo
          Tag = 4
          Left = 304
          Top = 12
          Width = 281
          Height = 24
          TabStop = False
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENKHO'#9'35'#9'TENKHO'#9'F'
            'MAKHO'#9'5'#9'MAKHO'#9'F')
          LookupTable = QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Navigator = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = CbMAQUAYChange
          OnCloseUp = CbMaKhoCloseUp
          OnExit = CbMAQUAYChange
          OnNotInList = CbMaKhoNotInList
ButtonEffects.Transparent=True
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaBanle: TPanel
        Left = 0
        Top = 0
        Width = 1037
        Height = 57
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          1037
          57)
        object LbHH1: TLabel
          Left = 585
          Top = 8
          Width = 90
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' h'#224'ng h'#243'a'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 332
        end
        object Label18: TLabel
          Left = 693
          Top = 8
          Width = 60
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Chi'#7871't kh'#7845'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 440
        end
        object Label19: TLabel
          Left = 801
          Top = 8
          Width = 78
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ti'#7873'n thu'#7871' VAT'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 548
        end
        object Label24: TLabel
          Left = 909
          Top = 8
          Width = 46
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ph'#7843'i thu'
          FocusControl = EdTriGiaTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 656
        end
        object Label1: TLabel
          Left = 12
          Top = 7
          Width = 59
          Height = 16
          Caption = 'Thu ng'#226'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText1: TDBText
          Left = 12
          Top = 27
          Width = 161
          Height = 16
          DataField = 'THUNGAN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 756
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 444
          Top = 8
          Width = 58
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'T'#236'nh tr'#7841'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdHH1: TwwDBEdit
          Left = 585
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCK: TwwDBEdit
          Left = 693
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU_MH'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTienVAT: TwwDBEdit
          Left = 801
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THUE'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTriGiaTT: TwwDBEdit
          Left = 909
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THANHTOAN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTINHTRANG: TwwDBLookupCombo
          Left = 443
          Top = 24
          Width = 139
          Height = 22
          Anchors = [akTop, akRight]
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DGIAI'#9'15'#9#9'F')
          DataField = 'TTTT'
          DataSource = DsBH
          LookupTable = DataMain.QrTTTT
          LookupField = 'MA'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMaKhoNotInList
ButtonEffects.Transparent=True
        end
      end
      object GrDetail: TwwDBGrid2
        Left = 0
        Top = 57
        Width = 1037
        Height = 447
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'STT'#9'3'#9' STT'#9'F'
          'MAVT'#9'13'#9'M'#227' h'#224'ng'#9'F'
          'TENVT'#9'32'#9'T'#234'n h'#224'ng'#9'F'
          'DVT'#9'6'#9#272'VT'#9'F'
          'SOLUONG'#9'9'#9'S'#7889' l'#432#7907'ng'#9'F'
          'DONGIA'#9'11'#9#272#417'n gi'#225#9'F'
          'SOTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'F'
          'THUE_SUAT'#9'6'#9'VAT'#9'F'
          'TL_CK'#9'5'#9'%CK'#9'F'
          'NG_DC'#9'30'#9'Ng'#432#7901'i '#273'i'#7873'u ch'#7881'nh'#9'F'
          'TRA_DATE'#9'10'#9'Ng'#224'y'#9'F'
          'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 3
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab]
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 903
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 903
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 420
    Top = 212
    object CmdPrint: TAction
      Category = 'NAV'
      Caption = 'In'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Category = 'NAV'
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Category = 'NAV'
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdCancel: TAction
      Category = 'NAV'
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Category = 'NAV'
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdThongtinCN2: TAction
      Caption = 'Th'#244'ng tin'
      ShortCut = 114
      OnExecute = CmdThongtinCN2Execute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsBH
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'SCT'
      'CHIETKHAU'
      'SOTIEN'
      'THANHTOAN'
      'THUNGAN'
      'NG_CK'
      'NG_HUY'
      'XOA')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 392
    Top = 212
  end
  object QrBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeEdit = QrBHBeforeEdit
    BeforePost = QrBHBeforePost
    AfterScroll = QrBHAfterScroll
    OnCalcFields = QrBHCalcFields
    OnDeleteError = QrBHPostError
    OnEditError = QrBHPostError
    OnPostError = QrBHPostError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'BANLE'
      ' where'#9'LCT = '#39'BLE'#39
      '   and'#9'isnull(PRINTED, 0) = 1'
      '   and'#9'(isnull(PTTT, '#39#39') in ('#39'02'#39', '#39'03'#39')'
      '  '#9' or isnull(PTTT2, '#39#39') in ('#39'02'#39', '#39'03'#39'))'
      '   and'#9'isnull(TTTT, '#39#39') <> '#39'03'#39)
    Left = 476
    Top = 212
    object QrBHXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#241'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrBHCHIETKHAU: TFloatField
      DisplayLabel = 'Chi'#213't kh'#202'u'
      DisplayWidth = 14
      FieldName = 'CHIETKHAU'
      Visible = False
    end
    object QrBHSOTIEN: TFloatField
      DisplayLabel = 'Tr'#222' gi'#184' h'#227'a '#174#172'n'
      DisplayWidth = 14
      FieldName = 'SOTIEN'
      Visible = False
    end
    object QrBHTHUE: TFloatField
      DisplayLabel = 'Thu'#213' VAT'
      DisplayWidth = 14
      FieldName = 'THUE'
      Visible = False
    end
    object QrBHTHUNGAN: TWideStringField
      DisplayLabel = 'Thu ng'#226'n'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'THUNGAN'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CREATE_BY'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHNG_CK: TWideStringField
      DisplayLabel = 'Duy'#214't chi'#213't kh'#202'u'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_CK'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CK_BY'
      Visible = False
      Size = 30
      Lookup = True
    end
    object QrBHNG_HUY: TWideStringField
      DisplayLabel = 'Ng'#173#234'i h'#241'y h'#227'a '#174#172'n'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_HUY'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'DELETE_BY'
      Visible = False
      Size = 30
      Lookup = True
    end
    object QrBHNGAY: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
    end
    object QrBHCA: TWideStringField
      FieldName = 'CA'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object QrBHPRINTED: TBooleanField
      FieldName = 'PRINTED'
      Visible = False
    end
    object QrBHCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrBHUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrBHDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrBHSCT: TWideStringField
      DisplayLabel = 'S'#232' bill'
      FieldName = 'SCT'
    end
    object QrBHTL_CK: TFloatField
      FieldName = 'TL_CK'
      Visible = False
      OnChange = QrBHTL_CKChange
    end
    object QrBHCK_BY: TIntegerField
      FieldName = 'CK_BY'
      Visible = False
    end
    object QrBHCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrBHUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrBHDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrBHPTTT: TWideStringField
      FieldName = 'PTTT'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrBHCHUATHOI: TFloatField
      FieldName = 'CHUATHOI'
      Visible = False
    end
    object QrBHDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      Visible = False
      BlobType = ftWideMemo
    end
    object QrBHIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrBHCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
    end
    object QrBHSOLUONG: TFloatField
      FieldName = 'SOLUONG'
      Visible = False
    end
    object QrBHMADT: TWideStringField
      FieldName = 'MADT'
      Visible = False
      Size = 15
    end
    object QrBHMAKHO: TWideStringField
      DisplayLabel = 'M'#183' kho'
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrBHTHANHTOAN: TFloatField
      DisplayLabel = 'Ph'#182'i thu'
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrBHLK_TENDT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMKH
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHTENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHQUAY: TWideStringField
      FieldName = 'QUAY'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrBHLK_PTTT: TWideStringField
      DisplayLabel = 'Ph'#432#417'ng th'#7913'c thanh to'#225'n'
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = DataMain.QrPTTT_BILL
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Visible = False
      Lookup = True
    end
    object QrBHDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      FixedChar = True
      Size = 1
    end
    object QrBHLK_TENQUAY: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENQUAY'
      LookupDataSet = QrDMQUAY
      LookupKeyFields = 'QUAY'
      LookupResultField = 'TENMAY'
      KeyFields = 'QUAY'
      Size = 200
      Lookup = True
    end
    object QrBHLK_USERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_USERNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'USERNAME'
      KeyFields = 'CREATE_BY'
      Size = 200
      Lookup = True
    end
    object QrBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrBHTTTT: TWideStringField
      FieldName = 'TTTT'
      OnValidate = QrBHTTTTValidate
      Size = 2
    end
    object QrBHLK_TTTT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TTTT'
      LookupDataSet = DataMain.QrTTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'TTTT'
      Size = 200
      Lookup = True
    end
    object QrBHPTTT2: TWideStringField
      FieldName = 'PTTT2'
      Size = 2
    end
    object QrBHLK_PTTT2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PTTT2'
      LookupDataSet = DataMain.QrPTTT_BILL
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT2'
      Size = 200
      Lookup = True
    end
    object QrBHCN_TENDV: TWideStringField
      FieldName = 'CN_TENDV'
      Size = 200
    end
    object QrBHCN_MST: TWideStringField
      FieldName = 'CN_MST'
      Size = 50
    end
    object QrBHCN_DIACHI: TWideStringField
      FieldName = 'CN_DIACHI'
      Size = 200
    end
    object QrBHCN_DIACHI_HD: TWideStringField
      FieldName = 'CN_DIACHI_HD'
      Size = 200
    end
    object QrBHCN_LIENHE: TWideStringField
      FieldName = 'CN_LIENHE'
      Size = 200
    end
    object QrBHCN_DTHOAI: TWideStringField
      FieldName = 'CN_DTHOAI'
      Size = 50
    end
    object QrBHCN_EMAIL: TWideStringField
      FieldName = 'CN_EMAIL'
      Size = 100
    end
    object QrBHLK_TENTK: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_DAIDIEN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DAIDIEN'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'DAIDIEN'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'NGANHANG'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHCN_MATK: TWideStringField
      FieldName = 'CN_MATK'
      Size = 30
    end
    object QrBHDEBT_BY: TIntegerField
      FieldName = 'DEBT_BY'
    end
    object QrBHDEBT_DATE: TDateTimeField
      FieldName = 'DEBT_DATE'
    end
    object QrBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
  end
  object QrCTBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBHBeforeOpen
    AfterInsert = QrCTBHAfterInsert
    AfterEdit = QrCTBHAfterInsert
    BeforePost = QrCTBHBeforePost
    OnDeleteError = QrBHPostError
    OnEditError = QrBHPostError
    OnPostError = QrBHPostError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from BANLE_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 504
    Top = 212
    object QrCTBHSTT: TIntegerField
      DisplayWidth = 3
      FieldName = 'STT'
    end
    object QrCTBHMAVT: TWideStringField
      DisplayLabel = 'M'#183' b'#184'n h'#181'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      FixedChar = True
      Size = 15
    end
    object QrCTBHTENVT: TWideStringField
      DisplayLabel = 'T'#170'n h'#181'ng'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTBHDVT: TWideStringField
      DisplayLabel = #167'VT'
      DisplayWidth = 6
      FieldKind = fkLookup
      FieldName = 'DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTBHSOLUONG: TFloatField
      DisplayLabel = 'S'#232' l'#173#238'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
    end
    object QrCTBHDONGIA: TFloatField
      DisplayLabel = #167#172'n gi'#184
      DisplayWidth = 11
      FieldName = 'DONGIA'
    end
    object QrCTBHLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTBHTHUE_SUAT: TFloatField
      DisplayLabel = 'VAT'
      DisplayWidth = 6
      FieldName = 'THUE_SUAT'
    end
    object QrCTBHNG_DC: TWideStringField
      DisplayLabel = 'Ng'#173#234'i '#174'i'#210'u ch'#216'nh'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_DC'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'TRA_BY'
      Size = 30
      Lookup = True
    end
    object QrCTBHTRA_DATE: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      DisplayWidth = 10
      FieldName = 'TRA_DATE'
    end
    object QrCTBHTRA_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'TRA_BY'
      Visible = False
    end
    object QrCTBHTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrCTBHCHIETKHAU: TFloatField
      FieldName = 'CHIETKHAU'
    end
    object QrCTBHTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrCTBHTENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TENTAT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 25
      Lookup = True
    end
    object QrCTBHSOTIEN: TFloatField
      DisplayLabel = 'Th'#181'nh ti'#210'n'
      FieldName = 'SOTIEN'
    end
    object QrCTBHGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTBHMABO: TWideStringField
      FieldName = 'MABO'
      Size = 15
    end
    object QrCTBHTL_CK2: TFloatField
      FieldName = 'TL_CK2'
    end
    object QrCTBHTL_CK3: TFloatField
      FieldName = 'TL_CK3'
    end
    object QrCTBHTL_CK4: TFloatField
      FieldName = 'TL_CK4'
    end
    object QrCTBHTL_CK5: TFloatField
      FieldName = 'TL_CK5'
    end
    object QrCTBHTL_CK6: TFloatField
      FieldName = 'TL_CK6'
    end
    object QrCTBHKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTBHTL_CK7: TFloatField
      FieldName = 'TL_CK7'
    end
  end
  object DsBH: TDataSource
    DataSet = QrBH
    Left = 476
    Top = 240
  end
  object DsCT: TDataSource
    AutoEdit = False
    DataSet = QrCTBH
    Left = 504
    Top = 240
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAVT, TENVT, TENTAT, DVT'
      '  from DM_VT'
      'order by MAVT')
    Left = 532
    Top = 212
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 80
    Top = 259
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 80
    Top = 228
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
    end
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsKHO
    Parameters = <
      item
        Name = 'MAKHO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_QUAYTN'
      ' where MAKHO = :MAKHO'
      'order by QUAY')
    Left = 560
    Top = 212
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from DM_KHO'
      'order by MAKHO')
    Left = 588
    Top = 212
    object QrDMKHOMAKHO: TWideStringField
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDMKHOTENKHO: TWideStringField
      DisplayWidth = 35
      FieldName = 'TENKHO'
      Size = 100
    end
  end
  object DsKHO: TDataSource
    AutoEdit = False
    DataSet = QrDMKHO
    Left = 588
    Top = 240
  end
end
