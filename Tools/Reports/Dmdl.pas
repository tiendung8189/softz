(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmdl;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, fcdbtreeview, Grids, ComCtrls,
  AppEvnts, Menus, AdvMenus, ActnList, Db, ADODB, wwDBGrid2,
  wwfltdlg, wwFltDlg2, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, RzPanel, ToolWin;

type
  TFrmDmdl = class(TForm)
    QrQuocgia: TADOQuery;
    DsQuocgia: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Panel2: TPanel;
    fcDBTreeView1: TfcDBTreeView;
    Bevel1: TBevel;
    QrTinh: TADOQuery;
    DsTinh: TDataSource;
    Status: TStatusBar;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdPrint: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    PopDetail: TAdvPopupMenu;
    Lctheomthng1: TMenuItem;
    Lcdliu1: TMenuItem;
    N1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    FilterQuocgia: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PgMain: TPageControl;
    TsQuocgia: TTabSheet;
    GrQuocgia: TwwDBGrid2;
    TsTinh: TTabSheet;
    GrTinh: TwwDBGrid2;
    CmdReload: TAction;
    FilterTinh: TwwFilterDialog2;
    TsHuyen: TTabSheet;
    GrHuyen: TwwDBGrid2;
    QrHuyen: TADOQuery;
    DsHuyen: TDataSource;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrQuocgiaCREATE_BY: TIntegerField;
    QrQuocgiaUPDATE_BY: TIntegerField;
    QrQuocgiaCREATE_DATE: TDateTimeField;
    QrQuocgiaUPDATE_DATE: TDateTimeField;
    QrTinhMATINH: TWideStringField;
    QrTinhMAQG: TWideStringField;
    QrTinhMA: TWideStringField;
    QrTinhTEN: TWideStringField;
    QrTinhCREATE_BY: TIntegerField;
    QrTinhUPDATE_BY: TIntegerField;
    QrTinhCREATE_DATE: TDateTimeField;
    QrTinhUPDATE_DATE: TDateTimeField;
    FilterHuyen: TwwFilterDialog2;
    QrHuyenMAHUYEN: TWideStringField;
    QrHuyenMATINH: TWideStringField;
    QrHuyenMA: TWideStringField;
    QrHuyenTEN: TWideStringField;
    QrHuyenCREATE_BY: TIntegerField;
    QrHuyenUPDATE_BY: TIntegerField;
    QrHuyenCREATE_DATE: TDateTimeField;
    QrHuyenUPDATE_DATE: TDateTimeField;
    CmdAudit: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrQuocgiaBeforeInsert(DataSet: TDataSet);
    procedure QrQuocgiaBeforePost(DataSet: TDataSet);
    procedure QrQuocgiaBeforeDelete(DataSet: TDataSet);
    procedure OnDBError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrTinhCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrQuocgiaBeforeEdit(DataSet: TDataSet);
    procedure QrTinhAfterInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
  private
    mCanEdit, fixCode1, fixCode2, fixCode3: Boolean;
    mQuery: TADOQuery;
    mDs: TDataSource;
    mFilter: TwwFilterDialog2;
  public
    procedure Execute(r: WORD);
  end;

var
  FrmDmdl: TFrmDmdl;

implementation

uses
    exCommon, isCommon, isMsg, isDb, MainData, Rights, isLib, RepEngine;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.Execute(r: WORD);
begin
    mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
	FORM_CODE1: String = 'DM_DL_QUOCGIA';
	FORM_CODE2: String = 'DM_DL_TINH';
    FORM_CODE3: String = 'DM_DL_HUYEN';

    FIELD_CODE: String = 'MA';

procedure TFrmDmdl.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    SetDictionary ([QrQuocgia, QrTinh, QrHuyen],
        [FORM_CODE1, FORM_CODE2, FORM_CODE3],
        [FilterQuocgia, FilterTinh, FilterHuyen]);
    SetCustomGrid([FORM_CODE1, FORM_CODE2, FORM_CODE3],
        [GrQuocgia, GrTinh, GrHuyen]);

    fixCode1 := SetCodeLength(FORM_CODE1, QrQuocgia.FieldByName(FIELD_CODE));
    fixCode2 := SetCodeLength(FORM_CODE2, QrTinh.FieldByName(FIELD_CODE));
    fixCode3 := SetCodeLength(FORM_CODE3, QrHuyen.FieldByName(FIELD_CODE));

    mQuery := QrQuocgia;
    mDs := DsQuocgia;
    mFilter := FilterQuocgia;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.FormShow(Sender: TObject);
begin
    OpenDataSets([QrQuocgia, QrTinh, QrHuyen]);
	GrQuocgia.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataset(mQuery, True);
end;

    (*
    **  PageTab
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrQuocgia;
            mDs := DsQuocgia; 
            mFilter := FilterQuocgia;
            GrQuocgia.SetFocus;
	    end;
    1:
	    begin
    	    mQuery := QrTinh;
            mDs := DsTinh;
            mFilter := FilterTinh;
            GrTinh.SetFocus;
	    end;
    2:
	    begin
    	    mQuery := QrHuyen;
            mDs := DsHuyen;
            mFilter := FilterHuyen;
            GrHuyen.SetFocus;
	    end;
    end;

   	HideAudit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdNewExecute(Sender: TObject);
begin
    mQuery.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdSaveExecute(Sender: TObject);
begin
    mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdCancelExecute(Sender: TObject);
begin
    mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdDelExecute(Sender: TObject);
begin
    mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdSearchExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
        exSearch(Name + '0', DsQuocgia);
    1:
        exSearch(Name + '1', DsTinh);
    2:
        exSearch(Name + '2', DsHuyen);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, 'DM_DIALY', [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdFilterExecute(Sender: TObject);
begin
    mFilter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdClearFilterExecute(Sender: TObject);
begin
    with mFilter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdReloadExecute(Sender: TObject);
begin
    QrQuocgia.Requery;
    QrTinh.Requery;
    QrHuyen.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, mQuery, mFilter, mCanEdit);
end;

    (*
    **  Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.QrQuocgiaBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    case PgMain.ActivePageIndex of
    1:
        if QrQuocgia.FieldByName('MA').AsString = '' then
            Abort;
    2:
        if QrTinh.FieldByName('MA').AsString = '' then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.QrQuocgiaBeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.QrQuocgiaBeforePost(DataSet: TDataSet);
var
    s: String;
begin
    case PgMain.ActivePageIndex of
    0:
    	with QrQuocgia do
    	begin
            if BlankConfirm(QrQuocgia, [FIELD_CODE]) then
                Abort;

            if fixCode1 then
		        if LengthConfirm(QrQuocgia, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrQuocgia, ['TEN']) then
                Abort;
        end;
    1:
    	with QrTinh do
    	begin
            if BlankConfirm(QrTinh, [FIELD_CODE]) then
                Abort;

            if fixCode2 then
		        if LengthConfirm(QrTinh, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrTinh, ['TEN']) then
                Abort;

            s := QrQuocgia.FieldByName('MA').AsString;
			FieldByName('MAQG').AsString := s;
            FieldByName('MATINH').AsString := s + '.' + FieldByName('MA').AsString;
        end;
    2:
    	with QrHuyen do
    	begin
            if BlankConfirm(QrHuyen, [FIELD_CODE]) then
                Abort;

            if fixCode3 then
		        if LengthConfirm(QrHuyen, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrHuyen, ['TEN']) then
                Abort;

            s := QrTinh.FieldByName('MATINH').AsString;
			FieldByName('MATINH').AsString := s;
            FieldByName('MAHUYEN').AsString := s + '.' + FieldByName('MA').AsString;
        end;
    end;
    SetAudit(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.QrQuocgiaBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.OnDBError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    Action := DbeMsg;
end;

    (*
    **  Other
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(mQuery, mFilter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.GrTinhCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if SameText(Field.FullName, 'MA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.QrTinhAfterInsert(DataSet: TDataSet);
begin
    DataSet.FieldByName('MA').Clear
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmdl.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, mDs);
end;

end.
