(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonMavt;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, wwdblook, Wwdbdlg, wwidlg;

type
  TFrmChonMavt = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    EdMAVT: TwwDBLookupComboDlg;
    QrDMVT: TADOQuery;
    procedure EdMAVTInitDialog(Dialog: TwwLookupDlg);
    procedure CmdReturnKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure EdMAVTPerformCustomSearch(Sender: TObject; LookupTable: TDataSet;
      SearchField, SearchValue: string; PerformLookup: Boolean;
      var Found: Boolean);
  private
  public
    function GetValue(var pVAT: Double): String;
  end;

var
  FrmChonMavt: TFrmChonMavt;

implementation

{$R *.DFM}

uses
	isDb, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonMavt.GetValue;
begin
	QrDMVT.Open;
    if ShowModal = mrOK then
    begin
    	pVAT := QrDMVT.FieldByName('VAT_RA').AsFloat;
    	Result := EdMAVT.LookupValue;
	end
    else
    	Result := '';
	QrDMVT.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonMavt.EdMAVTInitDialog(Dialog: TwwLookupDlg);
begin
	InitSearchDialog(Dialog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonMavt.EdMAVTPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonMavt.CmdReturnKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonMavt.QrDMVTAfterOpen(DataSet: TDataSet);
begin
	SetDisplayFormat(QrDMVT, '0.00;-0.00;#');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonMavt.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
