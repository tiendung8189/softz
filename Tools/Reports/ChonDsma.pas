(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsma;

interface

uses
  Classes, Controls, Forms,
  Wwdbdlg, StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg;

type
  TFrmChonDsma = class(TForm)
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    RgLoai: TRadioGroup;
    Panel1: TPanel;
    CbMANGANH: TwwDBLookupCombo;
    CbMANHOM: TwwDBLookupCombo;
    CbNHOM: TwwDBLookupCombo;
    CbNGANH: TwwDBLookupCombo;
    QrDMVT: TADOQuery;
    LbMaVT: TLabel;
    EdMAVT: TwwDBLookupComboDlg;
    Label2: TLabel;
    Label3: TLabel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbMANGANHExit(Sender: TObject);
    procedure EdMAVTInitDialog(Dialog: TwwLookupDlg);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure EdMAVTPerformCustomSearch(Sender: TObject; LookupTable: TDataSet;
      SearchField, SearchValue: string; PerformLookup: Boolean;
      var Found: Boolean);
  private
    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmChonDsma: TFrmChonDsma;

implementation

uses
	isDb, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrNGANH, QrNHOM, QrDMVT]);
	mTrigger := False;
	CbMANGANH.LookupValue := QrNGANH.FieldByName('MANGANH').AsString;
	CbMANHOM.LookupValue := QrNHOM.FieldByName('MANHOM').AsString;
    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsma.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    CbMANGANH.Enabled := n <> 2;
    CbNGANH.Enabled := n <> 2;

    CbMANHOM.Enabled := n = 1;
    CbNHOM.Enabled := n = 1;

    EdMAVT.Enabled :=  n = 2;

    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.CbMANGANHExit(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).Text = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    0:		// Ma nganh
    	begin
		    CbNGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;
        end;
    1:		// Ten nganh
    	begin
			CbMANGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;
        end;
    2:		// Ma nhom
		CbNHOM.LookupValue := s;
    3:		// Ten nhom
		CbMANHOM.LookupValue := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.EdMAVTInitDialog(Dialog: TwwLookupDlg);
begin
	InitSearchDialog(Dialog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.EdMAVTPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbMANGANH.SetFocus;
        1:
            CbMANHOM.SetFocus;
        2:
            EdMAVT.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
        	s1 := CbMANGANH.LookupValue;
        	s2 := CbNGANH.DisplayValue;
        end;
    1:
    	begin
        	s1 := CbMANHOM.LookupValue;
        	s2 := CbNHOM.DisplayValue;
        end;
    2:
    	begin
        	s1 := EdMAVT.Text;
            s2 := QrDMVT.FieldByName('TENVT').AsString;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;

        EdMAVT.Text := '';
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := CbMANGANH.LookupValue;
    1:
       	s := CbMANHOM.LookupValue;
    2:
       	s := EdMAVT.Text;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([QrNGANH, QrNHOM, QrDMVT]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsma.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
