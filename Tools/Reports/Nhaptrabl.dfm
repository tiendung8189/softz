object FrmNhaptrabl: TFrmNhaptrabl
  Left = 167
  Top = 114
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Nh'#7853'p Tr'#7843' B'#225'n L'#7867' - H'#243'a '#272#417'n'
  ClientHeight = 573
  ClientWidth = 831
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    831
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 831
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 831
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton11: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 831
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 823
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 77
        Width = 823
        Height = 406
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho'
          'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho'
          'LK_QUAY'#9'20'#9'Qu'#7847'y'#9'F'
          'LK_THUNGAN'#9'25'#9'Thu ng'#226'n'#9'F'
          'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'
          'CHIETKHAU'#9'14'#9'Chi'#7871't kh'#7845'u'#9'F'
          'SOTIEN'#9'19'#9'Tr'#7883' gi'#225' thanh to'#225'n'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      inline frKHO: TfrKHO
        Left = 0
        Top = 0
        Width = 823
        Height = 77
        Align = alTop
        AutoSize = True
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 823
        inherited Panel1: TPanel
          Width = 823
          ExplicitWidth = 823
          inherited Label65: TLabel
            Left = 84
            Width = 47
            Height = 16
            ExplicitLeft = 84
            ExplicitWidth = 47
            ExplicitHeight = 16
          end
          inherited Label66: TLabel
            Left = 284
            Width = 54
            Height = 16
            ExplicitLeft = 284
            ExplicitWidth = 54
            ExplicitHeight = 16
          end
          inherited CbMaKho: TwwDBLookupCombo
            Color = clBtnFace
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
          inherited CbTenKho: TwwDBLookupCombo
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Chi ti'#7871't'
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 823
        Height = 95
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 83
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 291
          Top = 15
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object Label10: TLabel
          Left = 62
          Top = 62
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object TntLabel3: TLabel
          Left = 5
          Top = 38
          Width = 106
          Height = 16
          Alignment = taRightJustify
          Caption = 'Thu ng'#226'n nh'#7853'p tr'#7843
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbDrcStatus: TLabel
          Left = 526
          Top = 69
          Width = 94
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#236'nh tr'#7841'ng phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdSCT: TwwDBEdit
          Left = 346
          Top = 10
          Width = 161
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 118
          Top = 10
          Width = 165
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 0
        end
        object DBEdit1: TDBMemo
          Left = 118
          Top = 58
          Width = 389
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
        end
        object CbMANV: TwwDBLookupCombo
          Tag = 1
          Left = 118
          Top = 34
          Width = 101
          Height = 22
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'USERNAME'#9'12'#9'USERNAME'#9'F'
            'FULLNAME'#9'32'#9'FULLNAME'#9'F')
          DataField = 'THUNGAN'
          DataSource = DsNX
          LookupTable = QrUSER
          LookupField = 'USERNAME'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object CbTENNV: TwwDBLookupCombo
          Tag = 2
          Left = 222
          Top = 34
          Width = 285
          Height = 22
          TabStop = False
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'FULLNAME'#9'32'#9'FULLNAME'#9'F'
            'USERNAME'#9'12'#9'USERNAME'#9'F')
          DataField = 'THUNGAN'
          DataSource = DsNX
          LookupTable = QrUSER
          LookupField = 'USERNAME'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
        end
        object CbDrcStatus: TwwDBComboBox
          Left = 629
          Top = 65
          Width = 181
          Height = 22
          TabStop = False
          ShowButton = False
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          Color = 15794175
          DataField = 'DRC_STATUS'
          DataSource = DsNX
          DropDownCount = 8
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ItemHeight = 0
          Items.Strings = (
            '1. '#272'ang nh'#7853'p li'#7879'u'#9'1'
            '2. Ho'#224'n t'#7845't, ch'#7901' chuy'#7875'n '#273'i'#9'2'
            '3. '#272#227' chuy'#7875'n '#273'i'#9'3')
          ParentFont = False
          ReadOnly = True
          Sorted = False
          TabOrder = 5
          UnboundDataType = wwDefault
        end
        object cxGroupBox1: TGroupBox
          Left = 517
          Top = 10
          Width = 293
          Height = 53
          Caption = 'Th'#244'ng tin bill nh'#7853'p tr'#7843
          TabOrder = 6
          object Label9: TLabel
            Left = 25
            Top = 28
            Width = 29
            Height = 16
            Alignment = taRightJustify
            Caption = 'Qu'#7847'y'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object TntLabel2: TLabel
            Left = 131
            Top = 28
            Width = 35
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' bill'
          end
          object CbMAQUAY: TwwDBLookupCombo
            Tag = 1
            Left = 61
            Top = 24
            Width = 54
            Height = 22
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'QUAY'#9'5'#9'QUAY'#9'F'
              'GHICHU'#9'35'#9'GHICHU'#9'F')
            DataField = 'QUAY'
            DataSource = DsNX
            LookupTable = QrDMQUAY
            LookupField = 'QUAY'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            OnNotInList = CbMAKHONotInList
ButtonEffects.Transparent=True
          end
          object EdSoBill: TwwDBEdit
            Left = 172
            Top = 24
            Width = 101
            Height = 22
            CharCase = ecUpperCase
            Ctl3D = False
            DataField = 'SCT2'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
      object GrDetail: TwwDBGrid2
        Left = 0
        Top = 95
        Width = 823
        Height = 362
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'RSTT'#9'3'#9'STT'#9'F'
          'MAVT'#9'15'#9'M'#227' h'#224'ng'#9'T'
          'LK_TENVT'#9'35'#9'T'#234'n h'#224'ng'#9'T'
          'LK_DVT'#9'8'#9#272'VT'#9'T'
          'SOLUONG2'#9'8'#9'B'#225'n'#9'T'#9'S'#7889' l'#432#7907'ng'
          'SOLUONG'#9'10'#9'Tr'#7843#9'F'#9'S'#7889' l'#432#7907'ng'
          'DONGIA'#9'12'#9#272#417'n gi'#225#9'T'
          'SOTIEN'#9'15'#9'Th'#224'nh ti'#7873'n'#9'T'
          'TL_CK'#9'8'#9'% C.K'#9'T'
          'CHIETKHAU'#9'10'#9'Chi'#7871't kh'#7845'u'#9'T'
          'TL_CK6'#9'10'#9'M'#7863't h'#224'ng'#9'T'#9'% Chi'#7871't kh'#7845'u'
          'TL_CK5'#9'10'#9'H'#243'a '#273#417'n'#9'T'#9'% Chi'#7871't kh'#7845'u'
          'TL_CK4'#9'10'#9'B'#7897#9'T'#9'% Chi'#7871't kh'#7845'u'
          'TL_CK2'#9'10'#9'VIP'#9'T'#9'% Chi'#7871't kh'#7845'u'
          'GHICHU'#9'50'#9'Ghi ch'#250#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 5
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopDetail
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = False
        UseTFields = False
        FooterColor = 13360356
        FooterCellColor = 13360356
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      object Panel2: TPanel
        Left = 0
        Top = 457
        Width = 823
        Height = 47
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          823
          47)
        object Label6: TLabel
          Left = 403
          Top = 3
          Width = 49
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'S'#7889' l'#432#7907'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 364
        end
        object Label16: TLabel
          Left = 695
          Top = 3
          Width = 100
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 656
        end
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object TntLabel6: TLabel
          Left = 471
          Top = 3
          Width = 90
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' h'#224'ng h'#243'a'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 432
        end
        object TntLabel1: TLabel
          Left = 583
          Top = 3
          Width = 60
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Chi'#7871't kh'#7845'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 544
        end
        object DBEdit5: TwwDBEdit
          Left = 403
          Top = 17
          Width = 65
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOLUONG'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit14: TwwDBEdit
          Left = 695
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THANHTOAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 471
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit4: TwwDBEdit
          Left = 583
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU_MH'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 689
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 689
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 396
    Top = 324
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Chuy'#7875'n tab nhanh'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 118
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDetail: TAction
      Hint = 'Danh s'#225'ch <-> Chi ti'#7871't'
      ShortCut = 16418
      OnExecute = CmdDetailExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng'
      OnExecute = CmdFilterComExecute
    end
    object CmdUpdateDetail: TAction
      Caption = 'C'#7853'p nh'#7853't chi ti'#7871't m'#7863't h'#224'ng'
      OnExecute = CmdUpdateDetailExecute
    end
    object CmdDelDetail: TAction
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdDelDetailExecute
    end
    object CmdUpdateQty: TAction
      Caption = 'C'#7853'p nh'#7853't s'#7889' l'#432#7907'ng tr'#7843' h'#224'ng'
      OnExecute = CmdUpdateQtyExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MANS'
      'TENNS'
      'MAKH'
      'TENKH'
      'SOLUONG'
      'SOTIEN'
      'NG_GIAO'
      'NG_NHAN'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 396
    Top = 352
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'TRAHANG'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :NGAYD'
      '   and '#9'NGAY <  :NGAYC  + 1')
    Left = 430
    Top = 4
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXXOA: TWideStringField
      Alignment = taRightJustify
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
      Visible = False
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      FieldName = 'SCT'
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
      OnValidate = QrNXSCT2Validate
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      OnChange = QrNXMAKHOChange
      FixedChar = True
      Size = 2
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 200
      Lookup = True
    end
    object QrNXQUAY: TWideStringField
      DisplayLabel = 'Qu'#7847'y'
      FieldName = 'QUAY'
      FixedChar = True
      Size = 2
    end
    object QrNXLK_QUAY: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_QUAY'
      LookupDataSet = QrDMQUAY
      LookupKeyFields = 'QUAY'
      LookupResultField = 'GHICHU'
      KeyFields = 'QUAY'
      Size = 100
      Lookup = True
    end
    object QrNXTHUNGAN: TWideStringField
      FieldName = 'THUNGAN'
      Size = 15
    end
    object QrNXLK_THUNGAN: TWideStringField
      DisplayLabel = 'T'#234'n thu ng'#226'n'
      FieldKind = fkLookup
      FieldName = 'LK_THUNGAN'
      LookupDataSet = QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'THUNGAN'
      Size = 200
      Lookup = True
    end
    object QrNXSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      FieldName = 'SOLUONG'
    end
    object QrNXSOTIEN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225' thanh to'#225'n'
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXCHIETKHAU: TFloatField
      FieldName = 'CHIETKHAU'
      OnChange = QrNXSOTIENChange
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      OnChange = QrNXSOTIENChange
    end
    object QrNXTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrNXDELETE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrNXSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrNXDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      FixedChar = True
      Size = 1
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXTHUE: TFloatField
      FieldName = 'THUE'
    end
    object QrNXKHOA2: TGuidField
      FieldName = 'KHOA2'
      FixedChar = True
      Size = 38
    end
    object QrNXMAVIP: TWideStringField
      FieldName = 'MAVIP'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '   from '#9'TRAHANG_CT'
      'where '#9'KHOA= :KHOA'
      'order by '#9'STT')
    Left = 458
    Top = 4
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrCTTENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 100
      Lookup = True
    end
    object QrCTLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTLK_DONGIA: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_DONGIA'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTSOLUONG2: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng b'#225'n'
      FieldName = 'SOLUONG2'
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng tr'#7843
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTDONGIA: TFloatField
      FieldName = 'DONGIA'
    end
    object QrCTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrCTTL_CK: TFloatField
      DisplayLabel = '% CK'
      FieldName = 'TL_CK'
    end
    object QrCTCHIETKHAU: TFloatField
      FieldName = 'CHIETKHAU'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
    end
    object QrCTSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrCTTL_CK2: TFloatField
      FieldName = 'TL_CK2'
    end
    object QrCTTL_CK3: TFloatField
      FieldName = 'TL_CK3'
    end
    object QrCTTL_CK4: TFloatField
      FieldName = 'TL_CK4'
    end
    object QrCTTL_CK5: TFloatField
      FieldName = 'TL_CK5'
    end
    object QrCTTL_CK6: TFloatField
      FieldName = 'TL_CK6'
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrCTTL_CK7: TFloatField
      FieldName = 'TL_CK7'
    end
    object QrCTTL_CK_MAX: TFloatField
      FieldName = 'TL_CK_MAX'
    end
    object QrCTTL_CK_THEM: TFloatField
      FieldName = 'TL_CK_THEM'
    end
    object QrCTTL_CK_THEM2: TFloatField
      FieldName = 'TL_CK_THEM2'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 430
    Top = 32
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 458
    Top = 32
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select  '#9'MAVT, TENVT, TENTAT, DVT, MADT, GIANHAPVAT, GIABAN, GIA' +
        'SI, GIANHAP, GIASIVAT, TINHTRANG'
      '  from  '#9'DM_VT')
    Left = 498
    Top = 4
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 292
    Top = 304
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 324
    Top = 304
  end
  object vlTotal: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'CHIETKHAU_MH'
      'THUE'
      'SOTIEN'
      'SOTIEN1')
    DetailFields.Strings = (
      'SOLUONG'
      'CHIETKHAU'
      'TIEN_THUE'
      'SOTIEN'
      'SOTIEN1')
    Left = 440
    Top = 292
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_QUAYTN'
      'order by '#9'QUAY'
      ' ')
    Left = 570
    Top = 5
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 472
    Top = 292
    object MenuItem6: TMenuItem
      Action = CmdUpdateDetail
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Cpnhtchititmthng1: TMenuItem
      Action = CmdUpdateQty
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdDelDetail
    end
  end
  object QrUSER: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'SYS_USER'
      ' where'#9'USERNAME <> '#39'ADMIN'#39
      '   and  '#9'IsNull(IS_GROUP, 0) = 0'
      'order by '#9'USERNAME')
    Left = 542
    Top = 5
  end
  object QrCHECKBILL: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'SCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'top 1 KHOA, MAVIP, TL_CK, NGAY'
      '  from'#9'BANLE'
      ' where'#9'MAKHO = :MAKHO'
      '   and'#9'SCT = :SCT'
      '   and'#9'LCT = '#39'BLE'#39
      '  and       (ISNULL(DELETE_BY, 0) = 0) '
      'order by  NGAY desc')
    Left = 614
    Top = 5
  end
  object QrCT2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'BANLE_CT'
      ' where'#9'KHOA =:KHOA'
      'order by STT')
    Left = 642
    Top = 5
  end
end
