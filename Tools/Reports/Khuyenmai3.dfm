object FrmKhuyenmai3: TFrmKhuyenmai3
  Left = 135
  Top = 87
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Khuy'#7871'n M'#227'i Theo H'#243'a '#272#417'n - H'#224'ng H'#243'a'
  ClientHeight = 730
  ClientWidth = 1008
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1008
    730)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 1008
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1008
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 66
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object TntToolButton2: TToolButton
      Left = 66
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 74
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 140
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 206
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 214
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 280
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 288
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 354
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 362
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 18
    end
    object TntToolButton4: TToolButton
      Left = 428
      Top = 0
      Width = 8
      Caption = 'TntToolButton4'
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 436
      Top = 0
      Cursor = 1
      Action = CmdDiemban
      ImageIndex = 8
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 502
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 6
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton7: TToolButton
      Left = 510
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 1008
    Height = 690
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 640
        Width = 1000
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1000
        Height = 591
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'LYDO'#9'30'#9'L'#253' do'#9'F'
          'TUNGAY'#9'10'#9'T'#7915' ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENNGAY'#9'10'#9#272#7871'n ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'TUGIO'#9'7'#9'T'#7915' gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENGIO'#9'7'#9#272#7871'n gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsKM
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 1000
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 1000
        inherited Panel1: TPanel
          Width = 1000
          ParentColor = False
          ExplicitWidth = 1000
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object Splitter1: TSplitter
        Left = 0
        Top = 138
        Width = 1000
        Height = 2
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 784
      end
      object Bevel3: TSplitter
        Left = 498
        Top = 267
        Width = 2
        Height = 373
        ExplicitLeft = 892
        ExplicitTop = 223
        ExplicitHeight = 411
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 265
        Width = 1000
        Height = 2
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 229
      end
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 138
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 56
          Top = 14
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y l'#7853'p'
        end
        object Label2: TLabel
          Left = 291
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 7
          Top = 38
          Width = 98
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#234'n ch'#432#417'ng tr'#236'nh'
        end
        object Label5: TLabel
          Left = 30
          Top = 62
          Width = 75
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y b'#7855't '#273#7847'u'
        end
        object TntLabel2: TLabel
          Left = 219
          Top = 62
          Width = 123
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y k'#7871't th'#250'c d'#7921' ki'#7871'n'
        end
        object Label10: TLabel
          Left = 56
          Top = 110
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object TntLabel4: TLabel
          Left = 120
          Top = 86
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' gi'#7901
        end
        object TntLabel5: TLabel
          Left = 297
          Top = 86
          Width = 44
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7871'n gi'#7901
        end
        object TntLabel6: TLabel
          Left = 527
          Top = 14
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#236'nh th'#7913'c'
        end
        object Label3: TLabel
          Left = 461
          Top = 62
          Width = 121
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y k'#7871't th'#250'c th'#7921'c t'#7871
        end
        object EdSCT: TwwDBEdit
          Left = 348
          Top = 10
          Width = 153
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdLyDo: TwwDBEdit
          Left = 112
          Top = 34
          Width = 657
          Height = 22
          Ctl3D = False
          DataField = 'LYDO'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTungay: TwwDBDateTimePicker
          Left = 112
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUNGAY'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 4
        end
        object CbDenngay: TwwDBDateTimePicker
          Left = 348
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENNGAY2'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object DBEdit1: TDBMemo
          Left = 112
          Top = 106
          Width = 657
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
        end
        object CbLCT: TwwDBLookupCombo
          Left = 588
          Top = 10
          Width = 181
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DGIAI'#9'0'#9'DGIAI'#9'F')
          DataField = 'LCT'
          DataSource = DsKM
          LookupTable = QrHinhthuc
          LookupField = 'MA'
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = False
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
          OnNotInList = CbLCTNotInList
ButtonEffects.Transparent=True
        end
        object wwDBEdit1: TwwDBDateTimePicker
          Left = 164
          Top = 82
          Width = 49
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUGIO'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = False
          TabOrder = 7
          UnboundDataType = wwDTEdtTime
        end
        object wwDBEdit2: TwwDBDateTimePicker
          Left = 348
          Top = 82
          Width = 49
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENGIO'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = False
          TabOrder = 8
          UnboundDataType = wwDTEdtTime
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 588
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENNGAY'
          DataSource = DsKM
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 6
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 267
        Width = 498
        Height = 373
        Align = alLeft
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: Danh s'#225'ch c'#225'c m'#7863't h'#224'ng '#273'i'#7873'u ki'#7879'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 498
          Height = 357
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'4'#9'STT'#9'T'
            'MAVT'#9'15'#9'M'#227' h'#224'ng'#9'F'
            'TENVT'#9'30'#9'T'#234'n h'#224'ng'#9'T'
            'GIABAN'#9'13'#9'Gi'#225' b'#225'n'#9'T'
            'LK_DVT'#9'11'#9#272'VT'#9'T'
            'GHICHU'#9'20'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = popDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
      object Status2: TStatusBar
        Left = 0
        Top = 640
        Width = 1000
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object isPanel1: TisPanel
        Left = 0
        Top = 140
        Width = 1000
        Height = 125
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Khuy'#7871'n m'#227'i theo tr'#7883' gi'#225' h'#243'a '#273#417'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrCTHD: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 1000
          Height = 109
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'4'#9'STT'#9'T'
            'TU'#9'15'#9'Tr'#7883' gi'#225' t'#7915#9'F'
            'DEN'#9'15'#9'Tr'#7883' gi'#225' '#273#7871'n'#9'F'
            'TL_CK'#9'10'#9'% C.K'#9'F'
            'GHICHU'#9'50'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          Ctl3D = True
          DataSource = DsCTHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
      object PaKM: TisPanel
        Left = 500
        Top = 267
        Width = 500
        Height = 373
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 3
        HeaderCaption = ' :: '#272'i'#7873'u ki'#7879'n v'#224' ch'#7881' ti'#234'u khuy'#7871'n m'#227'i'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrCTKM: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 500
          Height = 357
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'4'#9'STT'#9'F'
            'TU'#9'10'#9'T'#7915#9'F'#9'S'#7889' l'#432#7907'ng mua'
            'DEN'#9'10'#9#272#7871'n'#9'F'#9'S'#7889' l'#432#7907'ng mua'
            'TL_CK'#9'10'#9'% C.K'#9'F'#9'Chi'#7871't kh'#7845'u - Gi'#7843'm gi'#225
            'GIABAN'#9'10'#9'Gi'#225' b'#225'n'#9'F'#9'Chi'#7871't kh'#7845'u - Gi'#7843'm gi'#225
            'MAVT'#9'15'#9'M'#227#9'F'#9'Qu'#224' t'#7863'ng'
            'TENVT'#9'36'#9'T'#234'n m'#7863't h'#224'ng'#9'T'#9'Qu'#224' t'#7863'ng'
            'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'#9'Qu'#224' t'#7863'ng'
            'DONGIA'#9'10'#9#272#417'n gi'#225#9'T'#9'Qu'#224' t'#7863'ng'
            'GHICHU'#9'30'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          Ctl3D = True
          DataSource = DsCT2
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 866
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 866
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 142
    Top = 250
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdUpdate: TAction
      Caption = 'C'#7853'p nh'#7853't'
      Hint = 'C'#7853'p nh'#7853't chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i v'#224'o danh m'#7909'c'
    end
    object CmdDiemban: TAction
      Caption = #272'i'#7875'm b'#225'n'
      Hint = 'C'#225'c kho, '#273'i'#7875'm b'#225'n c'#243' chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i'
      OnExecute = CmdDiembanExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdEmpty: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyExecute
    end
    object CmdDsNCC: TAction
      Category = 'DETAIL'
      Caption = 'Danh s'#225'ch Nh'#224' cung c'#7845'p'
      OnExecute = CmdDsNCCExecute
    end
    object CmdDsNhom: TAction
      Category = 'DETAIL'
      Caption = 'Danh s'#225'ch Ng'#224'nh / Nh'#243'm h'#224'ng'
      OnExecute = CmdDsNhomExecute
    end
    object CmdChecked: TAction
      Caption = 'Duy'#7879't'
      OnExecute = CmdCheckedExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsKM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 114
    Top = 250
  end
  object QrDMHH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.*, b.TENDT, t.TENLT'
      
        '  from'#9'DM_VT_FULL a left join DM_KH_NCC b on a.MADT=b.MADT left ' +
        'join DM_LOAITHUE t on a.LOAITHUE = t.MALT'
      'order by a.MAVT'
      '')
    Left = 408
    Top = 419
  end
  object QrKM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKMBeforeOpen
    BeforeInsert = QrKMBeforeInsert
    AfterInsert = QrKMAfterInsert
    BeforeEdit = QrKMBeforeEdit
    BeforePost = QrKMBeforePost
    AfterCancel = QrKMAfterCancel
    AfterScroll = QrKMAfterScroll
    OnCalcFields = QrKMCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'KHUYENMAI'
      ' where  LCT in (select MA from DM_HINHTHUC_KM)'
      '   and  NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '')
    Left = 418
    Top = 291
    object QrKMXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#241'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrKMIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrKMNGAY: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      FieldName = 'NGAY'
    end
    object QrKMLCT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldName = 'LCT'
      OnChange = QrKMLCTChange
      OnValidate = QrKMLCTValidate
      Size = 2
    end
    object QrKMSCT: TWideStringField
      DisplayLabel = 'S'#232' ch'#248'ng t'#245
      FieldName = 'SCT'
    end
    object QrKMTUNGAY: TDateTimeField
      DisplayLabel = 'T'#245' ng'#181'y'
      FieldName = 'TUNGAY'
    end
    object QrKMDENNGAY: TDateTimeField
      DisplayLabel = #174#213'n ng'#181'y'
      FieldName = 'DENNGAY'
    end
    object QrKMLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrKMDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrKMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKMDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrKMDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrKMDENGIO: TDateTimeField
      FieldName = 'DENGIO'
    end
    object QrKMTUGIO: TDateTimeField
      FieldName = 'TUGIO'
    end
    object QrKMLK_HINHTHUC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HINHTHUC'
      LookupDataSet = QrHinhthuc
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LCT'
      Size = 200
      Lookup = True
    end
    object QrKMDENNGAY2: TDateTimeField
      FieldName = 'DENNGAY2'
      OnChange = QrKMDENNGAY2Change
    end
    object QrKMCHECKED: TBooleanField
      DisplayLabel = 'Duy'#7879't'
      FieldName = 'CHECKED'
    end
    object QrKMKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrKMCHECKED_BY: TIntegerField
      FieldName = 'CHECKED_BY'
    end
    object QrKMCHECKED_DATE: TDateTimeField
      FieldName = 'CHECKED_DATE'
    end
    object QrKMCHECKED_DESC: TWideMemoField
      FieldName = 'CHECKED_DESC'
      BlobType = ftWideMemo
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    BeforeEdit = QrCTBeforeEdit
    BeforePost = QrCTBeforePost
    BeforeDelete = QrCTBeforeDelete
    AfterScroll = QrCTAfterScroll
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from KHUYENMAI_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 446
    Top = 291
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      OnValidate = QrCTMAVTValidate
      Size = 15
    end
    object QrCTGIABAN: TFloatField
      DisplayLabel = 'Gi'#225' b'#225'n'
      FieldName = 'GIABAN'
      OnChange = QrCTGIABANChange
    end
    object QrCTTL_LAI: TFloatField
      DisplayLabel = 'T'#7927' l'#7879' l'#227'i'
      FieldName = 'TL_LAI'
    end
    object QrCTLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_TL_LAI: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_TL_LAI'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TL_LAI'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTGHICHU: TWideStringField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsKM: TDataSource
    DataSet = QrKM
    Left = 418
    Top = 319
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 446
    Top = 319
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 198
    Top = 250
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 254
    Top = 250
  end
  object Filter2: TwwFilterDialog2
    SortBy = fdSortByFieldNo
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 170
    Top = 250
  end
  object QrHinhthuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * from DM_HINHTHUC_KM'
      ''
      ' '
      ' '
      ' ')
    Left = 440
    Top = 420
  end
  object QrCTHD: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTHDBeforeInsert
    BeforeEdit = QrCTBeforeEdit
    BeforePost = QrCTHDBeforePost
    BeforeDelete = QrCTHDBeforeDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from KHUYENMAI_HD'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 479
    Top = 291
    object IntegerField1: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'STT'
    end
    object QrCTHDTU: TFloatField
      FieldName = 'TU'
    end
    object QrCTHDDEN: TFloatField
      FieldName = 'DEN'
    end
    object QrCTHDTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrCTHDGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTHDKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTHDKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsCTHD: TDataSource
    DataSet = QrCTHD
    Left = 479
    Top = 319
  end
  object QrCT2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCT2BeforeInsert
    BeforeEdit = QrCTBeforeEdit
    BeforePost = QrCT2BeforePost
    BeforeDelete = QrCTHDBeforeDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from KHUYENMAI_CHITIEU'
      ' where KHOA= :KHOA')
    Left = 511
    Top = 291
    object IntegerField4: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCT2MAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      FieldName = 'MAVT'
      OnChange = QrCT2MAVTChange
      Size = 15
    end
    object QrCT2GIABAN: TFloatField
      DisplayLabel = 'Gi'#225' b'#225'n'
      FieldName = 'GIABAN'
      OnChange = QrCT2GIABANChange
    end
    object QrCT2LK_TENVT: TWideStringField
      DisplayLabel = 'T'#170'n h'#181'ng'
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCT2LK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCT2LK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCT2TU: TFloatField
      FieldName = 'TU'
    end
    object QrCT2DEN: TFloatField
      FieldName = 'DEN'
    end
    object QrCT2TL_CK: TFloatField
      FieldName = 'TL_CK'
      OnChange = QrCT2GIABANChange
    end
    object QrCT2SOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrCT2DONGIA: TFloatField
      FieldName = 'DONGIA'
    end
    object QrCT2GHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCT2KHOACT2: TGuidField
      FieldName = 'KHOACT2'
      FixedChar = True
      Size = 38
    end
    object QrCT2KHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCT2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsCT2: TDataSource
    DataSet = QrCT2
    Left = 511
    Top = 319
  end
  object popDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 226
    Top = 250
    object Danhschnhcungcp1: TMenuItem
      Action = CmdDsNCC
    end
    object Danhschngnhnhmhng1: TMenuItem
      Action = CmdDsNhom
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
end
