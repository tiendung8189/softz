﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Thukhac;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2, AdvMenus,
  AppEvnts, isPanel, wwfltdlg,
  frameNgay, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmThukhac = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCNGUOI: TWideStringField;
    QrTCSOTIEN: TFloatField;
    QrTCDGIAI: TWideMemoField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Hinttc1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    PaMaster: TisPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label10: TLabel;
    Label31: TLabel;
    EdSCT: TwwDBEdit;
    EdNGUOIGIAO: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBMemo1: TDBMemo;
    CbMAKHO: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    CmdReRead: TAction;
    QrTCLCT: TWideStringField;
    QrTCTC_XONG: TBooleanField;
    QrTCLYDO: TWideStringField;
    QrTCLK_LYDO: TWideStringField;
    TntLabel1: TLabel;
    wwDBLookupCombo1: TwwDBLookupCombo;
    Label18: TLabel;
    EdSOTIEN: TwwDBEdit;
    CmdAudit: TAction;
    DBText1: TDBText;
    Label3: TLabel;
    CbHinhThuc: TwwDBLookupCombo;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    QrTCDRC_STATUS: TWideStringField;
    CmdRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    QrTCMATK: TWideStringField;
    PaTK: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    CbSTK: TwwDBLookupCombo;
    CbTenTK: TwwDBLookupCombo;
    EdNganhang: TwwDBEdit;
    EdChinhanh: TwwDBEdit;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepChecked: TToolButton;
    QrTCTHANHTOAN: TFloatField;
    QrTCLK_MANH: TWideStringField;
    QrTCLK_MACN: TWideStringField;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
  private
	mCanEdit, mFuncTK : Boolean;
   	fTungay, fDenngay 	: TDateTime;
    fLoc, fSQL: String;
    mMakho, mLydo, mPTTT, mLCT: String;

  public
	procedure Execute (r: WORD);
  end;

var
  FrmThukhac: TFrmThukhac;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isLib, isCommon;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_THUKHAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.Execute;
begin
    mLCT := 'PTK';
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

	mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := DataMain.GetSysParam('DEFAULT_LYDO_THU');
    mPTTT := DataMain.GetSysParam('DEFAULT_PTTT');

    mFuncTK := DataMain.GetFuncState('SZ_TAIKHOAN');

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
    end;

    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.FormShow(Sender: TObject);
begin
	// Open Database
    with DataMain do
    	OpenDataSets([QrDMKHO, QrLYDO_TK, QrPTTT, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);

	SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exHideDrc;
	RegWrite(Name, ['Makho'], [mMakho]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
    begin
    	exShowDrc(DsTC);
        try
   	    	CbNgay.SetFocus;
        except
        end;
    end
	else
    begin
    	exHideDrc;
    	GrBrowse.SetFocus;
    end;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            Sort := s;
        end;

		Screen.Cursor := crDefault;
        If PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
		QrDMKHO.Requery;
        QrLYDO_THU.Requery;
        QrPTTT.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdNewExecute(Sender: TObject);
begin
	if not (QrTC.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
			QrTC.Cancel;

	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex =0 then
        exSearch(Name, DsTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdReReadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdRefresh.Enabled := bBrowse;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := d;
		FieldByName('LCT').AsString    := mLCT;
		FieldByName('MAKHO').AsString  := mMakho;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('LYDO').AsString   := mLydo;
        FieldByName('PTTT').AsString   := mPTTT;
        FieldByName('DRC_STATUS').AsString := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_MSG = 'Đã có phiếu thu, không thể xóa / sửa được.';

procedure TFrmThukhac.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exCanEditVoucher(QrTC);
    with QrTC do
    begin
		if FieldByName('TC_XONG').AsBoolean then
    	begin
    		ErrMsg(RS_MSG);
	    	Abort;
    	end;
		exValidClosing(FieldByName('NGAY').AsDateTime, 2);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCBeforePost(DataSet: TDataSet);
begin
    with QrTC do
    begin
		if BlankConfirm(QrTC, ['NGAY', 'MAKHO', 'PTTT', 'SOTIEN', 'LYDO']) then
    		Abort;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

        exValidClosing(FieldByName('NGAY').AsDateTime, 2);
	    SetNull(QrTC, ['MAKHO']);
    end;

	exDrcValidate(QrTC);
    DataMain.AllocSCT(mLCT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrTC.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCAfterScroll(DataSet: TDataSet);
begin
   PgMainChange(PgMain);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.QrTCAfterPost(DataSet: TDataSet);
begin
    mMakho := QrTC.FieldByName('MAKHO').AsString;
    mLydo  := QrTC.FieldByName('LYDO').AsString;
    mPTTT  := QrTC.FieldByName('PTTT').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThukhac.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;


end.
