﻿(*==============================================================================
** Scan mabh co phan ra Ma bo
**------------------------------------------------------------------------------
*)
unit frameScanCode2;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms, ExtCtrls, StdCtrls, Mask, wwdbedit,
  DB, HTMLabel, ActnList, ADODB, pngimage;

type
  TfrScanCode2 = class(TFrame)
    PaBarcode: TPanel;
    EdCode: TwwDBEdit;
    LbCode: TLabel;
    Image4: TImage;
    HTMLabel4: THTMLabel;
    MyActionList: TActionList;
    CmdScanQty: TAction;
    CmdScanCode: TAction;
    QrDMVT_BO: TADOQuery;
    QrDMVT_BOMABO: TWideStringField;
    QrDMVT_BOMABH: TWideStringField;
    QrDMVT_BOSOLUONG: TFloatField;
    QrDMVT_BOTL_CK: TFloatField;
    procedure EdCodeEnter(Sender: TObject);
    procedure EdCodeKeyPress(Sender: TObject; var Key: Char);
    procedure EdCodeExit(Sender: TObject);
    procedure CmdScanQtyExecute(Sender: TObject);
    procedure CmdScanCodeExecute(Sender: TObject);
  private
    FDataset: TDataSet;
    FQtyName, FBarCodeName: String;
    FAddNew: Boolean;

    procedure InvalidAction(msg: String = '');
    function  ValidBarcode(pMa: String; var bo: Boolean): Boolean;

    procedure ItemAddNew(ma: String);
    procedure ItemAddNew2(ma,mabo: String;sl,ck : Double);
    procedure ItemAddQty(pSoluong: Double);

  public
    procedure Init(pDataset: TDataSet; pSOLUONG: String = 'SOLUONG'; pCODE: String = 'MAVT'; pCanAdd: Boolean = False);
  end;

implementation

{$R *.dfm}

uses
    isMsg, isLib, ExCommon, Maindata;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.CmdScanCodeExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 0;
        Text := '';
		SetFocus;
    end;
	LbCode.Caption := '&BARCODE';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.CmdScanQtyExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 1;
        Text := '';
		SetFocus;
    end;
	LbCode.Caption := 'NHẬP &SỐ LƯỢNG';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.EdCodeEnter(Sender: TObject);
begin
    EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_ACTION = 'Thao tác không hợp lệ.';
	RS_CODE_FAIL  = 'Nhập sai mã hàng hóa.';

procedure TfrScanCode2.EdCodeExit(Sender: TObject);
begin
    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.EdCodeKeyPress(Sender: TObject; var Key: Char);
var
    s: String;
    b,bo: Boolean;
    x: Double;
begin
    if Key <> #13 then
    	Exit;

        // Is empty
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Commodity mode
    Key := #0;
    x := 0;

    case EdCode.Tag  of
    	// So luong
    	1:
    	begin
            b := False;

            if FDataset.IsEmpty then
                InvalidAction
            else
            begin
                try
                    x := StrToFloat(s);
                    b := True;
                except
                end;

                if not b then
                    InvalidAction
                else if Length(s) > 7 then
                    InvalidAction
                else
                    ItemAddQty(x);
            end;
        end;
        // Ma hang
        0:
        begin
        	// Quick select
            if IsDotSelect(s) <> 0 then
            begin
                if not exDotMavt(4, DataMain.QrDmvt, s) then
                    Exit;
                EdCode.Text := s;
            end;

            if not ValidBarcode(s, bo) then
				// Sai ma hang
                InvalidAction(RS_CODE_FAIL)
            else
            begin
            	if bo then
                begin
                	with QrDMVT_BO do
                    begin
                        Close;
                        Parameters[0].Value := s;
                        Open;
                        while not eof do
                        begin
                            ItemAddNew2(FieldByName('MABH').AsString, s, FieldByName('SOLUONG').AsFloat, FieldByName('TL_CK').AsFloat);
                            Next;
                        end;

                    end;
                end
				else
                begin
                    //Da co mat hang
                    if FDataset.Locate(FBarCodeName + ';MABO', VarArrayOf([s,null]), []) then
                        //Tang so luong 1
                        ItemAddQty(1)
                    else
                    begin
                        if FAddNew then
                            ItemAddNew(s)
                        else
                            InvalidAction('Mặt hàng không hợp lệ.');
                    end;
            	end;
             end;
        end;
    end;

    EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.Init;
begin
    FDataset := pDataset;
    FQtyName := pSOLUONG;
    FBarCodeName := pCODE;
    FAddNew := pCanAdd;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.InvalidAction(msg: String);
begin
    if msg = '' then
		ErrMsg(RS_INVALID_ACTION)
    else
		ErrMsg(msg);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.ItemAddNew(ma: String);
begin
    with FDataset do
    begin
        Append;
        FieldByName(FBarCodeName).AsString := ma;
        FieldByName(FQtyName).AsFloat := 1;
        Post;
    end;

    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.ItemAddNew2(ma,mabo: String; sl,ck : Double);
begin
    with FDataset do
    begin
        if (Locate(FBarCodeName+';MABO',VarArrayOf([ma,mabo]), [])) then
           ItemAddQty(sl)
        else
    	begin
	        Append;
            FieldByName(FBarCodeName).AsString := ma;
            FieldByName('MABO').AsString := mabo;
            FieldByName('TL_CK4').AsFloat := ck;
            FieldByName(FQtyName).AsFloat := sl;
        	Post;
        end;

    end;

    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode2.ItemAddQty(pSoluong: Double);
var
    x: Double;
begin
    with FDataset do
    begin
        if IsEmpty then
        begin
            InvalidAction;
            Exit;
        end;

        x := FieldByName(FQtyName).AsFloat + pSoluong;
        //So luong moi < 0
        if x < 0  then
        begin
            InvalidAction;
            Exit;
        end
        //So luong moi = 0, xoa
        else if x = 0 then
        begin
            if not YesNo(Format('Xóa mặt hàng "%s"', [FieldByName(FBarCodeName).AsString])) then
                Exit;

            DeleteConfirm(False);
            Delete;
            DeleteConfirm(True);
        end
        else
        // Gan lai so luong
        begin
            if State in [dsBrowse] then
                Edit;

            FieldByName(FQtyName).AsFloat := x;
            Post;
        end;
    end;

    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TfrScanCode2.ValidBarcode(pMa: String; var bo: Boolean): Boolean;
begin
    with DataMain.QrDMVT do
    begin
        if Locate(FBarCodeName, pMa, []) then
           Result := True
        else
            Result := False;
        bo := FieldByName('BO').AsBoolean;
    end;
end;
end.
