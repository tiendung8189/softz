(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Vip;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TFrmVip = class(TForm)
    EdMA: TEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
  public
  	function GetCode: String;
  end;

var
  FrmVip: TFrmVip;

implementation

uses
    isLib, exCommon, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmVip.GetCode;
begin
	EdMA.Text := '';
	if ShowModal = mrOK then
    	Result := EdMA.Text
    else
    	Result := ''
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVip.FormKeyPress(Sender: TObject; var Key: Char);
begin
	case Key of
	#27:
    	ModalResult := mrCancel;
    #13:
    	ModalResult := mrOK;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVip.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init
end;

end.
