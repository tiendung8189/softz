(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameLoc;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, wwdblook;

type
  TfrLOC = class(TFrame)
    Panel1: TPanel;
    Label1: TLabel;
    CbLoc: TwwDBLookupCombo;
  private
  public
  	procedure Init; overload;
  	procedure Init(const d1, d2: TDateTime); overload;
  end;

implementation

uses
	ExCommon, MainData, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrLOC.Init;
begin
    Init(Date - sysLateDay, Date)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrLOC.Init(const d1, d2: TDateTime);
begin
    DataMain.QrLOC.Open;
    CbLoc.LookupValue := sysLoc;
    CbLoc.Enabled := sysIsCentral;
end;

end.
