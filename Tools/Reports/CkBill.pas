(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CkBill;

interface

uses
  Windows, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, DB, DBCtrls, kbmMemTable, Mask, Graphics,
  wwdbedit;

type
  TFrmChietkhau = class(TForm)
    Panel1: TPanel;
    CmdOK: TBitBtn;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    EdCK: TwwDBEdit;
    Label5: TLabel;
    EdGhichu: TwwDBEdit;
    Image1: TImage;
    DataSource1: TDataSource;
    tbTemp: TkbmMemTable;
    tbTempTL_CK: TFloatField;
    tbTempDGIAI: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure CmdOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  public
  	function Get(var ck: Double; var ghichu: String): Boolean;
  end;

var
  FrmChietkhau: TFrmChietkhau;

implementation

{$R *.DFM}

uses
	isLib, isMsg, exResStr;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChietkhau.Get(var ck: Double; var ghichu: String): Boolean;
begin
	with tbTemp do
    begin
        Open;
        Append;
        FieldByName('TL_CK').AsFloat := ck;
        FieldByName('DGIAI').AsString := ghichu;
    end;

   	Result := ShowModal = mrOK;
    if Result then
		with tbTemp do
    	begin
        	ck := FieldByName('TL_CK').AsFloat;
            ghichu := FieldByName('DGIAI').AsString;
        end;

    tbTemp.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    EdCk.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.CmdOKClick(Sender: TObject);
var
    x : Double;
begin
   	x := tbTemp.FieldByName('TL_CK').AsFloat;
    if (x >= 0) and (x <= 100) then
       	ModalResult := mrOK
    else
    begin
   		Msg(RS_INVALID_DISCOUNT);
       	EdCk.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

end.
