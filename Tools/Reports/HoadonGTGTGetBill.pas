(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoadonGTGTGetBill;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  Wwdbigrd, Wwdbgrid2, ActnList, Wwdbgrid,
  Menus, AdvMenus,
  wwdbdatetimepicker;

type
  TFrmHoadonGTGTGetBill = class(TForm)
    ActionList: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClear: TAction;
    Panel1: TPanel;
    GrBill: TwwDBGrid2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    PopList: TAdvPopupMenu;
    Search1: TMenuItem;
    CmdOK: TAction;
    Panel3: TPanel;
    CbNgay: TwwDBDateTimePicker;
    TntLabel16: TLabel;
    CmdGetList: TAction;
    LIST_BILL: TADOStoredProc;
    TntBitBtn1: TBitBtn;
    LIST_BILLSCT: TWideStringField;
    LIST_BILLSOTIEN: TFloatField;
    LIST_BILLCREATE_BY: TIntegerField;
    QrUSER: TADOQuery;
    LIST_BILLTHUNGAN: TWideStringField;
    DsList: TDataSource;
    TntLabel8: TLabel;
    CbMAQUAY: TwwDBLookupCombo;
    CbTENQUAY: TwwDBLookupCombo;
    LIST_BILLQUAY: TWideStringField;
    Panel2: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    GrList: TStringGrid;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    LIST_BILLKHOA: TGuidField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdOKExecute(Sender: TObject);
    procedure CmdGetListExecute(Sender: TObject);
    procedure CbMAQUAYNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMANSBeforeDropDown(Sender: TObject);
    procedure CbMAQUAYChange(Sender: TObject);
  private
  public
  	function  Get(var pList: TStrings): Boolean;
  end;

var
  FrmHoadonGTGTGetBill: TFrmHoadonGTGTGetBill;

implementation

uses
	isDb, ExCommon, isMsg, isLib, HoadonGTGT, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoadonGTGTGetBill.Get;
var
    i: Integer;
begin
    Result := ShowModal = mrOK;
    if Result then
    begin
        pList.Clear;;
        with GrList do
        begin
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[2, i] = '' then
            		Break;
                pList.Add(Cells[2, i]);
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.FormShow(Sender: TObject);
begin
    SetCustomGrid( 'HOADON_GTGT_BILL', GrBill);
    CmdGetList.Execute;

    CbMAQUAY.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdInsExecute(Sender: TObject);
var
	s1, s2, s3: String;
    i, mCount, n: Integer;
begin
    with LIST_BILL do
    begin
    	DisableControls;
    	mCount := GrBill.SelectedList.Count - 1;
        for i := 0 to mCount do
        begin
            GotoBookMark(TBookMark(GrBill.SelectedList.Items[i]));
            s1 := FieldByName('SCT').AsString;
            s2 := FieldByName('THUNGAN').AsString;
            s3 := TGuidEx.ToString(FieldByName('KHOA'));

            // Add to List
            with GrList do
                for n := 0 to RowCount - 1 do
                begin
                    if Cells[0, n] = '' then
                    begin
                        Cells[0, n] := s1;
                        Cells[1, n] := s2;
                        Cells[2, n] := s3;
                        Row := n;
                        Break;
                    end;

                    if Cells[2, n] = s3 then
                    begin
                        Row := n;
                        Break;
                    end;
                end;
        end;

        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        	Cells[2, i] := Cells[2, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
        Cells[2, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, GrBill.DataSource)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdIns.Enabled := not LIST_BILL.IsEmpty;
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
    CmdOK.Enabled  := GrList.Cells[0, 0] <> '';
    CmdGetList.Enabled := CbNgay.Text <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    with GrList do
    begin
        GrList.ColWidths[2] := 0;	// Hide Idx Column
    	ColWidths[1] := Width - ColWidths[0] - 25;
    end;

    CbNgay.DateTime := Date;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([LIST_BILL, QrUSER]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        	Cells[2, i] := '';
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdOKExecute(Sender: TObject);
begin
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CmdGetListExecute(Sender: TObject);
var
    sQuay: String;
begin
    Wait(PREPARING);

    if CbMAQUAY.DisplayValue = '' then
        sQuay := ''
    else
        sQuay := CbMAQUAY.LookupValue;

    with LIST_BILL do
    begin
        if Active then
            Close;
        Prepared := True;
        Parameters[1].Value := sQuay;
        Parameters[2].Value := CbNgay.DateTime;
        ExecProc;
        Open;

		SetDisplayFormat(LIST_BILL, sysCurFmt);
    end;
    ClearWait;

    GrBill.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CbMAQUAYNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CbMANSBeforeDropDown(Sender: TObject);
begin
    with FrmHoadonGTGT.QrDmKho do
    begin
        //Filter := 'KHO = 0 and MACTY=''' + sysMacty + '''';
        First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTGetBill.CbMAQUAYChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDbLookupCombo).Text = '' then
		s := ''
	else
		s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TWinControl).Tag of
    0:
        CbTENQUAY.LookupValue := s;
    1:
    	CbMAQUAY.LookupValue := s;
    end;
end;

end.
