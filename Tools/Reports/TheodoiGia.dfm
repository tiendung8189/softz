object FrmTheodoiGia: TFrmTheodoiGia
  Left = 176
  Top = 114
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Tham Kh'#7843'o Gi'#225
  ClientHeight = 479
  ClientWidth = 1226
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 1226
    Height = 105
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbDONVI: TLabel
      Left = 155
      Top = 17
      Width = 36
      Height = 16
      Alignment = taRightJustify
      Caption = #272#417'n v'#7883
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label31: TLabel
      Left = 170
      Top = 45
      Width = 21
      Height = 16
      Alignment = taRightJustify
      Caption = 'Kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label65: TLabel
      Left = 144
      Top = 73
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 444
      Top = 73
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object CbDV: TwwDBLookupCombo
      Tag = 1
      Left = 324
      Top = 12
      Width = 285
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENDT'#9'40'#9'T'#234'n'#9#9
        'MADT'#9'12'#9'M'#227#9#9)
      LookupTable = QrDM_KH_NCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMaDVChange
      OnExit = CbMaDVExit
      OnNotInList = CbMaDVNotInList
ButtonEffects.Transparent=True
    end
    object CbKHO: TwwDBLookupCombo
      Tag = 3
      Left = 260
      Top = 40
      Width = 349
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENKHO'#9'40'#9'T'#234'n'#9'F'
        'MAKHO'#9'6'#9'M'#227#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMaDVChange
      OnExit = CbMaDVExit
      OnNotInList = CbMaDVNotInList
ButtonEffects.Transparent=True
    end
    object CbMaDV: TwwDBLookupCombo
      Left = 200
      Top = 12
      Width = 121
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MADT'#9'12'#9'M'#227#9'F'
        'TENDT'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDM_KH_NCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMaDVChange
      OnExit = CbMaDVExit
      OnNotInList = CbMaDVNotInList
ButtonEffects.Transparent=True
    end
    object CbMAKHO: TwwDBLookupCombo
      Tag = 2
      Left = 200
      Top = 40
      Width = 57
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'6'#9'M'#227#9'F'
        'TENKHO'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMaDVChange
      OnExit = CbMaDVExit
      OnNotInList = CbMaDVNotInList
ButtonEffects.Transparent=True
    end
    object EdTungay: TwwDBDateTimePicker
      Left = 200
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 4
      OnExit = CbMaDVExit
    end
    object EdDenngay: TwwDBDateTimePicker
      Left = 508
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 5
      OnExit = CbMaDVExit
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 105
    Width = 1226
    Height = 353
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7913'ng t'#7915
      'HOADON_SO'#9'10'#9'S'#7889' h'#243'a '#273#417'n'#9'F'#9'Ch'#7913'ng t'#7915
      'HINHTHUC_GIA'#9'20'#9'H'#236'nh th'#7913'c'#9'F'
      'DVT1'#9'7'#9'BOX'#9'F'#9#272'VT'
      'QD1'#9'4'#9'='#9'F'#9#272'VT'
      'DVT'#9'7'#9'SKU'#9'F'#9#272'VT'
      'SOLUONG2'#9'7'#9'BOX'#9'F'#9'S'#7889' l'#432#7907'ng'
      'SOLUONG'#9'7'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
      'DONGIA2'#9'12'#9'BOX'#9'F'#9#272#417'n gi'#225
      'DONGIA'#9'12'#9'SKU'#9'F'#9#272#417'n gi'#225
      'TL_CK'#9'8'#9'M'#7863't h'#224'ng'#9'% Chi'#7871't Kh'#7845'u m'#7863't h'#224'ng'#9'% Chi'#7871't Kh'#7845'u'
      'CK_HD'#9'8'#9'H'#243'a '#273#417'n'#9'% Chi'#7871't kh'#7845'u h'#243'a '#273#417'n'#9'% Chi'#7871't Kh'#7845'u'
      'TENKHO'#9'30'#9'T'#234'n kho'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = CHUNGTU_CT
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object Status: TStatusBar
    Left = 0
    Top = 458
    Width = 1226
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c '
      ShortCut = 32856
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 234
    Top = 196
  end
  object QrDM_KH_NCC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrDM_KH_NCCBeforeOpen
    Parameters = <
      item
        Name = 'LOAI'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select MADT, TENDT'
      '  from DM_KH_NCC'
      ' where LOAI = :LOAI'
      'order by TENDT')
    Left = 206
    Top = 196
  end
  object CHUNGTU_CT_REF: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'CHUNGTU_CT_REF;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MABH'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 4
        Value = Null
      end
      item
        Name = '@SENDER'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end>
    Left = 368
    Top = 184
  end
  object CHUNGTU_CT: TDataSource
    DataSet = CHUNGTU_CT_REF
    Left = 396
    Top = 184
  end
end
