object FrmDieuchinhNhap: TFrmDieuchinhNhap
  Left = 142
  Top = 92
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #272'i'#7873'u Ch'#7881'nh Phi'#7871'u Nh'#7853'p'
  ClientHeight = 573
  ClientWidth = 882
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    882
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 882
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 882
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton11: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdExBarcode
      ImageIndex = 23
    end
    object ToolButton5: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 882
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 874
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 874
        Height = 434
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT2'#9'18'#9'S'#7889' phi'#7871'u nh'#7853'p'#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Tr'#7883' gi'#225#9'F'
          'MADT'#9'10'#9'M'#227#9'F'#9'Nh'#224' cung c'#7845'p'
          'TENDT'#9'40'#9'T'#234'n'#9'F'#9'Nh'#224' cung c'#7845'p'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
          'TENKHO'#9'30'#9'T'#234'n kho'#9'F'#9'Kho h'#224'ng'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        PaintOptions.AlternatingRowColor = 16119285
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 874
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 874
        inherited Panel1: TPanel
          Width = 874
          ParentColor = False
          ExplicitWidth = 874
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PaTotal: TPanel
        Left = 0
        Top = 457
        Width = 874
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 3
        DesignSize = (
          874
          47)
        object Label24: TLabel
          Left = 746
          Top = 3
          Width = 100
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
          FocusControl = EdTriGiaTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 656
        end
        object Label32: TLabel
          Left = 210
          Top = 3
          Width = 90
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' h'#224'ng h'#243'a'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 120
        end
        object Label18: TLabel
          Left = 478
          Top = 3
          Width = 65
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'C.K h'#243'a '#273#417'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 388
        end
        object Label12: TLabel
          Left = 434
          Top = 3
          Width = 33
          Height = 13
          Anchors = [akTop, akRight]
          Caption = '% C.K'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 344
        end
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object Label3: TLabel
          Left = 702
          Top = 3
          Width = 33
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'C.l'#7879'ch'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 612
        end
        object Label6: TLabel
          Left = 322
          Top = 3
          Width = 74
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'C.K m'#7863't h'#224'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 232
        end
        object Label19: TLabel
          Left = 590
          Top = 3
          Width = 78
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ti'#7873'n thu'#7871' VAT'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 500
        end
        object EdTriGiaTT: TwwDBEdit
          Left = 746
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THANHTOAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTriGiaHH: TwwDBEdit
          Left = 210
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCK: TwwDBEdit
          Left = 478
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTLCK: TwwDBEdit
          Left = 434
          Top = 17
          Width = 41
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'TL_CK'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCL: TwwDBEdit
          Left = 702
          Top = 17
          Width = 41
          Height = 22
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'CL_THUE'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit1: TwwDBEdit
          Left = 322
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU_MH'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTienVAT: TwwDBEdit
          Left = 590
          Top = 17
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THUE'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 874
        Height = 142
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 287
          Top = 16
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object TntLabel1: TLabel
          Left = 548
          Top = 15
          Width = 82
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u nh'#7853'p'
        end
        object Label11: TLabel
          Left = 560
          Top = 39
          Width = 70
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' ch'#7913'ng t'#7915
        end
        object LbNHAPCUA: TLabel
          Left = 28
          Top = 39
          Width = 77
          Height = 16
          Alignment = taRightJustify
          Caption = 'Nh'#224' cung c'#7845'p'
        end
        object Label5: TLabel
          Left = 569
          Top = 63
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
        end
        object Label31: TLabel
          Left = 52
          Top = 63
          Width = 53
          Height = 16
          Alignment = taRightJustify
          Caption = 'Kho h'#224'ng'
        end
        object Label8: TLabel
          Left = 565
          Top = 87
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
        end
        object Label10: TLabel
          Left = 56
          Top = 111
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object Label7: TLabel
          Left = 18
          Top = 87
          Width = 87
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#7841'n thanh to'#225'n'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TntLabel9: TLabel
          Left = 172
          Top = 86
          Width = 37
          Height = 16
          Caption = '(ng'#224'y)'
        end
        object LbDrcStatus: TLabel
          Left = 536
          Top = 112
          Width = 94
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#236'nh tr'#7841'ng phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 12
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdSCT: TwwDBEdit
          Left = 343
          Top = 12
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSCT2: TwwDBEdit
          Left = 640
          Top = 12
          Width = 181
          Height = 22
          CharCase = ecUpperCase
          Ctl3D = False
          DataField = 'SCT2'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSOHDON: TwwDBEdit
          Left = 640
          Top = 36
          Width = 101
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'LK_HOADON_SO'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          MaxLength = 6
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit5: TwwDBEdit
          Left = 744
          Top = 36
          Width = 77
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'LK_HOADON_SERI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbMADT: TwwDBEdit
          Left = 112
          Top = 36
          Width = 101
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'MADT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNCC: TwwDBEdit
          Left = 216
          Top = 36
          Width = 312
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'LK_TENDT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 640
          Top = 60
          Width = 181
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'NG_GIAO'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbMAKHO: TwwDBLookupCombo
          Left = 112
          Top = 60
          Width = 53
          Height = 22
          TabStop = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MAKHO'#9'6'#9'M'#227#9'F'
            'TENKHO'#9'40'#9'T'#234'n'#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Color = 15794175
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
ButtonEffects.Transparent=True
        end
        object CbKHO: TwwDBLookupCombo
          Left = 168
          Top = 60
          Width = 360
          Height = 22
          TabStop = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TENKHO'#9'40'#9'T'#234'n'#9'F'
            'MAKHO'#9'6'#9'M'#227#9'F')
          DataField = 'MAKHO'
          DataSource = DsNX
          LookupTable = DataMain.QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          Color = 15794175
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
ButtonEffects.Transparent=True
        end
        object DBEdit2: TwwDBEdit
          Left = 640
          Top = 84
          Width = 181
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'NG_NHAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBMemo1: TDBMemo
          Left = 112
          Top = 108
          Width = 416
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
        end
        object DBEdit6: TwwDBEdit
          Left = 112
          Top = 84
          Width = 53
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'HAN_TTOAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbDrcStatus: TwwDBComboBox
          Left = 640
          Top = 108
          Width = 181
          Height = 22
          TabStop = False
          ShowButton = False
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          Color = 15794175
          DataField = 'DRC_STATUS'
          DataSource = DsNX
          DropDownCount = 8
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ItemHeight = 0
          Items.Strings = (
            '1. '#272'ang nh'#7853'p li'#7879'u'#9'1'
            '2. Ho'#224'n t'#7845't, ch'#7901' chuy'#7875'n '#273'i'#9'2'
            '3. '#272#227' chuy'#7875'n '#273'i'#9'3')
          ParentFont = False
          ReadOnly = True
          Sorted = False
          TabOrder = 13
          UnboundDataType = wwDefault
        end
      end
      object PaHoadon: TisPanel
        Left = 0
        Top = 142
        Width = 874
        Height = 81
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' .: H'#243'a '#273#417'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PaHoadon2: TPanel
          Left = 2
          Top = 18
          Width = 870
          Height = 61
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object TntLabel2: TLabel
            Left = 41
            Top = 12
            Width = 62
            Height = 16
            Alignment = taRightJustify
            Caption = 'Nh'#243'm thu'#7871
          end
          object TntLabel3: TLabel
            Left = 38
            Top = 36
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' h'#243'a '#273#417'n'
          end
          object TntLabel4: TLabel
            Left = 371
            Top = 36
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object TntLabel7: TLabel
            Left = 414
            Top = 10
            Width = 22
            Height = 16
            Caption = '(%)'
          end
          object TntLabel8: TLabel
            Left = 228
            Top = 36
            Width = 23
            Height = 16
            Alignment = taRightJustify
            Caption = 'Seri'
          end
          object EdThueVAT: TwwDBEdit
            Left = 350
            Top = 8
            Width = 57
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'THUE_SUAT'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object CbLoaithue: TwwDBLookupCombo
            Left = 110
            Top = 8
            Width = 237
            Height = 22
            TabStop = False
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taRightJustify
            Selected.Strings = (
              'TENNHOMTHUE'#9'0'#9'TENNHOMTHUE'#9'F')
            DataField = 'LOAITHUE'
            DataSource = DsNX
            LookupTable = DataMain.QrDMLOAITHUE
            LookupField = 'MALT'
            Options = [loColLines]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            AutoDropDown = True
            ShowButton = False
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
ButtonEffects.Transparent=True
          end
          object EdSOHD: TwwDBEdit
            Left = 110
            Top = 32
            Width = 101
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SO'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            MaxLength = 6
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit2: TwwDBEdit
            Left = 258
            Top = 32
            Width = 89
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SERI'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 406
            Top = 32
            Width = 93
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = 15794175
            DataField = 'HOADON_NGAY'
            DataSource = DsNX
            Epoch = 1950
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 4
          end
          object ckDatt: TDBCheckBox
            Left = 586
            Top = 38
            Width = 105
            Height = 17
            TabStop = False
            Caption = #272#227' thanh to'#225'n'
            Ctl3D = True
            DataField = 'DA_TTOAN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 223
        Width = 874
        Height = 234
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 874
          Height = 218
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;Yes')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'B1'#9'7'#9'In tem'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'TENVT'#9'32'#9'T'#234'n h'#224'ng'#9'T'
            'DVT'#9'5'#9#272'VT'#9'T'
            'GIANHAP'#9'9'#9'Gi'#225' v'#7889'n'#9'T'
            'SOLUONG'#9'8'#9'Phi'#7871'u nh'#7853'p'#9'T'#9'S'#7889' l'#432#7907'ng'
            'SOLUONG_DC'#9'8'#9#272'i'#7873'u ch'#7881'nh'#9'F'#9'S'#7889' l'#432#7907'ng'
            'DONGIA'#9'10'#9'Phi'#7871'u nh'#7853'p'#9'T'#9#272#417'n gi'#225
            'DONGIA_DC'#9'10'#9#272'i'#7873'u ch'#7881'nh'#9'F'#9#272#417'n gi'#225
            'SOTIEN'#9'12'#9'Phi'#7871'u nh'#7853'p'#9'T'#9'Th'#224'nh ti'#7873'n'
            'SOTIEN_DC'#9'12'#9#272'i'#7873'u ch'#7881'nh'#9'F'#9'Th'#224'nh ti'#7873'n'
            'TL_CK'#9'4'#9'%C.K'#9'T'
            'EX_DATE'#9'11'#9'H'#7841'n d'#249'ng'#9'T'
            'GHICHU'#9'40'#9'Ghich'#250#9'T')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 740
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 740
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 168
    Top = 390
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Hint = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdExBarcode: TAction
      Caption = 'M'#227' v'#7841'ch'
      Hint = 'Xu'#7845't d'#7919' li'#7879'u m'#227' v'#7841'ch ra file'
      OnExecute = CmdExBarcodeExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdCapnhatct: TAction
      Category = 'DETAIL'
      Caption = 'C'#7853'p nh'#7853't danh s'#225'ch chi ti'#7871't t'#7915' phi'#7871'u nh'#7853'p'
      OnExecute = CmdCapnhatctExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'TENDT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 140
    Top = 390
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from CHUNGTU'
      ' where LCT = :LCT'
      '   and NGAY >= :NGAYD'
      '   and NGAY < :NGAYC + 1'
      '   and'#9'LOC = :LOC')
    Left = 496
    Top = 1
    object QrNXXOA: TWideStringField
      DisplayLabel = 'H'#361'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 20
      FieldName = 'SCT'
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
      OnChange = QrNXSCT2Change
      OnValidate = QrNXSCT2Validate
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225' thanh to'#225'n'
      DisplayWidth = 16
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 2
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrNXTENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 10
      FieldName = 'MADT'
      FixedChar = True
      Size = 15
    end
    object QrNXLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n NCC'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 100
      Lookup = True
    end
    object QrNXHOADON_SERI: TWideStringField
      DisplayLabel = 'S'#234'ri'
      DisplayWidth = 10
      FieldName = 'HOADON_SERI'
      Visible = False
      Size = 10
    end
    object QrNXSODDH: TWideStringField
      DisplayWidth = 20
      FieldName = 'SODDH'
      Visible = False
      FixedChar = True
    end
    object QrNXHOADON_NGAY: TDateTimeField
      DisplayWidth = 18
      FieldName = 'HOADON_NGAY'
      Visible = False
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXTL_CK: TFloatField
      DisplayWidth = 10
      FieldName = 'TL_CK'
      Visible = False
      OnChange = QrNXTL_CKChange
    end
    object QrNXCHIETKHAU: TFloatField
      DisplayWidth = 10
      FieldName = 'CHIETKHAU'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrNXTHUE_SUAT: TFloatField
      DisplayWidth = 10
      FieldName = 'THUE_SUAT'
      Visible = False
    end
    object QrNXTHUE: TFloatField
      DisplayWidth = 10
      FieldName = 'THUE'
      Visible = False
    end
    object QrNXCL_THUE: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_THUE'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXSOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXCL_SOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_SOTIEN'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXPTTT: TWideStringField
      FieldName = 'PTTT'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrNXCO_HD: TBooleanField
      FieldName = 'CO_HD'
      Visible = False
    end
    object QrNXCK_BY: TIntegerField
      FieldName = 'CK_BY'
      Visible = False
    end
    object QrNXPRINTED: TBooleanField
      FieldName = 'PRINTED'
      Visible = False
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXHAN_TTOAN: TIntegerField
      FieldName = 'HAN_TTOAN'
      Visible = False
    end
    object QrNXDA_TTOAN: TBooleanField
      FieldName = 'DA_TTOAN'
      Visible = False
    end
    object QrNXLK_HAN_TTOAN: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_HAN_TTOAN'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'HAN_TTOAN'
      KeyFields = 'MADT'
      Visible = False
      Lookup = True
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXTC_SOTIEN: TFloatField
      FieldName = 'TC_SOTIEN'
      Visible = False
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrNXLK_HOADON_SO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LK_HOADON_SO'
      Calculated = True
    end
    object QrNXLK_HOADON_SERI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LK_HOADON_SERI'
      Calculated = True
    end
    object QrNXCALC_NGAY_TTOAN: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_NGAY_TTOAN'
      Calculated = True
    end
    object QrNXHOADON_SO: TWideStringField
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      Size = 1
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from CHUNGTU_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 524
    Top = 1
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      OnValidate = QrCTMAVTValidate
      FixedChar = True
      Size = 15
    end
    object QrCTLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 46
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTDVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'DVT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTDONGIA: TFloatField
      DisplayLabel = #167#172'n gi'#184
      DisplayWidth = 10
      FieldName = 'DONGIA'
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
    end
    object QrCTTL_CK: TFloatField
      DisplayLabel = '%C.K'
      DisplayWidth = 7
      FieldName = 'TL_CK'
      OnChange = QrCTTL_CKChange
    end
    object QrCTCHIETKHAU: TFloatField
      DisplayLabel = 'Chi'#7871't kh'#7845'u'
      DisplayWidth = 12
      FieldName = 'CHIETKHAU'
    end
    object QrCTGIANHAP: TFloatField
      DisplayLabel = 'Gi'#225' g'#7889'c'
      DisplayWidth = 10
      FieldName = 'GIANHAP'
      Visible = False
    end
    object QrCTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
      Visible = False
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
      Visible = False
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTTENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TENTAT'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 25
      Lookup = True
    end
    object QrCTEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTSOLUONG_DC: TFloatField
      FieldName = 'SOLUONG_DC'
      OnChange = QrCTSOLUONG_DCChange
    end
    object QrCTDONGIA_DC: TFloatField
      FieldName = 'DONGIA_DC'
      OnChange = QrCTSOLUONG_DCChange
    end
    object QrCTSOTIEN_DC: TFloatField
      FieldName = 'SOTIEN_DC'
      OnChange = QrCTSOTIEN_DCChange
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 496
    Top = 29
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 524
    Top = 29
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 250
    Top = 396
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 314
    Top = 396
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 346
    Top = 396
    object Lydanhschchitittphieunhp1: TMenuItem
      Action = CmdCapnhatct
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Splithtmthng1: TMenuItem
      Action = CmdSapthutu
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'SOTIEN'
      'CHIETKHAU_MH')
    DetailFields.Strings = (
      'SOLUONG_DC'
      'SOTIEN_DC'
      'CHIETKHAU')
    Left = 378
    Top = 396
  end
  object QrPHIEUNHAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    BeforeOpen = QrPHIEUNHAPBeforeOpen
    AfterOpen = QrPHIEUNHAPAfterOpen
    Parameters = <
      item
        Name = 'SCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'select  '#9'a.HOADON_SO, a.HOADON_SERI, a.MADT, a.MAKHO, a.LOAITHUE' +
        ', a.THUE_SUAT, a.TL_CK as TL_CK_NX,'
      
        '        '#9'a.NG_GIAO, a.NG_NHAN, a.HAN_TTOAN, a.DA_TTOAN, a.DGIAI,' +
        ' b.MAVT, c.TENVT,'
      
        '        '#9'c.DVT, b.SOLUONG, b.DONGIA, b.SOTIEN, b.GIANHAP, b.TL_C' +
        'K, b.EX_DATE, b.GHICHU, b.B1,'
      '         '#9'c.GIABAN, GIASI, c.TL_LAI, d.TENDT, t.TENLT'
      
        '  from  '#9'T_CHUNGTU a, CHUNGTU_CT b, DM_VT c left join DM_KH_NCC ' +
        'd on c.MADT=d.MADT left join DM_LOAITHUE t on c.LOAITHUE = t.MAL' +
        'T'
      ' where  '#9'a.KHOA = b.KHOA'
      '   and  '#9'b.MAVT = c.MAVT'
      '   and  '#9'a.SCT = :SCT'
      'order by '#9'b.STT')
    Left = 624
    Top = 5
    object QrPHIEUNHAPHOADON_SO: TWideStringField
      FieldName = 'HOADON_SO'
    end
    object QrPHIEUNHAPHOADON_SERI: TWideStringField
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrPHIEUNHAPMADT: TWideStringField
      FieldName = 'MADT'
      Size = 15
    end
    object QrPHIEUNHAPMAKHO: TWideStringField
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrPHIEUNHAPLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
    end
    object QrPHIEUNHAPTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
    end
    object QrPHIEUNHAPTL_CK_NX: TFloatField
      FieldName = 'TL_CK_NX'
    end
    object QrPHIEUNHAPNG_GIAO: TWideStringField
      FieldName = 'NG_GIAO'
      Size = 30
    end
    object QrPHIEUNHAPNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
    end
    object QrPHIEUNHAPHAN_TTOAN: TIntegerField
      FieldName = 'HAN_TTOAN'
    end
    object QrPHIEUNHAPDA_TTOAN: TBooleanField
      FieldName = 'DA_TTOAN'
    end
    object QrPHIEUNHAPDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrPHIEUNHAPMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrPHIEUNHAPTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrPHIEUNHAPDVT: TWideStringField
      FieldName = 'DVT'
      Size = 10
    end
    object QrPHIEUNHAPSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrPHIEUNHAPDONGIA: TFloatField
      FieldName = 'DONGIA'
    end
    object QrPHIEUNHAPSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrPHIEUNHAPGIANHAP: TFloatField
      FieldName = 'GIANHAP'
    end
    object QrPHIEUNHAPTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrPHIEUNHAPEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
    object QrPHIEUNHAPGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrPHIEUNHAPB1: TBooleanField
      FieldName = 'B1'
    end
    object QrPHIEUNHAPGIABAN: TFloatField
      FieldName = 'GIABAN'
    end
    object QrPHIEUNHAPGIASI: TFloatField
      FieldName = 'GIASI'
    end
    object QrPHIEUNHAPTL_LAI: TFloatField
      FieldName = 'TL_LAI'
    end
    object QrPHIEUNHAPTENDT: TWideStringField
      FieldName = 'TENDT'
      Size = 200
    end
  end
end
