(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosExt;

interface

uses
  Windows, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, DB, DBCtrls, kbmMemTable, Mask, Graphics,
  wwdbedit, AdvGlowButton;

type
  TFrmPosExt = class(TForm)
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  public
  end;

var
  FrmPosExt: TFrmPosExt;

implementation

{$R *.DFM}

uses
	isLib, isMsg, exResStr, PosMain;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmPosExt.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosExt.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

end.
