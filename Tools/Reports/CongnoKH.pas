﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CongnoKH;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts, wwDialog, wwfltdlg, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, crDS_KH, crCommon, crTuden;

type
  TFrmCongnoKH = class(TForm)
    ActionList: TActionList;
    CmdUpdate: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    Status: TStatusBar;
    DsList: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    popCheck: TAdvPopupMenu;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrList: TADOStoredProc;
    CmdRefesh: TAction;
    frameTuden1: TframeTuden;
    frameDS_KH1: TframeDS_KH;
    CmdReload: TAction;
    CmdPrint: TAction;
    CmdPrint2: TAction;
    CmdPrint3: TAction;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    CmdPrint4: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdPrint4Execute(Sender: TObject);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure CmdPrint3Execute(Sender: TObject);
  private
    mMadt: String;
    mTungay, mDenngay: TDateTime;
  public
  	procedure Execute(pDenngay: TDateTime; pMadt: string = '');
  end;

var
  FrmCongnoKH: TFrmCongnoKH;

implementation

uses
	ExCommon, isDb, isLib, isMsg, MainData, GuidEx, isCommon, ChonDsKH;

const
    FORM_CODE   = 'TONGHOP_CN_KH';
    mLCT        = 'CNKH';

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.Execute;
begin
    mMadt := pMadt;
    mTungay := pDenngay - sysLateDay;
    mDenngay := pDenngay;
    frameTuden1.Init2(mTungay, mDenngay);

    frameDS_KH1.Init;
    frameDS_KH1.EdDS_KH.Text := pMadt;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    TframeTuden.Create(Self);
    TframeDS_KH.Create(Self);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.FormShow(Sender: TObject);
begin
    CmdReload.Execute;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        QrList.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdPrint2Execute(Sender: TObject);
var
    repname: String;
begin
    repname := 'RP_CONGNO_PHIEU';
    ShowReport(repname, repname,
        [sysLogonUID, mLCT,
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdTu.DateTime),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdDen.DateTime),
        frameDS_KH1.EdDS_KH.Text])
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdPrint3Execute(Sender: TObject);
var
    repname: String;
begin
    repname := 'RP_CONGNO_PHIEU_MH';
    ShowReport(repname, repname,
        [sysLogonUID, mLCT,
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdTu.DateTime),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdDen.DateTime),
        frameDS_KH1.EdDS_KH.Text])
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdPrint4Execute(Sender: TObject);
var
    repname: String;
begin
    repname := 'RP_CONGNO_DT';
    ShowReport(repname, repname,
        [sysLogonUID, mLCT,
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdTu.DateTime),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdDen.DateTime),
        frameDS_KH1.EdDS_KH.Text])
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdPrintExecute(Sender: TObject);
var
    repname: String;
begin
    repname := 'RP_CONGNO_TONGHOP';
    ShowReport(repname, repname,
        [sysLogonUID, mLCT,
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdTu.DateTime),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frameTuden1.EdDen.DateTime),
        frameDS_KH1.EdDS_KH.Text])
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
    with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdRefeshExecute(Sender: TObject);
var
	s, donvi, Kho: String;
    Tungay, Denngay: TDateTime;
begin
    donvi     := Trim(frameDS_KH1.EdDS_KH.Text);
    Tungay  := frameTuden1.EdTu.Date;
    Denngay := frameTuden1.EdDen.Date;
   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mMadt <> donvi)then
	begin
   	    mMadt := donvi;
        mTungay := Tungay;
        mDenngay := Denngay;
        with QrList do
        begin
			Close;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mLCT;
            Parameters[3].Value := mTungay;
            Parameters[4].Value := mDenngay;
            Parameters[5].Value := mMadt;

			Open;

            SetDisplayFormat(QrList, sysCurFmt);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoKH.CmdReloadExecute(Sender: TObject);
begin
    mTungay := -1;
    CmdRefesh.Execute;
end;

end.
