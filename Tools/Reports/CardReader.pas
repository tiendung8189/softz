﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CardReader;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, Db, ADODB, CR208U,
  wwDataInspector, ActnList, Grids, Buttons;

type
  TFrmCardReader = class(TForm)
    EdMA: TEdit;
    Inspect: TwwDataInspector;
    DsData: TDataSource;
    QrData: TADOQuery;
    ActionList1: TActionList;
    CmdConnect: TAction;
    Button1: TSpeedButton;
    CmdRead: TAction;
    CmdWrite: TAction;
    Button2: TSpeedButton;
    Button3: TSpeedButton;
    CmdClose: TAction;
    lbWriteCount: TLabel;
    SpeedButton1: TSpeedButton;
    CmdGenCard: TAction;
    procedure FormShow(Sender: TObject);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdConnectExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdReadExecute(Sender: TObject);
    procedure CmdWriteExecute(Sender: TObject);
    procedure EdMAChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdGenCardExecute(Sender: TObject);
  private
    mPort, mBaud: Integer;
    mWriteCount: Integer;
    mCardReader: TCr208U;
    mID: Integer;

    function OpenData(pCode: String): Boolean;
    procedure ConnectCaption;
  public
  	procedure Execute(pID: Integer = 99);
  end;

var
  FrmCardReader: TFrmCardReader;

implementation

uses
	ExCommon, isStr, isLib, isEnCoding, iniFiles, isMsg, MainData, isBarcode;

{$R *.DFM}
resourcestring
    RS_CARD_CONNECT = 'Kết nối';
    RS_CARD_DISCONNECT = 'Ngắt kết nối';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.Execute;
begin
    mID := pID;
	ShowModal;
    QrData.Close;
    mCardReader.DisConnect;
    mCardReader.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.FormShow(Sender: TObject);
var
    i: Integer;
begin
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        mPort := ReadInteger('CARD_READER', 'Port', 3);
        mBaud := ReadInteger('CARD_READER', 'Baud', 19200);
        Free;
    end;

    if not mCardReader.Connect(mPort, mBaud) then
            // Auto scan port
            for i := 1 to 10 do
            begin
                if mCardReader.Connect(i, mBaud) then
                begin
                    mPort := i;
                    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
                    begin
                        WriteInteger('CARD_READER', 'Port', mPort);
                        WriteInteger('CARD_READER', 'Baud', mBaud);
                        Free;
                    end;
                    Break;
                end;
            end;

    ConnectCaption;

    Inspect.Items[0].DisplayText := IntToStr(mPort);
	EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCardReader.OpenData(pCode: String): Boolean;
begin
    with QrData do
    begin
        Close;
        Parameters[0].Value := isEncodeMD5(pCode);
        Open;
        Result := not IsEmpty;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
    b: Boolean;
begin
    b := mCardReader.Connected;
    CmdRead.Enabled := b;
    CmdWrite.Enabled := b;
    lbWriteCount.Visible := mWriteCount > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.CmdConnectExecute(Sender: TObject);
var
    b: Boolean;
begin
    with mCardReader do
        if Connected then
        begin
            DisConnect;
        end
        else
        begin
            mPort := StrToIntDef(Inspect.Items[0].DisplayText, 0);
            try
                Screen.Cursor := crSQLWait;
                b := Connect(mPort, mBaud);
            finally
                Screen.Cursor := crDefault;
            end;
            if not b then
                ErrMsg(GetLastError);
        end;

    ConnectCaption;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.CmdGenCardExecute(Sender: TObject);
var
    mCode: String;
    function GenCode: String;
    var
        s: String;
        n: Integer;
        d: Double;
    begin
        d := now*100;
        n := Trunc(d);
        s :=  '893' + isRight(FormatFloat('000', mID), 3) + isright(FormatFloat('0.00000000', d-n), 6);
        Result := s + isEAN13Checksum(s);
    end;
    function CheckExitsCode(pCode: String): Boolean;
    begin
        with TADOQuery.Create(nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text := 'select IDX from DM_LOCATION where THELENH=:THELENH';
            Parameters[0].Value := isEncodeMD5(pCode);
            Open;
            Result := not IsEmpty;
            Close;
            Free;
        end;
    end;
begin
    mCode := GenCode;
    while CheckExitsCode(mCode) do
    begin
        mCode := GenCode;
    end;
    EdMA.Text := mCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.CmdReadExecute(Sender: TObject);
var
    s: String;
    b: Boolean;
begin
    QrData.Close;
    try
        Screen.Cursor := crSQLWait;
        b := mCardReader.ReadCard(s, True);
    finally
        Screen.Cursor := crDefault;
    end;
    if not b then
    begin
        s := '';
        ErrMsg(mCardReader.GetLastError);
    end;

    EdMA.Text := s;
    OpenData(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.CmdWriteExecute(Sender: TObject);
var
    s, s2: String;
    b: Boolean;
begin
    s := Trim(EdMA.Text);
    if s = '' then
    begin
        Msg('Phải nhập mã thẻ.');
        EdMA.SetFocus;
        Exit;
    end;

    Screen.Cursor := crSQLWait;
    try
        b := mCardReader.Write_Standard(s);
    except
    end;

    Screen.Cursor := crDefault;
    if not b then
        ErrMsg(mCardReader.GetLastError)
    else
    begin
        mCardReader.NoBeep := True;
        mCardReader.Reset_Command;
        mCardReader.ReadCard(s2, True);
        mCardReader.NoBeep := False;
        if s <> s2 then
            ErrMsg('Lỗi ghi thẻ')
        else
            Inc(mWriteCount);
    end;

    lbWriteCount.Caption := IntToStr(mWriteCount);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.ConnectCaption();
begin
    if mCardReader.Connected then
        CmdConnect.Caption := RS_CARD_DISCONNECT
    else
        CmdConnect.Caption := RS_CARD_CONNECT;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.EdMAChange(Sender: TObject);
begin
    mWriteCount := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    begin
        OpenData(Trim(edMA.Text));
        EdMa.SelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCardReader.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    mCardReader := TCr208U.Create(3, mBaud);
end;

end.
