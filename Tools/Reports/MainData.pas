﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit MainData;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Db, ADODB, Wwintl, isEnv,
  AdvMenuStylers, AdvMenus, ImgList, isLogin, NativeXml;

type
  TDataMain = class(TDataModule)
    Conn: TADOConnection;
    QrDMKHO: TADOQuery;
    QrUSER: TADOQuery;
    QrDMKH: TADOQuery;
    QrTEMP: TADOQuery;
    QrPTTT_BILL: TADOQuery;
    QrPTNHAP: TADOQuery;
    QrLYDO_NK: TADOQuery;
    QrLYDO_XK: TADOQuery;
    QrPTXUAT: TADOQuery;
    QrDM_DVT: TADOQuery;
    QrLOAI_NCC: TADOQuery;
    QrLYDO_CHIPHI: TADOQuery;
    QrLOAI_KH: TADOQuery;
    QrLYDO_THU: TADOQuery;
    QrLYDO_CHI: TADOQuery;
    QrDMVIP: TADOQuery;
    QrMAU: TADOQuery;
    QrSIZE: TADOQuery;
    QrTT_DDH: TADOQuery;
    QrTT_DDHLOAI: TWideStringField;
    QrTT_DDHMA: TWideStringField;
    QrTT_DDHDGIAI: TWideStringField;
    QrDMVT_TINHTRANG: TADOQuery;
    QrHT_THUCHI: TADOQuery;
    CALC_STOCK: TADOCommand;
    QrNGOAITE: TADOQuery;
    POS_ALLOC_BILL_NO: TADOCommand;
    ALLOC_MADT: TADOCommand;
    QrDMNXB: TADOQuery;
    QrDM_NHOM: TADOQuery;
    QrDM_NHOM2: TADOQuery;
    QrDMTT_NHAPTRABL: TADOQuery;
    QrPTTT: TADOQuery;
    QrDMLOAITHUE: TADOQuery;
    ALLOC_SCT: TADOCommand;
    QrLABEL: TADOQuery;
    EXPORT_LABEL: TADOStoredProc;
    QrDMVT: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrLOAI_HD: TADOQuery;
    CheckPrintStamp: TADOCommand;
    isEnv1: TisEnv;
    ImageMark: TImageList;
    ImageSort: TImageList;
    ImageSmall: TImageList;
    wwIntl: TwwIntl;
    ImageNavi: TImageList;
    ImageOrg: TImageList;
    QrLOAI_VIP_LE: TADOQuery;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    QrDMTACGIA: TADOQuery;
    QrHINHTHUC_GIA: TADOQuery;
    SYS_SEQ_GUID: TADOCommand;
    QrTTTT: TADOQuery;
    QrPTNHAPTRA: TADOQuery;
    QrPTXUATTRA: TADOQuery;
    QrDMTK: TADOQuery;
    spUPDATE_PRINT_NO: TADOCommand;
    QrDM_NGANH: TADOQuery;
    QrLOC: TADOQuery;
    GETACCESS: TADOCommand;
    GETSEQ: TADOCommand;
    QrFUNCS: TADOQuery;
    isLogon: TisLogon;
    GET_LOGON: TADOStoredProc;
    SYS_LOGOUT: TADOStoredProc;
    SETPASS: TADOStoredProc;
    QrLOAI_DT: TADOQuery;
    QrLYDO_TK: TADOQuery;
    QrLYDO_CK: TADOQuery;
    SETPARAM: TADOCommand;
    QrDMPB: TADOQuery;
    QrDMNV: TADOQuery;
    QrLOAI_KH_NCC: TADOQuery;
    QrLOAI_VIP_SI: TADOQuery;
    ALLOC_BARCODE: TADOCommand;
    DMVT_EXISTS_CHUNGTU: TADOCommand;
    DMVT_GET_REF: TADOCommand;
    QrDMNCC: TADOQuery;
    QrDMMENHGIA: TADOQuery;
    QrSql: TADOQuery;
    QrPTKETCHUYEN: TADOQuery;
    SYS_SEQ_SHA1: TADOCommand;
    QrDMTK_DT: TADOQuery;
    QrDMTK_NV: TADOQuery;
    QrDMTK_NB: TADOQuery;
    QrLOAI_KHUYENMAI: TADOQuery;
    QrNganhang: TADOQuery;
    QrNganhangCN: TADOQuery;
    QrLYDO_KM: TADOQuery;
    fnStripToneMark: TADOCommand;
    procedure ConnWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    mEximPath: String;
  public
    procedure InitParams(const subsys: Integer = 0);
    function  Logon(const subsys: Integer = 0): Boolean;

    function  ExecSQL(const sqlStr: String): Integer;
    function  GetNewGuid: TGuid;
    function  GetNewSHA1: String;
    function  GetOutletInfo(var pQuay, pMakho: String): Boolean;
    function  CheckSlKho(pMsg: Boolean = True): Boolean;
	procedure CalcStock(pDate: TDateTime);
	function  AllocRetailBillNumber(const makho, counter: String; buydate: TDateTime): String;
    function  GetPassCode(pMakho: String): String;
    function  StripToneMark(s: String): String;

    function  GetSysParam(param: String): Variant;
    procedure SetSysParam(const ParamName: String; const Value: Variant);
    function  GetSeqValue(seq: String): Integer;
	function  GetRights(func: String; denyshow: Boolean = True): Integer; overload;
	function  GetRights(user, func: String): Integer; overload;
    function  GetRights(uid: Integer; func: String): Integer; overload;
	function  GetFuncState(func: String): Boolean;

    function  GetUsername(uid: Integer): String;
    function  GetUID(username: String ): Integer;    

    // Cap ma khach hang, NCC
    function  AllocMADT(pLen, pType: Integer; pPrefix: String = ''): String;
    procedure AllocSCT(lct: String; qr: TADOQuery; AFIELDNAME: String = 'LOC');

    procedure UpdatePrintNo(pKhoa: TGUID);

    // In tem - lable
    procedure LabelOpen;
    procedure LabelAdd(mabh: String; soluong: Double; ngay: TDateTime);
    function  LabelClose(filename: String; addhdr, overwrite: Boolean): Boolean;
    function  LabelClose2: Boolean;
    function  LabelExport(khoa: String; fTable: String = 'CHUNGTU'; fName: String = 'SOLUONG'): Boolean;

    function  IsPrintStamp(pMavt: String): Boolean;

    //DMVT
    function  AllocEAN(mPrefix, nhom: String): String;
    function  BarcodeIsUsed(pMABH: String): Boolean;
    function  DmvtGetRef(pMABH, pLCT, pMadt, pHTGia: String; var pCK: Double; var pDongia: Double): Boolean;

    //Xml
    function  ListTables(pType: Integer; ls: TStrings): Boolean;
    procedure CopyData(qrSource, qrDest: TADOQuery);

    function CreateXml: TNativeXml;

    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer); overload;
    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer); overload;

    procedure XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
    procedure XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);

    procedure XmlCreateNode(pXml: TNativeXml; tabname: String);
    procedure ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String); overload;

    procedure ImportDataXml(pXml: TNativeXml; pTable: Integer); overload;
    procedure ImportDataXml(pXml: TNativeXml; pTable: string); overload;
  end;

var
  DataMain: TDataMain;

implementation

uses
	ExCommon, Rights, isMsg, isDb, Variants, isCommon, GuidEx, RepEngine,
    isOdbc, isStr, isType, isLib, isEnCoding, isFile, AdminData;

const
    EXCEPT_FIELDS: String = 'TABLE,';
    SELECT_SQL: String = 'select * from ';
    SELECT_SQL2: String = 'select FLEXIBLE from ';

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    NOT_CONFIG_YET = 'Chưa cấu hình số hiệu máy bán hàng.';
    NOT_CONFIG_SYSLOC = 'Chưa cấu hình Chinh nhánh. Kết thúc chương trình.';
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

function TDataMain.Logon(const subsys: Integer): Boolean;
begin
	Result := False;
//    if not exGetDba then
//        Exit;

//    if not DbConnect then
//        Exit;

//    if not ServerLicenseCheck then
//    	Exit;

//    if CheckSlKho(True) then
//        Exit;

    DataMain.Conn.Close;

  // Logon
    with isLogon do
    begin
        if not Logon2(RegReadString('', Iif(subsys = 1, 'LastLogonUserPOS' ,'LastLogonUser'), '')) then
        begin
            Exit;
        end;
    end;

    if not IsLicenseCheck then
        Exit;

    sysDbConnection := Conn;

    InitParams(subsys);
    sysIsAdmin := sysLogonUser = 'ADMIN';
    if sysIsAdmin then
        Data.Initial;

    if sysLoc = '' then
    begin
        ErrMsg(NOT_CONFIG_SYSLOC);
        Exit;
    end;
//    Screen.Cursor := crHourGlass;
//    CheckWebUpdate;
//    Screen.Cursor := crDefault;

    // ePos
    if subsys = 1 then
    begin
        // Get outlet info.
        with QrTEMP do
        begin
            SQL.Text := 'select top 1 a.QUAY, a.MAKHO from DM_QUAYTN a, DM_KHO b ' +
                        ' where a.MAKHO = b.MAKHO and a.TENMAY = :TENMAY ' +
                        ' order by case when a.MAKHO = (select top 1 DEFAULT_MAKHO from SYS_CONFIG) then 0 else 1 end';
            Parameters[0].Value := isGetComputerName;
            Open;
            posQuay0 := Fields[0].AsString;
            sysDefKho := Fields[1].AsString;
            Close;
        end;

        if posQuay0 = '' then
        begin
            ErrMsg(NOT_CONFIG_YET + #13 + RS_ADMIN_CONTACT);
            Application.Terminate;
            Exit;
        end;

        posQuay := posQuay0;
        posMakho := sysDefKho;
	end;

    if not sysIsAdmin  then
        RegWrite('', Iif(subsys = 1, 'LastLogonUserPOS' ,'LastLogonUser'), sysLogonUser);

    // ePos
    if subsys = 1 then
    begin
        // Validate access right
        if DataMain.GetRights('POS_BANHANG') = R_DENY then
            Exit;

        // Kiem tra xem co phieu chua in khong
        posKhoa := TGuidEx.EmptyGuid;;
        with QrTEMP do
        begin
            SQL.Text :=
                'select	KHOA, SCT from BANLE_TAM where LCT = ''BLE''' +
                ' and isnull(PRINTED, 0) = 0' +
                ' and QUAY = ''' + posQuay + '''' +
                ' and MAKHO = ''' + posMakho + '''';
            Open;

            if not IsEmpty then
                if YesNo(EXIST_HELD_INVOICE) then
                begin
                    posKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
                    lastSct := FieldByName('SCT').AsString;
                end
                else
                begin
                    Close;
                    Exit;
                end;
            Close;
        end;

        // Get maximum bill number
        posSct := AllocRetailBillNumber(posMakho, posQuay, Date);
    end;

    sysIsChecked := GetFuncState('SZ_CHECKED');
    exInitDotMavt;
	wwIntl.Connected := not sysEnglish;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.UpdatePrintNo(pKhoa: TGUID);
begin
    with spUPDATE_PRINT_NO do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.InitParams;
var
	s: String;
    yy, mm, dd: WORD;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select * from SYS_CONFIG';
        Open;

        sysDesc1 := FieldByName('HEADER1').AsString;
		sysDesc2 := FieldByName('HEADER2').AsString;
		sysDesc3 := FieldByName('HEADER3').AsString;

        if subsys = 0 then
        begin
			sysCurFmt := FieldByName('FMT_GMS_CUR').AsString;
			sysQtyFmt := FieldByName('FMT_GMS_QTY').AsString;
            
	        s := FieldByName('FMT_GMS_THOUSAND_SEP').AsString;
            if s <> '' then
				ThousandSeparator := s[1];

            s := FieldByName('FMT_GMS_DECIMAL_SEP').AsString;
            if s <> '' then
				DecimalSeparator := s[1];
        end
        else
        begin
			sysCurFmt := FieldByName('FMT_POS_CUR').AsString;
			sysQtyFmt := FieldByName('FMT_POS_QTY').AsString;

	        s := FieldByName('FMT_POS_THOUSAND_SEP').AsString;
            if s <> '' then
				ThousandSeparator := s[1];

            s := FieldByName('FMT_POS_DECIMAL_SEP').AsString;
            if s <> '' then
				DecimalSeparator := s[1];
        end;
        sysPerFmt := FieldByName('FMT_PER').AsString;
        sysTaxFmt := FieldByName('FMT_TAX').AsString;

        sysCurRound := FieldByName('ROUNDING_CUR').AsInteger;
        sysNumRound := FieldByName('ROUNDING_NUM').AsInteger;

        sysIntFmt := '#,##0;-#,##0;#';
        sysTinhlai := FieldByName('TINH_LAI').AsInteger;

        // Date format
        if subsys = 0 then
			s := FieldByName('FMT_GMS_DATE').AsString
        else
			s := FieldByName('FMT_POS_DATE').AsString;

        ShortDateFormat := 'dd/mm/yyyy';
        TimeFormatSQL := 'hh:nn:ss';

        DateTimeFmt := s + ' ' + TimeFormatSQL;
        ShortDateFmtSQL := 'yyyy/mm/dd';
        DateTimeFmtSQL := ShortDateFmtSQL + ' ' + TimeFormatSQL;
        sysMinFmt := 'HH:nn';


        sysCloseHH := Trunc(FieldByName('KS_HH').AsDateTime);
        sysCloseCN := Trunc(FieldByName('KS_CN').AsDateTime);

        sysDefKho := FieldByName('DEFAULT_MAKHO').AsString;
        sysLoc  := FieldByName('DEFAULT_LOC').AsString;
        sysIsCentral := FieldByName('IS_CENTRAL').AsBoolean;
        sysIsThue := FieldByName('IS_THUE_VAT').AsBoolean;
        sysRepPath := FieldByName('FOLDER_REPORT').AsString;
        sysHTGia := FieldByName('DEFAULT_HINHTHUC_GIA').AsString;

        sysLateDay := FieldByName('DEFAULT_LATE_DAY').AsInteger;

//        if FieldByName('SEARCH_MATCH_ANY').AsBoolean then
//            isSetSearchOption([CODE_ANY_WHERE])
//        else
//            exSetSearchOption([]);

        
        // Begin month year user Softz
        sysBegMon  := FieldByName('BEGIN_MONTH').AsInteger;
        sysBegYear := FieldByName('BEGIN_YEAR').AsInteger;
        sysIsDrc   := FieldByName('IS_DRC').AsBoolean;

        if FindField('KHTT_LOAIVIP_LE') <> nil then
            sysKhttLe := FieldByName('KHTT_LOAIVIP_LE').AsInteger
        else
            sysKhttLe := 0;

        if FindField('KHTT_LOAIVIP_SI') <> nil then
            sysKhttSi := FieldByName('KHTT_LOAIVIP_SI').AsInteger
        else
            sysKhttSi := 0;

        DecodeDate(Date, yy, mm, dd);
        sysMon  := mm;
        sysYear := yy;

        // Done
       	Close;
        Free;
    end;
    InitCommonVariables;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.IsPrintStamp(pMavt: String): Boolean;
begin
    with CheckPrintStamp do
    begin
        Parameters[0].Value := pMavt;
        Result := Execute.RecordCount > 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ConnWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
//	UserID := dbUser;
//    Password := dbPasswd;
//	ConnectionString := BuildConStr(ConnectionString);
    ConnectionString := GetConnectionString(isNameServer, isUserServer, isPasswordServer, isDatabaseServer);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleCreate(Sender: TObject);
begin
	CloseDataSets(Conn);
    Conn.Connected := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleDestroy(Sender: TObject);
begin
//	FreeReportEngine;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ExecSQL(const sqlStr: String): Integer;
var
	n: Integer;
begin
	with Conn do
        Execute(sqlStr, n);
    Result := n;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewGuid: TGuid;
begin
    with SYS_SEQ_GUID do
    begin
        Prepared := True;
        Execute;
        Result := TGuidEx.FromString(Parameters[1].Value);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewSHA1: String;
begin
    with SYS_SEQ_SHA1 do
    begin
        Prepared := True;
        Execute;
        Result := Parameters[1].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetOutletInfo;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select a.QUAY, a.MAKHO from DM_QUAYTN a, DM_KHO b where a.MAKHO = b.MAKHO and a.TENMAY = %s and b.LOC = %s',
				[QuotedStr(isGetComputerName), QuotedStr(sysLoc)]);

            SQL.Add(' order by case when a.MAKHO = (select top 1 DEFAULT_MAKHO from SYS_CONFIG) then 0 else 1 end');
        Open;
        pQuay  := Fields[0].AsString;
        pMakho := Fields[1].AsString;
		Result := (not IsEmpty) and (pQuay <> '') and (pMakho <> '');
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetPassCode(pMakho: String): String;
begin
    with TADOQuery.Create(Nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select THELENH from DM_LOCATION where LOC = ' + QuotedStr(pMakho);
        Open;
        Result := Fields[0].AsString;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.StripToneMark(s: String): String;
begin
    with fnStripToneMark do
    begin
    	Prepared := True;
    	Parameters[1].Value := s;
        Execute;
    	Result := Parameters[0].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetSysParam;
var
    x : Variant;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select %s from SYS_CONFIG', [param]);
        Open;

		x := Fields[0].Value;
        if x = Null then
        	case Fields[0].DataType of
			ftMemo, ftString, ftWideMemo, ftWideString:
				Result := '';
            ftSmallint, ftInteger, ftWord, ftBytes:
				Result := 0;
            ftBoolean:
				Result := False;
            ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime:
				Result := 0.0;
            else
				Result := x;
            end
        else
			Result := x;

        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUID(username: String): Integer;
begin
    with QrUSER do
    begin
        if Locate('USERNAME', username, []) then
            Result := FieldByName('UID').AsInteger
        else
            Result := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUsername(uid: Integer): String;
begin
    with QrUSER do
    begin
        if Locate('UID', uid, []) then
            Result := FieldByName('USERNAME').AsString
        else
            Result := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.DmvtGetRef(pMABH, pLCT, pMadt, pHTGia: String;
    var pCK: Double; var pDongia: Double): Boolean;
begin
    with DMVT_GET_REF do
    begin
        Prepared := True;
        Parameters[1].Value := pMABH;
        Parameters[2].Value := pLCT;
        Parameters[3].Value := pMadt;
        Parameters[4].Value := pHTGia;
        Execute;
        Result := Parameters[0].Value = 0;
        if Result then
        begin
            pCK := Parameters[5].Value;
            pDongia := Parameters[6].Value;
        end else
        begin
            pCK := 0;
            pDongia := 0;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.SetSysParam;
begin
	with SETPARAM do
    begin
        CommandText := Format('update SYS_CONFIG set [%s]=:Value', [ParamName]);
        Parameters.Refresh;
        Parameters[0].Value := Value;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetSeqValue;
begin
	with GETSEQ do
    begin
    	Prepared := True;
    	Parameters[1].Value := seq;
        Parameters[3].Value := True;
        Execute;
    	Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetRights(func: String; denyshow: Boolean = True): Integer;
begin
	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := sysLogonUID;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;

    if DenyShow and (Result = R_DENY) then
    	DenyMsg;
end;

function TDataMain.GetRights(user, func: String): Integer;
var
	uid: Integer;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select UID from SYS_USER where USERNAME=''%s''', [user]);
        Open;
		uid := Fields[0].AsInteger;
        Close;
        Free;
	end;

	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := uid;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;
end;

function TDataMain.GetRights(uid: Integer; func: String): Integer;
begin
	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := uid;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetFuncState(Func : String) : Boolean;
begin
	with QrFuncs do
    begin
    	if not Active then
        	Open;
        if Locate('FUNC_CODE', Func, []) then
        	Result := FieldByName('ENABLED').AsBoolean
        else
        	Result := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.AllocRetailBillNumber(const makho, counter: String; buydate: TDateTime): String;
begin
	with POS_ALLOC_BILL_NO do
    begin
    	Prepared := True;
        Parameters[1].Value := makho;
        Parameters[2].Value := counter;
        Parameters[3].Value := buydate;
        Execute;
        Result := Parameters[4].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.AllocEAN(mPrefix, nhom: String): String;
begin
    with ALLOC_BARCODE do
    begin
    	Prepared := True;
        Parameters[1].Value := mPrefix;
        Parameters[2].Value := nhom;
        Execute;
		Result := Parameters[3].Value;
    end;
end;

(*==============================================================================
** Cap ma khach hang, NCC
** Args:
**		pLen 	length of code
**		pType	0: Khach hang; 1: NCC
**------------------------------------------------------------------------------
*)
function TDataMain.AllocMADT(pLen, pType: Integer; pPrefix: String): String;
begin
    with ALLOC_MADT do
    begin
        Prepared := True;
        Parameters[1].Value := pLen;
        Parameters[2].Value := pType;
        Parameters[4].Value := pPrefix;
        Execute;
        Result := Parameters[3].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CalcStock;
begin
	Wait(PROCESSING);
	with CALC_STOCK do
    begin
    	Prepared := True;
		Parameters[1].Value := pDate;
        Execute;
    end;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_SL_KHO   = 'Dữ liệu có nhiều hơn 2 kho hàng. Kết thúc chương trình.';
function TDataMain.CheckSlKho(pMsg: Boolean): Boolean;
begin
    Result := False;
    with QrDMKHO do
    begin
        if Active then
            Close;
        Open;
        if RecordCount > 2 then
        begin
            if pMsg then
                ErrMsg(RS_SL_KHO);
            Result := True;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.AllocSCT(lct: String; qr: TADOQuery; AFIELDNAME: String);
var
    s, mLoc: String;
    d: TDateTime;
    x, m1, m2, y1, y2: Word;
    b: Boolean;

        (*
        **
        *)
    function Proc: String;
    begin
        with ALLOC_SCT do
        begin
            Prepared := True;
            Parameters[1].Value := Qr.FieldByName('SCT').AsString;
            Parameters[2].Value := lct;     // Loai chung tu
            Parameters[3].Value := d;       // Ngay phieu
            Parameters[4].Value := sysLoc;  // Location
            Execute;
            Result := Parameters[1].Value;
        end
    end;
begin
    with qr do
    begin
        // Ngay phieu
        d := FieldByName('NGAY').AsDateTime;

        // Chung tu moi
        if State in [dsInsert] then
            s := Proc
        // Chinh sua
        else if State in [dsEdit] then
        begin
            DecodeDate(FieldByName('NGAY').OldValue, y1, m1, x);
            DecodeDate(d, y2, m2, x);
            
            if (m1 <> m2) or (y1 <> y2) then    // Thang, nam khac or Kho khac
            begin
            	// TRANGTHAI = 3
            	b := FindField('DRC_STATUS') <> Nil;
                if b then
                	b := FieldByName('DRC_STATUS').AsString = '3';

                if b then	// Da truyen di
                begin
                	ErrMsg('Không được chỉnh ngày của chứng từ đã truyền đi.');
                	Abort;
                end
                else
                begin
	                s := Proc;
    	            Msg('Số phiếu thay đổi. Số cũ: ' + FieldByName('SCT').AsString +
        	            ' * Số mới: ' + s);
                end;
            end
            else
                Exit
        end
        else
            Exit;

        // SCT moi
        FieldByName('SCT').AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_BARCODE_USED = 'Đã có dữ liệu phát sinh.';
function TDataMain.BarcodeIsUsed(pMABH: String): Boolean;
begin
    with DMVT_EXISTS_CHUNGTU do
    begin
        Prepared := True;
        Parameters[1].Value := pMABH;
        Execute;
        Result := Parameters[0].Value <> 0;
    end;
    if Result then
        ErrMsg(RS_BARCODE_USED);
end;

(*
    **  In tem - lable
    *)
var
	labelTransNo: Integer;
    labelUtf8: Boolean;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.LabelOpen;
begin
	labelTransNo := GetSeqValue('IN_BARCODE');
    labelUtf8 := GetSysParam('STAMP_UTF8');
	with QrLABEL do
    begin
    	Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.LabelAdd(mabh: String; soluong: Double; ngay: TDateTime);
begin
	with QrLABEL do
    begin
        Append;
        FieldByName('LUOT').AsInteger := labelTransNo;
        FieldByName('MABH').AsString := mabh;
        FieldByName('SOLUONG').AsInteger := RoundUp(soluong);
        FieldByName('NGAY').AsDateTime := ngay;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    // Print Stamp
    RS_LABEL_FILE_ERROR = 'Lỗi ghi ra file.';
    RS_LABEL_NOTHING 	= 'Không có nhãn nào được xuất in.';
    RS_LABEL_COUNT 		= 'Đã xuất in %d nhãn.';
    RS_LABEL_COUNT2		= 'Đã xuất in %d nhãn theo định dạng sau:'#13#13;

function TDataMain.LabelClose(filename: String; addhdr, overwrite: Boolean): Boolean;
var
    s: String;
	ls: TStrings;
    i, n, k, cnt: Integer;
begin
	QrLABEL.Close;
    Result := True;

    ls := TStringList.Create;
    if (not overwrite) and FileExists(filename) then
    	ls.LoadFromFile(filename);		// Load existing file

    with EXPORT_LABEL do
    begin
        if Active then
            Close;

    	Prepared := True;
        Parameters[1].Value := labelTransNo;
        Open;
        n := FieldCount;

        // Export header
		if addhdr and (ls.Count = 0) then
        begin
        	s := '"' + Fields[1].FieldName + '"';	// Skip column SOLUONG
        	for i := 2 to n - 1 do
            	s := s + ',"' + Fields[i].FieldName + '"';
            ls.Add(s);
        end;

        // Export labels
        cnt := 0;
        while not Eof do
        begin
        	k := Fields[0].AsInteger;
        	s := '"' + Fields[1].AsString + '"';
        	for i := 2 to n - 1 do
            	s := s + ',"' + Fields[i].AsString + '"';

            if not labelUtf8 then
	            s := UpperCase(isStripToneMark(s));

	        cnt := cnt + k;
            while k > 0 do
            begin
	            ls.Add(s);
                Dec(k);
            end;
        	Next;
        end;
        Close;
    end;

    // Write to file
    try
        if labelUtf8 then
            ls.SaveToFile(filename, TEncoding.UTF8)
        else
            ls.SaveToFile(filename)
    except
    	ErrMsg(RS_LABEL_FILE_ERROR);
	    Result := False;
    end;

    // Final
    if Result then
        if cnt = 0 then
            Msg(RS_LABEL_NOTHING)
        else if addhdr and overwrite then
            Msg(Format(RS_LABEL_COUNT, [cnt]))
        else
            Msg(Format(RS_LABEL_COUNT2 + ls[0], [cnt]));

    ls.Free;
end;

function TDataMain.LabelClose2: Boolean;
begin
	QrLABEL.Close;
    Result := ShowReport('In tem', 'EX_LABEL_TEM',[sysLogonUID, labelTransNo]);
end;

(*==============================================================================
** Arguments:
**	khoa	: Khoa nhap xuat
**	fName	: So luong tem can in. (default: 'SOLUONG')
**------------------------------------------------------------------------------
*)
function TDataMain.LabelExport(khoa: String; fTable: String; fName: String): Boolean;
var
    mFilename: String;
begin
    LabelOpen;

    DataMain.Conn.Execute(
        Format(
            'insert into IN_LABEL (LUOT, MABH, SOLUONG, NGAY)' +
            ' select %d, b.MAVT, ceiling(b.%s), a.NGAY' +
            ' from  %s a, %s b' +
            ' where	a.KHOA = %s' +
            ' and	a.KHOA = b.KHOA' +
            ' and b.B1 = 1' +
            ' and IsNull(b.%s,0) <> 0', [labelTransNo, fName, fTable, fTable + '_CT',  QuotedStr(khoa), fName])
    );

    Result := LabelClose(
    	IncludeTrailingPathDelimiter(GetSysParam('FOLDER_BARCODE')) + 'Label.csv',
    	GetSysParam('STAMP_CSV_HEADER'),
        GetSysParam('STAMP_OVERWRITE'));
end;

(*==============================================================================
** Init XML
**------------------------------------------------------------------------------
*)
function TDataMain.ListTables(pType: Integer; ls: TStrings): Boolean;
begin
    Result := False;

    // OtherX
    if (pType and 64) <> 0 then
    begin
        ls.Add('DM_KHACX')
    end;

    // Dbe
    if (pType and 32) <> 0 then
    begin
        ls.Add('SYS_DBE_OBJ');
        ls.Add('SYS_DBE_MSG');
    end;

    // Reports
    if (pType and 16) <> 0 then
        ls.Add('SYS_REPORT');

    // Functions
    if (pType and 8) <> 0 then
        ls.Add('SYS_FUNC');

    // Flexible
    if (pType and 4) <> 0 then
        ls.Add('SYS_CONFIG');

    // Dictionary
    if (pType and 2) <> 0 then
        ls.Add('SYS_DICTIONARY');

    // Grid
    if (pType and 1) <> 0 then
        ls.Add('SYS_GRID');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CopyData(qrSource, qrDest: TADOQuery);
var
    i, n: Integer;
begin
    // Export
    with qrDest do
    begin
        n := qrSource.FieldCount - 1;
        while not qrSource.Eof do
        begin
            Append;
            for i := 0 to n do
                if Fields[i].DataType <> ftAutoInc then
                begin
                    FieldByName(qrSource.Fields[i].FullName).Value := qrSource.Fields[i].Value;
                end;
            Post;
            qrSource.Next;
        end;
        Close;
    end;
    qrSource.Close;
end;

function TDataMain.CreateXml: TNativeXml;
begin
	Result := TNativeXml.CreateName('ApplicationConfigs');
	with Result do
	begin
		EncodingString := 'Utf-8';
		Root.AttributeAdd('Exportby', ExtractFileName(Application.ExeName));
        Root.AttributeAdd('Version', VersionString);
        Root.AttributeAdd('Source', isNameServer + '.' + isDatabaseServer);
	end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlCreateNode(pXml: TNativeXml; tabname: String);
var
    i, n: Integer;
    sField: String;
    nodeR: TXmlNode;
begin
    // Table
    pXml.Root.NodeNew(tabname);

    // Column & Data
    with QrSql do
    begin
        SQL.Text := SELECT_SQL + tabname;
        Open;
        n := FieldCount - 1;
        while not Eof  do
        begin
            nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
            for i := 0 to n do
                if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                begin
                    sField := Fields[i].FullName;
                    nodeR.NodeNew(sField).ValueAsString :=FieldByName(sField).AsString;
                end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String);
var
    i: Integer;
    ls: TStrings;

    (*
    **
    *)
    function TableKey(pTabname: string): string;
    begin
        if (pTabname = 'SYS_GRID') then
            Result := 'NAME'
        else
        if (pTabname = 'SYS_DICTIONARY') then
            Result := 'TABLE;FIELD'
        else
        if (pTabname = 'SYS_CONFIG') then
            Result := 'HEADER1'
        else
        if (pTabname = 'SYS_FUNC') then
            Result := 'FUNC_CODE'
        else
        if (pTabname = 'SYS_REPORT') then
            Result := 'REP_NAME'
        else
        if (pTabname = 'SYS_DBE_MSG') then
            Result := 'ERROR'
        else
        if (pTabname = 'SYS_DBE_OBJ') then
            Result := 'KEY'
        else
        if (pTabname = 'DM_KHACX')then
            Result := 'LOAI'
        else
        if (pTabname =  'LIST_OTHER_TYPE')  then
            Result := 'Type'
        else
            Result := '';
    end;

    procedure CreateNode(pXml: TNativeXml; tabname: String);
    var
        i, n: Integer;
        sField: String;
        nodeR: TXmlNode;
    begin
        // Table
        pXml.Root.NodeNew(tabname);
        if (tabname = 'DM_KHACX') or (tabname = 'LIST_OTHER_TYPE') then
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 0)
        else
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 1);

        pXml.Root.NodeByName(tabname).AttributeAdd('Key', TableKey(tabname));

        // Column & Data
        with QrSql do
        begin
            SQL.Text := SELECT_SQL + tabname;
            Open;
            n := FieldCount - 1;
            if tabname = 'SYS_CONFIG' then
            begin
                nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
                nodeR.NodeNew('FLEXIBLE').ValueAsString := (FieldByName('FLEXIBLE').AsString);
//                nodeR.NodeNew('FLEXIBLE').ValueAsString := isEncode64(FieldByName('FLEXIBLE').AsString);
            end else
            while not Eof  do
            begin
                nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
                for i := 0 to n do
                    if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                    begin
                        sField := Fields[i].FullName;
                        nodeR.NodeNew(sField).ValueAsString := (FieldByName(sField).AsString);
//                        nodeR.NodeNew(sField).ValueAsString := isEncode64(FieldByName(sField).AsString);
                    end;
                Next;
            end;
        end;
    end;

begin
    mEximPath := pFile;
    Screen.Cursor := crSQLWait;
    ls := TStringList.Create;

    ListTables(pTable, ls);
    for i := 0 to ls.Count - 1 do
        CreateNode(pXml, ls[i]);

    ls.Free;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer) overload;
var
    i: Integer;
    data: string;
begin
    with pData do
    begin
        Append;
        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer) overload;
var
    i: Integer;
    data: string;
    sK1, sK2, sV1, sV2: string;
begin
    i := Pos(';', pKey);
    if i > 0 then
    begin
        sK1 := isLeft(pKey, i-1);
        sK2 := isRight(pKey, Length(pKey)- i);
    end else
        sK1 := pKey;

    sV1 := pNode.FindNode(sK1).ValueAsUnicodeString;
    if i > 0 then
        sV2 := pNode.FindNode(sK2).ValueAsUnicodeString;

    with pData do
    begin
        if i > 0 then
        begin
//            if Locate(pKey, VarArrayOf([isDecode64(sV1), isDecode64(sV2)]),[]) then
            if Locate(pKey, VarArrayOf([(sV1), (sV2)]),[]) then
                SetEditState(pData)
            else
                Append;
        end else
        begin
//            if Locate(pKey, isDecode64(sV1), []) then
            if Locate(pKey, (sV1), []) then
                SetEditState(pData)
            else
                Append;
        end;

        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;

    NodeName := isStrReplace(pField.FieldName,' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := node.ValueAsString;
    ftWideString, ftWideMemo:
        pField.AsString := node.ValueAsUnicodeString;
    ftBoolean:
        pField.AsBoolean := node.ValueAsBool;
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := node.ValueAsInteger;
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := node.ValueAsFloat;
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(node.ValueAsString);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
    data: string;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;
    NodeName := isStrReplace(pField.FieldName, ' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
//    data := isDecode64(node.ValueAsUnicodeString);
    data := (node.ValueAsUnicodeString);
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := data;
    ftWideString, ftWideMemo:
        pField.AsString := data;
    ftBoolean:
        pField.AsBoolean := StrToBoolDef(data, False);
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := StrToIntDef(data, 0)
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := StrToFloatDef(data, 0.0);
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(data);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: Integer);
var
    lsTB: TStrings;
    i, j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
    bFull: Boolean;
    sKey: string;
begin
    lsTB := TStringList.Create;
    LsNode := TXmlNodeList.Create;
    ListTables(pTable, lsTB);

    StartProgress(lsTB.Count, 'Import');

    //No check constraint
    Conn.Execute(
        'alter table SYS_RIGHT nocheck constraint all'#13 +
        'alter table SYS_RIGHT_REP nocheck constraint all');

    // Import
    for i := 0 to lsTB.Count - 1 do
    begin
        SetProgressDesc(lsTB[i]);
        with QrSql do
        begin
            SQL.Text := SELECT_SQL + lsTB[i];
            Open;
        end;
        node := pXml.Root.FindNode(lsTB[i]);
        if node <> nil then
        begin
            bFull := StrToBoolDef(node.AttributeByName['Full'], False);
            if SameText(lsTB[i], 'SYS_CONFIG') then
            begin
                if bFull then
                    with QrSql do
                    begin
                        node := pXml.Root.NodeByName(lsTB[i]).FindNode('ROW').FindNode('FLEXIBLE');
                        if node <> nil then
                        begin
                            Edit;
//                            FieldByName('FLEXIBLE').Value := isDecode64(node.ValueAsString);
                            FieldByName('FLEXIBLE').Value := (node.ValueAsString);
                            Post;
                        end;
                        Close;
                    end
                else
                    Continue;
            end
            else
            begin
                if bFull then
                begin
                    Conn.Execute('delete ' + lsTB[i]);
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, 0);
                        end;
                    end;
                end else
                begin
                    sKey := node.AttributeByName['Key'];
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, sKey, 0);
                        end;
                    end;
                end;
            end;
        end;
    end;

    // Check constraint
    Conn.Execute(
        'alter table SYS_RIGHT check constraint all'#13 +
        'alter table SYS_RIGHT_REP check constraint all');

    // CLean up
    Conn.Execute('delete SYS_RIGHT where FUNC_CODE not in (select FUNC_CODE from SYS_FUNC)');
    Conn.Execute('delete SYS_RIGHT_REP where REP_CODE not in (select REP_CODE from SYS_REPORT)');

    lsTB.Free;
    pXml.Free;
	StopProgress;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: string);
var
    j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
begin
    LsNode := TXmlNodeList.Create;
    // Import
    with QrSql do
    begin
        SQL.Text := SELECT_SQL + pTable;
        Open;
    end;

    node := pXml.Root.FindNode(pTable);
    if node <> nil then
    begin
        Conn.Execute('delete ' + pTable);
        with QrSql do
        begin
            pXml.Root.NodeByName(pTable).FindNodes('ROW', LsNode);
            for j := 0 to LsNode.Count - 1 do
                xmlNodeToDs(LsNode.Items[j], QrSql, 0);
        end;
    end;
    LsNode.Free;
end;

end.
