﻿(*==============================================================================
** 2008.09.18 - Yen Nguyen
**	- Dai tu
**------------------------------------------------------------------------------
*)
unit NXKhac;

interface

uses
  SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, frameNgay, frameNavi,
  isDb, isPanel, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, Wwdotdot, Wwdbcomb;

type
  TFrmNXKhac = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepBarcode: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_DVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_SO: TWideStringField;
    QrNHOADON_NGAY: TDateTimeField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    QrCTLK_GIANHAP: TFloatField;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXNG_NHAN: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    Label4: TLabel;
    CbLYDO: TwwDBLookupCombo;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    CmdBalance: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    Label31: TLabel;
    CbKHO: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    TntLabel9: TLabel;
    DBMemo2: TDBMemo;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    QrCTDONGIA_REF: TFloatField;
    Label5: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    Label8: TLabel;
    DBEdit2: TwwDBEdit;
    CmdAudit: TAction;
    QrCTB1: TBooleanField;
    BtnBarCode: TToolButton;
    BtnBarCode2: TToolButton;
    QrNXDRC_STATUS: TWideStringField;
    CmdListRefesh: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdSapthutu: TAction;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrCTDONGIA_REF2: TFloatField;
    QrCTQD1: TIntegerField;
    QrCTLK_DVT1: TWideStringField;
    QrCTLK_QD1: TIntegerField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrNXLK_LYDO_NHAP: TWideStringField;
    QrNXLK_LYDO_XUAT: TWideStringField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    CbDrcStatus: TwwDBComboBox;
    LbDrcStatus: TLabel;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIASI: TFloatField;
    QrCTLK_GIABAN: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTLK_GIAVON: TFloatField;
    QrCTSOTIEN1: TFloatField;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    N4: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrCTEX_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    QrCTLK_GIASIVAT: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrNXAfterEdit(DataSet: TDataSet);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
  private
  	mLCT, mPrefix: String;
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(kh: String; r: WORD);
  end;

var
  FrmNXKhac: TFrmNXKhac;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, Sapthutu, GuidEx,
    exThread, isCommon, isFile, ImportExcel;

{$R *.DFM}

const
	FORM_CODE_N = 'PHIEU_NHAPKHAC';
    FORM_CODE_X = 'PHIEU_XUATKHAC';

    LCT_N = 'NKHAC';
    LCT_X = 'XKHAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	CAPTION_NHAP = 'Nhập Khác';
    CAPTION_XUAT = 'Xuất Khác';

procedure TFrmNXKhac.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    if kh = 'N' then
    begin
    	Caption := CAPTION_NHAP;
       	mLCT := LCT_N;
        CbLYDO.LookupTable := DataMain.QrLYDO_NK;
	end
    else
    begin
    	Caption := CAPTION_XUAT;
       	mLCT := LCT_X;
        CbLYDO.LookupTable := DataMain.QrLYDO_XK;
	end;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormCreate(Sender: TObject);
begin
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
	mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');
    CmdImportExcel.Visible := FlexConfigBool(Iif(mLCT=LCT_N,FORM_CODE_N, FORM_CODE_X), 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;
    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;

	Wait(PREPARING);
    with  DataMain do
    begin
    	OpenDataSets([QrDMVT, QrDMKHO]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TL_LAI'], sysPerFmt);
    end;
    ClearWait;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

	with QrCT do
    begin
		SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
    end;

	if mLCT = LCT_N then
    begin
        TMyForm(Self).LoadIcon('_IDI_RECEIPT');
	    DataMain.QrLYDO_NK.Open;

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_N, FORM_CODE_N + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_N , FORM_CODE_N + '_CT'], [Filter, Nil]);
    end
    else
    begin
        TMyForm(Self).LoadIcon('_IDI_ISSUE');
	    DataMain.QrLYDO_XK.Open;

        // Invisible Barcode function
        BtnBarCode.Visible := False;
        BtnBarCode2.Visible := False;
        GrDetail.RemoveField('B1');

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_X, FORM_CODE_X + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_X, FORM_CODE_X + '_CT'], [Filter, Nil]);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exHideDrc;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
        QrDMVT.Requery;
        QrDMKHO.Requery;
        if mLCT = LCT_N then
        	QrLYDO_NK.Requery
        else
        	QrLYDO_XK.Requery;
    end;
        
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdPrintExecute(Sender: TObject);
var
	sRep: String;
begin
	if mLCT = LCT_N then
    	sRep := FORM_CODE_N
    else
    	sRep := FORM_CODE_X;

	CmdSave.Execute;
	ShowReport(Caption, sRep, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLS;XLSX', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, Iif(mLCT=LCT_N,FORM_CODE_N, FORM_CODE_X),
                    QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('MAKHO').AsString         := sysDefKho;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('DRC_STATUS').AsString 	  := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterEdit(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterPost(DataSet: TDataSet);
begin
	//
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrNX.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
        FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO', 'LYDO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
    exDrcValidate(QrNX);
	DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exCanEditVoucher(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := RoundUp(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger))
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := RoundUp(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTMAVTChange(Sender: TField);
var
    x: Double;
begin
   	exDotMavt(3, Sender);
    // Update referenced fields
    with QrCT do
    begin
        FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;

        x := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA_REF').AsFloat := x;
        FieldByName('DONGIA').AsFloat := x;

        if mLCT = LCT_N then
        begin
			if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
                FieldByName('B1').AsBoolean := True;
        end;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                d := FieldByName('SOLUONG').AsFloat /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

                if d = Trunc(d) then
                    FieldByName('SOLUONG2').AsFloat := d
                else
                    FieldByName('SOLUONG2').Clear;
            end
            else if Sender.FullName = 'SOLUONG2' then
            begin
                FieldByName('SOLUONG').AsFloat :=
                    FieldByName('SOLUONG2').AsFloat *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            end;
        mTrigger := bTrigger;

        if FieldByName('SOLUONG2').AsFloat >= 1 then
            d := exVNDRound(FieldByName('SOLUONG2').AsFloat * FieldByName('DONGIA2').AsFloat)
        else
            d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat);

        // Tinh tri gia tren BOX
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTSOTIENChange(Sender: TField);
begin
    with QrCT do
    begin
        FieldByName('SOTIEN1').AsFloat := FieldByName('SOTIEN').AsFloat;
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;
        exShowDrc(DsNX);
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	exHideDrc;
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdExBarcodeExecute(Sender: TObject);
begin
    CmdSave.Execute;
	DataMain.LabelExport(TGuidEx.ToString(QrNX.FieldByName('KHOA')));
end;

	(*
    ** End: Others
    *)


end.
