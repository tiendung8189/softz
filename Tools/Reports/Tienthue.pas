(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Tienthue;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, ActnList,
  Buttons, Db, ADODB, Grids, StdCtrls;

type
  TFrmTienthue = class(TForm)
    ActionList: TActionList;
    CmdClose: TAction;
    BitBtn3: TBitBtn;
    Inspect: TwwDataInspector;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  public
  	procedure Execute (pDataset: TDataSource);
  end;

var
  FrmTienthue: TFrmTienthue;

implementation

{$R *.DFM}

uses
	Rights, isDb, Excommon, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTienthue.Execute (pDataset: TDataSource);
begin
    Inspect.DataSource := pDataset;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTienthue.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTienthue.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTienthue.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

end.
