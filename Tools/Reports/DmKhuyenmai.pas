﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmKhuyenmai;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Wwdbcomb, Menus, wwdbdatetimepicker, AppEvnts, AdvMenus,
  isPanel, wwfltdlg, RzSplit,

  fctreecombo, fcTreeView, wwDialog, Wwdotdot, Mask, RzPanel, fcCombo, Grids,
  Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmKhuyenmai = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM_KM: TADOQuery;
    DsDM_KM: TDataSource;
    CmdRefresh: TAction;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    N2: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    ingnhnhm1: TMenuItem;
    Xl1: TMenuItem;
    ToolButton12: TToolButton;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD3: TisPanel;
    Label13: TLabel;
    LbTLLAI1: TLabel;
    LbGIALE: TLabel;
    LbGIASI: TLabel;
    EdGiavon: TwwDBEdit;
    EdTL_LAI: TwwDBEdit;
    EdGIABANLE: TwwDBEdit;
    EdGIASI: TwwDBEdit;
    PD1: TisPanel;
    Label14: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label12: TLabel;
    LbDVT1: TLabel;
    LbDVT2: TLabel;
    EdMA: TwwDBEdit;
    EdTen: TwwDBEdit;
    DBEdit11: TwwDBEdit;
    EdQD1: TwwDBEdit;
    PaKhuyenmai: TisPanel;
    TntLabel1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    TntLabel2: TLabel;
    CbDenNgay: TwwDBDateTimePicker;
    TntLabel3: TLabel;
    EdCK: TwwDBEdit;
    CbNhomhang: TfcTreeCombo;
    Label26: TLabel;
    CmdSwitch: TAction;
    LbTLLAI2: TLabel;
    N3: TMenuItem;
    TntLabel4: TLabel;
    wwDBEdit1: TwwDBDateTimePicker;
    TntLabel5: TLabel;
    wwDBEdit2: TwwDBDateTimePicker;
    TntLabel6: TLabel;
    CbKho: TwwDBLookupCombo;
    wwDBLookupCombo2: TwwDBLookupCombo;
    QrDM_KMMAKHO: TWideStringField;
    QrDM_KMTUNGAY: TDateTimeField;
    QrDM_KMDENNGAY: TDateTimeField;
    QrDM_KMTUGIO: TDateTimeField;
    QrDM_KMDENGIO: TDateTimeField;
    QrDM_KMTL_CK: TFloatField;
    QrDM_KMUPDATE_DATE: TDateTimeField;
    QrDMVT: TADOQuery;
    QrDM_KMMAVT: TWideStringField;
    QrDM_KMLK_TL_LAI: TFloatField;
    QrDM_KMLK_TENVT: TWideStringField;
    QrDM_KMLK_TENTAT: TWideStringField;
    QrDM_KMLK_DVT: TWideStringField;
    QrDM_KMLK_QD1: TIntegerField;
    QrDM_KMLK_DVT1: TWideStringField;
    QrDM_KMLK_GIANHAP: TFloatField;
    QrDM_KMLK_GIASI: TFloatField;
    QrDM_KMLK_GIABAN: TFloatField;
    QrDM_KMLK_MAMAU: TWideStringField;
    QrDM_KMLK_MASIZE: TWideStringField;
    QrDM_KMLK_TENMAU: TWideStringField;
    QrDM_KMLK_TENSIZE: TWideStringField;
    EdDVT: TwwDBEdit;
    EdDVT1: TwwDBEdit;
    wwDBEdit5: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    Filter: TwwFilterDialog2;
    CmdDelKhuyenMai: TAction;
    CmdUpdate: TAction;
    Xakhuynmihthn1: TMenuItem;
    TntLabel7: TLabel;
    CbLCT: TwwDBComboBox;
    KHUYENMAI_DEL: TADOCommand;
    KHUYENMAI_UPD: TADOCommand;
    QrDM_KMHTKM: TWideStringField;
    QrDM_KMLK_TENNGANH: TWideStringField;
    QrDM_KMLK_TENNHOM: TWideStringField;
    QrDM_KMLK_MANGANH: TWideStringField;
    QrDM_KMLK_MANHOM: TWideStringField;
    Label4: TLabel;
    Label7: TLabel;
    wwDBEdit3: TwwDBEdit;
    wwDBEdit4: TwwDBEdit;
    wwDBEdit7: TwwDBEdit;
    wwDBEdit8: TwwDBEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDM_KMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KMAfterInsert(DataSet: TDataSet);
    procedure QrDM_KMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDM_KMBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrDM_KMMAVTChange(Sender: TField);
    procedure CmdDelKhuyenMaiExecute(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure CbKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
  private
  	mCanEdit: Boolean;
    mGia: Double;
  	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4: String;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmKhuyenmai: TFrmDmKhuyenmai;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, RepEngine, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_KHUYENMAI';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.FormCreate(Sender: TObject);
var
	b, bLe, bSi: Boolean;
    x: Integer;
begin
    TMyForm(Self).Init2;

    SetDisplayFormat(QrDM_KM, sysCurFmt);
    SetDisplayFormat(QrDM_KM, ['LK_TL_LAI', 'TL_CK'], sysPerFmt);
    SetShortDateFormat(QrDM_KM);
    SetDisplayFormat(QrDM_KM, ['TUGIO', 'DENGIO'], 'HH:mm');

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDM_KM, FORM_CODE, Filter);

    // Don vi tinh quy doi
	b := FlexConfigInteger('DM_HH', 'Unit Level') <> 0;
    LbDVT1.Visible := b;
    EdQD1.Visible := b;
    LbDVT2.Visible := b;
    EdDVT1.Visible := b;

    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDM_KM.SQL.Text;

    // Tree Combo
    FlexGroupCombo(CbNhomhang);

    // Panels
    PaKhuyenmai.Collapsed := RegReadBool('PaKhuyenmai');
    PD3.Collapsed := RegReadBool('PD3');

    // Relocate
    bLe := FlexConfigBool('DM_HH', 'Retail');
    bSi := FlexConfigBool('DM_HH', 'Wholesale');
    if bLe and not bSi then
    begin
        x := (EdTL_LAI.Left + EdTL_LAI.Width) - (EdGIASI.Left + EdGIASI.Width);
        LbTLLAI1.Left := LbTLLAI1.Left - x;	LbTLLAI1.Top := LbGIASI.Top;
        LbTLLAI2.Left := LbTLLAI2.Left - x; LbTLLAI2.Top := LbGIASI.Top;
        EdTL_LAI.Left := EdTL_LAI.Left - x; EdTL_LAI.Top := EdGIASI.Top;
    end;

    // Gia ban le
    LbTLLAI1.Visible 	:= bLe;
    LbTLLAI2.Visible 	:= bLe;
    EdTL_LAI.Visible 	:= bLe;
    LbGIALE.Visible 	:= ble;
    EdGIABANLE.Visible 	:= bLe;

    // Gia ban si
    LbGIASI.Visible := bSi;
    EdGIASI.Visible := bSi;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.FormShow(Sender: TObject);
begin
	DsDM_KM.AutoEdit := mCanEdit;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    with DataMain do
    begin
    	OpenDataSets([QrMAU, QrSIZE, QrDMKHO, QrDM_NGANH, QrDM_NHOM]);
    end;
    OpenDataSets([QrDMVT]);
    SetDisplayFormat(QrDMVT, sysCurFmt);
    SetDisplayFormat(QrDMVT,['TL_LAI'], sysPerFmt);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataset(QrDM_KM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
    
    // Saving & closing
    CloseDataSets(DataMain.Conn);
    RegWrite(Name, ['PaKhuyenmai', 'PD3'],
    	[PaKhuyenmai.Collapsed, PD3.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdNewExecute(Sender: TObject);
begin
	QrDM_KM.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdSaveExecute(Sender: TObject);
begin
	QrDM_KM.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdCancelExecute(Sender: TObject);
begin
	QrDM_KM.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdDelExecute(Sender: TObject);
begin
	QrDM_KM.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdPrintExecute(Sender: TObject);
begin
   	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM_KM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec: Boolean;
begin
	with QrDM_KM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
	end;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;

    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdUpdate.Enabled := bBrowse and mCanEdit;
    CmdDelKhuyenMai.Enabled := bBrowse and mCanEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.QrDM_KMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) then
	begin
        mNganh  := sNganh;
        mNhom   := sNhom;
        mNhom2  := sNhom2;
        mNhom3  := sNhom3;
        mNhom4  := sNhom4;

        Wait(DATAREADING);
        with QrDM_KM do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(
                	' where MAVT in (select MAVT from DM_VT_FULL where MANGANH=''%s'')',
                	[mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(
                	' where MAVT in (select MAVT from DM_VT_FULL where MANGANH=''%s'' and MANHOM=''%s'')',
                    [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' where MAVT in (select MAVT from DM_VT_FULL where MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'')',
                    [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' where MAVT in (select MAVT from DM_VT_FULL where MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'')',
                    [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' where MAVT in (select MAVT from DM_VT_FULL where MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s'')',
                    [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            SQL.Text := sSQL + ' Order By UPDATE_DATE';
            Open;
        end;
        ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.QrDM_KMAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
	d := Now;
	with QrDM_KM do
    begin
    	FieldByName('HTKM').AsString 	  := 'K';
        FieldByName('TUNGAY').AsDateTime  := Date;
        FieldByName('DENNGAY').AsDateTime := Date;
        FieldByName('TUGIO').AsDateTime   := EncodeTime(0,0,0,0);
        FieldByName('DENGIO').AsDateTime  := 1 + EncodeTime(0,0,0,0);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)    
procedure TFrmDmKhuyenmai.QrDM_KMBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MAVT', 'MAKHO', 'TUNGAY', 'DENNGAY', 'HTKM', 'TL_CK']) then
			Abort;

        FieldByName('UPDATE_DATE').AsDateTime := Now;
	end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.QrDM_KMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM_KM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.QrDM_KMMAVTChange(Sender: TField);
var 
	mCond: String;
begin
    if CbNhomhang.Text <> '' then
        // Nganh, Nhom
        case CbNhomhang.SelectedNode.Level of
        0:  // Nganh
            mCond := Format('MANGANH=''%s''', [mNganh]);
        1:  // Nhom
            mCond := Format('MANGANH=''%s'' and MANHOM=''%s''',
                [mNganh, mNhom]);
        2:  // Nhom 2
            mCond := Format('MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''',
                [mNganh, mNhom, mNhom2]);
        3:  // Nhom 3
            mCond := Format('MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                [mNganh, mNhom, mNhom2, mNhom3]);
        4:  // Nhom 4
            mCond := Format(
                'MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
        end;

	exDotMavt(1, QrDMVT ,Sender, mCond);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_DEL_KHUYENMAI = 'Xóa danh sách mặt hàng hết hạn khuyến mãi. Tiếp tục?';
    RS_UPD_KHUYENMAI = 'Cập nhật danh sách mặt hàng khuyến mãi. Tiếp tục?';

procedure TFrmDmKhuyenmai.CmdDelKhuyenMaiExecute(Sender: TObject);
begin
	if not YesNo(RS_DEL_KHUYENMAI) then
    	Exit;

	with KHUYENMAI_DEL do
    begin
    	Prepared := True;
        Execute;
        if Parameters[0].Value = 0 then
        begin
        	CmdReRead.Execute;
        	MsgDone;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CmdUpdateExecute(Sender: TObject);
begin
	if not YesNo(RS_UPD_KHUYENMAI) then
    	Exit;

    with KHUYENMAI_UPD do
    begin
    	Prepared := True;
        Execute;
        if Parameters[0].Value = 0 then
        begin
        	CmdReRead.Execute;
        	MsgDone;
        end;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhuyenmai.CbKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
