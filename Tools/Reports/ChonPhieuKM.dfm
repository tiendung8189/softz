object FrmChonPhieuKM: TFrmChonPhieuKM
  Left = 176
  Top = 114
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n Phi'#7871'u'
  ClientHeight = 386
  ClientWidth = 767
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label65: TLabel
      Left = 144
      Top = 15
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 444
      Top = 15
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object EdTungay: TwwDBDateTimePicker
      Left = 200
      Top = 10
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
      OnExit = CbMaDVExit
    end
    object EdDenngay: TwwDBDateTimePicker
      Left = 508
      Top = 10
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 1
      OnExit = CbMaDVExit
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 49
    Width = 767
    Height = 316
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'12'#9'Ng'#224'y l'#7853'p phi'#7871'u'#9'F'
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
      'TUNGAY'#9'12'#9'T'#7915' ng'#224'y'#9'F'
      'DENNGAY'#9'12'#9#272#7871'n ng'#224'y'#9'F'
      'TL_CK'#9'6'#9'%C.K'#9'F'
      'LYDO'#9'40'#9'L'#253' do'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsPHIEUNX
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object Status: TStatusBar
    Left = 0
    Top = 365
    Width = 767
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object DsPHIEUNX: TDataSource
    DataSet = QrPHIEUNX
    Left = 172
    Top = 196
  end
  object QrPHIEUNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrPHIEUNXBeforeOpen
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'KHUYENMAI'
      ' where  '#9'LCT in ('#39'C'#39', '#39'K'#39')'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      ' order by NGAY, SCT')
    Left = 144
    Top = 196
    object QrPHIEUNXSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrPHIEUNXTUNGAY: TDateTimeField
      FieldName = 'TUNGAY'
    end
    object QrPHIEUNXDENNGAY: TDateTimeField
      FieldName = 'DENNGAY'
    end
    object QrPHIEUNXTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrPHIEUNXLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrPHIEUNXNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrPHIEUNXLCT: TWideStringField
      FieldName = 'LCT'
      Size = 2
    end
    object QrPHIEUNXDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrPHIEUNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdChose: TAction
      Caption = 'CmdChose'
    end
    object CmdClose: TAction
      Caption = 'CmdClose'
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin    '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 172
    Top = 224
    object Tm1: TMenuItem
      Action = CmdSearch
    end
  end
end
