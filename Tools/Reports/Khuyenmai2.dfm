object FrmKhuyenmai2: TFrmKhuyenmai2
  Left = 270
  Top = 195
  BorderIcons = [biSystemMenu]
  Caption = 'Danh M'#7909'c Kho H'#224'ng - '#208'i'#7875'm B'#225'n H'#224'ng Khuy'#7871'n M'#227'i'
  ClientHeight = 305
  ClientWidth = 537
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 537
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 57
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdUpdate2
      ImageIndex = 9
    end
    object ToolButton2: TToolButton
      Left = 57
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 65
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object TntToolButton1: TToolButton
      Left = 122
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 179
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 187
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 284
    Width = 537
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 537
    Height = 248
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'MARK;CheckBox;True;False')
    Selected.Strings = (
      'MAKHO'#9'10'#9'M'#227' kho'#9'F'
      'LK_TENKHO'#9'50'#9'T'#234'n kho'#9'F'
      'MARK'#9'10'#9'Ch'#7885'n'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = DsKHUYENMAI_CT2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    PopupMenu = popCheck
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdUpdate: TAction
      Caption = 'Danh s'#225'ch kho'
      Hint = 'C'#7853'p nh'#7853't danh s'#225'ch kho'
      OnExecute = CmdUpdateExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdCheckAll: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n t'#7845't c'#7843
      OnExecute = CmdCheckAllExecute
    end
    object CmdUnCheckAll: TAction
      Category = 'POPUP'
      Caption = 'B'#7887' ch'#7885'n'
      OnExecute = CmdUnCheckAllExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdUpdate2: TAction
      Caption = 'C'#7853'p nh'#7853't'
      Visible = False
      OnExecute = CmdUpdate2Execute
    end
  end
  object QrKHUYENMAI_CT2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrKHUYENMAI_CT2BeforeOpen
    BeforeEdit = QrKHUYENMAI_CT2BeforeEdit
    AfterPost = QrKHUYENMAI_CT2AfterPost
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from  KHUYENMAI_MAKHO'
      ' where  KHOA = :KHOA'
      'order by MAKHO')
    Left = 148
    Top = 196
    object QrKHUYENMAI_CT2MAKHO: TWideStringField
      DisplayLabel = 'M'#183' kho'
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrKHUYENMAI_CT2LK_TENKHO: TWideStringField
      DisplayLabel = 'T'#170'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 100
      Lookup = True
    end
    object QrKHUYENMAI_CT2MARK: TBooleanField
      DisplayLabel = 'Asp d'#244'ng'
      FieldName = 'MARK'
    end
    object QrKHUYENMAI_CT2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsKHUYENMAI_CT2: TDataSource
    DataSet = QrKHUYENMAI_CT2
    Left = 148
    Top = 224
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 168
  end
  object Filter: TwwFilterDialog2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 168
  end
  object popCheck: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 244
    Top = 168
    object Tm1: TMenuItem
      Action = CmdCheckAll
    end
    object Bchn1: TMenuItem
      Action = CmdUnCheckAll
    end
    object Bchn2: TMenuItem
      Caption = '-'
    end
    object Danhschkho1: TMenuItem
      Action = CmdUpdate
    end
  end
  object DMVT_KHUYENMAI: TADOCommand
    CommandText = 'DMVT_KHUYENMAI;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    Left = 348
    Top = 112
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from V_DM_KHO'
      'order by MAKHO')
    Left = 192
    Top = 80
  end
end
