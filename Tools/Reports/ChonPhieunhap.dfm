object FrmChonPhieunhap: TFrmChonPhieunhap
  Left = 149
  Top = 197
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n Phi'#7871'u Nh'#7853'p'
  ClientHeight = 386
  ClientWidth = 1038
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 1038
    Height = 105
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbNHAPCUA: TLabel
      Left = 114
      Top = 17
      Width = 77
      Height = 16
      Alignment = taRightJustify
      Caption = 'Nh'#224' cung c'#7845'p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label31: TLabel
      Left = 170
      Top = 45
      Width = 21
      Height = 16
      Alignment = taRightJustify
      Caption = 'Kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label65: TLabel
      Left = 144
      Top = 74
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 444
      Top = 74
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object CbNCC: TwwDBLookupCombo
      Tag = 1
      Left = 324
      Top = 12
      Width = 285
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENDT'#9'40'#9'T'#234'n'#9#9
        'MADT'#9'12'#9'M'#227#9#9)
      LookupTable = QrDMNCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
ButtonEffects.Transparent=True
    end
    object CbKHO: TwwDBLookupCombo
      Tag = 3
      Left = 260
      Top = 40
      Width = 349
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENKHO'#9'40'#9'T'#234'n'#9'F'
        'MAKHO'#9'6'#9'M'#227#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
ButtonEffects.Transparent=True
    end
    object CbMADT: TwwDBLookupCombo
      Left = 200
      Top = 12
      Width = 121
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MADT'#9'12'#9'M'#227#9'F'
        'TENDT'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDMNCC
      LookupField = 'MADT'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
      OnNotInList = CbMADTNotInList
ButtonEffects.Transparent=True
    end
    object CbMAKHO: TwwDBLookupCombo
      Tag = 2
      Left = 200
      Top = 40
      Width = 57
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'6'#9'M'#227#9'F'
        'TENKHO'#9'40'#9'T'#234'n'#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMADTChange
      OnExit = CbMADTExit
      OnNotInList = CbMADTNotInList
ButtonEffects.Transparent=True
    end
    object EdTungay: TwwDBDateTimePicker
      Left = 200
      Top = 69
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 4
      OnExit = CbMADTExit
    end
    object EdDenngay: TwwDBDateTimePicker
      Left = 508
      Top = 69
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 5
      OnExit = CbMADTExit
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 105
    Width = 1038
    Height = 260
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'12'#9'Ng'#224'y'#9'F'#9'Ch'#7915'ng t'#7915
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7915'ng t'#7915
      'HOADON_SO'#9'10'#9'S'#7889#9'F'#9'H'#243'a '#273#417'n'
      'HOADON_NGAY'#9'12'#9'Ng'#224'y'#9'F'#9'H'#243'a '#273#417'n'
      'MADT'#9'15'#9'M'#227#9'F'#9'Nh'#224' cung c'#7845'p'
      'TENDT'#9'25'#9'T'#234'n'#9'F'#9'Nh'#224' cung c'#7845'p'
      'MAKHO'#9'7'#9'M'#227#9'F'#9'Kho'
      'TENKHO'#9'23'#9'T'#234'n'#9'F'#9'Kho'
      'HAN_TTOAN'#9'15'#9'H'#7841'n thanh to'#225'n'#9'F'
      'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsPHIEUNHAP
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object Status: TStatusBar
    Left = 0
    Top = 365
    Width = 1038
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object DsPHIEUNHAP: TDataSource
    DataSet = QrPHIEUNHAP
    Left = 172
    Top = 196
  end
  object QrPHIEUNHAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrPHIEUNHAPBeforeOpen
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'a.*, b.TENKHO, c.TENDT'
      '  from'#9'T_CHUNGTU a, DM_KHO b, DM_NCC c'
      ' where'#9'a.MAKHO = b.MAKHO'
      '   and'#9'a.MADT = c.MADT'
      '   and '#9'a.LCT = '#39'NMUA'#39
      '   and'#9'a.NGAY >= :NGAYD'
      '   and'#9'a.NGAY <  :NGAYC + 1')
    Left = 144
    Top = 196
    object QrPHIEUNHAPSCT: TWideStringField
      DisplayLabel = 'S'#232' phi'#213'u'
      FieldName = 'SCT'
    end
    object QrPHIEUNHAPNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
    end
    object QrPHIEUNHAPTENDT: TWideStringField
      DisplayLabel = 'T'#234'n nh'#224' cung c'#7845'p'
      FieldName = 'TENDT'
      Size = 100
    end
    object QrPHIEUNHAPTENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldName = 'TENKHO'
      Size = 100
    end
    object QrPHIEUNHAPDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrPHIEUNHAPMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      FieldName = 'MADT'
      Size = 15
    end
    object QrPHIEUNHAPMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrPHIEUNHAPNG_GIAO: TWideStringField
      FieldName = 'NG_GIAO'
      Size = 30
    end
    object QrPHIEUNHAPNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Size = 30
    end
    object QrPHIEUNHAPTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
    end
    object QrPHIEUNHAPHAN_TTOAN: TIntegerField
      DisplayLabel = 'H'#7841'n thanh to'#225'n'
      FieldName = 'HAN_TTOAN'
    end
    object QrPHIEUNHAPHOADON_SO: TWideStringField
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrPHIEUNHAPHOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
    object QrPHIEUNHAPSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrPHIEUNHAPTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrPHIEUNHAPPHIEUGIAOHANG: TWideStringField
      FieldName = 'PHIEUGIAOHANG'
      Size = 30
    end
    object QrPHIEUNHAPKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdChose: TAction
      Caption = 'CmdChose'
    end
    object CmdClose: TAction
      Caption = 'CmdClose'
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin    '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 172
    Top = 224
    object Tm1: TMenuItem
      Action = CmdSearch
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO'
      '  from DM_KHO'
      'order by MAKHO')
    Left = 234
    Top = 196
  end
  object QrDMNCC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MADT, TENDT'
      '  from DM_NCC'
      'order by TENDT')
    Left = 206
    Top = 196
  end
end
