﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TheodoiGia;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  ActnList, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmTheodoiGia = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    ActionList1: TActionList;
    CmdClose: TAction;
    LbDONVI: TLabel;
    Label31: TLabel;
    CbDV: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMaDV: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    CHUNGTU_CT_REF: TADOStoredProc;
    CHUNGTU_CT: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVChange(Sender: TObject);
    procedure CbMaDVExit(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
  private
	mMADT, mKHO, mLCT, mMABH: String;
    mTungay, mDenngay: TDateTime;
    mLoai: Integer; //0: Khach hang; 1: NCC
  public
  	function Execute(pLCT, pMABH: String; pMADT, pKho: String;
    	pLoai: Integer): TGUID;
  end;

var
  FrmTheodoiGia: TFrmTheodoiGia;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTheodoiGia.Execute;
begin
    if pMABH = '' then
    begin
        Result := TGuidEx.EmptyGuid;
        Close;
        Exit;
    end;

	mMADT := pMADT;
    mKho := pKho;
    mLCT := pLCT;
    mLoai := pLoai;
    mMABH := pMABH;

    if pLoai = 1 then
    begin
        LbDONVI.Caption := 'Nhà cung cấp';
    	Caption := 'Tham Khảo Giá Nhập - ' + pMABH;
        SetCustomGrid('THAMKHAO_GIA_NHAP' , GrBrowse);
    end
    else
    begin
        LbDONVI.Caption := 'Khách hàng';
    	Caption := 'Tham Khảo Giá Xuất - ' + pMABH;
        SetCustomGrid('THAMKHAO_GIA_XUAT' , GrBrowse);
    end;

    if ShowModal = mrOK then
		Result := TGuidField(CHUNGTU_CT_REF.FieldByName('KHOACT')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

    CloseDataSets([CHUNGTU_CT_REF, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrDM_KH_NCC, QrDMKHO]);
    CbMaDV.LookupValue := mMADT;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mMADT = '' then
    	try
    		CbMaDV.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mMADT := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.GrBrowseDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CmdRefreshExecute(Sender: TObject);
var
	donvi, Kho: String;
    Tungay, Denngay: TDateTime;
begin
	if CbMaDV.DisplayValue = '' then
    	donvi := ''
    else
    	donvi   := CbMaDV.LookupValue;
    Kho     := CbMAKHO.LookupValue;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if	(mTungay <> Tungay) or
    	(mDenngay <> Denngay) or
       (mMADT <> donvi) or
       (mKHO <> Kho) then
	begin
   	    mMADT := donvi;
        mKHO := Kho;
        mTungay := Tungay;
        mDenngay := Denngay;

        with CHUNGTU_CT_REF do
        begin
			Close;
            Prepared := True;
            Parameters[1].Value := mMABH;
            Parameters[2].Value := mLCT;
            Parameters[3].Value := mTungay;
            Parameters[4].Value := mDenngay;
            Parameters[5].Value := mMADT;
            Parameters[6].Value := mKHO;
            Parameters[7].Value := '';
            Open;
        end;
        SetShortDateFormat(CHUNGTU_CT_REF);
        SetDisplayFormat(CHUNGTU_CT_REF, sysCurFmt);
        SetDisplayFormat(CHUNGTU_CT_REF, ['TL_CK', 'CK_HD'], sysPerFmt);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CbMaDVChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbDV.LookupValue := s;
    // Ten NCC
    1:
        CbMaDV.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CbMaDVExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
begin
	QrDM_KH_NCC.Parameters[0].Value := mLoai;
end;

end.
