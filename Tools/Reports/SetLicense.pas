﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SetLicense;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, wwdblook, Wwdbdlg, wwidlg;

type
  TFrmSetLicense = class(TForm)
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    EdLic: TEdit;
    LbText: TLabel;
    procedure CmdReturnKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
  public
    function Execute(var pLic: String): Boolean;
  end;

var
  FrmSetLicense: TFrmSetLicense;

implementation

{$R *.DFM}

uses
	isDb, isLib, SysUtils, ExCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmSetLicense.Execute;

begin
    if ShowModal = mrOK then
    begin
    	pLic := EdLic.Text;
    	Result := True;
	end
    else
    	Result := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetLicense.CmdReturnClick(Sender: TObject);
var
    s: String;
begin
    s := EdLic.Text;
    if (Length(s) <> 35) or not IsLicense(s) then
    begin
        LbText.Caption := 'Giấy phép không đúng, vui lòng liên hệ nhà cung cấp';
        Abort;
    end;

    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetLicense.CmdReturnKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetLicense.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
