(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Trangthai;

interface

uses
  Windows, SysUtils, Variants, Classes, Controls, Forms,
  wwdbedit, Wwdbcomb, Db, StdCtrls, Mask, Wwdotdot;

type
  TFrmDrcStatus = class(TForm)
    CbTrangthai: TwwDBComboBox;
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  	procedure Execute(ds: TDataSource);
  end;

var
  FrmDrcStatus: TFrmDrcStatus;

implementation

uses
	isLib;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDrcStatus.Execute(ds: TDataSource);
begin
    if Self.Tag = 2  then
        Exit;

	CbTrangthai.DataSource := ds;
    Show;
    SetActiveWindow(Self.ParentWindow);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDrcStatus.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	RegWrite(Name, ['Left', 'Top'], [Left, Top]);
    Action := caFree;
	FrmDrcStatus := Nil;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDrcStatus.FormCreate(Sender: TObject);
begin
    if not FileExists('Drc.exe') then
        Self.Tag := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDrcStatus.FormShow(Sender: TObject);
begin
    Left := RegRead(Name, 'Left', Left);
    Top := RegRead(Name, 'Top', Top);
end;

initialization
	FrmDrcStatus := Nil;
end.
