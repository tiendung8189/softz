﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TonkhoKiemke;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  ToolWin, Db, ADODB, wwdblook, Menus, Wwfltdlg2, wwDialog, wwidlg, AppEvnts,
  AdvMenus, wwfltdlg, Wwdbgrid;

type
  TFrmTonkhoDK = class(TForm)
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    Panel1: TPanel;
    Status: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    QrTONKHO: TADOQuery;
    DsTONKHO: TDataSource;
    QrDMVT: TADOQuery;
    QrTONKHOKY: TIntegerField;
    QrTONKHONAM: TIntegerField;
    QrTONKHOMAVT: TWideStringField;
    QrTONKHOTENVT: TWideStringField;
    QrTONKHODVT: TWideStringField;
    QrTONKHOTENNHOM: TWideStringField;
    CmdRefresh: TAction;
    Label3: TLabel;
    QrDMVTMAVT: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTTENNHOM: TWideStringField;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    CbMA: TwwDBLookupCombo;
    CbTEN: TwwDBLookupCombo;
    EdKy: TComboBox;
    EdNam: TComboBox;
    QrDMVTMANHOM: TWideStringField;
    QrTONKHOMANHOM: TWideStringField;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    QrTONKHOMAKHO: TWideStringField;
    QrTONKHOCREATE_DATE: TDateTimeField;
    QrTONKHOUPDATE_DATE: TDateTimeField;
    QrTONKHOCREATE_BY: TIntegerField;
    QrTONKHOUPDATE_BY: TIntegerField;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    QrTONKHOLG_DAU: TFloatField;
    QrTONKHOTIEN_DAU: TFloatField;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrTONKHODG_DAU: TFloatField;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTGIANHAPVAT: TFloatField;
    QrTONKHOGIANHAP: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    N1: TMenuItem;
    ImporttExcel1: TMenuItem;
    QrIM: TADOQuery;
    QrDMVTMADT: TWideStringField;
    QrDMVTTENTAT: TWideStringField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTGIASI: TFloatField;
    QrDMVTGIABQ: TFloatField;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTTH_SUAT: TFloatField;
    QrDMVTTH_SUAT_DAU_VAO: TFloatField;
    QrDMVTTENLT: TWideStringField;
    CmdSum: TAction;
    SUM_KIEMKE: TADOCommand;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    QrTONKHOKHOA: TAutoIncField;
    TntToolButton3: TToolButton;
    TntToolButton4: TToolButton;
    QrTONKHORSTT: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrTONKHOBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrTONKHOBeforePost(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrTONKHOPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrTONKHOBeforeDelete(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CbMAChange(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTONKHOLG_DAUChange(Sender: TField);
    procedure QrTONKHOTIEN_DAUChange(Sender: TField);
    procedure QrTONKHOBeforeEdit(DataSet: TDataSet);
    procedure CmdSumExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrTONKHOCalcFields(DataSet: TDataSet);
  private
  	mCanEdit, mBookClosed: Boolean;
  	mKy, mNam: Integer;
    mKho: String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmTonkhoDK: TFrmTonkhoDK;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, wwMemo,
    isType, isLib, wwStr, ShellApi, isFile, DateUtils;

{$R *.DFM}

const
	FORM_CODE = 'TONKHO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.Execute;
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

	vlMonthList(EdKy);
    vlYearList(EdNam);
  	mTrigger := False;

    SetDisplayFormat(QrTONKHO, sysCurFmt);
    SetDisplayFormat(QrTONKHO, ['LG_DAU'], sysQtyFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrTONKHO,FORM_CODE, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOBeforeOpen(DataSet: TDataSet);
begin
    with QrTONKHO do
    begin
        Parameters[0].Value := mKy;
        Parameters[1].Value := mNam;
        Parameters[2].Value := mKho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DATA_ARISE = 'Có dữ liệu tồn kho trước tháng bắt đầu sử dụng chương trình.' +
            	    #13 + 'Xin liên hệ với người quản trị hệ thống để hiệu chỉnh.';

procedure TFrmTonkhoDK.FormShow(Sender: TObject);
begin
	// Open database
    OpenDataSets([QrDMVT, DataMain.QrDMKHO]);
	CbMA.LookupValue := sysDefKho;

    // Start to use checking
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;

		SQL.Text := Format('select 1 from TONKHO where KY+12*NAM<%d',
        	[sysBegMon + 12 * sysBegYear]);
        Open;

        if not IsEmpty then
            Msg(RS_DATA_ARISE);

        Close;
        Free;
    end;
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOBeforePost(DataSet: TDataSet);
begin
	if BlankConfirm(QrTONKHO, ['MAVT'], [nil]) then
    	Abort;

	with DataSet do
    begin
    	FieldByName('KY').AsInteger := mKy;
    	FieldByName('NAM').AsInteger := mNam;
    	FieldByName('MAKHO').AsString := mKho;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_MONTH = 'Tháng làm việc không hợp lệ. Tháng bắt đầu sử dụng chương trình là %d/%d.';

procedure TFrmTonkhoDK.CmdRefreshExecute(Sender: TObject);
var
    s: String;
	k, n: Integer;
    b: Boolean;
    mDate: TDateTime;
begin
    k := StrToInt(EdKy.Text);
    n := StrToInt(EdNam.Text);

	if (mKy <> k) or (mNam <> n) or (CbMA.LookupValue <> mKho) then
    begin
    	mKy := k;
        mNam := n;
        mKho := CbMA.LookupValue;

    	with QrTONKHO do
        begin
        	s := Sort;
        	Close;
            Open;
            if s <> '' then
            	Sort := s;
        end;

        // Check valid month
        b := mKy + 12 * mNam >= sysBegMon + 12 * sysBegYear;

        if not b then
            Msg(Format(RS_INVALID_MONTH, [sysBegMon, sysBegYear]));
            
        // Check BookClose.
		mDate := EncodeDate(mNam, mKy, 1);
    	mBookClosed := mDate <= sysCloseHH;

		DsTONKHO.AutoEdit := b;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOBeforeDelete(DataSet: TDataSet);
begin
	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdSaveExecute(Sender: TObject);
begin
	QrTONKHO.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdCancelExecute(Sender: TObject);
begin
	QrTONKHO.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrTONKHO do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not QrTONKHO.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsTONKHO);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CbMAChange(Sender: TObject);
var
	s : String;
begin
	if mTrigger then
    	Exit;

    mTrigger := True;

	if (Sender as TwwDbLookupCombo).Text = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    0:		// Ma
	    CbTEN.LookupValue := s;
    1:		// Ten
	    CbMA.LookupValue := s;
    end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrTONKHO, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOLG_DAUChange(Sender: TField);
begin
  	if mTrigger then
    	Exit;
  	mTrigger := True;

	with QrTONKHO do
    	FieldByName('TIEN_DAU').AsFloat :=
	    	FieldByName('DG_DAU').AsFloat *
    		FieldByName('LG_DAU').AsFloat;

  	mTrigger := False;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOTIEN_DAUChange(Sender: TField);
begin
  	if mTrigger then
    	Exit;
  	mTrigger := True;

	with QrTONKHO do
		if FieldByName('LG_DAU').AsFloat = 0.0 then
        else
	    	FieldByName('DG_DAU').AsFloat :=
		    	FieldByName('TIEN_DAU').AsFloat /
    			FieldByName('LG_DAU').AsFloat;

  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdReReadExecute(Sender: TObject);
begin
	mKy := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Tổng hợp số tồn kho đầu kỳ tháng %d/%d cho "%s"'#13' từ dữ liệu kiểm kê thực tế. Tiếp tục?';
    RS_ALL_WAREHOUSE = 'Tất cả kho';

procedure TFrmTonkhoDK.CmdSumExecute(Sender: TObject);
begin
    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Exit;
    end;

    // Make sure the params are correct
    CmdRefresh.Execute;

    // Confirm
	if not YesNo(Format(RS_CONFIRM, [mKy, mNam, Iif(mKho='', RS_ALL_WAREHOUSE, CbTen.DisplayValue)]), 1) then
    	Exit;

    // Sum
    with SUM_KIEMKE do
    begin
    	Prepared := True;
        Parameters[1].Value := mKho;
        Parameters[2].Value := mKy;
        Parameters[3].Value := mNam;
        Execute;
    end;
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTONKHO);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [mKy, mNam, mKho]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkhoDK.QrTONKHOCalcFields(DataSet: TDataSet);
begin
   with QrTONKHO do
   	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

end.
