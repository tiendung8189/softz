﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FlexRep;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ActnList, ComCtrls, ExtCtrls, Grids, Wwdbigrd,
  Wwdbgrid2, Db, ADODB,
  Shellapi, Wwdbgrid, ExportDS,
  SME2OLE, isPanel, AppEvnts, crCommon, SME2Cell, ToolWin;

type
  TFrmFlexRep = class(TForm)
    ActionList1: TActionList;
    CmdRun: TAction;
    DsRep: TDataSource;
    CmdCalc: TAction;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    QrREP: TADOStoredProc;
    CmdReread: TAction;
    CmdClose: TAction;
    PaLeft: TPanel;
    Panel4: TPanel;
    Bevel4: TBevel;
    PgGroup: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    GrList: TwwDBGrid2;
    CmdSave: TAction;
    CmdFields: TAction;
    SMExportToExcel1: TSMExportToExcel;
    DsExcel: TDataSource;
    QrExcel: TADOStoredProc;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    PaRight: TisPanel;
    Bevel8: TBevel;
    PaParam: TPanel;
    Splitter1: TSplitter;
    PaExcel: TisPanel;
    Bevel3: TBevel;
    PaFirst: TPanel;
    ToolBar1: TToolBar;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton11: TToolButton;
    Bevel1: TBevel;
    GrExcel: TwwDBGrid2;
    CheckSP: TADOCommand;
    CmdShowRpt: TAction;
    procedure CmdRunExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure QrREPAfterScroll(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure PgGroupChange(Sender: TObject);
    procedure CmdCalcExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdFieldsExecute(Sender: TObject);
    procedure SMExportToExcel1Progress(Sender: TObject; CurValue,
      MaxValue: Integer; var Abort: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrExcelUpdateFooter(Sender: TObject);
    procedure GrListDblClick(Sender: TObject);
    procedure CmdRereadExecute(Sender: TObject);
    procedure CmdShowRptExecute(Sender: TObject);
  private
    mLastProc, mLastName, mCats: String;
    mGroup: Integer;
  	mStockControl, mMustCalc, mFixLogon: Boolean;
    mTotal: array of Variant;

    mFields: TStrings;			// List of criteria fields
    mFrames: array of TCrFrame;	// List of criteria frames

    procedure OpenReports;
    function  Proc(fs: String = ''): Boolean;

    function  IsStockControl: Boolean;
    procedure SetMustCalc(pValue: Boolean);
  public
  	function  Execute(group: Integer = 0): Boolean;
  end;

var
  FrmFlexRep: TFrmFlexRep;

implementation

{$R *.DFM}

uses
	MainData, isDb, ExCommon, isCommon, isStr, isMsg, Rights,
    isLib, isFile, FlexRep3, Variants, RepEngine,
    crKho, crDs_Kho, crTuden, crTon,crNgay, crQuy, crNam, crThoigian, crMANCC,
    crMAKH, crDS_NCC, crMAVIP, crLOAI_HD, crDS_KH, crCT_TH, crTOP, crMAVT,
    crSILE, crDS_NHOMVT, crMAHH, crNGANHNHOM, crTHANGNAM;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFlexRep.Execute;
var
	i, n, m: Integer;
begin
  	mTrigger := False;
    mFixLogon := False;
	mCats := FlexConfigString('Sams', 'Export Group' + Iif(group > 0, ' ' + IntToStr(group), ''));

    OpenReports;
    if QrREP.IsEmpty then
	begin
    	Result := False;
		CloseDataSets(DataMain.Conn);
    end
    else
    begin
    	Result := True;
        // List of criteria fields
        mFields := TStringList.Create;
        mFields.Text :=
            'KHO'#13 +
            'DS_KHO'#13 +
            'NGAY'#13 +
            'TUNGAY'#13 +
            'GIA'#13 +
            'THANGNAM'#13 +
            'NAM'#13 +
            'QUY'#13 +
            'THOIGIAN'#13 +
            'MADT'#13 +
            'MAKH'#13 +
            'DS_NCC'#13 +
            'DS_KH'#13 +
            'THUE'#13 +
            'SILE'#13+
            'CT_TH'#13 +
            'TOP10'#13 +
            'NHOMVT'#13 +
            'DS_NHOMVT'#13 +
            'MAVT'#13 +
            'MAVIP'#13 +
            'MAHH'#13;

        // Create criteria frames
        n := mFields.Count;

        SetLength(mFrames, n);
        mFrames[mFields.IndexOf('KHO')]     := TframeCRKho.Create(Self);
        mFrames[mFields.IndexOf('DS_KHO')]  := TframeDs_Kho.Create(Self);
        mFrames[mFields.IndexOf('NGAY')]    := TframeCrNgay.Create(Self);
        mFrames[mFields.IndexOf('TUNGAY')]  := TframeTuden.Create(Self);
        mFrames[mFields.IndexOf('GIA')]     := TframeTON.Create(Self);
        mFrames[mFields.IndexOf('THANGNAM')]:= TframeTHANGNAM.Create(Self);
        mFrames[mFields.IndexOf('NAM')]     := TframeNam.Create(Self);
        mFrames[mFields.IndexOf('QUY')]     := TframeQuy.Create(Self);
        mFrames[mFields.IndexOf('THOIGIAN')]:= TframeThoigian.Create(Self);
        mFrames[mFields.IndexOf('MANCC')]   := TframeMANCC.Create(Self);
        mFrames[mFields.IndexOf('MAKH')]    := TframeMAKH.Create(Self);
        mFrames[mFields.IndexOf('DS_NCC')]  := TframeDS_NCC.Create(Self);
        mFrames[mFields.IndexOf('DS_KH')]   := TframeDS_KH.Create(Self);
        mFrames[mFields.IndexOf('THUE')]    := TframeLOAI_HD.Create(Self);
        mFrames[mFields.IndexOf('SILE')]    := TframeSILE.Create(Self);
        mFrames[mFields.IndexOf('CT_TH')]   := TframeCT_TH.Create(Self);
        mFrames[mFields.IndexOf('TOP10')]   := TframeTOP.Create(Self);
        mFrames[mFields.IndexOf('NHOMVT')]  := TframeNGANHNHOM.Create(Self);
        mFrames[mFields.IndexOf('DS_NHOMVT')]:= TframeDS_NHOMVT.Create(Self);
        mFrames[mFields.IndexOf('MAVT')]    := TframeMAVT.Create(Self);
        mFrames[mFields.IndexOf('MAVIP')]    := TframeMAVIP.Create(Self);
        mFrames[mFields.IndexOf('MAHH')]    := TframeMAHH.Create(Self);

        n := Length(mFrames) - 1;
        m := PaParam.Width;
        for i := 0 to n do
            with mFrames[i] do
            begin
                Visible := False;	// Make it surely
                Parent := PaParam;
                Left := 0;
                Width := m;
                Anchors := Anchors + [akLeft, akRight];
            end;
        ShowModal;

        mFields.Free;
        SetLength(mFrames, 0);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.SetMustCalc(pValue: Boolean);
begin
    mMustCalc := pValue;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFlexRep.IsStockControl: Boolean;
begin
    Result := mStockControl;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	// Final
    RegWrite('Reports\Export Group ' + IntToStr(mGroup), ['Page', 'Id'],
    	[PgGroup.ActivePageIndex, QrRep.FieldByName('IDX').AsInteger]);

	// Done
	CloseDataSets(DataMain.Conn);
    SetLength(mTotal, 0);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.SMExportToExcel1Progress(Sender: TObject; CurValue,
  MaxValue: Integer; var Abort: Boolean);
begin
    SetProgress(CurValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2('excel');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.OpenReports;
begin
    with QrREP do
    begin
    	Close;
    	Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := mCats;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.FormShow(Sender: TObject);
var
	m, n, i: Integer;
    txt, ls: TStrings;
begin
    // Stock control
	mStockControl := rCanEdit(DataMain.GetRights('SZ_TONTT', False));

    // Show / hide groups
    txt := TStringList.Create;
    if sysEnglish then
	    txt.Text := isStrReplace(FlexConfigString('Sams', 'Report English Categories'), ';', #13)
    else
	    txt.Text := isStrReplace(FlexConfigString('Sams', 'Report Categories'), ';', #13);

    ls := TStringList.Create;
    if mCats <> '' then
		ls.Text := isStrReplace(mCats, ';', #13)
	else
        for i := 0 to txt.Count - 1 do
			ls.Add(txt.Names[i]);

    n := 0;
    with QrRep, PgGroup do
    begin
    	for i := 0 to ls.Count - 1 do
        begin
        	m := StrToInt(ls[i]);
		    if Locate('CAT', m, []) and (n < PageCount) then
            begin
	            Pages[n].Tag := m;
	            Pages[n].Caption := txt.Values[ls[i]];
                Inc(n);
            end;
        end;
        txt.Free;
	    ls.Free;

		while n < PageCount do
	    begin
		    Pages[n].TabVisible := False;
	    	Inc(n);
    	end;
    end;
    ClearWait;

    // Seek to the last called report
    m := RegRead('Reports\Export Group ' + IntToStr(mGroup) , 'Page', 0);
    n := RegRead('Id', -1);

    with PgGroup do
    begin
	    if PageCount <= m then
	    	m := 0;

		while (m < PageCount) and (not Pages[m].TabVisible) do
        	Inc(m);

		ActivePageIndex := m;
	end;
    PgGroup.OnChange(Nil);

    with QrRep do
    begin
    	Tag := 1;
    	if not Locate('IDX', n, []) then
        	First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.QrREPAfterScroll(DataSet: TDataSet);
var
    i, n1, n2, y: Integer;
begin
	if QrRep.Tag = 0 then
    	Exit;

	n1 := Length(mFrames) - 1;
    n2 := mFields.Count - 1;
    y := 0;
	with QrRep do
    begin
    	if FieldByName('SYS').AsBoolean then
        begin
	    	for i := 0 to n1 do
                mFrames[i].Visible := False;
        	Exit;
        end;

    	for i := 0 to n2 do
        	if i <= n1 then
                with mFrames[i] do
                begin
                    Visible := FieldByName(mFields[i]).AsBoolean;
                    if Visible then
                    begin
                        Top := y;
                        Inc(y, Height);
                    end;
                end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.GrExcelUpdateFooter(Sender: TObject);
var
	s: String;
    i, k: Integer;
    f: TField;
begin
	with GrExcel do
        for i := 0 to GetColCount - 1 do
        begin
        	s := Columns[i].FieldName;
            if s <> '' then
            begin
                f := QrExcel.FieldByName(s);
                if f.DataType in [ftFloat, ftCurrency, ftBCD] then
                begin
                    k := f.FieldNo - 1;
                    Columns[i].FooterValue := FormatFloat(sysCurFmt, mTotal[k]);
                end;
            end;
	    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if QrRep.FieldByName('SYS').AsBoolean then
    	with AFont do
        begin
            Style := [fsBold];
            if Highlight then
	        	Color := clWhite
            else
    	    	Color := clGreen;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.GrListDblClick(Sender: TObject);
begin
	CmdRun.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrExcel.Active then
		Status.SimpleText := RecordCount(QrExcel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFlexRep.Proc(fs: String = ''): Boolean;
var
	sRep, sCap, s , sParam: String;
    i, n: Integer;
    mVar: array of Variant;

    function GetNameDataType(DataType: TFieldType): string;
    begin
        case DataType of
            ftWideString,ftWideMemo,ftString,ftMemo:  Result := '(String)';
            ftFloat, ftCurrency, ftBCD:         Result := '(Float)';
            ftDate, ftDateTime :                Result := '(DateTime)';
            ftInteger, ftSmallint, ftShortint : Result := '(Integer)';
            ftBoolean :                         Result := '(Bit)'
        else
            Result := '(Unknown)'
        end;
    end;

    function BasicVariantType(const pVar: Variant): String;
    var
        basicType: Word;
    begin
         basicType := VarType(pVar) and VarTypeMask;
         // Set a string to match the type
        case basicType of
            varSmallInt, varInteger, varSingle, varInt64, varByte, varWord, varLongWord:
                Result := '(Integer)';
            varDouble, varCurrency:
                Result := '(Float)';
            varDate:
                Result := '(DateTime)';
            varString, varStrArg, varOleStr:
                Result := '(String)';
            varBoolean:
                Result := '(Bit)';
            varEmpty:
                Result := '(Empty)';
            varNull:
                Result := '(Null)';
            else
                Result := GetNameDataType(VarTypeToDataType(basicType));
        end;
    end;
begin
    Result := True;
    // Prepare
	with QrRep do
    begin
    	sRep := Trim(FieldByName('REP_NAME').AsString);
        if fs = '' then
	    	sCap := isProperCase(isStripToneMark(Trim(FieldByName('DESC').AsString)));

	    // Within month
        if FieldByName('TRONGTHANG').AsBoolean then
        begin
            if FieldByName('TUNGAY').AsBoolean then
                if not mFrames[mFields.IndexOf('TUNGAY')].Validate then
                    Abort;

            if FieldByName('THOIGIAN').AsBoolean then
                if not mFrames[mFields.IndexOf('THOIGIAN')].Validate then
                    Abort;
        end;
    end;

    if sRep = '' then
    begin
    	GrList.SetFocus;
        Result := False;
    	Exit;
    end;

    // Stock report
    if QrRep.FieldByName('GIA').AsBoolean then
    begin
		if mMustCalc then
    	    CmdCalc.Execute;
	end;

    // Get Criteria
	n := Length(mFrames) - 1;
    SetLength(mVar, 0);
	for i := 0 to n do
        with mFrames[i] do
        if Visible then
        begin
            SetLength(mVar, Length(mVar) + GetParamNo);
            GetCriteria(mVar);
        end;

    // Proc
    if sRep ='' then
    	GrList.SetFocus
    else
    begin
    	mLastProc := sRep;
		mLastName := sCap;
        PaExcel.HeaderCaption := ' :: ' + sCap;

        with CheckSP do
        begin
            Prepared := True;
            Parameters[0].Value := mLastProc;
            if Execute.RecordCount = 0 then
            begin
                s := Format('Lỗi cấu hình báo cáo "%s".', [mLastName]);
                s := s + #13 +'Store procedure '''+ mLastProc +'''';

                ErrMsg(s);
                Result := False;
                Exit;
            end;
        end;

        // Query data
        GrExcel.DataSource := Nil;
        with QrExcel do
        begin
        	Close;
            ProcedureName := mLastProc;
	        n := Length(mVar);

            try
                for i := 1 to n do
                    Parameters[i].Value := mVar[i - 1];

                ExecProc;
            except
                s := Format('Lỗi thực thi "%s".', [mLastName]);
                
                begin
                    for i := 0 to n - 1 do
                        sParam := sParam   + #13 + #9 + Format('%s <%s>',[BasicVariantType(mVar[i]), isleft(VarToWideStr(mVar[i]), 30)]);

                    s := s + #13 + 'Store procedure: ' + mLastProc;
                    if sParam <> '' then
                        s := s + #13 + 'Params:' + sParam;
                end;

                ErrMsg(s);
                Result := False;
                Exit;
            end;

            Active := True;
			SetDisplayFormat(QrExcel, sysCurFmt);

	        // Total
            SetLength(mTotal, FieldCount);
        	for i := 0 to FieldCount - 1 do
				mTotal[i] := 0.0;

            while not Eof do
            begin
	        	for i := 0 to FieldCount - 1 do
                    if Fields[i].DataType in [ftFloat, ftCurrency, ftBCD] then
                        mTotal[i] := mTotal[i] + Fields[i].AsFloat;
                Next;
            end;
            First;
        end;

        // Restore columns state
        s := RegRead('Reports', mLastProc, '');
        if s <> '' then
        begin
            s := ';' + s + ';';
            with QrExcel do
                for i := 0 to FieldCount - 1 do
                    Fields[i].Visible := Pos(';' + IntToStr(i) + ';', s) <= 0;
        end;

        // Realign grid
	    with GrExcel do
        begin
        	if not Visible then
            begin
                PaFirst.Visible := False;
            	Visible := True;
            end;

            DataSource := DsExcel;
            UpdateCustomEdit;
            for i := 1 to GetColCount - 1 do
            begin
                AutoSizeColumn(i);
                Columns[i - 1].DisplayWidth := Columns[i - 1].DisplayWidth + 2;
            end;
            SetFocus;
        end;
    end;
    SetLength(mVar, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdRereadExecute(Sender: TObject);
var
	n: Integer;
begin
	n := QrRep.FieldByName('IDX').AsInteger;
    OpenReports;
    QrRep.Locate('IDX', n, []);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdRunExecute(Sender: TObject);
var
    i: Integer;
begin
    Screen.Cursor := crSqlWait;
    try
	    Proc;
    	with GrExcel, QrExcel do
		for i := 0 to FieldCount - 1 do
			if Fields[i].DataType = ftBoolean then
				SetControlType(Fields[i].FieldName, fctCheckBox, 'True;False');
        GrExcel.RefreshDisplay;
    finally
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdSaveExecute(Sender: TObject);
var
    mFileName: String;
    bm: TBytes;
    b: Boolean;
begin
    with QrREP do
    begin
        DisableControls;
        bm := Bookmark;
        Filtered := False;
        try
            b := Locate('REP_NAME', mLastProc, []);
            if b then
                b := (FieldByName('RIGHT').AsInteger > R_READ);
        finally
            Filtered := True;
            Bookmark := bm;
            EnableControls;
        end;
    end;

    if not b then
    begin
        DenyMsg;
        Exit;
    end;

	// Get file name
    mFileName := RegReadString('Reports', 'Export Folder', '');
    mFileName := IncludeTrailingPathDelimiter(mFileName) + mLastName + '.xls';

    mFileName := isGetSaveFileName('xls', 1, mFileName);
    if mFileName = '' then
        Exit;
    RegWrite('Reports', 'Export Folder', ExtractFilePath(mFileName));

    // Do export
    Refresh;
	StartProgress(QrExcel.RecordCount, 'Exporting');
    SetProgressDesc(mLastName);
    with SMExportToExcel1 do
    begin
        KeyGenerator := mLastName;
    	FileName := mFileName;
    	Execute;
    end;
	StopProgress;

    // Open
    if YesNo('Mở file "' + mFileName + '". Tiếp tục?') then
        ShellExecute(0, Nil, PWChar(mFileName), Nil, Nil, SW_SHOW);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdShowRptExecute(Sender: TObject);
var
	sRep, sCap: String;
    i, n: Integer;
    mVar: array of Variant;
begin
    // Prepare
	with QrRep do
    begin
    	sRep := Trim(FieldByName('REP_NAME').AsString);
        sCap := isProperCase(isStripToneMark(Trim(FieldByName('DESC').AsString)));

	    // Within month
        if FieldByName('TRONGTHANG').AsBoolean then
        begin
            if FieldByName('TUNGAY').AsBoolean then
                if not mFrames[mFields.IndexOf('TUNGAY')].Validate then
                    Abort;

            if FieldByName('THOIGIAN').AsBoolean then
                if not mFrames[mFields.IndexOf('THOIGIAN')].Validate then
                    Abort;
        end;
    end;

    if sRep = '' then
    begin
    	GrList.SetFocus;
    	Exit;
    end;

    // Stock report
    if QrRep.FieldByName('GIA').AsBoolean then
    begin
		if mMustCalc then
    	    CmdCalc.Execute;
	end;

    // Get Criteria
	n := Length(mFrames) - 1;
    SetLength(mVar, 0);
	for i := 0 to n do
        with mFrames[i] do
        if Visible then
        begin
            SetLength(mVar, Length(mVar) + GetParamNo);
            GetCriteria(mVar);
        end;

    // Proc
    ShowReport(sCap, sRep, mVar);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.PgGroupChange(Sender: TObject);
begin
    with QrRep do
    begin
    	Filter := Format('CAT=%d', [PgGroup.ActivePage.Tag]);
        while not Eof do
        	if FieldByName('SYS').AsBoolean then
	        	Next
            else
            	Break;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdCalcExecute(Sender: TObject);
begin
    mFrames[mFields.IndexOf('GIA')].Action(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	with QrRep do
    begin
		CmdCalc.Enabled := FieldByName('GIA').AsBoolean and mStockControl;
    	CmdRun.Enabled := not FieldByName('SYS').AsBoolean;
        CmdFields.Enabled := QrExcel.Active;
        CmdSave.Enabled := QrExcel.Active;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep.CmdFieldsExecute(Sender: TObject);
var
	s: String;
    i: Integer;
begin
	Application.CreateForm(TFrmFlexRep3, FrmFlexRep3);
    if not FrmFlexRep3.Execute(QrExcel) then
    	Exit;

    // Save columns state
    s := '';
    with QrExcel do
        for i := 0 to FieldCount - 1 do
            if not Fields[i].Visible then
                s := s + ';' + IntToStr(i);
	RegWrite('Reports', Trim(QrRep.FieldByName('REP_NAME').AsString), isSubStr(s, 2));
end;

end.
