﻿(*==============================================================================
** Updated: 2004-02-24
** > Optimize
** > Them chuc nang dieu chinh thue dau ra
**------------------------------------------------------------------------------
** Updated: 2004-12-17
** > Optimize, xem lai viec total invoice bi sai
**------------------------------------------------------------------------------
*)
unit Banle;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill2,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit;

type
  TFrmBanle = class(TForm)
    ToolMain: TToolBar;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    QrDMVT: TADOQuery;
    CmdTotal: TAction;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    QrBHNGAY: TDateTimeField;
    QrBHCA: TWideStringField;
    QrBHSOTIEN: TFloatField;
    QrBHCHIETKHAU: TFloatField;
    QrBHTHUE: TFloatField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHTHUNGAN: TWideStringField;
    QrBHXOA: TWideStringField;
    QrBHNG_CK: TWideStringField;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    LbHH1: TLabel;
    EdHH1: TwwDBEdit;
    EdCK: TwwDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    Label24: TLabel;
    Label1: TLabel;
    QrCTBHNG_DC: TWideStringField;
    CmdDel: TAction;
    QrBHNG_HUY: TWideStringField;
    CmdSearch: TAction;
    QrCTBHTRA_DATE: TDateTimeField;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    QrBHSCT: TWideStringField;
    QrBHTL_CK: TFloatField;
    QrCTBHTHUE_SUAT: TFloatField;
    QrBHCK_BY: TIntegerField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    QrCTBHTRA_BY: TIntegerField;
    QrBHPTTT: TWideStringField;
    QrBHCHUATHOI: TFloatField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrBHDGIAI: TWideMemoField;
    QrBHIMG: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    QrCTBHTL_CK: TFloatField;
    QrCTBHCHIETKHAU: TFloatField;
    QrCTBHTIEN_THUE: TFloatField;
    QrCTBHTENTAT: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrBHSOLUONG: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    QrBHMAMADT: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHTHANHTOAN: TFloatField;
    QrBHLK_TENDT: TWideStringField;
    CmdReRead: TAction;
    QrBHLK_TENKHO: TWideStringField;
    CmdFilterCom: TAction;
    QrBHQUAY: TWideStringField;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    QrCTBHSOTIEN: TFloatField;
    Bevel1: TBevel;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHLOAITHUE: TWideStringField;
    QrBHLK_PTTT: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrBHDRC_STATUS: TWideStringField;
    TntLabel1: TLabel;
    CbMaKho: TwwDBLookupCombo;
    CbTenKho: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrCTBHMABO: TWideStringField;
    QrCTBHTL_CK2: TFloatField;
    QrCTBHTL_CK3: TFloatField;
    QrCTBHTL_CK4: TFloatField;
    QrCTBHTL_CK5: TFloatField;
    QrCTBHTL_CK6: TFloatField;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrBHPTTT2: TWideStringField;
    QrBHTTTT: TWideStringField;
    QrBHLK_PTTT2: TWideStringField;
    QrBHLK_TTTT: TWideStringField;
    QrBHLOC: TWideStringField;
    QrDMKHOLOC: TWideStringField;
    QrBHTTOAN1: TFloatField;
    QrBHTTOAN2: TFloatField;
    QrBHTTOAN3: TFloatField;
    QrBHTTOAN4: TFloatField;
    QrBHTTOAN5: TFloatField;
    QrBHTTOAN6: TFloatField;
    EdTENDT: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBDateTimePicker;
    Label4: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    QrCTBHTL_CK7: TFloatField;
    QrCTBHTL_CK_MAX: TFloatField;
    QrCTBHTL_CK_THEM: TFloatField;
    QrCTBHTL_CK_THEM2: TFloatField;
    QrCTBHCK_BY: TIntegerField;
    wwDBEdit6: TwwDBEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrBHBeforeEdit(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CbMAQUAYChange(Sender: TObject);
    procedure QrBHTL_CKChange(Sender: TField);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
  private
    mCanEdit, mClose: Boolean;
    mVATMode: Integer;
    mPrinter: TexPrintBill;

    // List filter
    fType: Integer;
   	fTungay, fDenngay: TDateTime;
    fKho, fSQL, fStr: String;

    procedure Total(fUpdate: Boolean);
  public
  	procedure Execute(r: WORD; bClose: Boolean = True);
  end;

var
  FrmBanle: TFrmBanle;

implementation

uses
	isDb, ExCommon, MainData, Rights, RepEngine, ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HOADON_BANLE';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.Execute(r: WORD; bClose: Boolean);
begin
	mCanEdit := rCanEdit(r);
    mClose := bClose;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrBH;

    exBillInitial(mPrinter, FORM_CODE + '.RPT');
    mPrinter.PrintPreview := True;
    
    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrBH.SQL.Text;
	EdFrom.Date := Date - sysLateDay;
	EdTo.Date := Date;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrLOC, QrDMKHO, QrDMVT]);

    CbMaKho.LookupValue := sysDefKho;
    CbTenKho.LookupValue := sysDefKho;

	QrDMQUAY.Open;

    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger('POS', 'VAT Form');

    with QrBH do
    begin
	    SetDisplayFormat(QrBH, sysCurFmt);
    	SetShortDateFormat(QrBH);
	    SetDisplayFormat(QrBH, ['NGAY'], 'dd/mm/yyyy hh:nn');
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
    	SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);
    
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exBillFinal(mPrinter);
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrBH, QrCTBH, QrDMVT, QrDMKHO, QrDMQUAY]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;
    (*
    ** End: Forms events
	*)

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin

   	if (EdFrom.Date <> fTungay) or (EdTo.Date <> fDenngay)or (CbMaKho.LookupValue <> fKho) then
    begin
		fTungay := EdFrom.Date;
		fDenngay := EdTo.Date;
        fKho := CbMaKho.LookupValue;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ CbMaKho.LookupValue + '''');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from BANLE_CT where KHOA = BANLE.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMaKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
	with QrDMQUAY do
    begin
    	Close;
        Open;
    end;
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdSaveExecute(Sender: TObject);
begin
	QrCTBH.UpdateBatch;
	QrBH.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdCancelExecute(Sender: TObject);
begin
	QrCTBH.CancelBatch;
    QrBH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdPrintExecute(Sender: TObject);
var
	k: TGUID;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  TGuidField(FieldByName('KHOA')).AsGuid;
	end;

    if mPrinter.BillPrint(k) then
        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdDelExecute(Sender: TObject);
var
    s: String;
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
    s := QrBH.FieldByName('DGIAI').AsString;
    if QrBH.FieldByName('DELETE_BY').AsInteger = 0 then
    begin
        s := FrmReceiptDesc.Execute(s);
        if s = '' then
        begin
            ErrMsg('Phải nhập ghi chú mới được xóa phiếu.');
            Exit;
        end;
    end;

   	MarkDataSet(QrBH);
    with QrBH do
    begin
    	Edit;
        FieldByName('DGIAI').AsString := s;
        FieldByName('DRC_STATUS').AsString := '2';
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
    Total(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrBH);

    CmdPrint.Enabled := n = 1;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse; 
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHBeforeEdit(DataSet: TDataSet);
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHTL_CKChange(Sender: TField);
begin
	Total(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh, xCkhd: Double;
	mSotien, mSoluong, mCkmh, mCkhd, mTlckhd, mThue: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mCkhd := 0;
    mThue := 0;
	mTlckhd := QrBH.FieldByName('TL_CK').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
        	xSotien := FieldByName('SOTIEN').AsFloat;		// Thanh tien chua chiet khau
            xThsuat := FieldByName('THUE_SUAT').AsFloat;	// Thue suat
            xCkmh := FieldByName('CHIETKHAU').AsFloat;		// Tien CKMH
            if xCkmh = 0.0 then								// Tinh tien CKHD
	            xCkhd := xSotien * mTlckhd / 100.0
            else
            	xCkhd := 0.0;

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
	        mCkmh := mCkmh + xCkmh;
	        mCkhd := mCkhd + xCkhd;

            if mVATMode = 0 then	// Tinh thue truoc chiet khau
	            mThue := mThue + xSotien * xThsuat / (100.0 + xThsuat)
	    	else					// Tinh thue sau chiet khau
	            mThue := mThue + (xSotien - xCkmh - xCkhd) * xThsuat / (100.0 + xThsuat);

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
    	FieldByName('CHIETKHAU_MH').AsFloat := mCkmh;
    	FieldByName('CHIETKHAU').AsFloat := mCkhd;
    	FieldByName('THUE').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mSotien - mCkmh - mCkhd;

        if fUpdate then
	        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMAQUAYChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDbLookupCombo).Text = '' then
		s := ''
	else
		s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TWinControl).Tag of
    3:
        CbTenkho.LookupValue := s;
    4:
    	CbMakho.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
