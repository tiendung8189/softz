(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosVIP;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  DB, ADODB, StdCtrls, Buttons, wwdblook, wwdbdatetimepicker,
  DBCtrls, ExtCtrls, isPanel, wwdbedit, ActnList, Mask, pngimage,
  dxGDIPlusClasses;

type
  TFrmPosVIP = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    QrDMVIP: TADOQuery;
    QrDMVIPMASO: TWideStringField;
    QrDMVIPHO: TWideStringField;
    QrDMVIPTEN: TWideStringField;
    QrDMVIPDCHI: TWideStringField;
    QrDMVIPDTHOAI: TWideStringField;
    QrDMVIPEMAIL: TWideStringField;
    QrDMVIPTL_CK: TFloatField;
    QrDMVIPMAVIP: TWideStringField;
    QrDMVIPCREATE_DATE: TDateTimeField;
    QrDMVIPUPDATE_DATE: TDateTimeField;
    DsDMVIP: TDataSource;
    isPanel1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    EdHo: TwwDBEdit;
    EdTen: TwwDBEdit;
    EdDChi: TwwDBEdit;
    EdDienthoai: TwwDBEdit;
    DBEdit6: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    PD2: TisPanel;
    TntLabel1: TLabel;
    Label25: TLabel;
    TntLabel4: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBEdit11: TwwDBEdit;
    CbLoai: TwwDBLookupCombo;
    ActionList1: TActionList;
    CmdSave: TAction;
    Image1: TImage;
    QrDMVIPDOANHSO: TFloatField;
    QrDMVIPNGAYDSO: TDateTimeField;
    QrDMVIPNGAYSINH: TDateTimeField;
    QrDMVIPCREATE_BY: TIntegerField;
    QrDMVIPUPDATE_BY: TIntegerField;
    QrDMVIPLOAI: TWideStringField;
    QrDMVIPNGAY_THAMGIA: TDateTimeField;
    QrDMVIPLOC_THAMGIA: TWideStringField;
    QrDMVIPHOTEN: TWideStringField;
    QrDMVIPSOTIEN_MUA: TFloatField;
    QrDMVIPSOTIEN_TRA: TFloatField;
    QrDMVIPDOANHSO_DAU: TFloatField;
    QrDMVIPDOANHSO_MUA: TFloatField;
    QrDMVIPTINHTRANG: TWideStringField;
    QrDMVIPCMND: TWideStringField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    procedure QrDMVIPBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure exDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMVIPBeforeEdit(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVIPHOChange(Sender: TField);
  private
  public
  	function  Execute(id: String; act: Integer): String;
  end;

var
  FrmPosVIP: TFrmPosVIP;

implementation

uses
	isDB, isLib, exCommon, isCommon, MainData;

{$R *.dfm}

(*==============================================================================
** Arguments:
**	id		MAVIP
**	act		0:	register; 1: view / edit
**------------------------------------------------------------------------------
** Return value:
**	VIP full name = last + ' ' + first name
** or
**	'' if not registered
**------------------------------------------------------------------------------
*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosVIP.Execute(id: String; act: Integer): String;
begin
    with QrDMVIP do
    begin
    	Parameters[0].Value := id;
        Open;
        if act = 0 then
        begin
        	SetEditState(QrDMVIP);
            FieldByName('LOC_THAMGIA').AsString := sysLoc;
            FieldByName('TINHTRANG').AsString := '02';
            FieldByName('NGAY_THAMGIA').AsDateTime := Now;
        end;
    end;
	ShowModal;

    with QrDMVIP do
    	Result := FieldByName('HOTEN').AsString;

    CloseDataSets([QrDMVIP, DataMain.QrLOAI_VIP_LE]);
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrDMVIP, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormShow(Sender: TObject);
begin
    OpenDataSets([DataMain.QrLOAI_VIP_LE]);

	SetDisplayFormat(QrDMVIP, sysCurFmt);
    SetDisplayFormat(QrDMVIP, ['NGAYDSO'], ShortDateFormat + ' hh:nn');

    SetDictionary(QrDMVIP, 'DM_VIP', Nil);
    EdHo.SetFocus;
    CbLoai.ReadOnly := sysKhttLe > 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPBeforeEdit(DataSet: TDataSet);
var
	s: String;
begin
	with QrDMVIP do
    begin
    	s := FieldByName('TINHTRANG').AsString;
        if (s <> '01') then
        	Abort;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPBeforePost(DataSet: TDataSet);
begin
	with QrDMVIP do
	begin
    end;
	SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPHOChange(Sender: TField);
begin
    with QrDMVIP do
    	FieldByName('HOTEN').AsString := Trim(FieldByName('HO').AsString) +
        	' ' + Trim(FieldByName('TEN').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
    with QrDMVIP do
    begin
        if not Active then
        	Exit;
        bBrowse := (State in [dsBrowse]);
    end;
    CmdSave.Enabled := not bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdSaveExecute(Sender: TObject);
begin
	with QrDMVIP do
    begin
        if not (State in [dsBrowse]) then
        	Post;
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.exDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    Action := DbeMsg;
end;

end.
