object FrmPosChitiet: TFrmPosChitiet
  Left = 108
  Top = 129
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin H'#243'a '#272#417'n Chi Ti'#7871't'
  ClientHeight = 408
  ClientWidth = 1210
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrList: TwwDBGrid2
    Left = 0
    Top = 0
    Width = 1210
    Height = 408
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MAVT'#9'15'#9'M'#227' h'#224'ng'#9'F'
      'TENVT'#9'35'#9'T'#234'n h'#224'ng'#9'F'
      'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'
      'DONGIA'#9'10'#9#272#417'n gi'#225#9'F'
      'SOTIEN'#9'10'#9'S'#7889' ti'#7873'n'#9'F'
      'TL_CK'#9'5'#9'Max'#9'F'#9'Chi'#7871't kh'#7845'u'
      'TL_CK3'#9'5'#9'V.I.P'#9'F'#9'Chi'#7871't kh'#7845'u'
      'TL_CK4'#9'5'#9'TL_CK4'#9'F'#9'Chi'#7871't kh'#7845'u'
      'TL_CK5'#9'5'#9'TL_CK5'#9'F'#9'Chi'#7871't kh'#7845'u'
      'TL_CK6'#9'5'#9'TL_CK6'#9'F'#9'Chi'#7871't kh'#7845'u'
      'TL_CK7'#9'5'#9'TL_CK7'#9'F'#9'Chi'#7871't kh'#7845'u'
      'CHIETKHAU'#9'10'#9'S'#7889' ti'#7873'n'#9'F'#9'Chi'#7871't kh'#7845'u'
      'THUE_SUAT'#9'5'#9'%'#9'F'#9'Thu'#7871' VAT'
      'TIEN_THUE'#9'10'#9'S'#7889' ti'#7873'n'#9'F'#9'Thu'#7871' VAT')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = FrmMain.DsCTBH
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
    ExplicitHeight = 103
  end
  object QrTAM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    Left = 442
    Top = 26
  end
  object DsTAM: TDataSource
    DataSet = QrTAM
    Left = 442
    Top = 58
  end
end
