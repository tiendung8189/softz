(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SoBill;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls;

type
  TFrmSoBill = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdSCT: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    EdThang: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    EdNam: TComboBox;
    procedure FormShow(Sender: TObject);
  private
  public
  	function GetBillNo (var m, y : Integer; var s : String) : Boolean;
  end;

var
  FrmSoBill: TFrmSoBill;

implementation

{$R *.DFM}

uses
	isLib, isStr;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmSoBill.GetBillNo;
begin
	isMonthList(EdThang, m);
    isYearList(EdNam, y);
	EdSct.Text := Trim(s);
	EdSct.SelectAll;

   	Result := ShowModal = mrOK;

    if Result then
    begin
	    m := StrToInt(EdThang.Text);
    	y := StrToInt(EdNam.Text);
	    s := EdSct.Text;
    end;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSoBill.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;
    EdSct.SetFocus;
end;
end.
