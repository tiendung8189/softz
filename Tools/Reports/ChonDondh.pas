(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDondh;

interface

uses
  Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmChonDondh = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsDONDH: TDataSource;
    QrDONDH: TADOQuery;
    QrDONDHNGAY: TDateTimeField;
    QrDONDHSCT: TWideStringField;
    QrDONDHMADT: TWideStringField;
    QrDONDHMAKHO: TWideStringField;
    QrDONDHNG_DATHANG: TWideStringField;
    QrDONDHTHANHTOAN: TFloatField;
    QrDONDHDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbNCC: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMADT: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMNCC: TADOQuery;
    QrDONDHTENKHO: TWideStringField;
    QrDONDHTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrDONDHNGAY_GIAO: TDateTimeField;
    QrDONDHCHIETKHAU_HD: TFloatField;
    QrDONDHSOLUONG: TFloatField;
    QrDONDHKHOA: TGuidField;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMADTChange(Sender: TObject);
    procedure CbMADTExit(Sender: TObject);
    procedure CbMADTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
	mNCC, mKHO, mSQL: String;
  public
  	function Execute(pFix: Boolean; pNcc, pKho: String): TGUID;
  end;

var
  FrmChonDondh: TFrmChonDondh;

implementation

uses
	isDb, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDondh.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    CbMADT.Enabled := not pFix;
    CbNCC.Enabled := not pFix;

	if ShowModal = mrOK then
		Result := TGuidField(QrDONDH.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrDONDH, QrDMNCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DONDH', GrBrowse);

    SetDisplayFormat(QrDONDH, sysCurFmt);
    SetShortDateFormat(QrDONDH);
    SetDisplayFormat(QrDONDH, ['NGAY'], DateTimeFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormShow(Sender: TObject);
begin
   	OpenDataSets([QrDONDH, QrDMNCC, QrDMKHO]);
	mSQL := QrDONDH.SQL.Text;
    CbMADT.LookupValue := mNCC;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mNCC = '' then
		try
    		CbMADT.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDONDH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
   	if (mNCC <> CbMADT.LookupValue) or
	   (mKHO <> CbMAKHO.LookupValue) then
	begin
   	    mNCC := CbMADT.LookupValue;
        mKHO := CbMAKHO.LookupValue;

        with QrDONDH do
        begin
			Close;
            s := mSQL;
            if mNCC <> '' then
            	s := s + 'and a.MADT=''' + mNCC + '''';

			if mKHO <> '' then
            	s := s + 'and a.MAKHO=''' + mKHO + '''';

            SQL.Text := s;
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CbMADTChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbNCC.LookupValue := s;
    // Ten NCC
    1:
        CbMADT.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CbMADTExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CbMADTNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

end.
