object FrmSoBill: TFrmSoBill
  Left = 192
  Top = 107
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n H'#243'a '#208#417'n'
  ClientHeight = 157
  ClientWidth = 301
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 285
    Height = 89
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 52
      Width = 65
      Height = 16
      Caption = 'S'#7889' h'#243'a '#273#417'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 45
      Top = 20
      Width = 36
      Height = 16
      Alignment = taRightJustify
      Caption = 'Th'#225'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 160
      Top = 20
      Width = 26
      Height = 16
      Caption = 'N'#259'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdSCT: TEdit
      Left = 92
      Top = 48
      Width = 173
      Height = 24
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object EdThang: TComboBox
      Left = 92
      Top = 16
      Width = 49
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ItemHeight = 0
      ParentFont = False
      TabOrder = 0
    end
    object EdNam: TComboBox
      Left = 196
      Top = 16
      Width = 69
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ItemHeight = 0
      ParentFont = False
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 96
    Top = 108
    Width = 97
    Height = 41
    Cursor = 1
    Caption = 'In'
    Default = True
    DoubleBuffered = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000000000000000
      00000000000000000000CC8F27FFCC8F27FFCC8F27FFCC8F27FFCC8F27FFCC8F
      27FFCC8F27FFCC8F27FF00000000000000000000000000000000000000001A18
      141B0000000000000000CC8F27FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFCC8F27FF000000003C342B451A18141C0000000000000000985C
      12EB9B5300FF9B5300FFCC8F27FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFCC8F27FF9F5A00FF9F5A00FF995C14EC00000000966623D5B47C
      2EFFDDBD87FFDDBD87FF863B1AFF863B1AFF863B1AFF863B1AFF863B1AFF863B
      1AFF863B1AFF863B1AFFDDBD87FFDEBD88FFB47C2EFF94601CDB9F5A00FFEED8
      AEFFEED8AEFFEED8AEFFEED8AEFFEED8AEFFEED8AEFFEED8AEFFEED8AEFFEED8
      AEFFEED8AEFFEED8AEFFEED8AEFFEFD9AFFFEFD9AFFF9F5A00FF9F5A00FFEAD1
      A6FFEAD1A5FFE9CFA1FFEAD1A2FFEAD1A2FFEAD1A2FFEAD1A2FFEAD1A2FFEAD1
      A2FFEAD1A2FFEAD1A2FFE9CFA1FFEAD1A6FFEAD1A6FF9F5A00FF9F5A00FFEDDD
      C0FFEDDDC0FFE9D7B7FFE9D7B7FFE9D7B7FFE9D7B7FFE9D7B8FFE9D7B8FFE9D7
      B8FFE9D7B8FFE9D7B8FFE9D7B8FFEDDDC2FFEDDDC0FF9F5A00FF9F5A00FFEBE1
      D0FFEFE8DBFFEBE1D0FFEBE1D0FFEBE1D0FFEBE1D0FFEBE1D0FFEBE1D0FFEBE1
      D0FFEBE1D0FFEBE1D0FFEBE1D0FFEFE9DCFFEBE1D0FF9F5A00FF9F5A00FFF5F6
      F5FFF5F6F5FFF0EDE7FFF0EDE7FFF0EDE7FFF0EDE7FFF0EDE7FFF0EDE7FFF0ED
      E7FFF0EDE7FFF0EDE7FFF0EDE7FFF5F6F6FFF5F6F6FF9F5A00FF92672CCBBC8F
      54FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6
      F6FFF5F6F6FFF5F6F6FFF5F6F6FFF5F6F6FFBC8F54FF906222D100000000905C
      1CD39F5A00FF9F5A00FF9F5A00FF9F5A00FF9F5A00FF9F5A00FF9F5A00FF9F5A
      00FF9F5A00FF9F5A00FF9F5A00FF9F5A00FF8F5C1FD400000000000000000504
      04050000000000000000E2B04DFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE7B44FFF00000000000000000000000000000000000000000000
      00000000000000000000EBBE64FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
      FAFFFFFFFFFFEBBE64FF00000000000000000000000000000000000000000000
      00000000000000000000EEC677FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFF7E6C3FFEBC476FC00000000000000000000000000000000000000000000
      00000000000000000000EEC677FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7E6
      C3FFE7C174F8BE9E5FCC00000000000000000000000000000000000000000000
      00000000000000000000EEC677FFEEC677FFEEC677FFEEC677FFEEC677FFEEC6
      77FFB29459BF0000000000000000000000000000000000000000}
    ModalResult = 1
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 196
    Top = 108
    Width = 97
    Height = 41
    Cursor = 1
    Cancel = True
    Caption = 'Tho'#225't'
    DoubleBuffered = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFEEEDF8A6A5E06765CE4946CB4946CB6765CEA6A5E0EEEDF8FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A6E20B07CD100CED120EFE13
      0EFF130EFF120EFE100CEE0B07CDA7A6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      8887E0100EE91412FC1313FC110FE90F0DE00F0DE0110FEA1211F71312FB100E
      EA8180DFFFFFFFFFFFFFFFFFFFA7A6E41214EB1619FC1416F21F1ED4AAA9ECAE
      AEE4B5B4E73C39C71315EF1416F61518F91214EBA7A6E4FFFFFFEEEEFA1013DB
      171FFB171DF18C8CECF6F6FDFFFFFFFFFFFF918FDF1A1FE3161EF7161DF4161E
      F6171EFA1013DBEEEEFAA8A7EC151EF11B27FB2224D9FFFFFFFFFFFFFFFFFF88
      85E0141BE61924F91923F61924F5151AE71A25F8151EF2A8A7EC7373E52739FD
      1521F1AAA9EDFFFFFFFFFFFF817EE1141BE31B2CF91B2AF61B2BF81113D93B38
      CF1521F02739FD7373E56062E6536CFF202FECB4B3F1FFFFFF8885E6161EE41E
      32F81C31F61E34F9161DE4918EE5C0BFF1202FEC546CFF5F61E35F61EC607BFF
      414FF1C1BFF4928EEA1820E62139F92037F62139F81821E98985E7FFFFFFB5B4
      F2414FF1607AFF5D5FE07978F15F77FF4C60F73D37E21318E5253FFA243CF826
      41FA1B26EC837EE8FFFFFFFFFFFFADABF54C60F95C75F87675E4ABA8F55C6CFD
      5E7CFE4251F43650FA223EF82845FA202FF28A86EEFFFFFFFFFFFFFFFFFF3B40
      EE6281FF5869E9A9A7EEEEEDFD4042F57490FF5E7BFD5C78FC3D5CFB2637F393
      90F2FFFFFFFFFFFFF6F6FF9B9EF9556DFC7591FD393BD2EEEDFDFFFFFFA8A6F8
      646EFA7A94FE617EFC5A73FB4843EFB6B3F9B1AEFAB4B5FB4047F35873FC7F9D
      FF5B65DFA6A4ECFFFFFFFFFFFFFFFFFF8D89F86772F98CA8FE7695FF5A72FC4F
      64FA4E63F95870FC7D9DFF8EAAFE636EE48887DAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFA8A6F84445F17A88F793A9FD98B1FF98B2FF92A9FB7988EF4042DFA6A4
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEDFEAEACF67E7CF369
      69EF6869EE7C7CEEADABEDEEEDFDFFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 2
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
  end
end
