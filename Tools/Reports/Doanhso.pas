(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Doanhso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, Buttons,
  ExtCtrls, DB, ADODB, Grids, wwDataInspector;

type
  TFrmDoanhso = class(TForm)
    TntActionList1: TActionList;
    CmdPrint: TAction;
    DOANHSO: TADOStoredProc;
    EdMA: TEdit;
    Inspect: TwwDataInspector;
    DsVIP: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure CmdPrintExecute(Sender: TObject);
    procedure TntFormShow(Sender: TObject);
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
  private
  	procedure Reopen;
  public
  	procedure Execute(ma: String);
  end;

var
  FrmDoanhso: TFrmDoanhso;

implementation

uses
	MainData, isMsg, isDb, ExCommon;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
	dsValues: array of Variant;

procedure TFrmDoanhso.CmdPrintExecute(Sender: TObject);
var
	i, k, n: Integer;
begin
	with DOANHSO do
    begin
        n := FieldCount;
        SetLength(dsValues, 2 * n + 2);

        k := 0;
        for i := 0 to n - 1 do
        begin
			dsValues[k] := Fields[i].FullName;
            Inc(k);
			dsValues[k] := Fields[i].Value;
            Inc(k);
        end;
        dsValues[k] := 'NGAY';
        Inc(k);
        dsValues[k] := Now;
        Inc(k);
	end;
	exPrintAccu(dsValues);
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDoanhso.Reopen;
begin
	with DOANHSO do
    begin
    	Close;
    	Prepared := True;
        Parameters[1].Value := EdMA.Text;
        ExecProc;
        Active := True;
    end;
    SetDisplayFormat(DOANHSO, sysCurFmt);
    EdMA.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDoanhso.TntFormShow(Sender: TObject);
begin
	Reopen;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDoanhso.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDoanhso.Execute(ma: String);
begin
	EdMA.Text := ma;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDoanhso.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	Reopen;
end;

end.
