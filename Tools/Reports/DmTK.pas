﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmTK;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, Wwdbgrid, RzSplit, wwDialog, Wwdbigrd, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdbdatetimepicker, wwcheckbox, wwdblook;

type
  TFrmDmTK = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    EdMA: TwwDBEdit;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit4: TwwDBEdit;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    QrDanhmucMATK: TWideStringField;
    QrDanhmucTENTK: TWideStringField;
    QrDanhmucDAIDIEN: TWideStringField;
    QrDanhmucNH_DIACHI: TWideStringField;
    QrDanhmucNH_DTHOAI: TWideStringField;
    QrDanhmucFAX: TWideStringField;
    QrDanhmucGHICHU: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TwwDBEdit;
    Label2: TLabel;
    Label8: TLabel;
    QrDanhmucPLOAI: TWideStringField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    QrDanhmucNGAY: TDateTimeField;
    QrDanhmucMADT: TWideStringField;
    QrDanhmucMANV: TWideStringField;
    CkbVISA: TwwCheckBox;
    CbNganhang: TwwDBLookupCombo;
    CbChinhanh: TwwDBLookupCombo;
    CbNganhangMa: TwwDBLookupCombo;
    CbChinhanhMa: TwwDBLookupCombo;
    QrDanhmucMANH: TWideStringField;
    QrDanhmucMACN: TWideStringField;
    QrDanhmucCHARGE_POS: TBooleanField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucMATKChange(Sender: TField);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucBeforeOpen(DataSet: TDataSet);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure CbChinhanhBeforeDropDown(Sender: TObject);
  private
  	mCanEdit, fixCode, mClose, mRet: Boolean;
    mSql, mLoai, mMadt, mManv, mMatk: String;

  public
  	function Execute(r: WORD; var pMatk: String; pLoai: String = 'NB'; pMadt: string = '';
            pManv: string = ''; pClose: Boolean = True): Boolean;
  end;

var
  FrmDmTK: TFrmDmTK;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_TAIKHOAN';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmTK.Execute(r: WORD; var pMatk: String; pLoai: String;
pMadt: string; pManv: string; pClose: Boolean): Boolean;
begin
	mCanEdit := rCanEdit(r);
	DsDanhmuc.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    mLoai := pLoai;
    mMadt := pMadt;
    mManv := pManv;
    mClose := pClose;
    ShowModal;
    pMatk := mMatk;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDanhmuc, FORM_CODE);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');
    mSql := QrDanhmuc.SQL.Text;

	fixCode := SetCodeLength(FORM_CODE, QrDanhmuc.FieldByName('MATK'));
    mTrigger := False;
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.FormShow(Sender: TObject);
begin
    with DataMain do
        OpenDataSets([QrNganhang, QrNganhangCN]);
    if mLoai <> 'NB' then
    begin
        if mMadt <> '' then
            mSql := mSql + ' and MADT = ''' + mMadt + '''';

        if mManv <> '' then
            mSql := mSql + ' and MANV = ''' + mManv + '''';

        mSql := mSql + ' order by NGAY'
    end;

    with QrDanhmuc do
    begin
        SQL.Text := mSql;
        Open;
    end;

    CkbVISA.Visible := mLoai = 'NB';

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
            CloseDataSets(DataMain.Conn)
        else
        begin
            with QrDanhmuc do
            begin
                if mRet then
                begin
                    Last;
                    mMatk := FieldByName('MATK').AsString;
                end
                else
                    mMatk := '';
            end;
            CloseDataSets([QrDanhmuc]);
        end;
    finally
    end;

	// Save state panel
    RegWrite(Name, 'PD2', PD2.Collapsed);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Date;
    with QrDanhmuc do
    begin
        FieldByName('NGAY').AsDateTime := d;
        FieldByName('PLOAI').AsString := mLoai;
        FieldByName('CHARGE_POS').AsBoolean := False;
        FieldByName('MADT').AsString := Iif(mMadt <> '', mMadt, '');
        FieldByName('MANV').AsString := Iif(mManv <> '', mManv, '');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
	    if BlankConfirm(QrDanhmuc, ['MATK', 'TENTK', 'MANH']) then
   			Abort;

	    if fixCode then
			if LengthConfirm(QrDanhmuc, ['MATK']) then
        		Abort;

	    if BlankConfirm(QrDanhmuc, ['TENTK', 'NGAY']) then
   			Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucMATKChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.QrDanhmucBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := mLoai;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MATK' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CbChinhanhBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MANH').AsString;
    (Sender as TwwDBLookupCombo).LookupTable.Filter := Format('MANH=''%s''', [s]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

end.
