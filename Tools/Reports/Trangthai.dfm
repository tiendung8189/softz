object FrmDrcStatus: TFrmDrcStatus
  Left = 601
  Top = 421
  AlphaBlend = True
  AlphaBlendValue = 192
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Tr'#7841'ng Th'#225'i Ch'#7913'ng T'#7915
  ClientHeight = 30
  ClientWidth = 213
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = TntFormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object CbTrangthai: TwwDBComboBox
    Left = 4
    Top = 4
    Width = 205
    Height = 24
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    Color = 15794175
    DataField = 'DRC_STATUS'
    DropDownCount = 8
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ItemHeight = 0
    Items.Strings = (
      '1. '#272'ang nh'#7853'p li'#7879'u'#9'1'
      '2. Ho'#224'n t'#7845't, ch'#7901' chuy'#7875'n '#273'i'#9'2'
      '3. '#272#227' chuy'#7875'n '#273'i'#9'3')
    ParentFont = False
    Sorted = False
    TabOrder = 0
    UnboundDataType = wwDefault
  end
end
