﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DondhNCC;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,  Variants,
  StdCtrls, Buttons, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi, isDb, Messages,
  isPanel, wwDialog, Grids, Wwdbgrid, ToolWin;

type
  TFrmDondhNCC = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA_REF: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTL_CK_HD: TFloatField;
    QrNXCHIETKHAU_HD: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    QrCTGIATK: TFloatField;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    PaMaster: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    Label5: TLabel;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    EdNG_DATHANG: TwwDBEdit;
    CbNCC: TwwDBEdit;
    CbKHO: TwwDBLookupCombo;
    CbMADT: TwwDBEdit;
    CbMAKHO: TwwDBLookupCombo;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    QrCTLK_DVT: TWideStringField;
    QrCTLK_TENVT: TWideStringField;
    QrNXNGAY_GIAO: TDateTimeField;
    QrNXNG_DATHANG: TWideStringField;
    QrNXSOLUONG: TFloatField;
    QrNXTINHTRANG: TWideStringField;
    Label4: TLabel;
    CbTINHTRANG: TwwDBLookupCombo;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrCTGHICHU: TWideStringField;
    QrCTLOAITHUE: TWideStringField;
    QrCTLK_LOAITHUE: TWideStringField;
    CmdFilterRef: TAction;
    QrCTTIEN_THUE: TFloatField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    CmdAudit: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    TntMenuItem1: TMenuItem;
    Xachitit1: TMenuItem;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    LAYDONDH: TADOStoredProc;
    CmdFromNX: TAction;
    Lytphiunhp1: TMenuItem;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrCTLK_DVT1: TWideStringField;
    QrCTLK_QD1: TIntegerField;
    CmdListRefesh: TAction;
    DONDH_LUONG_TON: TADOStoredProc;
    CmdFromMin: TAction;
    Lytslungtn1: TMenuItem;
    CHECK_NCC: TADOCommand;
    RefDMVT: TADOQuery;
    CmdSapthutu: TAction;
    N3: TMenuItem;
    Splithtmthng1: TMenuItem;
    CmdDmvt: TAction;
    ToolButton4: TToolButton;
    SepExport: TToolButton;
    QrNXNGAY_DAT: TDateTimeField;
    QrNXNGAY_DUKIEN: TDateTimeField;
    Label7: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label8: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    CbGia: TwwDBLookupCombo;
    LbGia: TLabel;
    QrCTQD1: TFloatField;
    QrNXHINHTHUC_GIA: TWideStringField;
    QrNXSOTIEN1: TFloatField;
    QrCTDONGIA_REF2: TFloatField;
    QrCTLK_GIABAN: TFloatField;
    CmdThamkhaoGia: TAction;
    N4: TMenuItem;
    hamkhogi1: TMenuItem;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLK_TINHTRANG: TWideStringField;
    QrNXLOC: TWideStringField;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIASI: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    BtnIn2: TToolButton;
    QrNXDRC_STATUS: TWideStringField;
    QrCTSOTIEN1: TFloatField;
    QrCTLK_TENTHUE: TWideStringField;
    QrCTLK_VAT_RA: TFloatField;
    QrCTLK_VAT_VAO: TFloatField;
    CbLOAITHUE: TwwDBLookupCombo;
    QrNXCALC_SOTIEN_SAUCK_MH: TFloatField;
    QrCTSOLUONG_NX: TFloatField;
    CmdDmncc: TAction;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    QrCTTIEN_THUE_5: TFloatField;
    QrCTTIEN_THUE_10: TFloatField;
    QrCTTIEN_THUE_OR: TFloatField;
    QrNXTHUE_5: TFloatField;
    QrNXTHUE_10: TFloatField;
    QrNXTHUE_OR: TFloatField;
    CmdPrintExcel: TAction;
    CmdPrintBaogia: TAction;
    ToolButton6: TToolButton;
    ToolButton10: TToolButton;
    SepBaogia: TToolButton;
    ToolButton13: TToolButton;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton5: TToolButton;
    ToolButton12: TToolButton;
    QrCTGIAVON: TFloatField;
    QrCTLK_GIAVON: TFloatField;
    QrCTTL_CK_HD: TFloatField;
    QrCTCHIETKHAU_HD: TFloatField;
    QrCTSOTIEN2: TFloatField;
    QrCTTHANHTIEN: TFloatField;
    QrNXSOTIEN2: TFloatField;
    QrNXTHANHTIEN: TFloatField;
    QrCTLOC: TWideStringField;
    BtCongno: TSpeedButton;
    CmdExBarcode: TAction;
    QrCTB1: TBooleanField;
    ToolButton14: TToolButton;
    SepChecked: TToolButton;
    PaTotal: TPanel;
    ImgTotal: TImage;
    PaTotal1: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EdTLCK: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    PaTotal2: TPanel;
    BtThue: TSpeedButton;
    Label13: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    PaTotal3: TPanel;
    Label24: TLabel;
    Label3: TLabel;
    EdCL: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrNXMADTChange(Sender: TField);
    procedure CmdFilterRefExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdFromOrderExecute(Sender: TObject);
    procedure CmdFromNXExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdFromMinExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAValidate(Sender: TField);
    procedure QrNXTINHTRANGValidate(Sender: TField);
    procedure CmdThamkhaoGiaExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTLOAITHUEChange(Sender: TField);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure BtThueClick(Sender: TObject);
    procedure CmdPrintExcelExecute(Sender: TObject);
    procedure CmdPrintBaogiaExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXTL_CK_HDChange(Sender: TField);
    procedure BtCongnoClick(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrCTTL_CKValidate(Sender: TField);

  private
	b1Ncc, mCanEdit, bCheck, bDuplicate: Boolean;
    mLCT, mMakho: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

    //Mac dinh cho hinh thuc gia
    fHTGia: String;

    // Ref filter
    refSQL: String;
    refType: Integer;

    function GetDetailFrom(pKhoa: TGUID; pStoredPro: TADOStoredProc; b: Boolean = False): Boolean;
    procedure escapeKey(pSleepTime: Variant);

  public
	procedure Execute(r: WORD);
  end;

var
  FrmDondhNCC: TFrmDondhNCC;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, ChonPhieuNX,
    ChonDondh3, Sapthutu, Dmvt, TheodoiGia, GuidEx, exThread, isCommon, DmKhNcc,
  Dmncc, isFile, ImportExcel, Tienthue, OfficeData, CongnoNCC;

{$R *.DFM}

const
	FORM_CODE = 'DONDH_NCC';
    
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.Execute;
begin
	// Ky hieu
	mLCT := 'DHN';

    // Audit setting
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    PaTotal.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;
    bCheck := False;

    // Done
    ShowModal;
end;

	(*
    ** Form events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    // Params
    b1Ncc := FlexConfigBool(FORM_CODE, '1 nha cung cap');
	mMakho := RegReadString(Name, 'Makho', sysDefKho);
    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    bDuplicate := FlexConfigBool(FORM_CODE, 'Duplicate Mavt');

    CmdPrintExcel.Visible := FlexConfigBool(FORM_CODE, 'Export Excel');
    SepExport.Visible := CmdPrintExcel.Visible;
    CmdPrintBaogia.Visible := FlexConfigBool(FORM_CODE, 'Bao gia');
    SepBaogia.Visible := CmdPrintBaogia.Visible;

    //Lay gia tri mac dinh cho HTGia
    fHTGia := sysHTGia;

    if fHTGia <> '' then
    begin
        cbGia.Visible := False;
        LbGia.Visible := False;
    end;

    // Initial
  	mTrigger := False;
    mTriggerCK := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;

    refSQL := RefDMVT.SQL.Text;
    refType := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.FormShow(Sender: TObject);
begin
	// Open database
	with DataMain do
		OpenDataSets([QrTT_DDH, QrDMLOAITHUE, QrDMNCC, QrDMVT, QrDMKHO, QrHINHTHUC_GIA]);

    RefDMVT.Open;
	SetDisplayFormat(RefDMVT, sysCurFmt);
    SetDisplayFormat(RefDMVT,['TL_LAI'], sysPerFmt);

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetDisplayFormat(QrNX, ['TL_CK_HD'], sysPerFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

	with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
	    SetDisplayFormat(QrCT, ['TL_CK', 'TL_CK_HD'], sysPerFmt);
        SetDisplayFormat(QrCT, ['THUE_SUAT'], sysTaxFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsThue then
    begin
        PaTotal2.Visible := sysIsThue;
        grRemoveFields(GrBrowse, ['THUE']);
        grRemoveFields(GrDetail, ['LK_TENTHUE', 'THUE_SUAT', 'TIEN_THUE', 'THANHTIEN']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    // Read master list
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, 'Makho', mMakho);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
    ** Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

	(*
    ** Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from DONDH_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = DONDH.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from DONDH_CT a, DM_VT_FULL b where a.KHOA = DONDH.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from DONDH_CT where KHOA = DONDH.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    
    with DataMain do
    begin
		QrTT_DDH.Requery;
        QrDMNCC.Requery;
        QrDMVT.Requery;
        QrDMKHO.Requery;
	end;
    RefDMVT.Requery;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
    if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdDmnccExecute(Sender: TObject);
var
    r: WORD;
    mRet: Boolean;
begin
    if DataMain.GetFuncState('SZ_KH_NCC') then
    begin
        r := DataMain.GetRights('SZ_KH_NCC');
        if r = R_DENY then
            Exit;

        Application.CreateForm(TFrmDmKhNcc, FrmDmKhNcc);
        FrmDmKhNcc.Execute(r, False);
    end else
    begin
        r := DataMain.GetRights('SZ_NCC');
        if r = R_DENY then
            Exit;

        Application.CreateForm(TFrmDmncc, FrmDmncc);
        FrmDmncc.Execute(r, False);
    end;

    if mRet then
        DataMain.QrDmncc.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := DataMain.GetRights('SZ_MAVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvt, FrmDmvt);
    mRet := FrmDmvt.Execute(r, False);

    if mRet then
    begin
    	DataMain.QrDMVT.Requery;
        RefDMVT.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		EdCL.SetFocus;
        except
        end
    else if (ActiveControl = PaTotal) or (ActiveControl.Parent = PaTotal) then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdThamkhaoGiaExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmTheodoiGia, FrmTheodoiGia);
    with QrNX do
	    FrmTheodoiGia.Execute('NMUA',
            QrCT.FieldByName('MAVT').AsString,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdTotalExecute(Sender: TObject);
begin
    vlTotal1.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdPrintBaogiaExecute(Sender: TObject);
var
    s: String;
begin
    CmdSave.Execute;

    s := FORM_CODE + '_BAOGIA';
    DataOffice.CreateReport2('\XLS\' + s + '.xlsx',
        [sysLogonUID, TGuidEx.ToString(QrNX.FieldByName('KHOA'))], s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdPrintExcelExecute(Sender: TObject);
var
    s: String;
begin
    CmdSave.Execute;

    s := FORM_CODE + '_EXCEL';
    DataOffice.CreateReport2('\XLS\' + s  + '.xlsx',
        [sysLogonUID, TGuidEx.ToString(QrNX.FieldByName('KHOA'))], s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
        bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
//                        and (QrNX.FieldByName('TINHTRANG').AsString = '02');
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdPrint.Enabled := not bEmpty;
    CmdPrintExcel.Enabled := not bEmpty;
    CmdPrintBaogia.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdDmvt.Enabled := n = 1;

    CmdFromMin.Enabled := (not bEmpty) and (QrNX.FieldByName('MADT').AsString <> '');

    with QrCT do
    begin
    	if Active then
        	bEmpty := IsEmpty
        else
            bEmpty := False;
    end;

    if b1Ncc then
	    CbMADT.ReadOnly := not bEmpty;
    CmdEmptyDetail.Enabled := (not bEmpty) and mCanEdit;
    CbGia.ReadOnly := not bEmpty;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime    := d;
		FieldByName('MAKHO').AsString     := mMakho;
		FieldByName('TINHTRANG').AsString := '01';
        FieldByName('LCT').AsString       := mLCT;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('HINHTHUC_GIA').AsString    := Iif(fHTGia = '', '03', fHTGia);
        FieldByName('NGAY_DAT').AsDateTime := Date;
        FieldByName('DRC_STATUS').AsString    := '1';
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;
    if BlankConfirm(QrNX, ['NGAY', 'MAKHO', 'MADT']) then
        Abort;

    DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
		mMakho := FieldByName('MAKHO').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXBeforeEdit(DataSet: TDataSet);
var
    s: String;
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

    with QrNX do
    begin
        s := FieldByName('TINHTRANG').AsString;
        if s = '02' then
        begin
            if DataMain.GetFuncState('SZ_DDH_NCC_TINHTRANG') then
                if DataMain.GetRights('SZ_DDH_NCC_TINHTRANG') = R_DENY then
                    Abort
        end
        else if (s = '03') or (s = '04') then
        begin
            Msg('Phiếu đã được giao hàng');
            Abort;
        end;
    end;
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    mThanhtien, mThanhtoan: Double;
    mHinhthuc: String;
begin
    with Sender.DataSet do
    begin
        mHinhthuc := FieldByName('HINHTHUC_GIA').AsString;
        mThanhtien := FieldByName('THANHTIEN').AsFloat;

//        if mHinhthuc = '03' then     // Nguoc
//            mThanhtoan := mThanhtien - FieldByName('CL_THUE').AsFloat
//        else // Xuoi va truc tiep
            mThanhtoan := mThanhtien + FieldByName('CL_THUE').AsFloat;

        FieldByName('THANHTOAN').AsFloat := mThanhtoan;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXHINHTHUC_GIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if QrCT.IsEmpty then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXMADTChange(Sender: TField);
begin
	exDotMancc(Sender);
    CbNCC.Text := CbNCC.Field.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrNX.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXTINHTRANGValidate(Sender: TField);
begin
    if (Sender.AsString = '03') or (Sender.AsString = '04') then
    begin
        ErrMsg('Không được đổi sang tình trạng này.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrNXTL_CK_HDChange(Sender: TField);
begin
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('TL_CK_HD').AsFloat := Sender.AsFloat;

            Next;
        end;

        CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

    	if FieldByName('TINHTRANG').AsString <> '01' then
        begin
        	Msg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                d := FieldByName('SOLUONG').AsFloat /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

                if d = Trunc(d) then
                    FieldByName('SOLUONG2').AsFloat := d
                else
                    FieldByName('SOLUONG2').Clear;
            end
            else if Sender.FullName = 'SOLUONG2' then
            begin
                FieldByName('SOLUONG').AsFloat :=
                    FieldByName('SOLUONG2').AsFloat *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat);

        // Tinh tri gia tren BOX
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTSOTIENChange(Sender: TField);
var
    mHinhthuc: String;
    mSotien, tlck, mCk, mCkhd, mThue, mTs, mSl, mThanhtien,
        mSotien1, //Sau chiết khấu mặt hàng
        mSotien2: Double; //Sau chiết khấu hóa đơn
begin
    if mTriggerCK then
        Exit;

	with QrCT do
    begin
        mHinhthuc := QrNX.FieldByName('HINHTHUC_GIA').AsString;
        mSotien := FieldByName('SOTIEN').AsFloat;
        mTs := FieldByName('THUE_SUAT').AsFloat;

        if Sender.FieldName = 'TL_CK' then
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            mCk := exVNDRound(mSotien * tlck / 100.0)
        end
        else
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            tlck := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end;
        mSotien1 := mSotien -  mCk;

        mCkhd := exVNDRound( mSotien1 * FieldByName('TL_CK_HD').AsFloat / 100.0);
        mSotien2 := mSotien1 -  mCkhd;

        if mHinhthuc = '02' then          // Xuoi
        begin
        	// Thue
            mThue := exVNDRound(mSotien2 * mTs / 100.0);
            mThanhtien := mSotien2 + mThue;
        end
        else if mHinhthuc = '03' then     // Nguoc
        begin
            // Thue
            if mTs = 0 then
                mThue := 0
            else
                mThue := exVNDRound(mSotien2 / (100/mTs + 1));
            mThanhtien := mSotien2;
        end;

    	mTriggerCK := True;
        FieldByName('TL_CK').AsFloat := tlck;
    	FieldByName('CHIETKHAU').AsFloat := mCk;
        FieldByName('CHIETKHAU_HD').AsFloat := mCkhd;
        mTriggerCK := False;
        if mTs = 5 then
        begin
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_5').AsFloat := mThue;
        end
        else if mTs = 10 then
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := mThue;
        end else
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := mThue;
        end;

        FieldByName('TIEN_THUE').AsFloat := mThue;
        FieldByName('SOTIEN1').AsFloat := mSotien1;
        FieldByName('SOTIEN2').AsFloat := mSotien2;
        FieldByName('THANHTIEN').AsFloat := mThanhtien;
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTTL_CKValidate(Sender: TField);
var
    b: Boolean;
begin
    if Sender.AsFloat = 0 then
        Exit;

    b := False;
    with QrCT do
    begin
        if (Sender.AsFloat < 0) then
            b := True
        else
        if ((Sender.FieldName = 'TL_CK') and (Sender.AsFloat > 100)) or
            ((Sender.FieldName = 'CHIETKHAU') and (Sender.AsFloat > FieldByName('SOTIEN').AsFloat))  then
            b := True
    end;

    if b then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTMAVTChange(Sender: TField);
var
	ck, gia: Double;
begin
    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

    if b1Ncc then
		exDotMavt(1, RefDMVT, Sender, 'MADT=''' + QrNX.FieldByName('MADT').AsString + '''')
    else
		exDotMavt(1, RefDMVT, Sender);

    with QrCT do
    begin
        // Ten, dvt
        FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;
        FieldByName('LOAITHUE').AsString := FieldByName('LK_LOAITHUE').AsString;

        ck := 0;
        gia := 0;
        DataMain.DmvtGetRef(Sender.AsString, mLCT, QrNX.FieldByName('MADT').AsString,
                QrNX.FieldByName('HINHTHUC_GIA').AsString, ck, gia);

        FieldByName('GIAVON').AsFloat := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA_REF').AsFloat := gia;
        FieldByName('DONGIA').AsFloat := gia;
        FieldByName('TL_CK').AsFloat := ck;
        FieldByName('TL_CK_HD').AsFloat := QrNX.FieldByName('TL_CK_HD').AsFloat;

        if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;

    end;
	GrDetail.InvalidateCurrentRow;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
    s: String;
begin
    if Highlight then
    begin
        AFont.Color := clWhite;
        Exit;
    end;

    s := QrNX.FieldByName('TINHTRANG').AsString;
    if s = '01' then
        AFont.Color := clBlue
    else if s = '02' then
        AFont.Color := clRed
    else if s = '03' then
        AFont.Color := clPurple
    else
        AFont.Color := clBlack;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('CHIETKHAU').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('CHIETKHAU_MH').AsFloat);
        ColumnByName('CHIETKHAU_HD').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('CHIETKHAU_HD').AsFloat);
		ColumnByName('TIEN_THUE').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THUE').AsFloat);
        ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN1').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN1').AsFloat);
        ColumnByName('SOTIEN2').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN2').AsFloat);
        ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THANHTIEN').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.BtCongnoClick(Sender: TObject);
begin
    if DataMain.GetRights('SZ_CONGNO_NCC') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.BtThueClick(Sender: TObject);
begin
    Application.CreateForm(TFrmTienthue, FrmTienthue);
    FrmTienthue.Execute(DsNX)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdFilterRefExecute(Sender: TObject);
var
	n: Integer;
    s: String;
begin
	n := refType;
    s := '';
	if not FrmChonDsma.Get(n, s) then
    	Exit;

    // Requery RefDMVT
    refType := n;
    with RefDMVT do
    begin
    	Close;
        SQL.Text := refSQL;

        if s <> '' then
	        case n of
            0:
            	SQL.Add('and b.MANGANH in (' + s + ')');
            1:
            	SQL.Add('and b.MANHOM in (' + s + ')');
            2:
            	SQL.Add('and a.MAVT in (' + s + ')');
            end;

        SQL.Add('order by a.TENVT');
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Đơn đặt hàng có mặt hàng sai mã. Tiếp tục?';
procedure TFrmDondhNCC.CmdFromOrderExecute(Sender: TObject);
var
	n: TGUID;
begin
    QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmChonDondh3, FrmChonDondh3);
    with QrNX do
	    n := FrmChonDondh3.Execute(bCheck,
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 'DM_NCC', 'DHN', '');
	FrmChonDondh3.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;
    // Lay chi tiet
	GetDetailFrom(n, LAYDONDH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

    // Get file name
    mFile := isGetOpenFileName('XLS;XLSX', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        if bDuplicate then
                        begin
                            QrCT.Append;
                            QrCT.FieldByName('MAVT').AsString := s
                        end else
                        begin
                            if (not QrCT.Locate('MAVT', s, []))  then
                            begin
                                QrCT.Append;
                                QrCT.FieldByName('MAVT').AsString := s
                            end
                            else
                                SetEditState(QrCT);
                        end;

                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdFromNXExecute(Sender: TObject);
var
	n: TGUID;
begin
    QrCT.CheckBrowseMode;

	// Chon phieu nhap
	Application.CreateForm(TFrmChonPhieuNX, FrmChonPhieuNX);
    with QrNX do
	    n := FrmChonPhieuNX.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString);
	FrmChonPhieuNX.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;
    GetDetailFrom(n, CHUNGTU_LAYPHIEU);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)  
function TFrmDondhNCC.GetDetailFrom(pKhoa: TGUID;
  pStoredPro: TADOStoredProc; b: Boolean = False): Boolean;
var
	mDT, mKho, mMavt, mLThue: String;
    mSoluong, mDongia, mCK, mCkhd: Double;
begin
	Result := False;

     // Lay chi tiet
    with pStoredPro do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            // Cap nhat nha cung cap
            mDT := FieldByName('MADT').AsString;
            mKho := FieldByName('MAKHO').AsString;
            mCkhd := FieldByName('TL_CK_HD').AsFloat;

            if (QrNX.FieldByName('MADT').AsString <> mDT) or
               (QrNX.FieldByName('MAKHO').AsString <> mKho) or
               (QrNX.FieldByName('TL_CK_HD').AsFloat <> mCkhd)then
            begin
                SetEditState(QrNX);
                QrNX.FieldByName('MADT').AsString := mDT;
                QrNX.FieldByName('MAKHO').AsString := mKho;
                QrNX.FieldByName('TL_CK_HD').AsFloat := mCkhd;
            end;

            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MAVT').AsString;
                if bCheck then
                    mSoluong := FieldByName('SOLUONG_CONLAI').AsFloat
                else
                    mSoluong := FieldByName('SOLUONG').AsFloat;
                mDongia := FieldByName('DONGIA').AsFloat;
                mLThue := FieldByName('LOAITHUE').AsString;
                mCK := FieldByName('TL_CK').AsFloat;

                with QrCT do
                begin
                    Append;
                    FieldByName('MAVT').AsString := mMavt;
                    FieldByName('DONGIA').AsFloat := mDongia;
                    FieldByName('SOLUONG').AsFloat := mSoluong;
                    FieldByName('TL_CK').AsFloat := mCK;
                    FieldByName('LOAITHUE').AsString := mLThue;
                end;

                Next;
            end;

            Active := False;
            QrCT.CheckBrowseMode;
        finally
            ClearWait;
        end;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdFromMinExecute(Sender: TObject);
var
	mMavt: String;
begin
	with DONDH_LUONG_TON do
    begin
    	Prepared := True;
        Parameters[1].Value := QrNX.FieldByName('NGAY').AsDateTime;
        Parameters[2].Value := QrNX.FieldByName('MADT').AsString;
        Parameters[3].Value := QrNX.FieldByName('MAKHO').AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MABH').AsString;

                with QrCT do
                begin
                    Append;
                    FieldByName('MAVT').AsString := mMavt;
                end;

                Next;
            end;

            Close;
            QrCT.CheckBrowseMode;
        finally
            ClearWait;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.CmdExBarcodeExecute(Sender: TObject);
begin
    CmdSave.Execute;
    DataMain.LabelExport(TGuidEx.ToString(QrNX.FieldByName('KHOA')), 'DONDH');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := RoundUp(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger))
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := RoundUp(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTLOAITHUEChange(Sender: TField);
begin
    with QrCT do
    begin
        if FieldByName('LOAITHUE').AsString = 'TTT' then
            FieldByName('THUE_SUAT').Clear
        else
            FieldByName('THUE_SUAT').AsFloat := FieldByName('LK_VAT_VAO').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhNCC.QrCTMAVTValidate(Sender: TField);
var
	s: String;
    bm: TBytes;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
	if s = '' then
    	Exit;

    if not bDuplicate then
    //Trung mat hang
    with QrCT do
    begin
        if IsDuplicateCode2(QrCT, Sender, bm) then
        begin
            //if YesNo(RS_DUPLICATE_CODE) then
            begin
                try
                    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
                    Abort;
                finally
                    Cancel;
                    GotoBookmark(bm);
                    Edit;
                    mgMyThread := TExThread.Create(escapeKey, 50);
                end;
            end;
        end;
    end;

	// Nhieu nha cung cap
	if not b1Ncc then
    	Exit;

	// Validate MADT
    if IsDotSelect(s) = 0 then
        with CHECK_NCC do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrNX.FieldByName('MADT').AsString;
            if Execute.RecordCount < 1 then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL2);
                Abort;
            end;
        end;
end;

end.
