(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Lienhe;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmLienhe = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    ToolButton9: TToolButton;
    QrDetail: TADOQuery;
    Image1: TImage;
    QrDetailHOTEN: TWideStringField;
    QrDetailDTHOAI: TWideStringField;
    QrDetailEMAIL: TWideStringField;
    QrDetailGHICHU: TWideStringField;
    QrDetailCREATE_BY: TIntegerField;
    QrDetailUPDATE_BY: TIntegerField;
    QrDetailCREATE_DATE: TDateTimeField;
    QrDetailUPDATE_DATE: TDateTimeField;
    CmdAudit: TAction;
    QrDetailKHOACT: TGuidField;
    QrDetailKHOA: TGuidField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrDetailBeforeInsert(DataSet: TDataSet);
    procedure GrListURLOpen(Sender: TObject; var URLLink: String;
      Field: TField; var UseDefault: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
  private
    mCanEdit: Boolean;
    mKhoa: TGUID;
  	mTrigger: Boolean;
  public
  	procedure Execute(pCanEdit: Boolean; pKhoa: TGUID; pMa, pTen: String);
  end;

var
  FrmLienhe: TFrmLienhe;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'NGUOI_LIEN_LAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.Execute;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;

    mKhoa := pKhoa;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdNewExecute(Sender: TObject);
begin
	QrDetail.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDetail, FORM_CODE);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.FormShow(Sender: TObject);
begin
	OpenDataSets([QrDetail]);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailBeforePost(DataSet: TDataSet);
begin
	with QrDetail do
	begin
		if BlankConfirm(QrDetail, ['HOTEN']) then
			Abort;
        if State in [dsInsert] then
        begin
            TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
            TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
        end;
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := TGuidEx.ToString(mKhoa);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.GrListURLOpen(Sender: TObject; var URLLink: String;
  Field: TField; var UseDefault: Boolean);
begin
    if URLLink = '' then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLienhe.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDetail);
end;

end.
