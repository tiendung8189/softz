(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonKh;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid;

type
  TFrmChonKh = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsKHACHHANG: TDataSource;
    QrKHACHHANG: TADOQuery;
    QrKHACHHANGKHOA: TIntegerField;
    QrKHACHHANGNGAY: TDateTimeField;
    QrKHACHHANGSCT: TWideStringField;
    QrKHACHHANGMAKHO: TWideStringField;
    QrKHACHHANGDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbKH: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMAKH: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMKH: TADOQuery;
    QrKHACHHANGTENKHO: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrKHACHHANGMAKH: TWideStringField;
    QrKHACHHANGNG_GIAO: TWideStringField;
    QrKHACHHANGNG_NHAN: TWideStringField;
    QrKHACHHANGTENKH: TWideStringField;
    QrKHACHHANGHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMAKHChange(Sender: TObject);
    procedure CbMAKHExit(Sender: TObject);
    procedure CbMAKHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
	mKH, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pFix: Boolean; pKH, pKho: String): Integer;
  end;

var
  FrmChonKh: TFrmChonKh;

implementation

uses
	isDb, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonKh.Execute;
begin
	mKH := pKH;
    mKho := pKho;
    CbMAKH.Enabled := not pFix;
    CbKH.Enabled := not pFix;

	if ShowModal = mrOK then
		Result := QrKHACHHANG.FieldByName('KHOA').AsInteger
    else
		Result := 0;

	CloseDataSets([QrKHACHHANG, QrDMKH, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_KHACHHANG', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrKHACHHANG, QrDMKH, QrDMKHO]);
	mSQL := QrKHACHHANG.SQL.Text;
    CbMAKH.LookupValue := mKH;
    CbMAKHO.LookupValue := mKho;

    // Smart focus
    if mKH = '' then
    	try
    		CbMAKH.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mKH := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsKHACHHANG);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CmdRefreshExecute(Sender: TObject);
var
    s, Kh, Kho: String;
    Tungay, Denngay: TDateTime;
begin
    Kh      := CbMAKH.LookupValue;
    Kho     := CbMAKHO.LookupValue;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mKH <> Kh)          or (mKHO <> Kho)         then
	begin
   	    mKH := Kh;
        mKHO := Kho;

        with QrKHACHHANG do
        begin
			Close;

            s := mSQL;
            if mKH <> '' then
            	s := s + 'and a.MAKH=''' + mKH + '''';
			if mKHO <> '' then
            	s := s + 'and a.MAKHO=''' + mKHO + '''';
            SQL.Text := s;

            Parameters[0].Value := mTungay;
            Parameters[1].Value := mDenngay;
            
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CbMAKHChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbKH.LookupValue := s;
    // Ten NCC
    1:
        CbMAKH.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CbMAKHExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CbMAKHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

end.
