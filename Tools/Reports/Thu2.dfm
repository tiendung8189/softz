object FrmThu2: TFrmThu2
  Left = 90
  Top = 37
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Phi'#7871'u Thu Theo Ch'#7913'ng T'#7915' Xu'#7845't '
  ClientHeight = 573
  ClientWidth = 823
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    823
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 823
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 823
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object SepChecked: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton11: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 823
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 815
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 815
        Height = 483
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GrBrowse: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 815
          Height = 434
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'IMG;ImageIndex;Original Size'
            'IMG2;ImageIndex;Original Size')
          Selected.Strings = (
            'IMG'#9'3'#9#9'F'
            'IMG2'#9'3'#9#9'F'
            'NGAY'#9'18'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
            'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
            'SOTIEN'#9'12'#9'Thanh to'#225'n'#9'F'
            'CALC_SOTIEN'#9'15'#9'T'#7893'ng c'#7897'ng'#9'F'
            'MADT'#9'10'#9'M'#227#9'F'#9#272#417'n v'#7883' n'#7897'p'
            'LK_TENDT'#9'40'#9'T'#234'n'#9'F'#9#272#417'n v'#7883' n'#7897'p'
            'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
            'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho h'#224'ng'
            'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgAllowInsert]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopMaster
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnDblClick = GrBrowseDblClick
          OnEnter = CmdRefreshExecute
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
        inline frDate: TfrNGAY
          Left = 0
          Top = 0
          Width = 815
          Height = 49
          Align = alTop
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 815
          inherited Panel1: TPanel
            Width = 815
            ParentColor = False
            ExplicitWidth = 815
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 815
        Height = 242
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        object Panel1: TPanel
          Left = 2
          Top = 2
          Width = 811
          Height = 81
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object DBText1: TDBText
            Left = 6
            Top = 4
            Width = 21
            Height = 17
            DataField = 'XOA'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 77
            Top = 14
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object Label2: TLabel
            Left = 284
            Top = 14
            Width = 50
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' phi'#7871'u'
          end
          object Label25: TLabel
            Left = 540
            Top = 14
            Width = 54
            Height = 16
            Alignment = taRightJustify
            Caption = 'H'#236'nh th'#7913'c'
          end
          object Label31: TLabel
            Left = 52
            Top = 38
            Width = 53
            Height = 16
            Alignment = taRightJustify
            Caption = 'Kho h'#224'ng'
          end
          object LbDV: TLabel
            Left = 44
            Top = 62
            Width = 61
            Height = 16
            Alignment = taRightJustify
            Caption = #272#417'n v'#7883' n'#7897'p'
          end
          object BtCongno: TSpeedButton
            Left = 502
            Top = 57
            Width = 23
            Height = 22
            Cursor = 1
            Hint = 'Xem c'#244'ng n'#7907' kh'#225'ch h'#224'ng'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000F0C575FFF0C5
              75FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C5
              75FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF3F9200FFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FFFFFFFFFFFFFFFFFF3F92
              00FFFFFFFFFF3F9200FFC37C00FFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC37C00FF3F9200FFFFFF
              FFFFFFFFFFFFC37C00FF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFC37C00FFFFFF
              FFFFC37C00FFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFC37C
              00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFF0C575FFFFFF
              FFFF1D25DAFFFFFFFFFFFFFFFFFF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFF1D25DAFF3F9200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFF3F9200FF1D25DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFF3F9200FFFFFFFFFFFFFFFFFF1D25DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D25DAFF1D25DAFF1D25DAFF1D25
              DAFF1D25DAFF1D25DAFF1D25DAFF1D25DAFF1D25DAFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C575FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            OnClick = BtCongnoClick
          end
          object CbMakho: TwwDBLookupCombo
            Left = 112
            Top = 34
            Width = 53
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'MAKHO'#9'6'#9'M'#183#9'F'
              'TENKHO'#9'40'#9'T'#170'n'#9'F')
            DataField = 'MAKHO'
            DataSource = DsTC
            LookupTable = DataMain.QrDMKHO
            LookupField = 'MAKHO'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            OnBeforeDropDown = CbMakhoBeforeDropDown
            OnCloseUp = CbMakhoCloseUp
            OnNotInList = CbMakhoNotInList
ButtonEffects.Transparent=True
          end
          object CbNgay: TwwDBDateTimePicker
            Left = 112
            Top = 10
            Width = 165
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NGAY'
            DataSource = DsTC
            Epoch = 1950
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ShowButton = True
            TabOrder = 0
          end
          object CbPTTT: TwwDBLookupCombo
            Left = 601
            Top = 10
            Width = 181
            Height = 22
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DGIAI'#9'0'#9'L'#253' do'#9'F')
            DataField = 'PTTT'
            DataSource = DsTC
            LookupTable = DataMain.QrPTTT
            LookupField = 'MA'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
            OnNotInList = CbMakhoNotInList
ButtonEffects.Transparent=True
          end
          object CbTenkho: TwwDBLookupCombo
            Left = 168
            Top = 34
            Width = 357
            Height = 22
            TabStop = False
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'TENKHO'#9'40'#9'T'#170'n'#9'F'
              'MAKHO'#9'6'#9'M'#183#9'F')
            DataField = 'MAKHO'
            DataSource = DsTC
            LookupTable = DataMain.QrDMKHO
            LookupField = 'MAKHO'
            Options = [loColLines]
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = CbMakhoCloseUp
            OnNotInList = CbMakhoNotInList
ButtonEffects.Transparent=True
          end
          object EdMaDT: TwwDBEdit
            Left = 112
            Top = 58
            Width = 101
            Height = 22
            Ctl3D = False
            DataField = 'MADT'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdSCT: TwwDBEdit
            Left = 340
            Top = 10
            Width = 185
            Height = 22
            TabStop = False
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'SCT'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdTenDT: TwwDBEdit
            Left = 216
            Top = 58
            Width = 285
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'LK_TENDT'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 131
          Width = 811
          Height = 109
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label10: TLabel
            Left = 63
            Top = 29
            Width = 42
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ghi ch'#250
          end
          object Label18: TLabel
            Left = 619
            Top = 53
            Width = 35
            Height = 16
            Alignment = taRightJustify
            Caption = 'H'#7895' tr'#7907
          end
          object Label5: TLabel
            Left = 47
            Top = 5
            Width = 58
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#432#7901'i n'#7897'p'
          end
          object TntLabel1: TLabel
            Left = 602
            Top = 29
            Width = 52
            Height = 16
            Alignment = taRightJustify
            Caption = 'Tr'#7843' h'#224'ng'
          end
          object TntLabel2: TLabel
            Left = 594
            Top = 5
            Width = 60
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#7893'ng c'#7897'ng'
          end
          object Label6: TLabel
            Left = 589
            Top = 77
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = 'Thanh to'#225'n'
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object edMaKH: TwwDBEdit
            Left = 112
            Top = 1
            Width = 241
            Height = 22
            Ctl3D = False
            DataField = 'NGUOI'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdSOTIEN: TwwDBEdit
            Left = 661
            Top = 25
            Width = 121
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = 15794175
            Ctl3D = False
            DataField = 'SOTIEN2'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit1: TwwDBEdit
            Left = 661
            Top = 49
            Width = 121
            Height = 22
            BorderStyle = bsNone
            Color = 15794175
            Ctl3D = False
            DataField = 'SOTIEN3'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit2: TwwDBEdit
            Left = 661
            Top = 1
            Width = 121
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = 15794175
            Ctl3D = False
            DataField = 'SOTIEN'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DBMemo1: TDBMemo
            Left = 112
            Top = 25
            Width = 413
            Height = 70
            Ctl3D = False
            DataField = 'DGIAI'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
          end
          object wwDBEdit3: TwwDBEdit
            Left = 661
            Top = 73
            Width = 121
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = 15794175
            Ctl3D = False
            DataField = 'THANHTOAN'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaTK: TPanel
          Left = 2
          Top = 83
          Width = 811
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 49
            Top = 7
            Width = 56
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#224'i kho'#7843'n'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 44
            Top = 29
            Width = 61
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#226'n h'#224'ng'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object CbSTK: TwwDBLookupCombo
            Left = 112
            Top = 1
            Width = 165
            Height = 22
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'MATK'#9'23'#9'MATK'#9#9
              'TENTK'#9'33'#9'TENTK'#9'F')
            DataField = 'MATK'
            DataSource = DsTC
            LookupTable = DataMain.QrDMTK_NB
            LookupField = 'MATK'
            Options = [loColLines]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
            OnBeforeDropDown = CbSTKBeforeDropDown
            OnCloseUp = CbMakhoCloseUp
            OnNotInList = CbMakhoNotInList
ButtonEffects.Transparent=True
          end
          object CbTenTK: TwwDBLookupCombo
            Left = 280
            Top = 1
            Width = 245
            Height = 22
            TabStop = False
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            BorderStyle = bsNone
            DropDownAlignment = taRightJustify
            Selected.Strings = (
              'TENTK'#9'35'#9'TENTK'#9'F'
              'MATK'#9'23'#9'MATK'#9#9)
            DataField = 'MATK'
            DataSource = DsTC
            LookupTable = DataMain.QrDMTK_NB
            LookupField = 'MATK'
            Options = [loColLines]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            UseTFields = False
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
            OnBeforeDropDown = CbSTKBeforeDropDown
            OnCloseUp = CbMakhoCloseUp
            OnNotInList = CbMakhoNotInList
ButtonEffects.Transparent=True
          end
          object EdNganhang: TwwDBEdit
            Left = 112
            Top = 25
            Width = 237
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'LK_NGANHANG'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdChinhanh: TwwDBEdit
            Left = 352
            Top = 25
            Width = 173
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'LK_CHINHANH'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 336
        Width = 815
        Height = 168
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 815
          Height = 152
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'CALC_STT'#9'3'#9'STT'#9'T'
            'LK_NGAY'#9'18'#9'Ng'#224'y'#9'T'#9'Ch'#7913'ng t'#7915
            'LK_SCT'#9'17'#9'S'#7889#9'T'#9'Ch'#7913'ng t'#7915
            'NGAYTT'#9'10'#9'H'#7841'n t.to'#225'n'#9'T'#9'Ch'#7913'ng t'#7915
            'LK_HOADON_SO'#9'12'#9'S'#7889#9'T'#9'H'#243'a '#273#417'n'
            'LK_HOADON_SERI'#9'8'#9'Seri'#9'T'#9'H'#243'a '#273#417'n'
            'LK_HOADON_NGAY'#9'10'#9'Ng'#224'y'#9'T'#9'H'#243'a '#273#417'n'
            'SODU'#9'13'#9'S'#7889' d'#432#9'T'#9'S'#7889' ti'#7873'n'
            'SOTIEN'#9'13'#9'Thanh to'#225'n'#9'F'#9'S'#7889' ti'#7873'n'
            'CALC_CONLAI'#9'13'#9'C'#242'n l'#7841'i'#9'T'#9'S'#7889' ti'#7873'n'
            'LK_DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'T')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopNX
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
      object PaTra: TisPanel
        Left = 0
        Top = 242
        Width = 815
        Height = 94
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Phi'#7871'u nh'#7853'p tr'#7843
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrNhaptra: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 815
          Height = 78
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'CALC_STT'#9'3'#9'STT'#9'F'
            'LK_NGAY'#9'18'#9'Ng'#224'y'#9'F'
            'LK_SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
            'LK_THANHTOAN'#9'13'#9'S'#7889' ti'#7873'n'#9'F'
            'LK_DGIAI'#9'60'#9'Di'#7877'n gi'#7843'i'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT2
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopNT
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 16119285
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 681
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 681
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 532
    Top = 444
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdSwitch: TAction
      Caption = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdChonCT2: TAction
      Category = 'CHITIET'
      Caption = 'Ch'#7885'n phi'#7871'u nh'#7853'p tr'#7843
      OnExecute = CmdChonCT2Execute
    end
    object CmdChonCT: TAction
      Category = 'CHITIET'
      Caption = 'Ch'#7885'n phi'#7871'u xu'#7845't'
      OnExecute = CmdChonCTExecute
    end
    object CmdTotal: TAction
      Hint = 'T'#237'nh t'#7893'ng s'#7889' ti'#7873'n thanh to'#225'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdEmpty: TAction
      Category = 'CHITIET'
      Caption = 'X'#243'a chi ti'#7871't'
      Hint = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTC
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MST'
      'LK_PTTT'
      'NGUOI')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 504
    Top = 444
  end
  object QrTC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTCBeforeOpen
    BeforeInsert = QrTCBeforeInsert
    AfterInsert = QrTCAfterInsert
    BeforeEdit = QrTCBeforeEdit
    BeforePost = QrTCBeforePost
    AfterPost = QrTCAfterPost
    AfterCancel = QrTCAfterCancel
    AfterScroll = QrTCAfterScroll
    OnCalcFields = QrTCCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'THUCHI'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 588
    Top = 444
    object QrTCIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrTCXOA: TWideStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrTCIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrTCCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrTCLK_PTTT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = DataMain.QrPTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Size = 100
      Lookup = True
    end
    object QrTCLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrTCLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n '#273#417'n v'#7883
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDM_KH_NCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 50
      Lookup = True
    end
    object QrTCCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTCUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTCDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrTCCALC_SOTIEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN'
      Calculated = True
    end
    object QrTCCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrTCUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrTCDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrTCCALC_CONLAI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_CONLAI'
      Calculated = True
    end
    object QrTCLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrTCNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrTCSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrTCMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 2
    end
    object QrTCPTTT: TWideStringField
      FieldName = 'PTTT'
      OnChange = QrTCPTTTChange
      Size = 2
    end
    object QrTCMST: TWideStringField
      FieldName = 'MST'
      Size = 30
    end
    object QrTCNGUOI: TWideStringField
      FieldName = 'NGUOI'
      Size = 50
    end
    object QrTCSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrTCSOTIENChange
    end
    object QrTCSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
      OnChange = QrTCSOTIENChange
    end
    object QrTCDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrTCSODU: TFloatField
      FieldName = 'SODU'
    end
    object QrTCTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrTCLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 2
    end
    object QrTCSOTIEN3: TFloatField
      FieldName = 'SOTIEN3'
      OnChange = QrTCSOTIENChange
    end
    object QrTCMADT: TWideStringField
      DisplayLabel = #272#417'n v'#7883' n'#7897'p'
      FieldName = 'MADT'
      OnChange = QrTCMADTChange
      OnValidate = QrTCMADTValidate
      Size = 15
    end
    object QrTCKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrTCLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrTCMATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'MATK'
      Size = 50
    end
    object QrTCLK_TENTK: TWideStringField
      DisplayLabel = 'Ch'#7911' t'#224'i kho'#7843'n'
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_MANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'MANH'
      KeyFields = 'MATK'
      Size = 100
      Lookup = True
    end
    object QrTCLK_MACN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MACN'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'MACN'
      KeyFields = 'MATK'
      Size = 100
      Lookup = True
    end
    object QrTCLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'LK_MANH'
      Size = 200
      Lookup = True
    end
    object QrTCLK_CHINHANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CHINHANH'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MACN'
      LookupResultField = 'TENCN'
      KeyFields = 'LK_MACN'
      Size = 200
      Lookup = True
    end
    object QrTCDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      Size = 1
    end
  end
  object DsTC: TDataSource
    DataSet = QrTC
    Left = 588
    Top = 472
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 89
    Top = 380
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 55
    Top = 380
  end
  object QrCHITIET: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCHITIETBeforeOpen
    AfterInsert = QrCHITIETAfterEdit
    BeforeEdit = QrCHITIETBeforeEdit
    AfterEdit = QrCHITIETAfterEdit
    BeforePost = QrCHITIETBeforePost
    AfterCancel = QrCHITIETAfterCancel
    BeforeDelete = QrCHITIETBeforeDelete
    AfterDelete = QrCHITIETAfterDelete
    OnCalcFields = QrCHITIETCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'THUCHI_CT'
      ' where '#9'KHOA =:KHOA'
      'order by '#9'NGAYTT')
    Left = 616
    Top = 444
    object QrCHITIETCALC_STT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CALC_STT'
      Calculated = True
    end
    object QrCHITIETLK_PTTT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'TEN_PTTT'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIETSODU: TFloatField
      FieldName = 'SODU'
      OnChange = QrCHITIETSODUChange
    end
    object QrCHITIETSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrCHITIETSOTIENChange
      OnValidate = QrCHITIETSOTIENValidate
    end
    object QrCHITIETLK_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_NGAY'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_SCT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_SCT'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'SCT'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_THANHTOAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_THANHTOAN'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'THANHTOAN'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETCALC_CONLAI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_CONLAI'
      Calculated = True
    end
    object QrCHITIETLK_HOADON_SO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SO'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SO'
      KeyFields = 'KHOANX'
      Size = 10
      Lookup = True
    end
    object QrCHITIETLK_HOADON_SERI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SERI'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SERI'
      KeyFields = 'KHOANX'
      Size = 10
      Lookup = True
    end
    object QrCHITIETLK_HOADON_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_NGAY'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_DGIAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DGIAI'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'DGIAI'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIETNGAYTT: TDateTimeField
      FieldName = 'NGAYTT'
    end
    object QrCHITIETSCT2: TWideStringField
      FieldName = 'SCT2'
    end
    object QrCHITIETGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCHITIETLK_TC_SOTIEN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_TC_SOTIEN'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'TC_SOTIEN'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETTHANHTOAN_NX: TFloatField
      FieldName = 'THANHTOAN_NX'
    end
    object QrCHITIETTC_SOTIEN_NX: TFloatField
      FieldName = 'TC_SOTIEN_NX'
    end
    object QrCHITIETLK_PHIEUGIAOHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PHIEUGIAOHANG'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'PHIEUGIAOHANG'
      KeyFields = 'KHOANX'
      Size = 0
      Lookup = True
    end
    object QrCHITIETKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrCHITIETKHOANX: TGuidField
      FieldName = 'KHOANX'
      FixedChar = True
      Size = 38
    end
  end
  object DsCT: TDataSource
    DataSet = QrCHITIET
    Left = 616
    Top = 472
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  a.*, b.DGIAI "TEN_PTTT"'
      '  from'
      '    ('
      'select  b.*'
      '  from  THUCHI_CT a, CHUNGTU b'
      ' where  a.KHOA =:KHOA'
      '   and  a.KHOANX = b.KHOA'
      'union all'
      'select  *'
      '  from  T_CHUNGTU'
      ' where  (LCT in ('#39'XBAN'#39', '#39'NTRA'#39',  '#39'XDC'#39', '#39'TCNO'#39'))'
      '   and'#9'MADT = :MADT'
      '   and  isnull(TC_XONG, 0) = 0'
      '    ) a left join DM_PT_THANHTOAN b on a.PTTT = b.MA'
      'order by a.KHOA')
    Left = 364
    Top = 396
  end
  object PopNT: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 121
    Top = 380
    object Chnphiunhptr1: TMenuItem
      Action = CmdChonCT2
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
  object QrCHITIET2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCHITIET2BeforeOpen
    AfterInsert = QrCHITIET2AfterEdit
    AfterEdit = QrCHITIET2AfterEdit
    BeforePost = QrCHITIETBeforePost
    AfterCancel = QrCHITIET2AfterCancel
    BeforeDelete = QrCHITIET2BeforeDelete
    AfterDelete = QrCHITIET2AfterDelete
    OnCalcFields = QrCHITIET2CalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'THUCHI_TRAHANG'
      ' where '#9'KHOA =:KHOA'
      'order by '#9'KHOACT')
    Left = 644
    Top = 444
    object QrCHITIET2CALC_STT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CALC_STT'
      Calculated = True
    end
    object QrCHITIET2LK_NGAY: TDateTimeField
      DisplayWidth = 18
      FieldKind = fkLookup
      FieldName = 'LK_NGAY'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_SCT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_SCT'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'SCT'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_THANHTOAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_THANHTOAN'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'THANHTOAN'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_DGIAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DGIAI'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'DGIAI'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_SERI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SERI'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SERI'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_SO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SO'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SO'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_NGAY'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2SOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrCHITIET2SOTIENChange
    end
    object QrCHITIET2KHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2LOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrCHITIET2KHOANX: TGuidField
      FieldName = 'KHOANX'
      OnChange = QrCHITIET2KHOANXChange
      FixedChar = True
      Size = 38
    end
  end
  object DsCT2: TDataSource
    DataSet = QrCHITIET2
    Left = 644
    Top = 472
  end
  object QrTRAHANG2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from  T_CHUNGTU'
      ' where  LCT = '#39'NTRA'#39
      '   and  MADT = :MADT'
      '   and  isnull(TC_XONG, 0) = 0'
      'order by NGAY, SCT')
    Left = 456
    Top = 396
    object QrTRAHANG2NGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrTRAHANG2SCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrTRAHANG2THANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrTRAHANG2DGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrTRAHANG2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTRAHANG2HOADON_SERI: TWideStringField
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrTRAHANG2HOADON_SO: TWideStringField
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrTRAHANG2HOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
  end
  object QrTRAHANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  x.*'
      '  from'
      '    ('
      '    select  b.*'
      '      from  THUCHI_TRAHANG a, CHUNGTU b'
      '     where  a.KHOA =:KHOA'
      '       and  a.KHOANX = b.KHOA'
      '    union all'
      '    select  *'
      '      from  T_CHUNGTU'
      '     where  LCT = '#39'NTRA'#39
      '       and  MADT = :MADT'
      '       and  isnull(TC_XONG, 0) = 0'
      '    ) x'
      'order by x.KHOA')
    Left = 428
    Top = 396
  end
  object PhieuCT2: TwwSearchDialog
    Selected.Strings = (
      'NGAY'#9'18'#9'Ng'#224'y'#9'F'
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
      'THANHTOAN'#9'12'#9'S'#7889' ti'#7873'n'#9'F'
      'DGIAI'#9'40'#9'Di'#7877'n gi'#7843'i'#9'F')
    GridTitleAlignment = taCenter
    GridColor = clWhite
    Options = [opShowOKCancel, opShowStatusBar]
    GridOptions = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgPerfectRowFit]
    ShadowSearchTable = QrTRAHANG2
    Caption = 'Ch'#7885'n'
    MaxWidth = 0
    MaxHeight = 209
    CharCase = ecNormal
    UseTFields = False
    OnInitDialog = PhieuCT2InitDialog
    Left = 56
    Top = 412
  end
  object QrNX2: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterOpen = QrNX2AfterOpen
    ProcedureName = 'TC_CHUNGTU_XUAT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 392
    Top = 396
  end
  object PopNX: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 153
    Top = 380
    object MenuItem1: TMenuItem
      Action = CmdChonCT
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Xachitit2: TMenuItem
      Action = CmdEmpty
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrTC
    DetailDataSet = QrCHITIET
    MasterFields.Strings = (
      'SODU'
      'SOTIEN')
    DetailFields.Strings = (
      'SODU'
      'SOTIEN')
    Left = 144
    Top = 424
  end
  object vlTotal2: TisTotal
    MasterDataSet = QrTC
    DetailDataSet = QrCHITIET2
    MasterFields.Strings = (
      'SOTIEN2')
    DetailFields.Strings = (
      'SOTIEN')
    Left = 184
    Top = 424
  end
end
