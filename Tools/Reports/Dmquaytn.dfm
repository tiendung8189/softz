object FrmDmquaytn: TFrmDmquaytn
  Left = 175
  Top = 122
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch Qu'#7847'y Thu Ng'#226'n'
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 85
    Width = 792
    Height = 467
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'TENMAY;CustomEdit;CbTenmay;F'
      'PRINTER;CustomEdit;CbPrinter;F'
      'TENKHO;CustomEdit;CbKho;F')
    PictureMasks.Strings = (
      'TENMAY'#9'*!'#9'T'#9'T'
      'QUAY'#9'*!'#9'T'#9'T')
    Selected.Strings = (
      'TENMAY'#9'25'#9'T'#234'n m'#225'y'#9'F'
      'QUAY'#9'14'#9'Qu'#7847'y s'#7889#9'F'
      'PRINTER'#9'23'#9'M'#225'y in'#9'F'
      'GHICHU'#9'43'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDMQUAY
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton8: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object CbTenmay: TwwDBComboBox
    Left = 182
    Top = 359
    Width = 121
    Height = 24
    ShowButton = True
    Style = csDropDown
    MapList = False
    AllowClearKey = False
    CharCase = ecUpperCase
    DataField = 'TENMAY'
    DataSource = DsDMQUAY
    DropDownCount = 10
    ItemHeight = 0
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
    OnDropDown = CbTenmayDropDown
  end
  object CbPrinter: TwwDBLookupCombo
    Left = 365
    Top = 322
    Width = 147
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'PRINTER'#9'1'#9'PRINTER'#9#9)
    DataField = 'PRINTER'
    DataSource = DsDMQUAY
    LookupTable = QrPRINTER
    LookupField = 'PRINTER'
    Options = [loColLines]
    Style = csDropDownList
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnNotInList = CbPrinterNotInList
ButtonEffects.Transparent=True
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 792
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object LbKHO: TLabel
      Left = 144
      Top = 16
      Width = 87
      Height = 16
      Alignment = taRightJustify
      Caption = #272'i'#7875'm b'#225'n h'#224'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbTenKho: TwwDBLookupCombo
      Left = 296
      Top = 12
      Width = 333
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENKHO'#9'35'#9'TENKHO'#9'F'
        'MAKHO'#9'5'#9'MAKHO'#9'F')
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnBeforeDropDown = CbMaKhoBeforeDropDown
      OnCloseUp = CbMaKhoCloseUp
      OnNotInList = CbPrinterNotInList
ButtonEffects.Transparent=True
    end
    object CbMaKho: TwwDBLookupCombo
      Left = 240
      Top = 12
      Width = 53
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'5'#9'MAKHO'#9'F'#9
        'TENKHO'#9'35'#9'TENKHO'#9'F'#9)
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnBeforeDropDown = CbMaKhoBeforeDropDown
      OnCloseUp = CbMaKhoCloseUp
      OnNotInList = CbPrinterNotInList
ButtonEffects.Transparent=True
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 124
    Top = 136
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin     '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDMQUAYBeforeInsert
    AfterInsert = QrDMQUAYAfterInsert
    BeforeEdit = QrDMQUAYBeforeEdit
    BeforePost = QrDMQUAYBeforePost
    BeforeDelete = QrDMQUAYBeforeDelete
    OnDeleteError = QrDMQUAYPostError
    OnEditError = QrDMQUAYPostError
    OnPostError = QrDMQUAYPostError
    DataSource = DsDMKHO
    Parameters = <
      item
        Name = 'MAKHO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '    from DM_QUAYTN'
      ' where MAKHO = :MAKHO')
    Left = 92
    Top = 136
    object QrDMQUAYTENMAY: TWideStringField
      DisplayLabel = 'T'#234'n m'#225'y'
      FieldName = 'TENMAY'
      Size = 50
    end
    object QrDMQUAYGHICHU: TWideStringField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrDMQUAYQUAY: TWideStringField
      DisplayLabel = 'S'#7889' hi'#7879'u qu'#7847'y thu ng'#226'n'
      FieldName = 'QUAY'
      OnValidate = QrDMQUAYQUAYValidate
      Size = 2
    end
    object QrDMQUAYPRINTER: TWideStringField
      DisplayLabel = 'M'#225'y in'
      FieldName = 'PRINTER'
    end
    object QrDMQUAYMAKHO: TWideStringField
      DisplayLabel = 'M'#227' '#273'i'#7875'm b'#225'n h'#224'ng'
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrDMQUAYTENKHO: TWideStringField
      DisplayLabel = #272'i'#7875'm b'#225'n h'#224'ng'
      FieldKind = fkLookup
      FieldName = 'TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 100
      Lookup = True
    end
    object QrDMQUAYCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMQUAYUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMQUAYDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrDMQUAYCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMQUAYUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMQUAYDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
  end
  object DsDMQUAY: TDataSource
    DataSet = QrDMQUAY
    Left = 92
    Top = 168
  end
  object QrPRINTER: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from PRINTERS')
    Left = 157
    Top = 136
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDMQUAY
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'TENMAY'
      'QUAY'
      'TENKHO'
      'PRINTER'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 309
    Top = 222
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 246
    Top = 222
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 278
    Top = 222
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lc1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 56
    Top = 136
    object QrDMKHOMAKHO: TWideStringField
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDMKHOTENKHO: TWideStringField
      FieldName = 'TENKHO'
      Size = 100
    end
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 56
    Top = 168
  end
end
