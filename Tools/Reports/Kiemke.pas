﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Kiemke;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi,
  isPanel, isDb, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, RzLaunch;

type
  TFrmKiemke = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_DVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXNG_NHAN: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LbKHO: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    CbKHO: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    DBMemo1: TDBMemo;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    BtImport: TToolButton;
    SepChecked: TToolButton;
    PopMaster: TAdvPopupMenu;
    QrDM_QUAYKE: TADOQuery;
    Label3: TLabel;
    wwDBLookupCombo3: TwwDBLookupCombo;
    QrNXQUAYKE: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    N2: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    PaTotal: TPanel;
    Label24: TLabel;
    ImgTotal: TImage;
    TntLabel9: TLabel;
    EdTriGiaTT: TwwDBEdit;
    EdQtyTotal: TwwDBEdit;
    QrNXSOLUONG: TFloatField;
    vlTotal1: TisTotal;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdRefresh: TAction;
    CmdSwitch: TAction;
    CmdFilter: TAction;
    CmdDel: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    CmdClearFilter: TAction;
    CmdImportExcel: TAction;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    QrCTDONGIA_REF: TFloatField;
    Label5: TLabel;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrNXDRC_STATUS: TWideStringField;
    QrNXNGAY2: TDateTimeField;
    Label4: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    CmdSapthutu: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    RzLauncher: TRzLauncher;
    CmdImport2: TAction;
    PopIm: TAdvPopupMenu;
    Lydliutthitbkimkho1: TMenuItem;
    QrCTDONGIA_REF2: TFloatField;
    QrCTQD1: TIntegerField;
    QrCTLK_DVT1: TWideStringField;
    QrCTLK_QD1: TIntegerField;
    QrCTB1: TBooleanField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrNXSOTIEN_SI: TFloatField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    PopupMenu2: TAdvPopupMenu;
    Phiuvnchuynnib1: TMenuItem;
    MenuItem3: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXLOC: TWideStringField;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIASI: TFloatField;
    QrCTLK_GIABAN: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTDONGIA_SI: TFloatField;
    QrCTSOTIEN_SI: TFloatField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    QrCTLK_GIAVON: TFloatField;
    QrCsv: TADOQuery;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    QrCTEX_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    Lydliutfiletext1: TMenuItem;
    QrCTLK_GIASIVAT: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);

    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure BtImportClick(Sender: TObject);
    procedure CmdImport2Execute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
  private
  	mLCT: String;
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;

    procedure OpenDetail;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmKiemke: TFrmKiemke;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, IniFiles, GuidEx, isCommon,
    ChonDsma, isLib, wwStr, ShellApi, isFile, Sapthutu, ImportExcel;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_KIEMKE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.Execute;
begin
	mLCT := 'KKE';
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled  := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    // Initial
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormShow(Sender: TObject);
begin
	Wait(PREPARING);
    OpenDataSets([DataMain.QrDMVT, DataMain.QrDMKHO, QrDM_QUAYKE]);
    SetDisplayFormat(DataMain.QrDMVT, sysCurFmt);
    SetDisplayFormat(DataMain.QrDMVT,['TL_LAI'], sysPerFmt);

    with QrNX do
    begin
		SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
	    TFloatField(FieldByName('NGAY2')).DisplayFormat := ShortDateFormat + ' HH:SS';
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exHideDrc;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
        exShowDrc(DsNX);
    end
	else
    begin
    	exHideDrc;
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

    (*
    **  commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from KIEMKE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from KIEMKE_CT a, DM_VT_FULL b where a.KHOA = KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from KIEMKE_CT where KHOA = KIEMKE.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	DataMain.QrDMVT.Requery;
    DataMain.QrDMKHO.Requery;
    QrDM_QUAYKE.Requery;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdImport2Execute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	if not YesNo('Thiết bị phải đang ở tình trạng gởi dữ liệu. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'Softz.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Remove current data file
    s := sysTempPath + 'upload.txt';
    DeleteFile(s);

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Up.exe';
		// Data_Read <File name>,<Upload via>,<COM Port>,<Baud rate>,<Save mode>,<Add CR character>,
		// <Add LF character>,<Show error>,<Show data>,<Show dialog>,<Keep online>,<Polling time>
        Parameters := s + ',2,' + IntToStr(n) + ',1,1,1,1,0,0,0,0,2';
        Launch;
    end;
    Screen.Cursor := crDefault;

	if not FileExists(s) then
		Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;
            mTrigger := True;
			try
            	Post;
                ls.Delete(i);
                Inc(n);
			except
            	Cancel;
				Inc(i);
			end;
            mTrigger := False;
		end;
    end;
	StopProgress;
    Refresh;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'Softz.log';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLS;XLSX', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';

procedure TFrmKiemke.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;
//            mTrigger := True;
			try
            	Post;
                ls.Delete(i);
                Inc(n);
			except
            	Cancel;
				Inc(i);
			end;
//            mTrigger := False;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
	end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      	:= d;
		FieldByName('NGAY2').AsDateTime         := d;
		FieldByName('LCT').AsString           	:= mLCT;
		FieldByName('MAKHO').AsString       	:= sysDefKho;
        FieldByName('LOC').AsString             := sysLoc;
        FieldByName('DRC_STATUS').AsString       := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrNX.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
    exDrcValidate(QrNX);
    DataMain.AllocSCT(mLCT, QrNX);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Detail DB
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforePost(DataSet: TDataSet);
begin
	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

//		if FieldByName('TINHTRANG').AsString <> '01' then
//        begin
//			ErrMsg(RS_ITEM_CODE_FAIL1);
//        	Abort;
//        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
        
    vlTotal1.Keep;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := RoundUp(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger))
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := RoundUp(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTMAVTChange(Sender: TField);
var
	x: Double;
begin
	exDotMavt(3, Sender);
    
    // Update referenced fields
    with QrCT do
    begin
        FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;

    	x := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA_REF').AsFloat := x;
        FieldByName('DONGIA').AsFloat := x;

        FieldByName('DONGIA_SI').AsFloat := FieldByName('LK_GIASIVAT').AsFloat;
        FieldByName('DONGIA_LE').AsFloat := FieldByName('LK_GIABAN').AsFloat;
    end;

	GrDetail.InvalidateCurrentRow;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN_SI').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN_SI').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.BtImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.OpenDetail;
begin
    Screen.Cursor := crSQLWait;
    with QrCT do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                d := FieldByName('SOLUONG').AsFloat /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

                if d = Trunc(d) then
                    FieldByName('SOLUONG2').AsFloat := d
                else
                    FieldByName('SOLUONG2').Clear;
            end
            else if Sender.FullName = 'SOLUONG2' then
            begin
                FieldByName('SOLUONG').AsFloat :=
                    FieldByName('SOLUONG2').AsFloat *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            end;
        mTrigger := bTrigger;

        if FieldByName('SOLUONG2').AsFloat >= 1 then
            d := exVNDRound(FieldByName('SOLUONG2').AsFloat * FieldByName('DONGIA2').AsFloat)
        else
            d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat);

        // Tinh tri gia tren BOX
        FieldByName('SOTIEN_SI').AsFloat := FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_SI').AsFloat;
        FieldByName('SOTIEN_LE').AsFloat := FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat;
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);    
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterEdit(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterInsert(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
    	FieldByName('THANHTOAN').Value := FieldByName('SOTIEN').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;



end.
