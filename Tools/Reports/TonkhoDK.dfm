object FrmTonkhoDK: TFrmTonkhoDK
  Left = 622
  Top = 216
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'T'#7891'n Kho '#272#7847'u K'#7923
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 59
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object TntToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSum
      ImageIndex = 9
    end
    object TntToolButton2: TToolButton
      Left = 59
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 67
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 126
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object TntToolButton3: TToolButton
      Left = 185
      Top = 0
      Width = 8
      Caption = 'TntToolButton3'
      Style = tbsSeparator
    end
    object TntToolButton4: TToolButton
      Left = 193
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object ToolButton6: TToolButton
      Left = 252
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 260
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 792
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 64
      Top = 15
      Width = 36
      Height = 16
      Alignment = taRightJustify
      Caption = 'Th'#225'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 184
      Top = 15
      Width = 26
      Height = 16
      Alignment = taRightJustify
      Caption = 'N'#259'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 327
      Top = 15
      Width = 21
      Height = 16
      Alignment = taRightJustify
      Caption = 'Kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbMA: TwwDBLookupCombo
      Left = 357
      Top = 12
      Width = 53
      Height = 24
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'4'#9'MAKHO'#9'F'
        'TENKHO'#9'40'#9'TENKHO'#9'F')
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMAChange
      OnExit = CbMAChange
      OnNotInList = CbMANotInList
ButtonEffects.Transparent=True
    end
    object CbTEN: TwwDBLookupCombo
      Tag = 1
      Left = 413
      Top = 12
      Width = 317
      Height = 24
      TabStop = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENKHO'#9'40'#9'TENKHO'#9'F'
        'MAKHO'#9'2'#9'MAKHO'#9'F')
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      OnChange = CbMAChange
      OnExit = CbMAChange
ButtonEffects.Transparent=True
    end
    object EdKy: TComboBox
      Left = 109
      Top = 12
      Width = 49
      Height = 24
      Style = csDropDownList
      DropDownCount = 12
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 0
    end
    object EdNam: TComboBox
      Left = 217
      Top = 12
      Width = 69
      Height = 24
      Style = csDropDownList
      DropDownCount = 12
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 1
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 85
    Width = 792
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 792
      Height = 467
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'RSTT'#9'3'#9'STT'#9'F'
        'MAVT'#9'14'#9'M'#227#9'F'#9'H'#224'ng h'#243'a'
        'TENVT'#9'42'#9'T'#234'n'#9'T'#9'H'#224'ng h'#243'a'
        'DVT'#9'7'#9#272'VT'#9'T'
        'LG_DAU'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'#9'T'#7891'n kho'
        'DG_DAU'#9'12'#9#272#417'n gi'#225#9'F'#9'T'#7891'n kho'
        'TIEN_DAU'#9'12'#9'S'#7889' ti'#7873'n'#9'F'#9'T'#7891'n kho'
        'MANHOM'#9'5'#9'M'#227#9'T'#9'Nh'#243'm h'#224'ng h'#243'a'
        'TENNHOM'#9'40'#9'T'#234'n'#9'T'#9'Nh'#243'm h'#224'ng h'#243'a')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 4
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsTONKHO
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      OnEnter = CmdRefreshExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 16119285
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 48
    Top = 132
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdSum: TAction
      Caption = 'T'#7893'ng h'#7907'p'
      Hint = 'T'#7893'ng h'#7907'p s'#7889' li'#7879'u ki'#7875'm k'#234' th'#7921'c t'#7871
      ShortCut = 118
      OnExecute = CmdSumExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
  end
  object QrTONKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTONKHOBeforeOpen
    BeforeEdit = QrTONKHOBeforeEdit
    BeforePost = QrTONKHOBeforePost
    BeforeDelete = QrTONKHOBeforeDelete
    OnCalcFields = QrTONKHOCalcFields
    OnDeleteError = QrTONKHOPostError
    OnEditError = QrTONKHOPostError
    OnPostError = QrTONKHOPostError
    Parameters = <
      item
        Name = 'KY'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MAKHO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from TONKHO'
      ' where KY = :KY'
      '   and NAM = :NAM'
      '   and MAKHO = :MAKHO'
      '')
    Left = 48
    Top = 164
    object QrTONKHOMAVT: TWideStringField
      DisplayLabel = 'M'#183
      DisplayWidth = 18
      FieldName = 'MAVT'
      FixedChar = True
      Size = 15
    end
    object QrTONKHOTENVT: TWideStringField
      DisplayLabel = 'T'#170'n v'#203't t'#173', h'#181'ng h'#227'a'
      DisplayWidth = 42
      FieldKind = fkLookup
      FieldName = 'TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrTONKHODVT: TWideStringField
      DisplayLabel = #167'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrTONKHOTIEN_DAU: TFloatField
      DisplayLabel = 'S'#232' ti'#210'n'
      FieldName = 'TIEN_DAU'
      OnChange = QrTONKHOTIEN_DAUChange
    end
    object QrTONKHOMANHOM: TWideStringField
      DisplayLabel = 'Nh'#227'm'
      DisplayWidth = 3
      FieldKind = fkLookup
      FieldName = 'MANHOM'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MANHOM'
      KeyFields = 'MAVT'
      Size = 3
      Lookup = True
    end
    object QrTONKHOTENNHOM: TWideStringField
      DisplayLabel = 'T'#170'n nh'#227'm'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'TENNHOM'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENNHOM'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrTONKHOKY: TIntegerField
      DisplayWidth = 10
      FieldName = 'KY'
      Visible = False
    end
    object QrTONKHONAM: TIntegerField
      DisplayWidth = 10
      FieldName = 'NAM'
      Visible = False
    end
    object QrTONKHOMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrTONKHOCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTONKHOUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTONKHOCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrTONKHOUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrTONKHOLG_DAU: TFloatField
      FieldName = 'LG_DAU'
      Visible = False
      OnChange = QrTONKHOLG_DAUChange
    end
    object QrTONKHOGIANHAP: TFloatField
      FieldKind = fkLookup
      FieldName = 'GIANHAP'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAP'
      KeyFields = 'MAVT'
      Visible = False
      Lookup = True
    end
    object QrTONKHORSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrTONKHOKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsTONKHO: TDataSource
    DataSet = QrTONKHO
    Left = 48
    Top = 196
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.MADT, a.MAVT, a.TENVT, a.TENTAT, a.DVT, '
      
        #9'a.GIANHAP, a.GIANHAPVAT, a.GIASI, a.GIASIVAT, a.GIABAN, a.GIAVO' +
        'N_TK, '
      #9'a.LOAITHUE, a.VAT_RA, a.VAT_VAO, b.TENLT, '
      #9'a.MANHOM, c.TENNHOM'
      '  from'#9'DM_VT a, DM_LOAITHUE b, DM_NHOM c'
      ' where'#9'a.LOAITHUE = b.MALT'
      #9'and a.MANHOM = c.MANHOM'
      'order by a.TENVT'
      ' ')
    Left = 80
    Top = 164
    object QrDMVTMAVT: TWideStringField
      DisplayLabel = 'M'#183
      DisplayWidth = 13
      FieldName = 'MAVT'
      FixedChar = True
      Size = 15
    end
    object QrDMVTTENVT: TWideStringField
      DisplayLabel = 'T'#170'n v'#203't t'#173', h'#181'ng h'#227'a'
      DisplayWidth = 40
      FieldName = 'TENVT'
      Size = 100
    end
    object QrDMVTDVT: TWideStringField
      DisplayLabel = #167'VT'
      DisplayWidth = 10
      FieldName = 'DVT'
      Size = 10
    end
    object QrDMVTTENNHOM: TWideStringField
      DisplayLabel = 'T'#170'n'
      DisplayWidth = 30
      FieldName = 'TENNHOM'
      ReadOnly = True
      Visible = False
      Size = 108
    end
    object QrDMVTMANHOM: TWideStringField
      DisplayWidth = 3
      FieldName = 'MANHOM'
      Visible = False
      FixedChar = True
      Size = 3
    end
    object QrDMVTGIANHAP: TFloatField
      FieldName = 'GIANHAP'
    end
    object QrDMVTGIANHAPVAT: TFloatField
      FieldName = 'GIANHAPVAT'
    end
    object QrDMVTMADT: TWideStringField
      FieldName = 'MADT'
      Size = 15
    end
    object QrDMVTTENTAT: TWideStringField
      FieldName = 'TENTAT'
      Size = 25
    end
    object QrDMVTGIABAN: TFloatField
      FieldName = 'GIABAN'
    end
    object QrDMVTGIASI: TFloatField
      FieldName = 'GIASI'
    end
    object QrDMVTGIAVON_TK: TFloatField
      FieldName = 'GIAVON_TK'
    end
    object QrDMVTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrDMVTTENLT: TWideStringField
      FieldName = 'TENLT'
      Size = 200
    end
    object QrDMVTGIASIVAT: TFloatField
      FieldName = 'GIASIVAT'
    end
    object QrDMVTVAT_RA: TFloatField
      FieldName = 'VAT_RA'
    end
    object QrDMVTVAT_VAO: TFloatField
      FieldName = 'VAT_VAO'
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTONKHO
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAVT'
      'TENVT'
      'DVT'
      'SL'
      'DG'
      'ST'
      'TENNHOM'
      'MANHOM')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 273
    Top = 134
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 218
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 304
    Top = 218
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ImporttExcel1: TMenuItem
      Caption = 'Import d'#7919' li'#7879'u t'#7915' file'
      ImageIndex = 28
    end
  end
  object QrIM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KY'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MAKHO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'TONKHO'
      ' where'#9'KY = :KY'
      '   and'#9'NAM = :NAM'
      '   and'#9'MAKHO = :MAKHO')
    Left = 48
    Top = 228
  end
  object SUM_KIEMKE: TADOCommand
    CommandText = 'TONKHO_KIEMKE;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@THANG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NAM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 260
  end
end
