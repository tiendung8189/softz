object frNGAY: TfrNGAY
  Left = 0
  Top = 0
  Width = 451
  Height = 49
  Align = alTop
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentBackground = False
    ParentColor = True
    TabOrder = 0
    object Label65: TLabel
      Left = 52
      Top = 16
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 252
      Top = 16
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object Label1: TLabel
      Left = 463
      Top = 16
      Width = 51
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7883'a '#273'i'#7875'&m'
      FocusControl = CbLoc
    end
    object EdFrom: TwwDBDateTimePicker
      Left = 108
      Top = 12
      Width = 113
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
    object EdTo: TwwDBDateTimePicker
      Left = 316
      Top = 12
      Width = 113
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 1
    end
    object CbLoc: TwwDBLookupCombo
      Left = 524
      Top = 12
      Width = 265
      Height = 24
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN'#9'30'#9'TEN'#9'F'
        'LOC'#9'6'#9'LOC'#9'F')
      LookupTable = DataMain.QrLOC
      LookupField = 'LOC'
      Style = csDropDownList
      TabOrder = 2
      AutoDropDown = False
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
ButtonEffects.Transparent=True
    end
  end
end
