﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtBo;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Dialogs,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit,
  fctreecombo, isDb, fcTreeView, wwDialog, Mask, RzPanel, fcCombo, Grids,
  Wwdbgrid, ToolWin, wwdbedit;

type
  TFrmDmvtBo = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTTL_LAI: TFloatField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTGIANHAPVAT: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTDVT1: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTQUAYKE: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTSTAMP: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    QrDM_QUAYKE: TADOQuery;
    ItmNCC: TMenuItem;
    ItmNHOM: TMenuItem;
    N1: TMenuItem;
    CmdPhoto: TAction;
    N2: TMenuItem;
    Xemhnh1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N4: TMenuItem;
    QrDMVTGIASI: TFloatField;
    PopIn: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    CmdChangeGroup: TAction;
    N5: TMenuItem;
    ingnhnhm1: TMenuItem;
    Xl1: TMenuItem;
    QrDMVTDAT_HANG: TFloatField;
    QrDMVTMAMAU: TWideStringField;
    QrDMVTMASIZE: TWideStringField;
    QrDMVTTINHTRANG: TWideStringField;
    DMVT_PLU_COUNTER: TADOCommand;
    CmdExport: TAction;
    ToolButton8: TToolButton;
    ToolButton12: TToolButton;
    SaveDlg: TSaveDialog;
    DMVT_EXPORT: TADOStoredProc;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label14: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label12: TLabel;
    LbDVT1: TLabel;
    LbDVT2: TLabel;
    EdMA: TwwDBEdit;
    EdTen: TwwDBEdit;
    DBEdit11: TwwDBEdit;
    CbQD1: TwwDBEdit;
    CbDVT: TwwDBLookupCombo;
    CbDVT1: TwwDBLookupCombo;
    PD2: TisPanel;
    EdGHICHU: TDBMemo;
    PD3: TisPanel;
    QrDMVTKM_TUNGAY: TDateTimeField;
    QrDMVTKM_DENNGAY: TDateTimeField;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    Label10: TLabel;
    wwDBLookupCombo6: TwwDBLookupCombo;
    Label3: TLabel;
    wwDBLookupCombo3: TwwDBLookupCombo;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    QrDMVTMANHOM2: TWideStringField;
    QrDMVTMANHOM3: TWideStringField;
    QrDMVTMANHOM4: TWideStringField;
    Label26: TLabel;
    wwDBLookupCombo4: TwwDBLookupCombo;
    wwDBLookupCombo5: TwwDBLookupCombo;
    PaCT: TisPanel;
    GrCT: TwwDBGrid2;
    PopDetail: TAdvPopupMenu;
    CmdDelDetail: TAction;
    Xachitit1: TMenuItem;
    CmdSwitch: TAction;
    QrCT_BO: TADOQuery;
    DsCT_BO: TDataSource;
    QrCT_BOMABO: TWideStringField;
    QrCT_BOMABH: TWideStringField;
    QrCT_BOSOLUONG: TFloatField;
    RefDM_HH_CT: TADOQuery;
    QrCT_BOMADT: TWideStringField;
    QrCT_BOTENDT: TWideStringField;
    TntPanel1: TPanel;
    QrDMVTBO: TBooleanField;
    Label38: TLabel;
    DBEdit4: TwwDBEdit;
    vlTotal: TisTotal;
    QrCT_BORSTT: TIntegerField;
    QrCT_BOTENVT: TWideStringField;
    ALLOC_BARCODE: TADOCommand;
    Bevel1: TBevel;
    CmdAudit: TAction;
    QrDMVTMAVT: TWideStringField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    DMVT_EXPORT2: TADOStoredProc;
    Label2: TLabel;
    DBEdit2: TwwDBEdit;
    ToolButton9: TToolButton;
    ToolButton13: TToolButton;
    CmdChecked: TAction;
    QrDMVTCHECKED: TBooleanField;
    Splitter1: TSplitter;
    RefDM_HH_CTMAVT: TWideStringField;
    RefDM_HH_CTTENVT: TWideStringField;
    RefDM_HH_CTGIANHAP: TFloatField;
    RefDM_HH_CTGIABAN: TFloatField;
    RefDM_HH_CTTL_LAI: TFloatField;
    RefDM_HH_CTMADT: TWideStringField;
    RefDM_HH_CTTENDT: TWideStringField;
    QrCT_BOGIABAN1: TFloatField;
    QrCT_BOTL_CK: TFloatField;
    QrCT_BOGIABAN: TFloatField;
    QrCT_BOLK_GIABAN: TFloatField;
    QrDMVTTL_LAI_SI: TFloatField;
    RefDM_HH_CTGIASI: TFloatField;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTGIABAN1: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    Label4: TLabel;
    Label7: TLabel;
    wwDBEdit2: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    wwDBEdit4: TwwDBEdit;
    wwDBEdit5: TwwDBEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTVAT_VAOChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure ItmNCCClick(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrDMVTAfterScroll(DataSet: TDataSet);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrCT_BOBeforeInsert(DataSet: TDataSet);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrCT_BOBeforeOpen(DataSet: TDataSet);
    procedure QrCT_BOBeforeDelete(DataSet: TDataSet);
    procedure QrCT_BOMABHChange(Sender: TField);
    procedure QrCT_BOBeforePost(DataSet: TDataSet);
    procedure QrCT_BOAfterCancel(DataSet: TDataSet);
    procedure QrCT_BOAfterDelete(DataSet: TDataSet);
    procedure QrCT_BOAfterEdit(DataSet: TDataSet);
    procedure QrCT_BOSOLUONGChange(Sender: TField);
    procedure QrCT_BOCalcFields(DataSet: TDataSet);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrDMVTBeforeEdit(DataSet: TDataSet);
    procedure QrCT_BOTL_CKChange(Sender: TField);
    procedure QrCT_BOTL_CKValidate(Sender: TField);
    procedure BtnInClick(Sender: TObject);
  private
  	mCanEdit, mEANConfirm: Boolean;
    mGia: Double;

    sCodeLen, defDvt, defTinhtrang: String;
 	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4, mPrefix: String;

    function  AllocEAN(nhom: String): String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmvtBo: TFrmDmvtBo;

implementation

uses
	ExCommon, isMsg, Rights, MainData, RepEngine, Scan, isCommon,
    CayNganhNhom, isBarcode, isLib, isFile;

{$R *.DFM}

const
	FORM_CODE = 'DM_HH_BO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvtBo.AllocEAN(nhom: String): String;
begin
    with ALLOC_BARCODE do
    begin
    	Prepared := True;
        Parameters[1].Value := mPrefix;
        Parameters[2].Value := nhom;
        Execute;
		Result := Parameters[3].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.FormCreate(Sender: TObject);
var
	b: Boolean;
begin
    TMyForm(Self).Init2;
    with QrDMVT do
    begin
    SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT, ['DAT_HANG'], sysQtyFmt);
    	SetDisplayFormat(QrDMVT, ['TL_LAI'], sysPerFmt);
	    SetShortDateFormat(QrDMVT);
    end;

	SetDisplayFormat(QrCT_BO, sysCurFmt);
    SetDisplayFormat(QrCT_BO, ['SOLUONG'], sysQtyFmt);
    SetDisplayFormat(QrCT_BO, ['TL_CK'], sysPerFmt);

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrList, GrCT]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Don vi tinh quy doi
	b := FlexConfigInteger('DM_HH', 'Unit Level') <> 0;
    LbDVT1.Visible := b;
    CbQD1.Visible := b;
    LbDVT2.Visible := b;
    CbDVT1.Visible := b;

    // Params
	mPrefix := FlexConfigString('DM_HH','Barcode Prefix');
    mEANConfirm := FlexConfigBool('DM_HH','EAN Confirm');
	sCodeLen := FlexConfigString('DM_HH', 'Code Length');

	// Default unit
	defDvt := DataMain.GetSysParam('DEFAULT_DVT');

    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    // Tree Combo
    FlexGroupCombo(CbNhomhang);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.FormShow(Sender: TObject);
begin
	DsDMVT.AutoEdit := mCanEdit;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    QrDM_QUAYKE.Open;
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG]);
        defTinhtrang := QrDMVT_TINHTRANG.FieldByName('MA').AsString;
    end;
    SetDisplayFormat(RefDM_HH_CT, sysCurFmt);
    SetDisplayFormat(RefDM_HH_CT, ['TL_LAI'], sysPerFmt);
    // Loading
	CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    // Saving & closing
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
    RegWrite(Name, ['PD2', 'PD3'], [PD2.Collapsed, PD3.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdNewExecute(Sender: TObject);
var
	ma: String;
begin
	with QrDMVT do
    begin
    	Append;
		ma := FieldByName('MAVT').AsString;
    end;

    EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdSaveExecute(Sender: TObject);
begin
    QrCT_BO.CheckBrowseMode;
	QrDMVT.Post;

    with QrCT_BO do
    begin
        mTrigger := True;
        DisableControls;
        First;

        while not Eof do
        begin
            Edit;
            FieldByName('MABO').AsString := QrDMVT.FieldByName('MAVT').AsString;
            Post;
            Next;
        end;

        EnableControls;
        mTrigger := False;
    end;

    QrCT_BO.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
    QrCT_BO.CancelBatch;    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdDelExecute(Sender: TObject);
begin
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.ItmNCCClick(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE + '_' + IntToStr((Sender as TComponent).Tag),
    	[sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdPrintExecute(Sender: TObject);
begin
   BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, bChecked: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
        bChecked := FieldByName('CHECKED').AsBoolean;
	end;

    CmdNew.Enabled := bBrowse and mCanEdit and (mNhom <> '');
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;
    PaList.Enabled := bBrowse;

    if FrmScan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec;
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and (not FrmScan.Visible);
        CmdFilter.Enabled := not FrmScan.Visible;
        CmdSearch.Enabled := not FrmScan.Visible;
    end;

    CmdChecked.Enabled := bHasRec;
    CmdChecked.Caption := exGetCheckedCaption(QrDMVT);

    CmdSearch.Enabled := CmdSearch.Enabled and bBrowse;
    CmdFilter.Enabled := CmdFilter.Enabled and bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdChangeGroup.Enabled := mCanEdit and bBrowse and bHasRec;

    CmdDelDetail.Enabled := mCanEdit and (not QrCT_BO.IsEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTBeforeEdit(DataSet: TDataSet);
begin
	if not mTrigger then
    	exIsChecked(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) then
	begin
        mNganh  := sNganh;
        mNhom   := sNhom;
        mNhom2  := sNhom2;
        mNhom3  := sNhom3;
        mNhom4  := sNhom4;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''', [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            SQL.Text := sSQL;
            Open;

            GrList.SetFocus;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
		FieldByName('MANHOM2').AsString := mNhom2;
		FieldByName('MANHOM3').AsString := mNhom3;
		FieldByName('MANHOM4').AsString := mNhom4;
//        FieldByName('GIABAN').AsFloat := mGia;
        FieldByName('DVT').AsString := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
		FieldByName('BO').AsBoolean := True;
		FieldByName('MAVT').AsString := AllocEAN(mNhom);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';

procedure TFrmDmvtBo.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['TENVT']) then
			Abort;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MAVT').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;

	    SetNull(DataSet, ['MAMAU' , 'MASIZE', 'MANHOM2', 'MANHOM3', 'MANHOM4']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOBeforeInsert(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    SetEditState(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdPhotoExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmScan, FrmScan);
	FrmScan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmDmvtBo.QrDMVTTENVTChange(Sender: TField);
var
	b : Boolean;
begin
	with QrDMVT do
    begin
    	b := FieldByName('TENTAT').AsString = '';

    	if not b then
        	b := YesNo(RS_NAME_DEF);

		if b then
			FieldByName('TENTAT').AsString := FieldByName('TENVT').AsString;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTVAT_VAOChange(Sender: TField);
begin
	with QrDMVT do
    	if Sender.AsFloat <> 0 then
	    	FieldByName('VAT_RA').AsFloat := Sender.AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmDmvtBo.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTAfterPost(DataSet: TDataSet);
begin
	with QrDMVT do
		mGia := FieldByName('GIABAN').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrDMVTAfterScroll(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';

procedure TFrmDmvtBo.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom, nhom2, nhom3, nhom4: String;
begin
	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
        nhom2 := FieldByName('MANHOM2').AsString;
        nhom3 := FieldByName('MANHOM3').AsString;
        nhom4 := FieldByName('MANHOM4').AsString;
    end;
	Application.CreateForm(TFrmCayNganhNhom, FrmCayNganhNhom);
    b := FrmCayNganhNhom.Execute(nganh, nhom, nhom2, nhom3, nhom4);
    FrmCayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;
            FieldByName('MANHOM2').AsString  := nhom2;
            FieldByName('MANHOM3').AsString  := nhom3;
            FieldByName('MANHOM4').AsString  := nhom4;

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdCheckedExecute(Sender: TObject);
begin
	exChecked(QrDMVT, 'SZ_MAVT_BO');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdExportExecute(Sender: TObject);
var
	s: String;
    sNganh, sNhom, sNhom2, sNhom3, sNhom4: String;
    fLevel: Integer;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
        with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT2 do
    begin
        Parameters[1].Value := 1; // Bo = 1
        Parameters[2].Value := sNganh;
        Parameters[3].Value := sNhom;
        Parameters[4].Value := sNhom2;
        Parameters[5].Value := sNhom3;
        Parameters[6].Value := sNhom4;
        Parameters[7].Value := fLevel;

    	ExecProc;
        Active := True;
//        TextExport(DMVT_EXPORT2, s, TEncoding.UTF8);
        TextExport(DMVT_EXPORT2, s);
        Active := False
    end;
	Cursor := crDefault;

    MsgDone();

        {
	s := vlGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	ExecProc;
        Active := True;
        TextExport(DMVT_EXPORT, s);
        Active := False
    end;
	Cursor := crDefault;
    }
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdDelDetailExecute(Sender: TObject);
begin
    QrCT_BO.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrCT then
        EdGHICHU.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOBeforeOpen(DataSet: TDataSet);
begin
    QrCT_BO.Parameters[0].Value := QrDMVT.FieldByName('MAVT').AsString;
end;
    
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;

    SetEditState(QrDMVT);
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOMABHChange(Sender: TField);
var
    x: Double;
begin
    exDotMavt(1, RefDM_HH_CT, Sender);
    with QrCT_BO do
    begin
        x := FieldByName('LK_GIABAN').AsFloat;
        FieldByName('GIABAN1').AsFloat := x;
        FieldByName('GIABAN').AsFloat := x;
    end;
    GrCT.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOAfterDelete(DataSet: TDataSet);
begin
	vlTotal.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOAfterEdit(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOSOLUONGChange(Sender: TField);
begin
    vlTotal.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOTL_CKChange(Sender: TField);
begin
    if mTrigger then
        Exit;

    with QrCT_BO do
    begin
        mTrigger := True;
        if Sender.FieldName = 'GIABAN1' then
            FieldByName('TL_CK').AsFloat :=
                (1 - SafeDiv(FieldByName('GIABAN1').AsFloat, FieldByName('GIABAN').AsFloat)) * 100
        else
            FieldByName('GIABAN1').AsFloat := exVNDRound(FieldByName('GIABAN').AsFloat *
        	(1 - FieldByName('TL_CK').AsFloat / 100), sysCurRound);

        mTrigger := False;
    end;

    vlTotal.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOTL_CKValidate(Sender: TField);
begin
    with QrCT_BO do
    begin
        if (FieldByName('TL_CK').AsFloat > 100)
//            or (FieldByName('GIABAN1').AsFloat > FieldByName('GIABAN').AsFloat)
            then
        begin
            ErrMsg('Chiết khấu không hợp lệ.');
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOBeforePost(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
	    if BlankConfirm(QrCT_BO, ['MABH', 'SOLUONG']) then
	        Abort;

        if FieldByName('MADT').AsString = '' then
        begin
            ErrMsg('Lỗi nhập liệu. Sai barcode.');
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOAfterCancel(DataSet: TDataSet);
begin
	vlTotal.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.QrCT_BOCalcFields(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
        if not (State in [dsInsert]) then
            FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtBo.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

end.
