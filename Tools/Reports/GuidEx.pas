unit GuidEx;

{
TGuidEx - Delphi class for manipulating Guid values

http://delphi.about.com/library/weekly/aa022205a.htm

A Guid type represent a 128-bit integer value.
The TGuidEx class exposes class (static) methods
that help operate GUID values and TGuidField
database field types.

~ Zarko Gajic
}


interface
uses SysUtils, DB, MainData;

type
  TGuidEx = class
    class function NewGuid : TGuid;
    class function NewGuidDate(pFieldGUID : TField): Boolean;
    class function EmptyGuid : TGuid;
    class function IsEmptyGuid(Guid : TGuid) : boolean; overload;
    class function IsEmptyGuid(Guid : TField) : boolean; overload;
    class function ToString(Guid : TGuid) : string; overload;
    class function ToString(Guid : TField) : string; overload;
    class function ToStringEx(Guid : TGuid) : string; overload;
    class function ToStringEx(Guid : TField) : string; overload;
    class function ToQuotedString(Guid : TGuid) : string; overload;
    class function ToQuotedString(Guid : TField) : string; overload;
    class function FromString(Value : string) : TGuid;
    class function EqualGuids(Guid1, Guid2 : TGuid) : boolean;
  end;

implementation

{ TGuidEx }
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.EmptyGuid: TGuid;
begin
  result := FromString('{00000000-0000-0000-0000-000000000000}');
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.EqualGuids(Guid1, Guid2: TGuid): boolean;
begin
  result := IsEqualGUID(Guid1, Guid2);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.FromString(Value: string): TGuid;
begin
  result := StringToGuid(Value);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.IsEmptyGuid(Guid: TField): boolean;
begin
    Result := IsEmptyGuid(TGuidField(Guid).AsGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.IsEmptyGuid(Guid : TGuid): boolean;
begin
  result := EqualGuids(Guid, EmptyGuid);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.NewGuidDate(pFieldGUID: TField): Boolean;
begin
    Result := pFieldGUID.DataType = ftGuid;
    if Result and pFieldGUID.IsNull then
        TGuidField(pFieldGUID).AsGuid := DataMain.GetNewGuid;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.NewGuid: TGuid;
var
  Guid : TGuid;
begin
  CreateGUID(Guid);
  Result := Guid;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToQuotedString(Guid: TGuid): string;
begin
  result := QuotedStr(ToString(Guid));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToQuotedString(Guid: TField): string;
begin
    Result := ToQuotedString(TGuidField(Guid).AsGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToString(Guid: TGuid): string;
begin
  result := GuidToString(Guid);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToString(Guid: TField): string;
begin
    result := GuidToString(TGuidField(Guid).AsGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToStringEx(Guid: TField): string;
begin
    Result := ToStringEx(TGuidField(Guid).AsGuid);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TGuidEx.ToStringEx(Guid: TGuid): string;
begin
    result := Copy(GuidToString(Guid), 2, 36)
end;

end.
