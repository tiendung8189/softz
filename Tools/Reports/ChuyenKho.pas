﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChuyenKho;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwstr, ShellAPI,
  frameNgay, frameNavi, isDb, isPanel, frameScanCode, wwDialog, Mask, Grids,
  Wwdbgrid, ToolWin, wwdbedit, Wwdotdot, Wwdbcomb;

type
  TFrmChuyenKho = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepChecked: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label10: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    LbKHO: TLabel;
    CbKHO: TwwDBLookupCombo;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    CbKHOXUAT: TwwDBLookupCombo;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_DVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    DBMemo1: TDBMemo;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXNG_NHAN: TWideStringField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    Label3: TLabel;
    wwDBLookupCombo1: TwwDBLookupCombo;
    CbKHONHAP: TwwDBLookupCombo;
    Label5: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Phiuvnchuynnib1: TMenuItem;
    N2: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TU_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopupMenu2: TAdvPopupMenu;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    CmdExBarcode: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    Bevel1: TBevel;
    N3: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    QrCTDONGIA_REF: TFloatField;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    QrNXDRC_STATUS: TWideStringField;
    CmdListRefesh: TAction;
    QrCTEX_DATE: TDateTimeField;
    PopDetail: TAdvPopupMenu;
    frScanCode1: TfrScanCode;
    QrCTLK_BO: TBooleanField;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    CmdEmptyDetail: TAction;
    N5: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrCTQD1: TIntegerField;
    QrCTLK_QD1: TIntegerField;
    QrCTLK_DVT1: TWideStringField;
    QrNXDGIAI: TWideMemoField;
    QrCTDONGIA_REF2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrCTSOLUONG2: TFloatField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    CbDrcStatus: TwwDBComboBox;
    LbDrcStatus: TLabel;
    QrCTLK_GIANHAP: TFloatField;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIASI: TFloatField;
    QrCTLK_GIABAN: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTDONGIA_SI: TFloatField;
    QrCTSOTIEN_SI: TFloatField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrNXSOTIEN_SI: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    QrCTLK_GIAVON: TFloatField;
    CmdImportExcel: TAction;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    CmdFromPN: TAction;
    Lytphiunhp1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    ToolButton10: TToolButton;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    LydliutfileText1: TMenuItem;
    QrCTLK_GIASIVAT: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbKHOXUATNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure BtnInClick(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbKHOXUATBeforeDropDown(Sender: TObject);
    procedure CbKHOXUATCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdFromPNExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
  private
  	mLCT, mPrefix: String;
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmChuyenKho: TFrmChuyenKho;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, Sapthutu,
    isCommon, GuidEx, isFile, ImportExcel, ChonPhieunhap;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_DIEUKHO';

    (*
    ** Form Events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.Execute;
begin
   	mLCT := 'CKHO';

	mCanEdit := rCanEdit(r);
    PaInFo.Enabled  := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;
    frScanCode1.Init(QrCT);

    frDate.CbLoc.Enabled := True;

    mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');
    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.FormShow(Sender: TObject);
begin
	// Open Database
    with DataMain do
    begin
		OpenDataSets([QrDMVT, QrDMKHO]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TL_LAI'], sysPerFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
    end;

    // Customize Grid
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if ActiveControl = frScanCode1.EdCode then
        Exit;

	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	exHideDrc;
    try
    	CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;
    (*
    ** End: Form Events
    *)

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from DIEUKHO_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from DIEUKHO_CT a, DM_VT_FULL b where a.KHOA = DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from DIEUKHO_CT where KHOA = DIEUKHO.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
		QrDMVT.Requery;
        QrDMKHO.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    frScanCode1.chkCheckExists.Checked := True;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';

procedure TFrmChuyenKho.CmdFromPNExecute(Sender: TObject);
var
	n: TGUID;
    mKho, mMavt: String;
    mSoluong, mDongia, mGiavon: Double;
begin
    QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmChonPhieunhap, FrmChonPhieunhap);
    with QrNX do
	    n := FrmChonPhieunhap.Execute(
        	False,
        	'',
            FieldByName('MAKHO').AsString);
	FrmChonPhieunhap.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with CHUNGTU_LAYPHIEU do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        mKho := FieldByName('MAKHO').AsString;

        if QrNX.FieldByName('TU_MAKHO').AsString <> mKho then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('TU_MAKHO').AsString := mKho;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mGiavon := FieldByName('GIAVON').AsFloat;

            with QrCT do
//            if not Locate('MAVT', mMavt, []) then
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := mGiavon;
                FieldByName('SOLUONG').AsFloat := mSoluong;
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLS;XLSX', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';
procedure TFrmChuyenKho.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;
//            mTrigger := True;
			try
            	Post;
                ls.Delete(i);
                Inc(n);
			except
            	Cancel;
				Inc(i);
			end;
//            mTrigger := False;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
	DataMain.LabelExport(TGuidEx.ToString(QrNX.FieldByName('KHOA')), 'DIEUKHO');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

	(*
    ** Master Db
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('DRC_STATUS').AsString    := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
//        Parameters[4].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_WAREHOUSE = 'Chuyển kho không hợp lệ.';

procedure TFrmChuyenKho.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'TU_MAKHO', 'MAKHO']) then
	    	Abort;

    	if FieldByName('TU_MAKHO').AsString = FieldByName('MAKHO').AsString then
        begin
        	Msg(RS_INVALID_WAREHOUSE);
            CbKHOXUAT.SetFocus;
            Abort;
        end;

		exValidClosing(FieldByName('NGAY').AsDateTime);
	end;

    exDrcValidate(QrNX);
    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrNXSOTIENChange(Sender: TField);
begin
	with QrNX do
    	FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat;
end;
	(*
    ** End: Master DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := RoundUp(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger))
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := RoundUp(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                d := FieldByName('SOLUONG').AsFloat /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

                if d = Trunc(d) then
                    FieldByName('SOLUONG2').AsFloat := d
                else
                    FieldByName('SOLUONG2').Clear;
            end
            else if Sender.FullName = 'SOLUONG2' then
            begin
                FieldByName('SOLUONG').AsFloat :=
                    FieldByName('SOLUONG2').AsFloat *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            end;
        mTrigger := bTrigger;

        if FieldByName('SOLUONG2').AsFloat >= 1 then
            d := exVNDRound(FieldByName('SOLUONG2').AsFloat * FieldByName('DONGIA2').AsFloat)
        else
            d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat);

        // Tinh tri gia tren BOX
        FieldByName('SOTIEN_SI').AsFloat := FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_SI').AsFloat;
        FieldByName('SOTIEN_LE').AsFloat := FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat;
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.QrCTMAVTChange(Sender: TField);
var
	x: Double;
begin
	exDotMavt(3, Sender);

    // Update referenced fields
    with QrCT do
    begin
        FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;

    	x := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA_REF').AsFloat := x;

        FieldByName('DONGIA').AsFloat := x;
        FieldByName('DONGIA_SI').AsFloat := FieldByName('LK_GIASIVAT').AsFloat;
        FieldByName('DONGIA_LE').AsFloat := FieldByName('LK_GIABAN').AsFloat;

		if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*
    ** End: Detail DB
    *)

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CbKHOXUATBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CbKHOXUATCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CbKHOXUATNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);
		ColumnByName('SOTIEN_SI').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN_SI').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
        exShowDrc(DsNX);

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
        exHideDrc;
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenKho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;
    (*
    **  End: Others
    *)


end.
