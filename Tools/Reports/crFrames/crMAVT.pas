﻿unit crMAVT;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeMAVT = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameMAVT: TframeMAVT;

implementation

{$R *.dfm}

uses
    ChonDsma;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo ngành hàng';
    RS_TIT1 = 'Theo nhóm hàng';
    RS_TIT2 = 'Theo Barcode';
	RS_TIT3 = 'Theo nhóm hàng cấp 2';
    RS_TIT_CAP = '<< %s  >>';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVT.LbMaClick(Sender: TObject);
const
    TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT2
    );
var
    n: Integer;
    s: String;
begin
	n := EdMa.Tag;
    if not FrmChonDsma.Get(n, s) then
    	Exit;

    LbMa.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMa do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVT.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := EdMa.Tag;
    inc(n);
    cr[n] := EdMa.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeMAVT.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVT.Init;
begin
  inherited;

end;

end.
