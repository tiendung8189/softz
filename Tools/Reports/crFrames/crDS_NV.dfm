inherited frameDS_NV: TframeDS_NV
  Height = 85
  ExplicitHeight = 85
  object PaDS_KH: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 85
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label4: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 158
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch nh'#226'n vi'#234'n >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Label4Click
    end
    object EdDS: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 52
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
