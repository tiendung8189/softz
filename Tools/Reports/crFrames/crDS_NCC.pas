unit crDS_NCC;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_NCC = class(TCrFrame)
    PaDS_NCC: TPanel;
    Label4: TLabel;
    EdDS_MA: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected
  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_NCC: TframeDS_NCC;

implementation
uses
    ChonDsNCC;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NCC.GetCode: Variant;
begin
    Result := Trim(EdDS_MA.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NCC.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsNCC.Get(s) then
    	Exit;

    with EdDS_MA do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
