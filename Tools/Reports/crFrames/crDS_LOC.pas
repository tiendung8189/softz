unit crDS_LOC;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_LOC = class(TCrFrame)
    PaDS: TPanel;
    Label7: TLabel;
    EdDS: TMemo;
    procedure Label7Click(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_LOC: TframeDS_LOC;

implementation

{$R *.dfm}

uses
    ChonDsKho;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_LOC.GetCode: Variant;
begin
    Result := EdDS.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LOC.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_LOC.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LOC.Init;
begin
  inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LOC.Label7Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsKho.Get(s) then
    	Exit;

    with EdDS do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
