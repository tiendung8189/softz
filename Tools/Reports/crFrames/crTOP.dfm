inherited frameTOP: TframeTOP
  Height = 74
  ExplicitHeight = 74
  object PaTOP: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 16
      Top = 29
      Width = 22
      Height = 16
      Caption = 'Top'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdTOP: TwwDBEdit
      Left = 48
      Top = 25
      Width = 69
      Height = 24
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Picture.PictureMask = '#*#'
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object RbTOP1: TRadioGroup
      Left = 140
      Top = 2
      Width = 121
      Height = 62
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Doanh thu'
        'S'#7889' l'#432#7907'ng')
      ParentFont = False
      TabOrder = 1
    end
    object RbTOP2: TRadioGroup
      Left = 264
      Top = 2
      Width = 121
      Height = 62
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Cao nh'#7845't'
        'Th'#7845'p nh'#7845't')
      ParentFont = False
      TabOrder = 2
    end
  end
end
