unit crTHOIGIAN;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, wwdbdatetimepicker, ComCtrls, ExtCtrls;

type
  TframeTHOIGIAN = class(TCrFrame)
    PaTHOIGIAN: TPanel;
    PgThoigian: TPageControl;
    TntTabSheet1: TTabSheet;
    TntLabel2: TLabel;
    TntLabel5: TLabel;
    CbTgThang: TComboBox;
    CbTgNam: TComboBox;
    TntTabSheet2: TTabSheet;
    TntLabel6: TLabel;
    TntLabel7: TLabel;
    CbTgQuy: TComboBox;
    CbTgNam1: TComboBox;
    TntTabSheet3: TTabSheet;
    TntLabel8: TLabel;
    CbTgNam2: TComboBox;
    TntTabSheet4: TTabSheet;
    TntLabel9: TLabel;
    TntLabel10: TLabel;
    CbTgTungay: TwwDBDateTimePicker;
    CbTgDenngay: TwwDBDateTimePicker;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;
    function Validate(pType: Integer = 0): Boolean; override;

  end;

var
  frameTHOIGIAN: TframeTHOIGIAN;

implementation

{$R *.dfm}
uses
    isStr, ExCommon, isLib, isMsg;

{ TframeTHOIGIAN }

procedure TframeTHOIGIAN.GetCriteria(var cr: array of Variant);
var
    n: Integer;
    mTgThang, mTgQuy, mTgNam: Integer;
    d1, d2: TDateTime;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := PgThoigian.ActivePageIndex;

    Inc(n);
    mTgThang := StrToInt(CbTgThang.Text);
    cr[n] := mTgThang;

    Inc(n);
    mTgQuy := StrToInt(CbTgQuy.Text);
    cr[n] := mTgQuy;

    Inc(n);
    case PgThoigian.ActivePageIndex of
    0:		// Thang
    begin
        mTgNam := StrToInt(CbTgNam.Text);
        cr[n] := mTgNam;
        d1 := EncodeDate(mTgNam, mTgThang, 1);
        d2 := IncMonth(d1) - 1;
    end;
    1:		// Quy
    begin
        mTgNam := StrToInt(CbTgNam1.Text);
        cr[n] := mTgNam;
        d1 := EncodeDate(mTgNam, (mTgQuy - 1) * 3 + 1, 1);
        d2 := IncMonth(d1, 3) - 1;
    end;
    2:		// Nam
    begin
        mTgNam := StrToInt(CbTgNam2.Text);
        cr[n] := mTgNam;
        d1 := EncodeDate(mTgNam,  1,  1);
        d2 := EncodeDate(mTgNam, 12, 31);
    end
    else	// Tu den
    begin
        mTgNam := StrToInt(CbTgNam2.Text);
        cr[n] := mTgNam;
        d1 := CbTgTungay.DateTime;
        d2 := CbTgDenngay.DateTime;
    end;
    end;

    Inc(n);
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', d1);
    Inc(n);
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', d2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTHOIGIAN.GetParamNo: Integer;
begin
    Result := 7;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTHOIGIAN.Init;
    procedure SetPeriodList(Sender : TComboBox);
    var
	i : Integer;
    yy, mm: WORD;
    begin
        with Sender do
        begin
            DecodeDate (Date, yy, mm, yy);

            Items.Clear;
            for i := 1 to 4 do
                Items.Add(isPadLeft(IntToStr(i), 2));

            if (mm >= 1) and (mm <= 3) then
                i := 0
            else if (mm >= 4) and (mm <= 6) then
                i := 1
            else if (mm >= 7) and (mm <= 9) then
                i := 2
            else
                i := 3;
            ItemIndex := i;
        end;
    end;
begin
    inherited;
    isMonthList(CbTgThang, 0);
    SetPeriodList(CbTgQuy);

    isYearList([CbTgNam, CbTgNam1, CbTgNam2], 0);

    CbTgTungay.Date := Date;
    CbTgDenngay.Date := Date + EncodeTime(23, 59, 59, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTHOIGIAN.Validate(pType: Integer): Boolean;
begin
    case pType of
    0:
    begin
        case PgThoigian.ActivePageIndex of
        0: Result := True;
        1, 2: Result := False;
        else
            Result := SameMonth(CbTgTungay.Date, CbTgDenngay.Date);
        end;

        if not Result then
            ErrMsg(RS_THESAME_MONTH);
    end;
    else
        Result := True;
    end;
end;

end.
