unit crSILE;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeSILE = class(TCrFrame)
    PaSILE: TPanel;
    RbLe: TRadioButton;
    RbSi: TRadioButton;
    RbSile: TRadioButton;
  private
  protected
    procedure Init; override;

  public
    function GetCode: Variant; override;
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant); override;
  end;

var
  frameSILE: TframeSILE;

implementation

{$R *.dfm}
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSILE.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSILE.GetCode: Variant;
begin
    if RbLe.Checked then
        Result := 'B'
    else if RbSi.Checked then
        Result := 'X'
    else
        Result := 'BX';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSILE.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSILE.Init;
begin
    inherited;
    RbSile.Checked:= True;
end;

end.
