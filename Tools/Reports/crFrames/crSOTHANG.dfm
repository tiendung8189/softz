inherited frameSOTHANG: TframeSOTHANG
  Height = 46
  ExplicitHeight = 46
  object PaSOLUONG: TPanel
    Left = 0
    Top = 0
    Width = 396
    Height = 46
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object lbTntLabel14: TLabel
      Left = 115
      Top = 15
      Width = 51
      Height = 16
      Alignment = taRightJustify
      Caption = 'S'#7889' th'#225'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdValue: TSpinEdit
      Left = 172
      Top = 12
      Width = 85
      Height = 26
      MaxLength = 9
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 3
    end
  end
end
