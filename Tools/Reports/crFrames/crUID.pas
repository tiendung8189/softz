unit crUID;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, Mask, Wwdbcomb, ExtCtrls, wwdbedit, Wwdotdot;

type
  TframeUID = class(TCrFrame)
    PaLOAINV: TPanel;
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;
  end;

var
  frameUID: TframeUID;

implementation

{$R *.dfm}
uses    isCommon;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeUID.GetCode: Variant;
begin
    Result := sysLogonUID;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeUID.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeUID.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeUID.Init;
begin
end;

end.
