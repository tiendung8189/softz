﻿unit crSOTHANG;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, Mask, Spin;

type
  TframeSOTHANG = class(TCrFrame)
    PaSOLUONG: TPanel;
    lbTntLabel14: TLabel;
    EdValue: TSpinEdit;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameSOTHANG: TframeSOTHANG;

implementation

{$R *.dfm}

{ TframeSOTHANG }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSOTHANG.GetCode: Variant;
begin
    Result := EdValue.Value
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSOTHANG.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSOTHANG.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSOTHANG.Init;
begin
  inherited;

end;

end.
