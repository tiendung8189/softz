unit crMAVIP;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeMAVIP = class(TCrFrame)
    PaMADT: TPanel;
    Label36: TLabel;
    EdMaDT: TEdit;
    EdTenDT: TEdit;
    procedure EdMaDTExit(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameMAVIP: TframeMAVIP;

implementation

{$R *.dfm}

uses
    MainData, ExCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVIP.EdMaDTExit(Sender: TObject);
var
    s: String;
begin
    s := (Sender as TEdit).Text;

    exDotVip(s);
    with DataMain.QrDMVIP do
    begin
        if Locate('MAVIP', s, []) then
        begin
            EdMaDT.Text := s;
            EdTenDT.Text := FieldByName('HOTEN').AsString;
        end
        else
        begin
            EdTenDT.Clear;
            EdMaDT.Clear;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeMAVIP.GetCode: Variant;
begin
    Result := EdMaDT.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVIP.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeMAVIP.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAVIP.Init;
begin
    inherited;
    with DataMain.QrDMVIP do
    if not Active then
        Open;
end;

end.
