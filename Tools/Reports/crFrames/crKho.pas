unit crKho;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook;

type
  TframeCrKho = class(TCrFrame)
    PaKHO: TPanel;
    Label27: TLabel;
    CbMAKHO: TwwDBLookupCombo;
    CbTENKHO: TwwDBLookupCombo;
    procedure CbMAKHOChange(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameCrKho: TframeCrKho;

implementation
uses
    SysUtils, Excommon, isCommon;

{$R *.dfm}

{ TframeCrKho }

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrKho.CbMAKHOBeforeDropDown(Sender: TObject);
begin
  inherited;
  if not sysIsCentral then
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrKho.CbMAKHOChange(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).DisplayValue = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    1:
        CbTENKHO.LookupValue := s;
    2:
        CbMAKHO.LookupValue := s;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrKho.GetCode: Variant;
begin
    if CbMAKHO.DisplayValue <> '' then
        Result := CbMAKHO.LookupValue
    else
        Result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrKho.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrKho.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrKho.Init;
begin
  inherited;
  with CbMAKHO.LookupTable do
    if not Active then
        Open;
  CbMAKHO.LookupValue := sysDefKho;
end;

end.
