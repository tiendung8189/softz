unit crDS_KH;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_KH = class(TCrFrame)
    PaDS_KH: TPanel;
    Label4: TLabel;
    EdDS_KH: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected

  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_KH: TframeDS_KH;

implementation
uses
    ChonDsKH;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KH.GetCode: Variant;
begin
    Result := Trim(EdDS_KH.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KH.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsKH.Get(s) then
    	Exit;

    with EdDS_KH do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
