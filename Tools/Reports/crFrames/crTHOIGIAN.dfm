inherited frameTHOIGIAN: TframeTHOIGIAN
  Height = 72
  ExplicitHeight = 72
  object PaTHOIGIAN: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 72
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    object PgThoigian: TPageControl
      Left = 1
      Top = 1
      Width = 393
      Height = 70
      ActivePage = TntTabSheet4
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object TntTabSheet1: TTabSheet
        Caption = ' Th'#225'ng '
        object TntLabel2: TLabel
          Left = 78
          Top = 12
          Width = 36
          Height = 16
          Alignment = taRightJustify
          Caption = 'Th'#225'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel5: TLabel
          Left = 200
          Top = 12
          Width = 26
          Height = 16
          Alignment = taRightJustify
          Caption = 'N'#259'm'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbTgThang: TComboBox
          Left = 122
          Top = 8
          Width = 53
          Height = 24
          Style = csDropDownList
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 0
        end
        object CbTgNam: TComboBox
          Left = 234
          Top = 8
          Width = 73
          Height = 24
          Style = csDropDownList
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 1
        end
      end
      object TntTabSheet2: TTabSheet
        Caption = ' Qu'#253' '
        object TntLabel6: TLabel
          Left = 85
          Top = 12
          Width = 22
          Height = 16
          Alignment = taRightJustify
          Caption = 'Qu'#253
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel7: TLabel
          Left = 193
          Top = 12
          Width = 26
          Height = 16
          Alignment = taRightJustify
          Caption = 'N'#259'm'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbTgQuy: TComboBox
          Left = 115
          Top = 8
          Width = 53
          Height = 24
          Style = csDropDownList
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 0
          Items.Strings = (
            '1'
            '2'
            '3'
            '4')
        end
        object CbTgNam1: TComboBox
          Left = 227
          Top = 8
          Width = 73
          Height = 24
          Style = csDropDownList
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 1
        end
      end
      object TntTabSheet3: TTabSheet
        Caption = ' N'#259'm '
        object TntLabel8: TLabel
          Left = 139
          Top = 12
          Width = 26
          Height = 16
          Alignment = taRightJustify
          Caption = 'N'#259'm'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbTgNam2: TComboBox
          Left = 173
          Top = 8
          Width = 73
          Height = 24
          Style = csDropDownList
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ItemHeight = 16
          ParentFont = False
          TabOrder = 0
        end
      end
      object TntTabSheet4: TTabSheet
        Caption = ' T'#7915' ng'#224'y - '#273#7871'n ng'#224'y '
        object TntLabel9: TLabel
          Left = 23
          Top = 12
          Width = 47
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel10: TLabel
          Left = 199
          Top = 12
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Caption = #208#7871'n ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbTgTungay: TwwDBDateTimePicker
          Left = 77
          Top = 8
          Width = 101
          Height = 24
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Epoch = 1950
          Frame.FocusBorders = []
          Frame.NonFocusBorders = []
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object CbTgDenngay: TwwDBDateTimePicker
          Left = 261
          Top = 8
          Width = 101
          Height = 24
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Epoch = 1950
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 1
        end
      end
    end
  end
end
