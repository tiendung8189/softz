inherited frameSILE: TframeSILE
  Height = 49
  ExplicitHeight = 49
  object PaSILE: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object RbLe: TRadioButton
      Left = 52
      Top = 16
      Width = 65
      Height = 17
      Caption = 'B'#225'n l'#7867
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object RbSi: TRadioButton
      Left = 163
      Top = 16
      Width = 65
      Height = 17
      Caption = 'B'#225'n s'#7881
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object RbSile: TRadioButton
      Left = 272
      Top = 16
      Width = 65
      Height = 17
      Caption = 'C'#7843' hai'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TabStop = True
    end
  end
end
