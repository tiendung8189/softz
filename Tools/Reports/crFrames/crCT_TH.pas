unit crCT_TH;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeCT_TH = class(TCrFrame)
    PaCT_TH: TPanel;
    RbCT: TRadioButton;
    RbTH: TRadioButton;
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameCT_TH: TframeCT_TH;

implementation

{$R *.dfm}

{ TframeCT_TH }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCT_TH.GetCode: Variant;
begin
    if RbCT.Checked then
        Result := 0
    else
        Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCT_TH.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCT_TH.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCT_TH.Init;
begin
    inherited;

end;

end.
