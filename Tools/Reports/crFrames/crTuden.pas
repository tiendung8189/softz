﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit crTuden;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, crCommon;

type
  TframeTuden = class(TCrFrame)
    PaTUDEN: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    EdDen: TwwDBDateTimePicker;
    EdTu: TwwDBDateTimePicker;
    procedure PaTUDENExit(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure Init2(pTu, pDen: TDateTime);
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant); override;
    function Validate(pType: Integer = 0): Boolean; override;

  end;
var
	frameTuden: TframeTuden;

implementation

{$R *.dfm}

uses
    ExCommon, isMSG, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TframeTuden.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    n := Length(cr) - GetParamNo;
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdTu.DateTime);
    inc(n);
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdDen.DateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTuden.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.Init;
var
    pTu, pDen: TDateTime;
begin
    pDen := Date + EncodeTime(23, 59, 59, 0);
    pTu := pDen - sysLateDay;
    Init2(pTu, pDen);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.Init2(pTu, pDen: TDateTime);
begin
    EdTu.Date := pTu;
    EdDen.Date := pDen;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_REPORTTIME = 'Thời gian không hợp lệ.';

procedure TframeTuden.PaTUDENExit(Sender: TObject);
begin
    if EdDen.DateTime < EdTu.DateTime then
    begin
        ErrMsg(RS_INVALID_REPORTTIME);
        EdTu.SelectAll;
        EdTu.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTuden.Validate(pType: Integer): Boolean;
begin
    case pType of
    0:
    begin
        Result := SameMonth(EdTu.Date, EdDen.Date);
        if not Result then
        begin
            ErrMsg(RS_THESAME_MONTH);
            EdTu.SelectAll;
            EdTu.SetFocus;
        end;
    end;
    else
        Result := True;
    end;
end;

end.

