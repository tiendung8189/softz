﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtCT;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid, wwdblook;

type
  TFrmDmvtCT = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    ToolButton9: TToolButton;
    QrDetail: TADOQuery;
    Image1: TImage;
    CmdAudit: TAction;
    QrDetailMABH: TWideStringField;
    QrDetailMAVT: TWideStringField;
    QrDetailMADT: TWideStringField;
    QrDetailMAMAU: TWideStringField;
    QrDetailMASIZE: TWideStringField;
    QrDetailGIABAN: TFloatField;
    QrDetailLK_TENDT: TWideStringField;
    QrDetailLK_TENMAU: TWideStringField;
    QrDetailLK_TENSIZE: TWideStringField;
    CbMau: TwwDBLookupCombo;
    CbSize: TwwDBLookupCombo;
    QrDetailRSTT: TIntegerField;
    QrDetailCREATE_BY: TIntegerField;
    QrDetailUPDATE_BY: TIntegerField;
    QrDetailCREATE_DATE: TDateTimeField;
    QrDetailUPDATE_DATE: TDateTimeField;
    CmdPhanbo: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrDetailBeforeInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDetailMADTChange(Sender: TField);
    procedure QrDetailAfterInsert(DataSet: TDataSet);
    procedure QrDetailMABHValidate(Sender: TField);
    procedure QrDetailMABHChange(Sender: TField);
    procedure QrDetailCalcFields(DataSet: TDataSet);
    procedure CmdPhanboExecute(Sender: TObject);
  private
    mCanEdit: Boolean;
    mMavt, mTen, mNhom, mPrefix, mSize, mMau, sCodeLen: String;
    mGiaban, mGiasi: Double;
  	mTrigger, mEANConfirm: Boolean;
  public
  	procedure Execute(pCanEdit: Boolean; pMa, pTen, pNhom, pPrefix: String);
  end;

var
  FrmDmvtCT: TFrmDmvtCT;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, isCommon, isBarcode, DmvtPB, Rights;

{$R *.DFM}

const
	FORM_CODE = 'DM_HH_CT';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.Execute;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;

    mMavt := pMa;
    mTen := pTen;
    mNhom := pNhom;
    mPrefix := pPrefix;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdNewExecute(Sender: TObject);
begin
	QrDetail.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdPhanboExecute(Sender: TObject);
begin
    if DataMain.GetRights('SZ_MAVT_CT_PB') = R_DENY then
        Exit;

    with QrDetail do
    begin
        Application.CreateForm(TFrmDmvtPB, FrmDmvtPB);
        FrmDmvtPB.Execute(mCanEdit,
            FieldByName('MABH').AsString, mTen);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//	HideAudit;
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDetail, FORM_CODE);
    mEANConfirm := FlexConfigBool('DM_HH', 'EAN Confirm');
    sCodeLen := FlexConfigString('DM_HH', 'Code Length');
    if not DataMain.GetFuncState('SZ_MAVT_CT_PB') then
    begin
        CmdPhanbo.Visible := False;
        ToolButton9.Visible := False;
    end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.FormShow(Sender: TObject);
begin
//    with DataMain do
//        OpenDataSets([QrDMNCC, QrMAU, QrSIZE]);
	OpenDataSets([QrDetail]);
    SetDisplayFormat(QrDetail, sysCurFmt);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';
procedure TFrmDmvtCT.QrDetailBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with QrDetail do
	begin
		if BlankConfirm(QrDetail, ['MABH', 'MADT']) then
			Abort;

        if FieldByName('LK_TENDT').AsString = '' then
        begin
            ErrMsg('Lỗi nhập liệu. Sai mã nhà cung cấp.');
            Abort;
        end;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MABH').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;

		FieldByName('MAVT').AsString := mMavt;
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';
procedure TFrmDmvtCT.QrDetailMABHChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    // Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailMABHValidate(Sender: TField);
begin
    with QrDetail do
        if FieldByName('MABH').OldValue <> null then
            if DataMain.BarcodeIsUsed(FieldByName('MABH').OldValue) then
                Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailMADTChange(Sender: TField);
begin
    exDotMancc(DataMain.QrDMNCC, Sender);
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    if DataMain.BarcodeIsUsed(QrDetail.FieldByName('MABH').AsString) then
        Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    with QrDetail do
    begin
        mMau := FieldByName('MAMAU').AsString;
        mSize := FieldByName('MASIZE').AsString;
        mGiaban := FieldByName('GIABAN').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailAfterInsert(DataSet: TDataSet);
begin
    with QrDetail do
    begin
        FieldByName('MABH').AsString := DataMain.AllocEAN(mPrefix, mNhom);

        FieldByName('MAMAU').AsString := mMau;
        FieldByName('MASIZE').AsString := mSize;
        FieldByName('GIABAN').AsFloat := mGiaban;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := mMavt;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtCT.QrDetailCalcFields(DataSet: TDataSet);
begin
    with QrDetail do
        FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

end.
