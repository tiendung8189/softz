object FrmPosVIP: TFrmPosVIP
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin VIP'
  ClientHeight = 316
  ClientWidth = 535
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Padding.Left = 8
  Padding.Top = 8
  Padding.Right = 8
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    535
    316)
  PixelsPerInch = 96
  TextHeight = 16
  object Image1: TImage
    Left = 11
    Top = 280
    Width = 32
    Height = 32
    Anchors = [akRight, akBottom]
    AutoSize = True
    Picture.Data = {
      0D546478536D617274496D61676589504E470D0A1A0A0000000D494844520000
      0020000000200806000000737A7AF40000000467414D410000B18F0BFC610500
      0000097048597300000B1300000B1301009A9C1800000489494441545847ED56
      6D4C5B5518268B3171893F96A16E41B3A4500A73685CF4872E8EAFED8764BAC4
      08129085C1FA31940FD73191081D6C3F10757130051258548C328644893298DB
      C01137A51F97021D030638A5F49B72FB41656BEFEB39B7A7F516EE0C6A9BF8C3
      2779D29C7BDEE77DDE73DE736E6FD4FFF8DB50C0865889FAD978B13A2F4EAAD9
      BB55A2DA4866228F44A946182F5151F1120D04B8BD88723F5DA66DC54591B0C8
      0019478BC4AA79AEF96A0AC51A39090F3F8412F5477CA65C8AC46A5F6CA13A8E
      48C28747DFBCF6408254E3E1335DCD0499A682C8C2877829B58BCF0CB3FA9349
      1049FF1C3F7344DB4564E183484265724D03949DBE0106FD0234F7DC0A3EDB75
      54DB4764E103EAED4B5C63CC23CD13A09F37C0D2D212984D2668EF9F85A4220A
      52DFD6761059F8203C3C2C0818EFA91C85EE815930190C60B7DBC1E974B2B498
      CDA01CFB0D32AAB4FB892CBC78E19DD19EAE8139B46A3D988D46A0D1CA03E601
      A2820C243CFC00800D56B37999BB6A1E9E25E1910132A85B65184287C3B19B84
      4606344D6F465CE03347EC26619105324A4227DFC735476387C562799084441E
      E80678F0D5C3271F1F489BCD76994C4516F820A21688AC168BD7B8B00098B808
      B40BD30CC34476074A5AAD8AAA4EBBC7E5F6B8F0B63B681A963837C2B1ECB195
      7F6E771F6AB21612C9BF474B8F7E63F9D9D933FB6B748BE9272CB0E7A415E865
      065C6E77D01873915E06FDA217D2D17CCA7123E4D6EBE6AADB678B499A7F8663
      6DD34DCFC947EFE2B79F484AB1C9317FB8B18256EB0597CB054EDAC6FEBA3C5E
      F856E361E7D350A1F1E4CFE9C59A71BAF6CBB91C92727D50748EDF9FFB9E6E2E
      F0EAC54C7C5D1B2CE0C35E173A0A00CE5FBE039F2A0FECF3D7D971ED578E604C
      826C24A8DD7E9882A32DD3EB7F4115374DF572CD59A21DD851AA63931FF8D80E
      E0BB038CFA2030A80066E40D601880974F2D42FA092B3C5E3C06221915A24F44
      E3931DB73389C55F23E3B8CEC915630A0E29E129F954708566DA07DE5F3BC1AB
      CC059FE90ACC98FCFDC74C2AD3A14F3375881EB3E8CCD4FAAEEACE120DB33A81
      A0E067D851E2DF01CC7EEDEF7067BE1FE8EFF7C15D8B12BA7EF2F71F53543402
      71A860AE1EE7C36D2516F746FDF99927B0605BFEB590047162156CCEBC08A9B5
      66D6A4EE1B2798C7BF600B70DEBE04951DFEFE27D71861D32B7D6B7600E7DB5B
      39E62636F746F5A733722CD8923310920033266F081DC651D628EBF422DC1C6A
      640BB8397C0EF6D5DBD8E742990A1E3BF063880E17B3257790FD58510CC07DC4
      8A1FA5CDD35D58F450F66510140E87240A3C4FA931B1667DDD0D6C01E7BBCFB1
      E3E715067838FBD21ACDB6FCEBC105293EBB9546ACF8917F6A72020762A398D7
      AEAE492628180611BA56D8B0B1AD9D2DA0B6E5023B168895105B18DA7BCCAD39
      83C1022ADAA6EB88153F32AAFC370017109D75714D32CC98BCABC8D004650D83
      6C01073FA020ADC6C0B6882F7E53667FB080A2C6C92BC48A1FF2D62963CA5BDA
      95B88241EF23D91798E463232B78CCE5EE726A6567A986C97F9F026D7701F3EA
      BB13F064B18A373619C54667F53209E2216F6A8576A5E1EBB91E62F55F4154D4
      1F0B38A61EB46A6E640000000049454E44AE426082}
    ExplicitTop = 309
  end
  object CmdReturn: TBitBtn
    Left = 328
    Top = 283
    Width = 97
    Height = 25
    Cursor = 1
    Action = CmdSave
    Anchors = [akRight, akBottom]
    Caption = 'Ti'#7871'p t'#7909'c'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
    ExplicitTop = 264
  end
  object BtnCancel: TBitBtn
    Left = 428
    Top = 283
    Width = 97
    Height = 25
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'K'#7871't th'#250'c'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFEEEDF8A6A5E06765CE4946CB4946CB6765CEA6A5E0EEEDF8FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A6E20B07CD100CED120EFE13
      0EFF130EFF120EFE100CEE0B07CDA7A6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      8887E0100EE91412FC1313FC110FE90F0DE00F0DE0110FEA1211F71312FB100E
      EA8180DFFFFFFFFFFFFFFFFFFFA7A6E41214EB1619FC1416F21F1ED4AAA9ECAE
      AEE4B5B4E73C39C71315EF1416F61518F91214EBA7A6E4FFFFFFEEEEFA1013DB
      171FFB171DF18C8CECF6F6FDFFFFFFFFFFFF918FDF1A1FE3161EF7161DF4161E
      F6171EFA1013DBEEEEFAA8A7EC151EF11B27FB2224D9FFFFFFFFFFFFFFFFFF88
      85E0141BE61924F91923F61924F5151AE71A25F8151EF2A8A7EC7373E52739FD
      1521F1AAA9EDFFFFFFFFFFFF817EE1141BE31B2CF91B2AF61B2BF81113D93B38
      CF1521F02739FD7373E56062E6536CFF202FECB4B3F1FFFFFF8885E6161EE41E
      32F81C31F61E34F9161DE4918EE5C0BFF1202FEC546CFF5F61E35F61EC607BFF
      414FF1C1BFF4928EEA1820E62139F92037F62139F81821E98985E7FFFFFFB5B4
      F2414FF1607AFF5D5FE07978F15F77FF4C60F73D37E21318E5253FFA243CF826
      41FA1B26EC837EE8FFFFFFFFFFFFADABF54C60F95C75F87675E4ABA8F55C6CFD
      5E7CFE4251F43650FA223EF82845FA202FF28A86EEFFFFFFFFFFFFFFFFFF3B40
      EE6281FF5869E9A9A7EEEEEDFD4042F57490FF5E7BFD5C78FC3D5CFB2637F393
      90F2FFFFFFFFFFFFF6F6FF9B9EF9556DFC7591FD393BD2EEEDFDFFFFFFA8A6F8
      646EFA7A94FE617EFC5A73FB4843EFB6B3F9B1AEFAB4B5FB4047F35873FC7F9D
      FF5B65DFA6A4ECFFFFFFFFFFFFFFFFFF8D89F86772F98CA8FE7695FF5A72FC4F
      64FA4E63F95870FC7D9DFF8EAAFE636EE48887DAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFA8A6F84445F17A88F793A9FD98B1FF98B2FF92A9FB7988EF4042DFA6A4
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEDFEAEACF67E7CF369
      69EF6869EE7C7CEEADABEDEEEDFDFFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 2
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 3
    ExplicitTop = 264
  end
  object isPanel1: TisPanel
    Left = 8
    Top = 8
    Width = 519
    Height = 181
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 0
    HeaderCaption = ' :: Th'#244'ng tin c'#225' nh'#226'n'
    HeaderColor = 16119285
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object Label6: TLabel
      Left = 56
      Top = 56
      Width = 37
      Height = 16
      Alignment = taRightJustify
      Caption = 'H'#7885' t'#234'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 54
      Top = 104
      Width = 39
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7883'a ch'#7881
      FocusControl = EdDChi
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 35
      Top = 128
      Width = 58
      Height = 16
      Alignment = taRightJustify
      Caption = #272'i'#7879'n tho'#7841'i'
      FocusControl = EdDienthoai
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 62
      Top = 152
      Width = 31
      Height = 16
      Alignment = taRightJustify
      Caption = 'Email'
      FocusControl = DBEdit6
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 59
      Top = 32
      Width = 34
      Height = 16
      Alignment = taRightJustify
      Caption = 'M'#227' s'#7889
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TntLabel3: TLabel
      Left = 37
      Top = 80
      Width = 55
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y sinh'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 227
      Top = 80
      Width = 34
      Height = 16
      Alignment = taRightJustify
      Caption = 'CMND'
      FocusControl = wwDBEdit1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdHo: TwwDBEdit
      Tag = 1
      Left = 100
      Top = 52
      Width = 165
      Height = 22
      CharCase = ecUpperCase
      Color = 15794175
      Ctl3D = False
      DataField = 'HO'
      DataSource = DsDMVIP
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object EdTen: TwwDBEdit
      Tag = 1
      Left = 268
      Top = 52
      Width = 62
      Height = 22
      CharCase = ecUpperCase
      Color = 15794175
      Ctl3D = False
      DataField = 'TEN'
      DataSource = DsDMVIP
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object EdDChi: TwwDBEdit
      Left = 100
      Top = 100
      Width = 405
      Height = 22
      Ctl3D = False
      DataField = 'DCHI'
      DataSource = DsDMVIP
      ParentCtl3D = False
      TabOrder = 5
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object EdDienthoai: TwwDBEdit
      Left = 100
      Top = 124
      Width = 165
      Height = 22
      Ctl3D = False
      DataField = 'DTHOAI'
      DataSource = DsDMVIP
      ParentCtl3D = False
      TabOrder = 6
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object DBEdit6: TwwDBEdit
      Left = 100
      Top = 148
      Width = 405
      Height = 22
      Ctl3D = False
      DataField = 'EMAIL'
      DataSource = DsDMVIP
      ParentCtl3D = False
      TabOrder = 7
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object DBEdit9: TwwDBEdit
      Tag = 1
      Left = 100
      Top = 28
      Width = 165
      Height = 22
      TabStop = False
      Color = 15794175
      Ctl3D = False
      DataField = 'MASO'
      DataSource = DsDMVIP
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object CbNGAY: TwwDBDateTimePicker
      Left = 100
      Top = 76
      Width = 101
      Height = 22
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NGAYSINH'
      DataSource = DsDMVIP
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 3
    end
    object wwDBEdit1: TwwDBEdit
      Left = 268
      Top = 76
      Width = 165
      Height = 22
      Ctl3D = False
      DataField = 'CMND'
      DataSource = DsDMVIP
      ParentCtl3D = False
      TabOrder = 4
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object PD2: TisPanel
    Left = 8
    Top = 189
    Width = 519
    Height = 87
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    ParentBackground = False
    TabOrder = 1
    HeaderCaption = ' .: Th'#244'ng tin VIP'
    HeaderColor = 16119285
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object TntLabel1: TLabel
      Left = 65
      Top = 32
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label25: TLabel
      Left = 264
      Top = 32
      Width = 97
      Height = 16
      Alignment = taRightJustify
      Caption = 'Doanh s'#7889' t'#237'ch l'#361'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TntLabel4: TLabel
      Left = 47
      Top = 55
      Width = 46
      Height = 16
      Alignment = taRightJustify
      Caption = 'Lo'#7841'i VIP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object wwDBDateTimePicker3: TwwDBDateTimePicker
      Left = 100
      Top = 28
      Width = 137
      Height = 22
      TabStop = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Color = 15794175
      DataField = 'NGAYDSO'
      DataSource = DsDMVIP
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 2
    end
    object DBEdit11: TwwDBEdit
      Tag = 1
      Left = 368
      Top = 28
      Width = 137
      Height = 22
      TabStop = False
      BorderStyle = bsNone
      Color = 15794175
      Ctl3D = False
      DataField = 'DOANHSO'
      DataSource = DsDMVIP
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object CbLoai: TwwDBLookupCombo
      Left = 100
      Top = 52
      Width = 212
      Height = 22
      TabStop = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'DGIAI'#9'0'#9'DGIAI'#9'F')
      DataField = 'LOAI'
      DataSource = DsDMVIP
      LookupTable = DataMain.QrLOAI_VIP_LE
      LookupField = 'MA'
      Color = 15794175
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ReadOnly = True
      TabOrder = 3
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
ButtonEffects.Transparent=True
    end
  end
  object QrDMVIP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeEdit = QrDMVIPBeforeEdit
    BeforePost = QrDMVIPBeforePost
    OnPostError = exDbError
    Parameters = <
      item
        Name = 'MAVIP'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DM_VIP'
      ' where'#9'MAVIP = :MAVIP')
    Left = 384
    Top = 65528
    object QrDMVIPMASO: TWideStringField
      DisplayLabel = 'M'#227' s'#7889
      FieldName = 'MASO'
      Size = 15
    end
    object QrDMVIPHO: TWideStringField
      DisplayLabel = 'H'#7885
      FieldName = 'HO'
      OnChange = QrDMVIPHOChange
      Size = 40
    end
    object QrDMVIPTEN: TWideStringField
      DisplayLabel = 'T'#234'n'
      FieldName = 'TEN'
      OnChange = QrDMVIPHOChange
      Size = 10
    end
    object QrDMVIPDCHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881
      FieldName = 'DCHI'
      Size = 250
    end
    object QrDMVIPDTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'DTHOAI'
      Size = 250
    end
    object QrDMVIPEMAIL: TWideStringField
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Size = 250
    end
    object QrDMVIPTL_CK: TFloatField
      DisplayLabel = 'T'#7927' l'#7879' chi'#7871't kh'#7845'u'
      FieldName = 'TL_CK'
      Visible = False
    end
    object QrDMVIPMAVIP: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MAVIP'
      Visible = False
      Size = 13
    end
    object QrDMVIPCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMVIPUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMVIPDOANHSO: TFloatField
      FieldName = 'DOANHSO'
    end
    object QrDMVIPNGAYDSO: TDateTimeField
      FieldName = 'NGAYDSO'
    end
    object QrDMVIPNGAYSINH: TDateTimeField
      FieldName = 'NGAYSINH'
    end
    object QrDMVIPCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDMVIPUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDMVIPLOAI: TWideStringField
      FieldName = 'LOAI'
      Size = 15
    end
    object QrDMVIPNGAY_THAMGIA: TDateTimeField
      FieldName = 'NGAY_THAMGIA'
    end
    object QrDMVIPLOC_THAMGIA: TWideStringField
      FieldName = 'LOC_THAMGIA'
      Size = 2
    end
    object QrDMVIPHOTEN: TWideStringField
      FieldName = 'HOTEN'
      Size = 200
    end
    object QrDMVIPSOTIEN_MUA: TFloatField
      FieldName = 'SOTIEN_MUA'
    end
    object QrDMVIPSOTIEN_TRA: TFloatField
      FieldName = 'SOTIEN_TRA'
    end
    object QrDMVIPDOANHSO_DAU: TFloatField
      FieldName = 'DOANHSO_DAU'
    end
    object QrDMVIPDOANHSO_MUA: TFloatField
      FieldName = 'DOANHSO_MUA'
    end
    object QrDMVIPTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      Size = 2
    end
    object QrDMVIPCMND: TWideStringField
      FieldName = 'CMND'
      Size = 100
    end
  end
  object DsDMVIP: TDataSource
    DataSet = QrDMVIP
    Left = 412
    Top = 65528
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 276
    Top = 65524
    object CmdSave: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
  end
end
