(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThuHT;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2, AdvMenus, AppEvnts, isPanel, wwfltdlg,
  frameNgay, frameNavi, wwDialog, Grids, Wwdbigrd, Wwdbgrid, ToolWin, Wwdotdot,
  Wwdbcomb;

type
  TFrmThuHT = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMADT: TWideStringField;
    QrTCMST: TWideStringField;
    QrTCNGUOI: TWideStringField;
    QrTCSOTIEN: TFloatField;
    QrTCDGIAI: TWideMemoField;
    QrTCLK_TENDT: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    PopupMenu2: TAdvPopupMenu;
    Bevel1: TBevel;
    PaMaster: TisPanel;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    HanbnlCVAT1: TMenuItem;
    N2: TMenuItem;
    Hanbnl1: TMenuItem;
    QrTCLCT: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrTCDRC_STATUS: TWideStringField;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label31: TLabel;
    LbNHAPCUA: TLabel;
    Label3: TLabel;
    DBText1: TDBText;
    CbKHO: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    CbNGAY: TwwDBDateTimePicker;
    EdSCT: TwwDBEdit;
    EdMaDT: TwwDBEdit;
    EdTenDT: TwwDBEdit;
    CbHinhThuc: TwwDBLookupCombo;
    PaTK: TPanel;
    TntLabel1: TLabel;
    Label4: TLabel;
    CbSTK: TwwDBLookupCombo;
    CbTenTK: TwwDBLookupCombo;
    EdNganhang: TwwDBEdit;
    EdChinhanh: TwwDBEdit;
    Panel4: TPanel;
    Label10: TLabel;
    Label18: TLabel;
    Label5: TLabel;
    LbDrcStatus: TLabel;
    CbDrcStatus: TwwDBComboBox;
    DBMemo1: TDBMemo;
    EdNGUOIGIAO: TwwDBEdit;
    EdSOTIEN: TwwDBEdit;
    QrTCMATK: TWideStringField;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTCTHANHTOAN: TFloatField;
    QrTCLK_MANH: TWideStringField;
    QrTCLK_MACN: TWideStringField;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCMADTChange(Sender: TField);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure CbSTKBeforeDropDown(Sender: TObject);
  private
  	mMakho, mPTTT, mLCT: String;
	mCanEdit, mFuncTK: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;
  public
	procedure Execute (r : WORD);
  end;

var
  FrmThuHT: TFrmThuHT;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    isLib;

{$R *.DFM}

const
    FORM_CODE: String = 'PHIEU_THUHT';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.Execute;
begin
	mCanEdit := rCanEdit(r);
    if not mCanEdit then
    	QrTC.LockType := ltReadOnly;
    mLCT := 'THUHT';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

	mMakho := RegReadString(Name, 'Makho', sysDefKho);
    mPTTT := DataMain.GetSysParam('DEFAULT_PTTT');
    mFuncTK := DataMain.GetFuncState('SZ_TAIKHOAN');

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
    end;

    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormShow(Sender: TObject);
begin
    with DataMain do
    	OpenDataSets([QrPTTT, QrDMKHO, QrDM_KH_NCC, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);

    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho'], [mMakho]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
   	    CbNgay.SetFocus
	else
    	GrBrowse.SetFocus;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
		QrPTTT.Requery;
        QrDMKHO.Requery;
        QrDM_KH_NCC.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdNewExecute(Sender: TObject);
begin
	if not (QrTC.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
			QrTC.Cancel;

	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
        exSearch(Name, DsTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdClearFilterExecute(Sender: TObject);
begin
    with  filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not bEmpty;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := d;
		FieldByName('LCT').AsString    := mLCT;
//NCN        try
            FieldByName('MAKHO').AsString  := mMakho;
            FieldByName('LOC').AsString           := sysLoc;
            FieldByName('PTTT').AsString   := mPTTT;
            FieldByName('DRC_STATUS').AsString    := '1';
//        except
//        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
		Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforePost(DataSet: TDataSet);
begin
	with QrTC do
    begin
		if BlankConfirm(QrTC, ['NGAY', 'MAKHO', 'MADT', 'PTTT', 'SOTIEN']) then
	    	Abort;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

	    exValidClosing(FieldByName('NGAY').AsDateTime, 2);
    	SetNull(QrTC, ['PTTT', 'MAKHO', 'MADT']);	// NHY
    end;
	DataMain.AllocSCT(mLCT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCMADTChange(Sender: TField);
begin
    exDotMancc(Sender);
    EdTenDT.Text := QrTC.FieldByName('LK_TENDT').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrTC.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('THANHTOAN').AsFloat := FieldByName('SOTIEN').AsFloat
end;

(*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CbSTKBeforeDropDown(Sender: TObject);
begin
    exComboBeforeDropDownTK(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterPost(DataSet: TDataSet);
begin
	with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;



end.
