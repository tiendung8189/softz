(*==============================================================================
**------------------------------------------------------------------------------
*)
unit exCOMPort;

interface
uses
  Windows, Messages, Classes, SysUtils, IniFiles;
const
    MY_MESSAGE_COM_STATUS = WM_USER + 4010;

type
  TIcdAction = (ICD_NOTHING, ICD_IDLE, ICD_SCAN, ICD_TOTAL, ICD_THANKS);

  TExComPort = class(TThread)
  private
    FicdAction: TIcdAction;
    FicdTime: TDateTime;
    FSecsDelay: Double;

    FEvent: THandle;
    FHandle: THandle;
    FConnected, FActive, FActiveThread: Boolean;
    FIsScroll1, FIsScroll2: Boolean;

    FBaudRate, FAuto: Integer;
    FSerialValid: Byte;
    FPort, FSerial: String;
    FMaxLen: Integer;
    FX, FY: Integer;
    FStr1, FStr2: String;
    FStrIde1, FStrIde2: String;
    FStrIdeTmp1, FStrIdeTmp2: String;
    FStrSum, FStrThanks1, FStrThanks2: String;
  private
    FSWbemLocator : OLEVariant;
    FWMIService   : OLEVariant;

    procedure SetStrIde1(const Value: String);
    procedure SetStrIde2(const Value: String);
    procedure SetActiveThread(const Value: Boolean);
    procedure SetSerial(const Value: String);
    procedure SetSecsDelay(const Value: Double);

    function  GetWMIstring(const WMIClass, WMIProperty: String;
        const WMIWhere: string = ''): string;
  protected
    procedure PostMessageState;
    procedure Execute; override;

    property Connected: Boolean read FConnected;
  public
    constructor Create(const pPort: String = 'COM6'); overload;
    constructor Create(const pPort, pIdeStr1, pIdeStr2: String;
        const pLen: Integer = 20; const pBaudRate: Integer = 9600); overload;
    destructor Destroy; override;

    procedure StoreIniFile(IniFile: TIniFile);
    procedure LoadIniFile(IniFile: TIniFile);

    class procedure ReconectCom();
    class function  IcdComCreate(const pIdeStr1, pIdeStr2: String): Boolean;
    class procedure IcdComFree();
    procedure StoreSettings(pManual: Boolean = False);
    procedure LoadSettings();
    procedure OpenCom;
    procedure CloseCom;
    function  SetupComPort(): Boolean;
    procedure InitDisplay();
    function  IcdTest: Boolean;
    function  GetSerial: String;
    function  GetDeviceID: String;

    procedure IcdStar();
    procedure IcdProcess;
    procedure IcdScan(pTenHH, pGia, pTong: String);
    procedure IcdThanks();

    function  Write(const Buffer; Count: Integer): Integer;
    function  WriteStr(Str: String): Integer;

    function WriteUnicodeString(const Str: UnicodeString): Integer;
    function ReadUnicodeString(var Str: UnicodeString; Count: Integer): Integer;

    procedure MoveRight;
    procedure MoveLeft;
    procedure MoveUp;
    procedure MoveDown;
    procedure MoveHome;
    procedure MoveEnd;
    procedure MoveXY(pX, pY: Integer);
    procedure ClearScreen();
    procedure ClearLine(const pLine: Integer = 0);
    procedure WriteLine(const pStr: String; const pLine: Integer; pAlign: TAlignment = taLeftJustify);
    procedure ScrollHor(pStr: String; const pLine2: Boolean = False);

    function  ProlificComDetect: String;
    Class function  IsConnected: Boolean;
    class procedure EnuicdComPorts(Ports: TStrings);
    property Str1: String read FStr1 write FStr1;
    property Str2: String read FStr2 write FStr2;
    property StrIde1: String read FStrIde1 write SetStrIde1;
    property StrIde2: String read FStrIde2 write SetStrIde2;
    property StrSum: String read FStrSum write FStrSum;
    property StrThanks1: String read FStrThanks1 write FStrThanks1;
    property StrThanks2: String read FStrThanks2 write FStrThanks2;
    property Port: String read FPort write FPort;
    property SerialNo: String read FSerial write SetSerial;
    property BaudRate: Integer read FBaudRate write FBaudRate;
    property ActiveThread: Boolean read FActiveThread write SetActiveThread;
    property MaxLenOfLine: Integer read FMaxLen write FMaxLen;
    property SecsDelay: Double read FSecsDelay write SetSecsDelay;
  end;

var
    icdComPort: TExComPort;
    mgTerminateApp: Boolean;

implementation

uses
    isStr, isLib, Forms, ActiveX, ComObj, Variants;

const
    C_SEC0 = 1.0 / 24.0 / 60.0 / 60.0;
    C_SEC1 = 0.4 * 1.0 / 24.0 / 60.0 / 60.0;
    C_ICD_SECTION = 'ICD_COM';

const
	// ESC/POS constants standard commanding to customer display
    NLL = $0;
    MD1 = $1;
    MD2 = $2;
    MD3 = $3;
    MD4 = $4;
    MD5 = $5;
    MD6 = $6;
    MD7 = $7;
    MD8 = $8;
    BS = $8;
    HT = $9;
    LF = $A;
    HOM = $B;
    CLR = $C;
    CR = $D;
    SLE1 = $E;
    RS = $F;
    SLE2 = $F;
    DLE = $10;
    DC1 = $11;
    DC2 = $12;
    DC3 = $13;
    DC4 = $14;
    CAN = $18;
    ESC = $1B;
    GS = $1D;
    SF1 = $1E;
    US = $1F;

{ TExComPort }
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.ClearLine(const pLine: Integer);
begin
    if pLine = 2 then
    begin
        MoveXY(1, 2);
        FStr2 := isPadLeft('', MaxLenOfLine, ' ')
    end
    else
    begin
        MoveXY(1, 1);
        FStr1 := isPadLeft('', MaxLenOfLine, ' ');
    end;
    WriteStr(Chr($18));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.ClearScreen;
begin
    FStr2 := isPadLeft('', MaxLenOfLine, ' ');
    FStr1 := isPadLeft('', MaxLenOfLine, ' ');
    WriteStr(Chr($0C));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.CloseCom;
var
    n: THandle;
begin
    FConnected := False;
    try
        n := FHandle;
        FHandle := INVALID_HANDLE_VALUE;
        if n <> INVALID_HANDLE_VALUE then
            CloseHandle(n);
    except
    end;
    {
	if not Suspended then
		Suspend;
		Sleep(10);
    }
    PostMessageState;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TExComPort.Create(const pPort, pIdeStr1, pIdeStr2: String; const pLen, pBaudRate: Integer);
begin
    inherited Create(True);
    FEvent:= CreateEvent(nil,
                       False,    // auto reset
                       False,    // initial state = not signaled
                       nil);
    SecsDelay := 2;
    FreeOnTerminate := True;
    Priority :=  tpIdle;
    FActive := False;
    FActiveThread := True;
    FBaudRate := pBaudRate;
    FPort := pPort;
    FSerial := '';
    FSerialValid := 0;
    FMaxLen := pLen;
    FConnected := False;
    FHandle := INVALID_HANDLE_VALUE;
    StrIde1 := Iif(pIdeStr1<>'', pIdeStr1, 'KINH CHAO QUY KHACH');
    StrIde2 := Iif(pIdeStr2<>'', pIdeStr2, 'WELCOME TO SOFTZ');
    FStr1 := FStrIde1;
    FStr2 := FStrIde2;
    FicdAction := ICD_NOTHING;
    FStrSum := 'TONG CONG - TOTAL';
    FStrThanks1 := 'CAM ON QUY KHACH';
    FStrThanks2 := 'THANK YOU';
    FX := 1;
	FY := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TExComPort.Create(const pPort: String);
begin
	Create(pPort, 'KINH CHAO QUY KHACH', 'WELCOME TO SOFTZ ', 20, 9600);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TExComPort.Destroy;
begin
    CloseCom;
    inherited Destroy;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class procedure TExComPort.EnuicdComPorts(Ports: TStrings);
var
    KeyHandle: HKEY;
    ErrCode, Index: Integer;
    ValueName, Data: string;
    ValueLen, DataLen, ValueType: DWORD;
    TmpPorts: TStringList;
begin
	ErrCode := RegOpenKeyEx(
        HKEY_LOCAL_MACHINE,
        'HARDWARE\DEVICEMAP\SERIALCOMM',
        0,
        KEY_READ,
        KeyHandle);

	if ErrCode <> ERROR_SUCCESS then
	begin
    	//raise EComPort.Create(CError_RegError, ErrCode);
    	Exit;
      end;
	TmpPorts := TStringList.Create;

	try
        Index := 0;
        repeat
            ValueLen := 256;
            DataLen := 256;
            SetLength(ValueName, ValueLen);
            SetLength(Data, DataLen);
            ErrCode := RegEnumValue(
                KeyHandle,
                Index,
                PChar(ValueName),
                Cardinal(ValueLen),
                nil,
                @ValueType,
                PByte(PChar(Data)),
                @DataLen);

            if (ErrCode = ERROR_SUCCESS) then
            begin
            if (Pos('Prolific', ValueName) > 0) then
            begin
                SetLength(Data, DataLen - 1);
                TmpPorts.Add(Data);
            end;
            Inc(Index);
            end
            else
            if ErrCode <> ERROR_NO_MORE_ITEMS then break;
//                raise EComPort.Create(CError_RegError, ErrCode);
        until (ErrCode <> ERROR_SUCCESS);

        TmpPorts.Sort;
        Ports.Assign(TmpPorts);
	finally
    	RegCloseKey(KeyHandle);
    	TmpPorts.Free;
  	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.Execute;
var
  F : TSearchRec;
  d: TDateTime;
  s: String;
begin
    while not Terminated do
    begin
// Check Serial
//        if FSerialValid = 0 then
//        begin
//            if KcitResAsString <> '' then
//            begin
//                if KcitResAsString = 'T' then
//                    FSerialValid := 1
//                else if KcitResAsString = 'F' then
//                    FSerialValid := 2
//                else
//                    FSerialValid := StrToIntDef(KcitResAsString, 0);
//
//                if FSerialValid = 2 then
//                    WriteLine('$', 1);
//
//                if FSerialValid = 0 then
//                begin
//                    // ###########################################
//                    s := ChangeFileExt(Application.ExeName, '.ini');
//                    if FindFirst(s, faAnyFile, F) > -1 then
//                        d := F.TimeStamp;
//                    FindClose(F);
//                    if d > EncodeDate(2016, 3, 16) then
//                    begin
//                        Randomize;
//                        if Odd(Random(100)) then
//                            FSerialValid := 2;
//                    end;
//                end;
//            end;
//        end;

        if FActiveThread and FConnected and (FSerialValid <> 2) then
        begin
            IcdProcess;
            Sleep(10);
        end
        else
            WaitForSingleObject(FEvent, INFINITE);  // No more Work;

        if mgTerminateApp then
            Break;
    end;
    ClearScreen;
    WriteLine('Bye!', 1, taCenter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.GetDeviceID: String;
begin
    Result := GetWMIstring('Win32_PortConnector', '*', '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.GetSerial: String;
begin
    Result := GetWMIstring('Win32_PnPEntity','*', '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.GetWMIstring(const WMIClass, WMIProperty, WMIWhere: string): string;
const
      wbemFlagForwardOnly = $00000020;
var
    FWbemObjectSet: OLEVariant;
    FWbemObject   : OLEVariant;
    oEnum         : IEnumvariant;
    iValue        : LongWord;
begin;
    if (FSWbemLocator = Unassigned) then
    begin
        FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
        FWMIService   := FSWbemLocator.ConnectServer('localhost', 'root\CIMV2', '', '');
    end;

    Result:='';
    FWbemObjectSet:= FWMIService.ExecQuery(Format('Select %s from %s %s',[WMIProperty, WMIClass, WMIWhere])
        ,'WQL', wbemFlagForwardOnly);

    oEnum         := IUnknown(FWbemObjectSet._NewEnum) as IEnumVariant;

    if WMIProperty = '*' then
        while oEnum.Next(1, FWbemObject, iValue) = 0 do
        begin

            if not VarIsNull(FWbemObject.Name) then
                Result := Result + (Format('Name: %s',[String(FWbemObject.Name)])) + #13#10;

            try
                if not VarIsNull(FWbemObject.PNPDeviceID) then
                    Result := Result + (Format('%s',[String(FWbemObject.PNPDeviceID)])) + #13#10;
            except
            end;
        end
    else
    if oEnum.Next(1, FWbemObject, iValue) = 0 then
        if not VarIsNull(FWbemObject.Properties_.Item(WMIProperty).Value) then
        try
            Result:=FWbemObject.Properties_.Item(WMIProperty).Value;
        except
            Result := '';
        end;

    FWbemObject:=Unassigned;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TExComPort.IcdComCreate(const pIdeStr1, pIdeStr2: String): Boolean;
begin
    icdComPort := TExComPort.Create('', pIdeStr1, pIdeStr2);
    icdComPort.LoadSettings;
    if icdComPort.FPort <> '' then
    begin
        icdComPort.FActive := True;
        icdComPort.OpenCom;
    end;
    Result := icdComPort.FConnected;
    if Result then
    begin
        icdComPort.SetupComPort;
        icdComPort.IcdStar;
    end
    else
        icdComPort.PostMessageState;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class procedure TExComPort.IcdComFree;
begin
    if Assigned(icdComPort) then
    begin
        try
            icdComPort.StoreSettings;
            icdComPort.Terminate;
            if icdComPort.Suspended then
                icdComPort.Resume;
            {
            Sleep(5);
            if Assigned(icdComPort) then
                icdComPort.Free;
            }
        except
        end;
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.IcdProcess;
begin
    if not FActive then
        Exit;

    case FicdAction of
    ICD_NOTHING:
		begin
            FStrIdeTmp1 := FStrIde1;
            FStrIdeTmp2 := FStrIde2;
            WriteLine(FStrIdeTmp1, 1, Iif(FIsScroll1, taLeftJustify, taCenter));
            WriteLine(FStrIdeTmp2, 2, Iif(FIsScroll2, taLeftJustify, taCenter));
            FicdAction := ICD_IDLE;
            FicdTime := Now;
        end;
    ICD_IDLE:
		if FIsScroll1 or FIsScroll2 then	// Scrolling
            if FicdTime + C_SEC1 <= Now then
	        begin
                if FIsScroll1 then
                begin
                    FStrIdeTmp1 := isSubStr(FStrIdeTmp1, 2) + isLeft(FStrIdeTmp1, 1);
                    FicdAction := ICD_IDLE;
                    WriteLine(FStrIdeTmp1, 1);
                end;

                if FIsScroll2 then
                begin
                    FStrIdeTmp2 := isSubStr(FStrIdeTmp2, 2) + isLeft(FStrIdeTmp2, 1);
                    FicdAction := ICD_IDLE;
                    WriteLine(FStrIdeTmp2, 2);
                end;

                FicdTime := Now;
	        end;
    ICD_SCAN:
		begin
            if FicdTime + SecsDelay <= Now then
            begin
                WriteLine(FStr1, 1);
                WriteLine(FStr2, 2, taRightJustify);
                FicdAction := ICD_TOTAL;
                FicdTime := Now;
            end;
        end;
    ICD_THANKS:
		begin
            if FicdTime + C_SEC0*5 <= Now then
                FicdAction := ICD_NOTHING;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.IcdScan(pTenHH, pGia, pTong: String);
begin
    if not FActive then
        Exit;

    WriteLine(isStripToneMark(pTenHH), 1);
    WriteLine(pGia, 2, taRightJustify);
    FStr1 := FStrSum;
    FStr2 := pTong;
    FicdAction := ICD_SCAN;
    FicdTime := Now;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.IcdTest: Boolean;
var
    BytesTrans: DWORD;
begin
    WriteFile(FHandle, Chr($1F)+Chr($40), 2, BytesTrans, nil);
    Result := BytesTrans > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.IcdThanks;
begin
    if not FActive then
        Exit;

    WriteLine(FStrThanks1, 1, taCenter);
    WriteLine(FStrThanks2, 2, taCenter);
    FicdAction := ICD_THANKS;
    FicdTime := Now;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.InitDisplay;
begin
    WriteStr(Chr($1B)+Chr($40));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class function TExComPort.IsConnected: Boolean;
begin
    Result := Assigned(icdComPort);
    if Result then
        Result := icdComPort.FConnected;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.LoadIniFile(IniFile: TIniFile);
var
    s: String;
begin
    with IniFile do
    begin
        FAuto := ReadInteger(C_ICD_SECTION, 'Auto', 0);
        if FAuto = 1 then
        begin
            s := ProlificComDetect;
            if s <> '' then
                FPort := s
            else
            begin
                FPort := ReadString(C_ICD_SECTION, 'Port_' + isGetComputerName, '');
                if FPort = '' then
                    FPort := ReadString(C_ICD_SECTION, 'Port', '');
            end;

            FBaudRate := 9600;
        end else
        begin
            FPort := ReadString(C_ICD_SECTION, 'Port_' + isGetComputerName, '');
            if FPort = '' then
                FPort := ReadString(C_ICD_SECTION, 'Port', '');

            FBaudRate := ReadInteger(C_ICD_SECTION, 'BaudRate', 9600);
        end;
        SerialNo := ReadString(C_ICD_SECTION, 'Serial', '');
        SecsDelay := ReadInteger(C_ICD_SECTION, 'TimeChange', Round(FSecsDelay/C_SEC0));
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.LoadSettings;
begin
    LoadIniFile(TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveDown;
begin
    WriteStr(Chr($0A));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveEnd;
begin
    WriteStr(Chr($1F)+Chr(42));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveHome;
begin
    WriteStr(Chr($0B));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveLeft;
begin
    WriteStr(Chr($08));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveRight;
begin
    WriteStr(Chr($09));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveUp;
begin
    WriteStr(Chr($1F)+Chr($0A));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.MoveXY(pX, pY: Integer);
var
    s: String;
begin
    s := Chr($1F) + Chr($24) + Chr(pX) + Chr(pY);
    WriteStr(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.OpenCom;
begin
    FHandle := CreateFile(
        PChar('\\.\' + FPort),
        GENERIC_READ or GENERIC_WRITE,
        0,
        nil,
        OPEN_EXISTING,
        FILE_SHARE_READ,
        0);
    if FHandle = INVALID_HANDLE_VALUE then
        FConnected := False
//        RaiseLastOSError
    else
        FConnected := True;

    PostMessageState;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.ReadUnicodeString(var Str: UnicodeString;
  Count: Integer): Integer;
begin
    Result := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
class procedure TExComPort.ReconectCom;
var
    mPort, mIdeStr1, mIdeStr2: String;
    mLen, mBaudRate: Integer;
begin
    if Assigned(icdComPort) then
    begin
        with icdComPort do
        begin
            mPort := FPort;
            mIdeStr1 := FStrIde1;
            mIdeStr2 := FStrIde2;
            mLen := FMaxLen;
            mBaudRate := FBaudRate;
        end;

        icdComPort.CloseCom;
//        icdComPort.Terminate;
    end
    else
        icdComPort := TExComPort.Create;

    if mPort <> '' then
        with icdComPort do
        begin
            FPort := mPort;
            StrIde1 := mIdeStr1;
            StrIde2 := mIdeStr2;
            FMaxLen := mLen;
            FBaudRate := mBaudRate;
        end;
    icdComPort.OpenCom;
    if icdComPort.FConnected then
    begin
        icdComPort.SetupComPort;
        icdComPort.FicdAction := ICD_NOTHING;
        icdComPort.IcdStar;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.ScrollHor(pStr: String; const pLine2: Boolean);
begin
    pStr := Chr($1F) + Chr($03) + pStr;
    if pLine2 then
        MoveXY(1, 2)
    else
        MoveXY(1, 1);
    WriteStr(pStr);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.IcdStar;
//var
//    s: String;
begin
//    s := Chr($0C) + Chr($1F) + Chr($03) + Chr($1F) + Chr($24) + Chr($20) + Chr($1);
//    WriteStr(s);
    InitDisplay;
    Resume;
    SetEvent(FEvent);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.SetActiveThread(const Value: Boolean);
begin
    FActiveThread := Value;
    if FActiveThread then
        SetEvent(FEvent);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.SetSecsDelay(const Value: Double);
begin
    FSecsDelay := Value * C_SEC0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.SetSerial(const Value: String);
begin
    FSerial := Value;
    FSerialValid := 0;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.SetStrIde1(const Value: String);
begin
    FStrIde1 := Value;
    FIsScroll1 := Length(FStrIde1) > FMaxLen;
    if FIsScroll1 then
    begin
        if Length(Value) = Length(Trim(Value)) then
            FStrIde1 := Value + isPadRight('', 10, ' ');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.SetStrIde2(const Value: String);
begin
    FStrIde2 := Value;
    FIsScroll2 := Length(FStrIde2) > FMaxLen;
    if FIsScroll2 then
    begin
        if Length(Value) = Length(Trim(Value)) then
            FStrIde2 := Value + isPadRight('', 10, ' ');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.SetupComPort(): Boolean;
const
  RxBufferSize = 256;
  TxBufferSize = 256;
var
  DCB: TDCB;
  Config: string;
  CommTimeouts: TCommTimeouts;
begin
  Result := True;

  if not SetupComm(FHandle, RxBufferSize, TxBufferSize) then
    Result := False;

  if not GetCommState(FHandle, DCB) then
    Result := False;

  Config := Format('baud=%d parity=n data=8 stop=1', [FBaudRate]);

  if not BuildCommDCB(@Config[1], DCB) then
    Result := False;

  if not SetCommState(FHandle, DCB) then
    Result := False;

  with CommTimeouts do
  begin
    ReadIntervalTimeout         := 0;
    ReadTotalTimeoutMultiplier  := 0;
    ReadTotalTimeoutConstant    := 1000;
    WriteTotalTimeoutMultiplier := 0;
    WriteTotalTimeoutConstant   := 1000;
  end;

  if not SetCommTimeouts(FHandle, CommTimeouts) then
    Result := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.StoreIniFile(IniFile: TIniFile);
begin
    with IniFile do
    begin
        WriteString(C_ICD_SECTION, 'Port', FPort);
        WriteInteger(C_ICD_SECTION, 'BaudRate', FBaudRate);
        WriteString(C_ICD_SECTION, 'Serial', FSerial);
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.StoreSettings(pManual: Boolean);
begin
    if FConnected or pManual then
        StoreIniFile(TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.PostMessageState;
begin
    PostMessage(Application.MainFormHandle, MY_MESSAGE_COM_STATUS, 0, LParam(FConnected));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.ProlificComDetect: String;
const
    WbemUser            ='';
    WbemPassword        ='';
    WbemComputer        ='localhost';
    wbemFlagForwardOnly = $00000020;
var
    FSWbemLocator: OLEVariant;
    FWMIService: OLEVariant;
    FWbemObjectSet: OLEVariant;
    FWbemObject: OLEVariant;
    oEnum: IEnumvariant;
    iValue: LongWord;
    s: String;
begin
	s := '';
	try
    	CoInitialize(nil);
		try
            FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
            FWMIService := FSWbemLocator.ConnectServer(WbemComputer, 'root\CIMV2', WbemUser, WbemPassword);
            FWbemObjectSet := FWMIService.ExecQuery('SELECT * FROM Win32_SerialPort','WQL', wbemFlagForwardOnly);
//            FWbemObjectSet := FWMIService.ExecQuery('SELECT * FROM Win32_PnPEntity WHERE Name LIKE ''%COM%''','WQL', wbemFlagForwardOnly);
            oEnum := IUnknown(FWbemObjectSet._NewEnum) as IEnumVariant;
            while oEnum.Next(1, FWbemObject, iValue) = 0 do
            begin
                if Pos('Prolific', FWbemObject.Caption) <> 0 then
                    s := FWbemObject.DeviceID;
                FWbemObject:=Unassigned;
            end;
    	finally
      		CoUninitialize;
    	end;
 	except
    	on E:EOleException do
        	Writeln(Format('EOleException %s %x', [E.Message,E.ErrorCode]));
    	on E:Exception do
        	Writeln(E.Classname, ':', E.Message);
	end;
	Result := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.Write(const Buffer; Count: Integer): Integer;
var
    BytesTrans: DWORD;
begin
    WriteFile(FHandle, Buffer, Count, BytesTrans, nil);
    Result := BytesTrans;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExComPort.WriteLine(const pStr: String; const pLine: Integer;
    pAlign: TAlignment);
var
    s: String;

    function PadRight(Str: String; Num: Integer; C: Char = ' '): String;
    var
        mS : String;
        n : Integer;
    begin
        mS := Str;
        for n := Length(mS) + 1 to Num do
            mS := mS + C;
        Result := mS;
    end;
begin
    case pAlign of
        taLeftJustify : s := isLeft(PadRight(pStr, FMaxLen, ' '), FMaxLen);
        taRightJustify: s := isRight(isPadLeft(pStr, FMaxLen, ' '), FMaxLen);
        taCenter      : s := isLeft(isPadCenter(pStr, FMaxLen, ' '), FMaxLen);
    end;
    if pLine = 2 then
    begin
        MoveXY(1, 2);
        FStr2 := s;
    end
    else
    begin
        FStr1 := s;
        MoveXY(1, 1);
    end;
    if pAlign = taCenter then
        WriteStr(Chr($18));
    WriteStr(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.WriteStr(Str: String): Integer;
var
    s: Ansistring;
    i, n: integer;
    BytesTrans: DWORD;
    mStatus: TIcdAction;
begin
  if FSerialValid = 2 then
    Str := isPadLeft('', 2*FMaxLen, '$');

  n := Length(Str);
  if n > 0 then
  begin
    setlength(s, n);
    {$IFDEF Unicode}
    for i := 1 to length(str) do
        s[i] := ansichar(byte(str[i]));
    move(s[1],str[1],length(s));
    {$ENDIF}
    WriteFile(FHandle, PChar(str)^, n, BytesTrans, nil);
    Result := BytesTrans;
    //Try Reconnect
    if Result = 0 then
    begin
        mStatus := icdComPort.FicdAction;
        ReconectCom;
        if icdComPort.Connected then
        begin
            icdComPort.FicdAction := mStatus;
            icdComPort.FicdTime := Now;
            WriteFile(FHandle, PChar(str)^, n, BytesTrans, nil);
        end;
    end;
  end
  else
    Result := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExComPort.WriteUnicodeString(const Str: UnicodeString): Integer;
begin
    Result := 0;
end;

end.
