﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmquaytn;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls, Db,
  Wwdbgrid2, Wwdbcomb, ADODb, Menus, AdvMenus,
  AppEvnts, wwdblook,
  wwfltdlg, wwFltDlg2, wwDialog, Mask, wwdbedit, Wwdotdot, ToolWin, Grids,
  Wwdbigrd, Wwdbgrid;

type
  TFrmDmquaytn = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrDMQUAY: TADOQuery;
    DsDMQUAY: TDataSource;
    QrDMQUAYTENMAY: TWideStringField;
    QrDMQUAYGHICHU: TWideStringField;
    CbTenmay: TwwDBComboBox;
    QrDMQUAYQUAY: TWideStringField;
    CbPrinter: TwwDBLookupCombo;
    QrDMQUAYPRINTER: TWideStringField;
    QrPRINTER: TADOQuery;
    QrDMQUAYMAKHO: TWideStringField;
    QrDMQUAYTENKHO: TWideStringField;
    Filter: TwwFilterDialog2;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    Hinttc1: TMenuItem;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdAudit: TAction;
    QrDMQUAYCREATE_BY: TIntegerField;
    QrDMQUAYUPDATE_BY: TIntegerField;
    QrDMQUAYDELETE_BY: TIntegerField;
    QrDMQUAYCREATE_DATE: TDateTimeField;
    QrDMQUAYUPDATE_DATE: TDateTimeField;
    QrDMQUAYDELETE_DATE: TDateTimeField;
    Panel1: TPanel;
    LbKHO: TLabel;
    CbTenKho: TwwDBLookupCombo;
    CbMaKho: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    DsDMKHO: TDataSource;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMQUAYBeforePost(DataSet: TDataSet);
    procedure QrDMQUAYBeforeDelete(DataSet: TDataSet);
    procedure QrDMQUAYPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMQUAYBeforeInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CbPrinterNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbTenmayDropDown(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDMQUAYAfterInsert(DataSet: TDataSet);
    procedure QrDMQUAYBeforeEdit(DataSet: TDataSet);
    procedure QrDMQUAYQUAYValidate(Sender: TField);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
    procedure CbMaKhoCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
  private
  	mCanEdit: Boolean;
    procedure QueryNetworkNeighborhood;
    function  IsInused(pQuay: String = ''): Boolean;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmquaytn: TFrmDmquaytn;

implementation

uses
	MainData, isDb, isMsg, ExCommon, Rights,
    isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1('_IDI_CASHIER');
    SetCustomGrid('DM_QUAYTN', GrList);
    SetDictionary(QrDMQUAY, 'DM_QUAYTN');
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdNewExecute(Sender: TObject);
begin
	QrDMQUAY.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdSaveExecute(Sender: TObject);
begin
	QrDMQUAY.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdCancelExecute(Sender: TObject);
begin
	QrDMQUAY.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdDelExecute(Sender: TObject);
begin
	QrDMQUAY.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdPrintExecute(Sender: TObject);
begin
	//
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    OpenDataSets([QrPRINTER, QrDMKHO]);
    CbMaKho.LookupValue := sysDefKho;
    CbTenKho.LookupValue := sysDefKho;
    with QrDMQUAY do
    begin
    	Close;
        Open;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDMQUAY);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDMQUAY, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDMQUAY, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYBeforePost(DataSet: TDataSet);
begin
	with QrDMQUAY do
    begin
		if BlankConfirm(QrDMQUAY, ['MAKHO', 'TENMAY', 'QUAY']) then
    		Abort;
	    SetNull(QrDMQUAY, ['PRINTER']);
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if IsInused then
		Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
    if QrDMKHO.IsEmpty then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INUSED = 'Đã có phát sinh dữ liệu, không thể sửa/xóa.';

function TFrmDmquaytn.IsInused;
var
	kho, quay: String;
begin
    with QrDMQUAY do
    begin
	    kho := FieldByName('MAKHO').AsString;
        if pQuay = '' then
        	quay := FieldByName('QUAY').AsString
        else
        	quay := pQuay;
	end;

    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
		SQL.Text := Format('select top 1 QUAY from BANLE where LCT=''BLE'' and MAKHO=%s and QUAY=%s',
        	[kho, quay]);
        Open;
		Result := not IsEmpty;
        Close;
        Free;
    end;
    
    if Result then
        ErrMsg(RS_INUSED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDmquaytn.QrDMQUAYQUAYValidate(Sender: TField);
begin
	if QrDMQUAY.State in [dsEdit] then
		if IsInused(Sender.OldValue) then
	    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QueryNetworkNeighborhood;
var
	s: String;
	hEnum: THandle;
	i, dwResult, cbBuffer, cEntries: DWORD;
	pMem, pNet: PNETRESOURCE;
begin
	Screen.Cursor := crSqlWait;
    CbTenmay.Items.Clear;
	cbBuffer := 16384;		// 16K is a good size
	cEntries := $FFFFFFFF;	// enumerate all possible entries

	dwResult := WNetOpenEnum(
		RESOURCE_CONTEXT,
        RESOURCETYPE_ANY,   // all resources
		0,					// enumerate all resources
		Nil,				// NULL first time the function is called
		hEnum);				// handle to the resource

	if dwResult <> NO_ERROR then
        Exit;

	// Call the GlobalAlloc function to allocate resources.
	pMem := PNETRESOURCE(GlobalAlloc(GPTR, cbBuffer));

	repeat
        // Initialize the buffer.
        ZeroMemory(pMem, cbBuffer);

        // Call the WNetEnumResource function to continue the enumeration.
        dwResult := WNetEnumResource(
        	hEnum,		// resource handle
            cEntries,	// defined locally as -1
            pMem,		// LPNETRESOURCE
            cbBuffer);	// buffer size

        // If the call succeeds, loop through the structures.
        if dwResult = NO_ERROR then
        begin
        	pNet := pMem;
			for i := 0 to cEntries - 1 do
            begin
            	s := pNet^.lpRemoteName;
                if s <> '' then
                begin
                	s := Copy(s, 3, 1000);
                    CbTenmay.Items.Add(s);
				end;
				Inc(pNet);
            end;
		end;
	until dwResult = ERROR_NO_MORE_ITEMS;

    // Call the GlobalFree function to free the memory.
    GlobalFree(HGLOBAL(pMem));

	// Call WNetCloseEnum to end the enumeration.
	WNetCloseEnum(hEnum);
	Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CbMaKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CbPrinterNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CbTenmayDropDown(Sender: TObject);
begin
	QueryNetworkNeighborhood;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := RecordCount(QrDMQUAY);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMQUAY);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmquaytn.QrDMQUAYAfterInsert(DataSet: TDataSet);
begin
	QrDMQUAY.FieldByName('MAKHO').AsString := QrDMKHO.FieldByName('MAKHO').AsString;
end;

end.
