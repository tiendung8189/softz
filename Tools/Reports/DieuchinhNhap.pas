﻿(*==============================================================================
** Ngan Long Jsc
**------------------------------------------------------------------------------
*)
unit DieuchinhNhap;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi,
  isDb, isPanel, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, Wwdotdot, Wwdbcomb;

type
  TFrmDieuchinhNhap = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    PaTotal: TPanel;
    Label24: TLabel;
    Label32: TLabel;
    Label18: TLabel;
    Label12: TLabel;
    EdTriGiaTT: TwwDBEdit;
    EdTriGiaHH: TwwDBEdit;
    EdCK: TwwDBEdit;
    EdTLCK: TwwDBEdit;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTGIANHAP: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_NGAY: TDateTimeField;
    QrNXNG_GIAO: TWideStringField;
    QrNXTL_CK: TFloatField;
    QrNXCHIETKHAU: TFloatField;
    QrNXTHUE_SUAT: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    ImgTotal: TImage;
    EdCL: TwwDBEdit;
    Label3: TLabel;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    Label6: TLabel;
    DBEdit1: TwwDBEdit;
    QrNXNG_NHAN: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaMaster: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXTENKHO: TWideStringField;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    QrNXPTTT: TWideStringField;
    QrNXCO_HD: TBooleanField;
    QrNXCK_BY: TIntegerField;
    QrNXPRINTED: TBooleanField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    CmdExBarcode: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    QrCTTENTAT: TWideStringField;
    PopDetail: TAdvPopupMenu;
    CmdEmptyDetail: TAction;
    Xachitit1: TMenuItem;
    QrCTEX_DATE: TDateTimeField;
    QrNXHAN_TTOAN: TIntegerField;
    QrNXDA_TTOAN: TBooleanField;
    QrNXLK_HAN_TTOAN: TIntegerField;
    Bevel1: TBevel;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    N4: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrNXTC_SOTIEN: TFloatField;
    QrNXSCT2: TWideStringField;
    QrCTSOLUONG_DC: TFloatField;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    TntLabel1: TLabel;
    EdSCT2: TwwDBEdit;
    Label11: TLabel;
    EdSOHDON: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    LbNHAPCUA: TLabel;
    CbMADT: TwwDBEdit;
    CbNCC: TwwDBEdit;
    Label5: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    Label31: TLabel;
    CbMAKHO: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    Label8: TLabel;
    DBEdit2: TwwDBEdit;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    Label7: TLabel;
    DBEdit6: TwwDBEdit;
    QrCTDONGIA_DC: TFloatField;
    QrCTSOTIEN_DC: TFloatField;
    QrPHIEUNHAP: TADOQuery;
    CmdCapnhatct: TAction;
    Lydanhschchitittphieunhp1: TMenuItem;
    QrCTLOAITHUE: TWideStringField;
    QrNXLOAITHUE: TWideStringField;
    QrNXLCT: TWideStringField;
    PaHoadon: TisPanel;
    PaHoadon2: TPanel;
    TntLabel2: TLabel;
    TntLabel3: TLabel;
    TntLabel4: TLabel;
    TntLabel7: TLabel;
    TntLabel8: TLabel;
    EdThueVAT: TwwDBEdit;
    CbLoaithue: TwwDBLookupCombo;
    EdSOHD: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    ckDatt: TDBCheckBox;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLK_HOADON_SO: TWideStringField;
    QrNXLK_HOADON_SERI: TWideStringField;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    DBText2: TDBText;
    QrNXCALC_NGAY_TTOAN: TDateTimeField;
    TntLabel9: TLabel;
    QrNXHOADON_SO: TWideStringField;
    QrPHIEUNHAPHOADON_SO: TWideStringField;
    QrPHIEUNHAPHOADON_SERI: TWideStringField;
    QrPHIEUNHAPMADT: TWideStringField;
    QrPHIEUNHAPMAKHO: TWideStringField;
    QrPHIEUNHAPLOAITHUE: TWideStringField;
    QrPHIEUNHAPTHUE_SUAT: TFloatField;
    QrPHIEUNHAPTL_CK_NX: TFloatField;
    QrPHIEUNHAPNG_GIAO: TWideStringField;
    QrPHIEUNHAPNG_NHAN: TWideStringField;
    QrPHIEUNHAPHAN_TTOAN: TIntegerField;
    QrPHIEUNHAPDA_TTOAN: TBooleanField;
    QrPHIEUNHAPDGIAI: TWideMemoField;
    QrPHIEUNHAPMAVT: TWideStringField;
    QrPHIEUNHAPTENVT: TWideStringField;
    QrPHIEUNHAPDVT: TWideStringField;
    QrPHIEUNHAPSOLUONG: TFloatField;
    QrPHIEUNHAPDONGIA: TFloatField;
    QrPHIEUNHAPSOTIEN: TFloatField;
    QrPHIEUNHAPGIANHAP: TFloatField;
    QrPHIEUNHAPTL_CK: TFloatField;
    QrPHIEUNHAPEX_DATE: TDateTimeField;
    QrPHIEUNHAPGHICHU: TWideStringField;
    QrPHIEUNHAPB1: TBooleanField;
    CmdListRefesh: TAction;
    QrPHIEUNHAPGIABAN: TFloatField;
    QrPHIEUNHAPGIASI: TFloatField;
    QrPHIEUNHAPTL_LAI: TFloatField;
    QrPHIEUNHAPTENDT: TWideStringField;
    CmdSapthutu: TAction;
    N2: TMenuItem;
    Splithtmthng1: TMenuItem;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrNXDRC_STATUS: TWideStringField;
    LbDrcStatus: TLabel;
    CbDrcStatus: TwwDBComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrNXTL_CKChange(Sender: TField);
    procedure QrCTTL_CKChange(Sender: TField);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTSOLUONG_DCChange(Sender: TField);
    procedure QrCTSOTIEN_DCChange(Sender: TField);
    procedure QrNXSCT2Validate(Sender: TField);
    procedure QrNXSCT2Change(Sender: TField);
    procedure QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
    procedure QrPHIEUNHAPAfterOpen(DataSet: TDataSet);
    procedure CmdCapnhatctExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);

  private
    mLCT, mPrefix: String;
	mCanEdit: Boolean;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

    function OpenQueryRef: Boolean;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmDieuchinhNhap: TFrmDieuchinhNhap;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, Sapthutu,
    isCommon, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAP_DC';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.Execute;
begin
    mLCT := 'NDC';

    // Audit setting
	mCanEdit := rCanEdit(r);
    PaMaster.Enabled := mCanEdit;
    PaTotal.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrNX;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');
    ckDatt.Visible := FlexConfigBool('DM_HH',  'Checkbox "Da thanh toan"');

    //Drc status
    LbDrcStatus.Visible := False;
    CbDrcStatus.Visible := False;
    // Initial
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;
    TMyForm(Self).LoadIcon('_IDI_RECEIPT');

    // Open database
	Wait(PREPARING);
    DataMain.QrDMLOAITHUE.Open;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetDisplayFormat(QrNX, ['THUE_SUAT', 'TL_CK'], sysPerFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG', 'SOLUONG_DC'], sysQtyFmt);
	    SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);
    end;

    with QrPHIEUNHAP do
    begin
	    SetDisplayFormat(QrPHIEUNHAP, sysCurFmt);
    	SetDisplayFormat(QrPHIEUNHAP, ['SOLUONG'], sysCurFmt);
	    SetDisplayFormat(QrPHIEUNHAP, ['TL_CK', 'TL_LAI'], sysPerFmt);
    end;

    // DicMap + Cutomize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
        OpenQueryRef;

        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

	(*
	** Commands
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (frDate.CbLoc.LookupValue   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := frDate.CbLoc.LookupValue;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY, SCT');
    	    Open;
            if s <> ''	then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
        	EdCL.SetFocus;
        except
        end
    else if ActiveControl.Parent = PaTotal then
    	try
    		EdSCT2.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdTotalExecute(Sender: TObject);
var
    bm: TBytes;
	mTGHH, mCkmh: Double;
    mSt, mTl, mCk: Double;
begin
    with QrCT do
    begin
        mTGHH := 0;
        mCkmh := 0;

    	bm := BookMark;
        DisableControls;
        First;

        while not Eof do
        begin
	    	mSt := FieldByName('SOTIEN_DC').AsFloat;
    		mTl := FieldByName('TL_CK').AsFloat;
       		mCk := Round (mSt * mTl / 100);

            if FieldByName('CHIETKHAU').AsFloat <> mCk then
            begin
            	Edit;
		    	FieldByName('CHIETKHAU').AsFloat := mCk;
            end;

        	mTGHH := mTGHH + mSt;
        	mCkmh := mCkmh + mCk;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;
    end;

    mTrigger := True;
	with QrNX do
    begin
	    if State in [dsBrowse] then
            Edit;;

        // Chiet khau mat hang
        FieldByName('CHIETKHAU_MH').AsFloat := mCkmh;

        // Tri gia hang hoa
        FieldByName('SOTIEN').AsFloat := RoundUp(mTGHH);
	end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdCapnhatctExecute(Sender: TObject);
begin
    if not YesNo(Format('Cập nhật lại danh sách mặt hàng từ phiếu "%s". Tiếp tục?',
        [QrNX.FieldByName('SCT2').AsString]), 1) then
        Exit;

    Wait(PROCESSING);        
    // Clean up
    with QrCT do
    begin
        DisableControls;
        First;

        EmptyDataset(QrCT);
    end;

    with TADOQuery.Create(Nil) do
    begin
        Connection  := DataMain.Conn;
        LockType    := ltReadOnly;
        SQL.Text    :=  'select b.B1, b.MAVT, b.SOLUONG, b.DONGIA, b.SOTIEN, b.GIANHAP, b.TL_CK, b.LOAITHUE, b.THUE_SUAT, b.TIEN_THUE, b.EX_DATE, b.GHICHU ' +
                        ' from T_CHUNGTU a, CHUNGTU_CT b ' +
                        ' where a.KHOA=b.KHOA and SCT=''' + QrNX.FieldByName('SCT2').AsString + '''' +
                        ' order by b.STT';
        Open;
        First;
        while not Eof do
        begin
            QrCT.Append;

    		QrCT.FieldByName('KHOA').Value          := QrNX.FieldByName('KHOA').Value;
            mTrigger := True;
            QrCT.FieldByName('MAVT').AsString       := FieldByName('MAVT').AsString;
            mTrigger := False;

			QrCT.FieldByName('B1').AsString       	:= FieldByName('B1').AsString;
            QrCT.FieldByName('SOLUONG').AsFloat     := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('SOLUONG_DC').AsFloat  := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('DONGIA').AsFloat      := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('DONGIA_DC').AsFloat   := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('SOTIEN').AsFloat      := FieldByName('SOTIEN').AsFloat;
            QrCT.FieldByName('GIANHAP').AsFloat      := FieldByName('GIANHAP').AsFloat;
            QrCT.FieldByName('TL_CK').AsFloat       := FieldByName('TL_CK').AsFloat;
            QrCT.FieldByName('LOAITHUE').AsString   := FieldByName('LOAITHUE').AsString;
            QrCT.FieldByName('THUE_SUAT').AsFloat   := FieldByName('THUE_SUAT').AsFloat;
            QrCT.FieldByName('TIEN_THUE').AsFloat   := FieldByName('TIEN_THUE').AsFloat;
            QrCT.FieldByName('EX_DATE').AsDateTime  := FieldByName('EX_DATE').AsDateTime;
            QrCT.FieldByName('GHICHU').AsString     := FieldByName('GHICHU').AsString;

            QrCT.Post;

            Next;
        end;

        Close;
        Free;
    end;

    QrCT.EnableControls;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
	DataMain.LabelExport(TGuidEx.ToStringEx(QrNX.FieldByName('KHOA')),'SOLUONG_DC');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and exCheckLoc(QrNX, False);
    CmdDel.Caption := GetMarkCaption(QrNX);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdTotal.Enabled  := n = 1;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    CmdEmptyDetail.Enabled := not bEmptyCT;
    CmdEmptyDetail.Enabled := mCanEdit and (QrNX.FieldByName('SCT2').AsString <> '');
    CmdExBarcode.Enabled := n = 1;

    EdSCT2.Enabled := bEmptyCT;
end;

	(*
	** Db events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      := d;
		FieldByName('LCT').AsString         := mLCT;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('DA_TTOAN').AsBoolean   := False;
        FieldByName('DRC_STATUS').AsString    := '1';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['NGAY', 'SCT2']) then
    	    Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
	with DataSet do
    begin
		// Validate: da co phieu thu / chi
    	if FieldByName('TC_SOTIEN').AsFloat <> 0 then
	    begin
    	    ErrMsg(RS_DA_THUCHI);
        	Abort;
	    end;

    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXNGAYValidate(Sender: TField);
begin
    exValidRecordDate(QrNX.FieldByName('NGAY').AsDateTime)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);

    if PgMain.ActivePageIndex = 1 then
        OpenQueryRef;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXTL_CKChange(Sender: TField);
begin
	with QrNX do
	    FieldByName('CHIETKHAU').AsFloat := RoundUp(
        	(FieldByName('SOTIEN').AsFloat -
            FieldByName('CHIETKHAU_MH').AsFloat) *
            FieldByName('TL_CK').AsFloat / 100);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXSOTIENChange(Sender: TField);
var
	mSt, mCk, mCkmh, mThue, mTt, mTs : Double;
begin
	if mTrigger then
    	Exit;
    mTrigger := True;

	with QrNX do
    begin
    	mTs   := FieldByName('THUE_SUAT').AsFloat;
    	mSt   := FieldByName('SOTIEN').AsFloat;
    	mCkmh := FieldByName('CHIETKHAU_MH').AsFloat;
    	mCk   := RoundUp((mSt - mCkmh) * FieldByName('TL_CK').AsFloat / 100);
		mTt   := mSt - mCk - mCkmh;

        mThue := RoundUp(mTt * mTs / 100);
        mTt := mTt + mThue + FieldByName('CL_THUE').AsFloat;

    	FieldByName('CHIETKHAU').AsFloat := mCk;
    	FieldByName('THUE').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mTt;
	end;

    mTrigger := False;
end;

(*==============================================================================
**  Detail DB
**------------------------------------------------------------------------------
*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
begin
    QrPHIEUNHAP.Parameters[0].Value := QrNX.FieldByName('SCT2').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrPHIEUNHAPAfterOpen(DataSet: TDataSet);
begin
	with QrPHIEUNHAP do
    begin
	    SetDisplayFormat(QrPHIEUNHAP, sysCurFmt);
    	SetDisplayFormat(QrPHIEUNHAP, ['SOLUONG'], sysCurFmt);
	    SetDisplayFormat(QrPHIEUNHAP, ['TL_CK'], sysPerFmt);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
   		Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTSOLUONG_DCChange(Sender: TField);
begin
	with QrCT do
    	FieldByName('SOTIEN_DC').AsFloat :=
	    	FieldByName('SOLUONG_DC').AsFloat *
    		FieldByName('DONGIA_DC').AsFloat;

    vlTotal1.Update;
	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTSOTIEN_DCChange(Sender: TField);
begin
	QrCTTL_CKChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTTL_CKChange(Sender: TField);
begin
	with QrCT do
    	FieldByName('CHIETKHAU').AsFloat := Round (
        	FieldByName('SOTIEN_DC').AsFloat *
            FieldByName('TL_CK').AsFloat / 100.0);

    vlTotal1.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTMAVTValidate(Sender: TField);
var
    s: String;
begin
    if mTrigger then
        Exit;

    s := Sender.AsString;
    if s = '' then
        Exit;

    if IsDotSelect(s) > 0 then
        Exit;

    with QrPHIEUNHAP do
    begin
        Filter := '';
        if not Locate('MAVT', s, []) then
        begin
            ErrMsg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrCTMAVTChange(Sender: TField);
begin
    if mTrigger then
        Exit;

    exDotMavt(1, QrPHIEUNHAP, Sender);

    // Update referenced fields
    with QrCT do
    begin
        if QrPHIEUNHAP.Locate('MAVT', Sender.AsString, []) then
        begin
            FieldByName('SOLUONG').AsFloat  := QrPHIEUNHAP.FieldByName('SOLUONG').AsFloat;
            FieldByName('DONGIA').AsFloat   := QrPHIEUNHAP.FieldByName('DONGIA').AsFloat;
            FieldByName('SOLUONG_DC').AsFloat := QrPHIEUNHAP.FieldByName('SOLUONG').AsFloat;
            FieldByName('DONGIA_DC').AsFloat  := QrPHIEUNHAP.FieldByName('DONGIA').AsFloat;
            FieldByName('SOTIEN').AsFloat   := QrPHIEUNHAP.FieldByName('SOTIEN').AsFloat;
            FieldByName('GIANHAP').AsFloat   := QrPHIEUNHAP.FieldByName('GIANHAP').AsFloat;
            FieldByName('TL_CK').AsFloat    := QrPHIEUNHAP.FieldByName('TL_CK').AsFloat;
            FieldByName('EX_DATE').AsDateTime := QrPHIEUNHAP.FieldByName('EX_DATE').AsDateTime;
            FieldByName('GHICHU').AsString  := QrPHIEUNHAP.FieldByName('GHICHU').AsString
        end
        else
        begin
            FieldByName('SOLUONG').Clear;
            FieldByName('DONGIA').Clear;
            FieldByName('SOTIEN').Clear;
            FieldByName('GIANHAP').Clear;
            FieldByName('TL_CK').Clear;
            FieldByName('EX_DATE').Clear;
            FieldByName('GHICHU').Clear;
        end;

		if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;
	end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXSCT2Validate(Sender: TField);
var
    s: String;
begin
    s := Sender.AsString;

    if s = '' then
        Exit;

    if not OpenQueryRef then
    begin
        ErrMsg('Lỗi nhập liệu. Số phiếu không hợp lệ.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.QrNXSCT2Change(Sender: TField);
begin
    with QrNX do
        if QrNX.FieldByName('SCT2').AsString <> '' then
        begin
            FieldByName('LK_HOADON_SO').AsString     := QrPHIEUNHAP.FieldByName('HOADON_SO').AsString;
            FieldByName('LK_HOADON_SERI').AsString        := QrPHIEUNHAP.FieldByName('HOADON_SERI').AsString;
            FieldByName('MADT').AsString       := QrPHIEUNHAP.FieldByName('MADT').AsString;
            FieldByName('MAKHO').AsString       := QrPHIEUNHAP.FieldByName('MAKHO').AsString;
            FieldByName('LOAITHUE').AsString    := QrPHIEUNHAP.FieldByName('LOAITHUE').AsString;
            FieldByName('THUE_SUAT').AsFloat    := QrPHIEUNHAP.FieldByName('THUE_SUAT').AsFloat;
            FieldByName('TL_CK').AsFloat        := QrPHIEUNHAP.FieldByName('TL_CK_NX').AsFloat;
            FieldByName('NG_GIAO').AsString     := QrPHIEUNHAP.FieldByName('NG_GIAO').AsString;
            FieldByName('NG_NHAN').AsString     := QrPHIEUNHAP.FieldByName('NG_NHAN').AsString;
            FieldByName('HAN_TTOAN').AsInteger  := QrPHIEUNHAP.FieldByName('HAN_TTOAN').AsInteger;
            FieldByName('DA_TTOAN').AsBoolean   := QrPHIEUNHAP.FieldByName('DA_TTOAN').AsBoolean;
            FieldByName('DGIAI').AsString       := QrPHIEUNHAP.FieldByName('DGIAI').AsString;
        end
        else
        begin
            FieldByName('LK_HOADON_SO').Clear;
            FieldByName('LK_HOADON_SERI').Clear;
            FieldByName('MADT').Clear;
            FieldByName('MAKHO').Clear;
            FieldByName('LOAITHUE').Clear;
            FieldByName('THUE_SUAT').Clear;
            FieldByName('TL_CK').Clear;
            FieldByName('NG_GIAO').Clear;
            FieldByName('NG_NHAN').Clear;
            FieldByName('HAN_TTOAN').Clear;
            FieldByName('DA_TTOAN').Clear;
            FieldByName('DGIAI').Clear;
        end;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDieuchinhNhap.OpenQueryRef: Boolean;
begin
    with QrPHIEUNHAP do
    begin
        Close;
        Open;                       
        Result := not IsEmpty;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhNhap.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX);
end;
end.
