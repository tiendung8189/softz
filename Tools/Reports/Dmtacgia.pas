(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmTacGia;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus, AdvMenus,
  AppEvnts, Wwdbgrid, ToolWin, Wwdbigrd;

type
  TFrmDmTacGia = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdSearch: TAction;
    ApplicationEvents1: TApplicationEvents;
    ToolButton2: TToolButton;
    CmdPrint: TAction;
    ToolButton9: TToolButton;
    CmdAudit: TAction;
    QrDanhmucRSTT: TIntegerField;
    QrDanhmucMATG: TWideStringField;
    QrDanhmucTENTG: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucCalcFields(DataSet: TDataSet);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);

  end;

var
  FrmDmTacGia: TFrmDmTacGia;

implementation

uses
	MainData, isDb, isMsg, RepEngine, ExCommon, Rights, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_TACGIA';
    KEYFIELD = 'MATG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE);

    fixCode := SetCodeLength(FORM_CODE, QrDanhmuc.FieldByName(KEYFIELD));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
    	QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
	QrDanhmuc.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, mCanEdit);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
		if BlankConfirm(QrDanhmuc, [KEYFIELD, 'TENTG']) then
    		Abort;

        if fixCode then
			if LengthConfirm(QrDanhmuc, [KEYFIELD]) then
            	Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.QrDanhmucCalcFields(DataSet: TDataSet);
begin
    with DataSet do
        if State in [dsInsert] then
	    else
    	    FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if SameText(Field.FullName, KEYFIELD) then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTacGia.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

end.
