﻿(*==============================================================================
** Scan mabh
**------------------------------------------------------------------------------
*)
unit frameScanCode;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms, ExtCtrls, StdCtrls, Mask, wwdbedit,
  DB, HTMLabel, ActnList, pngimage, dxGDIPlusClasses;

type
  TfrScanCode = class(TFrame)
    PaBarcode: TPanel;
    EdCode: TwwDBEdit;
    LbCode: TLabel;
    Image4: TImage;
    HTMLabel4: THTMLabel;
    MyActionList: TActionList;
    CmdScanQty: TAction;
    CmdScanCode: TAction;
    chkCheckExists: TCheckBox;
    procedure EdCodeEnter(Sender: TObject);
    procedure EdCodeKeyPress(Sender: TObject; var Key: Char);
    procedure EdCodeExit(Sender: TObject);
    procedure CmdScanQtyExecute(Sender: TObject);
    procedure CmdScanCodeExecute(Sender: TObject);
  private
    FDataset: TDataSet;
    FQtyName, FBarCodeName: String;
    FAddNew: Boolean;

    procedure InvalidAction(msg: String = '');
    function  ValidBarcode(var pMa:String): Boolean;

    procedure ItemAddNew(ma: String);
    procedure ItemAddQty(pSoluong: Double);

  public
    procedure Init(pDataset: TDataSet; pSOLUONG: String = 'SOLUONG'; pCODE: String = 'MAVT'; pCanAdd: Boolean = False);
  end;

implementation

{$R *.dfm}

uses
    isMsg, isLib, ExCommon, Maindata;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.CmdScanCodeExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 0;
        Text := '';
		SetFocus;
    end;
	LbCode.Caption := '&BARCODE';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.CmdScanQtyExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 1;
        Text := '';
		SetFocus;
    end;
	LbCode.Caption := 'NHẬP &SỐ LƯỢNG';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.EdCodeEnter(Sender: TObject);
begin
    EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_ACTION = 'Thao tác không hợp lệ.';
	RS_CODE_FAIL  = 'Nhập sai mã hàng hóa.';

procedure TfrScanCode.EdCodeExit(Sender: TObject);
begin
    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.EdCodeKeyPress(Sender: TObject; var Key: Char);
var
    s: String;
    b: Boolean;
    x: Double;
begin
    if Key <> #13 then
    	Exit;

        // Is empty
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Commodity mode
    Key := #0;
    x := 0;

    case EdCode.Tag  of
    	// So luong
    	1:
    	begin
            b := False;

            if FDataset.IsEmpty then
                InvalidAction
            else
            begin
                try
                    x := StrToFloat(s);
                    b := True;
                except
                end;

                if not b then
                    InvalidAction
                else if Length(s) > 7 then
                    InvalidAction
                else
                    ItemAddQty(x);
            end;
        end;
        // Ma hang
        0:
        begin
        	// Quick select
            if IsDotSelect(s) <> 0 then
            begin
                if not exDotMavt(3, DataMain.QrDmvt, s) then
                    Exit;
                EdCode.Text := s;
            end;

            if not ValidBarcode(s) then
            // Sai ma hang
                InvalidAction(RS_CODE_FAIL)
            //Da co mat hang
            else if FDataset.Locate(FBarCodeName, s, []) then
                //Tang so luong 1
                ItemAddQty(1)
            else
            begin
                if FAddNew or not chkCheckExists.Checked then
                    ItemAddNew(s)
                else
				    InvalidAction('Mặt hàng không hợp lệ.');
            end;
        end;
    end;

    EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.Init;
begin
    FDataset := pDataset;
    FQtyName := pSOLUONG;
    FBarCodeName := pCODE;
    FAddNew := pCanAdd;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.InvalidAction(msg: String);
begin
    if msg = '' then
		ErrMsg(RS_INVALID_ACTION)
    else
		ErrMsg(msg);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.ItemAddNew(ma: String);
begin
    with FDataset do
    begin
        Append;
        FieldByName(FBarCodeName).AsString := ma;
        FieldByName(FQtyName).AsFloat := 1;
        Post;
    end;

    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrScanCode.ItemAddQty(pSoluong: Double);
var
    x: Double;
begin
    with FDataset do
    begin
        if IsEmpty then
        begin
            InvalidAction;
            Exit;
        end;

        x := FieldByName(FQtyName).AsFloat + pSoluong;
        //So luong moi < 0
        if x < 0  then
        begin
            InvalidAction;
            Exit;
        end
        //So luong moi = 0, xoa
        else if x = 0 then
        begin
            if not YesNo(Format('Xóa mặt hàng "%s"', [FieldByName(FBarCodeName).AsString])) then
                Exit;

            DeleteConfirm(False);
            Delete;
            DeleteConfirm(True);
        end
        else
        // Gan lai so luong
        begin
            if State in [dsBrowse] then
                Edit;

            FieldByName(FQtyName).AsFloat := x;
            Post;
        end;
    end;

    EdCode.Text := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TfrScanCode.ValidBarcode(var pMa: String): Boolean;
begin
    with DataMain.QrDMVT do
        if Locate(FBarCodeName, pMa, []) then
           Result := True
        else
            Result := False;
end;

end.
