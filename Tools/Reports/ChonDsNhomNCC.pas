(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsNhomNCC;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg, Wwkeycb, Wwdbigrd, Wwdbgrid, wwDBGrid2;

type
  TFrmChonDsNhomNCC = class(TForm)
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    RgLoai: TRadioGroup;
    paNhom: TPanel;
    CbMANGANH: TwwDBLookupCombo;
    CbMANHOM: TwwDBLookupCombo;
    CbNHOM: TwwDBLookupCombo;
    CbNGANH: TwwDBLookupCombo;
    QrNHOM2: TADOQuery;
    Label2: TLabel;
    Label3: TLabel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    btnAdd: TBitBtn;
    btnRemove: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    DsNHOM: TDataSource;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GrList2: TStringGrid;
    GrList1: TwwDBGrid2;
    EdMaSearch: TwwIncrementalSearch;
    QrDMNCC: TADOQuery;
    DsDMNCC: TDataSource;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CmdIns2: TAction;
    CmdDel2: TAction;
    CmdClear2: TAction;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbMANGANHExit(Sender: TObject);
    procedure EdMAVTInitDialog(Dialog: TwwLookupDlg);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CmdIns2Execute(Sender: TObject);
    procedure CmdDel2Execute(Sender: TObject);
    procedure CmdClear2Execute(Sender: TObject);
    procedure GrList1ColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure EdMaSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String; var pLstNcc: String): Boolean;
  end;

var
  FrmChonDsNhomNCC: TFrmChonDsNhomNCC;

implementation

uses
	isDb, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.FormShow(Sender: TObject);
var
    n, i: Integer;
begin
    TMyForm(Self).Init;
	mTrigger := True;
    SetCustomGrid('CHON_DSNCC_NHOM', GrList1);
	OpenDataSets([QrNGANH, QrNHOM, QrNHOM2, QrDMNCC]);

	mTrigger := False;
	CbMANGANH.LookupValue := QrNGANH.FieldByName('MANGANH').AsString;
	CbMANHOM.LookupValue := QrNHOM.FieldByName('MANHOM').AsString;

//    n := FlexConfigInteger('DM_HH_NGANH', 'Depth') + 1;
//    with RgLoai do
//    begin
//        for i := Items.Count downto n do
//            Items.Delete(i-1);
//
//        Columns := Items.Count;
//    end;
//
//    i := Iif(n > 3, 0, 28);
//
//    paNhom.Height := 105 - i;
//    btnAdd.Top := 168 - i;
//    btnRemove.Top := 168 - i;
//
//    GrList.Top := 200 - i;
//    GrList.Height := 103 + i;

    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsNhomNCC.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;

        pLstNcc := '';
        with GrList2 do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLstNcc <> '' then
                    pLstNcc := pLstNcc + ',';
                pLstNcc := pLstNcc + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.GrList1ColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
    s: String;
begin
    s := GrList1.Columns[0].FieldName;
    EdMaSearch.SearchField := s;
    QrDMNCC.Sort := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    CbMANHOM.Enabled := n > 0;
    CbNHOM.Enabled := n > 0;

    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CbMANGANHExit(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).Text = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    0:		// Ma nganh
    	begin
		    CbNGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;

        end;
    1:		// Ten nganh
    	begin
			CbMANGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;
        end;
    2:		// Ma nhom
    	begin
			CbNHOM.LookupValue := s;
            QrNHOM.Locate('MANHOM', s, []);
    	end;
    3:		// Ten nhom
    	begin
			CbMANHOM.LookupValue := s;
            QrNHOM.Locate('MANHOM', s, []);
        end;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.EdMaSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.EdMAVTInitDialog(Dialog: TwwLookupDlg);
begin
	InitSearchDialog(Dialog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbMANGANH.SetFocus;
        1:
            CbMANHOM.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdIns2Execute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
    with QrDMNCC do
    begin
        s1 := FieldByName('MADT').AsString;
        s2 := FieldByName('TENDT').AsString
    end;

    with GrList2 do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
        	s1 := CbMANGANH.LookupValue;
        	s2 := CbNGANH.DisplayValue;
        end;
    1:
    	begin
        	s1 := CbMANHOM.LookupValue;
        	s2 := CbNHOM.DisplayValue;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;

//        EdMAVT.Text := '';
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdDel2Execute(Sender: TObject);
var
	i: Integer;
begin
	with GrList2 do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := CbMANGANH.LookupValue;
    1:
       	s := CbMANHOM.LookupValue;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';

    CmdIns2.Enabled := not QrDMNCC.IsEmpty;
    CmdDel2.Enabled := GrList2.Cells[0, GrList2.Row] <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
    with GrList2 do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrNGANH, QrNHOM, QrNHOM2, QrDMNCC]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdClear2Execute(Sender: TObject);
var
	i: Integer;
begin
	with GrList2 do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhomNCC.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
