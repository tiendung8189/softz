unit exThread;

interface

uses
	Classes, ActiveX, Forms, Variants;

type
  TExProc = procedure(pParam: Variant) of object;

  TExThread = class(TThread)
  private
    FOwner: TObject;
    FProc: TExProc;
    FParam: Variant;
  protected
    procedure Execute; override;
    procedure DoProgress;
  public
    constructor Create(proc: TExProc;
        pParam: Variant;
        pOwner: TObject = nil);
    destructor Destroy; override;

    function IsFinished: Boolean;
  end;

var
    mgMyThread: TExThread;

    procedure TerminateMyThread;
implementation

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TerminateMyThread;
begin
//    if (fMyThread <> nil) and not fMyThread.Finished then
//        try
//            fMyThread.Terminate;
//        except
//        end;
end;

{ TMyThread }
(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TExThread.Create(proc: TExProc; pParam: Variant; pOwner: TObject);
begin
    FOwner := pOwner;
    FProc := proc;
    FParam := pParam;
    FreeOnTerminate := True;
    inherited Create(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TExThread.Destroy;
begin
    FOwner := nil;
    VarClear(FParam);
    mgMyThread := nil;
  inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExThread.DoProgress;
begin
    FProc(FParam);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TExThread.Execute;
begin
    CoInitialize(Application);
//    Synchronize(DoProgress);
    DoProgress;
    CoUninitialize;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TExThread.IsFinished: Boolean;
begin
    Result :=
        Finished or (FOwner = nil);
end;
{ End TMyThread }
end.
