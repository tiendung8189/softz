object FrmBkgrOption: TFrmBkgrOption
  Left = 668
  Top = 363
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Canh Ch'#7881'nh'
  ClientHeight = 203
  ClientWidth = 410
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 8
    Top = 91
    Width = 285
    Height = 102
    Caption = ' Ch'#7919' '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 76
      Top = 40
      Width = 30
      Height = 16
      Caption = 'Color'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 35
      Top = 69
      Width = 66
      Height = 16
      Caption = 'Background'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbFontColor: TfcColorCombo
      Left = 116
      Top = 36
      Width = 157
      Height = 24
      ColorDialogOptions = [cdoEnabled, cdoAnyColor]
      ColorListOptions.Font.Charset = DEFAULT_CHARSET
      ColorListOptions.Font.Color = clWindowText
      ColorListOptions.Font.Height = -11
      ColorListOptions.Font.Name = 'MS Sans Serif'
      ColorListOptions.Font.Style = []
      ColorListOptions.Options = [ccoShowColorNone, ccoShowCustomColors, ccoShowStandardColors, ccoShowColorNames]
      DropDownCount = 8
      ReadOnly = False
      SelectedColor = 268435455
      TabOrder = 1
      OnChange = CbFontColorChange
    end
    object CkShow: TCheckBox
      Left = 116
      Top = 16
      Width = 83
      Height = 17
      Caption = 'Hi'#7879'n ch'#7919
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CkShowClick
    end
    object CbBkgrColor: TfcColorCombo
      Tag = 3
      Left = 116
      Top = 64
      Width = 157
      Height = 24
      ColorDialogOptions = [cdoEnabled, cdoAnyColor]
      ColorListOptions.Font.Charset = DEFAULT_CHARSET
      ColorListOptions.Font.Color = clWindowText
      ColorListOptions.Font.Height = -11
      ColorListOptions.Font.Name = 'MS Sans Serif'
      ColorListOptions.Font.Style = []
      ColorListOptions.Options = [ccoShowColorNone, ccoShowCustomColors, ccoShowStandardColors, ccoShowColorNames]
      DropDownCount = 8
      ReadOnly = False
      SelectedColor = 268435455
      TabOrder = 2
      OnChange = CbFontColorChange
    end
  end
  object CmdOK: TBitBtn
    Left = 304
    Top = 12
    Width = 97
    Height = 25
    Cursor = 1
    Caption = #272#7891'ng '#253
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    ModalResult = 1
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 3
  end
  object BitBtn3: TBitBtn
    Left = 304
    Top = 40
    Width = 97
    Height = 25
    Cursor = 1
    Caption = 'Tho'#225't'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Kind = bkCancel
    ParentFont = False
    TabOrder = 4
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 4
    Width = 285
    Height = 85
    Caption = ' H'#236'nh '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label5: TLabel
      Left = 44
      Top = 24
      Width = 62
      Height = 16
      Caption = 'Draw Style'
    end
    object CbStyle: TwwDBComboBox
      Left = 116
      Top = 20
      Width = 157
      Height = 24
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Center'#9'0'
        'Normal'#9'1'
        'Proportional'#9'2'
        'Proportional Center'#9'3'
        'Stretch'#9'4'
        'Tile'#9'5')
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
    end
    object CbColor: TfcColorCombo
      Left = 116
      Top = 48
      Width = 157
      Height = 24
      ColorDialogOptions = [cdoEnabled, cdoAnyColor]
      ColorListOptions.Font.Charset = DEFAULT_CHARSET
      ColorListOptions.Font.Color = clWindowText
      ColorListOptions.Font.Height = -11
      ColorListOptions.Font.Name = 'MS Sans Serif'
      ColorListOptions.Font.Style = []
      ColorListOptions.Options = [ccoShowColorNone, ccoShowCustomColors, ccoShowStandardColors, ccoShowColorNames]
      DropDownCount = 8
      ReadOnly = False
      SelectedColor = 268435455
      TabOrder = 2
    end
    object ChkChon: TCheckBox
      Left = 16
      Top = 52
      Width = 93
      Height = 17
      Caption = 'Transparent'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object CmdDefault: TBitBtn
    Left = 304
    Top = 100
    Width = 97
    Height = 25
    Cursor = 1
    Caption = 'M'#7863'c '#273#7883'nh'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CmdDefaultClick
  end
  object Panel1: TPanel
    Left = 304
    Top = 156
    Width = 97
    Height = 37
    BevelOuter = bvSpace
    Color =clBtnFace
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
ParentColor=False
    object LbXemtruoc: TLabel
      Left = 1
      Top = 1
      Width = 95
      Height = 37
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'DEMO'
      Color = 11711154
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -27
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
end
