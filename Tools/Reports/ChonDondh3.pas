(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDondh3;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, DBCtrls;

type
  TFrmChonDondh3 = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsDONDH: TDataSource;
    QrDONDH: TADOQuery;
    QrDONDHNGAY: TDateTimeField;
    QrDONDHSCT: TWideStringField;
    QrDONDHMADT: TWideStringField;
    QrDONDHMAKHO: TWideStringField;
    QrDONDHNG_DATHANG: TWideStringField;
    QrDONDHTHANHTOAN: TFloatField;
    QrDONDHDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    LbNHAPCUA: TLabel;
    Label31: TLabel;
    CbDV: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    CbMADV: TwwDBLookupCombo;
    CbMAKHO: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrDONDHTENKHO: TWideStringField;
    QrDONDHTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrDONDHNGAY_GIAO: TDateTimeField;
    QrDONDHCHIETKHAU_HD: TFloatField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrDONDHSOTIEN: TFloatField;
    QrDONDHSOLUONG: TFloatField;
    QrDONDHKHOA: TGuidField;
    Cbx1: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMADVChange(Sender: TObject);
    procedure CbMADVExit(Sender: TObject);
    procedure CbMADVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDONDHBeforeOpen(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL, mSqlDT, mLCT, mTT: String;

    fTungay, fDenngay: TDateTime;

  public
  	function Execute(var pCheck: Boolean; pFix: Boolean; pNcc, pKho: String;
    	pLoai: string = 'DM_NCC'; pLCT: String = 'DHN'; pTT: String = ''): TGUID;
  end;

var
  FrmChonDondh3: TFrmChonDondh3;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDondh3.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    mTT := pTT;
    CbMADV.Enabled := not pFix;
    CbDV.Enabled := not pFix;
    Cbx1.Checked := False;

    QrDM_KH_NCC.SQL.Text := Format(mSqlDT, [pLOAI]);
    mLCT := pLCT; 

	if ShowModal = mrOK then
    begin
        pCheck := Cbx1.Checked;
		Result := TGuidField(QrDONDH.FieldByName('KHOA')).AsGuid
    end
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DONDH', GrBrowse);

    SetDisplayFormat(QrDONDH, sysCurFmt);
    SetShortDateFormat(QrDONDH);
    SetDisplayFormat(QrDONDH, ['NGAY'], DateTimeFmt);

    mSQL := QrDONDH.SQL.Text;
    mSqlDT := QrDM_KH_NCC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);

    CbMADV.LookupValue := mNCC;
    CbMAKHO.LookupValue := mKho;

    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    // Smart focus
    if mNCC = '' then
		try
    		CbMADV.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDONDH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
   	if (fTungay <> EdTungay.Date)  or
    	(fDenngay <> EdDenngay.Date) or
    	(mNCC <> CbMADV.LookupValue) or
	   	(mKHO <> CbMAKHO.LookupValue) then
	begin
   	    mNCC := CbMADV.LookupValue;
        mKHO := CbMAKHO.LookupValue;
        fTungay  := EdTungay.Date;
	    fDenngay := EdDenngay.Date;

        with QrDONDH do
        begin
			Close;

            s := mSQL;

            if mNCC <> '' then
                s := s + ' and a.MADT=''' + mNCC + '''';

			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';

            if mTT <> '' then
                s := s + ' and a.TINHTRANG in (select Value from dbo.fnS_Table(''' + mTT + '''))';
            SQL.Text := s;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CbMADVChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDBLookupCombo).Text = '' then
        s := ''
    else
    	s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TComponent).Tag of
    // Ma NCC
    0:
		CbDV.LookupValue := s;
    // Ten NCC
    1:
        CbMADV.LookupValue := s;
    // Ma kho
    2:
        CbKHO.LookupValue := s;
    // Ten kho
    3:
		CbMAKHO.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CbMADVExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CbMADVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.QrDONDHBeforeOpen(DataSet: TDataSet);
begin
      with QrDONDH do
      begin
      	Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
        Parameters[2].Value := fDenngay;
      end;
end;

end.
