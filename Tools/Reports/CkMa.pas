(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CkMa;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Graphics;

type
  TFrmCkma = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdTEN: TEdit;
    CmdOK: TBitBtn;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    EdCK: TEdit;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure CmdOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  public
  	function GetDisCount (ten : String; var k : Double) : Boolean;
  end;

var
  FrmCkma: TFrmCkma;

implementation

{$R *.DFM}

uses
	isLib, isMsg, exCommon, isCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCkma.GetDisCount;
var
	cs : String;
begin
	EdTEN.Text := Trim(ten);
    EdCk.Text  := FloatToStr(k);
    EdCk.SelectAll;

   	Result := ShowModal = mrOK;
    if Result then
	begin
        cs := Trim(EdCk.Text);
        if cs = '' then
        	k := 0
        else
	    	k := StrToFloat(cs);
    end;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;
    EdCk.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.CmdOKClick(Sender: TObject);
var
	s : String;
    x : Double;
    b : Boolean;
begin
	b := True;
	s := Trim(EdCk.Text);
   	x := 0;

	if s <> '' then
	    try
		    x := StrToFloat(s);
	    except
			b := False;
	    end;

    if b then
    	b := x >= 0;

    if b then
       	ModalResult := mrOK
    else
    begin
   		ErrMsg(RS_INVALID_DISCOUNT);
       	EdCk.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
