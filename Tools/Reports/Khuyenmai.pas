﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Khuyenmai;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants, Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls, Wwmemo,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2,
  wwdbdatetimepicker, wwdbedit, Wwdbcomb, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi, isDb,
  isPanel, wwDialog, Wwdotdot, Mask, Grids, Wwdbgrid, ToolWin, wwdblook,
  frameLoc;

type
  TFrmKhuyenmai = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrKM: TADOQuery;
    QrCT: TADOQuery;
    DsKM: TDataSource;
    DsCT: TDataSource;
    QrCTRSTT: TIntegerField;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKMXOA: TWideStringField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrKMIMG: TIntegerField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    CmdAudit: TAction;
    BtnBarCode2: TToolButton;
    QrKMNGAY: TDateTimeField;
    QrKMSCT: TWideStringField;
    QrKMTUNGAY: TDateTimeField;
    QrKMDENNGAY: TDateTimeField;
    QrKMTL_CK: TFloatField;
    QrKMLYDO: TWideStringField;
    QrKMDGIAI: TWideMemoField;
    QrKMCREATE_BY: TIntegerField;
    QrKMCREATE_DATE: TDateTimeField;
    QrKMUPDATE_BY: TIntegerField;
    QrKMUPDATE_DATE: TDateTimeField;
    QrKMDELETE_BY: TIntegerField;
    QrKMDELETE_DATE: TDateTimeField;
    QrCTSTT: TIntegerField;
    QrCTMAVT: TWideStringField;
    QrCTGIABAN: TFloatField;
    QrCTTL_LAI: TFloatField;
    TntLabel1: TLabel;
    EdLyDo: TwwDBEdit;
    Label5: TLabel;
    CbTungay: TwwDBDateTimePicker;
    TntLabel2: TLabel;
    CbDenngay: TwwDBDateTimePicker;
    TntLabel3: TLabel;
    EdTLCK: TwwDBEdit;
    Label10: TLabel;
    DBEdit1: TDBMemo;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_GIABAN: TFloatField;
    TntToolButton2: TToolButton;
    QrDMHH: TADOQuery;
    QrCTLK_DVT: TWideStringField;
    QrCTLK_TL_LAI: TFloatField;
    QrKMDENGIO: TDateTimeField;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    CbHinhthuc: TwwDBLookupCombo;
    TntLabel6: TLabel;
    wwDBEdit1: TwwDBDateTimePicker;
    QrKMTUGIO: TDateTimeField;
    wwDBEdit2: TwwDBDateTimePicker;
    CmdDiemban: TAction;
    TntToolButton3: TToolButton;
    TntToolButton4: TToolButton;
    Status2: TStatusBar;
    Filter2: TwwFilterDialog2;
    CmdListRefesh: TAction;
    CmdChonNhom: TAction;
    QrTemp: TADOQuery;
    CmdEmpty: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    N3: TMenuItem;
    Xachitit1: TMenuItem;
    CmdChonNCC: TAction;
    Chnnhcungcp1: TMenuItem;
    CmdChonphieuKM: TAction;
    QrTemp2: TADOQuery;
    Chnnhcungcp2: TMenuItem;
    QrKMKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrKMLOC: TWideStringField;
    CmdChonNhomNCC: TAction;
    Chnnhcungcpnhmhng1: TMenuItem;
    QrCTTINHTRANG: TWideStringField;
    QrKMLCT: TWideStringField;
    QrKMHTKM: TWideStringField;
    QrCTLK_QD1: TIntegerField;
    QrCTLK_DVT1: TWideStringField;
    QrCTGIABAN1: TFloatField;
    QrCTTL_LAI1: TFloatField;
    QrCTB1: TBooleanField;
    QrCTTL_CK: TFloatField;
    QrCTLK_GIANHAPVAT: TFloatField;
    CmdIntem: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    QrCTLK_GIAVON: TFloatField;
    QrList: TADOStoredProc;
    Label3: TLabel;
    EdGiaban: TwwDBEdit;
    QrKMGIABAN: TFloatField;
    QrKMLK_HINHTHUC: TWideStringField;
    QrKMNOIDUNG: TWideStringField;
    QrDM: TADOQuery;
    QrDMMACHUNG: TWideStringField;
    QrDMMAVT: TWideStringField;
    QrDMTENVT: TWideStringField;
    QrKMCHECKED: TBooleanField;
    QrKMCHECKED_BY: TIntegerField;
    QrKMCHECKED_DATE: TDateTimeField;
    QrKMCHECKED_DESC: TWideMemoField;
    ToolButton6: TToolButton;
    CmdChecked: TAction;
    ToolButton8: TToolButton;
    QrKMIMG2: TIntegerField;
    Label4: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    EdCheckedDate: TwwDBComboDlg;
    Label6: TLabel;
    MemoDate: TwwMemoDialog;
    PopItem1: TMenuItem;
    PopItem2: TMenuItem;
    PopItemAll: TMenuItem;
    N4: TMenuItem;
    QrKMCAPDO: TIntegerField;
    QrList_Tonkho: TADOStoredProc;
    N6: TMenuItem;
    N7: TMenuItem;
    Label7: TLabel;
    CbLYDO: TwwDBLookupCombo;
    QrKMLK_LYDO: TWideStringField;
    QrCTTENVT: TWideStringField;
    QrCTGIANHAPVAT: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrCTCREATE_BY: TIntegerField;
    QrCTCREATE_DATE: TDateTimeField;
    QrCTUPDATE_BY: TIntegerField;
    QrCTUPDATE_DATE: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrKMAfterInsert(DataSet: TDataSet);
    procedure QrKMBeforeOpen(DataSet: TDataSet);
    procedure QrKMBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKMBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKMCalcFields(DataSet: TDataSet);
    procedure QrKMBeforeEdit(DataSet: TDataSet);
    procedure QrKMAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrKMAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDiembanExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdChonNhomExecute(Sender: TObject);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdChonNCCExecute(Sender: TObject);
    procedure CmdChonphieuKMExecute(Sender: TObject);
    procedure CmdChonNhomNCCExecute(Sender: TObject);
    procedure QrCTTL_CKChange(Sender: TField);
    procedure QrKMTL_CKChange(Sender: TField);
    procedure CmdIntemExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure QrKMHTKMChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure EdCheckedDateCustomDlg(Sender: TObject);
    procedure MemoDateInitDialog(Dialog: TwwMemoDlg);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure PopItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;

    // List filter
    fType, mFilter: Integer;
    mLCT, fSQL, fStr, tmpSQL, tmpSQL2: String;
    fLoc: String;
  public
	procedure Execute(r: WORD);
    procedure GetListHH(var pCount: Integer; pType: Integer;
        pString, pMadt: String; pKhoa: TGUID);
  end;

var
  FrmKhuyenmai: TFrmKhuyenmai;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, isCommon,
    ChonDsma, isLib, Khuyenmai2, ChonDsNhom, ChonDsNCC, ChonPhieuKM, GuidEx,
  ChonDsNhomNCC, ExcelData, ImportExcel, isFile, ChonDsHHKM;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_KHUYENMAI_BL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.EdCheckedDateCustomDlg(Sender: TObject);
begin
    EdCheckedDate.SelectAll;
	MemoDate.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.Execute;
begin
    mLCT := 'KM';

	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.FormCreate(Sender: TObject);
begin
    frNavi.DataSet := QrKM;

    // Display Format
    with QrKM do
    begin
        SetDisplayFormat(QrKM, sysCurFmt);
	    SetShortDateFormat(QrKM);
    	SetDisplayFormat(QrKM, ['TUGIO', 'DENGIO'], 'hh:nn');
	    SetDisplayFormat(QrKM, ['TL_CK'], sysPerFmt);
        SetDisplayFormat(QrKM, ['NGAY', 'CHECKED_DATE'], DateTimeFmt);
    end;
	SetDisplayFormat(QrCT, sysCurFmt);
    SetDisplayFormat(QrCT, ['TL_LAI', 'TL_LAI1', 'TL_CK'], sysPerFmt);

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrKM, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, nil]);

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrKM.SQL.Text;
    tmpSQL := QrTemp.SQL.Text;
    tmpSQL2 := QrTemp2.SQL.Text;
    mFilter := 1;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

	Wait(PREPARING);
    with DataMain do
        OpenDataSets([QrLOAI_KHUYENMAI, QrLYDO_KM]);
    QrDMHH.Open;
    SetDisplayFormat(QrDMHH, sysCurFmt);
    SetDisplayFormat(QrDMHH,['TL_LAI'], sysPerFmt);
    ClearWait;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrKM, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdRefreshExecute(Sender: TObject);
var
	s, s2 : String;
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    d := Now;
    DecodeDate(d, yy, mm, dd);
    DecodeTime(d, hh, nn, ss, ms);

    d1 := EncodeDate(yy, mm, dd);
    d2 := EncodeTime(hh, nn, ss, ms);
   	if (fLOC <> '')then
    begin
        fLoc     := '';
		Screen.Cursor := crSQLWait;
		with QrKM do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           		case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from KHUYENMAI_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = KHUYENMAI.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from KHUYENMAI_CT a, DM_VT_FULL b where a.KHOA = KHUYENMAI.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from KHUYENMAI_CT where KHOA = KHUYENMAI.KHOA and MAVT in (' + fStr + '))');
				end;
            s2 := '';
            case mFilter of
            1:
            begin
               s2 := ' and ( (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <=  DENNGAY' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' >= isnull(TUGIO, CAST(''1899/12/30 00:00:00'' as datetime))' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' <= isnull(DENGIO, CAST(''1899/12/30 23:59:59'' as datetime)) )' +
                          ' and isnull(DELETE_BY, 0) = 0 )';
                SQL.Add(s2);
            end;
            2:
            begin
                s2 := ' and ( not (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <=  DENNGAY' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' >= isnull(TUGIO, CAST(''1899/12/30 00:00:00'' as datetime))' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' <= isnull(DENGIO, CAST(''1899/12/30 23:59:59'' as datetime)) )' +
                          ' or isnull(DELETE_BY, 0) <> 0 )';
                SQL.Add(s2);
            end;
            end;

			SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMHH.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdNewExecute(Sender: TObject);
begin
	if not (QrKM.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrKM.Cancel;
        end;

	QrKM.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrKM.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrKM.Cancel;

    if QrKM.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdChonNCCExecute(Sender: TObject);
var
	s: String;
    x: Integer;
begin
    if not FrmChonDsNCC.Get(s) then
        Exit;

    if s = '' then
        Exit;

    GetListHH(x, 0, '', s, TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdChonNhomExecute(Sender: TObject);
var
	s: String;
    mType, x: Integer;
begin
    if not FrmChonDsNhom.Get(mType, s) then
        Exit;

    if s = '' then
        Exit;

    GetListHH(x, mType, s, '', TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdChonNhomNCCExecute(Sender: TObject);
var
	s, s2, ma: String;
    mType, x: Integer;
begin
    if not FrmChonDsNhomNCC.Get(mType, s, s2) then
        Exit;

    if (s = '') or (s2 = '') then
    begin
        ErrMsg('Dữ liệu chọn không hợp lệ.');
        Exit;
    end;

//    with QrTemp do
//    begin
//        Close;
//        SQL.Text := tmpSQL;
//        case mType of
//        0:	// Nganh
//            SQL.Add(' and MANGANH in (' + s + ')');
//        1:	// Nhom
//            SQL.Add(' and MANHOM in (' + s + ')');
//        else
//            SQL.Text := (' and 1=1');
//        end;
//
//        SQL.Add(' and b.MADT in (' + s2 + ')');
//
//        Open;
//        x := 0;
//        while not Eof do
//        begin
//            ma := FieldByName('MAVT').AsString;
//            with QrCT do
//                if not Locate('MAVT', ma, []) then
//                begin
//                    Append;
//                    FieldByName('MAVT').AsString := ma;
//                    Inc(x);
//                    CheckBrowseMode;
//                end;
//
//            Next;
//        end;
//        Close;
//    end;
    GetListHH(x, mType, s, s2, TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdChonphieuKMExecute(Sender: TObject);
var
    n: TGUID;
    s: String;
    x: Integer;
begin
    Application.CreateForm(TFrmChonPhieuKM, FrmChonPhieuKM);
    n := FrmChonPhieuKM.Execute;
	FrmChonPhieuKM.Free;

    if TGuidEx.IsEmptyGuid(n) then
        Exit;

    GetListHH(x, -1, '', '', n);

    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU) then
       	MarkDataSet(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not QrDMHH.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;

                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA );
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdIntemExecute(Sender: TObject);
var
    bm: TBytes;
begin
    CmdSave.Execute;
	if not ShowReport(Caption, FORM_CODE + '_INTEM', [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]) then
        Exit;

    with QrCT do
    begin
    	bm := Bookmark;
        DisableControls;
        First;
        while not Eof do
        begin
            if FieldByName('B1').AsBoolean then
            begin
                Edit;
                FieldByName('B1').AsBoolean := False;
            end;

            Next;
        end;
        Bookmark := bm;
        EnableControls;
    end;

    CmdSave.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsKM)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdReReadExecute(Sender: TObject);
begin
	fLoc := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted, bHt: Boolean;
    n: Integer;
begin
	with QrKM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
        bHt := FieldByName('HTKM').AsString = 'K';
    end;
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrKM);

    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrKM, False);
    CmdChecked.Caption := GetCheckedCaption(QrKM);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdIntem.Enabled := not bEmpty;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdDiemban.Enabled := mCanEdit and not bEmpty and bBrowse and (n = 1);

//    EdGiaban.ReadOnly := not bHt;

    CmdEmpty.Enabled := not bEmptyCT;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMAfterInsert(DataSet: TDataSet);
var
    d, d1, d2: TDateTime;
    xx, yy: WORD;
begin
    d := Now;
    d1 := Date;
    DecodeDate(d1, yy, xx, xx);
    d2 := EncodeDate(yy, 12, 31);
	with QrKM do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('LCT').AsString       := mLCT;
        FieldByName('HTKM').AsString      := 'K';
        FieldByName('LYDO').AsString      := CbLYDO.LookupTable.FieldByName('MA').AsString;
        FieldByName('CAPDO').AsInteger    := 1;
        FieldByName('CHECKED').AsBoolean  := False;
		FieldByName('NGAY').AsDateTime    := d;
        FieldByName('TUNGAY').AsDateTime  := d1;
        FieldByName('DENNGAY').AsDateTime := d2;
        FieldByName('TUGIO').AsDateTime   := EncodeTime(0,0,0,0);
        FieldByName('DENGIO').AsDateTime  := 1 + EncodeTime(23,59,0,0);
        FieldByName('LOC').AsString       := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMHTKMChange(Sender: TField);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CAPNHAT_CHIETKHAU =
        'Chỉ cập nhật các mặt hàng chưa có chiết khấu?';
procedure TFrmKhuyenmai.QrKMTL_CKChange(Sender: TField);
var
	bm: TBytes;
    b: TVnsModalResult;
begin
//    b := YesNoCancel(RS_CAPNHAT_CHIETKHAU);
//
//    if b = vmrCancel then
//        Exit;

	with QrCT do
    begin
        Wait(PROCESSING);
        try
            bm := Bookmark;
            DisableControls;
            First;
            while not Eof do
            begin
                Edit;

                if Sender.FieldName = 'TL_CK' then
                begin
    //                if FieldByName('TL_CK').AsFloat = 0 then
                        FieldByName('TL_CK').AsFloat := Sender.AsFloat
                end
                else
    //                if FieldByName('GIABAN1').AsFloat = 0 then
                        FieldByName('GIABAN1').AsFloat := Sender.AsFloat;
                Next;
            end;
            Bookmark := bm;
            EnableControls;
        finally
            ClearWait;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMBeforeOpen(DataSet: TDataSet);
begin
	with QrKM do
    begin
		Parameters[0].Value := mLCT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMBeforePost(DataSet: TDataSet);
var
	dengio: TDateTime;
begin
	with QrKM do
    begin
		if BlankConfirm(QrKM, ['NGAY', 'LYDO', 'HTKM', 'TUNGAY', 'DENNGAY']) then
	    	Abort;

//	    exValidClosing(FieldByName('NGAY').AsDateTime);

	    // Adjust hours
	    dengio := Frac(FieldByName('DENGIO').AsDateTime);
        if dengio <> 0 then
        	FieldByName('DENGIO').AsDateTime := dengio
        else
        	FieldByName('DENGIO').AsDateTime := 1;

        if FieldByName('TUGIO').AsDateTime > FieldByName('DENGIO').AsDateTime then
        begin
        	ErrMsg('Giờ khuyến mãi không hợp lệ.');
        	Abort;
        end;
	end;
    DataMain.AllocSCT(mLCT, QrKM);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrKM);
    if mTrigger then
        Exit;

    exIsChecked(QrKM);
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrKMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTBeforePost(DataSet: TDataSet);
begin
	if not mTrigger then
        with QrCT do
        begin
            if BlankConfirm(QrCT, ['MAVT']) then
                Abort;

            if FieldByName('LK_TENVT').AsString = '' then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL1);
                Abort;
            end;
        end;
    with DataSet do
    if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
    SetAudit(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTMAVTChange(Sender: TField);
begin
	exDotMavt(1, QrDMHH, Sender);
    with QrCT do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;

        FieldByName('GIANHAPVAT').AsFloat := FieldByName('LK_GIANHAPVAT').AsFloat;
    	FieldByName('GIABAN').AsFloat := FieldByName('LK_GIABAN').AsFloat;
        FieldByName('TL_LAI').AsFloat := FieldByName('LK_TL_LAI').AsFloat;

        if QrKM.FieldByName('HTKM').AsString = 'K' then
            FieldByName('TL_CK').AsFloat := QrKM.FieldByName('TL_CK').AsFloat
        else
            FieldByName('GIABAN1').AsFloat := QrKM.FieldByName('GIABAN').AsFloat
    end;
	GrDetail.InvalidateCurrentRow;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTMAVTValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
        if IsDuplicateCode(QrCT, Sender) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.QrCTTL_CKChange(Sender: TField);
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        mTrigger := True;
        if Sender.FieldName = 'TL_CK' then
        begin
            FieldByName('GIABAN1').AsFloat := exVNDRound2(FieldByName('GIABAN').AsFloat *
            	(1 - FieldByName('TL_CK').AsFloat / 100))
        end else
        begin
            FieldByName('TL_CK').AsFloat :=
                100 * (1 - SafeDiv(FieldByName('GIABAN1').AsFloat, FieldByName('GIABAN').AsFloat))
        end;
        mTrigger := False;

        FieldByName('TL_LAI1').AsFloat := 100 *
            SafeDiv((FieldByName('GIABAN1').AsFloat - FieldByName('LK_GIANHAPVAT').AsFloat)
                ,(FieldByName('GIABAN1').AsFloat));
    end;
end;

(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.GetListHH(var pCount: Integer; pType: Integer; pString, pMadt: String; pKhoa: TGUID);
var
    s: String;
begin

    pCount := 0;
    Application.CreateForm(TFrmChonDsHHKM, FrmChonDsHHKM);
    if not FrmChonDsHHKM.Execute(pType, pString, pMadt, pKhoa) then
        Exit;

    with QrList do
    begin
        Wait(PROCESSING);
        try
            Filter := 'SELECTED=1';
            Filtered := True;
            if RecordCount > 0 then
            begin
                SetEditState(QrKM);
                First;
                while not eof do
                begin
                    s := FieldByName('MAVT').AsString;
                    if not QrCT.Locate('MAVT', s, []) then
                    begin
                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        Inc(pCount);
                    end;

                    Next;
                end;
                QrCT.CheckBrowseMode;
            end;
            Filter := '';
        finally
            ClearWait;
        end;
    end;
    QrList.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    if Highlight then
        Exit;

    d := Now;
    DecodeDate(d, yy, mm, dd);
    DecodeTime(d, hh, nn, ss, ms);

    d1 := EncodeDate(yy, mm, dd);
    d2 := EncodeTime(hh, nn, ss, ms);
    with QrKM do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clRed;
            Exit;
        end;

        if  not ((d1 <= (FieldByName('DENNGAY').AsDateTime)) and
            (d2 >= FieldByName('TUGIO').AsDateTime) and
            (d2 <= FieldByName('DENGIO').AsDateTime))
        then
        begin
            AFont.Color := clSilver;
            Exit;
        end;

        if not FieldByName('CHECKED').AsBoolean then
        begin
            AFont.Color := clMaroon;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.GrBrowseDblClick(Sender: TObject);
begin
    if QrKM.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.MemoDateInitDialog(Dialog: TwwMemoDlg);
var
	ls: TStrings;
    i, n: Integer;
begin
	ls := TStringList.Create;
    ls.Text := QrKM.FieldByName('CHECKED_DESC').AsString;
    n := ls.Count;
    for i := 0 to ls.Count - 1 do
    begin
    	ls[i] := Format('%2d: %s', [n, ls[i]]);
        Dec(n);
    end;

	with Dialog do
    begin
    	Memo.ReadOnly := True;
        OKBtn.Visible := False;
        CancelBtn.Font.Style := [fsBold];
        Memo.Lines.AddStrings(ls);
    end;
    ls.Free;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if PgMain.ActivePageIndex = 1 then
    	Status2.SimpleText := exRecordCount(QrCT,Filter2)
    else
   		Status.SimpleText := exRecordCount(QrKM, Filter);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.PopItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.PopupMenu1Popup(Sender: TObject);
begin
    PopItem1.Checked := mFilter = 1;
    PopItem2.Checked := mFilter = 2;
    PopItemAll.Checked := mFilter = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsKM, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdDiembanExecute(Sender: TObject);
begin
	with QrKM do
    Begin
  		Application.CreateForm(TFrmKhuyenmai2, FrmKhuyenmai2);
		FrmKhuyenmai2.Execute(
            TGuidField(FieldByName('KHOA')).AsGuid,
            FieldByName('CHECKED').AsBoolean,
            FieldByName('SCT').AsString);
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai.CmdEmptyExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*
    ** End: Others
    *)


end.
