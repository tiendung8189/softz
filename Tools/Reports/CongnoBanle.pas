﻿(*==============================================================================
** Updated: 2004-02-24
** > Optimize
** > Them chuc nang dieu chinh thue dau ra
**------------------------------------------------------------------------------
** Updated: 2004-12-17
** > Optimize, xem lai viec total invoice bi sai
**------------------------------------------------------------------------------
*)
unit CongnoBanle;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit;

type
  TFrmCongnoBanle = class(TForm)
    ToolMain: TToolBar;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    QrDMVT: TADOQuery;
    CmdTotal: TAction;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    QrBHNGAY: TDateTimeField;
    QrBHCA: TWideStringField;
    QrBHSOTIEN: TFloatField;
    QrBHCHIETKHAU: TFloatField;
    QrBHTHUE: TFloatField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHTHUNGAN: TWideStringField;
    QrBHXOA: TWideStringField;
    QrBHNG_CK: TWideStringField;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    LbHH1: TLabel;
    EdHH1: TwwDBEdit;
    EdCK: TwwDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    Label24: TLabel;
    Label1: TLabel;
    DBText1: TDBText;
    QrCTBHNG_DC: TWideStringField;
    CmdDel: TAction;
    QrBHNG_HUY: TWideStringField;
    CmdSearch: TAction;
    QrCTBHTRA_DATE: TDateTimeField;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    QrBHSCT: TWideStringField;
    QrBHTL_CK: TFloatField;
    QrCTBHTHUE_SUAT: TFloatField;
    QrBHCK_BY: TIntegerField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    QrCTBHTRA_BY: TIntegerField;
    QrBHPTTT: TWideStringField;
    QrBHCHUATHOI: TFloatField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrBHDGIAI: TWideMemoField;
    QrBHIMG: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    QrCTBHTL_CK: TFloatField;
    QrCTBHCHIETKHAU: TFloatField;
    QrCTBHTIEN_THUE: TFloatField;
    QrCTBHTENTAT: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrBHSOLUONG: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    QrBHMADT: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHTHANHTOAN: TFloatField;
    QrBHLK_TENDT: TWideStringField;
    CmdReRead: TAction;
    QrBHTENKHO: TWideStringField;
    CmdFilterCom: TAction;
    QrBHQUAY: TWideStringField;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    QrCTBHSOTIEN: TFloatField;
    Bevel1: TBevel;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHLOAITHUE: TWideStringField;
    QrBHLK_PTTT: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrBHDRC_STATUS: TWideStringField;
    TntLabel1: TLabel;
    CbMaKho: TwwDBLookupCombo;
    CbTenKho: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrCTBHMABO: TWideStringField;
    QrCTBHTL_CK2: TFloatField;
    QrCTBHTL_CK3: TFloatField;
    QrCTBHTL_CK4: TFloatField;
    QrCTBHTL_CK5: TFloatField;
    QrCTBHTL_CK6: TFloatField;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrBHTTTT: TWideStringField;
    CbTINHTRANG: TwwDBLookupCombo;
    Label2: TLabel;
    QrBHLK_TTTT: TWideStringField;
    QrBHPTTT2: TWideStringField;
    QrBHLK_PTTT2: TWideStringField;
    QrCTBHTL_CK7: TFloatField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHLK_TENTK: TWideStringField;
    QrBHLK_DAIDIEN: TWideStringField;
    QrBHLK_NGANHANG: TWideStringField;
    CmdThongtinCN2: TAction;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    QrBHDEBT_BY: TIntegerField;
    QrBHDEBT_DATE: TDateTimeField;
    QrBHLOC: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrBHBeforeEdit(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CbMAQUAYChange(Sender: TObject);
    procedure QrBHTL_CKChange(Sender: TField);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrBHTTTTValidate(Sender: TField);
    procedure CmdThongtinCN2Execute(Sender: TObject);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
  private
    mCanEdit: Boolean;
    mVATMode: Integer;

    // List filter
    fType: Integer;
    fKho, fSQL, fStr: String;

    procedure Total(fUpdate: Boolean);
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmCongnoBanle: TFrmCongnoBanle;

implementation

uses
	isDb, ExCommon, MainData, Rights, RepEngine, ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, ThongtinCN2, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'POS_CONGNO';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrBH;

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrBH.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.FormShow(Sender: TObject);
begin
	OpenDataSets([QrDMKHO, DataMain.QrTTTT, DataMain.QrDMTK]);

    CbMaKho.LookupValue := sysDefKho;
    CbTenKho.LookupValue := sysDefKho;


	QrDMQUAY.Open;

    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexReadInteger('POS', 'VAT Form');

    with QrBH do
    begin
	    SetDisplayFormat(QrBH, sysCurFmt);
    	SetShortDateFormat(QrBH);
	    SetDisplayFormat(QrBH, ['NGAY'], 'dd/mm/yyyy hh:nn');
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
    	SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);
    
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDMKHO, DataMain.QrTTTT]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;
    (*
    ** End: Forms events
	*)

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin

   	if (CbMaKho.LookupValue <> fKho) then
    begin
	    fKho := CbMaKho.LookupValue;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ CbMaKho.LookupValue + '''');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT b, DM_NHOM c where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT b where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from BANLE_CT where KHOA = BANLE.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY, SCT');
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CbMaKhoBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CbMaKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
	with QrDMQUAY do
    begin
    	Close;
        Open;
    end;
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdSaveExecute(Sender: TObject);
begin
	QrCTBH.UpdateBatch;
	QrBH.Post;

    if QrBH.FieldByName('TTTT').AsString = '03' then
    begin
        CmdReRead.Execute;
        PgMain.ActivePageIndex := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdCancelExecute(Sender: TObject);
begin
	QrCTBH.CancelBatch;
    QrBH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdPrintExecute(Sender: TObject);
var
	k: TGUID;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  TGuidField(FieldByName('KHOA')).AsGuid;
	end;

    if ShowReport('Bill', FORM_CODE, [TGuidEx.ToStringEx(k)]) then
        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdDelExecute(Sender: TObject);
var
    s: String;
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
    s := QrBH.FieldByName('DGIAI').AsString;
    if QrBH.FieldByName('DELETE_BY').AsInteger = 0 then
    begin
        s := FrmReceiptDesc.Execute(s);
        if s = '' then
        begin
            ErrMsg('Phải nhập ghi chú mới được xóa phiếu.');
            Exit;
        end;
    end;

   	MarkDataSet(QrBH);
    with QrBH do
    begin
    	Edit;
        FieldByName('DGIAI').AsString := s;
//        FieldByName('TINHTRANG').AsString := '2';
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdThongtinCN2Execute(Sender: TObject);
var
    s: String;
    mPT1, mPT2: String;
begin
    s := '';
    with QrBH do
    begin
        mPT1 := FieldByName('PTTT').AsString;
        mPT2 := FieldByName('PTTT2').AsString;
        if (mPT1 = '02') or (mPT1 = '03') then
            s := mPT1;

        if (mPT2 = '02') or (mPT2 = '03') then
            s := mPT2;
    end;

    if s = '' then
        Exit;

    Application.CreateForm(TFrmThongtinCN2, FrmThongtinCN2);
    FrmThongtinCN2.Execute(s);
//    FrmThongtinCN2.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
    Total(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdReReadExecute(Sender: TObject);
begin
    fKho := '~';
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrBH);

    CmdPrint.Enabled := n = 1;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';

    CmdThongtinCN2.Enabled := n = 1;
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHBeforeEdit(DataSet: TDataSet);
begin
    with QrBH do
    begin
        if FieldByName('TTTT').AsString = '03' then
        begin
            ErrMsg('Phiếu đã thanh toán, không thể chỉnh sửa.');
            Abort;
        end;
    end;

	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHTL_CKChange(Sender: TField);
begin
	Total(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM_THANHTOAN = 'Lưu phiếu rồi sẽ không chỉnh sửa được. Tiếp tục?'#13
                        + 'Thu ngân: "%s". Ngày: "%s".';
procedure TFrmCongnoBanle.QrBHTTTTValidate(Sender: TField);
var
    d: TDateTime;
begin
    d := Now;
    if Sender.AsString = '03' then
    begin
        if not YesNo(Format(RS_CONFIRM_THANHTOAN, [sysLogonFullName, DateTimeToStr(d)])) then
            Abort;
        with QrBH do
        begin
            FieldByName('DEBT_BY').AsInteger := sysLogonUID;
            FieldByName('DEBT_DATE').AsDateTime := d;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh, xCkhd: Double;
	mSotien, mSoluong, mCkmh, mCkhd, mTlckhd, mThue: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mCkhd := 0;
    mThue := 0;
	mTlckhd := QrBH.FieldByName('TL_CK').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
        	xSotien := FieldByName('SOTIEN').AsFloat;		// Thanh tien chua chiet khau
            xThsuat := FieldByName('THUE_SUAT').AsFloat;	// Thue suat
            xCkmh := FieldByName('CHIETKHAU').AsFloat;		// Tien CKMH
            if xCkmh = 0.0 then								// Tinh tien CKHD
	            xCkhd := xSotien * mTlckhd / 100.0
            else
            	xCkhd := 0.0;

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
	        mCkmh := mCkmh + xCkmh;
	        mCkhd := mCkhd + xCkhd;

            if mVATMode = 0 then	// Tinh thue truoc chiet khau
	            mThue := mThue + xSotien * xThsuat / (100.0 + xThsuat)
	    	else					// Tinh thue sau chiet khau
	            mThue := mThue + (xSotien - xCkmh - xCkhd) * xThsuat / (100.0 + xThsuat);

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
    	FieldByName('CHIETKHAU_MH').AsFloat := mCkmh;
    	FieldByName('CHIETKHAU').AsFloat := mCkhd;
    	FieldByName('THUE').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mSotien - mCkmh - mCkhd;

        if fUpdate then
	        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CbMAQUAYChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDbLookupCombo).Text = '' then
		s := ''
	else
		s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TWinControl).Tag of
    3:
        CbTenkho.LookupValue := s;
    4:
    	CbMakho.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCongnoBanle.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
