﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TS_DmvtFile;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  ADODb, Db,
  Menus , AppEvnts,
  Wwdbgrid, RzDBBnEd,
  AdvMenus, StdCtrls, Mask, RzEdit, RzDBEdit, Grids, Wwdbigrd, ToolWin,
  Vcl.Graphics;

type
  TFrmTS_DmvtFile = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    Tmmutin1: TMenuItem;
    PaMaster: TPanel;
    BtImport: TRzDBButtonEdit;
    BtClearFile: TRzDBButtonEdit;
    BtView: TRzDBButtonEdit;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListF1: TIntegerField;
    QrListF2: TIntegerField;
    QrListF3: TIntegerField;
    QrListCAL_STT: TIntegerField;
    QrListCAL_EXT: TWideStringField;
    QrListMAVT: TWideStringField;
    QrListFileType: TIntegerField;
    QrListFileName: TWideStringField;
    QrListFileContent: TBlobField;
    QrListFileExt: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    Image1: TImage;
    QrListIdx: TGuidField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure BtImportButtonClick(Sender: TObject);
    procedure BtClearFileButtonClick(Sender: TObject);
    procedure BtViewButtonClick(Sender: TObject);
    procedure QrListAfterInsert(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
  private
    mMa: String;
    mCanEdit: Boolean;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
    procedure Execute(
    	r : WORD;
    	ma, ten: String);
  end;

var
  FrmTS_DmvtFile: TFrmTS_DmvtFile;

implementation

uses
	ExCommon,  isDb, islib, isMsg, Rights, isStr, isFile, MainData, GuidEx,
    ShellAPI, isCommon;

{$R *.DFM}

const
    FORM_CODE: String   = 'TS_DM_HH_FILE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.Execute(r: WORD; ma, ten: String);
begin
    mCanEdit := rCanEdit(r);
    DsList.AutoEdit := mCanEdit;

    mMa := ma;
    LbMA.Caption := ma;
    LbTEN.Caption := ten;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmTS_DmvtFile.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.FormShow(Sender: TObject);
begin
    ForceDirectories(sysAppData);
    QrList.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrList do
    begin
    	Edit;
		TBlobField(FieldByName('FileContent')).LoadFromFile(s);
	    FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.BtClearFileButtonClick(Sender: TObject);
begin
    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.BtImportButtonClick(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.BtViewButtonClick(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.Dettach();
begin
     with QrList do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileContent').Clear;
        FieldByName('FileExt').Clear;
     end;
end;


procedure TFrmTS_DmvtFile.ViewAttachment();
var
	s, sn: String;
begin
	with QrList do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
	        TBlobField(FieldByName('FileContent')).SaveToFile(s);
        except
        	ErrMsg('Lỗi trích xuất nội dung.');
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([QrList]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrList, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdNewExecute(Sender: TObject);
begin
    QrList.Append;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdSaveExecute(Sender: TObject);
begin
    QrList.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdCancelExecute(Sender: TObject);
begin
    QrList.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsList)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrList, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.QrListAfterInsert(DataSet: TDataSet);
begin
    with QrList do
    begin
        FieldByName('FileType').AsInteger := 1;
        TGuidField(FieldByName('Idx')).AsGuid := TGuidEx.NewGuid;
        FieldByName('MAVT').AsString := mMa;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.QrListBeforeOpen(DataSet: TDataSet);
begin
    with QrList do
    begin
        Parameters[0].Value := mMa;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.QrListBeforePost(DataSet: TDataSet);
begin
	with (DataSet) do
    begin
        if BlankConfirm(QrList,['FileName']) then
            Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.QrListCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrList do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('CAL_EXT').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DmvtFile.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrList);
end;

end.
