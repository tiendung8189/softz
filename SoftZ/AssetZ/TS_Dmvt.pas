﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TS_Dmvt;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons,
  AdvEdit, DBAdvEd;

type
  TFrmTS_Dmvt = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTGIANHAPVAT: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTDVT1: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    ItmNCC: TMenuItem;
    ItmNHOM: TMenuItem;
    N1: TMenuItem;
    CmdPhoto: TAction;
    N2: TMenuItem;
    Xemhnh1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N4: TMenuItem;
    QrDMVTTON_MIN: TFloatField;
    PopIn: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    CmdChangeGroup: TAction;
    N5: TMenuItem;
    ingnhnhm1: TMenuItem;
    Xl1: TMenuItem;
    QrDMVTMAMAU: TWideStringField;
    QrDMVTMASIZE: TWideStringField;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    CmdExport: TAction;
    BtExport: TToolButton;
    ToolButton12: TToolButton;
    DMVT_EXPORT: TADOStoredProc;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label14: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label12: TLabel;
    LbX: TLabel;
    EdMA: TwwDBEdit;
    EdTen: TwwDBEdit;
    DBEdit11: TwwDBEdit;
    CbQD1: TwwDBEdit;
    CbDVT_BOX: TwwDBLookupCombo;
    CbDVT: TwwDBLookupCombo;
    PD2: TisPanel;
    Label18: TLabel;
    EdVAT_VAO: TwwDBEdit;
    PD5: TisPanel;
    EdGHICHU: TDBMemo;
    PD6: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBEdit8: TwwDBEdit;
    Label21: TLabel;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    QrDMVTMANHOM2: TWideStringField;
    QrDMVTMANHOM3: TWideStringField;
    QrDMVTMANHOM4: TWideStringField;
    CmdSwitch: TAction;
    QrDMVTLOAITHUE: TWideStringField;
    TntLabel6: TLabel;
    CbLoaithue: TwwDBLookupCombo;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    TntLabel7: TLabel;
    CmdPLU: TAction;
    CmdAudit: TAction;
    CmdTimBarcode: TAction;
    N3: TMenuItem;
    mtheobarcode1: TMenuItem;
    QrDMVTCAL_TONMIN_BOX: TFloatField;
    DMVT_EXPORT2: TADOStoredProc;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    QrDMVTCAL_TONMAX_BOX: TFloatField;
    QrDMVTTON_MAX: TFloatField;
    QrDMVTTRONGLUONG: TFloatField;
    QrDMVTDAI: TFloatField;
    QrDMVTRONG: TFloatField;
    QrDMVTCAO: TFloatField;
    CmdImport: TAction;
    ToolButton9: TToolButton;
    ToolButton13: TToolButton;
    CmdExport2: TAction;
    RzLauncher: TRzLauncher;
    PopEx: TAdvPopupMenu;
    Export1: TMenuItem;
    N7: TMenuItem;
    Xutdliuchothitbkimkho1: TMenuItem;
    CmdTimBarcode2: TAction;
    CmdDmdvt: TAction;
    SpeedButton1: TSpeedButton;
    CmdDmvtCt: TAction;
    ToolButton8: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    Label2: TLabel;
    EdMADT: TDBAdvEdit;
    EdTENDT: TwwDBEdit;
    QrDMVTMADT: TWideStringField;
    QrDMVTLK_TENDT: TWideStringField;
    N6: TMenuItem;
    PopTinhtrang1: TMenuItem;
    PD7: TisPanel;
    LbGianhap: TLabel;
    LbGianhapVat: TLabel;
    wwDBEdit10: TwwDBEdit;
    EdGianhapVat: TwwDBEdit;
    PD8: TisPanel;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    Label9: TLabel;
    wwDBEdit6: TwwDBEdit;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang: TMenuItem;
    PopTinhtrang2: TMenuItem;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    EdDVT1_2: TwwDBEdit;
    EdDVT1_3: TwwDBEdit;
    QrDMVTLK_TENMAU: TWideStringField;
    QrDMVTLK_TENSIZE: TWideStringField;
    CmdUpdateTinhtrang: TAction;
    N8: TMenuItem;
    CpnhtTnhtrngtfileexcel1: TMenuItem;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    Label3: TLabel;
    Label11: TLabel;
    Label26: TLabel;
    wwDBEdit9: TwwDBEdit;
    wwDBEdit13: TwwDBEdit;
    wwDBEdit14: TwwDBEdit;
    wwDBEdit19: TwwDBEdit;
    wwDBLookupCombo4: TwwDBLookupCombo;
    wwDBLookupCombo5: TwwDBLookupCombo;
    QrDMVTTENVT_TA: TWideStringField;
    QrDMVTMADT2: TWideStringField;
    QrDMVTMODEL: TWideStringField;
    QrDMVTDACTINH_KT: TBlobField;
    QrDMVTDACTINH_KT_TA: TBlobField;
    QrDMVTTL_CK_NCC: TFloatField;
    Label29: TLabel;
    Label31: TLabel;
    wwDBEdit16: TwwDBEdit;
    Label8: TLabel;
    wwDBEdit7: TwwDBEdit;
    Label10: TLabel;
    wwDBEdit8: TwwDBEdit;
    Label13: TLabel;
    EdMADT2: TDBAdvEdit;
    EdTENDT2: TwwDBEdit;
    QrDMVTLK_TENDT2: TWideStringField;
    PopChitiet: TAdvPopupMenu;
    ItemKythuat: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    ItemKythuat1: TMenuItem;
    N11: TMenuItem;
    ItemPhukien: TMenuItem;
    CmdDmvtFile: TAction;
    QrDMVTURL: TWideStringField;
    QrDMVTURL2: TWideStringField;
    LbQD: TLabel;
    QrNhom: TADOQuery;
    QrNganh: TADOQuery;
    PD0: TisPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    cmbNganh: TwwDBLookupCombo;
    cmbTenNganh: TwwDBLookupCombo;
    cmbNhom: TwwDBLookupCombo;
    cmbTenNhom: TwwDBLookupCombo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure ItmNCCClick(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdTimBarcodeExecute(Sender: TObject);
    procedure QrDMVTCalcFields(DataSet: TDataSet);
    procedure QrDMVTTON_MINChange(Sender: TField);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdExport2Execute(Sender: TObject);
    procedure CmdTimBarcode2Execute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure QrDMVTMADTChange(Sender: TField);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDMVTDVTChange(Sender: TField);
    procedure CmdUpdateTinhtrangExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTMAVTValidate(Sender: TField);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrDMVTMADT2Change(Sender: TField);
    procedure CmdDmvtCtExecute(Sender: TObject);
    procedure CmdDmvtFileExecute(Sender: TObject);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure cmbNhomBeforeDropDown(Sender: TObject);
  private
  	mCanEdit, mEANConfirm: Boolean;
    mGia: Double;
    mRet, bUnitLevel : Boolean;
    defDvt: Integer;
    sCodeLen, defTinhtrang, defThue: String;
  	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4, mPrefix, mSearch: String;

  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmTS_Dmvt: TFrmTS_Dmvt;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, Scan, Variants,
    CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
  TimBarcode2, Dmkhac, DmvtCT, DmvtThuoctinh, DmvtTTSach, OfficeData,
  TS_DactinhKT, TS_DmvtFile, DmHotro;

{$R *.DFM}

const
	FORM_CODE = 'TS_DM_HH';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmTS_Dmvt.Execute;
begin
	mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    if closeDs then
        CloseDataSets(DataMain.Conn)
    else
    begin
        with DataMain do
            CloseDataSets([QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG]);
    end;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.FormCreate(Sender: TObject);
var
    x: Integer;
begin
    TMyForm(Self).Init2;

    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
    	SetDisplayFormat(QrDMVT, ['TON_MIN', 'TON_MAX'], sysQtyFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
    	SetShortDateFormat(QrDMVT);
    end;

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Don vi tinh quy doi
	bUnitLevel := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;

    LbX.Visible := bUnitLevel;
    LbQD.Visible := bUnitLevel;
    CbQD1.Visible := bUnitLevel;
    CbDVT_BOX.Visible := bUnitLevel;

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE,'EAN Confirm');
	sCodeLen := FlexConfigString(FORM_CODE, 'Code Length');

    // Default unit
	defDvt := GetSysParam('DEFAULT_DVT');
    defThue := GetSysParam('DEFAULT_LOAITHUE');

    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    // Tree Combo
    FlexGroupCombo(CbNhomhang, 'TS');

    // Thue
    if not sysIsThue then
    begin
        PD2.Visible := False;

        LbGianhap.Caption := 'Giá nhập';
        LbGianhapVat.Visible := False;
        EdGianhapVat.Visible := False;

    end;

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PD5.Collapsed := RegReadBool('PD5');
    PD6.Collapsed := RegReadBool('PD6');
    PD7.Collapsed := RegReadBool('PD7');
    PD8.Collapsed := RegReadBool('PD8');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.FormShow(Sender: TObject);
begin
//	DsDMVT.AutoEdit := mCanEdit;
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDMNSX, QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG,
            QrDMLOAITHUE]);
        defTinhtrang := QrDMVT_TINHTRANG.FieldByName('MA_HOTRO').AsString;
    end;
    OpenDataSets([QrNganh, QrNhom]);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('TINHTRANG').AsString <> '01' then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    RegWrite(Name, ['PD2', 'PD5', 'PD6', 'PD7', 'PD8'],
    	[PD2.Collapsed, PD5.Collapsed, PD6.Collapsed,
            PD7.Collapsed, PD8.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdNewExecute(Sender: TObject);
begin
	QrDMVT.Append;
    PD0.Enabled := True;
    cmbNganh.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdSaveExecute(Sender: TObject);
begin
    QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdDelExecute(Sender: TObject);
begin
    if DataMain.BarcodeIsUsed(QrDMVT.FieldByName('MAVT').AsString, True) then
        Abort;
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdDmvtCtExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    Application.CreateForm(TFrmTS_DactinhKT, FrmTS_DactinhKT);
	FrmTS_DactinhKT.Execute(R_FULL, DsDMVT, n);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdDmvtFileExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmTS_DmvtFile, FrmTS_DmvtFile);
	FrmTS_DmvtFile.Execute(R_FULL, QrDMVT.FieldByName('MAVT').AsString,
                            QrDMVT.FieldByName('TENVT').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.ItmNCCClick(Sender: TObject);
var
    n: Integer;
    REP_NAME: String;
begin
    n := (Sender as TComponent).Tag;
    if n = 1 then
        REP_NAME := FORM_CODE + '_DT'
    else
        REP_NAME := FORM_CODE + '_NN';

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdPrintExecute(Sender: TObject);
begin
   BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bInsert, b: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bInsert := State in [dsInsert];
	end;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdEdit.Enabled := bBrowse and mCanEdit and (not bEmpty);
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty);
    CmdReRead.Enabled := bBrowse;

    PD0.Enabled := bInsert;

    if FrmScan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and (not bEmpty);
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and (not bEmpty) and (not FrmScan.Visible);
        CmdFilter.Enabled := not FrmScan.Visible;
        CmdSearch.Enabled := not FrmScan.Visible;
    end;

    CmdSearch.Enabled := CmdSearch.Enabled and bBrowse;
    CmdFilter.Enabled := CmdFilter.Enabled and bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdChangeGroup.Enabled := mCanEdit and bBrowse and (not bEmpty);

    CmdDmvtCt.Enabled := bBrowse and (not bEmpty);
    CmdDmvtFile.Enabled := bBrowse and (not bEmpty);
    ItemKythuat.Enabled := bBrowse and (not bEmpty);
    ItemKythuat1.Enabled := bBrowse and (not bEmpty);
    ItemPhukien.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4, s1 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) or
        (EdSearch.Text <> mSearch) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mNhom2 := sNhom2;
        mNhom3 := sNhom3;
        mNhom4 := sNhom4;
        mSearch := EdSearch.Text;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''', [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSql := sSql + ' and (' +
                            'dbo.fnStripToneMark([MAVT]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([TENVT]) like ''%' + s1 + '%'''
                            + ')';
            end;

            SQL.Text := sSQL;
            Open;
            First;
        end;

//        with Filter do
//        begin
//            RefreshOriginalSQL; //NTD
//            ApplyFilter;
//        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
		FieldByName('MANHOM2').AsString := mNhom2;
		FieldByName('MANHOM3').AsString := mNhom3;
		FieldByName('MANHOM4').AsString := mNhom4;
        FieldByName('LOAITHUE').AsString := defThue;
        FieldByName('QD1').AsInteger := 1;
        FieldByName('DVT').AsInteger := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
		FieldByName('MAVT').AsString := DataMain.AllocEAN(mPrefix, '');
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';

procedure TFrmTS_Dmvt.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM','TENVT', 'DVT', 'MADT', 'LOAITHUE']) then
			Abort;


        if FieldByName('DVT1').AsString = '' then
        begin
           FieldByName('DVT1').AsString := FieldByName('DVT').AsString ;
           FieldByName('QD1').Value := 1;
        end;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MAVT').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;

	    SetNull(DataSet, ['MAMAU' , 'MASIZE', 'MANHOM2', 'MANHOM3', 'MANHOM4']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTGIANHAPChange(Sender: TField);
var
	mGiavon, mGianhap, mGianhapvat, mTL_CK_NCC: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDmvt do
    begin
        mGiavon := FieldByName('GIAVON').AsFloat;
        mTL_CK_NCC := FieldByName('TL_CK_NCC').AsFloat;
		mGianhap := FieldByName('GIANHAP').AsFloat;
        mGianhapvat := FieldByName('GIANHAPVAT').AsFloat;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := mLoaithue;
        Parameters[2].Value := Sender.FieldName;
    	Parameters[3].Value := mGianhap;
        Parameters[4].Value := mGianhapvat;
        Parameters[5].Value := mTL_CK_NCC;
        Parameters[6].Value := mGiavon;
        Execute;
    	mGianhap := Parameters[3].Value;
        mGianhapvat := Parameters[4].Value;
        mTL_CK_NCC := Parameters[5].Value;
        mGiavon := Parameters[6].Value;
    end;

    mTrigger := True;
	with QrDmvt do
    begin
        FieldByName('GIANHAP').AsFloat := mGianhap;
        FieldByName('GIANHAPVAT').AsFloat := mGianhapvat;

        FieldByName('GIAVON').AsFloat := mGiavon;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdPhotoExecute(Sender: TObject);
begin
// 	Application.CreateForm(TFrmScan, FrmScan);
//	FrmScan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmTS_Dmvt.QrDMVTTENVTChange(Sender: TField);
var
	b: Boolean;
begin
	with QrDMVT do
    begin
		b := FieldByName('TENTAT').AsString = '';
    	if not b then
        	b := YesNo(RS_NAME_DEF);
		if b then
			FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);

        FieldByName('TENVT_KHONGDAU').AsString := DataMain.StripToneMark(FieldByName('TENVT').AsString)
	end;
end;

procedure TFrmTS_Dmvt.QrDMVTTON_MINChange(Sender: TField);
begin
	with QrDMVT do
    begin
      	if FieldByName('TON_MAX').AsFloat < FieldByName('TON_MIN').AsFloat then
            FieldByName('TON_MAX').AsFloat := FieldByName('TON_MIN').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTMADT2Change(Sender: TField);
begin
    exDotMadt(DataMain.QrDMNSX, Sender);
    EdTENDT2.Text := EdTENDT2.Field.AsString;

    EdMADT2.Text := Sender.AsString;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmTS_Dmvt.QrDMVTMADTChange(Sender: TField);
begin
    exDotMadt(DataMain.QrDMNCC, Sender);
    EdTENDT.Text := EdTENDT.Field.AsString;

    EdMADT.Text := Sender.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmTS_Dmvt.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTMAVTValidate(Sender: TField);
begin
    with QrDMVT do
    if FieldByName('MAVT').OldValue <> null then
        if DataMain.BarcodeIsUsed(FieldByName('MAVT').OldValue, True) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';

procedure TFrmTS_Dmvt.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom, nhom2, nhom3, nhom4: String;
begin
	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
        nhom2 := FieldByName('MANHOM2').AsString;
        nhom3 := FieldByName('MANHOM3').AsString;
        nhom4 := FieldByName('MANHOM4').AsString;
    end;
	Application.CreateForm(TFrmCayNganhNhom, FrmCayNganhNhom);
    b := FrmCayNganhNhom.Execute(nganh, nhom, nhom2, nhom3, nhom4);
    FrmCayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;
            FieldByName('MANHOM2').AsString  := nhom2;
            FieldByName('MANHOM3').AsString  := nhom3;
            FieldByName('MANHOM4').AsString  := nhom4;

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdExport2Execute(Sender: TObject);
var
	s, sNganh, sNhom: String;
    n, fLevel: Integer;
begin
    with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
        end;

    // Export to file
    s := sysTempPath + 'download.txt';

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	Prepared := True;
    	Open;
        TextExport(DMVT_EXPORT, s);
        Close;
    end;

	Cursor := crDefault;
	if not YesNo('Thiết bị phải đang ở tình trạng nhận danh mục. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'ConfigZ.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Dn.exe';
        // [filename],[COM port],[Baud rate],[Interface],[Show dialog]
        Parameters := s + ',' + IntToStr(n) + ',1,2,0';
        Launch;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdExportExecute(Sender: TObject);
var
	s: String;
    sNganh, sNhom, sNhom2, sNhom3, sNhom4: String;
    fLevel: Integer;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
        with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT2 do
    begin
        Parameters[1].Value := 0;  // Bo = 0
        Parameters[2].Value := sNganh;
        Parameters[3].Value := sNhom;
        Parameters[4].Value := sNhom2;
        Parameters[5].Value := sNhom3;
        Parameters[6].Value := sNhom4;
        Parameters[7].Value := fLevel;

    	ExecProc;
        Active := True;
//        TextExport(DMVT_EXPORT2, s, TEncoding.UTF8);
        TextExport(DMVT_EXPORT2, s);
        Active := False
    end;
	Cursor := crDefault;

    MsgDone();

        {
	s := vlGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	ExecProc;
        Active := True;
        TextExport(DMVT_EXPORT, s);
        Active := False
    end;
	Cursor := crDefault;
    }
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.cmbNhomBeforeDropDown(Sender: TObject);
    var  s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdImportExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
    rs: Integer;
begin
//	sFile := isGetOpenFileName('XLS');
//    if sFile = '' then
//        Exit;
//
//    rs := YesNoCancel(RS_IM_EXCEL_YESNO);
//    if rs = mrCancel then
//        Exit;
//    b := rs = mrYes;
//
//    DataExcel.ExcelImport('IMP_' + FORM_CODE, sFile, 'IMP_DM_HH',
//        'spIMPORT_DM_HH;1', 'MAVT', [sysLogonUID, mPrefix, b], 0);
//
//    CmdReRead.Execute;
//    mRet := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdTimBarcode2Execute(Sender: TObject);
begin
    Application.CreateForm(TFrmTimBarcode2, FrmTimBarcode2);
    FrmTimBarcode2.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdTimBarcodeExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmTimBarcode, FrmTimBarcode);
    FrmTimBarcode.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.CmdUpdateTinhtrangExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
begin
//	sFile := isGetOpenFileName('XLSX;XLS');
//    if sFile = '' then
//        Exit;
//
//    DataExcel.ExcelImport('IMP_' + FORM_CODE + '_UPDATE', sFile, 'IMP_DM_HH_UPDATE',
//        'spUPDATE_DM_HH;1', 'MAVT', [sysLogonUID], 0);
//    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTCalcFields(DataSet: TDataSet);
var
	Qd: Double;
begin
	with QrDMVT do
    begin
    	Qd := FieldByName('QD1').AsFloat;

    	if Qd <> 0 then
        begin
    		FieldByName('CAL_TONMIN_BOX').AsFloat :=
        		RoundUp(FieldByName('TON_MIN').AsFloat * Qd);

    		FieldByName('CAL_TONMAX_BOX').AsFloat :=
        		RoundUp(FieldByName('TON_MAX').AsFloat * Qd);
        end
        else
        begin
        	FieldByName('CAL_TONMIN_BOX').Clear;
            FieldByName('CAL_TONMAX_BOX').Clear
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_Dmvt.QrDMVTDVTChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if FieldByName('DVT1').AsString = '' then
            FieldByName('DVT1').AsString := FieldByName('DVT').AsString;
    end;
end;

procedure TFrmTS_Dmvt.QrDMVTMANGANHChange(Sender: TField);
begin
    QrDMVT.FieldByName('MANHOM').AsString := '';
end;

end.
