﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TS_DactinhKT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db, DBCtrls, ComCtrls, ActnList, ImgList, ToolWin, wwriched,
  Vcl.Mask, wwdbedit, Vcl.ExtCtrls, isPanel;

type
  TFrmTS_DactinhKT = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    ToolbarImages: TImageList;
    StandardToolBar: TToolBar;
    CutButton: TToolButton;
    CopyButton: TToolButton;
    PasteButton: TToolButton;
    UndoButton: TToolButton;
    ToolButton10: TToolButton;
    FontName: TComboBox;
    ToolButton2: TToolButton;
    BoldButton: TToolButton;
    ItalicButton: TToolButton;
    UnderlineButton: TToolButton;
    ToolButton16: TToolButton;
    LeftAlign: TToolButton;
    CenterAlign: TToolButton;
    RightAlign: TToolButton;
    ToolButton20: TToolButton;
    BulletsButton: TToolButton;
    CmdCut: TAction;
    CmdCopy: TAction;
    CmdPaste: TAction;
    CmdUndo: TAction;
    CmdBold: TAction;
    CmdItalic: TAction;
    CmdUnderline: TAction;
    CmdLeft: TAction;
    CmdCenter: TAction;
    CmdRight: TAction;
    CmdBullets: TAction;
    FontSize: TComboBox;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    CmdFont: TAction;
    FontDialog: TFontDialog;
    ColorDialog: TColorDialog;
    ToolButton4: TToolButton;
    CmdColor: TAction;
    EdDactinh: TwwDBRichEdit;
    PD2: TisPanel;
    Label18: TLabel;
    Ed1: TwwDBEdit;
    Label1: TLabel;
    Ed2: TwwDBEdit;
    CmdRunURL: TAction;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReturnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TntFormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnCancelClick(Sender: TObject);
    procedure CmdCutExecute(Sender: TObject);
    procedure CmdCopyExecute(Sender: TObject);
    procedure CmdPasteExecute(Sender: TObject);
    procedure CmdUndoExecute(Sender: TObject);
    procedure CmdBoldExecute(Sender: TObject);
    procedure CmdItalicExecute(Sender: TObject);
    procedure CmdUnderlineExecute(Sender: TObject);
    procedure CmdLeftExecute(Sender: TObject);
    procedure CmdCenterExecute(Sender: TObject);
    procedure CmdRightExecute(Sender: TObject);
    procedure CmdBulletsExecute(Sender: TObject);
    procedure FontNameClick(Sender: TObject);
    procedure CmdFontExecute(Sender: TObject);
    procedure EdDactinhSelectionChange(Sender: TObject);
    procedure CmdColorExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FontSizeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRunURLExecute(Sender: TObject);
  private
	mDataSource: TDataSource;
    mCanEdit: Boolean;
    mLoai: Integer;
    IDSession: LongInt;

    procedure SelectionChange;
    function CurrText: TTextAttributes;
  public
  	procedure Execute(
    	r : WORD;
    	DataSource: TDataSource;
    	loai: Integer);				// 0: Viet 1: English
  end;

var
  FrmTS_DactinhKT: TFrmTS_DactinhKT;

implementation

{$R *.DFM}

uses
	 isDb, Rights, islib, ExCommon, ShellAPI;

(*==============================================================================
** Đoạn văn bản được chọn
**------------------------------------------------------------------------------
*)
function TFrmTS_DactinhKT.CurrText: TTextAttributes;
begin
     Result := EdDactinh.SelAttributes;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.SelectionChange;
begin
    with EdDactinh.Paragraph do
    try
        BoldButton.Down := fsBold in CurrText.Style;
        ItalicButton.Down := fsItalic in CurrText.Style;
        UnderlineButton.Down := fsUnderline in CurrText.Style;
        BulletsButton.Down := Boolean(Numbering);
        FontSize.Text := IntToStr(CurrText.Size);
        FontName.Text := CurrText.Name;
        case Ord(Alignment) of
            0: LeftAlign.Down := True;
            1: RightAlign.Down := True;
            2: CenterAlign.Down := True;
        end;
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.EdDactinhSelectionChange(Sender: TObject);
begin
	EdDactinh.DataSource.DataSet.Edit;
    SelectionChange;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.Execute;
begin
	mDataSource := DataSource;
    EdDactinh.DataSource := mDataSource;
    mLoai := loai;
	if loai = 0 then
    begin
    	Caption := Caption + ' - Việt';
        EdDactinh.DataField := 'DACTINH_KT';
    end
    else
    begin
    	Caption := Caption + ' - Anh';
        EdDactinh.DataField := 'DACTINH_KT_TA';
    end;

    Ed1.DataSource := mDataSource;
    Ed2.DataSource := mDataSource;
    Ed1.DataField := 'URL';
    Ed2.DataField := 'URL2';

    mCanEdit := rCanEdit(r);
    DataSource.AutoEdit := mCanEdit;
    EdDactinh.ReadOnly := not mCanEdit;
    StandardToolBar.Enabled := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.FontNameClick(Sender: TObject);
begin
    CurrText.Name := FontName.Items[FontName.ItemIndex];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.FontSizeChange(Sender: TObject);
begin
    CurrText.Size := StrToInt(FontSize.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
    FontName.Items.Assign(Screen.Fonts);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.FormKeyPress(Sender: TObject; var Key: Char);
begin
	//Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.FormShow(Sender: TObject);
begin
    SelectionChange;
    EdDactinh.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(mDataSource.DataSet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.TntFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdBoldExecute(Sender: TObject);
begin
    if BoldButton.Down then
        CurrText.Style := CurrText.Style + [fsBold]
    else
        CurrText.Style := CurrText.Style - [fsBold];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdBulletsExecute(Sender: TObject);
begin
    EdDactinh.Paragraph.Numbering := TNumberingStyle(BulletsButton.Down);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdCenterExecute(Sender: TObject);
begin
    EdDactinh.Paragraph.Alignment := TAlignment(2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdColorExecute(Sender: TObject);
begin
    if ColorDialog.Execute then
        CurrText.Color := ColorDialog.Color;
    EdDactinhSelectionChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdCopyExecute(Sender: TObject);
begin
    EdDactinh.CopyToClipboard;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdCutExecute(Sender: TObject);
begin
    EdDactinh.CutToClipboard;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdFontExecute(Sender: TObject);
begin
    FontDialog.Font.Assign(CurrText);
    if FontDialog.Execute then
        CurrText.Assign(FontDialog.Font);
    EdDactinhSelectionChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdItalicExecute(Sender: TObject);
begin
    if ItalicButton.Down then
        CurrText.Style := CurrText.Style + [fsItalic]
    else
        CurrText.Style := CurrText.Style - [fsItalic];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdLeftExecute(Sender: TObject);
begin
    EdDactinh.Paragraph.Alignment := TAlignment(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdPasteExecute(Sender: TObject);
begin
    EdDactinh.PasteFromClipboard;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdReturnClick(Sender: TObject);
var
    sHtml: string;
begin
    SetEditState(mDataSource.DataSet);
//
//    with mDataSource.DataSet do
//        if EdDactinh.Text = '' then
//        begin
//            EdDactinh.Field.Clear;
//            if mloai = 0 then
//                FieldByName('HTML_DACTINH_KT_VN').Clear;
//
//            if mloai = 1 then
//                FieldByName('HTML_DACTINH_KT_EN').Clear;
//        end
//        else
//        begin
//            IDSession := exHrsNewSession();
//
//            if mloai = 0 then
//                if exRtf2Html(IDSession, FieldByName('DACTINH_KT_VN').AsString, sHtml) then
//                    FieldByName('HTML_DACTINH_KT_VN').Value := sHtml;
//
//            if mloai = 1 then
//                if exRtf2Html(IDSession, FieldByName('DACTINH_KT_EN').AsString, sHtml) then
//                    FieldByName('HTML_DACTINH_KT_EN').Value := sHtml;
//
//            exHrsEndSession(IDSession);
//        end;

    mDataSource.DataSet.CheckBrowseMode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdRightExecute(Sender: TObject);
begin
    EdDactinh.Paragraph.Alignment := TAlignment(1);
end;

procedure TFrmTS_DactinhKT.CmdRunURLExecute(Sender: TObject);
var
    n: Integer;
    s: String;
begin
    n := (Sender as TComponent).Tag;
    with mDataSource.DataSet do
    begin
        if n = 1  then
            s := FieldByName('URL2').AsString
        else
            s := FieldByName('URL').AsString
    end;

    ShellExecute(0, Nil, PWideChar(s), Nil, Nil, SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdUnderlineExecute(Sender: TObject);
begin
    if UnderlineButton.Down then
        CurrText.Style := CurrText.Style + [fsUnderline]
    else
        CurrText.Style := CurrText.Style - [fsUnderline];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.CmdUndoExecute(Sender: TObject);
begin
    with EdDactinh do
        if HandleAllocated then
            SendMessage(Handle, EM_UNDO, 0, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdCut.Enabled := EdDactinh.SelLength > 0;
    CmdCopy.Enabled := CmdCut.Enabled;
    if EdDactinh.HandleAllocated then
    begin
        CmdUndo.Enabled := EdDactinh.Perform(EM_CANUNDO, 0, 0) <> 0;
        // CmdPaste.Enabled := EdDactinh.Perform(EM_CANPASTE, 0, 0) <> 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTS_DactinhKT.BtnCancelClick(Sender: TObject);
begin
    mDataSource.DataSet.Cancel;
end;

end.
