object FB_frScanCode: TFB_frScanCode
  Left = 0
  Top = 0
  Width = 800
  Height = 70
  Color = 16119285
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object PaBarcode: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 70
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      800
      70)
    object LbCode: TLabel
      Left = 128
      Top = 27
      Width = 77
      Height = 19
      Alignment = taRightJustify
      Caption = '&M'#195' V'#7840'CH'
      FocusControl = EdCode
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Image4: TImage
      Left = 466
      Top = 17
      Width = 32
      Height = 32
      Anchors = [akLeft, akBottom]
      AutoSize = True
    end
    object HTMLabel4: THTMLabel
      Left = 526
      Top = 23
      Width = 129
      Height = 38
      Anchors = [akLeft, akBottom]
      AutoSizing = True
      HTMLText.Strings = (
        
          '<FONT face="Tahoma" size="10""><B>F2</B> - NH'#7852'P M'#195' V'#7840'CH<br/><B>F' +
          '3</B> - NH'#7852'P S'#7888' L'#431#7906'NG</FONT>')
      Transparent = True
      Version = '1.9.0.1'
    end
    object EdCode: TwwDBEdit
      Left = 215
      Top = 17
      Width = 237
      Height = 35
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      Color = 15794175
      ParentBiDiMode = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -24
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      OnEnter = EdCodeEnter
      OnExit = EdCodeExit
      OnKeyPress = EdCodeKeyPress
    end
    object chkCheckExists: TCheckBox
      Left = 688
      Top = 28
      Width = 97
      Height = 17
      TabStop = False
      Caption = ' &Ki'#7875'm tra'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
    end
  end
  object MyActionList: TActionList
    Left = 252
    Top = 8
    object CmdScanQty: TAction
      Caption = 'Nh'#7853'p s'#7889' l'#432#7907'ng'
      ShortCut = 114
      OnExecute = CmdScanQtyExecute
    end
    object CmdScanCode: TAction
      Caption = 'Nh'#7853'p barcode'
      ShortCut = 113
      OnExecute = CmdScanCodeExecute
    end
  end
end
