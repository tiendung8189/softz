﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_CheckTonkho;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  ActnList, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmFB_CheckTonkho = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    ActionList1: TActionList;
    CmdClose: TAction;
    LbDONVI: TLabel;
    Label31: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    CHECK_TONKHO: TADOStoredProc;
    CHUNGTU_CT: TDataSource;
    LbMA: TLabel;
    LbTEN: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    LbMa2: TLabel;
    LbTen2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
	mMABH, mMakho: String;
    mKHOA: TGUID;
    mDenngay: TDateTime;

  public
  	function Execute(pMABH, pTENVT, pMAKHO, pTENKHO: String; pNGAY: TDateTime; pKhoa: TGUID): Boolean;
  end;

var
  FrmFB_CheckTonkho: TFrmFB_CheckTonkho;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_CheckTonkho.Execute;
begin
    if pMABH = '' then
    begin
        Result := False;
        Close;
        Exit;
    end;

	mMABH := pMABH;
    mMakho := pMAKHO;
    mKHOA := pKhoa;
    mDenngay := pNGAY;

    LbMA.Caption := pMABH;
    LbTEN.Caption := pTENVT;
    LbMA2.Caption := pMAKHO;
    LbTEN2.Caption := pTENKHO;

    SetCustomGrid('FB_CHECK_TONKHO' , GrBrowse);
    Result := ShowModal = mrOK;

    CloseDataSets([CHECK_TONKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.FormShow(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.GrBrowseDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.CmdRefreshExecute(Sender: TObject);
begin
   	begin
        with CHECK_TONKHO do
        begin
			Close;
            Prepared := True;
            Parameters[1].Value := mMABH;
            Parameters[2].Value := mMakho;
            Parameters[3].Value := mDenngay;
            Parameters[4].Value := TGuidEx.ToString(mKHOA);
            ExecProc;

            Open;
        end;

//        SetShortDateFormat(CHECK_TONKHO);
        SetDisplayFormat(CHECK_TONKHO, sysCurFmt);
//        SetDisplayFormat(CHECK_TONKHO, ['NGAY'], DateTimeFmt);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheckTonkho.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;


end.
