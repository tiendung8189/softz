﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Dmhh;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons,
  AdvEdit, isDb, fcTreeHeader, Variants, Messages, Wwdotdot, Wwdbcomb,
  wwcheckbox, DBGridEh, DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh;


type
  TFrmFB_Dmhh = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    ItmNHOM: TMenuItem;
    N2: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopPrint: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    CmdExport: TAction;
    BtExport: TToolButton;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    EdTen: TwwDBEdit;
    PD2: TisPanel;
    PD7: TisPanel;
    EdGHICHU: TDBMemo;
    PD8: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    CmdSwitch: TAction;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    CmdAudit: TAction;
    N3: TMenuItem;
    CmdImportExcel: TAction;
    BtnImport: TToolButton;
    ToolButton13: TToolButton;
    RzLauncher: TRzLauncher;
    PopExport: TAdvPopupMenu;
    Export1: TMenuItem;
    CmdDmdvt: TAction;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang2: TMenuItem;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    QrDMVTGIANHAP_CHUA_VAT: TFloatField;
    QrDMVTXUATXU: TWideStringField;
    QrDMVTMA_PREFIX: TWideStringField;
    QrDMVTMA: TWideStringField;
    Label14: TLabel;
    EdMA: TwwDBEdit;
    QrDMVTMAVT_NCC: TWideStringField;
    QrDMVTMADT: TWideStringField;
    PopImport: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    PD4: TisPanel;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    Label8: TLabel;
    QrDMVTDINHMUC_CHUA_HAOHUT: TFloatField;
    QrDMVTTHANHTIEN_DINHMUC: TFloatField;
    QrDMVTTHANHTIEN_CHIPHI: TFloatField;
    wwDBEdit7: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    wwDBEdit9: TwwDBEdit;
    Label9: TLabel;
    QrNhom: TADOQuery;
    QrNganh: TADOQuery;
    PD0: TisPanel;
    PD3: TisPanel;
    Label11: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label15: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label17: TLabel;
    wwDBEdit10: TwwDBEdit;
    QrDMVTGIABAN_CHUA_VAT: TFloatField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTTENVT_TA: TWideStringField;
    PD5: TisPanel;
    Label21: TLabel;
    Label23: TLabel;
    wwDBEdit12: TwwDBEdit;
    wwDBEdit14: TwwDBEdit;
    Label4: TLabel;
    wwDBEdit3: TwwDBEdit;
    QrKho: TADOQuery;
    QrDMVTMAKHO: TWideStringField;
    N1: TMenuItem;
    ingnhnhm1: TMenuItem;
    CmdChangeGroup: TAction;
    QrDMVTSOTIEN_DINHMUC_HAOHUT: TFloatField;
    Label16: TLabel;
    wwDBEdit5: TwwDBEdit;
    Label19: TLabel;
    QrDMVTTL_LAI: TFloatField;
    LbDinhMucChiPhi: TLabel;
    Label20: TLabel;
    wwDBEdit8: TwwDBEdit;
    Label22: TLabel;
    QrDMVTTL_LAI_GT: TFloatField;
    LbX: TLabel;
    LbQD: TLabel;
    CmdImportExcelTemplate: TAction;
    N4: TMenuItem;
    ivfileExcelmu1: TMenuItem;
    Label25: TLabel;
    Label26: TLabel;
    EditThoiGianCheBien: TwwDBEdit;
    QrPhanLoai: TADOQuery;
    QrDMVTPHANLOAI: TWideStringField;
    QrDMVTTHOIGIAN_CHEBIEN: TIntegerField;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    CmdImage: TAction;
    N5: TMenuItem;
    PopMainItemImg: TMenuItem;
    QrDMVTHIENTHI_ANHDAIDIEN: TBooleanField;
    Label27: TLabel;
    Label28: TLabel;
    EdMA_KYTU: TwwDBEdit;
    EdMA_PREFIX: TwwDBEdit;
    QrDMVTMA_KYTU: TWideStringField;
    QrDMVTCHECKED_DINHMUC: TBooleanField;
    QrDMVTCHECKED_CHIPHI: TBooleanField;
    QrDMVTLK_PREFIX: TStringField;
    Label29: TLabel;
    wwDBEdit11: TwwDBEdit;
    QrCamUng: TADOQuery;
    QrDMVTCAMUNG_NHOM: TWideStringField;
    QrHINH: TADOQuery;
    CmdPhoto: TAction;
    checkDinhMuc: TwwCheckBox;
    checkChiPhi: TwwCheckBox;
    QrDMVTMaDvt: TIntegerField;
    QrDMVTMaDvtLon: TIntegerField;
    QrDMVTQuyDoi: TIntegerField;
    DBNumberEditEh1: TDBNumberEditEh;
    cbbLOAITHUE: TDbLookupComboboxEh2;
    CbbMANGANH: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    CbMANHOM: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    DsNganh: TDataSource;
    DsNhom: TDataSource;
    DsCamUng: TDataSource;
    DsPhanLoai: TDataSource;
    cbbCAMUNG_NHOM: TDbLookupComboboxEh2;
    cbbTINHTRANG: TDbLookupComboboxEh2;
    CbbQuayCheBien: TDbLookupComboboxEh2;
    DsKho: TDataSource;
    cbbPHANLOAI: TDbLookupComboboxEh2;
    CbDVT: TDbLookupComboboxEh2;
    CbQuyDoi: TDBNumberEditEh;
    cbbMaDvtLon: TDbLookupComboboxEh2;
    QrDMVTLK_TINHTRANG_MA: TWideStringField;
    CmdShow: TAction;
    QrDMVTLK_Dvt: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDMVTDVTChange(Sender: TField);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTMAVTValidate(Sender: TField);
    procedure QrDMVTMAChange(Sender: TField);
    procedure CmdNewExecute(Sender: TObject);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure cmbNhomBeforeDropDown(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure LbDinhMucChiPhiClick(Sender: TObject);
    procedure CmdImportExcelTemplateExecute(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure QrDMVTPHANLOAIChange(Sender: TField);
    procedure CmdImageExecute(Sender: TObject);
    procedure QrKhoBeforeOpen(DataSet: TDataSet);
    procedure QrDMVTMA_KYTUChange(Sender: TField);
    procedure QrDMVTMANHOMChange(Sender: TField);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure CmdShowExecute(Sender: TObject);
    procedure QrDMVTMaDvtChange(Sender: TField);
  private
  	mCanEdit, mEANConfirm, mRet, bUnitLevel: Boolean;
    mChecked, defDvt : Integer;

    defTinhtrang, defTinhchat, defThue: String;
  	mSQL, mNganh, mNhom, mPrefix, mSearch, mPhanLoai: String;
    defTinhTrangFilter, defTinhTrangDangDung: String;

    mLoaiNganh: String;
    mGrid: TwwDBGrid2;
    mQuery: TADOQuery;
    mDs: TDataSource;
    function EnabledElement(sPhanLoai: string): Boolean;

  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmFB_Dmhh: TFrmFB_Dmhh;

implementation

uses
	ExCommon, isMsg, isStr, Rights, MainData, RepEngine,
    CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
  TimBarcode2, Dmkhac, DmvtCT, OfficeData, exThread, FB_CayNganhNhom, FB_Dmhh_Dinhmuc,
  ShellAPI, FB_Dmhh_File, FB_Scan, DmHotro;


{$R *.DFM}

const
	  FORM_CODE = 'FB_DM_HH';
    FORM_CODE2 = 'FB_DM_HH_DINHMUC';
    FORM_CODE3 = 'FB_DM_HH_CHIPHI';

    TABLE_NAME  = 'FB_DM_HH';
    TABLE_NAME2  = 'FB_DM_HH_DINHMUC';
    TABLE_NAME3  = 'FB_DM_HH_CHIPHI';

    REPORT_NAME = FORM_CODE;
    EXPORT_EXCEL = 'FB_RP_DM_HH_EXCEL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmFB_Dmhh.Execute;
begin
    mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.FormCreate(Sender: TObject);
var
    x, n: Integer;
begin
    TMyForm(Self).Init2;
    defTinhTrangDangDung := '01';
    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
	    SetDisplayFormat(QrDMVT, ['TL_LAI', 'TL_LAI_GT'], sysPerFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
    	SetShortDateFormat(QrDMVT);
    end;

    mPhanLoai := GetSysParam('FB_PHANLOAI');

    // Tuy bien luoi
    SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary([QrDMVT], [FORM_CODE], [Filter]);

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE, 'EAN Confirm');

    // Don vi tinh quy doi
	bUnitLevel := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;

    LbX.Visible := bUnitLevel;
    LbQD.Visible := bUnitLevel;
    CbQuyDoi.Visible := bUnitLevel;
    cbbMaDvtLon.Visible := bUnitLevel;


    // Default unit
	defDvt := GetSysParam('DEFAULT_DVT');
    defThue := GetSysParam('DEFAULT_LOAITHUE');

    PD3.Visible := FlexConfigInteger(FORM_CODE, 'Standard Cost') <> 0;

    // Initial
    mTrigger := False;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmFB_Scan := Nil;

    mLoaiNganh := 'FBTP';
    // Tree Combo
    FlexGroupCombo(CbNhomhang, mLoaiNganh);

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PD3.Collapsed := RegReadBool('PD3');
    PD4.Collapsed := RegReadBool('PD4');
    PD5.Collapsed := RegReadBool('PD5');
    PD7.Collapsed := RegReadBool('PD7');
    PD8.Collapsed := RegReadBool('PD8');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.FormShow(Sender: TObject);
var
    it: TMenuItem;
    i: Integer;
begin
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrDMLOAITHUE, QrDM_CHIPHI,
        QrFB_DM_NPL, QrV_FB_DMVT_TINHTRANG]);
    end;
    OpenDataSets([QrNganh, QrNhom, QrKho, QrPhanLoai, QrCamUng]);

    with DataMain.QrV_FB_DMVT_TINHTRANG do
    begin
        if Locate('MA', defTinhTrangDangDung, []) then
        begin
            defTinhtrang := FieldByName('MA_HOTRO').AsString;
            defTinhTrangFilter := FieldByName('MA_HOTRO').AsString;
        end;
    end;

    with DataMain.QrV_FB_DMVT_TINHTRANG do
    if not IsEmpty then
    begin
         First;
         i := 0;
         while not Eof do
         begin
             i := i + 1;
             it := TMenuItem.Create(Self);
             with it do
             begin
                Caption := FieldByName('TEN_HOTRO').AsString;
                Tag := i;
                RadioItem := true;
                Hint := FieldByName('MA_HOTRO').AsString;
                OnClick := CmdShowExecute;
                Checked := (defTinhTrangFilter = FieldByName('MA_HOTRO').AsString);
                GroupIndex := 2;
             end;
             PopMain.Items.Insert((4 + i), it);
             Next;
         end;
    end;

    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('LK_TINHTRANG_MA').AsString <> defTinhTrangDangDung then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.LbDinhMucChiPhiClick(Sender: TObject);
var
    pLoai: Integer;
    bDinhMuc, bChiPhi: Boolean;
begin

    with QrDMVT do
    begin
        if FieldByName('CHECKED_DINHMUC').AsBoolean
        and FieldByName('CHECKED_CHIPHI').AsBoolean then
            pLoai := 0
        else if FieldByName('CHECKED_DINHMUC').AsBoolean then
            pLoai := 1
        else  if FieldByName('CHECKED_CHIPHI').AsBoolean then
            pLoai := 2;

        Application.CreateForm(TFrmFB_Dmhh_Dinhmuc, FrmFB_Dmhh_Dinhmuc);
        if FrmFB_Dmhh_Dinhmuc.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString, pLoai) then
        begin

            exReSyncRecord(QrDMVT);
            SetEditState(QrDMVT);

            CloseDataSets([FrmFB_Dmhh_Dinhmuc.QrDMDinhMuc, FrmFB_Dmhh_Dinhmuc.QrDMChiPhi]);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmFB_Scan <> Nil then
    	FrmFB_Scan.Close;

    RegWrite(Name, ['PD2', 'PD3', 'PD4', 'PD5', 'PD7', 'PD8'],
    	[PD2.Collapsed, PD3.Collapsed, PD4.Collapsed, PD5.Collapsed, PD7.Collapsed, PD8.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdSaveExecute(Sender: TObject);
begin
	QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdCancelExecute(Sender: TObject);
begin
    QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';
procedure TFrmFB_Dmhh.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom: String;
    r: WORD;
begin
    r := GetRights('FB_DM_HH_DOINGANHNHOM');
    if r = R_DENY then
        Exit;

	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
    end;
	Application.CreateForm(TFrmFB_CayNganhNhom, FrmFB_CayNganhNhom);
    b := FrmFB_CayNganhNhom.Execute(nganh, nhom);
    FrmFB_CayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;


            with TADOQuery.Create(Nil) do
            begin
                Connection  := DataMain.Conn;
                LockType    := ltReadOnly;
                SQL.Text := Format('select PREFIX from DM_NHOM where MANGANH in (select MANGANH from DM_NGANH where LOAI = ''FBTP'' ) and MANHOM=''%s''', [nhom]);
                Open;
                QrDMVT.FieldByName('MA_PREFIX').AsString := FieldByName('PREFIX').AsString;
                Close;
            end;
            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdDelExecute(Sender: TObject);
begin
    if DataMain.FB_BarcodeIsUsed(QrDMVT.FieldByName('MAVT').AsString) then
        Abort;
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_PUB_DM_KHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdPhotoExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFB_Scan, FrmFB_Scan);
    FrmFB_Scan.Left := Width -  FrmFB_Scan.Width;
    FrmFB_Scan.Top := PD1.Top - 8;
    FrmFB_Scan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdPrintExecute(Sender: TObject);
var
    REP_NAME: String;
begin
    REP_NAME := REPORT_NAME;

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdShowExecute(Sender: TObject);
var
    n: Integer;
begin

    n := (Sender as TComponent).Tag;
    if n = -1 then
    begin
        defTinhTrangFilter := '';
        PopTinhtrang2.Checked := True;
    end
    else
    begin
        defTinhTrangFilter := (Sender as TMenuItem).Hint;
        (Sender as TMenuItem).Checked := True;
    end;
    mNganh := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, b, bInsert: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
        bInsert := State in [dsInsert];
	end;

    GrList.Enabled := bBrowse;
    CmdNew.Enabled := bBrowse and mCanEdit and sysIsCentral;
    CmdEdit.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    BtnImport.Enabled := bBrowse and mCanEdit;
    BtExport.Enabled := bBrowse;
    BtnIn.Enabled := bBrowse;
    CmdImage.Enabled := bBrowse;

    if FrmFB_Scan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and bBrowse;
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and bBrowse and (not FrmFB_Scan.Visible);
        CmdFilter.Enabled := not FrmFB_Scan.Visible;
        CmdSearch.Enabled := not FrmFB_Scan.Visible;
    end;

    LbDinhMucChiPhi.Enabled :=  not bInsert;
    PD0.Enabled := bInsert;

    checkDinhMuc.Enabled := QrDMVT.FieldByName('DINHMUC_CHUA_HAOHUT').AsFloat = 0;
    checkChiPhi.Enabled := QrDMVT.FieldByName('THANHTIEN_CHIPHI').AsFloat = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, s1 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (EdSearch.Text <> mSearch) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mSearch := EdSearch.Text;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            end;

            if (defTinhTrangFilter <> '')   then
            begin
                sSql := sSql + Format(' and TINHTRANG in (''%s'')', [defTinhTrangFilter]);
            end;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSql := sSql + ' and (' +
                            'dbo.fnStripToneMark([MAVT]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([TENVT]) like ''%' + s1 + '%'''
                            + ')';
            end;

            sSQL := sSQL + ' order by	MANGANH, MANHOM';
            SQL.Text := sSQL;
            Open;
            First;
        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
        FieldByName('LOAITHUE').AsString := defThue;
        FieldByName('QuyDoi').AsInteger := 1;
        FieldByName('MaDvt').AsInteger := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
        FieldByName('XUATXU').AsString := '084';
//        FieldByName('MA_PREFIX').AsString := mPrefix;
        FieldByName('HIENTHI_ANHDAIDIEN').AsBoolean := False;
        FieldByName('CHECKED_DINHMUC').AsBoolean := False;
        FieldByName('CHECKED_CHIPHI').AsBoolean := False;
        QrDMVTMANHOMChange(Nil);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';
    RS_INVALID_PROCESSING_TIME = 'Thời gian chế biến không hợp lệ.';
    RS_INVALID_QD = 'Quy đổi không hợp lệ';
    RS_INVALID_UNIT = 'Đơn vị tính không hợp lệ';
    RS_INVALID_DINHMUC_CHIPHI = 'Định mức hoặc chi phí chưa được chọn.';
procedure TFrmFB_Dmhh.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM','MA', 'TENVT', 'MaDvt', 'LOAITHUE', 'PHANLOAI']) then
			Abort;


        if (FieldByName('PHANLOAI').AsString = mPhanLoai) then
        begin
            if BlankConfirm(DataSet, ['THOIGIAN_CHEBIEN', 'MAKHO']) then
    			Abort;

            if (FieldByName('THOIGIAN_CHEBIEN').AsInteger < 1) then
            begin
                ErrMsg(RS_INVALID_PROCESSING_TIME);
                Abort;
            end;
        end;

        if bUnitLevel then
		begin
            if BlankConfirm(DataSet, ['QuyDoi', 'MaDvtLon']) then
                Abort;

            if (FieldByName('QuyDoi').AsInteger < 1) then
            begin
                ErrMsg(RS_INVALID_QD );
                Abort;
            end;

            if ((FieldByName('QuyDoi').Value = 1) and (FieldByName('MaDvt').AsInteger <> FieldByName('MaDvtLon').AsInteger)) or
                ((FieldByName('QuyDoi').Value > 1) and (FieldByName('MaDvt').AsInteger = FieldByName('MaDvtLon').AsInteger)) then
            begin
                ErrMsg(RS_INVALID_UNIT );
                Abort;
            end;
        end else
        begin
           FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger ;
           FieldByName('QuyDoi').Value := 1;
        end;

	    SetNull(DataSet, ['MADT']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTGIANHAPChange(Sender: TField);
var
	mGiavon, mGianhapchuaVAT, mGianhap, mGiabanchuaVAT, mGiaban, mLaiGiaThanh, mLai: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDMVT do
    begin
        mGiavon := FieldByName('GIAVON').AsFloat;
		mGianhapchuaVAT := FieldByName('GIANHAP_CHUA_VAT').AsFloat;
        mGianhap := FieldByName('GIANHAP').AsFloat;
        mLoaithue := FieldByName('LOAITHUE').AsString;
        mGiabanchuaVAT := FieldByName('GIABAN_CHUA_VAT').AsFloat;
        mGiaban := FieldByName('GIABAN').AsFloat;
        mLaiGiaThanh := FieldByName('TL_LAI_GT').AsFloat;
        mLai := FieldByName('TL_LAI').AsFloat;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := mLoaithue;
        Parameters[2].Value := Sender.FieldName;
    	Parameters[3].Value := mGianhapchuaVAT;
        Parameters[4].Value := mGianhap;
        Parameters[5].Value := mGiavon;
        Parameters[6].Value := mGiabanchuaVAT;
        Parameters[7].Value := mGiaban;
        Parameters[8].Value := mLaiGiaThanh;
        Parameters[9].Value := mLai;
        Execute;
    	mGianhapchuaVAT := Parameters[3].Value;
        mGianhap := Parameters[4].Value;
        mGiavon := Parameters[5].Value;
        mGiabanchuaVAT := Parameters[6].Value;
        mGiaban := Parameters[7].Value;
        mLaiGiaThanh := Parameters[8].Value;
        mLai := Parameters[9].Value;
    end;

    mTrigger := True;
	with QrDMVT do
    begin
        FieldByName('GIANHAP_CHUA_VAT').AsFloat := mGianhapchuaVAT;
        FieldByName('GIANHAP').AsFloat := mGianhap;
        FieldByName('GIABAN_CHUA_VAT').AsFloat := mGiabanchuaVAT;
        FieldByName('GIABAN').AsFloat := mGiaban;
        FieldByName('GIAVON').AsFloat := mGiavon;
        FieldByName('TL_LAI_GT').AsFloat := mLaiGiaThanh;
        FieldByName('TL_LAI').AsFloat := mLai;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmFB_Dmhh.QrDMVTTENVTChange(Sender: TField);
begin
	with QrDMVT do
    begin
        FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);

        FieldByName('TENVT_KHONGDAU').AsString := DataMain.StripToneMark(FieldByName('TENVT').AsString)
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrKhoBeforeOpen(DataSet: TDataSet);
begin
    with QrKho do
        Parameters[0].Value := sysLoc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.BtnImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmFB_Dmhh.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTMAVTValidate(Sender: TField);
begin
    with QrDMVT do
    if FieldByName('MAVT').OldValue <> null then
        if DataMain.BarcodeIsUsed(FieldByName('MAVT').OldValue, True) then
            Abort;
end;

procedure TFrmFB_Dmhh.QrDMVTMA_KYTUChange(Sender: TField);
var
    s, mMa: string;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

    with QrDMVT do
    begin
        FieldByName('MA').AsString := FieldByName('MA_PREFIX').AsString + FieldByName('MA_KYTU').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTPHANLOAIChange(Sender: TField);
begin
   EnabledElement(Sender.AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTMAChange(Sender: TField);
var
    n: Integer;
	mMa, s, mavt: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;

    with QrDMVT do
    begin
        mavt := FieldByName('MA').AsString;
        FieldByName('MAVT').AsString := mavt;
        FieldByName('MAVT_NCC').AsString := mavt;
    end;
end;

procedure TFrmFB_Dmhh.QrDMVTMaDvtChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('MaDvtLon').IsNull) or (FieldByName('MaDvtLon').AsInteger = 0) then
            FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTMANGANHChange(Sender: TField);
begin
    QrDMVT.FieldByName('MANHOM').AsString := '';
end;

procedure TFrmFB_Dmhh.QrDMVTMANHOMChange(Sender: TField);
var
	mMANHOM: String;
begin
    with QrDMVT do
    begin
        mMANHOM := FieldByName('MANHOM').AsString;
        if (FieldByName('MA_PREFIX').AsString = '') and (mMANHOM <> '') then
        begin
            with TADOQuery.Create(Nil) do
            begin
                Connection  := DataMain.Conn;
                LockType    := ltReadOnly;
                SQL.Text := Format('select PREFIX from DM_NHOM where MANGANH in (select MANGANH from DM_NGANH where LOAI = ''FBTP'' ) and MANHOM=''%s''', [mMANHOM]);
                Open;
                QrDMVT.FieldByName('MA_PREFIX').AsString := FieldByName('PREFIX').AsString;
                Close;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
    EnabledElement(QrDMVT.FieldByName('PHANLOAI').AsString);
    QrDMVTMANHOMChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdExportExecute(Sender: TObject);
begin
    DataOffice.CreateReport2('XLSX\' + EXPORT_EXCEL + '.xlsx',
        [sysLogonUID, mNganh, mNhom], EXPORT_EXCEL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Dmhh.cmbNhomBeforeDropDown(Sender: TObject);
    var  s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNhom do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdImageExecute(Sender: TObject);
begin
    with QrDMVT do
    begin
        Application.CreateForm(TFrmFB_Dmhh_File, FrmFB_Dmhh_File);
        FrmFB_Dmhh_File.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdImportExcelExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
    rs: Integer;
begin
	sFile := isGetOpenFileName('XLS');
    if sFile = '' then
        Exit;

    DataExcel.ExcelImportConfirm('IMP_' + FORM_CODE, sFile, 'IMP_FB_DM_HH',
        'spIMPORT_FB_DM_HH;1', 'MAVT', [sysLogonUID, mPrefix, 0], 0);

    CmdReRead.Execute;
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdImportExcelTemplateExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_FB_DM_HH_EXCEL');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdNewExecute(Sender: TObject);
var
	ma: String;
begin
  with QrDMVT do
    begin
    	Append;
		ma := FieldByName('MAVT').AsString;
    end;
    PD0.Enabled := True;
    CbbMANGANH.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh.QrDMVTDVTChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('MaDvtLon').IsNull) or (FieldByName('MaDvtLon').AsInteger = 0) then
            FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Dmhh.EnabledElement(sPhanLoai: string): Boolean;
begin
   if sPhanLoai = mPhanLoai then
   begin
       EditThoiGianCheBien.Color:= clWindow;
       CbbQuayCheBien.Color:= clWindow;
       EditThoiGianCheBien.ReadOnly := False;
       CbbQuayCheBien.ReadOnly := False;
   end
   else
   begin
       EditThoiGianCheBien.Color:= clBtnFace;
       CbbQuayCheBien.Color:= clBtnFace;
       EditThoiGianCheBien.ReadOnly := True;
       CbbQuayCheBien.ReadOnly := True;
       QrDMVT.FieldByName('MAKHO').Clear();
       QrDMVT.FieldByName('THOIGIAN_CHEBIEN').Clear();
   end;
   Result := True;
end;

end.
