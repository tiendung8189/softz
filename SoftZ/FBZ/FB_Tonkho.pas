﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Tonkho;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin, Wwkeycb, AdvEdit,
  Vcl.DBCtrls, wwdbdatetimepicker, Vcl.Mask, wwdbedit, DBAdvEd, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh, Variants, MemTableDataEh,
  MemTableEh;

type
  TFrmFB_Tonkho = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdClose: TAction;
    Panel1: TPanel;
    Status: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    DsLIST: TDataSource;
    CmdRefresh: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    EdKy: TComboBox;
    EdNam: TComboBox;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    SepTinhton: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    TntToolButton4: TToolButton;
    Panel3: TPanel;
    Label4: TLabel;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TaPayroll: TTabSheet;
    Panel4: TPanel;
    QrLIST: TADOStoredProc;
    Panel5: TPanel;
    GrList2: TwwDBGrid2;
    ckbTon: TCheckBox;
    Label16: TLabel;
    EdTonDen: TwwDBDateTimePicker;
    QrLIST2: TADOStoredProc;
    DsLIST2: TDataSource;
    Filter2: TwwFilterDialog2;
    EdSearch: TwwIncrementalSearch;
    Panel6: TPanel;
    Panel7: TPanel;
    QrNGANH: TADOQuery;
    QrNHOM: TADOQuery;
    DsNGANH: TDataSource;
    DsNHOM: TDataSource;
    EdMaKho: TDBEditEh;
    DbLookupComboboxEh21: TDbLookupComboboxEh2;
    CbbMaKho: TDbLookupComboboxEh2;
    CbMANGANH: TDbLookupComboboxEh2;
    CbMANHOM: TDbLookupComboboxEh2;
    EdMANGANH: TDBEditEh;
    EhMANHOM: TDBEditEh;
    EdMADT: TDBEditEh;
    CbMADT: TDbLookupComboboxEh2;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CbMABeforeDropDown(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrLISTAfterOpen(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure EdKyChange(Sender: TObject);
    procedure EdSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);

    procedure CbMANHOMBeforeDropDown(Sender: TObject);
    procedure EdMaDTKeyPress(Sender: TObject; var Key: Char);
    procedure CbMANHOMDropDown(Sender: TObject);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
  	mCanEdit, mBookClosed, mTonAm: Boolean;
  	mKy, mNam: Integer;
    mKho, mSearch, mNganh, mNhom, mNCC: String;
    mStockDate: TDateTime;

    function GetDate: TDateTime;
    function GetStockDate(d: TDateTime): TDateTime;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmFB_Tonkho: TFrmFB_Tonkho;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon,
  TudenTon, GmsRep, FB_TudenTon;

{$R *.DFM}

const
	FORM_CODE = 'FB_TONKHO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.EdKyChange(Sender: TObject);
begin
    GetDate;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.EdMaDTKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.EdSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(QrLIST, 'TENVT', SearchValue);
    exPerformCustomSearch(QrLIST2, 'TENVT', SearchValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.Execute;
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

	isMonthList(EdKy);
    isYearList(EdNam);
    ckbTon.Checked := False;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.QrLISTAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrLIST, sysQtyFmt);
    SetDisplayFormat(QrLIST2, sysQtyFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_TONG'], [GrList, GrList2]);
    SetDictionary([QrLIST, QrLIST2], [FORM_CODE, FORM_CODE + '_TONG'], [Filter, Filter2]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DATA_ARISE = 'Có dữ liệu tồn kho trước tháng bắt đầu sử dụng chương trình.' +
            	    #13 + 'Xin liên hệ với người quản trị hệ thống để hiệu chỉnh.';

procedure TFrmFB_Tonkho.FormShow(Sender: TObject);
begin
	// Open database
    OpenDataSets([DataMain.QrDMKHO, QrNGANH, QrNHOM, DataMain.QrDM_KH_NCC]);

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Start to use checking
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;

		SQL.Text := Format('select 1 from FB_TONKHO where KY+12*NAM<%d',
        	[sysBegMon + 12 * sysBegYear]);
        Open;

        if not IsEmpty then
            Msg(RS_DATA_ARISE);

        Close;
        Free;
    end;

    mStockDate := GetSysParam('FB_STOCK_DATE');
    GetStockDate(mStockDate);

    CmdCalc.Visible := FlexConfigBool('FB_TONKHO', 'Tinhton', False);
    SepTinhton.Visible := CmdCalc.Visible;
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Tonkho.GetDate: TDateTime;
var
    yy, mm, dd: word;
begin
    mm := StrToInt(EdKy.Text);
    yy := StrToInt(EdNam.Text);

    Result := IncMonth(EncodeDate(yy, mm, 1)) - 1;
    if Result > mStockDate then
        Result := mStockDate;

    EdTonDen.DateTime := Result;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Tonkho.GetStockDate(d: TDateTime): TDateTime;
var
    yy, mm, dd: word;

begin
    if d = 0 then
        if (sysMon = sysBegMon) and (sysYear = sysBegYear) then
            d := Date
        else
            d := IncMonth(sysBegDate) - 1;

    DecodeDate(d, yy, mm, dd);
    EdNam.ItemIndex := yy;
    EdKy.ItemIndex := mm - 1;
    EdTonDen.DateTime := d;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_MONTH = 'Tháng làm việc không hợp lệ. Tháng bắt đầu sử dụng chương trình là %d/%d.';

procedure TFrmFB_Tonkho.CmdRefreshExecute(Sender: TObject);
var
    s, sSearch, sNganh, sNhom, sNCC, sKho: String;
	ky, nam, n: Integer;
    b, bTonAm: Boolean;
    mDate: TDateTime;
begin
    ky := StrToInt(EdKy.Text);
    nam := StrToInt(EdNam.Text);
    bTonAm := ckbTon.Checked;
    n := PgMain.ActivePageIndex;
    sSearch := EdSearch.Text;
    sNganh := EdMANGANH.Text;
    sNhom := EhMANHOM.Text;
    sNCC := EdMaDT.Text;
    sKho := EdMaKho.Text;

	if (mKy <> ky) or (mNam <> nam)
        or (sKho <> mKho)
        or (sSearch <> mSearch)
        or (bTonAm <> mTonAm)
        or (sNganh <> mNganh)
        or (sNhom <> mNhom)
        or (sNCC <> mNCC)
    then
    begin
    	mKy := ky;
        mNam := nam;
        mSearch := sSearch;
        mTonAm := bTonAm;
        mNganh := sNganh;
        mNhom := sNhom;
        mNCC := sNCC;

        if n = 1 then
            mKho := ''
        else
            mKho := sKho;

    	with QrLIST do
        begin
        	s := Sort;
            if Active then
            	Close;

            Parameters[1].Value := nam;
            Parameters[2].Value := ky;
            Parameters[3].Value := False;
            Parameters[4].Value := mKho;
            Parameters[5].Value := sysLOC;
            Parameters[6].Value := mNganh;
            Parameters[7].Value := mNhom;
            Parameters[8].Value := mNCC;
            Open;
            if s <> '' then
            	Sort := s;
        end;

        with QrLIST2 do
        begin
        	s := Sort;
            if Active then
            	Close;

            Parameters[1].Value := nam;
            Parameters[2].Value := ky;
            Parameters[3].Value := False;
            Parameters[4].Value := sysLOC;
            Parameters[5].Value := mNganh;
            Parameters[6].Value := mNhom;
            Parameters[7].Value := mNCC;
            Open;
            if s <> '' then
            	Sort := s;
        end;
    end;

    if n = 1 then
        GrList2.SetFocus
    else
        GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrLIST do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        exSearch(Name, DsLIST2)
    else
        exSearch(Name, DsLIST);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        Filter2.Execute
    else
    	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CbMABeforeDropDown(Sender: TObject);
begin
//    exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
var
    s: String;
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    if n = 1 then
        s := exRecordCount(QrLIST2, Filter)
    else
        s := exRecordCount(QrLIST, Filter);

    Status.SimpleText := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdReReadExecute(Sender: TObject);
begin
	mKy := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CbMANHOMBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    if not VarIsNull(CbMANGANH.Value) then
        s := CbMANGANH.Value
    else
        s := '';

    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CbMANHOMDropDown(Sender: TObject);
var
    s: String;
begin
    if not VarIsNull(CbMANGANH.Value) then
        s := CbMANGANH.Value
    else
        s := '';

    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Tonkho.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Xác nhận tính tồn kho đến ngày %s. Tiếp tục?';

procedure TFrmFB_Tonkho.CmdCalcExecute(Sender: TObject);
var
    mDate: TDateTime;
begin
    if GetRights('FB_TONTT') = R_DENY then
        Exit;

    // Make sure the params are correct
    CmdRefresh.Execute;
    mDate := EdTonDen.DateTime;

    Application.CreateForm(TFrmFB_TudenTon, FrmFB_TudenTon);
    if FrmFB_TudenTon.Execute(mDate) then
    begin
        mStockDate := GetSysParam('FB_STOCK_DATE');
        GetStockDate(mDate);
        //
        CmdReread.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsLIST);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.CmdPrintExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('FnB') (*60*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.PgMainChange(Sender: TObject);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    if n = 1 then
        EdSearch.DataSource := DsLIST2
    else
        EdSearch.DataSource := DsLIST;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tonkho.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := sysDefKho;
    end;
end;

end.
