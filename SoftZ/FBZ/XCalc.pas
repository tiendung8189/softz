﻿with DataSet do
begin
	if FieldByName('DELETE_BY').AsString <> '' then
    begin
		FieldByName('XOA').AsString := 'X';
		FieldByName('IMG').AsInteger := 0;
    end
	else
    begin
		FieldByName('XOA').Clear;
		FieldByName('IMG').AsInteger := -1;
    end;
end;
