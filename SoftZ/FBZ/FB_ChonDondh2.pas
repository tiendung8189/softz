﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonDondh2;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit,
  AdvEdit, DBAdvEd, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2, Variants,
  kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmFB_ChonDondh2 = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsDONDH: TDataSource;
    QrDONDH: TADOQuery;
    QrDONDHNGAY: TDateTimeField;
    QrDONDHSCT: TWideStringField;
    QrDONDHMADT: TWideStringField;
    QrDONDHMAKHO: TWideStringField;
    QrDONDHNG_DATHANG: TWideStringField;
    QrDONDHTHANHTOAN: TFloatField;
    QrDONDHDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrDONDHTENKHO: TWideStringField;
    QrDONDHTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrDONDHNGAY_GIAO: TDateTimeField;
    QrDONDHCHIETKHAU_HD: TFloatField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrDONDHSOTIEN: TFloatField;
    QrDONDHSOLUONG: TFloatField;
    QrDONDHKHOA: TGuidField;
    QrDM_KH_NCCMADT: TWideStringField;
    QrDM_KH_NCCTENDT: TWideStringField;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMADT: TDBEditEh;
    DsDM_KH_NCC: TDataSource;
    DsDMKHO: TDataSource;
    EdMaKho: TDBEditEh;
    CbbKhoHang: TDbLookupComboboxEh2;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMADVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDONDHBeforeOpen(DataSet: TDataSet);
    procedure CbMADTKeyPress(Sender: TObject; var Key: Char);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL, mSqlDT, mLCT, mTT: String;

    fTungay, fDenngay: TDateTime;

  public
  	function Execute(pFix: Boolean; pMadt, pKho: String;
    	pLoai: string = 'DM_NCC'; pLCT: String = 'FDHN'; pTT: String = ''): TGUID;
  end;

var
  FrmFB_ChonDondh2: TFrmFB_ChonDondh2;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonDondh2.Execute;
begin
	mNCC := pMadt;
    mKho := pKho;
    mTT := pTT;

    QrDM_KH_NCC.SQL.Text := Format(mSqlDT, [pLoai]);
    mLCT := pLCT;

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

	if ShowModal = mrOK then
		Result := TGuidField(QrDONDH.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DONDH', GrBrowse);

    SetDisplayFormat(QrDONDH, sysCurFmt);
    SetShortDateFormat(QrDONDH);
    SetDisplayFormat(QrDONDH, ['NGAY'], DateTimeFmt);

    mSQL := QrDONDH.SQL.Text;
    mSqlDT := QrDM_KH_NCC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.FormShow(Sender: TObject);
begin
   	OpenDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);

    // Smart focus
    if mNCC = '' then
		try
    		CbbNhaCungCap.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
   	if (fTungay <> EdTungay.Date)  or
    	(fDenngay <> EdDenngay.Date) or
    	(mNCC <> EdMADT.Text) or
	   	(mKHO <> EdMaKho.Text) then
	begin
   	    mNCC := EdMADT.Text;
        mKHO := EdMaKho.Text;
        fTungay  := EdTungay.Date;
	    fDenngay := EdDenngay.Date;

        with QrDONDH do
        begin
			Close;

            s := mSQL;

            if mNCC <> '' then
                s := s + ' and a.MADT=''' + mNCC + '''';

			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';

            if mTT <> '' then
                s := s + ' and a.TINHTRANG in (select Value from dbo.fnS_Table(''' + mTT + '''))';
            SQL.Text := s;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.CbMADTKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.CbMADVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.QrDONDHBeforeOpen(DataSet: TDataSet);
begin
      with QrDONDH do
      begin
      	Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
        Parameters[2].Value := fDenngay;
      end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDondh2.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mNcc;
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

end.


