﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_TheodoiGia;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  ActnList, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2, Vcl.Mask, DBCtrlsEh, Variants;

type
  TFrmFB_TheodoiGia = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    ActionList1: TActionList;
    CmdClose: TAction;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    spFB_CHUNGTU_CT_List_ThamKhao: TADOStoredProc;
    CHUNGTU_CT: TDataSource;
    EdMADT: TDBEditEh;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    DsDM_KH_NCC: TDataSource;
    CbbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    DsDMKHO: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVExit(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
    procedure CbbNhaCungCapExit(Sender: TObject);
    procedure CbbKhoHangExit(Sender: TObject);
  private
	mMADT, mKho, mLCT, mMABH: String;
    mTungay, mDenngay: TDateTime;
    mLoai: Integer; //0: Khach hang; 1: NCC
  public
  	function Execute(pLCT, pMABH: String; pMADT, pKho: String;
    	pLoai: Integer): TGUID;
  end;

var
  FrmFB_TheodoiGia: TFrmFB_TheodoiGia;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_TheodoiGia.Execute;
begin
    if pMABH = '' then
    begin
        Result := TGuidEx.EmptyGuid;
        Close;
        Exit;
    end;

	mMADT := pMADT;
    mKho := pKho;
    mLCT := pLCT;
    mLoai := pLoai;
    mMABH := pMABH;

    CbbNhaCungCap.Value := mMADT;
    EdMADT.Text := mMADT;
    CbbKhoHang.Value := mKho;
    EdMaKho.Text := mKho;

    if pLoai = 1 then
    begin
        CbbNhaCungCap.ControlLabel.Caption := 'Nhà cung cấp';
    	Caption := 'Tham Khảo Giá Nhập - ' + pMABH;
        SetCustomGrid('FB_THAMKHAO_GIA_NHAP' , GrBrowse);
    end
    else
    begin
        CbbNhaCungCap.ControlLabel.Caption := 'Khách hàng';
    	Caption := 'Tham Khảo Giá Xuất - ' + pMABH;
        SetCustomGrid('FB_THAMKHAO_GIA_XUAT' , GrBrowse);
    end;

    if ShowModal = mrOK then
		Result := TGuidField(spFB_CHUNGTU_CT_List_ThamKhao.FieldByName('KHOACT')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

    CloseDataSets([spFB_CHUNGTU_CT_List_ThamKhao, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrDM_KH_NCC, QrDMKHO]);

    // Smart focus
    if mMADT = '' then
    	try
    		CbbNhaCungCap.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mMADT := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.GrBrowseDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.CmdRefreshExecute(Sender: TObject);
var
	sDonvi, sKho: String;
    Tungay, Denngay: TDateTime;
begin
    sDonvi := EdMADT.Text;
    sKho := EdMaKho.Text;

    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if	(mTungay <> Tungay) or
    	(mDenngay <> Denngay) or
       (mMADT <> sDonvi) or
       (mKho <> sKho) then
	begin
   	    mMADT := sDonvi;
        mKho := sKho;
        mTungay := Tungay;
        mDenngay := Denngay;

        with spFB_CHUNGTU_CT_List_ThamKhao do
        begin
			Close;
            Prepared := True;
            Parameters[1].Value := mMABH;
            Parameters[2].Value := mLCT;
            Parameters[3].Value := mTungay;
            Parameters[4].Value := mDenngay;
            Parameters[5].Value := mMADT;
            Parameters[6].Value := mKho;
            Parameters[7].Value := '';
            Open;
        end;
        SetShortDateFormat(spFB_CHUNGTU_CT_List_ThamKhao);
        SetDisplayFormat(spFB_CHUNGTU_CT_List_ThamKhao, sysCurFmt);
        SetDisplayFormat(spFB_CHUNGTU_CT_List_ThamKhao, ['TL_CK', 'CK_HD'], sysPerFmt);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.CbbNhaCungCapExit(Sender: TObject);
var
    sDoiTac: string;
begin
    if not VarIsNull(CbbNhaCungCap.Value) then
        sDoiTac := CbbNhaCungCap.Value
    else
        sDoiTac := '';
    EdMADT.Text := sDoiTac;
    CbMaDVExit(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.CbbKhoHangExit(Sender: TObject);
var
    sLoc: string;
begin
    if not VarIsNull(CbbKhoHang.Value) then
        sLoc := CbbKhoHang.Value
    else
        sLoc := '';
    EdMaKho.Text := sLoc;
    CbMaDVExit(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.CbMaDVExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TheodoiGia.QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
begin
	QrDM_KH_NCC.Parameters[0].Value := mLoai;
end;

end.
