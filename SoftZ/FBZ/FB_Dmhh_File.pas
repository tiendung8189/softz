﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Dmhh_File;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  ADODb, Db,
  Menus , AppEvnts,
  Wwdbgrid, RzDBBnEd,
  AdvMenus, StdCtrls, Mask, RzEdit, RzDBEdit, Grids, Wwdbigrd, ToolWin,
  Vcl.Graphics, fcImager, Vcl.Dialogs, Vcl.ExtDlgs, rImageZoom, rDBComponents,
  Vcl.ImgList;

type
  TFrmFB_Dmhh_File = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    Tmmutin1: TMenuItem;
    PaMaster: TPanel;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListCAL_EXT: TWideStringField;
    QrListMAVT: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    Image1: TImage;
    QrListNAME: TWideStringField;
    QrListEXT: TWideStringField;
    Panel2: TPanel;
    FB_DM_HH_FILE_UPDATE: TADOCommand;
    QrListCONTENT: TBlobField;
    PopPic: TAdvPopupMenu;
    Gnhnh1: TMenuItem;
    Xahnh1: TMenuItem;
    N2: TMenuItem;
    Export1: TMenuItem;
    N1: TMenuItem;
    PopStretch: TMenuItem;
    OpenDlg: TOpenPictureDialog;
    SaveDlg: TSaveDialog;
    rDBImage1: TrDBImage;
    CmdAssign: TAction;
    CmdClear: TAction;
    CmdGray: TAction;
    CmdExport: TAction;
    QrListCONTENT_WEB: TBlobField;
    QrListMAIN: TBooleanField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListAfterInsert(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure PopPicPopup(Sender: TObject);
    procedure CmdAssignExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure CmdGrayExecute(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure PopStretchClick(Sender: TObject);
    procedure QrListAfterPost(DataSet: TDataSet);
  private
    mMa: String;
    nDrawStyle: Integer;
    mCanEdit, isPopStretch: Boolean;
  public
    procedure Execute(
    	pCanEdit: Boolean;
    	ma, ten: String);
  end;
var
  FrmFB_Dmhh_File: TFrmFB_Dmhh_File;

implementation

uses
	ExCommon,  isDb, islib, isMsg, Rights,  isFile, MainData, GuidEx,
    ShellAPI, isCommon, isStr, FB_Scan;

{$R *.DFM}

const
    FORM_CODE: String   = 'FB_DM_HH_FILE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.Execute(pCanEdit: Boolean; ma, ten: String);
begin
    mCanEdit := pCanEdit;
    DsList.AutoEdit := mCanEdit;
    mMa := ma;
    LbMA.Caption := ma;
    LbTEN.Caption := ten;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Dmhh_File.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
//    AddAllFields(QrList, FORM_CODE);
    isPopStretch := true;
    rDBImage1.Stretch := isPopStretch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.FormShow(Sender: TObject);
begin
    OpenDataSets([QrList]);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrList.Close;
    finally
    end;

//	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrList, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdClearExecute(Sender: TObject);
begin
    CmdDel.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdNewExecute(Sender: TObject);
begin
    QrList.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdSaveExecute(Sender: TObject);
begin
    QrList.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdAssignExecute(Sender: TObject);
var
    s, ext, pathWeb: String;
    FileSize: Int64;
begin
   	FormStyle := fsNormal;
	if not OpenDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
        Exit;
	end;

    s := OpenDlg.FileName;

    ext := UpperCase(ExtractFileExt(s));
    FileSize := getSizeOfFile(s);
    FileSize := Round(FileSize / (1024*1024));

    //  FileSize > 5MB
    if FileSize > 5  then
    begin
        ErrMsg('Kích thước file vượt quá 5MB');
        Exit;
    end;

    pathWeb := isStrReplace(s, ExtractFileExt(s), '_' + FormatDateTime('yyyymmddhhnnss', Now) + ExtractFileExt(s));

    FrmFB_Scan.ResizeJpeg(s, pathWeb, 97, 300);


	with QrList do
    begin
	    FieldByName('NAME').AsString := ChangeFileExt(ExtractFileName(s), '');
        TBlobField(FieldByName('CONTENT')).LoadFromFile(s);
        TBlobField(FieldByName('CONTENT_WEB')).LoadFromFile(pathWeb);
	    FieldByName('EXT').AsString := ext;
        //Post;
        try
           DeleteFile(pathWeb);
        finally
        end;
	end;

   	FormStyle := fsStayOnTop;
end;

procedure TFrmFB_Dmhh_File.CmdCancelExecute(Sender: TObject);
begin
    QrList.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
end;

procedure TFrmFB_Dmhh_File.CmdExportExecute(Sender: TObject);
begin
    FormStyle := fsNormal;
	if not SaveDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
    	Exit;
    end;

    with QrList do
    begin
	    if FieldByName('CONTENT').IsNull then
        begin
	    	FormStyle := fsStayOnTop;
    		Exit;
        end;

	    TBlobField(FieldByName('CONTENT')).SaveToFile(
        	ChangeFileExt(SaveDlg.FileName, FieldByName('EXT').AsString));
    end;
	FormStyle := fsStayOnTop;
end;

procedure TFrmFB_Dmhh_File.CmdGrayExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsList)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, bInsert: Boolean;
begin
    exActionUpdate(ActionList, QrList, True);

    with QrList do
    begin
    	if not Active then
        	Exit;

	    b := IsEmpty;
        bInsert := State in [dsInsert];
	end;

	CmdClear.Enabled := not b;
	CmdExport.Enabled := not b;
    PopStretch.Enabled := not b;
    CmdAssign.Enabled := bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.QrListAfterInsert(DataSet: TDataSet);
begin
    with QrList do
    begin
        FieldByName('MAVT').AsString := mMa;
        FieldByName('MAIN').AsBoolean := False;
    end;
end;

procedure TFrmFB_Dmhh_File.QrListAfterPost(DataSet: TDataSet);
var
    bm: TBytes;
begin
    with QrList do
    begin
        try

            if(FieldByName('MAIN').AsBoolean) then
            begin
                with FB_DM_HH_FILE_UPDATE do
                begin
                    Prepared := True;
                    Parameters[1].Value := QrList.FieldByName('MAVT').AsString;
                    Parameters[2].Value := TGuidEx.ToString(QrList.FieldByName('IDX'));
                    Execute;
                end;
                bm := BookMark;
                DisableControls;
                Requery;
                BookMark := bm;
                EnableControls;
            end;
        except
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.QrListBeforeOpen(DataSet: TDataSet);
begin
    with QrList do
    begin
        Parameters[0].Value := mMa;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

procedure TFrmFB_Dmhh_File.PopPicPopup(Sender: TObject);
begin
    PopStretch.Checked := isPopStretch;

end;

procedure TFrmFB_Dmhh_File.PopStretchClick(Sender: TObject);
begin
    isPopStretch := not isPopStretch;
    rDBImage1.Stretch := isPopStretch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.QrListBeforePost(DataSet: TDataSet);
var
    dest, source, luot: string;
begin
	with QrList do
    begin
        if BlankConfirm(QrList,['NAME']) then
            Abort;
//
    end;
    SetAudit(QrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_File.QrListCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrList do
    begin
        // File extension
        s := FieldByName('EXT').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('CAL_EXT').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Dmhh_File.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrList);
end;

end.
