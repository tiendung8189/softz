object FrmFB_Xuattra: TFrmFB_Xuattra
  Left = 308
  Top = 68
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Xu'#7845't Tr'#7843' - Nh'#224' H'#224'ng'
  ClientHeight = 573
  ClientWidth = 1016
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1016
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 1016
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1016
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 69
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 77
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 146
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 215
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 223
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 292
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn2: TToolButton
      Left = 300
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object SepChecked: TToolButton
      Left = 369
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 377
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton11: TToolButton
      Left = 446
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 454
      Top = 0
      Cursor = 1
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object ToolButton5: TToolButton
      Left = 538
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 546
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 1016
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 1008
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1008
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Thanh to'#225'n'#9'F'
          'MADT'#9'10'#9'M'#227#9'F'#9'Nh'#224' cung c'#7845'p'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 1008
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 1008
        inherited Panel1: TPanel
          Width = 1008
          ParentColor = False
          ExplicitWidth = 1008
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 132
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 285
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object Label5: TLabel
          Left = 587
          Top = 62
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
        end
        object Label8: TLabel
          Left = 583
          Top = 86
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
        end
        object TntLabel9: TLabel
          Left = 56
          Top = 86
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object Label7: TLabel
          Left = 540
          Top = 14
          Width = 108
          Height = 16
          Alignment = taRightJustify
          Caption = 'Phi'#7871'u giao h'#224'ng s'#7889
        end
        object EdSCT: TwwDBEdit
          Left = 342
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 656
          Top = 58
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_GIAO'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 656
          Top = 82
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_NHAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBMemo2: TDBMemo
          Left = 112
          Top = 82
          Width = 415
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
        end
        object wwDBEdit4: TwwDBEdit
          Left = 656
          Top = 10
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'PHIEUGIAOHANG'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbLyDo: TDbLookupComboboxEh2
          Left = 656
          Top = 34
          Width = 181
          Height = 22
          ControlLabel.Width = 30
          ControlLabel.Height = 16
          ControlLabel.Caption = 'L'#253' do'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'LYDO'
          DataSource = DsNX
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsPTXUATTRA
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsPTXUATTRA
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 5
          Visible = True
        end
        object EdMaKho: TDBEditEh
          Left = 450
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object CbKhoHang: TDbLookupComboboxEh2
          Left = 112
          Top = 34
          Width = 336
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 3
          Visible = True
        end
        object EdMADT: TDBEditEh
          Left = 450
          Top = 58
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MADT'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <
            item
              Action = CmdXemCongNo
              DefaultAction = False
              Style = ebsEllipsisEh
              Width = 20
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
        object CbbNhaCungCap: TDbLookupComboboxEh2
          Left = 112
          Top = 58
          Width = 336
          Height = 22
          ControlLabel.Width = 77
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Nh'#224' cung c'#7845'p'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MADT'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENDT'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MADT'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMNCC
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MADT'
          ListField = 'TENDT'
          ListSource = DataMain.DsDMNCC
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 6
          Visible = True
        end
      end
      object PaHoadon: TisPanel
        Left = 0
        Top = 132
        Width = 1008
        Height = 58
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' .: H'#243'a '#273#417'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PaHoadon2: TPanel
          Left = 2
          Top = 18
          Width = 1004
          Height = 38
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object TntLabel2: TLabel
            Left = 38
            Top = 11
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' h'#243'a '#273#417'n'
          end
          object TntLabel3: TLabel
            Left = 371
            Top = 11
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object TntLabel8: TLabel
            Left = 224
            Top = 11
            Width = 41
            Height = 16
            Alignment = taRightJustify
            Caption = 'K'#253' hi'#7879'u'
          end
          object EdSOHD: TwwDBEdit
            Left = 110
            Top = 7
            Width = 101
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SO'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit2: TwwDBEdit
            Left = 272
            Top = 7
            Width = 89
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'HOADON_SERI'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 406
            Top = 7
            Width = 93
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = 15794175
            DataField = 'HOADON_NGAY'
            DataSource = DsNX
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 2
          end
          object cbbHinhThuc: TDbLookupComboboxEh2
            Left = 685
            Top = 7
            Width = 150
            Height = 22
            ControlLabel.Width = 54
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'HINHTHUC_GIA'
            DataSource = DsNX
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
              end>
            DropDownBox.ListSource = DataMain.DsHINHTHUC_GIA
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = DataMain.DsHINHTHUC_GIA
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 3
            Visible = True
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 190
        Width = 1008
        Height = 259
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Tag = 1
          Left = 0
          Top = 16
          Width = 1008
          Height = 243
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False'
            'LK_TENTHUE;CustomEdit;CbLOAITHUE;F')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'B1'#9'7'#9'In tem'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'LK_DVT1'#9'8'#9'BOX'#9'T'
            'LK_QD1'#9'3'#9'='#9'T'
            'SOLUONG2'#9'10'#9'BOX'#9'F'#9'S'#7889' l'#432#7907'ng'
            'SOLUONG'#9'10'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
            'DONGIA2'#9'10'#9'BOX'#9'F'#9#272#417'n gi'#225
            'DONGIA'#9'10'#9'SKU'#9'F'#9#272#417'n gi'#225
            'TL_CK'#9'8'#9'%C.K'#9'F'
            'SOTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'T'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F'
            'LK_TENTHUE'#9'15'#9'T'#234'n thu'#7871#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
      object PaTotal: TPanel
        Left = 0
        Top = 449
        Width = 1008
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 3
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object PaSotien: TPanel
          Left = 2
          Top = 2
          Width = 257
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            257
            43)
          object Label10: TLabel
            Left = 149
            Top = 2
            Width = 66
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' h'#224'ng'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 152
          end
          object wwDBEdit9: TwwDBEdit
            Left = 148
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SOTIEN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaSotien1: TPanel
          Left = 259
          Top = 2
          Width = 215
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            215
            43)
          object Label9: TLabel
            Left = 108
            Top = 2
            Width = 99
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' sau C.K MH'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 106
          end
          object Label3: TLabel
            Left = 4
            Top = 2
            Width = 65
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Ti'#7873'n C.K MH'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object wwDBEdit8: TwwDBEdit
            Left = 106
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SOTIEN1'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit7: TwwDBEdit
            Left = 3
            Top = 17
            Width = 101
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'CHIETKHAU_MH'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaThanhtoan: TPanel
          Left = 883
          Top = 2
          Width = 123
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          DesignSize = (
            123
            43)
          object Label24: TLabel
            Left = 4
            Top = 2
            Width = 100
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
            FocusControl = EdTriGiaTT
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 14
          end
          object EdTriGiaTT: TwwDBEdit
            Left = 4
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'THANHTOAN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaSotien2: TPanel
          Left = 474
          Top = 2
          Width = 161
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          DesignSize = (
            161
            43)
          object Label14: TLabel
            Left = 4
            Top = 2
            Width = 53
            Height = 13
            Anchors = [akTop, akRight]
            Caption = '% C.K H'#272
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 135
          end
          object Label15: TLabel
            Left = 60
            Top = 2
            Width = 64
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Ti'#7873'n C.K H'#272
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 191
          end
          object EdTLCK: TwwDBEdit
            Left = 4
            Top = 17
            Width = 54
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'TL_CK_HD'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit10: TwwDBEdit
            Left = 60
            Top = 17
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'CHIETKHAU_HD'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object PaThue: TPanel
          Left = 635
          Top = 2
          Width = 248
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 4
          DesignSize = (
            248
            43)
          object BtThue: TSpeedButton
            Left = 226
            Top = 16
            Width = 23
            Height = 23
            Cursor = 1
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = BtThueClick
            ExplicitLeft = 225
          end
          object Label19: TLabel
            Left = 116
            Top = 2
            Width = 78
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Ti'#7873'n thu'#7871' VAT'
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 117
          end
          object Label13: TLabel
            Left = 4
            Top = 2
            Width = 98
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Tr'#7883' gi'#225' sau C.K H'#272
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 71
          end
          object EdTienVAT: TwwDBEdit
            Left = 116
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'THUE'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit6: TwwDBEdit
            Left = 4
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SOTIEN2'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 874
    Top = 46
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 874
    ExplicitTop = 46
  end
  object CbLOAITHUE: TwwDBLookupCombo
    Left = 446
    Top = 416
    Width = 117
    Height = 22
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    BorderStyle = bsNone
    DropDownAlignment = taRightJustify
    Selected.Strings = (
      'TENLT'#9'0'#9'TENLT'#9'F')
    DataField = 'LOAITHUE'
    DataSource = DsCT
    LookupTable = DataMain.QrDMLOAITHUE
    LookupField = 'MALT'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnNotInList = CbMAKHONotInList
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 354
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Hint = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdFromOrder: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' phi'#7871'u nh'#7853'p'
      OnExecute = CmdFromOrderExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdThamkhaoGia: TAction
      Category = 'DETAIL'
      Caption = 'Tham kh'#7843'o gi'#225
      OnExecute = CmdThamkhaoGiaExecute
    end
    object CmdDmvt: TAction
      Caption = 'Nguy'#234'n ph'#7909' li'#7879'u'
      ImageIndex = 42
      OnExecute = CmdDmvtExecute
    end
    object CmdDmncc: TAction
      Caption = 'Nh'#224' cung c'#7845'p'
      OnExecute = CmdDmnccExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
      OnExecute = CmdChecktonExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
    object CmdDmtd: TAction
      Caption = 'Th'#7921'c '#273#417'n'
      ImageIndex = 42
      OnExecute = CmdDmtdExecute
    end
    object CmdXemCongNo: TAction
      Caption = 'CmdXemCongNo'
      OnExecute = CmdXemCongNoExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 124
    Top = 354
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    AfterEdit = QrNXAfterEdit
    BeforePost = QrNXBeforePost
    AfterPost = QrNXAfterPost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHUNGTU'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 724
    Top = 372
    object QrNXXOA: TWideStringField
      DisplayLabel = 'H'#361'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Thanh to'#225'n'
      DisplayWidth = 16
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 10
      FieldName = 'MADT'
      FixedChar = True
      Size = 15
    end
    object QrNXLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n NCC'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 100
      Lookup = True
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXTL_CK_HD: TFloatField
      DisplayWidth = 10
      FieldName = 'TL_CK_HD'
      Visible = False
      OnChange = QrNXTL_CK_HDChange
    end
    object QrNXCHIETKHAU: TFloatField
      DisplayWidth = 10
      FieldName = 'CHIETKHAU_HD'
      Visible = False
      OnChange = QrNXTL_CK_HDChange
    end
    object QrNXTHUE: TFloatField
      DisplayWidth = 10
      FieldName = 'THUE'
      Visible = False
    end
    object QrNXTHUE_5: TFloatField
      FieldName = 'THUE_5'
    end
    object QrNXTHUE_10: TFloatField
      FieldName = 'THUE_10'
    end
    object QrNXTHUE_OR: TFloatField
      FieldName = 'THUE_OR'
    end
    object QrNXCL_THUE: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_THUE'
      Visible = False
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXSOTIEN: TFloatField
      DisplayLabel = 'Ch'#173'a thu'#213
      DisplayWidth = 10
      FieldName = 'SOTIEN'
      Visible = False
    end
    object QrNXCL_SOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'CL_SOTIEN'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      LookupDataSet = DataMain.EXPORT_LABEL
      Visible = False
    end
    object QrNXPTTT: TWideStringField
      FieldName = 'PTTT'
      Visible = False
      FixedChar = True
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXHAN_TTOAN: TIntegerField
      FieldName = 'HAN_TTOAN'
      Visible = False
    end
    object QrNXDA_TTOAN: TBooleanField
      FieldName = 'DA_TTOAN'
      Visible = False
    end
    object QrNXLK_HAN_TTOAN: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_HAN_TTOAN'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'HAN_TTOAN'
      KeyFields = 'MADT'
      Visible = False
      Lookup = True
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXTC_SOTIEN: TFloatField
      FieldName = 'TC_SOTIEN'
      Visible = False
    end
    object QrNXHOADON_SO: TWideStringField
      DisplayLabel = 'S'#232' ch'#248'ng t'#245
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrNXHOADON_SERI: TWideStringField
      DisplayLabel = 'S'#170'ri'
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrNXHOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
    object CALC_NGAY_TTOAN: TDateTimeField
      DisplayLabel = 'Ng'#224'y thanh to'#225'n'
      FieldKind = fkCalculated
      FieldName = 'CALC_NGAY_TTOAN'
      Calculated = True
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrNXHINHTHUC_GIA: TWideStringField
      FieldName = 'HINHTHUC_GIA'
      OnChange = QrNXHINHTHUC_GIAChange
      OnValidate = QrNXHINHTHUC_GIAValidate
    end
    object QrNXSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
      OnChange = QrNXTL_CK_HDChange
    end
    object QrNXSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
      Visible = False
    end
    object QrNXTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
      Visible = False
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXPHIEUGIAOHANG: TWideStringField
      FieldName = 'PHIEUGIAOHANG'
      Size = 30
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXCALC_SOTIEN_SAUCK_MH: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN_SAUCK_MH'
      Calculated = True
    end
    object QrNXSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrNXKHOA2: TGuidField
      FieldName = 'KHOA2'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
    end
    object QrNXLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrPTXUATTRA
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrNXSOTIEN2_TRUOC_CL: TFloatField
      FieldName = 'SOTIEN2_TRUOC_CL'
    end
    object QrNXSOTIEN2_CL: TFloatField
      FieldName = 'SOTIEN2_CL'
    end
    object QrNXTHUE_TRUOC_CL: TFloatField
      FieldName = 'THUE_TRUOC_CL'
    end
    object QrNXTHUE_CL: TFloatField
      FieldName = 'THUE_CL'
    end
    object QrNXTHANHTIEN_TRUOC_VAT: TFloatField
      FieldName = 'THANHTIEN_TRUOC_VAT'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_CHUNGTU_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 756
    Top = 372
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      FixedChar = True
      Size = 15
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTDONGIA_REF: TFloatField
      FieldName = 'DONGIA_REF'
    end
    object QrCTDONGIA_REF2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DONGIA_REF2'
      Calculated = True
    end
    object QrCTDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 10
      FieldName = 'DONGIA'
      OnChange = QrCTDONGIAChange
    end
    object QrCTDONGIA2: TFloatField
      FieldName = 'DONGIA2'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSOLUONG2: TFloatField
      FieldName = 'SOLUONG2'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSOLUONG2_LE: TFloatField
      FieldName = 'SOLUONG2_LE'
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrCTSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
    end
    object QrCTTL_CK: TFloatField
      DisplayLabel = '%C.K'
      DisplayWidth = 7
      FieldName = 'TL_CK'
      OnChange = QrCTSOTIENChange
    end
    object QrCTTL_CK_HD: TFloatField
      FieldName = 'TL_CK_HD'
      OnChange = QrCTSOTIENChange
    end
    object QrCTCHIETKHAU: TFloatField
      DisplayLabel = 'Chi'#7871't kh'#7845'u'
      DisplayWidth = 12
      FieldName = 'CHIETKHAU'
      OnChange = QrCTSOTIENChange
    end
    object QrCTCHIETKHAU_HD: TFloatField
      FieldName = 'CHIETKHAU_HD'
    end
    object QrCTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      OnChange = QrCTLOAITHUEChange
      Size = 15
    end
    object QrCTLK_TENTHUE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTHUE'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'TENLT'
      KeyFields = 'LOAITHUE'
      Size = 200
      Lookup = True
    end
    object QrCTLK_VAT_RA: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_RA'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_RA'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTLK_VAT_VAO: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_VAO'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_VAO'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
      Visible = False
      OnChange = QrCTSOTIENChange
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
      Visible = False
    end
    object QrCTTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrCTTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrCTTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrCTTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTLK_TENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTAT'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 25
      Lookup = True
    end
    object QrCTEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNganTinhTrang'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTGIAVON: TFloatField
      FieldName = 'GIAVON'
    end
    object QrCTSOTIEN2_TRUOC_CL: TFloatField
      FieldName = 'SOTIEN2_TRUOC_CL'
    end
    object QrCTSOTIEN2_CL: TFloatField
      FieldName = 'SOTIEN2_CL'
      OnChange = QrCTSOTIENChange
    end
    object QrCTTIEN_THUE_TRUOC_CL: TFloatField
      FieldName = 'TIEN_THUE_TRUOC_CL'
    end
    object QrCTTIEN_THUE_CL: TFloatField
      FieldName = 'TIEN_THUE_CL'
      OnChange = QrCTSOTIENChange
    end
    object QrCTTHANHTIEN_TRUOC_VAT: TFloatField
      FieldName = 'THANHTIEN_TRUOC_VAT'
    end
    object QrCTDONGIA_LE: TFloatField
      FieldName = 'DONGIA_LE'
    end
    object QrCTSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrCTTL_LAI: TFloatField
      FieldName = 'TL_LAI'
    end
    object QrCTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTDVT: TWideStringField
      FieldName = 'DVT'
    end
    object QrCTDVT_BOX: TWideStringField
      FieldName = 'DVT_BOX'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 724
    Top = 424
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 756
    Top = 424
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 290
    Top = 312
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel2: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 354
    Top = 312
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 382
    Top = 428
    object MenuItem1: TMenuItem
      Action = CmdFromOrder
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Tag = 1
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Splithtmthng1: TMenuItem
      Action = CmdSapthutu
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object hamkhoginhp1: TMenuItem
      Action = CmdThamkhaoGia
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Kimtraslngtnkho1: TMenuItem
      Action = CmdCheckton
    end
  end
  object CHECK_NCC: TADOCommand
    CommandText = 'select 1 from DM_VT where MAVT=:MAVT and MADT=:MADT'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 548
    Top = 340
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'CHIETKHAU_MH'
      'SOTIEN'
      'SOTIEN1'
      'SOTIEN2_TRUOC_CL'
      'SOTIEN2_CL'
      'SOTIEN2'
      'THANHTIEN_TRUOC_VAT'
      'THUE_TRUOC_CL'
      'THUE_CL'
      'THUE_5'
      'THUE_10'
      'THUE_OR'
      'THUE'
      'THANHTIEN'
      'SOTIEN_LE')
    DetailFields.Strings = (
      'SOLUONG'
      'CHIETKHAU'
      'SOTIEN'
      'SOTIEN1'
      'SOTIEN2_TRUOC_CL'
      'SOTIEN2_CL'
      'SOTIEN2'
      'THANHTIEN_TRUOC_VAT'
      'TIEN_THUE_TRUOC_CL'
      'TIEN_THUE_CL'
      'TIEN_THUE_5'
      'TIEN_THUE_10'
      'TIEN_THUE_OR'
      'TIEN_THUE'
      'THANHTIEN'
      'SOTIEN_LE')
    Left = 418
    Top = 312
  end
  object CHUNGTU_LAYPHIEU: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'FB_CHUNGTU_LAYPHIEU;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 678
    Top = 340
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 176
    Top = 408
    object MenuItem3: TMenuItem
      Tag = 1
      Action = CmdDmvt
    end
    object hcn1: TMenuItem
      Action = CmdDmtd
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Tag = 2
      Action = CmdDmncc
      AutoHotkeys = maManual
      AutoLineReduction = maManual
    end
  end
end
