﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_DmvtBo;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Dialogs,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit,
  fctreecombo, isDb, fcTreeView, wwDialog, Mask, RzPanel, fcCombo, Grids,
  Wwdbgrid, ToolWin, wwdbedit, Vcl.Buttons, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmFB_DmvtBo = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    QrDMVTGIABAN: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTQUAYKE: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    QrKho: TADOQuery;
    ItmNCC: TMenuItem;
    ItmNHOM: TMenuItem;
    N1: TMenuItem;
    CmdPhoto: TAction;
    N2: TMenuItem;
    Xemhnh1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N4: TMenuItem;
    PopIn: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    CmdChangeGroup: TAction;
    N5: TMenuItem;
    ingnhnhm1: TMenuItem;
    QrDMVTTINHTRANG: TWideStringField;
    CmdExport: TAction;
    ToolButton8: TToolButton;
    ToolButton12: TToolButton;
    SaveDlg: TSaveDialog;
    DMVT_EXPORT: TADOStoredProc;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label14: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    EdMA: TwwDBEdit;
    EdTen: TwwDBEdit;
    DBEdit11: TwwDBEdit;
    PD2: TisPanel;
    EdGHICHU: TDBMemo;
    PD3: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    PaCT: TisPanel;
    GrCT: TwwDBGrid2;
    PopDetail: TAdvPopupMenu;
    CmdDelDetail: TAction;
    Xachitit1: TMenuItem;
    CmdSwitch: TAction;
    QrCT_BO: TADOQuery;
    DsCT_BO: TDataSource;
    QrCT_BOMABO: TWideStringField;
    QrCT_BOMAVT_CT: TWideStringField;
    QrCT_BOSOLUONG: TFloatField;
    RefFB_DM_HH_CT: TADOQuery;
    QrDMVTBO: TBooleanField;
    vlTotal: TisTotal;
    QrCT_BORSTT: TIntegerField;
    QrCT_BOLK_TENVT: TWideStringField;
    ALLOC_BARCODE: TADOCommand;
    Bevel1: TBevel;
    CmdAudit: TAction;
    QrDMVTMAVT: TWideStringField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    DMVT_EXPORT2: TADOStoredProc;
    DBEdit2: TwwDBEdit;
    ToolButton9: TToolButton;
    ToolButton13: TToolButton;
    CmdChecked: TAction;
    QrDMVTCHECKED: TBooleanField;
    Splitter1: TSplitter;
    RefFB_DM_HH_CTMAVT: TWideStringField;
    RefFB_DM_HH_CTTENVT: TWideStringField;
    RefFB_DM_HH_CTGIANHAP: TFloatField;
    RefFB_DM_HH_CTGIABAN: TFloatField;
    RefFB_DM_HH_CTMADT: TWideStringField;
    RefFB_DM_HH_CTTENDT: TWideStringField;
    QrCT_BOTL_CK: TFloatField;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTGIABAN1: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    QrNhom: TADOQuery;
    QrNganh: TADOQuery;
    PD0: TisPanel;
    QrDMVTGIANHAP_CHUA_VAT: TFloatField;
    QrDMVTGIABO_CHUA_CK_CHUA_VAT: TFloatField;
    QrDMVTGIABO_CHUA_CK: TFloatField;
    QrDMVTCHIETKHAU_BO_CHUA_VAT: TFloatField;
    QrDMVTCHIETKHAU_BO: TFloatField;
    QrCT_BODONGIA_CHUA_CK_CHUA_VAT: TFloatField;
    QrCT_BOTHANHTIEN_CHUA_CK_CHUA_VAT: TFloatField;
    QrCT_BODONGIA_CHUA_CK: TFloatField;
    QrCT_BOTHANHTIEN_CHUA_CK: TFloatField;
    QrCT_BOCHIETKHAU_CHUA_VAT: TFloatField;
    QrCT_BOCHIETKHAU: TFloatField;
    QrCT_BOLOAITHUE: TWideStringField;
    QrCT_BOVAT_VAO: TFloatField;
    QrCT_BOVAT_RA: TFloatField;
    QrCT_BODONGIA_CHUA_VAT: TFloatField;
    QrCT_BOTHANHTIEN_CHUA_VAT: TFloatField;
    QrCT_BODONGIA: TFloatField;
    QrCT_BOTHANHTIEN: TFloatField;
    QrDMVTLK_KHO: TWideStringField;
    QrCT_BOLK_DVT: TWideStringField;
    RefFB_DM_HH_CTVAT_VAO: TFloatField;
    RefFB_DM_HH_CTVAT_RA: TFloatField;
    RefFB_DM_HH_CTGIAVON: TFloatField;
    RefFB_DM_HH_CTLOAITHUE: TWideStringField;
    RefFB_DM_HH_CTGIABAN_CHUA_VAT: TFloatField;
    RefFB_DM_HH_CTGIANHAP_CHUA_VAT: TFloatField;
    Label4: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label7: TLabel;
    QrCT_BOLK_VAT_VAO: TFloatField;
    QrCT_BOLK_VAT_RA: TFloatField;
    QrCT_BOLK_GIABAN_CHUA_VAT: TFloatField;
    QrCT_BOLK_GIABAN: TFloatField;
    isPanel1: TisPanel;
    isPanel2: TisPanel;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label9: TLabel;
    wwDBEdit3: TwwDBEdit;
    Label11: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label13: TLabel;
    wwDBEdit5: TwwDBEdit;
    QrCT_BOLOAITHUE2: TWideStringField;
    DMVT_TINHLAI: TADOCommand;
    CmdDmdvt: TAction;
    QrDMVTMA: TWideStringField;
    QrDMVTMAVT_NCC: TWideStringField;
    ToolButton14: TToolButton;
    CmdImage: TAction;
    ToolButton15: TToolButton;
    QrDMVTHIENTHI_ANHDAIDIEN: TBooleanField;
    N3: TMenuItem;
    Hinthhnhidin1: TMenuItem;
    ToolButton16: TToolButton;
    PopDanhmuc: TAdvPopupMenu;
    NguynphliuFB1: TMenuItem;
    CmdNPL: TMenuItem;
    ToolButton17: TToolButton;
    CmdDMTD: TAction;
    CmdDMNPL: TAction;
    QrCamUng: TADOQuery;
    QrDMVTCAMUNG_NHOM: TWideStringField;
    QrDMVTTENVT_TA: TWideStringField;
    Label5: TLabel;
    wwDBEdit6: TwwDBEdit;
    isPanel3: TisPanel;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    QrDMVTMaDvt: TIntegerField;
    QrDMVTMaDvtLon: TIntegerField;
    QrDMVTQuyDoi: TIntegerField;
    DsCamUng: TDataSource;
    DsNganh: TDataSource;
    DsNhom: TDataSource;
    RefFB_DM_HH_CTMaDvt: TIntegerField;
    cbbTINHTRANG: TDbLookupComboboxEh2;
    cbbCAMUNG_NHOM: TDbLookupComboboxEh2;
    CbbQuayCheBien: TDbLookupComboboxEh2;
    DsKho: TDataSource;
    cbbMaDvtLon: TDbLookupComboboxEh2;
    LbQD: TLabel;
    CbQuyDoi: TDBNumberEditEh;
    LbX: TLabel;
    CbDVT: TDbLookupComboboxEh2;
    CbbMANGANH: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    DBEditEh2: TDBEditEh;
    CbMANHOM: TDbLookupComboboxEh2;
    DBNumberEditEh1: TDBNumberEditEh;
    cbbLOAITHUE: TDbLookupComboboxEh2;
    QrDMVTLK_Dvt: TWideStringField;
    QrDMVTLK_DvtLon: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTVAT_VAOChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure ItmNCCClick(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrDMVTAfterScroll(DataSet: TDataSet);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrCT_BOBeforeInsert(DataSet: TDataSet);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrCT_BOBeforeOpen(DataSet: TDataSet);
    procedure QrCT_BOBeforeDelete(DataSet: TDataSet);
    procedure QrCT_BOBeforePost(DataSet: TDataSet);
    procedure QrCT_BOAfterCancel(DataSet: TDataSet);
    procedure QrCT_BOAfterDelete(DataSet: TDataSet);
    procedure QrCT_BOAfterEdit(DataSet: TDataSet);
    procedure QrCT_BOCalcFields(DataSet: TDataSet);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrDMVTBeforeEdit(DataSet: TDataSet);
    procedure QrCT_BOTL_CKValidate(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure cmbNhomBeforeDropDown(Sender: TObject);
    procedure QrCT_BOMAVT_CTChange(Sender: TField);
    procedure QrCT_BODONGIAChange(Sender: TField);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure CmdImageExecute(Sender: TObject);
    procedure PopMainPopup(Sender: TObject);
    procedure QrKhoBeforeOpen(DataSet: TDataSet);
    procedure ToolButton16Click(Sender: TObject);
    procedure CmdDMNPLExecute(Sender: TObject);
    procedure CmdDMTDExecute(Sender: TObject);
    procedure QrCT_BOAfterInsert(DataSet: TDataSet);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure QrDMVTMaDvtChange(Sender: TField);
  private
  	mCanEdit, mEANConfirm, bUnitLevel: Boolean;
    mGia: Double;
    defDvt: Integer;
    sCodeLen, defTinhtrang, defLoc: String;
 	mSQL, mNganh, mNhom, mPrefix, mLoaiNganh: String;
    defTinhTrangFilter, defTinhTrangDangDung: String;
    function  AllocEAN(nhom: String): String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmFB_DmvtBo: TFrmFB_DmvtBo;

implementation

uses
	ExCommon, isMsg, Rights, MainData, RepEngine, isCommon,
    FB_CayNganhNhom, isBarcode, isLib, isFile, Dmkhac, OfficeData,
    FB_Dmhh_File, FB_Scan, FB_Dmhh, FB_DmvtNVL, isStr, DmHotro;

{$R *.DFM}

const
	FORM_CODE = 'FB_DM_HH_BO';
    EXPORT_EXCEL = 'FB_RP_DM_HH_BO_EXCEL';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_DmvtBo.AllocEAN(nhom: String): String;
begin
    with ALLOC_BARCODE do
    begin
    	Prepared := True;
        Parameters[1].Value := mPrefix;
        Parameters[2].Value := nhom;

        Execute;
		Result := Parameters[3].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.FormCreate(Sender: TObject);
var
	b: Boolean;
begin
    TMyForm(Self).Init2;
    defTinhTrangDangDung := '01';
    with QrDMVT do
    begin
        SetDisplayFormat(QrDMVT, sysCurFmt);
	    SetShortDateFormat(QrDMVT);
    end;

    SetDisplayFormat(QrCT_BO, sysCurFmt);
    SetDisplayFormat(QrCT_BO, ['SOLUONG'], ctQtyFmt);
    SetDisplayFormat(QrCT_BO, ['TL_CK'], sysPerFmt);

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrList, GrCT]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Params
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE, 'EAN Confirm');
	sCodeLen := FlexConfigString(FORM_CODE, 'Code Length');

    // Don vi tinh quy doi
	bUnitLevel := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;

    LbX.Visible := bUnitLevel;
    LbQD.Visible := bUnitLevel;
    CbQuyDoi.Visible := bUnitLevel;
    cbbMaDvtLon.Visible := bUnitLevel;
    //speedBtnDVT.Visible := bUnitLevel;

	// Default unit
	defDvt := GetSysParam('DEFAULT_DVT');

    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmFB_Scan := Nil;

    mLoaiNganh := 'FBTP';
    // Tree Combo
    FlexGroupCombo(CbNhomhang, mLoaiNganh);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.FormShow(Sender: TObject);
begin
	DsDMVT.AutoEdit := mCanEdit;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrMAU, QrDM_DVT, QrSIZE, QrV_FB_DMVT_TINHTRANG, QrDMLOAITHUE, QrFB_DM_HH]);
    end;

    OpenDataSets([QrKho, QrNganh, QrNhom, QrCamUng, RefFB_DM_HH_CT]);

    with DataMain.QrV_FB_DMVT_TINHTRANG do
    begin
        if Locate('MA', defTinhTrangDangDung, []) then
        begin
            defTinhtrang := FieldByName('MA_HOTRO').AsString;
            defTinhTrangFilter := FieldByName('MA_HOTRO').AsString;
        end;
    end;

    SetDisplayFormat(RefFB_DM_HH_CT, sysCurFmt);
    // Loading
	CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	if FrmFB_Scan <> Nil then
    	FrmFB_Scan.Close;

    // Saving & closing
    try
        CloseDataSets([QrKho, QrNganh, QrNhom, QrCamUng, RefFB_DM_HH_CT]);
    finally
    end;
    RegWrite(Name, ['PD2', 'PD3'], [PD2.Collapsed, PD3.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdNewExecute(Sender: TObject);
begin
    QrDMVT.Append;
    PD0.Enabled := True;
    CbbMANGANH.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdSaveExecute(Sender: TObject);
begin
    QrCT_BO.CheckBrowseMode;
	QrDMVT.Post;
    QrCT_BO.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdCancelExecute(Sender: TObject);
begin
    QrCT_BO.CancelBatch;
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdDelExecute(Sender: TObject);
begin
	QrDMVT.Delete;
end;

procedure TFrmFB_DmvtBo.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_PUB_DM_KHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

procedure TFrmFB_DmvtBo.CmdDMNPLExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    begin
        RefFB_DM_HH_CT.Requery;
    end;
end;

procedure TFrmFB_DmvtBo.CmdDMTDExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    begin
        RefFB_DM_HH_CT.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.ItmNCCClick(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE + '_' + IntToStr((Sender as TComponent).Tag),
    	[sysLogonUID, mNganh, mNhom]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdPrintExecute(Sender: TObject);
begin
   BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdImageExecute(Sender: TObject);
begin
   with QrDMVT do
    begin
        Application.CreateForm(TFrmFB_Dmhh_File, FrmFB_Dmhh_File);
        FrmFB_Dmhh_File.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, bInsert, bChecked: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
    	bHasRec := not IsEmpty;
        bChecked := FieldByName('CHECKED').AsBoolean;
	end;

    GrList.Enabled := bBrowse;
    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;
    PaList.Enabled := bBrowse;

    PD0.Enabled := bInsert;

    if FrmFB_Scan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec;
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and (not FrmFB_Scan.Visible);
        CmdFilter.Enabled := not FrmFB_Scan.Visible;
        CmdSearch.Enabled := not FrmFB_Scan.Visible;
    end;

    CmdChecked.Enabled := bHasRec;
    CmdChecked.Caption := exGetCheckedCaption(QrDMVT);

    CmdSearch.Enabled := CmdSearch.Enabled and bBrowse;
    CmdFilter.Enabled := CmdFilter.Enabled and bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdChangeGroup.Enabled := mCanEdit and bBrowse and bHasRec;

    CmdDelDetail.Enabled := mCanEdit and (not QrCT_BO.IsEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTBeforeEdit(DataSet: TDataSet);
begin
	if not mTrigger then
    	exIsChecked(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.PopMainPopup(Sender: TObject);
begin
    with QrDMVT do
    begin
         if FieldByName('HIENTHI_ANHDAIDIEN').AsBoolean then
         begin
             Hinthhnhidin1.Checked := True;
         end
         else
         begin
            Hinthhnhidin1.Checked := False;
         end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) then
	begin
        mNganh  := sNganh;
        mNhom   := sNhom;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            end;

            SQL.Text := sSQL;
            Open;

            GrList.SetFocus;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
        FieldByName('MaDvt').AsInteger := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
		FieldByName('BO').AsBoolean := True;
        FieldByName('QuyDoi').AsFloat := 1;
		FieldByName('MAVT').AsString := AllocEAN('');
        FieldByName('HIENTHI_ANHDAIDIEN').AsBoolean := False;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';
    RS_INVALID_QD = 'Quy đổi không hợp lệ';
    RS_INVALID_UNIT = 'Đơn vị tính không hợp lệ';

procedure TFrmFB_DmvtBo.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM','TENVT', 'MaDvt', 'LOAITHUE']) then
			Abort;

        if bUnitLevel then
		begin
            if BlankConfirm(DataSet, ['QuyDoi', 'MaDvtLon']) then
                Abort;

            if (FieldByName('QuyDoi').AsInteger < 1) then
            begin
                ErrMsg(RS_INVALID_QD );
                Abort;
            end;

            if ((FieldByName('QuyDoi').Value = 1) and (FieldByName('MaDvt').AsInteger <> FieldByName('MaDvtLon').AsInteger)) or
                ((FieldByName('QuyDoi').Value > 1) and (FieldByName('MaDvt').AsInteger = FieldByName('MaDvtLon').AsInteger)) then
            begin
                ErrMsg(RS_INVALID_UNIT );
                Abort;
            end;
        end else
        begin
           FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger ;
           FieldByName('QuyDoi').Value := 1;
        end;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MAVT').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;
	end;
    SetAudit(DataSet);
end;

(*
========================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end;
    end;

    with QrCT_BO do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('LOAITHUE').AsString := QrDMVT.FieldByName('LOAITHUE').AsString;
            FieldByName('VAT_VAO').AsFloat := QrDMVT.FieldByName('VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := QrDMVT.FieldByName('VAT_RA').AsFloat;
            Next;
        end;
    end;
end;

(*
========================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOBeforeInsert(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    if BlankConfirm(QrDMVT, ['LOAITHUE']) then
        Abort;

    SetEditState(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdPhotoExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmFB_Scan, FrmFB_Scan);
    FrmFB_Scan.Left := Width -  FrmFB_Scan.Width;
    FrmFB_Scan.Top := PD1.Top - 8;
    FrmFB_Scan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmFB_DmvtBo.QrDMVTTENVTChange(Sender: TField);
var
	b : Boolean;
begin
	with QrDMVT do
    begin
    	b := FieldByName('TENTAT').AsString = '';

    	if not b then
        	b := YesNo(RS_NAME_DEF);

		if b then
			FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTVAT_VAOChange(Sender: TField);
begin
	with QrDMVT do
    	if Sender.AsFloat <> 0 then
	    	FieldByName('VAT_RA').AsFloat := Sender.AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrKhoBeforeOpen(DataSet: TDataSet);
begin
    QrKho.Parameters[0].Value := sysLoc;
end;

procedure TFrmFB_DmvtBo.ToolButton16Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmFB_DmvtBo.QrDMVTMaDvtChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('MaDvtLon').IsNull) or (FieldByName('MaDvtLon').AsInteger = 0) then
            FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTMANGANHChange(Sender: TField);
begin
    QrDMVT.FieldByName('MANHOM').AsString := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
    with QrDMVT do
    begin
        FieldByName('MA').AsString := mMa;
        FieldByName('MAVT_NCC').AsString := mMa;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTAfterPost(DataSet: TDataSet);
begin
	with QrDMVT do
		mGia := FieldByName('GIABAN').AsFloat;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrDMVTAfterScroll(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';

procedure TFrmFB_DmvtBo.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom: String;
begin
	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;

    end;

    Application.CreateForm(TFrmFB_CayNganhNhom, FrmFB_CayNganhNhom);
    b := FrmFB_CayNganhNhom.Execute(nganh, nhom);
    FrmFB_CayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdCheckedExecute(Sender: TObject);
begin
	exChecked(QrDMVT, 'FB_KHUYENMAI_COMBO');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdExportExecute(Sender: TObject);
begin
    DataOffice.CreateReport2('XLSX\' + EXPORT_EXCEL + '.xlsx',
        [sysLogonUID, mNganh, mNhom], EXPORT_EXCEL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdDelDetailExecute(Sender: TObject);
begin
    QrCT_BO.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.cmbNhomBeforeDropDown(Sender: TObject);
    var  s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrCT then
        EdGHICHU.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOBeforeOpen(DataSet: TDataSet);
begin
    QrCT_BO.Parameters[0].Value := QrDMVT.FieldByName('MAVT').AsString;
end;
    
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;

    SetEditState(QrDMVT);
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOAfterDelete(DataSet: TDataSet);
begin
	vlTotal.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOAfterEdit(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOAfterInsert(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOTL_CKValidate(Sender: TField);
begin
    with QrCT_BO do
    begin
        if (FieldByName('TL_CK').AsFloat > 100) then
        begin
            ErrMsg('Tỷ lệ chiết khấu không hợp lệ.');
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOBeforePost(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
        if BlankConfirm(QrCT_BO, ['MAVT_CT', 'SOLUONG']) then
	        Abort;

        if IsDuplicateCode(QrCT_BO, FieldByName('MAVT_CT'), True) then
            Abort;

        FieldByName('MAVT').AsString := QrDMVT.FieldByName('MAVT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOAfterCancel(DataSet: TDataSet);
begin
	vlTotal.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOCalcFields(DataSet: TDataSet);
begin
    with QrCT_BO do
    begin
        if not (State in [dsInsert]) then
            FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BODONGIAChange(Sender: TField);
var
	mSoLuong,mGiaBanCKChuaVAT, mThanhTienCKChuaVAT, mGiaBanCKVAT, mThanhTienCKVAT, mTL_CK, mCKChuaVAT, mCKVAT, mGiaBanChuaVAT, mThanhTienChuaVAT, mGiaBanVAT, mThanhTienVAT: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

    mTrigger := True;
	with QrCT_BO do
    begin
        mLoaithue   := FieldByName('LOAITHUE').AsString;
        mSoLuong    := FieldByName('SOLUONG').AsFloat;
        mGiaBanCKChuaVAT    := FieldByName('DONGIA_CHUA_CK_CHUA_VAT').AsFloat;
        mGiaBanCKVAT        := FieldByName('DONGIA_CHUA_CK').AsFloat;
        mTL_CK      := FieldByName('TL_CK').AsFloat;
        mThanhTienCKChuaVAT := FieldByName('THANHTIEN_CHUA_CK_CHUA_VAT').AsFloat;
        mThanhTienCKVAT     := FieldByName('THANHTIEN_CHUA_CK').AsFloat;
		mCKChuaVAT          := FieldByName('CHIETKHAU_CHUA_VAT').AsFloat;
        mCKVAT              := FieldByName('CHIETKHAU').AsFloat;
        mGiaBanChuaVAT      := FieldByName('DONGIA_CHUA_VAT').AsFloat;
        mThanhTienChuaVAT   := FieldByName('THANHTIEN_CHUA_VAT').AsFloat;
        mGiaBanVAT          := FieldByName('DONGIA').AsFloat;
        mThanhTienVAT       := FieldByName('THANHTIEN').AsFloat;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := Sender.FieldName;
        Parameters[2].Value := mSoLuong;
    	Parameters[3].Value := mGiaBanCKChuaVAT;
        Parameters[4].Value := mGiaBanCKVAT;
        Parameters[5].Value := mTL_CK;
        Parameters[6].Value := mThanhTienCKChuaVAT;
    	Parameters[7].Value := mThanhTienCKVAT;
    	Parameters[8].Value := mCKChuaVAT;
        Parameters[9].Value := mCKVAT;
        Parameters[10].Value := mGiaBanChuaVAT;
        Parameters[11].Value := mThanhTienChuaVAT;
        Parameters[12].Value := mGiaBanVAT;
        Parameters[13].Value := mThanhTienVAT;
        Parameters[14].Value := mLoaithue;
        Execute;
        mGiaBanCKVAT := Parameters[4].Value;
        mTL_CK := Parameters[5].Value;
        mThanhTienCKChuaVAT := Parameters[6].Value;
        mThanhTienCKVAT := Parameters[7].Value;
    	mCKChuaVAT := Parameters[8].Value;
        mCKVAT := Parameters[9].Value;
        mGiaBanChuaVAT := Parameters[10].Value;
        mThanhTienChuaVAT := Parameters[11].Value;
        mGiaBanVAT := Parameters[12].Value;
        mThanhTienVAT := Parameters[13].Value;
    end;

	with QrCT_BO do
    begin
        FieldByName('DONGIA_CHUA_CK').AsFloat :=  exVNDRound(mGiaBanCKVAT, ctPriceRound);
        FieldByName('TL_CK').AsFloat := exVNDRound(mTL_CK, sysPerRound);
        FieldByName('THANHTIEN_CHUA_CK_CHUA_VAT').AsFloat := exVNDRound(mThanhTienCKChuaVAT, ctCurRound);
        FieldByName('THANHTIEN_CHUA_CK').AsFloat := exVNDRound(mThanhTienCKVAT, ctCurRound);
        FieldByName('CHIETKHAU_CHUA_VAT').AsFloat := exVNDRound(mCKChuaVAT, ctCurRound);
        FieldByName('CHIETKHAU').AsFloat := exVNDRound(mCKVAT, ctCurRound);
        FieldByName('DONGIA_CHUA_VAT').AsFloat := exVNDRound(mGiaBanChuaVAT, ctPriceRound);
        FieldByName('THANHTIEN_CHUA_VAT').AsFloat := exVNDRound(mThanhTienChuaVAT, ctCurRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(mGiaBanVAT, ctPriceRound);
        FieldByName('THANHTIEN').AsFloat := exVNDRound(mThanhTienVAT, ctCurRound);
    end;
    mTrigger := False;
    GrCT.InvalidateCurrentRow;
    vlTotal.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.QrCT_BOMAVT_CTChange(Sender: TField);
var
    x, x1: Double;
begin
    exDotFBMavt(4, RefFB_DM_HH_CT, Sender);
    with QrCT_BO do
    begin
        x := FieldByName('LK_GIABAN_CHUA_VAT').AsFloat;
        x1 := FieldByName('LK_GIABAN').AsFloat;

        FieldByName('LOAITHUE').AsString := QrDMVT.FieldByName('LOAITHUE').AsString;
        FieldByName('VAT_VAO').AsFloat := QrDMVT.FieldByName('VAT_VAO').AsFloat;
        FieldByName('VAT_RA').AsFloat := QrDMVT.FieldByName('VAT_RA').AsFloat;
        FieldByName('DONGIA_CHUA_CK_CHUA_VAT').AsFloat := exVNDRound(x, ctPriceRound);
//        FieldByName('DONGIA_CHUA_CK').AsFloat := exVNDRound(x1, ctPriceRound);
    end;
    QrCT_BODONGIAChange(Sender);
    GrCT.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtBo.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;
end.

