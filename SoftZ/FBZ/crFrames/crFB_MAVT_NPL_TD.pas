﻿unit crFB_MAVT_NPL_TD;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeFB_MAVT_NPL_TD = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameFB_MAVT_NPL_TD: TframeFB_MAVT_NPL_TD;

implementation

{$R *.dfm}

uses
    FB_ChonDsma_MAVT_NPL_TD;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo ngành hàng F&&B';
    RS_TIT1 = 'Theo nhóm hàng F&&B';
    RS_TIT2 = 'Theo mã hàng - Nguyên phụ liệu/ Thực đơn';
    RS_TIT_CAP = '<< %s >>';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT_NPL_TD.LbMaClick(Sender: TObject);
const
    TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT2
    );
var
    n: Integer;
    s: String;
begin
	n := EdMa.Tag;
    if not FrmFB_ChonDsma_MAVT_NPL_TD.Get(n, s) then
    	Exit;

    LbMa.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMa do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT_NPL_TD.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := EdMa.Text;

    n := Length(cr) - GetParamNo;
    cr[n] := EdMa.Tag;
    inc(n);
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'FB_DS_MAVT_NPL_TD' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeFB_MAVT_NPL_TD.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT_NPL_TD.Init;
begin
  inherited;

end;

end.
