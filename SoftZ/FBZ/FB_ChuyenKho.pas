﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChuyenKho;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwstr, ShellAPI,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Mask, Grids,
  Wwdbgrid, ToolWin, wwdbedit, Wwdotdot, Wwdbcomb, frameScanCode,
  FB_frameScanCode, DBCtrlsEh, DBGridEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_ChuyenKho = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepChecked: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXNG_NHAN: TWideStringField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    Label5: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Phiuvnchuynnib1: TMenuItem;
    N2: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TU_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopupMenu2: TAdvPopupMenu;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    CmdExBarcode: TAction;
    Bevel1: TBevel;
    N3: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    QrCTDONGIA_REF: TFloatField;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrCTEX_DATE: TDateTimeField;
    PopDetail: TAdvPopupMenu;
    frScanCode1: TFB_frScanCode;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    CmdEmptyDetail: TAction;
    N5: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrCTQD1: TIntegerField;
    QrNXDGIAI: TWideMemoField;
    QrCTDONGIA_REF2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrCTSOLUONG2: TFloatField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    CmdImportExcel: TAction;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    CmdFromPN: TAction;
    Lytphiunhp1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepBarcode: TToolButton;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    LydliutfileText1: TMenuItem;
    QrCTSOLUONG2_LE: TFloatField;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    QrNXLOCKED_BY: TIntegerField;
    QrNXLK_LOCKED_NAME: TWideStringField;
    QrNXLOCKED_DATE: TDateTimeField;
    CmdCheckton: TAction;
    N7: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    N10: TMenuItem;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTDVT_BOX: TWideStringField;
    DBMemoEh1: TDBMemoEh;
    EdKhoXuat: TDBEditEh;
    CbbKhoXuat: TDbLookupComboboxEh2;
    cbbKhoNhap: TDbLookupComboboxEh2;
    EhKhoNhap: TDBEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbKHOXUATNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure BtnInClick(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbKHOXUATBeforeDropDown(Sender: TObject);
    procedure CbKHOXUATCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdFromPNExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
  private
  	mLCT, mPrefix: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmFB_ChuyenKho: TFrmFB_ChuyenKho;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, isLib, FB_Sapthutu,
    isCommon, GuidEx, isFile, ImportExcel, InlabelChungtu,
  FB_CheckTonkho, OfficeData, FB_ChonDsma, FB_ChonPhieunhap, ExcelData;

{$R *.DFM}

const
	FORM_CODE = 'FB_PHIEU_DIEUKHO';

    (*
    ** Form Events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.Execute;
begin
   	mLCT := 'FCKHO';

	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;
    frScanCode1.Init(QrCT);

    frDate.CbbLoc.Enabled := True;

    mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
  	mTrigger := False;
    mObsolete := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.FormShow(Sender: TObject);
begin
	// Open Database
    Wait(PREPARING);
    InitFmtAndRoud(mLCT);

    with DataMain do
    begin
		OpenDataSets([QrDMKHO, QrFB_DM_HH]);
        SetDisplayFormat(QrFB_DM_HH, sysCurFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
    end;


    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA','DONGIA_LE'], ctPriceFmt);
    end;

    // Customize Grid
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsBarcode then
    begin
        CmdExBarcode.Visible := False;
        SepBarcode.Visible := False;
        frScanCode1.Visible := False;

        grRemoveFields(GrDetail, ['B1']);
    end;
    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if ActiveControl = frScanCode1.EdCode then
        Exit;

	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
    	CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;
    (*
    ** End: Form Events
    *)

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_DIEUKHO_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_DIEUKHO_CT a, FB_DM_HH b where a.KHOA = FB_DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_DIEUKHO_CT where KHOA = FB_DIEUKHO.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
		QrDMVT.Requery;
        QrDMKHO.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
//    frScanCode1.chkCheckExists.Checked := True;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);

//    SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdCancelExecute(Sender: TObject);
begin
    QrCT.CancelBatch;
    QrNX.Cancel;

    if QrNX.IsEmpty then
        ActiveSheet(PgMain, 0)
    else
        ActiveSheet(PgMain);

//    if QrNX.FieldByName('LOCKED_BY').AsInteger = sysLogonUID then
//        SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdCheckedExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdChecktonExecute(Sender: TObject);
begin
     QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
    with QrCT do
	    if FrmFB_CheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdDelExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';

procedure TFrmFB_ChuyenKho.CmdFromPNExecute(Sender: TObject);
var
	n: TGUID;
    mKho, mMavt: String;
    mSoluong, mDongia, mGiavon: Double;
begin
    QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmFB_ChonPhieunhap, FrmFB_ChonPhieunhap);
    with QrNX do
	    n := FrmFB_ChonPhieunhap.Execute(
        	False,
        	'',
            FieldByName('MAKHO').AsString);

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with CHUNGTU_LAYPHIEU do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        mKho := FieldByName('MAKHO').AsString;

        if QrNX.FieldByName('TU_MAKHO').AsString <> mKho then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('TU_MAKHO').AsString := mKho;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mGiavon := FieldByName('GIAVON').AsFloat;

            with QrCT do
//            if not Locate('MAVT', mMavt, []) then
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := exVNDRound(mGiavon, ctPriceRound);
                FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound)
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFile: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if sFile = '' then
    	Exit;

    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';

            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;
                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;

                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if not QrCT.Locate('MAVT', FieldByName(sFld).AsString, []) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                        end
                        else
                            SetEditState(QrCT);
                        QrCT.CheckBrowseMode;
                    end;
                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';
procedure TFrmFB_ChuyenKho.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;

            if FieldByName('TINHTRANG').AsString <> '01' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
    //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
    //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
	with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
    LydliutfileExcel1.Enabled := bEmptyCT;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

	(*
    ** Master Db
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
//        Parameters[4].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_WAREHOUSE = 'Chuyển kho không hợp lệ.';

procedure TFrmFB_ChuyenKho.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'TU_MAKHO', 'MAKHO']) then
	    	Abort;

    	if FieldByName('TU_MAKHO').AsString = FieldByName('MAKHO').AsString then
        begin
        	ErrMsg(RS_INVALID_WAREHOUSE);
            CbbKhoXuat.SetFocus;
            Abort;
        end;

		exValidClosing(FieldByName('NGAY').AsDateTime);
	end;

    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);

//    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(DataSet) then
//        Abort;
//    SetLockedBy(DataSet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrNXSOTIENChange(Sender: TField);
begin
	with QrNX do
    	FieldByName('THANHTOAN').AsFloat := exVNDRound(FieldByName('SOTIEN').AsFloat, ctCurRound);
end;
	(*
    ** End: Master DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat := exVNDRound(
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
        , ctPriceRound);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                sl  := FieldByName('SOLUONG').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SOLUONG2').AsFloat := exVNDRound(sl2, ctQtyRound);
                FieldByName('SOLUONG2_LE').AsFloat := exVNDRound(sl2le, ctQtyRound);
            end
            else if (Sender.FullName = 'SOLUONG2') or (Sender.FullName = 'SOLUONG2_LE') then
            begin
                sl2 := FieldByName('SOLUONG2').AsFloat;
                sl2le := FieldByName('SOLUONG2_LE').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat, ctCurRound);

        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat, ctCurRound);
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE : Double;
begin
	exDotFBMavt(3, DataMain.QrFB_DM_HH, Sender );

    // Update referenced fields
    with QrCT do
    begin
        gia := 0; giaLE := 0;
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DONGIA').AsFloat;
                giaLE := FieldByName('DONGIA_LE').AsFloat;
            end;

            QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
            QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
            QrCT.FieldByName('DVT_BOX').AsString := FieldByName('TenDvtLon').AsString;
            QrCT.FieldByName('QD1').AsInteger := FieldByName('QuyDoi').AsInteger;
        end;

        FieldByName('DONGIA_LE').AsFloat := exVNDRound(giaLE, ctPriceRound);
        FieldByName('DONGIA_REF').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(gia, ctPriceRound);

		if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*
    ** End: Detail DB
    *)

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CbKHOXUATBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CbKHOXUATCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CbKHOXUATNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChuyenKho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;
    (*
    **  End: Others
    *)


end.
