﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Xuattra;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Messages, Windows, System.Variants,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Grids, Wwdbgrid, ToolWin,
  Wwdotdot, Wwdbcomb, Buttons, AdvEdit, DBAdvEd, DBGridEh, DBCtrlsEh,
  DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_Xuattra = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXTL_CK_HD: TFloatField;
    QrNXCHIETKHAU: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXNG_NHAN: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaMaster: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXPTTT: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrCTLK_TENTAT: TWideStringField;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    QrCTEX_DATE: TDateTimeField;
    CHECK_NCC: TADOCommand;
    QrNXHAN_TTOAN: TIntegerField;
    QrNXDA_TTOAN: TBooleanField;
    QrNXLK_HAN_TTOAN: TIntegerField;
    Bevel1: TBevel;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrNXTC_SOTIEN: TFloatField;
    QrCTLOAITHUE: TWideStringField;
    PaHoadon: TisPanel;
    PaHoadon2: TPanel;
    TntLabel2: TLabel;
    TntLabel3: TLabel;
    TntLabel8: TLabel;
    EdSOHD: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    DBText2: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    Label5: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    TntLabel9: TLabel;
    DBMemo2: TDBMemo;
    QrNXHOADON_SO: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_NGAY: TDateTimeField;
    CALC_NGAY_TTOAN: TDateTimeField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    QrCTDONGIA_REF: TFloatField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    N4: TMenuItem;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    N5: TMenuItem;
    CmdThamkhaoGia: TAction;
    hamkhoginhp1: TMenuItem;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrNXSCT2: TWideStringField;
    QrNXHINHTHUC_GIA: TWideStringField;
    QrCTSOTIEN1: TFloatField;
    QrCTQD1: TIntegerField;
    QrNXSOTIEN1: TFloatField;
    QrCTDONGIA_REF2: TFloatField;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    QrNXPHIEUGIAOHANG: TWideStringField;
    wwDBEdit4: TwwDBEdit;
    Label7: TLabel;
    QrNXKHOA: TGuidField;
    QrNXKHOA2: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXCALC_SOTIEN_SAUCK_MH: TFloatField;
    QrNXLOC: TWideStringField;
    ToolButton4: TToolButton;
    CmdDmvt: TAction;
    CmdDmncc: TAction;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    ToolButton5: TToolButton;
    QrCTTINHTRANG: TWideStringField;
    BtnIn2: TToolButton;
    QrNXLYDO: TWideStringField;
    QrNXLK_LYDO: TWideStringField;
    QrCTLK_TENTHUE: TWideStringField;
    QrCTLK_VAT_RA: TFloatField;
    QrCTLK_VAT_VAO: TFloatField;
    QrCTTIEN_THUE_5: TFloatField;
    QrCTTIEN_THUE_10: TFloatField;
    QrCTTIEN_THUE_OR: TFloatField;
    QrNXTHUE_5: TFloatField;
    QrNXTHUE_10: TFloatField;
    QrNXTHUE_OR: TFloatField;
    QrCTGIAVON: TFloatField;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepChecked: TToolButton;
    QrCTSOTIEN2: TFloatField;
    QrCTTL_CK_HD: TFloatField;
    QrCTCHIETKHAU_HD: TFloatField;
    QrCTTHANHTIEN: TFloatField;
    QrNXSOTIEN2: TFloatField;
    QrNXTHANHTIEN: TFloatField;
    QrCTLOC: TWideStringField;
    PaTotal: TPanel;
    ImgTotal: TImage;
    QrCTSOLUONG2_LE: TFloatField;
    QrCTSOTIEN2_TRUOC_CL: TFloatField;
    QrCTSOTIEN2_CL: TFloatField;
    QrCTTIEN_THUE_TRUOC_CL: TFloatField;
    QrCTTIEN_THUE_CL: TFloatField;
    QrCTTHANHTIEN_TRUOC_VAT: TFloatField;
    QrNXSOTIEN2_TRUOC_CL: TFloatField;
    QrNXSOTIEN2_CL: TFloatField;
    QrNXTHUE_TRUOC_CL: TFloatField;
    QrNXTHUE_CL: TFloatField;
    QrNXTHANHTIEN_TRUOC_VAT: TFloatField;
    PaSotien: TPanel;
    Label10: TLabel;
    wwDBEdit9: TwwDBEdit;
    PaSotien1: TPanel;
    Label9: TLabel;
    Label3: TLabel;
    wwDBEdit8: TwwDBEdit;
    wwDBEdit7: TwwDBEdit;
    PaThanhtoan: TPanel;
    Label24: TLabel;
    EdTriGiaTT: TwwDBEdit;
    CbLOAITHUE: TwwDBLookupCombo;
    QrNXSOTIEN_LE: TFloatField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrCTTL_LAI: TFloatField;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    PaSotien2: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    EdTLCK: TwwDBEdit;
    wwDBEdit10: TwwDBEdit;
    PaThue: TPanel;
    BtThue: TSpeedButton;
    Label19: TLabel;
    Label13: TLabel;
    EdTienVAT: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    CmdCheckton: TAction;
    N7: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    N8: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    N9: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    hcn1: TMenuItem;
    CmdDmtd: TAction;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTDVT_BOX: TWideStringField;
    cbbHinhThuc: TDbLookupComboboxEh2;
    CbLyDo: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    CmdXemCongNo: TAction;
    EdMADT: TDBEditEh;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdFromOrderExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrNXAfterEdit(DataSet: TDataSet);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdThamkhaoGiaExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAValidate(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure QrCTLOAITHUEChange(Sender: TField);
    procedure BtThueClick(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrNXTL_CK_HDChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure CmdDmtdExecute(Sender: TObject);
    procedure CmdXemCongNoExecute(Sender: TObject);

  private
	mLCT: String;
	mCanEdit, bDuplicate, bClSotien, bClThue, bCk, bCkHD, mObsolete: Boolean;
	mLydo, mMakho: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

    //Mac dinh cho hinh thuc gia
    fHTGia: String;

	    (*
    	** Functions
	    *)
  public
	procedure Execute(r: WORD);
    procedure escapeKey(pSleepTime: Variant);
  end;

var
  FrmFB_Xuattra: TFrmFB_Xuattra;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, FB_DmvtNVL, FB_Sapthutu, FB_ChonDsma, isLib,
    FB_TheodoiGia, FB_ChonPhieunhap, GuidEx, exThread, isCommon, Dmncc, DmKhNcc,
  Tienthue, CongnoNCC, FB_CheckTonkho, OfficeData, ChonDsma, FB_Dmhh;

{$R *.DFM}

const
	FORM_CODE = 'FB_PHIEU_XUATTRA';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.Execute;
begin
    mLCT := 'FXTRA';

    // Audit setting
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrNX;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    bClSotien := FlexConfigBool(FORM_CODE, 'Chenhlech Sotien');
    bClThue := FlexConfigBool(FORM_CODE, 'Chenhlech Thue');
    bCk := FlexConfigBool(FORM_CODE, 'Chietkhau');
    bCkHD := FlexConfigBool(FORM_CODE, 'Chietkhau Hoadon');

	// Params
    bDuplicate := FlexConfigBool(FORM_CODE, 'Duplicate Mavt');

    //Lay gia tri mac dinh cho HTGia
    fHTGia := sysHTGia;

    if fHTGia <> '' then
    begin
        cbbHinhThuc.Visible := False;
    end;

    // Initial
    mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := RegReadString(Name, 'Lydo', GetSysParam('DEFAULT_PTXUAT'));

  	mTrigger := False;
    mTriggerCK := False;
    mObsolete := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2('_IDI_ISSUE');

	Wait(PREPARING);
    InitFmtAndRoud(mLCT);
    ctCurVatRound := -2;
    ctCurVatFmt := '#,##0.00;-#,##0.00;#';

    // Open database
	with DataMain do
    begin
		OpenDataSets([QrFB_DM_NPL_THUCDON, QrPTXUATTRA, QrDMLOAITHUE, QrDMNCC, QrDMKHO, QrHINHTHUC_GIA]);
        SetDisplayFormat(QrFB_DM_NPL_THUCDON, sysCurFmt);
        SetDisplayFormat(QrFB_DM_NPL_THUCDON,['TL_LAI'], sysPerFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, ctCurFmt);
        SetDisplayFormat(QrNX, ['TL_CK_HD'], sysPerFmt);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, ctCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG', 'SOLUONG2', 'SOLUONG2_LE', 'TL_CK', 'TL_CK_HD'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA', 'DONGIA2', 'DONGIA_REF', 'DONGIA_REF2',
                                     'DONGIA_LE'], ctPriceFmt);
        SetDisplayFormat(QrCT, ['THUE_SUAT'], sysTaxFmt);
        SetDisplayFormat(QrCT, ['TIEN_THUE_TRUOC_CL', 'TIEN_THUE_CL', 'TIEN_THUE'], ctCurVatFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not bCk then
    begin
        PaSotien1.Visible := False;
        grRemoveFields(GrDetail, ['TL_CK', 'CHIETKHAU', 'SOTIEN1']);
    end;

    if not bCkHD then
    begin
        PaSotien2.Visible := False;
        grRemoveFields(GrDetail, ['TL_CK_HD', 'CHIETKHAU_HD', 'SOTIEN2_TRUOC_CL']);
    end;

    if not bClSotien then
    begin
        grRemoveFields(GrDetail, ['SOTIEN2_CL', 'SOTIEN2']);
    end;

    if not sysIsThue then
    begin
        PaThue.Visible := False;
        PaHoadon.Visible := False;
        grRemoveFields(GrBrowse, ['THUE', 'HOADON_SO', 'HOADON_SERI', 'HOADON_NGAY']);
        grRemoveFields(GrDetail, ['THANHTIEN_TRUOC_VAT', 'LK_TENTHUE', 'THUE_SUAT',
                'TIEN_THUE_TRUOC_CL', 'THANHTIEN']);
    end;

    if not bClThue then
    begin
        grRemoveFields(GrDetail, ['TIEN_THUE_CL', 'TIEN_THUE']);
    end;

    if not sysIsThue then
    begin
        PaThue.Visible := False;
        PaHoadon.Visible := False;
        grRemoveFields(GrBrowse, ['THUE', 'HOADON_SO', 'HOADON_SERI', 'HOADON_NGAY']);
        grRemoveFields(GrDetail, ['LK_TENTHUE', 'THUE_SUAT', 'TIEN_THUE', 'THANHTIEN']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    PaSotien.Visible := bCk or bCkHD or sysIsThue;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Lydo'], [mMakho, mLydo]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
	** Actions
	*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

	with DataMain do
    begin
    	QrFB_DM_HH.Requery;
        QrPTNHAP.Requery;
        QrDMNCC.Requery;
        QrDMKHO.Requery;
    end;
            
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_CHUNGTU_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_CHUNGTU_CT a, FB_DM_HH b where a.KHOA = FB_CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_CHUNGTU_CT where KHOA = FB_CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
    	    if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdSapthutuExecute(Sender: TObject);
begin
	CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
    with QrCT do
	    if FrmFB_CheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdDmnccExecute(Sender: TObject);
var
    r: WORD;
    mRet: Boolean;
    loai: Integer;
begin
    if GetFuncState('SZ_DM_KH_NCC') then
    begin
        r := GetRights('SZ_DM_KH_NCC');
        if r = R_DENY then
            Exit;

        loai := -1;
    end else
    begin
        r := GetRights('SZ_DM_NCC');
        if r = R_DENY then
            Exit;

        loai := 1;
    end;

    Application.CreateForm(TFrmDmKhNcc, FrmDmKhNcc);
    FrmDmKhNcc.Execute(r, loai, False);

    if mRet then
        DataMain.QrDmncc.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdDmtdExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    	DataMain.QrFB_DM_NPL_THUCDON.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    	DataMain.QrFB_DM_NPL_THUCDON.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		EdTLCK.SetFocus;
        except
        end
    else if (ActiveControl = PaTotal) or (ActiveControl.Parent = PaTotal) then
		try
    		CbNgay.SetFocus;
        except
        end
    else if (ActiveControl = PaHoadon2) or (ActiveControl.Parent = PaHoadon2) then
        GrDetail.SetFocus
    else
    	try
    		EdSOHD.SetFocus;
        except
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsNX)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdThamkhaoGiaExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_TheodoiGia, FrmFB_TheodoiGia);
    with QrNX do
	    FrmFB_TheodoiGia.Execute('FNMUA',
            QrCT.FieldByName('MAVT').AsString,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdXemCongNoExecute(Sender: TObject);
begin
   if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';

procedure TFrmFB_Xuattra.CmdFromOrderExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue, chungTu: String;
    mSoluong, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmFB_ChonPhieunhap, FrmFB_ChonPhieunhap);
    with QrNX do
	    n := FrmFB_ChonPhieunhap.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString);

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with CHUNGTU_LAYPHIEU do
    begin
        chungTu := TGuidEx.ToString(n);
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        // Cap nhat nha cung cap
        mNcc := FieldByName('MADT').AsString;
        mKho := FieldByName('MAKHO').AsString;
        mSCT2 := FieldByName('SCT').AsString;
        mHTGia := FieldByName('HINHTHUC_GIA').AsString;
        mTlckHD := FieldByName('TL_CK_HD').AsFloat;

        if (QrNX.FieldByName('MADT').AsString <> mNcc) or
           (QrNX.FieldByName('MAKHO').AsString <> mKho) then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('MADT').AsString := mNcc;
	        QrNX.FieldByName('MAKHO').AsString := mKho;
            QrNX.FieldByName('HINHTHUC_GIA').AsString := mHTGia;
            QrNX.FieldByName('TL_CK_HD').AsFloat := mTlckHD;
//            QrNX.FieldByName('SCT2').AsString := mSCT2;
//            QrNX.FieldByName('KHOA2').AsInteger := n;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mTlck := FieldByName('TL_CK').AsFloat;
            mLThue := FieldByName('LOAITHUE').AsString;

            with QrCT do
            if not Locate('MAVT', mMavt, []) then
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := exVNDRound(mDongia, ctPriceRound);
                FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
                FieldByName('TL_CK').AsFloat := exVNDRound(mTlck, sysPerRound);
                FieldByName('LOAITHUE').AsString := mLThue;
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and isnull(BO, 0) = 0 ') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	// Master
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdPrint.Enabled := (not bEmpty) and (not bDeleted);
    CmdReRead.Enabled := bBrowse;
	CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdTotal.Enabled  := n = 1;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdDmvt.Enabled := n = 1;
    cmdDmtd.Enabled := n = 1;

	// Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;

    CmdEmptyDetail.Enabled := not bEmptyCT;
    CmdThamkhaoGia.Enabled := not bEmptyCT;
    cbbHinhThuc.ReadOnly := not bEmptyCT;

    CmdCheckton.Enabled := not bEmptyCT;
end;

    (*
    **  Master DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('LYDO').AsString          := mLydo;
		FieldByName('MAKHO').AsString         := mMakho;
        FieldByName('LOC').AsString           := sysLoc;
//		FieldByName('DA_TTOAN').AsBoolean     := False;
        FieldByName('HINHTHUC_GIA').AsString  := Iif(fHTGia = '', '03', fHTGia);
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXAfterEdit(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['NGAY', 'MAKHO', 'LYDO', 'MADT']) then
        	Abort;
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;

 	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		mMakho := FieldByName('MAKHO').AsString;
        mLydo := FieldByName('LYDO').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	with DataSet do
    begin
		// Validate: da co phieu thu / chi
    	if FieldByName('TC_SOTIEN').AsFloat <> 0 then
	    begin
    	    ErrMsg(RS_DA_THUCHI);
        	Abort;
	    end;

    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}

    with QrNX do
    begin
        FieldByName('CALC_NGAY_TTOAN').AsDateTime :=
            FieldByName('NGAY').AsDateTime +
            FieldByName('HAN_TTOAN').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    mThanhtien, mThanhtoan: Double;
    mHinhthuc: String;
begin
    with Sender.DataSet do
    begin
        mHinhthuc := FieldByName('HINHTHUC_GIA').AsString;
        mThanhtien := FieldByName('THANHTIEN').AsFloat;
        mThanhtoan := mThanhtien + FieldByName('CL_THUE').AsFloat;

        FieldByName('THANHTOAN').AsFloat := exVNDRound(mThanhtoan, ctCurRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXHINHTHUC_GIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if QrCT.IsEmpty then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXNGAYValidate(Sender: TField);
begin
	with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrNXTL_CK_HDChange(Sender: TField);
var
    bTrigger: Boolean;
    tlck, ck, sotien1: Double;
begin
    if mTrigger then
    	Exit;

	with QrNX do
    begin
        bTrigger := mTrigger;
        mTrigger := True;

        sotien1 := FieldByName('SOTIEN1').AsFloat;
        if Sender.FieldName = 'CHIETKHAU_HD' then
        begin
            ck := FieldByName('CHIETKHAU_HD').AsFloat;
            if ck = 0 then
                tlck := 0
            else
                tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, Sotien1)));

            FieldByName('TL_CK_HD').AsFloat := exVNDRound(tlck, sysPerRound);
        end else
        begin
            tlck := FieldByName('TL_CK_HD').AsFloat;
            ck := exVNDRound(sotien1 * tlck / 100.0, ctCurRound);

            FieldByName('CHIETKHAU_HD').AsFloat := exVNDRound(ck, ctCurRound);
        end;

        mTrigger := bTrigger;
    end;

    if Sender.FieldName <> 'SOTIEN1' then
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('TL_CK_HD').AsFloat := exVNDRound(tlck, sysPerRound);

            Next;
        end;

        CheckBrowseMode;
    end;
end;

(*
    ** End: Master DB
    *)

    (*
    **  Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if not bDuplicate then
        begin
            with QrCT do
            begin
                if IsDuplicateCode(QrCT, FieldByName('MAVT'), True) then
                    Abort;
            end;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
   		Abort;
    SetEditState(QrNX);
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if BlankConfirm(QrNX, ['HINHTHUC_GIA']) then
        Abort;

    if not bDuplicate then
    begin
        with QrCT do
        begin
            if IsDuplicateCode(QrCT, FieldByName('MAVT'), True) then
                Abort;
        end;
    end;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat := exVNDRound(
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            , ctPriceRound);
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTLOAITHUEChange(Sender: TField);
begin
    with QrCT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
            FieldByName('THUE_SUAT').Clear
        else
            FieldByName('THUE_SUAT').AsFloat := exVNDRound(FieldByName('LK_VAT_VAO').AsFloat, sysTaxRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                sl := exVNDRound(Sender.AsFloat, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := sl;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SOLUONG2').AsFloat := exVNDRound(sl2, ctQtyRound);
                FieldByName('SOLUONG2_LE').AsFloat := exVNDRound(sl2le, ctQtyRound);
            end
            else if (Sender.FullName = 'SOLUONG2') or (Sender.FullName = 'SOLUONG2_LE') then
            begin

                sl2 := exVNDRound(FieldByName('SOLUONG2').AsFloat, ctQtyRound);
                sl2le := exVNDRound(FieldByName('SOLUONG2_LE').AsFloat, ctQtyRound);

                FieldByName('SOLUONG2').AsFloat := sl2;
                FieldByName('SOLUONG2_LE').AsFloat := sl2le;

                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat, ctCurRound);

        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat, ctCurRound);
        FieldByName('SOTIEN').AsFloat := d;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTSOTIENChange(Sender: TField);
var
    mHinhthuc: String;
    tlck, tlckhd, mCk, mCkhd, mTs, mSl,
        mSotien,
        mSotien1, //Sau chiết khấu mặt hàng
        Sotien2TrCl, Sotien2Cl, mSotien2, //Sau chiết khấu hóa đơn
        ThanhtienTrT, thueTrCL, thueCl, mThue, mThanhtien: Double;
begin
    if mTriggerCK then
        Exit;

	with QrCT do
    begin
        mHinhthuc := QrNX.FieldByName('HINHTHUC_GIA').AsString;
        mSotien := FieldByName('SOTIEN').AsFloat;
        mTs := FieldByName('THUE_SUAT').AsFloat;

        if Sender.FieldName = 'CHIETKHAU' then
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            tlck := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end
        else
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            mCk := exVNDRound(mSotien * tlck / 100.0)
        end;

        mSotien1 := mSotien -  mCk;
        mCkhd := exVNDRound( mSotien1 * FieldByName('TL_CK_HD').AsFloat / 100.0, ctCurRound);

        Sotien2TrCl := mSotien1 -  mCkhd;
        Sotien2Cl := FieldByName('SOTIEN2_CL').AsFloat;
        mSotien2 := Sotien2TrCl + Sotien2Cl;

        if mHinhthuc = '02' then          // Xuoi
        begin
        	// Thue
            thueTrCL := exVNDRound(mSotien2 * mTs / 100.0, ctCurRound);
            thueCl := FieldByName('TIEN_THUE_CL').AsFloat;
            mThue := thueTrCL + thueCl;

            ThanhtienTrT := mSotien2;
            mThanhtien := mSotien2 + mThue;
        end
        else if mHinhthuc = '03' then     // Nguoc
        begin
            // Thue
            if mTs = 0 then
                thueTrCL := 0
            else
                thueTrCL := exVNDRound(mSotien2 / (100/mTs + 1), ctCurRound);

            thueCl := FieldByName('TIEN_THUE_CL').AsFloat;
            mThue := thueTrCL + thueCl;

            ThanhtienTrT := mSotien2 - mThue;
            mThanhtien := mSotien2;
        end;

    	mTriggerCK := True;
        FieldByName('TL_CK').AsFloat := exVNDRound(tlck, sysPerRound);
    	FieldByName('CHIETKHAU').AsFloat := exVNDRound(mCk, ctCurRound);
    	mTriggerCK := False;
        FieldByName('SOTIEN1').AsFloat := exVNDRound(mSotien1, ctCurRound);

        FieldByName('CHIETKHAU_HD').AsFloat := mCkhd;
        FieldByName('SOTIEN2_TRUOC_CL').AsFloat := exVNDRound(Sotien2TrCl, ctCurRound);
        FieldByName('SOTIEN2').AsFloat := exVNDRound(mSotien2, ctCurRound);

        FieldByName('THANHTIEN_TRUOC_VAT').AsFloat := exVNDRound(ThanhtienTrT, ctCurRound);
        FieldByName('TIEN_THUE_TRUOC_CL').AsFloat := exVNDRound(thueTrCL, ctCurVatRound);

        mThue := exVNDRound(mThue, ctCurVatRound);

        if mTs = 5 then
        begin
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_5').AsFloat := mThue;
        end
        else if mTs = 10 then
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := mThue;
        end else
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := mThue;
        end;

        FieldByName('TIEN_THUE').AsFloat := mThue;
        FieldByName('THANHTIEN').AsFloat := exVNDRound(mThanhtien, ctCurRound);
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.QrCTMAVTChange(Sender: TField);
var
	ck, gia, giaSI, giaLE : Double;
    lthue: String;
begin
    exDotFBMavt(4, DataMain.QrFB_DM_NPL_THUCDON, Sender);

    // Update referenced fields
    with QrCT do
    begin
        ck := 0;  gia := 0; giaSI := 0; giaLE := 0; lthue := 'T00';
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;
            Parameters[3].Value := QrNX.FieldByName('MADT').AsString;
            Parameters[4].Value := QrNX.FieldByName('HINHTHUC_GIA').AsString;

            Open;
            if RecordCount <> 0 then
            begin
                ck := FieldByName('TL_CK').AsFloat;
                gia := FieldByName('DONGIA').AsFloat;
                giaLE := FieldByName('DONGIA_LE').AsFloat;
                lthue := FieldByName('LOAITHUE').AsString;

                QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
                QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
                QrCT.FieldByName('DVT_BOX').AsString := FieldByName('TenDvtLon').AsString;
                QrCT.FieldByName('QD1').AsInteger := FieldByName('QuyDoi').AsInteger;
            end;
        end;

        FieldByName('LOAITHUE').AsString := lthue;
        FieldByName('DONGIA_LE').AsFloat := exVNDRound(giaLE, ctPriceRound);
        FieldByName('DONGIA_REF').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('TL_CK').AsFloat := exVNDRound(ck, sysPerRound);

        FieldByName('TL_CK_HD').AsFloat := exVNDRound(QrNX.FieldByName('TL_CK_HD').AsFloat, sysPerRound);
	end;

	GrDetail.InvalidateCurrentRow;
end;

	(*
    ** End: Detail DB
    *)

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('CHIETKHAU').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('CHIETKHAU_MH').AsFloat);
        ColumnByName('CHIETKHAU_HD').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('CHIETKHAU_HD').AsFloat);
		ColumnByName('TIEN_THUE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('THUE').AsFloat);
        ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN1').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN1').AsFloat);
        ColumnByName('SOTIEN2').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN2').AsFloat);
        ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('THANHTIEN').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.BtThueClick(Sender: TObject);
var
    p: TPoint;
begin
    p.x := GrDetail.Left + GrDetail.Width;
    p.y := PaTotal.Top;
    p := ClientToScreen(p);

    Application.CreateForm(TFrmTienthue, FrmTienthue);
    with FrmTienthue do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y - Height + 2;
    	Execute(DsNX);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Xuattra.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

end.
