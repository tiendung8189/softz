﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Khuyenmai3;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls, Wwmemo, Graphics,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwdbgrid,
  frameNgay, frameNavi, isDb, Variants,
  isPanel, wwDialog, Mask, Grids, ToolWin, Wwdotdot, Wwdbcomb, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_Khuyenmai3 = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrKM: TADOQuery;
    DsKM: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKMXOA: TWideStringField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrKMIMG: TIntegerField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    CmdAudit: TAction;
    QrKMNGAY: TDateTimeField;
    QrKMLCT: TWideStringField;
    QrKMSCT: TWideStringField;
    QrKMTUNGAY: TDateTimeField;
    QrKMDENNGAY: TDateTimeField;
    QrKMLYDO: TWideStringField;
    QrKMDGIAI: TWideMemoField;
    QrKMCREATE_BY: TIntegerField;
    QrKMCREATE_DATE: TDateTimeField;
    QrKMUPDATE_BY: TIntegerField;
    QrKMUPDATE_DATE: TDateTimeField;
    QrKMDELETE_BY: TIntegerField;
    QrKMDELETE_DATE: TDateTimeField;
    TntLabel1: TLabel;
    EdLyDo: TwwDBEdit;
    Label5: TLabel;
    CbTungay: TwwDBDateTimePicker;
    TntLabel2: TLabel;
    CbDenngay: TwwDBDateTimePicker;
    Label10: TLabel;
    DBEdit1: TDBMemo;
    CmdUpdate: TAction;
    TntToolButton2: TToolButton;
    QrKMDENGIO: TDateTimeField;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    wwDBEdit1: TwwDBDateTimePicker;
    QrKMTUGIO: TDateTimeField;
    wwDBEdit2: TwwDBDateTimePicker;
    CmdDiemban: TAction;
    TntToolButton4: TToolButton;
    Status2: TStatusBar;
    Filter2: TwwFilterDialog2;
    CmdListRefesh: TAction;
    QrKhuyenMai: TADOQuery;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    QrKMDENNGAY2: TDateTimeField;
    PgGiatri: TisPanel;
    GrDieukien: TwwDBGrid2;
    Splitter1: TSplitter;
    QrKMDieukien: TADOQuery;
    DsKMDieukien: TDataSource;
    PaKM: TisPanel;
    GrChitiet: TwwDBGrid2;
    Bevel3: TSplitter;
    Splitter2: TSplitter;
    QrKMChitiet: TADOQuery;
    DsKMChitiet: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    popDetail: TAdvPopupMenu;
    CmdEmpty: TAction;
    Xachitit1: TMenuItem;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    CmdChecked: TAction;
    QrKMCHECKED: TBooleanField;
    QrKMKHOA: TGuidField;
    QrKMLOC: TWideStringField;
    QrKMCHECKED_BY: TIntegerField;
    QrKMCHECKED_DATE: TDateTimeField;
    QrKMCHECKED_DESC: TWideMemoField;
    Label4: TLabel;
    cbNgayKhoa: TwwDBDateTimePicker;
    Label6: TLabel;
    EdCheckedDate: TwwDBComboDlg;
    QrKMNOIDUNG: TWideStringField;
    QrDoituong: TADOQuery;
    QrDieukien: TADOQuery;
    Panel2: TPanel;
    Panel1: TPanel;
    Panel4: TPanel;
    QrKMHTKM: TWideStringField;
    QrKMKHUYENMAI_DOITUONG: TWideStringField;
    QrKMKHUYENMAI_DIEUKIEN: TWideStringField;
    QrNganh: TADOQuery;
    QrNhom: TADOQuery;
    QrKMDieukienRSTT: TIntegerField;
    QrKMChitietRSTT: TIntegerField;
    QrKMDieukienTU: TFloatField;
    QrKMDieukienDEN: TFloatField;
    QrKMChitietTL_CK: TFloatField;
    QrKMChitietSOLUONG: TFloatField;
    QrKMDieukienKHOACT: TGuidField;
    QrKMDieukienKHOA: TGuidField;
    QrKMDieukienSTT: TIntegerField;
    QrKMDieukienGHICHU: TWideStringField;
    QrKMChitietMAVT: TWideStringField;
    QrKMChitietTENVT: TWideStringField;
    QrKMChitietSOTIEN: TFloatField;
    QrKMChitietGHICHU: TWideStringField;
    QrKMChitietLK_DVT: TWideStringField;
    QrKMDieukienLOC: TWideStringField;
    QrKMChitietKHOACT: TGuidField;
    QrKMChitietKHOA: TGuidField;
    QrKMChitietKHOA_DIEUKIEN: TGuidField;
    QrKMChitietSTT: TIntegerField;
    QrKMChitietLOC: TWideStringField;
    QrKMLK_LYDO: TWideStringField;
    QrKMDT: TADOQuery;
    IntegerField1: TIntegerField;
    DsKMDT: TDataSource;
    QrKMDTSTT: TIntegerField;
    QrKMDTMAVT: TWideStringField;
    QrKMDTTENVT: TWideStringField;
    QrKMDTMANGANH: TWideStringField;
    QrKMDTMANHOM: TWideStringField;
    QrKMDTMADT: TWideStringField;
    QrKMDTGHICHU: TWideStringField;
    GrKMDT: TwwDBGrid2;
    QrKMDTLK_TENDT: TWideStringField;
    QrKMDTLK_TENNGANH: TWideStringField;
    QrKMDTLK_TENNHOM: TWideStringField;
    QrKMDTLK_PREFIX: TWideStringField;
    QrKMDTLK_DVT: TWideStringField;
    QrKMDTLK_TINHTRANG: TWideStringField;
    QrKMDTKHOACT: TGuidField;
    QrKMDTKHOA: TGuidField;
    QrKMDTLOC: TWideStringField;
    MemoDate: TwwMemoDialog;
    CmdEmptyDieuKien: TAction;
    CmdEmptyKhuyenMai: TAction;
    popDetailDieuKien: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    popDetailKhuyenMai: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    PopDanhmuc: TAdvPopupMenu;
    NguynphliuFB1: TMenuItem;
    CmdNPL: TMenuItem;
    CmdDMTD: TAction;
    CmdDMNPL: TAction;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    QrKMDieukienTL_CK: TFloatField;
    QrKMDieukienSOTIEN: TFloatField;
    QrKMDTLK_GIABAN: TFloatField;
    QrKMChitietGIABAN: TFloatField;
    CbNhom: TwwDBComboBox;
    QrKMChitietNHOM: TIntegerField;
    cbbLyDo: TDbLookupComboboxEh2;
    DsDoituong: TDataSource;
    DsDieukien: TDataSource;
    DsKhuyenMai: TDataSource;
    CbbDoituong: TDbLookupComboboxEh2;
    CbbDieuKien: TDbLookupComboboxEh2;
    CbbKhuyenMai: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrKMAfterInsert(DataSet: TDataSet);
    procedure QrKMBeforeOpen(DataSet: TDataSet);
    procedure QrKMBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKMBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKMCalcFields(DataSet: TDataSet);
    procedure QrKMBeforeEdit(DataSet: TDataSet);
    procedure QrKMAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrKMAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDiembanExecute(Sender: TObject);
    procedure CbLydoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure QrKMDENNGAY2Change(Sender: TField);
    procedure QrKMDieukienBeforePost(DataSet: TDataSet);
    procedure QrKMChitietBeforePost(DataSet: TDataSet);
    procedure QrChitietMAVTChange(Sender: TField);
    procedure QrKMDieukienBeforeDelete(DataSet: TDataSet);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrKMDieukienBeforeOpen(DataSet: TDataSet);
    procedure QrKMChitietBeforeOpen(DataSet: TDataSet);
    procedure QrKMChitietAfterInsert(DataSet: TDataSet);
    procedure QrKMDieukienAfterScroll(DataSet: TDataSet);
    procedure QrKMDTBeforeOpen(DataSet: TDataSet);
    procedure QrKMDTCalcFields(DataSet: TDataSet);
    procedure QrKMChitietCalcFields(DataSet: TDataSet);
    procedure QrKMDieukienCalcFields(DataSet: TDataSet);
    procedure QrKMDoituongNCCBeforeEdit(DataSet: TDataSet);
    procedure QrKMDoituongAfterInsert(DataSet: TDataSet);
    procedure QrKMHTKMChange(Sender: TField);
    procedure QrKMDTBeforePost(DataSet: TDataSet);
    procedure QrKMDoituongBeforeDelete(DataSet: TDataSet);
    procedure QrKMChitietMAVTChange(Sender: TField);
    procedure QrKMKHUYENMAI_DOITUONGChange(Sender: TField);
    procedure QrKMDTMAVTChange(Sender: TField);
    procedure QrKMDTMADTChange(Sender: TField);
    procedure QrKMDTMANGANHChange(Sender: TField);
    procedure QrKMDTMANHOMChange(Sender: TField);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure QrKMDieukienAfterInsert(DataSet: TDataSet);
    procedure QrKMChitietBeforeDelete(DataSet: TDataSet);
    procedure QrKMChitietBeforeInsert(DataSet: TDataSet);
    procedure QrKMDTBeforeDelete(DataSet: TDataSet);
    procedure QrKMDTBeforeEdit(DataSet: TDataSet);
    procedure QrKMDTBeforeInsert(DataSet: TDataSet);
    procedure QrKMDTAfterInsert(DataSet: TDataSet);
    procedure MemoDateInitDialog(Dialog: TwwMemoDlg);
    procedure EdCheckedDateCustomDlg(Sender: TObject);
    procedure CmdEmptyDieuKienExecute(Sender: TObject);
    procedure CmdEmptyKhuyenMaiExecute(Sender: TObject);
    procedure CmdDMNPLExecute(Sender: TObject);
    procedure CmdDMTDExecute(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure QrKMChitietSOLUONGChange(Sender: TField);
    procedure QrKMChitietNhomBeforeOpen(DataSet: TDataSet);
    procedure QrKMChitietMAVTValidate(Sender: TField);
    procedure QrKMKHUYENMAI_DIEUKIENChange(Sender: TField);
    
  private
	mCanEdit, mDeleteAllConditionConfirm: Boolean;
   	fTungay, fDenngay: TDateTime;

    // List filter
    fType: Integer;
    mLCT, fSQL, fStr, mLDT, mLKM: String;
    procedure getComboboxGroup();
    procedure getLayoutPartner();
    procedure getLayoutCondition();
    procedure clearColumnCondition();
  public
	procedure Execute(r: WORD);
  end;

var
  FrmFB_Khuyenmai3: TFrmFB_Khuyenmai3;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, Math, isCommon,
    FB_ChonDsma, isLib, Khuyenmai2, ChonDsNCC, ChonDsNhom, GuidEx, isStr, FB_Dmhh, FB_DmvtNVL;

{$R *.DFM}

const
	FORM_CODE = 'FB_KHUYENMAI_DACHIEU';

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.EdCheckedDateCustomDlg(Sender: TObject);
begin
    EdCheckedDate.SelectAll;
	MemoDate.Execute;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.Execute;
begin
    mLCT := 'FKMAI2';
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrKMDT.ReadOnly := not mCanEdit;
    GrDieukien.ReadOnly := not mCanEdit;
    GrChitiet.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
    RS_CONDITION_NO_DATA = 'Điều kiện khuyến mãi không có dữ liệu';
    RS_CONDITION_INVAILD = 'Điều kiện khuyến mãi không hợp lệ';

procedure TFrmFB_Khuyenmai3.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrKM;

    // Display Format
    with QrKM do
    begin
	    SetShortDateFormat(QrKM);
    	SetDisplayFormat(QrKM, ['TUGIO', 'DENGIO'], 'hh:nn');
        SetDisplayFormat(QrKM, ['NGAY', 'CHECKED_DATE'], DateTimeFmt);
    end;

    SetDisplayFormat(QrKMDT, sysCurFmt);
    SetDisplayFormat(QrKMDieukien, sysCurFmt);
    SetDisplayFormat(QrKMDieukien, ['TL_CK'], sysPerFmt);

	SetDisplayFormat(QrKMChitiet, sysCurFmt);
    SetDisplayFormat(QrKMChitiet, ['SOLUONG'], ctQtyFmt);

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_DK', FORM_CODE + '_KM'], [GrBrowse, GrDieukien, GrChitiet]);
    SetDictionary([QrKM, QrKMDieukien, QrKMChitiet], [FORM_CODE, FORM_CODE + '_DK', FORM_CODE + '_KM'], [Filter, nil, nil]);

    SetCustomGrid([FORM_CODE + '_DT_NCC', FORM_CODE + '_DT_NGANH', FORM_CODE + '_DT_NHOM', FORM_CODE + '_DT_HH'], [GrKMDT, GrKMDT, GrKMDT, GrKMDT]);


    SetCustomGrid([FORM_CODE + '_DK_TL', FORM_CODE + '_DK_HD'], [GrDieukien, GrDieukien]);


    getComboboxGroup();

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrKM.SQL.Text;
  	mTrigger := False;
    mDeleteAllConditionConfirm := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

	Wait(PREPARING);
    InitFmtAndRoud(mLCT);

    with DataMain do
    begin
        OpenDataSets([QrLYDO_KM, QrFB_DM_THUCDON, QrDMNCC]);
        SetDisplayFormat(QrFB_DM_THUCDON, sysCurFmt);
    end;

    OpenDataSets([QrDoituong, QrDieukien, QrKhuyenMai, QrNganh, QrNhom]);

    ClearWait;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrKM, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDoituong, QrDieukien, QrKhuyenMai, QrNganh, QrNhom]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;

		Screen.Cursor := crSQLWait;
		with QrKM do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_KHUYENMAI_CT a, FB_DM_THUCDON b, DM_NHOM c where a.KHOA = FB_KHUYENMAI.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_KHUYENMAI_CT a, FB_DM_THUCDON b where a.KHOA = FB_KHUYENMAI.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_KHUYENMAI_CT where KHOA = FB_KHUYENMAI.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
            
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdNewExecute(Sender: TObject);
begin
	QrKM.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdSaveExecute(Sender: TObject);
begin
    QrKMChitiet.CheckBrowseMode;
    QrKMDieukien.CheckBrowseMode;
    QrKMDT.CheckBrowseMode;
    QrKM.Post;
    exSaveDetails(QrKMDT);
    exSaveDetails(QrKMDieukien);
    with QrKMChitiet do
    begin
        Filtered := False;
        UpdateBatch;
        Filtered := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdCancelExecute(Sender: TObject);
begin
    QrKMChitiet.CancelBatch;
    QrKMDieukien.CancelBatch;
    QrKMDT.CancelBatch;
	QrKM.Cancel;

    if QrKM.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsKM)
    else
        exSearch(Name + '_CT', DsKMChitiet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and MANGANH in (select MANGANH from DM_NGANH where LOAI IN(''FBTP'')) AND isnull(BO, 0) = 0') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrKM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrKM);

    CmdChecked.Enabled := not bEmpty;
    CmdChecked.Caption := exGetCheckedCaption(QrKM);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdUpdate.Enabled := mCanEdit and not bEmpty and bBrowse;
    CmdDiemban.Enabled := mCanEdit and not bEmpty and bBrowse;

    CmdDMNPL.Enabled := n = 1;
    CmdDMTD.Enabled := n = 1;

    with QrKMDT do
    begin
        if not Active then
        	Exit;
        CbbDoituong.Enabled := IsEmpty;
    end;

    with QrKMDieukien do
    begin
        if not Active then
        	Exit;
        CbbDieuKien.Enabled := IsEmpty;
    end;

    GrChitiet.ReadOnly := QrKM.FieldByName('HTKM').AsString <> '03';

end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrKM do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime    := d;
        FieldByName('TUNGAY').AsDateTime  := d;
        FieldByName('DENNGAY2').AsDateTime := d;
        FieldByName('TUGIO').AsDateTime   := EncodeTime(0,0,0,0);
        FieldByName('DENGIO').AsDateTime  := 1 + EncodeTime(0,0,0,0);
        FieldByName('LCT').AsString := mLCT;
        FieldByName('KHUYENMAI_DOITUONG').AsString := '01';
        FieldByName('KHUYENMAI_DIEUKIEN').AsString := '01';
        FieldByName('HTKM').AsString := '01';
        FieldByName('LOC').AsString := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDENNGAY2Change(Sender: TField);
begin
    with QrKM do
        FieldByName('DENNGAY').AsDateTime := FieldByName('DENNGAY2').AsDateTime;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMAfterCancel(DataSet: TDataSet);
begin
    if DataSet.Eof then
    with PgMain do
    begin
        ActivePageIndex := 0;
        OnChange(Nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMBeforeOpen(DataSet: TDataSet);
begin
	with QrKM do
    begin
		Parameters[0].Value := fTungay;
		Parameters[1].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMBeforePost(DataSet: TDataSet);
var
	dengio: TDateTime;
begin
	with QrKM do
    begin
		if BlankConfirm(QrKM, ['NGAY', 'TUNGAY', 'DENNGAY', 'LYDO', 'NOIDUNG']) then
	    	Abort;

	    exValidClosing(FieldByName('NGAY').AsDateTime);

	    // Adjust hours
        if FieldByName('TUGIO').IsNull then
            FieldByName('TUGIO').AsDateTime := 0;

	    dengio := Frac(FieldByName('DENGIO').AsDateTime);
        if dengio <> 0 then
        	FieldByName('DENGIO').AsDateTime := dengio
        else
        	FieldByName('DENGIO').AsDateTime := 1;

        if FieldByName('TUGIO').AsDateTime > FieldByName('DENGIO').AsDateTime then
        begin
        	ErrMsg('Giờ khuyến mãi không hợp lệ.');
        	Abort;
        end;
	end;

    DataMain.AllocSCT('FKM', QrKM);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMBeforeEdit(DataSet: TDataSet);
begin
    if not mTrigger then
    	exIsChecked(DataSet);

	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai3.QrKMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietAfterInsert(DataSet: TDataSet);
begin
    with QrKMChitiet do
    begin
        FieldByName('KHOA_DIEUKIEN').Value := QrKMDieukien.FieldByName('KHOACT').Value;
        TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        FieldByName('LOC').AsString := QrKM.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    with QrKMDieukien do
    begin
        if IsEmpty then
        begin
           ErrMsg(RS_CONDITION_NO_DATA);
           Abort;
        end;
    end;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietBeforePost(DataSet: TDataSet);
begin
    with QrKMChitiet do
    begin
         if (QrKM.FieldByName('HTKM').AsString = '03') and BlankConfirm(QrKMChitiet , ['MAVT', 'SOLUONG']) then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    if State in [dsInsert] then
    else
    begin
        FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietMAVTChange(Sender: TField);
begin
     exDotFBMavt(4, DataMain.QrFB_DM_THUCDON, Sender);
     if Sender.AsString <> '' then
     begin
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;

            Open;
            QrKMChitiet.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
            QrKMChitiet.FieldByName('GIABAN').AsFloat := FieldByName('DONGIA_LE').AsFloat;

        end;

        GrChitiet.InvalidateCurrentRow;
     end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietMAVTValidate(Sender: TField);
var
    mMAVT, mKHOACT: string;
begin
    with QrKMChitiet do
    begin
         mMAVT := FieldByName('MAVT').AsString;
         mKHOACT :=  TGuidEx.ToString(TGuidField(QrKMDieukien.FieldByName('KHOACT')).AsGuid);
         if IsDuplicateCode3(QrKMChitiet, FieldByName('MAVT'), 'KHOA_DIEUKIEN;MAVT', [mKHOACT, mMAVT]) then
         begin
            try
                Exit;
            finally
                Cancel;
                Edit;
            end;
         end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietNhomBeforeOpen(DataSet: TDataSet);
begin
   (DataSet as TADOQuery).Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMChitietSOLUONGChange(Sender: TField);
begin
    if mTrigger then
    	Exit;

	with QrKMChitiet do
    begin
        if Sender.FullName = 'SOLUONG' then
        begin
            FieldByName('SOTIEN').AsFloat := exVNDRound(FieldByName('GIABAN').AsFloat
                                            * FieldByName('SOLUONG').AsFloat, ctCurRound);
        end;
    end;
    GrChitiet.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrChitietMAVTChange(Sender: TField);
begin
    exDotMavt(4, DataMain.QrFB_DM_THUCDON, Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDoituongAfterInsert(DataSet: TDataSet);
begin
    with DataSet do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        FieldByName('LOC').AsString := QrKM.FieldByName('LOC').AsString;
        FieldByName('KHOA').AsString := QrKM.FieldByName('KHOA').AsString;
    end;
    if mTrigger then
    	Exit;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDoituongBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDoituongNCCBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    SetEditState(QrKM);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    if State in [dsInsert] then
    else
    begin
        FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTMADTChange(Sender: TField);
begin
     exDotMadt(DataMain.QrDMNCC, Sender);
     GrKMDT.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTMANGANHChange(Sender: TField);
begin
     exDotFBMavt(5, QrNganh, Sender, 'OTHER');
     GrKMDT.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTMANHOMChange(Sender: TField);
begin
     exDotFBMavt(6, QrNhom, Sender, 'OTHER');
     GrKMDT.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTMAVTChange(Sender: TField);
begin
     exDotFBMavt(4, DataMain.QrFB_DM_THUCDON, Sender);
     if Sender.AsString <> '' then
     begin
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;

            Open;
            QrKMDT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
        end;
        GrKMDT.InvalidateCurrentRow;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienAfterInsert(DataSet: TDataSet);
begin
    with QrKMDieukien do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        FieldByName('LOC').AsString := QrKM.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienAfterScroll(DataSet: TDataSet);
begin
    with QrKMDieukien do
    begin
        if (QrKM.FieldByName('HTKM').AsString <> '03') or IsEmpty then
        begin
            QrKMChitiet.Filter := '';
            DeleteConfirm(False);
            EmptyDataset(QrKMChitiet);
            DeleteConfirm(True);
        end
        else
        begin
           QrKMChitiet.Filter := 'KHOA_DIEUKIEN=' + FieldByName('KHOACT').AsString;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienBeforeDelete(DataSet: TDataSet);
begin
    if not mDeleteAllConditionConfirm  then
        if not DeleteConfirm then
            Abort;

    SetEditState(QrKM);

    DeleteConfirm(False);
	EmptyDataset(QrKMChitiet);
    DeleteConfirm(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    with QrKMDieukien do
    begin
        if BlankConfirm(QrKMDieukien, ['TU', 'DEN']) then
            Abort;

        if QrKMDieukien.FieldByName('TU').AsFloat > QrKMDieukien.FieldByName('DEN').AsFloat then
        begin
            ErrMsg(RS_CONDITION_INVAILD);
            Abort;
        end;

        if (QrKM.FieldByName('HTKM').AsString = '01') and BlankConfirm(QrKMDieukien, ['TL_CK']) then
            Abort;

        if (QrKM.FieldByName('HTKM').AsString = '02') and BlankConfirm(QrKMDieukien, ['SOTIEN']) then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDieukienCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    if State in [dsInsert] then
    else
    begin
        FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTAfterInsert(DataSet: TDataSet);
begin
    with QrKMDT do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        FieldByName('LOC').AsString := QrKM.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
	SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMDTBeforePost(DataSet: TDataSet);
var
    mField: string;
begin
    mLDT := QrKM.FieldByName('KHUYENMAI_DOITUONG').AsString;
    if mLDT = '02' then
        mField := 'MADT'
    else if mLDT = '03' then
        mField := 'MANGANH'
    else if mLDT = '04' then
        mField := 'MANHOM'
    else if mLDT = '05' then
        mField := 'MAVT';

    with QrKMDT do
    begin
        if BlankConfirm(QrKMDT, [mField]) then
	    	Abort;

        if IsDuplicateCode(QrKMDT, FieldByName(mField), True) then
            Abort;

        if (mLDT = '05') and (FieldByName('LK_TINHTRANG').AsString <> '01') then
        begin
            ErrMsg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMHTKMChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    getLayoutCondition();

    bTrigger := mTrigger;
    mTrigger := True;
    clearColumnCondition();
    mTrigger := bTrigger;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.GrBrowseDblClick(Sender: TObject);
begin
    if QrKM.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.MemoDateInitDialog(Dialog: TwwMemoDlg);
var
	ls: TStrings;
    i, n: Integer;
begin
	ls := TStringList.Create;
    ls.Text := QrKM.FieldByName('CHECKED_DESC').AsString;
    n := ls.Count;
    for i := 0 to ls.Count - 1 do
    begin
    	ls[i] := Format('%2d: %s', [n, ls[i]]);
        Dec(n);
    end;

	with Dialog do
    begin
    	Memo.ReadOnly := True;
        OKBtn.Visible := False;
        CancelBtn.Font.Style := [fsBold];
        Memo.Lines.AddStrings(ls);
    end;
    ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(QrKM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;

        with QrKMDT do
        begin
            Close;
            Open;
        end;

        with QrKMDieukien do
        begin
            Close;
            Open;
        end;

        with QrKMChitiet do
        begin
            Close;
            Open;
        end;

        getLayoutPartner();
        getLayoutCondition();

	    Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		Perform(WM_NEXTDLGCTL, 0, 0);
	   	end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CbLydoNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdDiembanExecute(Sender: TObject);
begin
	with QrKM do
    Begin
  		Application.CreateForm(TFrmKhuyenmai2, FrmKhuyenmai2);
        FrmKhuyenmai2.Execute(
            TGuidField(FieldByName('KHOA')).AsGuid,
            FieldByName('CHECKED').AsBoolean,
            FieldByName('SCT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdDMNPLExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    begin
        DataMain.QrFB_DM_THUCDON.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdDMTDExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    begin
        DataMain.QrFB_DM_THUCDON.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdEmptyDieuKienExecute(Sender: TObject);
begin
    if not YesNo(RS_XOA_CHITIET, 1) then
    	Exit;

    mDeleteAllConditionConfirm := True;
    DeleteConfirm(False);
	EmptyDataset(QrKMDieukien);
    DeleteConfirm(True);
    mDeleteAllConditionConfirm := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdEmptyExecute(Sender: TObject);
begin
    exEmptyDetails(QrKMDT, GrKMDT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.CmdEmptyKhuyenMaiExecute(Sender: TObject);
begin
    exEmptyDetails(QrKMChitiet, GrChitiet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMKHUYENMAI_DIEUKIENChange(Sender: TField);
var
    mDIEUKIEN: string;
begin
    mDIEUKIEN := QrKM.FieldByName('KHUYENMAI_DIEUKIEN').AsString;
    if mDIEUKIEN <> '' then
    begin
        if mDIEUKIEN = '01' then
        begin
            SetDisplayFormat(QrKMDieukien, ['TU', 'DEN'], sysCurFmt);
        end
        else if mDIEUKIEN = '02' then
        begin
            SetDisplayFormat(QrKMDieukien, ['TU', 'DEN'], ctQtyFmt);
        end
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.QrKMKHUYENMAI_DOITUONGChange(Sender: TField);
begin
    getLayoutPartner();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.ToolButton10Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai3.getComboboxGroup();
var
     i, n: Integer;
begin
    n := FlexConfigInteger(FORM_CODE + '_KM', 'Group Product');
    CbNhom.Items.Clear;
    if n > 0 then
    begin
        for i := 1 to n do
        begin
            CbNhom.Items.Add('Nhóm ' + IntToStr(i) + ' '#9''+ IntToStr(i));
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai3.getLayoutPartner();
var
    layout: string;
begin
    mLDT := QrKM.FieldByName('KHUYENMAI_DOITUONG').AsString;
    if mLDT <> '' then
    begin
        if mLDT = '02' then
        begin
            layout := '_DT_NCC';
        end
        else if mLDT = '03' then
        begin
             layout := '_DT_NGANH';
        end
        else if mLDT = '04' then
        begin
            layout := '_DT_NHOM';
        end
        else if mLDT = '05' then
        begin
            layout := '_DT_HH';
        end;

        SetCustomGrid([FORM_CODE + layout], [GrKMDT]);
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.getLayoutCondition();
var
    layout: string;
begin
    mLKM := QrKM.FieldByName('HTKM').AsString;
    if mLKM <> '' then
    begin
        if mLKM = '01' then
        begin
            layout := '_DK_TL';
        end
        else if mLKM = '02' then
        begin
             layout := '_DK_HD';
        end
        else if mLKM = '03' then
        begin
            layout := '_DK';
        end;
        SetCustomGrid([FORM_CODE + layout], [GrDieukien]);

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Khuyenmai3.clearColumnCondition();
begin
    with QrKMDieukien do
    if not IsEmpty then
    begin
        First;
        while not Eof do
        begin
            Edit;
            if QrKM.FieldByName('HTKM').AsString <> '01' then
            begin
                FieldByName('TL_CK').Clear;
            end;

            if QrKM.FieldByName('HTKM').AsString <> '02' then
            begin
                FieldByName('SOTIEN').Clear;
            end;
            Next;
        end;
    end;
end;

end.
