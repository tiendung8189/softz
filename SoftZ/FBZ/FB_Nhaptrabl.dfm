object FrmFB_Nhaptrabl: TFrmFB_Nhaptrabl
  Left = 167
  Top = 114
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Nh'#7853'p Tr'#7843' - Nh'#224' H'#224'ng'
  ClientHeight = 580
  ClientWidth = 903
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    903
    580)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 903
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 903
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton11: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object Sepchecked: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'Sepchecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 903
    Height = 540
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 490
        Width = 895
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 895
        Height = 441
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho'
          'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho'
          'LK_QUAY'#9'20'#9'Qu'#7847'y'#9'F'
          'LK_THUNGAN'#9'25'#9'Thu ng'#226'n'#9'F'
          'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'
          'SOTIEN'#9'19'#9'Tr'#7883' gi'#225' thanh to'#225'n'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 895
        Height = 49
        Align = alTop
        AutoSize = True
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 895
        inherited Panel1: TPanel
          Width = 895
          ExplicitWidth = 895
          inherited Label65: TLabel
            Left = 55
            ExplicitLeft = 55
          end
          inherited Label66: TLabel
            Left = 256
            ExplicitLeft = 256
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Chi ti'#7871't'
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 895
        Height = 137
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 83
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 291
          Top = 15
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object Label10: TLabel
          Left = 62
          Top = 82
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdSCT: TwwDBEdit
          Left = 346
          Top = 10
          Width = 170
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 118
          Top = 10
          Width = 165
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 0
        end
        object DBEdit1: TDBMemo
          Left = 118
          Top = 82
          Width = 398
          Height = 44
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
        end
        object cxGroupBox1: TGroupBox
          Left = 522
          Top = 2
          Width = 354
          Height = 54
          Caption = 'T'#236'm th'#244'ng tin bill b'#225'n l'#7867
          TabOrder = 6
          object Label3: TLabel
            Left = 8
            Top = 24
            Width = 153
            Height = 16
            Alignment = taRightJustify
            Caption = 'S'#7889' bill/ M'#227' v'#7841'ch tr'#234'n phi'#7871'u'
          end
          object EdSoBillMaVach: TwwDBEdit
            Left = 167
            Top = 20
            Width = 178
            Height = 22
            CharCase = ecUpperCase
            Ctl3D = False
            DataField = 'S1'
            DataSource = DsTemp
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object GroupBox1: TGroupBox
          Left = 522
          Top = 58
          Width = 354
          Height = 68
          Caption = 'Th'#244'ng tin bill b'#225'n l'#7867
          TabOrder = 7
          object Label7: TLabel
            Left = 3
            Top = 22
            Width = 28
            Height = 16
            Caption = 'Ng'#224'y'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 165
            Top = 22
            Width = 35
            Height = 16
            Caption = 'S'#7889' bill'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 10
            Top = 46
            Width = 21
            Height = 16
            Caption = 'B'#224'n'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 185
            Top = 46
            Width = 15
            Height = 16
            Caption = 'Ca'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object BtRemoveBill: TSpeedButton
            Left = 324
            Top = 18
            Width = 23
            Height = 22
            Cursor = 1
            Action = CmdCancelBill
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              1800000000000003000000000000000000000000000000000000FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
              FFFF00FFFF00FFFF00FFFF00FFFF00FF7E8FE30022CCBEC7F0FF00FFFF00FFFF
              00FFFF00FFFF00FFFF00FFBEC7F00022CC7E8FE3FF00FFFF00FFFF00FF8091E4
              0325CE3355FF0325CEBFC7F1FF00FFFF00FFFF00FFFF00FFBFC7F10325CE3355
              FF0325CE8091E4FF00FFFF00FF0729D288AAFF385AFF385AFF0729D2C0C8F2FF
              00FFFF00FFC0C8F20729D2385AFF385AFF88AAFF0729D2FF00FFFF00FFC1CAF2
              0C2ED588AAFF3E60FF3E60FF0C2ED5C1CAF2C1CAF20C2ED53E60FF3E60FF88AA
              FF0C2ED5C1CAF2FF00FFFF00FFFF00FFC3CBF41234DA88AAFF4668FF4668FF12
              34DA1234DA4668FF4668FF88AAFF1234DAC3CBF4FF00FFFF00FFFF00FFFF00FF
              FF00FFC4CDF5183ADE88AAFF4F71FF4F71FF4F71FF4F71FF88AAFF183ADEC4CD
              F5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6CEF61F41E37193FF59
              7BFF597BFF7193FF1F41E3C6CEF6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
              FF00FFFF00FFC7D0F72547E86284FF6284FF6284FF6284FF2547E8C7D0F7FF00
              FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC9D2F82C4EED6C8EFF6C8EFF88
              AAFF88AAFF6C8EFF6C8EFF2C4EEDC9D2F8FF00FFFF00FFFF00FFFF00FFFF00FF
              CBD3F93254F17597FF7597FF88AAFF3254F13254F188AAFF7597FF7597FF3254
              F1CBD3F9FF00FFFF00FFFF00FFCCD5FB385AF67D9FFF7D9FFF88AAFF385AF6CC
              D5FBCCD5FB385AF688AAFF7D9FFF7D9FFF385AF6CCD5FBFF00FFFF00FF3D5FF9
              88AAFF83A5FF88AAFF3D5FF9CDD6FBFF00FFFF00FFCDD6FB3D5FF988AAFF83A5
              FF88AAFF3D5FF9FF00FFFF00FF9FB0FC4163FD88AAFF4163FDCED7FCFF00FFFF
              00FFFF00FFFF00FFCED7FC4163FD88AAFF4163FD9FB0FCFF00FFFF00FFFF00FF
              A0B1FD4466FFCFD8FDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCFD8FD4466
              FFA0B1FDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
              00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          end
          object DaBillNgay: TwwDBDateTimePicker
            Left = 36
            Top = 18
            Width = 125
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'Tahoma'
            CalendarAttributes.Font.Style = []
            Color = clBtnFace
            DataField = 'NGAY2'
            DataSource = DsNX
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
          end
          object EdBill: TwwDBEdit
            Left = 205
            Top = 18
            Width = 116
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SCT2'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdBillCa: TwwDBEdit
            Left = 205
            Top = 42
            Width = 116
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'CA'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdBillBan: TwwDBEdit
            Left = 36
            Top = 42
            Width = 125
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'MABAN'
            DataSource = DsNX
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object EdKho: TDBEditEh
          Left = 439
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object cbbKho: TDbLookupComboboxEh2
          Left = 118
          Top = 34
          Width = 318
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 317
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 398
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
          OnChange = cbbKhoExit
          OnCloseUp = cbbKhoCloseUp
          OnDropDown = cbbKhoDropDown
          OnExit = cbbKhoExit
        end
        object cbbThuNgan: TDbLookupComboboxEh2
          Left = 118
          Top = 58
          Width = 318
          Height = 22
          ControlLabel.Width = 106
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Thu ng'#226'n nh'#7853'p tr'#7843
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'THUNGAN'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'FullName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 317
            end
            item
              FieldName = 'UserName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DsUSER
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 398
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'UID'
          ListField = 'FullName'
          ListSource = DsUSER
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
          OnChange = cbbKhoExit
          OnCloseUp = cbbKhoCloseUp
          OnDropDown = cbbKhoDropDown
          OnExit = cbbKhoExit
        end
        object EdThuNgan: TDBEditEh
          Left = 439
          Top = 58
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_USERNAME'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
      end
      object GrDetail: TwwDBGrid2
        Left = 0
        Top = 137
        Width = 895
        Height = 310
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'RSTT'#9'3'#9'STT'#9'F'
          'MAVT'#9'15'#9'M'#227' h'#224'ng'#9'T'
          'SOLUONG2'#9'8'#9'B'#225'n'#9'T'#9'S'#7889' l'#432#7907'ng'
          'SOLUONG'#9'10'#9'Tr'#7843#9'F'#9'S'#7889' l'#432#7907'ng'
          'DONGIA'#9'12'#9#272#417'n gi'#225#9'T'
          'SOTIEN'#9'15'#9'Th'#224'nh ti'#7873'n'#9'T'
          'TL_CK'#9'8'#9'% C.K'#9'T'
          'GHICHU'#9'50'#9'Ghi ch'#250#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 1
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopDetail
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = False
        UseTFields = False
        FooterColor = 13360356
        FooterCellColor = 13360356
        PadColumnStyle = pcsPadHeader
      end
      object Panel2: TPanel
        Left = 0
        Top = 447
        Width = 895
        Height = 64
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          895
          64)
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object LbHH2: TLabel
          Left = 86
          Top = 9
          Width = 90
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' h'#224'ng h'#243'a'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 198
          Top = 9
          Width = 60
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Chi'#7871't kh'#7845'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 310
          Top = 9
          Width = 78
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' sau C.K'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 422
          Top = 9
          Width = 43
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ph'#7909' thu'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 534
          Top = 9
          Width = 104
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' sau ph'#7909' thu'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label19: TLabel
          Left = 652
          Top = 9
          Width = 78
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ti'#7873'n thu'#7871' VAT'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 764
          Top = 9
          Width = 48
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Th'#7921'c tr'#7843
          FocusControl = EdTriGiaTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdHH1: TwwDBEdit
          Left = 86
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCK: TwwDBEdit
          Left = 198
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU_MH'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit7: TwwDBEdit
          Left = 310
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN_SAU_CKMH'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit10: TwwDBEdit
          Left = 422
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'PHUTHU'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit11: TwwDBEdit
          Left = 537
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THANHTIEN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTienVAT: TwwDBEdit
          Left = 652
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CALC_TIEN_THUE'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTriGiaTT: TwwDBEdit
          Left = 767
          Top = 25
          Width = 109
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CALC_THUCTRA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object cbCoHoaDon: TwwCheckBox
          Left = -20
          Top = 24
          Width = 101
          Height = 22
          DisableThemes = False
          AlwaysTransparent = False
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          DisplayValueChecked = 'True'
          DisplayValueUnchecked = 'False'
          NullAndBlankState = cbUnchecked
          Anchors = [akTop, akRight]
          Caption = 'Xu'#7845't H'#272' GTGT'
          DataField = 'CO_HOADON'
          DataSource = DsNX
          TabOrder = 5
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 761
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 761
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 396
    Top = 324
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Chuy'#7875'n tab nhanh'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 118
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDetail: TAction
      Hint = 'Danh s'#225'ch <-> Chi ti'#7871't'
      ShortCut = 16418
      OnExecute = CmdDetailExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng'
      OnExecute = CmdFilterComExecute
    end
    object CmdUpdateDetail: TAction
      Caption = 'C'#7853'p nh'#7853't chi ti'#7871't m'#7863't h'#224'ng'
      OnExecute = CmdUpdateDetailExecute
    end
    object CmdDelDetail: TAction
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdDelDetailExecute
    end
    object CmdUpdateQty: TAction
      Caption = 'C'#7853'p nh'#7853't s'#7889' l'#432#7907'ng tr'#7843' h'#224'ng'
      OnExecute = CmdUpdateQtyExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdCancelBill: TAction
      OnExecute = CmdCancelBillExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MANS'
      'TENNS'
      'MAKH'
      'TENKH'
      'SOLUONG'
      'SOTIEN'
      'NG_GIAO'
      'NG_NHAN'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 396
    Top = 352
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'FB_TRAHANG'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :NGAYD'
      '   and '#9'NGAY <  :NGAYC  + 1')
    Left = 70
    Top = 364
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Calculated = True
    end
    object QrNXXOA: TWideStringField
      Alignment = taRightJustify
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
      Visible = False
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 200
      Lookup = True
    end
    object QrNXQUAY: TWideStringField
      DisplayLabel = 'Qu'#7847'y'
      FieldName = 'QUAY'
      FixedChar = True
    end
    object QrNXLK_QUAY: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_QUAY'
      LookupDataSet = QrDMQUAY
      LookupKeyFields = 'QUAY'
      LookupResultField = 'GHICHU'
      KeyFields = 'QUAY'
      Size = 100
      Lookup = True
    end
    object QrNXLK_THUNGAN: TWideStringField
      DisplayLabel = 'T'#234'n thu ng'#226'n'
      FieldKind = fkLookup
      FieldName = 'LK_THUNGAN'
      LookupDataSet = QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'THUNGAN'
      Size = 200
      Lookup = True
    end
    object QrNXSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      FieldName = 'SOLUONG'
    end
    object QrNXSOTIEN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225' thanh to'#225'n'
      FieldName = 'SOTIEN'
      Visible = False
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
    end
    object QrNXTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
      OnChange = QrNXTHANHTOANChange
    end
    object QrNXDELETE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      Alignment = taRightJustify
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      DisplayWidth = 10
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrNXMAVIP: TWideStringField
      FieldName = 'MAVIP'
    end
    object QrNX_id: TLargeintField
      FieldName = '_id'
    end
    object QrNXNGAY2: TDateTimeField
      FieldName = 'NGAY2'
    end
    object QrNXTHUCTRA: TFloatField
      FieldName = 'THUCTRA'
    end
    object QrNXCO_HOADON: TBooleanField
      FieldName = 'CO_HOADON'
      OnChange = QrNXCO_HOADONChange
    end
    object QrNXTHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrNXPHUTHU: TFloatField
      FieldName = 'PHUTHU'
    end
    object QrNXSOTIEN_SAU_CKMH: TFloatField
      FieldName = 'SOTIEN_SAU_CKMH'
    end
    object QrNXTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrNXHINHTHUC_GIA: TWideStringField
      FieldName = 'HINHTHUC_GIA'
    end
    object QrNXMABAN: TWideStringField
      FieldName = 'MABAN'
    end
    object QrNXCA: TWideStringField
      FieldName = 'CA'
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrNXLK_USERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_USERNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'USERNAME'
      KeyFields = 'THUNGAN'
      Lookup = True
    end
    object QrNXCALC_TIEN_THUE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TIEN_THUE'
      Calculated = True
    end
    object QrNXCALC_THUCTRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_THUCTRA'
      Calculated = True
    end
    object QrNXTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrNXPTNX: TWideStringField
      FieldName = 'PTNX'
    end
    object QrNXMAQUAY: TWideStringField
      FieldName = 'MAQUAY'
    end
    object QrNXTL_PHUTHU: TFloatField
      FieldName = 'TL_PHUTHU'
    end
    object QrNXMAVIP_HOTEN: TWideStringField
      FieldName = 'MAVIP_HOTEN'
      Size = 200
    end
    object QrNXTL_CKHD: TFloatField
      FieldName = 'TL_CKHD'
    end
    object QrNXTL_CKVIPDM: TFloatField
      FieldName = 'TL_CKVIPDM'
    end
    object QrNXCKHD_BY: TIntegerField
      FieldName = 'CKHD_BY'
    end
    object QrNXTHANHTIEN_CHUA_CL: TFloatField
      FieldName = 'THANHTIEN_CHUA_CL'
    end
    object QrNXTHANHTIEN_CL: TFloatField
      FieldName = 'THANHTIEN_CL'
    end
    object QrNXTHANHTIEN_CHUA_VAT: TFloatField
      FieldName = 'THANHTIEN_CHUA_VAT'
    end
    object QrNXTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrNXTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrNXTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrNXMADT: TWideStringField
      FieldName = 'MADT'
      Size = 15
    end
    object QrNXTIEN_THUE_CHUA_CL: TFloatField
      FieldName = 'TIEN_THUE_CHUA_CL'
      OnChange = QrNXTIEN_THUE_CHUA_CLChange
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '   from '#9'FB_TRAHANG_COMBO'
      'where '#9'KHOA= :KHOA'
      'order by '#9'STT')
    Left = 90
    Top = 364
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTMAVT: TWideStringField
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      Size = 15
    end
    object QrCTSOLUONG2: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng b'#225'n'
      FieldName = 'SOLUONG2'
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng tr'#7843
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGChange
    end
    object QrCTDONGIA: TFloatField
      FieldName = 'DONGIA'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTDONGIAValidate
    end
    object QrCTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTTL_CK: TFloatField
      DisplayLabel = '% CK'
      FieldName = 'TL_CK'
      OnChange = QrCTSOTIENChange
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrCTTL_CK_MAX: TFloatField
      FieldName = 'TL_CK_MAX'
    end
    object QrCTTL_CK_THEM: TFloatField
      FieldName = 'TL_CK_THEM'
    end
    object QrCTTENVT2: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTTL_CKMH: TFloatField
      FieldName = 'TL_CKMH'
    end
    object QrCTTL_CKHD: TFloatField
      FieldName = 'TL_CKHD'
    end
    object QrCTTL_CK_VIPNHOM: TFloatField
      FieldName = 'TL_CK_VIPNHOM'
    end
    object QrCTTL_CK_VIPDM: TFloatField
      FieldName = 'TL_CK_VIPDM'
    end
    object QrCTTL_CK_PHIEU: TFloatField
      FieldName = 'TL_CK_PHIEU'
    end
    object QrCTDVT: TWideStringField
      FieldName = 'DVT'
    end
    object QrCTBO: TBooleanField
      FieldName = 'BO'
    end
    object QrCTCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
    end
    object QrCTSOTIEN_SAU_CKMH: TFloatField
      FieldName = 'SOTIEN_SAU_CKMH'
    end
    object QrCTTL_PHUTHU: TFloatField
      FieldName = 'TL_PHUTHU'
    end
    object QrCTPHUTHU: TFloatField
      FieldName = 'PHUTHU'
    end
    object QrCTTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
      OnChange = QrCTTHANHTIENChange
    end
    object QrCTDONGIA_SAU_CK: TFloatField
      FieldName = 'DONGIA_SAU_CK'
    end
    object QrCTTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
      OnChange = QrCTTHANHTOANChange
    end
    object QrCTTENTAT: TWideStringField
      FieldName = 'TENTAT'
      Size = 200
    end
    object QrCTTHANHTIEN_CHUA_CL: TFloatField
      FieldName = 'THANHTIEN_CHUA_CL'
    end
    object QrCTTHANHTIEN_CL: TFloatField
      FieldName = 'THANHTIEN_CL'
      OnChange = QrCTTHANHTIENChange
    end
    object QrCTTHANHTIEN_CHUA_VAT: TFloatField
      FieldName = 'THANHTIEN_CHUA_VAT'
    end
    object QrCTDONGIA_CHUA_VAT: TFloatField
      FieldName = 'DONGIA_CHUA_VAT'
    end
    object QrCTTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrCTTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrCTTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrCTCKHD_BY: TIntegerField
      FieldName = 'CKHD_BY'
    end
    object QrCTTRA_BY: TIntegerField
      FieldName = 'TRA_BY'
    end
    object QrCTTRA_DATE: TDateTimeField
      FieldName = 'TRA_DATE'
    end
    object QrCTCALC_THUE_SUAT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_THUE_SUAT'
      Calculated = True
    end
    object QrCTCALC_TIEN_THUE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TIEN_THUE'
      Calculated = True
    end
    object QrCTCALC_THUCTRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_THUCTRA'
      Calculated = True
    end
    object QrCT_id: TLargeintField
      FieldName = '_id'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 62
    Top = 392
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 90
    Top = 392
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'Vw_FB_DM_HH'
      'where '#9'isnull(CAMUNG_NHOM, '#39#39') <> '#39#39
      'order by  TENVT')
    Left = 130
    Top = 364
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 292
    Top = 304
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 324
    Top = 304
  end
  object vlTotal: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'PHUTHU'
      'SOLUONG'
      'SOTIEN'
      'CHIETKHAU_MH'
      'SOTIEN_SAU_CKMH'
      'THANHTIEN'
      'THANHTIEN_CHUA_CL'
      'THANHTIEN_CL'
      'THANHTIEN_CHUA_VAT'
      'TIEN_THUE_CHUA_CL'
      'TIEN_THUE_5'
      'TIEN_THUE_10'
      'TIEN_THUE_OR'
      'THANHTOAN')
    DetailFields.Strings = (
      'PHUTHU'
      'SOLUONG'
      'SOTIEN'
      'CHIETKHAU_MH'
      'SOTIEN_SAU_CKMH'
      'THANHTIEN'
      'THANHTIEN_CHUA_CL'
      'THANHTIEN_CL'
      'THANHTIEN_CHUA_VAT'
      'TIEN_THUE'
      'TIEN_THUE_5'
      'TIEN_THUE_10'
      'TIEN_THUE_OR'
      'THANHTOAN')
    Left = 440
    Top = 292
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_QUAYTN'
      'order by '#9'QUAY'
      ' ')
    Left = 202
    Top = 365
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 472
    Top = 292
    object MenuItem6: TMenuItem
      Action = CmdUpdateDetail
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Cpnhtchititmthng1: TMenuItem
      Action = CmdUpdateQty
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdDelDetail
    end
  end
  object QrUSER: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'SYS_USER'
      ' where'#9'USERNAME <> '#39'ADMIN'#39
      '   and  '#9'IsNull(IS_GROUP, 0) = 0'
      'order by '#9'USERNAME')
    Left = 174
    Top = 365
  end
  object spCHECK_BILL_FB_BANLE: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spCHECK_BILL_FB_BANLE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pSTR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 2000
        Value = 'Kh'#244'ng t'#236'm th'#7845'y H'#243'a '#273#417'n b'#225'n l'#7867'.'
      end
      item
        Name = '@pMAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@pS1'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 678
    Top = 340
  end
  object kbmTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 0
    LocaleID = 0
    Left = 517
    Top = 312
    object kbmTempS1: TWideStringField
      FieldName = 'S1'
      OnChange = kbmTempS1Change
      Size = 200
    end
    object kbmTempS3: TWideStringField
      FieldName = 'S3'
      Size = 200
    end
    object kbmTempS2: TWideStringField
      FieldName = 'S2'
      Size = 200
    end
  end
  object DsTemp: TDataSource
    DataSet = kbmTemp
    Left = 516
    Top = 348
  end
  object spCHECK_BILL_USED: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spFB_CASHIER_CHECK_BILL_TRAHANG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pSTR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 2000
        Value = Null
      end
      item
        Name = '@pS1'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end>
    Left = 750
    Top = 308
  end
  object spFB_BANLE_Select_Full: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spFB_BANLE_Select_Full;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 637
    Top = 287
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from DM_KHO'
      'where LOAIKHO in (select MA_HOTRO from V_LOAI_KHO)'
      'order by MAKHO'
      ' ')
    Left = 352
    Top = 384
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 358
    Top = 418
  end
  object DsUSER: TDataSource
    DataSet = QrUSER
    Left = 192
    Top = 418
  end
end
