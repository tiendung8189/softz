﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonDsHHKM;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls,
  wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwDialog, wwfltdlg,
  wwFltDlg2, AppEvnts, DBGridEh, kbmMemTable, DBLookupEh, DbLookupComboboxEh2,
  Vcl.Mask, DBCtrlsEh, MemTableDataEh, MemTableEh;

type
  TFrmFB_ChonDsHHKM = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPhieu: TDataSource;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    SpBt1: TSpeedButton;
    SpBt2: TSpeedButton;
    QrDMKHO: TADOQuery;
    Status: TStatusBar;
    Panel1: TPanel;
    ApplicationEvents1: TApplicationEvents;
    Filter: TwwFilterDialog2;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    DsDMKHO: TDataSource;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMAKHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure SpBt1Click(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mMADT, mMakho, mString: String;
    mType: Integer;
    mKhoa: TGUID;
  public
  	function Execute(pType: Integer; pString,
            pMadt: String; pKhoa: TGUID): Boolean;
  end;

var
  FrmFB_ChonDsHHKM: TFrmFB_ChonDsHHKM;

implementation

uses
	isDb, ExCommon, isLib, FB_Khuyenmai, isCommon, GuidEx;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonDsHHKM.Execute;
begin
    mMakho := '~';
	mMADT := pMADT;
    mString := pString;
    mType := pType;
    mKhoa := pKhoa;
    DsPhieu.DataSet := FrmFB_Khuyenmai.QrList;

    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('FB_CHON_DS_HHKM', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.FormShow(Sender: TObject);
begin
    QrDMKHO.Open;
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
    GrBrowse.SetFocus;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.SpBt1Click(Sender: TObject);
var
    bm: TBytes;
begin
    with FrmFB_Khuyenmai.QrList do
    begin
        bm := Bookmark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SELECTED').AsBoolean := (Sender as TSpeedButton).Tag = 0;
            Next;
        end;
        Bookmark := bm;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mMakho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPhieu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.CmdRefreshExecute(Sender: TObject);
var
    s: String;
begin
    s := EdMaKho.Text;
    if mMakho <> s then
	begin
        mMakho := s;

        with FrmFB_Khuyenmai.QrList do
        begin
            if Active then
    			Close;

            Parameters[1].Value := mMakho;
            Parameters[2].Value := mType;
            Parameters[3].Value := mString;
            Parameters[4].Value := mMadt;
            Parameters[5].Value := TGuidEx.ToString(mKhoa);

            Open;
        end;

        SetDisplayFormat(FrmFB_Khuyenmai.QrList, sysCurFmt);
        SetDisplayFormat(FrmFB_Khuyenmai.QrList, ['TL_CK'], sysPerFmt);
        SetShortDateFormat(FrmFB_Khuyenmai.QrList);
        SetDisplayFormat(FrmFB_Khuyenmai.QrList, ['TUGIO', 'DENGIO'], 'hh:nn');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(FrmFB_Khuyenmai.QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.CbMAKHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsHHKM.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

end.
