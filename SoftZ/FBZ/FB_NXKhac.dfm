object FrmFB_NXKhac: TFrmFB_NXKhac
  Left = 154
  Top = 130
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Nh'#7853'p Kh'#225'c - Nh'#224' H'#224'ng'
  ClientHeight = 573
  ClientWidth = 865
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    865
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 865
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 865
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 224
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 232
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 304
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 312
      Top = 0
      Cursor = 1
      Caption = 'In'
      DropdownMenu = PopupMenu2
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object SepChecked: TToolButton
      Left = 399
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 407
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object SepBarcode: TToolButton
      Left = 479
      Top = 0
      Width = 8
      Caption = 'SepBarcode'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 487
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 865
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 857
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 857
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Tr'#7883' gi'#225#9'F'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 857
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 857
        inherited Panel1: TPanel
          Width = 857
          ParentColor = False
          ExplicitWidth = 857
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 857
        Height = 97
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 16
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 287
          Top = 16
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 588
          Top = 40
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
        end
        object Label8: TLabel
          Left = 584
          Top = 64
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
        end
        object EdSCT: TwwDBEdit
          Left = 344
          Top = 12
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 12
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 656
          Top = 36
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_GIAO'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 656
          Top = 60
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_NHAN'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object cbbLyDo: TDbLookupComboboxEh2
          Left = 655
          Top = 12
          Width = 181
          Height = 22
          ControlLabel.Width = 30
          ControlLabel.Height = 16
          ControlLabel.Caption = 'L'#253' do'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'LYDO'
          DataSource = DsNX
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsFB_LYDO_NK
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsFB_LYDO_NK
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
        end
        object EdMAKHO: TDBEditEh
          Left = 452
          Top = 36
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object CbKhoHang: TDbLookupComboboxEh2
          Left = 112
          Top = 36
          Width = 338
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 335
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 418
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 3
          Visible = True
        end
        object DBMemoEh1: TDBMemoEh
          Left = 112
          Top = 60
          Width = 417
          Height = 22
          ControlLabel.Width = 49
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 6
          Visible = True
          WantReturns = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 97
        Width = 857
        Height = 399
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitTop = 116
        ExplicitHeight = 380
        object GrDetail: TwwDBGrid2
          Tag = 1
          Left = 0
          Top = 16
          Width = 857
          Height = 383
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'B1'#9'7'#9'In tem'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'TENVT'#9'30'#9'T'#234'n h'#224'ng'#9'T'
            'DVT'#9'8'#9#272'VT'#9'T'
            'SOLUONG'#9'10'#9'S'#7889' l'#432#7907'ng'#9'F'
            'DONGIA_REF'#9'10'#9'Gi'#225' T.K'#9'T'
            'DONGIA'#9'10'#9#272#417'n gi'#225#9'F'
            'SOTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'T'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 723
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 723
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 146
    Top = 250
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdBalance: TAction
      Caption = 'T'#7841'o phi'#7871'u'
      Hint = 'T'#7841'o phi'#7871'u do ch'#234'nh l'#7879'ch ki'#7875'm k'#234
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdImportExcel: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
      OnExecute = CmdChecktonExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 114
    Top = 250
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    AfterEdit = QrNXAfterEdit
    BeforePost = QrNXBeforePost
    AfterPost = QrNXAfterPost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHUNGTU'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'LOC = :LOC')
    Left = 606
    Top = 283
    object QrNXXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225
      DisplayWidth = 18
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXHOADON_SERI: TWideStringField
      DisplayLabel = 'S'#234'ri'
      DisplayWidth = 10
      FieldName = 'HOADON_SERI'
      Visible = False
      Size = 10
    end
    object QrNXHOADON_SO: TWideStringField
      DisplayLabel = 'S'#7889' h'#243'a '#273#417'n'
      DisplayWidth = 12
      FieldName = 'HOADON_SO'
      Visible = False
      Size = 10
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 10
      FieldName = 'MADT'
      Visible = False
      FixedChar = True
      Size = 15
    end
    object QrNHOADON_NGAY: TDateTimeField
      DisplayWidth = 18
      FieldName = 'HOADON_NGAY'
      Visible = False
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXSOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXSOTIEN_SI: TFloatField
      FieldName = 'SOTIEN_SI'
    end
    object QrNXSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrNXCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#170'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      DisplayWidth = 53
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
    end
    object QrNXLK_LYDO_NHAP: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO_NHAP'
      LookupDataSet = DataMain.QrFB_LYDO_NK
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrNXLK_LYDO_XUAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO_XUAT'
      LookupDataSet = DataMain.QrFB_LYDO_XK
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrNXLOCKED_BY: TIntegerField
      FieldName = 'LOCKED_BY'
    end
    object QrNXLK_LOCKED_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LOCKED_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'LOCKED_BY'
      Size = 200
      Lookup = True
    end
    object QrNXLOCKED_DATE: TDateTimeField
      FieldName = 'LOCKED_DATE'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_CHUNGTU_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 634
    Top = 283
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      FixedChar = True
      Size = 15
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTDONGIA_REF: TFloatField
      FieldName = 'DONGIA_REF'
    end
    object QrCTDONGIA_REF2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DONGIA_REF2'
      Calculated = True
    end
    object QrCTDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 10
      FieldName = 'DONGIA'
      OnChange = QrCTDONGIAChange
    end
    object QrCTDONGIA2: TFloatField
      FieldName = 'DONGIA2'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSOLUONG2: TFloatField
      FieldName = 'SOLUONG2'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSOLUONG2_LE: TFloatField
      FieldName = 'SOLUONG2_LE'
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrCTTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
      Visible = False
    end
    object QrCTTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
      Visible = False
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TINHTRANG'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTEX_DATE: TDateTimeField
      FieldName = 'EX_DATE'
    end
    object QrCTSOTIEN_SI: TFloatField
      FieldName = 'SOTIEN_SI'
    end
    object QrCTDONGIA_LE: TFloatField
      FieldName = 'DONGIA_LE'
    end
    object QrCTSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrCTTL_LAI: TFloatField
      FieldName = 'TL_LAI'
    end
    object QrCTTL_LAI_SI: TFloatField
      FieldName = 'TL_LAI_SI'
    end
    object QrCTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTDVT: TWideStringField
      FieldName = 'DVT'
    end
    object QrCTDVT_BOX: TWideStringField
      FieldName = 'DVT_BOX'
    end
    object QrCTGIAVON: TFloatField
      FieldName = 'GIAVON'
    end
    object QrCTLK_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNganTinhTrang'
      KeyFields = 'MAVT'
      Lookup = True
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 606
    Top = 311
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 634
    Top = 311
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 276
    Top = 286
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel2: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 340
    Top = 286
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'SOTIEN_SI'
      'SOTIEN_LE'
      'SOTIEN')
    DetailFields.Strings = (
      'SOLUONG'
      'SOTIEN_SI'
      'SOTIEN_LE'
      'SOTIEN')
    Left = 372
    Top = 286
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 273
    Top = 340
    object LydliutfileExcel1: TMenuItem
      Action = CmdImportExcel
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Tag = 1
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xachititchngt1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object MenuItem1: TMenuItem
      Action = CmdSapthutu
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Kimtraslngtnkho1: TMenuItem
      Action = CmdCheckton
    end
  end
  object PopupMenu2: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 232
    Top = 304
    object heogibn1: TMenuItem
      Caption = 'Theo gi'#225' b'#225'n'
      Hint = 'In phi'#7871'u'
      Visible = False
      OnClick = CmdPrintExecute
    end
    object N9: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Phiuvnchuynnib1: TMenuItem
      Tag = 1
      Caption = 'Theo gi'#225' b'#225'n'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      OnClick = CmdPrintExecute
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object Phiucginhp1: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'Theo gi'#225' v'#7889'n'
      Hint = 'In phi'#7871'u'
      OnClick = CmdPrintExecute
    end
  end
end
