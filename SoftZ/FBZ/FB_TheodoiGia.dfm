object FrmFB_TheodoiGia: TFrmFB_TheodoiGia
  Left = 176
  Top = 114
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Tham Kh'#7843'o Gi'#225
  ClientHeight = 479
  ClientWidth = 1226
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 1226
    Height = 105
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label65: TLabel
      Left = 148
      Top = 73
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 462
      Top = 73
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object EdTungay: TwwDBDateTimePicker
      Left = 200
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 4
      OnExit = CbMaDVExit
    end
    object EdDenngay: TwwDBDateTimePicker
      Left = 521
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 5
      OnExit = CbMaDVExit
    end
    object EdMADT: TDBEditEh
      Left = 545
      Top = 12
      Width = 77
      Height = 22
      TabStop = False
      Alignment = taLeftJustify
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clBtnFace
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      Visible = True
    end
    object CbbNhaCungCap: TDbLookupComboboxEh2
      Left = 200
      Top = 12
      Width = 343
      Height = 22
      ControlLabel.Width = 36
      ControlLabel.Height = 16
      ControlLabel.Caption = #272#417'n v'#7883
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      DynProps = <>
      DropDownBox.AutoFitColWidths = False
      DropDownBox.Columns = <
        item
          FieldName = 'TENDT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'T'#234'n'
          Width = 342
        end
        item
          FieldName = 'MADT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'M'#227
          Width = 60
        end>
      DropDownBox.ListSource = DsDM_KH_NCC
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.Width = 423
      EmptyDataInfo.Color = clInfoBk
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      KeyField = 'MADT'
      ListField = 'TENDT'
      ListSource = DsDM_KH_NCC
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 0
      Visible = True
      OnChange = CbbNhaCungCapExit
      OnExit = CbbNhaCungCapExit
    end
    object CbbKhoHang: TDbLookupComboboxEh2
      Left = 200
      Top = 40
      Width = 343
      Height = 22
      ControlLabel.Width = 21
      ControlLabel.Height = 16
      ControlLabel.Caption = 'Kho'
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      DynProps = <>
      DropDownBox.AutoFitColWidths = False
      DropDownBox.Columns = <
        item
          FieldName = 'TENKHO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'T'#234'n'
          Width = 342
        end
        item
          FieldName = 'MAKHO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'M'#227
          Width = 60
        end>
      DropDownBox.ListSource = DsDMKHO
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.Width = 423
      EmptyDataInfo.Color = clInfoBk
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      KeyField = 'MAKHO'
      ListField = 'TENKHO'
      ListSource = DsDMKHO
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 2
      Visible = True
      OnChange = CbbKhoHangExit
      OnExit = CbbKhoHangExit
    end
    object EdMaKho: TDBEditEh
      Left = 545
      Top = 40
      Width = 77
      Height = 22
      TabStop = False
      Alignment = taLeftJustify
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clBtnFace
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      Visible = True
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 105
    Width = 1226
    Height = 353
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'#9'Ch'#7913'ng t'#7915
      'HOADON_SO'#9'10'#9'S'#7889' h'#243'a '#273#417'n'#9'F'#9'Ch'#7913'ng t'#7915
      'HINHTHUC_GIA'#9'20'#9'H'#236'nh th'#7913'c'#9'F'
      'TenDvtLon'#9'7'#9'BOX'#9'F'#9#272'VT'
      'QuyDoi'#9'4'#9'='#9'F'#9#272'VT'
      'TenDvt'#9'7'#9'SKU'#9'F'#9#272'VT'
      'SOLUONG2'#9'7'#9'BOX'#9'F'#9'S'#7889' l'#432#7907'ng'
      'SOLUONG'#9'7'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
      'DONGIA2'#9'12'#9'BOX'#9'F'#9#272#417'n gi'#225
      'DONGIA'#9'12'#9'SKU'#9'F'#9#272#417'n gi'#225
      'TL_CK'#9'8'#9'M'#7863't h'#224'ng'#9'% Chi'#7871't Kh'#7845'u m'#7863't h'#224'ng'#9'% Chi'#7871't Kh'#7845'u'
      'CK_HD'#9'8'#9'H'#243'a '#273#417'n'#9'% Chi'#7871't kh'#7845'u h'#243'a '#273#417'n'#9'% Chi'#7871't Kh'#7845'u'
      'TENKHO'#9'30'#9'T'#234'n kho'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = CHUNGTU_CT
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object Status: TStatusBar
    Left = 0
    Top = 458
    Width = 1226
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c '
      ShortCut = 32856
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 234
    Top = 196
  end
  object QrDM_KH_NCC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrDM_KH_NCCBeforeOpen
    Parameters = <
      item
        Name = 'LOAI'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select MADT, TENDT'
      '  from DM_KH_NCC'
      ' where LOAI = :LOAI'
      'order by TENDT')
    Left = 206
    Top = 196
  end
  object spFB_CHUNGTU_CT_List_ThamKhao: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'spFB_CHUNGTU_CT_List_ThamKhao;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MABH'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@SENDER'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 368
    Top = 184
  end
  object CHUNGTU_CT: TDataSource
    DataSet = spFB_CHUNGTU_CT_List_ThamKhao
    Left = 396
    Top = 184
  end
  object DsDM_KH_NCC: TDataSource
    DataSet = QrDM_KH_NCC
    Left = 212
    Top = 224
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 244
    Top = 224
  end
end
