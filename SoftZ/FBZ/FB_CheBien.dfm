object FrmFB_CheBien: TFrmFB_CheBien
  Left = 104
  Top = 195
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Ch'#7871' Bi'#7871'n - Nh'#224' H'#224'ng'
  ClientHeight = 573
  ClientWidth = 858
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    858
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 858
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 858
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 224
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 232
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 304
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 312
      Top = 0
      Cursor = 1
      Hint = 'Phi'#7871'u nh'#7853'p'
      Caption = 'In'
      DropdownMenu = PopupMenu2
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object SepChecked: TToolButton
      Left = 399
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 407
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton5: TToolButton
      Left = 479
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 487
      Top = 0
      Cursor = 1
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = ToolButton4Click
    end
    object SepBarcode: TToolButton
      Left = 574
      Top = 0
      Width = 8
      Caption = 'SepBarcode'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 582
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 858
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 850
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 850
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'TU_MAKHO'#9'5'#9'M'#227#9'F'#9'Kho xu'#7845't'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho nh'#7853'p'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 850
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 850
        inherited Panel1: TPanel
          Width = 850
          ParentColor = False
          ExplicitWidth = 850
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      DesignSize = (
        850
        496)
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 850
        Height = 134
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 287
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 56
          Top = 86
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 588
          Top = 38
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 584
          Top = 62
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdSCT: TwwDBEdit
          Left = 344
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object DBMemo1: TDBMemo
          Left = 112
          Top = 82
          Width = 725
          Height = 44
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsNX
          ParentCtl3D = False
          TabOrder = 8
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 656
          Top = 34
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_GIAO'
          DataSource = DsNX
          ParentCtl3D = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 656
          Top = 58
          Width = 181
          Height = 22
          Ctl3D = False
          DataField = 'NG_NHAN'
          DataSource = DsNX
          ParentCtl3D = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object cbbKhoNPL: TDbLookupComboboxEh2
          Left = 112
          Top = 34
          Width = 337
          Height = 22
          ControlLabel.Width = 74
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho xu'#7845't NPL'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'TU_MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
          OnCloseUp = cbbKhoNPLCloseUp
          OnDropDown = cbbKhoNPLDropDown
        end
        object EdKhoNPL: TDBEditEh
          Left = 452
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'TU_MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object cbbKhoCB: TDbLookupComboboxEh2
          Left = 112
          Top = 58
          Width = 337
          Height = 22
          ControlLabel.Width = 72
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho nh'#7853'p CB'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DsDMKHOCB
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DsDMKHOCB
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 5
          Visible = True
        end
        object EdKhoCB: TDBEditEh
          Left = 452
          Top = 58
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 134
        Width = 850
        Height = 362
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Tag = 1
          Left = 0
          Top = 16
          Width = 850
          Height = 346
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False'
            'EX_BTN;CustomEdit;ExBt;F'
            'STT;CustomEdit;ExBt;F')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'MAVT'#9'14'#9'M'#227#9'F'#9'H'#224'ng ho'#225
            'TENVT'#9'30'#9'T'#234'n'#9'T'#9'H'#224'ng ho'#225
            'DVT'#9'8'#9#272'VT'#9'T'#9'H'#224'ng ho'#225
            'SOLUONG'#9'10'#9'Duy'#7879't'#9'F'#9'S'#7889' l'#432#7907'ng'
            'SOTIEN'#9'13'#9'Gi'#225' nh'#7853'p'#9'T'#9'Th'#224'nh ti'#7873'n'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F'
            'STT'#9'3'#9'#'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
      object PaChiTietNPL: TPageControl
        Left = 20
        Top = 231
        Width = 525
        Height = 214
        ActivePage = TabSheet3
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        Visible = False
        object TabSheet3: TTabSheet
          Caption = 'Nguy'#234'n ph'#7909' li'#7879'u'
          object GrCheBienNPL: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 517
            Height = 185
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            Selected.Strings = (
              'KHOACT'#9'38'#9'KHOACT'#9#9
              'KHOA'#9'38'#9'KHOA'#9#9
              'STT'#9'10'#9'STT'#9#9
              'MAVT'#9'30'#9'MAVT'#9#9
              'TENVT'#9'200'#9'TENVT'#9#9
              'DVT'#9'20'#9'DVT'#9#9
              'DONGIA_REF'#9'10'#9'DONGIA_REF'#9#9
              'DONGIA'#9'10'#9'DONGIA'#9#9
              'SOLUONG_DANHMUC'#9'10'#9'SOLUONG_DANHMUC'#9#9
              'SOLUONG_CHUNGTU'#9'10'#9'SOLUONG_CHUNGTU'#9#9
              'SOLUONG'#9'10'#9'SOLUONG'#9#9
              'SOTIEN'#9'10'#9'SOTIEN'#9#9
              'GHICHU'#9'200'#9'GHICHU'#9#9
              'LOC'#9'5'#9'LOC'#9#9
              'RSTT'#9'20'#9'RSTT'#9#9)
            IniAttributes.Delimiter = ';;'
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            Align = alClient
            Ctl3D = False
            DataSource = DsCheBienNPL
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
            MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgTabExitsOnLastCol]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            TitleLines = 2
            TitleButtons = False
            UseTFields = False
            OnFieldChanged = GrCheBienCPFieldChanged
            TitleImageList = DataMain.ImageSort
            PadColumnStyle = pcsPadHeader
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Chi ph'#237
          ImageIndex = 1
          object GrCheBienCP: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 517
            Height = 185
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            Selected.Strings = (
              'DONGIA'#9'10'#9'DONGIA'
              'GHICHU'#9'200'#9'GHICHU'
              'LK_TENCP'#9'20'#9'Chi ph'#237
              'LOC'#9'5'#9'LOC'
              'MACP'#9'20'#9'MACP'
              'SOLUONG'#9'10'#9'SOLUONG'
              'SOLUONG_CHUNGTU'#9'10'#9'SOLUONG_CHUNGTU'
              'SOLUONG_DANHMUC'#9'10'#9'SOLUONG_DANHMUC'
              'SOTIEN'#9'10'#9'SOTIEN'
              'STT'#9'10'#9'STT')
            IniAttributes.Delimiter = ';;'
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            Align = alClient
            Ctl3D = False
            DataSource = DsCheBienCP
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgTabExitsOnLastCol]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            TitleLines = 2
            TitleButtons = False
            UseTFields = False
            OnFieldChanged = GrCheBienCPFieldChanged
            TitleImageList = DataMain.ImageSort
            PadColumnStyle = pcsPadHeader
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 716
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 716
    ExplicitTop = 39
  end
  object ExBt: TwwExpandButton
    Left = 592
    Top = 304
    Width = 46
    Height = 17
    DisableThemes = False
    Grid = PaChiTietNPL
    OnBeforeExpand = ExBtBeforeExpand
    OnAfterCollapse = ExBtAfterCollapse
    Caption = 'ExBt'
    TabOrder = 3
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 414
    Top = 256
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnExecute = CmdFilterComExecute
    end
    object CmdExBarcode: TAction
      Caption = 'M'#227' v'#7841'ch'
      Hint = 'Xu'#7845't d'#7919' li'#7879'u m'#227' v'#7841'ch ra file'
      OnExecute = CmdExBarcodeExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdImportExcel: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdFromPN: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' phi'#7871'u nh'#7853'p'
      OnExecute = CmdFromPNExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdImportTxt: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Text'
      OnExecute = CmdImportTxtExecute
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
      OnExecute = CmdChecktonExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
    object CmdDMNPL: TAction
      Caption = 'Nguy'#234'n ph'#7909' li'#7879'u'
      OnExecute = CmdDMNPLExecute
    end
    object CmdDMTD: TAction
      Caption = 'Th'#7921'c '#273#417'n'
      OnExecute = CmdDMTDExecute
    end
    object CmdDMCP: TAction
      Caption = 'Chi ph'#237
      OnExecute = CmdDMCPExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldsFetchMethod = fmUseTFields
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'TU_MAKHO'
      'MAKHO')
    FilterOptimization = fdUseActiveIndex
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 386
    Top = 256
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHEBIEN'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'LOC = :LOC')
    Left = 554
    Top = 374
    object QrNXXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXTU_MAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho xu'#7845't'
      DisplayWidth = 5
      FieldName = 'TU_MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TU_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho xu'#7845't'
      FieldKind = fkLookup
      FieldName = 'LK_TU_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'TU_MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXTHANHTOAN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225
      DisplayWidth = 18
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho nh'#7853'p'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho nh'#7853'p'
      DisplayWidth = 47
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 10
      FieldName = 'MADT'
      Visible = False
      FixedChar = True
      Size = 15
    end
    object QrNXNG_GIAO: TWideStringField
      DisplayWidth = 50
      FieldName = 'NG_GIAO'
      Visible = False
      Size = 50
    end
    object QrNXSOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'SOTIEN'
      Visible = False
      OnChange = QrNXSOTIENChange
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXNG_NHAN: TWideStringField
      FieldName = 'NG_NHAN'
      Visible = False
      Size = 30
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
    end
    object QrNXSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrNXLOCKED_BY: TIntegerField
      FieldName = 'LOCKED_BY'
    end
    object QrNXLK_LOCKED_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LOCKED_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'LOCKED_BY'
      Size = 200
      Lookup = True
    end
    object QrNXLOCKED_DATE: TDateTimeField
      FieldName = 'LOCKED_DATE'
    end
    object QrNXSOTIEN_CHUA_HAOHUT_NPL: TFloatField
      FieldName = 'SOTIEN_CHUA_HAOHUT_NPL'
    end
    object QrNXSOTIEN_HAOHUT_NPL: TFloatField
      FieldName = 'SOTIEN_HAOHUT_NPL'
    end
    object QrNXSOTIEN_NPL: TFloatField
      FieldName = 'SOTIEN_NPL'
    end
    object QrNXSOTIEN_CHIPHI: TFloatField
      FieldName = 'SOTIEN_CHIPHI'
    end
    object QrNXSOTIEN_VON: TFloatField
      FieldName = 'SOTIEN_VON'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterInsert
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHEBIEN_CT'
      ' where'#9'KHOA =:KHOA'
      'order by STT')
    Left = 582
    Top = 374
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      FixedChar = True
      Size = 15
    end
    object QrCTSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      DisplayWidth = 12
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNganTinhTrang'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTDONGIA_LE: TFloatField
      FieldName = 'DONGIA_LE'
    end
    object QrCTSOTIEN_LE: TFloatField
      FieldName = 'SOTIEN_LE'
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTDVT: TWideStringField
      FieldName = 'DVT'
    end
    object QrCTLK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DINHMUC: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_DINHMUC'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'CHECKED_DINHMUC'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_CHIPHI: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_CHIPHI'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'CHECKED_CHIPHI'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTSOTIEN_CHUA_HAOHUT_NPL: TFloatField
      FieldName = 'SOTIEN_CHUA_HAOHUT_NPL'
    end
    object QrCTSOTIEN_HAOHUT_NPL: TFloatField
      FieldName = 'SOTIEN_HAOHUT_NPL'
    end
    object QrCTSOTIEN_NPL: TFloatField
      FieldName = 'SOTIEN_NPL'
    end
    object QrCTSOTIEN_CHIPHI: TFloatField
      FieldName = 'SOTIEN_CHIPHI'
    end
    object QrCTDONGIA_VON: TFloatField
      FieldName = 'DONGIA_VON'
    end
    object QrCTSOTIEN_VON: TFloatField
      FieldName = 'SOTIEN_VON'
    end
    object QrCTDONGIA_CHUA_HAOHUT_NPL: TFloatField
      FieldName = 'DONGIA_CHUA_HAOHUT_NPL'
    end
    object QrCTDONGIA_NPL: TFloatField
      FieldName = 'DONGIA_NPL'
    end
    object QrCTDONGIA_CHIPHI: TFloatField
      FieldName = 'DONGIA_CHIPHI'
    end
    object QrCTDONGIA: TFloatField
      FieldName = 'DONGIA'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTDONGIA_HAOHUT_NPL: TFloatField
      FieldName = 'DONGIA_HAOHUT_NPL'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 554
    Top = 402
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 582
    Top = 402
  end
  object PopupMenu2: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 512
    Top = 248
    object Phiuvnchuynnib1: TMenuItem
      Tag = 1
      Caption = 'Theo gi'#225' b'#225'n'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      OnClick = CmdPrintExecute
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Phiucginhp1: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'Theo gi'#225' v'#7889'n'
      Hint = 'In phi'#7871'u'
      OnClick = CmdPrintExecute
    end
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 700
    Top = 252
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 604
    Top = 248
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOLUONG'
      'SOTIEN_CHUA_HAOHUT_NPL'
      'SOTIEN_HAOHUT_NPL'
      'SOTIEN_NPL'
      'SOTIEN_CHIPHI'
      'SOTIEN'
      'SOTIEN_VON'
      'SOTIEN_LE')
    DetailFields.Strings = (
      'SOLUONG'
      'SOTIEN_CHUA_HAOHUT_NPL'
      'SOTIEN_HAOHUT_NPL'
      'SOTIEN_NPL'
      'SOTIEN_CHIPHI'
      'SOTIEN'
      'SOTIEN_VON'
      'SOTIEN_LE')
    Left = 636
    Top = 248
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 456
    Top = 248
    object Lytphiunhp1: TMenuItem
      Action = CmdFromPN
      Visible = False
    end
    object LydliutfileExcel1: TMenuItem
      Action = CmdImportExcel
    end
    object LydliutfileText1: TMenuItem
      Action = CmdImportTxt
      Visible = False
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel2: TMenuItem
      Tag = 1
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object Xachititchngt1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Splithtmthng1: TMenuItem
      Action = CmdSapthutu
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Kimtraslngtnkho1: TMenuItem
      Action = CmdCheckton
    end
  end
  object CHUNGTU_LAYPHIEU: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'FB_CHUNGTU_LAYPHIEU;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 678
    Top = 340
  end
  object QrCheBienCP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    BeforeOpen = QrCheBienCPBeforeOpen
    BeforeInsert = QrCheBienCPBeforeInsert
    AfterInsert = QrCheBienCPAfterInsert
    BeforeEdit = QrCTBeforeEdit
    BeforeDelete = QrCheBienCPBeforeDelete
    OnCalcFields = QrCheBienCPCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHEBIEN_CT_CHIPHI'
      ' where'#9'KHOA =:KHOA'
      'order by KHOACT, STT')
    Left = 750
    Top = 246
    object QrCheBienCPDONGIA: TFloatField
      DisplayWidth = 10
      FieldName = 'DONGIA'
    end
    object QrCheBienCPGHICHU: TWideStringField
      DisplayWidth = 200
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCheBienCPLK_TENCP: TWideStringField
      DisplayLabel = 'Chi ph'#237
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'LK_TENCP'
      LookupDataSet = DataMain.QrDM_CHIPHI
      LookupKeyFields = 'MACP'
      LookupResultField = 'TENCP'
      KeyFields = 'MACP'
      Lookup = True
    end
    object QrCheBienCPLOC: TWideStringField
      DisplayWidth = 5
      FieldName = 'LOC'
      Size = 5
    end
    object QrCheBienCPMACP: TWideStringField
      DisplayWidth = 20
      FieldName = 'MACP'
    end
    object QrCheBienCPSOLUONG: TFloatField
      DisplayWidth = 10
      FieldName = 'SOLUONG'
    end
    object QrCheBienCPSOLUONG_CHUNGTU: TFloatField
      DisplayWidth = 10
      FieldName = 'SOLUONG_CHUNGTU'
      OnChange = QrCheBienCPSOLUONG_CHUNGTUChange
    end
    object QrCheBienCPSOLUONG_DANHMUC: TFloatField
      DisplayWidth = 10
      FieldName = 'SOLUONG_DANHMUC'
    end
    object QrCheBienCPSOTIEN: TFloatField
      DisplayWidth = 10
      FieldName = 'SOTIEN'
    end
    object QrCheBienCPSTT: TIntegerField
      DisplayWidth = 10
      FieldName = 'STT'
    end
    object QrCheBienCPKHOA: TGuidField
      DisplayWidth = 38
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCheBienCPKHOACT: TGuidField
      DisplayWidth = 38
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCheBienCPKHOACT2: TGuidField
      DisplayWidth = 38
      FieldName = 'KHOACT2'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCheBienCPRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
  end
  object DsCheBienCP: TDataSource
    DataSet = QrCheBienCP
    Left = 758
    Top = 298
  end
  object QrCheBienNPL: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    BeforeOpen = QrCheBienNPLBeforeOpen
    BeforeInsert = QrCheBienNPLBeforeInsert
    AfterInsert = QrCheBienNPLAfterInsert
    BeforeEdit = QrCTBeforeEdit
    BeforeDelete = QrCheBienNPLBeforeDelete
    OnCalcFields = QrCheBienNPLCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_CHEBIEN_CT_NPL'
      ' where'#9'KHOA =:KHOA'
      'order by KHOACT, STT')
    Left = 798
    Top = 246
    object QrCheBienNPLKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCheBienNPLKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCheBienNPLSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCheBienNPLMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 30
    end
    object QrCheBienNPLTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCheBienNPLDVT: TWideStringField
      FieldName = 'DVT'
    end
    object QrCheBienNPLDONGIA_REF: TFloatField
      FieldName = 'DONGIA_REF'
    end
    object QrCheBienNPLDONGIA: TFloatField
      FieldName = 'DONGIA'
    end
    object QrCheBienNPLSOLUONG_DANHMUC: TFloatField
      FieldName = 'SOLUONG_DANHMUC'
    end
    object QrCheBienNPLSOLUONG_CHUNGTU: TFloatField
      FieldName = 'SOLUONG_CHUNGTU'
      OnChange = QrCheBienNPLSOLUONG_CHUNGTUChange
    end
    object QrCheBienNPLSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrCheBienNPLSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrCheBienNPLGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCheBienNPLLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCheBienNPLSOLUONG_CHUA_HAOHUT: TFloatField
      FieldName = 'SOLUONG_CHUA_HAOHUT'
    end
    object QrCheBienNPLSOTIEN_CHUA_HAOHUT: TFloatField
      FieldName = 'SOTIEN_CHUA_HAOHUT'
    end
    object QrCheBienNPLHAOHUT: TFloatField
      FieldName = 'HAOHUT'
    end
    object QrCheBienNPLSOTIEN_HAOHUT: TFloatField
      FieldName = 'SOTIEN_HAOHUT'
    end
    object QrCheBienNPLKHOACT2: TGuidField
      FieldName = 'KHOACT2'
      FixedChar = True
      Size = 38
    end
    object QrCheBienNPLRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
  end
  object DsCheBienNPL: TDataSource
    DataSet = QrCheBienNPL
    Left = 806
    Top = 306
  end
  object FB_CHEBIEN_LAYNGUYENPHULIEU: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'FB_CHEBIEN_LAYNGUYENPHULIEU;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end>
    Left = 718
    Top = 396
  end
  object FB_CHEBIEN_LAYCHIPHI: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'FB_CHEBIEN_LAYCHIPHI;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end>
    Left = 758
    Top = 340
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 224
    Top = 256
    object NguynphliuFB1: TMenuItem
      Action = CmdDMNPL
      ImageIndex = 42
    end
    object CmdNPL: TMenuItem
      Tag = 1
      Action = CmdDMTD
      ImageIndex = 42
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from DM_KHO'
      'where LOAIKHO in (select MA_HOTRO from V_LOAI_KHO)'
      'order by MAKHO'
      ' ')
    Left = 640
    Top = 448
  end
  object QrDMKHOCB: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from DM_KHO'
      
        'where LOAIKHO in (select MA_HOTRO from V_LOAI_KHO where MA_HOTRO' +
        ' = '#39'03'#39')'
      'order by MAKHO'
      ' ')
    Left = 696
    Top = 448
  end
  object QrFB_DM_HH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM FB_DM_THUCDON'
      'where (CHECKED_DINHMUC = 1) or (CHECKED_CHIPHI= 1)')
    Left = 112
    Top = 248
  end
  object CHECK_DINHMUC: TADOCommand
    CommandText = 'select MAVT from FB_DM_HH_DINHMUC where MAVT =:MAVT'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'MAVT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    Left = 604
    Top = 332
  end
  object CHECK_CHIPHI: TADOCommand
    CommandText = 'select MAVT from FB_DM_HH_CHIPHI where MAVT =:MAVT'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'MAVT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 636
    Top = 388
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 646
    Top = 482
  end
  object DsDMKHOCB: TDataSource
    DataSet = QrDMKHOCB
    Left = 694
    Top = 482
  end
end
