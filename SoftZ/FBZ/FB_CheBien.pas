﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_CheBien;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwstr, ShellAPI,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Mask, Grids,
  Wwdbgrid, ToolWin, wwdbedit, Wwdotdot, Wwdbcomb, frameScanCode,
  FB_frameScanCode, wwcheckbox, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmFB_CheBien = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepChecked: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label10: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    DBMemo1: TDBMemo;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXNG_NHAN: TWideStringField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    Label5: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Phiuvnchuynnib1: TMenuItem;
    N2: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TU_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopupMenu2: TAdvPopupMenu;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    CmdExBarcode: TAction;
    Bevel1: TBevel;
    N3: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    PopDetail: TAdvPopupMenu;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    CmdEmptyDetail: TAction;
    N5: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrNXDGIAI: TWideMemoField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    CmdImportExcel: TAction;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    CmdFromPN: TAction;
    Lytphiunhp1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepBarcode: TToolButton;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    LydliutfileText1: TMenuItem;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    QrNXLOCKED_BY: TIntegerField;
    QrNXLK_LOCKED_NAME: TWideStringField;
    QrNXLOCKED_DATE: TDateTimeField;
    CmdCheckton: TAction;
    N7: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    N10: TMenuItem;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTLK_TENVT: TWideStringField;
    QrCheBienCP: TADOQuery;
    DsCheBienCP: TDataSource;
    ExBt: TwwExpandButton;
    QrCheBienCPKHOACT2: TGuidField;
    QrCheBienCPKHOACT: TGuidField;
    QrCheBienCPKHOA: TGuidField;
    QrCheBienCPSTT: TIntegerField;
    QrCheBienCPMACP: TWideStringField;
    QrCheBienCPDONGIA: TFloatField;
    QrCheBienCPSOLUONG_DANHMUC: TFloatField;
    QrCheBienCPSOLUONG_CHUNGTU: TFloatField;
    QrCheBienCPSOLUONG: TFloatField;
    QrCheBienCPSOTIEN: TFloatField;
    QrCheBienCPGHICHU: TWideStringField;
    QrCheBienCPLOC: TWideStringField;
    QrCheBienCPLK_TENCP: TWideStringField;
    GrCheBienCP: TwwDBGrid2;
    PaChiTietNPL: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GrCheBienNPL: TwwDBGrid2;
    QrCheBienNPL: TADOQuery;
    DsCheBienNPL: TDataSource;
    QrCheBienNPLKHOACT: TGuidField;
    QrCheBienNPLKHOA: TGuidField;
    QrCheBienNPLSTT: TIntegerField;
    QrCheBienNPLMAVT: TWideStringField;
    QrCheBienNPLTENVT: TWideStringField;
    QrCheBienNPLDVT: TWideStringField;
    QrCheBienNPLDONGIA_REF: TFloatField;
    QrCheBienNPLDONGIA: TFloatField;
    QrCheBienNPLSOLUONG_DANHMUC: TFloatField;
    QrCheBienNPLSOLUONG_CHUNGTU: TFloatField;
    QrCheBienNPLSOLUONG: TFloatField;
    QrCheBienNPLSOTIEN: TFloatField;
    QrCheBienNPLGHICHU: TWideStringField;
    QrCheBienNPLLOC: TWideStringField;
    GrDetail: TwwDBGrid2;
    FB_CHEBIEN_LAYNGUYENPHULIEU: TADOStoredProc;
    FB_CHEBIEN_LAYCHIPHI: TADOStoredProc;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopDanhmuc: TAdvPopupMenu;
    CmdNPL: TMenuItem;
    CmdDMNPL: TAction;
    CmdDMTD: TAction;
    CmdDMCP: TAction;
    NguynphliuFB1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMKHOCB: TADOQuery;
    QrFB_DM_HH: TADOQuery;
    QrCTLK_DINHMUC: TBooleanField;
    QrCTLK_CHIPHI: TBooleanField;
    CHECK_DINHMUC: TADOCommand;
    CHECK_CHIPHI: TADOCommand;
    QrCTSOTIEN_CHUA_HAOHUT_NPL: TFloatField;
    QrCTSOTIEN_HAOHUT_NPL: TFloatField;
    QrCTSOTIEN_NPL: TFloatField;
    QrCTSOTIEN_CHIPHI: TFloatField;
    QrCTDONGIA_VON: TFloatField;
    QrCTSOTIEN_VON: TFloatField;
    QrCheBienNPLSOLUONG_CHUA_HAOHUT: TFloatField;
    QrCheBienNPLSOTIEN_CHUA_HAOHUT: TFloatField;
    QrCheBienNPLHAOHUT: TFloatField;
    QrCheBienNPLSOTIEN_HAOHUT: TFloatField;
    QrCheBienNPLKHOACT2: TGuidField;
    QrNXSOTIEN_CHUA_HAOHUT_NPL: TFloatField;
    QrNXSOTIEN_HAOHUT_NPL: TFloatField;
    QrNXSOTIEN_NPL: TFloatField;
    QrNXSOTIEN_CHIPHI: TFloatField;
    QrNXSOTIEN_VON: TFloatField;
    QrCheBienNPLRSTT: TIntegerField;
    QrCheBienCPRSTT: TIntegerField;
    QrCTDONGIA_CHUA_HAOHUT_NPL: TFloatField;
    QrCTDONGIA_NPL: TFloatField;
    QrCTDONGIA_CHIPHI: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTDONGIA_HAOHUT_NPL: TFloatField;
    cbbKhoNPL: TDbLookupComboboxEh2;
    EdKhoNPL: TDBEditEh;
    cbbKhoCB: TDbLookupComboboxEh2;
    EdKhoCB: TDBEditEh;
    DsDMKHO: TDataSource;
    DsDMKHOCB: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbKHOXUATNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure BtnInClick(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure CbKHOXUATBeforeDropDown(Sender: TObject);
    procedure CbKHOXUATCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdFromPNExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure ExBtBeforeExpand(Sender: TObject);
    procedure ExBtAfterCollapse(Sender: TObject);
    procedure QrCheBienCPCalcFields(DataSet: TDataSet);
    procedure GrCheBienCPFieldChanged(Sender: TObject; Field: TField);
    procedure QrCheBienNPLCalcFields(DataSet: TDataSet);
    procedure QrCheBienCPBeforeDelete(DataSet: TDataSet);
    procedure QrCheBienNPLBeforeDelete(DataSet: TDataSet);
    procedure QrCheBienCPBeforeInsert(DataSet: TDataSet);
    procedure QrCheBienNPLBeforeInsert(DataSet: TDataSet);
    procedure QrCheBienNPLBeforeOpen(DataSet: TDataSet);
    procedure QrCheBienCPBeforeOpen(DataSet: TDataSet);
    procedure QrCheBienCPAfterInsert(DataSet: TDataSet);
    procedure QrCheBienNPLAfterInsert(DataSet: TDataSet);
    procedure ToolButton4Click(Sender: TObject);
    procedure CmdDMNPLExecute(Sender: TObject);
    procedure CmdDMTDExecute(Sender: TObject);
    procedure CmdDMCPExecute(Sender: TObject);
    procedure QrCheBienNPLSOLUONG_CHUNGTUChange(Sender: TField);
    procedure QrCheBienCPSOLUONG_CHUNGTUChange(Sender: TField);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cbbKhoNPLDropDown(Sender: TObject);
    procedure cbbKhoNPLCloseUp(Sender: TObject; Accept: Boolean);
  private
  	mLCT, mPrefix, qtyFmt: String;
	mCanEdit, mObsolete, disabledConfirm: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType, qtyRound: Integer;
    fSQL, fStr: String;
    khoaDeleted : TGuid;
    procedure GenerateChildrenDetail(pMAVT: String; pKHOACT: TGuid; pSoLuong: Double = 0);
    procedure DeleteChildrenDetail(pQuery: TCustomADODataSet; pKhoaCT: TGuid);
    procedure ChangeDetail;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmFB_CheBien: TFrmFB_CheBien;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, isLib, FB_Sapthutu,
    isCommon, GuidEx, isFile, ImportExcel, InlabelChungtu,
  FB_CheckTonkho, OfficeData, FB_ChonDsma, FB_ChonPhieunhap, ExcelData,
  FB_DmvtNVL, FB_Dmhh, DmChiphi;

{$R *.DFM}

const
	FORM_CODE = 'FB_CHEBIEN';

    (*
    ** Form Events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.ExBtAfterCollapse(Sender: TObject);
begin
    QrCheBienCP.Filter := '';
    QrCheBienNPL.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.ExBtBeforeExpand(Sender: TObject);
begin
    with QrCT do
    begin
        if IsEmpty or (FieldByName('MAVT').AsString = '') then
            Abort;

        QrCheBienCP.Filter := 'KHOACT=' + TGuidEx.ToString(TGuidField(FieldByName('KHOACT')).AsGuid);
        QrCheBienNPL.Filter := 'KHOACT=' + TGuidEx.ToString(TGuidField(FieldByName('KHOACT')).AsGuid);
        GrDetail.SetActiveField('STT');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.Execute;
begin
   	mLCT := 'FCB';

	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;

    frDate.CbbLoc.Enabled := True;

    mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
    qtyFmt := '#,##0.00;-#,##0.00;#';
    qtyRound := -2;
    disabledConfirm := False;
    khoaDeleted := TGuidEx.EmptyGuid;
  	mTrigger := False;
    mObsolete := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.FormShow(Sender: TObject);
begin
	// Open Database
     Wait(PREPARING);
     InitFmtAndRoud(mLCT);
    OpenDataSets([QrDMKHO, QrDMKHOCB, QrFB_DM_HH]);

    SetDisplayFormat(QrFB_DM_HH, sysCurFmt);

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrNX, ['SOTIEN_HAOHUT_NPL'], curHaohutFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA_VON', 'DONGIA_LE', 'DONGIA_CHUA_HAOHUT_NPL',
                    'DONGIA_HAOHUT_NPL', 'DONGIA_NPL', 'DONGIA_CHIPHI', 'DONGIA'], ctPriceFmt);
        SetDisplayFormat(QrCT, ['DONGIA_HAOHUT_NPL', 'SOTIEN_HAOHUT_NPL'], curHaohutFmt);
    end;

    with QrCheBienCP do
    begin
	    SetDisplayFormat(QrCheBienCP, sysCurFmt);
        SetDisplayFormat(QrCheBienCP, ['SOLUONG', 'SOLUONG_CHUNGTU', 'SOLUONG_DANHMUC'], qtyFmt);
        SetDisplayFormat(QrCheBienCP, ['DONGIA'], ctPriceFmt);
    end;

    with QrCheBienNPL do
    begin
	    SetDisplayFormat(QrCheBienNPL, sysCurFmt);
        SetDisplayFormat(QrCheBienNPL, ['HAOHUT', 'SOLUONG', 'SOLUONG_CHUNGTU', 'SOLUONG_DANHMUC', 'SOLUONG_CHUA_HAOHUT'], qtyFmt);
        SetDisplayFormat(QrCheBienNPL, ['DONGIA'], ctPriceFmt);
    end;

    // Customize Grid
	SetCustomGrid([FORM_CODE,
                    FORM_CODE + '_CT',
                    FORM_CODE + '_CT_CHIPHI',
                    FORM_CODE + '_CT_NPL'],
                 [GrBrowse, GrDetail, GrCheBienCP, GrCheBienNPL]);
    SetDictionary([QrNX, QrCT, QrCheBienCP, QrCheBienNPL],
                    [FORM_CODE,
                    FORM_CODE + '_CT',
                    FORM_CODE + '_CT_CHIPHI',
                    FORM_CODE + '_CT_NPL'],
                  [Filter, Nil, Nil, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;
    ClearWait;
    CmdReRead.Execute;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
    	CloseDataSets([QrDMKHO, QrDMKHOCB, QrFB_DM_HH]);
    finally
    end;
    Action := caFree;
end;
    (*
    ** End: Form Events
    *)

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
            case fType of
            0:	// Nganh
                SQL.Add('and KHOA in (select a.KHOA from FB_CHEBIEN_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_CHEBIEN.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
            1:	// Nhom
                SQL.Add('and KHOA in (select a.KHOA from FB_CHEBIEN_CT a, FB_DM_HH b where a.KHOA = FB_CHEBIEN.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
            else
                SQL.Add('and KHOA in (select KHOA from FB_CHEBIEN_CT where KHOA = FB_CHEBIEN.KHOA and MAVT in (' + fStr + '))');
            end;

			SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
        QrDMKHO.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdNewExecute(Sender: TObject);
begin
    QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdSaveExecute(Sender: TObject);
begin
    QrCheBienCP.CheckBrowseMode;
    QrCheBienNPL.CheckBrowseMode;
	QrCT.CheckBrowseMode;
	QrNX.Post;

    exSaveDetails(QrCT);
    exSaveDetails(QrCheBienNPL);
    exSaveDetails(QrCheBienCP);

//    SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdCancelExecute(Sender: TObject);
begin
    QrCheBienCP.CancelBatch;
    QrCheBienNPL.CancelBatch;
    QrCT.CancelBatch;
    QrNX.Cancel;

    if QrNX.IsEmpty then
        ActiveSheet(PgMain, 0)
    else
        ActiveSheet(PgMain);

//    if QrNX.FieldByName('LOCKED_BY').AsInteger = sysLogonUID then
//        SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdCheckedExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdChecktonExecute(Sender: TObject);
begin
     QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
    with QrCT do
	    if FrmFB_CheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdDelExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdDMCPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_CHIPHI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChiphi, FrmDmChiphi);
    FrmDmChiphi.Execute(r);

    DataMain.QrDM_CHIPHI.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdDMNPLExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    begin
        QrFB_DM_HH.Requery;
        with QrCT do
        if not isEmpty then
        begin
           GenerateChildrenDetail(FieldByName('MAVT').AsString, TGuidField(FieldByName('KHOACT')).AsGuid, FieldByName('SOLUONG').AsFloat);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdDMTDExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    begin
        QrFB_DM_HH.Requery;
        with QrCT do
        if not isEmpty then
        begin
            GenerateChildrenDetail(FieldByName('MAVT').AsString, TGuidField(FieldByName('KHOACT')).AsGuid, FieldByName('SOLUONG').AsFloat);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';

procedure TFrmFB_CheBien.CmdFromPNExecute(Sender: TObject);
var
	n: TGUID;
    mKho, mMavt: String;
    mSoluong, mDongia, mGiavon: Double;
begin
    QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmFB_ChonPhieunhap, FrmFB_ChonPhieunhap);
    with QrNX do
	    n := FrmFB_ChonPhieunhap.Execute(
        	False,
        	'',
            FieldByName('MAKHO').AsString);

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with CHUNGTU_LAYPHIEU do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        mKho := FieldByName('MAKHO').AsString;

        if QrNX.FieldByName('TU_MAKHO').AsString <> mKho then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('TU_MAKHO').AsString := mKho;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mGiavon := FieldByName('GIAVON').AsFloat;

            with QrCT do
            if not Locate('MAVT', mMavt, []) then
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := exVNDRound(mGiavon, ctPriceRound);
                FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
                TGuidEx.NewGuidDate(FieldByName('KHOACT'));

                GenerateChildrenDetail(mMavt, TGuidField(FieldByName('KHOACT')).AsGuid, mSoluong);
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFldSL,sFile: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if sFile = '' then
    	Exit;


    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';

            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;

                    if SameText(Fields[i].DisplayLabel, 'SOLUONG') and (sFldSL = '') then
                    begin
                       sFldSL := Fields[i].FieldName;
                    end;

                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;

                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if not QrCT.Locate('MAVT', FieldByName(sFld).AsString, []) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                            TGuidEx.NewGuidDate(QrCT.FieldByName('KHOACT'));
                            GenerateChildrenDetail(FieldByName(sFld).AsString, TGuidField(QrCT.FieldByName('KHOACT')).AsGuid, FieldByName(sFldSL).AsFloat);
                        end;
                    end;
                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';
procedure TFrmFB_CheBien.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;

            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            GenerateChildrenDetail(s, TGuidField(FieldByName('KHOACT')).AsGuid, x);

            if FieldByName('TINHTRANG').AsString <> '01' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
    //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
    //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and MANGANH in (select MANGANH from DM_NGANH where LOAI = ''FBTP'') AND isnull(BO, 0) = 0 ') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdEmptyDetailExecute(Sender: TObject);
begin
    if QrCT.State in [dsEdit, dsInsert] then
        QrCT.Cancel;

    disabledConfirm := True;
    GrDetail.SetActiveRow(0);
    exEmptyDetails(QrCT, GrDetail);
    disabledConfirm := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
	with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    CmdDMNPL.Enabled := n = 1;
    CmdDMTD.Enabled := n = 1;
    CmdDMCP.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;

    LydliutfileExcel1.Enabled := bEmptyCT;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

	(*
    ** Master Db
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLAfterInsert(DataSet: TDataSet);
begin
    with DataSet do
    begin
        FieldByName('LOC').AsString := QrNX.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
        FieldByName('KHOACT').Value := QrCT.FieldByName('KHOACT').Value;
        TGuidEx.NewGuidDate(FieldByName('KHOACT2'));
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLBeforeInsert(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienNPLSOLUONG_CHUNGTUChange(Sender: TField);
var
    haoHut, soLuong, donGia, soLuongDanhMuc, soLuongChungTu, soLuongChuaHaoHut,
    soTienHaoHut, soTienChuaHaoHut, soTien: Double;
begin
    with QrCheBienNPL do
    begin
        soLuongChungTu := FieldByName('SOLUONG_CHUNGTU').AsFloat;
        soLuongDanhMuc := FieldByName('SOLUONG_DANHMUC').AsFloat;
        donGia := FieldByName('DONGIA').AsFloat;
        haoHut := FieldByName('HAOHUT').AsFloat;

        soLuongChuaHaoHut := soLuongDanhMuc * soLuongChungTu;
        soTienChuaHaoHut := soLuongChuaHaoHut * donGia;

        soTienHaoHut := exVNDRound(soTienChuaHaoHut * haoHut / 100, curHaohutRound);
        soLuong := exVNDRound(soLuongChuaHaoHut + (soLuongChuaHaoHut* haoHut / 100), qtyRound);
        soTien := exVNDRound(soTienChuaHaoHut + soTienHaoHut, ctCurRound);

        FieldByName('SOLUONG_CHUA_HAOHUT').AsFloat := exVNDRound(soLuongChuaHaoHut, qtyRound);
        FieldByName('SOTIEN_CHUA_HAOHUT').AsFloat := exVNDRound(soTienChuaHaoHut, ctCurRound);

        FieldByName('SOTIEN_HAOHUT').AsFloat := soTienHaoHut;
        FieldByName('SOLUONG').AsFloat := soLuong;
        FieldByName('SOTIEN').AsFloat := soTien;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    with PgMain do
    begin
        ActivePageIndex := 0;
        OnChange(Nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_WAREHOUSE = 'Chuyển kho không hợp lệ.';
    RS_NPL_EMPTY	 = 'Nguyên phụ liệu không có dữ liệu';
    RS_CHIPHI_EMPTY		 = 'Chi phí không có dữ liệu';
procedure TFrmFB_CheBien.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'TU_MAKHO', 'MAKHO']) then
	    	Abort;

//    	if FieldByName('TU_MAKHO').AsString = FieldByName('MAKHO').AsString then
//        begin
//        	ErrMsg(RS_INVALID_WAREHOUSE);
//            CbKHOXUAT.SetFocus;
//            Abort;
//        end;

		exValidClosing(FieldByName('NGAY').AsDateTime);
	end;

    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrNXSOTIENChange(Sender: TField);
begin
	with QrNX do
    	FieldByName('THANHTOAN').AsFloat := exVNDRound(FieldByName('SOTIEN').AsFloat, ctCurRound);
end;
	(*
    ** End: Master DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTBeforeOpen(DataSet: TDataSet);
begin
	(DataSet as TADOQuery).Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCTBeforePost(DataSet: TDataSet);
var
	mMa: String;
    checkDinhMuc, checkChiPhi: Boolean;
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if IsDuplicateCode(QrCT, FieldByName('MAVT'), True) then
           Abort;

        mMa := FieldByName('MAVT').AsString;
        checkDinhMuc := FieldByName('LK_DINHMUC').AsBoolean;
        checkChiPhi := FieldByName('LK_CHIPHI').AsBoolean;


        if checkDinhMuc then
        begin
            with CHECK_DINHMUC do
            begin
                Prepared := True;
                Parameters[0].Value := mMa;
                if Execute.RecordCount < 1 then
                begin
                    ErrMsg(RS_NPL_EMPTY);
                    Abort;
                end;
            end;
        end;

        if checkChiPhi then
        begin
            with CHECK_CHIPHI do
            begin
                Prepared := True;
                Parameters[0].Value := mMa;
                if Execute.RecordCount < 1 then
                begin
                    ErrMsg(RS_CHIPHI_EMPTY);
                    Abort;
                end;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;


    if not disabledConfirm then
        if not DeleteConfirm then
            Abort;

    khoaDeleted := TGuidField(QrCT.FieldByName('KHOACT')).AsGuid;
    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienCPAfterInsert(DataSet: TDataSet);
begin
    with DataSet do
    begin
        FieldByName('LOC').AsString := QrNX.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
        FieldByName('KHOACT').Value := QrCT.FieldByName('KHOACT').Value;
        TGuidEx.NewGuidDate(FieldByName('KHOACT2'));
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienCPBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

 (*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienCPBeforeInsert(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienCPBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCheBienCPCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCheBienCPSOLUONG_CHUNGTUChange(Sender: TField);
var
    soLuong, donGia: Double;
begin
    with QrCheBienCP do
    begin
        donGia := FieldByName('DONGIA').AsFloat;
        soLuong := FieldByName('SOLUONG_CHUNGTU').AsFloat * FieldByName('SOLUONG_DANHMUC').AsFloat;
        FieldByName('SOLUONG').AsFloat :=  exVNDRound(soLuong, qtyRound);
        FieldByName('SOTIEN').AsFloat :=  exVNDRound(soLuong * donGia, ctCurRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTAfterDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if khoaDeleted <> TGuidEx.EmptyGuid then
    begin
        DeleteChildrenDetail(QrCheBienNPL,khoaDeleted);
        DeleteChildrenDetail(QrCheBienCP, khoaDeleted);
        khoaDeleted := TGuidEx.EmptyGuid;
    end;

    vlTotal1.Update(True);
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.QrCTAfterInsert(DataSet: TDataSet);
begin
    with QrCT do
    begin
        FieldByName('LOC').AsString := QrNX.FieldByName('LOC').AsString;
        FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
        TGuidEx.NewGuidDate(FieldByName('KHOACT'));
    end;
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    soLuong, donGiaVon, donGiaLe, donGiaChuaHaoHutNPL,
    donGiaHaoHutNPL, donGiaNPL, donGiaChiPhi, donGia,
    soTienChuaHaoHutNPL, soTienHaoHutNPL, soTienNPL, soTienChiPhi, soTien: Double;

begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        soLuong  := FieldByName('SOLUONG').AsFloat;
        donGiaVon := FieldByName('DONGIA_VON').AsFloat;
        donGiaLe := FieldByName('DONGIA_LE').AsFloat;
        donGiaChuaHaoHutNPL := FieldByName('DONGIA_CHUA_HAOHUT_NPL').AsFloat;
        donGiaHaoHutNPL := FieldByName('DONGIA_HAOHUT_NPL').AsFloat;
        donGiaNPL := FieldByName('DONGIA_NPL').AsFloat;
        donGiaChiPhi := FieldByName('DONGIA_CHIPHI').AsFloat;
        donGia := FieldByName('DONGIA').AsFloat;

        soTienChuaHaoHutNPL := exVNDRound(soLuong * donGiaChuaHaoHutNPL, ctCurRound);
        soTienHaoHutNPL := exVNDRound(soLuong * donGiaHaoHutNPL, curHaohutRound);
        soTienNPL := exVNDRound(soTienChuaHaoHutNPL + soTienHaoHutNPL, ctCurRound);
        soTienChiPhi := exVNDRound(soLuong * donGiaChiPhi, ctCurRound);
        soTien := soTienNPL + soTienChiPhi;

        FieldByName('SOTIEN_VON').AsFloat := exVNDRound(soLuong * donGiaVon, ctCurRound);
        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(soLuong * donGiaLe, ctCurRound);

        FieldByName('SOTIEN_CHUA_HAOHUT_NPL').AsFloat := soTienChuaHaoHutNPL;
        FieldByName('SOTIEN_HAOHUT_NPL').AsFloat := soTienHaoHutNPL;
        FieldByName('SOTIEN_NPL').AsFloat := soTienNPL;
        FieldByName('SOTIEN_CHIPHI').AsFloat :=  soTienChiPhi;
        FieldByName('SOTIEN').AsFloat := soTien;

        ChangeDetail;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.QrCTMAVTChange(Sender: TField);
var
	donGiaNPL, donGia, donGiaVon, donGiaLe, donGiaHaoHut : Double;
    donGiaChuaHaoHutNPL, donGiaChiPhi, donGiaChuaHaoHut: Double;
    pMAVT: string;
begin
	exDotFBMavt(3, QrFB_DM_HH, Sender);
    pMAVT := Sender.AsString;
    // Update referenced fields
    with QrCT do
    begin
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := pMAVT;

            Open;
            if RecordCount <> 0 then
            begin
                donGiaChuaHaoHutNPL := FieldByName('DINHMUC_CHUA_HAOHUT').AsFloat;
                donGiaChiPhi := FieldByName('THANHTIEN_CHIPHI').AsFloat;
                donGiaNPL := FieldByName('THANHTIEN_DINHMUC').AsFloat;
                donGiaHaoHut := FieldByName('SOTIEN_DINHMUC_HAOHUT').AsFloat;
                donGiaVon := FieldByName('GIAVON').AsFloat;
                donGiaLe := FieldByName('GIABAN_CHUA_VAT').AsFloat;
            end;

            QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
            QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
        end;

        donGiaChuaHaoHut := donGiaChuaHaoHutNPL +  donGiaChiPhi;
        donGia := donGiaNPL + donGiaChiPhi;

        FieldByName('DONGIA_CHUA_HAOHUT_NPL').AsFloat := exVNDRound(donGiaChuaHaoHutNPL, ctPriceRound);
        FieldByName('DONGIA_HAOHUT_NPL').AsFloat := exVNDRound(donGiaHaoHut, ctPriceRound);
        FieldByName('DONGIA_NPL').AsFloat := exVNDRound(donGiaNPL, ctPriceRound);
        FieldByName('DONGIA_CHIPHI').AsFloat := exVNDRound(donGiaChiPhi, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(donGia, ctPriceRound);

        FieldByName('DONGIA_VON').AsFloat := exVNDRound(donGiaVon, ctPriceRound);
        FieldByName('DONGIA_LE').AsFloat := exVNDRound(donGiaLe, ctPriceRound);

        GenerateChildrenDetail(pMAVT, TGuidField(FieldByName('KHOACT')).AsGuid);

        ChangeDetail;
    end;
    GrDetail.InvalidateCurrentRow;
end;



(*
    ** End: Detail DB
    *)

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CbKHOXUATBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CbKHOXUATCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CbKHOXUATNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;

	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.GrCheBienCPFieldChanged(Sender: TObject; Field: TField);
begin
    GrCheBienCP.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown;
end;

procedure TFrmFB_CheBien.cbbKhoNPLCloseUp(Sender: TObject; Accept: Boolean);
begin
    QrDMKHO.Filter := '';
    QrDMKHO.Filtered := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.cbbKhoNPLDropDown(Sender: TObject);
begin
    QrDMKHO.Filter := 'LOC='+QuotedStr(sysLoc);
    QrDMKHO.Filtered := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
            FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('SOTIEN_CHUA_HAOHUT_NPL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_CHUA_HAOHUT_NPL').AsFloat);
        ColumnByName('SOTIEN_HAOHUT_NPL').FooterValue :=
        	FormatFloat(curHaohutFmt, FieldByName('SOTIEN_HAOHUT_NPL').AsFloat);
        ColumnByName('SOTIEN_NPL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_NPL').AsFloat);
        ColumnByName('SOTIEN_CHIPHI').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_CHIPHI').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN_VON').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_VON').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
        with QrCheBienNPL do
        begin
            Close;
            Open;
        end;
        with QrCheBienCP do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CheBien.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.ChangeDetail;
var
    khoaCT: TGUID;
	soLuong: Double;
begin
    with QrCT do
    begin
        khoaCT := TGuidField(FieldByName('KHOACT')).AsGuid;
        soLuong := FieldByName('SOLUONG').AsFloat;
        with QrCheBienNPL do
        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin
                if (khoaCT = TGuidField(FieldByName('KHOACT')).AsGuid) and  Locate('KHOACT;MAVT', VarArrayOf([FieldByName('KHOACT').Value, FieldByName('MAVT').AsString]), []) then
                begin
                    Edit;
                    FieldByName('SOLUONG_CHUNGTU').AsFloat := exVNDRound(soLuong, qtyRound);
                end;
                Next;
            end;
        end;

        with QrCheBienCP do
        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin
                if (khoaCT = TGuidField(FieldByName('KHOACT')).AsGuid) and Locate('KHOACT;MACP', VarArrayOf([FieldByName('KHOACT').Value, FieldByName('MACP').AsString]), []) then
                begin
                    Edit;
                    FieldByName('SOLUONG_CHUNGTU').AsFloat := exVNDRound(soLuong, qtyRound);
                end;
                Next;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.ToolButton4Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.GenerateChildrenDetail(pMAVT: String; pKHOACT: TGuid; pSoLuong: Double = 0);
var
   mMAVT, mTENVT, mDVT, mMACP, mTENCP: String;
   soLuongDanhMuc, haoHut, soLuong, soLuongChuaHaoHut, soTienChuaHaoHut, soTienHaoHut,
   donGia, soTien : Double;
   donGiaChiPhi, soLuongDanhMucChiPhi, soLuongChungTuChiPhi: Double;
begin
    if mTrigger then
        Exit;

    DeleteChildrenDetail(QrCheBienNPL, pKHOACT);
    DeleteChildrenDetail(QrCheBienCP, pKHOACT);

    with FB_CHEBIEN_LAYNGUYENPHULIEU do
    begin
        if Active then
            Close;

        Prepared := True;
        Parameters[1].Value := pMAVT;
        Open;

        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin
                mMAVT := FieldByName('MAVT_NPL').AsString;
                mTENVT := FieldByName('TENVT').AsString;
                mDVT := FieldByName('TenDvt').AsString;

                soLuongDanhMuc := FieldByName('DINHMUC').AsFloat;
                donGia := FieldByName('DONGIA').AsFloat;
                haoHut := FieldByName('HAOHUT').AsFloat;

                with QrCheBienNPL do
                begin
                    Append;

                    FieldByName('MAVT').AsString := mMAVT;
                    FieldByName('TENVT').AsString := mTENVT;
                    FieldByName('DVT').AsString := mDVT;

                    FieldByName('SOLUONG_DANHMUC').AsFloat := exVNDRound(soLuongDanhMuc, qtyRound);
                    FieldByName('DONGIA_REF').AsFloat := exVNDRound(donGia, qtyRound);
                    FieldByName('DONGIA').AsFloat := exVNDRound(donGia, qtyRound);
                    FieldByName('HAOHUT').AsFloat := exVNDRound(haoHut, qtyRound);
                end;
                Next;
            end;
        end;
    end;

    with FB_CHEBIEN_LAYCHIPHI do
    begin
        if Active then
            Close;

        Prepared := True;
        Parameters[1].Value := pMAVT;
        Open;

        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin

                mMACP := FieldByName('MACP').AsString;
                soLuongDanhMucChiPhi := FieldByName('SOLUONG').AsFloat;
                donGiaChiPhi := FieldByName('DONGIA').AsFloat;

                with QrCheBienCP do
                begin
                    Append;
                    FieldByName('MACP').AsString := mMACP;
                    FieldByName('SOLUONG_DANHMUC').AsFloat := exVNDRound(soLuongDanhMucChiPhi, qtyRound);
                    FieldByName('DONGIA').AsFloat := exVNDRound(donGiaChiPhi, ctPriceRound);
                end;
                Next;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_CheBien.DeleteChildrenDetail(pQuery: TCustomADODataSet; pKhoaCT: TGuid);
var
    b: Boolean;
begin
    DeleteConfirm(False);
	with pQuery do
    if not IsEmpty then
    begin
        First;
        while not Eof do
        begin
            if TGuidField(FieldByName('KHOACT')).AsGuid = pKhoaCT then
                Delete
            else
                Next;
        end;
    end;
    DeleteConfirm(True);
end;

    (*
    **  End: Others
    *)


end.
