﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonPhieuKM;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, kbmMemTable,
  MemTableDataEh, MemTableEh;

type
  TFrmFB_ChonPhieuKM = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNX: TDataSource;
    QrPHIEUNX: TADOQuery;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNXSCT: TWideStringField;
    QrPHIEUNXTUNGAY: TDateTimeField;
    QrPHIEUNXDENNGAY: TDateTimeField;
    QrPHIEUNXTL_CK: TFloatField;
    QrPHIEUNXLYDO: TWideStringField;
    QrPHIEUNXNGAY: TDateTimeField;
    QrPHIEUNXLCT: TWideStringField;
    QrPHIEUNXDGIAI: TWideMemoField;
    QrPHIEUNXKHOA: TGuidField;
    DsDummyEh: TDataSource;
    TbDummyEh: TMemTableEh;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrPHIEUNXBeforeOpen(DataSet: TDataSet);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute: TGUID;
  end;

var
  FrmFB_ChonPhieuKM: TFrmFB_ChonPhieuKM;

implementation

uses
	isDb, ExCommon, isLib, GuidEx;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonPhieuKM.Execute;
begin

	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUNX.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNX]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('FB_CHON_PHIEU_KM', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.FormShow(Sender: TObject);

begin
   	OpenDataSets([QrPHIEUNX]);
	mSQL := QrPHIEUNX.SQL.Text;
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
    GrBrowse.SetFocus;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.CmdRefreshExecute(Sender: TObject);
var
    dTungay, dDenngay: TDateTime;
begin
    dTungay  := EdTungay.Date;
    dDenngay := EdDenngay.Date;

   	if (mTungay <> dTungay)  or (mDenngay <> dDenngay)    then
	begin
        mTungay := dTungay;
        mDenngay := dDenngay;

        with QrPHIEUNX do
        begin
			Close;
			Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.QrPHIEUNXBeforeOpen(DataSet: TDataSet);
begin
	with QrPHIEUNX do
    begin
        Parameters[0].Value := mTungay;
        Parameters[1].Value := mDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieuKM.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;

    end;
end;

end.
