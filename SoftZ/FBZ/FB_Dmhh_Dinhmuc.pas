﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Dmhh_Dinhmuc;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid, wwdblook, Vcl.Menus,
  AdvMenus;

type
  TFrmFB_Dmhh_Dinhmuc = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    Image1: TImage;
    CmdAudit: TAction;
    ToolButton10: TToolButton;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    GrDinhMuc: TwwDBGrid2;
    TabSheet2: TTabSheet;
    GrChiPhi: TwwDBGrid2;
    QrDMDinhMuc: TADOQuery;
    QrDMDinhMucMAVT: TWideStringField;
    QrDMDinhMucMAVT_NPL: TWideStringField;
    QrDMDinhMucTENVT_NPL: TWideStringField;
    QrDMDinhMucDINHMUC: TFloatField;
    QrDMDinhMucHAOHUT: TFloatField;
    QrDMDinhMucGIAVON: TFloatField;
    QrDMDinhMucDONGIA: TFloatField;
    QrDMDinhMucLK_GIAVON: TFloatField;
    QrDMDinhMucGHICHU: TWideStringField;
    QrDMDinhMucTHANHTIEN_CHUA_HAOHUT: TFloatField;
    QrDMDinhMucTHANHTIEN: TFloatField;
    QrDMDinhMucSOTIEN_HAOHUT: TFloatField;
    DsDMDinhMuc: TDataSource;
    QrDMChiPhi: TADOQuery;
    WideStringField1: TWideStringField;
    QrDMChiPhiMACP: TWideStringField;
    QrDMChiPhiLK_TENCP: TWideStringField;
    QrDMChiPhiSOLUONG: TFloatField;
    QrDMChiPhiDONGIA: TFloatField;
    QrDMChiPhiGHICHU: TWideStringField;
    QrDMChiPhiTHANHTIEN: TFloatField;
    DsDMChiPhi: TDataSource;
    PopDanhmuc: TAdvPopupMenu;
    CmdNPL: TMenuItem;
    ToolButton2: TToolButton;
    CmdDmvt: TAction;
    CmdDmtd: TAction;
    CmdDmcp: TAction;
    CmdCP: TMenuItem;
    ToolButton8: TToolButton;
    QrDMDinhMucLK_MaDvt: TIntegerField;
    QrDMDinhMucLK_TenDvt: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDMDinhMucBeforeOpen(DataSet: TDataSet);
    procedure QrDMChiPhiBeforeOpen(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrDMDinhMucBeforeDelete(DataSet: TDataSet);
    procedure QrDMChiPhiBeforeDelete(DataSet: TDataSet);
    procedure QrDMDinhMucAfterDelete(DataSet: TDataSet);
    procedure QrDMChiPhiAfterDelete(DataSet: TDataSet);
    procedure QrDMDinhMucAfterPost(DataSet: TDataSet);
    procedure QrDMChiPhiAfterPost(DataSet: TDataSet);
    procedure QrDMChiPhiBeforePost(DataSet: TDataSet);
    procedure QrDMChiPhiMACPChange(Sender: TField);
    procedure QrDMChiPhiSOLUONGChange(Sender: TField);
    procedure QrDMDinhMucBeforePost(DataSet: TDataSet);
    procedure QrDMDinhMucDINHMUCChange(Sender: TField);
    procedure QrDMDinhMucMAVT_NPLChange(Sender: TField);
    procedure GrActionUpdateFooter(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdDmcpExecute(Sender: TObject);
    procedure QrDMChiPhiAfterInsert(DataSet: TDataSet);
    procedure QrDMDinhMucDINHMUCValidate(Sender: TField);

  private
    mCanEdit: Boolean;
    mMavt: String;
  	mRet: Boolean;
    mGrid: TwwDBGrid2;
    mQuery: TADOQuery;
    mDs: TDataSource;
    mLoai: Integer;
  public
  	function Execute(pCanEdit: Boolean; pMa, pTen: String; pLoai: Integer): Boolean;
  end;

var
  FrmFB_Dmhh_Dinhmuc: TFrmFB_Dmhh_Dinhmuc;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, isCommon, Rights,
    ImportExcel, OfficeData, isFile, FB_Dmhh, FB_DmvtNVL, DmChiphi;

{$R *.DFM}

const
	FORM_CODE = 'FB_DM_HH';
    FORM_CODE2 = 'FB_DM_HH_DINHMUC';
    FORM_CODE3 = 'FB_DM_HH_CHIPHI';

    TABLE_NAME  = 'FB_DM_HH';
    TABLE_NAME2  = 'FB_DM_HH_DINHMUC';
    TABLE_NAME3  = 'FB_DM_HH_CHIPHI';

    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Dmhh_Dinhmuc.Execute(pCanEdit: Boolean; pMa, pTen: String; pLoai: Integer): Boolean;
begin
    mCanEdit := pCanEdit;

    mMavt := pMa;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;
    mLoai := pLoai;
    ShowModal;

    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
    DataMain.FB_UpdateHH(mMavt);
    exReSyncRecord(FrmFB_Dmhh.QrDMVT);
    GrActionUpdateFooter(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdDmcpExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_CHIPHI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChiphi, FrmDmChiphi);
    FrmDmChiphi.Execute(r);

    DataMain.QrDM_CHIPHI.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Dmhh_Dinhmuc.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
        DataMain.QrFB_DM_NPL.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, mQuery, mCanEdit);
    CmdDmvt.Enabled := True;
    CmdDmcp.Enabled := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDMDinhMuc, TABLE_NAME2);
    AddAllFields(QrDMChiPhi, TABLE_NAME3);
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDMDinhMuc, QrDMChiPhi, DataMain.QrDM_CHIPHI, DataMain.QrFB_DM_NPL]);

    with QrDMDinhMuc do
    begin
        SetDisplayFormat(QrDMDinhMuc, sysCurFmt);
        SetDisplayFormat(QrDMDinhMuc, ['DINHMUC', 'HAOHUT'], sysPerFmt);
        SetDisplayFormat(QrDMDinhMuc, ['SOTIEN_HAOHUT'], curHaohutFmt);
        SetDisplayFormat(QrDMDinhMuc, ['DONGIA'], sysPriceFmt);
    	SetShortDateFormat(QrDMDinhMuc);
    end;

    with QrDMChiPhi do
    begin
        SetDisplayFormat(QrDMChiPhi, sysCurFmt);
	    SetDisplayFormat(QrDMChiPhi, ['SOLUONG'], sysQtyFmt);
        SetDisplayFormat(QrDMChiPhi, ['DONGIA'], sysPriceFmt);
    	SetShortDateFormat(QrDMChiPhi);
    end;


    SetCustomGrid([FORM_CODE2, FORM_CODE3], [GrDinhMuc, GrChiPhi]);
    SetDictionary([QrDMDinhMuc, QrDMChiPhi], [FORM_CODE2, FORM_CODE3], [nil, Nil]);
    PgMain.OnChange(PgMain);


//    if mLoai = 1 then
//    begin
//       GrChiPhi.ReadOnly := True;
//       PgMain.ActivePageIndex := 0;
//       GrDinhMuc.SetFocus;
//    end
//    else if mLoai = 2 then
//    begin
//       GrDinhMuc.ReadOnly := True;
//       PgMain.ActivePageIndex := 1;
//       GrChiPhi.SetFocus;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.GrActionUpdateFooter(Sender: TObject);
begin
    with GrDinhMuc, FrmFB_Dmhh.QrDMVT do
    begin
		ColumnByName('THANHTIEN_CHUA_HAOHUT').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('DINHMUC_CHUA_HAOHUT').AsFloat);
        ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THANHTIEN_DINHMUC').AsFloat);
        ColumnByName('SOTIEN_HAOHUT').FooterValue :=
        	FormatFloat(curHaohutFmt, FieldByName('SOTIEN_DINHMUC_HAOHUT').AsFloat);
    end;

    with GrChiPhi, FrmFB_Dmhh.QrDMVT do
    begin
        if ColumnByName('THANHTIEN').Visible then
        begin
           ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THANHTIEN_CHIPHI').AsFloat);
        end
        else
        begin
            ColumnByName('DONGIA').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('THANHTIEN_CHIPHI').AsFloat);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	begin

        	mQuery := QrDMDinhMuc;
            mDs := DsDMDinhMuc;
            mGrid  := GrDinhMuc;
        end;
    1:
    	begin
            mQuery := QrDMChiPhi;
            mDs := DsDMChiPhi;
            mGrid  := GrChiPhi;
        end;
    end;

	HideAudit;
    mGrid.SetFocus;
    GrActionUpdateFooter(mGrid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
   AllowChange := CheckBrowseDataSet(mQuery, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiAfterDelete(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiAfterInsert(DataSet: TDataSet);
begin
    with QrDMChiPhi do
    begin
        FieldByName('SOLUONG').AsFloat := 1;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiAfterPost(DataSet: TDataSet);
begin
    mRet := True;
    DataMain.FB_UpdateHH(mMavt);
    exReSyncRecord(FrmFB_Dmhh.QrDMVT);
    GrActionUpdateFooter(QrDMChiPhi);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiBeforeOpen(DataSet: TDataSet);
begin
    QrDMChiPhi.Parameters[0].Value := mMavt;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiBeforePost(DataSet: TDataSet);
begin
    with QrDMChiPhi do
    begin
        if IsDuplicateCode(QrDMChiPhi, FieldByName('MACP'), True) then
            Abort;
	    if BlankConfirm(QrDMChiPhi, ['MACP', 'DONGIA']) then
	        Abort;

        FieldByName('MAVT').AsString := mMavt;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiMACPChange(Sender: TField);
begin
    exDotMACP(DataMain.QrDM_CHIPHI, Sender, 'TINHTRANG = 0');
    GrChiPhi.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMChiPhiSOLUONGChange(Sender: TField);
var
    sl: Double;
begin
	with QrDMChiPhi do
    begin
        FieldByName('THANHTIEN').AsFloat := FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat;
    end;

    GrChiPhi.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucAfterDelete(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucAfterPost(DataSet: TDataSet);
begin
    mRet := True;
    DataMain.FB_UpdateHH(mMavt);
    exReSyncRecord(FrmFB_Dmhh.QrDMVT);
    GrActionUpdateFooter(QrDMDinhMuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucBeforeOpen(DataSet: TDataSet);
begin
    QrDMDinhMuc.Parameters[0].Value := mMavt;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucBeforePost(DataSet: TDataSet);
begin
    with QrDMDinhMuc do
    begin
        if IsDuplicateCode(QrDMDinhMuc, FieldByName('MAVT_NPL'), True) then
            Abort;
	    if BlankConfirm(QrDMDinhMuc, ['MAVT_NPL', 'DINHMUC', 'DONGIA']) then
            Abort;

        FieldByName('MAVT').AsString := mMavt;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucDINHMUCChange(Sender: TField);
var
    thanhtienChuaHH, haohut, thanhtien: Double;
begin
    with QrDMDinhMuc do
    begin
        thanhtienChuaHH := FieldByName('DINHMUC').AsFloat * FieldByName('DONGIA').AsFloat;
        haohut := exVNDRound(FieldByName('THANHTIEN_CHUA_HAOHUT').AsFloat * (FieldByName('HAOHUT').AsFloat /100), curHaohutRound);

        FieldByName('THANHTIEN_CHUA_HAOHUT').AsFloat := thanhtienChuaHH;
        FieldByName('SOTIEN_HAOHUT').AsFloat := haohut;
        FieldByName('THANHTIEN').AsFloat := exVNDRound(thanhtienChuaHH + haohut, sysCurRound);
    end;
    GrDinhMuc.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucDINHMUCValidate(Sender: TField);
begin
    if not QrDMDinhMuc.FieldByName('DINHMUC').IsNull and  (QrDMDinhMuc.FieldByName('DINHMUC').AsFloat < 0) then
    begin
        ErrMsg(RS_INVALID_QTY);
        Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.QrDMDinhMucMAVT_NPLChange(Sender: TField);
var
    x: Extended;
begin
    exDotFBMavt(2, DataMain.QrFB_DM_NPL, Sender);
    with QrDMDinhMuc do
    begin
        x := FieldByName('LK_GIAVON').AsFloat;
        FieldByName('DONGIA').AsFloat := x;
        FieldByName('GIAVON').AsFloat := x;
    end;
    GrDinhMuc.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.ToolButton2Click(Sender: TObject);
begin
   (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Dmhh_Dinhmuc.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, mDs);
end;

end.
