object FrmFB_Khuyenmai: TFrmFB_Khuyenmai
  Left = 135
  Top = 87
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Khuy'#7871'n M'#227'i - Chi'#7871't Kh'#7845'u, '#272#7891'ng Gi'#225' - Nh'#224' H'#224'ng'
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    792
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 792
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object TntToolButton2: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 224
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 232
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 304
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 312
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton5: TToolButton
      Left = 384
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 392
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton8: TToolButton
      Left = 464
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object TntToolButton3: TToolButton
      Left = 472
      Top = 0
      Cursor = 1
      Action = CmdDiemban
      ImageIndex = 12
    end
    object ToolButton10: TToolButton
      Left = 544
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 552
      Top = 0
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = ToolButton4Click
    end
    object BtnBarCode2: TToolButton
      Left = 639
      Top = 0
      Width = 8
      Caption = 'BtnBarCode2'
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 647
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 504
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 784
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
        ExplicitTop = 483
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 784
        Height = 475
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'LYDO'#9'30'#9'L'#253' do'#9'F'
          'TUNGAY'#9'10'#9'T'#7915' ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENNGAY'#9'10'#9#272#7871'n ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'TUGIO'#9'7'#9'T'#7915' gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENGIO'#9'7'#9#272#7871'n gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'TL_CK'#9'8'#9'% CK'#9'F'#9'Khuy'#7871'n m'#227'i'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsKM
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrBrowseCalcCellColors
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
        ExplicitHeight = 483
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 137
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label2: TLabel
          Left = 259
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 55
          Top = 38
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'N'#7897'i dung'
        end
        object Label5: TLabel
          Left = 58
          Top = 62
          Width = 47
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' ng'#224'y'
        end
        object TntLabel2: TLabel
          Left = 236
          Top = 62
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7871'n ng'#224'y'
        end
        object TntLabel3: TLabel
          Left = 537
          Top = 62
          Width = 35
          Height = 16
          Alignment = taRightJustify
          Caption = '% C.K'
        end
        object Label10: TLabel
          Left = 56
          Top = 110
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object TntLabel4: TLabel
          Left = 120
          Top = 86
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' gi'#7901
        end
        object TntLabel5: TLabel
          Left = 297
          Top = 86
          Width = 44
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7871'n gi'#7901
        end
        object Label3: TLabel
          Left = 639
          Top = 62
          Width = 43
          Height = 16
          Alignment = taRightJustify
          Caption = 'Gi'#225' b'#225'n'
        end
        object Label4: TLabel
          Left = 477
          Top = 86
          Width = 94
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y kh'#243'a phi'#7871'u'
        end
        object Label6: TLabel
          Left = 516
          Top = 110
          Width = 55
          Height = 16
          Alignment = taRightJustify
          Caption = 'L.s'#7917' kh'#243'a'
        end
        object EdSCT: TwwDBEdit
          Left = 316
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdLyDo: TwwDBEdit
          Left = 112
          Top = 34
          Width = 389
          Height = 22
          Ctl3D = False
          DataField = 'NOIDUNG'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTungay: TwwDBDateTimePicker
          Left = 112
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUNGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object CbDenngay: TwwDBDateTimePicker
          Left = 296
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENNGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 6
        end
        object EdTLCK: TwwDBEdit
          Left = 577
          Top = 58
          Width = 55
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'TL_CK'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit1: TDBMemo
          Left = 112
          Top = 106
          Width = 389
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
        end
        object wwDBEdit1: TwwDBDateTimePicker
          Left = 164
          Top = 82
          Width = 49
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUGIO'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 9
          UnboundDataType = wwDTEdtTime
        end
        object wwDBEdit2: TwwDBDateTimePicker
          Left = 348
          Top = 82
          Width = 49
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENGIO'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 10
          UnboundDataType = wwDTEdtTime
        end
        object EdGiaban: TwwDBEdit
          Left = 688
          Top = 58
          Width = 81
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'GIABAN'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object cbNgayKhoa: TwwDBDateTimePicker
          Left = 577
          Top = 82
          Width = 192
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'CHECKED_DATE'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 11
        end
        object EdCheckedDate: TwwDBComboDlg
          Left = 577
          Top = 106
          Width = 192
          Height = 22
          TabStop = False
          OnCustomDlg = EdCheckedDateCustomDlg
          ShowButton = True
          Style = csDropDown
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHECKED_DESC'
          DataSource = DsKM
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 13
          WordWrap = False
          UnboundDataType = wwDefault
        end
        object cbbLyDo: TDbLookupComboboxEh2
          Left = 577
          Top = 10
          Width = 192
          Height = 22
          ControlLabel.Width = 30
          ControlLabel.Height = 16
          ControlLabel.Caption = 'L'#253' do'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'LYDO'
          DataSource = DsKM
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsLYDO_KM
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsLYDO_KM
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
        end
        object cbbHinhThuc: TDbLookupComboboxEh2
          Left = 577
          Top = 34
          Width = 192
          Height = 22
          ControlLabel.Width = 54
          ControlLabel.Height = 16
          ControlLabel.Caption = 'H'#236'nh th'#7913'c'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'HTKM'
          DataSource = DsKM
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsLOAI_KHUYENMAI
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsLOAI_KHUYENMAI
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 137
        Width = 784
        Height = 338
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 784
          Height = 322
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'6'#9'STT'#9'T'
            'MAVT'#9'29'#9'M'#227' h'#224'ng'#9'F'
            'LK_DVT'#9'18'#9#272'VT'#9'T'
            'GIABAN'#9'23'#9'Gi'#225' b'#225'n'#9'T'
            'TL_CK'#9'9'#9'%C.K'#9'F'
            'GIABAN1'#9'17'#9'Gi'#225' b'#225'n'#9'F'#9'Sau C.K')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint, dgProportionalColResize]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
      object Status2: TStatusBar
        Left = 0
        Top = 475
        Width = 784
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 650
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 650
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 146
    Top = 250
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdDiemban: TAction
      Caption = #272'i'#7875'm b'#225'n'
      Hint = 'C'#225'c kho, '#273'i'#7875'm b'#225'n c'#243' chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i'
      OnExecute = CmdDiembanExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChonNhom: TAction
      Caption = 'Ch'#7885'n nh'#243'm h'#224'ng'
      OnExecute = CmdChonNhomExecute
    end
    object CmdEmpty: TAction
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyExecute
    end
    object CmdChonNCC: TAction
      Caption = 'Ch'#7885'n nh'#224' cung c'#7845'p'
      OnExecute = CmdChonNCCExecute
    end
    object CmdChonphieuKM: TAction
      Caption = 'Ch'#7885'n phi'#7871'u khuy'#7871'n m'#227'i'
      OnExecute = CmdChonphieuKMExecute
    end
    object CmdChonNhomNCC: TAction
      Caption = 'Ch'#7885'n nh'#224' cung c'#7845'p v'#224' nh'#243'm h'#224'ng'
      OnExecute = CmdChonNhomNCCExecute
    end
    object CmdIntem: TAction
      Caption = 'In tem'
      OnExecute = CmdIntemExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdImportExcel: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdDMTD: TAction
      Caption = 'Th'#7921'c '#273#417'n'
      OnExecute = CmdDMTDExecute
    end
    object CmdDMNPL: TAction
      Caption = 'Nguy'#234'n ph'#7909' li'#7879'u'
      OnExecute = CmdDMNPLExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsKM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 114
    Top = 250
  end
  object QrDMHH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'a.*, c.TENDT, t.TENLT'
      '  from '#9'FB_DM_THUCDON a '
      '  left join DM_LOAITHUE t on a.LOAITHUE = t.MALT'
      '  left join DM_NCC c on a.MADT = c.MADT'
      'where IsNull(a.BO, 0) = 0'
      'order by a.MAVT'
      '')
    Left = 480
    Top = 291
  end
  object QrKM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKMBeforeOpen
    BeforeInsert = QrKMBeforeInsert
    AfterInsert = QrKMAfterInsert
    BeforeEdit = QrKMBeforeEdit
    BeforePost = QrKMBeforePost
    AfterCancel = QrKMAfterCancel
    AfterScroll = QrKMAfterScroll
    OnCalcFields = QrKMCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_KHUYENMAI'
      ' where     LCT = :LCT')
    Left = 418
    Top = 291
    object QrKMXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrKMIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrKMNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
    end
    object QrKMIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Calculated = True
    end
    object QrKMSCT: TWideStringField
      DisplayLabel = 'S'#7889' ch'#7913'ng t'#7915
      FieldName = 'SCT'
    end
    object QrKMTUNGAY: TDateTimeField
      DisplayLabel = 'T'#7915' ng'#224'y'
      FieldName = 'TUNGAY'
    end
    object QrKMDENNGAY: TDateTimeField
      DisplayLabel = #272#7871'n ng'#224'y'
      FieldName = 'DENNGAY'
    end
    object QrKMGIABAN: TFloatField
      FieldName = 'GIABAN'
      OnChange = QrKMTL_CKChange
    end
    object QrKMDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrKMTL_CK: TFloatField
      DisplayLabel = '% CK'
      FieldName = 'TL_CK'
      OnChange = QrKMTL_CKChange
    end
    object QrKMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKMLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrKMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKMDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrKMDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrKMDENGIO: TDateTimeField
      FieldName = 'DENGIO'
    end
    object QrKMTUGIO: TDateTimeField
      FieldName = 'TUGIO'
    end
    object QrKMLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrKMLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrKMKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMHTKM: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldName = 'HTKM'
      OnChange = QrKMHTKMChange
      Size = 1
    end
    object QrKMLK_HINHTHUC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HINHTHUC'
      LookupDataSet = DataMain.QrLOAI_KHUYENMAI
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'HTKM'
      Size = 200
      Lookup = True
    end
    object QrKMNOIDUNG: TWideStringField
      FieldName = 'NOIDUNG'
      Size = 200
    end
    object QrKMCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrKMCHECKED_BY: TIntegerField
      FieldName = 'CHECKED_BY'
    end
    object QrKMCHECKED_DATE: TDateTimeField
      FieldName = 'CHECKED_DATE'
    end
    object QrKMCAPDO: TIntegerField
      FieldName = 'CAPDO'
    end
    object QrKMCHECKED_DESC: TWideMemoField
      FieldName = 'CHECKED_DESC'
      BlobType = ftWideMemo
    end
    object QrKMLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_KM
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    BeforeEdit = QrCTBeforeEdit
    BeforePost = QrCTBeforePost
    BeforeDelete = QrCTBeforeDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_KHUYENMAI_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 446
    Top = 291
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTMAVT: TWideStringField
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      Size = 15
    end
    object QrCTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTGIANHAPVAT: TFloatField
      FieldName = 'GIANHAPVAT'
    end
    object QrCTGIABAN: TFloatField
      FieldName = 'GIABAN'
    end
    object QrCTTL_LAI: TFloatField
      FieldName = 'TL_LAI'
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTGIABAN1: TFloatField
      FieldName = 'GIABAN1'
      OnChange = QrCTTL_CKChange
    end
    object QrCTTL_CK: TFloatField
      FieldName = 'TL_CK'
      OnChange = QrCTTL_CKChange
    end
    object QrCTTL_LAI1: TFloatField
      FieldName = 'TL_LAI1'
    end
    object QrCTB1: TBooleanField
      FieldName = 'B1'
    end
    object QrCTCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrCTCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrCTUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrCTUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN_CHUA_VAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_QD1: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_QD1'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'QuyDoi'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DVT_BOX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT_BOX'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvtLon'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNganTinhTrang'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTLK_GIANHAPVAT: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIANHAPVAT'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIANHAP'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_GIAVON: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIAVON'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIAVON'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrFB_DM_HH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
  end
  object DsKM: TDataSource
    DataSet = QrKM
    Left = 418
    Top = 319
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 446
    Top = 319
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 276
    Top = 286
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PopItem1: TMenuItem
      Tag = 1
      Caption = 'Phi'#7871'u c'#243' hi'#7879'u l'#7921'c'
      OnClick = PopItemAllClick
    end
    object PopItem2: TMenuItem
      Tag = 2
      Caption = 'Phi'#7871'u h'#7871't hi'#7879'u l'#7921'c'
      OnClick = PopItemAllClick
    end
    object PopItemAll: TMenuItem
      Caption = 'T'#7845't c'#7843' c'#225'c phi'#7871'u'
      OnClick = PopItemAllClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 340
    Top = 286
  end
  object Filter2: TwwFilterDialog2
    SortBy = fdSortByFieldNo
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 116
    Top = 312
  end
  object QrTemp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'MAVT'
      '  from'#9'FB_DM_HH'
      ' where'#9'IsNull(a.BO, 0) = 0'
      '  ')
    Left = 512
    Top = 291
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 228
    Top = 286
    object MenuItem6: TMenuItem
      Action = CmdChonNhom
    end
    object Chnnhcungcp1: TMenuItem
      Action = CmdChonNCC
    end
    object Chnnhcungcpnhmhng1: TMenuItem
      Action = CmdChonNhomNCC
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Chnnhcungcp2: TMenuItem
      Action = CmdChonphieuKM
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object LydliutfileExcel1: TMenuItem
      Action = CmdImportExcel
      SubMenuImages = DataMain.ImageMark
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
  object QrTemp2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_KHUYENMAI_CT'
      ' where  '#9'1 = 1')
    Left = 560
    Top = 291
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from FB_DM_HH'
      'order by TENVT')
    Left = 340
    Top = 384
    object QrDMMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrDMTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
  end
  object MemoDate: TwwMemoDialog
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -16
    Font.Name = 'Courier'
    Font.Style = []
    Caption = 'Danh S'#225'ch C'#225'c L'#7847'n Kh'#243'a Phi'#7871'u'
    DlgLeft = 0
    DlgTop = 0
    DlgWidth = 561
    DlgHeight = 396
    OnInitDialog = MemoDateInitDialog
    Left = 176
    Top = 404
  end
  object QrList: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'FB_KHUYENMAI_DANHSACH;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@TYPE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@STRING'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 324
  end
  object QrList_Tonkho: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'KHUYENMAI_DANHSACH_TON;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@SOTHANG'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 624
    Top = 332
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 224
    Top = 256
    object NguynphliuFB1: TMenuItem
      Action = CmdDMNPL
      ImageIndex = 42
    end
    object CmdNPL: TMenuItem
      Tag = 1
      Action = CmdDMTD
      ImageIndex = 42
    end
  end
end
