﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Banle;

interface

uses
  SysUtils, Classes, Controls, Forms, Vcl.Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit, wwcheckbox, DBGridEh, DBLookupEh, DbLookupComboboxEh2,
  DBCtrlsEh, System.Variants;

type
  TFrmFB_Banle = class(TForm)
    ToolMain: TToolBar;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    QrDMVT: TADOQuery;
    CmdTotal: TAction;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    QrBHNGAY: TDateTimeField;
    QrBHCA: TWideStringField;
    QrBHSOTIEN: TFloatField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHTHUNGAN: TWideStringField;
    QrBHXOA: TWideStringField;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    LbHH2: TLabel;
    EdHH1: TwwDBEdit;
    EdCK: TwwDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    Label24: TLabel;
    Label1: TLabel;
    QrCTBHNG_DC: TWideStringField;
    CmdDel: TAction;
    QrBHNG_HUY: TWideStringField;
    CmdSearch: TAction;
    QrCTBHTRA_DATE: TDateTimeField;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    QrBHSCT: TWideStringField;
    QrCTBHTHUE_SUAT: TFloatField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    QrCTBHTRA_BY: TIntegerField;
    QrBHCHUATHOI: TFloatField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrBHDGIAI: TWideMemoField;
    QrBHIMG: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    QrCTBHTL_CK: TFloatField;
    QrCTBHTIEN_THUE: TFloatField;
    QrCTBHTENTAT: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrBHSOLUONG: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    QrBHMAMADT: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHTHANHTOAN: TFloatField;
    QrBHLK_TENDT: TWideStringField;
    CmdReRead: TAction;
    QrBHLK_TENKHO: TWideStringField;
    CmdFilterCom: TAction;
    QrBHQUAY: TWideStringField;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    QrCTBHSOTIEN: TFloatField;
    Bevel1: TBevel;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHLOAITHUE: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrBHTINHTRANG: TWideStringField;
    QrBHLK_THANHTOAN: TWideStringField;
    QrBHLOC: TWideStringField;
    QrDMKHOLOC: TWideStringField;
    EdTENDT: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBDateTimePicker;
    Label4: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    QrCTBHTL_CK_MAX: TFloatField;
    QrCTBHTL_CK_THEM: TFloatField;
    wwDBEdit6: TwwDBEdit;
    QrBHLCT: TWideStringField;
    QrBHDELIVERY: TBooleanField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHDEBT_BY: TIntegerField;
    QrBHDEBT_DATE: TDateTimeField;
    QrBHLK_DEBT_TEN: TWideStringField;
    QrBHLK_FULLNAME: TWideStringField;
    N2: TMenuItem;
    Item1: TMenuItem;
    Item2: TMenuItem;
    Item3: TMenuItem;
    N3: TMenuItem;
    ItemAll: TMenuItem;
    QrCTBHTL_CKMH: TFloatField;
    QrCTBHTL_CKHD: TFloatField;
    QrCTBHTL_CK_VIPNHOM: TFloatField;
    QrCTBHTL_CK_VIPDM: TFloatField;
    QrCTBHTL_CK_PHIEU: TFloatField;
    QrCTBHCHIETKHAU_MH: TFloatField;
    QrCTBHSOTIEN_SAU_CKMH: TFloatField;
    QrCTBHTL_PHUTHU: TFloatField;
    QrCTBHPHUTHU: TFloatField;
    QrCTBHTHANHTIEN: TFloatField;
    QrCTBHDONGIA_SAU_CK: TFloatField;
    QrCTBHTHANHTIEN_CHUA_CL: TFloatField;
    QrCTBHTHANHTIEN_CL: TFloatField;
    QrCTBHTHANHTIEN_CHUA_VAT: TFloatField;
    QrCTBHDONGIA_CHUA_VAT: TFloatField;
    QrCTBHTIEN_THUE_5: TFloatField;
    QrCTBHTIEN_THUE_10: TFloatField;
    QrCTBHTIEN_THUE_OR: TFloatField;
    QrCTBHTHANHTOAN: TFloatField;
    QrCTBHCKHD_BY: TIntegerField;
    QrCTBHLOC: TWideStringField;
    QrBHQUAY_ORDER: TWideStringField;
    QrBHSCT2: TWideStringField;
    QrBHMAQUAY: TWideStringField;
    QrBHMAQUAY_ORDER: TWideStringField;
    QrBHMABAN: TWideStringField;
    QrBHTL_PHUTHU: TFloatField;
    QrBHPHUTHU: TFloatField;
    QrBHMAVIP: TWideStringField;
    QrBHMAVIP_HOTEN: TWideStringField;
    QrBHSOLUONG_KHACH: TFloatField;
    QrBHSOLUONG_DAT: TFloatField;
    QrBHTL_CKHD: TFloatField;
    QrBHTL_CKVIPDM: TFloatField;
    QrBHCKHD_BY: TIntegerField;
    QrBHSOTIEN_SAU_CKMH: TFloatField;
    QrBHTHANHTIEN: TFloatField;
    QrBHHINHTHUC_GIA: TWideStringField;
    QrBHTHANHTIEN_CHUA_CL: TFloatField;
    QrBHTHANHTIEN_CL: TFloatField;
    QrBHTHANHTIEN_CHUA_VAT: TFloatField;
    QrBHTIEN_THUE: TFloatField;
    QrBHTIEN_THUE_5: TFloatField;
    QrBHTIEN_THUE_10: TFloatField;
    QrBHTIEN_THUE_OR: TFloatField;
    QrBHTHUCTRA: TFloatField;
    QrBHTTOAN_CASH: TFloatField;
    QrBHTTOAN_CARD: TFloatField;
    QrBHTTOAN_TRAHANG: TFloatField;
    QrBHTTOAN_VOUCHER: TFloatField;
    QrBHTTOAN_EWALLET: TFloatField;
    QrBHTTOAN_BANK: TFloatField;
    QrBHTTOAN_MEMBER: TFloatField;
    QrBHTIENTHOI: TFloatField;
    QrBHPRINT_NO: TIntegerField;
    QrBHCO_HOADON: TBooleanField;
    QrBHCN_GHICHU: TWideStringField;
    QrBHORDER_BY: TIntegerField;
    QrBHORDER_DATE: TDateTimeField;
    Label6: TLabel;
    Label7: TLabel;
    wwDBEdit8: TwwDBEdit;
    wwDBEdit9: TwwDBEdit;
    QrDMBAN: TADOQuery;
    QrDMKHUVUC: TADOQuery;
    QrBHLK_TENBAN: TWideStringField;
    QrBHLK_MAKV: TWideStringField;
    QrBHLK_TENKV: TWideStringField;
    QrBHCALC_TIEN_THUE: TFloatField;
    Label8: TLabel;
    wwDBEdit7: TwwDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    wwDBEdit10: TwwDBEdit;
    wwDBEdit11: TwwDBEdit;
    QrCTBHCALC_TIEN_THUE: TFloatField;
    QrCTBHCALC_THUCTRA: TFloatField;
    QrCTBHCALC_THUE_SUAT: TFloatField;
    wwCheckBox1: TwwCheckBox;
    QrCTBHBO: TBooleanField;
    EdMAKHO: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrBHBeforeEdit(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrBHTL_CKChange(Sender: TField);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure ItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure QrCTBHCalcFields(DataSet: TDataSet);
    procedure CbKhoHangDropDown(Sender: TObject);
    procedure CbKhoHangExit(Sender: TObject);
    procedure CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
  private
    mCanEdit, mClose: Boolean;
    mVATMode: Integer;
    mPrinter: TexPrintBill;

    // List filter
    fType, mFilter: Integer;
   	fTungay, fDenngay: TDateTime;
    fKho, fSQL, fStr, mLCT: String;

    procedure Total(fUpdate: Boolean);
  public
  	procedure Execute(r: WORD; bClose: Boolean = True);
  end;

var
  FrmFB_Banle: TFrmFB_Banle;

implementation

uses
	isDb, ExCommon, MainData, Rights, RepEngine, FB_ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'FB_HOADON_BANLE';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.Execute(r: WORD; bClose: Boolean);
begin
    mLCT := 'FBLE';
	mCanEdit := rCanEdit(r);
    mClose := bClose;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    AddAllFields(QrBH, 'FB_BANLE', 0);

    frNavi.DataSet := QrBH;

    // Initial
    fType := 2;
    fStr := '';
    mFilter := 0;
    fSQL := QrBH.SQL.Text;
	EdFrom.Date := Date - sysLateDay;
	EdTo.Date := Date;

    exBillInitial(mPrinter, 'FB_RP_CASHIER.rpt');
    mPrinter.PrintPreview := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.FormShow(Sender: TObject);
begin
    InitFmtAndRoud(mLCT);
    ctCurVatFmt := '#,##0.00;-#,##0.00;#';

	OpenDataSets([DataMain.QrLOC, QrDMKHO, QrDMVT, QrDMBAN, QrDMKHUVUC]);

    CbKhoHang.Value := sysDefKho;
    EdMAKHO.Text := sysDefKho;

	QrDMQUAY.Open;

    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger('POS', 'VAT Form');

    with QrBH do
    begin
	    SetDisplayFormat(QrBH, sysCurFmt);
        SetDisplayFormat(QrBH, ['SOLUONG'], ctQtyFmt);
    	SetShortDateFormat(QrBH);
	    SetDisplayFormat(QrBH, ['NGAY'], 'dd/mm/yyyy hh:nn');
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
        SetDisplayFormat(QrCTBH, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCTBH, ['TL_CK'], sysPerFmt);
        SetDisplayFormat(QrCTBH, ['DONGIA'], ctPriceFmt);
        SetDisplayFormat(QrCTBH, ['TIEN_THUE'], ctCurVatFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);
    
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exBillFinal(mPrinter);
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrBH, QrCTBH, QrDMVT, QrDMKHO, QrDMQUAY]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.PopupMenu1Popup(Sender: TObject);
begin
    Item1.Checked := mFilter = 1;
    Item2.Checked := mFilter = 2;
    Item3.Checked := mFilter = 3;
    ItemAll.Checked := mFilter = 0;
end;

(*
    ** End: Forms events
	*)

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdRefreshExecute(Sender: TObject);
var
	s, s1, sKho: String;
begin
   if not VarIsNull(CbKhoHang.Value) then
        sKho := CbKhoHang.Value
   else
        sKho := '';

   	if (EdFrom.Date <> fTungay) or (EdTo.Date <> fDenngay) or (sKho <> fKho) then
    begin
		fTungay := EdFrom.Date;
		fDenngay := EdTo.Date;
        fKho := sKho;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ sKho + '''');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add(' and KHOA in (select a.KHOA from FB_BANLE_COMBO a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add(' and KHOA in (select a.KHOA from FB_BANLE_COMBO a, FB_DM_HH b where a.KHOA = FB_BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add(' and KHOA in (select KHOA from FB_BANLE_COMBO where KHOA = FB_BANLE.KHOA and MAVT in (' + fStr + '))');
				end;

            s1 := '';
            case mFilter of
                1:
                    s1 := ' and isnull(DELIVERY, 0) = 1 and isnull(TINHTRANG, '''') <> ''T03''';
                2:
                    s1 := ' and isnull(DELIVERY, 0) = 1 and isnull(TINHTRANG, '''') = ''T03''';
                3:
                    s1 := ' and isnull(DELIVERY, 0) = 1';
            end;
            SQL.Add(s1);
			SQL.Add(' order by NGAY desc, SCT desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
begin
    QrDMKHO.Filter := '';
    QrDMKHO.Filtered := True;
	with QrDMQUAY do
    begin
    	Close;
        Open;
    end;
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CbKhoHangDropDown(Sender: TObject);
begin
    if not sysIsCentral then
    begin
        QrDMKHO.Filter := 'LOC='+QuotedStr(sysLoc);
        QrDMKHO.Filtered := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CbKhoHangExit(Sender: TObject);
var
   sKho : string;
begin
   if not VarIsNull(CbKhoHang.Value) then
        sKho := CbKhoHang.Value
   else
        sKho := '';

    EdMAKHO.Text :=  sKho;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdSaveExecute(Sender: TObject);
begin
	QrCTBH.UpdateBatch;
	QrBH.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdCancelExecute(Sender: TObject);
begin
	QrCTBH.CancelBatch;
    QrBH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdPrintExecute(Sender: TObject);
var
	k: TGUID;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  TGuidField(FieldByName('KHOA')).AsGuid;
	end;

    if mPrinter.BillPrint(k) then
        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdDelExecute(Sender: TObject);
var
    s: String;
    _khoa: TGUID;
begin
    with QrBH do
    begin

        if FieldByName('DELETE_BY').AsInteger <> 0 then
            Exit;

        exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);

        _khoa := TGuidField(FieldByName('KHOA')).AsGuid;
        s := FieldByName('DGIAI').AsString;

        if not DataMain.IsCheckDelete(_khoa, FORM_CODE) then
            Exit;

        Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
        if not FrmReceiptDesc.Execute(s) then
            Exit;

        if not YesNo(RS_CONFIRM_XOAPHIEU, 1) then
            Exit;
    	Edit;
        FieldByName('DGIAI').AsString := s;
        MarkDataSet(QrBH);

//        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
    //Total(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrBH);

    CmdPrint.Enabled := not bEmpty;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHBeforeEdit(DataSet: TDataSet);
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    with QrBH do
    begin
        if FieldByName('CO_HOADON').AsBoolean then
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat
        else
            FieldByName('CALC_TIEN_THUE').AsFloat := 0
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrCTBHCalcFields(DataSet: TDataSet);
begin
    with QrCTBH do
    begin
        if QrBH.FieldByName('CO_HOADON').AsBoolean then
        begin
            FieldByName('CALC_THUE_SUAT').AsFloat := FieldByName('THUE_SUAT').AsFloat;
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTOAN').AsFloat
        end
        else
        begin
            FieldByName('CALC_THUE_SUAT').AsFloat := 0;
            FieldByName('CALC_TIEN_THUE').AsFloat := 0;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTIEN').AsFloat
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHTL_CKChange(Sender: TField);
begin
	//Total(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrBH do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clGray;
            Exit;
        end;

        if FieldByName('DELIVERY').AsBoolean then
        begin
            if  FieldByName('TINHTRANG').AsString = 'T01' then
            begin
                AFont.Color := clRed;
                Exit;
            end;

            if FieldByName('TINHTRANG').AsString = 'T02' then
            begin
                AFont.Color := clPurple;
                Exit;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.ItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh, xCkhd: Double;
	mSotien, mSoluong, mCkmh, mCkhd, mTlckhd, mThue: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mCkhd := 0;
    mThue := 0;
	mTlckhd := QrBH.FieldByName('TL_CK').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
        	xSotien := FieldByName('SOTIEN').AsFloat;		// Thanh tien chua chiet khau
            xThsuat := FieldByName('THUE_SUAT').AsFloat;	// Thue suat
            xCkmh := FieldByName('CHIETKHAU').AsFloat;		// Tien CKMH
            if xCkmh = 0.0 then								// Tinh tien CKHD
	            xCkhd := xSotien * mTlckhd / 100.0
            else
            	xCkhd := 0.0;

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
	        mCkmh := mCkmh + xCkmh;
	        mCkhd := mCkhd + xCkhd;

            if mVATMode = 0 then	// Tinh thue truoc chiet khau
	            mThue := mThue + xSotien * xThsuat / (100.0 + xThsuat)
	    	else					// Tinh thue sau chiet khau
	            mThue := mThue + (xSotien - xCkmh - xCkhd) * xThsuat / (100.0 + xThsuat);

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('SOTIEN').AsFloat  := exVNDRound(mSotien, ctCurRound);
    	FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
    	FieldByName('CHIETKHAU_MH').AsFloat := exVNDRound(mCkmh, ctCurRound);
    	FieldByName('CHIETKHAU').AsFloat := exVNDRound(mCkhd, ctCurRound);
    	FieldByName('THUE').AsFloat := exVNDRound(mThue, ctCurRound);
    	FieldByName('THANHTOAN').AsFloat := exVNDRound(mSotien - mCkmh - mCkhd, ctCurRound);

        if fUpdate then
	        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Banle.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
