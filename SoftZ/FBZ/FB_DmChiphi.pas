(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_DmChiphi;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  wwdblook, AppEvnts, Menus, AdvMenus, wwfltdlg, wwFltDlg2, wwDBGrid2, wwDialog,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbigrd, ToolWin;

type
  TFrmFB_DmChiphi = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsNhom: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    QrNhom: TADOQuery;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PgMain: TPageControl;
    tsBehaviour: TTabSheet;
    tsDep: TTabSheet;
    GrLydo: TwwDBGrid2;
    GrNhom: TwwDBGrid2;
    QrDm: TADOQuery;
    DsDm: TDataSource;
    Bevel1: TBevel;
    CbNhom: TwwDBLookupCombo;
    RefNhom: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Filter: TwwFilterDialog2;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrNhomCREATE_BY: TIntegerField;
    QrNhomUPDATE_BY: TIntegerField;
    QrNhomCREATE_DATE: TDateTimeField;
    QrNhomUPDATE_DATE: TDateTimeField;
    QrDmCREATE_BY: TIntegerField;
    QrDmUPDATE_BY: TIntegerField;
    QrDmCREATE_DATE: TDateTimeField;
    QrDmUPDATE_DATE: TDateTimeField;
    QrDmLK_NHOM: TWideStringField;
    CmdReload: TAction;
    QrDmMACP: TWideStringField;
    QrDmTENCP: TWideStringField;
    QrDmTENCP_TA: TWideStringField;
    QrDmMACP_NHOM: TWideStringField;
    QrDmTINHTRANG: TBooleanField;
    QrDmGHICHU: TWideStringField;
    QrNhomMACP_NHOM: TWideStringField;
    QrNhomTENCP_NHOM: TWideStringField;
    QrNhomGHICHU: TWideStringField;
    QrNhomPLOAI: TWideStringField;
    CbPLOAI: TwwDBLookupCombo;
    QrNhomLK_TEN_LOAINHOM: TWideStringField;
    QrDmLK_PLOAI: TWideStringField;
    QrDmLK_TEN_LOAINHOM: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrNhomBeforePost(DataSet: TDataSet);
    procedure QrNhomBeforeDelete(DataSet: TDataSet);
    procedure QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNhomBeforeInsert(DataSet: TDataSet);
    procedure GrLydoCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure GrNhomCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrDmBeforePost(DataSet: TDataSet);
    procedure CbNhomNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
  private
  	mCanEdit, mTrigger, fixCodeLd, fixCodeNhom: Boolean;
    mQuery: TADOQuery;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmFB_DmChiphi: TFrmFB_DmChiphi;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, isCommon, MainData;

{$R *.DFM}

const
    FORM_CODE = 'FB_DM_CHIPHI';
    FORM_CODE2 = 'FB_DM_CHIPHI_NHOM';

    TABLE_NAME = FORM_CODE;
    TABLE_NAME2 = FORM_CODE2;

    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrDm, TABLE_NAME);
    AddFields(QrNhom, TABLE_NAME2);

    mTrigger := False;
    mQuery := QrDm;

    SetCustomGrid(
    	[FORM_CODE, FORM_CODE2], [GrLydo, GrNhom]);
    SetDictionary([QrDM, QrNhom], [FORM_CODE, FORM_CODE2], [Filter, nil]);

    // Ly do
    fixCodeLd := setCodeLength(FORM_CODE, QrDm.FieldByName('MACP'));

    // Nhom
    fixCodeNhom := SetCodeLength(FORM_CODE2, QrNhom.FieldByName('MACP_NHOM'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.FormShow(Sender: TObject);
begin
	GrLydo.ReadOnly := not mCanEdit;
    GrNhom.ReadOnly := not mCanEdit;
	OpenDataSets([QrNhom, QrDm]);

    SetDisplayFormat(QrDm, sysCurFmt);
    ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrNhom, QrDm]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
    n: Integer;
begin
	b := mQuery.State in [dsBrowse];
    n := PgMain.ActivePageIndex;
    CmdNew.Enabled 	  := b and mCanEdit;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdDel.Enabled	  := b and mCanEdit and (not mQuery.IsEmpty);
    CmdSearch.Enabled := b;
    CmdClearFilter.Enabled := b and (Filter.FieldInfo.Count > 0);

    CmdFilter.Enabled := b and (n = 0);
    CmdFilter.Visible := n = 0;
    CmdClearFilter.Visible := n = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.QrDmBeforePost(DataSet: TDataSet);
begin
    begin
		if BlankConfirm(QrDm, ['MACP']) then
    		Abort;

        if fixCodeLd then
            if LengthConfirm(QrDm, ['MACP']) then
                Abort;

		if BlankConfirm(QrDm, ['TENCP', 'MACP_NHOM']) then
    		Abort;

		SetNull(QrDm, ['MACP_NHOM']);
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.QrNhomBeforePost(DataSet: TDataSet);
begin
    begin
	    if BlankConfirm(QrNhom, ['MACP_NHOM']) then
    	    Abort;

	    if fixCodeNhom then
    	    if LengthConfirm(QrNhom, ['MACP_NHOM']) then
        	    Abort;

	    if BlankConfirm(QrNhom, ['TENCP_NHOM']) then
    	    Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.QrNhomBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
        exSearch(Name + '_0', DsDm)
    else
        exSearch(Name + '_1', DsNhom)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.QrNhomBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.GrLydoCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MACP') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.GrNhomCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if (Field.FullName = 'MACP_NHOM') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 0 then
    	Status.SimpleText := exRecordCount(mQuery, Filter)
    else
    	Status.SimpleText := RecordCount(mQuery);    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, REPORT_NAME, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := mQuery.State in [dsBrowse];
    if not AllowChange then
        exCompleteConfirm;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.PgMainChange(Sender: TObject);
begin
	case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrDm;
			GrLydo.SetFocus;
            RefNhom.Requery;
	    end;
    1:
	    begin
    	    mQuery := QrNhom;
        	GrNhom.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CbNhomNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmChiphi.CmdReloadExecute(Sender: TObject);
begin
    QrDm.Requery;
    QrNhom.Requery;
    RefNhom.Requery;
end;

end.
