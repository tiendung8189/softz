﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_DondhNCC;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,  Variants,
  StdCtrls, Buttons, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi, isDb, Messages,
  isPanel, wwDialog, Grids, Wwdbgrid, ToolWin, AdvEdit, DBAdvEd, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_DondhNCC = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA_REF: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTL_CK_HD: TFloatField;
    QrNXCHIETKHAU_HD: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    PaMaster: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    Label5: TLabel;
    EdNG_DATHANG: TwwDBEdit;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    QrNXNGAY_GIAO: TDateTimeField;
    QrNXNG_DATHANG: TWideStringField;
    QrNXSOLUONG: TFloatField;
    QrNXTINHTRANG: TWideStringField;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrCTGHICHU: TWideStringField;
    QrCTLOAITHUE: TWideStringField;
    CmdFilterRef: TAction;
    QrCTTIEN_THUE: TFloatField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    CmdAudit: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    TntMenuItem1: TMenuItem;
    Xachitit1: TMenuItem;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    spLAY_1DONDH: TADOStoredProc;
    CmdFromNX: TAction;
    Lytphiunhp1: TMenuItem;
    CHUNGTU_LAYPHIEU: TADOStoredProc;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    CmdListRefesh: TAction;
    DONDH_LUONG_TON: TADOStoredProc;
    CmdFromMin: TAction;
    Lytslungtn1: TMenuItem;
    CHECK_NCC: TADOCommand;
    CmdSapthutu: TAction;
    N3: TMenuItem;
    Splithtmthng1: TMenuItem;
    CmdDmvt: TAction;
    ToolButton4: TToolButton;
    SepExport: TToolButton;
    QrNXNGAY_DAT: TDateTimeField;
    QrNXNGAY_DUKIEN: TDateTimeField;
    Label7: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label8: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    QrNXHINHTHUC_GIA: TWideStringField;
    QrNXSOTIEN1: TFloatField;
    QrCTDONGIA_REF2: TFloatField;
    CmdThamkhaoGia: TAction;
    N4: TMenuItem;
    hamkhogi1: TMenuItem;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLK_TINHTRANG: TWideStringField;
    QrNXLOC: TWideStringField;
    QrCTTINHTRANG: TWideStringField;
    BtnIn2: TToolButton;
    QrCTSOTIEN1: TFloatField;
    QrCTLK_TENTHUE: TWideStringField;
    QrCTLK_VAT_RA: TFloatField;
    QrCTLK_VAT_VAO: TFloatField;
    CbLOAITHUE: TwwDBLookupCombo;
    QrNXCALC_SOTIEN_SAUCK_MH: TFloatField;
    QrCTSOLUONG_NX: TFloatField;
    CmdDmncc: TAction;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    QrCTTIEN_THUE_5: TFloatField;
    QrCTTIEN_THUE_10: TFloatField;
    QrCTTIEN_THUE_OR: TFloatField;
    QrNXTHUE_5: TFloatField;
    QrNXTHUE_10: TFloatField;
    QrNXTHUE_OR: TFloatField;
    CmdPrintExcel: TAction;
    CmdPrintBaogia: TAction;
    ToolButton6: TToolButton;
    ToolButton10: TToolButton;
    SepBaogia: TToolButton;
    ToolButton13: TToolButton;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton5: TToolButton;
    QrCTGIAVON: TFloatField;
    QrCTTL_CK_HD: TFloatField;
    QrCTCHIETKHAU_HD: TFloatField;
    QrCTSOTIEN2: TFloatField;
    QrCTTHANHTIEN: TFloatField;
    QrNXSOTIEN2: TFloatField;
    QrNXTHANHTIEN: TFloatField;
    QrCTLOC: TWideStringField;
    QrCTB1: TBooleanField;
    SepChecked: TToolButton;
    PaTotal: TPanel;
    ImgTotal: TImage;
    QrCTSOLUONG2_LE: TFloatField;
    PaSotien1: TPanel;
    Label9: TLabel;
    wwDBEdit8: TwwDBEdit;
    PaThanhtoan: TPanel;
    Label24: TLabel;
    EdTriGiaTT: TwwDBEdit;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrCTTL_LAI: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    N5: TMenuItem;
    ItemObsolete: TMenuItem;
    Panel1: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    wwDBEdit1: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    PaSotien2: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    EdTLCK: TwwDBEdit;
    wwDBEdit10: TwwDBEdit;
    PaThue: TPanel;
    BtThue: TSpeedButton;
    Label19: TLabel;
    Label13: TLabel;
    EdTienVAT: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    CmdExportDataGrid: TAction;
    XutdliutliraExcel1: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    CmdCheckton: TAction;
    QrCTQD1: TIntegerField;
    QrNXSOTIEN_SI: TFloatField;
    QrCTSOTIEN_SI: TFloatField;
    cmdDmtd: TAction;
    hcn1: TMenuItem;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTDVT_BOX: TWideStringField;
    spLAY_DIACHI_MADT: TADOStoredProc;
    QrNXDCHI_NHAN: TWideMemoField;
    Label3: TLabel;
    DBMemo2: TDBMemo;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    CbTinhTrangDDH: TDbLookupComboboxEh2;
    CbGia: TDbLookupComboboxEh2;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMADT: TDBEditEh;
    CmdXemCongNo: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrNXMADTChange(Sender: TField);
    procedure CmdFilterRefExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdFromOrderExecute(Sender: TObject);
    procedure CmdFromNXExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdFromMinExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAValidate(Sender: TField);
    procedure QrNXTINHTRANGValidate(Sender: TField);
    procedure CmdThamkhaoGiaExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTLOAITHUEChange(Sender: TField);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure BtThueClick(Sender: TObject);
    procedure CmdPrintExcelExecute(Sender: TObject);
    procedure CmdPrintBaogiaExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXTL_CK_HDChange(Sender: TField);
    procedure BtCongnoClick(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure cmdDmtdExecute(Sender: TObject);
    procedure CmdXemCongNoExecute(Sender: TObject);

  private
	b1Ncc, mCanEdit, bCheck, bDuplicate, mObsolete: Boolean;
    mLCT, mMakho: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

    //Mac dinh cho hinh thuc gia
    fHTGia: String;

    // Ref filter
    refSQL: String;
    refType: Integer;

    function GetDetailFrom(pKhoa: TGUID; pStoredPro: TADOStoredProc; b: Boolean = False): Boolean;
    procedure escapeKey(pSleepTime: Variant);

  public
	procedure Execute(r: WORD);
  end;

var
  FrmFB_DondhNCC: TFrmFB_DondhNCC;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, FB_ChonDsma, isLib, ChonPhieuNX,
    ChonDondh3, FB_Sapthutu, Dmvt, FB_TheodoiGia, GuidEx, exThread, isCommon, DmKhNcc,
  Dmncc, isFile, ImportExcel, Tienthue, OfficeData, CongnoNCC, InlabelChungtu,
  CheckTonkho, ChonDsma, FB_Dmhh, FB_DmvtNVL, FB_ChonPhieunhap, FB_ChonDondh2, ExcelData;

{$R *.DFM}

const
	FORM_CODE = 'FB_PHIEU_DONDH_NCC';
    
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.Execute;
begin
	// Ky hieu
	mLCT := 'FDHN';

    // Audit setting
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    bCheck := False;

    // Done
    ShowModal;
end;

	(*
    ** Form events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    // Params
    b1Ncc := FlexConfigBool(FORM_CODE, '1 nha cung cap');
    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    bDuplicate := FlexConfigBool(FORM_CODE, 'Duplicate Mavt');

    CmdPrintExcel.Visible := FlexConfigBool(FORM_CODE, 'Export Excel');
    SepExport.Visible := CmdPrintExcel.Visible;
    CmdPrintBaogia.Visible := FlexConfigBool(FORM_CODE, 'Bao gia');
    SepBaogia.Visible := CmdPrintBaogia.Visible;

    //Lay gia tri mac dinh cho HTGia
    fHTGia := sysHTGia;

    if fHTGia <> '' then
    begin
        cbGia.Visible := False;
    end;

    // Initial
    mMakho := RegReadString(Name, 'Makho', sysDefKho);

  	mTrigger := False;
    mTriggerCK := False;
    mObsolete := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;

    refType := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.FormShow(Sender: TObject);
begin
	Wait(PREPARING);
    InitFmtAndRoud(mLCT);
    ctCurVatRound := -2;
    ctCurVatFmt := '#,##0.00;-#,##0.00;#';

	// Open database
	with DataMain do
		OpenDataSets([QrTT_DDH, QrDMLOAITHUE, QrDMNCC, QrDMKHO, QrHINHTHUC_GIA, QrFB_DM_NPL_THUCDON]);

	SetDisplayFormat(DataMain.QrFB_DM_NPL_THUCDON, sysCurFmt);

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
        SetDisplayFormat(QrNX, ['TL_CK_HD'], sysPerFmt);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

	with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
        SetDisplayFormat(QrCT, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['TL_CK', 'TL_CK_HD'], sysPerFmt);
        SetDisplayFormat(QrCT, ['DONGIA', 'DONGIA2', 'DONGIA_REF', 'DONGIA_REF2',
                                     'DONGIA_LE'], ctPriceFmt);

        SetDisplayFormat(QrCT, ['THUE_SUAT'], sysTaxFmt);
        SetDisplayFormat(QrCT, ['TIEN_THUE'], ctCurVatFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsThue then
    begin
        PaThue.Visible := sysIsThue;
        grRemoveFields(GrBrowse, ['THUE']);
        grRemoveFields(GrDetail, ['LK_TENTHUE', 'THUE_SUAT', 'TIEN_THUE', 'THANHTIEN']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;
    // Read master list
    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, 'Makho', mMakho);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
    ** Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    ** Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_DONDH_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_DONDH.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_DONDH_CT a, FB_DM_HH b where a.KHOA = FB_DONDH.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_DONDH_CT where KHOA = FB_DONDH.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    
    with DataMain do
    begin
		QrTT_DDH.Requery;
        QrDMNCC.Requery;
        QrDMKHO.Requery;
	end;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

procedure TFrmFB_DondhNCC.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmCheckTonkho, FrmCheckTonkho);
    with QrCT do
	    if FrmCheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
    if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdDmnccExecute(Sender: TObject);
var
    r: WORD;
    mRet: Boolean;
    loai: Integer;
begin
    if GetFuncState('SZ_DM_KH_NCC') then
    begin
        r := GetRights('SZ_DM_KH_NCC');
        if r = R_DENY then
            Exit;

        loai := -1;
    end else
    begin
        r := GetRights('SZ_DM_NCC');
        if r = R_DENY then
            Exit;

        loai := 1;
    end;

    Application.CreateForm(TFrmDmKhNcc, FrmDmKhNcc);
    FrmDmKhNcc.Execute(r, loai, False);

    if mRet then
        DataMain.QrDmncc.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.cmdDmtdExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    	DataMain.QrFB_DM_NPL_THUCDON.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    	DataMain.QrFB_DM_NPL_THUCDON.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdSwitchExecute(Sender: TObject);
begin
	if (ActiveControl = GrDetail) or (ActiveControl = PaTotal) or (ActiveControl.Parent = PaTotal) then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdThamkhaoGiaExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_TheodoiGia, FrmFB_TheodoiGia);
    with QrNX do
	    FrmFB_TheodoiGia.Execute('FNMUA',
            QrCT.FieldByName('MAVT').AsString,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdTotalExecute(Sender: TObject);
begin
    vlTotal1.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdXemCongNoExecute(Sender: TObject);
begin
    if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdPrintBaogiaExecute(Sender: TObject);
var
    s: String;
begin
    CmdSave.Execute;
    s := 'FB_RP_PHIEU_DONDH_NCC_BAOGIA';
    DataOffice.CreateReport2('XLSX\' + s + '.xlsx',
        [sysLogonUID, TGuidEx.ToString(QrNX.FieldByName('KHOA'))], s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdPrintExcelExecute(Sender: TObject);
var
    s: String;
begin
    CmdSave.Execute;
    s := 'FB_RP_PHIEU_DONDH_NCC_EXCEL';
    DataOffice.CreateReport2('XLSX\' + s  + '.xlsx',
        [sysLogonUID, TGuidEx.ToString(QrNX.FieldByName('KHOA'))], s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and isnull(BO, 0) = 0 ') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
        bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
//                        and (QrNX.FieldByName('TINHTRANG').AsString = '02');
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdPrint.Enabled := (not bEmpty) and (not bDeleted);
    CmdPrintExcel.Enabled := (not bEmpty) and (not bDeleted);
    CmdPrintBaogia.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdDmvt.Enabled := n = 1;
    cmdDmtd.Enabled := n = 1;

    CmdFromMin.Enabled := (not bEmpty) and (QrNX.FieldByName('MADT').AsString <> '');

    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;

    if b1Ncc then
	    CbbNhaCungCap.ReadOnly := not bEmptyCT;
    CmdEmptyDetail.Enabled := (not bEmptyCT) and mCanEdit;
    CbGia.ReadOnly := not bEmptyCT;
    LydliutfileExcel1.Enabled := bEmptyCT;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime    := d;
		FieldByName('MAKHO').AsString     := mMakho;
		FieldByName('TINHTRANG').AsString := '01';
        FieldByName('LCT').AsString       := mLCT;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('HINHTHUC_GIA').AsString    := Iif(fHTGia = '', '03', fHTGia);
        FieldByName('NGAY_DAT').AsDateTime := Date;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;
    if BlankConfirm(QrNX, ['NGAY', 'MAKHO', 'MADT']) then
        Abort;

    DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
		mMakho := FieldByName('MAKHO').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXBeforeEdit(DataSet: TDataSet);
var
    s: String;
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

    with QrNX do
    begin
        s := FieldByName('TINHTRANG').AsString;
        if s = '02' then
        begin
            if GetFuncState('SZ_DDH_NCC_TINHTRANG') then
                if GetRights('SZ_DDH_NCC_TINHTRANG') = R_DENY then
                    Abort
        end
        else if (s = '03') or (s = '04') then
        begin
            Msg('Phiếu đã được giao hàng');
            Abort;
        end;
    end;
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    mThanhtien, mThanhtoan: Double;
    mHinhthuc: String;
begin
    with Sender.DataSet do
    begin
        mHinhthuc := FieldByName('HINHTHUC_GIA').AsString;
        mThanhtien := FieldByName('THANHTIEN').AsFloat;

//        if mHinhthuc = '03' then     // Nguoc
//            mThanhtoan := mThanhtien - FieldByName('CL_THUE').AsFloat
//        else // Xuoi va truc tiep
            mThanhtoan := mThanhtien + FieldByName('CL_THUE').AsFloat;

        FieldByName('THANHTOAN').AsFloat := exVNDRound(mThanhtoan, ctCurRound);
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXHINHTHUC_GIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if QrCT.IsEmpty then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXMADTChange(Sender: TField);
begin
	//exDotMancc(Sender);;

    with spLAY_DIACHI_MADT do
    begin
    	Prepared := True;
        Parameters[1].Value := Sender.AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;
        QrNX.FieldByName('DCHI_NHAN').AsString := FieldByName('DiaChiDatHang').AsString;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXTINHTRANGValidate(Sender: TField);
begin
    if (Sender.AsString = '03') or (Sender.AsString = '04') then
    begin
        ErrMsg('Không được đổi sang tình trạng này.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrNXTL_CK_HDChange(Sender: TField);
var
    bTrigger: Boolean;
    tlck, ck, sotien1: Double;
begin
    if mTrigger then
    	Exit;

	with QrNX do
    begin
        bTrigger := mTrigger;
        mTrigger := True;

        sotien1 := FieldByName('SOTIEN1').AsFloat;
        if Sender.FieldName = 'CHIETKHAU_HD' then
        begin
            ck := FieldByName('CHIETKHAU_HD').AsFloat;
            if ck = 0 then
                tlck := 0
            else
                tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, Sotien1)));

            FieldByName('TL_CK_HD').AsFloat := exVNDRound(tlck, sysPerRound);
        end else
        begin
            tlck := FieldByName('TL_CK_HD').AsFloat;
            ck := exVNDRound(sotien1 * tlck / 100.0, ctCurRound);

            FieldByName('CHIETKHAU_HD').AsFloat := ck;
        end;

        mTrigger := bTrigger;
    end;

    if Sender.FieldName <> 'SOTIEN1' then
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('TL_CK_HD').AsFloat := exVNDRound(tlck, sysPerRound);

            Next;
        end;

        CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

    	if (FieldByName('TINHTRANG').AsString <> '') and (FieldByName('TINHTRANG').AsString <> '01') then
        begin
        	ErrMsg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat := exVNDRound(
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
          , ctPriceRound);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                sl  := FieldByName('SOLUONG').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SOLUONG2').AsFloat := exVNDRound(sl2, ctQtyRound);
                FieldByName('SOLUONG2_LE').AsFloat := exVNDRound(sl2le, ctQtyRound);
            end
            else if (Sender.FullName = 'SOLUONG2') or (Sender.FullName = 'SOLUONG2_LE') then
            begin
                sl2 := FieldByName('SOLUONG2').AsFloat;
                sl2le := FieldByName('SOLUONG2_LE').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := exVNDRound(sl, ctQtyRound);
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat, ctCurRound);

        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat, ctCurRound);
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTSOTIENChange(Sender: TField);
var
    mHinhthuc: String;
    mSotien, tlck, mCk, mCkhd, mThue, mTs, mSl, mThanhtien,
        mSotien1, //Sau chiết khấu mặt hàng
        mSotien2: Double; //Sau chiết khấu hóa đơn
begin
    if mTriggerCK then
        Exit;

	with QrCT do
    begin
        mHinhthuc := QrNX.FieldByName('HINHTHUC_GIA').AsString;
        mSotien := FieldByName('SOTIEN').AsFloat;
        mTs := FieldByName('THUE_SUAT').AsFloat;

        if Sender.FieldName = 'CHIETKHAU' then
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            tlck := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end
        else
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            mCk := exVNDRound(mSotien * tlck / 100.0);
        end;
        mSotien1 := mSotien -  mCk;

        mCkhd := exVNDRound( mSotien1 * FieldByName('TL_CK_HD').AsFloat / 100.0, ctCurRound);
        mSotien2 := mSotien1 -  mCkhd;

        if mHinhthuc = '02' then          // Xuoi
        begin
        	// Thue
            mThue := exVNDRound(mSotien2 * mTs / 100.0, ctCurRound);
            mThanhtien := mSotien2 + mThue;
        end
        else if mHinhthuc = '03' then     // Nguoc
        begin
            // Thue
            if mTs = 0 then
                mThue := 0
            else
                mThue := exVNDRound(mSotien2 / (100/mTs + 1), ctCurRound);
            mThanhtien := mSotien2;
        end;

    	mTriggerCK := True;
        FieldByName('TL_CK').AsFloat := exVNDRound(tlck, sysPerRound);
    	FieldByName('CHIETKHAU').AsFloat := exVNDRound(mCk, ctCurRound);
        FieldByName('CHIETKHAU_HD').AsFloat := exVNDRound(mCkhd, ctCurRound);

        mThue := exVNDRound(mThue, ctCurVatRound);

        mTriggerCK := False;
        if mTs = 5 then
        begin
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_5').AsFloat := mThue;
        end
        else if mTs = 10 then
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := mThue;
        end else
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := mThue;
        end;

        FieldByName('TIEN_THUE').AsFloat := mThue;
        FieldByName('SOTIEN1').AsFloat := exVNDRound(mSotien1, ctCurRound);
        FieldByName('SOTIEN2').AsFloat := exVNDRound(mSotien2, ctCurRound);
        FieldByName('THANHTIEN').AsFloat := exVNDRound(mThanhtien, ctCurRound);
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTMAVTChange(Sender: TField);
var
	ck, gia, giaSI, giaLE : Double;
    lthue: String;
begin
    exDotFBMavt(4, DataMain.QrFB_DM_NPL_THUCDON, Sender);

    with QrCT do
    begin
        ck := 0;  gia := 0; giaLE := 0; lthue := 'T00';
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;
            Parameters[3].Value := QrNX.FieldByName('MADT').AsString;
            Parameters[4].Value := QrNX.FieldByName('HINHTHUC_GIA').AsString;

            Open;
            if RecordCount <> 0 then
            begin
                ck := FieldByName('TL_CK').AsFloat;
                gia := FieldByName('DONGIA').AsFloat;
                giaLE := FieldByName('DONGIA_LE').AsFloat;
                lthue := FieldByName('LOAITHUE').AsString;

                QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
                QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
                QrCT.FieldByName('DVT_BOX').AsString := FieldByName('TenDvtLon').AsString;
                QrCT.FieldByName('QD1').AsInteger := FieldByName('QuyDoi').AsInteger;

            end;
        end;
        //FieldByName('QD1').AsInteger := FieldByName('LK_QD1').AsInteger;

        FieldByName('LOAITHUE').AsString := lthue;
        FieldByName('DONGIA_LE').AsFloat := exVNDRound(giaLE, ctPriceRound);
        FieldByName('DONGIA_REF').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('TL_CK').AsFloat := exVNDRound(ck, sysPerRound);

        FieldByName('TL_CK_HD').AsFloat := exVNDRound(QrNX.FieldByName('TL_CK_HD').AsFloat, sysPerRound);

        if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;

    end;
	GrDetail.InvalidateCurrentRow;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
    s: String;
begin
    if Highlight then
    begin
        AFont.Color := clWhite;
        Exit;
    end;

    s := QrNX.FieldByName('TINHTRANG').AsString;
    if s = '01' then
        AFont.Color := clBlue
    else if s = '02' then
        AFont.Color := clRed
    else if s = '03' then
        AFont.Color := clPurple
    else
        AFont.Color := clBlack;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('CHIETKHAU').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('CHIETKHAU_MH').AsFloat);
        ColumnByName('CHIETKHAU_HD').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('CHIETKHAU_HD').AsFloat);
		ColumnByName('TIEN_THUE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('THUE').AsFloat);
        ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN1').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN1').AsFloat);
        ColumnByName('SOTIEN2').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN2').AsFloat);
        ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('THANHTIEN').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.BtCongnoClick(Sender: TObject);
begin
    if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.BtThueClick(Sender: TObject);
var
    p: TPoint;
begin
    p.x := GrDetail.Left + GrDetail.Width;
    p.y := PaTotal.Top;
    p := ClientToScreen(p);

    Application.CreateForm(TFrmTienthue, FrmTienthue);
    with FrmTienthue do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y - Height + 2;
    	Execute(DsNX);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdFilterRefExecute(Sender: TObject);
var
	n: Integer;
    s: String;
begin
	n := refType;
    s := '';
	if not FrmFB_ChonDsma.Get(n, s) then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Đơn đặt hàng có mặt hàng sai mã. Tiếp tục?';
procedure TFrmFB_DondhNCC.CmdFromOrderExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue: String;
    mSoluong, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmFB_ChonDondh2, FrmFB_ChonDondh2);
    with QrNX do
 	    n := FrmFB_ChonDondh2.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 'DM_NCC','FDHN');
	FrmFB_ChonDondh2.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with spLAY_1DONDH do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        // Cap nhat nha cung cap
        mNcc := FieldByName('MADT').AsString;
        mKho := FieldByName('MAKHO').AsString;
        mSCT2 := FieldByName('SCT').AsString;
        mHTGia := FieldByName('HINHTHUC_GIA').AsString;
        mTlckHD := FieldByName('TL_CK_HD').AsFloat;

        if (QrNX.FieldByName('MADT').AsString <> mNcc) or
           (QrNX.FieldByName('MAKHO').AsString <> mKho) or
           (QrNX.FieldByName('SCT').AsString <> mSCT2) then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('MADT').AsString := mNcc;
	        QrNX.FieldByName('MAKHO').AsString := mKho;
            QrNX.FieldByName('HINHTHUC_GIA').AsString := mHTGia;
            QrNX.FieldByName('TL_CK_HD').AsFloat := mTlckHD;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mTlck := FieldByName('TL_CK').AsFloat;
            mLThue := FieldByName('LOAITHUE').AsString;

            with QrCT do
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := exVNDRound(mDongia, ctPriceRound);
                FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
                FieldByName('TL_CK').AsFloat := exVNDRound(mTlck, sysPerRound);
                FieldByName('LOAITHUE').AsString := mLThue;
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFile: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    if BlankConfirm(QrNX, ['MADT', 'HINHTHUC_GIA']) then
        Abort;

    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if sFile = '' then
    	Exit;

    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;
                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;

                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if bDuplicate or (not QrCT.Locate('MAVT', FieldByName(sFld).AsString, [])) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                        end
                        else
                            SetEditState(QrCT);
                        QrCT.CheckBrowseMode;
                    end;
                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED_PN = 'Phiếu đơn đặt hàng có mặt hàng sai mã. Tiếp tục?';
procedure TFrmFB_DondhNCC.CmdFromNXExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue: String;
    mSoluong, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;

	// Chon Don dat hang

	Application.CreateForm(TFrmFB_ChonPhieunhap, FrmFB_ChonPhieunhap);
    with QrNX do

	    n := FrmFB_ChonPhieunhap.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString);


    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with CHUNGTU_LAYPHIEU do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(n);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED_PN, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;


    try
       	Wait('Đang xử lý...');
        // Cap nhat nha cung cap
        mNcc := FieldByName('MADT').AsString;
        mKho := FieldByName('MAKHO').AsString;
        mSCT2 := FieldByName('SCT').AsString;
        mHTGia := FieldByName('HINHTHUC_GIA').AsString;
        mTlckHD := FieldByName('TL_CK_HD').AsFloat;

        if (QrNX.FieldByName('MADT').AsString <> mNcc) or
           (QrNX.FieldByName('MAKHO').AsString <> mKho) then
        begin
        	SetEditState(QrNX);
	        QrNX.FieldByName('MADT').AsString := mNcc;
	        QrNX.FieldByName('MAKHO').AsString := mKho;
            QrNX.FieldByName('HINHTHUC_GIA').AsString := mHTGia;
            QrNX.FieldByName('TL_CK_HD').AsFloat := mTlckHD;
//            QrNX.FieldByName('SCT2').AsString := mSCT2;
//            QrNX.FieldByName('KHOA2').AsInteger := n;
        end;

        // Chi tiet
        while not Eof do
        begin
			mMavt := FieldByName('MAVT').AsString;
			mSoluong := FieldByName('SOLUONG').AsFloat;
			mDongia := FieldByName('DONGIA').AsFloat;
            mTlck := FieldByName('TL_CK').AsFloat;
            mLThue := FieldByName('LOAITHUE').AsString;

            with QrCT do
            if bDuplicate or (not Locate('MAVT', mMavt, [])) then
            begin
            	Append;
                FieldByName('MAVT').AsString := mMavt;
                FieldByName('DONGIA').AsFloat := exVNDRound(mDongia, ctPriceRound);
                FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
                FieldByName('TL_CK').AsFloat := exVNDRound(mTlck, sysPerRound);
                FieldByName('LOAITHUE').AsString := mLThue;
            end;

        	Next;
        end;

        Active := False;
		QrCT.CheckBrowseMode;
    finally
		ClearWait;
	end;

    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)  
function TFrmFB_DondhNCC.GetDetailFrom(pKhoa: TGUID;
  pStoredPro: TADOStoredProc; b: Boolean = False): Boolean;
var
	mDT, mKho, mMavt, mLThue: String;
    mSoluong, mDongia, mCK, mCkhd: Double;
begin
	Result := False;

     // Lay chi tiet
    with pStoredPro do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            // Cap nhat nha cung cap
            mDT := FieldByName('MADT').AsString;
            mKho := FieldByName('MAKHO').AsString;
            mCkhd := FieldByName('TL_CK_HD').AsFloat;

            if (QrNX.FieldByName('MADT').AsString <> mDT) or
               (QrNX.FieldByName('MAKHO').AsString <> mKho) or
               (QrNX.FieldByName('TL_CK_HD').AsFloat <> mCkhd)then
            begin
                SetEditState(QrNX);
                QrNX.FieldByName('MADT').AsString := mDT;
                QrNX.FieldByName('MAKHO').AsString := mKho;
                QrNX.FieldByName('TL_CK_HD').AsFloat := mCkhd;
            end;

            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MAVT').AsString;
                if bCheck then
                    mSoluong := FieldByName('SOLUONG_CONLAI').AsFloat
                else
                    mSoluong := FieldByName('SOLUONG').AsFloat;
                mDongia := FieldByName('DONGIA').AsFloat;
                mLThue := FieldByName('LOAITHUE').AsString;
                mCK := FieldByName('TL_CK').AsFloat;

                with QrCT do
                begin
                    Append;
                    FieldByName('MAVT').AsString := mMavt;
                    FieldByName('DONGIA').AsFloat := exVNDRound(mDongia, ctPriceRound);
                    FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
                    FieldByName('TL_CK').AsFloat := exVNDRound(mCK, sysPerRound);
                    FieldByName('LOAITHUE').AsString := mLThue;
                end;

                Next;
            end;

            Active := False;
            QrCT.CheckBrowseMode;
        finally
            ClearWait;
        end;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdFromMinExecute(Sender: TObject);
var
	mMavt: String;
begin
	with DONDH_LUONG_TON do
    begin
    	Prepared := True;
        Parameters[1].Value := QrNX.FieldByName('NGAY').AsDateTime;
        Parameters[2].Value := QrNX.FieldByName('MADT').AsString;
        Parameters[3].Value := QrNX.FieldByName('MAKHO').AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MABH').AsString;

                with QrCT do
                begin
                    Append;
                    FieldByName('MAVT').AsString := mMavt;
                end;

                Next;
            end;

            Close;
            QrCT.CheckBrowseMode;
        finally
            ClearWait;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdExBarcodeExecute(Sender: TObject);
begin
    CmdSave.Execute;
    with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTLOAITHUEChange(Sender: TField);
begin
    with QrCT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
            FieldByName('THUE_SUAT').Clear
        else
            FieldByName('THUE_SUAT').AsFloat := exVNDRound(FieldByName('LK_VAT_VAO').AsFloat, sysTaxRound)
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DondhNCC.QrCTMAVTValidate(Sender: TField);
var
	s: String;
    bm: TBytes;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
	if s = '' then
    	Exit;

    if not bDuplicate then
    //Trung mat hang
    with QrCT do
    begin
        if IsDuplicateCode2(QrCT, Sender, bm) then
        begin
            //if YesNo(RS_DUPLICATE_CODE) then
            begin
                try
                    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
                    Abort;
                finally
                    Cancel;
                    GotoBookmark(bm);
                    Edit;
                    mgMyThread := TExThread.Create(escapeKey, 50);
                end;
            end;
        end;
    end;

	// Nhieu nha cung cap
	if not b1Ncc then
    	Exit;

	// Validate MADT
    if IsDotSelect(s) = 0 then
        with CHECK_NCC do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrNX.FieldByName('MADT').AsString;
            if Execute.RecordCount < 1 then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL2);
                Abort;
            end;
        end;
end;

end.
