﻿if Key = VK_DELETE then
    with (Sender as TfcTreeCombo) do
        if IsDroppedDown then
        begin
            Clear;
            CloseUp(False);
        end;
