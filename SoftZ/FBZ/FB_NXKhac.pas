﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_NXKhac;

interface

uses
  SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, frameNgay, frameNavi,
  isDb, isPanel, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, Wwdotdot, Wwdbcomb,
  DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_NXKhac = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepBarcode: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_SO: TWideStringField;
    QrNHOADON_NGAY: TDateTimeField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXNG_NHAN: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    CmdBalance: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    QrCTDONGIA_REF: TFloatField;
    Label5: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    Label8: TLabel;
    DBEdit2: TwwDBEdit;
    CmdAudit: TAction;
    QrCTB1: TBooleanField;
    CmdListRefesh: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdSapthutu: TAction;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrCTDONGIA_REF2: TFloatField;
    QrCTQD1: TIntegerField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrNXLK_LYDO_NHAP: TWideStringField;
    QrNXLK_LYDO_XUAT: TWideStringField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrCTTINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTSOTIEN1: TFloatField;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    N4: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrCTEX_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    QrCTSOLUONG2_LE: TFloatField;
    QrNXSOTIEN_SI: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    QrCTSOTIEN_SI: TFloatField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrCTTL_LAI: TFloatField;
    QrCTTL_LAI_SI: TFloatField;
    PopupMenu2: TAdvPopupMenu;
    Phiuvnchuynnib1: TMenuItem;
    MenuItem2: TMenuItem;
    Phiucginhp1: TMenuItem;
    N5: TMenuItem;
    ItemObsolete: TMenuItem;
    QrNXLOCKED_BY: TIntegerField;
    QrNXLK_LOCKED_NAME: TWideStringField;
    QrNXLOCKED_DATE: TDateTimeField;
    CmdCheckton: TAction;
    N6: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    N7: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    N8: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    N9: TMenuItem;
    heogibn1: TMenuItem;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTDVT_BOX: TWideStringField;
    QrCTGIAVON: TFloatField;
    cbbLyDo: TDbLookupComboboxEh2;
    EdMAKHO: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    DBMemoEh1: TDBMemoEh;
    QrCTLK_TINHTRANG: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrNXAfterEdit(DataSet: TDataSet);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
  private
  	mLCT: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(kh: String; r: WORD);
  end;

var
  FrmFB_NXKhac: TFrmFB_NXKhac;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, FB_ChonDsma, isLib, FB_Sapthutu, GuidEx,
    exThread, isCommon, isFile, ImportExcel, InlabelChungtu, FB_CheckTonkho, OfficeData, ExcelData;

{$R *.DFM}

const
	FORM_CODE_N = 'FB_PHIEU_NHAPKHAC';
    FORM_CODE_X = 'FB_PHIEU_XUATKHAC';

    LCT_N = 'FNKHAC';
    LCT_X = 'FXKHAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	CAPTION_NHAP = 'Nhập Khác - Nhà Hàng';
    CAPTION_XUAT = 'Xuất Khác - Nhà Hàng';

procedure TFrmFB_NXKhac.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    if kh = 'N' then
    begin
    	Caption := CAPTION_NHAP;
       	mLCT := LCT_N;
        cbbLyDo.DropDownBox.ListSource := DataMain.DsFB_LYDO_NK;
        cbbLyDo.ListSource := DataMain.DsFB_LYDO_NK;
	end
    else
    begin
    	Caption := CAPTION_XUAT;
       	mLCT := LCT_X;
        cbbLyDo.DropDownBox.ListSource := DataMain.DsFB_LYDO_XK;
        cbbLyDo.ListSource := DataMain.DsFB_LYDO_XK;
	end;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.FormCreate(Sender: TObject);
begin
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
    CmdImportExcel.Visible := FlexConfigBool(Iif(mLCT=LCT_N,FORM_CODE_N, FORM_CODE_X), 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;
    
    mObsolete := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;

	Wait(PREPARING);
    InitFmtAndRoud(mLCT);

    with  DataMain do
    begin
    	OpenDataSets([QrDMKHO, QrFB_DM_HH]);
        SetDisplayFormat(QrFB_DM_HH, sysCurFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

	with QrCT do
    begin
		SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA', 'DONGIA_LE'], ctPriceFmt);
    end;

	if mLCT = LCT_N then
    begin
        TMyForm(Self).LoadIcon('_IDI_RECEIPT');
	    DataMain.QrFB_LYDO_NK.Open;

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_N, FORM_CODE_N + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_N , FORM_CODE_N + '_CT'], [Filter, Nil]);
    end
    else
    begin
        TMyForm(Self).LoadIcon('_IDI_ISSUE');
	    DataMain.QrFB_LYDO_XK.Open;

        GrDetail.RemoveField('B1');

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_X, FORM_CODE_X + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_X, FORM_CODE_X + '_CT'], [Filter, Nil]);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsBarcode then
    begin
        grRemoveFields(GrDetail, ['B1']);
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_CHUNGTU_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_CHUNGTU_CT a, FB_DM_HH b where a.KHOA = FB_CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_CHUNGTU_CT where KHOA = FB_CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
        QrDMKHO.Requery;
        if mLCT = LCT_N then
        	QrFB_LYDO_NK.Requery
        else
        	QrFB_LYDO_XK.Requery;
    end;
        
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);

//    SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);

//    if QrNX.FieldByName('LOCKED_BY').AsInteger = sysLogonUID then
//        SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdCheckedExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;
//
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
    with QrCT do
	    if FrmFB_CheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdDelExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdPrintExecute(Sender: TObject);
var
	sRep: String;
	n: Integer;
begin
	if mLCT = LCT_N then
    	sRep := FORM_CODE_N
    else
    	sRep := FORM_CODE_X;

	CmdSave.Execute;
    n := (Sender as TComponent).Tag;

	ShowReport(Caption, sRep + '_' + IntToStr(n), [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFile, FORM_CODE: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if sFile = '' then
    	Exit;

    if mLCT = LCT_N then
       FORM_CODE := FORM_CODE_N
    else
       FORM_CODE := FORM_CODE_X;

    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;
                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;


                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if not QrCT.Locate('MAVT', FieldByName(sFld).AsString, []) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                        end
                        else
                            SetEditState(QrCT);
                        QrCT.CheckBrowseMode;
                    end;
                    Next;
                end;

            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and isnull(BO, 0) = 0 ') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    BtnIn.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
    LydliutfileExcel1.Enabled := bEmptyCT;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('MAKHO').AsString         := sysDefKho;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXAfterEdit(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXAfterPost(DataSet: TDataSet);
begin
	//
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
        FieldByName('THANHTOAN').AsFloat := exVNDRound(FieldByName('SOTIEN').AsFloat, ctCurRound);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO', 'LYDO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
	DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);

//    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(DataSet) then
//        Abort;
//    SetLockedBy(DataSet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat := exVNDRound(
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
            , ctPriceRound);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE : Double;
begin
   	exDotFBMavt(3, DataMain.QrFB_DM_HH, Sender);
    // Update referenced fields
    with QrCT do
    begin
        gia := 0; giaSI := 0; giaLE := 0;
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DONGIA').AsFloat;
                giaLE := FieldByName('DONGIA_LE').AsFloat;

                QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
                QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
                QrCT.FieldByName('DVT_BOX').AsString := FieldByName('TenDvtLon').AsString;
                QrCT.FieldByName('QD1').AsInteger := FieldByName('QuyDoi').AsInteger;
            end;
        end;
       
        FieldByName('DONGIA_LE').AsFloat := exVNDRound(giaLE, ctPriceRound);
        FieldByName('DONGIA_REF').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(gia, ctPriceRound);

        if mLCT = LCT_N then
        begin
			if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
                FieldByName('B1').AsBoolean := True;
        end;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                sl  := FieldByName('SOLUONG').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SOLUONG2').AsFloat := exVNDRound(sl2, ctQtyRound);
                FieldByName('SOLUONG2_LE').AsFloat := exVNDRound(sl2le, ctQtyRound);
            end
            else if (Sender.FullName = 'SOLUONG2') or (Sender.FullName = 'SOLUONG2_LE') then
            begin
                sl2 := FieldByName('SOLUONG2').AsFloat;
                sl2le := FieldByName('SOLUONG2_LE').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat, ctCurRound);

        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat, ctCurRound);
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.QrCTSOTIENChange(Sender: TField);
begin
    with QrCT do
    begin
        FieldByName('SOTIEN1').AsFloat := exVNDRound(FieldByName('SOTIEN').AsFloat, ctCurRound);
    end;

    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_NXKhac.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

end.
