﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_DmvtNVL;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons,
  AdvEdit, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_DmvtNVL = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    ItmNHOM: TMenuItem;
    N2: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopPrint: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    CmdExport: TAction;
    BtExport: TToolButton;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    EdTen: TwwDBEdit;
    PD2: TisPanel;
    PD5: TisPanel;
    EdGHICHU: TDBMemo;
    PD6: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    CmdSwitch: TAction;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    CmdAudit: TAction;
    N3: TMenuItem;
    CmdImport: TAction;
    BtnImport: TToolButton;
    ToolButton13: TToolButton;
    RzLauncher: TRzLauncher;
    PopExport: TAdvPopupMenu;
    Export1: TMenuItem;
    CmdDmdvt: TAction;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    PD7: TisPanel;
    LbGianhap: TLabel;
    wwDBEdit10: TwwDBEdit;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang2: TMenuItem;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    QrDMVTTL_CK_NCC: TFloatField;
    QrDMVTGIANHAP_CHUA_VAT: TFloatField;
    QrDMVTXUATXU: TWideStringField;
    QrDMVTMA_PREFIX: TWideStringField;
    QrDMVTMA: TWideStringField;
    Label14: TLabel;
    EdMA: TwwDBEdit;
    QrQuocgia: TADOQuery;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrDMVTMAVT_NCC: TWideStringField;
    QrDMVTMADT: TWideStringField;
    PopImport: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    QrDMVTGIABQ: TFloatField;
    QrDMVTDINHMUC_CHUA_HAOHUT: TFloatField;
    QrDMVTTHANHTIEN_DINHMUC: TFloatField;
    QrDMVTCONGDOAN_CHUA_HAOHUT: TFloatField;
    QrDMVTTHANHTIEN_CONGDOAN: TFloatField;
    QrDMVTTHANHTIEN_CHIPHI: TFloatField;
    QrDMVTCHECKED: TBooleanField;
    QrNhom: TADOQuery;
    QrNganh: TADOQuery;
    PD0: TisPanel;
    Label3: TLabel;
    LbGianhapVat: TLabel;
    wwDBEdit1: TwwDBEdit;
    EdGianhapVat: TwwDBEdit;
    N1: TMenuItem;
    ingnhnhm1: TMenuItem;
    CmdChangeGroup: TAction;
    CmdImportFileSample: TAction;
    filemu1: TMenuItem;
    N4: TMenuItem;
    QrDMVTMaDvt: TIntegerField;
    QrDMVTMaDvtLon: TIntegerField;
    QrDMVTQuyDoi: TIntegerField;
    CbDVT: TDbLookupComboboxEh2;
    LbX: TLabel;
    CbQuyDoi: TDBNumberEditEh;
    LbQD: TLabel;
    cbbMaDvtLon: TDbLookupComboboxEh2;
    cbbTINHTRANG: TDbLookupComboboxEh2;
    CbbMANGANH: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    DsNganh: TDataSource;
    DsNhom: TDataSource;
    CbMANHOM: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    DBNumberEditEh1: TDBNumberEditEh;
    cbbLOAITHUE: TDbLookupComboboxEh2;
    QrDMVTLK_TINHTRANG_MA: TWideStringField;
    CmdShow: TAction;
    QrDMVTLK_Dvt: TWideStringField;
    QrDMVTLK_DvtLon: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTMAVTValidate(Sender: TField);
    procedure QrDMVTMAChange(Sender: TField);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure cmbNhomBeforeDropDown(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CmdImportFileSampleExecute(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure CbMANHOMDropDown(Sender: TObject);
    procedure CmdShowExecute(Sender: TObject);
    procedure QrDMVTMaDvtChange(Sender: TField);

  private
  	mCanEdit, mEANConfirm, bUnitLevel, mRet: Boolean;
    defDvt : Integer;

    defTinhtrang, defTinhchat, defThue: String;
  	mSQL, mNganh, mNhom, mPrefix, mSearch: String;
    defTinhTrangFilter, defTinhTrangDangDung: String;

    mLoaiNganh: String;
  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmFB_DmvtNVL: TFrmFB_DmvtNVL;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, Scan, Variants,
    FB_CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
  TimBarcode2, Dmkhac, DmvtCT, OfficeData, ShellAPI, DmHotro;

{$R *.DFM}

const
	FORM_CODE = 'FB_DM_NPL';
    TABLE_NAME  = 'FB_DM_HH';
    REPORT_NAME = FORM_CODE;
    EXPORT_EXCEL = 'FB_RP_DM_NPL_EXCEL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmFB_DmvtNVL.Execute;
begin
	mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.FormCreate(Sender: TObject);
var
    x: Integer;
begin
    TMyForm(Self).Init2;
    defTinhTrangDangDung := '01';
    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
	    SetDisplayFormat(QrDMVT, ['TL_CK_NCC'], sysPerFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
    	SetShortDateFormat(QrDMVT);
    end;

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE, 'EAN Confirm');

     // Don vi tinh quy doi
	bUnitLevel := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;

    LbX.Visible := bUnitLevel;
    LbQD.Visible := bUnitLevel;
    CbQuyDoi.Visible := bUnitLevel;
    cbbMaDvtLon.Visible := bUnitLevel;

    // Default unit
	defDvt := GetSysParam('DEFAULT_DVT');
    defThue := GetSysParam('DEFAULT_LOAITHUE');

    // Initial
    mTrigger := False;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    mLoaiNganh := 'FBNPL';
    // Tree Combo
    FlexGroupCombo(CbNhomhang, mLoaiNganh);

    with QrNganh do
    begin
        if Active then
            Close;
        Parameters[0].Value := mLoaiNganh;
        Open;
    end;

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PD5.Collapsed := RegReadBool('PD5');
    PD6.Collapsed := RegReadBool('PD6');
    PD7.Collapsed := RegReadBool('PD7');
    PD7.Height := 50;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.FormShow(Sender: TObject);
var
    it: TMenuItem;
    i: Integer;
begin
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    QrQuocgia.Open;
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrDMLOAITHUE, QrV_FB_DMVT_TINHTRANG]);
    end;



    with DataMain.QrV_FB_DMVT_TINHTRANG do
    begin

        if Locate('MA', defTinhTrangDangDung, []) then
        begin
            defTinhtrang := FieldByName('MA_HOTRO').AsString;
            defTinhTrangFilter := FieldByName('MA_HOTRO').AsString;
        end;
    end;

    with DataMain.QrV_FB_DMVT_TINHTRANG do
    if not IsEmpty then
    begin
         First;
         i := 0;
         while not Eof do
         begin
             i := i + 1;
             it := TMenuItem.Create(Self);
             with it do
             begin
                Caption := FieldByName('TEN_HOTRO').AsString;
                Tag := i;
                RadioItem := true;
                Hint := FieldByName('MA_HOTRO').AsString;
                OnClick := CmdShowExecute;
                Checked := (defTinhTrangFilter = FieldByName('MA_HOTRO').AsString);
                GroupIndex := 2;
             end;
             PopMain.Items.Insert((4 + i), it);
             Next;
         end;
    end;

    OpenDataSets([QrNganh, QrNhom]);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('LK_TINHTRANG_MA').AsString <> defTinhTrangDangDung then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    RegWrite(Name, ['PD2', 'PD5', 'PD6', 'PD7'],
    	[PD2.Collapsed, PD5.Collapsed, PD6.Collapsed, PD7.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdNewExecute(Sender: TObject);
begin
	QrDMVT.Append;
    PD0.Enabled := True;
    CbbMANGANH.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdSaveExecute(Sender: TObject);
begin
    QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';
procedure TFrmFB_DmvtNVL.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom: String;
    r: WORD;
begin
    r := GetRights('FB_DM_NPL_DOINGANHNHOM');
    if r = R_DENY then
        Exit;

	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
    end;
	Application.CreateForm(TFrmFB_CayNganhNhom, FrmFB_CayNganhNhom);
    b := FrmFB_CayNganhNhom.Execute(nganh, nhom, mLoaiNganh);
    FrmFB_CayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdDelExecute(Sender: TObject);
begin
    if DataMain.FB_BarcodeIsUsed(QrDMVT.FieldByName('MAVT').AsString) then
        Abort;
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_PUB_DM_KHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdPrintExecute(Sender: TObject);
var
    REP_NAME: String;
begin
    REP_NAME := REPORT_NAME;

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

procedure TFrmFB_DmvtNVL.CmdShowExecute(Sender: TObject);
var
    n: Integer;
begin

    n := (Sender as TComponent).Tag;
    if n = -1 then
    begin
        defTinhTrangFilter := '';
        PopTinhtrang2.Checked := True;
    end
    else
    begin
        defTinhTrangFilter := (Sender as TMenuItem).Hint;
        (Sender as TMenuItem).Checked := True;
    end;
    mNganh := '~';
    CmdRefresh.Execute;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, bInsert, b: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
        bInsert := State in [dsInsert];
	end;

    GrList.Enabled := bBrowse;
    CmdNew.Enabled := bBrowse and mCanEdit and sysIsCentral;
    CmdEdit.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;

    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    PD0.Enabled := bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, s1 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (EdSearch.Text <> mSearch) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mSearch := EdSearch.Text;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            end;

            if (defTinhTrangFilter <> '')   then
            begin
                sSql := sSql + Format(' and TINHTRANG in (''%s'')', [defTinhTrangFilter]);
            end;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSql := sSql + ' and (' +
                            'dbo.fnStripToneMark([MAVT]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([TENVT]) like ''%' + s1 + '%'''
                            + ')';
            end;

            sSQL := sSQL + ' order by	MANGANH, MANHOM ';
            SQL.Text := sSQL;
            Open;
            First;
        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
        FieldByName('LOAITHUE').AsString := defThue;
        FieldByName('MaDvt').AsInteger := defDvt;
        FieldByName('QuyDoi').AsInteger := 1;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
        FieldByName('XUATXU').AsString := '084';
        FieldByName('MA_PREFIX').AsString := mPrefix;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';
    RS_INVALID_QD = 'Quy đổi không hợp lệ';
    RS_INVALID_UNIT = 'Đơn vị tính không hợp lệ';

procedure TFrmFB_DmvtNVL.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM', 'MA', 'TENVT', 'MaDvt', 'LOAITHUE']) then
			Abort;

        if bUnitLevel then
		begin
            if BlankConfirm(DataSet, ['QuyDoi']) then
                Abort;

            if (FieldByName('QuyDoi').AsInteger < 1) then
            begin
                ErrMsg(RS_INVALID_QD );
                Abort;
            end;

            if ((FieldByName('QuyDoi').Value = 1) and (FieldByName('MaDvt').AsInteger <> FieldByName('MaDvtLon').AsInteger)) or
                ((FieldByName('QuyDoi').Value > 1) and (FieldByName('MaDvt').AsInteger = FieldByName('MaDvtLon').AsInteger)) then
            begin
                ErrMsg(RS_INVALID_UNIT );
                Abort;
            end;
        end else
        begin
           FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger ;
           FieldByName('QuyDoi').Value := 1;
        end;

	    SetNull(DataSet, ['MADT']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTGIANHAPChange(Sender: TField);
var
	mGiavon, mGianhapchuaVAT, mGianhap, mTL_CK_NCC, mGiabanChuaVAT, mGiaban: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDmvt do
    begin
        mGiavon := FieldByName('GIAVON').AsFloat;
        mTL_CK_NCC := FieldByName('TL_CK_NCC').AsFloat;
		mGianhapchuaVAT := FieldByName('GIANHAP_CHUA_VAT').AsFloat;
        mGianhap := FieldByName('GIANHAP').AsFloat;
        mLoaithue := FieldByName('LOAITHUE').AsString;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := mLoaithue;
        Parameters[2].Value := Sender.FieldName;
    	Parameters[3].Value := mGianhapchuaVAT;
        Parameters[4].Value := mGianhap;
        Parameters[5].Value := mGiavon;
        Parameters[6].Value := mGiabanChuaVAT;
        Parameters[7].Value := mGiaban;
        Parameters[8].Value := 0;
        Parameters[9].Value := 0;
        Execute;
    	mGianhapchuaVAT := Parameters[3].Value;
        mGianhap := Parameters[4].Value;
        mGiavon := Parameters[5].Value;
        mGiabanChuaVAT := Parameters[6].Value;
        mGiaban := Parameters[7].Value;
    end;

    mTrigger := True;
	with QrDmvt do
    begin
        FieldByName('GIANHAP_CHUA_VAT').AsFloat := mGianhapchuaVAT;
        FieldByName('GIANHAP').AsFloat := mGianhap;

        FieldByName('GIAVON').AsFloat := mGiavon;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmFB_DmvtNVL.QrDMVTTENVTChange(Sender: TField);
var
	b: Boolean;
begin
	with QrDMVT do
    begin
        FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);

        FieldByName('TENVT_KHONGDAU').AsString := DataMain.StripToneMark(FieldByName('TENVT').AsString)
	end;
end;

procedure TFrmFB_DmvtNVL.BtnImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmFB_DmvtNVL.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTMAVTValidate(Sender: TField);
begin
    with QrDMVT do
    if FieldByName('MAVT').OldValue <> null then
        if DataMain.BarcodeIsUsed(FieldByName('MAVT').OldValue, True) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTMAChange(Sender: TField);
var
    n: Integer;
	mMa, s, mavt: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;

    with QrDMVT do
    begin
        mavt := FieldByName('MA_PREFIX').AsString + FieldByName('MA').AsString;
        FieldByName('MAVT').AsString := mavt;
        FieldByName('MAVT_NCC').AsString := mavt;
    end;
end;

procedure TFrmFB_DmvtNVL.QrDMVTMaDvtChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('MaDvtLon').IsNull) or (FieldByName('MaDvtLon').AsInteger = 0) then
            FieldByName('MaDvtLon').AsInteger := FieldByName('MaDvt').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTMANGANHChange(Sender: TField);
begin
    QrDMVT.FieldByName('MANHOM').AsString := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmFB_DmvtNVL.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.QrDMVTAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdExportExecute(Sender: TObject);
begin
    DataOffice.CreateReport2('XLSX\' + EXPORT_EXCEL + '.xlsx',
        [sysLogonUID, mNganh, mNhom], EXPORT_EXCEL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CbMANHOMDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.cmbNhomBeforeDropDown(Sender: TObject);
    var  s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdImportExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
    rs: Integer;
begin
	sFile := isGetOpenFileName('XLS');
    if sFile = '' then
        Exit;

    DataExcel.ExcelImportConfirm('IMP_' + FORM_CODE, sFile, 'IMP_FB_DM_HH',
        'spIMPORT_FB_DM_NPL;1', 'MAVT', [sysLogonUID, mPrefix, 0], 0);

    CmdReRead.Execute;
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_DmvtNVL.CmdImportFileSampleExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_FB_DM_NPL_EXCEL');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmvtNVL.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

end.
