﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonPhieunhap;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh, Variants, kbmMemTable,
  MemTableDataEh, MemTableEh;

type
  TFrmFB_ChonPhieunhap = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNHAP: TDataSource;
    QrPHIEUNHAP: TADOQuery;
    QrPHIEUNHAPNGAY: TDateTimeField;
    QrPHIEUNHAPSCT: TWideStringField;
    QrPHIEUNHAPMADT: TWideStringField;
    QrPHIEUNHAPMAKHO: TWideStringField;
    QrPHIEUNHAPDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMNCC: TADOQuery;
    QrPHIEUNHAPTENKHO: TWideStringField;
    QrPHIEUNHAPTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrPHIEUNHAPNG_GIAO: TWideStringField;
    QrPHIEUNHAPNG_NHAN: TWideStringField;
    QrPHIEUNHAPTHUE_SUAT: TFloatField;
    QrPHIEUNHAPHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNHAPHOADON_SO: TWideStringField;
    QrPHIEUNHAPHOADON_NGAY: TDateTimeField;
    QrPHIEUNHAPSOLUONG: TFloatField;
    QrPHIEUNHAPTHANHTOAN: TFloatField;
    QrPHIEUNHAPPHIEUGIAOHANG: TWideStringField;
    QrPHIEUNHAPKHOA: TGuidField;
    QrDMNCCMADT: TWideStringField;
    QrDMNCCTENDT: TWideStringField;
    DsDMNCC: TDataSource;
    DsDMKHO: TDataSource;
    EdMADT: TDBEditEh;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    CbbKhoHang: TDbLookupComboboxEh2;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMADTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
    mDelete: Boolean;
  public
  	function Execute(pFix: Boolean; pNcc, pKho: String; bDelete: Boolean = False): TGUID;
  end;

var
  FrmFB_ChonPhieunhap: TFrmFB_ChonPhieunhap;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonPhieunhap.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    mDelete := bDelete;

	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUNHAP.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;

    SetDisplayFormat(QrPHIEUNHAP, sysCurFmt);
    SetDisplayFormat(QrPHIEUNHAP, ['NGAY'], DateTimeFmt);

    SetCustomGrid('CHON_PHIEUNHAP', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.FormShow(Sender: TObject);
begin
	mSQL := QrPHIEUNHAP.SQL.Text;

	OpenDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Smart focus
    if mNCC = '' then
    	try
    		EdMADT.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
   

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNHAP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.CmdRefreshExecute(Sender: TObject);
var
	s, Ncc, Kho: String;
    dTungay, dDenngay: TDateTime;
begin
    Ncc     := EdMADT.Text;
    Kho     := EdMaKho.Text;
    dTungay  := EdTungay.Date;
    dDenngay := EdDenngay.Date;

   	if (mTungay <> dTungay)  or (mDenngay <> dDenngay) or
       (mNCC <> Ncc)        or (mKHO <> Kho)         then
	begin
   	    mNCC := Ncc;
        mKHO := Kho;
        mTungay := dTungay;
        mDenngay := dDenngay;

        with QrPHIEUNHAP do
        begin
			Close;

            s := mSQL;
            if mNCC <> '' then
            	s := s + ' and a.MADT=''' + mNCC + '''';
			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
            if not mDelete then
                s := s + ' and isnull(a.DELETE_BY, 0) = 0';

            SQL.Text := s;
            SQL.Add(' order by NGAY, SCT');
			Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
begin
    with QrPHIEUNHAP do
    begin
    	Parameters[0].Value := mTungay;
        Parameters[1].Value := mDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mNCC;
        FieldByName('TuNgay').AsDateTime := Date - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.CbMADTNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonPhieunhap.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;


end.
