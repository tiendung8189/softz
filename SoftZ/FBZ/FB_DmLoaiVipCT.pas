﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_DmLoaiVipCT;

interface

uses
  SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, ExtCtrls, StdCtrls, Db,
  Wwdbgrid2, ADODb, Menus, AdvMenus,
  AppEvnts, wwdblook,
  wwFltDlg2, Grids,
  Wwdbigrd, wwDialog, wwfltdlg, ToolWin, Wwdbgrid, DBGridEh, Vcl.Mask,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_DmLoaiVipCT = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrNhom: TADOQuery;
    Filter: TwwFilterDialog2;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdAudit: TAction;
    Panel1: TPanel;
    QrDmChietkhau: TADOQuery;
    DsDmChietkhau: TDataSource;
    QrDmChietkhauTL_CK: TFloatField;
    QrDmChietkhauMA: TWideStringField;
    QrDmChietkhauLK_TENNHOM: TWideStringField;
    QrDmChietkhauLK_TENNGANH: TWideStringField;
    QrDmChietkhauCREATE_BY: TWideStringField;
    QrDmChietkhauUPDATE_BY: TWideStringField;
    QrDmChietkhauCREATE_DATE: TDateTimeField;
    QrDmChietkhauUPDATE_DATE: TDateTimeField;
    QrLOAI_THE_VIP: TADOQuery;
    DsLOAI_THE_VIP: TDataSource;
    QrNhomMANHOM: TWideStringField;
    QrNhomTENNHOM: TWideStringField;
    QrNhomMANGANH: TWideStringField;
    QrNhomTENNGANH: TWideStringField;
    QrDmChietkhauMANHOM: TWideStringField;
    CmdChonNhom: TAction;
    QrTemp: TADOQuery;
    Chnnhmhng1: TMenuItem;
    N1: TMenuItem;
    cbbLoaiTheVIP: TDbLookupComboboxEh2;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDM1BeforePost(DataSet: TDataSet);
    procedure QrDM1BeforeDelete(DataSet: TDataSet);
    procedure QrDM1PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDM1BeforeInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDmChietkhauAfterInsert(DataSet: TDataSet);
    procedure QrDmChietkhauBeforePost(DataSet: TDataSet);
    procedure QrDmChietkhauBeforeDelete(DataSet: TDataSet);
    procedure QrDmChietkhauPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDmChietkhauBeforeInsert(DataSet: TDataSet);
    procedure CbMaNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure QrDmChietkhauMANHOMChange(Sender: TField);
    procedure QrLOAI_THE_VIPBeforeOpen(DataSet: TDataSet);
    procedure CmdChonNhomExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
  private
  	mCanEdit: Boolean;
    mLoaithe, FORM_CODE, tmpSQL: String;
    mLoai: Integer;
    function  IsInused(pQuay: String = ''): Boolean;
    function  exDotNhom2(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
  public
  	procedure Execute(r: WORD; pLoai: Integer = 0); overload;
    procedure Execute(const loaithe: String; pLoai: Integer = 0); overload;
  end;

var
  FrmFB_DmLoaiVipCT: TFrmFB_DmLoaiVipCT;

implementation

uses
	MainData, isDb, isMsg, ExCommon, Rights, isCommon, FB_ChonDsNhomCk,
    isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.Execute(r: WORD; pLoai: Integer);
begin
	mCanEdit := rCanEdit(r);
    mLoai := pLoai;
    if mLoai = 0 then
    begin
        FORM_CODE := 'FB_DM_LOAIVIP_CT';
        Caption := 'Chiết Khấu Thẻ VIP - Nhà Hàng';
    end else
    begin
        FORM_CODE := 'SZ_DM_LOAIVIP_KH_CT';
        Caption := 'Chiết Khấu Theo Phân Cấp Khách Hàng';
    end;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1('_IDI_BOOKS');
    mTrigger := False;
    tmpSQL := QrTemp.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdNewExecute(Sender: TObject);
begin
	QrDmChietkhau.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdSaveExecute(Sender: TObject);
begin
	QrDmChietkhau.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdCancelExecute(Sender: TObject);
begin
	QrDmChietkhau.Cancel;
end;

procedure TFrmFB_DmLoaiVipCT.CmdChonNhomExecute(Sender: TObject);
var
	s: String;
    mType: Integer;
    ck: Double;
begin
    ck := QrDmChietkhau.FieldByName('TL_CK').AsFloat;
    if not FrmFB_ChonDsNhomCk.Get(mType, s, ck) then
        Exit;

    if s = '' then
        Exit;

    with QrTemp do
    begin
        Close;
        SQL.Text := tmpSQL;
        case mType of
        0:	// Nganh
            SQL.Add(' and b.MANGANH in (' + s + ')');
        1:	// Nhom
            SQL.Add(' and b.MANHOM in (' + s + ')');
        else
            SQL.Text := (' and 1=1');
        end;
        Open;
        while not Eof do
        begin
            s := FieldByName('MANHOM').AsString;
            with QrDmChietkhau do
                if Locate('MANHOM', s, []) then
                begin
                    Edit;
                    FieldByName('TL_CK').AsFloat := ck;
                    CheckBrowseMode
                end else
                begin
                    Append;
                    FieldByName('MANHOM').AsString := s;
                    FieldByName('TL_CK').AsFloat := ck;
                    CheckBrowseMode;
                end;

            Next;
        end;
        Close;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdDelExecute(Sender: TObject);
begin
	QrDmChietkhau.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdPrintExecute(Sender: TObject);
begin
	//
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    OpenDataSets([QrNhom, QrLOAI_THE_VIP]);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDmChietkhau, FORM_CODE, Filter);
    SetDisplayFormat(QrDmChietkhau, ['TL_CK'], sysPerFmt);

    if mLoaithe = '' then
    	mLoaithe := QrLOAI_THE_VIP.FieldByName('MA').AsString;

    cbbLoaiTheVIP.Value := mLoaithe;
    with QrDmChietkhau do
    begin
    	Close;
        Open;
    end;
    with GrList do
    begin
    	ReadOnly := not mCanEdit;
    	SetFocus;
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDmChietkhau);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.Execute(const loaithe: String; pLoai: Integer);
begin
    mCanEdit := False;
    Panel1.Enabled := False;
    mLoaithe := loaithe;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmChietkhau, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmChietkhau, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDM1BeforePost(DataSet: TDataSet);
begin
	with QrDmChietkhau do
    begin
		if BlankConfirm(QrDmChietkhau, ['MA', 'MANHOM']) then
    		Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDM1BeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if IsInused then
		Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDM1PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDM1BeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
    if QrDmChietkhau.IsEmpty then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
	RS_INUSED = 'Đã có phát sinh dữ liệu, không thể sửa/xóa.';

function TFrmFB_DmLoaiVipCT.IsInused;
var
	kho, quay: String;
begin
{NTD
    with QrDmChietkhau do
    begin
	    kho := FieldByName('MA').AsString;
        if pQuay = '' then
        	quay := FieldByName('QUAY').AsString
        else
        	quay := pQuay;
	end;

    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
		SQL.Text := Format('select top 1 QUAY from CHUNGTU where LCT=''BLE'' and MAKHO=%s and QUAY=%s',
        	[kho, quay]);
        Open;
		Result := not IsEmpty;
        Close;
        Free;
    end;

    if Result then
        ErrMsg(RS_INUSED);
  }
end;

procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauAfterInsert(DataSet: TDataSet);
begin
    with QrDmChietkhau do
        FieldByName('MA').AsString := QrDmChietkhau.FieldByName('MA').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDmChietkhau, ['MANHOM', 'TL_CK']) then
    	Abort;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MANHOM =
    	'MANHOM'#9'12'#9'Mã nhóm'#13 +
        'TENNHOM'#9'30'#9'Tên nhóm'#13 +
    	'MANGANH'#9'12'#9'Mã Ngành'#13 +
        'TENNGANH'#9'30'#9'Tên Ngành'#13;

function TFrmFB_DmLoaiVipCT.exDotNhom2(DataSet: TDataSet; Sender: TField;
  exCond: String = ''): Boolean;
    var
    s: String;
begin
    s := Sender.AsString;
    Result := QuickSelect(DataSet, s, DOT_SEARCH_MANHOM, exCond);
    if Result then
        Sender.AsString := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauMANHOMChange(Sender: TField);
begin
    exDotNhom2(QrNHOM, Sender);
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrDmChietkhauPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
   	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.QrLOAI_THE_VIPBeforeOpen(DataSet: TDataSet);
begin
    (DataSet as TADOQuery).Parameters[0].Value := mLoai;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := RecordCount(QrDmChietkhau);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CbMaNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_DmLoaiVipCT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDmChietkhau);
end;
end.
