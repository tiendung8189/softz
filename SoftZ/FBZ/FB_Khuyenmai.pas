﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Khuyenmai;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants, Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls, Wwmemo,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2,
  wwdbdatetimepicker, wwdbedit, Wwdbcomb, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi, isDb,
  isPanel, wwDialog, Wwdotdot, Mask, Grids, Wwdbgrid, ToolWin, wwdblook,
  frameLoc, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_Khuyenmai = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrKM: TADOQuery;
    QrCT: TADOQuery;
    DsKM: TDataSource;
    DsCT: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKMXOA: TWideStringField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrKMIMG: TIntegerField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    CmdAudit: TAction;
    BtnBarCode2: TToolButton;
    QrKMNGAY: TDateTimeField;
    QrKMSCT: TWideStringField;
    QrKMTUNGAY: TDateTimeField;
    QrKMDENNGAY: TDateTimeField;
    QrKMTL_CK: TFloatField;
    QrKMLYDO: TWideStringField;
    QrKMDGIAI: TWideMemoField;
    QrKMCREATE_BY: TIntegerField;
    QrKMCREATE_DATE: TDateTimeField;
    QrKMUPDATE_BY: TIntegerField;
    QrKMUPDATE_DATE: TDateTimeField;
    QrKMDELETE_BY: TIntegerField;
    QrKMDELETE_DATE: TDateTimeField;
    TntLabel1: TLabel;
    EdLyDo: TwwDBEdit;
    Label5: TLabel;
    CbTungay: TwwDBDateTimePicker;
    TntLabel2: TLabel;
    CbDenngay: TwwDBDateTimePicker;
    TntLabel3: TLabel;
    EdTLCK: TwwDBEdit;
    Label10: TLabel;
    DBEdit1: TDBMemo;
    TntToolButton2: TToolButton;
    QrDMHH: TADOQuery;
    QrKMDENGIO: TDateTimeField;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    wwDBEdit1: TwwDBDateTimePicker;
    QrKMTUGIO: TDateTimeField;
    wwDBEdit2: TwwDBDateTimePicker;
    CmdDiemban: TAction;
    TntToolButton3: TToolButton;
    Status2: TStatusBar;
    Filter2: TwwFilterDialog2;
    CmdListRefesh: TAction;
    CmdChonNhom: TAction;
    QrTemp: TADOQuery;
    CmdEmpty: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    Xachitit1: TMenuItem;
    CmdChonNCC: TAction;
    Chnnhcungcp1: TMenuItem;
    CmdChonphieuKM: TAction;
    QrTemp2: TADOQuery;
    Chnnhcungcp2: TMenuItem;
    QrKMKHOA: TGuidField;
    QrKMLOC: TWideStringField;
    CmdChonNhomNCC: TAction;
    Chnnhcungcpnhmhng1: TMenuItem;
    QrKMLCT: TWideStringField;
    QrKMHTKM: TWideStringField;
    CmdIntem: TAction;
    ToolButton5: TToolButton;
    Label3: TLabel;
    EdGiaban: TwwDBEdit;
    QrKMGIABAN: TFloatField;
    QrKMLK_HINHTHUC: TWideStringField;
    QrKMNOIDUNG: TWideStringField;
    QrDM: TADOQuery;
    QrDMMAVT: TWideStringField;
    QrDMTENVT: TWideStringField;
    QrKMCHECKED: TBooleanField;
    QrKMCHECKED_BY: TIntegerField;
    QrKMCHECKED_DATE: TDateTimeField;
    QrKMCHECKED_DESC: TWideMemoField;
    ToolButton6: TToolButton;
    CmdChecked: TAction;
    ToolButton8: TToolButton;
    QrKMIMG2: TIntegerField;
    Label4: TLabel;
    EdCheckedDate: TwwDBComboDlg;
    Label6: TLabel;
    MemoDate: TwwMemoDialog;
    PopItem1: TMenuItem;
    PopItem2: TMenuItem;
    PopItemAll: TMenuItem;
    N4: TMenuItem;
    QrKMCAPDO: TIntegerField;
    N6: TMenuItem;
    QrKMLK_LYDO: TWideStringField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrCTSTT: TIntegerField;
    QrCTMAVT: TWideStringField;
    QrCTTENVT: TWideStringField;
    QrCTGIANHAPVAT: TFloatField;
    QrCTGIABAN: TFloatField;
    QrCTTL_LAI: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrCTGIABAN1: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTTL_LAI1: TFloatField;
    QrCTB1: TBooleanField;
    QrCTCREATE_BY: TIntegerField;
    QrCTCREATE_DATE: TDateTimeField;
    QrCTUPDATE_BY: TIntegerField;
    QrCTUPDATE_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    QrCTLK_GIABAN: TFloatField;
    QrCTLK_DVT: TWideStringField;
    QrCTLK_QD1: TWideStringField;
    QrCTLK_DVT_BOX: TWideStringField;
    QrCTTINHTRANG: TWideStringField;
    QrCTLK_GIANHAPVAT: TFloatField;
    QrCTLK_GIAVON: TFloatField;
    QrCTRSTT: TIntegerField;
    QrList: TADOStoredProc;
    QrList_Tonkho: TADOStoredProc;
    N3: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    CmdImportExcel: TAction;
    N5: TMenuItem;
    cbNgayKhoa: TwwDBDateTimePicker;
    ToolButton4: TToolButton;
    ToolButton10: TToolButton;
    PopDanhmuc: TAdvPopupMenu;
    NguynphliuFB1: TMenuItem;
    CmdNPL: TMenuItem;
    CmdDMTD: TAction;
    CmdDMNPL: TAction;
    cbbLyDo: TDbLookupComboboxEh2;
    cbbHinhThuc: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrKMAfterInsert(DataSet: TDataSet);
    procedure QrKMBeforeOpen(DataSet: TDataSet);
    procedure QrKMBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKMBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKMCalcFields(DataSet: TDataSet);
    procedure QrKMBeforeEdit(DataSet: TDataSet);
    procedure QrKMAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrKMAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDiembanExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdChonNhomExecute(Sender: TObject);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure CmdChonNCCExecute(Sender: TObject);
    procedure CmdChonphieuKMExecute(Sender: TObject);
    procedure CmdChonNhomNCCExecute(Sender: TObject);
    procedure QrCTTL_CKChange(Sender: TField);
    procedure QrKMTL_CKChange(Sender: TField);
    procedure CmdIntemExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure QrKMHTKMChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure EdCheckedDateCustomDlg(Sender: TObject);
    procedure MemoDateInitDialog(Dialog: TwwMemoDlg);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure PopItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure CmdDMNPLExecute(Sender: TObject);
    procedure CmdDMTDExecute(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;

    // List filter
    fType, mFilter: Integer;
    mLCT, fSQL, fStr, tmpSQL, tmpSQL2: String;
    mMaTinhTrang: String;
    fLoc: String;
  public
	procedure Execute(r: WORD);
    procedure GetListHH(var pCount: Integer; pType: Integer;
        pString, pMadt: String; pKhoa: TGUID);
  end;

var
  FrmFB_Khuyenmai: TFrmFB_Khuyenmai;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, isCommon,
    isLib, FB_Khuyenmai2,  FB_ChonDsNhom, ChonDsNCC, FB_ChonPhieuKM, GuidEx,
  FB_ChonDsNhomNCC, ExcelData, ImportExcel, isFile, FB_ChonDsHHKM, FB_ChonDsma, FB_Dmhh, FB_DmvtNVL;

{$R *.DFM}

const
	FORM_CODE = 'FB_PHIEU_KHUYENMAI_BL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.EdCheckedDateCustomDlg(Sender: TObject);
begin
    EdCheckedDate.SelectAll;
	MemoDate.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.Execute;
begin
    mLCT := 'FKMAI';

	mCanEdit := rCanEdit(r);
    DsKM.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.FormCreate(Sender: TObject);
begin
    frNavi.DataSet := QrKM;

    // Display Format
    with QrKM do
    begin
        SetDisplayFormat(QrKM, sysCurFmt);
	    SetShortDateFormat(QrKM);
    	SetDisplayFormat(QrKM, ['TUGIO', 'DENGIO'], 'hh:nn');
	    SetDisplayFormat(QrKM, ['TL_CK'], sysPerFmt);
        SetDisplayFormat(QrKM, ['NGAY', 'CHECKED_DATE'], DateTimeFmt);
    end;
	SetDisplayFormat(QrCT, sysCurFmt);
    SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrKM, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, nil]);

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrKM.SQL.Text;
    tmpSQL := QrTemp.SQL.Text;
    tmpSQL2 := QrTemp2.SQL.Text;
    mFilter := 1;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

	Wait(PREPARING);
    InitFmtAndRoud(mLCT);

    with DataMain do
        OpenDataSets([QrLOAI_KHUYENMAI, QrLYDO_KM, QrV_FB_DMVT_TINHTRANG, QrFB_DM_NPL_THUCDON]);

    OpenDataSets([QrDMHH]);


    SetDisplayFormat(QrDMHH, sysCurFmt);
    ClearWait;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrKM, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDMHH]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdRefreshExecute(Sender: TObject);
var
	s, s2 : String;
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    d := Now;
    DecodeDate(d, yy, mm, dd);
    DecodeTime(d, hh, nn, ss, ms);

    d1 := EncodeDate(yy, mm, dd);
    d2 := EncodeTime(hh, nn, ss, ms);
   	if (fLOC <> '')then
    begin
        fLoc     := '';
		Screen.Cursor := crSQLWait;
		with QrKM do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           		case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_Khuyenmai_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_Khuyenmai.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_Khuyenmai_CT a, FB_DM_HH b where a.KHOA = FB_Khuyenmai.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_Khuyenmai_CT where KHOA = FB_Khuyenmai.KHOA and MAVT in (' + fStr + '))');
				end;
            s2 := '';
            case mFilter of
            1:
            begin
               s2 := ' and ( (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <=  DENNGAY' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' >= isnull(TUGIO, CAST(''1899/12/30 00:00:00'' as datetime))' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' <= isnull(DENGIO, CAST(''1899/12/30 23:59:59'' as datetime)) )' +
                          ' and isnull(DELETE_BY, 0) = 0 )';
                SQL.Add(s2);
            end;
            2:
            begin
                s2 := ' and ( not (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <=  DENNGAY' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' >= isnull(TUGIO, CAST(''1899/12/30 00:00:00'' as datetime))' +
                            ' and ''' + FormatDateTime(DateTimeFmtSQL, d2) + ''' <= isnull(DENGIO, CAST(''1899/12/30 23:59:59'' as datetime)) )' +
                          ' or isnull(DELETE_BY, 0) <> 0 )';
                SQL.Add(s2);
            end;
            end;

			SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMHH.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdNewExecute(Sender: TObject);
begin
	QrKM.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrKM.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrKM.Cancel;

    if QrKM.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrKM);
 end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdChonNCCExecute(Sender: TObject);
var
	s: String;
    x: Integer;
begin
    if not FrmChonDsNCC.Get(s) then
        Exit;

    if s = '' then
        Exit;

    GetListHH(x, 0, '', s, TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdChonNhomExecute(Sender: TObject);
var
	s: String;
    mType, x: Integer;
begin
    if not FrmFB_ChonDsNhom.Get(mType, s) then
        Exit;

    if s = '' then
        Exit;

    GetListHH(x, mType, s, '', TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdChonNhomNCCExecute(Sender: TObject);
var
	s, s2, ma: String;
    mType, x: Integer;
begin
    if not FrmFB_ChonDsNhomNCC.Get(mType, s, s2) then
        Exit;

    if (s = '') or (s2 = '') then
    begin
        ErrMsg('Dữ liệu chọn không hợp lệ.');
        Exit;
    end;
    GetListHH(x, mType, s, s2, TGuidEx.EmptyGuid());
    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdChonphieuKMExecute(Sender: TObject);
var
    n: TGUID;
    s: String;
    x: Integer;
begin
    Application.CreateForm(TFrmFB_ChonPhieuKM, FrmFB_ChonPhieuKM);
    n := FrmFB_ChonPhieuKM.Execute;
	FrmFB_ChonPhieuKM.Free;

    if TGuidEx.IsEmptyGuid(n) then
        Exit;

    GetListHH(x, -1, '', '', n);

    if x > 0  then
        Msg(Format('Đã thêm %d mặt hàng', [x]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFile: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);
    if sFile = '' then
    	Exit;

    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';

            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;
                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;

                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if not QrCT.Locate('MAVT', FieldByName(sFld).AsString, []) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                        end;
                        QrCT.CheckBrowseMode;
                    end;
                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA );
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdIntemExecute(Sender: TObject);
var
    bm: TBytes;
begin
    CmdSave.Execute;
	if not ShowReport(Caption, FORM_CODE + '_INTEM', [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]) then
        Exit;

    with QrCT do
    begin
    	bm := Bookmark;
        DisableControls;
        First;
        while not Eof do
        begin
            if FieldByName('B1').AsBoolean then
            begin
                Edit;
                FieldByName('B1').AsBoolean := False;
            end;

            Next;
        end;
        Bookmark := bm;
        EnableControls;
    end;

    CmdSave.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsKM)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s, ' and isnull(BO, 0) = 0 and MANGANH in (select MANGANH from DM_NGANH where LOAI = ''FBTP'')') then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdReReadExecute(Sender: TObject);
begin
	fLoc := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted, bHt: Boolean;
    n: Integer;
begin
	with QrKM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
        bHt := FieldByName('HTKM').AsString = 'K';
    end;
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrKM);

    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrKM, False);
    CmdChecked.Caption := GetCheckedCaption(QrKM);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdIntem.Enabled := not bEmpty;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdDiemban.Enabled := not bEmpty and bBrowse and (n = 1);
    CmdEmpty.Enabled := not bEmptyCT;

    CmdDMNPL.Enabled := n = 1;
    CmdDMTD.Enabled := n = 1;

// hinhthuc
    EdTLCK.Enabled:= QrKM.FieldByName('HTKM').AsString = 'K';
    begin
        if EdTLCK.Enabled then
            EdTLCK.Color:= clWindow
        else
            EdTLCK.Color := clLtGray;

        if EdTLCK.Enabled then
            EdGiaban.Clear;
    end;
    EdGiaban.Enabled := QrKM.FieldByName('HTKM').AsString = 'D';
    begin
         if EdGiaban.Enabled then
               EdGiaban.Color:= clWindow
         else
            EdGiaban.Color := clLtGray;

         if EdGiaban.Enabled then
            EdTLCK.Clear;
    end;
    LydliutfileExcel1.Enabled := bEmptyCT;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMAfterInsert(DataSet: TDataSet);
var
    d, d1, d2: TDateTime;
    xx, yy: WORD;
begin
    d := Now;
    d1 := Date;
    DecodeDate(d1, yy, xx, xx);
    d2 := EncodeDate(yy, 12, 31);
	with QrKM do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('LCT').AsString       := mLCT;
        FieldByName('HTKM').AsString      := 'K';
        with DataMain.QrLYDO_KM do
        begin
            QrKM.FieldByName('LYDO').AsString      := FieldByName('MA').AsString;
        end;
        FieldByName('CAPDO').AsInteger    := 1;
        FieldByName('CHECKED').AsBoolean  := False;
		FieldByName('NGAY').AsDateTime    := d;
        FieldByName('TUNGAY').AsDateTime  := d1;
        FieldByName('DENNGAY').AsDateTime := d2;
        FieldByName('TUGIO').AsDateTime   := EncodeTime(0,0,0,0);
        FieldByName('DENGIO').AsDateTime  := 1 + EncodeTime(23,59,0,0);
        FieldByName('LOC').AsString       := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMHTKMChange(Sender: TField);
begin
    if mTrigger then
        Exit;
    with QrKM do
    begin
//        mTrigger := True;
//        if Sender.AsString  = 'D' then
            FieldByName('TL_CK').AsFloat := 0;
//        else
//            FieldByName('GIABAN').AsFloat := 0;
//        mTrigger := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CAPNHAT_CHIETKHAU =
        'Chỉ cập nhật các mặt hàng chưa có chiết khấu?';
procedure TFrmFB_Khuyenmai.QrKMTL_CKChange(Sender: TField);
var
	bm: TBytes;
    b: TVnsModalResult;
begin
//    b := YesNoCancel(RS_CAPNHAT_CHIETKHAU);
//
//    if b = vmrCancel then
//        Exit;

	with QrCT do
    begin
        if IsEmpty then
            Exit;
        Wait(PROCESSING);
        try
            bm := Bookmark;
            DisableControls;
            First;
            while not Eof do
            begin
                Edit;

                if Sender.FieldName = 'TL_CK' then
                begin
    //                if FieldByName('TL_CK').AsFloat = 0 then
                        FieldByName('TL_CK').AsFloat := Sender.AsFloat
                end
                else
    //                if FieldByName('GIABAN1').AsFloat = 0 then
                        FieldByName('GIABAN1').AsFloat := Sender.AsFloat;
                Next;
            end;
            Bookmark := bm;
            EnableControls;
        finally
            ClearWait;
        end;
    end;
end;

procedure TFrmFB_Khuyenmai.ToolButton4Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMBeforeOpen(DataSet: TDataSet);
begin
	with QrKM do
    begin
		Parameters[0].Value := mLCT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMBeforePost(DataSet: TDataSet);
var
	dengio: TDateTime;
begin
	with QrKM do
    begin
		if BlankConfirm(QrKM, ['NGAY', 'LYDO', 'HTKM', 'TUNGAY', 'DENNGAY']) then
	    	Abort;

//	    exValidClosing(FieldByName('NGAY').AsDateTime);

	    // Adjust hours
	    dengio := Frac(FieldByName('DENGIO').AsDateTime);
        if dengio <> 0 then
        	FieldByName('DENGIO').AsDateTime := dengio
        else
        	FieldByName('DENGIO').AsDateTime := 1;

        if FieldByName('TUGIO').AsDateTime > FieldByName('DENGIO').AsDateTime then
        begin
        	ErrMsg('Giờ khuyến mãi không hợp lệ.');
        	Abort;
        end;
	end;
    DataMain.AllocSCT(mLCT, QrKM);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrKM);
    if mTrigger then
        Exit;

    exIsChecked(QrKM);
    exReSyncRecord(QrKM);
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrKMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    with QrCT do
    begin
        if BlankConfirm(QrCT, ['MAVT']) then
            Abort;

        if FieldByName('TINHTRANG').AsString <> '01' then
        begin
            ErrMsg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;


        with QrCT do
        begin
            if IsDuplicateCode(QrCT, FieldByName('MAVT'), True) then
                Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTMAVTChange(Sender: TField);
begin
	exDotFBMavt(1, QrDMHH, Sender);

    with QrCT do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;

        FieldByName('GIANHAPVAT').AsFloat := exVNDRound(FieldByName('LK_GIANHAPVAT').AsFloat, ctPriceRound);
    	FieldByName('GIABAN').AsFloat := exVNDRound(FieldByName('LK_GIABAN').AsFloat, ctPriceRound);

        if QrKM.FieldByName('HTKM').AsString = 'K' then
            FieldByName('TL_CK').AsFloat := exVNDRound(QrKM.FieldByName('TL_CK').AsFloat, sysPerRound)
        else
            FieldByName('GIABAN1').AsFloat := exVNDRound(QrKM.FieldByName('GIABAN').AsFloat, ctPriceRound);

    end;
	GrDetail.InvalidateCurrentRow;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.QrCTTL_CKChange(Sender: TField);
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        mTrigger := True;
        if Sender.FieldName = 'TL_CK' then
        begin
            FieldByName('GIABAN1').AsFloat := exVNDRound(FieldByName('GIABAN').AsFloat *
            	(1 - FieldByName('TL_CK').AsFloat / 100), ctPriceRound);
        end else
        begin
            FieldByName('TL_CK').AsFloat := exVNDRound(
                100 * (1 - SafeDiv(FieldByName('GIABAN1').AsFloat, FieldByName('GIABAN').AsFloat)), sysPerRound);
        end;
         mTrigger := False;
    end;
end;

(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.GetListHH(var pCount: Integer; pType: Integer; pString, pMadt: String; pKhoa: TGUID);
var
    s: String;
begin

    pCount := 0;
    Application.CreateForm(TFrmFB_ChonDsHHKM, FrmFB_ChonDsHHKM);
    if not FrmFB_ChonDsHHKM.Execute(pType, pString, pMadt, pKhoa) then
        Exit;

    with QrList do
    begin
        Wait(PROCESSING);
        try
            Filter := 'SELECTED=1';
            Filtered := True;
            if RecordCount > 0 then
            begin
                SetEditState(QrKM);
                First;
                while not eof do
                begin
                    s := FieldByName('MAVT').AsString;
                    if not QrCT.Locate('MAVT', s, []) then
                    begin
                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        Inc(pCount);
                    end;

                    Next;
                end;
                QrCT.CheckBrowseMode;
            end;
            Filter := '';
        finally
            ClearWait;
        end;
    end;
    QrList.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    if Highlight then
        Exit;

    d := Now;
    DecodeDate(d, yy, mm, dd);
    DecodeTime(d, hh, nn, ss, ms);

    d1 := EncodeDate(yy, mm, dd);
    d2 := EncodeTime(hh, nn, ss, ms);
    with QrKM do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clRed;
            Exit;
        end;

        if  not ((d1 <= (FieldByName('DENNGAY').AsDateTime)) and
            (d2 >= FieldByName('TUGIO').AsDateTime) and
            (d2 <= FieldByName('DENGIO').AsDateTime))
        then
        begin
            AFont.Color := clSilver;
            Exit;
        end;

        if not FieldByName('CHECKED').AsBoolean then
        begin
            AFont.Color := clMaroon;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.GrBrowseDblClick(Sender: TObject);
begin
    if QrKM.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.MemoDateInitDialog(Dialog: TwwMemoDlg);
var
	ls: TStrings;
    i, n: Integer;
begin
	ls := TStringList.Create;
    ls.Text := QrKM.FieldByName('CHECKED_DESC').AsString;
    n := ls.Count;
    for i := 0 to ls.Count - 1 do
    begin
    	ls[i] := Format('%2d: %s', [n, ls[i]]);
        Dec(n);
    end;

	with Dialog do
    begin
    	Memo.ReadOnly := False;
        OKBtn.Visible := False;
        CancelBtn.Font.Style := [fsBold];
        Memo.Lines.AddStrings(ls);
    end;
    ls.Free;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if PgMain.ActivePageIndex = 1 then
    	Status2.SimpleText := exRecordCount(QrCT,Filter2)
    else
   		Status.SimpleText := exRecordCount(QrKM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.PopItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.PopupMenu1Popup(Sender: TObject);
begin
    PopItem1.Checked := mFilter = 1;
    PopItem2.Checked := mFilter = 2;
    PopItemAll.Checked := mFilter = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsKM, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdDiembanExecute(Sender: TObject);
begin
	with QrKM do
    Begin
  		Application.CreateForm(TFrmFB_Khuyenmai2, FrmFB_Khuyenmai2);
		FrmFB_Khuyenmai2.Execute(
            TGuidField(FieldByName('KHOA')).AsGuid,
            FieldByName('CHECKED').AsBoolean,
            FieldByName('SCT').AsString);
    end;
end;
procedure TFrmFB_Khuyenmai.CmdDMNPLExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    mRet := FrmFB_DmvtNVL.Execute(r, False);

    if mRet then
    begin
        QrDMHH.Requery;
    end;
end;

procedure TFrmFB_Khuyenmai.CmdDMTDExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    mRet := FrmFB_Dmhh.Execute(r, False);

    if mRet then
    begin
        QrDMHH.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Khuyenmai.CmdEmptyExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;
end.
