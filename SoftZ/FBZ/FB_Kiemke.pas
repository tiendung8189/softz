﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Kiemke;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi,
  isPanel, isDb, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, RzLaunch, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_Kiemke = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXNG_GIAO: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXDGIAI: TWideMemoField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXNG_NHAN: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    BtImport: TToolButton;
    SepChecked: TToolButton;
    PopMaster: TAdvPopupMenu;
    QrDM_QUAYKE: TADOQuery;
    QrNXQUAYKE: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    N2: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    PaTotal: TPanel;
    Label24: TLabel;
    ImgTotal: TImage;
    TntLabel9: TLabel;
    EdTriGiaTT: TwwDBEdit;
    EdQtyTotal: TwwDBEdit;
    QrNXSOLUONG: TFloatField;
    vlTotal1: TisTotal;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdRefresh: TAction;
    CmdSwitch: TAction;
    CmdFilter: TAction;
    CmdDel: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    CmdClearFilter: TAction;
    CmdImportExcel: TAction;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    QrCTDONGIA_REF: TFloatField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrNXNGAY2: TDateTimeField;
    Label4: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    CmdSapthutu: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    RzLauncher: TRzLauncher;
    CmdImport2: TAction;
    PopIm: TAdvPopupMenu;
    Lydliutthitbkimkho1: TMenuItem;
    QrCTDONGIA_REF2: TFloatField;
    QrCTQD1: TIntegerField;
    QrCTB1: TBooleanField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA2: TFloatField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    PopupMenu2: TAdvPopupMenu;
    Phiuvnchuynnib1: TMenuItem;
    MenuItem3: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXLOC: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTDONGIA_LE: TFloatField;
    QrCTSOTIEN_LE: TFloatField;
    QrNXSOTIEN_LE: TFloatField;
    QrCsv: TADOQuery;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    QrCTEX_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    Lydliutfiletext1: TMenuItem;
    QrCTSOLUONG2_LE: TFloatField;
    N5: TMenuItem;
    ItemObsolete: TMenuItem;
    CmdCheckton: TAction;
    N6: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    XutdliutliraExcel1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    ToolButton10: TToolButton;
    QrCTTENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTDVT_BOX: TWideStringField;
    QrFB_LYDO_KKE: TADOQuery;
    QrNXLK_LYDO: TWideStringField;
    CmdImportExcelTemplate: TAction;
    N7: TMenuItem;
    LydliutfileExcel2: TMenuItem;
    ivfileExcelmu1: TMenuItem;
    CbbQuayKe: TDbLookupComboboxEh2;
    CbLyDo: TDbLookupComboboxEh2;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMAKHO: TDBEditEh;
    DsFB_LYDO_KKE: TDataSource;
    DsDM_QUAYKE: TDataSource;
    DBMemoEh1: TDBMemoEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);

    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure BtImportClick(Sender: TObject);
    procedure CmdImport2Execute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure CmdImportExcelTemplateExecute(Sender: TObject);
  private
  	mLCT: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;

    procedure OpenDetail;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmFB_Kiemke: TFrmFB_Kiemke;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, IniFiles, GuidEx, isCommon,
    isLib, wwStr, ShellApi, isFile, FB_Sapthutu, ImportExcel,
  FB_CheckTonkho, OfficeData, InlabelChungtu, FB_ChonDsma, ExcelData;

{$R *.DFM}
const
	FORM_CODE = 'FB_PHIEU_KIEMKE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.Execute;
begin
	mLCT := 'FKKE';
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    // Initial
  	mTrigger := False;
    mObsolete := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.FormShow(Sender: TObject);
begin
	Wait(PREPARING);
    InitFmtAndRoud(mLCT);

    OpenDataSets([DataMain.QrDMKHO, QrDM_QUAYKE, DataMain.QrFB_DM_HH, QrFB_LYDO_KKE]);
    SetDisplayFormat(DataMain.QrFB_DM_HH, sysCurFmt);

    with QrNX do
    begin
		SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
	    TFloatField(FieldByName('NGAY2')).DisplayFormat := ShortDateFormat + ' HH:SS';
        SetDisplayFormat(QrNX, ['NGAY','NGAY2'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA','DONGIA_LE'], ctPriceFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDM_QUAYKE, QrFB_LYDO_KKE]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add(' and KHOA in (select a.KHOA from FB_KIEMKE_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add(' and KHOA in (select a.KHOA from FB_KIEMKE_CT a, FB_DM_HH b where a.KHOA = FB_KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add(' and KHOA in (select KHOA from FB_KIEMKE_CT where KHOA = FB_KIEMKE.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    DataMain.QrDMKHO.Requery;
    QrDM_QUAYKE.Requery;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmFB_Sapthutu, FrmFB_Sapthutu);
    if FrmFB_Sapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
    with QrCT do
	    if FrmFB_CheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdImport2Execute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	if not YesNo('Thiết bị phải đang ở tình trạng gởi dữ liệu. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'ConfigZ.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Remove current data file
    s := sysTempPath + 'upload.txt';
    DeleteFile(s);

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Up.exe';
		// Data_Read <File name>,<Upload via>,<COM Port>,<Baud rate>,<Save mode>,<Add CR character>,
		// <Add LF character>,<Show error>,<Show data>,<Show dialog>,<Keep online>,<Polling time>
        Parameters := s + ',2,' + IntToStr(n) + ',1,1,1,1,0,0,0,0,2';
        Launch;
    end;
    Screen.Cursor := crDefault;

	if not FileExists(s) then
		Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;

            if FieldByName('TENVT').AsString = '' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
            //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
            //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;
    Refresh;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'Softz.log';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, sFile: string;
    i, k, n, exIndex: Integer;
    b: Boolean;
    dError, colHeaders: TStrings;
begin
    // Get file name
    sFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if sFile = '' then
    	Exit;

    //File excel
    if SameText(Copy(ExtractFileExt(sFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(sFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            colHeaders := TStringList.Create;
            colHeaders.Clear;
            colHeaders.NameValueSeparator := '=';
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  Fields.Count - 1;
                for i := 0 to n do
                begin
                    exIndex := FrmImportExcel.cbFields.Items.IndexOf(Fields[i].FieldName);
                    if exIndex >= 0  then
                    begin
                        colHeaders.Add(Fields[i].DisplayLabel + '=' + Char(exIndex + 65));
                    end;

                    if SameText(Fields[i].DisplayLabel, 'MAVT') and (sFld = '') then
                    begin
                       sFld := Fields[i].FieldName;
                    end;
                end;

                DataExcel.ExcelImportByFlexConfig(FORM_CODE, sFile, 'IMP_FB_CHUNGTU',
                 'spIMPORT_FB_CHUNGTU;1', 'MAVT', [sysLogonUID, mLCT, '', 0], colHeaders, 0);

                dError := TStringList.Create;
                with DataMain.QrTEMP do
                begin
                    SQL.Text := 'select MAVT from IMP_FB_CHUNGTU where isnull(ErrCode, '''') <> ''''';
                    Open;

                    First;
                    while not Eof do
                    begin
                        dError.Add(FieldByName('MAVT').AsString);
                        Next;
                    end;
                end;

                First;
                while not Eof do
                begin
                    if (FieldByName(sFld).AsString <> '') and (dError.IndexOf(FieldByName(sFld).AsString) = -1)   then
                    begin
                        if not QrCT.Locate('MAVT', FieldByName(sFld).AsString, []) then
                        begin
                            QrCT.Append;
                            for i := 0 to n do
                            begin
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                            end;
                        end
                        else
                            SetEditState(QrCT);
                        QrCT.CheckBrowseMode;
                    end;
                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';

procedure TFrmFB_Kiemke.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SOLUONG').AsFloat := x;

            if FieldByName('TENVT').AsString = '' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
    //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
    //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
	end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn.Enabled := (not bEmpty) and (not bDeleted);
    BtImport.Enabled := (n = 1) and (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
    LydliutfileExcel1.Enabled := bEmptyCT;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      	:= d;
		FieldByName('NGAY2').AsDateTime         := d;
		FieldByName('LCT').AsString           	:= mLCT;
		FieldByName('MAKHO').AsString       	:= sysDefKho;
        FieldByName('LOC').AsString             := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO', 'LYDO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
    DataMain.AllocSCT(mLCT, QrNX);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Detail DB
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTBeforePost(DataSet: TDataSet);
begin
	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;
        
    vlTotal1.Keep;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('DONGIA_REF2').AsFloat :=  exVNDRound(
            FieldByName('DONGIA_REF').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)
        , ctPriceRound);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DONGIA' then
                FieldByName('DONGIA2').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DONGIA2' then
                FieldByName('DONGIA').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE : Double;
begin
	exDotFBMavt(3,DataMain.QrFB_DM_HH, Sender);

    // Update referenced fields
    with QrCT do
    begin
        gia := 0;giaLE := 0;
        with DataMain.spFB_CHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters[1].Value := mLCT;
            Parameters[2].Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DONGIA').AsFloat;
                giaLE := FieldByName('DONGIA_LE').AsFloat;

                QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
                QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
                QrCT.FieldByName('DVT_BOX').AsString := FieldByName('TenDvtLon').AsString;
                QrCT.FieldByName('QD1').AsInteger := FieldByName('QuyDoi').AsInteger;
            end;
        end;

        FieldByName('DONGIA_LE').AsFloat := exVNDRound(giaLE, ctPriceRound);
        FieldByName('DONGIA_REF').AsFloat := exVNDRound(gia, ctPriceRound);
        FieldByName('DONGIA').AsFloat := exVNDRound(gia, ctPriceRound);
    end;

	GrDetail.InvalidateCurrentRow;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SOLUONG').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SOLUONG').AsFloat);
		ColumnByName('SOTIEN').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat);
        ColumnByName('SOTIEN_LE').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SOTIEN_LE').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.BtImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.OpenDetail;
begin
    Screen.Cursor := crSQLWait;
    with QrCT do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SOLUONG' then
            begin
                sl  := FieldByName('SOLUONG').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SOLUONG2').AsFloat := exVNDRound(sl2, ctQtyRound);
                FieldByName('SOLUONG2_LE').AsFloat := exVNDRound(sl2le, ctQtyRound);
            end
            else if (Sender.FullName = 'SOLUONG2') or (Sender.FullName = 'SOLUONG2_LE') then
            begin
                sl2 := FieldByName('SOLUONG2').AsFloat;
                sl2le := FieldByName('SOLUONG2_LE').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SOLUONG').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SOLUONG').AsFloat * FieldByName('DONGIA').AsFloat, ctCurRound);

        FieldByName('SOTIEN_LE').AsFloat := exVNDRound(FieldByName('SOLUONG').AsFloat
                                                * FieldByName('DONGIA_LE').AsFloat, ctCurRound);
        FieldByName('SOTIEN').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTSOTIENChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTAfterEdit(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrCTAfterInsert(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
    	FieldByName('THANHTOAN').Value := exVNDRound(FieldByName('SOTIEN').Value, ctCurRound);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Kiemke.CmdImportExcelTemplateExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_FB_CHUNGTU_EXCEL');
end;

end.
