object FrmFB_Khuyenmai3: TFrmFB_Khuyenmai3
  Left = 135
  Top = 87
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Khuy'#7871'n M'#227'i - Chi'#7871't Kh'#7845'u, '#272'a Chi'#7873'u - Nh'#224' H'#224'ng'
  ClientHeight = 730
  ClientWidth = 1008
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1008
    730)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 1008
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1008
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton3: TToolButton
      Left = 72
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object TntToolButton2: TToolButton
      Left = 144
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton1: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 224
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 232
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 304
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 312
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 384
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 392
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object TntToolButton4: TToolButton
      Left = 464
      Top = 0
      Width = 8
      Caption = 'TntToolButton4'
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 472
      Top = 0
      Cursor = 1
      Action = CmdDiemban
      ImageIndex = 12
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 544
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 6
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton10: TToolButton
      Left = 552
      Top = 0
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = ToolButton10Click
    end
    object ToolButton11: TToolButton
      Left = 639
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 9
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 647
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 682
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 632
        Width = 1000
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1000
        Height = 583
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'LYDO'#9'30'#9'L'#253' do'#9'F'
          'TUNGAY'#9'10'#9'T'#7915' ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENNGAY'#9'10'#9#272#7871'n ng'#224'y'#9'F'#9'Khuy'#7871'n m'#227'i'
          'TUGIO'#9'7'#9'T'#7915' gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'DENGIO'#9'7'#9#272#7871'n gi'#7901#9'F'#9'Khuy'#7871'n m'#227'i'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsKM
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 1000
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 1000
        inherited Panel1: TPanel
          Width = 1000
          ParentColor = False
          ExplicitWidth = 1000
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object Splitter1: TSplitter
        Left = 0
        Top = 135
        Width = 1000
        Height = 2
        Cursor = crVSplit
        Align = alTop
        ExplicitLeft = 498
        ExplicitWidth = 505
      end
      object Bevel3: TSplitter
        Left = 420
        Top = 137
        Width = 2
        Height = 495
        ExplicitLeft = 892
        ExplicitTop = 223
        ExplicitHeight = 411
      end
      object Splitter2: TSplitter
        Left = 422
        Top = 137
        Width = 578
        Height = 495
        Align = alClient
        ExplicitLeft = 598
        ExplicitTop = 260
        ExplicitWidth = 402
        ExplicitHeight = 380
      end
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 135
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 56
          Top = 14
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y l'#7853'p'
        end
        object Label2: TLabel
          Left = 291
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 7
          Top = 38
          Width = 98
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#234'n ch'#432#417'ng tr'#236'nh'
        end
        object Label5: TLabel
          Left = 30
          Top = 62
          Width = 75
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y b'#7855't '#273#7847'u'
        end
        object TntLabel2: TLabel
          Left = 264
          Top = 62
          Width = 77
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y k'#7871't th'#250'c'
        end
        object Label10: TLabel
          Left = 56
          Top = 108
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object TntLabel4: TLabel
          Left = 520
          Top = 63
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' gi'#7901
        end
        object TntLabel5: TLabel
          Left = 647
          Top = 63
          Width = 44
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7871'n gi'#7901
        end
        object Label3: TLabel
          Left = 519
          Top = 36
          Width = 121
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y k'#7871't th'#250'c th'#7921'c t'#7871
          Visible = False
        end
        object Label4: TLabel
          Left = 11
          Top = 85
          Width = 94
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y kh'#243'a phi'#7871'u'
        end
        object Label6: TLabel
          Left = 286
          Top = 85
          Width = 55
          Height = 16
          Alignment = taRightJustify
          Caption = 'L.s'#7917' kh'#243'a'
        end
        object EdSCT: TwwDBEdit
          Left = 348
          Top = 10
          Width = 159
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdLyDo: TwwDBEdit
          Left = 112
          Top = 34
          Width = 395
          Height = 22
          Ctl3D = False
          DataField = 'NOIDUNG'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTungay: TwwDBDateTimePicker
          Left = 112
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUNGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object CbDenngay: TwwDBDateTimePicker
          Left = 348
          Top = 58
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENNGAY2'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 6
        end
        object DBEdit1: TDBMemo
          Left = 111
          Top = 106
          Width = 636
          Height = 22
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
        end
        object wwDBEdit1: TwwDBDateTimePicker
          Left = 564
          Top = 58
          Width = 49
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'TUGIO'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = False
          TabOrder = 7
          UnboundDataType = wwDTEdtTime
        end
        object wwDBEdit2: TwwDBDateTimePicker
          Left = 698
          Top = 58
          Width = 49
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENGIO'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = False
          TabOrder = 8
          UnboundDataType = wwDTEdtTime
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 646
          Top = 34
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'DENNGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 4
          Visible = False
        end
        object cbNgayKhoa: TwwDBDateTimePicker
          Left = 111
          Top = 82
          Width = 163
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'CHECKED_DATE'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 9
        end
        object EdCheckedDate: TwwDBComboDlg
          Left = 348
          Top = 82
          Width = 399
          Height = 22
          TabStop = False
          OnCustomDlg = EdCheckedDateCustomDlg
          ShowButton = True
          Style = csDropDown
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHECKED_DESC'
          DataSource = DsKM
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 10
          WordWrap = False
          UnboundDataType = wwDefault
        end
        object cbbLyDo: TDbLookupComboboxEh2
          Left = 564
          Top = 10
          Width = 183
          Height = 22
          ControlLabel.Width = 30
          ControlLabel.Height = 16
          ControlLabel.Caption = 'L'#253' do'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'LYDO'
          DataSource = DsKM
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsLYDO_KM
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsLYDO_KM
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 137
        Width = 420
        Height = 495
        Align = alLeft
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: Danh s'#225'ch c'#225'c '#273#7889'i t'#432#7907'ng '#225'p d'#7909'ng'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Panel1: TPanel
          Left = 0
          Top = 16
          Width = 420
          Height = 33
          ParentCustomHint = False
          Align = alTop
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          Ctl3D = False
          DoubleBuffered = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentBackground = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          object CbbDoituong: TDbLookupComboboxEh2
            Left = 103
            Top = 7
            Width = 181
            Height = 22
            ControlLabel.Width = 56
            ControlLabel.Height = 16
            ControlLabel.Caption = #272#7889'i t'#432#7907'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'KHUYENMAI_DOITUONG'
            DataSource = DsKM
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
              end>
            DropDownBox.ListSource = DsDoituong
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA'
            ListField = 'TEN_HOTRO'
            ListSource = DsDoituong
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
        end
        object GrKMDT: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 420
          Height = 446
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          Ctl3D = True
          DataSource = DsKMDT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = popDetail
          TabOrder = 2
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
        object CbNhom: TwwDBComboBox
          Left = 148
          Top = 349
          Width = 121
          Height = 24
          ShowButton = True
          Style = csDropDown
          MapList = True
          AllowClearKey = False
          DataField = 'NHOM'
          DataSource = DsKMChitiet
          DropDownCount = 8
          ItemHeight = 0
          Sorted = False
          TabOrder = 3
          UnboundDataType = wwDefault
        end
      end
      object Status2: TStatusBar
        Left = 0
        Top = 632
        Width = 1000
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel2: TPanel
        Left = 422
        Top = 137
        Width = 578
        Height = 495
        Align = alClient
        TabOrder = 3
        object PgGiatri: TisPanel
          Left = 1
          Top = 1
          Width = 576
          Height = 200
          Align = alTop
          BevelOuter = bvNone
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          HeaderCaption = ' :: '#272'i'#7873'u ki'#7879'n khuy'#7871'n m'#227'i'
          HeaderColor = 16119285
          ImageSet = 4
          RealHeight = 0
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clBlue
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = [fsBold]
          object GrDieukien: TwwDBGrid2
            Left = 0
            Top = 49
            Width = 576
            Height = 151
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            Selected.Strings = (
              'TU'#9'10'#9'TU'#9#9
              'DEN'#9'10'#9'DEN'#9#9
              'SOTIEN'#9'10'#9'SOTIEN'#9'F'
              'TL_CK'#9'10'#9'TL_CK'#9'F')
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 1
            ShowHorzScrollBar = True
            Align = alClient
            Ctl3D = True
            DataSource = DsKMDieukien
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
            ParentCtl3D = False
            ParentFont = False
            PopupMenu = popDetailDieuKien
            TabOrder = 2
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 1
            TitleButtons = False
            UseTFields = False
            FooterColor = 13360356
            FooterCellColor = 13360356
            PadColumnStyle = pcsPadHeader
          end
          object Panel4: TPanel
            Left = 0
            Top = 16
            Width = 576
            Height = 33
            ParentCustomHint = False
            Align = alTop
            BevelOuter = bvNone
            BiDiMode = bdLeftToRight
            Ctl3D = False
            DoubleBuffered = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentBackground = False
            ParentCtl3D = False
            ParentDoubleBuffered = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            object CbbDieuKien: TDbLookupComboboxEh2
              Left = 103
              Top = 6
              Width = 181
              Height = 22
              ControlLabel.Width = 53
              ControlLabel.Height = 16
              ControlLabel.Caption = #272'i'#7873'u ki'#7879'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'KHUYENMAI_DIEUKIEN'
              DataSource = DsKM
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                end>
              DropDownBox.ListSource = DsDieukien
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA'
              ListField = 'TEN_HOTRO'
              ListSource = DsDieukien
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object CbbKhuyenMai: TDbLookupComboboxEh2
              Left = 362
              Top = 6
              Width = 181
              Height = 22
              ControlLabel.Width = 66
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Khuy'#7871'n m'#227'i'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'HTKM'
              DataSource = DsKM
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                end>
              DropDownBox.ListSource = DsKhuyenMai
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA'
              ListField = 'TEN_HOTRO'
              ListSource = DsKhuyenMai
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 1
              Visible = True
            end
          end
        end
        object PaKM: TisPanel
          Left = 1
          Top = 201
          Width = 576
          Height = 293
          Align = alClient
          BevelOuter = bvNone
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
          HeaderCaption = ' :: Khuy'#7871'n m'#227'i '#273#432#7907'c h'#432#7903'ng'
          HeaderColor = 16119285
          ImageSet = 4
          RealHeight = 0
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clBlue
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = [fsBold]
          object GrChitiet: TwwDBGrid2
            Left = 0
            Top = 16
            Width = 576
            Height = 277
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            ControlType.Strings = (
              'NHOM_STT;CustomEdit;CbNhom;F'
              'NHOM;CustomEdit;CbNhom;F')
            Selected.Strings = (
              'MAVT'#9'15'#9'M'#227' h'#224'ng'#9#9
              'TL_CK'#9'10'#9'TL_CK'#9#9
              'SOLUONG'#9'10'#9'SOLUONG'#9#9
              'GHICHU'#9'200'#9'GHICHU'#9'F'
              'RSTT'#9'10'#9'RSTT'#9'F'
              'SOTIEN'#9'10'#9'SOTIEN'#9'F'
              'TENVT'#9'200'#9'TENVT'#9'F')
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 1
            ShowHorzScrollBar = True
            Align = alClient
            Ctl3D = True
            DataSource = DsKMChitiet
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
            ParentCtl3D = False
            ParentFont = False
            PopupMenu = popDetailKhuyenMai
            TabOrder = 1
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 2
            TitleButtons = False
            UseTFields = False
            FooterColor = 13360356
            FooterCellColor = 13360356
            PadColumnStyle = pcsPadHeader
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 866
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 866
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 78
    Top = 482
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdUpdate: TAction
      Caption = 'C'#7853'p nh'#7853't'
      Hint = 'C'#7853'p nh'#7853't chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i v'#224'o danh m'#7909'c'
    end
    object CmdDiemban: TAction
      Caption = #272'i'#7875'm b'#225'n'
      Hint = 'C'#225'c kho, '#273'i'#7875'm b'#225'n c'#243' chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i'
      OnExecute = CmdDiembanExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
    end
    object CmdEmpty: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyExecute
    end
    object CmdChecked: TAction
      Caption = 'Duy'#7879't'
      OnExecute = CmdCheckedExecute
    end
    object CmdEmptyDieuKien: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyDieuKienExecute
    end
    object CmdEmptyKhuyenMai: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyKhuyenMaiExecute
    end
    object CmdDMTD: TAction
      Caption = 'Th'#7921'c '#273#417'n'
      OnExecute = CmdDMTDExecute
    end
    object CmdDMNPL: TAction
      Caption = 'Nguy'#234'n ph'#7909' li'#7879'u'
      OnExecute = CmdDMNPLExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsKM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 50
    Top = 482
  end
  object QrKM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKMBeforeOpen
    BeforeInsert = QrKMBeforeInsert
    AfterInsert = QrKMAfterInsert
    BeforeEdit = QrKMBeforeEdit
    BeforePost = QrKMBeforePost
    AfterCancel = QrKMAfterCancel
    AfterScroll = QrKMAfterScroll
    OnCalcFields = QrKMCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_KHUYENMAI'
      ' where  LCT in ('#39'FKMAI2'#39')'
      '   and  NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '')
    Left = 170
    Top = 403
    object QrKMXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#241'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrKMIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrKMNGAY: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      FieldName = 'NGAY'
    end
    object QrKMLCT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldName = 'LCT'
      Size = 2
    end
    object QrKMSCT: TWideStringField
      DisplayLabel = 'S'#232' ch'#248'ng t'#245
      FieldName = 'SCT'
    end
    object QrKMTUNGAY: TDateTimeField
      DisplayLabel = 'T'#245' ng'#181'y'
      FieldName = 'TUNGAY'
    end
    object QrKMDENNGAY: TDateTimeField
      DisplayLabel = #174#213'n ng'#181'y'
      FieldName = 'DENNGAY'
    end
    object QrKMLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrKMDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrKMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKMDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrKMDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrKMDENGIO: TDateTimeField
      FieldName = 'DENGIO'
    end
    object QrKMTUGIO: TDateTimeField
      FieldName = 'TUGIO'
    end
    object QrKMDENNGAY2: TDateTimeField
      FieldName = 'DENNGAY2'
      OnChange = QrKMDENNGAY2Change
    end
    object QrKMCHECKED: TBooleanField
      DisplayLabel = 'Duy'#7879't'
      FieldName = 'CHECKED'
    end
    object QrKMKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKMCHECKED_BY: TIntegerField
      FieldName = 'CHECKED_BY'
    end
    object QrKMCHECKED_DATE: TDateTimeField
      FieldName = 'CHECKED_DATE'
    end
    object QrKMCHECKED_DESC: TWideMemoField
      FieldName = 'CHECKED_DESC'
      BlobType = ftWideMemo
    end
    object QrKMNOIDUNG: TWideStringField
      FieldName = 'NOIDUNG'
      Size = 200
    end
    object QrKMHTKM: TWideStringField
      FieldName = 'HTKM'
      OnChange = QrKMHTKMChange
    end
    object QrKMKHUYENMAI_DOITUONG: TWideStringField
      FieldName = 'KHUYENMAI_DOITUONG'
      OnChange = QrKMKHUYENMAI_DOITUONGChange
    end
    object QrKMKHUYENMAI_DIEUKIEN: TWideStringField
      FieldName = 'KHUYENMAI_DIEUKIEN'
      OnChange = QrKMKHUYENMAI_DIEUKIENChange
    end
    object QrKMLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_KM
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Lookup = True
    end
  end
  object DsKM: TDataSource
    DataSet = QrKM
    Left = 170
    Top = 431
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 134
    Top = 482
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 190
    Top = 482
  end
  object Filter2: TwwFilterDialog2
    SortBy = fdSortByFieldNo
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 106
    Top = 482
  end
  object QrKhuyenMai: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * from V_HTKM')
    Left = 104
    Top = 356
  end
  object QrKMDieukien: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrKMDieukienBeforeOpen
    BeforeInsert = QrKMDTBeforeInsert
    AfterInsert = QrKMDieukienAfterInsert
    BeforeEdit = QrKMDTBeforeEdit
    BeforePost = QrKMDieukienBeforePost
    BeforeDelete = QrKMDieukienBeforeDelete
    AfterScroll = QrKMDieukienAfterScroll
    OnCalcFields = QrKMDieukienCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_KHUYENMAI_DIEUKIEN'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 231
    Top = 403
    object QrKMDieukienRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrKMDieukienTU: TFloatField
      FieldName = 'TU'
    end
    object QrKMDieukienDEN: TFloatField
      FieldName = 'DEN'
    end
    object QrKMDieukienKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrKMDieukienKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMDieukienSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrKMDieukienGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrKMDieukienLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKMDieukienTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrKMDieukienSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
  end
  object DsKMDieukien: TDataSource
    DataSet = QrKMDieukien
    Left = 231
    Top = 431
  end
  object QrKMChitiet: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltBatchOptimistic
    BeforeOpen = QrKMChitietBeforeOpen
    BeforeInsert = QrKMChitietBeforeInsert
    AfterInsert = QrKMChitietAfterInsert
    BeforeEdit = QrKMDTBeforeEdit
    BeforePost = QrKMChitietBeforePost
    BeforeDelete = QrKMChitietBeforeDelete
    OnCalcFields = QrKMChitietCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_KHUYENMAI_CT'
      ' where KHOA= :KHOA')
    Left = 263
    Top = 403
    object QrKMChitietRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrKMChitietTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrKMChitietSOLUONG: TFloatField
      FieldName = 'SOLUONG'
      OnChange = QrKMChitietSOLUONGChange
    end
    object QrKMChitietMAVT: TWideStringField
      FieldName = 'MAVT'
      OnChange = QrKMChitietMAVTChange
      OnValidate = QrKMChitietMAVTValidate
      Size = 30
    end
    object QrKMChitietTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrKMChitietSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrKMChitietGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrKMChitietLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrFB_DM_THUCDON
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaDvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrKMChitietKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrKMChitietKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMChitietKHOA_DIEUKIEN: TGuidField
      FieldName = 'KHOA_DIEUKIEN'
      FixedChar = True
      Size = 38
    end
    object QrKMChitietSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrKMChitietLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKMChitietGIABAN: TFloatField
      FieldName = 'GIABAN'
    end
    object QrKMChitietNHOM: TIntegerField
      FieldName = 'NHOM'
    end
  end
  object DsKMChitiet: TDataSource
    DataSet = QrKMChitiet
    Left = 263
    Top = 431
  end
  object popDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 162
    Top = 482
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
  object QrDoituong: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * from V_KHUYENMAI_DOITUONG')
    Left = 40
    Top = 356
  end
  object QrDieukien: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * from V_KHUYENMAI_DIEUKIEN')
    Left = 72
    Top = 356
  end
  object QrNganh: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MANGANH, TENNGANH'
      'from DM_NGANH '
      'where LOAI='#39'FBTP'#39
      'order by MANGANH')
    Left = 296
    Top = 504
  end
  object QrNhom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from DM_NHOM'
      'where 1 = 1'
      
        'and MANGANH in (select MANGANH from DM_NGANH where LOAI = '#39'FBTP'#39 +
        ' )'
      'order by MANHOM')
    Left = 352
    Top = 504
  end
  object QrKMDT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrKMDTBeforeOpen
    BeforeInsert = QrKMDTBeforeInsert
    AfterInsert = QrKMDTAfterInsert
    BeforeEdit = QrKMDTBeforeEdit
    BeforePost = QrKMDTBeforePost
    BeforeDelete = QrKMDTBeforeDelete
    OnCalcFields = QrKMDTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_KHUYENMAI_DOITUONG'
      ' where KHOA= :KHOA')
    Left = 199
    Top = 403
    object IntegerField1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrKMDTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrKMDTMAVT: TWideStringField
      FieldName = 'MAVT'
      OnChange = QrKMDTMAVTChange
      Size = 30
    end
    object QrKMDTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrKMDTMANGANH: TWideStringField
      FieldName = 'MANGANH'
      OnChange = QrKMDTMANGANHChange
      Size = 5
    end
    object QrKMDTMANHOM: TWideStringField
      FieldName = 'MANHOM'
      OnChange = QrKMDTMANHOMChange
      Size = 10
    end
    object QrKMDTMADT: TWideStringField
      FieldName = 'MADT'
      OnChange = QrKMDTMADTChange
    end
    object QrKMDTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrKMDTLK_TENDT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Lookup = True
    end
    object QrKMDTLK_TENNGANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNGANH'
      LookupDataSet = QrNganh
      LookupKeyFields = 'MANGANH'
      LookupResultField = 'TENNGANH'
      KeyFields = 'MANGANH'
      Lookup = True
    end
    object QrKMDTLK_TENNHOM: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNHOM'
      LookupDataSet = QrNhom
      LookupKeyFields = 'MANHOM'
      LookupResultField = 'TENNHOM'
      KeyFields = 'MANHOM'
      Lookup = True
    end
    object QrKMDTLK_PREFIX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PREFIX'
      LookupDataSet = QrNhom
      LookupKeyFields = 'MANHOM'
      LookupResultField = 'PREFIX'
      KeyFields = 'MANHOM'
      Lookup = True
    end
    object QrKMDTLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrFB_DM_THUCDON
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaDvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrKMDTLK_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TINHTRANG'
      LookupDataSet = DataMain.QrFB_DM_THUCDON
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TINHTRANG'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrKMDTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrKMDTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMDTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKMDTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = DataMain.QrFB_DM_THUCDON
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN'
      KeyFields = 'MAVT'
      Lookup = True
    end
  end
  object DsKMDT: TDataSource
    DataSet = QrKMDT
    Left = 199
    Top = 431
  end
  object MemoDate: TwwMemoDialog
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -16
    Font.Name = 'Courier'
    Font.Style = []
    Caption = 'Danh S'#225'ch C'#225'c L'#7847'n Kh'#243'a Phi'#7871'u'
    DlgLeft = 0
    DlgTop = 0
    DlgWidth = 561
    DlgHeight = 396
    OnInitDialog = MemoDateInitDialog
    Left = 320
    Top = 404
  end
  object popDetailDieuKien: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 218
    Top = 482
    object MenuItem1: TMenuItem
      Action = CmdEmptyDieuKien
    end
  end
  object popDetailKhuyenMai: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 250
    Top = 482
    object MenuItem2: TMenuItem
      Action = CmdEmptyKhuyenMai
    end
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 280
    Top = 480
    object NguynphliuFB1: TMenuItem
      Action = CmdDMNPL
      ImageIndex = 42
    end
    object CmdNPL: TMenuItem
      Tag = 1
      Action = CmdDMTD
      ImageIndex = 42
    end
  end
  object DsDoituong: TDataSource
    DataSet = QrDoituong
    Left = 42
    Top = 399
  end
  object DsDieukien: TDataSource
    DataSet = QrDieukien
    Left = 74
    Top = 399
  end
  object DsKhuyenMai: TDataSource
    DataSet = QrKhuyenMai
    Left = 106
    Top = 400
  end
end
