﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_CayNganhNhom;

interface

uses
  Classes, Controls, Forms,
  Db, ADODB, fcdbtreeview, Buttons, StdCtrls;

type
  TFrmFB_CayNganhNhom = class(TForm)
    Tree: TfcDBTreeView;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNHOM: TDataSource;
    DsNGANH: TDataSource;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    procedure TreeDblClick(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure TntFormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReturnClick(Sender: TObject);
    procedure TreeCalcNodeAttributes(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode);
  public
  	function Execute(var sNganh, sNhom: String;  sLoai: String = 'FBTP'): Boolean;
  end;

var
  FrmFB_CayNganhNhom: TFrmFB_CayNganhNhom;

implementation

uses
    isDb, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_CayNganhNhom.Execute;
begin

    with QrNGANH do
    begin
        if Active then
           Close;
        Parameters[0].Value := sLoai;
        Open;
    end;

    OpenDataSets([QrNGANH, QrNHOM]);
    QrNGANH.Locate('MANGANH', sNganh, []);
    QrNHOM.Locate('MANHOM', sNhom, []);

    Tree.MakeActiveDataSet(QrNHOM, False);

	Result := ShowModal = mrOK;
    if Result then
    begin
	    sNganh := QrNGANH.FieldByName('MANGANH').AsString;
        sNhom  := QrNHOM.FieldByName('MANHOM').AsString;
	end;
	CloseDataSets([QrNGANH, QrNHOM]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CayNganhNhom.TreeDblClick(TreeView: TfcDBCustomTreeView;
  Node: TfcDBTreeNode; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
	CmdReturnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CayNganhNhom.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CayNganhNhom.TntFormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)      
procedure TFrmFB_CayNganhNhom.CmdReturnClick(Sender: TObject);
begin
    if Tree.ActiveNode <> Nil then
    	if Tree.ActiveNode.Level > 0 then
        begin
			ModalResult := mrOK;
            Exit;
        end;
    Tree.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_CayNganhNhom.TreeCalcNodeAttributes(
  TreeView: TfcDBCustomTreeView; Node: TfcDBTreeNode);
begin
	with Node do
        ImageIndex := Level
end;

end.

