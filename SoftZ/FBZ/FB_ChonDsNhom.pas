﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonDsNhom;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg;

type
  TFrmFB_ChonDsNhom = class(TForm)
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    RgLoai: TRadioGroup;
    paNhom: TPanel;
    CbMANGANH: TwwDBLookupCombo;
    CbMANHOM: TwwDBLookupCombo;
    CbNHOM: TwwDBLookupCombo;
    CbNGANH: TwwDBLookupCombo;
    QrNHOM2: TADOQuery;
    Label2: TLabel;
    Label3: TLabel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    btnAdd: TBitBtn;
    btnRemove: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    lbNhom2: TLabel;
    CbMANHOM2: TwwDBLookupCombo;
    CbNHOM2: TwwDBLookupCombo;
    DsNHOM: TDataSource;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbMANGANHExit(Sender: TObject);
    procedure EdMAVTInitDialog(Dialog: TwwLookupDlg);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmFB_ChonDsNhom: TFrmFB_ChonDsNhom;

implementation

uses
	isDb, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormShow(Sender: TObject);
var
    n, i: Integer;
begin
    TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrNGANH, QrNHOM, QrNHOM2]);
	mTrigger := False;
	CbMANGANH.LookupValue := QrNGANH.FieldByName('MANGANH').AsString;
	CbMANHOM.LookupValue := QrNHOM.FieldByName('MANHOM').AsString;
    CbMANHOM2.LookupValue := QrNHOM2.FieldByName('MANHOM2').AsString;

    n := FlexConfigInteger('FB_DM_HH_NGANH', 'Depth') + 1;
    with RgLoai do
    begin
        for i := Items.Count downto n do
            Items.Delete(i-1);

        Columns := Items.Count;
    end;

    lbNhom2.Visible := n > 3;
    CbMANHOM2.Visible := n > 3;
    CbNHOM2.Visible := n > 3;

    i := Iif(n > 3, 0, 28);

    paNhom.Height := 105 - i;
    btnAdd.Top := 168 - i;
    btnRemove.Top := 168 - i;

    GrList.Top := 200 - i;
    GrList.Height := 103 + i;

    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonDsNhom.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    CbMANHOM.Enabled := n > 0;
    CbNHOM.Enabled := n > 0;

    CbNHOM2.Enabled := n > 1;
    CbMANHOM2.Enabled := n > 1;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CbMANGANHExit(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).Text = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    0:		// Ma nganh
    	begin
		    CbNGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;

            s := CbNHOM2.LookupValue;
            with QrNHOM2 do
	            if (s <> '') and not Locate('MANHOM2', s, []) then
    	        begin
                	First;
					CbMANHOM2.LookupValue := FieldByName('MANHOM2').AsString;
					CbNHOM2.LookupValue := FieldByName('MANHOM2').AsString;
	            end;
        end;
    1:		// Ten nganh
    	begin
			CbMANGANH.LookupValue := s;
            QrNGANH.Locate('MANGANH', s, []);

            s := CbNHOM.LookupValue;
            with QrNHOM do
	            if (s <> '') and not Locate('MANHOM', s, []) then
    	        begin
                	First;
					CbMANHOM.LookupValue := FieldByName('MANHOM').AsString;
					CbNHOM.LookupValue := FieldByName('MANHOM').AsString;
	            end;

            s := CbNHOM2.LookupValue;
            with QrNHOM2 do
	            if (s <> '') and not Locate('MANHOM2', s, []) then
    	        begin
                	First;
					CbMANHOM2.LookupValue := FieldByName('MANHOM2').AsString;
					CbNHOM2.LookupValue := FieldByName('MANHOM2').AsString;
	            end;
        end;
    2:		// Ma nhom
    	begin
			CbNHOM.LookupValue := s;
            QrNHOM.Locate('MANHOM', s, []);

            s := CbNHOM2.LookupValue;
            with QrNHOM2 do
	            if (s <> '') and not Locate('MANHOM2', s, []) then
    	        begin
                	First;
					CbMANHOM2.LookupValue := FieldByName('MANHOM2').AsString;
					CbNHOM2.LookupValue := FieldByName('MANHOM2').AsString;
	            end;
    	end;
    3:		// Ten nhom
    	begin
			CbMANHOM.LookupValue := s;
            QrNHOM.Locate('MANHOM', s, []);

            s := CbNHOM2.LookupValue;
            with QrNHOM2 do
	            if (s <> '') and not Locate('MANHOM2', s, []) then
    	        begin
                	First;
					CbMANHOM2.LookupValue := FieldByName('MANHOM2').AsString;
					CbNHOM2.LookupValue := FieldByName('MANHOM2').AsString;
	            end;
        end;
    4:
    	CbNHOM2.LookupValue := s;
    5:
    	CbMANHOM2.LookupValue := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.EdMAVTInitDialog(Dialog: TwwLookupDlg);
begin
	InitSearchDialog(Dialog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbMANGANH.SetFocus;
        1:
            CbMANHOM.SetFocus;
        2:
            CbMANHOM2.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
        	s1 := CbMANGANH.LookupValue;
        	s2 := CbNGANH.DisplayValue;
        end;
    1:
    	begin
        	s1 := CbMANHOM.LookupValue;
        	s2 := CbNHOM.DisplayValue;
        end;
    2:
    	begin
        	s1 := CbMANHOM2.LookupValue;
            s2 := CbNHOM2.DisplayValue;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;

//        EdMAVT.Text := '';
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := CbMANGANH.LookupValue;
    1:
       	s := CbMANHOM.LookupValue;
    2:
       	s := CbMANHOM2.LookupValue;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrNGANH, QrNHOM, QrNHOM2]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
