﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Sapthutu;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, Db, ADODB, Buttons,
  Wwdbigrd, Wwdbgrid2, ActnList, kbmMemTable, Grids, Wwdbgrid;

type
  TFrmFB_Sapthutu = class(TForm)
    BtnOK: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    GrList: TwwDBGrid2;
    BitBtn1: TSpeedButton;
    BitBtn2: TSpeedButton;
    GrListSearchIButton: TwwIButton;
    DsTemp: TDataSource;
    QrTemp: TkbmMemTable;
    QrTempMAVT: TWideStringField;
    QrTempTENVT: TWideStringField;
    QrTempSTT: TIntegerField;
    QrTempRSTT: TIntegerField;
    Label1: TLabel;
    EdNum: TEdit;
    CmdUp: TAction;
    CmdDown: TAction;
    CmdMove: TAction;
    SpeedButton1: TSpeedButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdUpExecute(Sender: TObject);
    procedure CmdDownExecute(Sender: TObject);
    procedure CmdMoveExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure BtnCancelClick(Sender: TObject);
  private
	srcBuf, dstBuf: array[0..2] of Variant;
	procedure RecRead(var buf: array of Variant);
	procedure RecWrite(var buf: array of Variant);
  public
  	function  Execute(Dataset: TCustomADODataSet): Boolean;
  end;

var
  FrmFB_Sapthutu: TFrmFB_Sapthutu;

implementation

uses
	isLib, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.RecRead(var buf: array of Variant);
var
	i: Integer;
begin
	with QrTemp do
		for i := 0 to Length(buf) - 1 do
        	buf[i] := Fields[i + 1].Value
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.RecWrite(var buf: array of Variant);
var
	i: Integer;
begin
    with QrTemp do
    begin
    	Edit;
		for i := 0 to Length(buf) - 1 do
        	Fields[i + 1].Value := buf[i];
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.ActionUpdate(Action: TBasicAction; var Handled: Boolean);
begin
    CmdUp.Enabled := not QrTemp.Bof;
    CmdDown.Enabled := not QrTemp.Eof;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.BtnCancelClick(Sender: TObject);
begin
	if QrTemp.IsDataModified then
    	if not DiscardConfirm then
        	Exit;
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.CmdDownExecute(Sender: TObject);
begin
	with QrTemp do
    begin
        DisableControls;
        RecRead(srcBuf);
        if MoveBy(1) <> 0 then
        begin
            RecRead(dstBuf);
            RecWrite(srcBuf);
            MoveBy(-1);
            RecWrite(dstBuf);
            MoveBy(1);
        end;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.CmdMoveExecute(Sender: TObject);
var
	si, di: Integer;
begin
	di := StrToIntDef(EdNum.Text, -1);
    if di < 1 then	// Invalid new position
    begin
    	Beep;
        EdNum.SetFocus;
        Exit;
    end;

	with QrTemp do
    begin
        si := Fields[0].AsInteger;

        // Invalid new position
    	if (di > RecordCount) or (si = di) then
        begin
            Beep;
	        EdNum.SetFocus;
            Exit;
        end;

        DisableControls;
        RecRead(srcBuf);
        Delete;

        // Đánh số lại
        if si < di then
        begin
			while si < di  do
    	    begin
	            Edit;
    	        Fields[0].AsInteger := si;
        	    MoveBy(1);
            	Inc(si);
	        end;
	        if di = RecordCount + 1 then
    	        Append
        	else
            	Insert;
        end
        else
        begin
			while si > di  do
	        begin
                if si = RecordCount + 1 then
                else
                    MoveBy(-1);
    	        Edit;
        	    Fields[0].AsInteger := si;
            	Dec(si);
	        end;
            Insert;
        end;

        Fields[0].AsInteger := di;
        RecWrite(srcBuf);

        EnableControls;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.CmdUpExecute(Sender: TObject);
begin
	with QrTemp do
    begin
        DisableControls;
        RecRead(srcBuf);
        if MoveBy(-1) <> 0 then
        begin
	        RecRead(dstBuf);
    	    RecWrite(srcBuf);
        	MoveBy(1);
	        RecWrite(dstBuf);
    	    MoveBy(-1);
        end;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Sapthutu.Execute(Dataset: TCustomADODataSet): Boolean;
var
	bm: TBytes;
    rstt, stt: Integer;
    ma, ten: String;
begin
	// Copy to memory table
	QrTemp.Open;
    with DataSet do
    begin
    	bm := BookMark;
        rstt := FieldByName('STT').AsInteger;
        DisableControls;
        First;
        while not Eof do
        begin
        	stt := FieldByName('STT').AsInteger;
            ma := FieldByName('MAVT').AsString;
            ten := FieldByName('TENVT').AsString;

        	with QrTemp do
            begin
            	Append;
            	Fields[0].AsInteger := stt;
            	Fields[1].AsInteger := stt;
	            Fields[2].AsString := ma;
    	        Fields[3].AsString := ten;
            end;

            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

    with QrTemp do
    begin
    	Locate('RSTT', rstt, []);
        IsDataModified := False;
	end;

    // Show dialog for reordering
	Result := ShowModal = mrOK;

    // Update from memory table
    if Result then
    begin
	    with QrTemp do
    	begin
        	DisableControls;
            First;
            while not Eof do
            begin
            	rstt := Fields[0].AsInteger;
                stt := Fields[1].AsInteger;

            	// Có bị đổi thứ tự
            	if rstt <> stt then
                	with DataSet do
                    begin
                        First;
                        MoveBy(stt - 1);
                        Edit;
                        FieldByName('STT').AsInteger := rstt;
                	end;
                Next;
            end;
		end;
        DataSet.CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Sapthutu.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;
end;

end.
