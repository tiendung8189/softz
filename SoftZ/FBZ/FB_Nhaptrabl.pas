﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Nhaptrabl;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, isDb,
  frameNavi,
  frameKho, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, wwdbedit, Wwdotdot,
  Wwdbcomb, kbmMemTable, Vcl.Buttons, wwcheckbox, frameNgay, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh;

type
  TFrmFB_Nhaptrabl = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label10: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrDMVT: TADOQuery;
    QrCTRSTT: TIntegerField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    GrDetail: TwwDBGrid2;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdDetail: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXSOTIEN: TFloatField;
    QrNXCREATE_BY: TIntegerField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_BY: TIntegerField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXIMG: TIntegerField;
    QrNXXOA: TWideStringField;
    QrCTSTT: TIntegerField;
    DBEdit1: TDBMemo;
    Panel2: TPanel;
    vlTotal: TisTotal;
    Bevel1: TBevel;
    frNavi: TfrNavi;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    QrNXSOLUONG: TFloatField;
    QrCTSOLUONG2: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrNXLK_THUNGAN: TWideStringField;
    QrDMQUAY: TADOQuery;
    CmdUpdateDetail: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    CmdDelDetail: TAction;
    N3: TMenuItem;
    Xachitit1: TMenuItem;
    CmdUpdateQty: TAction;
    N4: TMenuItem;
    Cpnhtchititmthng1: TMenuItem;
    QrUSER: TADOQuery;
    QrNXSCT2: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXCHIETKHAU_MH: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXLK_TENKHO: TWideStringField;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrNXQUAY: TWideStringField;
    QrNXLK_QUAY: TWideStringField;
    QrNXLCT: TWideStringField;
    CmdAudit: TAction;
    QrNXDGIAI: TWideMemoField;
    ImgTotal: TImage;
    DBText2: TDBText;
    QrCTGHICHU: TWideStringField;
    QrCTTHUE_SUAT: TFloatField;
    CmdListRefesh: TAction;
    frDate: TfrNGAY;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    cxGroupBox1: TGroupBox;
    QrCTLOAITHUE: TWideStringField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLOC: TWideStringField;
    QrNXMAVIP: TWideStringField;
    QrCTTL_CK_MAX: TFloatField;
    QrCTTL_CK_THEM: TFloatField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    Sepchecked: TToolButton;
    QrNXIMG2: TIntegerField;
    spCHECK_BILL_FB_BANLE: TADOStoredProc;
    QrNX_id: TLargeintField;
    QrCTTENVT2: TWideStringField;
    Label3: TLabel;
    EdSoBillMaVach: TwwDBEdit;
    kbmTemp: TkbmMemTable;
    DsTemp: TDataSource;
    kbmTempS1: TWideStringField;
    Label7: TLabel;
    Label8: TLabel;
    DaBillNgay: TwwDBDateTimePicker;
    EdBill: TwwDBEdit;
    GroupBox1: TGroupBox;
    QrNXNGAY2: TDateTimeField;
    QrCTTL_CKMH: TFloatField;
    QrCTTL_CKHD: TFloatField;
    QrCTTL_CK_VIPNHOM: TFloatField;
    QrCTTL_CK_VIPDM: TFloatField;
    QrCTTL_CK_PHIEU: TFloatField;
    spCHECK_BILL_USED: TADOStoredProc;
    QrNXTHUCTRA: TFloatField;
    QrNXCO_HOADON: TBooleanField;
    QrNXTHUNGAN: TIntegerField;
    QrNXPHUTHU: TFloatField;
    QrNXSOTIEN_SAU_CKMH: TFloatField;
    QrNXTHANHTIEN: TFloatField;
    QrNXHINHTHUC_GIA: TWideStringField;
    QrNXMABAN: TWideStringField;
    Label4: TLabel;
    Label5: TLabel;
    EdBillCa: TwwDBEdit;
    EdBillBan: TwwDBEdit;
    QrNXCA: TWideStringField;
    BtRemoveBill: TSpeedButton;
    CmdCancelBill: TAction;
    QrNXCHECKED: TBooleanField;
    QrNXLK_USERNAME: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTBO: TBooleanField;
    QrCTCHIETKHAU_MH: TFloatField;
    QrCTSOTIEN_SAU_CKMH: TFloatField;
    QrCTTL_PHUTHU: TFloatField;
    QrCTPHUTHU: TFloatField;
    QrCTTHANHTIEN: TFloatField;
    QrCTDONGIA_SAU_CK: TFloatField;
    QrCTTHANHTOAN: TFloatField;
    QrNXCALC_TIEN_THUE: TFloatField;
    LbHH2: TLabel;
    EdHH1: TwwDBEdit;
    Label18: TLabel;
    EdCK: TwwDBEdit;
    Label9: TLabel;
    wwDBEdit7: TwwDBEdit;
    Label11: TLabel;
    wwDBEdit10: TwwDBEdit;
    Label12: TLabel;
    wwDBEdit11: TwwDBEdit;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    Label24: TLabel;
    EdTriGiaTT: TwwDBEdit;
    cbCoHoaDon: TwwCheckBox;
    QrCTTENTAT: TWideStringField;
    QrCTTHANHTIEN_CHUA_CL: TFloatField;
    QrCTTHANHTIEN_CL: TFloatField;
    QrCTTHANHTIEN_CHUA_VAT: TFloatField;
    QrCTDONGIA_CHUA_VAT: TFloatField;
    QrCTTIEN_THUE_5: TFloatField;
    QrCTTIEN_THUE_10: TFloatField;
    QrCTTIEN_THUE_OR: TFloatField;
    QrCTCKHD_BY: TIntegerField;
    QrCTTRA_BY: TIntegerField;
    QrCTTRA_DATE: TDateTimeField;
    QrNXCALC_THUCTRA: TFloatField;
    QrCTCALC_THUE_SUAT: TFloatField;
    QrCTCALC_TIEN_THUE: TFloatField;
    QrCTCALC_THUCTRA: TFloatField;
    QrNXTIEN_THUE: TFloatField;
    QrNXPTNX: TWideStringField;
    QrNXMAQUAY: TWideStringField;
    QrNXTL_PHUTHU: TFloatField;
    QrNXMAVIP_HOTEN: TWideStringField;
    QrNXTL_CKHD: TFloatField;
    QrNXTL_CKVIPDM: TFloatField;
    QrNXCKHD_BY: TIntegerField;
    QrNXTHANHTIEN_CHUA_CL: TFloatField;
    QrNXTHANHTIEN_CL: TFloatField;
    QrNXTHANHTIEN_CHUA_VAT: TFloatField;
    QrNXTIEN_THUE_5: TFloatField;
    QrNXTIEN_THUE_10: TFloatField;
    QrNXTIEN_THUE_OR: TFloatField;
    QrNXMADT: TWideStringField;
    QrCT_id: TLargeintField;
    QrNXTIEN_THUE_CHUA_CL: TFloatField;
    spFB_BANLE_Select_Full: TADOStoredProc;
    EdKho: TDBEditEh;
    cbbKho: TDbLookupComboboxEh2;
    QrDMKHO: TADOQuery;
    DsDMKHO: TDataSource;
    cbbThuNgan: TDbLookupComboboxEh2;
    EdThuNgan: TDBEditEh;
    DsUSER: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure CmdUpdateDetailExecute(Sender: TObject);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure CmdUpdateQtyExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrCTMAVTChange(Sender: TField);
    procedure kbmTempS1Change(Sender: TField);
    procedure CmdCancelBillExecute(Sender: TObject);
    procedure QrCTSOTIENChange(Sender: TField);
    procedure QrCTTHANHTIENChange(Sender: TField);
    procedure QrCTTHANHTOANChange(Sender: TField);
    procedure QrNXTHANHTOANChange(Sender: TField);
    procedure QrCTDONGIAValidate(Sender: TField);
    procedure QrNXCO_HOADONChange(Sender: TField);
    procedure QrNXTIEN_THUE_CHUA_CLChange(Sender: TField);
    procedure cbbKhoDropDown(Sender: TObject);
    procedure cbbKhoExit(Sender: TObject);
    procedure cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
  private
    mLCT: String;
	mCanEdit, mReadOnly, mDeleteAll: Boolean;
   	fTungay, fDenngay: TDateTime;
    fKho: String;
    mKhoa: TGUID;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
    function checkBillUsed(pAbort: Boolean = True): Boolean;
    function clearBillInfo(): Boolean;
  end;

var
  FrmFB_Nhaptrabl: TFrmFB_Nhaptrabl;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    FB_ChonDsma, isLib, FastReport;

{$R *.DFM}

const
	FORM_CODE = 'FB_NHAPTRA_BANLE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.Execute;
begin
    mLCT := 'FNTBL';
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
//    ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    // Initial
    fType := 3;
    fStr := '';
    fSQL := QrNX.SQL.Text;

    mKhoa := TGuidEx.EmptyGuid;
  	mTrigger := False;
    mDeleteAll := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.FormShow(Sender: TObject);
begin
    InitFmtAndRoud(mLCT);
    ctCurRound := 0;
    ctCurVatRound := -2;
    ctPriceRound := 0;
    ctPriceNonVATRound := -2;
    ctQtyRound := -1;
    ctCurVatFmt := '#,##0.00;-#,##0.00;#';

	// Open Database
    OpenDataSets([QrDMVT, QrDMQUAY, QrDMKHO, DataMain.QrDMKHO]);
    frDate.Init(Date - sysLateDay, Date);

    CbNGAY.ReadOnly := not sysIsCentral;

    SetDisplayFormat(QrDMVT, ctCurFmt);

    with QrNX do
    begin
    	SetDisplayFormat(QrNX, sysCurFmt);
	    SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
        SetDisplayFormat(QrCT, ['SOLUONG2', 'SOLUONG'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA'], ctPriceFmt);
        SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);
        SetDisplayFormat(QrCT, ['TIEN_THUE'], ctCurVatFmt);
    end;

    // Customize
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	try
	    CloseDataSets([QrDMVT, QrCT, QrNX]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        
        with QrUSER do
        begin
        	Close;
            Open;
        end;

        with QrCT do
        begin
            Close;
            Open;
        end;

        with kbmTemp do
        begin
            if Active then
                Close;

            Open;
            Append;
        end;

        Screen.Cursor := crDefault;
        mKhoa := TGuidEx.EmptyGuid;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
        GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
//			if fKho <> '' then
//            	SQL.Add(' and MAKHO = ''' + fKho + '''');
            //  Loc theo mat hang
            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_TRAHANG_COMBO a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_TRAHANG_COMBO a, FB_DM_HH b where a.KHOA = FB_TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_TRAHANG_COMBO where KHOA = FB_TRAHANG.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMVT.Requery;
    QrDMQUAY.Requery;
    QrUSER.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdNewExecute(Sender: TObject);
begin
    //mTrigger := False;
	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdSaveExecute(Sender: TObject);
var
    bTrigger: Boolean;
begin
	QrCT.CheckBrowseMode;
    // Xoa cac dong thua
    bTrigger := mTrigger;
	mTrigger := True;
    DeleteConfirm(False);
    with QrCT do
    begin
        DisableControls;
        First;
        while not Eof do
            if FieldByName('SOLUONG').AsFloat = 0 then
                Delete
            else
                Next;
        First;
        EnableControls;
    end;
    DeleteConfirm(True);

    mTrigger := bTrigger;
//
    with QrNX do
    begin
        if (FieldByName('SCT2').AsString <> '') and QrCT.IsEmpty then
        begin
            clearBillInfo;
        end;
    end;
    QrNX.Post;
    // Đánh số lại và lưu
    exSaveDetails(QrCT);
    DataMain.UpdateAfterPostPhieu(TGuidField(QrNX.FieldByName('KHOA')).AsGuid, mLCT);
    exReSyncRecord(QrNX);
    QrNXAfterScroll(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdCancelBillExecute(Sender: TObject);
begin
   	if YesNo(RS_CONFIRM_DELETED_BILL, 1) then
    begin
        clearBillInfo;
        exEmptyDetails(QrCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdDelExecute(Sender: TObject);
begin
    checkBillUsed;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
    begin
        clearBillInfo;
       	MarkDataSet(QrNX);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	FrmFastReport.ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdTotalExecute(Sender: TObject);
var
    canDoi, soLuong, donGiaChuaVAT, thanhTienChuaChenhLech, thanhTienChuaVAT: Double;
begin
    canDoi := 0;
    with QrCT do
    begin
        First;
        if FieldByName('THANHTIEN_CL').AsFloat <> 0 then
        begin
            Edit;
            FieldByName('THANHTIEN_CL').AsFloat := 0;
            CheckBrowseMode;
        end;
    end;

    with QrNX do
    begin
        if (FieldByName('THANHTOAN').AsFloat <> 0) then
        begin
            if FieldByName('HINHTHUC_GIA').AsString = '03' then
                canDoi := FieldByName('THANHTOAN').AsFloat
                            - FieldByName('TIEN_THUE').AsFloat
                            - FieldByName('THANHTIEN_CHUA_VAT').AsFloat
            else
                canDoi := FieldByName('THANHTIEN_CHUA_VAT').AsFloat
                            + FieldByName('TIEN_THUE').AsFloat
                            - FieldByName('THANHTOAN').AsFloat
        end;
    end;
    if canDoi <> 0 then
    with QrCT do
    begin
        First;
        Edit;
        FieldByName('THANHTIEN_CL').AsFloat := exVNDRound(candoi, ctCurRound);
        CheckBrowseMode;
    end;
	vlTotal.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDelete: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDelete := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and not bDelete;
    CmdDel.Caption := GetMarkCaption(QrNX);

    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    CmdDetail.Enabled := not bEmpty;

    CmdPrint.Enabled  := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    //CmdSearch.Enabled := bBrowse and (n = 0);
    CmdFilter.Enabled := bBrowse and (n = 0);
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    CmdUpdateDetail.Enabled := mCanEdit and (not mReadOnly);

    bEmpty := QrCT.IsEmpty;
    CmdDelDetail.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);
    CmdUpdateQty.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);
    BtRemoveBill.Enabled := QrNX.FieldByName('SCT2').AsString <> '';
    cbCoHoaDon.ReadOnly := QrNX.FieldByName('SCT2').AsString <> '';
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

	(*
    ** Master events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXAfterInsert(DataSet: TDataSet);
var
	mKho: String;
begin
//	mKho := iIf(frKHO.CbMaKho.LookupValue = '', sysDefKho, frKHO.CbMaKho.LookupValue);
    mKho := sysDefKho;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('_id').Value := NewIntID;
		FieldByName('NGAY').AsDateTime := Now;
		FieldByName('LCT').AsString := mLCT;
        FieldByName('MAKHO').AsString := mKho;
        FieldByName('LOC').AsString  := sysLoc;
        FieldByName('THUNGAN').AsInteger := sysLogonUID;
        FieldByName('HINHTHUC_GIA').AsString := sysHTGiaBL;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DETAIL_NOT_EMPTY = 'Phải nhập số lượng trả.';
procedure TFrmFB_Nhaptrabl.QrNXBeforePost(DataSet: TDataSet);
begin
    with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'THUNGAN']) then
	    	Abort;

	    exValidClosing(FieldByName('NGAY').AsDateTime);
        SetNull(QrNX, ['MAVIP']);
    end;

    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
	if mTrigger then
    	Exit;

    checkBillUsed;
    exIsChecked(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}

     with QrNX do
     begin
        if FieldByName('CO_HOADON').AsBoolean then
        begin
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTOAN').AsFloat
        end
        else
        begin
            FieldByName('CALC_TIEN_THUE').AsFloat := 0;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTIEN').AsFloat
        end;
     end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXCO_HOADONChange(Sender: TField);
begin
    if mTrigger then
        Exit;

//    QrCT.Refresh;
    with QrCT do
    begin
        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin
                Edit;
                QrCTTHANHTIENChange(Sender);
                Next;
            end;
            GrDetail.InvalidateCurrentRow;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.kbmTempS1Change(Sender: TField);
var
    s: String;
begin

    if SameText(Sender.AsString, '') then
        Exit;

    if not QrCT.IsEmpty then
    begin
        ErrMsg('Không được thay đổi Số bill/ Mã vạch trên phiếu khi chi tiết có dữ liệu.');
        Abort;
    end;

    with spCHECK_BILL_FB_BANLE do
    begin
        if Active then
            Close;

        Parameters[2].Value := QrNX.FieldByName('MAKHO').AsString;
        Parameters[3].Value := kbmTemp.FieldByName('S1').AsString;

        ExecProc;
        if Parameters[0].Value <> 0 then
        begin
            s := Parameters[1].Value;
            ErrMsg(s);
            Abort;
        end
        else
        begin
            Active := True;
            mKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
            with DataMain.QrTEMP do
            begin
                SQL.Text := 'select * from T_FB_BANLE where KHOA=:KHOA';
                Parameters[0].Value := TGuidEx.ToString(mKhoa);
                Open;

                if not IsEmpty then
                begin
                    SetEditState(QrNX);
                    QrNX.FieldByName('SCT2').AsString := FieldByName('SCT').AsString;
                    QrNX.FieldByName('NGAY2').AsDateTime := FieldByName('NGAY').AsDateTime;
                    QrNX.FieldByName('MAVIP').AsString := FieldByName('MAVIP').AsString;
                    QrNX.FieldByName('MAVIP_HOTEN').AsString := FieldByName('MAVIP_HOTEN').AsString;
                    QrNX.FieldByName('QUAY').AsString := FieldByName('QUAY').AsString;
                    QrNX.FieldByName('CA').AsString := FieldByName('CA').AsString;
                    QrNX.FieldByName('MAQUAY').AsString := FieldByName('MAQUAY').AsString;
                    QrNX.FieldByName('MABAN').AsString := FieldByName('MABAN').AsString;
                    QrNX.FieldByName('TL_PHUTHU').AsFloat := exVNDRound(FieldByName('TL_PHUTHU').AsFloat, sysPerRound);
                    QrNX.FieldByName('TL_CKHD').AsFloat := exVNDRound(FieldByName('TL_CKHD').AsFloat, sysPerRound);
                    QrNX.FieldByName('TL_CKVIPDM').AsFloat := exVNDRound(FieldByName('TL_CKVIPDM').AsFloat, sysPerRound);
                    QrNX.FieldByName('CO_HOADON').AsBoolean := FieldByName('CO_HOADON').AsBoolean;
                    QrNX.FieldByName('MAKHO').AsString := FieldByName('MAKHO').AsString;
                    QrNX.FieldByName('HINHTHUC_GIA').AsString := FieldByName('HINHTHUC_GIA').AsString;
                end;
                Close;
            end;
        end;
    end;

    if QrCT.IsEmpty then
    	CmdUpdateDetail.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXTHANHTOANChange(Sender: TField);
begin
    with QrNX do
    begin
        if FieldByName('CO_HOADON').AsBoolean then
            FieldByName('THUCTRA').AsFloat :=  FieldByName('THANHTOAN').AsFloat
        else
            FieldByName('THUCTRA').AsFloat :=  FieldByName('THANHTIEN').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrNXTIEN_THUE_CHUA_CLChange(Sender: TField);
begin
    with QrNX do
    begin
        FieldByName('TIEN_THUE').AsFloat := exVNDRound(FieldByName('TIEN_THUE_CHUA_CL').AsFloat, ctCurVatRound);
    end;
end;

(*
    ** Detail events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTSOLUONGValidate(Sender: TField);
begin
    with QrCT do
    if (FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG2').AsFloat)
    and (QrNX.FieldByName('SCT2').AsString <> '') then
    begin
        ErrMsg('Lỗi nhập liệu. Số lượng trả không hợp lệ.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTSOLUONGChange(Sender: TField);
var
	soLuong, donGia, soTien: Double;
begin
	with QrCT do
    begin
        soLuong := FieldByName('SOLUONG').AsFloat;
        donGia := FieldByName('DONGIA').AsFloat;

    	soTien := exVNDRound(soLuong * donGia, ctCurRound);

        FieldByName('SOTIEN').AsFloat := soTien;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTSOTIENChange(Sender: TField);
var
	soLuong, donGia, soTien, tyLeChietKhau, chietKhauMatHang, soTienSauChietKhau, mThanhTien,
    tlPhuthu, phuThu, donGiaSauChietKhau : Double;
begin
    with QrCT do
    begin
        soLuong := FieldByName('SOLUONG').AsFloat;
        donGia := FieldByName('DONGIA').AsFloat;
    	soTien := FieldByName('SOTIEN').AsFloat;
        tyLeChietKhau := FieldByName('TL_CK').AsFloat;
        tlPhuthu := FieldByName('TL_PHUTHU').AsFloat;

        chietKhauMatHang := exVNDRound(soTien * tyLeChietKhau / 100, ctCurRound) ;
        soTienSauChietKhau := exVNDRound(soTien - chietKhauMatHang, ctCurRound);
        phuThu := exVNDRound(soTien * tlPhuthu / 100, ctCurRound);
        mThanhTien := exVNDRound(soTienSauChietKhau + phuThu, ctCurRound);
        donGiaSauChietKhau := exVNDRound(SafeDiv(mThanhTien, soLuong), ctPriceNonVATRound);

        FieldByName('CHIETKHAU_MH').AsFloat := chietKhauMatHang;
        FieldByName('SOTIEN_SAU_CKMH').AsFloat := soTienSauChietKhau;
        FieldByName('DONGIA_SAU_CK').AsFloat := donGiaSauChietKhau;
        FieldByName('PHUTHU').AsFloat := phuThu;
        FieldByName('THANHTIEN').AsFloat := mThanhTien;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTTHANHTIENChange(Sender: TField);
var
	soLuong, donGiaSauChietKhau, donGiaChuaVAT, thueSuat, chietKhauMatHang, tienThue,
    mThanhTien, thanhTienChenhLech, mThanhTienChuaChenhlech, thanhTienChuaVAT,
    dgChuaVAT, soTienSauChietKhau, thanhToan : Double;
    hinhThucGia: string;
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        thueSuat := FieldByName('THUE_SUAT').AsFloat;
        hinhThucGia := QrNX.FieldByName('HINHTHUC_GIA').AsString;
        soLuong := FieldByName('SOLUONG').AsFloat;
        donGiaSauChietKhau := FieldByName('DONGIA_SAU_CK').AsFloat;
        mThanhTien := FieldByName('THANHTIEN').AsFloat;
        soTienSauChietKhau := FieldByName('SOTIEN_SAU_CKMH').AsFloat;

        thanhTienChenhLech := FieldByName('THANHTIEN_CL').AsFloat;

        if hinhThucGia = '03' then     // Nguoc
        begin
            thanhToan := exVNDRound(mThanhTien, ctCurRound);
            mThanhTienChuaChenhlech := exVNDRound(donGiaSauChietKhau / (1 + thueSuat / 100) * soLuong, ctCurVatRound);

            thanhTienChuaVAT := exVNDRound(mThanhTienChuaChenhlech + thanhTienChenhLech, ctCurRound);
            dgChuaVAT := exVNDRound(Iif(soLuong=0,0, SafeDiv(thanhTienChuaVAT, soLuong)), ctPriceNonVatRound);
            tienThue := exVNDRound(mThanhTienChuaChenhlech * thueSuat / 100, ctCurVatRound);
        end
        else if hinhThucGia = '02' then          // Xuoi
        begin
            mThanhTienChuaChenhlech := mThanhTien;
            thanhTienChuaVAT := exVNDRound(mThanhTienChuaChenhlech, ctCurRound);

            dgChuaVAT := exVNDRound(Iif(soLuong=0,0, SafeDiv(thanhTienChuaVAT, soLuong)), ctPriceNonVatRound);
            tienThue := exVNDRound(mThanhTienChuaChenhlech * thueSuat / 100, ctCurVatRound);
            thanhToan := exVNDRound(mThanhTienChuaChenhlech + tienThue + thanhTienChenhLech, ctCurRound);
        end;

        bTrigger := mTrigger;
        mTrigger := True;

        FieldByName('DONGIA_CHUA_VAT').AsFloat := dgChuaVAT;
        FieldByName('THANHTIEN_CHUA_CL').AsFloat := mThanhTienChuaChenhlech;
        FieldByName('THANHTIEN_CL').AsFloat := thanhTienChenhLech;
        FieldByName('THANHTIEN_CHUA_VAT').AsFloat := thanhTienChuaVAT;
        FieldByName('TIEN_THUE').AsFloat := tienThue;
        if thueSuat = 5 then
        begin
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_5').AsFloat := tienThue;
        end
        else if thueSuat = 10 then
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := tienThue;
        end else
        begin
            FieldByName('TIEN_THUE_5').AsFloat := 0;
            FieldByName('TIEN_THUE_10').AsFloat := 0;
            FieldByName('TIEN_THUE_OR').AsFloat := tienThue;
        end;
        FieldByName('THANHTOAN').AsFloat := thanhToan;
        mTrigger := bTrigger;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTTHANHTOANChange(Sender: TField);
begin
    vlTotal.Update;
    GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTBeforePost(DataSet: TDataSet);
begin
	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if not mDeleteAll then
        if not DeleteConfirm then
            Abort;

    SetEditState(QrNX);

    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if QrNX.FieldByName('SCT2').AsString <> '' then
        if not mTrigger  then
            Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTAfterCancel(DataSet: TDataSet);
begin
    vlTotal.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTAfterDelete(DataSet: TDataSet);
begin
//	if mTrigger then
//    	Exit;

    vlTotal.Update(True);

    GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);

        if QrNX.FieldByName('CO_HOADON').AsBoolean then
        begin
            FieldByName('CALC_THUE_SUAT').AsFloat := FieldByName('THUE_SUAT').AsFloat;
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTOAN').AsFloat
        end
        else
        begin
            FieldByName('CALC_THUE_SUAT').AsFloat := 0;
            FieldByName('CALC_TIEN_THUE').AsFloat := 0;
            FieldByName('CALC_THUCTRA').AsFloat := FieldByName('THANHTIEN').AsFloat
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTDONGIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if (QrNX.FieldByName('SCT2').AsString <> '') then
        begin
            ErrMsg('Thông tin được lấy từ hóa đơn. Không thể điều chỉnh.');
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.QrCTMAVTChange(Sender: TField);
begin
    exDotFBMavt(4, QrDMVT, Sender, 'OTHER');

    with DataMain.QrTEMP do
    begin
        SQL.Text := 'select	* from Vw_FB_DM_HH where isnull(CAMUNG_NHOM, '''') <> '''' and MAVT = ''' + Sender.AsString + '''';
        Open;
        QrCT.FieldByName('_id').Value := NewIntID;
        QrCT.FieldByName('TRA_DATE').AsDateTime := Now;
        QrCT.FieldByName('TRA_BY').AsInteger := sysLogonUID;
        QrCT.FieldByName('TENVT').AsString := FieldByName('TENVT').AsString;
        QrCT.FieldByName('TENTAT').AsString := FieldByName('TENTAT').AsString;
        QrCT.FieldByName('DVT').AsString := FieldByName('TenDvt').AsString;
        QrCT.FieldByName('BO').AsBoolean := FieldByName('BO').AsBoolean;
        QrCT.FieldByName('DONGIA').AsFloat := FieldByName('GIABAN').AsFloat;
        QrCT.FieldByName('LOAITHUE').AsString := FieldByName('LOAITHUE').AsString;
        QrCT.FieldByName('THUE_SUAT').AsFloat := FieldByName('VAT_VAO').AsFloat;
        Close;
    end;
	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdDetailExecute(Sender: TObject);
begin
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
begin
    QrDMKHO.Filter := '';
    QrDMKHO.Filtered := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.cbbKhoDropDown(Sender: TObject);
begin
   QrDMKHO.Filter := 'LOC='+QuotedStr(sysLoc);
   QrDMKHO.Filtered := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.cbbKhoExit(Sender: TObject);
begin
   
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmFB_ChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdDelDetailExecute(Sender: TObject);
begin
    if not YesNo('Xóa toàn bộ chi tiết mặt hàng. Tiếp tục?') then
        Exit;

    mDeleteAll := True;
    EmptyDataSet(QrCT);
    mDeleteAll := False;
    if QrNX.FieldByName('SCT2').AsString <> '' then
        CmdCancelBill.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu bán lẻ có mặt hàng sai mã. Tiếp tục?';
procedure TFrmFB_Nhaptrabl.CmdUpdateDetailExecute(Sender: TObject);
var
    bTrigger: Boolean;
begin
//    mKhoa := QrNX.FieldByName('KHOA').AsInteger;

    // Clean up
    DeleteConfirm(False);
    EmptyDataset(QrCT);
    DeleteConfirm(True);

    with spFB_BANLE_Select_Full do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(mKhoa);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters[0].Value <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;

        bTrigger := mTrigger;
        mTrigger := True;
        Screen.Cursor := crSqlWait;
        QrCT.DisableControls;
        while not Eof do
        begin
            QrCT.Append;
            QrCT.FieldByName('_id').Value := NewIntID;
            QrCT.FieldByName('TRA_DATE').AsDateTime := Now;
            QrCT.FieldByName('TRA_BY').AsInteger := sysLogonUID;
            QrCT.FieldByName('MAVT').AsString     := FieldByName('MAVT').AsString;
            QrCT.FieldByName('SOLUONG2').AsFloat   := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('DONGIA').AsFloat    := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('LOAITHUE').AsString     := FieldByName('LOAITHUE').AsString;
            QrCT.FieldByName('THUE_SUAT').AsFloat := FieldByName('THUE_SUAT').AsFloat;
            QrCT.FieldByName('TL_PHUTHU').AsFloat := FieldByName('TL_PHUTHU').AsFloat;
            QrCT.FieldByName('TL_CK').AsFloat     := FieldByName('TL_CK').AsFloat;
            QrCT.Post;

            Next;
        end;
        Close;

        mTrigger := bTrigger;
        QrCT.EnableControls;

    end;
    QrCT.First;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdUpdateQtyExecute(Sender: TObject);
begin
    with QrCT do
    begin
        if IsEmpty then
            Exit;

        CheckBrowseMode;
        DisableControls;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG2').AsFloat;
            Next;
        end;
        First;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Nhaptrabl.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Nhaptrabl.checkBillUsed(pAbort: Boolean): Boolean;
var
    s: string;
begin
    Result := False;
    with spCHECK_BILL_USED do
    begin
        if Active then
            Close;

        Parameters[2].Value := QrNX.FieldByName('_id').AsString;
        ExecProc;
        if Parameters[0].Value <> 0 then
        begin
            Result := True;
            if pAbort then
            begin
                s := Parameters[1].Value;
                ErrMsg(s);
                Abort;
            end;
        end;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Nhaptrabl.clearBillInfo: Boolean;
begin
    with QrNX do
    begin
        Edit;
        kbmTemp.FieldByName('S1').Clear;
        FieldByName('SCT2').Clear;
        FieldByName('NGAY2').Clear;
        FieldByName('MAVIP').Clear;
        FieldByName('MAVIP_HOTEN').Clear;
        FieldByName('QUAY').Clear;
        FieldByName('CA').Clear;
        FieldByName('MAQUAY').Clear;
        FieldByName('MABAN').Clear;
        FieldByName('TL_PHUTHU').Clear;
        FieldByName('TL_CKHD').Clear;
        FieldByName('TL_CKVIPDM').Clear;
        FieldByName('CO_HOADON').Clear;

        FieldByName('MAKHO').AsString := sysDefKho;
        FieldByName('HINHTHUC_GIA').AsString := sysHTGiaBL;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)


end.
