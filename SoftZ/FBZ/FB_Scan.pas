﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Scan;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Dialogs, ExtDlgs, Menus, ActnList, Db,
  ADODB, ExtCtrls, AdvMenus, ImgList, rImageZoom, rDBComponents,
  Vcl.Graphics, jpeg;

type
  TFrmFB_Scan = class(TForm)
    Action: TActionList;
    CmdAssign: TAction;
    CmdClear: TAction;
    OpenDlg: TOpenPictureDialog;
    CmdGray: TAction;
    QrHINH: TADOQuery;
    DsHINH: TDataSource;
    CmdExport: TAction;
    SaveDlg: TSaveDialog;
    rDBImage1: TrDBImage;
    PopPic: TAdvPopupMenu;
    Gnhnh1: TMenuItem;
    Xahnh1: TMenuItem;
    N2: TMenuItem;
    Export1: TMenuItem;
    N1: TMenuItem;
    PopStretch: TMenuItem;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAssignExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdExportExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PopStretchClick(Sender: TObject);
    procedure PopPicPopup(Sender: TObject);
    procedure QrHINHAfterInsert(DataSet: TDataSet);
  private
     mMa: String;
     isPopStretch: Boolean;
  public
  	procedure Execute(master: TDataSource; ma: String = 'MAVT');

    function ResizeJpeg(const inFile, outFile: TFileName;
    const aQuality: TJPEGQualityRange = 95;
    const NewWidth: Integer = 0; const NewHeight: Integer = 0): Boolean;

  end;

var
  FrmFB_Scan: TFrmFB_Scan;

implementation

{$R *.DFM}

uses
	isLib, GuidEx, isDb, isStr, MainData, isCommon, ExCommon, isMsg;

type
  TRGBArray = array[Word] of TRGBTriple;
  pRGBArray = ^TRGBArray;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.Execute;
begin
    with QrHINH do
    begin
        if Active then
            Close;

    	SQL.Text := Format(
			'select *' +
			'  from FB_DM_HH_FILE' +
			' where isnull([MAIN], 0) = 1 and %s = :%s', [ma, ma]);

    	DataSource := master;
		Open;
        mMa := master.DataSet.FieldByName('MAVT').AsString;
    end;
    Show;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

procedure TFrmFB_Scan.FormShow(Sender: TObject);
begin
   //
end;

procedure TFrmFB_Scan.PopPicPopup(Sender: TObject);
begin
     PopStretch.Checked := isPopStretch;
end;

procedure TFrmFB_Scan.PopStretchClick(Sender: TObject);
begin
    isPopStretch := not isPopStretch;
    rDBImage1.Stretch := isPopStretch;
end;

procedure TFrmFB_Scan.QrHINHAfterInsert(DataSet: TDataSet);
begin
    with QrHINH do
    begin
        TGuidField(FieldByName('IDX')).AsGuid := DataMain.GetNewGuid;
        FieldByName('MAVT').AsString := mMa;
        FieldByName('MAIN').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.CmdAssignExecute(Sender: TObject);
var
    s, ext, pathWeb: String;
    FileSize: Int64;
    temp: TBitmap;
begin
   	FormStyle := fsNormal;
	if not OpenDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
        Exit;
	end;

    s := OpenDlg.FileName;

    ext := UpperCase(ExtractFileExt(s));
    FileSize := getSizeOfFile(s);
    FileSize := Round(FileSize / (1024*1024));

    //  FileSize > 5MB
    if FileSize > 5  then
    begin
        ErrMsg('Kích thước file vượt quá 5MB');
        Exit;
    end;


    pathWeb := isStrReplace(s, ExtractFileExt(s), '_' + FormatDateTime('yyyy', Now) + ExtractFileExt(s));

    ResizeJpeg(s, pathWeb, 97, 300);

	with QrHINH do
    begin
    	if IsEmpty then
        begin
        	Append;
        end
		else
	        Edit;

	    FieldByName('NAME').AsString := ChangeFileExt(ExtractFileName(s), '');
        TBlobField(FieldByName('CONTENT')).LoadFromFile(s);
        TBlobField(FieldByName('CONTENT_WEB')).LoadFromFile(pathWeb);
	    FieldByName('EXT').AsString := ext;
        Post;
        try
           DeleteFile(pathWeb);
        finally
        end;
	end;

   	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.CmdClearExecute(Sender: TObject);
begin
	QrHINH.Delete;
end;



(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    isPopStretch := true;
    rDBImage1.Stretch := isPopStretch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, bInsert: Boolean;
begin
    with QrHINH do
    begin
    	if not Active then
        	Exit;

	    b := IsEmpty;
        bInsert := State in [dsInsert];
	end;


	CmdClear.Enabled := not b;
	CmdExport.Enabled := not b;
    PopStretch.Enabled := not b;
    CmdAssign.Enabled := not bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrHINH.Close;
    finally
    end;
    //Action := caFree;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Scan.CmdExportExecute(Sender: TObject);
begin
    FormStyle := fsNormal;
	if not SaveDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
    	Exit;
    end;

    with QrHINH do
    begin

        if FieldByName('CONTENT').IsNull then
        begin
	    	FormStyle := fsStayOnTop;
    		Exit;
        end;

	    TBlobField(FieldByName('CONTENT')).SaveToFile(
        	ChangeFileExt(SaveDlg.FileName, FieldByName('EXT').AsString));

    end;
	FormStyle := fsStayOnTop;
end;


function TFrmFB_Scan.ResizeJpeg(const inFile, outFile: TFileName;
  const aQuality: TJPEGQualityRange = 95;
  const NewWidth: Integer = 0; const NewHeight: Integer = 0): Boolean;
var
  JPEG: TJPEGImage;
  BMP: TBitmap;
begin
  Result:= FileExists(inFile);
  if not Result then
    Exit;

  JPEG:= TJPEGImage.Create;
  BMP:= TBitmap.Create;
  try
    try
      // Load
      JPEG.LoadFromFile(inFile);
    except
      Result:= False;
      Exit;
    end;
    if (NewWidth = 0) then
      BMP.Width:= Round(JPEG.Width*(NewHeight/JPEG.Height))
    else
      BMP.Width:= NewWidth;
    if (NewHeight = 0) then
      BMP.Height:= Round(JPEG.Height*(NewWidth/JPEG.Width))
    else
      BMP.Height:= NewHeight;
    BMP.PixelFormat:= pf24bit;
    // Change size
    with BMP.Canvas do
      StretchDraw(ClipRect, JPEG);
    // Move from Bitmap to Jpeg
    JPEG.Assign(BMP);
    // Change quality
    JPEG.CompressionQuality:= aQuality;
    JPEG.Compress;
    try
      // Save file
      JPEG.SaveToFile(outFile);
    except
      Result:= False;
    end;
  finally
    FreeAndNil(JPEG);
    FreeAndNil(BMP);
  end;
end;


end.
