program HrZ;

uses
  Forms,
  HRMain in 'HrZ\HRMain.pas' {FrmHRMain},
  crCommon in 'Common\crFrames\crCommon.pas' {CrFrame: TFrame},
  AddList in '..\Softz.Admin\AddList.pas' {FrmAddList},
  AdminData in '..\Softz.Admin\AdminData.pas' {Data: TDataModule},
  ExAdminCommon in '..\Softz.Admin\ExAdminCommon.pas',
  ListUser in '..\Softz.Admin\ListUser.pas' {FrmListUser},
  Users in '..\Softz.Admin\Users.pas' {FrmUser},
  ListGroup in '..\Softz.Admin\ListGroup.pas' {FrmListGroup},
  Groups in '..\Softz.Admin\Groups.pas' {FrmGroup},
  FunctionAccess in '..\Softz.Admin\FunctionAccess.pas' {FrmFuncAccess},
  ReportAccess in '..\Softz.Admin\ReportAccess.pas' {FrmRepAccess},
  crDS_NV in 'Common\crFrames\crDS_NV.pas' {frameDS_NV: TFrame},
  crDS_USER in 'Common\crFrames\crDS_USER.pas' {frameDS_USER: TFrame},
  crNAM in 'Common\crFrames\crNAM.pas' {frameNAM: TFrame},
  crNgay in 'Common\crFrames\crNgay.pas' {frameCrNgay: TFrame},
  crQUY in 'Common\crFrames\crQUY.pas' {frameQUY: TFrame},
  crSONGAY in 'Common\crFrames\crSONGAY.pas' {frameSONGAY: TFrame},
  crSOTHANG in 'Common\crFrames\crSOTHANG.pas' {frameSOTHANG: TFrame},
  crTHANGNAM in 'Common\crFrames\crTHANGNAM.pas' {frameTHANGNAM: TFrame},
  crTHOIGIAN in 'Common\crFrames\crTHOIGIAN.pas' {frameTHOIGIAN: TFrame},
  crTuden in 'Common\crFrames\crTuden.pas' {frameTuden: TFrame},
  crUID in 'Common\crFrames\crUID.pas' {frameUID: TFrame},
  HrExCommon in 'HrZ\HrExCommon.pas',
  SystemCriticalU in 'Common\SystemCriticalU.pas',
  MainData in 'Common\MainData.pas' {DataMain: TDataModule},
  ExCommon in 'Common\ExCommon.pas',
  GuidEx in 'Common\GuidEx.pas',
  RepEngine in 'Common\RepEngine.pas' {FrmRep: TDataModule},
  FastReport in 'Common\FastReport.pas' {FrmFastReport: TDataModule},
  GmsRep in 'Common\GmsRep.pas' {FrmGmsRep},
  crDS_KHO in 'Common\crFrames\crDS_KHO.pas' {frameDS_KHO: TFrame},
  crKho in 'Common\crFrames\crKho.pas' {frameCrKho: TFrame},
  ChonDsKho in 'Common\ChonDsKho.pas' {FrmChonDsKho},
  crTon in 'Common\crFrames\crTon.pas' {frameTon: TFrame},
  crMANCC in 'Common\crFrames\crMANCC.pas' {frameMANCC: TFrame},
  crMAKH in 'Common\crFrames\crMAKH.pas' {frameMAKH: TFrame},
  crDS_NCC in 'Common\crFrames\crDS_NCC.pas' {frameDS_NCC: TFrame},
  ChonDsNCC in 'Common\ChonDsNCC.pas' {FrmChonDsNCC},
  crMAVIP in 'Common\crFrames\crMAVIP.pas' {frameMAVIP: TFrame},
  crDS_KH in 'Common\crFrames\crDS_KH.pas' {frameDS_KH: TFrame},
  ChonDsKH in 'Common\ChonDsKH.pas' {FrmChonDsKH},
  crCT_TH in 'Common\crFrames\crCT_TH.pas' {frameCT_TH: TFrame},
  crTOP in 'Common\crFrames\crTOP.pas' {frameTOP: TFrame},
  crMAVT in 'Common\crFrames\crMAVT.pas' {frameMAVT: TFrame},
  ChonDsma in 'Common\ChonDsma.pas' {FrmChonDsma},
  crSILE in 'Common\crFrames\crSILE.pas' {frameSILE: TFrame},
  crDS_NHOMVT in 'Common\crFrames\crDS_NHOMVT.pas' {FrameDS_NHOMVT: TFrame},
  ChonDsNhom in 'Common\ChonDsNhom.pas' {FrmChonDsNhom},
  crMAHH in 'Common\crFrames\crMAHH.pas' {FrameMAHH: TFrame},
  ChonDsHH in 'Common\ChonDsHH.pas' {FrmChonDsHH},
  crNGANHNHOM in 'Common\crFrames\crNGANHNHOM.pas' {frameNGANHNHOM: TFrame},
  OfficeData in 'Common\OfficeData.pas' {DataOffice: TDataModule},
  crDS_LYDO_NK in 'Common\crFrames\crDS_LYDO_NK.pas' {frameDS_LYDO_NK: TFrame},
  ChonDsLydoNK in 'Common\ChonDsLydoNK.pas' {FrmChonDsLydoNK},
  crDS_LYDO_XK in 'Common\crFrames\crDS_LYDO_XK.pas' {frameDS_LYDO_XK: TFrame},
  ChonDsLydoXK in 'Common\ChonDsLydoXK.pas' {FrmChonDsLydoXK},
  ChonDsUser in 'Common\ChonDsUser.pas' {FrmChonDsUser},
  crDS_LOC in 'Common\crFrames\crDS_LOC.pas' {frameDS_LOC: TFrame},
  ChonDsLoc in 'Common\ChonDsLoc.pas' {FrmChonDsLoc},
  ChonDsNV in 'Common\ChonDsNV.pas' {FrmChonDsNV},
  crLOC in 'Common\crFrames\crLOC.pas' {frameCrLOC: TFrame},
  crCHIETKHAU in 'Common\crFrames\crCHIETKHAU.pas' {frameCHIETKHAU: TFrame},
  crBANLE_TT in 'Common\crFrames\crBANLE_TT.pas' {frameBANLE_TT: TFrame},
  crDS_VIP in 'Common\crFrames\crDS_VIP.pas' {frameDS_VIP: TFrame},
  ChonDsVIP in 'Common\ChonDsVIP.pas' {FrmChonDsVIP},
  crDS_LYDO_THU in 'Common\crFrames\crDS_LYDO_THU.pas' {frameDS_LYDO_THU: TFrame},
  ChonDsLydoTHU in 'Common\ChonDsLydoTHU.pas' {FrmChonDsLydoTHU},
  crDS_LYDO_THUKHAC in 'Common\crFrames\crDS_LYDO_THUKHAC.pas' {frameDS_LYDO_THUKHAC: TFrame},
  ChonDsLydoTHUKHAC in 'Common\ChonDsLydoTHUKHAC.pas' {FrmChonDsLydoTHUKHAC},
  crDS_LYDO_CHI in 'Common\crFrames\crDS_LYDO_CHI.pas' {frameDS_LYDO_CHI: TFrame},
  ChonDsLydoCHI in 'Common\ChonDsLydoCHI.pas' {FrmChonDsLydoCHI},
  crDS_LYDO_CHIKHAC in 'Common\crFrames\crDS_LYDO_CHIKHAC.pas' {frameDS_LYDO_CHIKHAC: TFrame},
  ChonDsLydoCHIKHAC in 'Common\ChonDsLydoCHIKHAC.pas' {FrmChonDsLydoCHIKHAC},
  crFB_DS_LYDO_NK in 'Common\crFrames\crFB_DS_LYDO_NK.pas' {frameFB_DS_LYDO_NK: TFrame},
  crFB_DS_LYDO_XK in 'Common\crFrames\crFB_DS_LYDO_XK.pas' {frameFB_DS_LYDO_XK: TFrame},
  crFB_DS_NHOMVT in 'Common\crFrames\crFB_DS_NHOMVT.pas' {FrameFB_DS_NHOMVT: TFrame},
  crFB_MAVT in 'Common\crFrames\crFB_MAVT.pas' {frameFB_MAVT: TFrame},
  crFB_MAVT_BO in 'Common\crFrames\crFB_MAVT_BO.pas' {frameFB_MAVT_BO: TFrame},
  crFB_MAVT_NPL in 'Common\crFrames\crFB_MAVT_NPL.pas' {frameFB_MAVT_NPL: TFrame},
  crFB_MAVT_NPL_TD in 'Common\crFrames\crFB_MAVT_NPL_TD.pas' {frameFB_MAVT_NPL_TD: TFrame},
  crFB_MAVT_TD in 'Common\crFrames\crFB_MAVT_TD.pas' {frameFB_MAVT_TD: TFrame},
  FB_ChonDsLydoNK in 'Common\FB_ChonDsLydoNK.pas' {FrmFB_ChonDsLydoNK},
  FB_ChonDsLydoXK in 'Common\FB_ChonDsLydoXK.pas' {FrmFB_ChonDsLydoXK},
  FB_ChonDsma_MAVT in 'Common\FB_ChonDsma_MAVT.pas' {FrmFB_ChonDsma_MAVT},
  FB_ChonDsma_MAVT_BO in 'Common\FB_ChonDsma_MAVT_BO.pas' {FrmFB_ChonDsma_MAVT_BO},
  FB_ChonDsma_MAVT_NPL in 'Common\FB_ChonDsma_MAVT_NPL.pas' {FrmFB_ChonDsma_MAVT_NPL},
  FB_ChonDsma_MAVT_NPL_TD in 'Common\FB_ChonDsma_MAVT_NPL_TD.pas' {FrmFB_ChonDsma_MAVT_NPL_TD},
  FB_ChonDsma_MAVT_TD in 'Common\FB_ChonDsma_MAVT_TD.pas' {FrmFB_ChonDsma_MAVT_TD},
  FB_ChonDsNhom in 'Common\FB_ChonDsNhom.pas' {FrmFB_ChonDsNhom},
  crDS_ChucDanh in 'Common\crFrames\crDS_ChucDanh.pas' {frameHR_DS_ChucDanh: TFrame},
  crDS_NhanVien in 'Common\crFrames\crDS_NhanVien.pas' {frameHR_DS_NhanVien: TFrame},
  ChonDsChucDanh in 'Common\ChonDsChucDanh.pas' {FrmChonDsChucDanh},
  ChonDsNhanVien in 'Common\ChonDsNhanVien.pas' {FrmChonDsNhanVien},
  crDS_DanToc in 'Common\crFrames\crDS_DanToc.pas' {FrameHR_DS_DanToc: TFrame},
  crDS_HopDong in 'Common\crFrames\crDS_HopDong.pas' {frameHR_DS_HopDong: TFrame},
  crDS_NguonTD in 'Common\crFrames\crDS_NguonTD.pas' {FrameHR_DS_NguonTD: TFrame},
  crDS_QuocTich in 'Common\crFrames\crDS_QuocTich.pas' {FrameHR_DS_QuocTich: TFrame},
  crDS_TonGiao in 'Common\crFrames\crDS_TonGiao.pas' {FrameHR_DS_TonGiao: TFrame},
  crDS_VanHoa in 'Common\crFrames\crDS_VanHoa.pas' {FrameHR_DS_VanHoa: TFrame},
  crGioiTinh in 'Common\crFrames\crGioiTinh.pas' {frameHR_GioiTinh: TFrame},
  ChonDsDanToc in 'Common\ChonDsDanToc.pas' {FrmChonDsDanToc},
  ChonDsHopDong in 'Common\ChonDsHopDong.pas' {FrmChonDsHopDong},
  ChonDsQuocTich in 'Common\ChonDsQuocTich.pas' {FrmChonDsQuocTich},
  ChonDsTonGiao in 'Common\ChonDsTonGiao.pas' {FrmChonDsTonGiao},
  ChonDsVanHoa in 'Common\ChonDsVanHoa.pas' {FrmChonDsVanHoa},
  ChonDsNguonTD in 'Common\ChonDsNguonTD.pas' {FrmChonDsNguonTD},
  crDS_QuaTrinhLV in 'Common\crFrames\crDS_QuaTrinhLV.pas' {frameHR_DS_QuaTrinhLV: TFrame},
  crKThuongKLuat in 'Common\crFrames\crKThuongKLuat.pas' {frameHR_KThuongKLuat: TFrame},
  crQuanHeGiaDinh in 'Common\crFrames\crQuanHeGiaDinh.pas' {frameHR_QuanHeGiaDinh: TFrame},
  ChonDsKLuat in 'Common\ChonDsKLuat.pas' {FrmChonDsKLuat},
  ChonDsKThuong in 'Common\ChonDsKThuong.pas' {FrmChonDsKThuong},
  ChonDsQuaTrinhLV in 'Common\ChonDsQuaTrinhLV.pas' {FrmChonDsQuaTrinhLV},
  crDoiTuong in 'Common\crFrames\crDoiTuong.pas' {frameHR_DoiTuong: TFrame},
  crTinhTrangNhanVien in 'Common\crFrames\crTinhTrangNhanVien.pas' {frameHR_TinhTrangNhanVien: TFrame},
  crLoaiNhanVien in 'Common\crFrames\crLoaiNhanVien.pas' {frameHR_LoaiNhanVien: TFrame},
  crDS_BoPhan in 'Common\crFrames\crDS_BoPhan.pas' {frameHR_DS_BoPhan: TFrame},
  crDS_ChiNhanh in 'Common\crFrames\crDS_ChiNhanh.pas' {FrameHR_DS_ChiNhanh: TFrame},
  crDS_ChiNhanhNotCN in 'Common\crFrames\crDS_ChiNhanhNotCN.pas' {FrameHR_DS_ChiNhanhNotCN: TFrame},
  crDS_KLuat in 'Common\crFrames\crDS_KLuat.pas' {FrameHR_DS_KLuat: TFrame},
  crDS_KThuong in 'Common\crFrames\crDS_KThuong.pas' {FrameHR_DS_KThuong: TFrame},
  crDS_LyDoThoiViec in 'Common\crFrames\crDS_LyDoThoiViec.pas' {frameHR_DS_LyDoThoiViec: TFrame},
  crDS_LyDoVangMat in 'Common\crFrames\crDS_LyDoVangMat.pas' {FrameHR_DS_LyDoVangMat: TFrame},
  crDS_PhongBan in 'Common\crFrames\crDS_PhongBan.pas' {FrameHR_DS_PhongBan: TFrame},
  crHonNhan in 'Common\crFrames\crHonNhan.pas' {frameHR_HonNhan: TFrame},
  ChonDsChiNhanh in 'Common\ChonDsChiNhanh.pas' {FrmChonDsChiNhanh},
  ChonDsLyDoThoiViec in 'Common\ChonDsLyDoThoiViec.pas' {FrmChonDsLyDoThoiViec},
  ChonDsLyDoVangMat in 'Common\ChonDsLyDoVangMat.pas' {FrmChonDsLyDoVangMat},
  ChonDsPhongBan in 'Common\ChonDsPhongBan.pas' {FrmChonDsPhongBan},
  ChonDsBoPhan in 'Common\ChonDsBoPhan.pas' {FrmChonDsBoPhan},
  DmHotro_HR in 'HrZ\DmHotro_HR.pas' {FrmDmHotro_HR},
  Scan in 'HrZ\Scan.pas' {FrmScan},
  Dmdl in 'Common\Dmdl.pas' {FrmDmdl},
  DmPhongban in 'HrZ\DmPhongban.pas' {FrmDmPhongban},
  DmTK_NV in 'HrZ\DmTK_NV.pas' {FrmDmTK_NV},
  Params2 in 'HrZ\Params2.pas' {FrmParams2},
  DmVangmat in 'HrZ\DmVangmat.pas' {FrmDmVangmat},
  DmNgayle in 'HrZ\DmNgayle.pas' {FrmDmNgayle},
  DevList in 'HrZ\DevList.pas' {FrmDevList},
  zkem in 'HrZ\zkem.pas',
  DevOption in 'HrZ\DevOption.pas' {FrmDevOption},
  DmCalamviec in 'HrZ\DmCalamviec.pas' {FrmDmCalamviec},
  DmChucvu in 'HrZ\DmChucvu.pas' {FrmDmChucvu},
  Vantay in 'HrZ\Vantay.pas' {FrmVantay},
  DevFP in 'HrZ\DevFP.pas' {FrmDevFP},
  DevFP2 in 'HrZ\DevFP2.pas' {FrmDevFP2},
  LogReader in 'HrZ\LogReader.pas',
  devResStr in 'HrZ\devResStr.pas',
  WaitProgress in 'HrZ\WaitProgress.pas' {FrmWaitProgress},
  DevRAW in 'HrZ\DevRAW.pas' {FrmDevRAW},
  DmNhomlv in 'HrZ\DmNhomlv.pas' {FrmDmNhomlv},
  DmNganhang in 'HrZ\DmNganhang.pas' {FrmDmNganhang},
  DmNhomlvDetail in 'HrZ\DmNhomlvDetail.pas' {FrmNhomlvDetail},
  DmQTLV in 'HrZ\DmQTLV.pas' {FrmDmQTLV},
  DmPhucap in 'HrZ\DmPhucap.pas' {FrmDmPhucap},
  DmHopdong in 'HrZ\DmHopdong.pas' {FrmDmHopdong},
  DmNoichuabenh in 'HrZ\DmNoichuabenh.pas' {FrmDmNoichuabenh},
  Bangluong in 'HrZ\Bangluong.pas' {FrmBangluong},
  Bangcong in 'HrZ\Bangcong.pas' {FrmBangcong},
  BangThongso in 'HrZ\BangThongso.pas' {FrmBangThongso},
  frameMY in 'HrZ\frameMY.pas' {frMY: TFrame},
  Tuden in 'HrZ\Tuden.pas' {FrmTuden},
  BosungLuong in 'HrZ\BosungLuong.pas' {FrmBosungLuong},
  BosungCong in 'HrZ\BosungCong.pas' {FrmBosungCong},
  DangkyVangmat in 'HrZ\DangkyVangmat.pas' {FrmDangkyVangmat},
  frameD2D in 'HrZ\frameD2D.pas' {frD2D: TFrame},
  frameEmp in 'HrZ\frameEmp.pas' {frEmp: TFrame},
  ImportExcel in 'HrZ\ImportExcel.pas' {FrmImportExcel},
  ImportExcelRev in 'HrZ\ImportExcelRev.pas' {FrmImportExcelRev},
  DangkyVangmatDanhSach in 'HrZ\DangkyVangmatDanhSach.pas' {FrmDangkyVangmatDanhSach},
  DangkyVangmatTools in 'HrZ\DangkyVangmatTools.pas' {FrmDangkyVangmatTools},
  ImportExcelToTable in 'HrZ\ImportExcelToTable.pas' {FrmImportExcelToTable},
  Phepnam in 'HrZ\Phepnam.pas' {FrmPhepnam},
  frameYearNow in 'HrZ\frameYearNow.pas' {frYearNow: TFrame},
  PhepnamBangThongso in 'HrZ\PhepnamBangThongso.pas' {FrmPhepnamBangThongso},
  PhepnamThongbao in 'HrZ\PhepnamThongbao.pas' {FrmPhepnamThongbao},
  HoSonv in 'HrZ\HoSonv.pas' {FrmHoSonv},
  DmnvThoiviec in 'HrZ\DmnvThoiviec.pas' {FrmDmnvThoiviec},
  Dmnv in 'HrZ\Dmnv.pas' {FrmDmnv},
  DmPhongbanNotCN in 'HrZ\DmPhongbanNotCN.pas' {FrmDmPhongbanNotCN},
  DmDiaDiemLamViec in 'HrZ\DmDiaDiemLamViec.pas' {FrmDmDiaDiemLamViec},
  DmNguonTuyenDung in 'HrZ\DmNguonTuyenDung.pas' {FrmDmNguonTuyenDung},
  HopdongLaodong in 'HrZ\HopdongLaodong.pas' {FrmHopdongLaodong},
  KhenThuongKyLuatDanhSach in 'HrZ\KhenThuongKyLuatDanhSach.pas' {FrmKhenThuongKyLuatDanhSach},
  KhenThuongKyLuatDangKy in 'HrZ\KhenThuongKyLuatDangKy.pas' {FrmKhenThuongKyLuatDangKy},
  KhenThuongKyLuat in 'HrZ\KhenThuongKyLuat.pas' {FrmKhenThuongKyLuat},
  DmLyDoKTKL in 'HrZ\DmLyDoKTKL.pas' {FrmDmLyDoKTKL},
  HoSonvThoiviec in 'HrZ\HoSonvThoiviec.pas' {FrmHoSonvThoiviec},
  ThietBiTaiSan in 'HrZ\ThietBiTaiSan.pas' {FrmThietBiTaiSan},
  DmThietBiTaiSanLaoDong in 'HrZ\DmThietBiTaiSanLaoDong.pas' {FrmDmThietBiTaiSanLaoDong},
  HoSonvQuanHeGiaDinh in 'HrZ\HoSonvQuanHeGiaDinh.pas' {FrmHoSonvQuanHeGiaDinh},
  HoSonvKienThucTaiLieu in 'HrZ\HoSonvKienThucTaiLieu.pas' {FrmHoSonvKienThucTaiLieu},
  HosonvThongTinKhac in 'HrZ\HosonvThongTinKhac.pas' {FrmHosonvThongTinKhac},
  HoSonvTruocKhiVaoCongTy in 'HrZ\HoSonvTruocKhiVaoCongTy.pas' {FrmHoSonvTruocKhiVaoCongTy},
  HosonvTaiKhoanTruyCap in 'HrZ\HosonvTaiKhoanTruyCap.pas' {FrmHosonvTaiKhoanTruyCap},
  HosonvDesc in 'HrZ\HosonvDesc.pas' {FrmHosonvDesc},
  HoSonvNgayKyNiem in 'HrZ\HoSonvNgayKyNiem.pas' {FrmHoSonvNgayKyNiem},
  LichSuCongTac in 'HrZ\LichSuCongTac.pas' {FrmLichSuCongTac},
  QuaTrinhLamViec in 'HrZ\QuaTrinhLamViec.pas' {FrmQuaTrinhLamViec},
  ScanAvatar in 'HrZ\ScanAvatar.pas' {FrmScanAvatar},
  HosonvCCCD in 'HrZ\HosonvCCCD.pas' {FrmHosonvCCCD},
  HosonvHoChieu in 'HrZ\HosonvHoChieu.pas' {FrmHosonvHoChieu},
  HoSonvPhuCapDangky in 'HrZ\HoSonvPhuCapDangky.pas' {FrmHoSonvPhuCapDangky},
  HoSonvPhuCapDanhSach in 'HrZ\HoSonvPhuCapDanhSach.pas' {FrmHoSonvPhuCapDanhSach},
  DangkyThaisan in 'HrZ\DangkyThaisan.pas' {FrmDangkyThaisan},
  DangkyTangCa in 'HrZ\DangkyTangCa.pas' {FrmDangkyTangCa},
  DangkyTangCaTools in 'HrZ\DangkyTangCaTools.pas' {FrmDangkyTangCaTools},
  DangkyTangCaDanhSach in 'HrZ\DangkyTangCaDanhSach.pas' {FrmDangkyTangCaDanhSach},
  SelectFields in 'HrZ\SelectFields.pas' {FrmSelectFields},
  DevLog in 'HrZ\DevLog.pas' {FrmDevLog},
  Params in 'HrZ\Params.pas' {FrmParams},
  ReceiptDesc in 'Common\ReceiptDesc.pas' {FrmReceiptDesc},
  HrData in 'HrZ\HrData.pas' {HrDataMain: TDataModule},
  DmBieuMau in 'HrZ\DmBieuMau.pas' {FrmDmBieuMau},
  DmBacLuong in 'HrZ\DmBacLuong.pas' {FrmDmBacLuong},
  DmCanhBao in 'HrZ\DmCanhBao.pas' {FrmDmCanhBao},
  frameYear in 'HrZ\frameYear.pas' {frYear: TFrame},
  NghiBu in 'HrZ\NghiBu.pas' {FrmNghiBu},
  HoSonvBieuMau in 'HrZ\HoSonvBieuMau.pas' {FrmHoSonvBieuMau},
  DataAccess in '..\Softz.Admin\DataAccess.pas' {FrmDataAccess},
  ExcelData in 'Common\ExcelData.pas' {DataExcel: TDataModule},
  HoSonvDanhSachImport in 'HrZ\HoSonvDanhSachImport.pas' {FrmHoSonvDanhSachImport},
  DaoTao in 'HrZ\DaoTao.pas' {FrmDaoTao},
  frameNavi in 'Common\frameNavi.pas' {frNavi: TFrame},
  LichLamViecTools in 'HrZ\LichLamViecTools.pas' {FrmLichLamViecTools},
  LichLamViec in 'HrZ\LichLamViec.pas' {FrmLichLamViec},
  HoSonvLichSuBieuMau in 'HrZ\HoSonvLichSuBieuMau.pas' {FrmHoSonvLichSuBieuMau},
  DangkyMangThai in 'HrZ\DangkyMangThai.pas' {FrmDangkyMangThai},
  exThread in 'Common\exThread.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Softz Solutions - HR';
  Application.CreateForm(TFrmHRMain, FrmHRMain);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TDataAdmin, DataAdmin);
  Application.CreateForm(THrDataMain, HrDataMain);
  Application.CreateForm(TDataExcel, DataExcel);
  Application.CreateForm(TDataOffice, DataOffice);
  Application.CreateForm(TFrmImportExcel, FrmImportExcel);
  Application.CreateForm(TFrmImportExcelRev, FrmImportExcelRev);
  Application.CreateForm(TFrmSelectFields, FrmSelectFields);
  Application.CreateForm(TFrmImportExcelToTable, FrmImportExcelToTable);
  Application.CreateForm(TFrmRep, FrmRep);
  Application.CreateForm(TFrmFastReport, FrmFastReport);
  Application.CreateForm(TFrmChonDsKho, FrmChonDsKho);
  Application.CreateForm(TFrmChonDsNCC, FrmChonDsNCC);
  Application.CreateForm(TFrmChonDsKH, FrmChonDsKH);
  Application.CreateForm(TFrmChonDsma, FrmChonDsma);
  Application.CreateForm(TFrmChonDsNhom, FrmChonDsNhom);
  Application.CreateForm(TFrmChonDsHH, FrmChonDsHH);
  Application.CreateForm(TFrmChonDsLydoNK, FrmChonDsLydoNK);
  Application.CreateForm(TFrmChonDsLydoXK, FrmChonDsLydoXK);
  Application.CreateForm(TFrmChonDsUser, FrmChonDsUser);
  Application.CreateForm(TFrmChonDsLoc, FrmChonDsLoc);
  Application.CreateForm(TFrmChonDsNV, FrmChonDsNV);
  Application.CreateForm(TFrmChonDsVIP, FrmChonDsVIP);
  Application.CreateForm(TFrmChonDsLydoTHU, FrmChonDsLydoTHU);
  Application.CreateForm(TFrmChonDsLydoTHUKHAC, FrmChonDsLydoTHUKHAC);
  Application.CreateForm(TFrmChonDsLydoCHI, FrmChonDsLydoCHI);
  Application.CreateForm(TFrmChonDsLydoCHIKHAC, FrmChonDsLydoCHIKHAC);
  Application.CreateForm(TFrmFB_ChonDsLydoNK, FrmFB_ChonDsLydoNK);
  Application.CreateForm(TFrmFB_ChonDsLydoXK, FrmFB_ChonDsLydoXK);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT, FrmFB_ChonDsma_MAVT);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_BO, FrmFB_ChonDsma_MAVT_BO);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_NPL, FrmFB_ChonDsma_MAVT_NPL);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_NPL_TD, FrmFB_ChonDsma_MAVT_NPL_TD);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_TD, FrmFB_ChonDsma_MAVT_TD);
  Application.CreateForm(TFrmFB_ChonDsNhom, FrmFB_ChonDsNhom);
  Application.CreateForm(TFrmChonDsDanToc, FrmChonDsDanToc);
  Application.CreateForm(TFrmChonDsHopDong, FrmChonDsHopDong);
  Application.CreateForm(TFrmChonDsQuocTich, FrmChonDsQuocTich);
  Application.CreateForm(TFrmChonDsTonGiao, FrmChonDsTonGiao);
  Application.CreateForm(TFrmChonDsVanHoa, FrmChonDsVanHoa);
  Application.CreateForm(TFrmChonDsNguonTD, FrmChonDsNguonTD);
  Application.CreateForm(TFrmChonDsKLuat, FrmChonDsKLuat);
  Application.CreateForm(TFrmChonDsKThuong, FrmChonDsKThuong);
  Application.CreateForm(TFrmChonDsQuaTrinhLV, FrmChonDsQuaTrinhLV);
  Application.CreateForm(TFrmChonDsChiNhanh, FrmChonDsChiNhanh);
  Application.CreateForm(TFrmChonDsLyDoThoiViec, FrmChonDsLyDoThoiViec);
  Application.CreateForm(TFrmChonDsLyDoVangMat, FrmChonDsLyDoVangMat);
  Application.CreateForm(TFrmChonDsPhongBan, FrmChonDsPhongBan);
  Application.CreateForm(TFrmWaitProgress, FrmWaitProgress);
  Application.CreateForm(TFrmTuden, FrmTuden);
  if not DataMain.Logon then
  begin
      Application.Terminate;
      Exit;
  end;

  (*==============================================================================
   ** Form su dung LookupComboboxEh tu dong ket noi va open DataSource
   *)
  Application.CreateForm(TFrmChonDsChucDanh, FrmChonDsChucDanh);
  Application.CreateForm(TFrmChonDsNhanVien, FrmChonDsNhanVien);
  Application.CreateForm(TFrmKhenThuongKyLuatDangKy, FrmKhenThuongKyLuatDangKy);
  Application.CreateForm(TFrmDangkyVangmatTools, FrmDangkyVangmatTools);
  Application.CreateForm(TFrmDangkyTangCaTools, FrmDangkyTangCaTools);
  Application.CreateForm(TFrmHoSonvPhuCapDangky, FrmHoSonvPhuCapDangky);
  Application.CreateForm(TFrmChonDsBoPhan, FrmChonDsBoPhan);

  Application.Run;
end.
