﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosChitiet;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Db, wwDBGrid2, ADODB, Buttons, StdCtrls, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmPosChitiet = class(TForm)
    QrTAM: TADOQuery;
    DsTAM: TDataSource;
    GrList: TwwDBGrid2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GrListDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  public
    function Execute(pQuery: TCustomADODataSet): Boolean;
  end;

var
  FrmPosChitiet: TFrmPosChitiet;

implementation

{$R *.DFM}

uses
    isLib, ExCommon, isDb, GuidEx, isCommon, PosMain;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosChitiet.Execute(pQuery: TCustomADODataSet): Boolean;
begin
    with QrTAM do
    begin
        if Active then
            Close;
        Clone(pQuery);
        Open;
    end;
    SetDisplayFormat(QrTAM, sysCurFmt);

    Result := ShowModal = mrOk;
    QrTAM.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosChitiet.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('FB_POS_CHITIET', GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosChitiet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosChitiet.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then
        Close
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosChitiet.GrListDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

end.
