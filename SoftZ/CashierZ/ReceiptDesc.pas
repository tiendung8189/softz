﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ReceiptDesc;

interface

uses
  Classes, Controls, Forms,
  StdCtrls;

type
  TFrmReceiptDesc = class(TForm)
    EdGhichu: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    function Execute(s: String = ''): Boolean;
  end;

var
  FrmReceiptDesc: TFrmReceiptDesc;

implementation

{$R *.DFM}

uses
    isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmReceiptDesc.Execute;
begin
	EdGhichu.Text := s;
    Result := ShowModal = mrOk;
    if Result then
        s := EdGhichu.Text;
    Free
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
        ModalResult := mrOk;
end;

end.
