object FrmThongtinCN_View: TFrmThongtinCN_View
  Left = 561
  Top = 513
  HelpContext = 1
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Th'#244'ng Tin Giao H'#224'ng'
  ClientHeight = 434
  ClientWidth = 775
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 374
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 212
      Width = 775
      Height = 162
      Align = alBottom
      Caption = ' Th'#244'ng tin h'#243'a '#273#417'n '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        775
        162)
      object Label6: TLabel
        Left = 122
        Top = 28
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'T'#234'n '#273#417'n v'#7883
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label7: TLabel
        Left = 760
        Top = 24
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object Label1: TLabel
        Left = 80
        Top = 116
        Width = 149
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' h'#243'a '#273#417'n'
        FocusControl = wwDBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 17
      end
      object Label4: TLabel
        Left = 126
        Top = 72
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'M'#227' s'#7889' thu'#7871
        FocusControl = DBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 63
      end
      object EdTEN: TwwDBEdit
        Tag = 1
        Left = 245
        Top = 24
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        AutoSize = False
        Color = 15794175
        Ctl3D = False
        DataField = 'CN_TENDV'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TwwDBEdit
        Left = 245
        Top = 112
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI_HD'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit1: TwwDBEdit
        Left = 245
        Top = 68
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_MST'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 775
      Height = 212
      Align = alClient
      Caption = ' Th'#244'ng tin giao h'#224'ng '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        775
        212)
      object Label15: TLabel
        Left = 178
        Top = 167
        Width = 51
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Email'
        FocusControl = wwDBEdit4
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 115
      end
      object Label2: TLabel
        Left = 49
        Top = 74
        Width = 180
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' giao h'#224'ng'
        FocusControl = DBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 92
        Top = 29
        Width = 137
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#432#7901'i li'#234'n h'#7879
        FocusControl = DBEdit2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 122
        Top = 122
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272'i'#7879'n tho'#7841'i'
        FocusControl = DBEdit5
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label10: TLabel
        Left = 760
        Top = 114
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label8: TLabel
        Left = 760
        Top = 26
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label9: TLabel
        Left = 760
        Top = 70
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object DBEdit1: TwwDBEdit
        Left = 245
        Top = 70
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit2: TwwDBEdit
        Left = 245
        Top = 26
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_LIENHE'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit5: TwwDBEdit
        Left = 245
        Top = 114
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DTHOAI'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit4: TwwDBEdit
        Left = 245
        Top = 158
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_EMAIL'
        DataSource = FrmBanleCN.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 374
    Width = 775
    Height = 60
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      775
      60)
    object BtClose: TRzBitBtn
      Left = 660
      Top = 4
      Width = 106
      Height = 50
      Cursor = 1
      FrameColor = 7617536
      ModalResult = 2
      ShowFocusRect = False
      Anchors = [akRight, akBottom]
      Caption = 'Tho'#225't'#13'Esc'
      Color = 15791348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      HotTrack = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      Glyph.Data = {
        C2040000424DC204000000000000420000002800000018000000180000000100
        10000300000080040000202E0000202E00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
        924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
        6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
        FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
        261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
        FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
        F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
        E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
        8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
        271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
        A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
        0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
        2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F}
      Margin = 1
      Spacing = 1
    end
  end
  object MyActionList: TActionList
    Left = 316
    Top = 328
    object CmdOK: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      ShortCut = 115
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      OnExecute = CmdCloseExecute
    end
  end
end
