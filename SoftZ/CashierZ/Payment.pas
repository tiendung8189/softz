﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Payment;

interface

uses
  Classes, Controls, Forms,
  DB, ADODB, wwdblook,
  ExtCtrls, wwdbedit, ActnList, HTMLabel, RzBckgnd,
  RzPanel, StdCtrls, RzButton, Mask, pngimage, isPanel,
  Vcl.Touch.Keyboard, AdvSmoothTouchKeyBoard;

type
  TFrmPayment = class(TForm)
    ActionList1: TActionList;
    CmdReturn: TAction;
    BtReturn: TRzBitBtn;
    BtClose: TRzBitBtn;
    CmdClose: TAction;
    Panel2: TPanel;
    Panel1: TPanel;
    PD1: TisPanel;
    EdSotien1: TwwDBEdit;
    PD2: TisPanel;
    HTMLabel1: THTMLabel;
    EdChuanchi: TwwDBEdit;
    EdSotien2: TwwDBEdit;
    PD3: TisPanel;
    HTMLabel2: THTMLabel;
    EdTTOAN3_MA: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    isPanel3: TisPanel;
    HTMLabel9: THTMLabel;
    HTMLabel7: THTMLabel;
    HTMLabel3: THTMLabel;
    EdChange: TwwDBEdit;
    wwDBEdit23: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    Panel3: TPanel;
    Panel4: TPanel;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    wwDBEdit4: TwwDBEdit;
    HTMLabel4: THTMLabel;
    HTMLabel5: THTMLabel;
    Panel5: TPanel;
    BtnDelivery: TRzBitBtn;
    CmdDelivery: TAction;
    PD4: TisPanel;
    HTMLabel6: THTMLabel;
    wwDBEdit5: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    CmdTTOAN1: TAction;
    CmdTTOAN50: TAction;
    PD5: TisPanel;
    EdSotien5: TwwDBEdit;
    EdVidientu: TwwDBLookupCombo;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdReturnExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure CmdDeliveryExecute(Sender: TObject);
    procedure CmdTTOAN1Execute(Sender: TObject);
  private
    mLoai: Integer;
    _loai_PTTT: Integer;
  public
  	function  Execute(pLoai: Integer): Boolean;
  end;

var
  FrmPayment: TFrmPayment;

implementation

uses
	isLib, PosMain, isMsg, exResStr, isDb, ExCommon, ThongtinCN, MainData;

{$R *.dfm}

(*==============================================================================
** 0: Save!
** 1: Save an Print!
** 3: Save an Delivery!
**------------------------------------------------------------------------------
*)
function TFrmPayment.Execute(pLoai: Integer): Boolean;
begin
    mLoai := pLoai;
	Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.FormShow(Sender: TObject);
begin
    PD2.Visible := FlexConfigBool('POS', 'PTTT2');
    PD3.Visible := FlexConfigBool('POS', 'PTTT3');
    PD4.Visible := FlexConfigBool('POS', 'PTTT4');
    PD5.Visible := FlexConfigBool('POS', 'PTTT5');
    with FrmMain.QrBH do
    begin
        SetEditState(FrmMain.QrBH);
        if FrmMain.dThanhtoan and (not FieldByName('DELIVERY').AsBoolean) then
        begin
            mTrigger := True;
            CmdTTOAN1.Execute;
            mTrigger := False;
        end;
    end;

    EdSotien1.SetFocus;
    EdSotien1.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
    bTt2, bDelirery: Boolean;
begin
    with FrmMain.QrBH do
    begin
        if not Active then
            Exit;
        bTt2 := FieldByName('TTOAN_CARD').AsFloat <> 0;
        bDelirery := FieldByName('DELIVERY').AsBoolean;

        EdChuanchi.ReadOnly := not bTt2;

//        if bDelirery then
//            BtnDelivery.Caption := 'Hủy G.hàng' + #13 + 'F3'
//        else
//            BtnDelivery.Caption := 'Giao hàng' + #13 + 'F3'
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdDeliveryExecute(Sender: TObject);
begin
    with FrmMain.QrBH do
    begin
        Application.CreateForm(TFrmThongtinCN, FrmThongtinCN);
        if not FrmThongtinCN.Execute then
            Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdReturnExecute(Sender: TObject);
var
    bErr: Boolean;
    ttoan1, ttoan2, ttoan3, ttoan4, ttoan5, thanhtoan: Double;
begin
    BtReturn.SetFocus;
    bErr := False;
    with FrmMain.QrBH do
    begin
        ttoan1 := FieldByName('TTOAN_CASH').AsFloat;
        ttoan2 := FieldByName('TTOAN_CARD').AsFloat;
        ttoan3 := FieldByName('TTOAN_TRAHANG').AsFloat;
        ttoan4 := FieldByName('TTOAN_VOUCHER').AsFloat;
        ttoan5 := FieldByName('TTOAN_EWALLET').AsFloat;
        thanhtoan := FieldByName('THANHTOAN').AsFloat;

        if ttoan2 > FieldByName('THANHTOAN').AsFloat then
            bErr := True;

        if FieldByName('CHUATHOI').AsFloat < thanhtoan then
            bErr := True;

        if bErr then
        begin
            ErrMsg('Số tiền không hợp lệ.', 'Thanh toán',1);
            if ttoan1 <> 0 then
                EdSotien1.SetFocus
            else
                EdSotien2.SetFocus;
            Exit;
        end;

    end;

	if not FrmMain.SaveBill(mLoai) then
    	Exit;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayment.CmdTTOAN1Execute(Sender: TObject);
begin
    _loai_PTTT := 1;

    with FrmMain.QrBH do
    begin
        FieldByName('TTOAN_CARD').AsFloat := 0;

        FieldByName('TTOAN_TRAHANG').AsFloat := 0;
        FieldByName('TTOAN_VOUCHER').AsFloat := 0;

        FieldByName('TTOAN_EWALLET').AsFloat := 0;
        FieldByName('TTOAN_BANK').AsFloat := 0;
        FieldByName('TTOAN_MEMBER').AsFloat := 0;

        FieldByName('TTOAN_CASH').AsFloat := FieldByName('THANHTOAN').AsFloat;
        EdSotien1.SetFocus;
        EdSotien1.SelectAll;
    end;
end;
end.
