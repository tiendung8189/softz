unit FastReport;

interface

uses
  SysUtils, Classes, frxChBox, frxOLE, frxDCtrl, frxDMPExport, frxCross,
  frxClass, frxADOComponents, frxBarcode, frxRich, frxDesgn, frxGradient,
  frxCrypt, frxGZip, frxExportMail, frxExportImage, frxExportCSV, frxExportODF,
  frxExportPDF, frxExportXML, frxExportXLS, frxExportHTML, frxExportText,
  frxExportRTF, frxExportBaseDialog;

type
  TFrmFastReport = class(TDataModule)
    frxBMPExport1: TfrxBMPExport;
    frxJPEGExport1: TfrxJPEGExport;
    frxTIFFExport1: TfrxTIFFExport;
    frxRTFExport1: TfrxRTFExport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxHTMLExport1: TfrxHTMLExport;
    frxXLSExport1: TfrxXLSExport;
    frxXMLExport1: TfrxXMLExport;
    frxPDFExport1: TfrxPDFExport;
    frxODSExport1: TfrxODSExport;
    frxODTExport1: TfrxODTExport;
    frxCSVExport1: TfrxCSVExport;
    frxGIFExport1: TfrxGIFExport;
    frxMailExport1: TfrxMailExport;
    frxGZipCompressor1: TfrxGZipCompressor;
    frxCrypt1: TfrxCrypt;
    frxGradientObject1: TfrxGradientObject;
    frxDesigner1: TfrxDesigner;
    frxReport1: TfrxReport;
    frxRichObject1: TfrxRichObject;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxADOComponents1: TfrxADOComponents;
    frxCrossObject1: TfrxCrossObject;
    frxDotMatrixExport1: TfrxDotMatrixExport;
    frxDialogControls1: TfrxDialogControls;
    frxOLEObject1: TfrxOLEObject;
    frxCheckBoxObject1: TfrxCheckBoxObject;
  private
    function  InitReport(const RepName: String; const Args: array of Variant): Boolean;
  public
    function  ShowReport(const pCaption, RepName: String; const Args: array of Variant;
	    const HidePrintBtn: Boolean = False;
        const HideExportBtn: Boolean = False): Boolean;
    procedure  DesignReport(const RepName: String; const Args: array of Variant);
    function  PrintReport(const RepName: String; const Args: array of Variant;
        const PrinterName: String = '';
        const NumOfCopy: Integer = 1;
        const EncryptOnly: Boolean = False): Boolean;
  end;

var
  FrmFastReport: TFrmFastReport;

implementation

{$R *.dfm}
uses
    MainData, isLib, isCommon;

{ TFrmFastReport }
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFastReport.DesignReport(const RepName: String;
  const Args: array of Variant);
begin
    InitReport(RepName, Args);
    frxReport1.DesignReport;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFastReport.InitReport(const RepName: String;
  const Args: array of Variant): Boolean;
var
	i, n: Integer;
    mRep, s: String;
    ls: TStrings;
begin
    mRep := sysRepPath + RepName + '.FR3';

    with frxReport1 do
    begin
        Result := LoadFromFile(mRep);
        if not Result then
            Exit;

        n := Length(Args);
        ls := TStringList.Create;
        try
            frxReport1.Variables.GetVariablesList('PARAMS', ls);

            if ls.Count < n then
                n := ls.Count;

            for i := 0 to n - 1 do
            begin
                s := ls[i];
                frxReport1.Variables.Variables[s] := Args[i];
            end;
        finally
            ls.Free;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFastReport.PrintReport(const RepName: String;
  const Args: array of Variant; const PrinterName: String;
  const NumOfCopy: Integer; const EncryptOnly: Boolean): Boolean;
begin
    Result := InitReport(RepName, Args);
    if Result then
        with frxReport1 do
        begin
            frxReport1.PrepareReport;
            frxReport1.PrintOptions.Printer := PrinterName;
            Result := Print;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFastReport.ShowReport(const pCaption, RepName: String;
  const Args: array of Variant; const HidePrintBtn,
  HideExportBtn: Boolean): Boolean;
begin
    Result := InitReport(RepName, Args);
    if Result then
        with frxReport1 do
        begin
            if HideExportBtn then
                PreviewOptions.Buttons := PreviewOptions.Buttons - [pbSave,pbExport,pbExportQuick]
            else
                PreviewOptions.Buttons := PreviewOptions.Buttons + [pbSave,pbExport,pbExportQuick];

            if HidePrintBtn then
                PreviewOptions.Buttons := PreviewOptions.Buttons - [pbPrint]
            else
                PreviewOptions.Buttons := PreviewOptions.Buttons + [pbPrint];
            frxReport1.PreviewForm.Caption := pCaption;
            ShowReport;
        end;
end;

end.
