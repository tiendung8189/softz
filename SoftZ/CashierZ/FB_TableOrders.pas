﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_TableOrders;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants, Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls, Wwmemo,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2,
  wwdbdatetimepicker, wwdbedit, Wwdbcomb, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, frameNavi, isDb,
  isPanel, wwDialog, Wwdotdot, Mask, Grids, Wwdbgrid, ToolWin, wwdblook, DateUtils ;

type
  TFrmFB_TableOrders = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrKM: TADOQuery;
    QrCT: TADOQuery;
    DsKM: TDataSource;
    DsCT: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKMXOA: TWideStringField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrKMIMG: TIntegerField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    CmdAudit: TAction;
    BtnBarCode2: TToolButton;
    QrKMNGAY: TDateTimeField;
    QrKMSCT: TWideStringField;
    QrKMLYDO: TWideStringField;
    QrKMDGIAI: TWideMemoField;
    QrKMCREATE_BY: TIntegerField;
    QrKMCREATE_DATE: TDateTimeField;
    QrKMUPDATE_BY: TIntegerField;
    QrKMUPDATE_DATE: TDateTimeField;
    QrKMDELETE_BY: TIntegerField;
    QrKMDELETE_DATE: TDateTimeField;
    TntLabel1: TLabel;
    EdLyDo: TwwDBEdit;
    Label5: TLabel;
    Label10: TLabel;
    DBEdit1: TDBMemo;
    TntToolButton2: TToolButton;
    QrDMHH: TADOQuery;
    CmdDatBan: TAction;
    TntToolButton3: TToolButton;
    Status2: TStatusBar;
    Filter2: TwwFilterDialog2;
    CmdListRefesh: TAction;
    CmdChonNhom: TAction;
    QrTemp: TADOQuery;
    CmdEmpty: TAction;
    PopDetail: TAdvPopupMenu;
    Xachitit1: TMenuItem;
    CmdChonNCC: TAction;
    CmdChonphieuKM: TAction;
    QrTemp2: TADOQuery;
    QrKMKHOA: TGuidField;
    QrKMLOC: TWideStringField;
    CmdChonNhomNCC: TAction;
    QrKMLCT: TWideStringField;
    CmdIntem: TAction;
    ToolButton5: TToolButton;
    QrDM: TADOQuery;
    QrDMMAVT: TWideStringField;
    QrDMTENVT: TWideStringField;
    QrKMCHECKED: TBooleanField;
    QrKMCHECKED_BY: TIntegerField;
    QrKMCHECKED_DATE: TDateTimeField;
    QrKMCHECKED_DESC: TWideMemoField;
    CmdChecked: TAction;
    QrKMIMG2: TIntegerField;
    MemoDate: TwwMemoDialog;
    PopItem1: TMenuItem;
    PopItem2: TMenuItem;
    PopItemAll: TMenuItem;
    N4: TMenuItem;
    QrKMLK_LYDO: TWideStringField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrCTSTT: TIntegerField;
    QrCTMAVT: TWideStringField;
    QrCTTENVT: TWideStringField;
    QrCTGIABAN: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrCTTL_CK: TFloatField;
    QrCTCREATE_BY: TIntegerField;
    QrCTCREATE_DATE: TDateTimeField;
    QrCTUPDATE_BY: TIntegerField;
    QrCTUPDATE_DATE: TDateTimeField;
    QrCTLOC: TWideStringField;
    QrCTLK_GIABAN: TFloatField;
    QrCTTINHTRANG: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrList: TADOStoredProc;
    CmdImportExcel: TAction;
    CmdDMTD: TAction;
    CmdDMNPL: TAction;
    EdGiaban: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBEdit;
    QrKMHOTEN: TWideStringField;
    QrKMDTHOAI: TWideStringField;
    QrKMDS_MABAN: TWideStringField;
    QrKMSOLUONG_KHACH: TFloatField;
    QrCTSOLUONG: TFloatField;
    QrCTSOTIEN: TFloatField;
    Label1: TLabel;
    QrKMTINHTRANG: TWideStringField;
    QrKMTINHTRANG_DATE: TDateTimeField;
    Label2: TLabel;
    CbTinhTrang: TwwDBLookupCombo;
    QrTINHTRANG: TADOQuery;
    DsDatBan: TDataSource;
    QrBan: TADOQuery;
    QrKMTHOIGIAN_GIUCHO: TFloatField;
    QrKMNGAY_HETHAN: TDateTimeField;
    QrKMSOLUONG_BAN: TFloatField;
    Label4: TLabel;
    CbNGAYHETHAN: TwwDBDateTimePicker;
    Label6: TLabel;
    wwDBEdit2: TwwDBEdit;
    isPanel2: TisPanel;
    GrDetail2: TwwDBGrid2;
    Label8: TLabel;
    wwDBEdit4: TwwDBEdit;
    QrKhuVuc: TADOQuery;
    QrDatBan: TADOQuery;
    GuidField1: TGuidField;
    WideStringField1: TWideStringField;
    BooleanField1: TBooleanField;
    WideStringField2: TWideStringField;
    QrDatBanLK_MAKV: TWideStringField;
    QrDatBanLK_KHUVUC: TWideStringField;
    IntegerField1: TIntegerField;
    WideStringField3: TWideStringField;
    QrKMCAL_NGAYHIENTAI: TDateTimeField;
    Panel1: TPanel;
    CmdHuyBan: TAction;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    QrKMLK_TEN_TINHTRANG: TWideStringField;
    QrCTLK_TenDvt: TWideStringField;
    QrCTLK_QuyDoi: TIntegerField;
    QrCTLK_TenDvtLon: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrKMAfterInsert(DataSet: TDataSet);
    procedure QrKMBeforeOpen(DataSet: TDataSet);
    procedure QrKMBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKMBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKMCalcFields(DataSet: TDataSet);
    procedure QrKMBeforeEdit(DataSet: TDataSet);
    procedure QrKMAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrKMAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDatBanExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure EdCheckedDateCustomDlg(Sender: TObject);
    procedure MemoDateInitDialog(Dialog: TwwMemoDlg);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure PopItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure QrKMTINHTRANGChange(Sender: TField);
    procedure QrDatBanBeforeOpen(DataSet: TDataSet);
    procedure QrKMNGAYChange(Sender: TField);
    procedure CmdHuyBanExecute(Sender: TObject);
  private
	mCanEdit: Boolean;

    // List filter
    fType, mFilter: Integer;
    mLCT, fSQL, fStr, tmpSQL, tmpSQL2, pMABAN: String;
    fLoc: String;
  public
	procedure Execute(r: WORD; mMABAN: string = '');
  end;

var
  FrmFB_TableOrders: TFrmFB_TableOrders;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, isCommon,
    isLib, GuidEx, isFile, FB_Tables;

{$R *.DFM}

const
	FORM_CODE = 'FB_POS_DATBAN';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.EdCheckedDateCustomDlg(Sender: TObject);
begin
	MemoDate.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.Execute;
begin
    mLCT := 'FDBAN';
    pMABAN := mMABAN;
	mCanEdit := rCanEdit(r);
    DsKM.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.FormCreate(Sender: TObject);
begin
    frNavi.DataSet := QrKM;

    // Display Format
    with QrKM do
    begin
        SetDisplayFormat(QrKM, sysCurFmt);
	    SetShortDateFormat(QrKM);
    	SetDisplayFormat(QrKM, ['NGAY', 'NGAY_HETHAN', 'CAL_NGAYHIENTAI'], ShortDateFormat + ' hh:nn');
        SetDisplayFormat(QrKM, ['CHECKED_DATE'], DateTimeFmt);
    end;
	SetDisplayFormat(QrCT, sysCurFmt);
    SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_MABAN'], [GrBrowse, GrDetail, GrDetail2]);
    SetDictionary([QrKM, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, nil]);

    // Initial
    fType := 2;
    fStr := '';
    mFilter := 1;
    fSQL := QrKM.SQL.Text;
    tmpSQL := QrTemp.SQL.Text;
    tmpSQL2 := QrTemp2.SQL.Text;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

	Wait(PREPARING);
    InitFmtAndRoud(mLct);

    with DataMain do
        OpenDataSets([QrLYDO_KM]);

    OpenDataSets([QrDMHH, QrTINHTRANG, QrKhuVuc, QrBan]);


    SetDisplayFormat(QrDMHH, sysCurFmt);
    ClearWait;


    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrKM, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrKM, QrDMHH]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdRefreshExecute(Sender: TObject);
var
	s, s2: String;
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    d := Now;
    DecodeDate(d, yy, mm, dd);
    DecodeTime(d, hh, nn, ss, ms);

    d1 := EncodeDate(yy, mm, dd);
    d2 := EncodeTime(hh, nn, ss, ms);
   	if (fLOC <> '')then
    begin
        fLoc     := '';
		Screen.Cursor := crSQLWait;
		with QrKM do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            if pMABAN <> '' then
                SQL.Add(' and KHOA in (select KHOA from FB_DATBAN_MABAN where MARK = 1 and MABAN = ''' + pMABAN + ''')');


            // Detail Filter
            if fStr <> '' then
	           		case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from FB_DATBAN_CT a, FB_DM_HH b, DM_NHOM c where a.KHOA = FB_DATBAN.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from FB_DATBAN_CT a, FB_DM_HH b where a.KHOA = FB_DATBAN.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from FB_DATBAN_CT where KHOA = FB_DATBAN.KHOA and MAVT in (' + fStr + '))');
				end;
            s2 := '';
            case mFilter of
            1:
            begin
               s2 := ' and ( (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <= NGAY_HETHAN' +
                          ' and isnull(DELETE_BY, 0) = 0 ))';
                SQL.Add(s2);
            end;
            2:
            begin
                s2 := ' and ( not (''' + FormatDateTime(ShortDateFmtSQL, d1) + ''' <= NGAY_HETHAN' +
                          ' or isnull(DELETE_BY, 0) <> 0 ))';
                SQL.Add(s2);
            end;
            end;
			SQL.Add('order by NGAY_HETHAN');
    	    Open;
            
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMHH.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdNewExecute(Sender: TObject);
begin
	QrKM.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrKM.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrKM.Cancel;

    if QrKM.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrKM);
 end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;


procedure TFrmFB_TableOrders.CmdHuyBanExecute(Sender: TObject);
begin
    if not YesNo(RS_ORDER_TABLE_CANCEL, 1) then
        Exit;

    with QrKM do
    begin
        SetEditState(QrKM);
        FieldByName('TINHTRANG').AsString := '03';
        FieldByName('TINHTRANG_DATE').AsDateTime := Now;
        QrKM.Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsKM)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdReReadExecute(Sender: TObject);
begin
	fLoc := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted, bCancel: Boolean;
    n: Integer;
begin
	with QrKM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
        bCancel := FieldByName('TINHTRANG').AsString = '03';
    end;
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrKM);

    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrKM, False);
    CmdChecked.Caption := GetCheckedCaption(QrKM);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdIntem.Enabled := not bEmpty;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdEmpty.Enabled := not bEmptyCT;

    CmdDatBan.Enabled := not bEmpty and bBrowse and (n = 1);

    CmdHuyBan.Enabled := not bCancel and bBrowse and (n = 1);

end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMAfterInsert(DataSet: TDataSet);
var
    d, d1, d2: TDateTime;
    xx, yy: WORD;
begin
    d := Now;
    d1 := Date;
    DecodeDate(d1, yy, xx, xx);
    d2 := EncodeDate(yy, 12, 31);
	with QrKM do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('LCT').AsString       := mLCT;
        FieldByName('LYDO').AsString      := '';
        FieldByName('CHECKED').AsBoolean  := False;
		FieldByName('NGAY').AsDateTime    := d;
        FieldByName('LOC').AsString       := sysLoc;
        FieldByName('THOIGIAN_GIUCHO').AsInteger := 30;
        FieldByName('TINHTRANG').AsString:= '01';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;
procedure TFrmFB_TableOrders.QrKMNGAYChange(Sender: TField);
begin
    with QrKM do
    begin
        FieldByName('NGAY_HETHAN').AsDateTime  := IncMinute(FieldByName('NGAY').AsDateTime, FieldByName('THOIGIAN_GIUCHO').AsInteger);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_TableOrders.QrKMTINHTRANGChange(Sender: TField);
begin
    with QrKM do
    begin
        if not FieldByName('TINHTRANG').IsNull then
        begin
		    FieldByName('TINHTRANG_DATE').AsDateTime  := Now;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_TableOrders.ToolButton4Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMBeforeOpen(DataSet: TDataSet);
begin
	with QrKM do
    begin
		Parameters[0].Value := mLCT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMBeforePost(DataSet: TDataSet);
var
	dengio: TDateTime;
begin
	with QrKM do
    begin
		if BlankConfirm(QrKM, ['NGAY', 'HOTEN', 'DTHOAI']) then
	    	Abort;

	end;
    DataMain.AllocSCT(mLCT, QrKM);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrKM);
    if mTrigger then
        Exit;

    exIsChecked(QrKM);
//    exReSyncRecord(QrKM);
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrKMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    with QrCT do
    begin
        if BlankConfirm(QrCT, ['MAVT', 'SOLUONG']) then
            Abort;


        with QrCT do
        begin
            if IsDuplicateCode(QrCT, FieldByName('MAVT'), True) then
                Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTAfterInsert(DataSet: TDataSet);
begin
    with QrCT do
    begin
        FieldByName('LOC').AsString := sysLoc;
    end;
end;

procedure TFrmFB_TableOrders.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTMAVTChange(Sender: TField);
begin
	exDotFBMavt(1, QrDMHH, Sender);
    with QrCT do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;
    	FieldByName('GIABAN').AsFloat := FieldByName('LK_GIABAN').AsFloat;
    end;
	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.QrCTSOLUONGChange(Sender: TField);
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        FieldByName('SOTIEN').AsFloat := exVNDRound(FieldByName('GIABAN').AsFloat * FieldByName('SOLUONG').AsFloat, ctCurRound);
    end;
    GrDetail.InvalidateCurrentRow;
end;

procedure TFrmFB_TableOrders.QrDatBanBeforeOpen(DataSet: TDataSet);
begin
    QrDatBan.Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
    d, d1, d2: TDateTime;
    dd, mm, yy, hh,nn, ss, ms: WORD;
begin
    if Highlight then
        Exit;

    d := Now;
//    DecodeDate(d, yy, mm, dd);
//    DecodeTime(d, hh, nn, ss, ms);
//
//    d1 := EncodeDate(yy, mm, dd);
//    d2 := EncodeTime(hh, nn, ss, ms);
    with QrKM do
    begin
        //Bị xóa là màu đỏ
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clRed;
            Exit;
        end;

        if  (d >= (FieldByName('NGAY').AsDateTime)) then
        begin
            AFont.Color := clMaroon;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.GrBrowseDblClick(Sender: TObject);
begin
    if QrKM.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.MemoDateInitDialog(Dialog: TwwMemoDlg);
var
	ls: TStrings;
    i, n: Integer;
begin
	ls := TStringList.Create;
    ls.Text := QrKM.FieldByName('CHECKED_DESC').AsString;
    n := ls.Count;
    for i := 0 to ls.Count - 1 do
    begin
    	ls[i] := Format('%2d: %s', [n, ls[i]]);
        Dec(n);
    end;

	with Dialog do
    begin
    	Memo.ReadOnly := False;
        OKBtn.Visible := False;
        CancelBtn.Font.Style := [fsBold];
        Memo.Lines.AddStrings(ls);
    end;
    ls.Free;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if PgMain.ActivePageIndex = 1 then
    	Status2.SimpleText := exRecordCount(QrCT,Filter2)
    else
   		Status.SimpleText := exRecordCount(QrKM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
        with QrDatBan do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.PopItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.PopupMenu1Popup(Sender: TObject);
begin
    PopItem1.Checked := mFilter = 1;
    PopItem2.Checked := mFilter = 2;
    PopItemAll.Checked := mFilter = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsKM, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdDatBanExecute(Sender: TObject);
var
    s: String;
    mCount: Integer;
begin
	with QrKM do
    Begin
        Edit;

        FieldByName('CAL_NGAYHIENTAI').AsDateTime := Now;

  		Application.CreateForm(TFrmFB_Tables, FrmFB_Tables);
		FrmFB_Tables.Get(TGuidField(FieldByName('KHOA')).AsGuid, FieldByName('CHECKED').AsBoolean, s, mCount);

        FieldByName('DS_MABAN').AsString := s;
        FieldByName('SOLUONG_BAN').AsInteger := mCount;
        Post;

        QrDatBan.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_TableOrders.CmdEmptyExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;
end.
