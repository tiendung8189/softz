﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CkMa;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Graphics, Data.DB, kbmMemTable, Vcl.Mask,
  wwdbedit, RzButton;

type
  TFrmCkma = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdTEN: TEdit;
    Label4: TLabel;
    Label2: TLabel;
    tbTemp: TkbmMemTable;
    tbTempTL_CK: TFloatField;
    DataSource1: TDataSource;
    EdCK: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    tbTempCHIETKHAU: TFloatField;
    BtClose: TRzBitBtn;
    BtReturn: TRzBitBtn;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CmdOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tbTempTL_CKChange(Sender: TField);
    procedure tbTempTL_CKValidate(Sender: TField);
    procedure tbTempCHIETKHAUValidate(Sender: TField);
  private
    mSotien: Double;
  public
  	function GetDisCount (ten : String; sotien: Double; var k : Double) : Boolean;
  end;

var
  FrmCkma: TFrmCkma;

implementation

{$R *.DFM}

uses
	isLib, isMsg, exCommon, isCommon, isDb;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCkma.GetDisCount;
var
	cs : String;
begin
    mSotien := sotien;
	EdTEN.Text := Trim(ten);
    with tbTemp do
    begin
        Open;
        Append;
        FieldByName('TL_CK').AsFloat := k;
    end;

   	Result := ShowModal = mrOK;
    if Result then
	begin
        with tbTemp do
    		k := FieldByName('TL_CK').AsFloat;
    end;

    tbTemp.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.tbTempCHIETKHAUValidate(Sender: TField);
var
    x: Double;
begin
    x := Sender.AsFloat;
    if (x < 0) or (x > mSotien ) or (x > 99999999) then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.tbTempTL_CKChange(Sender: TField);
var
    b: Boolean;
    tlck, mCk: Double;
begin
    if mTrigger then
        Exit;

    b := mTrigger;
    with tbTemp do
    begin
        mTrigger := True;
        if Sender.FieldName = 'TL_CK' then
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            FieldByName('CHIETKHAU').AsFloat := exVNDRound(mSotien * tlck / 100.0, sysCurRound)
        end
        else
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            FieldByName('TL_CK').AsFloat := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end;
        mTrigger := b;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.tbTempTL_CKValidate(Sender: TField);
var
    x: Double;
begin
    x := Sender.AsFloat;
    if (x < 0) or (x > 100 ) then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;

    TFloatField(tbTempTL_CK).DisplayFormat := sysPerFmt;
    TFloatField(tbTempCHIETKHAU).DisplayFormat := sysCurFmt;
    EdCk.SelectAll;
    EdCk.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.CmdOKClick(Sender: TObject);
begin
    tbTemp.CheckBrowseMode;
    ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCkma.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

end.
