﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CashierSplitTableOrders;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants, Types, Windows,
  Db, wwDBGrid2, ADODB, Buttons, StdCtrls, Grids, Wwdbigrd, Wwdbgrid,
  AdvSmoothTileList, AdvSmoothTileListEx, ExtCtrls, HTMLabel, AppEvnts,
  Vcl.ActnList, RzButton, AdvSmoothTileListImageVisualizer,
  AdvSmoothTileListHTMLVisualizer, AdvGlowButton, AdvSmoothPanel,
  AdvSmoothScrollBar, AdvMetroScrollBox, DBAdvSmoothLabel, AdvSmoothLabel,
  AdvSmoothListBox, DBAdvSmoothListBox;

type
  TFrmCashierSplitTableOrders = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    QrDmKhuVuc: TADOQuery;
    MyActionList: TActionList;
    CmdTableOrders: TAction;
    CmdOrders: TAction;
    CmdChangeTable: TAction;
    CmdMergeTable: TAction;
    CmdSplitTable: TAction;
    CmdClose: TAction;
    Panel20: TPanel;
    Panel3: TPanel;
    spDmBan: TADOStoredProc;
    Panel4: TPanel;
    Panel7: TPanel;
    Panel1: TPanel;
    AdvGlowButton3: TAdvGlowButton;
    Panel6: TPanel;
    AdvGlowButton2: TAdvGlowButton;
    Panel9: TPanel;
    Panel8: TPanel;
    TtKhuVuc: TAdvSmoothTileListEx;
    TtKhuVucBan: TAdvSmoothTileListEx;
    spMergeTable: TADOStoredProc;
    spChangeTable: TADOStoredProc;
    Panel2: TPanel;
    Panel5: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    LbTable: TAdvSmoothLabel;
    EdTable: TDBAdvSmoothLabel;
    LbTableSplit: TAdvSmoothLabel;
    EdTableSplit: TDBAdvSmoothLabel;
    GrDetail: TwwDBGrid2;
    GrDetailSplit: TwwDBGrid2;
    QrBHSplit: TADOQuery;
    QrBHSplitCALC_TONGCK: TFloatField;
    QrBHSplitCALC_TIENTHOI: TFloatField;
    QrBHSplitLK_TENKHO: TWideStringField;
    QrBHSplitLCT: TWideStringField;
    QrBHSplitNGAY: TDateTimeField;
    QrBHSplitQUAY: TWideStringField;
    QrBHSplitCA: TWideStringField;
    QrBHSplitSCT: TWideStringField;
    QrBHSplitMAVIP: TWideStringField;
    QrBHSplitMAKHO: TWideStringField;
    QrBHSplitCHIETKHAU_MH: TFloatField;
    QrBHSplitCK_BY: TIntegerField;
    QrBHSplitTL_CK: TFloatField;
    QrBHSplitCHIETKHAU: TFloatField;
    QrBHSplitTHUE: TFloatField;
    QrBHSplitSOTIEN: TFloatField;
    QrBHSplitSOTIEN1: TFloatField;
    QrBHSplitSOLUONG: TFloatField;
    QrBHSplitCHUATHOI: TFloatField;
    QrBHSplitTHANHTOAN: TFloatField;
    QrBHSplitQUAYKE: TWideStringField;
    QrBHSplitDGIAI: TWideMemoField;
    QrBHSplitPRINTED: TBooleanField;
    QrBHSplitCREATE_BY: TIntegerField;
    QrBHSplitCREATE_DATE: TDateTimeField;
    QrBHSplitTL_CK2: TFloatField;
    QrBHSplitTTOAN1: TFloatField;
    QrBHSplitTTOAN2: TFloatField;
    QrBHSplitTL_CK_TAM: TFloatField;
    QrBHSplitKHOA: TGuidField;
    QrBHSplitTINHTRANG: TWideStringField;
    QrBHSplitCN_TENDV: TWideStringField;
    QrBHSplitCN_MST: TWideStringField;
    QrBHSplitCN_DIACHI: TWideStringField;
    QrBHSplitCN_DIACHI_HD: TWideStringField;
    QrBHSplitCN_LIENHE: TWideStringField;
    QrBHSplitCN_DTHOAI: TWideStringField;
    QrBHSplitCN_EMAIL: TWideStringField;
    QrBHSplitCN_MATK: TWideStringField;
    QrBHSplitLK_TENTK: TWideStringField;
    QrBHSplitLK_DAIDIEN: TWideStringField;
    QrBHSplitLK_NGANHANG: TWideStringField;
    QrBHSplitPRINT_NO: TIntegerField;
    QrBHSplitLOC: TWideStringField;
    QrBHSplitSO_CHUANCHI: TWideStringField;
    QrBHSplitTTOAN3: TFloatField;
    QrBHSplitTTOAN3_SCT: TWideStringField;
    QrBHSplitTTOAN3_KHOA: TGuidField;
    QrBHSplitTTOAN4: TFloatField;
    QrBHSplitDELIVERY: TBooleanField;
    QrBHSplitTTOAN1_1: TFloatField;
    QrBHSplitTTOAN1_2: TFloatField;
    QrBHSplitTTOAN4_INPUT: TWideStringField;
    QrBHSplitTTOAN4_MATHE: TWideStringField;
    QrBHSplitTIENTHOI: TFloatField;
    QrBHSplitLK_TENVIP: TWideStringField;
    QrBHSplitLK_VIP_DTHOAI: TWideStringField;
    QrBHSplitLK_VIP_DCHI: TWideStringField;
    QrBHSplit_id: TLargeintField;
    QrBHSplitTTOAN5: TFloatField;
    QrBHSplitTTOAN5_LOAI: TWideStringField;
    QrBHSplitTTOAN6: TFloatField;
    QrBHSplitTTOAN7: TFloatField;
    QrBHSplitMAQUAY: TWideStringField;
    QrBHSplitMABAN: TWideStringField;
    QrBHSplitSOLUONG_KHACH: TFloatField;
    QrBHSplitLK_BAN: TWideStringField;
    QrBHSplitSOLUONG_GIAO: TFloatField;
    QrBHSplitHINHTHUC_GIA: TWideStringField;
    QrBHSplitUPDATE_DATE: TDateTimeField;
    QrBHSplitUPDATE_BY: TIntegerField;
    DsBHSplit: TDataSource;
    DsCTBHSplit: TDataSource;
    QrCTBHSplit: TADOQuery;
    QrCTBHSplitSTT: TIntegerField;
    QrCTBHSplitMAVT: TWideStringField;
    QrCTBHSplitTENVT: TWideStringField;
    QrCTBHSplitTENTAT: TWideStringField;
    QrCTBHSplitLK_TENVT: TWideStringField;
    QrCTBHSplitLK_TENVT_KHONGDAU: TWideStringField;
    QrCTBHSplitLK_TENTAT: TWideStringField;
    QrCTBHSplitDVT: TWideStringField;
    QrCTBHSplitSOLUONG: TFloatField;
    QrCTBHSplitDONGIA: TFloatField;
    QrCTBHSplitSOTIEN: TFloatField;
    QrCTBHSplitGHICHU: TWideStringField;
    QrCTBHSplitRSTT: TIntegerField;
    QrCTBHSplitCALC_SOTIEN_SAUCK: TFloatField;
    QrCTBHSplitKHOA: TGuidField;
    QrCTBHSplitKHOACT: TGuidField;
    QrCTBHSplitLOC: TWideStringField;
    QrCTBHSplitBO: TBooleanField;
    QrCTBHSplitSOLUONG_GIAO: TFloatField;
    DBAdvSmoothLabel1: TDBAdvSmoothLabel;
    DBAdvSmoothLabel2: TDBAdvSmoothLabel;
    lbDbState: TAdvSmoothLabel;
    AdvSmoothLabel1: TAdvSmoothLabel;
    CmdIns: TAction;
    CmDel: TAction;
    QrBH: TADOQuery;
    QrBHCALC_TONGCK: TFloatField;
    QrBHCALC_TIENTHOI: TFloatField;
    QrBHLK_TENKHO: TWideStringField;
    QrBHLCT: TWideStringField;
    QrBHNGAY: TDateTimeField;
    QrBHQUAY: TWideStringField;
    QrBHCA: TWideStringField;
    QrBHSCT: TWideStringField;
    QrBHMAVIP: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    QrBHCK_BY: TIntegerField;
    QrBHTL_CK: TFloatField;
    QrBHCHIETKHAU: TFloatField;
    QrBHTHUE: TFloatField;
    QrBHSOTIEN: TFloatField;
    QrBHSOTIEN1: TFloatField;
    QrBHSOLUONG: TFloatField;
    QrBHCHUATHOI: TFloatField;
    QrBHTHANHTOAN: TFloatField;
    QrBHQUAYKE: TWideStringField;
    QrBHDGIAI: TWideMemoField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_BY: TIntegerField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHTL_CK2: TFloatField;
    QrBHTTOAN1: TFloatField;
    QrBHTTOAN2: TFloatField;
    QrBHTL_CK_TAM: TFloatField;
    QrBHKHOA: TGuidField;
    QrBHTINHTRANG: TWideStringField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHLK_TENTK: TWideStringField;
    QrBHLK_DAIDIEN: TWideStringField;
    QrBHLK_NGANHANG: TWideStringField;
    QrBHPRINT_NO: TIntegerField;
    QrBHLOC: TWideStringField;
    QrBHSO_CHUANCHI: TWideStringField;
    QrBHTTOAN3: TFloatField;
    QrBHTTOAN3_SCT: TWideStringField;
    QrBHTTOAN3_KHOA: TGuidField;
    QrBHTTOAN4: TFloatField;
    QrBHDELIVERY: TBooleanField;
    QrBHTTOAN1_1: TFloatField;
    QrBHTTOAN1_2: TFloatField;
    QrBHTTOAN4_INPUT: TWideStringField;
    QrBHTTOAN4_MATHE: TWideStringField;
    QrBHTIENTHOI: TFloatField;
    QrBHLK_TENVIP: TWideStringField;
    QrBHLK_VIP_DTHOAI: TWideStringField;
    QrBHLK_VIP_DCHI: TWideStringField;
    QrBH_id: TLargeintField;
    QrBHTTOAN5: TFloatField;
    QrBHTTOAN5_LOAI: TWideStringField;
    QrBHTTOAN6: TFloatField;
    QrBHTTOAN7: TFloatField;
    QrBHMAQUAY: TWideStringField;
    QrBHMABAN: TWideStringField;
    QrBHSOLUONG_KHACH: TFloatField;
    QrBHLK_BAN: TWideStringField;
    QrBHSOLUONG_GIAO: TFloatField;
    QrBHHINHTHUC_GIA: TWideStringField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHUPDATE_BY: TIntegerField;
    QrCTBH: TADOQuery;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHTENTAT: TWideStringField;
    QrCTBHLK_TENVT: TWideStringField;
    QrCTBHLK_TENVT_KHONGDAU: TWideStringField;
    QrCTBHLK_TENTAT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHSOTIEN: TFloatField;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHRSTT: TIntegerField;
    QrCTBHCALC_SOTIEN_SAUCK: TFloatField;
    QrCTBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHLOC: TWideStringField;
    QrCTBHBO: TBooleanField;
    QrCTBHSOLUONG_GIAO: TFloatField;
    DsBH: TDataSource;
    DsCTBH: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

    procedure TtKhuVucTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
  
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TtKhuVucBanTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure CmdSplitTableExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrCTBHSplitCalcFields(DataSet: TDataSet);
    procedure QrCTBHSplitBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHSplitBeforePost(DataSet: TDataSet);
    procedure QrCTBHSplitAfterPost(DataSet: TDataSet);
    procedure QrCTBHSplitAfterInsert(DataSet: TDataSet);
    procedure QrBHSplitAfterInsert(DataSet: TDataSet);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmDelExecute(Sender: TObject);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure QrBHBeforeOpen(DataSet: TDataSet);
    procedure QrBHSplitBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHSOLUONGChange(Sender: TField);
    procedure QrCTBHSplitSOLUONGChange(Sender: TField);
    procedure QrCTBHSplitSOLUONGValidate(Sender: TField);
  private
    procedure tbAddUnit;
    procedure tbAddGroup;
    procedure OpenQueries;

    procedure TotalList(QrMaster, QrDetail: TADOQuery);

  public
    mMAKV, mMABAN, mTENBAN, mMABAN_NEW, mTENBAN_NEW : String;
    curKhoa, splitKhoa: TGUID;
    curStt: Integer;
    function Execute(sMAKV, sMABAN, sTENBAN: string; sKhoa: TGUID): Boolean;
  end;

var
  FrmCashierSplitTableOrders: TFrmCashierSplitTableOrders;

implementation

{$R *.DFM}

uses
    isLib, ExCommon, isDb, GuidEx, isCommon, PosMain, isMsg, KeypadNumeric, CashierMain, MainData,
    exCOMPort, isStr;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

procedure TFrmCashierSplitTableOrders.CmDelExecute(Sender: TObject);
begin
     //
end;

procedure TFrmCashierSplitTableOrders.CmdInsExecute(Sender: TObject);
var
    sMAVT: string;
    sSOLUONG, sDONGIA: Double;
begin
   with QrCTBH do
   begin
       sMAVT := FieldByName('MAKV').AsString;
       sDONGIA := FieldByName('DONGIA').AsFloat;
//       sSOLUONG := 1;
//       if (sMAVT <> '') then
//       begin
//            with QrCTBHSplit do
//            begin
//               if Locate('MAKV', sMAVT, []) then
//               begin
//                    sSOLUONG := FieldByName('SOLUONG').AsFloat;
//                    Edit;
//                    FieldByName('SOLUONG').AsFloat := sSOLUONG;
//               end
//               else
//               begin
//                    Append;
//                    FieldByName('MAVT').AsString := sMAVT;
//                    FieldByName('DONGIA').AsFloat := sDONGIA;
//                    FieldByName('SOLUONG').AsFloat := sSOLUONG;
//               end;
//            end;
//       end;
   end;
end;

procedure TFrmCashierSplitTableOrders.CmdSplitTableExecute(Sender: TObject);
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin


        FrmCashierMain.CmdRefreshExecute(Nil);
        FrmCashierMain.RefreshSelectTable(mMABAN);
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCashierSplitTableOrders.Execute(sMAKV, sMABAN, sTENBAN: string; sKhoa: TGUID): Boolean;
begin
    mMAKV := sMAKV;
    mMABAN := sMABAN;
    mTENBAN := sTENBAN;
    EdTable.Caption.Text := mTENBAN;
    curKhoa := sKhoa;

    Caption := 'BÀN ' + sTENBAN;
    Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierSplitTableOrders.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;

    with spDmBan do
    begin
        if Active then
            Close;

        ExecProc;
    end;

    OpenDatasets([QrDmKhuVuc, spDmBan, QrBH, QrCTBH]);


    QrDmKhuVuc.Requery;
    spDmBan.Requery;


     // Display format
	SetDisplayFormat(QrBH, sysCurFmt);
    SetShortDateFormat(QrBH);

    with QrCTBH do
    begin
		SetDisplayFormat(QrCTBH, sysCurFmt);
    end;

    SetDisplayFormat(QrBHSplit, sysCurFmt);
    SetShortDateFormat(QrBHSplit);

    with QrCTBHSplit do
    begin
		SetDisplayFormat(QrCTBHSplit, sysCurFmt);
    end;


    EdTableSplit.Caption.Text := '';
    DBAdvSmoothLabel2.Caption.Text := '';

    // Touch
    with TtKhuVuc do
    begin
        Columns := 4;
        Rows := 1;
    end;

    with TtKhuVucBan do
    begin
        Columns := 5;
        Rows := round(Height / 85);
    end;


    tbAddGroup;
    tbAddUnit;

    TtKhuVuc.SelectTile(0);
    TtKhuVucTileClick(TtKhuVuc, TtKhuVuc.Tiles[0], tsNormal);

end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdSplitTable.Enabled := (mMABAN_NEW <> '');

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierSplitTableOrders.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierSplitTableOrders.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then
        Close
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierSplitTableOrders.tbAddGroup;
var
    mTiles: TAdvSmoothTile;
begin
    TtKhuVuc.Tiles.Clear;
    TtKhuVuc.BeginUpdate;
    with QrDmKhuVuc do
    begin
        First;
        while not Eof do
        begin
            mTiles := TtKhuVuc.Tiles.Add;
            with mTiles do
            begin
                Data := FieldByName('MAKV').AsString;
                Content.Text := FieldByName('TENKV').AsString;
                Content.TextPosition := tpCenterCenter;
                Tag := 0;
            end;
            Next;
        end;
    end;
    TtKhuVuc.EndUpdate;
    TtKhuVuc.FirstPage;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.tbAddUnit;
var
    fContent, fName, fPersons, fTimes, mColor, mBackGround, fTotal: string;
    fAmount: Double;
begin
    TtKhuVucBan.Tiles.Clear;
    TtKhuVucBan.BeginUpdate;
    with spDmBan do
    begin
        First;
        while not Eof do
        begin
            with TtKhuVucBan.Tiles.Add do
            begin
                mColor := '0000FF';
                mBackGround := 'FF0000';

                fName := FieldByName('TENBAN').AsString;
                fPersons := IntToStr(FieldByName('SOLUONG_MACDINH').AsInteger)
                  + ' Khách';
                if FieldByName('SOLUONG_KHACH').AsInteger > 0 then
                begin
                    fPersons := IntToStr(FieldByName('SOLUONG_KHACH').AsInteger)
                      + '/' + IntToStr(FieldByName('SOLUONG_MACDINH').AsInteger)
                      + ' Khách';
                end;
                fTimes := FormatDatetime('HH:MM', FieldByName('NGAY').AsDateTime);
                fAmount := FieldByName('SOTIEN_ORDER').AsFloat;
                fTotal := FloatToStrF(fAmount, ffNumber, 10, 0) + 'đ';
                fContent := '';
                fContent := fContent +
                  '<p align="right"><font  size="2" color="#' + mColor + '">' +
                  fPersons + '</font></p>';
                fContent := fContent +
                  '<p align="center"><font align="center" size="4" color="#' +
                  mColor + '">' + fName + '</font></p>';
                if FieldByName('SOLUONG_KHACH').AsInteger <> 0 then
                begin
                    fContent := fContent +
                      '<p align="left"><font size="2" color="#' + mColor + '">'
                      + fTimes + '</font></p>';

                    StatusIndicator := fTotal;
                    StatusIndicatorLeft := -5 - Length(fTotal) * 6 div 2;
                    StatusIndicatorTop := 60;
                    TtKhuVucBan.TileAppearance.StatusIndicatorAppearance.Font.
                      Color := $FF0000;
                end;
                Data := FieldByName('MABAN').AsString;
                Content.Text := fContent;
                Content.TextPosition := tpHTMLAlign;

            end;
            Next;
        end;
    end;
    TtKhuVucBan.EndUpdate;
    TtKhuVucBan.FirstPage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.TtKhuVucBanTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
begin
    mMABAN_NEW := Tile.Data;
    with spDmBan do
    if Locate('MABAN', mMABAN_NEW, []) then
    begin
        mTENBAN_NEW := FieldByName('TENBAN').AsString;
        splitKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
        EdTableSplit.Caption.Text := mTENBAN_NEW;

        OpenQueries;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.TtKhuVucTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Data;
    if (s <> '') and (s <> 'ALL') then
      sf := 'MAKV=' + QuotedStr(s) + ' and TINHTRANG = 0 ';

    mMAKV := s;
    spDmBan.Filter := sf;
    tbAddUnit;
end;

procedure TFrmCashierSplitTableOrders.OpenQueries;
begin

    CloseDataSets([QrBHSplit, QrCTBHSplit]);
    OpenDataSets([QrBHSplit, QrCTBHSplit]);

    with QrBHSplit do
    begin
	    if IsEmpty then
		    Append
        else
        begin
            with QrCTBHSplit do
            begin
                Last;
            end;
            Edit;
        end;
    end;
end;

procedure TFrmCashierSplitTableOrders.QrBHBeforeOpen(DataSet: TDataSet);
begin
    with QrBH do
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
end;

procedure TFrmCashierSplitTableOrders.QrBHSplitAfterInsert(DataSet: TDataSet);
begin
    curStt := 0;
	with QrBHSplit do
    begin
		TGuidField(FieldByName('KHOA')).AsGuid := splitKhoa;
        FieldByName('_id').Value := NewIntID;
		FieldByName('NGAY').AsDateTime := Now;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
		FieldByName('CREATE_BY').AsInteger := sysLogonUID;

        FieldByName('LCT').AsString := FrmCashierMain.mLCT;
		FieldByName('QUAY').AsString := posQuay;
        FieldByName('MAQUAY').AsString := posMaQuay;
		FieldByName('CA').AsString := '1';
		FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('HINHTHUC_GIA').AsString := sysHinhThucGia;
        FieldByName('DELIVERY').AsBoolean := False;
        FieldByName('MABAN').AsString := mMABAN;
        FieldByName('SOLUONG_KHACH').AsInteger := 1;
        posSct := DataMain.AllocRetailBillNumber(posMakho, posQuay, Date);
        FieldByName('SCT').AsString := posSct;
        posSct := isSmartInc(posSct, 2);
    end;
end;

procedure TFrmCashierSplitTableOrders.QrBHSplitBeforeOpen(DataSet: TDataSet);
begin
    with QrBHSplit do
    	Parameters[0].Value := TGuidEx.ToString(splitKhoa);
end;

procedure TFrmCashierSplitTableOrders.QrCTBHAfterInsert(DataSet: TDataSet);
begin
//    if mTrigger then
//    	Exit;
//
//    with QrCTBH do
//    begin
//
//    	Inc(curStt);
//    	FieldByName('STT').AsInteger := curStt;
//        TGuidField(FieldByName('KHOA')).AsGuid := splitKhoa;
//        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
//        FieldByName('LOC').AsString := QrBHSplit.FieldByName('LOC').AsString;
//    end;
//
//    // lay ngay gio de tinh chiet khau, khuyen mai.
//    if Is1Record(QrCTBH) and (not sysIsCentral) then
//    begin
//        SetEditState(QrCTBH);
//        QrCTBH.FieldByName('NGAY').AsDateTime := Now;
//    end;
//
//    TotalList(QrBH, QrBH);
//	TotalList(QrBHSplit, QrCTBHSplit);
end;

procedure TFrmCashierSplitTableOrders.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
    with QrCTBH do
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSOLUONGChange(Sender: TField);
var
	sl, dg, st: Double;
begin
    if mTrigger then
    	Exit;

	with QrCTBH do
    begin
        if (Sender <> Nil) then
        begin
            dg  := FieldByName('DONGIA').AsFloat;
            sl  := FieldByName('SOLUONG').AsFloat;
            st := exVNDRound(sl*dg);
            FieldByName('SOTIEN').AsFloat := st;
        end;
        GrDetail.InvalidateCurrentRow;
	end;
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitAfterInsert(DataSet: TDataSet);
begin
//    if mTrigger then
//    	Exit;
//
//    with QrCTBHSplit do
//    begin
//
//    	Inc(curStt);
//    	FieldByName('STT').AsInteger := curStt;
//        TGuidField(FieldByName('KHOA')).AsGuid := splitKhoa;
//        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
//        FieldByName('LOC').AsString := QrBHSplit.FieldByName('LOC').AsString;
//    end;
//
//    // lay ngay gio de tinh chiet khau, khuyen mai.
//    if Is1Record(QrCTBHSplit) and (not sysIsCentral) then
//    begin
//        SetEditState(QrBHSplit);
//        QrBHSplit.FieldByName('NGAY').AsDateTime := Now;
//    end;
//
//    TotalList(QrBH, QrCTBH);
//	TotalList(QrBHSplit, QrCTBHSplit);

end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitAfterPost(DataSet: TDataSet);
begin
//    if mTrigger then
//    	Exit;
//
//    TotalList(QrBH, QrCTBH);
//	TotalList(QrBHSplit, QrCTBHSplit);
//
//    // Reread DMVT
//    with QrCTBHSplit do
//    begin
//        if FieldByName('LK_TENVT').AsString = '' then
//        begin
//            FieldByName('LK_TENVT').RefreshLookupList;
//            FieldByName('LK_TENVT_KHONGDAU').RefreshLookupList;
//        end;
//
//        icdComPort.IcdScan(FieldByName('LK_TENVT_KHONGDAU').AsString
//            ,FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat)
//            ,FormatFloat(ctCurFmt, QrBHSplit.FieldByName('SOTIEN').AsFloat) + ' VND');
//    end;
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitBeforeOpen(DataSet: TDataSet);
begin
    with QrCTBHSplit do
    	Parameters[0].Value := TGuidEx.ToString(splitKhoa);
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    with QrBHSplit do
    if State in [dsInsert] then
    begin
        Post;
        Edit;
    end;
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);

    end;
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitSOLUONGChange(Sender: TField);
var
	sl, dg, st: Double;
begin
    if mTrigger then
    	Exit;

	with QrCTBHSplit do
    begin
        if (Sender <> Nil) then
        begin
            dg  := FieldByName('DONGIA').AsFloat;
            sl  := FieldByName('SOLUONG').AsFloat;
            st := exVNDRound(sl*dg);
            FieldByName('SOTIEN').AsFloat := st;
        end;
        GrDetailSplit.InvalidateCurrentRow;
	end;
end;

procedure TFrmCashierSplitTableOrders.QrCTBHSplitSOLUONGValidate(
  Sender: TField);
begin
    //
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierSplitTableOrders.TotalList(QrMaster, QrDetail: TADOQuery);
var
    xSotien, mSotien, mSoluong, mSoluonggiao: Double;
begin
	if mTrigger then
    	Exit;

	mSotien := 0;
    mSoluong := 0;
    mSoluonggiao := 0;

	with QrDetail do
    begin
		First;
        while not Eof do
        begin
        	xSotien := FieldByName('SOTIEN').AsFloat;
	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
            mSoluonggiao := mSoluonggiao + FieldByName('SOLUONG_GIAO').AsFloat;
    	    Next;
        end;
    end;

    with QrMaster do
    begin
    	if State in [dsBrowse] then
        	Edit;
    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
        FieldByName('SOLUONG_GIAO').AsFloat := mSoluonggiao;
    	FieldByName('THANHTOAN').AsFloat := mSotien;
        CheckBrowseMode;
    end;
end;

end.
