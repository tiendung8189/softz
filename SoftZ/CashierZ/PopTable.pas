unit PopTable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RzLabel, ExtCtrls, RzPanel, AdvSmoothLabel, AdvGlowButton;

type
  TFrmPopTable = class(TForm)
    Panel3: TPanel;
    AdvGlowButton12: TAdvGlowButton;
    Panel4: TPanel;
    AdvGlowButton1: TAdvGlowButton;
    Panel1: TPanel;
    AdvGlowButton2: TAdvGlowButton;
    Panel2: TPanel;
    AdvGlowButton3: TAdvGlowButton;
    Panel5: TPanel;
    AdvGlowButton4: TAdvGlowButton;
    Panel7: TPanel;
    AdvGlowButton17: TAdvGlowButton;
  private
  public
    procedure Popup(pX, Py: Integer);
  end;

var
  FrmPopTable: TFrmPopTable;

implementation

{$R *.dfm}
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmPopTable.Popup(pX, Py: Integer);
begin
    Left := pX;
    Top := Py;
    Show;
end;
end.
