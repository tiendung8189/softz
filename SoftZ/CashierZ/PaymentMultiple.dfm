object FrmPaymentMultiple: TFrmPaymentMultiple
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Thanh To'#225'n'
  ClientHeight = 547
  ClientWidth = 928
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Padding.Left = 8
  Padding.Top = 8
  Padding.Right = 8
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    928
    547)
  PixelsPerInch = 96
  TextHeight = 16
  object Panel5: TPanel
    Left = 8
    Top = 8
    Width = 915
    Height = 533
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 415
      Top = 0
      Width = 500
      Height = 468
      Align = alRight
      TabOrder = 0
      object PD_TTOAN_CASH: TisPanel
        Left = 1
        Top = 59
        Width = 498
        Height = 58
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 0
        Visible = False
        HeaderCaption = '  .: Ti'#7873'n m'#7863't - Cash'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          58)
        object HTMLabel4: THTMLabel
          Left = 103
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Color = cl3DLight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentColor = False
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdSOTIEN01: TwwDBEdit
          Left = 223
          Top = 17
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clWhite
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD_TTOAN_CARD: TisPanel
        Left = 1
        Top = 211
        Width = 498
        Height = 91
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 1
        Visible = False
        HeaderCaption = '  .: Th'#7867' ng'#226'n h'#224'ng - Bank card'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          91)
        object HTMLabel1: THTMLabel
          Left = 103
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' chu'#7849'n chi</B>' +
              '<br/>(Auth. Code)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel3: THTMLabel
          Left = 103
          Top = 55
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdChuanchi: TwwDBEdit
          Left = 223
          Top = 22
          Width = 229
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'TypeInput'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 9109504
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSOTIEN04: TwwDBEdit
          Left = 223
          Top = 50
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD_TTOAN_TRAHANG: TisPanel
        Left = 1
        Top = 490
        Width = 498
        Height = 94
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 2
        Visible = False
        HeaderCaption = '  .: Phi'#7871'u nh'#7853'p tr'#7843' - Pos return'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          94)
        object HTMLabel2: THTMLabel
          Left = 103
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' phi'#7871'u/M'#227' v'#7841'ch' +
              '</B><br/>(Number/ID)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel10: THTMLabel
          Left = 103
          Top = 55
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdTTOAN3_MA: TwwDBEdit
          Left = 223
          Top = 22
          Width = 229
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'TypeInput'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 9109504
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSOTIEN05: TwwDBEdit
          Left = 223
          Top = 50
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD_TTOAN_VOUCHER: TisPanel
        Left = 1
        Top = 396
        Width = 498
        Height = 94
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 3
        Visible = False
        HeaderCaption = '  .: Phi'#7871'u qu'#224' t'#7863'ng - Gift voucher'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          94)
        object HTMLabel6: THTMLabel
          Left = 103
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>M'#227' phi'#7871'u</B><br/' +
              '>(Code)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel9: THTMLabel
          Left = 103
          Top = 55
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object wwDBEdit5: TwwDBEdit
          Left = 223
          Top = 22
          Width = 229
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'TypeInput'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 9109504
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSOTIEN06: TwwDBEdit
          Left = 223
          Top = 50
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD_TTOAN_EWALLET: TisPanel
        Left = 1
        Top = 302
        Width = 498
        Height = 94
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 4
        Visible = False
        HeaderCaption = '  .: V'#237' '#273'i'#7879'n t'#7917' - E.Wallet'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          94)
        object HTMLabel5: THTMLabel
          Left = 103
          Top = 20
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>V'#237' '#273'i'#7879'n t'#7917'</B><b' +
              'r/>E-wallet account</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel8: THTMLabel
          Left = 103
          Top = 55
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdSOTIEN03: TwwDBEdit
          Left = 223
          Top = 50
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdVidientu: TwwDBLookupCombo
          Left = 223
          Top = 22
          Width = 229
          Height = 24
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = 9109504
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TENTK'#9'19'#9'T'#234'n t'#224'i kho'#7843'n'#9'F'#9)
          DataField = 'TypeInput'
          DataSource = DsTT
          LookupTable = QrPA
          LookupField = 'MATK'
          Style = csDropDownList
          Color = clBtnFace
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          AutoDropDown = False
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object PD00: TisPanel
        Left = 1
        Top = 1
        Width = 498
        Height = 58
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 5
        Visible = False
        HeaderCaption = '  .: Ti'#7873'n m'#7863't - Cash'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          58)
        object HTMLabel11: THTMLabel
          Left = 103
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Color = cl3DLight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentColor = False
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdSOTIEN00: TwwDBEdit
          Left = 223
          Top = 17
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object Panel4: TPanel
        Left = 168
        Top = 6
        Width = 280
        Height = 287
        TabOrder = 6
        Visible = False
        object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
          Left = 1
          Top = 1
          Width = 278
          Height = 285
          AutoCompletion.Font.Charset = DEFAULT_CHARSET
          AutoCompletion.Font.Color = clWhite
          AutoCompletion.Font.Height = -19
          AutoCompletion.Font.Name = 'Tahoma'
          AutoCompletion.Font.Style = []
          AutoCompletion.Color = clBlack
          Fill.ColorMirror = clNone
          Fill.ColorMirrorTo = clNone
          Fill.GradientType = gtVertical
          Fill.GradientMirrorType = gtSolid
          Fill.BorderColor = clNone
          Fill.Rounding = 0
          Fill.ShadowOffset = 0
          Fill.Glow = gmNone
          KeyboardType = ktNUMERIC
          Keys = <
            item
              Caption = 'Back Space'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skBackSpace
              Color = 10526880
              X = 2
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '/'
              KeyValue = 111
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skDivide
              X = 71
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '*'
              KeyValue = 106
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skMultiply
              X = 140
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '-'
              KeyValue = 109
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skSubstract
              X = 209
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '7'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '8'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '9'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '+'
              KeyValue = 107
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skAdd
              X = 209
              Y = 58
              Height = 113
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '4'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '5'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '6'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '1'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '2'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '3'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = 'Enter'
              KeyValue = 13
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skReturn
              Color = 10526880
              X = 209
              Y = 172
              Height = 113
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '0'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 229
              Height = 57
              Width = 139
              SubKeys = <>
            end
            item
              Caption = '.'
              KeyValue = 110
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skDecimal
              X = 140
              Y = 229
              Height = 57
              Width = 69
              SubKeys = <>
            end>
          SmallFont.Charset = DEFAULT_CHARSET
          SmallFont.Color = clWindowText
          SmallFont.Height = -16
          SmallFont.Name = 'Tahoma'
          SmallFont.Style = []
          Version = '1.8.5.2'
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
        end
      end
      object PD_TTOAN_BANK: TisPanel
        Left = 1
        Top = 117
        Width = 498
        Height = 94
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 7
        Visible = False
        HeaderCaption = '  .: Chuy'#7875'n kho'#7843'n - Bank transfer'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          94)
        object HTMLabel12: THTMLabel
          Left = 105
          Top = 22
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>T'#224'i kho'#7843'n</B><br' +
              '/>Account name</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
          ExplicitLeft = 103
        end
        object HTMLabel13: THTMLabel
          Left = 105
          Top = 55
          Width = 110
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' ti'#7873'n</B><br/>' +
              'Amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
          ExplicitLeft = 103
        end
        object EdSOTIEN02: TwwDBEdit
          Left = 223
          Top = 50
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'SOTIEN'
          DataSource = DsTT
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBLookupCombo1: TwwDBLookupCombo
          Left = 223
          Top = 22
          Width = 229
          Height = 24
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = 9109504
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TENTK'#9'19'#9'T'#234'n t'#224'i kho'#7843'n'#9'F'#9)
          DataField = 'TypeInput'
          DataSource = DsTT
          LookupTable = QrPA
          LookupField = 'MATK'
          Style = csDropDownList
          Color = clBtnFace
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          AutoDropDown = False
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 468
      Width = 915
      Height = 65
      Align = alBottom
      TabOrder = 1
      DesignSize = (
        915
        65)
      object BtClose: TRzBitBtn
        Left = 796
        Top = 6
        Width = 106
        Height = 50
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdClose
        Caption = 'Tho'#225't'#13'Esc'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        Glyph.Data = {
          C2040000424DC204000000000000420000002800000018000000180000000100
          10000300000080040000202E0000202E00000000000000000000007C0000E003
          00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
          924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
          6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
          FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
          261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
          FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
          F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
          E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
          8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
          271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
          A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
          0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
          D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
          2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F}
        Margin = 1
        Spacing = 1
      end
      object BtContinue: TRzBitBtn
        Left = 685
        Top = 6
        Width = 106
        Height = 50
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdContinue
        Anchors = [akRight, akBottom]
        Caption = 'Ti'#7871'p t'#7909'c'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        Visible = False
        Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004280B3D09631B950C92
          29DE0EA62EFF0FAE31FF0FAE31FF0EA62EFF0C9229DE09631B9504280B3D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000031D082C0DA02AFD11B133FF18C344FF1FCE
          4AFF20D14EFF20D04DFF20D04DFF20D14EFF1FCE4AFF18C344FF11B133FF0DA0
          2AFD031D082C0000000000000000000000000000000000000000000000000000
          00000000000000000000085D17950EA32AFF1DC945FF20D04DFF1FCF4CFF1ECE
          4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECE4BFF1FCF4CFF20D14DFF1DC9
          45FF0EA32AFF085D179500000000000000000000000000000000000000000000
          000000000000076919AB10AE31FF1FD150FF1FCF50FF1ECD4FFF1DCF51FF1FD2
          4EFF1FD24EFF1DD04CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECD50FF20D0
          51FF1FD251FF10AE31FF076919AB000000000000000000000000000000000000
          00000759149511AA30FF20D353FF1ECE4FFF1ECD4EFF1CD052FF2BC04BFF0093
          0FFF009311FF2BC04CFF1DD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECE
          4AFF1ECE4FFF21D454FF11AA30FF07591495000000000000000000000000021A
          062C0D9922FF1FD150FF1ECE50FF1ECD4FFF1FD052FF14BA3DFF008F01FFFFFF
          FFFFBFEBCBFF009008FF2BC04CFF1FD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE
          4AFF1ECD4FFF1ECE50FF1FD251FF0D9922FF021A062C00000000000000000B8C
          19FD19C748FF1ECF52FF1DCD50FF1ED153FF16BF42FF008E01FFFFFDFFFFFBF7
          F9FFFFFFFFFFBBE9C8FF009108FF12BB3EFF1FD14EFF1ECE4BFF1ECE4BFF1ECE
          4AFF1DCD51FF1DCD51FF1FD053FF19C748FF0B8C19FD000000000322063D0F9F
          25FF1FD459FF1DCE53FF1ED358FF0FA72CFF008F01FFFFFBFFFFEFF1F1FFF2F4
          F2FFFAF8FAFFFFFFFFFFB5E6C0FF00910AFF13BB3EFF1FD14DFF1ECE4AFF1ECE
          4AFF1ECE4AFF1ECE49FF1DCD53FF20D55AFF0F9F25FF0322063D064F0D9515BB
          3EFF1CD058FF1CD159FF0FAD32FF009103FFFFF7FFFFEEEDEEFFF1F1F1FFF5F4
          F5FFF8F7F8FFFCFAFCFFFFFFFFFFAEE2BAFF00910AFF13BB3DFF1DD25DFF1CCF
          58FF1CCF58FF1CD25AFF1CCF57FF1CD159FF15BB3EFF064F0D9507700BDE31D4
          6AFF17CD53FF15BB42FF009102FFFFF2FFFFEAE9E9FFEFECEFFFFFF8FFFFFFF7
          FFFFFFFDFFFFFDFBFDFFFEFEFEFFFFFFFFFFABE2B8FF00910AFF14BB3CFF17D5
          5FFF16D25BFF16D15AFF1ED25BFF13CC4FFF31D46AFF07700BDE027C08FF77EA
          AAFF32D56FFF039913FFFFF1FFFFE2E3E1FFEAE8EAFFFFF7FFFF008D00FF00B5
          2BFF008B00FFFFFFFFFFFEFCFEFFFFFFFFFFFFFFFFFFABE2B8FF00910AFF14BB
          3CFF17D45FFF16D15AFF11D056FF2DD46BFF77EAAAFF027C08FF0A8712FF6CE8
          A6FF6BE09FFF019406FFFFEDFFFFF1E6F1FFFFF5FFFF008C00FF1CD860FF1ED4
          5DFF1CD860FF008B00FFFFFFFFFFFFFCFEFFFFFEFFFFFFFFFFFFABE2B8FF0091
          0AFF13BB3CFF1CD45BFF30DB74FF6BE09FFF6CE8A6FF0A800FFF037909FF6CE8
          A9FF63DE9BFF66E1A2FF008C00FFE8DFE2FF008D00FF13CB4DFF18D256FF18D0
          54FF18D155FF17D759FF008A00FFFFFFFFFFFEFCFEFFFEFEFEFFFFFFFFFFABE2
          B9FF009109FF2ABF48FF6CE4A6FF63DE9BFF6CE8A9FF037909FF006800FF68E8
          A6FF61DF9EFF67E5A8FF64E8B2FF29BC59FF50D791FF57D99AFF56D898FF55D8
          98FF56D898FF56D999FF58DFA1FF008900FFFFFFFFFFFBFAFBFFFBFBFAFFFFFF
          FFFFABE3B9FF008F05FF67E4A5FF61DF9EFF68E8A6FF006800FF005F00DE71DC
          9EFF5CE19DFF62E1A2FF54DB99FF61E4AAFF60E2A8FF5FE2A7FF5EDEA0FF68DE
          A3FF5FE2A6FF5FE2A6FF60E2A8FF67EAB6FF008700FFFFFFFFFFF9F8F9FFF7F7
          F8FFFFFDFFFFADE4BAFF009006FF5DE4A1FF71DC9EFF005F00DE0342019544B3
          61FF8DEDC2FF58E19DFF62E4A5FF5EE1A6FF5EE1A5FF5EE1A6FF5EE1A6FF5DE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF61E5ABFF008900FFFFFFFFFFF6F4
          F6FFF2F3F2FFFFFFFFFF089712FF92F0C8FF44B361FF03420195021C023D0878
          11FFA2F0D1FF74E5B1FF55DF9CFF5EE1A0FF60E1A1FF5FE1A1FF5EE1A5FF5EE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A8FF63E6AFFF008900FFFFFF
          FFFFFFF6FFFFFFFDFFFF37B956FFA2F0D1FF087811FF021C023D000000000066
          00FD52C278FFA9F1D6FF85E8BCFF54E09DFF5AE0A2FF5FE1A6FF5EE1A5FF5EE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF68EAB6FF008B
          00FF23BF5DFF2AAA3BFFADF3DBFF52C278FF006600FD00000000000000000113
          012C006600FF79E2AEFFA2EFD1FF96EBC9FF6EE3B2FF4AE09BFF5BE0A3FF5CE0
          A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE0A4FF5BE0A3FF4CE19DFF73E8
          BBFF9DEED0FFA6F1D5FF79E2AEFF006600FF0113012C00000000000000000000
          000003400195107D18FF7FE9BAFF9DEFD2FF9BECCBFF97EBCAFF7DEAC2FF6DE3
          B0FF6DE3B1FF6DE3B1FF6DE3B2FF6DE3B2FF6DE3B1FF7DEAC2FF99EBCBFF9DED
          CCFF9EEFD3FF7FE9BAFF107D18FF034001950000000000000000000000000000
          000000000000004400AB0F7817FF6DDDA7FF98EED3FF99ECCFFF98EBCDFF9EEC
          D2FFA2EED6FFA4EED8FF9EEDCCFFA3EED8FF9DEDCDFF99EBCDFF9AECD0FF98EE
          D3FF6DDDA7FF0F7817FF004400AB000000000000000000000000000000000000
          00000000000000000000043E0095006000FF46BC74FF81E7C1FF97EFD4FF9AEE
          D5FF97ECD1FF96ECCFFF96ECCFFF97ECD1FF9AEED5FF97EFD4FF81E7C1FF46BC
          74FF006000FF043E009500000000000000000000000000000000000000000000
          00000000000000000000000000000113012C005B00FD0B730FFF31A554FF5BCE
          90FF75E1B4FF86EAC4FF86EAC4FF75E1B4FF5BCE90FF31A554FF0B730FFF005B
          00FD0113012C0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000011A013D013C0095004D
          00DE005000FF006400FF006400FF005000FF004D00DE013C0095011A013D0000
          0000000000000000000000000000000000000000000000000000}
        Margin = 1
        Spacing = 1
      end
      object BtDeleted: TRzBitBtn
        Left = 573
        Top = 6
        Width = 106
        Height = 50
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdDeleted
        Anchors = [akRight, akBottom]
        Caption = 'X'#243'a thanh to'#225'n'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        Visible = False
        Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004280B3D09631B950C92
          29DE0EA62EFF0FAE31FF0FAE31FF0EA62EFF0C9229DE09631B9504280B3D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000031D082C0DA02AFD11B133FF18C344FF1FCE
          4AFF20D14EFF20D04DFF20D04DFF20D14EFF1FCE4AFF18C344FF11B133FF0DA0
          2AFD031D082C0000000000000000000000000000000000000000000000000000
          00000000000000000000085D17950EA32AFF1DC945FF20D04DFF1FCF4CFF1ECE
          4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECE4BFF1FCF4CFF20D14DFF1DC9
          45FF0EA32AFF085D179500000000000000000000000000000000000000000000
          000000000000076919AB10AE31FF1FD150FF1FCF50FF1ECD4FFF1DCF51FF1FD2
          4EFF1FD24EFF1DD04CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECD50FF20D0
          51FF1FD251FF10AE31FF076919AB000000000000000000000000000000000000
          00000759149511AA30FF20D353FF1ECE4FFF1ECD4EFF1CD052FF2BC04BFF0093
          0FFF009311FF2BC04CFF1DD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECE
          4AFF1ECE4FFF21D454FF11AA30FF07591495000000000000000000000000021A
          062C0D9922FF1FD150FF1ECE50FF1ECD4FFF1FD052FF14BA3DFF008F01FFFFFF
          FFFFBFEBCBFF009008FF2BC04CFF1FD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE
          4AFF1ECD4FFF1ECE50FF1FD251FF0D9922FF021A062C00000000000000000B8C
          19FD19C748FF1ECF52FF1DCD50FF1ED153FF16BF42FF008E01FFFFFDFFFFFBF7
          F9FFFFFFFFFFBBE9C8FF009108FF12BB3EFF1FD14EFF1ECE4BFF1ECE4BFF1ECE
          4AFF1DCD51FF1DCD51FF1FD053FF19C748FF0B8C19FD000000000322063D0F9F
          25FF1FD459FF1DCE53FF1ED358FF0FA72CFF008F01FFFFFBFFFFEFF1F1FFF2F4
          F2FFFAF8FAFFFFFFFFFFB5E6C0FF00910AFF13BB3EFF1FD14DFF1ECE4AFF1ECE
          4AFF1ECE4AFF1ECE49FF1DCD53FF20D55AFF0F9F25FF0322063D064F0D9515BB
          3EFF1CD058FF1CD159FF0FAD32FF009103FFFFF7FFFFEEEDEEFFF1F1F1FFF5F4
          F5FFF8F7F8FFFCFAFCFFFFFFFFFFAEE2BAFF00910AFF13BB3DFF1DD25DFF1CCF
          58FF1CCF58FF1CD25AFF1CCF57FF1CD159FF15BB3EFF064F0D9507700BDE31D4
          6AFF17CD53FF15BB42FF009102FFFFF2FFFFEAE9E9FFEFECEFFFFFF8FFFFFFF7
          FFFFFFFDFFFFFDFBFDFFFEFEFEFFFFFFFFFFABE2B8FF00910AFF14BB3CFF17D5
          5FFF16D25BFF16D15AFF1ED25BFF13CC4FFF31D46AFF07700BDE027C08FF77EA
          AAFF32D56FFF039913FFFFF1FFFFE2E3E1FFEAE8EAFFFFF7FFFF008D00FF00B5
          2BFF008B00FFFFFFFFFFFEFCFEFFFFFFFFFFFFFFFFFFABE2B8FF00910AFF14BB
          3CFF17D45FFF16D15AFF11D056FF2DD46BFF77EAAAFF027C08FF0A8712FF6CE8
          A6FF6BE09FFF019406FFFFEDFFFFF1E6F1FFFFF5FFFF008C00FF1CD860FF1ED4
          5DFF1CD860FF008B00FFFFFFFFFFFFFCFEFFFFFEFFFFFFFFFFFFABE2B8FF0091
          0AFF13BB3CFF1CD45BFF30DB74FF6BE09FFF6CE8A6FF0A800FFF037909FF6CE8
          A9FF63DE9BFF66E1A2FF008C00FFE8DFE2FF008D00FF13CB4DFF18D256FF18D0
          54FF18D155FF17D759FF008A00FFFFFFFFFFFEFCFEFFFEFEFEFFFFFFFFFFABE2
          B9FF009109FF2ABF48FF6CE4A6FF63DE9BFF6CE8A9FF037909FF006800FF68E8
          A6FF61DF9EFF67E5A8FF64E8B2FF29BC59FF50D791FF57D99AFF56D898FF55D8
          98FF56D898FF56D999FF58DFA1FF008900FFFFFFFFFFFBFAFBFFFBFBFAFFFFFF
          FFFFABE3B9FF008F05FF67E4A5FF61DF9EFF68E8A6FF006800FF005F00DE71DC
          9EFF5CE19DFF62E1A2FF54DB99FF61E4AAFF60E2A8FF5FE2A7FF5EDEA0FF68DE
          A3FF5FE2A6FF5FE2A6FF60E2A8FF67EAB6FF008700FFFFFFFFFFF9F8F9FFF7F7
          F8FFFFFDFFFFADE4BAFF009006FF5DE4A1FF71DC9EFF005F00DE0342019544B3
          61FF8DEDC2FF58E19DFF62E4A5FF5EE1A6FF5EE1A5FF5EE1A6FF5EE1A6FF5DE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF61E5ABFF008900FFFFFFFFFFF6F4
          F6FFF2F3F2FFFFFFFFFF089712FF92F0C8FF44B361FF03420195021C023D0878
          11FFA2F0D1FF74E5B1FF55DF9CFF5EE1A0FF60E1A1FF5FE1A1FF5EE1A5FF5EE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A8FF63E6AFFF008900FFFFFF
          FFFFFFF6FFFFFFFDFFFF37B956FFA2F0D1FF087811FF021C023D000000000066
          00FD52C278FFA9F1D6FF85E8BCFF54E09DFF5AE0A2FF5FE1A6FF5EE1A5FF5EE1
          A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF68EAB6FF008B
          00FF23BF5DFF2AAA3BFFADF3DBFF52C278FF006600FD00000000000000000113
          012C006600FF79E2AEFFA2EFD1FF96EBC9FF6EE3B2FF4AE09BFF5BE0A3FF5CE0
          A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE0A4FF5BE0A3FF4CE19DFF73E8
          BBFF9DEED0FFA6F1D5FF79E2AEFF006600FF0113012C00000000000000000000
          000003400195107D18FF7FE9BAFF9DEFD2FF9BECCBFF97EBCAFF7DEAC2FF6DE3
          B0FF6DE3B1FF6DE3B1FF6DE3B2FF6DE3B2FF6DE3B1FF7DEAC2FF99EBCBFF9DED
          CCFF9EEFD3FF7FE9BAFF107D18FF034001950000000000000000000000000000
          000000000000004400AB0F7817FF6DDDA7FF98EED3FF99ECCFFF98EBCDFF9EEC
          D2FFA2EED6FFA4EED8FF9EEDCCFFA3EED8FF9DEDCDFF99EBCDFF9AECD0FF98EE
          D3FF6DDDA7FF0F7817FF004400AB000000000000000000000000000000000000
          00000000000000000000043E0095006000FF46BC74FF81E7C1FF97EFD4FF9AEE
          D5FF97ECD1FF96ECCFFF96ECCFFF97ECD1FF9AEED5FF97EFD4FF81E7C1FF46BC
          74FF006000FF043E009500000000000000000000000000000000000000000000
          00000000000000000000000000000113012C005B00FD0B730FFF31A554FF5BCE
          90FF75E1B4FF86EAC4FF86EAC4FF75E1B4FF5BCE90FF31A554FF0B730FFF005B
          00FD0113012C0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000011A013D013C0095004D
          00DE005000FF006400FF006400FF005000FF004D00DE013C0095011A013D0000
          0000000000000000000000000000000000000000000000000000}
        ImageIndex = 2
        Images = DataMain.ImageNavi
        Margin = 1
        Spacing = 1
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 415
      Height = 468
      Align = alClient
      TabOrder = 2
      object isPanel3: TisPanel
        Left = 1
        Top = 392
        Width = 413
        Height = 75
        Align = alBottom
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = '  :: T'#7893'ng c'#7897'ng - Total'
        HeaderColor = clScrollBar
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clPurple
        HeaderFont.Height = -15
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          413
          75)
        object HTMLabel7: THTMLabel
          Left = 33
          Top = 28
          Width = 102
          Height = 38
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>T'#7893'ng ti'#7873'n nh'#7853'n</' +
              'B><br/>Advance deposit</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
          ExplicitLeft = 118
        end
        object wwDBEdit23: TwwDBEdit
          Left = 141
          Top = 30
          Width = 250
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = 15794175
          Ctl3D = False
          DataField = 'CALC_CHUA_THOI'
          DataSource = FrmMain.DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object GrList: TwwDBGrid2
        Left = 1
        Top = 1
        Width = 413
        Height = 391
        TabStop = False
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'LK_SALENAME;CustomEdit;EdTHUNGAN;T')
        Selected.Strings = (
          'STT'#9'3'#9'STT'#9'F'
          'LK_PTTT'#9'20'#9'H'#236'nh th'#7913'c'#9'F'
          'SOTIEN'#9'13'#9'S'#7889' ti'#7873'n'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsTT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = []
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowFooter]
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 2237106
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = False
        UseTFields = False
        LineColors.ShadowColor = clSilver
        FooterColor = 13360356
        FooterCellColor = 13360356
        PadColumnStyle = pcsPadHeader
      end
    end
  end
  object ActionList1: TActionList
    Left = 349
    Top = 489
    object CmdReturn: TAction
      Caption = 'In'
      ShortCut = 115
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      ShortCut = 27
      OnExecute = CmdCloseExecute
    end
    object CmdDelivery: TAction
      Caption = 'Giao h'#224'ng'
      ShortCut = 114
    end
    object CmdTTOAN1: TAction
      Hint = 'Thanh to'#225'n - Ti'#7873'n m'#7863't'
    end
    object CmdTTOAN50: TAction
      Hint = 'Thanh to'#225'n VNPAY'
    end
    object CmdContinue: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      OnExecute = CmdContinueExecute
    end
    object CmdDeleted: TAction
      Caption = 'X'#243'a thanh to'#225'n'
      OnExecute = CmdDeletedExecute
    end
  end
  object QrTT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTTBeforeOpen
    AfterInsert = QrTTAfterInsert
    BeforePost = QrTTBeforePost
    AfterScroll = QrTTAfterScroll
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM_TTOAN'
      ' where'#9'KHOA =:KHOA')
    Left = 52
    Top = 228
    object QrTTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrTTPTTT: TWideStringField
      FieldName = 'PTTT'
      Size = 2
    end
    object QrTTSOTIEN: TFloatField
      DisplayLabel = 'S'#7889' ti'#7873'n'
      FieldName = 'SOTIEN'
    end
    object QrTTTypeInput: TWideStringField
      FieldName = 'TypeInput'
      OnChange = QrTTTypeInputChange
      Size = 30
    end
    object QrTTTypeCode: TWideStringField
      FieldName = 'TypeCode'
      Size = 30
    end
    object QrTTTypeBalance: TFloatField
      FieldName = 'TypeBalance'
    end
    object QrTTTypeID: TGuidField
      FieldName = 'TypeID'
      FixedChar = True
      Size = 38
    end
    object QrTTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrTTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTTTTOAN_CASH: TFloatField
      FieldName = 'TTOAN_CASH'
    end
    object QrTTTTOAN_CARD: TFloatField
      FieldName = 'TTOAN_CARD'
    end
    object QrTTTTOAN_TRAHANG: TFloatField
      FieldName = 'TTOAN_TRAHANG'
    end
    object QrTTTTOAN_VOUCHER: TFloatField
      FieldName = 'TTOAN_VOUCHER'
    end
    object QrTTTTOAN_EWALLET: TFloatField
      FieldName = 'TTOAN_EWALLET'
    end
    object QrTTTHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrTTCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrTTCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrTTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrTTRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrTTLK_PTTT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = QrPTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Lookup = True
    end
    object QrTT_id: TLargeintField
      FieldName = '_id'
    end
    object QrTTTTOAN_BANK: TFloatField
      FieldName = 'TTOAN_BANK'
    end
    object QrTTTTOAN_MEMBER: TFloatField
      FieldName = 'TTOAN_MEMBER'
    end
  end
  object DsTT: TDataSource
    AutoEdit = False
    DataSet = QrTT
    Left = 52
    Top = 256
  end
  object QrPTTT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_DM_PT_THANHTOAN_BL')
    Left = 92
    Top = 228
  end
  object QrPA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrPABeforeOpen
    Parameters = <
      item
        Name = 'PTTT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DM_TAIKHOAN'
      'where PTTT=:PTTT')
    Left = 204
    Top = 236
    object QrPATENTK: TWideStringField
      DisplayLabel = 'T'#234'n t'#224'i kho'#7843'n'
      DisplayWidth = 19
      FieldName = 'TENTK'
      Size = 100
    end
    object QrPAMATK: TWideStringField
      DisplayWidth = 30
      FieldName = 'MATK'
      Visible = False
      Size = 30
    end
  end
end
