﻿program CashierZ;

uses
  Forms,
  MainData in 'MainData.pas' {DataMain: TDataModule},
  ExCommon in 'ExCommon.pas',
  CkMa in 'CkMa.pas' {FrmCkma},
  SoBill in 'SoBill.pas' {FrmSoBill},
  PosCommoInfo in 'PosCommoInfo.pas' {FrmCommoInfo},
  Printer in 'Printer.pas' {FrmPrinter},
  TempReceipt in 'TempReceipt.pas' {FrmTempReceipt},
  ReceiptDesc in 'ReceiptDesc.pas' {FrmReceiptDesc},
  exResStr in 'exResStr.pas',
  PosMain in 'PosMain.pas' {FrmMain},
  PosVIP in 'PosVIP.pas' {FrmPosVIP},
  CkBill in 'CkBill.pas' {FrmChietkhau},
  Retailer in 'Retailer.pas' {FrmRetailer},
  Payment in 'Payment.pas' {FrmPayment},
  frameNavi in 'frameNavi.pas' {frNavi: TFrame},
  TheLenh in 'TheLenh.pas' {FrmThelenh},
  CR208U in 'CR208U.pas',
  exCOMPort in 'exCOMPort.pas',
  SystemCriticalU in 'SystemCriticalU.pas',
  ThongtinCN in 'ThongtinCN.pas' {FrmThongtinCN},
  exPrintBill in 'exPrintBill.pas',
  RepEngine in 'RepEngine.pas' {FrmRep},
  exPrintBill2 in 'exPrintBill2.pas',
  PosChitiet in 'PosChitiet.pas' {FrmPosChitiet},
  KeypadNumeric in 'KeypadNumeric.pas' {FrmKeypadNumeric},
  SetGhichu in 'SetGhichu.pas' {FrmSetGhichu},
  frameKho in 'frameKho.pas' {frKHO: TFrame},
  ThongtinCN_View in 'ThongtinCN_View.pas' {FrmThongtinCN_View},
  FastReport in 'FastReport.pas' {FrmFastReport: TDataModule},
  CashierMain in 'CashierMain.pas' {FrmCashierMain},
  CashierTableOrders in 'CashierTableOrders.pas' {FrmCashierTableOrders},
  FB_TableOrders in 'FB_TableOrders.pas' {FrmFB_TableOrders},
  FB_Tables in 'FB_Tables.pas' {FrmFB_Tables},
  SoKhach in 'SoKhach.pas' {FrmSoKhach},
  Pop in 'Pop.pas' {FrmPop},
  PopTable in 'PopTable.pas' {FrmPopTable},
  PaymentMultiple in 'PaymentMultiple.pas' {FrmPaymentMultiple},
  PosKetCa in 'PosKetCa.pas' {FrmPosKetca},
  FB_ChonDsma in 'FB_ChonDsma.pas' {FrmFB_ChonDsma},
  FB_Banle in '..\FBZ\FB_Banle.pas' {FrmFB_Banle};

{$R *.RES}

begin
    Application.Initialize;
    Application.Title := 'Bán lẻ';
    Application.CreateForm(TFrmCashierMain, FrmCashierMain);
  Application.CreateForm(TFrmRep, FrmRep);
  Application.CreateForm(TFrmFastReport, FrmFastReport);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TFrmPop, FrmPop);
  Application.CreateForm(TFrmFB_ChonDsma, FrmFB_ChonDsma);
  if not DataMain.Logon(1) then
    begin
        Application.Terminate;
        Exit;
    end;

    Application.CreateForm(TFrmPosKetca, FrmPosKetca);
    if not DataMain.PosKetCa(0, 0) then
    begin
        if not FrmPosKetca.Execute(True) then
        begin
            Application.Terminate;
            Exit;
        end;
    end;
	
  Application.Run;
end.
