﻿unit CashierMain;

interface

uses
    Windows, SysUtils, Classes, Graphics, Forms, ActnList, Menus, ExtCtrls,
    StdCtrls, Wwdbigrd, Wwdbgrid2, Mask, Db, ADODB, wwdblook, isEnv, AppEvnts,
    AdvMenus, fcStatusBar, RzPanel, Controls, wwdbedit, HTMLabel,
    RzButton, isDb, ImgList, Grids, Wwdbgrid, pngimage, isOneInstance,
    exPrintBill,
    AdvGlassButton, AdvGlowButton, AdvSmoothButton, AdvMetroButton,
    AdvSelectors,
    AdvOfficeSelectors, wwdbdatetimepicker, RzSplit, AdvSmoothTileList,
    AdvSmoothTileListEx, GDIPPictureContainer, AdvAlertWindow,
    AdvSmoothTileListImageVisualizer, AdvSmoothTileListHTMLVisualizer,
    AdvOfficeStatusBar, AdvOfficeStatusBarStylers, rImprovedComps,
    AdvSmoothLabel, StrUtils, AdvTouchKeyboard, AdvSmoothTouchKeyBoard,
    DBAdvSmoothLabel, DateUtils, GDIPFill, GDIPAPI, GDIPOBJ, Vcl.Buttons,
  dbhtmlab, Vcl.DBCtrls, isPanel;

type
    TFrmCashierMain = class(TForm)
    PaMain: TPanel;
    PaRight: TPanel;
        Panel2: TPanel;
        TtKhuVuc: TAdvSmoothTileListEx;
        Panel5: TPanel;
        AdvGlowButton2: TAdvGlowButton;
        MyActionList: TActionList;
        ApplicationEvents1: TApplicationEvents;
        CmdTableOrders: TAction;
        CmdOrders: TAction;
        CmdChangeTable: TAction;
        CmdMergeTable: TAction;
        CmdSplitTable: TAction;
        CmdClose: TAction;
        QrDmKhuVuc: TADOQuery;
        AdvSmoothTileListHTMLVisualizer1: TAdvSmoothTileListHTMLVisualizer;
        TtKhuVucBan: TAdvSmoothTileListEx;
        Panel6: TPanel;
        AdvGlowButton3: TAdvGlowButton;
        CmdRefresh: TAction;
    PaLeft: TPanel;
    Panel9: TPanel;
    QrNhomCamUng: TADOQuery;
    Panel12: TPanel;
    GrDetail: TwwDBGrid2;
    Panel13: TPanel;
    Panel14: TPanel;
    QrCamUng: TADOQuery;
    CmdSendOrder: TAction;
    CmdPayment: TAction;
    LbPanel: TAdvSmoothLabel;
    RzPanel1: TRzPanel;
    LbTable: TAdvSmoothLabel;
    EdTable: TDBAdvSmoothLabel;
    DBAdvSmoothLabel1: TDBAdvSmoothLabel;
    lbDbState: TAdvSmoothLabel;
    CmdSelect: TAction;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    QrBH: TADOQuery;
    QrBHCALC_TONGCK: TFloatField;
    QrBHCALC_TIENTHOI: TFloatField;
    QrBHLK_TENKHO: TWideStringField;
    QrBHLCT: TWideStringField;
    QrBHNGAY: TDateTimeField;
    QrBHQUAY: TWideStringField;
    QrBHCA: TWideStringField;
    QrBHSCT: TWideStringField;
    QrBHMAVIP: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHCHIETKHAU_MH: TFloatField;
    QrBHSOTIEN: TFloatField;
    QrBHSOLUONG: TFloatField;
    QrBHCHUATHOI: TFloatField;
    QrBHTHANHTOAN: TFloatField;
    QrBHDGIAI: TWideMemoField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_BY: TIntegerField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHKHOA: TGuidField;
    QrBHTINHTRANG: TWideStringField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHLK_TENTK: TWideStringField;
    QrBHLK_DAIDIEN: TWideStringField;
    QrBHLK_NGANHANG: TWideStringField;
    QrBHPRINT_NO: TIntegerField;
    QrBHLOC: TWideStringField;
    QrBHDELIVERY: TBooleanField;
    QrBHTIENTHOI: TFloatField;
    QrBHLK_TENVIP: TWideStringField;
    QrBHLK_VIP_DTHOAI: TWideStringField;
    QrBHLK_VIP_DCHI: TWideStringField;
    QrBH_id: TLargeintField;
    QrBHMAQUAY: TWideStringField;
    QrBHMABAN: TWideStringField;
    QrBHSOLUONG_KHACH: TFloatField;
    QrCTBH: TADOQuery;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHTENTAT: TWideStringField;
    QrCTBHLK_TENVT: TWideStringField;
    QrCTBHLK_TENVT_KHONGDAU: TWideStringField;
    QrCTBHLK_TENTAT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHSOTIEN: TFloatField;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHRSTT: TIntegerField;
    QrCTBHCALC_SOTIEN_SAUCK: TFloatField;
    QrCTBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    DsCTBH: TDataSource;
    POS_GIABAN: TADOCommand;
    GetUPDATE_DATE: TADOCommand;
    DsBH: TDataSource;
    QrCTBHLOC: TWideStringField;
    QrCTBHBO: TBooleanField;
    QrBHHINHTHUC_GIA: TWideStringField;
    vlTotal1: TisTotal;
    AdvSmoothTileListHTMLVisualizer2: TAdvSmoothTileListHTMLVisualizer;
    spDmBan: TADOStoredProc;
    CmdWinKeyboard: TAction;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHUPDATE_BY: TIntegerField;
    QrCTBHDVT2: TWideStringField;
    isOneInstance1: TisOneInstance;
    CmdCustomer: TAction;
    QrCTBHCREATE_BY: TIntegerField;
    QrCTBHUPDATE_BY: TIntegerField;
    QrCTBHCREATE_DATE: TDateTimeField;
    QrCTBHUPDATE_DATE: TDateTimeField;
    spDmBanTemp: TADOStoredProc;
    Panel23: TPanel;
    AdvGlowButton10: TAdvGlowButton;
    CmdShowImage: TAction;
    QrCTBHLK_BO: TBooleanField;
    CmdRefreshMenu: TAction;
    QrCTBHORDER_NO: TIntegerField;
    GET_ORDER_NO: TADOCommand;
    QrBHCKHD_BY: TIntegerField;
    QrBHTL_CKVIPDM: TFloatField;
    QrBHTL_CKHD: TFloatField;
    QrCTBHSOLUONG_DAT: TFloatField;
    QrBHSOLUONG_DAT: TFloatField;
    Panel1: TPanel;
    AdvGlowButton11: TAdvGlowButton;
    Panel8: TPanel;
    AdvGlowButton13: TAdvGlowButton;
    Panel20: TPanel;
    btnMore: TAdvGlowButton;
    CmdMore: TAction;
    Panel21: TPanel;
    AdvGlowButton8: TAdvGlowButton;
    CmdPrintMore: TAction;
    Panel4: TPanel;
    Panel18: TPanel;
    AdvGlowButton7: TAdvGlowButton;
    Panel3: TPanel;
    Panel16: TPanel;
    AdvGlowButton1: TAdvGlowButton;
    Panel17: TPanel;
    AdvGlowButton5: TAdvGlowButton;
    CmdCalTemp: TAction;
    CmdListTableOrders: TAction;
    QrBHNGAYVAO: TDateTimeField;
    QrBHQUAY_ORDER: TWideStringField;
    QrBHMAQUAY_ORDER: TWideStringField;
    Panel7: TPanel;
    CmdAreaPrev: TAction;
    CmdAreaNext: TAction;
    BtnPrev: TBitBtn;
    BitBtn1: TBitBtn;
    Panel19: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    QrCTBHMAKHO: TWideStringField;
    QrCTBHTHOIGIAN_CHEBIEN: TFloatField;
    QrCTBHORDER_BY: TIntegerField;
    QrCTBHORDER_DATE: TDateTimeField;
    CmdAreaTablePrev: TAction;
    CmdAreaTableNext: TAction;
    CmdGroupMenuNext: TAction;
    CmdGroupMenuPrev: TAction;
    CmdMenuNext: TAction;
    CmdMenuPrev: TAction;
    Panel22: TPanel;
    QrCTBH_id: TLargeintField;
    Panel10: TPanel;
    Panel11: TPanel;
    TtNganhhang: TAdvSmoothTileListEx;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    TtVattu: TAdvSmoothTileListEx;
    QrCTBHCALC_THOIGIAN_CONLAI: TFloatField;
    QrCTBHTEXT_THOIGIAN_CHUYENCHEBIEN: TWideStringField;
    QrCTBHTEXT_THOIGIAN_CHEBIEN: TWideStringField;
    QrCTBHTEXT_THOIGIAN_CONLAI: TWideStringField;
    isPanel1: TisPanel;
    DBText4: TDBText;
    isPanel2: TisPanel;
    DBText1: TDBText;
    isPanel3: TisPanel;
    DBText5: TDBText;
    Panel15: TPanel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    QrBHTL_PHUTHU: TFloatField;
    QrBHCO_HOADON: TBooleanField;
    CmdEndOfShift: TAction;
    FB_CASHIER_DELETE_ORDER: TADOCommand;
    CmdScanVIP: TAction;
    QrBHCN_GHICHU: TWideStringField;
    QrBHMAVIP_HOTEN: TWideStringField;
    POS_VIP: TADOCommand;
    CmdBillList: TAction;
    QrBHORDER_BY: TIntegerField;
    QrBHORDER_DATE: TDateTimeField;
    QrBHPHUTHU: TFloatField;
    QrBHTHUCTRA: TFloatField;
        procedure CmdQuitExecute(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure CmdSetpassExecute(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure FormClose(Sender: TObject; var Action: TCloseAction);
        procedure CmdReprintExecute(Sender: TObject);
        procedure QrDBError(DataSet: TDataSet; E: EDatabaseError;
          var Action: TDataAction);

        procedure CmdHelpExecute(Sender: TObject);
        procedure CmdSendOrderExecute(Sender: TObject);

        procedure CmdLockExecute(Sender: TObject);
        procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
        procedure CmdReconnectExecute(Sender: TObject);
        procedure CmdReLoadExecute(Sender: TObject);
        procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
          var Action: TDataAction);
        procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
        procedure CmdCloseExecute(Sender: TObject);
        procedure CmdTableOrdersExecute(Sender: TObject);
        procedure CmdMergeTableExecute(Sender: TObject);
        procedure CmdSplitTableExecute(Sender: TObject);
        procedure TtKhuVucTileClick(Sender: TObject; Tile: TAdvSmoothTile;
          State: TTileState);
        procedure CmdChangeTableExecute(Sender: TObject);
        procedure TtKhuVucBanTileClick(Sender: TObject; Tile: TAdvSmoothTile;
          State: TTileState);
        procedure TtKhuVucBanTileDblClick(Sender: TObject; Tile: TAdvSmoothTile;
          State: TTileState);
        procedure CmdRefreshExecute(Sender: TObject);
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TtNganhhangTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure CmdPaymentExecute(Sender: TObject);
    procedure CmdSelectExecute(Sender: TObject);
    procedure TtVattuTileDblClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure QrBHBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure QrCTBHAfterPost(DataSet: TDataSet);
    procedure QrBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHMAVTChange(Sender: TField);
    procedure QrCTBHSOLUONGChange(Sender: TField);
    procedure QrCTBHSOTIENChange(Sender: TField);
    procedure AdvSmoothTouchKeyBoard1KeyClick(Sender: TObject; Index: Integer);
    procedure CmdWinKeyboardExecute(Sender: TObject);
    procedure CmdCustomerExecute(Sender: TObject);
    procedure CmdShowImageExecute(Sender: TObject);
    procedure GrDetailDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure TtKhuVucBanTileFill(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState; Fill: TGDIPFill);
    
    procedure TtKhuVucBanTileFont(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState; AFont: TFont);
    procedure CmdRefreshMenuExecute(Sender: TObject);
    procedure TtNganhhangDblClick(Sender: TObject);
    procedure TtFillterMouseLeave(Sender: TObject);
    
    procedure TtFillterTileEnter(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure TtFillterMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CmdMoreExecute(Sender: TObject);
    procedure CmdPrintMoreExecute(Sender: TObject);
    procedure CmdCalTempExecute(Sender: TObject);
    procedure CmdListTableOrdersExecute(Sender: TObject);
    procedure QrCTBHSOLUONG_DATChange(Sender: TField);
    procedure AdvGlowButton14MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CmdAreaPrevExecute(Sender: TObject);
    procedure CmdAreaNextExecute(Sender: TObject);
    procedure CmdAreaTablePrevExecute(Sender: TObject);
    procedure CmdAreaTableNextExecute(Sender: TObject);
    procedure CmdMenuPrevExecute(Sender: TObject);
    procedure CmdMenuNextExecute(Sender: TObject);
    procedure CmdGroupMenuPrevExecute(Sender: TObject);
    procedure GrDetailCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure TtKhuVucBanTileDeleteIndicator(Sender: TObject;
      Tile: TAdvSmoothTile; State: TTileState; var Indicator: string);

    procedure CmdGroupMenuNextExecute(Sender: TObject);
    procedure QrCTBHSOLUONGValidate(Sender: TField);
    procedure CmdEndOfShiftExecute(Sender: TObject);
    procedure CmdScanVIPExecute(Sender: TObject);
    procedure CmdBillListExecute(Sender: TObject);
    procedure QrCTBHAfterDelete(DataSet: TDataSet);
    procedure TtVattuTileFill(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState; Fill: TGDIPFill);

    

    private
        mLastDate, mLastRead: TDateTime;
        // Hóa đơn hiện tại
        curKhoa: TGUID;

        procedure tbAddUnit;
        procedure tbAddGroup;
        procedure tbAddUnitBottom;
        procedure tbAddGroupBottom;
        procedure OpenQueries;
    public
        mSqlTam: String;
        // Thông tin mặt hàng scan sau cùng
        curStt, fIndexCurrent, mTINHTRANG, seconds,  pX, Py: Integer;
        curSoluong, curGiaban, curTlck7, curTlck3, curThsuat, curSlBo,
          curTlckBo: Double;
        curLThue, mVipTen, mMABAN, mMAKV, mTENBAN, mLCT, fPathNoImage: String;
        mDefSoluong, tyLePhuThu: Double;
        isAction, isShowImage, isMain, toGo, isSelectTable: Boolean;
        emptyColor, emptyBackground: TColor;
        hoverColor, hoverBackground: TColor;
        orderColor, orderBackground: TColor;
        activeColor, activeBackground: TColor;

        mPrinter: TexPrintBill;

        function Total(pReOrder: Boolean = False): Boolean ;
        procedure InvalidAction(msg: String = '');
        function  GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
        procedure ItemAddNew(const ma: String; const pMabo: String = '');
        procedure ItemSetQty(const pSoluong: Double);
        function  RefreshSelectTable(mMABAN: String): Boolean;
        function  updateContentTable(fIndex: Integer): Boolean;
        function drawContentTable(spStore: TADOStoredProc; ttTileList: TAdvSmoothTileListEx; fIndex: Integer): Boolean;
        function updateOrderNo(mQuery: TADOQuery; orderNo: Integer): Boolean;
        function updateArea(TtList: TAdvSmoothTileListEx; viewFontColor, viewBackgroundColor,
    hoverFontColor, hoverBackgroundColor, selectedFontColor, selectedBackgroundColor: TColor): Boolean;
        procedure selectTab(isTable: Boolean = True);

        function updateAreaTable(TtList: TAdvSmoothTileListEx;
        viewFontColor, viewBackgroundColor, viewBorderColor,
        hoverFontColor, hoverBackgroundColor, hoverBorderColor,
        selectedFontColor, selectedBackgroundColor, selectedBorderColor: TColor): Boolean;

        function smoothTitleBackNext(TtList: TAdvSmoothTileListEx; bNext: Boolean = True): Boolean;
        function enabledButtonBackNext(TtList: TAdvSmoothTileListEx; buttonPrev, buttonNext: TAction): Boolean;

        procedure PrintBill(pKhoa: TGUID);
        function  deleteOrderByKey(pKhoa: TGUID): Boolean;

        procedure ProcScanVIP;

        function  getIndexTileListByValue(sMA: string; smoothTileList: TAdvSmoothTileListEx): Integer;
    end;

var
    FrmCashierMain: TFrmCashierMain;

implementation

uses
    MainData, ExCommon, isMsg, isStr, SoBill, Rights, CkBill, isDba, isOdbc,
    isLib, PosCommoInfo, RepEngine, Printer, TempReceipt, CkMa,
    ReceiptDesc, isCommon, exResStr, PosVIP, Retailer, Payment,
    Variants, ADOInt, GuidEx, TheLenh, exCOMPort, SystemCriticalU,
    PosChitiet, Banle, SetGhichu, ThongtinCN, BanleCN,
    PosMain, CashierTableOrders, FB_TableOrders, SoKhach, Pop,
    PopTable, PosKetCa, FB_Banle;

{$R *.DFM}

const
    FORM_CODE = 'FB_POS_BANHANG';

{$REGION '<< Form events >>'}

    (* ==============================================================================
      **------------------------------------------------------------------------------
    *)
procedure TFrmCashierMain.FormCreate(Sender: TObject);
begin
    seconds := 10;
    mLastRead := Now;
    EdTable.Caption.Text := '';
    DBAdvSmoothLabel1.Caption.Text := '';
    Panel9.Visible := True;
    Panel10.Visible := False;
    Panel9.Align := alClient;
    Panel10.Align := alClient;

    emptyColor := clWindowText ;
    emptyBackground := clWhite;

    hoverColor := clBlue;
    hoverBackground := clBlue;


    orderColor := clWhite;
    orderBackground := clGrayText;

    activeColor := clWhite;
    activeBackground := clTeal;

    fPathNoImage := sysFolderImageWeb + '\System\IMG\cooking_loader.gif';
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.FormShow(Sender: TObject);
var
    bRet: Boolean;
begin
    TMyForm(Self).Init2;
    LoadCustomIcon;
    DbeInitial;
    FlexInitial;

    // Other init
    SetDefButton(1);

    ShortTimeFormat := 'hh:nn';
    mTrigger := False;
    mLastDate := Now;
    isMain := True;

    mLCT := 'FBLE';

    // Init bill printer
    exBillInitial(mPrinter, '', 'a.PRINTER_TEMP = b.PRINTER');

    // Open database
    with DataMain do
    begin
        OpenDatasets([QrDMKHO, QrUser, QrDMTK, QrLOAI_VIDIENTU]);
    end;

    with spDmBan do
    begin
        if Active then
            Close;

        ExecProc;
    end;

    with spDmBanTemp do
    begin
        if Active then
            Close;

        ExecProc;
    end;

    OpenDatasets([QrDmKhuVuc, spDmBan, spDmBanTemp, QrNhomCamUng, QrCamUng]);

    // Tuy bien luoi
    SetCustomGrid(FORM_CODE + '_MAIN', GrDetail);

    // Display format
	SetDisplayFormat(QrBH, sysCurFmt);
    SetDisplayFormat(QrBH,['SOLUONG', 'SOLUONG_DAT'], sysQtyFmt);
    SetShortDateFormat(QrBH);

    with QrCTBH do
    begin
		SetDisplayFormat(QrCTBH, sysCurFmt);
        SetDisplayFormat(QrCTBH,['SOLUONG', 'SOLUONG_DAT'], sysQtyFmt);
    end;

    // Touch
    with TtKhuVuc do
    begin
        Columns := 4;
        Rows := 1;
    end;

    with TtKhuVucBan do
    begin
        Columns := 4;
        Rows := 6;
    end;

    updateArea(TtKhuVuc, clWindowText, clNone, clWindowText, $0000000f, clWhite, 10903848);
    updateAreaTable(TtKhuVucBan, clWindowText, clWhite, clGray , clWindowText, clWhite, clTeal, clWhite, clTeal, clTeal);
    updateArea(TtNganhhang, clWindowText, clNone, clWindowText, $0000000f, clWhite, 10903848);


    tbAddGroup;
    tbAddUnit;

    TtKhuVuc.SelectTile(0);
    TtKhuVucTileClick(TtKhuVuc, TtKhuVuc.Tiles[0], tsNormal);


    with TtNganhhang do
    begin
        Columns := 4;
        Rows := 1;
    end;

    with TtVattu do
    begin
        Columns := 5;
        Rows := 3;
    end;

    tbAddGroupBottom();
    tbAddUnitBottom();

    TtNganhhang.SelectTile(0);
    TtNganhhangTileClick(TtNganhhang, TtNganhhang.Tiles[0], tsNormal);
    isSelectTable := True;

    // Form initial
    //SetApplicationVersion;

    // Create System ODBC data sources
     isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer,
      isDatabaseServer);

    // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');

    TExComPort.IcdComCreate(GetSysParam('ICD_STR1'), GetSysParam('ICD_STR2'));
    SystemCritical.IsCritical := True;


//    if not DataMain.PosKetCa(0, 0) then
//    begin
//        bRet := FrmPosKetca.Execute(True);
//        if not bRet then
//            Close;
//    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    isAction := (mTINHTRANG > 0) and (AdvGlowButton2.Caption <> 'Chọn bàn');
    CmdSplitTable.Enabled := isSelectTable and (mTINHTRANG = 1);
    CmdChangeTable.Enabled := isSelectTable and (mTINHTRANG = 1);
    CmdMergeTable.Enabled := isSelectTable and (mTINHTRANG = 1);
    CmdCalTemp.Enabled := (mMABAN <> '') and (not QrCTBH.IsEmpty);
    CmdPayment.Enabled := (mMABAN <> '') and (not QrCTBH.IsEmpty);
    CmdCustomer.Enabled := mMABAN <> '';
    CmdScanVIP.Enabled := (mMABAN <> '') and (not QrCTBH.IsEmpty);
    CmdShowImage.Enabled := not isSelectTable;// and (AdvGlowButton2.Caption <> 'Chọn món');
    if not isSelectTable then
        CmdSelect.Caption := 'Chọn bàn'
    else
        CmdSelect.Caption := 'Chọn món';

    CmdSendOrder.Enabled := mMABAN <> '';
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
    b: Boolean;
begin
    // Cho close nếu thấy connection failured
    b := False;

    if b and (DataMain.Conn.Errors.Count > 0) then
    begin
        CanClose := True;
        Exit;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    exBillFinal(mPrinter);
    FlexFinal;
    try
        DataMain.isLogon.Logoff;
    except
    end;

    DbeFinal;
    Action := caFree;
end;

{$ENDREGION}

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdScanVIPExecute(Sender: TObject);
begin
    FrmPopTable.Hide;
    FrmPopTable := Nil;
    ProcScanVIP;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdSelectExecute(Sender: TObject);
begin
    isSelectTable := not isSelectTable;
    selectTab(isSelectTable);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdSetpassExecute(Sender: TObject);
begin
    DataMain.isLogon.ResetPassword;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdShowImageExecute(Sender: TObject);
var
    i: Integer;
    mStream: TStream;
begin
    isShowImage := not isShowImage;
    with QrCamUng do
    begin
        First;
        i := 0;
        while not Eof do
        begin
            if isShowImage then
            begin
                AdvGlowButton8.Caption := 'Ẩn hình';
                try
                    mStream := CreateBlobStream(FieldByName('CONTENT_WEB'), bmRead);
                    TtVattu.Tiles[i].Content.Image.LoadFromStream(mStream);
                except
                    TtVattu.Tiles[i].Content.Image.LoadFromFile(fPathNoImage);
                end;
            end
            else
            begin
                AdvGlowButton8.Caption := 'Hình ảnh';
                TtVattu.Tiles[i].Content.Image := nil;
            end;
            Inc(i);
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdSplitTableExecute(Sender: TObject);
begin
    if (mMAKV <> '') and (mMABAN <> '') and (mTINHTRANG > 0) then
    begin
        FrmPopTable.Hide;
        FrmPopTable := Nil;
        isMain := False;
        Application.CreateForm(TFrmCashierTableOrders, FrmCashierTableOrders);
        if not FrmCashierTableOrders.Execute(mMAKV, mMABAN, mTENBAN, 'SPLIT', curKhoa) then
        begin
            isMain := True;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdTableOrdersExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('FB_POS_DATBAN');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_TableOrders, FrmFB_TableOrders);
    FrmFB_TableOrders.Execute(r, mMABAN);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdWinKeyboardExecute(Sender: TObject);
begin
   inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdListTableOrdersExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('FB_POS_DATBAN');
    if r = R_DENY then
        Exit;

    FrmPopTable.Hide;
    FrmPopTable := Nil;
    Application.CreateForm(TFrmFB_TableOrders, FrmFB_TableOrders);
    FrmFB_TableOrders.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdLockExecute(Sender: TObject);
begin
    DataMain.isLogon.Lock;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdMenuNextExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtVattu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdMenuPrevExecute(Sender: TObject);
begin
   smoothTitleBackNext(TtVattu, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdMergeTableExecute(Sender: TObject);
begin
    if (mMAKV <> '') and (mMABAN <> '') and (mTINHTRANG > 0) then
    begin
        FrmPopTable.Hide;
        FrmPopTable := Nil;
        Application.CreateForm(TFrmCashierTableOrders, FrmCashierTableOrders);
        if not FrmCashierTableOrders.Execute(mMAKV, mMABAN, mTENBAN, 'MERGE', curKhoa) then
            Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdMoreExecute(Sender: TObject);
var
    x, y: Integer;
begin
    if Assigned(FrmPopTable) and (FrmPopTable <> Nil) then
    begin
        FrmPopTable.Hide;
        FrmPopTable := Nil;
    end
    else
    begin
        Application.CreateForm(TFrmPopTable, FrmPopTable);
        x := PaRight.Width - FrmPopTable.Width;
        y := PaMain.Height - Panel12.Height div 2 - FrmPopTable.Height - 15;
        FrmPopTable.Popup(x, y);
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdHelpExecute(Sender: TObject);
begin
    ShowHelp;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdReconnectExecute(Sender: TObject);
begin
    if not YesNo(RS_RECONNECT) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdRefreshExecute(Sender: TObject);
begin
    CloseDataSets([spDmBan]);
    OpenDatasets([spDmBan]);
    tbAddUnit;
    mLastRead := Now;
    with spDmBan do
    if (mMABAN <> '') and Locate('MABAN', mMABAN, []) and
            (QrBH.FieldByName('SOTIEN').AsFloat <> FieldByName('SOTIEN_ORDER').AsFloat) then
    begin
        RefreshSelectTable(mMABAN);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdRefreshMenuExecute(Sender: TObject);
begin
    Wait(DATAREADING);
    CmdRefreshExecute(Nil);
    QrNhomCamUng.Requery;
    QrCamUng.Requery;
    tbAddGroupBottom();
    tbAddUnitBottom();
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdReLoadExecute(Sender: TObject);
begin
    Wait(DATAREADING);
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.AdvGlowButton14MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    FrmPop.Hide;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.AdvSmoothTouchKeyBoard1KeyClick(Sender: TObject;
  Index: Integer);
begin
    case Index of
    18:
        CmdWinKeyboard.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    try
        if SecondSpan(Now, mLastRead) > seconds  then
        begin
            CmdRefreshExecute(nil);
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdAreaNextExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtKhuVuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdAreaPrevExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtKhuVuc, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdAreaTableNextExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtKhuVucBan);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdAreaTablePrevExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtKhuVucBan, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdBillListExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('FB_POS_SUABILL');
    if r = R_DENY then
        Exit;

    FrmPopTable.Hide;
    FrmPopTable := Nil;

    Application.CreateForm(TFrmFB_Banle, FrmFB_Banle);
    FrmFB_Banle.Execute(r, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdCalTempExecute(Sender: TObject);
var
    result: Boolean;
begin
    if QrCTBH.IsEmpty then
        Exit;

    result := cloneDataDetail(curKhoa);
    if result then
    begin
        PrintBill(curKhoa);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdChangeTableExecute(Sender: TObject);
begin
    if (mMAKV <> '') and (mMABAN <> '') and (mTINHTRANG > 0) then
    begin
        Application.CreateForm(TFrmCashierTableOrders, FrmCashierTableOrders);
        if FrmCashierTableOrders.Execute(mMAKV, mMABAN, mTENBAN, 'CHANGE', curKhoa) then
           CmdRefresh.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdCustomerExecute(Sender: TObject);
begin
   if (mMAKV <> '') and (mMABAN <> '') then
    begin
        Application.CreateForm(TFrmSoKhach, FrmSoKhach);
        if not FrmSoKhach.Execute(mMABAN, mTENBAN) then
        begin
            with QrBH do
            begin
                if (FieldByName('SOLUONG_KHACH').AsInteger = 0) and
                    QrCTBH.IsEmpty then
                begin
                    mMABAN := '';
                    deleteOrderByKey(curKhoa);
                    Delete;
                end;
            end;
        end;
        CmdRefresh.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdEndOfShiftExecute(Sender: TObject);
var
    b: Boolean;
begin
    FrmPopTable.Hide;
    FrmPopTable := Nil;

    b := not DataMain.PosKetCa(0, 0);

    if FrmPosKetca.Execute(b) then
        Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdGroupMenuNextExecute(Sender: TObject);
begin
    smoothTitleBackNext(TtNganhhang);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.CmdGroupMenuPrevExecute(Sender: TObject);
begin
     smoothTitleBackNext(TtNganhhang, False);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.PrintBill(pKhoa: TGUID);
begin
    try
       mPrinter.BillPrint(pKhoa);
    except
        on E: Exception do
            ErrMsg(E.Message, 'Lỗi in Bill');
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.CmdPaymentExecute(Sender: TObject);
var
    result: Boolean;
begin
    result := cloneDataDetail(curKhoa);
    if result then
    begin
        Application.CreateForm(TFrmMain, FrmMain);
        if not FrmMain.Execute(curKhoa, mMABAN, mTENBAN) then
        begin
            exBillInitial(mPrinter, '', 'a.PRINTER_TEMP = b.PRINTER');
        end;
    end;

    selectTab(True);
    if not QrBH.IsEmpty then
        exReSyncRecord(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.CmdSendOrderExecute(Sender: TObject);
var
    orderNo: Integer;
begin
    if mMABAN <> '' then
    begin
        with GET_ORDER_NO do
        begin
            Prepared := True;
            Parameters[1].Value := TGuidEx.ToString(curKhoa);
            try
                Execute;
                orderNo := Parameters[0].Value + 1;
                updateOrderNo(QrCTBH, orderNo);
            except
                DbeMsg;
            end;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
resourcestring
    RS_RECEIPT_NO = 'Số hóa đơn không đúng.';

procedure TFrmCashierMain.CmdReprintExecute(Sender: TObject);
var
    s: String;
    m, y: Integer;
    dd, mm, yy: WORD;
    k: TGUID;
begin
    DecodeDate(Date, yy, mm, dd);
    m := mm;
    y := yy;
    s := lastSct;

    Application.CreateForm(TFrmSoBill, FrmSoBill);
    if not FrmSoBill.GetBillNo(m, y, s) then
        Exit;

    // In lai
    PrintBill(k);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
function TFrmCashierMain.Total;
var
    bm: TBytes;
    xSotien, mSotien, mSoluong, mSoluongthuc: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mSoluongthuc := 0;

	with QrCTBH do
    begin
        DisableControls;
        bm := BookMark;

        First;
        while not Eof do
        begin
            if pReOrder and (FieldByName('STT').AsInteger <> FieldByName('RSTT').AsInteger)then
            begin
                try
                    mTrigger := True;
                    SetEditState(QrCTBH);
                    FieldByName('STT').AsInteger := FieldByName('RSTT').AsInteger;
                    Post;
                finally
                    mTrigger := False;
                end;
            end;

            xSotien := FieldByName('SOTIEN').AsFloat;
            mSotien := mSotien + xSotien;
            mSoluong := mSoluong + FieldByName('SOLUONG_DAT').AsFloat;
            mSoluongthuc := mSoluongthuc + FieldByName('SOLUONG').AsFloat;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

    with QrBH do
    begin
        if State in [dsBrowse] then
        	Edit;
            
    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG_DAT').AsFloat := mSoluong;
        FieldByName('SOLUONG').AsFloat := mSoluongthuc;
        CheckBrowseMode;
    end;

    with QrCTBH do
    begin
        if IsEmpty then
        begin
           deleteOrderByKey(curKhoa);
           QrBH.Delete;
        end;
    end;

    updateContentTable(fIndexCurrent);
    Result := True;
end;


(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtKhuVucTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Data;
    if (s = '') and (mMAKV <> '') then
        s := mMAKV;

    if (s <> '') and (s <> 'ALL') then
    begin
        sf := 'MAKV=' + QuotedStr(s);
    end;

    mMAKV := s;
    spDmBan.Filter := sf;
    tbAddUnit;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtNganhhangDblClick(Sender: TObject);
begin
    TtNganhhang.GoBack;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)


procedure TFrmCashierMain.TtNganhhangTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Data;
    if (s <> '') and (s <> 'ALL') then
    begin
        sf := 'CAMUNG_NHOM=' + QuotedStr(s);
    end;

    QrCamUng.Filter := sf;
    tbAddUnitBottom;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtVattuTileDblClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s: String;
begin
    s := Tile.Data;

    if (s = '') or (not QrBH.Active) then
        Exit;

    mDefSoluong := 1;

    with QrBH do
    begin
        if IsEmpty then
        begin
            curKhoa := DataMain.GetNewGuid;
            Append;
        end
        else
            Edit;
    end;

    with QrCTBH do
    begin
        GetSalingPrice(s);
        // Mã cũ
        if Locate('MAVT;ORDER_NO;ORDER_BY', VarArrayOf([s, FieldByName('ORDER_NO').AsInteger, FieldByName('ORDER_BY').AsInteger]), []) then
        begin
            if (FieldByName('ORDER_NO').IsNull) or (FieldByName('ORDER_NO').AsInteger = 0) then
                ItemSetQty(FieldByName('SOLUONG_DAT').AsFloat + mDefSoluong)
            else
                ItemAddNew(s);
        end
        // Mã mới
        else
        begin
            // Chua co gia ban
            if curGiaban = 0 then
            begin
                Beep;
                ErrMsg(RS_POS_NOT_PRICE);
                Exit;
            end;

            ItemAddNew(s);
        end;
        Total(true);
    end;
end;

procedure TFrmCashierMain.TtVattuTileFill(Sender: TObject; Tile: TAdvSmoothTile;
  State: TTileState; Fill: TGDIPFill);
begin
    with QrCTBH do
    begin
        if (not IsEmpty) and (Locate('MAVT', Tile.Data, []))then
        begin
           Fill.BorderColor := clTeal;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtFillterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtFillterMouseLeave(Sender: TObject);
begin
    FrmPop.Hide;
end;


(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.TtFillterTileEnter(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Content.Text;

    FrmPop.Popup(s, pX, pY);
    if s = '*' then
        sf := ''
    else
    if s = '#' then
        sf := 'TENTAT2 like ''0'' or ' +
            'TENTAT2 like ''1'' or ' +
            'TENTAT2 like ''2'' or ' +
            'TENTAT2 like ''3'' or ' +
            'TENTAT2 like ''4'' or ' +
            'TENTAT2 like ''5'' or ' +
            'TENTAT2 like ''6'' or ' +
            'TENTAT2 like ''7'' or ' +
            'TENTAT2 like ''8'' or ' +
            'TENTAT2 like ''9'''
    else
        sf := 'TENTAT2 = '''+ s + '''';

    QrCamUng.Filter := sf;
    tbAddUnitBottom;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtKhuVucBanTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s: string;
begin
    s := Tile.Data;

    if s = '' then
        Exit;

    mMABAN := s;
    fIndexCurrent := Tile.Index;
    RefreshSelectTable(mMABAN);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.TtKhuVucBanTileDblClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
begin
    mMABAN := Tile.Data;
    Panel10.Visible := True;
    AdvGlowButton2.Caption := 'Chọn bàn';
    isSelectTable := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtKhuVucBanTileDeleteIndicator(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState; var Indicator: string);
begin
    Tile.DeleteIndicator :=  Indicator;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtKhuVucBanTileFill(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState; Fill: TGDIPFill);
begin
    with spDmBan do
    begin
        if Locate('MABAN', Tile.Data, []) then
        begin
            if FieldByName('TINHTRANG').AsInteger = 1 then
            begin
                Fill.Color := activeBackground;
                Fill.BorderColor := activeBackground;
            end
            else if FieldByName('TINHTRANG').AsInteger = 2 then
            begin
                Fill.Color := orderBackground;
                Fill.BorderColor := orderBackground;
            end
            else
            begin
                Fill.Color := emptyBackground;

            end;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.TtKhuVucBanTileFont(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState; AFont: TFont);
begin
    with spDmBan do
    begin
        if Locate('MABAN', Tile.Data, []) then
        begin
            if FieldByName('TINHTRANG').AsInteger = 1 then
            begin
                AFont.Color := activeColor;
            end
            else if FieldByName('TINHTRANG').AsInteger = 2 then
            begin
                AFont.Color := orderColor;
            end
            else
            begin
                 AFont.Color := emptyColor;
            end;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.QrDBError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    Action := DbeMsg;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.QrBHAfterInsert(DataSet: TDataSet);
begin
    curStt := 0;
	with QrBH do
    begin
		TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        FieldByName('_id').Value := NewIntID;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
        FieldByName('CREATE_DATE').AsDateTime := Now;
		FieldByName('CREATE_BY').AsInteger := sysLogonUID;
        FieldByName('LCT').AsString := mLCT;
        FieldByName('QUAY_ORDER').AsString := posQuay;
        FieldByName('MAQUAY_ORDER').AsString := posMaQuay;
        FieldByName('NGAYVAO').AsDateTime := Now;
		FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('HINHTHUC_GIA').AsString := sysHinhThucGia;
        FieldByName('DELIVERY').AsBoolean := False;
        FieldByName('CO_HOADON').AsBoolean := False;
        FieldByName('MABAN').AsString := mMABAN;
        FieldByName('SOLUONG_KHACH').AsInteger := 1;
        FieldByName('TL_PHUTHU').AsFloat := tyLePhuThu;
        FieldByName('ORDER_DATE').AsDateTime := Now;
		FieldByName('ORDER_BY').AsInteger := sysLogonUID;
    end;
end;
(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrBHBeforeOpen(DataSet: TDataSet);
begin
    with QrBH do
    begin
        if curKhoa = TGuidEx.EmptyGuid then
        begin
            curKhoa := DataMain.GetNewGuid;
        end;
        
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.QrBHBeforePost(DataSet: TDataSet);
var
	mDate: TDateTime;
begin
	mDate := Now;
    if (Trunc(mDate) <> Trunc(mLastDate)) or (mLastDate > mDate) then
    begin
		ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
    	Abort;
    end;

    SetAudit(DataSet);

    with DataSet do
    begin
        if FieldByName('NGAY').IsNull then //
		    FieldByName('NGAY').AsDateTime := mDate;
        mLastDate := mDate;
    end;
end;
(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    // For TEST Hoang phuc Mail: 2012-08-02
    with DataMain.Conn.Errors[0] do
    if (NativeError = 32) and
      (Pos('CANNOT BE LOCATED', UpperCase(Description)) < 1) then
    begin
        ErrMsg(Description);
        Action := daAbort;
    end
    else
        Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHAfterDelete(DataSet: TDataSet);
var
    result: Boolean;
begin
    result := cloneDataDetail(curKhoa);
    if result then
    begin
         exReSyncRecord(QrBH);
         TtVattu.Refresh;
    end;
end;

procedure TFrmCashierMain.QrCTBHAfterInsert(DataSet: TDataSet);
begin
    with QrCTBH do
    begin
    	Inc(curStt);
        FieldByName('_id').Value := NewIntID;
    	FieldByName('STT').AsInteger := curStt;
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
        FieldByName('LOC').AsString := QrBH.FieldByName('LOC').AsString;
        FieldByName('ORDER_NO').AsInteger := 0;
        FieldByName('MAKHO').AsString := posMakho;
        FieldByName('ORDER_BY').AsInteger := sysLogonUID;
        FieldByName('THOIGIAN_CHEBIEN').AsFloat := QrCamUng.FieldByName('THOIGIAN_CHEBIEN').AsFloat;

    end;

    // lay ngay gio de tinh chiet khau, khuyen mai.
    if Is1Record(QrCTBH) and (not sysIsCentral) then
    begin
        SetEditState(QrBH);
        QrBH.FieldByName('NGAY').AsDateTime := Now;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHAfterPost(DataSet: TDataSet);
var
    result: Boolean;
begin
    if mTrigger then
    	Exit;

    // Reread DMVT
    with QrCTBH do
    begin
        if FieldByName('LK_TENVT').AsString = '' then
        begin
            FieldByName('LK_TENVT').RefreshLookupList;
            FieldByName('LK_TENVT_KHONGDAU').RefreshLookupList;
        end;

        result := cloneDataDetail(curKhoa);
        if result then
        begin
             exReSyncRecord(QrBH);
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
    with QrCTBH do
    begin
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    SetAudit(DataSet);

	with QrBH do
    if State in [dsInsert] then
    begin
        Post;
        Edit;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHCalcFields(DataSet: TDataSet);
var
    expiredDate: TDateTime;
    minutes: Integer;
begin
    with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);

        minutes := FieldByName('THOIGIAN_CHEBIEN').AsInteger;
        if not (FieldByName('ORDER_DATE').IsNull)then
        begin

            expiredDate := FieldByName('ORDER_DATE').AsDateTime;
            expiredDate := IncMinute(expiredDate, minutes);
            if (expiredDate >= Now) then
            begin
                FieldByName('CALC_THOIGIAN_CONLAI').AsFloat := RoundUp(MinuteSpan(expiredDate, Now));
            end
            else
                FieldByName('CALC_THOIGIAN_CONLAI').AsFloat := 0;
        end;

        if (not FieldByName('ORDER_DATE').IsNull) and (FieldByName('ORDER_NO').AsInteger > 0) and (FieldByName('THOIGIAN_CHEBIEN').AsFloat > 0) then
        begin
            FieldByName('TEXT_THOIGIAN_CHUYENCHEBIEN').AsString := FormatDatetime('hh:nn', FieldByName('ORDER_DATE').AsDateTime);
            FieldByName('TEXT_THOIGIAN_CONLAI').AsString := FloatToStr(FieldByName('CALC_THOIGIAN_CONLAI').AsFloat) + ' phút'
        end
        else
        begin
            FieldByName('TEXT_THOIGIAN_CHUYENCHEBIEN').AsString := '';
            FieldByName('TEXT_THOIGIAN_CONLAI').AsString := '';
        end;

        if (FieldByName('THOIGIAN_CHEBIEN').AsFloat > 0) then
            FieldByName('TEXT_THOIGIAN_CHEBIEN').AsString := FloatToStr(FieldByName('THOIGIAN_CHEBIEN').AsFloat) + ' phút'
        else
            FieldByName('TEXT_THOIGIAN_CHEBIEN').AsString := '';

    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHMAVTChange(Sender: TField);
begin
    with QrCTBH do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;
        FieldByName('TENTAT').AsString := FieldByName('LK_TENTAT').AsString;
        FieldByName('DVT').AsString := FieldByName('LK_DVT').AsString;
        FieldByName('BO').AsBoolean := FieldByName('LK_BO').AsBoolean;
    end;
end;


(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHSOLUONGChange(Sender: TField);
var
	sl, dg, st: Double;
begin
    if mTrigger then
    	Exit;

	with QrCTBH do
    begin
        if (Sender <> Nil) and (Sender.FieldName = 'SOLUONG') then
        begin
            sl  := FieldByName('SOLUONG').AsFloat;
            dg  := FieldByName('DONGIA').AsFloat;
            st := exVNDRound(sl*dg);
            FieldByName('SOTIEN').AsFloat := st;
            GrDetail.InvalidateCurrentRow;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHSOLUONGValidate(Sender: TField);
begin
    if (Sender <> Nil) and (Sender.FieldName = 'SOLUONG') then
    begin
        with QrCTBH do
        begin
            if (FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG_DAT').AsFloat) then
            begin
                FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG_DAT').AsFloat;
                Edit;
            end;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHSOLUONG_DATChange(Sender: TField);
begin
    if mTrigger then
    	Exit;

	with QrCTBH do
    begin
        if (Sender <> Nil) then
        begin
            if toGo then
            begin
               Edit;
            end;

            if FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG_DAT').AsFloat then
            begin
                Edit;
                FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG_DAT').AsFloat;
            end;

            if FieldByName('SOLUONG_DAT').AsFloat = 0 then
                Delete;

        end;
        GrDetail.InvalidateCurrentRow;
	end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.QrCTBHSOTIENChange(Sender: TField);
begin
    with QrCTBH do
    begin
        if isMain then
            Total;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.tbAddGroup;
var
    mTiles: TAdvSmoothTile;
begin
    TtKhuVuc.Tiles.Clear;
    TtKhuVuc.BeginUpdate;
    with QrDmKhuVuc do
    begin
        First;
        while not Eof do
        begin
            mTiles := TtKhuVuc.Tiles.Add;
            with mTiles do
            begin
                Data := FieldByName('MAKV').AsString;
                Content.Text := FieldByName('TENKV').AsString;
                Content.TextPosition := tpCenterCenter;
                Tag := 0;
            end;
            Next;
        end;
    end;
    TtKhuVuc.EndUpdate;
    TtKhuVuc.FirstPage;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.tbAddUnit;
var
    fIndex: Integer;
begin
    TtKhuVucBan.Tiles.Clear;
    TtKhuVucBan.BeginUpdate;
    with spDmBan do
    begin
        First;
        fIndex := 0;
        while not Eof do
        begin
            with TtKhuVucBan.Tiles.Add do
            begin
                drawContentTable(spDmBan, TtKhuVucBan, fIndex);
                Inc(fIndex);
            end;
            Next;
        end;
    end;
    TtKhuVucBan.EndUpdate;
    TtKhuVucBan.FirstPage;
end;


(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.tbAddGroupBottom;
var
    mTiles: TAdvSmoothTile;
begin
    TtNganhhang.Tiles.Clear;
    TtNganhhang.BeginUpdate;
    with QrNhomCamUng do
    begin
        First;
        while not Eof do
        begin
            mTiles := TtNganhhang.Tiles.Add;
            with mTiles do
            begin
                Data := FieldByName('MACAMUNG').AsString;
                Content.Text := FieldByName('TENCAMUNG').AsString;
                Content.TextPosition := tpCenterCenter;
                Tag := 0;
            end;
            Next;
        end;
    end;
    TtNganhhang.EndUpdate;
    TtNganhhang.FirstPage;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.tbAddUnitBottom;
var
    fContent, fName, fPrice: string;
    mStream : TStream;
begin

    TtVattu.Tiles.Clear;
    with QrCamUng do
    begin
        First;

        while not Eof do
        begin
            with TtVattu.Tiles.Add do
            begin
                fName := FieldByName('TENTAT').AsString;
                fPrice := FloatToStrF(FieldByName('GIABAN').AsFloat, ffNumber, 10, 0);
                fContent := '';

                fContent := fContent + '<p align="center">';
                fContent := fContent + fName + '<br>';
                fContent := fContent + fPrice + 'đ';
                fContent := fContent + '</p>';

                if isShowImage then
                begin
                    try
                        mStream := CreateBlobStream(FieldByName('CONTENT_WEB'), bmRead);
                        Content.Image.LoadFromStream(mStream);
                    except
                        Content.Image.LoadFromFile(fPathNoImage);
                    end;
                end
                else
                begin
                    Content.Image := nil;
                end;

                Data := FieldByName('MAVT').AsString;
                Content.Text := fContent;
                Content.TextPosition := tpBottomCenter;

            end;
            Next;
        end;
    end;
    TtVattu.EndUpdate;
    TtVattu.FirstPage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.ItemAddNew(const ma: String; const pMabo: String);
begin
    with QrCTBH do
    begin
	    Append;
        FieldByName('MAVT').AsString := ma;
        FieldByName('DONGIA').AsFloat := curGiaban;
        FieldByName('SOLUONG_DAT').AsFloat := mDefSoluong;
        if toGo then
        begin
           FieldByName('SOLUONG').AsFloat := mDefSoluong;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.ItemSetQty(const pSoluong: Double);
begin
    with QrCTBH do
    begin
        Edit;
		FieldByName('DONGIA').AsFloat := curGiaban;
		FieldByName('SOLUONG_DAT').AsFloat := pSoluong;
        if toGo then
        begin
           FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG_DAT').AsFloat;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.InvalidAction;
begin
    Beep;
    if msg = '' then
		ErrMsg(RS_INVALID_ACTION, 'Lỗi nhập liệu', 1)
    else
		ErrMsg(msg, 'Lỗi nhập liệu', 1);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.OpenQueries;
begin
    CloseDataSets([QrCTBH, QrBH]);
    OpenDataSets([QrBH, QrCTBH]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
begin
    with QrCamUng do
	if Locate('MAVT', mavt, []) then
    begin
        curGiaban := FieldByName('GIABAN').AsFloat;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.GrDetailCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if ((Field.DataSet.FieldByName('ORDER_NO').IsNull) or (Field.DataSet.FieldByName('ORDER_NO').AsInteger = 0)) and
    (not Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').IsNull) and (Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').AsFloat > 0) then
    begin
        GrDetail.Canvas.Font.Color := 10903848;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.GrDetailDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
    if (not Field.DataSet.FieldByName('ORDER_NO').IsNull) and (Field.DataSet.FieldByName('ORDER_NO').AsInteger > 0) then
    begin
        GrDetail.Canvas.Font.Color := clWhite;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.RefreshSelectTable(mMABAN: String): Boolean;
begin
    with spDmBan do
    if Locate('MABAN', mMABAN, []) then
    begin
        mMAKV := FieldByName('MAKV').AsString;
        mTENBAN := FieldByName('TENBAN').AsString;
        mTINHTRANG := FieldByName('TINHTRANG').AsInteger;
        EdTable.Caption.Text := mTENBAN;
        curKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
        toGo :=  FieldByName('TOGO').AsBoolean;
        tyLePhuThu := FieldByName('TL_PHUTHU').AsFloat;
    end;
    OpenQueries;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.updateContentTable(fIndex: Integer): Boolean;
begin
    spDmBan.Requery;
    with spDmBan do
    if Locate('MABAN', mMABAN, []) then
    begin
        mTINHTRANG := FieldByName('TINHTRANG').AsInteger;
        drawContentTable(spDmBan, TtKhuVucBan, fIndex);
    end;
    Result := True;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.drawContentTable(spStore: TADOStoredProc; ttTileList: TAdvSmoothTileListEx; fIndex: Integer): Boolean;
var
    fContent, fName, fPersons, fTimes, fTotal: string;
    fAmount: Double;
    iTINHTRANG: Integer;
begin
    with spStore do
    begin
        iTINHTRANG := FieldByName('TINHTRANG').AsInteger;

        with ttTileList.Tiles[fIndex] do
        begin
            fName := FieldByName('TENBAN').AsString;
            fPersons := IntToStr(FieldByName('SOLUONG_MACDINH').AsInteger)
              + ' Khách';
            if (iTINHTRANG > 0) then
            begin
                fPersons := IntToStr(FieldByName('SOLUONG_KHACH').AsInteger)
                  + '/' + IntToStr(FieldByName('SOLUONG_MACDINH').AsInteger)
                  + ' Khách';

            end;

            if (FieldByName('SOLUONG').AsFloat <> 0) or (FieldByName('SOLUONG_ORDER').AsFloat <> 0) then
            begin
                  fPersons := fPersons + ' (' +IntToStr(FieldByName('SOLUONG').AsInteger)
                  + '/' + IntToStr(FieldByName('SOLUONG_ORDER').AsInteger)
                  + ' món)';
            end;


            fAmount := FieldByName('THUCTRA').AsFloat;
            fTotal := '';
            fContent := '';

             fContent := fContent +
              '<p align="right"><font size="2" >' + fPersons + '</font></p>';
                fContent := fContent +
              '<p align="center"><font align="center" size="4" >' + fName + '</font></p>';

            if iTINHTRANG > 0 then
            begin
                if iTINHTRANG = 1 then
                begin
                    fTimes := FieldByName('GIOVAO').AsString;
                    ttTileList.TileAppearance.StatusIndicatorAppearance.Font.
                     Color := activeColor;
                end
                else if iTINHTRANG = 2 then
                begin
                    fTimes := FieldByName('GIODAT').AsString;
                    ttTileList.TileAppearance.StatusIndicatorAppearance.Font.
                     Color := orderColor;
                end;

                fContent := fContent +
                  '<p align="left"><font size="2" >' + fTimes + '</font></p>';

                fTotal :=  FloatToStrF(fAmount, ffNumber, 10, 0) + 'đ';
                StatusIndicatorLeft := -5 - Length(fTotal) * 6 div 2;
                StatusIndicatorTop := 58;

            end;

            DeleteIndicator := fTimes;
            DeleteIndicatorTop := 58;
            DeleteIndicatorLeft := -5;

            StatusIndicator := fTotal;
            Data := FieldByName('MABAN').AsString;
            Content.Text := fContent;
            Content.TextPosition := tpHTMLAlign;
        end;
    end;
    Result := True;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.updateOrderNo(mQuery: TADOQuery; orderNo: Integer): Boolean;
var
    bm: TBytes;
begin
    with mQuery do
    begin
        DisableControls;
        bm := BookMark;
        First;
        while not Eof do
        begin
            if (FieldByName('ORDER_NO').IsNull) or (FieldByName('ORDER_NO').AsInteger = 0) then
            begin
                Edit;
                FieldByName('ORDER_NO').AsInteger := orderNo;
                FieldByName('ORDER_BY').AsInteger := sysLogonUID;
                FieldByName('ORDER_DATE').AsDateTime := Now;
                Post;
            end;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.updateArea(TtList: TAdvSmoothTileListEx; viewFontColor, viewBackgroundColor,
    hoverFontColor, hoverBackgroundColor, selectedFontColor, selectedBackgroundColor: TColor): Boolean;
begin
    with TtList do
    begin
        TileAppearance.SmallViewFill.Color := viewBackgroundColor;
        TileAppearance.SmallViewFill.ColorTo := viewBackgroundColor;
        TileAppearance.SmallViewFill.ColorMirror := clNone;
        TileAppearance.SmallViewFill.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFill.BorderColor := clNone;
        TileAppearance.SmallViewFill.Rounding := 0;
        TileAppearance.SmallViewFill.ShadowColor := clNone ;
        TileAppearance.SmallViewFill.ShadowOffset := 0;
        TileAppearance.SmallViewFont.Color := viewFontColor;

        TileAppearance.SmallViewFillHover.Color := hoverBackgroundColor;
        TileAppearance.SmallViewFillHover.ColorTo := hoverBackgroundColor;
        TileAppearance.SmallViewFillHover.ColorMirror := clNone;
        TileAppearance.SmallViewFillHover.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFillHover.BorderColor := clNone;
        TileAppearance.SmallViewFillHover.Rounding := 0;
        TileAppearance.SmallViewFillHover.ShadowColor := clNone ;
        TileAppearance.SmallViewFillHover.ShadowOffset := 0;
        TileAppearance.SmallViewFillHover.Opacity := 60;
        TileAppearance.SmallViewFillHover.OpacityTo := 60;
        TileAppearance.SmallViewFontHover.Color := hoverFontColor;


        TileAppearance.SmallViewFillSelected.Color := selectedBackgroundColor;
        TileAppearance.SmallViewFillSelected.ColorTo := selectedBackgroundColor;
        TileAppearance.SmallViewFillSelected.ColorMirror := clNone ;
        TileAppearance.SmallViewFillSelected.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFillSelected.BorderColor := clNone;
        TileAppearance.SmallViewFillSelected.Rounding := 0;
        TileAppearance.SmallViewFillSelected.ShadowColor := clNone ;
        TileAppearance.SmallViewFillSelected.ShadowOffset := 0;
        TileAppearance.SmallViewFontSelected.Color := selectedFontColor;

    end;
    Result := True;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.updateAreaTable(TtList: TAdvSmoothTileListEx;
        viewFontColor, viewBackgroundColor, viewBorderColor,
        hoverFontColor, hoverBackgroundColor, hoverBorderColor,
        selectedFontColor, selectedBackgroundColor, selectedBorderColor: TColor): Boolean;
begin
    with TtList do
    begin
        TileAppearance.SmallViewFill.Color := viewBackgroundColor;
        TileAppearance.SmallViewFill.ColorTo := viewBackgroundColor;
        TileAppearance.SmallViewFill.ColorMirror := clNone;
        TileAppearance.SmallViewFill.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFill.BorderWidth := 1;
        TileAppearance.SmallViewFill.BorderColor := viewBorderColor;
        TileAppearance.SmallViewFill.Rounding := 5;
        TileAppearance.SmallViewFill.ShadowColor := clNone ;
        TileAppearance.SmallViewFill.ShadowOffset := 0;
        TileAppearance.SmallViewFont.Color := viewFontColor;

        TileAppearance.SmallViewFillHover.Color := hoverBackgroundColor;
        TileAppearance.SmallViewFillHover.ColorTo := hoverBackgroundColor;
        TileAppearance.SmallViewFillHover.ColorMirror := clNone;
        TileAppearance.SmallViewFillHover.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFillHover.BorderWidth := 1;
        TileAppearance.SmallViewFillHover.BorderColor := hoverBorderColor;
        TileAppearance.SmallViewFillHover.Rounding := 5;
        TileAppearance.SmallViewFillHover.ShadowColor := clNone ;
        TileAppearance.SmallViewFillHover.ShadowOffset := 0;
        TileAppearance.SmallViewFontHover.Color := hoverFontColor;


        TileAppearance.SmallViewFillSelected.Color := selectedBackgroundColor;
        TileAppearance.SmallViewFillSelected.ColorTo := selectedBackgroundColor;
        TileAppearance.SmallViewFillSelected.ColorMirror := clNone ;
        TileAppearance.SmallViewFillSelected.ColorMirrorTo := clNone;
        TileAppearance.SmallViewFillSelected.BorderWidth := 1;
        TileAppearance.SmallViewFillSelected.BorderColor := selectedBorderColor;
        TileAppearance.SmallViewFillSelected.Rounding := 5;
        TileAppearance.SmallViewFillSelected.ShadowColor := clNone ;
        TileAppearance.SmallViewFillSelected.ShadowOffset := 0;
        TileAppearance.SmallViewFontSelected.Color := selectedFontColor;

    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierMain.CmdPrintMoreExecute(Sender: TObject);
begin
    FrmPopTable.Hide;
    FrmPopTable := Nil;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierMain.selectTab(isTable: Boolean);
begin
    Panel9.Visible := isTable;
    Panel10.Visible := not isTable;
    CmdRefreshExecute(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCashierMain.smoothTitleBackNext(TtList: TAdvSmoothTileListEx; bNext: Boolean = True): Boolean;
begin
     with TtList do
     begin
        if bNext then
        begin
            NextPage;
            PageIndex := PageIndex + 1;
        end
        else
        begin
            PreviousPage;
            PageIndex := PageIndex - 1;
        end;
     end;
     Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierMain.enabledButtonBackNext(TtList: TAdvSmoothTileListEx; buttonPrev, buttonNext: TAction): Boolean;
begin
     with TtList do
     begin
        buttonPrev.Enabled :=  PageIndex > 0;
        buttonNext.Enabled :=  PageIndex < (PageCount - 1);
     end;
     Result := True;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function  TFrmCashierMain.deleteOrderByKey(pKhoa: TGUID): Boolean;
begin

    with FB_CASHIER_DELETE_ORDER do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        try
            Execute;
        except
            DbeMsg;
        end;
    end;
    Result := True;
end;
(* ==============================================================================
  **------------------------------------------------------------------------------
*)

resourcestring
    RS_INVALID_VIP = 'Sai mã khách hàng thân thiết.';
procedure TFrmCashierMain.ProcScanVIP;
var
    ma, ten: String;
    ck: Double;
    bm: TBytes;
    result: Boolean;
begin
    with QrBH do
        ma := FieldByName('MAVIP').AsString;

    Application.CreateForm(TFrmPosVIP, FrmPosVIP);
    ten := FrmPosVIP.Execute(ma, 0);


//     Xóa mã VIP
    if ma = '' then
    begin
        SetEditState(QrBH);
        with QrBH do
        begin
            FieldByName('MAVIP').Clear;
            FieldByName('MAVIP_HOTEN').Clear;
            FieldByName('TL_CKVIPDM').Clear;
            Total;
        end;
        Exit;
    end;

    if ten = '' then
        Exit;

    // Lấy thông tin VIP
    with POS_VIP do
    begin
        Prepared := True;
        Parameters[1].Value := ma;
        Execute;

        if Parameters[0].Value = 0 then
        begin
            mVipTen := Trim(Parameters[2].Value);
            ck := Parameters[3].Value;
        end
        else
        begin
            ErrMsg(RS_INVALID_VIP);
            Exit;
        end;
    end;
//
    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('MAVIP').AsString := ma;
        FieldByName('MAVIP_HOTEN').AsString := mVipTen;
        FieldByName('TL_CKVIPDM').AsFloat := ck;
    end;
    Total;


    result := cloneDataDetail(curKhoa);
    if result then
    begin
         exReSyncRecord(QrBH);
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function  TFrmCashierMain.getIndexTileListByValue(sMA: string; smoothTileList: TAdvSmoothTileListEx): Integer;
var
    i, index: Integer;
    tile: TAdvSmoothTile;
begin
    index := 0;
    if smoothTileList.Tiles.Count > 0  then
    begin
        for i := 0 to smoothTileList.Tiles.Count do
        begin
            tile := smoothTileList.Tiles[i];
            if (sMA <> '') and (sMA = tile.Data) then
            begin
                index := tile.Index;
                break;
            end;
        end;
    end;
    Result := index;
end;

end.
