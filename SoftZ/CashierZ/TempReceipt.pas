﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TempReceipt;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Db, wwDBGrid2, ADODB, Buttons, StdCtrls, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmTempReceipt = class(TForm)
    QrTAM: TADOQuery;
    DsTAM: TDataSource;
    GrList: TwwDBGrid2;
    btOK: TBitBtn;
    btCancel: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GrListDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
    function Execute(pMakho: String; pQuay: String; pMaBan: String): TGUID;
  end;

var
  FrmTempReceipt: TFrmTempReceipt;

implementation

{$R *.DFM}

uses
    isLib, ExCommon, isDb, GuidEx, isCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTempReceipt.Execute(pMakho: String; pQuay: String; pMaBan: String): TGUID;
begin
    with QrTAM do
    begin
        Close;
        Parameters[0].Value := posMaQuay;
        Parameters[1].Value := pMaBan;
        Open;
    end;
    SetDisplayFormat(QrTAM, sysCurFmt);
    SetShortDateFormat(QrTAM, ShortDateFormat + ' HH:mm');

    if ShowModal = mrOK then
        Result := TGuidField(QrTAM.FieldByName('KHOA')).AsGuid
    else
        Result := TGuidEx.EmptyGuid;
    QrTAM.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTempReceipt.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('POS_LUUTAM', GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTempReceipt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTempReceipt.GrListDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

end.
