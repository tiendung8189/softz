object FrmBanleCN: TFrmBanleCN
  Left = 214
  Top = 131
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#243'a '#208#417'n B'#225'n L'#7867' - Giao H'#224'ng'
  ClientHeight = 576
  ClientWidth = 1045
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000000000000680500001600000028000000100000002000
    0000010008000000000040010000000000000000000000000000000000006666
    66004242A600B2423200E6825200CE623E00DA724600C6523A00E6A23A00EEAE
    5A00BE4A3600DA6A4200E27E5200D2D2D200BA463200D66A4200E2724600F2BA
    6E00CA5A3E00EEAE5600FEFEFE00F2BA72008E8E8E00B6463200D2623E00DE72
    4600C6563A00E6A23E00BE4E3600DE6E4600FEDEBE00BA463600D26E4200E676
    4600F6BA6E000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000002222
    2222151522222222221515222222222200150C0C15000000150C0C1522222222
    22000C0C00222222000C0C000022222222220000222222222200002200222222
    2222222222222222222222220022222222220202020202020202020202222222
    2206020202020202020202020222222222060207020702070207020702222222
    0A0606060606060606060606062222220A080408170817081708170806222205
    0A0A0A0A0A0A0A0A0A0A0A0A172222050510051005100F10051405100A220F0F
    0F0F0F0F0F0F0F0F0F0F0F0F0A220F1D1D1D1D1D1D1D1D1D1D1D1D1D0B220F0F
    0F0F0F0F0F0F0F0F0F0F0F03010122222222222222222222222222220101F3E7
    0000C0030000E1C10000F3E50000FFFD0000F0010000E0010000E0010000C001
    0000C00100008001000080010000000100000001000000000000FFFC0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1045
    576)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 1045
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1045
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton3: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 60
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton9: TToolButton
      Left = 120
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton4: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton6: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdThongtinCN_View
      ImageIndex = 25
    end
    object ToolButton11: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 1045
    Height = 536
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 486
        Width = 1037
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 73
        Width = 1037
        Height = 413
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'15'#9'Ng'#224'y'#9'F'
          'QUAY'#9'4'#9'M'#227#9'F'#9'Qu'#7847'y thu ng'#226'n'
          'LK_TENQUAY'#9'20'#9'T'#234'n'#9'F'#9'Qu'#7847'y thu ng'#226'n'
          'SCT'#9'10'#9'S'#7889' bill'#9'F'
          'THANHTOAN'#9'14'#9'Ph'#7843'i thu'#9'F'
          'THUNGAN'#9'30'#9'T'#234'n'#9'F'#9'Thu ng'#226'n'
          'DELETE_DATE'#9'12'#9'Ng'#224'y'#9'F'#9'H'#7911'y h'#243'a '#273#417'n'
          'NG_HUY'#9'20'#9'Ng'#432#7901'i h'#7911'y'#9'F'#9'H'#7911'y h'#243'a '#273#417'n')
        MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrBrowseCalcCellColors
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1037
        Height = 73
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object Label65: TLabel
          Left = 191
          Top = 16
          Width = 47
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = CbMAQUAYChange
        end
        object Label66: TLabel
          Left = 420
          Top = 16
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7871'n ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 151
          Top = 44
          Width = 87
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7875'm b'#225'n h'#224'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdFrom: TwwDBDateTimePicker
          Left = 248
          Top = 12
          Width = 101
          Height = 24
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.FocusBorders = []
          Frame.NonFocusBorders = []
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdTo: TwwDBDateTimePicker
          Left = 484
          Top = 12
          Width = 101
          Height = 24
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 1
        end
        object CbMaKho: TwwDBLookupCombo
          Tag = 3
          Left = 248
          Top = 40
          Width = 53
          Height = 24
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MAKHO'#9'5'#9'MAKHO'#9'F'
            'TENKHO'#9'35'#9'TENKHO'#9'F')
          LookupTable = QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Navigator = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = CbMAQUAYChange
          OnBeforeDropDown = CbMaKhoBeforeDropDown
          OnCloseUp = CbMaKhoCloseUp
          OnExit = CbMAQUAYChange
          OnNotInList = CbMaKhoNotInList
        end
        object CbTenKho: TwwDBLookupCombo
          Tag = 4
          Left = 304
          Top = 40
          Width = 281
          Height = 24
          TabStop = False
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENKHO'#9'35'#9'TENKHO'#9'F'
            'MAKHO'#9'5'#9'MAKHO'#9'F')
          LookupTable = QrDMKHO
          LookupField = 'MAKHO'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Navigator = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = CbMAQUAYChange
          OnBeforeDropDown = CbMaKhoBeforeDropDown
          OnCloseUp = CbMaKhoCloseUp
          OnExit = CbMAQUAYChange
          OnNotInList = CbMaKhoNotInList
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaBanle: TPanel
        Left = 0
        Top = 0
        Width = 1037
        Height = 97
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          1037
          97)
        object LbHH1: TLabel
          Left = 570
          Top = 8
          Width = 90
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' h'#224'ng h'#243'a'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 678
          Top = 8
          Width = 60
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Chi'#7871't kh'#7845'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label19: TLabel
          Left = 786
          Top = 8
          Width = 78
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Ti'#7873'n thu'#7871' VAT'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 894
          Top = 8
          Width = 100
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
          FocusControl = EdTriGiaTT
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 12
          Top = 7
          Width = 26
          Height = 16
          Caption = 'Kho:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 1008
          Top = 2
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 13
          Top = 48
          Width = 34
          Height = 16
          Caption = 'Qu'#7847'y:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 256
          Top = 5
          Width = 33
          Height = 16
          Caption = 'Ng'#224'y:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 257
          Top = 48
          Width = 40
          Height = 16
          Caption = 'S'#7889' bill:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 392
          Top = 5
          Width = 70
          Height = 16
          Caption = 'Thu ng'#226'n 1:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 570
          Top = 48
          Width = 58
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'T'#236'nh tr'#7841'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 393
          Top = 48
          Width = 70
          Height = 16
          Caption = 'Thu ng'#226'n 2:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdHH1: TwwDBEdit
          Left = 570
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'SOTIEN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCK: TwwDBEdit
          Left = 678
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'CHIETKHAU_MH'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTienVAT: TwwDBEdit
          Left = 786
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THUE'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTriGiaTT: TwwDBEdit
          Left = 894
          Top = 24
          Width = 105
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'THANHTOAN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdTENDT: TwwDBEdit
          Left = 12
          Top = 24
          Width = 44
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit1: TwwDBEdit
          Left = 58
          Top = 24
          Width = 194
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'LK_TENKHO'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit2: TwwDBEdit
          Left = 13
          Top = 65
          Width = 39
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'QUAY'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit3: TwwDBDateTimePicker
          Left = 256
          Top = 24
          Width = 125
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'Tahoma'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'NGAY'
          DataSource = DsBH
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 7
        end
        object wwDBEdit4: TwwDBEdit
          Left = 256
          Top = 65
          Width = 125
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit5: TwwDBEdit
          Left = 391
          Top = 24
          Width = 131
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'THUNGAN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit6: TwwDBEdit
          Left = 56
          Top = 65
          Width = 196
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'LK_TENQUAY'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTINHTRANG: TwwDBLookupCombo
          Left = 570
          Top = 64
          Width = 213
          Height = 22
          Anchors = [akTop, akRight]
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'15'#9#9'F')
          DataField = 'TINHTRANG'
          DataSource = DsBH
          LookupTable = DataMain.QrTT_BANLE
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 11
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMaKhoNotInList
        end
        object wwDBEdit7: TwwDBEdit
          Left = 392
          Top = 65
          Width = 131
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'LK_DEBT_TEN'
          DataSource = DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object GrDetail: TwwDBGrid2
        Left = 0
        Top = 97
        Width = 1037
        Height = 410
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'STT'#9'3'#9' STT'#9'F'
          'MAVT'#9'13'#9'M'#227' h'#224'ng'#9'F'
          'TENVT'#9'32'#9'T'#234'n h'#224'ng'#9'F'
          'DVT'#9'6'#9#272'VT'#9'F'
          'SOLUONG'#9'9'#9'S'#7889' l'#432#7907'ng'#9'F'
          'DONGIA'#9'11'#9#272#417'n gi'#225#9'F'
          'SOTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'F'
          'THUE_SUAT'#9'6'#9'VAT'#9'F'
          'TL_CK'#9'5'#9'%CK'#9'F'
          'NG_DC'#9'30'#9'Ng'#432#7901'i '#273'i'#7873'u ch'#7881'nh'#9'F'
          'TRA_DATE'#9'10'#9'Ng'#224'y'#9'F'
          'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 3
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab]
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
        PadColumnStyle = pcsPadHeader
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 903
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 903
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 396
    Top = 300
    object CmdPrint: TAction
      Category = 'NAV'
      Caption = 'In'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Category = 'NAV'
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Category = 'NAV'
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdCancel: TAction
      Category = 'NAV'
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Category = 'NAV'
      Caption = 'X'#243'a'
      Hint = 'X'#243'a phi'#7871'u'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdThongtinCN_View: TAction
      Caption = 'Th'#244'ng tin'
      OnExecute = CmdThongtinCN_ViewExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsBH
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'SCT'
      'CHIETKHAU'
      'SOTIEN'
      'THANHTOAN'
      'THUNGAN'
      'NG_CK'
      'NG_HUY'
      'XOA')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 368
    Top = 300
  end
  object QrBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeEdit = QrBHBeforeEdit
    BeforePost = QrBHBeforePost
    AfterScroll = QrBHAfterScroll
    OnCalcFields = QrBHCalcFields
    OnDeleteError = QrBHPostError
    OnEditError = QrBHPostError
    OnPostError = QrBHPostError
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'BANLE'
      ' where'#9'LCT = '#39'BLE'#39
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY < :NGAYC + 1'
      '   and'#9'isnull(DELIVERY,0) = 1')
    Left = 452
    Top = 300
    object QrBHXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrBHCHIETKHAU: TFloatField
      DisplayLabel = 'Chi'#7871't kh'#7845'u'
      DisplayWidth = 14
      FieldName = 'CHIETKHAU'
      Visible = False
    end
    object QrBHSOTIEN: TFloatField
      DisplayLabel = 'Tr'#7883' gi'#225' h'#243'a '#273#417'n'
      DisplayWidth = 14
      FieldName = 'SOTIEN'
      Visible = False
    end
    object QrBHTHUE: TFloatField
      DisplayLabel = 'Thu'#7871' VAT'
      DisplayWidth = 14
      FieldName = 'THUE'
      Visible = False
    end
    object QrBHTHUNGAN: TWideStringField
      DisplayLabel = 'Thu ng'#226'n'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'THUNGAN'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CREATE_BY'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHNG_CK: TWideStringField
      DisplayLabel = 'Duy'#7879't chi'#7871't kh'#7845'u'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_CK'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CK_BY'
      Visible = False
      Size = 30
      Lookup = True
    end
    object QrBHNG_HUY: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i h'#7911'y h'#243'a '#273#417'n'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_HUY'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'DELETE_BY'
      Visible = False
      Size = 30
      Lookup = True
    end
    object QrBHNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
    end
    object QrBHCA: TWideStringField
      FieldName = 'CA'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object QrBHPRINTED: TBooleanField
      FieldName = 'PRINTED'
      Visible = False
    end
    object QrBHCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrBHUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrBHDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrBHSCT: TWideStringField
      DisplayLabel = 'S'#7889' bill'
      FieldName = 'SCT'
    end
    object QrBHTL_CK: TFloatField
      FieldName = 'TL_CK'
      Visible = False
      OnChange = QrBHTL_CKChange
    end
    object QrBHCK_BY: TIntegerField
      FieldName = 'CK_BY'
      Visible = False
    end
    object QrBHCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrBHUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrBHDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrBHCHUATHOI: TFloatField
      FieldName = 'CHUATHOI'
      Visible = False
    end
    object QrBHDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      Visible = False
      BlobType = ftWideMemo
    end
    object QrBHIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrBHCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
      Visible = False
    end
    object QrBHSOLUONG: TFloatField
      FieldName = 'SOLUONG'
      Visible = False
    end
    object QrBHMAMADT: TWideStringField
      FieldName = 'MADT'
      Visible = False
      Size = 15
    end
    object QrBHMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrBHTHANHTOAN: TFloatField
      DisplayLabel = 'Ph'#7843'i thu'
      FieldName = 'THANHTOAN'
      Visible = False
    end
    object QrBHLK_TENDT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMKH
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrBHQUAY: TWideStringField
      FieldName = 'QUAY'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrBHDRC_STATUS: TWideStringField
      FieldName = 'DRC_STATUS'
      FixedChar = True
      Size = 1
    end
    object QrBHLK_TENQUAY: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENQUAY'
      LookupDataSet = QrDMQUAY
      LookupKeyFields = 'QUAY'
      LookupResultField = 'TENMAY'
      KeyFields = 'QUAY'
      Size = 200
      Lookup = True
    end
    object QrBHLK_USERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_USERNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'USERNAME'
      KeyFields = 'CREATE_BY'
      Size = 200
      Lookup = True
    end
    object QrBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrBHTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      OnValidate = QrBHTINHTRANGValidate
      Size = 2
    end
    object QrBHLK_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TINHTRANG'
      LookupDataSet = DataMain.QrTT_BANLE
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'TINHTRANG'
      Size = 200
      Lookup = True
    end
    object QrBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrBHTTOAN1: TFloatField
      FieldName = 'TTOAN1'
    end
    object QrBHTTOAN2: TFloatField
      FieldName = 'TTOAN2'
    end
    object QrBHTTOAN3: TFloatField
      FieldName = 'TTOAN3'
    end
    object QrBHTTOAN4: TFloatField
      FieldName = 'TTOAN4'
    end
    object QrBHTTOAN5: TFloatField
      FieldName = 'TTOAN5'
    end
    object QrBHTTOAN6: TFloatField
      FieldName = 'TTOAN6'
    end
    object QrBHLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrBHTTOAN1_1: TFloatField
      FieldName = 'TTOAN1_1'
    end
    object QrBHTTOAN1_2: TFloatField
      FieldName = 'TTOAN1_2'
    end
    object QrBHDELIVERY: TBooleanField
      FieldName = 'DELIVERY'
    end
    object QrBHCN_TENDV: TWideStringField
      FieldName = 'CN_TENDV'
      Size = 200
    end
    object QrBHCN_MST: TWideStringField
      FieldName = 'CN_MST'
      Size = 50
    end
    object QrBHCN_DIACHI: TWideStringField
      FieldName = 'CN_DIACHI'
      Size = 200
    end
    object QrBHCN_DIACHI_HD: TWideStringField
      FieldName = 'CN_DIACHI_HD'
      Size = 200
    end
    object QrBHCN_LIENHE: TWideStringField
      FieldName = 'CN_LIENHE'
      Size = 200
    end
    object QrBHCN_DTHOAI: TWideStringField
      FieldName = 'CN_DTHOAI'
      Size = 50
    end
    object QrBHCN_EMAIL: TWideStringField
      FieldName = 'CN_EMAIL'
      Size = 100
    end
    object QrBHCN_MATK: TWideStringField
      FieldName = 'CN_MATK'
      Size = 30
    end
    object QrBHDEBT_BY: TIntegerField
      FieldName = 'DEBT_BY'
    end
    object QrBHDEBT_DATE: TDateTimeField
      FieldName = 'DEBT_DATE'
    end
    object QrBHLK_DEBT_TEN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DEBT_TEN'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'DEBT_BY'
      Size = 200
      Lookup = True
    end
    object QrBHLK_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'CREATE_BY'
      Size = 200
      Lookup = True
    end
  end
  object QrCTBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBHBeforeOpen
    AfterInsert = QrCTBHAfterInsert
    AfterEdit = QrCTBHAfterInsert
    BeforePost = QrCTBHBeforePost
    OnDeleteError = QrBHPostError
    OnEditError = QrBHPostError
    OnPostError = QrBHPostError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from BANLE_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 480
    Top = 300
    object QrCTBHSTT: TIntegerField
      DisplayWidth = 3
      FieldName = 'STT'
    end
    object QrCTBHMAVT: TWideStringField
      DisplayLabel = 'M'#227' b'#225'n h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      FixedChar = True
      Size = 15
    end
    object QrCTBHTENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTBHDVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 6
      FieldKind = fkLookup
      FieldName = 'DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTBHSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 9
      FieldName = 'SOLUONG'
    end
    object QrCTBHDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 11
      FieldName = 'DONGIA'
    end
    object QrCTBHLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTBHTHUE_SUAT: TFloatField
      DisplayLabel = 'VAT'
      DisplayWidth = 6
      FieldName = 'THUE_SUAT'
    end
    object QrCTBHNG_DC: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i '#273'i'#7873'u ch'#7881'nh'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'NG_DC'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'TRA_BY'
      Size = 30
      Lookup = True
    end
    object QrCTBHTRA_DATE: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'TRA_DATE'
    end
    object QrCTBHTRA_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'TRA_BY'
      Visible = False
    end
    object QrCTBHTL_CK: TFloatField
      FieldName = 'TL_CK'
    end
    object QrCTBHCHIETKHAU: TFloatField
      FieldName = 'CHIETKHAU'
    end
    object QrCTBHTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrCTBHTENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TENTAT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 25
      Lookup = True
    end
    object QrCTBHSOTIEN: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      FieldName = 'SOTIEN'
    end
    object QrCTBHGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTBHMABO: TWideStringField
      FieldName = 'MABO'
      Size = 15
    end
    object QrCTBHTL_CK2: TFloatField
      FieldName = 'TL_CK2'
    end
    object QrCTBHTL_CK3: TFloatField
      FieldName = 'TL_CK3'
    end
    object QrCTBHTL_CK4: TFloatField
      FieldName = 'TL_CK4'
    end
    object QrCTBHTL_CK5: TFloatField
      FieldName = 'TL_CK5'
    end
    object QrCTBHTL_CK6: TFloatField
      FieldName = 'TL_CK6'
    end
    object QrCTBHKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTBHTL_CK7: TFloatField
      FieldName = 'TL_CK7'
    end
    object QrCTBHTL_CK_MAX: TFloatField
      FieldName = 'TL_CK_MAX'
    end
    object QrCTBHTL_CK_THEM: TFloatField
      FieldName = 'TL_CK_THEM'
    end
    object QrCTBHTL_CK_THEM2: TFloatField
      FieldName = 'TL_CK_THEM2'
    end
    object QrCTBHCK_BY: TIntegerField
      FieldName = 'CK_BY'
    end
  end
  object DsBH: TDataSource
    DataSet = QrBH
    Left = 452
    Top = 328
  end
  object DsCT: TDataSource
    AutoEdit = False
    DataSet = QrCTBH
    Left = 480
    Top = 328
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAVT, TENVT, TENTAT, DVT, TINHTRANG'
      '  from DM_VT'
      'order by MAVT')
    Left = 508
    Top = 300
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 80
    Top = 259
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 80
    Top = 228
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Item1: TMenuItem
      Tag = 1
      Caption = 'H'#243'a '#273#417'n ch'#432'a ho'#224'n t'#7845't giao h'#224'ng'
      OnClick = ItemAllClick
    end
    object Item2: TMenuItem
      Tag = 2
      Caption = 'H'#243'a '#273#417'n '#273#227' ho'#224'n t'#7845't giao h'#224'ng'
      OnClick = ItemAllClick
    end
    object ItemAll: TMenuItem
      Caption = 'T'#7845't c'#7843' c'#225'c h'#243'a '#273#417'n'
      OnClick = ItemAllClick
    end
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsKHO
    Parameters = <
      item
        Name = 'MAKHO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_QUAYTN'
      ' where MAKHO = :MAKHO'
      'order by QUAY')
    Left = 536
    Top = 300
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_KHO'
      'order by MAKHO')
    Left = 564
    Top = 300
    object QrDMKHOMAKHO: TWideStringField
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDMKHOTENKHO: TWideStringField
      DisplayWidth = 35
      FieldName = 'TENKHO'
      Size = 100
    end
    object QrDMKHOLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
  end
  object DsKHO: TDataSource
    AutoEdit = False
    DataSet = QrDMKHO
    Left = 564
    Top = 328
  end
end
