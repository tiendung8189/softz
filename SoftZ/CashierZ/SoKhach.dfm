object FrmSoKhach: TFrmSoKhach
  Left = 192
  Top = 107
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'B'#224'n'
  ClientHeight = 311
  ClientWidth = 217
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    217
    311)
  PixelsPerInch = 96
  TextHeight = 16
  object LbTable: TAdvSmoothLabel
    Left = 0
    Top = 0
    Width = 217
    Height = 37
    Fill.Color = clTeal
    Fill.ColorTo = clTeal
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    Caption.Text = '  B'#224'n:'
    Caption.Location = plCenterLeft
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clWindowText
    Caption.Font.Height = -21
    Caption.Font.Name = 'Tahoma'
    Caption.Font.Style = [fsBold]
    Caption.ColorStart = clWhite
    Caption.ColorEnd = clWhite
    CaptionShadow.Text = '  B'#224'n:'
    CaptionShadow.Font.Charset = DEFAULT_CHARSET
    CaptionShadow.Font.Color = clWindowText
    CaptionShadow.Font.Height = -27
    CaptionShadow.Font.Name = 'Tahoma'
    CaptionShadow.Font.Style = []
    Version = '1.6.0.1'
    Transparent = False
    TextRendering = tClearType
    Align = alTop
    ExplicitLeft = -133
    ExplicitTop = 1
    ExplicitWidth = 351
  end
  object EdTable: TDBAdvSmoothLabel
    Left = 64
    Top = 4
    Width = 153
    Height = 30
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    Caption.Text = 'EdTable'
    Caption.Location = plCenterLeft
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clWindowText
    Caption.Font.Height = -21
    Caption.Font.Name = 'Tahoma'
    Caption.Font.Style = [fsBold]
    Caption.ColorStart = clWhite
    Caption.ColorEnd = clWhite
    CaptionShadow.Text = 'EdTable'
    CaptionShadow.Font.Charset = DEFAULT_CHARSET
    CaptionShadow.Font.Color = clWindowText
    CaptionShadow.Font.Height = -27
    CaptionShadow.Font.Name = 'Tahoma'
    CaptionShadow.Font.Style = []
    Version = '1.6.0.1'
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbDbState: TAdvSmoothLabel
    Left = 0
    Top = 9
    Width = 20
    Height = 24
    Fill.Color = clTeal
    Fill.ColorTo = clTeal
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    Caption.Text = '*'
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clWindowText
    Caption.Font.Height = -21
    Caption.Font.Name = 'Tahoma'
    Caption.Font.Style = [fsBold]
    Caption.ColorStart = clRed
    Caption.ColorEnd = clRed
    CaptionShadow.Text = '*'
    CaptionShadow.Font.Charset = DEFAULT_CHARSET
    CaptionShadow.Font.Color = clWindowText
    CaptionShadow.Font.Height = -27
    CaptionShadow.Font.Name = 'Tahoma'
    CaptionShadow.Font.Style = []
    Version = '1.6.0.1'
    Transparent = False
    TextRendering = tClearType
    Align = alCustom
  end
  object Panel1: TPanel
    Left = 4
    Top = 40
    Width = 212
    Height = 62
    Align = alCustom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      212
      62)
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 88
      Height = 16
      Caption = 'S'#7889' l'#432#7907'ng kh'#225'ch'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdSoKhach: TwwDBEdit
      Left = 6
      Top = 28
      Width = 197
      Height = 27
      BorderStyle = bsNone
      DataField = 'SOLUONG_KHACH'
      DataSource = FrmCashierMain.DsBH
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Frame.Enabled = True
      Frame.FocusBorders = [efBottomBorder]
      ParentFont = False
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object BtClose: TRzBitBtn
      Left = 91
      Top = 136
      Width = 97
      Height = 41
      Cursor = 1
      FrameColor = 7617536
      ShowFocusRect = False
      Anchors = [akRight, akBottom]
      Caption = 'Tho'#225't'
      Color = 15791348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      HotTrack = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      Glyph.Data = {
        C2040000424DC204000000000000420000002800000018000000180000000100
        10000300000080040000202E0000202E00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
        924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
        6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
        FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
        261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
        FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
        F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
        E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
        8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
        271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
        A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
        0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
        2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F}
      Margin = 1
      Spacing = 1
    end
  end
  object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
    Left = 4
    Top = 106
    Width = 212
    Height = 204
    AllowAutoZoom = False
    AutoCompletion.Font.Charset = DEFAULT_CHARSET
    AutoCompletion.Font.Color = clWhite
    AutoCompletion.Font.Height = -19
    AutoCompletion.Font.Name = 'Tahoma'
    AutoCompletion.Font.Style = []
    AutoCompletion.Color = clBlack
    Fill.Color = clNone
    Fill.ColorTo = clNone
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowColor = clNone
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    HighlightCaps = clWhite
    HighlightAltGr = clWhite
    KeyboardType = ktNUMERIC
    Keys = <
      item
        Caption = 'Back Space'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skBackSpace
        Color = 10526880
        X = 1
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '/'
        KeyValue = 111
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDivide
        X = 53
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '*'
        KeyValue = 106
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skMultiply
        X = 105
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '-'
        KeyValue = 109
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skSubstract
        X = 157
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '7'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '8'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '9'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '+'
        KeyValue = 107
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skAdd
        X = 157
        Y = 42
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '4'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '5'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '6'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '1'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '2'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '3'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = 'Enter'
        KeyValue = 13
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skReturn
        Color = 10526880
        X = 157
        Y = 122
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '0'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 162
        Width = 104
        SubKeys = <>
      end
      item
        Caption = '.'
        KeyValue = 110
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDecimal
        X = 105
        Y = 162
        Width = 52
        SubKeys = <>
      end>
    SmallFont.Charset = DEFAULT_CHARSET
    SmallFont.Color = clWindowText
    SmallFont.Height = -16
    SmallFont.Name = 'Tahoma'
    SmallFont.Style = []
    Version = '1.7.0.1'
    OnKeyClick = AdvSmoothTouchKeyBoard1KeyClick
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object MyActionList: TActionList
    Left = 452
    Top = 232
    object CmdWinKeyboard: TAction
      Caption = 'Screen Keyboard'
      OnExecute = CmdWinKeyboardExecute
    end
  end
end
