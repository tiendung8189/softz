﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameKho;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, wwdblook, DB, ActnList;

type
  TfrKHO = class(TFrame)
    Panel1: TPanel;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    LbKHO: TLabel;
    CbMaKho: TwwDBLookupCombo;
    CbTenKho: TwwDBLookupCombo;
    procedure CbMaKhoChange(Sender: TObject);
    procedure CbMaKhoCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
  private
  	FAction: TAction;
  public
  	procedure Init; overload;
  	procedure Init2(d1, d2: TDateTime; ac: TAction); overload;
  end;

implementation

uses
	ExCommon, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.Init;
begin
    Init2(Date - sysLateDay, Date, NIL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.Init2(d1, d2: TDateTime; ac: TAction);
begin
	EdFrom.Date := d1;
	EdTo.Date := d2;
    FAction := ac;
    CbMaKho.LookupValue := sysDefKho;
    CbTenKho.LookupValue := sysDefKho;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    if (not sysIsDrc) or (not sysIsCentral) then
        (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.CbMaKhoChange(Sender: TObject);
var
	s: String;
begin
	if (Sender as TwwDbLookupCombo).Text = '' then
		s := ''
	else
		s := (Sender as TwwDbLookupCombo).LookupValue;

    case (Sender as TWinControl).Tag of
    1:
        CbMaKho.LookupValue := s;
    2:
    	CbTenKho.LookupValue := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.CbMaKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
	if Assigned(FAction) then
	    if modified then
    	    FAction.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
