﻿object frKHO: TfrKHO
  Left = 0
  Top = 0
  Width = 451
  Height = 77
  Align = alTop
  AutoSize = True
  Color = 16119285
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 77
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentBackground = False
    ParentColor = True
    TabOrder = 0
    object Label65: TLabel
      Left = 91
      Top = 16
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 291
      Top = 16
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object LbKHO: TLabel
      Left = 44
      Top = 44
      Width = 87
      Height = 16
      Alignment = taRightJustify
      Caption = #272'i'#7875'm b'#225'n h'#224'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdFrom: TwwDBDateTimePicker
      Left = 140
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
    object EdTo: TwwDBDateTimePicker
      Left = 348
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 1
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
    object CbMaKho: TwwDBLookupCombo
      Tag = 2
      Left = 140
      Top = 40
      Width = 53
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'5'#9'MAKHO'#9'F'#9
        'TENKHO'#9'35'#9'TENKHO'#9'F'#9)
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnChange = CbMaKhoChange
      OnBeforeDropDown = CbMaKhoBeforeDropDown
      OnCloseUp = CbMaKhoCloseUp
      OnExit = CbMaKhoChange
      OnNotInList = CbMaKhoNotInList
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
    object CbTenKho: TwwDBLookupCombo
      Tag = 1
      Left = 196
      Top = 40
      Width = 301
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENKHO'#9'35'#9'TENKHO'#9'F'
        'MAKHO'#9'5'#9'MAKHO'#9'F')
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnChange = CbMaKhoChange
      OnCloseUp = CbMaKhoCloseUp
      OnExit = CbMaKhoChange
      OnNotInList = CbMaKhoNotInList
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
  end
end
