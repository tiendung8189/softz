﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_Tables;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts, wwDialog, wwfltdlg, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, Vcl.StdCtrls, wwdblook, Vcl.ExtCtrls, isPanel, wwdbdatetimepicker;

type
  TFrmFB_Tables = class(TForm)
    ActionList: TActionList;
    CmdUpdate: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton11: TToolButton;
    Status: TStatusBar;
    QrBan: TADOQuery;
    DsDatBan: TDataSource;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    Filter: TwwFilterDialog2;
    popCheck: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdCheckAll: TAction;
    CmdUnCheckAll: TAction;
    Bchn1: TMenuItem;
    CmdCancel: TAction;
    TntToolButton1: TToolButton;
    CmdUpdate2: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKhuVuc: TADOQuery;
    CbNhom: TwwDBLookupCombo;
    QrDatBan: TADOQuery;
    QrDatBanKHOA: TGuidField;
    QrDatBanMABAN: TWideStringField;
    QrDatBanMARK: TBooleanField;
    QrDatBanLK_TENBAN: TWideStringField;
    QrDatBanLK_MAKV: TWideStringField;
    QrDatBanLK_KHUVUC: TWideStringField;
    QrDatBanLK_SOLUONG: TIntegerField;
    QrDatBanGHICHU: TWideStringField;
    isPanel2: TisPanel;
    isPanel1: TisPanel;
    GrDetail: TwwDBGrid2;
    GrList: TwwDBGrid2;
    QrDatBanCT: TADOQuery;
    QrDatBanCTXOA: TWideStringField;
    QrDatBanCTIMG: TIntegerField;
    QrDatBanCTNGAY: TDateTimeField;
    QrDatBanCTIMG2: TIntegerField;
    QrDatBanCTSCT: TWideStringField;
    QrDatBanCTDGIAI: TWideMemoField;
    QrDatBanCTCREATE_BY: TIntegerField;
    QrDatBanCTUPDATE_BY: TIntegerField;
    QrDatBanCTLYDO: TWideStringField;
    QrDatBanCTCREATE_DATE: TDateTimeField;
    QrDatBanCTUPDATE_DATE: TDateTimeField;
    QrDatBanCTDELETE_DATE: TDateTimeField;
    QrDatBanCTDELETE_BY: TIntegerField;
    QrDatBanCTLOC: TWideStringField;
    QrDatBanCTLCT: TWideStringField;
    QrDatBanCTKHOA: TGuidField;
    QrDatBanCTCHECKED: TBooleanField;
    QrDatBanCTCHECKED_BY: TIntegerField;
    QrDatBanCTCHECKED_DATE: TDateTimeField;
    QrDatBanCTCHECKED_DESC: TWideMemoField;
    QrDatBanCTLK_LYDO: TWideStringField;
    QrDatBanCTHOTEN: TWideStringField;
    QrDatBanCTDTHOAI: TWideStringField;
    QrDatBanCTDS_MABAN: TWideStringField;
    QrDatBanCTSOLUONG_KHACH: TFloatField;
    QrDatBanCTTINHTRANG: TWideStringField;
    QrDatBanCTTINHTRANG_DATE: TDateTimeField;
    QrDatBanCTTHOIGIAN_GIUCHO: TFloatField;
    QrDatBanCTNGAY_HETHAN: TDateTimeField;
    QrDatBanCTSOLUONG_BAN: TFloatField;
    DsDatBanCT: TDataSource;
    QrDatBanCTMABAN: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    dateNow: TwwDBDateTimePicker;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdCheckAllExecute(Sender: TObject);
    procedure CmdUnCheckAllExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBanAfterPost(DataSet: TDataSet);
    procedure QrDatBanBeforeEdit(DataSet: TDataSet);
    procedure QrDatBanBeforeOpen(DataSet: TDataSet);
    procedure QrDatBanCTBeforeOpen(DataSet: TDataSet);
    procedure QrDatBanAfterScroll(DataSet: TDataSet);
  private
  	mKhoa: TGUID;
    mChange, mChecked: Boolean;
    mLst, mLCT: string;
    mCount: Integer;
    procedure MarkAll(bCheck: Boolean);
    procedure getMarkChange();
  public
    function  Get(const pKhoa: TGUID; const pChecked: Boolean; var pLst: String; var pCount: Integer): Boolean;

  end;

var
  FrmFB_Tables: TFrmFB_Tables;

implementation

uses
	ExCommon, isDb, isLib, isMsg, MainData, GuidEx, FB_TableOrders;

{$R *.DFM}

const
	FORM_CODE = 'FB_POS_DATBAN';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_Tables.Get;
begin
    mKhoa := pKhoa;
    mChecked := pChecked;
    ShowModal;
    pLst := mLst;
    pCount := mCount;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    mTrigger := False;
    mLCT := 'FDBAN';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.FormShow(Sender: TObject);
begin
    SetDisplayFormat(QrDatBanCT, ['NGAY', 'NGAY_HETHAN'], ShortDateFormat + ' hh:nn');
    SetCustomGrid([FORM_CODE + '_MABAN_CT', FORM_CODE + '_MABAN_LICHSU'], [GrList, GrDetail]);
	OpenDataSets([QrKhuVuc, QrBan, QrDatBan]);
    CmdUpdate.Execute;
    mChange := False;
    dateNow.DateTime := Now;
    getMarkChange();
    QrDatBan.First;
    DsDatBan.AutoEdit := not mChecked;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrKhuVuc, QrBan, QrDatBan]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    try
    	if mChange then
            case SaveConfirm3 of
            mrYes:
                begin
                    CmdSave.Execute;
                    mChange := False;
                end;
            mrNo:
                begin
                    CmdCancel.Execute;
                    mChange := False;
                end;
            end;
    finally
	    CanClose := not mChange;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.CmdUpdateExecute(Sender: TObject);
var
	maBan: String;
begin
	mTrigger := True;
	with QrBan do
    begin
    	First;
        while not Eof do
        begin
	    	maBan := FieldByName('MABAN').AsString;
    	    with QrDatBan do
            if not Locate('MABAN', maBan, []) then
            begin
                Append;
                TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
                FieldByName('MABAN').AsString := maBan;
                Post;
            end;
            Next;
		end;
    end;
	mTrigger := False;
	QrDatBan.First;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.CmdSaveExecute(Sender: TObject);
begin
   with QrDatBan do
    begin
    	DisableControls;
    	CheckBrowseMode;
        First;
        while not Eof do
        begin
        	if not FieldByName('MARK').AsBoolean then
            	Delete
            else
            	Next;
        end;
        UpdateBatch;
        First;
    	EnableControls;
    end;
    mChange := False;
    getMarkChange();
    GrList.SetFocus;
end;


procedure TFrmFB_Tables.CmdCancelExecute(Sender: TObject);
begin
    with QrDatBan do
    begin
        CancelBatch;
        Requery;
    end;
    mChange := False;
    CmdUpdate.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdSave.Enabled := mChange and not mChecked;
    CmdCancel.Enabled := mChange and not mChecked;
    CmdUpdate2.Enabled := not CmdSave.Enabled;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.QrBanAfterPost(DataSet: TDataSet);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmFB_Tables.QrDatBanAfterScroll(DataSet: TDataSet);
var
    mMABAN: string;
begin
    with QrDatBanCT do
    begin
        Close;
        Open;
    end;
end;

procedure TFrmFB_Tables.QrDatBanBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    mChange := True;
end;

procedure TFrmFB_Tables.QrDatBanBeforeOpen(DataSet: TDataSet);
begin
    QrDatBan.Parameters[0].Value := TGuidEx.ToString(mKhoa);
end;

procedure TFrmFB_Tables.QrDatBanCTBeforeOpen(DataSet: TDataSet);
begin
    with QrDatBanCT do
    begin
        Parameters[0].Value := mLCT;
        Parameters[1].Value := QrDatBan.FieldByName('MABAN').AsString;
        Parameters[2].Value := Now;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrBan, Filter);
end;



(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.CmdCheckAllExecute(Sender: TObject);
begin
	MarkAll(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.CmdUnCheckAllExecute(Sender: TObject);
begin
	MarkAll(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_Tables.MarkAll(bCheck: Boolean);

begin
	with QrDatBan do
    begin
        DisableControls;
        First;
        while not Eof do
        begin
        	Edit;
        	FieldByName('MARK').AsBoolean := bCheck;
        	Next;
        end;
        EnableControls;
    end;
end;

procedure TFrmFB_Tables.getMarkChange();
begin
    mLst := '';
    mCount := 0;
    with QrDatBan do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('MARK').AsBoolean then
            begin
                if FieldByName('MABAN').AsString = '' then
                    Break;
                if mLst <> '' then
                    mLst := mLst + ',';
                mLst := mLst + '' + FieldByName('MABAN').AsString + '';
                Inc(mCount);
            end;
            Next;
        end;
    end;
end;

end.
