﻿(* ==============================================================================
  **------------------------------------------------------------------------------
*)
unit PosMain;

interface

uses
    Windows, SysUtils, Classes, Graphics, Forms, ActnList, Menus, ExtCtrls,
    StdCtrls, Wwdbigrd, Wwdbgrid2, Mask, Db, ADODB, wwdblook, isEnv, AppEvnts,
    AdvMenus, fcStatusBar, RzPanel, Controls, wwdbedit, HTMLabel,
    RzButton, isDb, ImgList, Grids, Wwdbgrid, pngimage, isOneInstance,
    exPrintBill,
    AdvGlassButton, AdvGlowButton, AdvSmoothButton, AdvMetroButton,
    AdvSelectors,
    AdvOfficeSelectors, wwdbdatetimepicker, RzSplit, AdvSmoothTileList,
    AdvSmoothTileListEx, GDIPPictureContainer, AdvAlertWindow,
    AdvSmoothTileListImageVisualizer, AdvSmoothTileListHTMLVisualizer,
    AdvOfficeStatusBar, AdvOfficeStatusBarStylers, rImprovedComps,
    AdvSmoothTouchKeyBoard, isPanel, RzTabs, PictureContainer, htmlbtns,
  Vcl.DBCtrls, wwcheckbox;

type
    TFrmMain = class(TForm)
        MyActionList: TActionList;
        CmdQuit: TAction;
        CmdSetpass: TAction;
        ImgLarge: TImageList;
        CmdSave: TAction;
        Bevel1: TBevel;
        QrBH: TADOQuery;
        DsBH: TDataSource;
        QrCTBH: TADOQuery;
        DsCTBH: TDataSource;
        QrCTBHMAVT: TWideStringField;
        QrCTBHSOLUONG: TFloatField;
        QrCTBHDONGIA: TFloatField;
        QrCTBHLK_TENVT: TWideStringField;
        QrCTBHDVT: TWideStringField;
        CmdReprint: TAction;
        CmdDiscount: TAction;
        QrTAM: TADOQuery;
        QrINLAI: TADOQuery;
        CmdScanReturnQty: TAction;
        QrCTBHSTT: TIntegerField;
        Bevel4: TBevel;
        QrCTBHTRA_DATE: TDateTimeField;
        CmdPrint: TAction;
        QrCTBHTL_CK: TFloatField;
        QrBHCALC_TONGCK: TFloatField;
        Popup: TAdvPopupMenu;
        CmdScanVIP: TAction;
        POS_VIP: TADOCommand;
        Hthng1: TMenuItem;
        CmdLock: TAction;
        CmdCommInfo: TAction;
        QrCTBHSOTIEN: TFloatField;
        CmdSetPrinter: TAction;
        ApplicationEvents1: TApplicationEvents;
        GrList: TwwDBGrid2;
        CmdSave2: TAction;
        QrBHCALC_TIENTHOI: TFloatField;
        QrCTBHGHICHU: TWideStringField;
        QrCTBHRSTT: TIntegerField;
        PaHeader: TPanel;
        HTMLabel15: THTMLabel;
        EdCounter: TwwDBEdit;
        HTMLabel17: THTMLabel;
        EdCashier: TwwDBEdit;
        Status: TfcStatusBar;
        ImageList1: TImageList;
        HTMLabel13: THTMLabel;
        EdCus: TwwDBEdit;
        CmdScanSku: TAction;
        CmdScanQty: TAction;
        HTMLabel1: THTMLabel;
        EdRno: TwwDBEdit;
        QrBHLK_TENKHO: TWideStringField;
        CmdChangeRetailer: TAction;
        QrBHLCT: TWideStringField;
        QrBHNGAY: TDateTimeField;
        QrBHQUAY: TWideStringField;
        QrBHCA: TWideStringField;
        QrBHSCT: TWideStringField;
        QrBHMAVIP: TWideStringField;
        QrBHMAKHO: TWideStringField;
        QrBHCHIETKHAU_MH: TFloatField;
        QrBHSOTIEN: TFloatField;
        QrBHSOLUONG: TFloatField;
        QrBHCHUATHOI: TFloatField;
        QrBHTHANHTOAN: TFloatField;
        QrBHDGIAI: TWideMemoField;
        QrBHPRINTED: TBooleanField;
        QrBHCREATE_BY: TIntegerField;
        QrBHCREATE_DATE: TDateTimeField;
        QrCTBHTRA_BY: TIntegerField;
        QrDMVT_BO: TADOQuery;
        CmdReconnect: TAction;
        POS_PAID: TADOCommand;
        QrDMVT: TADOQuery;
        CmdReLoad: TAction;
        QrBHKHOA: TGuidField;
        QrCTBHKHOA: TGuidField;
        QrCTBHKHOACT: TGuidField;
        QrBHTINHTRANG: TWideStringField;
        CmdCongnobl: TAction;
        QrBHCN_TENDV: TWideStringField;
        QrBHCN_MST: TWideStringField;
        QrBHCN_DIACHI: TWideStringField;
        QrBHCN_DIACHI_HD: TWideStringField;
        QrBHCN_LIENHE: TWideStringField;
        QrBHCN_DTHOAI: TWideStringField;
        QrBHCN_EMAIL: TWideStringField;
        QrBHCN_MATK: TWideStringField;
        QrBHLK_TENTK: TWideStringField;
        QrBHLK_DAIDIEN: TWideStringField;
        QrBHLK_NGANHANG: TWideStringField;
        CmdDiscount1: TAction;
        QrBHPRINT_NO: TIntegerField;
        QrBHLOC: TWideStringField;
        CmdChitiet: TAction;
        Panel2: TPanel;
        AdvGlowButton12: TAdvGlowButton;
        Panel5: TPanel;
        Panel9: TPanel;
        AdvGlowButton15: TAdvGlowButton;
        AdvGlowButton19: TAdvGlowButton;
        QrCTBHLK_TENVT_KHONGDAU: TWideStringField;
        HTMLabel2: THTMLabel;
        HTMLabel5: THTMLabel;
        EdDate: TwwDBDateTimePicker;
        QrDMKHO: TADOQuery;
        CmdBanle: TAction;
        CmdTrahang: TAction;
        QrCTBHTL_CK_MAX: TFloatField;
        QrCTBHTL_CK_THEM: TFloatField;
        QrCTBHTENVT: TWideStringField;
        Panel13: TPanel;
        Panel14: TPanel;
        Panel15: TPanel;
        Panel17: TPanel;
        AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler;
        AlFillter: TAdvAlertWindow;
        PicBan: TGDIPPictureContainer;
        Image48: TImageList;
        CmdCamung: TAction;
        QrCTBHTENTAT: TWideStringField;
        QrCTBHLK_TENTAT: TWideStringField;
        QrBHDELIVERY: TBooleanField;
        CmdPhieuquatang: TAction;
        CmdGiaohang: TAction;
        POS_PQT: TADOCommand;
        QrBHTIENTHOI: TFloatField;
        QrBHLK_TENVIP: TWideStringField;
        QrBHLK_VIP_DTHOAI: TWideStringField;
        QrBHLK_VIP_DCHI: TWideStringField;
        QrBH_id: TLargeintField;
        QrBHMAQUAY: TWideStringField;
        QrBHMABAN: TWideStringField;
        QrBHSOLUONG_KHACH: TFloatField;
        isPanel3: TisPanel;
        HTMLabel9: THTMLabel;
        HTMLabel7: THTMLabel;
        EdChange: TwwDBEdit;
        wwDBEdit23: TwwDBEdit;
        Panel3: TPanel;
        AdvGlowButton1: TAdvGlowButton;
        QrCTBHBO: TBooleanField;
        QrCTBHCHIETKHAU_MH: TFloatField;
        QrCTBHSOTIEN_SAU_CKMH: TFloatField;
        QrCTBHTHANHTIEN: TFloatField;
        QrCTBHLOC: TWideStringField;
        QrCTBHTL_CKMH: TFloatField;
        QrCTBHTL_CKHD: TFloatField;
        QrCTBHTL_CK_VIPNHOM: TFloatField;
        QrCTBHTL_CK_VIPDM: TFloatField;
        QrCTBHTL_CK_PHIEU: TFloatField;
        QrCTBHCKHD_BY: TIntegerField;
        QrBHCKHD_BY: TIntegerField;
        QrBHTL_CKVIPDM: TFloatField;
        QrBHPTNX: TWideStringField;
        QrBHNGAY2: TDateTimeField;
        QrBHSCT2: TWideStringField;
        QrBHTL_CKHD: TFloatField;
        QrBHSOTIEN_SAU_CKMH: TFloatField;
        QrBHTHANHTIEN: TFloatField;
        QrBHHINHTHUC_GIA: TWideStringField;
        QrBHTHANHTIEN_CHUA_CL: TFloatField;
        QrBHTHANHTIEN_CL: TFloatField;
        QrBHTHANHTIEN_CHUA_VAT: TFloatField;
        QrBHTIEN_THUE: TFloatField;
        QrBHTIEN_THUE_5: TFloatField;
        QrBHTIEN_THUE_10: TFloatField;
        QrBHTIEN_THUE_OR: TFloatField;
        QrBHTTOAN_CASH: TFloatField;
        QrBHTTOAN_CARD: TFloatField;
        QrBHTTOAN_TRAHANG: TFloatField;
        QrBHTTOAN_VOUCHER: TFloatField;
        QrBHTTOAN_EWALLET: TFloatField;
        QrBHUPDATE_BY: TIntegerField;
        QrBHDEBT_BY: TIntegerField;
        QrBHDELETE_BY: TIntegerField;
        QrBHUPDATE_DATE: TDateTimeField;
        QrBHDEBT_DATE: TDateTimeField;
        QrBHDELETE_DATE: TDateTimeField;
        QrBHMADT: TWideStringField;
    QrCTBHCALC_SOTIEN_SAUCK: TFloatField;
    QrBHSOLUONG_DAT: TFloatField;
    QrBHTHUNGAN: TIntegerField;
    QrBHNGAYVAO: TDateTimeField;
    QrBHQUAY_ORDER: TWideStringField;
    QrBHMAQUAY_ORDER: TWideStringField;
    QrBHTHUCTRA: TFloatField;
    QrBHCO_HOADON: TBooleanField;
    QrBHORDER_BY: TIntegerField;
    QrBHORDER_DATE: TDateTimeField;
    EdOrder: TwwDBEdit;
    QrBHTL_PHUTHU: TFloatField;
    QrBHPHUTHU: TFloatField;
    QrCTBHTL_PHUTHU: TFloatField;
    QrCTBHPHUTHU: TFloatField;
    Panel4: TPanel;
    HTMLabel4: THTMLabel;
    HTMLabel18: THTMLabel;
    HTMLabel19: THTMLabel;
    wwDBEdit1: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    PN_TTOAN_CASH: TPanel;
    wwDBEdit4: TwwDBEdit;
    isPanel1: TisPanel;
    isPanel2: TisPanel;
    HTMLButton1: THTMLButton;
    PN_TTOAN_CARD: TPanel;
    wwDBEdit5: TwwDBEdit;
    HTMLButton2: THTMLButton;
    PN_TTOAN_EWALLET: TPanel;
    wwDBEdit6: TwwDBEdit;
    HTMLButton3: THTMLButton;
    HTMLabel3: THTMLabel;
    wwDBEdit7: TwwDBEdit;
    HTMLabel6: THTMLabel;
    wwDBEdit8: TwwDBEdit;
    PN_TTOAN_VOUCHER: TPanel;
    wwDBEdit9: TwwDBEdit;
    HTMLButton4: THTMLButton;
    PN_TTOAN_TRAHANG: TPanel;
    wwDBEdit10: TwwDBEdit;
    HTMLButton5: THTMLButton;
    Panel12: TPanel;
    AdvGlowButton2: TAdvGlowButton;
    Panel16: TPanel;
    CmdXuatHD: TAction;
    QrBHCALC_TIEN_THUE: TFloatField;
    CmdPaymentCash: TAction;
    CmdPaymentBankCard: TAction;
    CmdPaymentEWallet: TAction;
    CmdPaymentVoucher: TAction;
    CmdPaymentInvoice: TAction;
    QrBHCALC_CHUA_THOI: TFloatField;
    CmdPaymentList: TAction;
    Panel1: TPanel;
    HTMLButton6: THTMLButton;
    QrTT: TADOQuery;
    QrTTSTT: TIntegerField;
    QrTTPTTT: TWideStringField;
    QrTTSOTIEN: TFloatField;
    QrTTTypeInput: TWideStringField;
    QrTTTypeCode: TWideStringField;
    QrTTTypeBalance: TFloatField;
    QrTTTypeID: TGuidField;
    QrTTKHOACT: TGuidField;
    QrTTKHOA: TGuidField;
    QrTTTTOAN_CASH: TFloatField;
    QrTTTTOAN_CARD: TFloatField;
    QrTTTTOAN_TRAHANG: TFloatField;
    QrTTTTOAN_VOUCHER: TFloatField;
    QrTTTTOAN_EWALLET: TFloatField;
    QrTTTHUNGAN: TIntegerField;
    QrTTCREATE_BY: TIntegerField;
    QrTTCREATE_DATE: TDateTimeField;
    QrTTLOC: TWideStringField;
    QrTTRSTT: TIntegerField;
    QrTT_id: TLargeintField;
    QrBHMAVIP_HOTEN: TWideStringField;
    QrBHTTOAN_BANK: TFloatField;
    QrBHTTOAN_MEMBER: TFloatField;
    PN_TTOAN_BANK: TPanel;
    wwDBEdit11: TwwDBEdit;
    HTMLButton7: THTMLButton;
    CmdPaymentBankTransfer: TAction;
    QrBHCN_GHICHU: TWideStringField;
    wwCheckBox1: TwwCheckBox;
    wwCheckBox2: TwwCheckBox;
    Panel6: TPanel;
    AdvGlowButton3: TAdvGlowButton;
    Panel7: TPanel;
    AdvGlowButton4: TAdvGlowButton;
    QrCTBHDONGIA_SAU_CK: TFloatField;
    QrCTBHTHANHTIEN_CHUA_CL: TFloatField;
    QrCTBHTHANHTIEN_CL: TFloatField;
    QrCTBHTHANHTIEN_CHUA_VAT: TFloatField;
    QrCTBHDONGIA_CHUA_VAT: TFloatField;
    QrCTBHLOAITHUE: TWideStringField;
    QrCTBHTHUE_SUAT: TFloatField;
    QrCTBHTIEN_THUE: TFloatField;
    QrCTBHTIEN_THUE_5: TFloatField;
    QrCTBHTIEN_THUE_10: TFloatField;
    QrCTBHTIEN_THUE_OR: TFloatField;
    QrCTBHTHANHTOAN: TFloatField;
        procedure CmdQuitExecute(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure CmdSetpassExecute(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure CmdSaveExecute(Sender: TObject);
        procedure FormClose(Sender: TObject; var Action: TCloseAction);
        procedure CmdReprintExecute(Sender: TObject);
        procedure QrBHAfterPost(DataSet: TDataSet);
        procedure MyActionListUpdate(Action: TBasicAction;
          var Handled: Boolean);
        procedure QrBHAfterInsert(DataSet: TDataSet);
        procedure QrBHBeforePost(DataSet: TDataSet);
        procedure QrCTBHAfterPost(DataSet: TDataSet);
        procedure CmdDiscountExecute(Sender: TObject);
        procedure QrDBError(DataSet: TDataSet; E: EDatabaseError;
          var Action: TDataAction);
        procedure QrCTBHAfterInsert(DataSet: TDataSet);
        procedure CmdScanReturnQtyExecute(Sender: TObject);
        procedure QrBHBeforeOpen(DataSet: TDataSet);
        procedure QrBHCalcFields(DataSet: TDataSet);
        procedure CmdHelpExecute(Sender: TObject);
        procedure CmdPrintExecute(Sender: TObject);
        procedure GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
          FooterCellRect: TRect; Field: TField; FooterText: String;
          var DefaultDrawing: Boolean);
        procedure GrListUpdateFooter(Sender: TObject);
        procedure GrListKeyPress(Sender: TObject; var Key: Char);
        procedure CmdAboutExecute(Sender: TObject);
        procedure CmdScanVIPExecute(Sender: TObject);
        procedure CmdLockExecute(Sender: TObject);
        procedure CmdCommInfoExecute(Sender: TObject);
        procedure CmdFaqExecute(Sender: TObject);
        procedure CmdSetPrinterExecute(Sender: TObject);
        procedure MyActionListExecute(Action: TBasicAction;
          var Handled: Boolean);
        procedure TntFormResize(Sender: TObject);
        procedure CmdSave2Execute(Sender: TObject);
        procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
        procedure QrCTBHBeforePost(DataSet: TDataSet);
        procedure QrCTBHCalcFields(DataSet: TDataSet);
        procedure QrCTBHAfterDelete(DataSet: TDataSet);
        procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
        procedure QrTAMBeforeOpen(DataSet: TDataSet);
        procedure FormKeyPress(Sender: TObject; var Key: Char);
        procedure CmdChangeRetailerExecute(Sender: TObject);
        procedure CmdReconnectExecute(Sender: TObject);
        procedure CmdReLoadExecute(Sender: TObject);
        procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
          var Action: TDataAction);
        procedure CmdDiscount1Execute(Sender: TObject);
        procedure QrBHCN_DIACHI_HDChange(Sender: TField);
        procedure CmdChitietExecute(Sender: TObject);
        procedure QrCTBHTL_CK_MAXChange(Sender: TField);
        procedure QrCTBHMAVTChange(Sender: TField);
        procedure QrCTBHTL_CKChange(Sender: TField);
        procedure QrBHTTOAN3_SCTChange(Sender: TField);
        procedure CmdPhieuquatangExecute(Sender: TObject);
        procedure CmdGiaohangExecute(Sender: TObject);
        procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure QrBHTTOAN_CASHChange(Sender: TField);
  
    procedure QrBHSOTIEN_SAU_CKMHChange(Sender: TField);
    procedure CmdXuatHDExecute(Sender: TObject);
    procedure CmdPaymentCashExecute(Sender: TObject);
    procedure CmdPaymentInvoiceExecute(Sender: TObject);
    procedure CmdPaymentVoucherExecute(Sender: TObject);
    procedure CmdPaymentEWalletExecute(Sender: TObject);
    procedure CmdPaymentBankCardExecute(Sender: TObject);
    procedure CmdPaymentListExecute(Sender: TObject);
    procedure QrTTBeforeOpen(DataSet: TDataSet);
    procedure QrTTAfterInsert(DataSet: TDataSet);
    procedure CmdPaymentBankTransferExecute(Sender: TObject);
    procedure QrCTBHTL_CKMHChange(Sender: TField);
    
    private
        mVATMode: Integer;
        mLastDate, mLastUpDate: TDateTime;

        function IsEnableFunc(const pFunc: String): Boolean;
        procedure OpenQueries;
        procedure Total(pReOrder: Boolean = False);

        procedure GetCustomerName;

        procedure posPaid;
        procedure GetOrderInfo;

    protected
    public
        mSqlTam, mMABAN, mTENBAN: String;
        dThanhtoan, mNhacnhoTT: Boolean;
        // Thông tin mặt hàng scan sau cùng
        curStt, payStt, mDefaultZero: Integer;
        curSoluong, curGiaban, curTlck7, curTlck3, curThsuat, curSlBo,
          curTlckBo, mNhanTT: Double;
        curLThue, mVipTen: String;
        mDefSoluong: Double;
        // Hóa đơn hiện tại
        curKhoa: TGUID;

        procedure InvalidAction(msg: String = '');
        procedure ItemAddNew(const ma: String; const pMabo: String = '');
        procedure ItemSetQty(const pSoluong: Double);

        procedure GoLast;
        function SaveBill(pLoai: Integer; pDesc: String = ''): Boolean;
        procedure PrintBill(pKhoa: TGUID);
        Function exThelenh: Boolean;
        function Execute(pKhoa: TGUID; pMABAN, pTENBAN: string): Boolean;
        procedure ProcScanVIP;
        function getPosVip(mMAKHO, mMAVIP, mMAVT, mMABO: String; mNGAY: TDateTime): Boolean;
    end;

var
    FrmMain: TFrmMain;

implementation

uses
    MainData, ExCommon, isMsg, isStr, SoBill, Rights, CkBill, isDba, isOdbc,
    isLib, PosCommoInfo, RepEngine, Printer, TempReceipt, CkMa,
    ReceiptDesc, isCommon, exResStr, PosVIP, Retailer, Payment,
    Variants, ADOInt, GuidEx, TheLenh, exCOMPort, SystemCriticalU,
    PosChitiet, SetGhichu, ThongtinCN, CashierMain,
    PaymentMultiple;

{$R *.DFM}

const
    FORM_CODE = 'FB_POS_BANHANG';

{$REGION '<< Form events >>'}

    (* ==============================================================================
      **------------------------------------------------------------------------------
    *)
procedure TFrmMain.FormCreate(Sender: TObject);
const
    imgext: array [1 .. 3] of String = ('png', 'jpg', 'bmp');
var
    i: Integer;
begin
    mSqlTam := QrTAM.SQL.Text;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
resourcestring
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

procedure TFrmMain.FormShow(Sender: TObject);
const
    imgext: array [1 .. 3] of String = ('png', 'jpg', 'bmp');
var
    i, h: Integer;    s: String;
begin
    TMyForm(Self).Init;

    // Other init
    SetDefButton(1);

    ShortTimeFormat := 'hh:nn';
    mTrigger := False;
    mLastDate := Now;

    (*
      ** Optional settings
    *)
    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger(FORM_CODE, 'VAT Form');

    dThanhtoan := FlexConfigBool(FORM_CODE, 'Default Thanh Toan', True);

    mNhanTT := GetSysParam('BL_DONVI_TIENTHOI');
    mNhacnhoTT := GetSysParam('BL_NHACNHO_TIENTHOI');

    CmdChangeRetailer.Visible := IsEnableFunc('POS_CHANGE_RETAILER');
    CmdScanVIP.Enabled := IsEnableFunc('SZ_VIP');

     // Init bill printer
    exBillInitial(FrmCashierMain.mPrinter, '', 'a.PRINTER = b.PRINTER');


    PN_TTOAN_CASH.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_CASH', False);
    PN_TTOAN_EWALLET.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_EWALLET', False);
    PN_TTOAN_VOUCHER.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_VOUCHER', False);
    PN_TTOAN_CARD.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_CARD', False);
    PN_TTOAN_BANK.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_BANK', False);
    PN_TTOAN_TRAHANG.Visible := FlexConfigBool(FORM_CODE, 'TTOAN_TRAHANG', False);

    // Tuy bien luoi
    SetCustomGrid(FORM_CODE, GrList);

    OpenQueries;

    // Display format
    SetDisplayFormat(QrBH, sysCurFmt);
    SetDisplayFormat(QrBH, ['SOLUONG'], sysQtyFmt);
    SetShortDateFormat(QrBH);

    SetDisplayFormat(QrDMVT, sysCurFmt);
    SetDisplayFormat(QrDMVT, ['TL_LAI'], sysPerFmt);

    with QrCTBH do
    begin
        SetDisplayFormat(QrCTBH, sysCurFmt);
        SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
        SetDisplayFormat(QrCTBH, ['TL_CK'], sysPerFmt);
    end;

    EdCashier.Text := sysLogonFullName;
    MyActionList.Tag := 1;
    mDefaultZero := 0;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
    b: Boolean;
begin
    // Cho close nếu thấy connection failured
    b := False;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;
{$ENDREGION}

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
    Close;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetpassExecute(Sender: TObject);
begin
    DataMain.isLogon.ResetPassword;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLockExecute(Sender: TObject);
begin
    DataMain.isLogon.Lock;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHelpExecute(Sender: TObject);
begin
    ShowHelp;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)


procedure TFrmMain.CmdAboutExecute(Sender: TObject);
begin
    // Application.CreateForm(TFrmAbout, FrmAbout);
    // FrmAbout.ShowModal;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
    if not YesNo(RS_RECONNECT) then
        Exit;
    // DbConnect;
    OpenQueries;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReLoadExecute(Sender: TObject);
begin
    Wait(DATAREADING);
    with QrDMVT do
        if Active then
            Requery
        else
            Open;

    ClearWait;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bEmpty, bDelivery, isInvoice, isPayment: Boolean;
    fChuaThoi, fThucTra, fTienThoi: Double;
begin
    with QrBH do
    begin
        if not Active then
            Exit;

        bDelivery := FieldByName('DELIVERY').AsBoolean;
        isInvoice := FieldByName('CO_HOADON').AsBoolean;

        fChuaThoi := FieldByName('CHUATHOI').AsFloat;
        fThucTra  := FieldByName('THUCTRA').AsFloat;
        fTienThoi := fChuaThoi - fThucTra;
        isPayment := fTienThoi >= 0;
    end;
    with QrCTBH do
    begin
        if not Active then
            Exit;

        bEmpty := IsEmpty;
    end;

    CmdChangeRetailer.Enabled := bEmpty;
    CmdScanReturnQty.Enabled := not bEmpty;
    CmdReprint.Enabled := bEmpty;
    // CmdSetPrinter.Enabled := mPrinter.PrintType = 0;
    // CmdSetPrinter.Visible := mPrinter.PrintType = 0;

    CmdDiscount.Enabled := not bEmpty;
    CmdDiscount1.Enabled := not bEmpty;
    CmdPrint.Enabled := not bEmpty;

    CmdSave.Enabled := not bEmpty;
    // CmdSave2.Enabled := not bEmpty;
    CmdChitiet.Enabled := not bEmpty;

    CmdPhieuquatang.Enabled := bEmpty;
    CmdGiaohang.Enabled := not bEmpty and FrmCashierMain.toGo;
    CmdXuatHD.Enabled := not bEmpty;

    PN_TTOAN_CASH.Enabled := not isPayment;
    PN_TTOAN_EWALLET.Enabled := not isPayment;
    PN_TTOAN_VOUCHER.Enabled := not isPayment;
    PN_TTOAN_CARD.Enabled := not isPayment;
    PN_TTOAN_BANK.Enabled := not isPayment;
    PN_TTOAN_TRAHANG.Enabled := not isPayment;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChangeRetailerExecute(Sender: TObject);
var
    n, mUser: Integer;
begin
    if GetRights('POS_CHANGE_RETAILER', False) <> R_DENY then
        mUser := sysLogonUID
    else
    begin
        // Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, 'POS_CHANGE_RETAILER') = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    Application.CreateForm(TFrmRetailer, FrmRetailer);
    if FrmRetailer.Execute(posMakho, posQuay, posQuay0) then
        with QrBH do
        begin
            FieldByName('MAKHO').AsString := posMakho;
            FieldByName('QUAY').AsString := posQuay;
            posSct := DataMain.AllocRetailBillNumberFB(posQuay, Date);
            FieldByName('SCT').AsString := posSct;
            posSct := isSmartInc(posSct, 2);
        end;
    FrmRetailer.Free;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCommInfoExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmCommoInfo, FrmCommoInfo);
    FrmCommoInfo.Execute(DataMain.QrDMVT);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSaveExecute(Sender: TObject);
var
    s: string;
    fChuaThoi, fThucTra, fTienThoi, fSoTien: Double;
begin
    s := '';
    if QrCTBH.IsEmpty then
        Exit;

    with QrBH do
    begin
        fChuaThoi := FieldByName('CHUATHOI').AsFloat;
        fThucTra  := FieldByName('THUCTRA').AsFloat;
        fTienThoi := fChuaThoi - fThucTra;

        if fTienThoi  < 0 then
        begin
            if not YesNo('Số tiền thanh toán còn lại sẽ mặc định "Tiền mặt", đồng thời chỉ Lưu và không in hóa đơn. Tiếp tục?') then
                Exit;

            fSoTien := fThucTra - fChuaThoi;

            QrTT.Open;

            with QrTT do
            begin
                payStt := RecordCount;
                Append;
                FieldByName('SOTIEN').AsFloat := exVNDRound(fSoTien, ctCurRound);
                FieldByName('TTOAN_CASH').AsFloat := exVNDRound(fSoTien, ctCurRound);
                Post;
            end;


            with QrBH do
            begin
                SetEditState(QrBH);
                mTrigger := True;
                FieldByName('TTOAN_CASH').AsFloat := exVNDRound(FieldByName('TTOAN_CASH').AsFloat + fSoTien, ctCurRound);
                mTrigger := False;
                Post;
            end;

            SaveBill(0);
        end
        else
        begin
             if not YesNo('Chỉ lưu và không in hóa đơn. Tiếp tục?') then
                Exit;

             SaveBill(0);
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.PrintBill(pKhoa: TGUID);
begin
    try
        if FrmCashierMain.mPrinter.BillPrint(pKhoa) then
            DataMain.UpdatePrintNo(pKhoa);
    except
        on E: Exception do
            ErrMsg(E.Message, 'Lỗi in Bill');
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPrintExecute(Sender: TObject);
var
    fChuaThoi, fThucTra, fTienThoi: Double;
begin
    if QrCTBH.IsEmpty then
        Exit;

    with QrBH do
    begin
       fChuaThoi := FieldByName('CHUATHOI').AsFloat;
       fThucTra  := FieldByName('THUCTRA').AsFloat;
       fTienThoi := fChuaThoi - fThucTra;
       if FieldByName('DELIVERY').AsBoolean then
       begin
           if fTienThoi < 0 then
           begin
               PrintBill(curKhoa);
           end
           else
           begin
               SaveBill(1);
               PrintBill(curKhoa);
           end;
       end
       else
       begin
           if fTienThoi < 0 then
           begin
               ErrMsg('Số tiền không hợp lệ.', 'Thanh toán',1);
               Exit;
           end
           else
           begin;
               SaveBill(1);
               PrintBill(curKhoa);
           end;
       end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
resourcestring
    RS_RECEIPT_NO = 'Số hóa đơn không đúng.';

procedure TFrmMain.CmdReprintExecute(Sender: TObject);
var
    s: String;
    m, y: Integer;
    dd, mm, yy: WORD;
    k: TGUID;
begin
    DecodeDate(Date, yy, mm, dd);
    m := mm;
    y := yy;
    s := lastSct;

    Application.CreateForm(TFrmSoBill, FrmSoBill);
    if not FrmSoBill.GetBillNo(m, y, s) then
        Exit;

    // Lay khoa chung tu
    with QrINLAI do
    begin
        Parameters[0].Value := m;
        Parameters[1].Value := y;
        Parameters[2].Value := posMaQuay;
        Parameters[3].Value := s;
        Open;
        if IsEmpty then
        begin
            msg(RS_RECEIPT_NO);
            Close;
            Exit;
        end;
        k := TGuidField(FieldByName('KHOA')).AsGuid;
        Close;
    end;

    // In lai
    PrintBill(k);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscount1Execute(Sender: TObject);
var
    mMa: String;
    mCk: Double;
    bm: TBytes;
    mUser: Integer;
    result: Boolean;
begin
    if QrCTBH.FieldByName('DONGIA').AsFloat = 0.0 then
        Exit;

    if GetRights('FB_POS_CHIETKHAU_MH', False) <> R_DENY then
        mUser := sysLogonUID
    else
    begin
        // Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, 'FB_POS_CHIETKHAU_MH') = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    with QrCTBH do
    begin
        if IsEmpty then
            Exit;

        mCk := FieldByName('TL_CKMH').AsFloat;

        Application.CreateForm(TFrmCkma, FrmCkma);
        if not FrmCkma.GetDiscount(FieldByName('TENVT').AsString,
          FieldByName('SOTIEN').AsFloat, mCk) then
            Exit;

        mMa := FieldByName('MAVT').AsString;
        bm := BookMark;
        DisableControls;

        First;
        while not Eof do
        begin
            if (FieldByName('MAVT').AsString = mMa) and
              (FieldByName('DONGIA').AsFloat <> 0.0) then
            begin
                Edit;
                FieldByName('TL_CKMH').AsFloat := exVNDRound(mCk, sysPerRound);
            end;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;

        result := cloneDataDetail(curKhoa);
        if result then
        begin
             QrBH.Requery;
             QrCTBH.Requery;
        end;
    end;
end;


(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscountExecute(Sender: TObject);
var
    mUser: Integer;
    dgiai: String;
    k: Double;
    result: Boolean;
begin
    if GetRights('FB_POS_CHIETKHAU', False) <> R_DENY then
        mUser := sysLogonUID
    else
    begin
        // Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, 'FB_POS_CHIETKHAU') = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    with QrBH do
    begin
        k := FieldByName('TL_CKHD').AsFloat;
        dgiai := FieldByName('DGIAI').AsString;
    end;
    // Get discount info.
    Application.CreateForm(TFrmChietkhau, FrmChietkhau);
    if not FrmChietkhau.Get(QrBH.FieldByName('SOTIEN').AsFloat, k, dgiai) then
        Exit;

    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('TL_CKHD').AsFloat := exVNDRound(k, sysPerRound);
        FieldByName('CKHD_BY').AsInteger := mUser;
        FieldByName('DGIAI').AsString := dgiai;
    end;
    Total;

    result := cloneDataDetail(curKhoa);
    if result then
    begin
         QrBH.Requery;
         QrCTBH.Requery;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanReturnQtyExecute(Sender: TObject);
var
    mUser: Integer;
begin
    if GetRights('FB_POS_TRAHANG', False) <> R_DENY then
        mUser := sysLogonUID
    else
    begin
        if DataMain.GetPassCode(posMakho) <> '' then // test. Will remove
        begin
            if not exThelenh then
                Exit;
            mUser := sysLogonUID;
        end
        else
        begin
            // Logon
            if not DataMain.isLogon.Logon3(mUser) then
                Exit;

            if GetRights(mUser, 'FB_POS_TRAHANG') = R_DENY then
            begin
                DenyMsg;
                Exit;
            end;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_VIP = 'Sai mã khách hàng thân thiết.';

procedure TFrmMain.CmdScanVIPExecute(Sender: TObject);
begin
    // with EdCode do
    // begin
    // Tag := 2;
    // Text := '';
    // SetFocus;
    // end;
    // ScanType;
    //ProcScanVIP;
end;

(*
  ** Functions
*)
(* ==============================================================================
  **------------------------------------------------------------------------------\
  pLoai:
  0: Save           --> paid
  1: Save and Print --> paid
  2: Save temp      --> not paid
  3: Save and delivery    --> paid
*)
function TFrmMain.SaveBill(pLoai: Integer; pDesc: String): Boolean;
var
    bPrinted, bPosPaid: Boolean;
begin
    Result := False;
    with QrCTBH do
    begin
        CheckBrowseMode;
        if IsEmpty and (isSmartInc(QrBH.FieldByName('SCT').AsString) = posSct)
        then
            Exit;
    end;

    Total(True);

    with QrBH do
    begin
        if State in [dsBrowse] then
            Edit;

        case pLoai of
            1:
                begin
                    bPrinted := True;
                    bPosPaid := True;
                end;
            2:
                begin
                    bPrinted := False;
                    bPosPaid := False;

                    FieldByName('DGIAI').AsString := pDesc;
                end;
            3:
                begin
                    bPrinted := True;
                    bPosPaid := True;

                end;
        else
            begin
                bPrinted := False;
                bPosPaid := True;

//                mTrigger := True;
//                FieldByName('TTOAN_CASH').AsFloat := FieldByName('THUCTRA').AsFloat;
//                mTrigger := False;
            end;
        end;

        FieldByName('NGAY').AsDateTime := Now;
        FieldByName('QUAY').AsString := posQuay;
        FieldByName('MAQUAY').AsString := posMaQuay;
//        FieldByName('CA').AsString := '1';
        FieldByName('THUNGAN').AsInteger := sysLogonUID;

        FieldByName('PRINTED').AsBoolean := bPrinted;
        FieldByName('SCT').AsString := DataMain.AllocRetailBillNumberFB(posMaQuay, Date);
        Post;

        if bPosPaid then
            posPaid;
    end;

    // Done
    icdComPort.IcdThanks;
    Close;

    Result := True;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.Total;
var
    bm: TBytes;
    xSotien, xCkmh: Double;
    mSotien, mSoluong, mCkmh, mTlckhd, mTlckvipdm, mThue: Double;
begin
    if mTrigger then
        Exit;

    mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mThue := 0;
    mTlckhd := QrBH.FieldByName('TL_CKHD').AsFloat;
    mTlckvipdm := QrBH.FieldByName('TL_CKVIPDM').AsFloat;

    with QrCTBH do
    begin
        DisableControls;
        bm := BookMark;

        First;
        while not Eof do
        begin
            if FieldByName('TL_CKHD').AsFloat <> mTlckhd then
            begin
                SetEditState(QrCTBH);
                FieldByName('TL_CKHD').AsFloat := mTlckhd;
            end;

            // CK VIP theo danh muc VIP
        	if FieldByName('TL_CK_VIPDM').AsFloat <> mTlckvipdm then
            begin
                SetEditState(QrCTBH);
            	FieldByName('TL_CK_VIPDM').AsFloat := mTlckvipdm;
            end;

            // Xoa chiet khau khi xoa VIP  - theo ck nhom hang
            if (QrBH.FieldByName('MAVIP').AsString = '') and (FieldByName('TL_CK_VIPNHOM').AsFloat > 0) then
            begin
                SetEditState(QrCTBH);
                FieldByName('TL_CK_VIPNHOM').Clear;
            end;



            xSotien := FieldByName('SOTIEN').AsFloat;
            xCkmh := FieldByName('CHIETKHAU_MH').AsFloat;
            // Thanh tien chua chiet khau

            mSotien := mSotien + xSotien;
            mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
            mCkmh := mCkmh + xCkmh;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

    with QrBH do
    begin
        if State in [dsBrowse] then
            Edit;

        FieldByName('SOTIEN').AsFloat := exVNDRound(mSotien, ctCurRound);
        FieldByName('SOLUONG').AsFloat := exVNDRound(mSoluong, ctQtyRound);
        FieldByName('CHIETKHAU_MH').AsFloat := exVNDRound(mCkmh, ctCurRound);
        FieldByName('THANHTOAN').AsFloat := exVNDRound(mSotien - mCkmh, ctCurRound);
        Post;
        CheckBrowseMode;
    end;

    GrListUpdateFooter(GrList);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.OpenQueries;
begin
    CloseDataSets([QrCTBH, QrBH]);
    OpenDataSets([QrBH, QrCTBH]);

    with QrBH do
    begin
        if IsEmpty then
            Append
        else
        begin
            with QrCTBH do
            begin
                curStt := RecordCount;
                Last;
            end;
            Edit;
        end;
    end;

    mDefSoluong := 1;
    GetCustomerName;
    GetOrderInfo;
    EdCounter.Text := posQuay;
    EdDate.Date := Now;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.posPaid;
begin
    with POS_PAID do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrBH.FieldByName('KHOA'));
        Parameters[2].Value := posMaQuay;
        Parameters[3].Value := Date;
        try
            Execute;
        except
            if DataMain.Conn.Errors.Count > 0 then
                ErrMsg(DataMain.Conn.Errors[0].Description + #13 +
                  RS_ADMIN_CONTACT);
            Exit;
        end;
    end;
end;

(* ==============================================================================
  ** Last record
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GoLast;
begin
    with QrCTBH do
    begin
        Last;
        curSoluong := FieldByName('SOLUONG').AsFloat;
    end;
end;

(* ==============================================================================
  @MAVT		nvarchar(15),

  @GIA		float out,	-- Gia ban
  @TL_CK		float out,	-- Chiet khau
  @VAT		float out
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GetCustomerName;
var
    mavip: String;
begin
    mavip := QrBH.FieldByName('MAVIP').AsString;
    if mavip = '' then
        EdCus.Text := ''
    else
        EdCus.Text := mavip + ' - '+ QrBH.FieldByName('MAVIP_HOTEN').AsString;
end;

(*
  ** Db events
*)
(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterPost(DataSet: TDataSet);
begin
    with QrBH do
        lastSct := FieldByName('SCT').AsString;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterInsert(DataSet: TDataSet);
begin
    curStt := 0;
    with QrBH do
    begin
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        FieldByName('_id').Value := NewIntID;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
        FieldByName('CREATE_DATE').AsDateTime := Now;
        FieldByName('CREATE_BY').AsInteger := sysLogonUID;

        FieldByName('LCT').AsString := 'FBLE';
        FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('DELIVERY').AsBoolean := False;
        FieldByName('MABAN').AsString := mMABAN;
        FieldByName('SOLUONG_KHACH').AsInteger := FrmCashierMain.QrBH.FieldByName('SOLUONG_KHACH').AsInteger;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmMain.QrBHBeforePost(DataSet: TDataSet);
var
    mDate: TDateTime;
begin
    mDate := Now;
    if (Trunc(mDate) <> Trunc(mLastDate)) or (mLastDate > mDate) then
    begin
        ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
        Abort;
    end;

    with DataSet do
    begin
        if FieldByName('NGAY').IsNull then //
            FieldByName('NGAY').AsDateTime := mDate;
        FieldByName('CREATE_DATE').AsDateTime := mDate;
        FieldByName('TIENTHOI').AsFloat := exVNDRound(FieldByName('CALC_TIENTHOI').AsFloat, ctCurRound);

        mLastDate := mDate;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrDBError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    Action := DbeMsg;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    // For TEST Hoang phuc Mail: 2012-08-02
    with DataMain.Conn.Errors[0] do
        if (NativeError = 32) and
          (Pos('CANNOT BE LOCATED', UpperCase(Description)) < 1) then
        begin
            ErrMsg(Description);
            Action := daAbort;
        end
        else
            Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHSOTIEN_SAU_CKMHChange(Sender: TField);
begin
    with QrBH do
    begin
        FieldByName('THANHTIEN').AsFloat := exVNDRound(FieldByName('SOTIEN_SAU_CKMH').AsFloat + FieldByName('PHUTHU').AsFloat, ctCurRound);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN3_SCTChange(Sender: TField);
begin
    with QrBH do
    if Sender.AsString = '' then
    begin
        FieldByName('TTOAN_TRAHANG').AsFloat := 0;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN_CASHChange(Sender: TField);
begin
    with QrBH do
    begin

        if not mTrigger then
        begin
            if mNhacnhoTT and (Sender.AsFloat <> 0) then
                if YesNo(RS_NHACNHO_THANHTOAN) then
                begin
                    mTrigger := True;
                    if Sender.FieldName = 'TTOAN_CASH' then
                        FieldByName('TTOAN_CASH').AsFloat := exVNDRound(Sender.AsFloat * mNhanTT, ctCurRound)
                    else
                        FieldByName('TTOAN_CARD').AsFloat := exVNDRound(Sender.AsFloat * mNhanTT, ctCurRound);
                    mTrigger := False;
                end;
        end;

        FieldByName('CHUATHOI').AsFloat := exVNDRound(FieldByName('TTOAN_CASH').AsFloat +
          FieldByName('TTOAN_CARD').AsFloat + FieldByName('TTOAN_TRAHANG').AsFloat +
          FieldByName('TTOAN_VOUCHER').AsFloat + FieldByName('TTOAN_EWALLET').AsFloat +
          FieldByName('TTOAN_BANK').AsFloat +FieldByName('TTOAN_MEMBER').AsFloat, ctCurRound);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterDelete(DataSet: TDataSet);
begin
    Total;
    if QrCTBH.IsEmpty then
    begin
        with QrBH do
        begin
            SetEditState(QrBH);
            FieldByName('CN_LIENHE').Clear;
            FieldByName('CN_DIACHI').Clear;
            FieldByName('CN_DTHOAI').Clear;
            FieldByName('TINHTRANG').Clear;
            FieldByName('DELIVERY').Clear;
            FieldByName('CO_HOADON').Clear;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterInsert(DataSet: TDataSet);
begin
    with QrCTBH do
    begin
        Inc(curStt);
        FieldByName('STT').AsInteger := curStt;
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
    end;

    // lay ngay gio de tinh chiet khau, khuyen mai.
    if Is1Record(QrCTBH) and (not sysIsCentral) then
    begin
        SetEditState(QrBH);
        QrBH.FieldByName('NGAY').AsDateTime := Now;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := TGuidEx.ToString(curKhoa);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHCalcFields(DataSet: TDataSet);
var
   fchuathoi, fthoi, fthuctra: Double;
begin
    with DataSet do
    begin

        if FieldByName('CO_HOADON').AsBoolean then
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat
        else
            FieldByName('CALC_TIEN_THUE').AsFloat := 0;

        FieldByName('CALC_TONGCK').AsFloat :=
          FieldByName('CHIETKHAU_MH').AsFloat;

        fchuathoi := FieldByName('CHUATHOI').AsFloat;
        fthuctra  := FieldByName('THUCTRA').AsFloat;

        if FieldByName('CHUATHOI').IsNull or (mDefaultZero > 0)
            or ((fchuathoi > fthuctra) and (FieldByName('TIENTHOI').AsFloat = 0)) then
            fthoi := 0
        else
        begin
            fthoi := fchuathoi -  fthuctra;
        end;

        FieldByName('CALC_CHUA_THOI').AsFloat := fchuathoi;
        FieldByName('CALC_TIENTHOI').AsFloat := fthoi;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHCN_DIACHI_HDChange(Sender: TField);
var
    s: String;
begin
    with QrBH do
    begin
        s := Sender.AsString;
        if s <> '' then
            if YesNo('Mặc định địa chỉ giao hàng?') then
                FieldByName('CN_DIACHI').AsString := s
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterPost(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    Total;

    // Reread DMVT
    with QrCTBH do
    begin
        if FieldByName('LK_TENVT').AsString = '' then
        begin
            QrDMVT.Requery;
            FieldByName('LK_TENVT').RefreshLookupList;
            FieldByName('LK_TENVT_KHONGDAU').RefreshLookupList;
        end;

        icdComPort.IcdScan(FieldByName('LK_TENVT_KHONGDAU').AsString,
          FormatFloat(ctCurFmt, FieldByName('SOTIEN').AsFloat),
          FormatFloat(ctCurFmt, QrBH.FieldByName('SOTIEN').AsFloat) + ' VND');
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
    with QrCTBH do
    begin
        Parameters[0].Value := TGuidEx.ToString(curKhoa);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    with QrBH do
        if State in [dsInsert] then
        begin
            Post;
            Edit;
        end;
end;

(* ==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
        if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(* ==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHMAVTChange(Sender: TField);
begin
    with QrCTBH do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;
        FieldByName('TENTAT').AsString := FieldByName('LK_TENTAT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CKChange(Sender: TField);
var
    sl, ck, dg, st: Double;
begin
    with QrCTBH do
    begin
        dg := FieldByName('DONGIA').AsFloat;
        sl := FieldByName('SOLUONG').AsFloat;

        st := exVNDRound(sl * dg);
        ck := exVNDRound(st * FieldByName('TL_CK').AsFloat / 100.0);

        FieldByName('SOTIEN').AsFloat := exVNDRound(st, ctCurRound);
        FieldByName('CHIETKHAU_MH').AsFloat := exVNDRound(ck, ctCurRound);
        FieldByName('SOTIEN_SAU_CKMH').AsFloat := exVNDRound(st - ck, ctCurRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CKMHChange(Sender: TField);
begin
    with QrCTBH do
    begin
		FieldByName('TL_CK_MAX').AsFloat := exVNDRound(max(
        max(FieldByName('TL_CKMH').AsFloat,
        max(FieldByName('TL_CKHD').AsFloat, FieldByName('TL_CK_VIPNHOM').AsFloat)),
        max(FieldByName('TL_CK_VIPDM').AsFloat, FieldByName('TL_CK_PHIEU').AsFloat))
        , sysPerRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTL_CK_MAXChange(Sender: TField);
begin
    with QrCTBH do
    begin
        FieldByName('TL_CK').AsFloat := exVNDRound(FieldByName('TL_CK_MAX').AsFloat +
          FieldByName('TL_CK_THEM').AsFloat, sysPerRound);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.QrTAMBeforeOpen(DataSet: TDataSet);
begin
    with QrTAM do
    begin
        Parameters[0].Value := posMaQuay;
        Parameters[1].Value := mMABAN;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrTTAfterInsert(DataSet: TDataSet);
begin
    with QrTT do
    begin
        Inc(payStt);
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        FieldByName('_id').Value := NewIntID;
        FieldByName('CREATE_DATE').AsDateTime := Now;
        FieldByName('CREATE_BY').AsInteger := sysLogonUID;
        FieldByName('THUNGAN').AsInteger := sysLogonUID;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('PTTT').AsString := '01';
        FieldByName('STT').AsInteger := payStt;
        FieldByName('TypeInput').AsString := '';
        FieldByName('TypeCode').AsString := '';
        FieldByName('TypeBalance').AsFloat := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrTTBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := TGuidEx.ToString(curKhoa);
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.InvalidAction;
begin
    Beep;
    if msg = '' then
        ErrMsg(RS_INVALID_ACTION, 'Lỗi nhập liệu', 1)
    else
        ErrMsg(msg, 'Lỗi nhập liệu', 1);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
function TFrmMain.IsEnableFunc(const pFunc: String): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text :=
          format('select FUNC_CODE from SYS_FUNC where isnull(ENABLED, 0)=1 and FUNC_CODE = %s',
          [Quotedstr(pFunc)]);
        Open;
        Result := not IsEmpty;
        Free;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanVIP;
var
    ma, ten: String;
    ck: Double;
    bm: TBytes;
begin
    with QrBH do
        ma := FieldByName('MAVIP').AsString;

    Application.CreateForm(TFrmPosVIP, FrmPosVIP);
    ten := FrmPosVIP.Execute(ma, 0);

    // ma := Trim(EdCode.Text);
    //
    // Xóa mã VIP
    if ma = '' then
    begin
        SetEditState(QrBH);
        with QrBH do
        begin
            FieldByName('MAVIP').Clear;
            FieldByName('TL_CKVIPDM').Clear;
            EdCus.Text := '';
            Total;
        end;
        Exit;
    end;

    if ten = '' then
        Exit;

    // Lấy thông tin VIP
    with POS_VIP do
    begin
        Prepared := True;
        Parameters[1].Value := ma;
        Execute;

        if Parameters[0].Value = 0 then
        begin
            mVipTen := Trim(Parameters[2].Value);
            ck := Parameters[3].Value;
        end
        else
        begin
            ErrMsg(RS_INVALID_VIP);
            Exit;
        end;
    end;
    // EdCus.Text := ten +' - '+ ma;
    EdCus.Text := ma + ' - ' + mVipTen;
    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('MAVIP').AsString := ma;
        FieldByName('TL_CKVIPDM').AsFloat := exVNDRound(ck, sysPerRound);
    end;

    with QrCTBH do
    begin
        DisableControls;
        bm := BookMark;
        First;
        while not Eof do
        begin
            getPosVip(QrBH.FieldByName('MAKHO').AsString, ma, FieldByName('MAVT').AsString, '', QrBH.FieldByName('NGAY').AsDateTime);
            SetEditState(QrCTBH);
            FieldByName('TL_CK_VIPNHOM').AsFloat := exVNDRound(curTlck3, sysPerRound);
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;
    Total;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
function TFrmMain.exThelenh: Boolean;
begin
    Application.CreateForm(TFrmTheLenh, FrmTheLenh);
    Result := FrmTheLenh.Execute(0, posMakho);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
  FooterCellRect: TRect; Field: TField; FooterText: String;
  var DefaultDrawing: Boolean);
begin
    with Canvas.Font do
    begin
        Size := 10;
        Style := [fsBold];

        if (Field.FullName = 'MAVT') or (Field.FullName = 'TENVT') then
            Color := $00804000
        else
            Color := $002222B2;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListUpdateFooter(Sender: TObject);
begin
    with GrList, QrBH do
    begin
        ColumnByName('SOLUONG').FooterValue :=
          FormatFloat(sysQtyFmt, FieldByName('SOLUONG').AsFloat);
        ColumnByName('SOTIEN').FooterValue :=
          FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat);;
        ColumnByName('SOTIEN_SAU_CKMH').FooterValue :=
          FormatFloat(sysCurFmt, FieldByName('SOTIEN').AsFloat - FieldByName('CHIETKHAU_MH').AsFloat);
    end;

end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
    begin
        Key := #0;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemAddNew(const ma: String; const pMabo: String);
begin
    with QrCTBH do
    begin
        Append;
        FieldByName('MAVT').AsString := ma;
        if pMabo <> '' then
        begin
            FieldByName('MABO').AsString := pMabo;
            FieldByName('SOLUONG').AsFloat := exVNDRound(mDefSoluong * curSlBo, ctQtyRound);
        end
        else
            FieldByName('SOLUONG').AsFloat :=  exVNDRound(mDefSoluong, ctQtyRound);

        FieldByName('DONGIA').AsFloat := exVNDRound(curGiaban, ctPriceRound);
        Post;
    end;

    mDefSoluong := 1;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemSetQty(const pSoluong: Double);
begin
    with QrCTBH do
    begin
        if State in [dsBrowse] then
            Edit;
        FieldByName('DONGIA').AsFloat := exVNDRound(curGiaban, ctPriceRound);
        FieldByName('SOLUONG').AsFloat := exVNDRound(pSoluong, ctQtyRound);
        FieldByName('TL_CK_VIPNHOM').AsFloat := exVNDRound(curTlck3, sysPerRound);
        Post;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFaqExecute(Sender: TObject);
begin
    Faqs
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetPrinterExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPrinter, FrmPrinter);
    if FrmPrinter.Execute then
    begin
        exBillFinal(FrmCashierMain.mPrinter);
        exBillInitial(FrmCashierMain.mPrinter , '', 'a.PRINTER = b.PRINTER');
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatHDExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmThongtinCN, FrmThongtinCN);
    if not FrmThongtinCN.Execute() then
        Exit;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPaymentBankCardExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('04', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPaymentBankTransferExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('02', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPaymentCashExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('01', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.CmdPaymentEWalletExecute(Sender: TObject);
begin
     Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('03', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPaymentInvoiceExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('05', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.CmdPaymentListExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('00', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPaymentVoucherExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPaymentMultiple, FrmPaymentMultiple);
    if not FrmPaymentMultiple.Execute('06', curKhoa) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPhieuquatangExecute(Sender: TObject);
begin

end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChitietExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPosChitiet, FrmPosChitiet);
    FrmPosChitiet.Execute(QrCTBH);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
    Handled := (MyActionList.Tag = 0) and Self.Active;
    // if Handled then
    // TestDbConnection; //NTD
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
    StatusBarAdjustSize(Status)
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSave2Execute(Sender: TObject);
var
    b: Boolean;
    k: TGUID;
    s: String;
begin
    if QrCTBH.IsEmpty then // Restore saved receipt
    begin
        with QrBH do // Truong hop xoa chi tiet phieu o luu tam
            if (State in [dsEdit]) and
              (FieldByName('THANHTOAN').OldValue <> FieldByName('THANHTOAN')
              .Value) then
                CheckBrowseMode;
    end
    (*
      ** Temporarily save receipt
    *)
    else
    begin
        // Temporarily save
        s := QrBH.FieldByName('DGIAI').AsString;
        if s = '' then
        begin
            Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
            if not FrmReceiptDesc.Execute(s) then
                Exit;
        end;

        SaveBill(2, s);

        // New receipt
        curKhoa := TGuidEx.EmptyGuid;
    end;

    OpenQueries;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGiaohangExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmThongtinCN, FrmThongtinCN);
    if not FrmThongtinCN.Execute(False) then
        Exit;
//    Application.CreateForm(TFrmBanleCN, FrmBanleCN);
//    FrmBanleCN.Execute(R_FULL, False);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
    s: String;
begin
    if mDefSoluong > 1 then
        s := FloatToStr(mDefSoluong)
    else
        s := '';
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
function TFrmMain.Execute(pKhoa: TGUID; pMABAN, pTENBAN: string): Boolean;
begin
    mMABAN := pMABAN;
    curKhoa := pKhoa;
    mTENBAN := pTENBAN;
    Result := ShowModal = mrOk;
    Free;
end;
(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmMain.GetOrderInfo;
var
    orderNo, orderBy : String;
begin
    orderNo := QrBH.FieldByName('QUAY_ORDER').AsString;
    orderBy := QrBH.FieldByName('ORDER_BY').AsString;
    if orderBy = '' then
        EdOrder.Text := orderNo + ''
    else
        with DataMain.QrTEMP do
        begin
            SQL.Text := 'select FULLNAME from SYS_USER where UID=''' +
              orderBy + '''';
            Open;
            EdOrder.Text := orderNo + ' - ' + FieldByName('FULLNAME').AsString;
            Close;
        end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
function TFrmMain.getPosVip(mMAKHO, mMAVIP, mMAVT, mMABO: String; mNGAY: TDateTime): Boolean;
begin
    curTlck3 := 0;
	with FrmCashierMain.POS_GIABAN do
    begin
    	Prepared := True;
        Parameters[1].Value := mMAKHO;
        Parameters[2].Value := mNGAY;
        Parameters[3].Value := mMAVT;
        Parameters[4].Value := mMAVIP;
        Parameters[10].Value := mMABO;
        Execute;
        Result := Parameters[0].Value = 0;
        if Result then
        begin
            curTlck3 := Parameters[7].Value;
    	end;
    end;
end;


end.
