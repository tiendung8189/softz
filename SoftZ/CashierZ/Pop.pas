unit Pop;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RzLabel, ExtCtrls, RzPanel, AdvSmoothLabel;

type
  TFrmPop = class(TForm)
    RzPanel1: TRzPanel;
    LbMsg: TAdvSmoothLabel;
  private
  public
    procedure Popup(pMsg: string; pX, Py: Integer);
  end;

var
  FrmPop: TFrmPop;

implementation

{$R *.dfm}
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPop.Popup(pMsg: string; pX, Py: Integer);
begin
    LbMsg.Caption.Text := pMsg;
    Left := pX;
    Top := Py;
    Show;
end;
end.
