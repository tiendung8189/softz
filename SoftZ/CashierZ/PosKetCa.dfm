object FrmPosKetca: TFrmPosKetca
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'K'#7871't Ca'
  ClientHeight = 584
  ClientWidth = 877
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  GlassFrame.Enabled = True
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PgMain: TPageControl
    Left = 0
    Top = 0
    Width = 622
    Height = 584
    Cursor = 1
    ActivePage = TabSheet1
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    Style = tsButtons
    TabHeight = 41
    TabOrder = 0
    TabWidth = 167
    OnChange = PgMainChange
    object TabSheet1: TTabSheet
      Caption = 'K'#7871't Ca'
      object DBVertGridEh1: TDBVertGridEh
        Left = 12
        Top = 12
        Width = 599
        Height = 509
        AllowedOperations = []
        AllowedSelections = []
        RowCategories.Active = True
        RowCategories.CategoryProps = <
          item
            Name = 'CA'
            DisplayText = 'Ca'
            DefaultExpanded = True
          end
          item
            Name = 'TIEN'
            DisplayText = 'Ti'#7873'n'
            DefaultExpanded = True
          end>
        RowCategories.Font.Charset = DEFAULT_CHARSET
        RowCategories.Font.Color = clWindow
        RowCategories.Font.Height = -16
        RowCategories.Font.Name = 'Tahoma'
        RowCategories.Font.Style = [fsBold]
        RowCategories.ParentFont = False
        LabelColParams.Color = clWhite
        LabelColParams.FillStyle = cfstThemedEh
        LabelColParams.Font.Charset = DEFAULT_CHARSET
        LabelColParams.Font.Color = clDefault
        LabelColParams.Font.Height = -16
        LabelColParams.Font.Name = 'Tahoma'
        LabelColParams.Font.Style = []
        LabelColParams.HorzLines = True
        LabelColParams.ParentFont = False
        PrintService.ColorSchema = pcsFullColorEh
        DataColParams.RowHeight = 23
        DataSource = DsKETCA
        DrawGraphicData = True
        DrawMemoText = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        Options = [dgvhAlwaysShowEditor, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
        OptionsEh = [dgvhHighlightFocusEh, dgvhEnterToNextRowEh, dgvhHotTrackEh]
        ParentFont = False
        RowsDefValues.Layout = tlCenter
        RowsDefValues.RowLabel.ToolTips = True
        TabOrder = 0
        LabelColWidth = 413
        Rows = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'SCT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'S'#7889' k'#7871't ca'
            CategoryName = 'CA'
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'LK_THUNGAN'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'Thu ng'#226'n'
            CategoryName = 'CA'
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'NGAY_BATDAU'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'B'#7855't '#273#7847'u'
            CategoryName = 'CA'
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'NGAY_KETTHUC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'K'#7871't th'#250'c'
            CategoryName = 'CA'
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'SCT_BATDAU'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'S'#7889' bill '#273#7847'u'
            CategoryName = 'CA'
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'SCT_KETTHUC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'S'#7889' bill cu'#7889'i'
            CategoryName = 'CA'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOBILL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'T'#7893'ng s'#7889' bill'
            CategoryName = 'CA'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'TTOAN_CASH'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = '(1) Thu ti'#7873'n m'#7863't trong ca'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'TTOAN_OTHER'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = '(2) Thu h'#236'nh th'#7913'c kh'#225'c trong ca'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'THUTIEN_THUCTRA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'T'#7893'ng kh'#225'ch '#273#432'a = (1) + (2)'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'THUCTRA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'T'#7893'ng doanh s'#7889
            CategoryName = 'CA'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOTIEN_NHAN'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = '(3) Ti'#7873'n d'#7921' ph'#242'ng'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOTIEN_NOP'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = '(4) Ti'#7873'n th'#7921'c t'#7871' trong POS'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'CALC_NHANNOP'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = '(5) Ti'#7873'n th'#7921'c t'#7871' b'#225'n trong ca = (4) - (3)'
            CategoryName = 'TIEN'
          end
          item
            Alignment = taRightJustify
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOTIEN_CL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8404992
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            RowLabel.Caption = 'Ch'#234'nh l'#7879'ch ti'#7873'n m'#7863't = (5) - (1)'
            CategoryName = 'TIEN'
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'B'#7843'ng k'#234' ti'#7873'n d'#7921' ph'#242'ng'
      ImageIndex = 1
      object GrNHAN: TDBGridEh
        Left = 12
        Top = 12
        Width = 599
        Height = 509
        AllowedOperations = [alopUpdateEh]
        DataSource = DsMENHGIA
        DynProps = <>
        EditActions = [geaDeleteEh]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        GridLineParams.DataHorzLines = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        ParentFont = False
        RowHeight = 29
        SumList.Active = True
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -20
        TitleParams.Font.Name = 'Tahoma'
        TitleParams.Font.Style = []
        TitleParams.ParentFont = False
        TitleParams.RowHeight = 26
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'MENHGIA'
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'M'#7879'nh gi'#225
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOLUONG_NHAN'
            Footers = <>
            Layout = tlCenter
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' t'#7901
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOTIEN_NHAN'
            Footer.ValueType = fvtSum
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' ti'#7873'n'
            Width = 143
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'B.k'#234' ti'#7873'n th'#7921'c t'#7871' trong POS'
      ImageIndex = 2
      object GrNOP: TDBGridEh
        Left = 12
        Top = 12
        Width = 605
        Height = 509
        AllowedOperations = [alopUpdateEh]
        DataSource = DsMENHGIA
        DynProps = <>
        EditActions = [geaDeleteEh]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        ParentFont = False
        RowHeight = 29
        SumList.Active = True
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -20
        TitleParams.Font.Name = 'Tahoma'
        TitleParams.Font.Style = []
        TitleParams.ParentFont = False
        TitleParams.RowHeight = 26
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'MENHGIA'
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'M'#7879'nh gi'#225
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOLUONG_NOP'
            Footers = <>
            Layout = tlCenter
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' t'#7901
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SOTIEN_NOP'
            Footer.ValueType = fvtSum
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' ti'#7873'n'
            Width = 143
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object BtnIn: TRzBitBtn
    Left = 671
    Top = 350
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdIn
    Caption = 'In'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    ImageIndex = 4
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object BtnXn: TRzBitBtn
    Left = 671
    Top = 462
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdXacNhan
    Caption = 'X'#225'c Nh'#7853'n'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 3
    TabStop = False
    ImageIndex = 9
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object RzBitBtn3: TRzBitBtn
    Left = 671
    Top = 518
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdClose
    Caption = 'Tho'#225't'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 4
    TabStop = False
    ImageIndex = 5
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
    Left = 642
    Top = 59
    Width = 213
    Height = 208
    AllowAutoZoom = False
    AutoCompletion.Font.Charset = DEFAULT_CHARSET
    AutoCompletion.Font.Color = clWhite
    AutoCompletion.Font.Height = -19
    AutoCompletion.Font.Name = 'Tahoma'
    AutoCompletion.Font.Style = []
    AutoCompletion.Color = clBlack
    Fill.Color = clNone
    Fill.ColorTo = clNone
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowColor = clNone
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    HighlightCaps = clWhite
    HighlightAltGr = clWhite
    KeyboardType = ktNUMERIC
    Keys = <
      item
        Caption = 'Back Space'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skBackSpace
        Color = 10526880
        X = 1
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '/'
        KeyValue = 111
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDivide
        X = 53
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '*'
        KeyValue = 106
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skMultiply
        X = 105
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '-'
        KeyValue = 109
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skSubstract
        X = 157
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '7'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '8'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '9'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '+'
        KeyValue = 107
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skAdd
        X = 157
        Y = 42
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '4'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '5'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '6'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '1'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '2'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '3'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = 'Enter'
        KeyValue = 13
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skReturn
        Color = 10526880
        X = 157
        Y = 122
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '0'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 162
        Width = 104
        SubKeys = <>
      end
      item
        Caption = '.'
        KeyValue = 110
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDecimal
        X = 105
        Y = 162
        Width = 52
        SubKeys = <>
      end>
    SmallFont.Charset = DEFAULT_CHARSET
    SmallFont.Color = clWindowText
    SmallFont.Height = -16
    SmallFont.Name = 'Tahoma'
    SmallFont.Style = []
    Version = '1.8.5.2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object BtnTKC: TRzBitBtn
    Left = 671
    Top = 406
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdTongKetCa
    Caption = 'T'#7893'ng k'#7871't ca'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    ImageIndex = 37
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 242
    Top = 174
    object CmdNhan: TAction
      Caption = 'B'#7843'ng k'#234' Nh'#7853'n ti'#7873'n'
    end
    object CmdNop: TAction
      Caption = 'B'#7843'ng k'#234' N'#7897'p ti'#7873'n'
    end
    object CmdIn: TAction
      Caption = 'In'
      OnExecute = CmdInExecute
    end
    object CmdXacNhan: TAction
      Caption = 'X'#225'c Nh'#7853'n'
      OnExecute = CmdXacNhanExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      OnExecute = CmdCloseExecute
    end
    object CmdTongKetCa: TAction
      Caption = 'T'#7893'ng k'#7871't ca'
      OnExecute = CmdTongKetCaExecute
    end
  end
  object QrKETCA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKETCABeforeOpen
    AfterInsert = QrKETCAAfterInsert
    BeforePost = QrKETCABeforePost
    OnCalcFields = QrKETCACalcFields
    Parameters = <
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'SCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      '  select *  from FB_KETCA where LOC=:LOC and SCT=:SCT')
    Left = 308
    Top = 215
    object QrKETCACALC_NHANNOP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_NHANNOP'
      Calculated = True
    end
    object QrKETCASCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrKETCAMALOC: TWideStringField
      FieldName = 'MALOC'
      Size = 5
    end
    object QrKETCAMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 5
    end
    object QrKETCAMAQUAY: TWideStringField
      FieldName = 'MAQUAY'
    end
    object QrKETCAQUAY: TWideStringField
      FieldName = 'QUAY'
    end
    object QrKETCANGAY_BATDAU: TDateTimeField
      FieldName = 'NGAY_BATDAU'
    end
    object QrKETCATHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrKETCANGAY_KETTHUC: TDateTimeField
      FieldName = 'NGAY_KETTHUC'
    end
    object QrKETCASCT_BATDAU: TWideStringField
      FieldName = 'SCT_BATDAU'
    end
    object QrKETCASCT_KETTHUC: TWideStringField
      FieldName = 'SCT_KETTHUC'
    end
    object QrKETCASOBILL: TIntegerField
      FieldName = 'SOBILL'
    end
    object QrKETCASOBILL_CHUA_HOANTAT: TIntegerField
      FieldName = 'SOBILL_CHUA_HOANTAT'
    end
    object QrKETCATHUCTRA: TFloatField
      FieldName = 'THUCTRA'
    end
    object QrKETCATHUNGAN_TRUOC_THU: TFloatField
      FieldName = 'THUNGAN_TRUOC_THU'
    end
    object QrKETCATHUTIEN_THUCTRA: TFloatField
      FieldName = 'THUTIEN_THUCTRA'
    end
    object QrKETCATHUTIEN_CHUA_HOANTAT: TFloatField
      FieldName = 'THUTIEN_CHUA_HOANTAT'
    end
    object QrKETCADOANHSO: TFloatField
      FieldName = 'DOANHSO'
    end
    object QrKETCASOTIEN_NHAN: TFloatField
      FieldName = 'SOTIEN_NHAN'
    end
    object QrKETCASOTIEN_NOP: TFloatField
      FieldName = 'SOTIEN_NOP'
    end
    object QrKETCASOTIEN_CL: TFloatField
      FieldName = 'SOTIEN_CL'
    end
    object QrKETCALOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKETCAKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKETCACAL_TIEN_KHACHDUA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CAL_TIEN_KHACHDUA'
      Calculated = True
    end
    object QrKETCALK_THUNGAN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_THUNGAN'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'THUNGAN'
      Lookup = True
    end
    object QrKETCACREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKETCAUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKETCACREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKETCAUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKETCATTOAN_CASH: TFloatField
      FieldName = 'TTOAN_CASH'
    end
    object QrKETCATTOAN_OTHER: TFloatField
      FieldName = 'TTOAN_OTHER'
    end
  end
  object DsKETCA: TDataSource
    DataSet = QrKETCA
    Left = 306
    Top = 264
  end
  object QrMENHGIA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrMENHGIABeforeOpen
    AfterInsert = QrMENHGIAAfterInsert
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '    from  FB_KETCA_MENHGIA'
      'where KHOA =:KHOA')
    Left = 326
    Top = 92
    object QrMENHGIAMENHGIA: TIntegerField
      FieldName = 'MENHGIA'
    end
    object QrMENHGIAKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrMENHGIAKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrMENHGIASOLUONG_NHAN: TIntegerField
      FieldName = 'SOLUONG_NHAN'
      OnChange = QrMENHGIASOLUONG_NHANChange
    end
    object QrMENHGIASOTIEN_NHAN: TFloatField
      FieldName = 'SOTIEN_NHAN'
    end
    object QrMENHGIASOLUONG_NOP: TIntegerField
      FieldName = 'SOLUONG_NOP'
      OnChange = QrMENHGIASOLUONG_NHANChange
    end
    object QrMENHGIASOTIEN_NOP: TFloatField
      FieldName = 'SOTIEN_NOP'
    end
    object QrMENHGIAGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrMENHGIALOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrMENHGIASTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'STT'
      Calculated = True
    end
  end
  object DsMENHGIA: TDataSource
    DataSet = QrMENHGIA
    Left = 326
    Top = 144
  end
end
