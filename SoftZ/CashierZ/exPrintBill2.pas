﻿unit exPrintBill2;

interface
uses
	Classes, ADODb;

type
    arrVariant = array of Variant;
type
  TexPrintBill = class(TObject)
    private
        FType: Integer;
    protected
        FPrinterI: Integer;
        FPrinterName: String;
        FPreview: Boolean;
    public
        property PrintType: Integer read FType write FType;
        property PrintPreview: Boolean read FPreview write FPreview;
        procedure BillInit;
        function  BillPrint(const pKhoa: TGUID; const pCopy: Integer = 1): Boolean; virtual;
        function  BillSetPrinter(const pPrinterName: String): Boolean;
    end;

  //
  TexPrintRpt = class(TexPrintBill)
    private
        FFileName: String;
    public
        constructor Create(const pFileName: String); overload;
        function  BillPrint(const pKhoa: TGUID; const pCopy: Integer = 1): Boolean; override;
    end;

  procedure exBillInitial(var pPrintBill: TexPrintBill; pFileName: String = '');
  procedure exBillFinal(pPrintBill: TexPrintBill);

implementation
uses
	Forms, SysUtils, Printers, MainData, isNameVal, IniFiles, isLib,  isStr,
    isCommon, RepEngine, GuidEx, ExCommon, isMsg;

{ TexPrintBill }
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintBill.BillInit;
var
    s: string;
begin
    try
        FPreview := GetSysParam('BILL_PREVIEW');
    except
        FPreview := False;
    end;
    FPrinterName := '';
    FPrinterI := -1;
    //Lay chon may in.
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        s := ReadString('COMMON', 'PosPrinter_' + isGetComputerName, '');
        if s = '' then
            s := ReadString('COMMON', 'PosPrinter', '');
        Free;
    end;
    if s <> '' then
        BillSetPrinter(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintBill.BillPrint(const pKhoa: TGUID; const pCopy: Integer): Boolean;
begin
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintBill.BillSetPrinter(const pPrinterName: string): Boolean;
var
    i: Integer;
begin
    i := Printer.Printers.IndexOf(pPrinterName);
    Result := i >= 0;
    if not Result then
        Exit;

    FPrinterI := i;
    FPrinterName := pPrinterName;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exBillInitial(var pPrintBill: TexPrintBill; pFileName: String);
var
    mType: Integer;
    mFile, s, mComName, mRepDef: String;
begin
    mType := -1;
    if pFileName <> '' then
        mFile := pFileName
    else
        with TADOQuery.Create(Nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text :=
                'select b.* from DM_QUAYTN a, PRINTERS b where a.PRINTER = b.PRINTER and a.TENMAY=' +
                    QuotedStr(isGetComputerName);
            try
                Open;

                if IsEmpty then
                    raise Exception.Create('Chưa cấu hình máy in.');

                mFile := Trim(FieldByName('BILL_HEADER').AsString);

                if FindField('PRINTER_TYPE') <> nil then
                    mType := FieldByName('PRINTER_TYPE').AsInteger;
            except
                on E: Exception do
                    raise Exception.Create('PRINTER message: ' + E.Message);
            end;

            Close;
            Free;
        end;

    if mType < 0 then
    begin
        s := ExtractFileExt(mFile);
        if SameText(s, '.RPT') or SameText(s, '.SZR') then
            mType := 1
        else if SameText(s, '.FR3') then
            mType := 2
        else
            mType := 0;
    end;

    case mType of
    1:
    begin
        pPrintBill := TexPrintRpt.Create(ChangeFileExt(mFile, ''));
    end;
    else
        pPrintBill := TexPrintBill.Create;
    end;
    pPrintBill.PrintType := mType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exBillFinal;
begin
    pPrintBill.Free;
end;

{ TexPrintRpt }
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintRpt.BillPrint(const pKhoa: TGUID; const pCopy: Integer): Boolean;
begin
    if FPreview then
        Result := ShowReport('Bill', FFileName, [sysLogonUID, TGuidEx.ToStringEx(pKhoa), sysLoc])
    else
        Result := PrintReport(FFileName, [sysLogonUID, TGuidEx.ToStringEx(pKhoa), sysLoc], FPrinterName, pCopy);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TexPrintRpt.Create(const pFileName: String);
begin
    BillInit;
    FFileName := pFileName;
end;

end.
