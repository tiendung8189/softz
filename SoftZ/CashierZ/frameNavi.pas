﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameNavi;

interface

uses
  Classes, Controls, Forms,
  Buttons, ActnList, Db;

type
  TfrNavi = class(TFrame)
    MyActions: TActionList;
    CmdFirst: TAction;
    CmdPrior: TAction;
    CmdNext: TAction;
    CmdLast: TAction;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure CmdFirstExecute(Sender: TObject);
    procedure CmdPriorExecute(Sender: TObject);
    procedure CmdNextExecute(Sender: TObject);
    procedure CmdLastExecute(Sender: TObject);
    procedure MyActionsUpdate(Action: TBasicAction; var Handled: Boolean);
  private
  	FDataset: TDataSet;
  published
  	property DataSet: TDataSet read FDataset write FDataset;
  end;

implementation

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNavi.MyActionsUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	if not Assigned(FDataset) then
    	Exit;

	if not FDataset.Active then
    	Exit;

    with FDataset do
    begin
    	b := State in [dsBrowse];
		CmdFirst.Enabled := b and not Bof;
        CmdPrior.Enabled := b and not Bof;
        CmdNext.Enabled  := b and not Eof;
        CmdLast.Enabled  := b and not Eof;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNavi.CmdFirstExecute(Sender: TObject);
begin
	FDataset.First;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNavi.CmdPriorExecute(Sender: TObject);
begin
	FDataset.Prior;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNavi.CmdNextExecute(Sender: TObject);
begin
	FDataset.Next;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNavi.CmdLastExecute(Sender: TObject);
begin
	FDataset.Last;
end;

end.
