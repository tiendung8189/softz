object FrmFB_Tables: TFrmFB_Tables
  Left = 270
  Top = 195
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Danh S'#225'ch B'#224'n'
  ClientHeight = 423
  ClientWidth = 1007
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1007
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 57
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdUpdate2
      ImageIndex = 9
    end
    object ToolButton2: TToolButton
      Left = 57
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 65
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object TntToolButton1: TToolButton
      Left = 122
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 179
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 187
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 402
    Width = 1007
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object CbNhom: TwwDBLookupCombo
    Left = 23
    Top = 127
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'MAKV'#9'10'#9'MAKV'#9#9'F'
      'TENKV'#9'15'#9'TENKV'#9#9'F')
    DataField = 'MAKV'
    DataSource = DsDatBan
    LookupTable = QrKhuVuc
    LookupField = 'MAKV'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
  end
  object isPanel2: TisPanel
    Left = 0
    Top = 36
    Width = 489
    Height = 366
    Align = alLeft
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 3
    HeaderCaption = ' :: Danh s'#225'ch b'#224'n'
    HeaderColor = 16119285
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object GrList: TwwDBGrid2
      Left = 2
      Top = 18
      Width = 485
      Height = 346
      TabStop = False
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'MARK;CheckBox;True;False'
        'LK_KHUVUC;CustomEdit;CbNhom;T')
      Selected.Strings = (
        'LK_KHUVUC'#9'20'#9'Khu v'#7921'c'#9'T'
        'LK_TENBAN'#9'20'#9'T'#234'n b'#224'n'#9'T'
        'LK_SOLUONG'#9'10'#9'S'#7889' kh'#225'ch'#9'T'
        'MARK'#9'5'#9'Ch'#7885'n'#9'F'
        'GHICHU'#9'20'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDatBan
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
      ParentFont = False
      PopupMenu = popCheck
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      GroupFieldName = 'LK_KHUVUC'
    end
  end
  object isPanel1: TisPanel
    Left = 489
    Top = 36
    Width = 518
    Height = 366
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 4
    HeaderCaption = ' :: L'#7883'ch '#273#7863't b'#224'n'
    HeaderColor = 16119285
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object GrDetail: TwwDBGrid2
      Left = 2
      Top = 49
      Width = 514
      Height = 315
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'NGAY'#9'2'#9'Ng'#224'y'#9'F'
        'HOTEN'#9'30'#9'HOTEN'#9'F'
        'DTHOAI'#9'31'#9'DTHOAI'#9'F'
        'NGAY_HETHAN'#9'2'#9'NGAY_HETHAN'#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDatBanCT
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgWordWrap, dgShowCellHint, dgProportionalColResize]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      ImageList = DataMain.ImageMark
      TitleImageList = DataMain.ImageSort
      FooterColor = 13360356
      FooterCellColor = 13360356
      PadColumnStyle = pcsPadHeader
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 514
      Height = 31
      ParentCustomHint = False
      Align = alTop
      BiDiMode = bdLeftToRight
      Color = 16119285
      Ctl3D = False
      DoubleBuffered = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBiDiMode = False
      ParentBackground = False
      ParentCtl3D = False
      ParentDoubleBuffered = False
      ParentFont = False
      ParentShowHint = False
      ShowCaption = False
      ShowHint = False
      TabOrder = 2
      object Label4: TLabel
        Left = 4
        Top = 8
        Width = 112
        Height = 16
        Alignment = taRightJustify
        Caption = 'Th'#7901'i gian c'#7911'a Phi'#7871'u'
      end
      object Label1: TLabel
        Left = 265
        Top = 8
        Width = 99
        Height = 16
        Alignment = taRightJustify
        Caption = 'Th'#7901'i gian hi'#7879'n t'#7841'i'
      end
      object CbNGAY: TwwDBDateTimePicker
        Left = 120
        Top = 5
        Width = 143
        Height = 22
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        Color = clBtnFace
        DataField = 'NGAY'
        DataSource = FrmFB_TableOrders.DsKM
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        ShowButton = True
        TabOrder = 0
      end
      object dateNow: TwwDBDateTimePicker
        Left = 367
        Top = 5
        Width = 143
        Height = 22
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        Color = clBtnFace
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        ShowButton = True
        TabOrder = 1
        DisplayFormat = 'dd/MM/yyyy hh:nn'
      end
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdUpdate: TAction
      Caption = 'Danh s'#225'ch kho'
      Hint = 'C'#7853'p nh'#7853't danh s'#225'ch kho'
      OnExecute = CmdUpdateExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdCheckAll: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n t'#7845't c'#7843
      OnExecute = CmdCheckAllExecute
    end
    object CmdUnCheckAll: TAction
      Category = 'POPUP'
      Caption = 'B'#7887' ch'#7885'n'
      OnExecute = CmdUnCheckAllExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdUpdate2: TAction
      Caption = 'C'#7853'p nh'#7853't'
      Visible = False
    end
  end
  object QrBan: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterPost = QrBanAfterPost
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'FB_DM_BAN'
      'order by MAKV')
    Left = 316
    Top = 220
  end
  object DsDatBan: TDataSource
    DataSet = QrDatBan
    Left = 396
    Top = 256
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 168
  end
  object Filter: TwwFilterDialog2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 168
  end
  object popCheck: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 244
    Top = 168
    object Tm1: TMenuItem
      Action = CmdCheckAll
    end
    object Bchn1: TMenuItem
      Action = CmdUnCheckAll
    end
  end
  object QrKhuVuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'MAKV, TENKV, VIP, TL_PHUTHU, GHICHU'
      '  from '#9'FB_DM_BAN_KHUVUC')
    Left = 352
    Top = 216
  end
  object QrDatBan: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrDatBanBeforeOpen
    BeforeEdit = QrDatBanBeforeEdit
    AfterScroll = QrDatBanAfterScroll
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'FB_DATBAN_MABAN'
      'where KHOA =:KHOA')
    Left = 392
    Top = 216
    object QrDatBanKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrDatBanMABAN: TWideStringField
      FieldName = 'MABAN'
      Size = 5
    end
    object QrDatBanMARK: TBooleanField
      FieldName = 'MARK'
    end
    object QrDatBanLK_TENBAN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENBAN'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'TENBAN'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrDatBanLK_MAKV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MAKV'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'MAKV'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrDatBanLK_KHUVUC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_KHUVUC'
      LookupDataSet = QrKhuVuc
      LookupKeyFields = 'MAKV'
      LookupResultField = 'TENKV'
      KeyFields = 'LK_MAKV'
      Lookup = True
    end
    object QrDatBanLK_SOLUONG: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_SOLUONG'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'SOLUONG'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrDatBanGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
  end
  object QrDatBanCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrDatBanCTBeforeOpen
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'MABAN'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'db.*, ct.MABAN'
      '  from'#9'FB_DATBAN db'
      '  inner join FB_DATBAN_MABAN ct on ct.KHOA = db.KHOA'
      ' where db.LCT =:LCT'
      'and ct.MABAN =:MABAN'
      'and db.NGAY >=:NGAY')
    Left = 434
    Top = 211
    object QrDatBanCTXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrDatBanCTIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrDatBanCTNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
    end
    object QrDatBanCTIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Calculated = True
    end
    object QrDatBanCTSCT: TWideStringField
      DisplayLabel = 'S'#7889' ch'#7913'ng t'#7915
      FieldName = 'SCT'
    end
    object QrDatBanCTDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrDatBanCTCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDatBanCTUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDatBanCTLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrDatBanCTCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDatBanCTUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDatBanCTDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrDatBanCTDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrDatBanCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrDatBanCTLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrDatBanCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrDatBanCTCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrDatBanCTCHECKED_BY: TIntegerField
      FieldName = 'CHECKED_BY'
    end
    object QrDatBanCTCHECKED_DATE: TDateTimeField
      FieldName = 'CHECKED_DATE'
    end
    object QrDatBanCTCHECKED_DESC: TWideMemoField
      FieldName = 'CHECKED_DESC'
      BlobType = ftWideMemo
    end
    object QrDatBanCTLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_KM
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrDatBanCTHOTEN: TWideStringField
      FieldName = 'HOTEN'
      Size = 200
    end
    object QrDatBanCTDTHOAI: TWideStringField
      FieldName = 'DTHOAI'
      Size = 200
    end
    object QrDatBanCTDS_MABAN: TWideStringField
      FieldName = 'DS_MABAN'
      Size = 200
    end
    object QrDatBanCTSOLUONG_KHACH: TFloatField
      FieldName = 'SOLUONG_KHACH'
    end
    object QrDatBanCTTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      Size = 5
    end
    object QrDatBanCTTINHTRANG_DATE: TDateTimeField
      FieldName = 'TINHTRANG_DATE'
    end
    object QrDatBanCTTHOIGIAN_GIUCHO: TFloatField
      FieldName = 'THOIGIAN_GIUCHO'
    end
    object QrDatBanCTNGAY_HETHAN: TDateTimeField
      FieldName = 'NGAY_HETHAN'
    end
    object QrDatBanCTSOLUONG_BAN: TFloatField
      FieldName = 'SOLUONG_BAN'
    end
    object QrDatBanCTMABAN: TWideStringField
      FieldName = 'MABAN'
      Size = 5
    end
  end
  object DsDatBanCT: TDataSource
    AutoEdit = False
    DataSet = QrDatBanCT
    Left = 442
    Top = 255
  end
end
