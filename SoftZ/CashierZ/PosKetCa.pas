﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosKetCa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, htmlbtns, ActnList, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBVertGridsEh, DB, ADODB, RzButton, ComCtrls, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, ImgList, DBGridEh, ExtCtrls, AdvTouchKeyboard,
  AdvSmoothTouchKeyBoard, Vcl.Mask, Vcl.DBCtrls, wwdblook;

type
  TFrmPosKetca = class(TForm)
    ActionList1: TActionList;
    CmdNhan: TAction;
    CmdNop: TAction;
    CmdIn: TAction;
    CmdXacNhan: TAction;
    CmdClose: TAction;
    QrKETCA: TADOQuery;
    DsKETCA: TDataSource;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBVertGridEh1: TDBVertGridEh;
    GrNHAN: TDBGridEh;
    TabSheet3: TTabSheet;
    BtnIn: TRzBitBtn;
    BtnXn: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    QrMENHGIA: TADOQuery;
    DsMENHGIA: TDataSource;
    GrNOP: TDBGridEh;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    QrKETCACALC_NHANNOP: TFloatField;
    QrKETCASCT: TWideStringField;
    QrKETCAMALOC: TWideStringField;
    QrKETCAMAKHO: TWideStringField;
    QrKETCAMAQUAY: TWideStringField;
    QrKETCAQUAY: TWideStringField;
    QrKETCATHUNGAN: TIntegerField;
    QrKETCANGAY_BATDAU: TDateTimeField;
    QrKETCANGAY_KETTHUC: TDateTimeField;
    QrKETCASCT_BATDAU: TWideStringField;
    QrKETCASCT_KETTHUC: TWideStringField;
    QrKETCASOBILL: TIntegerField;
    QrKETCASOBILL_CHUA_HOANTAT: TIntegerField;
    QrKETCATHUCTRA: TFloatField;
    QrKETCATHUNGAN_TRUOC_THU: TFloatField;
    QrKETCATHUTIEN_THUCTRA: TFloatField;
    QrKETCATHUTIEN_CHUA_HOANTAT: TFloatField;
    QrKETCADOANHSO: TFloatField;
    QrKETCASOTIEN_NHAN: TFloatField;
    QrKETCASOTIEN_NOP: TFloatField;
    QrKETCASOTIEN_CL: TFloatField;
    QrKETCALOC: TWideStringField;
    QrKETCAKHOA: TGuidField;
    QrKETCACAL_TIEN_KHACHDUA: TFloatField;
    QrKETCALK_THUNGAN: TWideStringField;
    QrMENHGIAMENHGIA: TIntegerField;
    QrMENHGIAKHOACT: TGuidField;
    QrMENHGIAKHOA: TGuidField;
    QrMENHGIASOLUONG_NHAN: TIntegerField;
    QrMENHGIASOTIEN_NHAN: TFloatField;
    QrMENHGIASOLUONG_NOP: TIntegerField;
    QrMENHGIASOTIEN_NOP: TFloatField;
    QrMENHGIAGHICHU: TWideStringField;
    QrMENHGIALOC: TWideStringField;
    QrMENHGIASTT: TIntegerField;
    QrKETCACREATE_BY: TIntegerField;
    QrKETCAUPDATE_BY: TIntegerField;
    QrKETCACREATE_DATE: TDateTimeField;
    QrKETCAUPDATE_DATE: TDateTimeField;
    CmdTongKetCa: TAction;
    BtnTKC: TRzBitBtn;
    QrKETCATTOAN_CASH: TFloatField;
    QrKETCATTOAN_OTHER: TFloatField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrKETCABeforeOpen(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure QrKETCACalcFields(DataSet: TDataSet);
    procedure CmdInExecute(Sender: TObject);
    procedure CmdXacNhanExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure QrMENHGIASOLUONG_NHANChange(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure QrKETCAAfterInsert(DataSet: TDataSet);
    procedure QrKETCABeforePost(DataSet: TDataSet);
    procedure QrMENHGIABeforeOpen(DataSet: TDataSet);
    procedure QrMENHGIAAfterInsert(DataSet: TDataSet);
    procedure CmdTongKetCaExecute(Sender: TObject);
  
  private
    { Private declarations }
    bRet, bFlag: Boolean;
    mLCT: string;
    procedure TongKetCa;
  public
    { Public declarations }
    function Execute(const xflag: Boolean = False): Boolean; // xflag: Tiền dự phòng

    function totalByFieldName(fieldName: string):  Double;
    function OpenQueries():  Boolean;
  end;

var
  FrmPosKetca: TFrmPosKetca;

implementation

uses
	MainData, ExCommon, GuidEx, PosMain, isDb, isLib, isCommon, isMsg, exResStr;



{$R *.dfm}

const
	FORM_CODE = 'FB_POS_KETCA';

(*==============================================================================
** xflag:
**	Tiền dự phòng
** Return value:
**	Kết ca or not
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.Execute(const xflag: Boolean): Boolean;
begin
	// Chỉ nhập tiền dự phòng?
    TabSheet1.TabVisible := not xflag;
    TabSheet3.TabVisible := not xflag;
    BtnIn.Visible := not xflag;
    BtnTKC.Visible := not xflag;

    bFlag := xflag;

    if bFlag then
    begin
        BtnXn.Caption := 'Nhận ca';
    	Caption := 'Nhận Ca';
	    PgMain.ActivePageIndex := 1;
    end
    else
    begin
        BtnXn.Caption := 'Kết thúc ca';
    	Caption := 'Kết Thúc Ca';
	    PgMain.ActivePageIndex := 0;
    end;

	// Go ahead
    bRet := False;
	ShowModal;
    Result := bRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	//CmdXacNhan.Enabled := PgMain.ActivePageIndex = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdCloseExecute(Sender: TObject);
begin
    QrMENHGIA.CancelBatch;
	QrKETCA.Cancel;
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdInExecute(Sender: TObject);
begin
	TongKetCa;
	ShowReport(Caption, FORM_CODE, [TGuidEx.ToStringEx(QrKETCA.FieldByName('KHOA'))]);
end;

procedure TFrmPosKetca.CmdTongKetCaExecute(Sender: TObject);
begin
    if posCa <> '' then
    begin
        SetEditState(QrKETCA);
        QrMENHGIA.CheckBrowseMode;
        QrKETCA.Post;
        exSaveDetails(QrMENHGIA);
        TongKetCa;
        QrKETCA.Requery;
        QrMENHGIA.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdXacNhanExecute(Sender: TObject);
var
	b: Boolean;
    sum: Double;
begin
    if posCa = '' then
    begin
        sum := totalByFieldName('SOTIEN_NHAN');
        if (sum = 0) and  not YesNo('Không có tiền dự phòng, vẫn thực hiện Nhận Ca. Tiếp tục?') then
            Exit;

        b := False;
    end
    else
    begin
        // Confirm
        if not YesNo('Xác nhận kết thúc ca bán hàng.') then
            Exit;

        SetEditState(QrKETCA);
        b := True;
    end;

    QrMENHGIA.CheckBrowseMode;
    QrKETCA.Post;
    exSaveDetails(QrMENHGIA);

    if b then
    begin
       DataMain.PosKetCa(2)
    end
    else
    begin
       DataMain.PosKetCa(0, 1);
    end;

    bRet := True;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrKETCA, QrMENHGIA]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.FormCreate(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.TongKetCa;
begin
	// Tổng kết ca
    if posCa <> '' then
    begin
    	DataMain.PosKetCa(1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.FormShow(Sender: TObject);
var
    r: TRect;
    mMENHGIA: Double;
begin
    mLCT := 'FB_KETCA';

	SystemParametersInfo(SPI_GETWORKAREA, 0, r, 0);
//	with Sender do
//    begin
//	    Left := (r.Right - Width) div 2;
//        Top := r.Top;
//    end;

	SetDictionary(QrKETCA, FORM_CODE);
	SetDictionary(QrMENHGIA, FORM_CODE + '_MENHGIA');

    SetDisplayFormat(QrKETCA, sysCurFmt);
    SetDisplayFormat(QrKETCA, ['NGAY_BATDAU', 'NGAY_KETTHUC'], ShortDateFormat + ' hh:nn');

    SetDisplayFormat(QrMENHGIA, sysCurFmt);

	if bFlag then
    begin
        TongKetCa;
    end;

    QrKETCA.Open;
    OpenQueries();

    PgMain.OnChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.PgMainChange(Sender: TObject);
begin
    if QrMENHGIA.Active then
        QrMENHGIA.CheckBrowseMode;

	case PgMain.ActivePageIndex of
    0:
    begin
        TongKetCa;
        QrKETCA.Requery;
    end;
    1:
       	GrNHAN.SetFocus;
    2:
       	GrNOP.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCAAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrKETCA do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('THUNGAN').AsInteger      := sysLogonUID;
		FieldByName('NGAY_BATDAU').AsDateTime := d;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('MAKHO').AsString         := posMakho;
        FieldByName('QUAY').AsString          := posQuay;
        FieldByName('MAQUAY').AsString        := posMaQuay;
    end;
    mTrigger := False;
end;

procedure TFrmPosKetca.QrKETCABeforeOpen(DataSet: TDataSet);
begin
	with QrKETCA do
    begin
        Parameters[0].Value := sysLoc;
        Parameters[1].Value := posCa;
    end;
end;

procedure TFrmPosKetca.QrKETCABeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCACalcFields(DataSet: TDataSet);
begin
	with QrKETCA do
    begin
    	FieldByName('CALC_NHANNOP').AsFloat :=
    		FieldByName('SOTIEN_NOP').AsFloat -
	    	FieldByName('SOTIEN_NHAN').AsFloat;

//        FieldByName('CAL_TIEN_KHACHDUA').AsFloat :=
//    		FieldByName('TTOAN_CASH').AsFloat +
//            FieldByName('TTOAN_CARD').AsFloat +
//            FieldByName('TTOAN_EWALLET').AsFloat +
//            FieldByName('TTOAN_TRAHANG').AsFloat +
//	    	FieldByName('TTOAN_VOUCHER').AsFloat +
//            FieldByName('TTOAN_BANK').AsFloat +
//            FieldByName('TTOAN_MEMBER').AsFloat;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIAAfterInsert(DataSet: TDataSet);
begin
    with QrMENHGIA do
    begin
        FieldByName('KHOA').Value := QrKETCA.FieldByName('KHOA').Value;
        TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        FieldByName('LOC').AsString := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIABeforeOpen(DataSet: TDataSet);
begin
    QrMENHGIA.Parameters[0].Value := QrKETCA.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIASOLUONG_NHANChange(Sender: TField);
var
	x: Integer;
begin
    with QrMENHGIA do
    begin
    	x := FieldByName('MENHGIA').AsInteger;
        FieldByName('SOTIEN_NHAN').AsFloat := x * FieldByName('SOLUONG_NHAN').AsInteger;
        FieldByName('SOTIEN_NOP').AsFloat := x * FieldByName('SOLUONG_NOP').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.totalByFieldName(fieldName: string):  Double;
var
    sum: Double;
begin
    with QrMENHGIA do
    begin
        sum := 0;
        First;
        while not Eof do
        begin
            if not FieldByName(fieldName).IsNull then
            begin
                sum := sum  + FieldByName(fieldName).AsFloat
            end;
            Next;
        end;
    end;
    Result := sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.OpenQueries():  Boolean;
var
    mMENHGIA: Double;
begin
    if posCa <> '' then
        QrMENHGIA.Open
    else
    begin
        QrKETCA.Append;
        with QrMENHGIA do
        begin
            Close;
            Open;
        end;

        with DataMain.QrTEMP do
        begin
            SQL.Text := 'select * from dbo.fnLoaiMenhgia()';
            Open;
            First;
            while not Eof do
            begin
                mMENHGIA := FieldByName('MENHGIA').AsFloat;
                with QrMENHGIA do
                begin
                    Append;
                    FieldByName('MENHGIA').AsFloat := mMENHGIA;
                end;
                Next;
            end;
            QrMENHGIA.CheckBrowseMode;
        end;
    end;

    Result := True;
end;

end.
