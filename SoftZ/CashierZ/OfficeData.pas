﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit OfficeData;

interface

uses
  SysUtils, Classes, ARWordReport, ARWordReportPerfom, ADODB, DB, Variants,
  EXLReportExcelTLB, EXLReportBand, EXLReport, AdvMenus, Menus, ActnList,
  Wwdbgrid, Forms, ComCtrls;

type
  TDataOffice = class(TDataModule)
    WordReport: TARWordReport;
    SP_EXPORT: TADOStoredProc;
    QrDataPool: TADOQuery;
    XLSReport: TEXLReport;
    USER_REP_PRINT: TADOStoredProc;
    USER_REP_PRINT2: TADOStoredProc;
    procedure WordReportNeedSQL(Sender: TObject; SQLstring: string;
      var ResultDataset: TDataSet);
    procedure WordReportTag(Sender: TObject; var TagValue: string);
  private
    function OpenData(const pStorePro: String;
        const pParams: array of Variant; pShowMsg: Boolean = True): Boolean;

    function OpenDoc(pTemplate: String): Boolean;
    function OpenXLS(pTemplate: String): Boolean;

  public
    procedure BuildMenu(const pFormCode: String; pMenu: TAdvPopupMenu; pAction: TAction); overload;
    procedure BuildMenu(pForm: TForm; const pFormCode: String; pButton: TToolButton; pAction: TAction); overload;
    procedure ToolButtonClick(Sender: TObject);

    function CreateReport(const iniSection: String;
    	const pParams: array of Variant; pCap: String = ''): Boolean; overload;
    function CreateReport(const pFormCode: String; const pRepCode: Integer;
    	const pParams: array of Variant; pCap: String = ''; pKeyValue: String = '0'): Boolean; overload;
    function CreateReport2(const pTempLate: String;
    	const pParams: array of Variant; pName: String): Boolean;

    function CreateDoc(const iniSection, hdrProc: String;
    	const pParams: array of Variant): Boolean;
    function CreateDoc2(const pTempLate, pStorePro: String;
    	const pParams: array of Variant): Boolean;

    function CreateXLS(const iniSection, hdrProc: String;
    	const pParams: array of Variant): Boolean;
    function CreateXLS2(const pTempLate, pStorePro: String;
    	const pParams: array of Variant): Boolean;

    function ExportDataGrid(pGrid: TwwDBGrid; pCaption: String = '';
        pFileName: String = ''; pShow: Boolean=True): Boolean;
  end;

var
  DataOffice: TDataOffice;

implementation

{$R *.dfm}
uses
    IniFiles, ExCommon, isCommon, isMsg, RepEngine, Rights, Windows,
    isFile, {$IFDEF VER230} system.win.ComObj, {$ELSE}ComObj, {$ENDIF}
    Character, ShellAPI;

resourcestring
    RS_FILE_NOT_FOUND = 'Lỗi không tìm thấy file.';
    RS_DATA_EMPTY = 'Không có dữ liệu.';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataOffice.BuildMenu(const pFormCode: String; pMenu: TAdvPopupMenu; pAction: TAction);
var
    mItem: TMenuItem;
begin
    with USER_REP_PRINT do
    begin
        Close;
        Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := pFormCode;
        Open;
        while not eof do
        begin
            mItem := TMenuItem.Create(pMenu);
            if not FieldByName('SYS').AsBoolean then
                mItem.OnClick := pAction.OnExecute;

            mItem.Caption := FieldByName('DESC').AsString;
            mItem.Tag := FieldByName('REP_CODE').AsInteger;

            pMenu.Items.Add(mItem);
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataOffice.BuildMenu(pForm: TForm; const pFormCode: String;
  pButton: TToolButton; pAction: TAction);
var
    mItem: TMenuItem;
begin
    with USER_REP_PRINT do
    begin
        Close;
        Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := pFormCode;
        Open;

        if not IsEmpty then
        begin
            pButton.DropdownMenu := TAdvPopupMenu.Create(pForm);
            pButton.OnClick := ToolButtonClick;
            pButton.Action := nil;

            mItem := TMenuItem.Create(pButton.DropdownMenu);
            mItem.Action := pAction;
            pButton.DropdownMenu.Items.Add(mItem);

            while not eof do
            begin
                mItem := TMenuItem.Create(pButton.DropdownMenu);
                if not FieldByName('SYS').AsBoolean then
                    mItem.OnClick := pAction.OnExecute;

                mItem.Caption := FieldByName('DESC').AsString;
                mItem.Tag := FieldByName('REP_CODE').AsInteger;

                pButton.DropdownMenu.Items.Add(mItem);
                Next;
            end;
        end;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateDoc(const iniSection, hdrProc: String;
  const pParams: array of Variant): Boolean;
var
    b: Boolean;
    ls: TStrings;
    docTemplate, mRepPath, mProc: String;
begin
    Result := False;
    // List of fields
    ls := TStringList.Create;
    mRepPath := sysRepPath;

    if not FileExists(mRepPath + 'Rps.ini') then
        mRepPath := sysAppPath + 'Rps\';

    with TIniFile.Create(mRepPath + 'Rps.ini') do
    begin
        ReadSectionValues(iniSection, ls);
    	Free;
    end;

    b := ls.Count > 1;
    if not b then
    begin
        ls.Free;
//        if sysDebug then
            ErrMsg('Lỗi cấu hình: ' + iniSection + #13 + 'File: ' + mRepPath + 'Rps.ini');
		Exit;
    end;

    // Doc template
    docTemplate := ls.Values['DOC'];
    mProc := ls.Values['ProcedureName'];
    ls.Free;
    if mProc = '' then
        mProc := hdrProc;

    docTemplate := mRepPath + docTemplate;
    if not FileExists(docTemplate) then
    begin
//        if sysDebug then
            ErrMsg(RS_FILE_NOT_FOUND + #13 + docTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    Result := OpenDoc(docTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateDoc2(const pTempLate, pStorePro: String;
  const pParams: array of Variant): Boolean;
var
    docTemplate, mProc: String;
begin
    Result := False;

    mProc := pStorePro;
    docTemplate := sysRepPath + pTemplate;

    if not FileExists(docTemplate) then
    begin
        ErrMsg(RS_FILE_NOT_FOUND + #13 + docTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    Result := OpenDoc(docTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateReport(const iniSection: String;
  const pParams: array of Variant; pCap: String): Boolean;
var
    b: Boolean;
    ls: TStrings;
    mTemplate, mRepPath, mProc, s: String;
begin
    Result := False;
    // List of fields
    ls := TStringList.Create;
    mRepPath := sysRepPath;

    if not FileExists(mRepPath + 'Rps.ini') then
        mRepPath := sysAppPath + 'Rps\';

    with TIniFile.Create(mRepPath + 'Rps.ini') do
    begin
        ReadSectionValues(iniSection, ls);
    	Free;
    end;

    b := ls.Count > 0;
    if not b then
    begin
        ls.Free;
        ShowReport(pCap, iniSection, pParams);
		Exit;
    end;

    // Doc template
    mTemplate := ls.Values['Template'];
    mProc := ls.Values['ProcedureName'];
    ls.Free;

    s := ExtractFileExt(mTemplate);
    if SameText(s, '.RPT') or SameText(s, '.SZ') then
    begin
        mTemplate := ChangeFileExt(ExtractFileName(mTemplate), '');
        Result := ShowReport(pCap, mTemplate, pParams);
        Exit;
    end;

//    if SameText(s, '.FR3') then
//    begin
//        Result := FrmFastReport.ShowReport(pCap, mTemplate, pParams);
//        Exit;
//    end;

    if mProc = '' then
        mProc := pCap;

    mTemplate := mRepPath + mTemplate;
    if not FileExists(mTemplate) then
    begin
        ErrMsg(RS_FILE_NOT_FOUND + #13 + mTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    if SameText(copy(s, 0, 4), '.DOC') then
        OpenDoc(mTemplate)
    else if SameText(copy(s, 0, 4), '.XLS') then
        OpenXLS(mTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateReport(const pFormCode: String;
  const pRepCode: Integer; const pParams: array of Variant; pCap, pKeyValue: String): Boolean;
var
    mTemplate, mRepPath, mProc, s, s2: String;
    mR: Integer;
begin
    if pRepCode = 0 then
    begin
        Result := ShowReport(pCap, pFormCode, pParams);
        Exit;
    end;

    mRepPath := sysRepPath;
    with USER_REP_PRINT2 do
    begin
        Close;
        Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := pFormCode;
        Parameters[4].Value := pRepCode;
        Parameters[5].Value := pKeyValue;
        Open;
        Result := not IsEmpty;
        if not Result then
        begin
//            if sysDebug then
                ErrMsg(Format('Print Error: FormCode: %s, ReportCode: %d', [pFormCode, pRepCode]));
            Exit;
            Close;
        end;

        mTemplate := FieldByName('TEMPLATE').AsString;
        mProc := FieldByName('REP_NAME').AsString;
        s2 := FieldByName('DESC').AsString;
        mR := FieldByName('RIGHT').AsInteger;
        Close;
    end;
    s := ExtractFileExt(mTemplate);
    if SameText(s, '.RPT') or SameText(s, '.SZ') or (s = '') then
    begin
        s := ChangeFileExt(mTemplate, '');
//        Result := ShowReport(s2, s, pParams, mR = R_READ, mR <> R_REPORT);
//        Result := ShowReport(s2, s, pParams, mR = R_READ, True);
        Result := ShowReport(s2, s, pParams);
        Exit;
    end;

//    if SameText(s, '.FR3') then
//    begin
//        s := ChangeFileExt(mTemplate, '');
//        Result := FrmFastReport.ShowReport(s2, s, pParams);
//        Exit;
//    end;

    mTemplate := mRepPath + mTemplate;
    if not FileExists(mTemplate) then
    begin
//        if sysDebug then
            ErrMsg(RS_FILE_NOT_FOUND + #13 + mTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    if SameText(copy(s, 0, 4), '.DOC') then
        OpenDoc(mTemplate)
    else if SameText(copy(s, 0, 4), '.XLS') then
        OpenXLS(mTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateReport2(const pTempLate: String;
  const pParams: array of Variant; pName: String): Boolean;
var
    s: String;
begin
    s := ExtractFileExt(pTempLate);
    if SameText(copy(s, 0, 4), '.DOC') then
        Result := CreateDoc2(pTemplate, pName, pParams)
    else if SameText(copy(s, 0, 4), '.XLS') then
        Result := CreateXLS2(pTemplate, pName, pParams)
    else
//    else if SameText(s, '.FR3') then
//        Result := FrmFastReport.ShowReport(pName, pTemplate, pParams)
//    else
        Result := ShowReport(pName, ExtractFileName(pTempLate), pParams);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateXLS(const iniSection, hdrProc: String;
  const pParams: array of Variant): Boolean;
var
    b: Boolean;
    ls: TStrings;
    mTemplate, mRepPath, mProc: String;
begin
    Result := False;
    // List of fields
    ls := TStringList.Create;
    mRepPath := sysRepPath;

    if not FileExists(mRepPath + 'Rps.ini') then
        mRepPath := sysAppPath + 'Rps\';

    with TIniFile.Create(mRepPath + 'Rps.ini') do
    begin
        ReadSectionValues(iniSection, ls);
    	Free;
    end;

    b := ls.Count > 1;
    if not b then
    begin
        ls.Free;
//        if sysDebug then
            ErrMsg('Lỗi cấu hình: ' + iniSection + #13 + 'File: ' + mRepPath + 'Rps.ini');
		Exit;
    end;

    // Doc template
    mTemplate := ls.Values['XLS'];
    mProc := ls.Values['ProcedureName'];
    ls.Free;
    if mProc = '' then
        mProc := hdrProc;

    mTemplate := mRepPath + mTemplate;
    if not FileExists(mTemplate) then
    begin
//        if sysDebug then
            ErrMsg(RS_FILE_NOT_FOUND + #13 + mTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    Result := OpenXLS(mTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.CreateXLS2(const pTempLate, pStorePro: String;
  const pParams: array of Variant): Boolean;
var
    mTemplate, mProc: String;
begin
    Result := False;

    mProc := pStorePro;
    mTemplate := sysRepPath + pTemplate;

    if not FileExists(mTemplate) then
    begin
        ErrMsg(RS_FILE_NOT_FOUND + #13 + mTemplate);
        Exit;
    end;

    if not OpenData(mProc, pParams) then
        Exit;

    Result := OpenXLS(mTemplate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.ExportDataGrid(pGrid: TwwDBGrid; pCaption, pFileName: String;
    pShow: Boolean): Boolean;
const
  C_Worksheet = -4167;
  // SheetType
  xlChart = -4109;
  xlWorksheet = -4167;
  // WBATemplate
  xlWBATWorksheet = -4167;
  xlWBATChart = -4109;
  // Page Setup
  xlPortrait = 1;
  xlLandscape = 2;
  xlPaperA4 = 9;
  // Format Cells
  xlBottom = -4107;
  xlLeft = -4131;
  xlRight = -4152;
  xlTop = -4160;
  // Text Alignment
  xlHAlignCenter = -4108;
  xlVAlignCenter = -4108;
  // Cell Borders
  xlThick = 4;
  xlThin = 2;
var
    i, m1stRow, mCurRow, mRows, mCols: Integer;
    mBookmask: TBytes;
    mExcel, mSheet, mRange, mData: OLEVariant;
    mCloseApp: Boolean;

    // Convert
    function RefToCell(ARow, ACol: Integer): string;
    begin
        if ACol < 27 then
            Result := Chr(Ord('A') + ACol - 1) + IntToStr(ARow)
        else
            Result := 'A' + Char(Ord('A') +  ACol mod 27)+ IntToStr(ARow);
    end;
begin
    Result := False;
    if pGrid.DataSource.DataSet = nil then
        Exit;

    try
        mExcel := GetActiveOleObject('Excel.Application');
        mCloseApp := False;
    except
        try
            mExcel := CreateOleObject('Excel.Application');
            mCloseApp := True;
        except
            Exit;
        end;
    end;

    if pFileName = '' then
        if mExcel.Version > 11 then
            pFileName := isGetSaveFileName('xlsx')
        else
            pFileName := isGetSaveFileName('xls');

    if pFileName = '' then
        Exit;

    mCols := pGrid.FieldCount;
    mRows := pGrid.DataSource.DataSet.RecordCount;
    mCurRow := 0;
    m1stRow := 1;
    if pCaption <> '' then
        m1stRow := 4;

    //Create a variant array the size of your data
    mData := VarArrayCreate([0, mRows, 1, mCols], varVariant);

    //write the titles
    for i := 0 to mCols - 1 do
        mData[mCurRow, i+1] := pGrid.Fields[i].DisplayLabel;
    Inc(mCurRow);

    with pGrid.DataSource.DataSet do
    begin
        mBookmask := Bookmark;
        DisableControls;
        First;

        try
            while not Eof do
            begin
                for i := 0 to mCols - 1 do
                    if (pGrid.Fields[i].DataType in [ftWideString, ftString, ftWideMemo, ftMemo]) then
                    begin
                        if pGrid.Fields[i].AsString <> '' then
                            if TCharacter.IsNumber(pGrid.Fields[i].AsString, 1) then
                                mData[mCurRow, i+1] := '''' + pGrid.Fields[i].AsString
                            else
                                mData[mCurRow, i+1] := pGrid.Fields[i].AsString;
                    end
                    else
                        mData[mCurRow, i+1] := pGrid.Fields[i].Value;

                Inc(mCurRow);
                Next;
            end;
            Bookmark := mBookmask;
        finally
            EnableControls;
        end;
    end;

    try
        //Don't show excel
        mExcel.Visible := False;

        mExcel.Workbooks.Add(C_Worksheet);
        mSheet := mExcel.Workbooks[1].WorkSheets[1];
        mSheet.Name := 'Sheet1';
        //Fill up the sheet
        mSheet.Range[RefToCell(m1stRow, 1), RefToCell(m1stRow + mRows, mCols)].Value := mData;
        mRange := mSheet.Range[RefToCell(m1stRow, 1), RefToCell(m1stRow, mCols)];
        mRange.Font.Bold := True;
        mRange.EntireColumn.AutoFit;

        if pCaption <> '' then
        begin
            if mCols > 3 then
                i := mCols
            else
                i := 3;
            mRange := mSheet.Range[RefToCell(1, 3), RefToCell(2, i)];
            mRange.Font.Size := 16;
            mRange.Font.Bold := True;
            mRange.Mergecells := True;
            mRange.Value := pCaption;
            mRange.HorizontalAlignment := xlHAlignCenter;
            mRange.VerticalAlignment := xlVAlignCenter;

            mRange := mSheet.Range[RefToCell(1, 1), RefToCell(1, 1)];
            mRange.Font.Bold := True;
            mRange.Value := sysDesc1;
            mSheet.Range[RefToCell(2, 1), RefToCell(2, 1)].Value := sysDesc2;
        end;
        //Save Excel Worksheet
        try
            mExcel.DisplayAlerts := False; //override exists file
            mExcel.Workbooks[1].SaveAs(pFileName);
        except
            on E: Exception do
                raise Exception.Create('Data transfer error: ' + E.Message);
        end;
    finally
        if mCloseApp and not VarIsEmpty(mExcel) then
        begin
          mExcel.DisplayAlerts := False;
          mExcel.Quit;
        end;
        mExcel := Unassigned;
        mSheet := Unassigned;
        mRange := Unassigned;
    end;
    Result := True;
    if Result and pShow then
        if FileExists(pFileName) then
            ShellExecute(0, Nil, PChar(pFileName), Nil, Nil, SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.OpenData(const pStorePro: String;
  const pParams: array of Variant; pShowMsg: Boolean): Boolean;
var
    i, iParamCount: Integer;
    s: String;
begin
    with SP_EXPORT do
    begin
        Close;
         if ProcedureName = pStorePro then
            Prepared := True
        else
        begin
            ProcedureName := pStorePro;
            Parameters.Refresh;
        end;
        iParamCount := Parameters.Count - 1;
        if iParamCount > Length(pParams) then
            iParamCount := Length(pParams);

        try
        for i := 1 to iParamCount do
            Parameters[i].Value := pParams[i - 1];
        except
            on E: Exception do
            begin
                ErrMsg('Lỗi thông số truyền dữ liệu.' + #13 + e.Message);
                Result := False;
            end;
        end;

        try
            Open;
            Result := not IsEmpty;
            if not Result then
                if pShowMsg and IsEmpty then
                    isMsg.Msg(RS_DATA_EMPTY);
        except
            on E: Exception do
            begin
                if Length(pParams) > 0 then
                    s := pStorePro + #13 + 'Param: ' + VarToStrDef(pParams[0], 'null');
                for i := 1 to Length(pParams) - 1 do
                    s := s + ';' + VarToStrDef(pParams[i], 'null');

//                if sysDebug then
//                    s := s + #13#10 + E.Message;
                ErrMsg('Lỗi thực thi stored ' + s);
                Result := False;
            end;
        end;
        if not Result then
            Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.OpenDoc(pTemplate: String): Boolean;
var
    i, n: Integer;
begin
    Wait(PROCESSING);
    try
        with WordReport do
        begin
            CustTagValues.Clear;
            Filename := pTemplate;
            CustTagValues.Add('HEADER1='+sysDesc1);
            CustTagValues.Add('HEADER2='+sysDesc2);
            CustTagValues.Add('HEADER3='+sysDesc3);
            CustTagValues.Add('FULLNAME='+sysLogonFullName);

            n := SP_EXPORT.Parameters.Count - 1;
            for i := 1 to n do
                if SP_EXPORT.Parameters[i].Value = null then
                    CustTagValues.Add(SP_EXPORT.Parameters[i].Name + '=')
                else
                    CustTagValues.Add(SP_EXPORT.Parameters[i].Name + '=' + QuotedStr(String(SP_EXPORT.Parameters[i].Value)));

            try
                Preview;
            finally
                WordReport.CloseWord;
            end;
        end;
        Result := True;
    finally
        ClearWait;
        QrDataPool.Close;
        QrDataPool.SQL.Clear;
        SP_EXPORT.Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataOffice.OpenXLS(pTemplate: String): Boolean;
var
    i, n: Integer;
    mGroupHeaderBand: TExlReportGroupHeaderBand;
    mGroupFooterBand: TExlReportGroupFooterBand;
    s: String;
    mParam: TParameter;
begin
    with XLSReport do
    begin
        DataSet := SP_EXPORT;

        Bands.Clear;
        s := 'BandTitle';
        mParam := SP_EXPORT.Parameters.FindParam('@p'+s);
        if mParam <> nil then
            Bands.AddBand(xlrbtTitle).Range := mParam.Value
        else if SP_EXPORT.FindField(s) <> nil then
            Bands.AddBand(xlrbtTitle).Range := SP_EXPORT.FieldByName(s).AsString;

        s := 'BandGroup';
        mParam := SP_EXPORT.Parameters.FindParam('@p'+ s);
        if mParam <> nil then
            with TEXLReportGroupHeaderBand(Bands.AddBand(xlrbtGroupHeader)) do
            begin
                Range := mParam.Value;
                FieldName := SP_EXPORT.Parameters.FindParam('@pBandGroupFieldName').Value;
            end
        else if SP_EXPORT.FindField(s) <> nil then
            with TEXLReportGroupHeaderBand(Bands.AddBand(xlrbtGroupHeader)) do
            begin
                Range := SP_EXPORT.FieldByName(s).AsString;
                FieldName := SP_EXPORT.FieldByName('BandGroupFieldName').AsString;
            end;


        for i := 1 to 10 do
        begin
            s := 'BandGroupHeader' + IntToStr(i);
            mParam := SP_EXPORT.Parameters.FindParam('@p' + s);
            if mParam <> nil then
            begin
                mGroupHeaderBand := TEXLReportGroupHeaderBand(Bands.AddBand(xlrbtGroupHeader));
                with mGroupHeaderBand do
                begin
                    Range := mParam.Value;
                    FieldName := SP_EXPORT.Parameters.FindParam('@pBandGroupHeaderFieldName' + IntToStr(i)).Value;
                end;

                s := '@pBandGroupFooter' + IntToStr(i);
                mParam := SP_EXPORT.Parameters.FindParam(s);
                if mParam <> nil then
                begin
                    mGroupFooterBand := TEXLReportGroupFooterBand(Bands.AddBand(xlrbtGroupFooter));
                    mGroupFooterBand.Range := mParam.Value;
                    mGroupHeaderBand.Footer := mGroupFooterBand;
                end;
            end
            else if SP_EXPORT.FindField(s) <> nil then
            begin
                mGroupHeaderBand := TEXLReportGroupHeaderBand(Bands.AddBand(xlrbtGroupHeader));
                with mGroupHeaderBand do
                begin
                    Range := SP_EXPORT.FieldByName(s).AsString;
                    FieldName := SP_EXPORT.FieldByName('BandGroupHeaderFieldName' + IntToStr(i)).AsString;
                end;

                if SP_EXPORT.FindField('BandGroupFooter' + IntToStr(i)) <> nil then
                begin
                    mGroupFooterBand := TEXLReportGroupFooterBand(Bands.AddBand(xlrbtGroupFooter));
                    mGroupFooterBand.Range := SP_EXPORT.FieldByName('BandGroupFooter' + IntToStr(i)).AsString;
                    mGroupHeaderBand.Footer := mGroupFooterBand;
                end;
            end;
        end;

        s := 'BandData';
        mParam := SP_EXPORT.Parameters.FindParam('@p' + s);
        if mParam <> nil then
            Bands.AddBand(xlrbtMasterData).Range := mParam.Value
        else if SP_EXPORT.FindField(s) <> nil then
            Bands.AddBand(xlrbtMasterData).Range := SP_EXPORT.FieldByName(s).AsString;

        s := 'BandDetailHeader';
        mParam := SP_EXPORT.Parameters.FindParam('@p' + s);
        if mParam <> nil then
            Bands.AddBand(xlrbtDetailHeader).Range := mParam.Value
        else if SP_EXPORT.FindField(s) <> nil then
            Bands.AddBand(xlrbtDetailHeader).Range := SP_EXPORT.FieldByName(s).AsString;

        s := 'BandDetailData';
        mParam := SP_EXPORT.Parameters.FindParam('@p' + s);
        if mParam <> nil then
            Bands.AddBand(xlrbtDetailData).Range := mParam.Value
        else if SP_EXPORT.FindField(s) <> nil then
            Bands.AddBand(xlrbtDetailData).Range := SP_EXPORT.FieldByName(s).AsString;

        s := 'BandDetailFooter';
        mParam := SP_EXPORT.Parameters.FindParam('@p' + s);
        if mParam <> nil then
            Bands.AddBand(xlrbtDetailFooter).Range := mParam.Value
        else if SP_EXPORT.FindField(s) <> nil then
            Bands.AddBand(xlrbtDetailFooter).Range := SP_EXPORT.FieldByName(s).AsString;

        mParam := SP_EXPORT.Parameters.FindParam('@pBandSummary');
        if mParam <> nil then
            Bands.AddBand(xlrbtSummary).Range := mParam.Value
        else if SP_EXPORT.FindField('BandSummary') <> nil then
            Bands.AddBand(xlrbtSummary).Range := SP_EXPORT.FieldByName('BandSummary').AsString;

        Dictionary.Clear;
        with Dictionary.Add do
        begin
            FieldName := 'HEADER1';
            ValueAsString := sysDesc1;
        end;
        with Dictionary.Add do
        begin
            FieldName := 'HEADER2';
            ValueAsString := sysDesc2;
        end;
        with Dictionary.Add do
        begin
            FieldName := 'HEADER3';
            ValueAsString := sysDesc3;
        end;
        with Dictionary.Add do
        begin
            FieldName := 'FULLNAME';
            ValueAsString := sysLogonFullName;
        end;

        n := SP_EXPORT.Parameters.Count - 1;
        for i := 1 to n do
        begin
            if pos('@pBand', String(SP_EXPORT.Parameters[i].Name)) = 0 then
            with Dictionary.Add do
            begin
                FieldName := Copy(SP_EXPORT.Parameters[i].Name, 2, 200);
                if SP_EXPORT.Parameters[i].Value = null then
                    ValueAsString := ''
                else
                    ValueAsString := String(SP_EXPORT.Parameters[i].Value);
            end;
        end;

        Template := pTemplate;
        try
            Show;
            Result := True;
        except
            on E: Exception do
            begin
                ErrMsg(E.Message);
                Result := False;
            end;
        end;
    end;

    SP_EXPORT.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataOffice.ToolButtonClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataOffice.WordReportNeedSQL(Sender: TObject; SQLstring: string;
  var ResultDataset: TDataSet);
begin
    try
        QrDataPool.Close;
        QrDataPool.SQL.Clear;
        QrDataPool.SQL.Add(SQLstring);
        QrDataPool.Open;
        ResultDataset:= QrDataPool;
    except
        ResultDataset:= nil;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataOffice.WordReportTag(Sender: TObject; var TagValue: string);
var
  i: integer;
  s, s1: string;
  report: TARWordReport;
begin
  report := (Sender as TARWordReport);
  // Using OnTag Event
  TagValue:= AnsiUpperCase(TagValue);
  if TagValue='NOWDATE' then TagValue:= DateToStr(now)
  else if TagValue='NOWTIME' then TagValue:= TimeToStr(now)
  else if TagValue='2DOTS' then TagValue:= ':'
  else if TagValue = 'TOTEXT' then begin
    TagValue := '';
    if (Sender as TARWordReport).W.Selection.Information[12 {wdWithInTable}] = True then begin
      (Sender as TARWordReport).W.Selection.Cells.Delete(2 {wdDeleteCellsEntireRow});
      (Sender as TARWordReport).W.Selection.Tables.Item(1).ConvertToText(1{wdSeparateByTabs});
    end;
  end
  else if TagValue='REM' then begin
		report.W.Selection.HomeKey(5 {wdLine}, False);
		report.W.Selection.MoveDown(5 {wdLine}, 1, True);
		report.W.Selection.TypeBackspace;
		TagValue:=''; //to prevent further OnNeedTag call
  end
  else if TagValue='PBR' then begin
    TagValue:= '';
    report.W.Selection.Delete;
    report.W.Selection.InsertBreak(7{wdPageBreak});
  end
  else if TagValue = 'BACK' then begin
    (Sender as TARWordReport).W.Selection.Delete;
    (Sender as TARWordReport).W.Selection.TypeBackspace;
    TagValue := '';
  end
  else if TagValue = 'DEL' then begin
    (Sender as TARWordReport).W.Selection.Delete;
    (Sender as TARWordReport).W.Selection.Delete;
    TagValue := '';
  end
  else if TagValue = '2DEL' then begin
    (Sender as TARWordReport).W.Selection.Delete;
    (Sender as TARWordReport).W.Selection.Delete;
    (Sender as TARWordReport).W.Selection.Delete;
    TagValue := '';
  end
  else if TagValue='CLIPBOARD' then begin
    report.W.Selection.Paste; //Using MS Word OLE interface
    TagValue:='';
  end
  else if AnsiPos('INC_TEMPLATE("',TagValue)=1 then begin
    //Insert external report
    s:= GetTextBetween(TagValue,'INC_TEMPLATE("','")');
    i:= AnsiPos(':',s);
    if i>0 then begin
      s1:= Copy(s,i+1,maxint); //parameters
      s:= Copy(s,1, i-1); //file name
    end;
    InsertReport(report, s, s1);
    TagValue:='';
  end else
    TagValue:='';
end;

end.
