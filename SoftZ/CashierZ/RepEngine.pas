﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit RepEngine;

interface

uses
  SysUtils, Classes, Controls, Forms, UCrpe32, ActnList, UCrpeClasses, Graphics,
  Dialogs, Windows;

type
  TFrmRep = class(TDataModule)
    Crpe: TCrpe;
    procedure CrpeWindowClose(Sender: TObject);
  private
  	mDelete: String;
  	function  DecryptReport(pRep: String): String;
    function  Proc(
    	pTitle, pRepName: String; Args: array of Variant;
        pTest: Boolean = False;
        pPrint: Boolean = False;
        pHidePrint: Boolean = False;
        const PrinterName: String = '';
        const pCopy: Integer = 1): Boolean;
  public
  	function  BuildFullPath(pRepName: String; var pPath: String; pEnc: Boolean = False): Boolean;
	function  Execute(
    	pTitle, pRepName: String; Args: array of Variant;
        pPrint: Boolean = False;
        pHidePrint: Boolean = False;
        const PrinterName: String = '';
        const pCopy: Integer = 1): Boolean;
	function  Test(pFile: String; pRepName: String; Args: array of Variant): Boolean;
  end;

var
  FrmRep: TFrmRep;

implementation

uses
	isStr, ExCommon, isFile, isMsg, Variants, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRep.DecryptReport(pRep: String): String;
var
    fp: TMemoryStream;
	gp: TFileStream;
    c, msk: Byte;
begin
	Result := isGetTempFileName;
	fp := TMemoryStream.Create;
    fp.LoadFromFile(pRep);

	gp := TFileStream.Create(Result, fmCreate);
    msk := 0;
    while True do
    begin
    	if fp.Read(c, 1) <= 0 then
        	Break;

        msk := (msk + 1) mod 256;
        c := c xor msk;
    	gp.Write(c, 1);
    end;

    fp.Free;
    gp.Free;
end;

(*==============================================================================
** Smart detect report path
**------------------------------------------------------------------------------
*)
function TFrmRep.BuildFullPath;
var
	mExt: String;
begin
	Result := True;
    if pEnc then
	    mExt := '.szr'
    else
	    mExt := '.rpt';

	if sysRepPath <> '' then
    begin
		pPath := sysRepPath + pRepName + mExt;
    	if FileExists(pPath) then
            Exit;
    end;

	pPath := sysAppPath + 'Rps\' + pRepName + mExt;
	Result := FileExists(pPath);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRep.Proc(
    	pTitle, pRepName: String; Args: array of Variant;
        pTest: Boolean;
        pPrint: Boolean;
        pHidePrint: Boolean;
        const PrinterName: String;
        const pCopy: Integer): Boolean;
var
	i, k, n: Integer;
    sRep: String;
begin
    Result := False;
	if BuildFullPath(pRepName, sRep, True) then
    begin
    	sRep := DecryptReport(sRep);
        mDelete := sRep;
    end
    else if BuildFullPath(pRepName, sRep) then
    else
    begin
        if not pTest then
            Msg(Format('Không tìm thấy report "%s"', [pRepName]));
    	Exit;
    end;

    // Number of arguments
    Result := True;
    n := Length(Args) - 1;

	with Crpe do
    begin
        CloseJob;
        LogOnServer.Clear;
        ReportName := sRep;

        for i := 0 to Subreports.Count - 1 do
        begin
            SubReports[i];
            with Connect do
            begin
                ServerName := isDatabaseServer + '_ODBC';
                DatabaseName := isDatabaseServer;
                UserID := isUserServer;
                Password := isPasswordServer;
                Propagate := True;
            end;

            for k := 0 to Tables.Count - 1 do
            begin
                try
                    Tables[k].ConnectBuffer := '';
                except
                end;
            end;
            Tables.Propagate := True;
        end;

        // Fill arguments
        SubReports[0];
        k := 0;
        for i := 0 to n do
        begin
        	if pTest then
                try
			        ParamFields[k].CurrentValue := Args[i];
                except
                    Result := False;
                end
            else
                ParamFields[k].CurrentValue := Args[i];

            // Halt on error
            if not Result then
            	Exit;
			Inc(k);
        end;

        // Proc
        {
        try
        }
            if pTest then
            begin
                Output := toExport;
                ExportOptions.FileName := pTitle;
                ExportOptions.FileType := CrystalReportRPT;
                Execute;
            end
            else
            begin
                if pPrint then
                begin
                    Output := toPrinter;
                    Printer.Name := PrinterName;
                    PrintOptions.Copies := pCopy;
                end
                else
                begin
                    WindowStyle.Title := pRepName;
                    Output := toWindow;
                    WindowButtonBar.PrintSetupBtn := not pHidePrint;
                    WindowButtonBar.PrintBtn := not pHidePrint;
                    WindowButtonBar.ExportBtn := not pHidePrint;
                end;
                Execute;
            end;
            Result := True;
        {
		except
            Result := False;
        end;
        }
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRep.Execute(
    	pTitle, pRepName: String; Args: array of Variant;
        pPrint: Boolean = False;
        pHidePrint: Boolean = False;
        const PrinterName: String = '';
        const pCopy: Integer = 1): Boolean;
begin
    Result := Proc(pTitle, pRepName, Args, False, pPrint, pHidePrint, PrinterName,
    	pCopy);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRep.Test(pFile: String; pRepName: String; Args: array of Variant): Boolean;
begin
	Result := Proc(pFile, pRepName, Args, True)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRep.CrpeWindowClose(Sender: TObject);
begin
	Crpe.CloseJob;
	DeleteFile(PWideChar(mDelete));
end;

end.
