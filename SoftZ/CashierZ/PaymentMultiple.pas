﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PaymentMultiple;

interface

uses
  Classes, Controls, Forms,
  DB, ADODB, wwdblook,
  ExtCtrls, wwdbedit, ActnList, HTMLabel, RzBckgnd,
  RzPanel, StdCtrls, RzButton, Mask, pngimage, isPanel,
  Vcl.Touch.Keyboard, AdvSmoothTouchKeyBoard, Vcl.Grids, Wwdbigrd, Wwdbgrid,
  wwDBGrid2, SysUtils;

type
  TFrmPaymentMultiple = class(TForm)
    ActionList1: TActionList;
    CmdReturn: TAction;
    BtClose: TRzBitBtn;
    CmdClose: TAction;
    Panel2: TPanel;
    Panel1: TPanel;
    PD_TTOAN_CASH: TisPanel;
    EdSOTIEN01: TwwDBEdit;
    PD_TTOAN_CARD: TisPanel;
    HTMLabel1: THTMLabel;
    EdChuanchi: TwwDBEdit;
    EdSOTIEN04: TwwDBEdit;
    PD_TTOAN_TRAHANG: TisPanel;
    HTMLabel2: THTMLabel;
    EdTTOAN3_MA: TwwDBEdit;
    EdSOTIEN05: TwwDBEdit;
    isPanel3: TisPanel;
    HTMLabel7: THTMLabel;
    wwDBEdit23: TwwDBEdit;
    Panel3: TPanel;
    HTMLabel4: THTMLabel;
    Panel5: TPanel;
    CmdDelivery: TAction;
    PD_TTOAN_VOUCHER: TisPanel;
    HTMLabel6: THTMLabel;
    wwDBEdit5: TwwDBEdit;
    EdSOTIEN06: TwwDBEdit;
    CmdTTOAN1: TAction;
    CmdTTOAN50: TAction;
    PD_TTOAN_EWALLET: TisPanel;
    EdSOTIEN03: TwwDBEdit;
    EdVidientu: TwwDBLookupCombo;
    GrList: TwwDBGrid2;
    HTMLabel3: THTMLabel;
    HTMLabel5: THTMLabel;
    HTMLabel8: THTMLabel;
    HTMLabel9: THTMLabel;
    HTMLabel10: THTMLabel;
    QrTT: TADOQuery;
    DsTT: TDataSource;
    BtContinue: TRzBitBtn;
    CmdContinue: TAction;
    QrTTSTT: TIntegerField;
    QrTTPTTT: TWideStringField;
    QrTTSOTIEN: TFloatField;
    QrPTTT: TADOQuery;
    QrTTTypeInput: TWideStringField;
    QrTTTypeCode: TWideStringField;
    QrTTTypeBalance: TFloatField;
    QrTTTypeID: TGuidField;
    QrTTKHOACT: TGuidField;
    QrTTKHOA: TGuidField;
    QrTTTTOAN_CASH: TFloatField;
    QrTTTTOAN_CARD: TFloatField;
    QrTTTTOAN_TRAHANG: TFloatField;
    QrTTTTOAN_VOUCHER: TFloatField;
    QrTTTTOAN_EWALLET: TFloatField;
    QrTTTHUNGAN: TIntegerField;
    QrTTCREATE_BY: TIntegerField;
    QrTTCREATE_DATE: TDateTimeField;
    QrTTLOC: TWideStringField;
    QrTTRSTT: TIntegerField;
    QrTTLK_PTTT: TWideStringField;
    QrTT_id: TLargeintField;
    CmdDeleted: TAction;
    BtDeleted: TRzBitBtn;
    PD00: TisPanel;
    HTMLabel11: THTMLabel;
    EdSOTIEN00: TwwDBEdit;
    Panel4: TPanel;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    PD_TTOAN_BANK: TisPanel;
    HTMLabel12: THTMLabel;
    HTMLabel13: THTMLabel;
    EdSOTIEN02: TwwDBEdit;
    wwDBLookupCombo1: TwwDBLookupCombo;
    QrPA: TADOQuery;
    QrPAMATK: TWideStringField;
    QrPATENTK: TWideStringField;
    QrTTTTOAN_BANK: TFloatField;
    QrTTTTOAN_MEMBER: TFloatField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrTTBeforeOpen(DataSet: TDataSet);
    procedure QrTTAfterInsert(DataSet: TDataSet);
    procedure QrTTBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdContinueExecute(Sender: TObject);
    procedure QrTTAfterScroll(DataSet: TDataSet);
    procedure CmdDeletedExecute(Sender: TObject);
    procedure QrPABeforeOpen(DataSet: TDataSet);
    procedure QrTTTypeInputChange(Sender: TField);
  private
    mLoai, mLoaiTam, mField: string;
    mKhoa: TGUID;
    curStt: Integer;
  public
  	function  Execute(pLoai: string; pKhoa: TGUID): Boolean;
    procedure getFieldNamePTTT(pPTTT: string);
    procedure Total;
    procedure FillTypeCode;
    procedure TypeInputInvoiceReturn(sInputValue: string);
    procedure TypeInputVoucher(sInputValue: string);
  end;
var
  FrmPaymentMultiple: TFrmPaymentMultiple;

implementation

uses
	isLib, PosMain, isMsg, exResStr, isDb, ExCommon, ThongtinCN, MainData, GuidEx, isCommon;

{$R *.dfm}

const
    FORM_CODE = 'FB_POS_BANHANG_THANHTOAN';

procedure TFrmPaymentMultiple.CmdContinueExecute(Sender: TObject);
begin
    getFieldNamePTTT(mLoai);
    Total;
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.CmdDeletedExecute(Sender: TObject);
begin
    with QrTT do
    begin
        mLoaiTam := FieldByName('PTTT').AsString;
        if YesNo(RS_CONFIRM_PAYMENT, 1) then
            Delete;
    end;

    getFieldNamePTTT(mLoaiTam);
    Total;
    BtDeleted.Enabled := not QrTT.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPaymentMultiple.Execute(pLoai: string; pKhoa: TGUID): Boolean;
begin
    mLoai := pLoai;
    mKhoa := pKhoa;

	Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
   CloseDataSets([QrTT, QrPTTT])
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.FormShow(Sender: TObject);
var
   mTisPanel: TisPanel;
   mTypeName: String;
begin
    FrmMain.mDefaultZero := 0;
    getFieldNamePTTT(mLoai);
    mTisPanel := FindComponent('PD_' + mField) as TisPanel;
    if (mTisPanel <> nil)then
    begin
        mTisPanel.Visible := True;
        Panel4.Top := mTisPanel.Top + mTisPanel.Height + 10;
    end;

    OpenDataSets([QrPTTT, QrTT, QrPA]);

    BtDeleted.Enabled := not QrTT.IsEmpty;

    with QrTT do
    begin
        if mLoai = '02' then mTypeName := 'Tài khoản'
        else if mLoai = '03' then mTypeName := 'Loại ví điện tử'
        else if mLoai = '04' then mTypeName := 'Số chuẩn chi'
        else if mLoai = '05' then mTypeName := 'Số phiếu/Mã vạch'
        else if mLoai = '06' then mTypeName := 'Mã phiếu';
        FieldByName('TypeInput').DisplayLabel := mTypeName;
    end;


    if mLoai <> '00' then
    begin
        BtContinue.Visible := True;
        with QrTT do
        begin
            curStt := RecordCount;
            Append;
        end;
        Panel4.Visible := True;
        Panel4.Left := (Panel1.Width - Panel4.Width) div 2;
    end
    else
    begin
        BtDeleted.Visible := True;
        BtDeleted.Left := 685;
    end;

    // Tuy bien luoi
    SetDictionary ([QrTT], [FORM_CODE], [Nil]);
    SetCustomGrid(FORM_CODE, GrList);

    SetDisplayFormat(QrTT, sysCurFmt);

    FillTypeCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrPABeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := mLoai;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrTTAfterInsert(DataSet: TDataSet);
begin
    with QrTT do
    begin
        Inc(curStt);
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
        TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
        FieldByName('_id').Value := NewIntID;
        FieldByName('CREATE_DATE').AsDateTime := Now;
        FieldByName('CREATE_BY').AsInteger := sysLogonUID;
        FieldByName('THUNGAN').AsInteger := sysLogonUID;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('PTTT').AsString := mLoai;
        FieldByName('STT').AsInteger := curStt;
        if (mLoai <> '05') and  (mLoai <> '06') then
        begin
            if not FrmMain.QrBH.FieldByName('CHUATHOI').IsNull then
                FieldByName('SOTIEN').AsFloat := FrmMain.QrBH.FieldByName('THUCTRA').AsFloat - FrmMain.QrBH.FieldByName('CHUATHOI').AsFloat
            else
                FieldByName('SOTIEN').AsFloat := FrmMain.QrBH.FieldByName('THUCTRA').AsFloat;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrTTAfterScroll(DataSet: TDataSet);
begin
    with QrTT do
    begin
        PD00.HeaderCaption := '.: '+ FieldByName('LK_PTTT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrTTBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := TGuidEx.ToString(mKhoa);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrTTBeforePost(DataSet: TDataSet);
var
    mTypeCode: String;
    mTypeBalance, mSotien: Double;
    mTypeID: TGUID;
begin
    mTypeCode:= '';
    mTypeBalance := 0;
    mSotien := 0;
    mTypeID := TGUID.Empty;

    with QrTT do
    begin
        if (mLoai <> '01') and (BlankConfirm(QrTT, ['TypeInput'])) then
           Abort;

        if BlankConfirm(QrTT, ['SOTIEN']) then
           Abort;

        if FieldByName('SOTIEN').AsFloat <= 0 then
        begin
            ErrMsg(RS_INVALID_MONEY);
            Abort;
        end;

        mSotien := FieldByName('SOTIEN').AsFloat;
        if mLoai = '01'then
        begin
            FieldByName('TTOAN_CASH').AsFloat := mSotien;
        end
        else if mLoai = '02'then
        begin
            mTypeCode:= FieldByName('TypeInput').AsString;
            FieldByName('TTOAN_BANK').AsFloat := mSotien;
        end
        else if mLoai = '03'then
        begin
            mTypeCode:= FieldByName('TypeInput').AsString;
            FieldByName('TTOAN_EWALLET').AsFloat := mSotien;
        end
        else if mLoai = '04'then
        begin
            mTypeCode:= FieldByName('TypeInput').AsString;
            FieldByName('TTOAN_CARD').AsFloat := mSotien;
        end
        else if (mLoai = '05') and (not DataMain.spCHECK_BILL_TRAHANG.IsEmpty) then
        begin
            mTypeCode:= DataMain.spCHECK_BILL_TRAHANG.FieldByName('SCT').AsString;
            mTypeBalance := DataMain.spCHECK_BILL_TRAHANG.FieldByName('THANHTOAN').AsFloat;
            mTypeID := TGuidField(DataMain.spCHECK_BILL_TRAHANG.FieldByName('KHOA')).AsGuid;
            FieldByName('TTOAN_TRAHANG').AsFloat := mTypeBalance;
            TGuidField(FieldByName('TypeID')).AsGuid := mTypeID;
        end
        else if (mLoai = '06') and (not DataMain.spCHECK_PQT.IsEmpty) then
        begin
            mTypeCode:= DataMain.spCHECK_PQT.FieldByName('MATHE').AsString;
            mTypeBalance := DataMain.spCHECK_PQT.FieldByName('MENHGIA').AsFloat;
            mTypeID := TGuidField(DataMain.spCHECK_PQT.FieldByName('KHOACT')).AsGuid;
            FieldByName('TTOAN_VOUCHER').AsFloat := mTypeBalance;
            TGuidField(FieldByName('TypeID')).AsGuid := mTypeID;
        end;

        FieldByName('TypeCode').AsString := mTypeCode;
        FieldByName('TypeBalance').AsFloat := mTypeBalance;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.QrTTTypeInputChange(Sender: TField);
var
    s: string;
begin

    s := Sender.AsString;
    if s = '' then
        Exit;

    if mLoai = '05' then
        TypeInputInvoiceReturn(s)
    else if mLoai = '06' then
        TypeInputVoucher(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.CmdCloseExecute(Sender: TObject);
begin
    QrTT.Cancel;
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.getFieldNamePTTT(pPTTT: string);
begin
    with QrTT do
    begin
        if pPTTT = '01'then
        begin
            mField := FieldByName('TTOAN_CASH').FieldName;
        end
        else if pPTTT = '02'then
        begin
            mField := FieldByName('TTOAN_BANK').FieldName;
        end
        else if pPTTT = '03'then
        begin
            mField := FieldByName('TTOAN_EWALLET').FieldName;
        end
        else if pPTTT = '04'then
        begin
            mField := FieldByName('TTOAN_CARD').FieldName;
        end
        else if pPTTT = '05'then
        begin
            mField := FieldByName('TTOAN_TRAHANG').FieldName;
        end
        else if pPTTT = '06'then
        begin
            mField := FieldByName('TTOAN_VOUCHER').FieldName;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.Total;
var
    bm: TBytes;
    mSotien, xSotien, mTotal: Double;
begin
    if mTrigger then
        Exit;

    mTotal := 0;
    mSotien := 0;
    with QrTT do
    begin
        DisableControls;
        bm := BookMark;

        curStt := 0;
        First;
        while not Eof do
        begin
            if (FieldByName('PTTT').AsString = mLoai) or (FieldByName('PTTT').AsString = mLoaiTam)   then
            begin
                xSotien := FieldByName('SOTIEN').AsFloat;
                mSotien := mSotien + xSotien;
            end;

            mTotal := mTotal + FieldByName('SOTIEN').AsFloat;

            Inc(curStt);
            if curStt <> FieldByName('STT').AsInteger then
            begin
                SetEditState(QrTT);
                FieldByName('STT').AsInteger := curStt;
            end;

            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

    if (mTotal > FrmMain.QrBH.FieldByName('THUCTRA').AsFloat) and
        ((mLoai = '05') or (mLoai = '06'))  then
    begin
        FrmMain.mDefaultZero := 1;
    end;

    with FrmMain.QrBH do
    begin
        if State in [dsBrowse] then
            Edit;

        FieldByName(mField).AsFloat := mSotien;
        Post;
        CheckBrowseMode;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.FillTypeCode;
begin
     if (mLoai = '02') or (mLoai = '03') or (mLoai = '04') then
     begin
         with DataMain.QrTEMP do
         begin
              if Active then
                Close;

              if mLoai = '04' then
                  SQL.Text := 'select MATK from DM_TAIKHOAN where PLOAI =''NB'' and CHARGE_POS = 1 and PTTT = ''02'''
              else if mLoai = '02' then
                  SQL.Text := 'select MATK from DM_TAIKHOAN where PLOAI =''NB'' and PTTT = ''02'' and (select count(*) from DM_TAIKHOAN where PLOAI =''NB'' and PTTT = ''02'') = 1'
              else if mLoai = '03' then
                  SQL.Text := 'select MATK from DM_TAIKHOAN where PLOAI =''NB'' and PTTT = ''03'' and (select count(*) from DM_TAIKHOAN where PLOAI =''NB'' and PTTT = ''03'') = 1';
              Open;

              SetEditState(QrTT);
              QrTT.FieldByName('TypeInput').AsString := FieldByName('MATK').AsString;
         end;
     end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.TypeInputInvoiceReturn(sInputValue: string);
var
    s: string;
begin
    with DataMain.spCHECK_BILL_TRAHANG do
    begin
        if Active then
            Close;

        Parameters[2].Value := sInputValue;
        ExecProc;
        if Parameters[0].Value <> 0 then
        begin
            QrTT.FieldByName('TypeID').Clear;
            QrTT.FieldByName('TTOAN_TRAHANG').AsFloat := 0;
            QrTT.FieldByName('SOTIEN').AsFloat := 0;
            s := Parameters[1].Value;
            ErrMsg(s);
            Exit;
        end
        else
        begin
            Active := True;
            QrTT.FieldByName('SOTIEN').AsFloat := FieldByName('THANHTOAN').AsFloat;
        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)
procedure TFrmPaymentMultiple.TypeInputVoucher(sInputValue: string);
var
    s: string;
begin

    with DataMain.spCHECK_PQT do
    begin
        if Active then
            Close;

        Parameters[2].Value := sInputValue;
        ExecProc;

        if Parameters[0].Value <> 0 then
        begin
            QrTT.FieldByName('TypeID').Clear;
            QrTT.FieldByName('TTOAN_VOUCHER').AsFloat := 0;
            QrTT.FieldByName('SOTIEN').AsFloat := 0;
            s := Parameters[1].Value;
            ErrMsg(s);
            Exit;
        end
        else
        begin
            Active := True;
            QrTT.FieldByName('SOTIEN').AsFloat := FieldByName('MENHGIA').AsFloat;
        end;
    end;
end;

end.
