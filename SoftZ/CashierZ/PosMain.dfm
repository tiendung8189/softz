object FrmMain: TFrmMain
  Left = 306
  Top = 173
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Chi Ti'#7871't H'#243'a '#272#417'n'
  ClientHeight = 744
  ClientWidth = 1018
  Color = 16119285
  Constraints.MinWidth = 1024
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCloseQuery = TntFormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnResize = TntFormResize
  OnShow = FormShow
  DesignSize = (
    1018
    744)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 1018
    Height = 1
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 52
    ExplicitWidth = 792
  end
  object Bevel4: TBevel
    Left = 0
    Top = 1
    Width = 1018
    Height = 1
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 216
    ExplicitWidth = 792
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 721
    Width = 1018
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnFace
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageSmall
    Panels = <
      item
        Bevel = pbNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel1'
        Style = psDateTime
        Tag = 0
        Text = '10/06/2023 1:26 PM'
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '200'
      end
      item
        Bevel = pbNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = 56
        Name = 'Panel5'
        PopupMenu = Popup
        Style = psGlyph
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '25'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        PopupMenu = Popup
        Style = psHintContainerOnly
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '600'
      end
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel4'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '200'
      end>
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    SimplePanel = False
    SizeGrip = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object Panel13: TPanel
    Left = 8
    Top = 8
    Width = 1002
    Height = 707
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object Panel14: TPanel
      Left = 1
      Top = 1
      Width = 656
      Height = 635
      Align = alClient
      TabOrder = 0
      object GrList: TwwDBGrid2
        Left = 1
        Top = 123
        Width = 654
        Height = 482
        TabStop = False
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'LK_SALENAME;CustomEdit;EdTHUNGAN;T')
        Selected.Strings = (
          'RSTT'#9'5'#9'STT'#9'F'
          'MAVT'#9'18'#9'M'#227#9'F'#9'H'#224'ng h'#243'a'
          'TENVT'#9'53'#9'T'#234'n'#9'F'#9'H'#224'ng h'#243'a'
          'SOLUONG'#9'11'#9'S'#7889' l'#432#7907'ng'#9'F'
          'DONGIA'#9'11'#9'Gi'#225' b'#225'n'#9'F'
          'SOTIEN'#9'13'#9'Tr'#7883' gi'#225' b'#225'n'#9'F'
          'TL_CK'#9'12'#9'% chi'#7871't kh'#7845'u'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCTBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = []
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowFooter]
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 2237106
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = False
        UseTFields = False
        LineColors.ShadowColor = clSilver
        OnDblClick = CmdDiscount1Execute
        OnKeyPress = GrListKeyPress
        OnDrawFooterCell = GrListDrawFooterCell
        OnUpdateFooter = GrListUpdateFooter
        FooterColor = 13360356
        FooterCellColor = 13360356
        PadColumnStyle = pcsPadHeader
      end
      object PaHeader: TPanel
        Left = 1
        Top = 1
        Width = 654
        Height = 122
        Align = alTop
        BevelInner = bvRaised
        TabOrder = 1
        object HTMLabel15: THTMLabel
          Left = 21
          Top = 43
          Width = 47
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            '<FONT face="Tahoma" size="8"><B>Qu'#7847'y</B><br/>Counter</FONT>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel17: THTMLabel
          Left = 242
          Top = 43
          Width = 60
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            '<FONT face="Tahoma" size="8"><B>Thu ng'#226'n</B><br/>Cashier</FONT>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel13: THTMLabel
          Left = 242
          Top = 79
          Width = 73
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8"><B>Kh'#225'ch h'#224'ng</B><br/>Customer</FON' +
              'T>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel1: THTMLabel
          Left = 21
          Top = 79
          Width = 54
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            '<FONT face="Tahoma" size="8"><B>B'#224'n</B><br/>Table No.</FONT>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel2: THTMLabel
          Left = 21
          Top = 6
          Width = 36
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            '<FONT face="Tahoma" size="8"><B>Ng'#224'y</B><br/>Date</FONT>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel5: THTMLabel
          Left = 242
          Top = 6
          Width = 52
          Height = 35
          AutoSizing = True
          AutoSizeType = asBoth
          HTMLText.Strings = (
            '<FONT face="Tahoma" size="8"><B>Ph'#7909'c v'#7909'</B><br/>Order</FONT>')
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdCounter: TwwDBEdit
          Left = 97
          Top = 50
          Width = 129
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCashier: TwwDBEdit
          Left = 314
          Top = 50
          Width = 335
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdCus: TwwDBEdit
          Left = 314
          Top = 83
          Width = 335
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdRno: TwwDBEdit
          Left = 97
          Top = 83
          Width = 129
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'MABAN'
          DataSource = DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdDate: TwwDBDateTimePicker
          Left = 99
          Top = 14
          Width = 129
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = False
          TabOrder = 0
        end
        object EdOrder: TwwDBEdit
          Left = 314
          Top = 14
          Width = 335
          Height = 27
          TabStop = False
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object Panel16: TPanel
        Left = 1
        Top = 605
        Width = 654
        Height = 29
        ParentCustomHint = False
        Align = alBottom
        BiDiMode = bdLeftToRight
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentBackground = False
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
        object wwCheckBox1: TwwCheckBox
          Left = 165
          Top = 7
          Width = 137
          Height = 17
          TabStop = False
          DisableThemes = False
          AlwaysTransparent = False
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          DisplayValueChecked = 'True'
          DisplayValueUnchecked = 'False'
          NullAndBlankState = cbUnchecked
          BiDiMode = bdRightToLeft
          Caption = 'Xu'#7845't h'#243'a '#273#417'n GTGT'
          DataField = 'CO_HOADON'
          DataSource = DsBH
          ParentBiDiMode = False
          TabOrder = 0
        end
        object wwCheckBox2: TwwCheckBox
          Left = 320
          Top = 7
          Width = 137
          Height = 17
          TabStop = False
          DisableThemes = False
          AlwaysTransparent = False
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          DisplayValueChecked = 'True'
          DisplayValueUnchecked = 'False'
          NullAndBlankState = cbUnchecked
          BiDiMode = bdRightToLeft
          Caption = 'Giao h'#224'ng/ Delivery'
          DataField = 'DELIVERY'
          DataSource = DsBH
          ParentBiDiMode = False
          TabOrder = 1
        end
      end
    end
    object Panel15: TPanel
      Left = 657
      Top = 1
      Width = 344
      Height = 635
      Align = alRight
      TabOrder = 1
      object isPanel3: TisPanel
        Left = 1
        Top = 524
        Width = 342
        Height = 110
        Align = alBottom
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = '* Ti'#7873'n th'#7889'i - Change amount'
        HeaderColor = clScrollBar
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clPurple
        HeaderFont.Height = -15
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          342
          110)
        object HTMLabel9: THTMLabel
          Left = 11
          Top = 69
          Width = 98
          Height = 36
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>S'#7889' ti'#7873'n th'#7889'i l'#7841'i' +
              '</B><br/>Change</FONT>')
          ParentFont = False
          Transparent = True
          VAlignment = tvaCenter
          Version = '2.2.1.2'
        end
        object HTMLabel7: THTMLabel
          Left = 11
          Top = 28
          Width = 102
          Height = 36
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>T'#7893'ng ti'#7873'n nh'#7853'n</' +
              'B><br/>Advance deposit</FONT></FONT>')
          ParentFont = False
          Transparent = True
          VAlignment = tvaCenter
          Version = '2.2.1.2'
        end
        object EdChange: TwwDBEdit
          Left = 134
          Top = 69
          Width = 193
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = clBtnFace
          Ctl3D = False
          DataField = 'CALC_TIENTHOI'
          DataSource = DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 2237106
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit23: TwwDBEdit
          Left = 134
          Top = 29
          Width = 193
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = clBtnFace
          Ctl3D = False
          DataField = 'CHUATHOI'
          DataSource = DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 9109504
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object isPanel1: TisPanel
        Left = 1
        Top = 217
        Width = 342
        Height = 307
        Align = alClient
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = '* T'#7893'ng c'#7897'ng thanh to'#225'n - Total payment'
        HeaderColor = clScrollBar
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clPurple
        HeaderFont.Height = -15
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PN_TTOAN_CASH: TPanel
          Left = 1
          Top = 21
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 1
          ExplicitTop = 17
          DesignSize = (
            340
            40)
          object wwDBEdit4: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_CASH'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton1: THTMLButton
            Left = 4
            Top = 6
            Width = 128
            Height = 34
            Cursor = 1
            Action = CmdPaymentCash
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Ti'#7873'n m'#7863't</B><br/>' +
              '  Cash</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
        object PN_TTOAN_CARD: TPanel
          Left = 1
          Top = 61
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 2
          ExplicitTop = 57
          DesignSize = (
            340
            40)
          object wwDBEdit5: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_CARD'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton2: THTMLButton
            Left = 4
            Top = 6
            Width = 127
            Height = 34
            Cursor = 1
            Action = CmdPaymentBankCard
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Th'#7867' ng'#226'n h'#224'ng</B>' +
              '<br/>  Bank card</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
        object PN_TTOAN_EWALLET: TPanel
          Left = 1
          Top = 101
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 3
          ExplicitTop = 97
          DesignSize = (
            340
            40)
          object wwDBEdit6: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_EWALLET'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton3: THTMLButton
            Left = 4
            Top = 6
            Width = 127
            Height = 34
            Cursor = 1
            Action = CmdPaymentEWallet
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>V'#237' '#273'i'#7879'n t'#7917'</B><br' +
              '/>  E.Wallet</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
        object PN_TTOAN_VOUCHER: TPanel
          Left = 1
          Top = 141
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 4
          ExplicitTop = 137
          DesignSize = (
            340
            40)
          object wwDBEdit9: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_VOUCHER'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton4: THTMLButton
            Left = 4
            Top = 6
            Width = 127
            Height = 34
            Cursor = 1
            Action = CmdPaymentVoucher
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Th'#7867' qu'#224' t'#7863'ng</B><' +
              'br/>Gift voucher</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
        object PN_TTOAN_TRAHANG: TPanel
          Left = 1
          Top = 181
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 5
          ExplicitTop = 177
          DesignSize = (
            340
            40)
          object wwDBEdit10: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_TRAHANG'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton5: THTMLButton
            Left = 4
            Top = 6
            Width = 127
            Height = 34
            Cursor = 1
            Action = CmdPaymentInvoice
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Phi'#7871'u tr'#7843' h'#224'ng</B' +
              '><br/>Invoice return</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
        object Panel1: TPanel
          Left = 1
          Top = 266
          Width = 340
          Height = 40
          Align = alBottom
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 6
          object HTMLButton6: THTMLButton
            Left = 0
            Top = 0
            Width = 340
            Height = 40
            Cursor = 1
            Action = CmdPaymentList
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Danh s'#225'ch thanh t' +
              'o'#225'n</B><br/>Payment list</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 0
            Alignment = haCenter
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphBottom
            ShadowColor = clTeal
            VAlignment = vaCenter
            Version = '1.7.0.0'
          end
        end
        object PN_TTOAN_BANK: TPanel
          Left = 1
          Top = 221
          Width = 340
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 7
          ExplicitTop = 217
          DesignSize = (
            340
            40)
          object wwDBEdit11: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'TTOAN_BANK'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            Frame.NonFocusFontColor = 9109504
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object HTMLButton7: THTMLButton
            Left = 4
            Top = 6
            Width = 127
            Height = 34
            Cursor = 1
            Action = CmdPaymentBankTransfer
            BiDiMode = bdLeftToRight
            Caption = 
              '<FONT face="Tahoma" size="8" color="clTeal"><B>Chuy'#7875'n kho'#7843'n</B><' +
              'br/>Bank transfer</FONT></FONT>'
            DoubleBuffered = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            Alignment = haLeft
            Background = stNormal
            BorderColor = clTeal
            Flat = True
            Layout = blGlyphRight
            ShadowColor = clTeal
            VAlignment = vaTop
            Version = '1.7.0.0'
          end
        end
      end
      object isPanel2: TisPanel
        Left = 1
        Top = 1
        Width = 342
        Height = 216
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = '* T'#7893'ng c'#7897'ng h'#224'ng h'#243'a - Total value'
        HeaderColor = clScrollBar
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clPurple
        HeaderFont.Height = -15
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Panel4: TPanel
          Left = 1
          Top = 21
          Width = 340
          Height = 194
          Align = alClient
          BevelOuter = bvNone
          BevelWidth = 2
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 1
          ExplicitTop = 17
          ExplicitHeight = 198
          DesignSize = (
            340
            194)
          object HTMLabel4: THTMLabel
            Left = 9
            Top = 4
            Width = 80
            Height = 36
            Anchors = [akTop, akRight]
            AutoSizing = True
            AutoSizeType = asBoth
            HTMLText.Strings = (
              
                '<FONT face="Tahoma" size="9" color="#004080"><B>Tr'#7883' gi'#225' h'#224'ng<br/' +
                '></B>Gross total</FONT></FONT>')
            Transparent = True
            VAlignment = tvaCenter
            Version = '2.2.1.2'
            ExplicitLeft = 11
          end
          object HTMLabel18: THTMLabel
            Left = 9
            Top = 41
            Width = 59
            Height = 36
            Anchors = [akTop, akRight]
            AutoSizing = True
            AutoSizeType = asBoth
            HTMLText.Strings = (
              
                '<FONT face="Tahoma" size="9" color="#004080"><B>Gi'#7843'm gi'#225'</B><br/' +
                '>Discount</FONT></FONT>')
            Transparent = True
            VAlignment = tvaCenter
            Version = '2.2.1.2'
          end
          object HTMLabel19: THTMLabel
            Left = 9
            Top = 78
            Width = 63
            Height = 36
            Anchors = [akTop, akRight]
            AutoSizing = True
            AutoSizeType = asBoth
            HTMLText.Strings = (
              
                '<FONT face="Tahoma" size="9" color="#004080"><B>Ph'#7909' thu</B><br/>' +
                'Surcharge</FONT></FONT>')
            Transparent = True
            VAlignment = tvaCenter
            Version = '2.2.1.2'
          end
          object HTMLabel3: THTMLabel
            Left = 9
            Top = 152
            Width = 82
            Height = 36
            Anchors = [akTop, akRight]
            AutoSizing = True
            AutoSizeType = asBoth
            HTMLText.Strings = (
              
                '<FONT face="Tahoma" size="9" color="#004080"><B>Th'#7921'c tr'#7843'</B><br/' +
                '>Total amount</FONT></FONT>')
            Transparent = True
            VAlignment = tvaCenter
            Version = '2.2.1.2'
          end
          object HTMLabel6: THTMLabel
            Left = 9
            Top = 115
            Width = 66
            Height = 36
            Anchors = [akTop, akRight]
            AutoSizing = True
            AutoSizeType = asBoth
            HTMLText.Strings = (
              
                '<FONT face="Tahoma" size="9" color="#004080"><B>Thu'#7871' VAT</B><br/' +
                '>TAX</FONT></FONT>')
            Transparent = True
            VAlignment = tvaCenter
            Version = '2.2.1.2'
          end
          object wwDBEdit1: TwwDBEdit
            Left = 133
            Top = 6
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'SOTIEN'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 9109504
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit2: TwwDBEdit
            Left = 133
            Top = 80
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'PHUTHU'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 9109504
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit3: TwwDBEdit
            Left = 133
            Top = 43
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'CALC_TONGCK'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 2237106
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit7: TwwDBEdit
            Left = 133
            Top = 154
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'THUCTRA'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 9109504
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwDBEdit8: TwwDBEdit
            Left = 133
            Top = 117
            Width = 193
            Height = 34
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'CALC_TIEN_THUE'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 2237106
            Font.Height = -23
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Frame.Enabled = True
            Frame.FocusBorders = [efBottomBorder]
            ParentColor = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
    object Panel17: TPanel
      Left = 1
      Top = 636
      Width = 1000
      Height = 70
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        1000
        70)
      object Panel2: TPanel
        Left = 10
        Top = 5
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 0
        object AdvGlowButton12: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdPrint
          Caption = 'In            F4'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
            F40000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C00000D4069545874584D4C3A636F6D2E61646F62652E786D7000
            000000003C3F787061636B657420626567696E3D22EFBBBF222069643D225735
            4D304D7043656869487A7265537A4E54637A6B633964223F3E0A3C783A786D70
            6D65746120786D6C6E733A783D2261646F62653A6E733A6D6574612F2220783A
            786D70746B3D2241646F626520584D5020436F726520342E322E322D63303633
            2035332E3335323632342C20323030382F30372F33302D31383A31323A313820
            20202020202020223E0A203C7264663A52444620786D6C6E733A7264663D2268
            7474703A2F2F7777772E77332E6F72672F313939392F30322F32322D7264662D
            73796E7461782D6E7323223E0A20203C7264663A4465736372697074696F6E20
            7264663A61626F75743D22220A20202020786D6C6E733A64633D22687474703A
            2F2F7075726C2E6F72672F64632F656C656D656E74732F312E312F220A202020
            20786D6C6E733A786D705269676874733D22687474703A2F2F6E732E61646F62
            652E636F6D2F7861702F312E302F7269676874732F220A20202020786D6C6E73
            3A70686F746F73686F703D22687474703A2F2F6E732E61646F62652E636F6D2F
            70686F746F73686F702F312E302F220A20202020786D6C6E733A497074633478
            6D70436F72653D22687474703A2F2F697074632E6F72672F7374642F49707463
            34786D70436F72652F312E302F786D6C6E732F220A202020786D705269676874
            733A4D61726B65643D2246616C7365220A202020786D705269676874733A5765
            6253746174656D656E743D22220A20202070686F746F73686F703A417574686F
            7273506F736974696F6E3D22223E0A2020203C64633A7269676874733E0A2020
            20203C7264663A416C743E0A20202020203C7264663A6C6920786D6C3A6C616E
            673D22782D64656661756C74222F3E0A202020203C2F7264663A416C743E0A20
            20203C2F64633A7269676874733E0A2020203C64633A63726561746F723E0A20
            2020203C7264663A5365713E0A20202020203C7264663A6C692F3E0A20202020
            3C2F7264663A5365713E0A2020203C2F64633A63726561746F723E0A2020203C
            64633A7469746C653E0A202020203C7264663A416C743E0A20202020203C7264
            663A6C6920786D6C3A6C616E673D22782D64656661756C74222F3E0A20202020
            3C2F7264663A416C743E0A2020203C2F64633A7469746C653E0A2020203C786D
            705269676874733A55736167655465726D733E0A202020203C7264663A416C74
            3E0A20202020203C7264663A6C6920786D6C3A6C616E673D22782D6465666175
            6C74222F3E0A202020203C2F7264663A416C743E0A2020203C2F786D70526967
            6874733A55736167655465726D733E0A2020203C4970746334786D70436F7265
            3A43726561746F72436F6E74616374496E666F0A202020204970746334786D70
            436F72653A43694164724578746164723D22220A202020204970746334786D70
            436F72653A4369416472436974793D22220A202020204970746334786D70436F
            72653A4369416472526567696F6E3D22220A202020204970746334786D70436F
            72653A436941647250636F64653D22220A202020204970746334786D70436F72
            653A4369416472437472793D22220A202020204970746334786D70436F72653A
            436954656C576F726B3D22220A202020204970746334786D70436F72653A4369
            456D61696C576F726B3D22220A202020204970746334786D70436F72653A4369
            55726C576F726B3D22222F3E0A20203C2F7264663A4465736372697074696F6E
            3E0A203C2F7264663A5244463E0A3C2F783A786D706D6574613E0A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            20202020202020202020202020202020202020202020202020200A3C3F787061
            636B657420656E643D2277223F3EB00AC09D000006824944415478DAE457596C
            5555145D77BE6FE84865506A8B856AA2600C20126CA84315D1384F20094E8911
            FC51FF34FEA8314663E287261A7F8C266AA35109811F49710AA212C5688C8840
            A1145A3ABEF7FA863BBBCEB9AF2DD057AB42E2872FD93DEFDE77EF39EBACBDF6
            DAA74A1445F82F3F2AFEE38F3ED3035B76FC2C2872C2300A4F664B7E8B22D5B0
            ECECE75D3BEF7FEDA527B77B6E71CAFB33313C238020088BB75F7F6922F03D68
            9A0628EA0404CF751006FE390B1B6FED743D677D6FCFE1ADC9640A99D1416CFB
            E8CDB3C380A22A28958AC866F3B06D0BA66941D7358461805C2E8FC1C14134CE
            9B937E7CF3C6CECF761D58E707D832D077E46F03F87B1A208DD144847254C986
            AEEB045742EFB13EB434D625AEB9A2B933F48B37E7C772674F848A243B8A732E
            AFE26BF1D5F77D0960647454DE5FD4D460DD74D5C5EFA753893BEC64D5D94981
            D410230C02497B186A50435532118471948A93E2BBA0B13EB9F1DE8ECEE1A1E7
            D7EDF9A6EBC33306201615940704E033C1503C281462A432059A0EDBB2D077BC
            0F3B76744946AAABD358D8D2AC6D7AF0CE773F5FBCDCE014EF9D510A5CB7049F
            8B174975A15880537250E23DD7732590DADA3A34359D8F9A9A6AD88904868647
            B1EB9BEF616B45ABBEDA78FD8C19C88F6508C2A3E2C7385A702D079667C3E2CE
            35325055550D8B0BC70C799205CFF3914AA551C81FB1FE11808FD7AE99ADE9FA
            D386616E302C2B61B0E4C696752466CDAA632A426882765D65182C47438E8AAA
            966D4991E912600588749A00325F27763FBAA9E079BC47D6E81B24CF792F08FC
            E76FD9BAAD6F0A0045515E603C3469360AF6EDDB8F479E78B16C7D38D509A7A9
            9AF12F3A5959AAAA0995205582672458BE9BB999249F78706A0A22DC15CF1C8E
            7B2DEE738E410F0D49B798484CACC47F4E5E6E0A00E9192AC51B29F40C553225
            34C30D8A9FEFAC0C00912D6650551D4C451C5CF887253722AA69281B1164094E
            9813A2B2DF47A7D0A2E586B1F2F72E044213AE4B5DB814AE27CB99EE6A561621
            21B6AC6E47EB9A1BA051584C3214D3C4979FEEC5DA152DD00D13356913494B87
            A92BD01465DCA924083F8CE0383EF245076F777661D933CF20CAE7118A208852
            218F833B77E2F0B7BBB5E9AA406B6E6B43D8D3135F1000E94066B81F2706B338
            38E020599596E566DA2A01A872D3BE47F1391E8AF9220AB92C2E5D508BCC503F
            F0CB2F88848171E74EA100978675EEF2E538B86B576500DC8522CB29938161DB
            92669520464786502C94082443D773C1160CD332A5B0443AD811A53F385CC02D
            E6E1CC4B622C9B4548FA7D2EFE6BCB2A34CE4960EC9D376056578B7995CA0048
            61697804612E175B2F43670A8CC041616C0C3E27F74A2E34C32030438A525128
            343AA4CF1C8B309490DE9187163A64C5E1F3251CED8FA3259B4190198D355439
            05110A4343084647E464969722D5362E6B4861EFDE9FB078C9622412362D58A3
            9034F68148BAA42AD56F8B6C49CAF7ECF9114B66A5502268417DEEDB9763FA93
            AC3EDE131BAD0880471E9AC708DCE1617865F4265F6A9FCD43C6A1016CF9E463
            78410445A97CDA1125267E5B3AA716D7B6CC467E64440A6F718A60B8EB02D3A2
            538CD35B31DD4ED8689E0C784E49A23785E018B7CD4BE3EEE67A5909AA64402D
            BB605C965110CA9409E65C02F7B2A3C8F07D8715A03CF0141650033F3C763FD2
            041D621A06A4A2291C81D473389155C46B47D370A30C7830C27317D6C9FC8BA3
            9910A0382D892D47041E5207B45804149DA8FB670F15849DC1E4ACEBCB1A5008
            282040447F01C013A81942BD5EC9C0E51DEB65390A7A87BEFB0096111BD4F8EE
            C52801043100313AEC076DD76D90EF88EB7DAF3C2C85DA4ABC0E9B5A341D00C1
            A593CDAA22FFC2C102DD934ED8B8602E8E76F7E3D58C2D1B0E8FAAE5A8D4DD85
            EFDB58C5F79B5AE7A3B7FB049A044059AE211C5642ECF59501D07D34D42E6A85
            595505931DEDAB9E03A863151C3B72002D172D91BB953D0182FA40A640156990
            120A2523E2D7BEE387D130B7067DBDDDB8A2BD1DAE68E73C2BCA5E104572144C
            9C022088A22F8EEFF97E852C6E25CE6FCF0557A656AE6EC3D53776C0B269C116
            5BB221DAB2120B51E0E6E3024BC80AF1842B967C7A00B5942BE2F081DFF0C7FE
            2FF293E73B593DBB272AE7E47C10D57C0E97887F44C669BAE7A1A7B7F37E520A
            AEDC8C265B9E583C26BEDC3C274A342C3F469885F7DF7A6EED69523BC8E88D84
            F59E0620CDE13CD1064EEAAE75E5F8B79F11C6D069F706C4BD29002A1E3014A5
            9E43F334AA9BE92336D2CD3586A79DFF7FFFDFF19F020C0087AEA79FFA4104B0
            0000000049454E44AE426082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel5: TPanel
        Left = 130
        Top = 5
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 1
        object AdvGlowButton15: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdSave
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
            F40000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000005B54944415478DAC4563B4F244710AEDE9DD9D927FBE078
            1C07C1610971677116173B427604014EECC072789223FF0242FE80234B263BC9
            0E2C5B0E20B0641EB28444641959E8644BD6C9E22540E2BD0BFB9A19D7573D3D
            CC2E0B8BE5C0238AEE9EA9AEFAAAEAEBEA55BEEFD3FFF958D1C5DCDCDC973C7C
            61D6E7E7E7D46C3629994C523A9DA66C364BF97C9E52A914150A05721C872CCB
            A2EBEB6BD13D3E3E16393A3AA28B8B0BAAD56AA23F3C3CDCEED79D9F9FB75A00
            B0F3AF7978353636261BCAE5322D2E2ED2C2C2C27F8A707A7A9A66676705081E
            647C757535DE2903AF9E3D7B268ED7D6D6A85AAD522C16A39393132A954AB4B2
            B242954A45A2EEEFEFA7C9C9C9AECEE10C36D6D7D7256B78A6A6A6EE2EC1D0D0
            10D0856BA4DFF33C996F6E6ED2DEDE9E189C9898781080ABAB2B01A19412E9CA
            013C4611A3EBBA52473C1801060691A58738073760230AA01DC8BD001A8D069D
            9E9E5226931183261B20DC5D0F1C8280D0AFD7EBA1F30765A01D29041C40DD91
            01C30B8C70148FC725230006B0788FA8E1DCE8C306F6404C39BA02309B60F8F2
            F252D6C638E628C1F6F6B61C411335F8821151038059431F7AB0D7B504F888A8
            30261209190100D10D0E0E522E971343E8013B3B3B2D19C00887060CF6A027A0
            7CC5625180E13140BA0280626F6F2F6D6D6DD1D3A74FE584E01DBE43F6F7F7C3
            B49AE306E7667D7070401B1B1B343A3A2A6B93AD7B01E023140D80274F9ED0EE
            EE2E2D2F2F87E9340F40B49F7923D045969E3F7F4E7D7D7D21793BEDBB05C0B6
            ED1000CA303E3EDEF118DDC5EA4ECD283ABF17003E0280612DA4FD18750380F7
            C6A9617D74DD1500A2C6F8FDE713BC23306A0C28CC6FC6303265F46EE6666D9E
            4F5FFFA51D5AD6FD0070F309A1EC18395FBD248735D22C1987281748C6264AF2
            3B8B8389C71019B76D97BB254BB5415466C25778ACF078CDE3D167BF8ADD7F95
            013719278F37ACFFAD9DC41538A24788C29CF7187AB99E06E2020C8BC760F88F
            3E18E596EE68BB5D33808FE87AC84083C3CCB0139BBD2478B430C76869C16152
            81A0540D38F63410489D15EA4D0DB49EB50540D70C18E643A9564850827553AC
            F1C90B5F40242C9F921C08CA0260A6DC705C6367951A4449FA11FD4F7F2A4AB1
            5E3D673FAC0F985320004A0E652C36C04E7F7EAB28E9008CA234D7DFB66ECA82
            E8FDA004572EAE70AE3B0B1A5F96C166795F9D83793000D3B71BF904F5A4D940
            96E8E377890A49969462D1244C583A03A8378806F25DD47C3AAF0600587EFC83
            49CB366A8F9CEE778139B30001E53A0328F066064F1B7B44799EA7133E658212
            C4024E800C68740D38F534982B067151D57B8B0CBC594CB6008836272B7A8F47
            EF02B7C7A622479BE70CCC8E2BEACF10F5B2302E4AD94A9CFB141C41765C65FA
            C3791900EA9A13DF6CF994671B6EC16E711C6DCD2100DC5E2D5D2F6351893717
            39F2DF4FD8D035A7F392B3C0CE4146905962523A03CD406A72FE7DBAE40C1401
            986DC4B2564BFAE1EB1600F3D30B57A85CC7698D1E359F79470998BCA3598DD3
            21BD407390FB85E64203A59086A4A4297DFBC693C6E5E4141D1E1E8A7DFCE2C6
            6F8B8E005A7A76324639AE1FCAF0E6CCA7021FAF1473C0B1835E0000CAF0479F
            02E9869CFA2A4B191C482BE14C22DDCAFC8E00F0128E718DCAAFA1982D9BF39C
            C61E303F09126AF6A329E11846019832E0885A8D9B1382969DE07F03030361ED
            F1CBEA1600BCC40FD0919111518C05CDE6FD2144AE646E05675F45A2BFB99174
            5BF6824E689AD335B7C814037FFCF8B10476767616963BB8E874DA67666636B9
            F6EF990F3B1FFEC0D12BEAE5343ECA1195B82F230B765CC911BCE53F68483A13
            BEF4854B0EF4B4E2D349D923F5DD472109D9E76F4B4B4B2F5B00F0C7020F00F0
            8225D7E9B705E9FB47E9D32FF358F0BE19B997FC8878145EEA926D30F11796B7
            ECD76D0700058018E900C0046C1C9A7BC8DC2C5E44CF8D38F623DFF1FE8C6597
            7D966F95200001034EC470D479FBDABF0380DF41A29969B0CFB013FD23C00047
            6FE41E9F2702BA0000000049454E44AE426082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel9: TPanel
        Left = 250
        Top = 5
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 2
        object AdvGlowButton19: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdXuatHD
          Caption = 'Xu'#7845't HD GTGT        F1'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
            F4000000097048597300000B1300000B1301009A9C180000020C49444154789C
            ED97494BC34014C77BD18FEAA975296EB77A101125AD3BB46017A3E272D083A2
            B517410471BB7910A56AADB8803669D5D84C264F6630B58EA9666672D3814792
            1732FFDF5B66860402FFC3C38844969AA77AE612A31D194309A640C4A2C1148A
            06530342E2E35D6A4E8DAD23329159CABB1AAEDC82FDFA0448BBA4F76068D4AC
            CA2D7D5FB9BB8068288DB8C4E32DF1A6F1F04C76465933B7CE5E692422E2A85C
            04304AF47B61F15CBEFA0D00E905CFE2C4E71920EE22CE0260322946601B9AAB
            38D2AFBF8873014CF6A80952F3AD73A326BE795286585BE613E0F91E6CF3C53D
            72ED0A6CCB04305F6A7E2E8091F6EFDD1E6BCD407671DB53CDF19B0E74600BE0
            ADCC0FA04874BBA57FA41D1900044424038A0BC07464E1C7754EDE13F164DF32
            7D26D77A716900EC357246D41700EC83B83000FE101FEB54B9B6DE89EE397900
            5C1739F185F757E875B7687EB1463E2900CCA45D1680948B0BC0666A2E72023A
            E2640E92512E00C4349C68069C46E506C075E2A2257022170200267DA35DFCAB
            80DD4BB8012CCE75CEEE0F5200163D520B60572BBE887303984EE414E077616D
            781E1E7BE3352B0DA99219D0FD49BB440F943C8BB3DDEE1380563B528F7347D4
            DC7C247272141381838D1D6ACEF1CDFA7C0738CCEE3514F30500246B2E5C8258
            288D8DC7075FC5CB37A7A084D2A62780D9C1D5790221FA1BD6E8D74C0926FB3D
            0104FED27807A37D79F4582C2A340000000049454E44AE426082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel3: TPanel
        Left = 896
        Top = 5
        Width = 105
        Height = 66
        Anchors = [akTop, akRight]
        BevelWidth = 2
        TabOrder = 4
        object AdvGlowButton1: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 101
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdQuit
          Caption = 'Tr'#7903' v'#7873'        Alt + X'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            424DC20400000000000042000000280000001800000018000000010010000300
            00008004000000000000000000000000000000000000007C0000E00300001F00
            0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77344F
            DB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967FF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416924FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A6412D65B
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565BFE7BFF7F
            FF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A261EC822
            FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7FFF7FFF7F
            FF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863F24A6C32
            061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822E619651A
            FB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D368C324A2A
            E619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105271E4A2A
            2822282228220822C515C411C415E515E61D2826082208220926E721A1095353
            FF7FFF7FFF7FFF7FFF7F94522105C51529260722082208220722072207220822
            07260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A230D
            C4152826C61D830D620D620D83118311830D82096209630DE821F452FF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C322B2E2B2E
            2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
            FF7F}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel12: TPanel
        Left = 490
        Top = 5
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 3
        object AdvGlowButton2: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdDiscount1
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
            F80000000473424954080808087C0864880000000970485973000000B1000000
            B101C62D498D0000001974455874536F667477617265007777772E696E6B7363
            6170652E6F72679BEE3C1A0000033C49444154488995945F685B551CC73FBF7B
            6FBA24BB5D4CEA1A5717E2BCCE7FD051873A756C2BFE792B32F6B062EB9853A9
            6CF867EE5127D4810813D6820FB64F4E99F341454198A2323A1C0C86C8A61B1A
            29D5759B266B47D2A4E94D7AB37B7C68D3A66DD2DCFEE03E9C7BBFE7F3F971CE
            B9475841291076ECE9405417C856948A224C01C308DF21DAA09CFEF43A80656D
            7A1BD46EF10CDFBE67039AFA1CD8B24CCC067564E3E8C5554AA417E4B0E109BE
            A3FB11449D026EAF130D0C66C7DE57228852BDC323BFBFA77983CB0F75E036E0
            0C666FF0C1C47F74DA163FD1BE0560D925AA80DF563B246FD15C38FAD085C4A1
            9CAB8E76DA16BB0B169AB83445EDCD35059EE07051864EB4015856EB2BCDF89B
            0732DB8E2835830D858A67AA0A96C0CD20F474C2DD3138731EBEF8BE1CFD5986
            4E6C2F0FFEBEEB057F2E17B1DD5981DF5FCA2FD983AA9DEFEF82A71E87EC241C
            E8866D0FCF86D99A6EEBD95B8E8916FCB60C07701C2DB84050735936C6E1FC6F
            70B81FF253704F1C80E40D53BB32DA783CD1F2FAC41FEBDEB03399C0D395D35C
            57C4A80B0738FB2BECDB056D0F42C00FE72E904C99A452AB01B00BBE354BE600
            86A14A02A09E7C7E33B7E4474445AA051181679E800D3138FB0BC9D3C939F872
            659AC511F1785A662AD4C878AC95A93B6218F108DABA464A89316EBE73AA6ABC
            2992EF3540BE5C021781E73A20DE026B23734F6EBA88333989AF1C43C3E7D3AB
            C2037E271B1B1E78D7403806F42FF8BAAA01DA1F05A704E36918B94AF67A9109
            15C0191E67FA72122791C22D94AAC275DD751B4DA763A60940B5771F04E9ABB5
            32C994C9988A62C4C338979228DBA91545D75D150E17F6ADFFEBA34FE6041592
            632CBA3E922993CCFDAD84FA9E85069D5BFF6649779DC41DCF57854722532FDD
            9918FCB8FC6EEE3F90A1CFFA411D0254253C955A4DB0E7319C4B49D2DD27D1C2
            0102BB5A3DC11708164B2ACFB9CA4FA34582180F44C1A7E34E163DC1172C5165
            DDDCF472DFD56BA183E5B171DF5A421FEE446F59C3F4B92B4CBCF60D6A768375
            DD55E126FBC5F57F0E1CAFC6AA799BFE631DF83A930EEC9C4F0A9AD9809B9BEF
            7EB9CEEB0AAA4A2ACA0BBCAEA096C42BDC9360B16425F015D5A8F5EA5797A36F
            3AD7EEDDBFB77E7ABEFE07D34362EA3DF9D6870000000049454E44AE426082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel6: TPanel
        Left = 370
        Top = 4
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 5
        object AdvGlowButton3: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdGiaohang
          Caption = 'Giao h'#224'ng        F2'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
            F4000000097048597300000B1300000B1301009A9C18000004F449444154789C
            ED956B4C536718C7BB2CFBB2AFDB074E675C966CCE2CC62FCE204C1848A10C71
            8A8B51F1321550268A262EF1B20D1AB9086D11C1527AA1D416480B6E4C274EC0
            DBA64E99380547088AA082964B6D0B32CEADA7FD2F3D68D5F42218FDC63FF9A5
            C949DEF7F9F579DEF71C81603AD3F193881C6B6E44CE50D564582819D82D78DD
            8994F43A5B7449B8AA5B1A94CB8A655890D56F7FED02A2FDB79C76CDA7B06B66
            05E55ED9BC37231091631D89C8B16232844B1EF67BD62466E3DD3839B53D4E46
            EE0E44BC6C7C7EC0A2A813BC8DBA902F61168A164BED43B3336E6156469797D0
            1F06B0D3F008EB55A3C8AA1DC23AD52876181E419CDBD3CB550A3755CAF32AC5
            720A4191916331F9A3EF05102036E3970F805A21BE2DB98363FF70A86D61B0BD
            9AC1C9760EFB8F53509E63D1D0E644BA9EC289360EDA3F686C28BC04B682C0E9
            BC0D8CA7486C8619D109C93EC4A61B9F8890FBFC0B9889BD38362190A6EC8527
            8E31862FEA49633B85EE4117DC6E37546729FE59F780AF404C8A02A29858C48A
            C4888E5AF40C5122C405368FC4C3796ABCE35FA06986B703C7AFD1A86F25B1A3
            86C6C91B14640D2434E76934B693D8A2A7D0709D46D545D2AF40D2F26FB072E5
            6A2C8A8E7941222AA31E6219DF85D54105361EEE41C703175A7B5864D533B83D
            E882FA3CCD8FA26B80C39E3A1AB7065C68BAC9F8155892F83556AD5E837871C2
            0B0273D61EC5E7BBFB3C12AD4105D29E8CC0FED877042E57F011C46577227E5B
            351237CBF1D5561D6233EBBDCCCFF8139FA4772032C701B1940C0B28B0EED01D
            94349138746A1C1BB4145467487C6F2221A9A7A0394B62858284F23485BCDF7C
            47F032C2B28611271D479C9C3205144851F480E58021078B834D131DF8F51A8D
            7FFB273A50708282DB0D74F4FB8E202ACF8EB0EC8708CFB6F8155855DC079336
            17068D7430A800E772E3D12885B2B32C5C2E179ADB49DC1EE0E0743A51DA44F1
            CFBA2D94EF0864E310158EF1F813A8D4140317D6C2D172C0E92B7072E21A2E97
            F6204D37828D6A071264766C3738B0ACD881E4323B32F40EC41CB063ABDE8135
            4A3BD6165C9ED2080C1A1960BD8AB1CE6AEE0501A79ED07936F270BD3C01BF1F
            4E9D14BDAA5030E55314F83B13A33714CF3A202A1C171E95EDBAD2989F42BF2A
            05853A7B64EEB06DE1FEC191A744E5D986A2F26D83CF9354D0359222BB447F27
            3F67F30A8865D45FDE4372B01F4A950A35DA7CEC519C41BC9C7CE9BF7A4A74BE
            03A1597D08FDA90FE1120B12E46328549AF943272FAF4162D1E8F3DF854EAFC0
            9DAA18B2CDB81C5B4ADA30629A0377ED0C50E68FF9F3D0A2499EB4C0F32C291A
            41A77131BF07699AC5FFDE3346625D51373CB53C35BD029945CDC329C56DB869
            5C0A4BCD02A495DD455AE5C4BC38A310E5A5A58895FE372574EA2270E699F851
            710A9B7434761DBE08C6FC115A9549482DBE81CC83A787BC0291B9C3B22F24FD
            56C6F4218E68E468EEE0F87BBE513B0EC638132DA5896CB8C4629B0A972B5630
            9E0EE4374CBC474A9A595C3524635839DB1D26B158237386A57E5EC7C2FB17F4
            9BB0EF6786FF1C6F53B4F1B78255876804530C6A0995DD3417A91A3B1A6F72D8
            5C398607C60560CA43EE075E5447ECF4CCEACA91F5A8AB90C0AA9F0BAA8C70A2
            EA7D62CA0275219FA196A0EE5647F387B0CB2006AD1482AD10A6075E04C15BCE
            2321C5AC86B0D14A826394848555130B05AF18464BA4B25AA28F56111CAD241E
            B36A62EFABEE359DE908DE54FE073767FEF774E8F1200000000049454E44AE42
            6082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
      object Panel7: TPanel
        Left = 610
        Top = 4
        Width = 110
        Height = 66
        BevelWidth = 2
        TabOrder = 6
        object AdvGlowButton4: TAdvGlowButton
          Left = 2
          Top = 2
          Width = 106
          Height = 62
          Cursor = 1
          Align = alClient
          Action = CmdDiscount
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          Picture.Data = {
            89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
            F80000000473424954080808087C0864880000000970485973000000B1000000
            B101C62D498D0000001974455874536F667477617265007777772E696E6B7363
            6170652E6F72679BEE3C1A00000315494441544889A5954D6C545514C77FE7DE
            F75E3BD3020269C142086DA89BDA0821501116C6B870815823F58320D105EE34
            7103446149023B36BAD144914D814416AD61A1061217208154032486048B4981
            A135749461E6CDFBB8D7C523F44DE77518C6FFE6E59D8FDF39F7E4BCFB842675
            F79D2D7BAC95C30022F6F3674F5EF8AE993CC9324E7EF072BBFB4FB963D5994B
            F705EC9DB7B70C837C9F8AB758FB56CFE90B672CC8ED37372F0B97E41FF67E7B
            DE7F6281C2C84BAF186114E8022908F692855781FCBCD032961F111902BB1298
            B1985DAB4E5DFC291DA4E61730C297091CC0AEB4B023030E904778E3111CA04B
            505F343C81FD68A37BB7D81664C09A521886B891E9E819BB52CE3CC1CCAC5AFE
            7FE0D572195FECBAB45DE051E7B3DE08F04932D3D6E016504A97B456C7DB45EF
            EB19BB5296991D5B1785EDF617E085AC64BDD86203C1D4ED078886C09F83A7A5
            B4AE6827D7ABA29C7D7F2178FEF998EEDD01DDBBABE825B5888EC198AE3D0FD1
            2BEAE100268E73D604C7EAB628ADB6D549AA78E0759B1A78E7269FB81850B9DD
            88002A32761498CE72962614D17DC1BFA5F127935EF203319D9B2A44C52A53A7
            1466819D1385318E3A28F0F81A38DEB89704BE68A842540C983AA989CA0BC7BA
            AEF775DFF8C4DE86236A159E96FCB56BDB5237327F00DDCDC22D905F6BF0EF08
            6131F33A439418D3995FE778A179D74AF3F0A80C3DC311DE324B5C85A951071B
            D717B1C62AD70F0F371C51EE3993C06703A646E7C6A2BDE4A95C10A7110194C6
            3B01FC9EE5346E40753A4A3AAFCCD9A7CF691EDC504CFFAC31D5EC1129AD2B22
            DEA702707D64C05BCAE29D56E4638117A1F6F37F1A29A54BCAD127FCF660DFC0
            E9EBA59AF2F7DE1B5A11C7BAD02A1C20E7E406D7FC70F9DAE38269E7DF516936
            0A43AA95D6E0000FF2955BE9F7BA01DE786D30B0D6B8ADC0955241FFD9AB6D35
            B6F941AEEB1D1285015022466B5D5808A8B52E281103C9DE6BE57C363F26FBA7
            3FBCFE1913D1D537FEDB4D0133F9FA86234110EC4FC7789E77B4776CE200C0CD
            EDEBFB4DDC76AFFFECAFFF3655204B7F6EDFF0551C451F0268C7F9A66F7C626F
            3379FF0150815B39D02436550000000049454E44AE426082}
          TabOrder = 0
          Appearance.Color = cl3DLight
          Appearance.ColorTo = clScrollBar
          Appearance.ColorChecked = cl3DLight
          Appearance.ColorCheckedTo = cl3DLight
          Appearance.ColorDisabled = clMenuBar
          Appearance.ColorDisabledTo = clMenuBar
          Appearance.ColorDown = cl3DLight
          Appearance.ColorDownTo = cl3DLight
          Appearance.ColorHot = cl3DLight
          Appearance.ColorHotTo = cl3DLight
          Appearance.ColorMirror = cl3DLight
          Appearance.ColorMirrorTo = clScrollBar
          Appearance.ColorMirrorHot = cl3DLight
          Appearance.ColorMirrorHotTo = cl3DLight
          Appearance.ColorMirrorDown = cl3DLight
          Appearance.ColorMirrorDownTo = cl3DLight
          Appearance.ColorMirrorChecked = cl3DLight
          Appearance.ColorMirrorCheckedTo = cl3DLight
          Appearance.ColorMirrorDisabled = clMenuBar
          Appearance.ColorMirrorDisabledTo = clMenuBar
        end
      end
    end
  end
  object MyActionList: TActionList
    OnExecute = MyActionListExecute
    OnUpdate = MyActionListUpdate
    Left = 164
    Top = 216
    object CmdQuit: TAction
      Caption = 'Tr'#7903' v'#7873
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdSetpass: TAction
      Category = 'POPUP'
      Caption = #208#7893'i m'#7853't kh'#7849'u'
      OnExecute = CmdSetpassExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u        F7'
      Hint = 'Ch'#7881' l'#432'u, kh'#244'ng in ra h'#243'a '#273#417'n'
      ShortCut = 118
      OnExecute = CmdSaveExecute
    end
    object CmdReprint: TAction
      Caption = 'In l'#7841'i'
      Hint = 'In l'#7841'i h'#243'a '#273#417'n'
      OnExecute = CmdReprintExecute
    end
    object CmdDiscount: TAction
      Caption = 'Khuy'#7871'n m'#227'i h'#243'a '#273#417'n       F5'
      Hint = 'Khuy'#7871'n m'#227'i h'#243'a '#273#417'n'
      ShortCut = 116
      OnExecute = CmdDiscountExecute
    end
    object CmdScanReturnQty: TAction
      Caption = 'Tr'#7843' h'#224'ng'
      Hint = 'Nh'#7853'p h'#224'ng tr'#7843' l'#7841'i t'#7841'i ch'#7893
      OnExecute = CmdScanReturnQtyExecute
    end
    object CmdPrint: TAction
      Caption = 'Thanh to'#225'n'
      Hint = 'L'#432'u v'#224' in h'#243'a '#273#417'n'
      ShortCut = 115
      OnExecute = CmdPrintExecute
    end
    object CmdScanVIP: TAction
      Caption = 'VIP'
      Hint = 'Nh'#7853'p th'#7867' VIP'
      OnExecute = CmdScanVIPExecute
    end
    object CmdLock: TAction
      Caption = 'Kh'#243'a ch'#432#417'ng tr'#236'nh'
      Visible = False
      OnExecute = CmdLockExecute
    end
    object CmdCommInfo: TAction
      Hint = 'Xem th'#244'ng tin h'#224'ng h'#243'a'
      OnExecute = CmdCommInfoExecute
    end
    object CmdSetPrinter: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n m'#225'y in h'#243'a '#273#417'n    '
      OnExecute = CmdSetPrinterExecute
    end
    object CmdSave2: TAction
      Caption = 'H'#243'a '#273#417'n t'#7841'm      F10'
      OnExecute = CmdSave2Execute
    end
    object CmdScanSku: TAction
      Caption = 'Nh'#7853'p m'#227' h'#224'ng'
    end
    object CmdScanQty: TAction
      Caption = 'Nh'#7853'p s'#7889' l'#432#7907'ng'
    end
    object CmdChangeRetailer: TAction
      Category = 'POPUP'
      Caption = 'Thay '#273#7893'i '#273'i'#7875'm b'#225'n'
      OnExecute = CmdChangeRetailerExecute
    end
    object CmdReconnect: TAction
      Category = 'POPUP'
      Caption = 'K'#7871't n'#7889'i c'#417' s'#7903' d'#7919' li'#7879'u'
      OnExecute = CmdReconnectExecute
    end
    object CmdReLoad: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      OnExecute = CmdReLoadExecute
    end
    object CmdCongnobl: TAction
      Category = 'POPUP'
      Caption = 'C'#244'ng n'#7907
    end
    object CmdDiscount1: TAction
      Caption = 'Khuy'#7871'n m'#227'i m'#7863't h'#224'ng       F6'
      Hint = 'Khuy'#7871'n m'#227'i m'#7863't h'#224'ng'
      ShortCut = 117
      OnExecute = CmdDiscount1Execute
    end
    object CmdChitiet: TAction
      Caption = 'Chi ti'#7871't h'#224'ng h'#243'a'
      OnExecute = CmdChitietExecute
    end
    object CmdBanle: TAction
      Category = 'POPUP'
      Caption = 'H'#243'a '#273#417'n b'#225'n l'#7867
    end
    object CmdTrahang: TAction
      Category = 'POPUP'
      Caption = 'Nh'#7853'p tr'#7843' h'#224'ng'
    end
    object CmdCamung: TAction
      Caption = 'Danh s'#225'ch h'#224'ng c'#7843'm '#7913'ng'
    end
    object CmdPhieuquatang: TAction
      Caption = 'Ph'#225't h'#224'nh Th'#7867' Qu'#224' T'#7863'ng'
      Enabled = False
      OnExecute = CmdPhieuquatangExecute
    end
    object CmdGiaohang: TAction
      Caption = 'Giao h'#224'ng'
      ShortCut = 113
      OnExecute = CmdGiaohangExecute
    end
    object CmdXuatHD: TAction
      Caption = 'Xu'#7845't HD GTGT'
      ShortCut = 112
      OnExecute = CmdXuatHDExecute
    end
    object CmdPaymentCash: TAction
      Caption = ' '
      OnExecute = CmdPaymentCashExecute
    end
    object CmdPaymentBankCard: TAction
      Caption = ' '
      OnExecute = CmdPaymentBankCardExecute
    end
    object CmdPaymentEWallet: TAction
      Caption = ' '
      OnExecute = CmdPaymentEWalletExecute
    end
    object CmdPaymentVoucher: TAction
      Caption = ' '
      OnExecute = CmdPaymentVoucherExecute
    end
    object CmdPaymentInvoice: TAction
      Caption = ' '
      OnExecute = CmdPaymentInvoiceExecute
    end
    object CmdPaymentList: TAction
      Caption = 'Danh s'#225'ch thanh to'#225'n'
      OnExecute = CmdPaymentListExecute
    end
    object CmdPaymentBankTransfer: TAction
      Caption = 'Chuy'#7875'n kho'#7843'n'
      OnExecute = CmdPaymentBankTransferExecute
    end
  end
  object ImgLarge: TImageList
    Height = 32
    Width = 32
    Left = 12
    Top = 216
    Bitmap = {
      494C01010A000D00240020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000008000000060000000010020000000000000C0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE1DEDC00CBC7C400CAC8C200CECB
      C500CECBC500CECBC500DAD6D300DAD6D300DAD6D300DAD6D300CFCAC700CECB
      C500CECBC500CECBC500CECBC500E3E1DF00000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF80756B004E3E2E004E3E2E004E3E2E004E3E
      2E004E3E2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E
      2E004F3E2E004F3E2E004F3E2E004F3E2E007F746900000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFBEB7B2004E3E2E004E3E2E004E3E2E004E3E2E004E3E
      2E004E3E2E004E3E2E0059493A00E2E0DD00E1DEDC0058493B004F3E2E004F3E
      2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E00B9B3AD00000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9E958E004F3E2E004E3E2E004E3E2E004E3E2E004E3E
      2E004E3E2E004E3E2E0070645600000000FF000000FF75675B004F3E2E004F3E
      2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E009A918800000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB0000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E004F3E2E004E3E2E004E3E2E004E3E
      2E004E3E2E004E3E2E004E3E2E008E847A0090857B004F3E2E004F3E2E004F3E
      2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CB9E7300A252
      0200A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A2520200CCA075000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00685A4E0070645600706456006E62
      55006E6255006E6255006E6255006E6255007064560070645600706456007064
      5600706456007064560070645600685A4E004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EAD9C900A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300EAD9CA0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCBEA100A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DCBEA10000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FFEEDEC300ECDA
      BA00ECDABA00ECDABA00ECDABA00ECDABA00EBDABA00E8D6B700E8D6B700E8D6
      B700E8D6B700EADBC000000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A6550600E5CDB600E7D1BC00BF854C00A5530300A654
      0500E6CEB800E6D0BA00A6560700CD9F7200EAD7C500F1E5D800E7D0BB00C794
      6200AA5D1100E6CFB900E6CEB800A8590C00A5530300A5540400E4CCB400E0C4
      A900A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300EFE0C600BF7B0000BF7B
      0000BF7B0000BF7B0000BF7B0000BF7B0000BF7B0000BE7A0000BC790100BB79
      0100BB790100BB790100EDE1CA00DAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300B9793A00FFFFFF00FFFFFF00E8D3BF00A5530300A553
      0300F2E6DA00FFFFFF00B36D2A00DFC2A500F6EDE500EBD9C700FAF6F100FFFF
      FF00D1A77E00E3CAB100FFFFFF00E3CAB200DBBA9A00DEC0A200FFFFFF00E6CE
      B800A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FFF4EB
      DC00F1E4CF00EDDCBF00EDDCBF00EDDCBF00EDDCBF00EDDCBF00ECDCBF00EDE1
      CB00F0E7D800F1ECE000000000FFD7D3D0004E3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300D0A67D00FFFFFF00FDFBF900FFFEFE00B36E2B00A553
      0300E3CAB100FFFFFF00C1895200AD631B00A6540500A5530300BF844B00FFFF
      FF00F2E6DA00BE824800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D3AB
      8300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FFC58D2800BF7B0000BF7B0000C2861800C1821300BF7B0000BF7B0000C78E
      2900000000FF000000FF000000FFD6D2D0004E3E2E00978E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300E8D3BF00FFFFFF00D6B18C00FFFFFF00D7B39000A553
      0300D5AE8800FFFFFF00D0A57C00A5530300A5530300C48E5900F7EFE800FFFF
      FF00E8D2BE00A5540400F1E5D800FEFCFB00AD631B00D7B28E00FFFFFF00C087
      4F00A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FFE5CC
      9F00BF7B0000BF7B0000C2861800DFBE8500E5CB9E00C0821000BF7B0000BF7B
      0000E4CCA000000000FF000000FFD6D2D0004E3E2E00978C8400000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A95A0D00FDFAF800FAF6F100AB5E1300FBF8F500F9F4EF00A95A
      0D00C6926000FFFFFF00DFC1A400A5530300D5AF8A00FFFFFF00FFFFFF00E7D2
      BD00AF651E00A5530300CC9E7100FFFFFF00CD9F7300E9D6C300FFFFFF00AE64
      1C00A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9A9188004E3E2E00DAD6D300000000FF000000FFD5AB
      6000BF7B0000BF7B0000BF7B0000BE7B0200E2C59200C78C2600BF7B0000BF7B
      0000D6AB6100000000FF000000FFD6D2D0004E3E2E00978C8400000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300BE844A00FFFFFF00E9D5C200A5530300E0C4A800FFFFFF00C794
      6200B7763600FFFFFF00EDDDCD00A5530300F7EFE800FFFFFF00CB9B6D00A553
      0300A5530300A5530300AA5D1200FBF7F300EEDFD100FBF7F300F4EBE100A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9A918A004E3E2E00DAD6D300000000FF000000FFD4A9
      5C00BF7B0000BF7B0000BF800C00E6CDA100CC983A00BF7B0000BF7B0000BF7B
      0000D5AA5D00000000FF000000FFD7D3D0004E3E2E00978C8400000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300D6B18C00FFFFFF00D7B38F00A5530300BF844B00FFFFFF00EBD9
      C800A95B0F00FFFEFE00FBF8F500A6540500E3CAB200FFFFFF00E8D3BF00D4AD
      8600E3CAB100A7560800A5530300DBBA9900FFFFFF00FFFFFF00E2C7AD00A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9A918A004E3E2E00D7D3D000000000FF000000FFE1C3
      8E00BF7B0000BF7B0000C1821300E5CA9D00D5AB6000BE7B0200BF7B0000BF7B
      0000E2C59100000000FF000000FFDAD6D3004E3E2E00978C8400000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300B979
      3A00DCBC9D00FDFBF900FCF9F600BD804500A5530300A6550600F6EDE500FFFF
      FF00B6743300F4EBE100FFFFFF00B0692300AC611800E2C8AE00FEFDFC00FFFF
      FF00FDFAF800B0692300A5530300B26B2700F7EFE800FFFFFF00CFA37900A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9A918A004E3E2E00D6D2D000000000FF000000FF0000
      00FFC1841400BF7B0000BF7B0000C68B2100CA943200BF7B0000BF7B0000C285
      1700000000FF000000FF000000FFDAD6D3004E3E2E00978C8400000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A7570900AF66
      1F00A7580A00A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9A9188004E3E2E00D6D2D000000000FF000000FF0000
      00FFEEDFC400C2831200BF7B0000BF7B0000BF7B0000BF7B0000C1841400EFE1
      C800000000FF000000FF000000FFDAD6D3004F3E2E00978E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004E3E2E00D6D2D000000000FF000000FF0000
      00FF000000FF000000FFDEBD8200CFA14B00CFA14B00DEBE8400000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004E3E2E00D7D3D000000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DBBC9F00A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DBBC9F0000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCBEA100A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300DCBEA10000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EAD8C800A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300EAD9C90000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CB9E7200A252
      0200A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A5530300A5530300A5530300A5530300A5530300A5530300A5530300A553
      0300A2520200CB9E73000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00DAD6D300000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFDAD6D3004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDBCB00EBDB
      CB0000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9C928B004F3E2E00ABA39B00C2BDB700C2BDB700C2BD
      B700C2BDB700C0BCB500BFBAB400BFBAB400BFBAB400C0BCB500C2BDB700C2BD
      B700C2BDB700C2BDB700C2BDB700ABA39B004F3E2E00998E8500000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF9E958E004F3E2E004F3E2E004F3E2E004F3E2E004F3E
      2E004F3E2E004E3E2E004E3E2E004E3E2E004E3E2E004E3E2E004E3E2E004E3E
      2E004F3E2E004F3E2E004F3E2E004F3E2E004F3E2E009A918800000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FFB8B1AA004F3E2E004F3E2E004F3E2E004F3E2E004F3E
      2E004F3E2E00A8A09900000000FF000000FF000000FF000000FFA79E97004E3E
      2E004E3E2E004F3E2E004F3E2E004F3E2E004F3E2E00B7B1AA00000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF7E7165004F3E2E004F3E2E004F3E2E004F3E
      2E004F3E2E005241320064564800635648006356480063564800514132004E3E
      2E004E3E2E004E3E2E004F3E2E004F3E2E007E716500000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FFE4E3E000DAD6D300DAD6D300DAD6
      D300DAD6D300DAD6D300DAD6D300DAD6D300DAD6D300D7D3D000D6D2D000D6D2
      D000D6D2D000D7D3D000DAD6D300E4E3E000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CBAF
      9400C1997100BC8C5D00B9865500B8834F00B7824D00B8834F00B9865300BB8C
      5D00C1986F00CAAE920000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5C3B100B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00D2BFAC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BA855100B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B88451000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF956B00B57C4500BF946A000000
      00000000000000000000000000000000000000000000BA844F00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B7834F000000000000000000000000000000
      000000000000000000000000000000000000BF956B00B57C4500BF946A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BA8A5A00B3783E00B3783E00B3783E00BA88
      56000000000000000000000000000000000000000000C0946800B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00BE9267000000000000000000000000000000
      0000000000000000000000000000BA8A5A00B3783E00B3783E00B3783E00BA88
      56000000000000000000000000000000000000000000314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00000000000000000000000000000000000000
      000000000000C8CDEF005D72D500354FCB00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00354FCB005D72D500C8CDEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B3783E00B3783E00B3783E00B3783E00B378
      3E000000000000000000000000000000000000000000D0B79E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00CDB59D000000000000000000000000000000
      0000000000000000000000000000B3783E00B3783E00B3783E00B3783E00B378
      3E000000000000000000000000000000000000000000314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00000000000000000000000000000000000000
      0000C8CDEF00354DCB00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00354DCB00C8CD
      EF00000000000000000000000000000000000000000000000000000000000000
      000000000000B9C3CA004895C7001E95E3001E95E3004896C600BAC4CA000000
      0000B7C2C9004795C7001E95E3001F97E3004896C600BCC5CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B67F4900B3783E00B3783E00B3783E00B57D
      4600000000000000000000000000000000000000000000000000B3783F00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00000000000000000000000000000000000000
      0000000000000000000000000000B67F4900B3783E00B3783E00B3783E00B57D
      46000000000000000000000000000000000000000000314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00000000000000000000000000000000000000
      00005D72D500334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC005D72
      D500000000000000000000000000000000000000000000000000000000000000
      00000000000014A0E80010B9F2000FC1F1000FC1F10010BAF20015A1E7000000
      000013A1E90010BAF20010B7F20012A5F300129EF3001199EF004896C600BCC5
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7814D00B3783E00B7814C000000
      000000000000000000000000000000000000000000000000000000000000B378
      3E00B3783E00B3783E00B3783E00B3783E00B57D4600B3783E00B3783E00B378
      3E00B3783E00B3783E0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7814D00B3783E00B7814C000000
      00000000000000000000000000000000000000000000314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314ACC00314A
      CC00314ACC00314ACC00314ACC00000000000000000000000000000000000000
      0000354DCB00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00354F
      CB00000000000000000000000000000000000000000000000000000000000000
      00000000000012B1E90012A5F300129EF300129EF30011A6F20013B1E9000000
      000011BCE9000FC3F10011A8F20010BAF2000FC1F1000FC1F10010BAF20016A1
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C29B7400B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B6804A00000000000000
      0000BA885700B67F4900CBB09400000000000000000000000000CDB49B00B680
      4A00B88552000000000000000000000000000000000000000000000000000000
      00000000000000000000C29B7400B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00D3C1B00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000060AE270012A7EF0010B9F2000FC1F1000FC1F10010BAF20012A7EE0060AE
      270042B670001BC0D3000FC3F2000FC4F1000FC4F1000FC4F1000FC4F10012C1
      EA0060AE270060AE270060AE270060AE270060AE270060AE270060AE270060AE
      2700000000000000000000000000000000000000000000000000000000000000
      000000000000C7A58300B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B57E4800D0BC
      A8000000000000000000D0BBA600B9875600B3783E00B8835000CDB39A000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C7A58300B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00D1B9A3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC003F56
      CF00344DCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00344D
      CC003F56CF00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0011B8EF0012A5F300129EF300129EF30011A6F20012B8EE0071CC
      2E0071CC2E0071CC2E004BC978001DC5D5000FC4F1000FC4F1001FC5D10050C9
      6F0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      000000000000B4793E00B3783E00C5A27F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BE916400B3783E00B3783E00B3783E00B3783E00B3783E00B885
      5300000000000000000000000000000000000000000000000000000000000000
      000000000000B4793E00B3783E00C5A27F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00435AD000DADE
      F500B0B9EB00344DCC00334CCC00334CCC00334CCC00334CCC00344DCC00AEB8
      EA00DBDFF600435AD000334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000060AE270012A7EF0010B9F2000FC1F1000FC1F10010BAF20012A7EE0060AE
      270060AE270060AE270060AE270060AE27005DAE2D005DAE2D0060AE270060AE
      270060AE270060AE270060AE270060AE270060AE270060AE270060AE270060AE
      2700000000000000000000000000000000000000000000000000000000000000
      000000000000C9A98A00B3783E00B3783E00D1BEAA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CFB79E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00C7A686000000000000000000000000000000000000000000000000000000
      000000000000C9A98A00B3783E00B3783E00D1BEAA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC003D54CE00DADEF500FFFF
      FF00FFFFFF00B0B9EB00344DCC00334CCC00334CCC00344DCC00AEB8EA00FFFF
      FF00FFFFFF00DBDFF6003E55CE00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0011B8EF0012A5F300129EF300129EF30011A6F20012B8EE0071CC
      2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C1987100B3783E00B3783E00BD8F6200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BA875500B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000000000000000000000000000000000000000000000000
      00000000000000000000C1987100B3783E00B3783E00BD8F6200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00A3AEE800FFFF
      FF00FFFFFF00FFFFFF00B0B9EB00344DCC00344DCC00AEB8EA00FFFFFF00FFFF
      FF00FFFFFF00A6B1E800334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0012A7EF0010B9F2000FC1F1000FC1F10010BAF20015A8F100F1F0
      EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0
      EC00F1F0EC00F1F0EC00F1F0EC0075CD330071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CEB7A100B3783E00B3783E00B1753A00B073
      3800B2773D00B3773D00B8834D00BF906100C59D7500CBA98900D3BDA7000000
      0000B4793E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CEB7A100B3783E00B3783E00B1753A00B073
      3800B2773D00B3773D00B8834D00BF906100C59D7500CBA98900D0B69C00D5C2
      AF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00A3AE
      E800FFFFFF00FFFFFF00FFFFFF00B0B9EB00AEB8EA00FFFFFF00FFFFFF00FFFF
      FF00A6B1E800334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0011B8EF0012A5F300129EF300129EF30011A6F20014B8F200F1F0
      EC00F1F0EC00E8EDDE00A2D9770077CE380078CE3800A3DA7800E9EEE000F1F0
      EC00F1F0EC00F1F0EC00F1F0EC0091D55E0071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B3783E00B1753A00C69B7200D1AF
      8E00B2763B00AF733700B2773C00B0733700B1753A00B2763B00B2783F000000
      0000BA875500B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B3783E00B1753A00C69B7200D1AF
      8E00B2763B00AF733700B2773C00B0733700B1753A00B2763B00B0723600B378
      3E00B1753A00B3783E00B2763B00B5793F00B87F470000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00A3AEE800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A6B1
      E800334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0012A7EF0010B9F2000FC1F1000FC1F10010BAF20015A8F100F1F0
      EC00EDEEE60089D3520078CE380079CE3A0079CE3A0079CE3A008AD35400EDEF
      E700F1F0EC00F1F0EC00F1F0EC00D5E8C20075CD330071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1BCA600B3783E00AF703300BA855000F1E7
      DD00A8642300F5EEE700AE703500DBC0A400C79D7300BA855100DBC1A7000000
      0000CFB69E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00C7A686000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1BCA600B3783E00AF703300BA855000F1E7
      DD00A8642300F5EEE700AE703500DBC0A400C79D7300BA855100DBC1A700AD6E
      2F00C59A6F00B0733600B9855100B3773D00B3783E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00A3AFE800FFFFFF00FFFFFF00FFFFFF00FFFFFF00A6B1E800334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0011C3ED000FC4F1000FC4F1000FC4F1000FC4F10015C3F100F1F0
      EC00B6E0950071CC2E0084D14900C8E5AF00DAEAC90088D24F0071CC2E00B8E0
      9700F1F0EC00F1F0EC00F1F0EC00F1F0EC00D2E8BF0089D3510071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BB8A5A00AF713500DFC9B000AF713500AD70
      3200CAA27900FFFFFF00DEC4AC00AD703200AB6B2B00EDDFD200FFFFFF00C6A1
      7D0000000000BE916400B3783E00B3783E00B3783E00B3783E00B3783E00B885
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BB8A5A00AF713500DFC9B000AF713500AD70
      3200CAA27900FFFFFF00DEC4AC00AD703200AB6B2B00EDDFD200FFFFFF00BC89
      5700B1763B00B4794100FFFFFF00B3794000B3773D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00344DCC00AEB8EA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B0B9EB00344D
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E009CDFE60032CBF0000FC4F1000FC4F10033CBF000A3E1EE00F1F0
      EC0088D2510071CC2E0071CC2E00B5DF9400CDE6B70071CC2E0071CC2E0089D3
      5200F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00E9EEDF0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B2773D00AE713300FFFFFF00D3B08E00AB6A
      2A00F9F5F100FFFFFF00FFFFFF00AF723700BD8A5900FFFFFF00FFFFFF00ECDC
      CF00C4A2800000000000CFB7A000B9865500B3783E00B8834F00CBB094000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B2773D00AE713300FFFFFF00D3B08E00AB6A
      2A00F9F5F100FFFFFF00FFFFFF00AF723700BD8A5900FFFFFF00FFFFFF00ECDC
      CF00AB6A2A00E0C9B200FFFFFF00B1743900B3783E00D5C3B200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00344D
      CC00AEB8EA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B0B9
      EB00344DCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E00F1F0EC00F1F0EC00EAEEED00EAEEED00F1F0EC00F1F0EC00F1F0
      EC0074CD330071CC2E0071CC2E00B6DF9300CDE6B70071CC2E0071CC2E0075CD
      3300F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8A78700B0733800CFA88400FFFFFF00B2753B00AC6C
      2D00D4B39300FFFFFF00E8D7C500AC6B2C00AB6A2C00F6EEE800FFFFFF00C59A
      6F00AC6C2D00C59D780000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8A78700B0733800CFA88400FFFFFF00B2753B00AC6C
      2D00D4B39300FFFFFF00E8D7C500AC6B2C00AB6A2C00F6EEE800FFFFFF00C59A
      6F00AC6C2D00BC865300FFFFFF00AF723600B3783E00CEB39900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00344DCC00AEB8
      EA00FFFFFF00FFFFFF00FFFFFF00A6B1E800A3AEE800FFFFFF00FFFFFF00FFFF
      FF00B0B9EB00344DCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0
      EC0074CD330071CC2E0071CC2E00B6DF9300CDE6B70071CC2E0071CC2E0075CD
      3300F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B3783E00AF723600DEC7AF00D8BA9D00AF733600E0C9
      B200A9662500FFFFFF00B4794000D0AC8800C0906000C1926400F5EEE700A762
      1F00E5D3C100A9662500E8D6C500DABFA600B7845200C9A88700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B3783E00AF723600DEC7AF00D8BA9D00AF733600E0C9
      B200A9662500FFFFFF00B4794000D0AC8800C0906000C1926400F5EEE700A762
      1F00E5D3C100A9662500E8D6C500DABEA300AF723600C7A38000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00344DCC00AEB8EA00FFFF
      FF00FFFFFF00FFFFFF00A6B1E800334CCC00334CCC00A3AEE800FFFFFF00FFFF
      FF00FFFFFF00B0B9EB00344DCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0
      EC0088D2510071CC2E0071CC2E00B7E09700CDE6B70071CC2E0071CC2E0089D3
      5200F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      000000000000D6C5B400B3783E00AF723600B47C4300AE6F3200DEC6AE00FFFF
      FF00B67F4800B57C4400AB6D2E00FEFFFF00F3EAE200AA682700B47C4400C395
      6800FFFFFF00CFAA8700B47A4000FFFFFF00AE6F3200BF916600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C5B400B3783E00AF723600B47C4300AE6F3200DEC6AE00FFFF
      FF00B67F4800B57C4400AB6D2E00FEFFFF00F3EAE200AA682700B47C4400C395
      6800FFFFFF00CFAA8700B47A4000FFFFFF00AE6F3200BF916600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC003C54CE00D3D8F400FFFF
      FF00FFFFFF00A6B1E800334CCC00334CCC00334CCC00334CCC00A3AEE800FFFF
      FF00FFFFFF00D4D9F4003D54CE00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0077CD37009AD76A00DEEBD100F1F0EC00F1F0EC00F1F0EC00F1F0
      EC00B6DF930072CC2F0088D24F00C1E3A400C8E5B00071CC2E0071CC2E00B7E0
      9700F1F0EC00F1F0EC00F1F0EC00F1F0EC00DDEACF0099D7690077CD370071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      000000000000BF936800B0733700DABEA200B57D4500B67F4900FFFFFF00FFFF
      FF00EBDCCD00AB6A2A00D8BB9D00FFFFFF00FFFFFF00C9A07900AB6A2A00F8F4
      F100FFFFFF00FFFFFF00AD6E3200FFFFFF00BB855100B6804B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF936800B0733700DABEA200B57D4500B67F4900FFFFFF00FFFF
      FF00EBDCCD00AB6A2A00D8BB9D00FFFFFF00FFFFFF00C9A07900AB6A2A00F8F4
      F100FFFFFF00FFFFFF00AD6E3200FFFFFF00BB855100B6804B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC003F56CF00D3D8
      F400A6B1E800334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00A3AE
      E800D4D9F4003F57CF00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0071CC2E0071CC2E0079CE3B00DEEBD100F1F0EC00F1F0EC00F1F0
      EC00ECEFE60088D2510071CC2E0071CC2E0071CC2E0071CC2E0089D35200EDEE
      E600F1F0EC00F1F0EC00F1F0EC00DDEACE0079CE3A0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      000000000000B3783E00AD6E2F00F1E6DC00AC6D2F00AD6E3100FBF7F400FFFF
      FF00D5B59500AC6C2C00C1926200FFFFFF00FFFFFF00B57D4600AC6C2D00E3CD
      B800FFFFFF00EDE1D500AC6C2C00FFFFFF00C2946700B1753A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B3783E00AD6E2F00F1E6DC00AC6D2F00AD6E3100FBF7F400FFFF
      FF00D5B59500AC6C2C00C1926200FFFFFF00FFFFFF00B57D4600AC6C2D00E3CD
      B800FFFFFF00EDE1D500AC6C2C00FFFFFF00C2946700B1753A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC003B53
      CE00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC003B53CE00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0071CC2E0071CC2E0071CC2E009AD76A00F1F0EC00F1F0EC00F1F0
      EC00F1F0EC00E7EDDD00A0D9740077CD370077CD3700A1D97600E8EDDE00F1F0
      EC00F1F0EC00F1F0EC00F1F0EC0098D6670071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000CDB19600B1753A00C79C7300C5986D00C2926400BB875400C79C7200FFFF
      FF00AA672600D3B29100A8652300ECE0D200DDC3AA00AD6F3000CFAA8600B072
      3600FFFFFF00B8824B00C79E7400F9F6F100AC6C2E00B3783D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CDB19600B1753A00C79C7300C5986D00C2926400BB875400C79C7200FFFF
      FF00AA672600D3B29100A8652300ECE0D200DDC3AA00AD6F3000CFAA8600B072
      3600FFFFFF00B8824B00C79E7400F9F6F100AC6C2E00B3783D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0071CC2E0071CC2E0071CC2E0077CD3700F1F0EC00F1F0EC00F1F0
      EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0EC00F1F0
      EC00F1F0EC00F1F0EC00F1F0EC0076CD360071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000B67F4A00B2763B00C1916300A9672500F4EDE500EDE1D400A9672600C08F
      6000B57E4700FFFFFF00C9A07700B9834E00B0733800D9BB9E00FFFFFF00AD6F
      3100C08F6000AA682700FCF9F700BF8E5F00AF723600B3773D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B67F4A00B2763B00C1916300A9672500F4EDE500EDE1D400A9672600C08F
      6000B57E4700FFFFFF00C9A07700B9834E00B0733800D9BB9E00FFFFFF00AD6F
      3100C08F6000AA682700FCF9F700BF8E5F00AF723600B3773D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000354DCB00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00354F
      CB00000000000000000000000000000000000000000000000000000000000000
      000071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC2E0071CC
      2E00000000000000000000000000000000000000000000000000000000000000
      0000B3783E00B3773D00AF723600CDA68000FFFFFF00FFFFFF00C59A6F00AB6B
      2B00F0E5DB00FFFFFF00FFFFFF00AE6F3200B57D4600FFFFFF00FFFFFF00E2CB
      B600AB6B2B00D5B59500FFFFFF00AD6E3000E1C9B300B0723600D3BDA8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B3783E00B3773D00AF723600CDA68000FFFFFF00FFFFFF00C59A6F00AB6B
      2B00F0E5DB00FFFFFF00FFFFFF00AE6F3200B57D4600FFFFFF00FFFFFF00E2CB
      B600AB6B2B00D5B59500FFFFFF00AD6E3000E1C9B300B0723600D3BDA8000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005D72D500334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC005D72
      D500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C49E
      7800B3773D00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B2773C00CBAB8C000000
      000000000000000000000000000000000000000000000000000000000000C49E
      7800B3773D00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B2773C00CBAB8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C8CDEF00354DCB00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00354DCB00C8CD
      EF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B378
      3E00B3783E00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B3783E00000000000000
      000000000000000000000000000000000000000000000000000000000000B378
      3E00B3783E00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B3783E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C8CDEF005D72D500354FCB00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334CCC00334C
      CC00334CCC00334CCC00334CCC00334CCC00354FCB005D72D500C8CDEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1BDA800B378
      3E00B57D46000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1BDA800B378
      3E00B57D46000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3BCA600B8834F00B378
      3E00CAAD91000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3BCA600B8834F00B378
      3E00CAAD91000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7834F00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7834F00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE916600B3783E00C7A483000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE916600B3783E00C7A483000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B7D4E6008CC5E9006EBAE9005CB3EA006AB7E90086C2
      E900B1D2E6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007878
      7800787878007878780078787800787878007878780078787800787878007878
      7800787878000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008DC3E6005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E90087C1E6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B8D3E4005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E900ACCEE400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007878
      7800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00787878000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000076BBE7005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E9005DB2E8005EB3E9005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E90070B9E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000081A9
      4600C4D0B2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B0D0
      E5005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E900AACEE4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007878
      7800FFFFFF00D9BB9E00D9BB9E00D9BB9E00D9BB9E00D9BB9E00D9BB9E00FFFF
      FF00787878000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7D2E3005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E9001C2123001A1A1A005DB0E6005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E900AFCFE4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008CB058007EA8
      41007EA841000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BF956B00B57C4500BF946A000000
      00000000000000000000000000000000000000000000000000000000000061B4
      EA005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E90061B3E8000000000000000000000000000000
      000000000000000000000000000000000000BF956B00B57C4500BF946A000000
      0000000000000000000000000000000000008585850078787800787878007878
      7800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00787878007878780078787800878787000000000000000000000000000000
      000000000000000000000000000000000000BF956B00B57C4500BF946A000000
      000000000000000000000000000000000000000000007CBDE7005EB3E9005EB3
      E9005EB3E9005EB3E9005EB3E90033678800336788005DB2E8005EB3E9005EB3
      E9005EB3E9005EB3E90076BBE700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008FB25D007EA841007EA8
      41007EA841007EA8410000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BA8A5A00B3783E00B3783E00B3783E00BA88
      5600000000000000000000000000000000000000000000000000000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      0000000000000000000000000000BA8A5A00B3783E00B3783E00B3783E00BA88
      56000000000000000000000000000000000078787800FFFFFF00FFFFFF007878
      7800787878007878780078787800787878007878780078787800787878007878
      7800787878007878780078787800787878000000000000000000000000000000
      0000000000000000000000000000BA8A5A00B3783E00B3783E00B3783E00BA88
      56000000000000000000000000000000000000000000000000005FB3E8005EB3
      E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3E9005EB3
      E9005EB3E9005EB3E900BDD4E300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000092B361007EA841007EA841007EA8
      41007EA841007EA841007EA84100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B3783E00B3783E00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      0000000000000000000000000000B3783E00B3783E00B3783E00B3783E00B378
      3E00000000000000000000000000000000007878780078787800787878007878
      7800787878007878780078787800787878007878780078787800787878007878
      7800787878007878780078787800787878000000000000000000000000000000
      0000000000000000000000000000B3783E00B3783E00B3783E00B3783E00B378
      3E000000000000000000000000000000000000000000000000008BC3E6005EB3
      E9005EB3E9005EB3E9005EB3E9001A1A1A001A1A1A005EB3E9005EB3E9005EB3
      E9005EB3E90083C0E60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000095B566007EA841007EA841007EA841007EA8
      41007EA841007EA841007EA841007EA841000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B67F4900B3783E00B3783E00B3783E00B57D
      4600000000000000000000000000000000000000000000000000000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      0000000000000000000000000000B67F4900B3783E00B3783E00B3783E00B57D
      4600000000000000000000000000000000007878780078787800787878007878
      7800787878007878780078787800787878007878780078787800787878007878
      7800787878007878780078787800787878000000000000000000000000000000
      0000000000000000000000000000B67F4900B3783E00B3783E00B3783E00B57D
      46000000000000000000000000000000000000000000000000000000000062B4
      E8005EB3E9005EB3E9005EB3E9001A1A1A001A1A1A005EB3E9005EB3E9005EB3
      E90060B3E8000000000000000000000000000000000078787800000000007878
      7800787878007878780000000000000000007878780078787800787878000000
      0000000000000000000098B76B007EA841007EA841007EA841007EA841000000
      00008DB059007EA841007EA841007EA841007EA8410000000000000000008383
      8300787878007878780000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7814D00B3783E00B7814C000000
      0000000000000000000000000000000000000000000000000000000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      000000000000000000000000000000000000B7814D00B3783E00B7814C000000
      0000000000000000000000000000000000007878780078787800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000078787800787878000000000000000000000000000000
      000000000000000000000000000000000000B7814D00B3783E00B7814C000000
      0000000000000000000000000000000000000000000000000000000000009BC8
      E5005EB3E9005EB3E9005EB3E9001A1A1A001A1A1A005EB3E9005EB3E9005EB3
      E90092C5E500000000000000000000000000787878008DB6D0005AB3EB005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9006FB9
      E7000000000092B462007EA841007EA841007EA841007EA84100C4D0B2000000
      00000000000084AC4C007EA841007EA841007EA841007EA84100000000000000
      00006E97B2000000000000000000000000000000000000000000000000000000
      00000000000000000000C29B7400B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      00000000000000000000C29B7400B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E0000000000787878007878780000000000B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000078787800787878000000000000000000000000000000
      00000000000000000000C29B7400B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00C5A17D000000
      000068B6E8005EB3E9005EB3E9001A1A1A001A1A1A005EB3E9005EB3E90064B5
      E80000000000000000000000000000000000787878005AB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9006BB7
      E70000000000A5BE80007EA841007EA841007EA84100C1CEAD000000000075BB
      E700A6CCE4000000000082AA48007EA841007EA841007EA841007EA841000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C7A58300B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00000000005DB3
      EB005CB2E9005CB2E9005BB1E90059B1E80057B0E80056B0E80057B0E80059B0
      E8005BB1E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      000000000000C7A58300B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E0000000000787878007878780000000000B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000078787800787878000000000000000000000000000000
      000000000000C7A58300B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783F000000
      0000AACDE4005EB3E9005EB3E9001A1A1A001A1A1A005EB3E9005EB3E900A1CA
      E40000000000000000000000000000000000787878005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E90082BFE60000000000A7BF84007EA84100BECCA8000000000075BBE7005BB2
      E9005BB2E800ABCEE4000000000081A946007EA841007EA841007EA841007EA8
      4100000000000000000000000000000000000000000000000000000000000000
      000000000000B4793E00B3783E00C5A27F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005CB3
      EB0054AFE80050ADE60055AEE60073BDEB0089C6ED0095CDF0008AC8ED0075BE
      EB0056B0E60050ACE60054AEE8005BB1E8000000000000000000000000000000
      000000000000B4793E00B3783E00C5A27F000000000000000000000000000000
      000000000000000000000000000000000000858585007878780000000000B378
      3E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B3783E00B378
      3E00B3783E000000000078787800878787000000000000000000000000000000
      000000000000B4793E00B3783E00C5A27F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000072BAE7005DB2E7005EB3E9005EB3E9005DB1E6006CB8E8000000
      000000000000000000000000000000000000000000005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E90083C0E60000000000000000000000000076BBE7005BB2E9005BB2
      E9005BB2E9005BB2E800ADCEE400000000007EA842007EA841007EA841007EA8
      41007EA841000000000000000000000000000000000000000000000000000000
      000000000000C9A98A00B3783E00B3783E00D1BEAA0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000059B2
      EA00B6DCF400F5FBFD00F4FAFC00D8ECFA00C3E1F500B6DCF400C3E1F500D8EC
      FA00F5FBFC00F8FCFE00BADFF4005AB0E8000000000000000000000000000000
      000000000000C9A98A00B3783E00B3783E00D1BEAA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A2A2A200FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00A2A2
      A200000000000000000000000000000000000000000000000000000000000000
      000000000000C9A98A00B3783E00B3783E00D1BEAA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B9D3E3005EB3E9005EB3E9005EB3E9005EB3E900B0D0E3000000
      000000000000000000000000000000000000000000005BB2E9005BB2E9005BB2
      E90059B1E70055AFE70053AEE60051AEE60054AFE70059B1E7005BB2E9005BB2
      E9005BB2E90058B0E7007CBCE500AFCFE20071B9E5005AB1E9005BB2E9005BB2
      E9005BB2E9005BB2E9005CB2E800AECFE400000000007EA841007EA841007EA8
      41007EA841007EA8410000000000000000000000000000000000000000000000
      00000000000000000000C1987100B3783E00B3783E00BD8F6200000000000000
      000000000000000000000000000000000000000000000000000000000000F3FB
      FE0096CDF00055AFE80050ADE60052AEE60053AEE80054AFE80053AEE80052AE
      E60050ACE60055AFE60095CDF000ECF5FC000000000000000000000000000000
      00000000000000000000C1987100B3783E00B3783E00BD8F6200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000078787800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD007878
      7800000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C1987100B3783E00B3783E00BD8F6200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007EBEE6005EB3E9005EB3E90077BCE700000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90059B1
      E7005FB3E700A0D1F000C7E4F600CDE6F600B0DAF3005DB2E90058B0E7005BB2
      E90055AFE7006EBAEB00C2E2F500D0E7F600A0D2F0004EABE6005AB1E9005BB2
      E9005BB2E9005BB2E9005BB2E9005CB2E800AFCFE300000000007EA841007EA8
      41007EA841007EA841007EA84100000000000000000000000000000000000000
      0000000000000000000000000000CEB7A100B3783E00B3783E00B1753A00B073
      3800B2773D00B3773D00B8834D00BF906100C59D7500CBA989000000000067BA
      EC0054AFE8005BB1E8005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005BB1E90055AFE80062B4E9000000000000000000000000000000
      0000000000000000000000000000CEB7A100B3783E00B3783E00B1753A00B073
      3800B2773D00B3773D00B8834D00BF906100C59D7500CBA98900D0B69C000000
      000078787800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD007878
      7800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CEB7A100B3783E00B3783E00B1753A00B073
      3800B2773D00B3773D00B8834D00BF906100C59D7500CBA98900D0B69C00D5C2
      AF000000000000000000000000005FB3E8005EB3E800BFD5E300000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90053AE
      E600CDE7F800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF005FB3E90056B0
      E70074BEEB00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D1E9F9004EABE6005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005CB2E800B1D0E300000000007EA8
      41007EA841007EA8410000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B3783E00B1753A00C69B7200D1AF
      8E00B2763B00AF733700B2773C00B0733700B1753A00B2763B0000000000A3CB
      E5005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9009EC9E4000000000000000000000000000000
      000000000000000000000000000000000000B3783E00B1753A00C69B7200D1AF
      8E00B2763B00AF733700B2773C00B0733700B1753A00B2763B00B07236000000
      0000787878007878780078787800787878007878780078787800787878007878
      7800000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B3783E00B1753A00C69B7200D1AF
      8E00B2763B00AF733700B2773C00B0733700B1753A00B2763B00B0723600B378
      3E00B1753A00B8834F000000000098C7E50093C5E50000000000000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90054AE
      E700C1E1F500A5D4F20065B6EA006FBCEA00F3F9FD00FFFFFF00C1E1F50047A8
      E500E3F2FB00FFFFFF00ADD7F20062B4E700FDFDFE00FFFFFF0079C0ED0058B0
      E7005BB2E90054AFE70058B1E7005BB2E90057B0E70052ADE500B5D1E2000000
      00007EA841000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1BCA600B3783E00AF703300BA855000F1E7
      DD00A8642300F5EEE700AE703500DBC0A400C79D7300BA855100DDCFC1000000
      000000000000ADD0E6008CC4E80076BBE90066B5E8005DB2E80066B5E80075BB
      E8008CC4E800ACCFE60000000000000000000000000000000000000000000000
      0000000000000000000000000000D1BCA600B3783E00AF703300BA855000F1E7
      DD00A8642300F5EEE700AE703500DBC0A400C79D7300BA855100DBC1A7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1BCA600B3783E00AF703300BA855000F1E7
      DD00A8642300F5EEE700AE703500DBC0A400C79D7300BA855100DBC1A700AD6E
      2F00C59A6F00B0733600C8A78700000000000000000000000000000000000000
      000000000000000000000000000000000000000000005BB2E9005BB2E9005BB2
      E90054AEE70055AFE70059B1E70051AEE6009BD0F000FFFFFF00E2F1FB004FAB
      E600FFFFFF00FFFFFF0056AFE6004FABE600BFE0F400FFFFFF00B5DCF40054AE
      E70054AFE700C0E1F50071BCEB0054AFE70089C6EE00F2F8FC009DD0EF00B8D2
      E300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BB8A5A00AF713500DFC9B000AF713500AD70
      3200CAA27900FFFFFF00DEC4AC00AD703200AB6B2B00EDDFD200FFFFFF00BC8A
      5900C4A17E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BB8A5A00AF713500DFC9B000AF713500AD70
      3200CAA27900FFFFFF00DEC4AC00AD703200AB6B2B00EDDFD200FFFFFF00BC89
      5700B1763B00B4794100FFFFFF00B3794000B3773D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BB8A5A00AF713500DFC9B000AF713500AD70
      3200CAA27900FFFFFF00DEC4AC00AD703200AB6B2B00EDDFD200FFFFFF00BC89
      5700B1763B00B4794100FFFFFF00B47C4500B47A420000000000000000000000
      000000000000000000000000000000000000000000005BB2E9005BB2E9005BB2
      E90057B0E70051ADE60055AFE7004CAAE600ABD8F300FFFFFF00DCEDF9006EBA
      EA00FFFFFF00FFFFFF004CAAE60054AFE700A8D6F200FFFFFF00D2E9F80051AD
      E60058B0E70075BDEB00DDEEF90042A6E500FFFFFF0057AFE700FFFFFF005BB1
      E80074BBE7007A7A7A0000000000000000000000000000000000000000000000
      0000000000000000000000000000B2773D00AE713300FFFFFF00D3B08E00AB6A
      2A00F9F5F100FFFFFF00FFFFFF00AF723700BD8A5900FFFFFF00FFFFFF00ECDC
      CF00AB6A2A00E0C9B200FFFFFF00B1743900B3783E00D5C3B200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B2773D00AE713300FFFFFF00D3B08E00AB6A
      2A00F9F5F100FFFFFF00FFFFFF00AF723700BD8A5900FFFFFF00FFFFFF00ECDC
      CF00AB6A2A00E0C9B200FFFFFF00B1743900B3783E00D5C3B200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B2773D00AE713300FFFFFF00D3B08E00AB6A
      2A00F9F5F100FFFFFF00FFFFFF00AF723700BD8A5900FFFFFF00FFFFFF00ECDC
      CF00AB6A2A00E0C9B200FFFFFF00B1743900B3783E00D5C3B200000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90057B0
      E70088C6EE00DBEEF900A0D2F100AFD8F300FFFFFF00FFFFFF00ABD7F2007AC0
      EC00FFFFFF00FDFDFE004EABE60055AFE700A3D2F100FFFFFF00DCEEF90050AD
      E60058B0E7004AAAE600E0EFFB0063B4E900FFFFFF003DA4E300FFFFFF005AB1
      E7005BB2E9000000000000000000000000000000000000000000000000000000
      00000000000000000000C8A78700B0733800CFA88400FFFFFF00B2753B00AC6C
      2D00D4B39300FFFFFF00E8D7C500AC6B2C00AB6A2C00F6EEE800FFFFFF00C59A
      6F00AC6C2D00BC865300FFFFFF00AF723600B3783E00CEB39900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8A78700B0733800CFA88400FFFFFF00B2753B00AC6C
      2D00D4B39300FFFFFF00E8D7C500AC6B2C00AB6A2C00F6EEE800FFFFFF00C59A
      6F00AC6C2D00BC865300FFFFFF00AF723600B3783E00CEB39900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8A78700B0733800CFA88400FFFFFF00B2753B00AC6C
      2D00D4B39300FFFFFF00E8D7C500AC6B2C00AB6A2C00F6EEE800FFFFFF00C59A
      6F00AC6C2D00BC865300FFFFFF00AF723600B3783E00CEB39900000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90057B0
      E70083C4EE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7F3FB0050ADE6007AC1
      ED00FFFFFF00FFFFFE004DABE60055AFE700A6D5F100FFFFFF00D5ECF8004DAB
      E60071BDEB0094CCEF0069B7EA00D7EDFA009ED1F000FFFFFE00C3E2F6005BB2
      E9005BB2E9000000000000000000000000000000000000000000000000000000
      00000000000000000000B3783E00AF723600DEC7AF00D8BA9D00AF733600E0C9
      B200A9662500FFFFFF00B4794000D0AC8800C0906000C1926400F5EEE700A762
      1F00E5D3C100A9662500E8D6C500DABEA300AF723600C7A38000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B3783E00AF723600DEC7AF00D8BA9D00AF733600E0C9
      B200A9662500FFFFFF00B4794000D0AC8800C0906000C1926400F5EEE700A762
      1F00E5D3C100A9662500E8D6C500DABEA300AF723600C7A38000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B3783E00AF723600DEC7AF00D8BA9D00AF733600E0C9
      B200A9662500FFFFFF00B4794000D0AC8800C0906000C1926400F5EEE700A762
      1F00E5D3C100A9662500E8D6C500DABEA300AF723600C7A38000000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90057B0
      E70083C4EE00FFFFFF00B8DEF3008DC9EE007BC0ED004BA9E50058B0E70062B5
      E900FFFFFF00FFFFFF0050AEE50051AEE700B7DCF300FFFFFF00BDE0F5005BB2
      E700FFFFFF00C7E4F800B4DBF300D6ECFA006AB8E9004DAAE60053AEE7005BB2
      E9005BB2E9007878780000000000000000000000000000000000000000000000
      000000000000D6C5B400B3783E00AF723600B47C4300AE6F3200DEC6AE00FFFF
      FF00B67F4800B57C4400AB6D2E00FEFFFF00F3EAE200AA682700B47C4400C395
      6800FFFFFF00CFAA8700B47A4000FFFFFF00AE6F3200BF916600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C5B400B3783E00AF723600B47C4300AE6F3200DEC6AE00FFFF
      FF00B67F4800B57C4400AB6D2E00FEFFFF00F3EAE200AA682700B47C4400C395
      6800FFFFFF00CFAA8700B47A4000FFFFFF00AE6F3200BF916600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C5B400B3783E00AF723600B47C4300AE6F3200DEC6AE00FFFF
      FF00B67F4800B57C4400AB6D2E00FEFFFF00F3EAE200AA682700B47C4400C395
      6800FFFFFF00CFAA8700B47A4000FFFFFF00AE6F3200BF916600000000000000
      000000000000000000000000000000000000000000005BB2E9005BB2E90057B0
      E70083C4EE00FFFFFF00B2DAF40068B7EA006FBAEB0075BEEC005CB2E7004DAB
      E600F4FAFC00FFFFFF008AC8EE0044A6E400EFF6FC00FFFFFF0087C6ED0089C7
      EF00CEE7F80063B4E900ECF5FB0065B5E900E0F0FB004FABE6005BB2E9005BB2
      E9005BB2E9007878780000000000000000000000000000000000000000000000
      000000000000BF936800B0733700DABEA200B57D4500B67F4900FFFFFF00FFFF
      FF00EBDCCD00AB6A2A00D8BB9D00FFFFFF00FFFFFF00C9A07900AB6A2A00F8F4
      F100FFFFFF00FFFFFF00AD6E3200FFFFFF00BB855100B6804B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF936800B0733700DABEA200B57D4500B67F4900FFFFFF00FFFF
      FF00EBDCCD00AB6A2A00D8BB9D00FFFFFF00FFFFFF00C9A07900AB6A2A00F8F4
      F100FFFFFF00FFFFFF00AD6E3200FFFFFF00BB855100B6804B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BF936800B0733700DABEA200B57D4500B67F4900FFFFFF00FFFF
      FF00EBDCCD00AB6A2A00D8BB9D00FFFFFF00FFFFFF00C9A07900AB6A2A00F8F4
      F100FFFFFF00FFFFFF00AD6E3200FFFFFF00BB855100B6804B00000000000000
      000000000000000000000000000000000000000000005BB2E9005BB2E90057B0
      E70084C4EE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0072BCEB0054AE
      E70090C9EF00FFFFFF00FFFFFF00FFFFFE00FFFFFF00EFF6FD004CAAE50063B5
      E900FFFFFF00C7E5F800C0E0F50048A8E500DEEEF90078BFEC0058B0E7005BB2
      E9005BB2E9007878780000000000000000000000000000000000000000000000
      000000000000B3783E00AD6E2F00F1E6DC00AC6D2F00AD6E3100FBF7F400FFFF
      FF00D5B59500AC6C2C00C1926200FFFFFF00FFFFFF00B57D4600AC6C2D00E3CD
      B800FFFFFF00EDE1D500AC6C2C00FFFFFF00C2946700B1753A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B3783E00AD6E2F00F1E6DC00AC6D2F00AD6E3100FBF7F400FFFF
      FF00D5B59500AC6C2C00C1926200FFFFFF00FFFFFF00B57D4600AC6C2D00E3CD
      B800FFFFFF00EDE1D500AC6C2C00FFFFFF00C2946700B1753A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B3783E00AD6E2F00F1E6DC00AC6D2F00AD6E3100FBF7F400FFFF
      FF00D5B59500AC6C2C00C1926200FFFFFF00FFFFFF00B57D4600AC6C2D00E3CD
      B800FFFFFF00EDE1D500AC6C2C00FFFFFF00C2946700B1753A00000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E90058B0
      E70079BFEC00DEEFFA00D6ECF900D6ECF900D6ECF900DFF0FA0069B8EA0059B1
      E70050ADE60095CDF000EDF5FC00FAFCFD00CCE6F6005BB1E90059B1E70057B0
      E70071BAEB0097CDEF0053AEE60059B1E70069B7EA0082C3ED0058B0E7005BB2
      E9005BB2E9000000000000000000000000000000000000000000000000000000
      0000CDB19600B1753A00C79C7300C5986D00C2926400BB875400C79C7200FFFF
      FF00AA672600D3B29100A8652300ECE0D200DDC3AA00AD6F3000CFAA8600B072
      3600FFFFFF00B8824B00C79E7400F9F6F100AC6C2E00B3783D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CDB19600B1753A00C79C7300C5986D00C2926400BB875400C79C7200FFFF
      FF00AA672600D3B29100A8652300ECE0D200DDC3AA00AD6F3000CFAA8600B072
      3600FFFFFF00B8824B00C79E7400F9F6F100AC6C2E00B3783D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CDB19600B1753A00C79C7300C5986D00C2926400BB875400C79C7200FFFF
      FF00AA672600D3B29100A8652300ECE0D200DDC3AA00AD6F3000CFAA8600B072
      3600FFFFFF00B8824B00C79E7400F9F6F100AC6C2E00B3783D00000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E9005BB2
      E90058B0E70050ADE60050ADE60050ADE60050ADE60050ADE60059B1E7005BB2
      E9005BB2E90056AFE7004FADE6004EABE60051AEE60059B1E7005BB2E9005BB2
      E90058B1E70056B0E7005AB1E9005BB2E90059B1E70058B0E7005BB2E9005BB2
      E9005BB2E9000000000000000000000000000000000000000000000000000000
      0000B67F4A00B2763B00C1916300A9672500F4EDE500EDE1D400A9672600C08F
      6000B57E4700FFFFFF00C9A07700B9834E00B0733800D9BB9E00FFFFFF00AD6F
      3100C08F6000AA682700FCF9F700BF8E5F00AF723600B3773D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B67F4A00B2763B00C1916300A9672500F4EDE500EDE1D400A9672600C08F
      6000B57E4700FFFFFF00C9A07700B9834E00B0733800D9BB9E00FFFFFF00AD6F
      3100C08F6000AA682700FCF9F700BF8E5F00AF723600B3773D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B67F4A00B2763B00C1916300A9672500F4EDE500EDE1D400A9672600C08F
      6000B57E4700FFFFFF00C9A07700B9834E00B0733800D9BB9E00FFFFFF00AD6F
      3100C08F6000AA682700FCF9F700BF8E5F00AF723600B3773D00000000000000
      000000000000000000000000000000000000787878005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9007878780000000000000000000000000000000000000000000000
      0000B3783E00B3773D00AF723600CDA68000FFFFFF00FFFFFF00C59A6F00AB6B
      2B00F0E5DB00FFFFFF00FFFFFF00AE6F3200B57D4600FFFFFF00FFFFFF00E2CB
      B600AB6B2B00D5B59500FFFFFF00AD6E3000E1C9B300B0723600D3BDA8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B3783E00B3773D00AF723600CDA68000FFFFFF00FFFFFF00C59A6F00AB6B
      2B00F0E5DB00FFFFFF00FFFFFF00AE6F3200B57D4600FFFFFF00FFFFFF00E2CB
      B600AB6B2B00D5B59500FFFFFF00AD6E3000E1C9B300B0723600D3BDA8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B3783E00B3773D00AF723600CDA68000FFFFFF00FFFFFF00C59A6F00AB6B
      2B00F0E5DB00FFFFFF00FFFFFF00AE6F3200B57D4600FFFFFF00FFFFFF00E2CB
      B600AB6B2B00D5B59500FFFFFF00AD6E3000E1C9B300B0723600D3BDA8000000
      000000000000000000000000000000000000000000005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E900787878000000000000000000000000000000000000000000C49E
      7800B3773D00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B2773C00CBAB8C000000
      000000000000000000000000000000000000000000000000000000000000C49E
      7800B3773D00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B2773C00CBAB8C000000
      000000000000000000000000000000000000000000000000000000000000C49E
      7800B3773D00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B2773C00CBAB8C000000
      000000000000000000000000000000000000000000005AB3EB005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005AB1E900787878000000000000000000000000000000000000000000B378
      3E00B3783E00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B3783E00000000000000
      000000000000000000000000000000000000000000000000000000000000B378
      3E00B3783E00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B3783E00000000000000
      000000000000000000000000000000000000000000000000000000000000B378
      3E00B3783E00B3773D00B3783E00B2773C00B2763C00B2763C00B2773D00B378
      3E00B2763C00B2763C00B2763C00B2773D00B2773D00B2763C00B2763C00B277
      3C00B3783E00B2773C00B2763C00B3783E00B2773C00B3783E00000000000000
      000000000000000000000000000000000000787878006490AD005AB3EA005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2
      E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005BB2E9005AB2
      EA009CC2DA007878780000000000000000000000000000000000D1BDA800B378
      3E00B57D46000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1BDA800B378
      3E00B57D46000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1BDA800B378
      3E00B57D46000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000078787800787878007878
      7800000000000000000078787800787878007878780000000000000000007878
      7800787878007878780000000000000000007878780078787800787878000000
      0000000000007878780078787800787878000000000000000000787878007878
      78000000000000000000000000000000000000000000D3BCA600B8834F00B378
      3E00CAAD91000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3BCA600B8834F00B378
      3E00CAAD91000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3BCA600B8834F00B378
      3E00CAAD91000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7834F00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7834F00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B7834F00B3783E00B3783E00B378
      3E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE916600B3783E00C7A483000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE916600B3783E00C7A483000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE916600B3783E00C7A483000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000600000000100010000000000000600000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFF0000FF0000000000000000
      FFFFFFFFFE00007F0000000000000000FFFFFFFFFC00003F0000000000000000
      FFFFFFFFFC01803F0000000000000000F000000FFC00003F0000000000000000
      C0000003FC00003F000000000000000080000001FC7FFE3F0000000000000000
      80000001FC7FFE3F000000000000000080000001FC7FFE3F0000000000000000
      80000001FC7FFE3F000000000000000080000001FC7FFE3F0000000000000000
      80000001FC40023F000000000000000080000001FC00003F0000000000000000
      80000001FC60023F000000000000000080000001FC700E3F0000000000000000
      80000001FC60063F000000000000000080000001FC60063F0000000000000000
      80000001FC60063F000000000000000080000001FC60063F0000000000000000
      80000001FC700E3F000000000000000080000001FC700E3F0000000000000000
      80000001FC7C3E3F000000000000000080000001FC7FFE3F0000000000000000
      80000001FC7FFE3F000000000000000080000001FC7FFE3F0000000000000000
      80000001FC7FFE3F0000000000000000C0000003FC7FFE3F0000000000000000
      F000000FFC00003F0000000000000000FFFFFFFFFC00003F0000000000000000
      FFFFFFFFFC03C03F0000000000000000FFFFFFFFFE00007F0000000000000000
      FFFFFFFFFF0000FF0000000000000000FFFFE003FFFFFFFFFFFFFFFFFFFFFFFF
      FFFF8000FFFFFFFFFFFFFFFFFFFFFFFFFFFF8000FFFFFFFFFFFFFFFFFFFFFFFF
      FF1F8000FF1FFFFFFFFFFFFFFFFFFFFFFE0F8000FE0F8001F800001FFFFFFFFF
      FE0F8000FE0F8001F000000FF8103FFFFE0FC001FE0F8001F000000FF8100FFF
      FF1FE003FF1F8001F000000FF8100FFFFC0031C7FC007FFFF000000FF000000F
      F8000C1FF800001FF000000FF000000FF8FFF80FF8FFFFFFF000000FF000000F
      F87FF007F87FFFFFF000000FF000000FFC3FF007FC3FFFFFF000000FF000000F
      FE001007FE000FFFF000000FF000000FFF001007FF00007FF000000FF000000F
      FE001007FE00007FF000000FF000000FFE00080FFE00007FF000000FF000000F
      FE00041FFE00003FF000000FF000000FFC0003FFFC00003FF000000FF000000F
      FC00003FFC00003FF000000FF000000FF800003FF800003FF000000FF000000F
      F800003FF800003FF000000FF000000FF800003FF800003FF000000FF000000F
      F000003FF000003FF000000FF000000FF000003FF000003FF000000FF000000F
      F000001FF000001FF000000FFFFFFFFFE000001FE000001FF000000FFFFFFFFF
      E000003FE000003FF800001FFFFFFFFFC7FFFFFFC7FFFFFFFFFFFFFFFFFFFFFF
      87FFFFFF87FFFFFFFFFFFFFFFFFFFFFF0FFFFFFF0FFFFFFFFFFFFFFFFFFFFFFF
      1FFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFC07FFFFE007FFFF0000FFFFFFFF
      FFFFF001FFFFE007FFFF0000FFFFE7FFFFFFE000FFFFE007FFFF0000FFFFC7FF
      FF1FE000FF1F0000FF1F8001FFFF83FFFE0FE000FE0F0000FE0FC001FFFF01FF
      FE0FE000FE0F0000FE0FC003FFFE00FFFE0FE000FE0F0000FE0FE007A31C1063
      FF1FE000FF1F3FFCFF1FE00700081837FC002000FC012004FC00100F0008241F
      F8002000F8012004F800100F0004420FF8FFE000F8FF2004F8FFF81F80038107
      F87FE000F87FF00FF87FF81F80000083FC3FE000FC3FF00FFC3FFC3F00000041
      FE002000FE00100FFE000E3F00000023FF002000FF00100FFF00027F00000017
      FE001803FE001FFFFE0001FF8000000FFE0007FFFE00007FFE00007F80000003
      FE00003FFE00003FFE00003F00000007FC00003FFC00003FFC00003F00000007
      FC00003FFC00003FFC00003F00000003F800003FF800003FF800003F80000003
      F800003FF800003FF800003F80000003F800003FF800003FF800003F00000007
      F000003FF000003FF000003F00000007F000003FF000003FF000003F00000003
      F000001FF000001FF000001F80000003E000001FE000001FE000001F80000003
      E000003FE000003FE000003F00000003C7FFFFFFC7FFFFFFC7FFFFFF8C6318CF
      87FFFFFF87FFFFFF87FFFFFFFFFFFFFF0FFFFFFF0FFFFFFF0FFFFFFFFFFFFFFF
      1FFFFFFF1FFFFFFF1FFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object QrBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrBHBeforeOpen
    AfterInsert = QrBHAfterInsert
    BeforePost = QrBHBeforePost
    AfterPost = QrBHAfterPost
    OnCalcFields = QrBHCalcFields
    OnDeleteError = QrDBError
    OnEditError = QrDBError
    OnPostError = QrBHPostError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM'
      ' where'#9'KHOA =:KHOA')
    Left = 52
    Top = 228
    object QrBHCALC_TONGCK: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TONGCK'
      Calculated = True
    end
    object QrBHCALC_TIENTHOI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TIENTHOI'
      Calculated = True
    end
    object QrBHLK_TENKHO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 200
      Lookup = True
    end
    object QrBHLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrBHNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrBHQUAY: TWideStringField
      FieldName = 'QUAY'
      Size = 2
    end
    object QrBHCA: TWideStringField
      FieldName = 'CA'
      Size = 1
    end
    object QrBHSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrBHMAVIP: TWideStringField
      FieldName = 'MAVIP'
      Size = 15
    end
    object QrBHMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 2
    end
    object QrBHCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
    end
    object QrBHSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrBHSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrBHCHUATHOI: TFloatField
      FieldName = 'CHUATHOI'
    end
    object QrBHTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrBHDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrBHPRINTED: TBooleanField
      FieldName = 'PRINTED'
    end
    object QrBHCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrBHCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrBHTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      Size = 2
    end
    object QrBHCN_TENDV: TWideStringField
      DisplayLabel = 'T'#234'n '#273#417'n v'#7883
      FieldName = 'CN_TENDV'
      Size = 200
    end
    object QrBHCN_MST: TWideStringField
      FieldName = 'CN_MST'
      Size = 50
    end
    object QrBHCN_DIACHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881' giao h'#224'ng'
      FieldName = 'CN_DIACHI'
      Size = 200
    end
    object QrBHCN_DIACHI_HD: TWideStringField
      FieldName = 'CN_DIACHI_HD'
      OnChange = QrBHCN_DIACHI_HDChange
      Size = 200
    end
    object QrBHCN_LIENHE: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i li'#234'n h'#7879
      FieldName = 'CN_LIENHE'
      Size = 200
    end
    object QrBHCN_DTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'CN_DTHOAI'
      Size = 50
    end
    object QrBHCN_EMAIL: TWideStringField
      FieldName = 'CN_EMAIL'
      Size = 100
    end
    object QrBHCN_MATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'CN_MATK'
      Size = 30
    end
    object QrBHLK_TENTK: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_DAIDIEN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DAIDIEN'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'DAIDIEN'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHPRINT_NO: TIntegerField
      FieldName = 'PRINT_NO'
    end
    object QrBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrBHDELIVERY: TBooleanField
      FieldName = 'DELIVERY'
    end
    object QrBHTIENTHOI: TFloatField
      FieldName = 'TIENTHOI'
    end
    object QrBHLK_TENVIP: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVIP'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'HOTEN'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBHLK_VIP_DTHOAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_VIP_DTHOAI'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'DTHOAI'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBHLK_VIP_DCHI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_VIP_DCHI'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'DCHI'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBH_id: TLargeintField
      FieldName = '_id'
    end
    object QrBHMAQUAY: TWideStringField
      FieldName = 'MAQUAY'
    end
    object QrBHMABAN: TWideStringField
      FieldName = 'MABAN'
    end
    object QrBHSOLUONG_KHACH: TFloatField
      FieldName = 'SOLUONG_KHACH'
    end
    object QrBHCKHD_BY: TIntegerField
      FieldName = 'CKHD_BY'
    end
    object QrBHTL_CKVIPDM: TFloatField
      FieldName = 'TL_CKVIPDM'
    end
    object QrBHPTNX: TWideStringField
      FieldName = 'PTNX'
    end
    object QrBHNGAY2: TDateTimeField
      FieldName = 'NGAY2'
    end
    object QrBHSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrBHTL_CKHD: TFloatField
      FieldName = 'TL_CKHD'
    end
    object QrBHSOTIEN_SAU_CKMH: TFloatField
      FieldName = 'SOTIEN_SAU_CKMH'
    end
    object QrBHTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrBHHINHTHUC_GIA: TWideStringField
      FieldName = 'HINHTHUC_GIA'
    end
    object QrBHTHANHTIEN_CHUA_CL: TFloatField
      FieldName = 'THANHTIEN_CHUA_CL'
    end
    object QrBHTHANHTIEN_CL: TFloatField
      FieldName = 'THANHTIEN_CL'
    end
    object QrBHTHANHTIEN_CHUA_VAT: TFloatField
      FieldName = 'THANHTIEN_CHUA_VAT'
    end
    object QrBHTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrBHTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrBHTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrBHTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrBHTTOAN_CASH: TFloatField
      FieldName = 'TTOAN_CASH'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHTTOAN_CARD: TFloatField
      FieldName = 'TTOAN_CARD'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHTTOAN_TRAHANG: TFloatField
      FieldName = 'TTOAN_TRAHANG'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHTTOAN_VOUCHER: TFloatField
      FieldName = 'TTOAN_VOUCHER'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHTTOAN_EWALLET: TFloatField
      FieldName = 'TTOAN_EWALLET'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrBHDEBT_BY: TIntegerField
      FieldName = 'DEBT_BY'
    end
    object QrBHDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrBHUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrBHDEBT_DATE: TDateTimeField
      FieldName = 'DEBT_DATE'
    end
    object QrBHDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrBHMADT: TWideStringField
      FieldName = 'MADT'
      Size = 15
    end
    object QrBHSOLUONG_DAT: TFloatField
      FieldName = 'SOLUONG_DAT'
    end
    object QrBHTHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrBHNGAYVAO: TDateTimeField
      FieldName = 'NGAYVAO'
    end
    object QrBHQUAY_ORDER: TWideStringField
      FieldName = 'QUAY_ORDER'
    end
    object QrBHMAQUAY_ORDER: TWideStringField
      FieldName = 'MAQUAY_ORDER'
    end
    object QrBHTHUCTRA: TFloatField
      FieldName = 'THUCTRA'
    end
    object QrBHCO_HOADON: TBooleanField
      FieldName = 'CO_HOADON'
    end
    object QrBHORDER_BY: TIntegerField
      FieldName = 'ORDER_BY'
    end
    object QrBHORDER_DATE: TDateTimeField
      FieldName = 'ORDER_DATE'
    end
    object QrBHTL_PHUTHU: TFloatField
      FieldName = 'TL_PHUTHU'
    end
    object QrBHPHUTHU: TFloatField
      FieldName = 'PHUTHU'
    end
    object QrBHCALC_TIEN_THUE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TIEN_THUE'
      Calculated = True
    end
    object QrBHCALC_CHUA_THOI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_CHUA_THOI'
      Calculated = True
    end
    object QrBHMAVIP_HOTEN: TWideStringField
      FieldName = 'MAVIP_HOTEN'
      Size = 200
    end
    object QrBHTTOAN_BANK: TFloatField
      FieldName = 'TTOAN_BANK'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHTTOAN_MEMBER: TFloatField
      FieldName = 'TTOAN_MEMBER'
      OnChange = QrBHTTOAN_CASHChange
    end
    object QrBHCN_GHICHU: TWideStringField
      FieldName = 'CN_GHICHU'
      Size = 200
    end
  end
  object DsBH: TDataSource
    DataSet = QrBH
    Left = 52
    Top = 256
  end
  object QrCTBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrCTBHBeforeOpen
    AfterInsert = QrCTBHAfterInsert
    BeforePost = QrCTBHBeforePost
    AfterPost = QrCTBHAfterPost
    AfterDelete = QrCTBHAfterDelete
    OnCalcFields = QrCTBHCalcFields
    OnDeleteError = QrDBError
    OnEditError = QrDBError
    OnPostError = QrBHPostError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM_COMBO'
      ' where'#9' KHOA =:KHOA'
      'order by  KHOA, STT')
    Left = 84
    Top = 228
    object QrCTBHSTT: TIntegerField
      DisplayWidth = 3
      FieldName = 'STT'
    end
    object QrCTBHMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 17
      FieldName = 'MAVT'
      OnChange = QrCTBHMAVTChange
      Size = 15
    end
    object QrCTBHTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTBHTENTAT: TWideStringField
      FieldName = 'TENTAT'
      Size = 200
    end
    object QrCTBHLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 37
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTBHLK_TENVT_KHONGDAU: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT_KHONGDAU'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT_KHONGDAU'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTBHLK_TENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTAT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTBHDVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvt'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTBHSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 8
      FieldName = 'SOLUONG'
    end
    object QrCTBHDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 12
      FieldName = 'DONGIA'
      Visible = False
    end
    object QrCTBHTRA_DATE: TDateTimeField
      FieldName = 'TRA_DATE'
      Visible = False
    end
    object QrCTBHSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrCTBHGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTBHRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTBHTL_CK: TFloatField
      DisplayWidth = 10
      FieldName = 'TL_CK'
      OnChange = QrCTBHTL_CKChange
    end
    object QrCTBHTRA_BY: TIntegerField
      FieldName = 'TRA_BY'
    end
    object QrCTBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTBHKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTBHTL_CK_MAX: TFloatField
      FieldName = 'TL_CK_MAX'
      OnChange = QrCTBHTL_CK_MAXChange
    end
    object QrCTBHTL_CK_THEM: TFloatField
      FieldName = 'TL_CK_THEM'
      OnChange = QrCTBHTL_CK_MAXChange
    end
    object QrCTBHBO: TBooleanField
      FieldName = 'BO'
    end
    object QrCTBHCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
    end
    object QrCTBHSOTIEN_SAU_CKMH: TFloatField
      FieldName = 'SOTIEN_SAU_CKMH'
    end
    object QrCTBHTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrCTBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTBHTL_CKMH: TFloatField
      FieldName = 'TL_CKMH'
      OnChange = QrCTBHTL_CKMHChange
    end
    object QrCTBHTL_CKHD: TFloatField
      FieldName = 'TL_CKHD'
      OnChange = QrCTBHTL_CKMHChange
    end
    object QrCTBHTL_CK_VIPNHOM: TFloatField
      FieldName = 'TL_CK_VIPNHOM'
      OnChange = QrCTBHTL_CKMHChange
    end
    object QrCTBHTL_CK_VIPDM: TFloatField
      FieldName = 'TL_CK_VIPDM'
      OnChange = QrCTBHTL_CKMHChange
    end
    object QrCTBHTL_CK_PHIEU: TFloatField
      FieldName = 'TL_CK_PHIEU'
      OnChange = QrCTBHTL_CKMHChange
    end
    object QrCTBHCKHD_BY: TIntegerField
      FieldName = 'CKHD_BY'
    end
    object QrCTBHCALC_SOTIEN_SAUCK: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN_SAUCK'
      Calculated = True
    end
    object QrCTBHTL_PHUTHU: TFloatField
      FieldName = 'TL_PHUTHU'
    end
    object QrCTBHPHUTHU: TFloatField
      FieldName = 'PHUTHU'
    end
    object QrCTBHDONGIA_SAU_CK: TFloatField
      FieldName = 'DONGIA_SAU_CK'
    end
    object QrCTBHTHANHTIEN_CHUA_CL: TFloatField
      FieldName = 'THANHTIEN_CHUA_CL'
    end
    object QrCTBHTHANHTIEN_CL: TFloatField
      FieldName = 'THANHTIEN_CL'
    end
    object QrCTBHTHANHTIEN_CHUA_VAT: TFloatField
      FieldName = 'THANHTIEN_CHUA_VAT'
    end
    object QrCTBHDONGIA_CHUA_VAT: TFloatField
      FieldName = 'DONGIA_CHUA_VAT'
    end
    object QrCTBHLOAITHUE: TWideStringField
      FieldName = 'LOAITHUE'
      Size = 15
    end
    object QrCTBHTHUE_SUAT: TFloatField
      FieldName = 'THUE_SUAT'
    end
    object QrCTBHTIEN_THUE: TFloatField
      FieldName = 'TIEN_THUE'
    end
    object QrCTBHTIEN_THUE_5: TFloatField
      FieldName = 'TIEN_THUE_5'
    end
    object QrCTBHTIEN_THUE_10: TFloatField
      FieldName = 'TIEN_THUE_10'
    end
    object QrCTBHTIEN_THUE_OR: TFloatField
      FieldName = 'TIEN_THUE_OR'
    end
    object QrCTBHTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
  end
  object DsCTBH: TDataSource
    DataSet = QrCTBH
    Left = 84
    Top = 256
  end
  object QrTAM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrTAMBeforeOpen
    Parameters = <
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'MABAN'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM'
      'where'#9'LCT = '#39'FBLE'#39'  '
      '   and'#9'LOC = :LOC'
      '   and'#9'MABAN = :MABAN')
    Left = 236
    Top = 292
  end
  object QrINLAI: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'THANG'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MAQUAY'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'SCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'KHOA'
      '  from'#9'FB_BANLE'
      ' where'#9'LCT = '#39'FBLE'#39
      '   and  isnull(DELETE_BY, 0) = 0'
      '   and  isnull(PRINTED, 0) = 1'
      '   and'#9'month(NGAY) = :THANG'
      '   and'#9'year(NGAY) = :NAM'
      '   and'#9'MAQUAY = :MAQUAY'
      '   and'#9'SCT = :SCT')
    Left = 268
    Top = 292
  end
  object Popup: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 136
    Top = 256
    object Hthng1: TMenuItem
      Action = CmdSetPrinter
    end
  end
  object POS_VIP: TADOCommand
    CommandText = 'POS_VIP;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAVIP'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TEN'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@TLCK'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end>
    Left = 281
    Top = 250
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 132
    Top = 216
  end
  object ImageList1: TImageList
    Left = 172
    Top = 256
    Bitmap = {
      494C010102000500240010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000C000404
      4800070765000707650004044500000006000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CD96640083603F007C5F400056422B003525
      1500110B06000000000000000000000000000000000000001E001414A5002525
      E7005D5DDD005F5FD7003030E6001414A10003031A000D090400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B583E00CD977400C3A68C00D9C0A700D3B69A00CFAF
      9000C8A48100AA876100755C3D0044301A0000001D002222AC005D5DF9003535
      FC00DBDBF200D2D2E6004343FC006666F7001F1FA700A3836C00A88561008367
      49005D483000362917000F0A0500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000885539007D4D
      2C004C2E1A0028190E00B2806500EFB39700FEFEFD00FDFCFB00FBF8F400F7F1
      EC00F1E8DF00EBDED100E3D0BD00AF814A0008087D006666E8006B6AFF004A4A
      FF006262E3006969D8005555FF007172FF006565E8007771A600E9DACB00E3CF
      BD00DBC4AD00D5BA9F00CEAE8D009A703D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000052362300C9895900F7C9
      9700F0B67C00E2A26B00EFB79D00F9E9DF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F8F3EE00E6B29500D69F720008088F007E7EFC006C6CFF006262
      FF00BFBFEE00B5B5D7006D6DFF007474FF007A7AFC005F5FB900FFFFFF00FFFF
      FF00FEFDFC00FCFAF800E2C4AE00CA9563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000E070700C27E4B00D7B39900FFFD
      FA00DCC9C000AD785A00E6C1A200FCE9D900FEF1E800FEF8F300FFFDFB00FFFE
      FE00F8F4F000EABAA000A07962000000000008087D006868F7006D6DFF007F7F
      FE00E9E9F400D8D8DD008A8AFC007979FF006E6EF9006C6CC100FFFFFF00FFFF
      FF00FFFFFF00ECCEBA00E7AF94009D7655000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000492E2000EDAF7400DAC7BF00FFF6
      EC00DCB6A500BC6F45007E5A3D008A654800A6795000C38F6100DEA77700ECB9
      9000D9B69900F5D7BA008A5E3D0000000000171040003C3CD6007474FF009C9C
      F600FEFEFE00EEEEED00A0A0F0008282FF004242D900A8889600F6D3B700FADF
      C800E3CAB400EEC4A2007A503300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004F372600FADCBD00DCC8BA00FFED
      D800E8B39A00D9693D00EA865600E58D5E00CF896200B67F600095684F007F50
      3400DCAD9500FEFAF7009D7B6500000000004F3726006754A5005353E300A6A6
      F700FAFAFB00F6F6F700A4A4F2005A5AE7004D389900946C500082593F008757
      3800DAB19900FEFBF80099786100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004E403600F4EEEA00EDCDAD00FFE0
      BB00EB9C7B00F06F3F00FA815000FB855600FB7E4C00FB7B4800FB804D00F37D
      4D00EEAD9200FEF2E500906F570000000000503D3100F2EAE5008675AE004945
      C7004F50D1004F4FD100463BC0008A4B8100F27A4E00FA835000F98A5700E67D
      4E00ECAB9000FEF4EA0097755C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004D424000EEDBCB00FFD19B00FACE
      9E00D0664000E5835A00ED9C7900EFA38200EE987200E97A4A00E4703E00D561
      3400F3C9AD00FBE0C300543D2E00000000004C443F00ECDED400FED3A000FAD2
      A600B65C5400C56E6400EE967300F4A07D00F3946C00F0794700ED734000DF66
      3800F1C0A500FCE5CC00654B3A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000026211B00D8B59200FFD19900E2A1
      7800CF826000CE886700CC8E6E00D1967700CD907100C1744D00B25A2F00C47C
      5800FFDCB200D0A7820000000000000000002D242400DDBB9A00FFD09800E5A6
      7C00CD7C5A00D0886600D1917100D5997900D2927200C7754D00B95D3100C475
      5000FDDBB400D7B18D00160B0B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000032241B008B5E
      4400CB8A6B00D5947600D9967800D8927600D6907200CD886700C5896300FCCE
      9A00BB946D0014140A00000000000000000000000000140000003E2F24009C68
      4D00CF8F7000D8957A00E09B7F00DE977B00D9947400C9876500BB7D5800FBCD
      9900D5AB7F002010100000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000190F
      0A00E99C7F00E39A7A0059412D00D28C6B00BF805A005A39280039281C004031
      2200000000000000000000000000000000000000000000000000000000001C17
      0B00E0957700DE9775006E4C3500CF896A00C1805D00714C36005D4735006650
      3A000D0D0D000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003D2A
      1D00DBA18300C78D6D00714B3900F2AA8C00D293630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003E2A
      1E00DBA18300C78D6D00704A3800F2AA8C00D293630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003B2A
      1E00B7866400BB876600BC866300C2865F00D5956A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003B2A
      1E00B7866400BB886600BC866300C2866000D5956A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object QrDMVT_BO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAVT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'MAVT_CT'
      '  from'#9'FB_DM_HH_BO'
      ' where'#9'MAVT= :MAVT')
    Left = 300
    Top = 292
  end
  object POS_PAID: TADOCommand
    CommandText = 'FB_POS_PAID;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@MAQUAY'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 281
    Top = 166
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 0
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.MAVT MACHUNG, a.MAVT MAVT, a.TENVT, a.TENVT_KHONGDAU, a' +
        '.TENTAT, a.MaDvt, b.DGIAI TenDvt, a.QuyDoi, a.MaDvtLon, c.DGIAI ' +
        ' TenDvtLon, a.BO, a.MADT,  a.TINHTRANG,'
      
        #9'a.GIABAN_CHUA_VAT GIANHAP, a.GIANHAP GIANHAPVAT, a.GIABAN_CHUA_' +
        'VAT GIABAN, a.GIABAN GIABANVAT,  0 GIASI, 0 GIASIVAT, a.TL_LAI, ' +
        'a.UPDATE_DATE,'
      #9'e.TENDT'
      '  from'#9'FB_DM_HH a '
      #9#9'left join DM_KH_NCC e on e.MADT = a.MAVT_NCC'
      #9#9'left join DM_DVT b on a.MaDvt = b.MA'
      #9#9'left join DM_DVT c on a.MaDvtLon = c.MA'
      'order by a.TENVT')
    Left = 236
    Top = 340
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from V_DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 208
    Top = 424
  end
  object AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler
    Style = psOffice2007Luna
    BorderColor = 11566422
    PanelAppearanceLight.BorderColor = clNone
    PanelAppearanceLight.BorderColorHot = 10079963
    PanelAppearanceLight.BorderColorDown = 4548219
    PanelAppearanceLight.Color = 16377559
    PanelAppearanceLight.ColorTo = 16309447
    PanelAppearanceLight.ColorHot = 16515071
    PanelAppearanceLight.ColorHotTo = 12644607
    PanelAppearanceLight.ColorDown = 7845111
    PanelAppearanceLight.ColorDownTo = 4561657
    PanelAppearanceLight.ColorMirror = 16109747
    PanelAppearanceLight.ColorMirrorTo = 16244941
    PanelAppearanceLight.ColorMirrorHot = 7067903
    PanelAppearanceLight.ColorMirrorHotTo = 10544892
    PanelAppearanceLight.ColorMirrorDown = 1671928
    PanelAppearanceLight.ColorMirrorDownTo = 241407
    PanelAppearanceLight.TextColor = 6365193
    PanelAppearanceLight.TextColorHot = clBlack
    PanelAppearanceLight.TextColorDown = clBlack
    PanelAppearanceLight.TextStyle = []
    PanelAppearanceDark.BorderColor = clNone
    PanelAppearanceDark.BorderColorHot = 10079963
    PanelAppearanceDark.BorderColorDown = 4548219
    PanelAppearanceDark.Color = 16309445
    PanelAppearanceDark.ColorTo = 16103047
    PanelAppearanceDark.ColorHot = 16515071
    PanelAppearanceDark.ColorHotTo = 12644607
    PanelAppearanceDark.ColorDown = 7845111
    PanelAppearanceDark.ColorDownTo = 4561657
    PanelAppearanceDark.ColorMirror = 15382160
    PanelAppearanceDark.ColorMirrorTo = 12752244
    PanelAppearanceDark.ColorMirrorHot = 7067903
    PanelAppearanceDark.ColorMirrorHotTo = 10544892
    PanelAppearanceDark.ColorMirrorDown = 1671928
    PanelAppearanceDark.ColorMirrorDownTo = 241407
    PanelAppearanceDark.TextColor = 6365193
    PanelAppearanceDark.TextColorHot = 6365193
    PanelAppearanceDark.TextColorDown = 6365193
    PanelAppearanceDark.TextStyle = []
    Left = 56
    Top = 312
  end
  object AlFillter: TAdvAlertWindow
    AlertMessages = <
      item
        Text.Strings = (
          
            '<FONT  size="16" face="Courier New"><B><P align="center"><SHAD>A' +
            '</SHAD></P></B></FONT>                                    ')
        ImageIndex = 0
        Tag = 0
      end>
    AlwaysOnTop = False
    AutoHide = False
    AutoSize = False
    AutoDelete = False
    BorderColor = clGray
    BtnHoverColor = clSilver
    BtnHoverColorTo = clSilver
    BtnDownColor = clHighlight
    BtnDownColorTo = clHighlight
    CaptionColor = clHighlight
    CaptionColorTo = clHighlight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    GradientDirection = gdVertical
    HintNextBtn = 'Next'
    HintPrevBtn = 'Previous'
    HintCloseBtn = 'Close'
    HintDeleteBtn = 'Delete'
    HintPopupBtn = 'Popup'
    Hover = False
    MarginX = 4
    MarginY = 4
    PopupLeft = 100
    PopupTop = 100
    PopupWidth = 60
    PopupHeight = 60
    PositionFormat = 'Order notification %d of %d'
    WindowColor = clBtnFace
    WindowColorTo = clBtnFace
    ShowScrollers = False
    ShowDelete = False
    ShowPopup = False
    AlphaEnd = 0
    AlphaStart = 0
    FadeTime = 100
    DisplayTime = 1000
    FadeStep = 2
    WindowPosition = wpCenter
    Style = asTerminal
    Version = '2.1.0.1'
    Left = 152
    Top = 424
  end
  object PicBan: TGDIPPictureContainer
    Items = <
      item
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000000467414D410000B18F0BFC6105000000017352474200AECE1CE90000
          00206348524D00007A26000080840000FA00000080E8000075300000EA600000
          3A98000017709CBA513C00000006624B4744000000000000F943BB7F00000009
          7048597300000DD700000DD70142289B78000000097670416700000018000000
          1800784CA5A6000003FB4944415448C7CD935F6C145514C67FE7DE99D99DDD96
          96AE50A08040918216D4284548C01713F149453426C628F8E08B441F0413E1C1
          C43F0444C383422411082F06050C312626C63F3185A6188D44FE541A14508A81
          42BBEC4E67BBB373AF0FBB2B4B2DA80931DEE4E4DE3B99F37DE7DCEF3BF07F5A
          9D3BD19D3BD1FF2647FD63F0EDD469E57DA975E2F3CEEDD4DD70022DDE86C671
          B31634645A1769F1D6DFD0A739B095895DDBDC30CA7F65A3FCD7B66B9B3B7460
          2BE36F58075ABC57A6CC7B4239C9DB70FC76A6CC5BA1B5245EFD27B9F2773F1C
          DCCCCDCAF18ECF5F79C0171904F1B0B6816FDFEF084DA93867D10B9CBE5EBE33
          2AE8261A8176A05DE1AC987AF7E39E3869281C045C947F1F53E73FE99DE9DAF5
          E1C14DD176E0087064D18B64AFD941D7069668D7DD688D9963ADF55399E6B07E
          D26CB77ED29DFED899CBA174184C3FA0413783339781DE3DE4FABE0F737DC74B
          4317CF25452414A57AE2285ABDF025BEF993A0EB75B42875B1A5B5B561FC1D0F
          E18E5F0C5AB092031B40E91C984B205E4536032A037A02A834223EC496A8BF93
          F33F7CCCD99EDEAC3526B3702DB103B0702DF1A1D764CB60DFF9E727DE954AD9
          C26FD8A1CF2001382E880F920071AF10C417A1F823D8105B8A91E452B4F6193C
          F37BA06279B7631DF1552E32C5785D98CDEF3FB67F73DE86FD88BF182205A522
          D83CD81C98CB60B2E5DDE6CADFE318F11663872E706CCFDB4198CDEF35C578DD
          A82EEA7E19518EDE91ACAF5B3EFBE167D3CAAFC30E7783A3404BB91C016CB909
          628B781D98429E9FF6BE1784D9DC47A614AF5CF006F69A363DB406D18EDE9518
          53B7ACEDD15529B167CAE26A0125350416D44D58359513BBDF09C2C1CBFB4C29
          7EAA63E315F06BCEC18C0CDEEE67547FFB634FD7BB7E04E64219BC762C0DA0C7
          11852E473ED8917F608B69EECF1301512DD6C849D680DF9AA1418AD677958230
          0F0503851842533957EF01AED248D1FA739B6902FC4AA8D10834E00189FB6732
          4B3B5E2845034100C38069046901990C71030C0B0479A418A3DD44B87416B702
          C92A46F5759C11040EE04C4CD1964CA6AD0D72603D901436283070BA07C08E9D
          364D44374234848D7324936999942AB45526DA54C201A25A82AA1ED29490B674
          AA2E4D90C5123370E224678F9E2C6443F333C098EE9E192DED33124D53268BC8
          30E954DACF242EB581ADD5D48EEC20AE9A2FADD538B1565F3A759ABEA3BF1486
          8AA6F7939ED2E6F55D1C0364F53DB43F18F43E973E7CF2969639D39362AD4A69
          69021B5770AAF11717B940E2D3652C9F3046BF1523A7BA7F2DBDB9EA0BBEAB3C
          E15593B0F15E6E5F32CD59ED8A9D7E6A205EF3C87EF651562CAC76309A4D5585
          C8ABECBA26A486C0D6545AAAD8B350395F7F0E46085F1B55D755C14D0530AE56
          FC9FAF3F00F0E697789883ABA90000002574455874646174653A637265617465
          00323031302D30312D31315430393A31333A32332D30373A3030113A2D2A0000
          002574455874646174653A6D6F6469667900323031302D30312D31315430393A
          31333A32332D30373A30306067959600000034744558744C6963656E73650068
          7474703A2F2F6372656174697665636F6D6D6F6E732E6F72672F6C6963656E73
          65732F47504C2F322E302F6C6A06A80000001374455874536F7572636500474E
          4F4D452D436F6C6F7273AA9944E20000003174455874536F757263655F55524C
          00687474703A2F2F636F64652E676F6F676C652E636F6D2F702F676E6F6D652D
          636F6C6F72732F501DB5EB0000000049454E44AE426082}
        Name = '0'
        Tag = 0
      end
      item
        Picture.Data = {
          89504E470D0A1A0A0000000D49484452000000300000003008060000005702F9
          870000000467414D410000AFC837058AE90000001974455874536F6674776172
          650041646F626520496D616765526561647971C9653C00000D454944415478DA
          62FCFFFF3FC350060001C438D43D00104043DE03000184E20146CB75982A9818
          1918D8581818D881988D9981811588FFFE05E23F0C0C7F7E03F11F28FB8F3210
          1B00E534807C7586FFFFE580BA05412600F107A0E9CF1818196E01D93781F822
          105F05E27FE438FAFF9934381B208058280C0051207607E23086FF0C8E0C7FFE
          F1307C077AEA1FD05DCC8C10CF3302F15F20FFCF5F88382B1328107E02C54F01
          3DB40AA8772B10DF27D7010001446E0C080343BB84E1EFEF28869FBFE5187EFC
          66606463649093E061B0D41165D094E3659012E660E0E66005BBFFFB8F3F0CAF
          3EFC60B8F9F813C3890B2F186EDF7BC3F0E7CB0F88275959DE023DB201685313
          103F2235060002889C18F003BAAA1B18AA6A0CBFFE33888AF33184D8C930F859
          4A301828F33348F032E3D5FCF6FB7F86ABF73F32EC3AFE8461C5B69B0C776FBE
          14060A273370B08062B21388A790E2188000222506B88031D0C8C0C454C4F0EB
          0F93AC243743AC8B2C43A89D148386341B032B2841038D62FC0F4935E0D4030B
          3110FE0FC1FFA07220B1076FFF326C3CF08061E1BAAB0C972F3E86C42E33D30A
          A05429103F212606000288580F8803F17C20CF139494021DE5194A8215193424
          5921D9109AB45980F6B3801CC804F5049A074059E00F90FE03A47F03B3C47FA0
          022660D83C7DF78FA17BFE798615EB2E32FCFCF60364DF15A0966820BE44C803
          0001448C07448006EE01BA409F57889BA124519F21DC5684819D0912DAA06C01
          C22C4C9000648186302CFF32C01C0FC5200FFC857AE03790FEF90712067F81FA
          F79E79C3D0DCB79FE1C9BD970C0C9C6C2FC001C6C070019F070002880945E6DF
          7F74CC050CBEC5405BF4B9B9D919AA520D19026D44187EFE6260F8F10BA2E43F
          541B3C89E0C0FFD1D4C069068827BE7D6760B036146168AE766790511607E6FC
          5F1240A995402C872F0F000410AA074041828CFFFE6B62F8F5D743889F83A12C
          C384C1C64088E1F3574889F8F73F14FF836268D2F80B7518BA1C3AFE83A4E61F
          54EFA72F0C0C6A0A7C0CD5E5EE0C8A6A40F7FFFAA30674D5547C850D4000A17A
          005C41C1B137D0864256609A70083662D0D71566F8F60D12F5C896C31C8DEC09
          74FC1729EDC3F87FD1C461EC2F403B1464F8187C135D1838F9B840A1E5037459
          392E0F000410AA0710B60A0113E614C6DF7F98B46C5419EE8A88337CFC8224FD
          17918691D960CF21CBFD43645818FE831CC950F530B13F50FE776072BAC321C0
          A0E56DC5C0CE0C2E05AA80A409360F000410AE3C500C345D415E598881C9448B
          E1CE7B483A454F6160FC1FE118643190437E4131580D32FD174D3D927E502C80
          F43C00DAF94A498D41C5401128F01B18150CF5D83C001040D83C00AA58129980
          458CB8992AC3CD3F2C0CFF81063FFA0A29067F2139ECD71F2846E2FFFC0BF1EC
          4F24753FFFA0CAFFFA8BC51C281B141B2F8131F01A589A3E03D2BC467A0CEC1C
          CCA0587003BA4B17DD030001C484C5531EC0C42C29252FC0F05F458EE1FF6F48
          11790118223FFE421CF003C9913FD1D984F01FFC7C50205DFE08F1083B107F95
          9462D0315200C5021BD06D31E88E050820740F30026104B07260D0B6566478CB
          CACAC00D2CCBF98065C075A0A1373F422C00590AF2C48F3F98F82714FFF8835B
          1E450CC91C50D9F10418D3A7DF32307003ED04B54A5EFD646010D253676001D5
          94FF19A2806E144076304000B1A0391F98E0185DB8F859197ECB49317CFA0631
          089491403E3DFC9A8141989D814184035A01C32A272648DA05C5140B522506AF
          C8902A335869034BF3B06405CA335F81EC83AF207C1EA0BD3F81F29F8129E097
          B83483B0182FC3CB979F651898992D80C6ED803919208050638089C908681387
          849C00C33F417E06A05B193899219817D86AF802B4603FB0927C012A4E41A106
          34FC1B3AFE03C1DFFF609183C903E9EF300C4DFF6F8069FE20C86C60BAE703DA
          C501B5971BE8C2EF1C6C0C3A86B210850C0CC6C84E060820D4186066D464001A
          A8A52FC6F0969989810B68000728061821B1C0C604F1C489370C0C3AC08894E1
          048A032DFB0D6A46FC83C400331324B6C0ED2106CC18803525FE40EB13500CBC
          043AFE22308FBDF909092860239781F92FB4D1F71F1223D232E20C8C2C574166
          A9233B192080D03CC0AC060A7676113E70A5C401740407C8F14C10C7C33C0172
          D9CD4FC09A131882D25C104BD9A1ED216646DCADD17F48353128304131012A69
          9E7C838881421E946C98FE41DA5960BDA0A4044A461C3C0C9C5C2C0CDF7EFE97
          4276324000A17980498E05E8EAFF1C1C0CFFFF42A21184D9911C0FF60CA85FC3
          08490E0FBF40D22BC813DCAC1079702CA079E01FB435FAEB1F24AD83D2F65768
          26E6847A1E942740FA98A1691B1A56E012898D9B1398919941A58008B2930102
          08D5032C8CFCAC6017B28035831C0A723807D403EC681E00798815EA587029F2
          1FDAAC6684C6049A07FEFC47545C20410EA8C37F41C57E322235C36105009006
          B61B814995052807EE48F0203B192080503DC0CACC0C72010BD076600F11E270
          2846F60472720277711921AD2D507F8019DA85407203A48E04A9F90F51FB175A
          6AC16A71664644D26344D307F23CC8032C4C8C88B212090004107A1EF8F8076C
          D13F707260FA8314EAC81E81F2611825D46121CF88DE8887E6050644EB13E421
          988761C905DCA383AA07273B907D20357F7E3380FB2E8C0C5F918D050820540F
          B0313DFB0DD4C5FAEF37031F3B24A3B141930C727EE0408A013668C685953AFF
          195093009A1FE065373328F9FC837AFC2F92C3A1C9E63F03BCB307CE238C5F7F
          32FCFD031E85798F6C264000A127A15BA05CF5F7FB0F7089F0112DB9B023E705
          280D0B750668C823675EF4B4F01F46438B474650FE4172383889431D0F6A7F81
          931A90E60016D7CC2FBE003B3DC024C1C4FC1CD9C90001C484E6811B0C1CAC0C
          57EE7D64E0059ACC020D7DE4E4C28E8499916A5C26682CE0C38C30364C1F940D
          32978319358FB121E53141606AF8FAF22DC37F5085C0C8701BD9C9000184EA01
          16A60B4013FEDC7E046CF4FCF80E361094E958D11DCF0C753CD4518C48CD0726
          4654873221790E450F13925A4654F391F316A848E6FEFB87E1F4E947A0312490
          2BCF233B192080D09AD37FAF030BEB935FDF7D67F8F6F23503B0F9014EA3E022
          13C923D8429E11A9FC87C706B23C128DE1614684276076B042EDE505269FDFAF
          5E323C7AF81EE41850FA3F86EC64800042EB91012BF87F7F96FD07360B8F9E78
          C820C8F217EC2836E8880388666142A45726464C0731A27B8209CD1368A316E0
          EC032D8298A001050B2C105F1258EADFBD7087E1CF7750F261040D9BBC407632
          4000A1F589C103B59B8165DB87ABD75E31BCBCFF92818F1352D481628205C991
          8C8CD843132509A1A57994FC80A496910161163313A2550B6CC331FC04A6FD3D
          FBEE802A5750DE5E805EB20104105A0C80DBB58F814969D53F60D57AE4F04D06
          71CEFF600B5990DA37700B19506381113D2921399899094D2DBAA791CC83D925
          C9C7C070E6D015862FC0240D4CB72781D2C7D13D001040681EF80D1B326F029A
          F2FCCCE9C70C670F5F6110E1451AA862440D315C699A114B1E404F768CF0D080
          566448E27CDCC056EA8D070C7B77DD0046052BD0510C95A03482EE018000C235
          ACF214D89A6B02D57C2BD75D62F8FDEE1D030F17B4EC66408B7606EC8E438E01
          F490666240CB03C8D50568B40F5807F1FCFBC1B060E13186EF3FC0EDEA7940A9
          03D83AF50001842306C0B1301358CBACFCF4F62BC3F469FB1898BF7D62E0E284
          58C0F01FA517C7801490A80E45F3040372A8A355768CD00A0CD88B651064FBC5
          B068DE018687F73E800695CF42431F2B0008203C1E0036B5FEFE2D02E6A86B57
          2FBD60E8E9DDC9C0F8FD330327CC1368FE40710C036A518A92DC18B063909920
          C7F3B3FE62983B7B3FC39E5D7740BD2950B19981DE7C4006000184560FFC43C7
          CF8046873370B1BDB874F1254377EF2E863F1FDF33F0F140AA7CA2C6EF91021C
          9716500B871B98E6B9FE7F67983E7D3FC3F6EDC0CA96870DD83F638807E233F8
          CC070820D4D169D3D93806E4FF1B00C965C046BFA6A82817436E9A2583A9B53A
          C3E75FA0D10E68270414E2CC68452923EA78D9FF7F888ECD3FE850240BB072E5
          E36060B876EE3EC38C39C719EEDC7F0FCAB4EFA18EDF4C68781D208088F50088
          5400E23E86DF7F0319812E73B35360F0F6D26150D394610005D5F79FA8C52A13
          13A2550A9B1B401E95660796F15CC024F3E4FE6B869D3BAF32ACDF7E93E137A8
          ADC3CE022A2E0B80F80431131C0001448A07C03D06F040EBFFFF950CDFFFF0B0
          70B230B8DACA33383BAA3128AB4931F00A70809303A877F60F29B3FF87C60A68
          9A0D54C67F07F6271FDE7BC970FCF85D864DBBEF307C0695F39CAC7FA0A54D05
          BE348FEE01800022D503306006F6C8BFFF1EC03E2A17A8DE9791E46570B09063
          50571165E017E4017A868B81831DD20DFCF9F30FC3E7CF3F183E7DF8CCF0E0E1
          5B8603C71F31DC7DF811D86C07F59880352C33E351E8D8E73E5227F90002885C
          0FC0803E108703E5A380095A1E3CEC066C07B070B030B0011B4E4C4C4CD0B2E1
          3FB899F50B1435A00E30782A87E90D30C4416D9BE5407C88943963640F000410
          A51E80014168AC1802930D68925B063C87FC9F811B5AF87C0392EF80D1F10CDA
          9EBF086D163CA774A21B208086FC520380001AF21E0008A021EF01800003009B
          F6FEF70C63B0860000000049454E44AE426082}
        Name = '1'
        Tag = 0
      end
      item
        Picture.Data = {
          89504E470D0A1A0A0000000D49484452000000300000003008060000005702F9
          870000000467414D410000AFC837058AE90000001974455874536F6674776172
          650041646F626520496D616765526561647971C9653C00000F2F4944415478DA
          62FCFFFF3FC350060001C4C430C40140000D790F0004D090F70040000D790F00
          04100B3641469359C49BF01F4AFC671004624386FFFFB419FE316802692520CD
          CBF0FF3F2350C177A0DC03207D1D4803F1FFB340FA05C33F903E28FE079485F1
          FF82F8FF30AD7A518A210610402C54080423A0A5A10C7FFE4531FCF923C7C00C
          34128899985819585898C17EFBF3FB37C3BF3F400701693066647CC3C0C4BC0E
          A87725101F04E2BFE45A0E10409478C004E8F05A865FBFDD191898D8B9850419
          D4B49519646444182445051878F9381998999818FE009DF6EBF75F860F1FBF33
          BC7AF981E1F9B3D70C8F6FDE17F9F0FC551AD033690CCCCC47801E6A079AB78D
          1C4700041023B67A804012E200E20A86DF7FF219189905E4D51519CC4C551974
          B56418248480A10E4C30FF808EFE0BC4BFFF30803DF01B8A7F01F177A0D8DB0F
          BF191EDE7DC270EBF24D8627D7EE30FCFD0E14656659008CAD5A60A0BC202509
          010410A91E90015A3083E1D71F6F7E71510667777306672B7906511EA0838129
          E317D8C1FF10F8F77F30FD1B987C7EFD86798285E1CF7F36863F0C8C0C5F7F31
          30DCBDFD88E1E2BEA30CEF1E3C064624F365606CC403F3C27986BFC479002080
          484942CA4083D7035DA3AB6966C8101164CCA029CB0E4CDB4087FD82E4BF7FA0
          A007D22C4C8CC05860042AFD0B763CB8B863FACFF00FE8C3BFBF7E30FC05EAF9
          F50F943FB819E415E51804C22418AE9E38C570EBF0715D606ED806541C0BD4B2
          871847010410B11E9004BA7003306E74CC5DAC18E2820D19C4F920F9115688FC
          013A940998E6DF7DFAC170FACA0B86E7AF3F0393D15F60E10274F83F46064E76
          760655396106011E56A0A7801EF9F30B88BF33FCFECFC5C0CAC2CFA06565C3C0
          CAC1C970F3C011893FDF7F2C07C68607B8B42200000288180F70030D9A07B415
          E8786B869408030641766008FE824882922AC4F10C0C67AEBF623874FA3E83AE
          0C3B4386872883AE020F308F3232DC7DF68D61C7A9370C07CEDF6110161662D0
          521006262D46A8473E000BB0EF0C7F984518E4F58DC129E7E6AEBD22FFFFFE5B
          014C4E4E402B1EE3731C400011E3815260E2F6D0303300863CD4F17FA08EFFCF
          004E262C40D7EF39F394E1163063CECE57673055E7638014EC90E8D190E564F0
          B61061B8FFFC2743E9DC5B0C072F3C6530D390007AE23F24A3FFF90674F83386
          5FAC520C121AC60C3FBF7C65B87FE8880A03234B3730AB44E0731C400011AA89
          0D813614F002336C54901938D9C01D0F2ED6FF02038991E1DCAD370C976E3C61
          5859A907743CB0EE0226725076780074F0ED273F187EFC02554EFF18E4459819
          9694EA30C809FF65B870E70D38E9FDFE03CCE8204F00F3C69FCF404FFCFCC720
          A265C620A0200FB2200C184A91900C86BDD50C1040F83DF0FF7F33032313BFAB
          BB2530C3B282D33CDCF1C090FF03B4FC07B06C3C72F11943538C2283AC181BD0
          F1FF18EEBEFCC310DB7383C1BDE60A8357DD358680A6AB0CC76F7D0366212606
          56C63F0CD37334187EFEFACAF0F1CB6F6829058D895F5F187E7F79092CA13818
          244DAC1958B93919811E6F00BA838B0147B31F20809870360FFE33E833FCFCED
          2EAFAEC4E062250B2E6DFEFF87259B7F60C7FF030ADC7DF299415E9889C1C74C
          1818EA7F196E3DFFC510D87285E1ED774E06570B15066773250626360186E0B6
          6B0CFBAF7C067A82814141829DC1CD5890E1CEB34FF0C080788291E1CFF7B70C
          BFBF7E666012906710505507A52F35A0455E0CFFB08731400031E16CE0FCFF1F
          050C311623237506111E488504493690721D64E17F20FFDAC30FC0D8E1029693
          A0629C9961C1DE170C1C9CBC0C46AA2220B73230028D9217E3615094106198B3
          EB25D01DC06C070C0D43256E86CFDF7F82CDFB03763C840615087F80B1F0E327
          2303979C1AB08E6705855A06D03DCCD85C0A1040B862801FE8E2285E1141063E
          496986D79F808E638485D45FA8072041F21558890AF0308335FD0566CAFB2F7F
          3168C90B317CFFF9079C4941F83B3099890B72323C7DFB87E1DB8FBFE0FC20CA
          CF0ACC3FA066C63F683EF88788899F5F81B5F347064E7E0E061E692990C58E40
          4F6863732A4000B1E048FBC640136594349419D83859180EDFFAC3E0A8CA082C
          6DFEC32D6304C26B0F3F325CBDFB9E21C4421A1A6B8C40C7FE67387DE319033B
          2733D09DA03AE03F38D9FDFAF18741901F145E8CE014CACECACCF0EEEB6F8607
          AFBF3288F1B2036B69A8D9401AD476FAF3F91E03A7101F0397A424C3A73B8F98
          806EB2026ABB84EE548000C2E5011D605392410458FA806C3B72FB27C3AB0FFF
          19224DD980A1FC0F2C76ECDA6B86A7AFDE32B4C44833B81A09014B8FBF406126
          8692002986C7AFBE83A30C14C2C8A9929F878501D440FD06F48C16B0689D97AF
          C2B0E6D85B862397BF3088727142920F30823E7FFFC1F0F5E777066E6E4E0656
          7E3E6069CAC2F0FFCF1F1D6C4E050820EC1EF8F75F9309A88907A8F91BD02DEF
          3EFF61D8F70C1882ECFF199C3559195E7FFEC5F0E0C57B8615C5AAC0F4CD0A2C
          3D20C9E5FFFF3F0CB69AC0B69E362703BCD440A241CCAF3F7E82638497ED0F43
          B4350F43B8B510835BE34D862BB77E31F0B031031DFF9DE1C3974F601FFFFCF9
          9381999D8381899505D804F9AD88CDA90001842B0614599898819991031C5A3F
          41651CD33F86F567BE01D9EC0CEAA2FF1834643818E4455919BE7FFF0DAC8581
          16002D0439F5F34F888341490CE27646B45CC704CAEFC0086264F8F4FD17031F
          CF1F061B2D3E86C3179E0343F91730B0808EFF0FA9047F03CB6D265676A027D8
          19FE7EFE2A8ACDA90001842B06789981250AA86DF3139801FF838A1FC67FC0F4
          FC8F61CBF96F0C7744FE31582B338193053B3B33C3AA639F18A66E7A0E8C7216
          44270BD24B8394C8D0D08775DE7E7DFBCB10EC28C250E8210016606106159FDF
          19DE0243FCFFFFBF1045C09AF02FB086FB0FF400B0780319C289CDA900018423
          0618C0CD6C5006FC0F73D13F58D3E01FC38D7B3F188CE5186101CAF0F0F50F86
          271F1818CC45788149045892809AD2D092EA0FB478FC0DADB04079E8E19B9F0C
          FACF7F80F582C30B144B20C7B3FF85DB01C2FF40F81F3C093262732A4000BACB
          1D09401804A27CA299F1FE77B530A33E02934A4B0A02599605FE76A1337E3FE8
          28535B0F2686778A7B042D7E373754C5E4E8410FA63377C0851FC0CA08F9A57A
          8EDDF00B7BE7EAD93CD54817CD0A2829B0664C9DC3F1090AD3125F89BE020857
          0C3CFC0774FCB72FDF18D8F9F9195881C5E7CFFF50C7C33CC2C408570C4A02A0
          76CFEFBF9F1980453703371BB0C9C0C400A907409D1920FE012CA53E03D3FC97
          1FBF183E7D00BA855114E260465836017900DA3586DAC1088CA2BFC022F5EF0F
          90DB81FD682C00208070C5C08DBFC092E5F3874F0CBCC0BEAE101723C37360BA
          6560FA8F141BD01004616086FC096C84DD7BFE0D9C579880C1CACCF81F1C1B20
          B5A06403EAEC804213D29BF98F5C602062F33FB4BD00F4083330DD3331034BB8
          CF5F19FEFD0407FE236C0E0508205C35F1555083FFFDCBB7E0F6BC082F332474
          4051FB0FE10178010F2AF061ED067007E61FB8A5FA13D88FFC092A62FFFC8138
          FE3F5283868911D13E638479E02FBC43CDC6CE09EC72B330FCFAF011D604BE86
          CDA9000184C303FFCF00E3EFC3D3DBF780C9E807B02DC40AC970FFA18E80453D
          3CDDC25A807F91620829CF405B87703E4ACB1249FFBFFFF0E4C3C6CE0DF40B30
          F9BD7C0D6E3B01C1496C4E0508205C31F01218426B3FBE78CDF0FAC90B065E6E
          560621502CFCFD8B928410F63342DDF60F210FC3E058FB8B8AC18A9990028011
          91F6817630B3B032B00063E0F7C74F0CBF9E3C074A309F072A3D87CDA90001C4
          84A31E0019BAF8FFAFDFFF9FDCBC0B4EE34A621C9058F807F5042323AC900765
          378843FF42C753FE20E1BFE8F80F98FE8F5449A3242120E6E0E207261F56869F
          4F9E31FCFF01CA578C3341851B36A7020410AE9A18E4B823C00C70EAD98DBBE6
          D21AAA0C22C0E6B0B4303BC3E3679FA1C9022913834A243660B39795052963FE
          4765C38720817A7F80DCC4849A8941727F7F33B072F231B0F308023B36C05AFF
          0128DF32BE04CAADC6D5E702082016DCE39D0CA0FE62FD9FAFDF36DD38728ACD
          D8D79541518C9BE1E3A7EF0C9F5EFF023B1A3CE809ACB8BCF5B918946A9580A1
          C68896AE114D09B833C101FD9F41418C05DC4CE10576BAC001008C412660D2E1
          E21305370ABF5DBBC1F00FD4AC6060EE010ABCC3E5018000C2D1948095168C3B
          81A1BAF8FD8327C9F72F5C635034D40576D0F918CE7DF909EC36021B65A0740C
          749134EF6F06455D26445F08DA7C8009C0D8C88368BFFEFC00D70F0C2C3CC026
          35D0A18C7F19B8052481ED1E6E861F8F9F32FC7EF01094C28F008909F87ABD00
          0184C3034851C1C8500D2C942DEF1D3DADC5C2C6CE20A5A9CAA0A322C470FECE
          1786D2956F19EA028480DE0055569064F01FDE90FB8FA860E189E43F2282403D
          380E468619077F30ACDE076C47090933B0700B30FC7CF98AE1FBB90BA0A4064C
          AB4CB940957FF079002080B00F2DCAF6A10BE90333E42E16763631251B730631
          154586CFC0E2F5FEEB6F0C42FC8C0C3CEC90EA9491910135BD230FBFA3545C90
          50FAF9EB1FB05FFC93810B98D959D9D8187EBD7A0D76FCBF2F9F7F03934E1850
          D106D4ACD988E1568000227664EE223016A2FE7CFFB1ECF69EC362BF809D0431
          75550635091E8667C0E8BF76FF03C3BF6F3F60E5353409FE43D419F02215A918
          059632ACDC7C0C7CBC020C8CC0CCFFF3C953B0E3FFFF0255BBCC05E88EC70500
          0288D818804D3C9802FB898B81585D40419641CA449F81534000D81BFBC5F019
          58E57FFAF495E1CBD72F0CBF81B5F87F58CD0DF504E37FD0E81D23B082E20263
          16200679E2CF97AF0CDFAFDF64F875FF21482DB04DCB9404B46D3DF6C2113306
          000288B4F9014686D3C074E2067449C787BB0F22BFBC78C520A4A2C4C0272FC3
          C005F408373737B04D2400EE49FD063623C0ED79685F00D4B760024F7EB0826B
          585025F5F3C93D6051F9105ADA301D00E21220E32C294E020820D262003642F6
          175C83C60063A21C3466CAC4C1CEC02D25C9C02D2901EEC332037B72D0A61CA4
          53FF17D4390176D48149EF17B081F81BE8F19F4F9F432A290646D0B8FA6420A3
          9F5086C516030001448907404919D860F9E70D144B017AC41E58A0B331027BED
          4CACACE06E20B02284E469607BFE1FA82F0CEAB4FC06D7C4A0A6ED79606CCE03
          CAAE01AA794DD4741C160F000410A573645F817815D021AB1858580C803658FC
          FFF3571BD8145704F661C5C0DDC0FFE032F51730585E01D5820AF71B408F9D04
          8A9FC5D5492105000410E3505F6A001040437E9E18208086BC07000268C87B00
          208086BC0700020C008F7B8B35DFBAE1CC0000000049454E44AE426082}
        Name = '2'
        Tag = 0
      end>
    Version = '1.0.0.0'
    Left = 152
    Top = 312
  end
  object Image48: TImageList
    Height = 48
    Width = 48
    Left = 152
    Top = 372
    Bitmap = {
      494C01010B000D00240030003000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000C000000090000000010020000000000000B0
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E3E3E300DFDFDF00E1E1
      E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0
      E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0
      E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0
      E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0
      E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0E100E1E0
      E100E1E0E100E0E0E000E1E1E100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B2B0B200C7C7C600CCCC
      C800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCC
      C800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCC
      C800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCC
      C800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCC
      C800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCCC800CBCC
      C800CBCCC800CCCDCA00B6B6B600D3D3D3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B7B6B600CDD7B800A9C1
      7800A5BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF
      7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF
      7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF
      7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF
      7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF7200A6BF
      7200A5BF7200C4D4A300C4C5C000CDCDCD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C58A006896
      0D00639404006394040063940400639404006394040063940400639404006394
      0400639404006394040063940400639404006394040063940400639404006394
      0400639404006394040063940400639404006394040063940400639404006394
      0400639404006394040063940400639404006394040063940400639404006394
      0400639404006394040063940400639404006394040063940400639404006394
      0400629305009AB75E00C3C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006796
      0900639300006393010063930000639300006393000063930100639300006393
      0100639300006393000063930000639301006393000063930000639300006393
      0000639301006393000063930000639301006394010063940000629400006193
      0000619200006092000061920000619300006193000062940000639401006393
      0100639300006393010063930000639300006393000063930000639400006394
      01006292010099B85C00C3C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006896
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669400006694000064930000629402006B99
      11007BA32C0089AC43008BAD47007FA533006D99150064930200659300006694
      0100669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      00006693000066930000669300006593000065930100749D1C0098B55A00C4D5
      A100E3EBCF00F0F3E500F1F4E800E7EED800C8D7A70095B354006E9812006592
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      000066930000649300006393000068960A008EAE4800C8D6A600F1F4E800FEFD
      FD00FFFFFF00FFFEFF00FFFEFF00FFFFFF00FEFEFE00EDF1E100B3C88500759E
      1F00659201006693010066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      00006593000061930000709C1A00A8C27400E7EDD700FDFCFC00FFFFFF00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FFFFFF00F6F8F100BFD1
      9800759D1D006593010066940000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000659300006493
      0000649301007BA32D00C0D29B00F5F7EE00FFFFFF00FEFEFF00FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFD00FEFEFE00FFFEFE00FEFDFD00E2EA
      CE008CAD44006795080066930100669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006699660033996600669966000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000649300006394
      040084A93A00CFDCB200FAFBF600FFFEFF00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFD00FFFFFE00F6F8F000CDDAAF009CB8
      63009EBA65008EB04A0066940400669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000066CC660033CC660033CC66003399
      660066CC9900C0DCC00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      00006693000066930000669300006693000066930000659300006493020085AA
      3D00D5E0BD00FDFDFB00FEFEFE00FEFDFE00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FFFFFF00FDFDFB00E5EBD300ADC37E0094B25800BDCF
      9800EDF1E200B7CC8C0069960A00659300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990066CC9900339966003399660033CC660033CC660033CC660033CC660033CC
      6600339966003399660066CC9900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      000066930000669300006693000066930000649301006394020086AA3D00D5E1
      BC00FDFDFC00FFFEFF00FEFEFE00FEFEFE00FEFDFF00FFFEFF00FFFFFF00FFFE
      FF00FEFDFF00FFFEFE00EFF3E500C5D6A2009AB75F00A7BF7500DDE6C800FCFC
      FA00FFFEFF00C0D19A006C971000659200006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B0B0CC007777A4003F3F7E001616
      690021216E0042428000666699008B8BB200AFAFCA00CFCFDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000066CC99003399660033CC
      660033CC660033CC660033CC660033CC660033CC660033CC660033CC660033CC
      660033CC660033CC660033CC660066CC66000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      000066930000669300006693000065930000629301007FA63400D3DFBB00FCFC
      FA00FEFDFE00FEFEFE00FEFEFE00FEFDFE00FAFAF700ECF1E000E8EEDA00F4F6
      EE00F8F9F400D7E2C000A2BC6B009EBA6700CDDBAF00F4F6EE00FEFEFF00F8F9
      F400D3DFB8008FB04C0066930600669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CFCF
      DF009B9BBD0063639700303076000C0C6A0000197A00013F9800016FBB000093
      D8000080CF000060B70000439F00002A8A00001578000207690014146A002F2F
      77007C7CAB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C0DCC00033CC660033CC660033CC660033CC
      660033CC660033CC660033CC660033CC660033CC660033CC660033CC660033CC
      660033CC660033CC660033CC660033996600C0DCC00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      000066930000669300006693000064910000759D2200C7D6A700FCFCFB00FEFE
      FF00FEFDFE00FEFDFE00FFFEFF00F2F4E900B9CC8E008CAD450085A939009EB9
      6300ACC4790096B55A00B5CB8A00E8EEDB00FFFEFE00FEFEFD00E8EEDA00B1C7
      820079A228006594040065930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000068689F000007
      6800032787000650A8000A7ECC000BAAE9000BCBFD0008DDFF0004E1FF0001DD
      FF0000DEFF0000DDFF0000D8FF0000CDFF0000BBFB0000A6EE000090DD00005D
      BA00010161000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099CCCC0066CC990033CC990033CC660033CC660033CC
      660033CC660033CC660033CC660033CC660033CC660033CC660033CC660033CC
      660033CC660033CC990033CC660099CC99000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      00006693000066930000659201006C961000B2C78400F8F9F300FFFEFF00FEFE
      FE00FEFEFE00FEFEFE00F6F7F000B0C580006E97120064920000649200006592
      0200749D1D00BCCF9300F8F9F300FFFFFF00F6F8EE00CAD9AA008CAF47006896
      0B00649200006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000011116200107B
      C4001BD5FF0018D9FF0013DAFF000ED5FF0008CDFF0003C4FF0001BEFF0000BB
      FF0000BEFF0000C1FF0000C4FF0000C8FF0000CBFF0000CDFF0000D5FF0000B5
      FD0000096F005A5A8E008D8DB300A1A1C000BEBED50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099FFCC0033CC990033CC990033CC990033CC660033CC990033CC
      990033CC660033CC990033CC660033CC990033CC660033CC660033CC660033CC
      990033CC990033CC990066CC99000000000000000000000000003399660066CC
      9900000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      000066930000669300006693030097B35600EBF0DE00FFFEFF00FEFDFE00FEFE
      FE00FEFEFE00FEFEFD00CFDDB30077A023006492000066930100669300006693
      00006996080087AB3F00C3D49F00D9E4C000A9C27300749F1F00639401006293
      0000659300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000020206B00168A
      CB001BD9FF0007C8FF0002C5FF0000C3FF0000C3FF0000C3FF0000C4FF0000C4
      FF0000C4FF0000C4FF0000C4FF0000C4FF0000C4FF0000C5FF0000CAFF0000C3
      FF00005BB80000379400002B8C00001F820000066600B2B2CD00000000000000
      000000000000000000000000000000000000DBDBE70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000066CC990033CC990033CC990033CC990033CC990033CC990033CC
      990033CC990033CC990066CC990066CC990066CC990033CC990033CC990033CC
      990033CC990066CC99000000000000000000000000003399660033CC66003399
      6600C0DCC0000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      0000669300006492000077A02500D4DFBA00FEFDFE00FEFEFE00FEFEFE00FEFE
      FE00FFFFFF00EEF2E50097B65A00649303006693000066930000669300006693
      00006593000064920000729B17007CA227006996090064920000659300006593
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001F1F69001781
      C4001BD7FF0000CEFF0000CFFF0000D0FF0000D0FF0000CFFF0000CFFF0000D0
      FF0000D0FF0000D0FF0000D0FF0000D0FF0000D0FF0000D0FF0000CFFF0000CA
      FF0000CDFF0000C8FF0000C1FF0000BCFF00002C900019195F00434382004E4E
      88005A5A9100ACACC900000000008B8BB2000A0A6300131368008F8FB6000000
      00000000000000000000000000009696B9004D4D8800AFAFCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000066CCCC0033CC990033CC990033CC990033CC990033CC990033CC990033CC
      990099FFCC000000000000000000000000000000000066CC990033CC990033CC
      990066CC990000000000000000000000000033CC660033CC660033CC660033CC
      660066CC99000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      00006693000068950900ACC47B00F8FAF400FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FDFDFB00C2D39D006D9A1600639200006693000066930000669300006693
      0000669300006693000065920000659200006593000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000024246E001A7D
      C2001ED9FF0000D9FF0009D7FF000CD8FF0005D6FF0000DAFF0000DAFF0000D9
      FF0000D9FF0000DAFF0000DAFF0000DAFF0000DAFF0000DAFF0000DAFF0000D8
      FF0000D7FF0000D6FF0000D1FF0000C4FF00008BDF000068BE00005EB8000056
      B2000041A1000101620031317200001A7700007CD5000060C10000106E003A3A
      7C00A6A6C40069699B00151567000020820000389D0004046000A6A6C5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000066CC990033CC990033CC990033CC990033CC990033CC990033CC990099FF
      CC00000000000000000000000000000000000000000066CCCC0033CC990066CC
      99000000000000000000000000006699990033CC660033CC660033CC660033CC
      660033CC99000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006593
      00006392000082A63600DFE7CC00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00E7EDD80089AC410063930100659300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000002B2B71001B77
      BE0025D8FF0014DCFF000FD8FE000ECBF70022D8FF0010D7FF0001E2FF0000E4
      FF0000E3FF0000E3FF0000E4FF0000E4FF0000E4FF0000E4FF0000E4FF0000E5
      FF0000E5FF0000E6FF0000E2FF0000C9FF0000CBFF0000CAFF0000C7FF0000C2
      FF0000BBFF000062C00000439B0000A1F50000B5FF0000ACFF00008CEB000039
      9A00000F6A00003998000071D6000092FF000097FF00004FB50003035D00A6A6
      C500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099FFCC0099CCCC0066CC990066CC990033CC990033CC990099FFCC000000
      00000000000000000000000000000000000000000000C0DCC00099CCCC000000
      000000000000000000000000000066CC990033CC660033CC660033CC660033CC
      660033CC990099CCCC0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659300006293
      000067960A00B0C78100FAFAF500FFFEFF00FEFEFE00FEFEFE00FEFEFE00F9FA
      F400B3C985006B960B0065930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000002A2A6F001C75
      BD002DD9FF000DADE5000339920000136E001D4A9A004CCDF8000CDDFF0000EF
      FF0000EDFF0000EDFF0000EDFF0000EEFF0000EEFF0000EFFF0000F3FF0000F2
      FF0000F2FF0000F2FF0000F2FF0000F1FF0000EFFF0000EDFF0000EBFF0000EA
      FF0000CCFF0000B6FF0000B7FF0000C9FF0000EAFF0000D7FF0000B3FF0000A1
      FF000092F30000A3FF0000B1FF0000CAFF0000ADFF000093FF00004FB7000101
      5B00A3A3C3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099CCCC0033CC660033CC660033CC660033CC
      990033CC990066CC990000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659300006192
      00007CA32E00DDE6C700FFFFFF00FEFDFE00FEFEFE00FEFEFE00FFFEFF00DCE6
      C7007FA530006492000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000323274001E71
      B90026CAFE00033B920040407D00B8B8D0003B3B77005473B10033E1FF0000F3
      FF0000F8FF0000F7FF0000F8FF0000F8FF0000F9FF0000F4FB0000DBF90000DF
      FF0000E0FF0000E1FF0000E2FF0000E3FF0000E4FF0000E6FF0000E9FF0000EC
      FF0000E8FF0000C1FF0000CCFF0000EDFF0000EEFF0000F1FF0000E4FF0000BB
      FF0000B6FF0000D4FF0000EDFF0000F7FF0000E9FF0000B1FF0000A4FF000050
      B900020460009F9FC10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033CC990033CC660033CC990033CC
      660033CC990066CC990000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659300006394
      0300A0BC6700F5F8F000FFFEFF00FEFEFD00FEFDFE00FFFEFF00F7F8F100A8C2
      7500669508006593000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000393979002171
      BA0022B7F50000187200ABABC700000000009797BA0036458F0049E0FF0003F5
      FF0005FFFF0006FFFF0006FFFF0006FFFF0006FFFF0006F1F4000195D800009C
      EA00009AE800009CE90000A1EC0000A0EB0000A0E90000A4EC0000A6ED0000A5
      EB0000A8EC0000AEEF0000AEED0000ADED0000B1F00000B3F00000B5EF0000BA
      F10000BEF30000BFF20000BEF20000BFF40000C5F40000CDF30000CAF50009B7
      FF001352AC0007075B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033CC990033CC990033CC990033CC
      990033CC990066CC990000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659300006D9A
      1400CAD9AA00FEFEFE00FEFEFE00FEFEFE00FEFDFE00FFFDFE00D4E0BA0078A2
      2700619200006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000373779002068
      B3002DC5FC000236880044448100B1B1CB00303070005A7BB50038DFFF001AFB
      FF001DFFFF001EFFFF001EFFFF001EFFFF001FFFFF001FFEFF001DF1F50020F1
      F30021EEF00021EDF00022EEF10022EBEF0021E8ED0022EAEF0022EAEF0022E6
      EC0022E7ED0022E8EE0022E4EC0021E2EA0022E4EC0022E3EB0021DEE80021DF
      E90022E1EA0021DDE70021DBE60022DFE9001FD8E80002A2E50014A7EC003E9C
      CE00132274006E6EA10000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC660033CC660033CC660033CC660033CC660033CC660033CC66003399
      660066CC99000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099FFCC0033CC990033CC990033CC990033CC
      990033CC990066CC990000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000066930000669300006493000081A6
      3700E8EEDA00FFFFFF00FEFEFE00FEFEFE00FEFEFF00F2F5EA009BB85F006494
      0400649301006693010066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042427F002164
      AF003CD3FF001B97D9000431840006146A0036579E0057CDFA0028E2FF0038FF
      FF0038FFFF0038FFFF0038FFFF0039FFFF0039FFFF0039FFFF003AFFFF002CF7
      FF0022F3FF001DF5FF001DF5FF001DF5FF001DF5FF001DF3FF001CF1FF001AEF
      FF001BEDFF001BECFF001BEBFF001CEAFF001CE9FF001DE8FF001DE8FF001DE6
      FF001BE2FF001BE0FF001BDFFF001CDCFF000EC5FF0027BBFF004280BE000B08
      5F008282AC000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC660033CC660033CC660033CC660033CC660033CC660033CC660066CC
      990099CCCC000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099CCCC0033CC990033CC990033CC990033CC
      990033CC990066CCCC0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659301009EBA
      6300F7F9F200FEFEFE00FEFEFE00FEFEFE00FDFDFC00C9D9AA00729B19006593
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000046468300205A
      A90041D3FF0040D8FF002BBCF9002EB5F2003ED0FF0036DEFF004FFDFF0052FF
      FF0052FFFF0052FFFF0052FFFF0052FFFF0053FFFF0054FFFF0055FFFF0016DB
      FF0019D5FF003EDFFF003ADFFF003CDAFC003CD7FC003CD5FC003BD4FC003BD2
      FC003CD0FC003CCEFC003CCCFC003CCAFC003DC8FB0040C2F8003FC1F8003FBF
      F8003FBDF8003FBBF8003FBAF80040B9F90049BAF3003A5FA000020156009595
      BA00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC660033CC660033CC660033CC660033CC660033CC660033CC990099CC
      CC00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000033CC990033CC990033CC990033CC990033CC
      990033CC990099CCCC0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      090066930000669300006693000066930000669300006693000069950800B9CD
      8D00FDFDFB00FEFEFE00FEFEFE00FFFFFF00EBF1DF0091B25100639201006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000464683002259
      A80046D3FF0066FCFF0062F4FF0059EEFF005EF3FF006BFFFF006CFFFF006BFF
      FF006CFFFF006CFFFF006CFFFF006DFFFF006DFFFF006EFFFF006FFFFF001FE2
      FF001AAAEA00282D7E00252C7A00222677002125770021257700212577002225
      770022257700222677002226770022267700202576001C1F72001D1F72001D1F
      72001D1F72001D1F72001D2072001E21720017186D000C0C6000A9A9C7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC660033CC990033CC990033CC99000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990099CCCC0099FFCC0066CCCC0033CC990033CC990033CC990033CC990033CC
      990066CC99000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693010065920000729A1800CFDC
      B100FFFEFE00FEFEFE00FEFEFF00FDFDFD00C2D39F006C991400639200006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000515189001F51
      A30046CFFF007CF9FF008DFFFF008CFFFF0089FFFF0085FFFF0085FFFF0085FF
      FF0085FFFF0086FFFF0086FFFF0087FFFF0087FFFF0089FFFF008AFFFF0027E4
      FF000F92DB000E0E60009494B8009E9EBF00A1A1C100AAAAC700A9A9C600A9A9
      C600A9A9C600A9A9C600A9A9C600A9A9C600A9A9C600A9A9C600B0B0CC00B5B5
      CE00B5B5CE00B5B5CE00B5B5CE00B5B5CE00B5B5CE00D6D6E500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC660033CC660033CC660033CC990066CC
      CC000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC990033CC990033CC990033CC990033CC
      990066CCCC000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      090066930000669300006693000066930000669300006492000080A42D00E1E9
      CD00FFFFFF00FEFEFE00FFFFFF00F8FAF400A6BF710062930300639400006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004C4C85003459
      A20070E2FF005BD8FF005FDDFF0072E8FF008CF6FF00A0FFFF00A8FFFF00A7FF
      FF00A3FFFF00A4FFFF00A8FFFF00AAFFFF00AAFFFF00A5FFFF0094FBFF0021E1
      FF000E87CF002F2F750000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC990033CC990033CC990033CC990033CC
      99000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC990033CC990000CC990033CC990033CC
      9900000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000065930100629200008BAD4100ECF1
      DE00FFFFFF00FEFEFE00FFFFFF00F9FAF500A8C2740062940300639300006693
      0000669300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066940000669300006694
      0000649400006494000064940000649301006593010066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009393B6001517
      6600546298007197C3007DBFE90073D5FF0061DAFF005ADBFF0068E1FF0089EC
      FF00A9F9FF00A2F6FF0087EEFF006DE8FF0056E4FF0049E4FF0045E4FF0050F0
      FF003A85BE0045457E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000066CC
      990033CC990033CC990033CC990033CC990033CC990033CC990033CC990033CC
      990033CC99000000000000000000000000000000000000000000000000000000
      000033CC990000CC990033CC990000CC990033CC990033CC990033CC990033CC
      9900000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      090066930000669300006693000066930000649301006192000092B35200F0F4
      E800FFFFFF00FEFEFE00FEFEFE00FEFEFE00CEDCB00076A02300629200006592
      0000659300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669301006593
      0000619300006093000060920000619300006394010066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B8B8
      CD005B5B8A0022226100120D570037387600616F9F007BA4CC007DC9EF006BDA
      FF005BDEFF005DDFFF0066DCFF0070D3F80076C0E50074A5CB006781AD00565B
      8F00202067007878A10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000099CC
      CC0066CCCC0099FFCC0066CCCC0033CC990033CC990033CC990033CC990033CC
      990033CC990033CC990066CC990066CCCC000000000000000000000000000000
      000033CC990033CC990000CC990033CC990033CC990033CC990033CC990033CC
      990033CC990099CCCC0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000064930000619300009BB96000F4F7
      EE00FFFFFF00FEFEFE00FEFEFE00FEFEFE00F6F8F000BED198007AA127006F99
      10006D970E006592000065930000669300006693000066930000669300006693
      000066930000669300006693000066930000669300006693000066930100719B
      17008EB0490091B3510091B350007EA531006494040065930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CCCCDB008F8FAD0051517D001E1E5A001A1357004443
      7A006774A1005D669600423F7700261F60001D1558002C2C6500545481008282
      A300B2B2C7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000066CC990033CC990033CC990033CC990033CC
      990033CC990033CC990033CC990033CC990033CC990066CC9900000000000000
      000033CC990033CC990033CC990033CC990000CC990033CC990033CC990033CC
      990066CCCC000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006493000062930200A1BD6A00F7F9
      F200FFFFFF00FEFEFE00FEFEFE00FEFEFE00FFFEFE00F6F8F000A3BE6B009AB7
      5B00BACD8E008BAC44006A970D00649300006693000066930000669300006693
      00006693000066930000669300006693000066930000669300006592000089AB
      3D00E4EBD200F0F5E800EFF4E600B6CB8B0067950A0065930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B3B3C6006A6A
      8F00252557003838670074749700AAAABF00D8D8E20000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099FFCC0066CC990033CC990033CC990033CC
      990033CC990000CC990000CC990033CC990033CC990033CC9900000000000000
      000066FFCC0033CC990033CC990033CC990033CC990033CC990066CC990099FF
      CC00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006493000063930300A6C07000F8FA
      F400FFFFFF00FEFEFE00FEFEFE00FEFEFE00FEFFFD00FAFCF800A9C17700ABC3
      7A00F9FAF500E3EAD3008BAE4500629200006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000649200008DAD
      4200F2F5E800FFFFFF00FFFFFF00BFD2990068960A0065930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099FFCC0066CC990033CC990033CC
      990033CC990000CC990000CC990033CC990033CC990033CC9900000000000000
      00000000000033CC990033CC990033CC990033CC990066CCCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006494000062930200A0BD6700F6F9
      F100FFFFFF00FEFEFE00FEFEFE00FEFEFE00FEFFFD00FAFBF700A9C17500ABC3
      7900FBFCF900F3F5EC0095B55600629200006693000066930000669300006693
      0000669300006693000066930000659301006694040066950400649304008CAD
      4500F1F4E600FFFEFF00FDFEFD00BFD19A0069970D0065940400659404006494
      02006293010098B85B00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099FFCC0066CCCC0033CC
      990033CC990033CC990033CC990033CC990033CC990033CC9900000000000000
      00000000000066CC990066CC990066CCCC0099FFCC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      090066930000669300006693000066930000649400006192000092B35000F0F4
      E700FFFFFF00FEFEFE00FEFEFE00FEFEFE00FEFFFD00FAFBF700A9C27500ABC3
      7900FBFCF900F2F5EB0095B55600629200006693000066930000669300006693
      00006693000066930000649300006F9B1700A2BE6700B2CA8100B0C87F00C4D5
      A000F7F9F200FEFEFE00FEFEFD00DEE7CA00B3CA8400B1C98000ADC67C0082A9
      38006393020099B85B00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000099FF
      CC0099CCCC0066CC990033CC990033CC990033CC990033CC9900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000065940000619200007EA52F00E0E8
      CE00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FEFFFD00FAFBF700A9C27500ABC3
      7900FBFCF900F2F5EB0095B55600629200006693000066930000669300006693
      00006693000066930000639200007AA12900DCE6C700FCFDFA00FCFDF900FCFD
      FB00FEFEFE00FEFEFE00FEFEFD00FDFDFB00FCFCF900FCFDFA00F5F8F000A2BF
      6D006292030099B75B00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099FFCC0066FFCC0066CCCC0099FFCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000066930000649300006C970F00BDCF
      9500FDFCFB00FEFEFE00FEFEFE00FEFEFE00FEFFFD00FAFBF700A9C27500ABC3
      7900FBFCF900F2F5EB0095B55600629200006693000066930000669300006693
      00006693000066930000639200007AA22900DFE8CA00FFFFFF00FEFFFF00FEFE
      FF00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FFFEFF00F8FAF500A3BF
      70006292030099B75C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      09006693000066930000669300006693000066930000669300006593010089AC
      4000E4EBD300FFFEFF00FEFEFF00FEFEFE00FEFFFD00FAFBF700A9C27500ABC3
      7900FBFCF900F2F5EB0095B55600629200006693000066930000669300006693
      000066930000669300006492000079A22700DAE4C200FAFBF600F9FAF400FAFB
      F600FDFEFD00FEFEFE00FDFDFE00FBFBFA00F9FAF500FAFAF600F3F6EB00A1BD
      6B006292030099B75C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000659300006895
      08009BB75D00E3EAD000FDFDFB00FFFFFF00FFFEFE00FAFBF700A9C17500ABC3
      7800FBFCF900F3F6EC0096B55600629200006693000066930000669300006693
      00006693000066930000659300006D99110095B35200A2BC6700A1BB6500B9CC
      8B00F5F8EF00FEFEFF00FDFDFD00D7E1C100A2BC6C00A1BB66009FBA62007DA4
      2D006293020098B75C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006692
      0100699409008BAD4200C1D39B00E8EED800F9FAF400F9FAF500A9C27400ABC3
      7800F9FAF400E1E9CF0089AD4200629200006693000066930000669300006693
      0000669300006693000066930000669300006592000065920000649200008CAC
      4100F1F5E600FFFEFF00FDFEFD00BED1980066940A0064920000659200006593
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      010066930000649201006D99140088AC3D00A4BE6D00B9CB8D0090B14C0099B7
      5B00B9CC8D008BAC45006A980E00649300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930100659200008DAD
      4200F2F6E800FFFFFF00FFFFFF00C0D2990069960A0065930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006996
      0900669300006693000066930000669300006693000066930000669300006693
      000066930000669300006592000064920000669303006A950900699508006D98
      0E006B960C006492000065930000669300006693000066930000669300006693
      00006693000066930000669300006693000066930000669301006593000088AA
      3B00E1E9CB00EDF2E100ECF1DF00B6C986006A960A0066930000669300006594
      00006293010098B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B700B1C488006896
      0800659300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000669300006593
      0000659300006693000066930000669300006693000066930000669300006693
      0000669300006693000066930000669300006693000066930000659300006F99
      120087AB3B008AAE41008CAD41007DA228006794030066930000669300006594
      00006293000097B85C00C2C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B8B8B800B3C68D006D9A
      1100699609006997090069970900699709006997090069970900699709006997
      0900699709006997090069970900699709006997090069970900699709006997
      0900699709006997090069970900699709006997090069970900699709006997
      0900699709006997090069970900699709006997090069970900699709006896
      0900659606006596060066960600689608006997090069970900699709006997
      090067960A009CBA6200C3C5BC00CECECF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E1E1E100B4B4B400D2D9C000B9CC
      9300B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB
      8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB
      8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB
      8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB
      8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB8F00B7CB
      8F00B7CA8F00CED9B500C1C1BE00CCCCCC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A3A3A300ABABAA00B1B1
      B100B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1
      B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1
      B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1
      B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1
      B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1B000B1B1
      B000B1B0B100B0AFB000A1A0A100CFCFCF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DFDFDF00D5D5D500D5D5
      D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D5D5D500D5D5D500D9D9D900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007D87
      D7002234BD007A84D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006868C2000000
      9B006868C2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007D89D8003F59
      D0006890EB003F5AD0007A84D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006868C3000E19B4004461
      DC001220B6006868C20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000818CDA003C57D0005072
      E100587BE400658BE9003F5AD0007A84D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006868C400050EB1001624C9002538
      CF00405ADA001220B6006868C200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009DBFEA00669CE000DAE6F600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000818EDB00405AD1004B6ADF00425E
      DB004865DD005678E300658BE9003F5AD0007A84D60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009DA1DD006269CA00C6C8EB000000000000000000C4C6EA005C63C700999B
      DB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006868C5000610B2000610C3000000BD00030C
      C2002335CE00405ADA001220B6006868C2000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D5D5
      D500C2C2C200C1C1C100B4B4B400B2B2B200AFAFAF00AEAEAE00B3B3B300B7B7
      B700C5C5C500C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600D2D2
      D200DCDCDC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000091B9
      E8002678D600317DD700CFDFF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002F45C6004F6EE1004562DD00415D
      DB00415CDB004865DD005678E300658BE9003F5AD0007A84D600000000000000
      0000000000000000000000000000000000000000000000000000000000007077
      D0002D42C600405CD2006269CA0000000000000000005E65C7003952CF002639
      C200696DCA000000000000000000000000000000000000000000000000000000
      000000000000000000006868C5000813B4000812C4000000BC000000BB000000
      BB00030CC2002538CF004461DC0000009B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F0C5A200F0C5A200F0C5A200F0C5A200F0C5A200F0C5A200F0C5
      A200F0C5A200F0C5A200F0C5A200F0C5A300F0C7A300F0C7A300F0C7A400F0C7
      A400F0C7A400F0C7A400F0C8A500F0C8A500F0C8A500F0C8A600F0C8A600F0C8
      A600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCDC00DBDBDB00DADADA00DADADA00DADADA00DBDBDB00DCDC
      DC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000081AEE5003285
      DA003E9CE400408BDB00CFDEF300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008491DC003A54D0004562DC004360
      DC00415DDB00415CDB004865DD005678E300658BE9003F5AD0007A84D6000000
      0000000000000000000000000000000000000000000000000000737AD100273B
      C4004664DD00587BE4000C18B00000000000000000000712AC00405ADA005071
      E1002639C200696DCA0000000000000000000000000000000000000000000000
      0000000000006868C6000B16B5000B17C5000000BE000000BC000000BB000000
      BB000000BD001624C9000E19B4006868C2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F0C49F00FDDAB500FDDAB500FDDAB500FDDAB500FDDAB500FDDA
      B500FDDBB600FDDBB700FDDBB800FDDCB900FDDCBA00FDDDBB00FDDDBC00FDDD
      BC00FDDEBD00FDDFBE00FDDFBF00FDE0C100FDE0C200FDE1C200FDE1C300F1CA
      A800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEDE00DADADA00D6D6D600D3D3
      D300D0D0D000C8C8C800B4B4B400A7A7A700A6A6A600A8A8A800B6B6B600CCCC
      CC00D0D0D000D3D3D300D6D6D600DADADA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000071A4E2003187DB0056B5
      ED005AB9EE00428DDC00CFDDF300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008491DC003A53D0004562
      DC004360DC00415DDB00415CDB004865DD005678E300658BE9003F5AD0007A84
      D6000000000000000000000000000000000000000000747DD100293EC5003149
      D400364DD5005374E2000E1AB10000000000000000000913AE002436CF003A53
      D8005071E1002639C200696DCA00000000000000000000000000000000000000
      00006868C7000D19B5000E1AC6000003BF000000BD000000BD000000BC000000
      BC000610C300050EB1006868C300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDEDE000000
      000000000000F2C39C00FED4AB00FED4AB00FED4AB00FED4AB00FED4AB00FED6
      AD00FED6AD00FED7AF00FED7B000FED8B100FED9B300FED9B500FED9B500FEDA
      B600FEDBB800FEDCB900FEDDBB00FEDEBD00FEDEBE00FEDEBE00FEDFBF00F3CD
      AA0000000000DEDEDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DDDDDD00D7D7D700D2D2D200C3C3C3008B8C8B006767
      6700535554004B4E4C00555856005D615F005E6260005B5F5D00535653004B4E
      4C00565957006E706E009A9A9A00CCCCCC00D2D2D200D8D8D800DDDDDD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D7E5F500669CE000358ADC0056B7EE0050C2
      F30051B6ED00438DDC00D0DEF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008491DC003A53
      D0004562DC004360DC00415DDB00415CDB004865DD005678E300658BE9003F5A
      D0007A84D6000000000000000000000000007780D3002C41C600324AD5002539
      D000324AD4005373E2000F1CB20000000000000000000B16AF001725C9002234
      CF003A53D8005071E1002639C200696DCA000000000000000000000000006869
      C900101CB800111FC8000007C0000002BE000000BE000000BD000000BE000812
      C4000610B2006868C400000000000000000000000000D6D6D600CDCDCD00CBCB
      CB00C3C3C300AEAEAE009B9B9B00909090008D8D8D008C8C8C008E8E8E008989
      890076767600F4C39900FFD0A300FFD0A300FFD0A300FFD0A300FFD1A400FFD2
      A500FFD2A600FFD3A800FFD4AA00FFD4AC00FFD5AE00FFD6AF00FFD6B000FFD8
      B200FFD9B300FFDAB600FFDBB800FFDBBA00FFDCBA00FFDCBB00FFDDBD00F6D0
      AF0077777700999999009696960096969600989898009B9B9B00A0A0A000AFAF
      AF00CACACA00CDCDCD00D6D6D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DADADA00D4D4D400CACACA00797979004F525000696E6B008185
      8200828783007D827E0085898500949896009BA09C008F939100818582007D82
      7E00838885007B807F0062656300525552008F908F00CECECE00D4D4D400DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CDDDF3005995DE003A8EDE005FBBF10046BDF30034B9
      F1004EB5ED003B87DA00AFC8ED00D9E4F600D9E4F600DBE6F600DDE7F6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008491
      DC003A53D0004562DC004360DC00415DDB00415CDB004865DD005678E300658B
      E9003F5AD0007A84D600000000007781D4002F45C800374FD7002B3FD2002538
      D000344BD5005576E200111FB30000000000000000000C18B0001624C9001725
      C9002234CF003A53D8005071E1002639C200696DCA0000000000686CC9001220
      B9001322C900040DC2000006C0000003BF000002BE000003BF000B17C5000813
      B4006868C500000000000000000000000000000000000000000000000000DEDE
      DE00ACACAC00A5A5A500B5B5B500BBBBBB00BBBBBB00BBBBBB00BBBBBB00BBBB
      BB00332E2F00EAB68E00EDB88F00EDB88F00EDB88F00EDB99000EDBA9200EDBA
      9300EDBB9500EDBC9600EDBD9800EDBE9B00EDBF9C00EDBF9D00EDC09F00EDC1
      A000EDC2A200EDC3A500EDC4A700EDC5A800EDC5A900EDC7AA00EDC9AC00ECC4
      A70032313200C9C9C900BCBCBC00BCBCBC00BCBCBC00BCBCBC00B7B7B700A4A4
      A400A6A6A6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DADADA00D3D3D300CBCBCB005F616100676B6A00868B88009BA09E00BEC1
      BF00D7D9D800E8EAE900E8EAE900E2E4E300DEE1DF00E3E5E400E8EAE900E1E3
      E200CCCFCD00B0B5B2008F94910084898600585C5A007C7E7E00CDCDCD00D3D3
      D300DADADA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C1D5F1004D8EDD004194E10065BEF30049BCF4002DB4F10031B8
      F10054B9ED003289DC003C88DA004B92DD005196DF00589BE0005F9FE20068A4
      E3006FA9E50078AEE60080B3E8008AB9E90094BEEB009CC3EC00A7C9EE00B0CE
      F000BAD5F100C4DAF300D0E1F500DCE8F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008491DC003A53D0004562DC004360DC00415DDB00415CDB004865DD005678
      E300658BE9003F59CF007A84D600324ACA003B55D9002E44D3002A3FD100293B
      D100364DD7005577E3001221B40000000000000000000E1AB1001827CA001624
      C9001725C9002234CF003A53D8005071E1002538C200696DCA001623BA001826
      C9000711C300020BC2000108C0000006C0000007C0000E1AC6000B16B5006868
      C50000000000000000000000000000000000000000000000000000000000E0E0
      E000ADADAD00C5C5C500C5C5C500C5C5C500C5C5C500C5C5C500C5C5C500C2C2
      C200342B2D00DFAA8500DEA98400DEA98400DEA98400DEAA8700DEAA8700DEAB
      8800DEAC8A00DEAD8C00DEAD8E00DEAE9000DEAE9000DEAF9100DEB19300DEB2
      9500DEB29700DEB39900DEB49B00DEB49B00DEB59C00DEB69E00DEB7A000E1B9
      9F0032313100CCCCCC00C3C3C300C3C3C300C3C3C300C3C3C300C3C3C300C3C3
      C300AFAFAF00D1D1D10000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DDDD
      DD00D6D6D600CFCFCF00929392006466640095999600BBC0BD00CBCFCD00BFC3
      C100B4B9B700ACB2AF00AAB0AD00AAB0AD00AAB0AD00AAB0AD00AAB0AD00AEB4
      B100B6BBB900C0C4C200C5C9C700ABAFAD009197930052555300B7B7B700CFCF
      CF00D6D6D600DDDDDD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B3CCEF00468ADC004A9AE4006AC0F5004DBCF50034B4F2002CB3F0002EB6
      F00054C1EF0059BAEC005AB5EB0060B7EB0067B8EC006FBBED0076BDEE007DC0
      EE0085C3EF008DC5EF0096C8F0009DCCF100A5CFF200ACD2F300B3D6F400BBD9
      F400C2DDF500CBE1F600D3E5F700DEEBF900E8F1FA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008491DC003A53D0004562DC004360DC00415DDB00415CDB004865
      DD005678E3005F83E600364DCC00405ADA00324BD5002F44D3002C41D2002A3F
      D1003850D8005678E4001523B50000000000000000000F1CB2001B2BCC001827
      CA001624C9001725C9002234CF003A53D8004864DD001826BC001A2BCC000C17
      C500060FC300040DC200020BC200040DC200111FC8000D19B5006868C6000000
      000000000000000000000000000000000000000000000000000000000000C2C2
      C200C3C3C300D1D1D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1
      D10035292C00D39D7D00CF9A7A00CF9A7A00CF9A7C00CF9B7C00CF9B7D00CF9C
      7F00CF9D8000CF9E8100CF9E8300CF9F8400CF9F8500CFA18600CFA18800CFA2
      8A00CFA38B00CFA48D00CFA48E00CFA48E00CFA59000CFA79200CEA79300D5AC
      960033313100CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00C5C5C500B1B1B10000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DBDB
      DB00D4D4D400CDCDCD008181810071757300A7ACAA00AEB4B100AAB0AD00AAB0
      AD00A0A5A200A0A5A200A0A5A200A0A5A200A0A5A200A0A5A200A0A5A200A0A5
      A200A0A5A200AAB0AD00AAB0AD00AAB0AD00A0A5A2005B5F5B00A6A6A600CDCD
      CD00D4D4D400DBDBDB0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A5C3
      EC004088DB0053A0E70070C3F60051BCF6003CB5F40034B4F2002CB3F00027B3
      EF002DB4ED003AB7EE0043B9EE004CB9ED0056BBEF005FBDEF0069C0EF0072C2
      F0007BC4F00087C7F1008FCAF20098CDF200A1D1F300ABD4F400B3D8F400BDDC
      F600C6E0F600CFE4F700D8E9F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008491DC003A53D0004562DC004360DC00415DDB00415C
      DB004865DD004C6BDF00435FDB003850D700344BD5003048D4002F44D3002C41
      D2003953D8005879E4001624B5000000000000000000111FB3001C2DCC001B2B
      CC001827CA001624C9001725C9002234CF002B40D2001F2FCD000F1BC6000915
      C5000812C400060FC3000711C3001322C900101CB8006868C700000000000000
      000000000000000000000000000000000000000000000000000000000000B8B8
      B800CCCCCC00D3D3D300D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2
      D20032252800AD816700A1776000A2786100A2786200A2786300A2796400A27A
      6400A27A6600A27B6800A27B6800A27B6800A27C6A00A27D6B00A27E6C00A27E
      6E00A27E6F00A27F6F00A27F7000A27F7100A2817200A2827400A1817400AB8B
      7B00322F3000CECECE00CDCDCD00CDCDCD00CDCDCD00CCCCCC00CCCCCC00CCCC
      CC00CBCBCB00AAAAAA0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDC
      DC00D5D5D500CECECE00B6B6B600525553008F939100A9AFAC00AAB0AD00AAB0
      AD008D9290008D9290008D9290008D9290008D9290008D9290008D9290008D92
      90008D929000AAB0AD00AAB0AD00A8AEAB00797C7A005B5E5D00C6C6C600CECE
      CE00D5D5D500DCDCDC0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000096BAE9003B85
      DB0060A9EC0074C5F80054BBF70043B6F5003CB5F40034B4F2002CB3F10025B2
      F00022AFEC002BB1ED0035B2ED003FB4ED0049B6EE0053B7EE005EBAEE0067BD
      EE0073C0EF007DC3F00085C6F00090C9F1009BCDF200A4D1F300AFD5F400B8D9
      F500C3DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008491DC003A53D0004562DC004360DC00415D
      DB00415CDB00405CDA003C56D9003850D700364DD500344BD5003048D4002F44
      D3003B55D900587CE4001826B60000000000000000001221B4001F2FCD001C2D
      CC001B2BCC001827CA001624C9001725C9001826CA001321C8000E1AC6000C17
      C5000915C5000C17C5001826C9001220B9006869C90000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BBBB
      BB00CECECE00D5D5D500D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4
      D400CDCDCD00D2D0CF00D2D0CF00D2D0CF00D2D0CF00D2D0CF00D2D0CF00D2D0
      CF00D2D0CF00D2D0CF00D2D0CF00D1CFCF00D1CFCF00D1CFCF00D1CFCF00D1CF
      CF00D1CFCF00D1CFCF00D1CFCF00D1CFCF00D1CFCF00D1CFCF00D1CFCF00D1CF
      CF00D2D2D200D8D8D800D0D0D000D0D0D000C0CBC700CFCFCF00CFCFCF00CFCF
      CF00CECECE00ADADAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D8D8D800D2D2D200CBCBCB009EA09E00505351006469660092979400AAB0
      AD00878B8900878B8900878B8900878B8900878B8900878B8900878B8900878B
      8900868B8900A6ABA900898E8B00585C59005A5C5A00B3B3B300CBCBCB00D2D2
      D200D9D9D9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000088B2E7003D86DC006AAF
      EF0078C5FA005ABBF8004BB7F70043B6F6003CB5F40034B4F3002CB3F10025B2
      F00023B0EC002BB2ED0036B2ED0040B5ED0049B6ED0053B8EE005EBBEE0069BE
      EF0071C0EF007CC3F00086C6F10091C9F1009BCDF200A4D1F300AED5F400B8DA
      F500C3DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008491DC003A53D0004562DC004360
      DC00415DDB003F59DA003C57D9003A54D8003850D700364DD500344BD5003048
      D4003E57D900597DE5001929B80000000000000000001523B5002234CE001F2F
      CD001C2DCC001B2BCC001827CA001624C9001322C800121FC800101CC7000E1A
      C6000F1BC6001A2BCC001623BA00686CC9000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD00D0D0D000D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D5D5D500D5D5D500D5D5D500D5D5D500D5D5D500D4D4D400D4D4D400D4D4
      D400D4D4D400D4D4D400D4D4D400D3D3D300D3D3D300D3D3D300D3D3D300D3D3
      D300D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D1D1D100D1D1
      D100D1D1D100D1D1D100D1D1D1009BC1B10030A171008CBCA800D1D1D100D1D1
      D100D0D0D000B0B0B00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DDDDDD00D6D6
      D600D1D1D100CBCBCB00C6C6C600BFBFBF00B4B4B4007D7D7D00585959004E51
      4F007D817F007D817F007D817F007D817F007D817F007D817F007D817F007D81
      7F007C807E004D504E005E5F5E0089898900B9B9B900BFBFBF00C4C4C400CBCB
      CB00D1D1D100D6D6D600DDDDDD00000000000000000000000000000000000000
      00000000000000000000000000000000000071A2E2003682DB0076B5F2007FC7
      FB005FBBFA0053B8F8004BB7F70044B6F6003CB5F40034B4F3002CB3F10025B2
      F00023B0EC002BB2ED0036B2ED0040B5ED0049B6ED0053B8EE005EBBEE0067BD
      EF0071C0EF007CC3F00087C6F10091C9F1009BCDF200A4D1F300AED5F400B8DA
      F500C3DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008491DC003A53D0004562
      DC004360DC00415DDB003F59DA003C57D9003A54D8003850D700364DD500344B
      D5003F5ADA005C7FE5001B2BB90000000000000000001624B5002336CF002234
      CE001F2FCD001C2DCC001B2BCC001827CA001624C9001322C800121FC8001321
      C8001F2FCD001826BC00696DCA00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BFBF
      BF00D2D2D200D8D8D800D7D7D700D7D7D700D7D7D700D7D7D700D7D7D700D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D5D5D500D5D5D500D5D5D500D5D5D500D5D5
      D500D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D3D3D300D3D3
      D300D3D3D300D3D3D300D3D3D300BDCDC5006DB39600B5CAC000D2D2D200D2D2
      D200D1D1D100B3B3B30000000000000000000000000000000000000000000000
      0000000000007A7A7A007C7C7C008384830081818100808180007F807F007D7E
      7D007C7D7C007B7C7B00797A7900787978007777770075757500747474007374
      7300747575007474740072737300707171006F7070006E6F6F006D6E6E006B6C
      6B006A6B6A006767670066666600656565006464640063636300616161006060
      60005F5F5F005E5E5E005F5F5F005E5E5E005E5E5E005D5D5D00535353007878
      78000000000000000000000000000000000075A5E3003782DA0074B4F20080C7
      FB0060BBFA0053B8F8004BB7F70044B6F6003CB5F40034B4F3002CB3F10025B2
      F00023B0EC002BB2ED0036B2ED0040B5ED0049B6ED0053B8EE005EBBEE0067BD
      EF0071C0EF007CC3F00087C6F10091C9F1009BCDF200A4D1F300AED5F400B8DA
      F500C3DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008795DD00435FD3004D6DDF004965
      DD004562DC004360DC00415DDB003F59DA003C57D9003A54D8003850D700364D
      D500425EDB005D80E6001C2DBA0000000000000000001826B6002538D0002336
      CF002234CE001F2FCD001C2DCC001B2BCC001827CA001624C9001322C8001826
      CA002B40D2004864DD002538C200696DCA000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C5C5
      C500D5D5D500DADADA00D9D9D900D9D9D900D9D9D900E9E9E900E9E9E900E1E5
      E300E1E5E300E1E5E300E1E5E300E1E5E300E1E5E300E0E4E200E0E4E200E0E4
      E200E0E4E200E0E4E200E0E4E200DFE3E100DFE3E100DFE3E100DFE3E100DEE3
      E100DEE3E100DEE3E100DEE3E100DEE3E100DEE3E100DEE3E100DEE2E000DEE2
      E000DDE2E000DDE2E000E4E4E400E4E4E400D4D4D400D4D4D400D4D4D400D4D4
      D400D4D4D400BCBCBC0000000000000000000000000000000000000000000000
      000084848400B6B9B800E4E8E600DEE3E000DEE2E000DEE2DF00DDE2DF00DDE1
      DF00DCE1DE00DCE1DE00DCE0DE00DCDFDE00DBDFDD00DBDEDC00DADEDC00D9DE
      DB00D9DDDB00D9DDDB00D8DDDB00D8DCDB00D8DCDA00D7DCD900D7DBD900D6DA
      D900D5DAD800D5D9D800D5D9D800D4D9D700D4D9D700D3D8D600D3D8D500D2D7
      D500D2D7D500D1D7D400D1D6D400D1D6D400D0D5D300D0D5D300D3D7D5007476
      7600A0A0A000000000000000000000000000000000008CB4E7003D86DB0068AE
      EF0079C5FA005BBCF9004BB7F70043B6F6003CB5F40034B4F3002CB3F10025B2
      F00023B0EC002BB2ED0036B2ED0040B5ED0049B6ED0053B8EE005EBBEE0069BE
      EF0071C0EF007CC3F00086C6F10091C9F1009BCDF200A4D1F300AED5F400B8DA
      F500C3DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008796DF004967D7005879E4004E6EE1004A69
      DE004965DD004562DC004360DC00415DDB003F59DA003C57D9003A54D8003850
      D700435FDB005E82E6001F2FBB0000000000000000001929B800293BD1002538
      D0002336CF002234CE001F2FCD001C2DCC001B2BCC001827CA001624C9001725
      C9002234CF003A53D8005071E1002639C200696DCA0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C5C5
      C500D6D6D600DBDBDB00DBDBDB00DBDBDB00EAECEB005BAE8B002C9A6A002899
      6800289968002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      680028996800289968002A99690049A67F00DBE3DF00E8E8E800D6D6D600D6D6
      D600D6D6D600BDBDBD0000000000000000000000000000000000000000000000
      000077777700E2E6E400C5CAC700B9BFBD00B8BFBC00B8BFBC00B8BFBC00B7BF
      BB00B7BEBB00B7BEBB00B7BEBA00B7BDBA00B7BDBA00B6BDB900B6BCB900B6BC
      B900B5BCB800B5BBB800B5BBB800B4BBB700B4BAB700B4BAB700B3BAB700B3B9
      B700B3B9B700B2B9B600B2B8B600B2B8B600B1B8B500B1B7B500B1B7B500B1B7
      B500B0B7B400B0B7B400B0B7B400AFB6B300AFB6B300AFB6B300C4CAC800B0B3
      B1007575750000000000000000000000000000000000000000009ABDE9003C84
      DB005DA7EC0075C5F80056BCF70043B6F6003CB5F40034B4F2002CB3F10025B2
      F00022AFEC002BB1ED0035B2ED003FB4ED0049B5ED0053B7EE005EBAEE0067BD
      EE0073C0EF007DC3F00085C6F00090C9F1009BCDF200A4D1F300AFD5F400B8D9
      F500C2DEF600CDE3F700D6E8F800E2EEF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008998E0004C6BD8005C7FE5005274E2004F6EE1004D6C
      DF004A69DE004965DD004562DC004360DC00415DDB003F59DA003C57D9003A54
      D8004562DC005F83E6002031BC0000000000000000001B2BB9002A3FD100293B
      D1002538D0002336CF002234CE001F2FCD001C2DCC001B2BCC001827CA001624
      C9001725C9002234CF003A53D8005071E1002639C200696DCA00000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600D8D8D800DDDDDD00DCDCDC00F0F0F00063B39200279E6D0055D2A7004ACC
      9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC
      9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC
      9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC9F004ACC
      9F004ACC9F004ACC9F0055D2A70027A06F0046A77E00ECECEC00D7D7D700D7D7
      D700D7D7D700BDBDBD0000000000000000000000000000000000000000000000
      00007A7A7A00E4E7E600D3DBD700D1D9D500D1D8D500D0D8D400D0D8D400D0D7
      D400CFD7D300CFD7D300CFD6D300CED6D200CED5D200CED5D200CDD5D100CDD4
      D100CDD4D100CCD4D000B4BAB700A8ADAB00999E9C00C4CBC800CBD2CF00CAD2
      CE00CAD1CE00CAD1CE00CAD1CE00C9D0CD00C9D0CD00C9D0CD00C8CFCC00C8CF
      CC00C8CFCC00C7CECB00C7CECB00C7CECB00C6CDCA00C6CDCA00CDD3D000B0B3
      B20078787800000000000000000000000000000000000000000000000000A9C6
      EC004388DB00509EE60070C3F70052BDF7003CB5F40034B4F2002CB3F00027B3
      F0002EB4EE003BB7EE0044B9EE004DBAED0056BCEF0061BEEF006AC0EF0073C3
      F0007DC5F10085C7F1008FCBF20099CEF200A2D1F300ACD5F400B3D8F500BDDC
      F500C6E0F600CFE4F700D8E9F800E1EDF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008B9BE1004F6FDA005F83E7005779E4005475E2005072E1004F6E
      E1004D6CDF004A69DE004965DD004562DC004360DC00415DDB003F59DA003C57
      D9004864DD006186E7002234BD0000000000000000001C2DBA002C41D2002A3F
      D100293BD1002538D0002336CF002234CE001F2FCD001C2DCC001B2BCC001827
      CA001624C9001725C9002234CF003A53D8005071E1002639C200696DCA000000
      000000000000000000000000000000000000000000000000000000000000C7C7
      C700DADADA00DFDFDF00DEDEDE00F4F4F40033A1730057D4A900289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      680028996800289968002899680057D4A900289C6B00ECEEED00D9D9D900D9D9
      D900D9D9D900BEBEBE0000000000000000000000000000000000000000000000
      00007D7E7D00E0E4E200D5DCD900B18E8B009F6563009F6563009F6563009F65
      63009F6463009F6463009F6463009F6463009E6462009E6462009E6362009E63
      62009E6362009E6362009E6362009E6361009E6361009E6361009D6361009D63
      62009D6362009D6361009D6261009D6261009D6261009D6261009D6261009D62
      60009D6260009D6260009C615F00AE8C8A00C7CECB00C6CDCA00CED4D200AAAD
      AB00828282000000000000000000000000000000000000000000000000000000
      0000B6CFEF00488CDC004698E3006AC0F5004FBDF50035B4F2002CB3F0002EB6
      F00055C1F00059B9EC0058B4EB005FB6EB0065B7EB006DBAEC0074BCEC007CBF
      ED0084C2EF008BC5EF0094C7EF009BCBF100A4CEF100ACD2F200B3D5F300BAD8
      F400C1DCF500CAE0F600D3E5F700DDEAF900E8F0FA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008E9FE2005273DB006389E8005C80E5005779E4005677E3005475E2005072
      E1004F6EE1004D6CDF004A69DE004965DD004562DC004360DC00415DDB003F59
      DA004968DE006287E7002336BE0000000000000000001F2FBB002F44D3002C41
      D2002A3FD100293BD1002538D0002336CF002234CE001F2FCD001C2DCC001B2B
      CC001827CA001624C9001725C9002234CF003A53D8005071E1002639C200696D
      CA0000000000000000000000000000000000000000000000000000000000C8C8
      C800DDDDDD00E4E4E400E4E4E400F8F8F8002B9F6F005CD7AE002899680025A2
      700025A2700025A2700025A2700025A2700025A2700025A2700025A2700025A2
      700025A2700025A2700025A2700025A2700025A2700025A2700025A2700025A2
      700025A2700025A2700025A2700025A2700025A2700025A2700025A2700025A2
      700025A2700025A27000289968005CD7AE00259F6E00E6EEEA00DFDFDF00DFDF
      DF00DFDFDF00BEBEBE0000000000000000000000000000000000000000000000
      000082828200DEE1DF00D7DEDA009A575600824B4B00824B4B00824B4B00824B
      4B00834C4C00834C4C00844C4C00844D4D00844D4D00854D4D00854D4D00854E
      4E00864E4E00864E4E00864E4E00874F4F00874F4F00874F4F00885050008850
      50008950500089505000895151008A5151008A5151008A5151008B5252008B52
      52008B5252008C5252008C53530098565500C7CECB00C7CECB00D0D7D300A4A7
      A5008E8E8E000000000000000000000000000000000000000000000000000000
      000000000000C4D7F1004E90DD003D92E00065BEF3004CBEF4002EB4F10030B7
      F00054B8ED003089DB003D88DB004D93DE005396DF005A9CE10061A0E2006AA5
      E40071A9E50078AEE60081B3E8008CB8E90093BFEB009EC4ED00A7C9EE00B0CF
      F000BAD6F200C4DCF300D1E1F500DBE8F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B3BF
      EB005677DD00688DE9005F83E6005D80E6005A7EE5005779E4005677E3005475
      E2005072E1004F6EE1004D6CDF004A69DE004965DD004562DC004360DC00415D
      DB004B6ADE006389E8002538BF0000000000000000002031BC003048D4002F44
      D3002C41D2002A3FD100293BD1002538D0002336CF002234CE001F2FCD001C2D
      CC001B2BCC001827CA001624C9001725C9002234CF003A53D8005071E1002639
      C200999BDB00000000000000000000000000000000000000000000000000CBCB
      CB00E5E5E500ECECEC00ECECEC00FBFBFB002AA2720056D6AB0024A371001EB2
      7F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB2
      7F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB2
      7F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB27F001EB2
      7F001EB27F001EB27F0024A3710057D6AB0024A16F00ECF3F000E7E7E700E7E7
      E700E6E6E600C0C0C00000000000000000000000000000000000000000000000
      000086878600DADDDC00D9DFDC009D5F5D007E4747007E4848007F4848007F49
      49007F4949008049490080494900804A4A00814A4A00814A4A00824B4B00824B
      4B00824B4B00824B4B00834C4C00834C4C00834C4C00844C4C00844D4D00844D
      4D00854D4D00854E4E00864E4E00864E4E00864E4E00874F4F00874F4F00874F
      4F00884F4F0088505000874D4D009C5F5E00C8CFCC00C7CECB00D3D8D6009A9C
      9B00989898000000000000000000000000000000000000000000000000000000
      00000000000000000000D0DFF4005C97DF00378CDE005EBAF0004ABEF30035B9
      F1004EB5ED003B87DA00B1C9ED00DCE7F600DDE7F600DEE9F700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDEAE4006FAD8E009FC8
      B400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000869A
      E2005E81E300648AE8006186E7005E82E6005D80E6005A7EE5005779E4005677
      E3005475E2005072E1004F6EE1004D6CDF004A69DE004965DD004562DC004461
      DC004F6FE1004F6EDB00737FD5000000000000000000707CD3002D43CD003048
      D4002F44D3002C41D2002A3FD100293BD1002538D0002336CF002234CE001F2F
      CD001C2DCC001B2BCC001827CA001624C9001725C9002436CF00405ADA003952
      CF005C63C700000000000000000000000000000000000000000000000000CCCC
      CC00EBEBEB00F4F4F400F4F4F400FDFDFD002BA5750051D5A9001EB783001EB7
      83001EB783001EB783001EB783001EB783001EB783001EB783001EB783001EB7
      83001EB783001EB783001EB783001EB783001EB783001EB783001EB783001EB7
      83001EB783001EB783001EB783001EB783001EB783001EB783001EB783001EB7
      83001EB783001EB783001EB7830052D5A90023A57300F0F6F400F1F1F100F1F1
      F100EFEFEF00C3C3C30000000000000000000000000000000000000000000000
      00008A8A8A00D6D9D700DBE2DE00A46E6C007A4040007B4545007B4646007B46
      46007C4646007C4646007C4747007D4747007D474700535C670015809D00027D
      9D00007998000076940000728F00006F8B00006B86000067820000647D000064
      7D0000647D0005667E0027607100784C4E00834C4C00834C4C00834C4C00844C
      4C00844D4D00844D4D0084454500A26F6E00C8CFCC00C8CFCC00D4DAD7009396
      9500A3A3A3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D9E6F6006B9FE1003288DC0055B5EE0054C4
      F30052B6ED00438DDC00D0DEF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E4DA003D936900328F
      640096C4AD000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D3DA
      F300869AE2004361D300425FD200415DD1003F5AD0003E58CF003B56CE003A54
      CD003853CC003750CC00364ECA00344CC900324AC8003048C7002F45C6002D43
      C5002C41C4007583D700CDD2EF000000000000000000CBD0EE00707CD3002031
      BC001F2FBB001C2DBA001B2BB9001929B8001826B6001624B5001523B5001221
      B400111FB3000F1CB2000E1AB1000C18B0000B16AF000913AE000712AC005E65
      C700C4C6EA00000000000000000000000000000000000000000000000000CDCD
      CD00F2F2F200FCFCFC00FCFCFC00FFFFFF002BAB7A007EF4D0002CC593002CC5
      93002CC593002CC593002CC593002CC593002AC392002AC392002AC392002AC3
      92002AC392002AC392002AC392002AC392002AC392002AC392002AC392002AC3
      92002AC392002AC392002AC392002AC392002AC392002AC392002AC392002AC3
      92002AC392002AC392002AC392007EF4D00025AB7900F4FAF700FCFCFC00FCFC
      FC00F8F8F800C6C6C60000000000000000000000000000000000000000000000
      00008F8F8F00D2D4D400DDE3E000AA7D7C00793C3C0079454500774343007843
      430078434300784444007944440079444400635057001E92AF00A0E8F300A5F5
      FE0092F3FE0088F1FC007DEEFA0073EAF70068E7F5005DE3F20058E1F00053DF
      ED004DDCEB0047CFE0002397AB00246071007F4949007F49490080494900804A
      4A00814A4A00814A4A00803E3E00A9807E00C8D0CC00C8CFCC00D7DCDA008C8E
      8D00ADADAD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000076A6E3002E84DA0052B3
      EC005AB9EE00438DDC00CFDDF300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E3D9004C9F7A0054B1
      9100419B750087BBA10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D0D0
      D000F7F7F700FFFFFF00FFFFFF00FFFFFF002DB2810072EEC8006AEBC4006AEB
      C4006AEBC4006AEBC4006AEBC4006AEBC40068EBC30068EBC30068EBC30068EB
      C30068EBC30068EBC30068EBC30068EBC30068EBC30068EBC30068EBC30068EB
      C30067EAC20067EAC20067EAC20067EAC20067EAC20067EAC20067EAC20067EA
      C20067EAC20067EAC20067EAC20075EFCA0029B38100F5FAF800FFFFFF00FFFF
      FF00FEFEFE00C8C8C80000000000000000000000000000000000000000000000
      000096969600CDD0CF00DFE5E200B28E8C007B3D3D007F5050007D4E4E007B49
      490076444400754141007541410075414100406475007CCCDC00A0F5FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30007C7DC0047CAD90004657E007B4646007C4646007C4646007C47
      47007D4747007D4747007D383800B1929000C9D0CD00C8D0CC00D9DEDC008585
      8500B8B8B8000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000085B0E6002E84
      DA003899E3003E8ADB00CFDEF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E3D9004EA17C0074CB
      B40071C8B000429C760077B29600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D2D2
      D200F7F7F700FFFFFF00FFFFFF00FFFFFF002FB8870072EEC9006DEDC7006DED
      C7006DEDC7006DEDC7006DEDC7006DEDC7006BEDC5006BEDC5006BEDC5006BED
      C5006BEDC5006BEDC5006BEDC5006BEDC5006BEDC5006BEDC5006BEDC5006BED
      C5006AECC5006AECC5006AECC5006AECC5006AECC5006AECC5006AECC5006AEC
      C5006AECC5006AECC5006AECC50075F0CB002DB98800F6FAF800FFFFFF00FFFF
      FF00FEFEFE00C9C9C90000000000000000000000000000000000000000000000
      00009F9F9F00CACCCB00E1E7E400BBA29F00793939007E5252007F5151007F51
      51007D5050007B4C4C0075434300713E3E003C64760088D3E30098F4FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30000C5DB0047CCDB0004667F007843430078434300784444007944
      440079444400794444007C353500B8A2A000C9D0CD00C9D0CD00DCE0DE007D7F
      7E00C3C3C3000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000095BA
      E9002679D4002F7CD600CFDFF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E4DB004EA17C006EC9
      B10074D7C30071CAB20046A07B006EAD8F00D6E7E00000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D4DC
      F4008A9DE3004967D5004664D5004562D4004361D300425FD200415DD1003F5A
      D0003E58CF003B56CE003A54CD003853CC003750CC00364ECA00344CC900324A
      C8003048C7007886D900CDD2F0000000000000000000CDD2EF00737FD5002538
      BF002336BE002234BD002031BC001F2FBB001C2DBA001B2BB9001929B8001826
      B6001624B5001523B5001221B400111FB3000F1CB2000E1AB1000C18B0006269
      CA00C6C8EB00000000000000000000000000000000000000000000000000D3D3
      D300F8F8F800FFFFFF00FFFFFF00FFFFFF0031BD8D0072F0CA006FEFC9006FEF
      C9006FEFC9006FEFC9006FEFC9006FEFC9006EEFC9006EEFC9006EEFC9006EEF
      C9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEF
      C9006CEEC8006CEEC8006CEEC8006CEEC8006CEEC8006CEEC8006CEEC8006CEE
      C8006CEEC8006CEEC8006CEEC80074F0CC0031C08F00F7FBF900FFFFFF00FFFF
      FF00FEFEFE00CACACA0000000000000000000000000000000000000000000000
      0000A5A5A500C1C3C200E4E8E600C2AFAC007C3B3B007F5353007E5353007F53
      53007E5252007E5151007D5151007B4E4E003D67790089D4E40099F4FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30000C5DB0047CCDB0004667F007440400074414100754141007541
      410075414100764242007B343400BDAFAD00CAD1CE00C9D0CD00DEE2E0007778
      7700CECECE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A1C1EB006A9EE000D9E6F500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DDEBE400DBEAE200DAE9E200DAE9E200B0D1C100469C76006BC8
      B0005ED0B9006AD3BC0078CFB80049A3800061A78700CDE2D800000000000000
      0000000000000000000000000000000000000000000000000000000000008C9F
      E5006C95E9007EABF3007DAAF2007CA9F2007BA8F10079A6F10078A5F10077A2
      F00075A1F000759FF000749EEF00729CEE00719BEE006F9AED006E97ED006E97
      ED00719BEE005A7FE1007886D90000000000000000007583D7004F6EDB006389
      E8006287E7006186E7005F83E6005E82E6005D80E6005C7FE500597DE500587C
      E4005879E4005678E4005577E3005576E2005373E2005374E200587BE400405C
      D2006269CA00000000000000000000000000000000000000000000000000D4D4
      D400F6F6F600FFFFFF00FFFFFF00FFFFFF0032C1900077F2CD0070F0CA0070F0
      CA0070F0CA0070F0CA0070F0CA0070F0CA006FEFC9006FEFC9006FEFC9006FEF
      C9006FEFC9006FEFC9006FEFC9006FEFC9006FEFC9006FEFC9006FEFC9006FEF
      C9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEFC9006EEF
      C9006EEFC9006EEFC9006EEFC90074F1CC0032C39300F7FBF900FFFFFF00FFFF
      FF00FEFEFE00CBCBCB0000000000000000000000000000000000000000000000
      0000ADADAD00BABCBB00E5EAE800C7BBB9007E3C3C007F5656007F5656007F55
      55007E5454007E5353007E5353007D535300437183008AD5E6009AF4FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30000C5DB0047CCDB0004667F00703D3D00703E3E00713E3E00713E
      3E00723F3F00723F3F007C333300C1BDB900CAD1CE00CAD1CE00E0E4E2007071
      7100DBDBDB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D7E9E300CBE3DA00C2DED200B7D9
      CB00AFD4C400A4CFBD009ECBB70093C6B1008AC2AB0084BDA5007BB99E0073B6
      99006BB1930064AD8E005EA9880056A6830050A37E00429A73003D9D760072CD
      B7005CCFB80055CDB3006AD3BC007CD2BB0051AA870055A17D00C1DBCF000000
      000000000000000000000000000000000000000000000000000000000000B7C3
      EE005E82E200759FEF00739DEF00719BEE006F99ED006D96EC006C94EC006A92
      EC006990EB00658CE900648AE9006289E8006085E7005F83E6005D81E5005C80
      E500648AE900719BEE003048C70000000000000000002C41C4004F6FE1004B6A
      DE004968DE004864DD004562DC00435FDB00425EDB003F5ADA003E57D9003B55
      D9003953D8003850D800364DD700344BD500324AD400364DD5004664DD002D42
      C6009DA1DD00000000000000000000000000000000000000000000000000D6D6
      D600F3F3F300FFFFFF00FFFFFF00FFFFFF0032C3930075F1CC0035D9A70035D9
      A70035D9A70034D2A00034CE9C0034CE9C0034CE9C0034CE9C0034CE9C0034CD
      9C0034CD9B0034CD9B0034CD9B0034CD9B0034CD9B0034CD9B0034CD9B0034CD
      9B0034CD9B0034CD9B0034CC9B0034CC9A0034CC9A0034CC9A0034CC9A0034CC
      9A0035D7A50035D9A70035D9A70075F1CC0031C59400F8FCFA00FFFFFF00FFFF
      FF00FCFCFC00CECECE0000000000000000000000000000000000000000000000
      0000B5B5B500B3B4B300E7ECE900CDC9C500803D3C007F575700805757007F56
      56007F5555007F5656007E5656007E555500437384008BD6E6009BF4FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30000C5DB0047CCDB0004667F006D3B3B006D3B3B006D3B3B006D3B
      3B006E3C3C006E3C3C007E313100C8CAC700CAD2CE00CAD1CE00E3E6E5006868
      6800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E5F2ED00DAEDE600D0E8E100C8E5DB00C0E2
      D700BADFD300B3DCD000ADDACD00A6D7C8009ED5C50097D2C20090D0BE008BCE
      BB0083CCB8007DCBB50077C9B30072C7B1006CC6AF0067C4AE006ECCB60076D5
      C10059CEB60055CCB20059CDB3006BD3BB007FD4BD0058AF8E004F9E7900B4D4
      C500000000000000000000000000000000000000000000000000000000000000
      000095A9E7005C7FE2006F99ED006D96ED006B93EC006991EB00678FE900658B
      E9006389E8006186E7005E82E6005D80E6005A7EE5005779E4005677E3005576
      E2005C80E5006E97ED00324AC80000000000000000002D43C5004461DC00415D
      DB003F59DA003C57D9003A54D8003850D700364DD500344BD5003048D4002F44
      D3002C41D2002A3FD100293BD1002538D0002539D0003149D400273BC4007077
      D00000000000000000000000000000000000000000000000000000000000DADA
      DA00ECECEC00FDFDFD00FDFDFD00FFFFFF002FC392005BE6BC002DD4A2002EC3
      92002EC392002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      6800289968002899680028996800289968002899680028996800289968002899
      680039AC7C002DD19F002DD4A20052E3B7002CC49300FAFCFB00FDFDFD00FDFD
      FD00F5F5F500D3D3D30000000000000000000000000000000000000000000000
      0000BFBFBF00ADAEAE00E9EEEB00D1D5D100813B3A00815B5B007F5959007F59
      59007F5858007E5858007E5858007F565600437587008BD8E8009CF4FF0056EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30000C5DB0047CCDB0004667F0069383800693838006A3838006A39
      39006A3939006B3939007F313100CBD2CF00CBD2CF00CBD2CF00E5E8E7006161
      6100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DFF0EB00D5ECE500CDE8E000C4E5
      DC00BCE2D800B4E0D400ACDDD000A3DBCD009CD8CA0093D6C6008BD4C30085D2
      C1007BD1BE0074D0BC006DCEBA0065CDB8005DCCB70056CBB60056CDB70057CD
      B60053CCB30055CDB20059CDB3005CCEB3006CD3BA0083D7C00061B59500499C
      7500A8CDBA000000000000000000000000000000000000000000000000000000
      00000000000095A9E7005C7FE2006F99ED006D96ED006B93EC006991EB00678F
      E900658BE9006389E8006186E7005E82E6005D80E6005A7EE5005779E4005677
      E3005D81E5006E97ED00344CC90000000000000000002F45C6004562DC004360
      DC00415DDB003F59DA003C57D9003A54D8003850D700364DD500344BD5003048
      D4002F44D3002C41D2002A3FD1002B3FD200324AD500293EC500737AD1000000
      000000000000000000000000000000000000000000000000000000000000E6E6
      E600DCDCDC00F3F3F300F3F3F300FFFFFF0028C2910022CF9B001FCE990029B4
      830029B48300E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B0047A6770021C893001FCE990023CF9B0022C28E00FBFDFC00F3F3F300F3F3
      F300E3E3E300DFDFDF0000000000000000000000000000000000000000000000
      0000C9C9C900A7A8A700EBEFED00D6DEDA00833C3B00815D5D00805C5C00805B
      5B007F5B5B007F5A5A007F5959007F5959004576870081D3E500A6F6FF0057EE
      FF003DEAFD0036E7FA002FE3F70028DFF30021DCF00019D8ED0012D5EA000BD1
      E60003CDE30006C6DC0047CAD90004657F006535350066353500663636006636
      3600673636006736360081353500CCD3D000CBD3CF00CBD2CF00E8EBE9005C5C
      5C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFE900D4EBE400CBE8DF00C1E4
      DA00B7E0D500AFDDD000A6DACC009BD7C80094D5C5008AD3C10082D0BD007ACF
      BB0071CDB80069CBB50061CAB30059C8B10051C7B00049C6AF0049C9B1004DCA
      B10051CBB20055CCB20059CDB3005DCEB40060CFB4006DD3B90085D9C2006CBE
      9F00469B73009AC5B00000000000000000000000000000000000000000000000
      0000000000000000000095A9E7005C7FE2006F99ED006D96ED006B93EC006991
      EB00678FE900658BE9006389E8006186E7005E82E6005D80E6005A7EE5005779
      E4005F83E6006F9AED00364ECA0000000000000000003048C7004965DD004562
      DC004360DC00415DDB003F59DA003C57D9003A54D8003850D700364DD500344B
      D5003048D4002F44D3002E44D300374FD7002C41C600747DD100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D5D5D500E3E3E300EAEAEA00FFFFFF0020BF8D0012C58F0012C58F0022B1
      7E0022B17E00EBBC9500ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE
      9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE
      9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700ECBE9700EBBC
      950047A6770015C08B0012C58F0012C58F0015BD8800FBFDFC00EAEAEA00E5E5
      E500D2D2D2000000000000000000000000000000000000000000000000000000
      0000D3D3D300A0A2A000EDF0EF00D6DEDA00863F3F00815F5F00815E5E00815E
      5E00805E5E00805D5D00805C5C007F5C5C0068666D0021A1C300ADEEF700B8F6
      FD00AAF3FC00A1F1FA0098EEF9008DEBF60083E8F40078E5F1006DE2EE0061DE
      EC0055D9E80047CFDF00249AAE001B5B6D006132320062323200623333006333
      3300633333006334340083393900CCD3D000CCD3D000CCD4D000E5E8E7005F5F
      5F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFEA00D4EBE400CBE7DF00C2E4
      DA00B8E1D500AFDDD000A5DACC009CD8C90093D5C5008BD3C20082D1BE007ACE
      BB0072CDB9006BCBB70061C9B4005AC9B20052C8B0004AC6B00049C9B1004DCA
      B20052CBB20055CCB20059CDB3005DCEB40060CFB40064D0B5006ED3B90086D9
      C20075C4A700479C74008DBEA500000000000000000000000000000000000000
      000000000000000000000000000095A9E7005C7FE2006F99ED006D96ED006B93
      EC006991EB00678FE900658BE9006389E8006186E7005E82E6005D80E6005A7E
      E5006085E700719BEE003750CC000000000000000000324AC8004A69DE004965
      DD004562DC004360DC00415DDB003F59DA003C57D9003A54D8003850D700364D
      D500344BD500324BD5003B55D9002F45C8007780D30000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DBDBDB00D6D6D600D8D8D8005FB391005FB391005FB391005DB1
      8F005DB18F00F0C49D00F3CBA300F3CBA300F3CBA300F3CBA300F3CBA300F3CB
      A300F3CBA300F3CCA300F3CCA300F3CCA300F3CCA300F3CCA300F3CCA300F3CC
      A300F3CCA300F3CCA300F3CCA300F3CCA300F3CCA400F3CCA400F3CCA400F0C5
      9E004CA87A005FB391005FB391005FB391005FB39100D8D8D800D6D6D600DADA
      DA00F6F6F6000000000000000000000000000000000000000000000000000000
      0000DFDFDF009C9C9C00EEF2F000D7DFDB0086403F0083616100825F5F008260
      6000825F5F00815E5E00815F5F00815F5F00805E5E0055717E001397B8000795
      BA000692B600068EB000068AAC000687A8000583A40005819E00047C9900047A
      97000476920006738D001C5D7000563335005E2F2F005E3030005E3030005F30
      30005F30300060313100853D3D00C6CDCA00A5A7A600CBD0CE00E1E4E3006767
      6700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFEA00D4EBE400CBE7DF00C2E4
      DA00B8E1D500AFDDD000A5DACC009CD8C90093D5C5008BD3C20082D1BE007ACE
      BB0072CDB9006BCBB70061C9B4005AC9B20052C8B0004AC6B00049C9B1004DCA
      B20052CBB20055CCB20059CDB3005DCEB40060D0B40064D0B50068D1B50070D4
      B8008ADBC3007EC9AE0040986E0075B092000000000000000000000000000000
      00000000000000000000000000000000000095A9E7005C7FE2006F99ED006D96
      ED006B93EC006991EB00678FE900658BE9006389E8006186E7005E82E6005D80
      E6006289E800729CEE003853CC000000000000000000344CC9004D6CDF004A69
      DE004965DD004562DC004360DC00415DDB003F59DA003C57D9003A54D8003850
      D7003850D700405ADA00324ACA007781D4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F5F5F5000000000000000000F5F5F5000000
      000000000000F2CAA100F8D4AA00F8D4AB00F8D4AB00F8D4AB00F8D4AB00F8D4
      AB00F8D4AB00F8D4AB00F8D4AB00F8D4AB00F8D4AB00F8D4AC00F8D4AC00F8D5
      AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F2CB
      A300FAF5F0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097979700F0F3F100D7DFDB008843420084636300836363008362
      620083626200826161008261610082606000815F5F00815F5F007E828000E9EC
      EA00717371007E5F5F007F5D5D007F5C5C007E5B5B007E5B5B007D5A5A006D71
      6F00F4F6F5006D716F00592C2C005A2C2C005A2D2D005A2D2D005B2D2D005B2D
      2D005B2E2E005C2E2E0087414000CDD4D100CAD2CE00D0D7D300DEE1DF006C6E
      6E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFEA00D4EBE400CBE7DF00C2E4
      DA00B8E1D500AFDDD000A5DACC009CD8C90093D5C5008BD3C20082D1BE007ACE
      BB0072CDB9006BCBB70061C9B4005AC9B20052C8B0004AC6B00049C9B1004DCA
      B20052CBB20055CCB20059CDB3005DCEB40060D0B40064D0B50068D1B50070D4
      B8008ADBC4007EC9AD0040986E0075B092000000000000000000000000000000
      0000000000000000000000000000000000000000000095A9E7005C7FE2006F99
      ED006D96ED006B93EC006991EB00678FE900658BE9006389E8006186E7005E82
      E600648AE900749EEF003A54CD000000000000000000364ECA004F6EE1004D6C
      DF004A69DE004965DD004562DC004360DC00415DDB003F59DA003C57D9003C56
      D900435FDB00364DCC007A84D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F1C79E00F8D3A900F8D3A900F8D4AA00F8D4AA00F8D4AA00F8D4
      AA00F8D4AA00F8D4AA00F8D4AA00F8D4AB00F8D4AB00F8D4AB00F8D4AB00F8D4
      AB00F8D4AB00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F1C8
      A100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000092929200F2F4F300D8DFDC008A46450085676700856666008465
      65008364640083646400836363008262620082626200816161007E828000F0F2
      F100717472007F616100805F5F00805E5E00805E5E007F5D5D007E5D5D006D71
      6F00E1E7E4006D716F00694141005A2F2F00572A2A00572A2A00572A2A00582B
      2B00582B2B00582B2B008B464500C1C8C500ABADAC00D0D7D400DADDDC007171
      7100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFEA00D4EBE400CBE7DF00C2E4
      DA00B8E1D500AFDDD000A5DACC009CD8C90093D5C5008BD3C20082D1BE007ACE
      BB0072CDB9006BCBB70061C9B4005AC9B20052C8B0004AC6B00049C9B1004DCA
      B20052CBB20055CCB20059CDB3005DCEB40060CFB40064D0B5006FD3B90087D9
      C20074C4A600479C74008DBEA500000000000000000000000000000000000000
      00000000000000000000000000000000000096AAE8006186E40074A0EF00719B
      EE006F99ED006D96ED006B93EC006991EB00678FE900658BE9006389E8006186
      E700658CE900759FF0003B56CE0000000000000000003750CC005072E1004F6E
      E1004D6CDF004A69DE004965DD004562DC004360DC00415DDB003F59DA00405C
      DA004C6BDF005F83E6003F59CF007A84D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F0C49B00F8D3A800F8D3A800F8D3A800F8D3A900F8D3A900F8D3
      A900F8D4A900F8D4AA00F8D4AA00F8D4AA00F8D4AB00F8D4AB00F8D4AB00F8D4
      AB00F8D4AB00F8D4AB00F8D4AB00F8D5AC00F8D5AC00F8D5AC00F8D5AC00F0C5
      9E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008D8D8D00F3F5F400D8E0DC008B494800876A6A00866969008668
      6800866868008567670084666600846565008365650083646400888C8A00F4F5
      F4007275730080636300816161008162620081616100806060007E6160007173
      7100CED6D2006D716F007E5C5C007D5B5B00704B4B00633A3A00572C2C005428
      280054282800552828008D4A4900CED5D200CBD3CF00D5DBD900D6D9D8007777
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEEFE900D4EBE400CBE8DF00C1E4
      DA00B7E0D500AFDDD000A6DACC009BD7C80094D5C5008AD3C10082D0BD007ACF
      BB0071CDB80069CBB50061CAB30059C8B10051C7B00049C6AF0049C9B1004DCA
      B10051CBB20055CCB20059CDB3005DCEB40060CFB4006DD3BA0086D9C2006BBE
      9F00459B73009AC5B00000000000000000000000000000000000000000000000
      000000000000000000000000000098ACE900648CE6007BA7F10076A1F000739E
      EF00719BEE006F99ED006D96ED006B93EC006991EB00678FE900658BE9006389
      E8006990EB0075A1F0003E58CF0000000000000000003853CC005475E2005072
      E1004F6EE1004D6CDF004A69DE004965DD004562DC004360DC00415DDB00415C
      DB004865DD005678E300658BE9003F5AD0007A84D60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EEC19A00F8D3A900F8D3A900F8D3A900F8D4A900F8D4AA00F8D4
      AA00F8D4AA00F8D4AA00F8D4AB00F8D4AB00F8D4AB00F8D5AB00F8D5AC00F8D5
      AC00F8D5AC00F8D5AC00F8D5AD00F8D5AD00F8D6AD00F8D6AD00F8D6AD00EEC3
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008E8F8F00F1F3F200D9E1DD008C4A4900886D6D00886C6C00886C
      6C00876B6B00866A6A00866A6A0086696900856868008568680090939100F5F6
      F5008A8C8A007C696900826464008264640082646400816262007C6665007F82
      7F00BBC5C000707371007F6060007F5F5F007F5E5E007E5D5D007D5C5C007452
      52006B4444005B32320091504F00BCC1BF00B0B3B100D6DDD900D1D4D3007D7D
      7D00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DFF0EB00D5ECE500CDE8E000C4E5
      DC00BCE2D800B4E0D400ACDDD000A3DBCD009CD8CA0093D6C6008BD4C30085D2
      C1007BD1BE0074D0BC006DCEBA0065CDB8005DCCB70056CBB60056CDB70057CD
      B60053CCB30055CDB20059CDB3005DCEB3006DD3BA0083D7C1005FB49400499B
      7500A8CDBA000000000000000000000000000000000000000000000000000000
      000000000000000000009AAEEA006890E7007EABF20079A6F10077A2F00075A0
      F000739EEF00719BEE006F99ED006D96ED006B93EC006991EB00678FE900658B
      E9006A92EC0077A2F0003F5AD00000000000000000003A54CD005677E3005475
      E2005072E1004F6EE1004D6CDF004A69DE004965DD004562DC004360DC00415D
      DB00415CDB004865DD005678E300658BE9003F5AD0007A84D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EEC09800F9D4A900F9D4AA00F9D4AA00F9D4AA00F9D4AA00F9D4
      AA00F9D4AB00F9D5AC00F9D5AC00F9D5AC00F9D5AC00F9D5AD00F9D5AD00F9D5
      AD00F9D5AD00F9D6AE00F9D6AE00F9D6AE00F9D6AE00F9D6AF00F9D6AF00EEC1
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008F8F8F00EEF0EF00DCE3DF00904F4E00876D6D00897070008A71
      710089707000876E6E00876E6E00876E6E00876C6C00876B6B009B989600EBED
      EC00CBCFCC00767776007C6B6A0080696800806868007B69690073757200AEB6
      B200A9B1AD007774720081636300806161007F6060007F6060007F6060007F5F
      5F007F5F5F006743430095575500CED6D200CBD3CF00DADFDD00CDD0CE008385
      8300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E5F2ED00DAEDE600D0E8E100C8E5DB00C0E2
      D700BADFD300B3DCD000ADDACD00A6D7C8009ED5C50097D2C20090D0BE008BCE
      BB0083CCB8007DCBB50077C9B30072C7B1006CC6AF0067C4AE006ECCB60076D5
      C10059CEB60055CCB20059CDB3006CD3BB007FD4BD0056AE8D004F9E7800B4D4
      C500000000000000000000000000000000000000000000000000000000000000
      0000000000009BB0EB006A93E80080AFF5007DABF2007BA7F10078A5F10077A2
      F00076A1F00074A0EF005C7FE2006F99ED006D96ED006B93EC006991EB00678F
      E9006C94EC0078A5F100415DD10000000000000000003B56CE005779E4005677
      E3005475E2005072E1004F6EE1004E6EE1004D6DDF003A53D0004562DC004360
      DC00415DDB00415CDB004865DD005678E300658BE9003F5AD0007A84D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ECBD9700F9D4AB00F9D4AB00F9D5AB00F9D5AB00F9D5AC00F9D5
      AC00F9D6AD00F9D6AD00F9D6AD00F9D6AE00F9D6AE00F9D6AF00F9D6AF00F9D6
      AF00F9D6AF00F9D7B000F9D7B000F9D7B000F9D7B000F9D7B100F9D7B100ECBF
      9900FDF9F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008F909000EAEDEC00DDE4E10095555300886F6F008D7575008D74
      74008C7474008B7373008B7272008A7171008A717100897070008D7B7B00B9BB
      BA00E6EAE800C9CDCA00919392007C7E7C007A7D7B008C8F8C00BCC1BE00CBD2
      CE007A7E7B007D69690082656500816464008164640081636300816363008163
      630080626200673F3F009C676600B6BAB800B4B7B600DCE2DF00C8CBC9008A8A
      8A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E4EFEB00D7E9E300CBE3DA00C2DED200B7D9
      CB00AFD4C400A4CFBD009ECBB70093C6B1008AC2AB0084BDA5007BB99E0073B6
      99006BB1930064AD8E005EA9880056A6830050A37E00429A73003D9D760072CD
      B7005CCFB70056CDB3006CD3BD007CD2BB004EA8860053A07C00C1DBCF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009CB3EC006C95E90083B3F60080AFF3007EABF3007DAAF2007BA7F10079A6
      F1007BA7F1006186E40095A9E7005C7FE2006F99ED006D96ED006B93EC006991
      EB006D96EC0079A6F100425FD20000000000000000003E58CF005A7EE5005779
      E4005677E3005475E2005274E2005879E400435FD3008491DC003A53D0004562
      DC004360DC00415DDB00415CDB004865DD005678E300658BE9003F5AD0007A84
      D600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EBBC9500FAD6AD00FAD6AD00FAD6AD00FAD6AE00FAD6AF00FAD6
      AF00FAD7AF00FAD7AF00FAD7B000FAD7B000FAD7B000FAD7B000FAD7B100FAD8
      B100FAD8B200FAD8B200FAD8B200FAD8B300FAD8B300FAD8B300FAD9B400EBBD
      9700FDF9F5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000090909000E7E9E800DFE5E2009A605E008A6F6F00927B7B00917A
      7A0090797900907878008F7777008E7676008D7575008D7474008C747400958C
      8B00AEB0AF00CED3D100D4DAD700DEE3E000E1E5E200D8DBD900B0B4B2007779
      77007B717000856A6A0084696900836868008368680082676700826666008266
      660081656500683B3B00A5797700CFD7D300CED5D200DFE4E200C3C6C5009494
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DDEBE400DBEAE200DAE9E200DAE9E200B0D1C100469C76006BC8
      B0005FD1B9006CD4BD0077CFB70046A27E0061A68500CDE2D800000000000000
      0000000000000000000000000000000000000000000000000000000000009FB3
      ED006E99EC0087B6F70082B2F50081B0F50080AFF3007EABF3007DABF2007EAB
      F200648CE60096AAE8000000000095A9E7005C7FE2006F99ED006D96ED006B93
      EC006F99ED007BA8F1004361D30000000000000000003F5AD0005D80E6005A7E
      E5005779E4005779E4005C7FE5004967D7008795DD00000000008491DC003A53
      D0004562DC004360DC00415DDB00415CDB004865DD005678E300658BE9003F5A
      D0007A84D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EABA9400FAD7B000FAD7B000FAD8B000FAD8B100FAD8B100FAD8
      B100FAD8B100FAD8B200FAD9B300FAD9B300FAD9B300FAD9B400FAD9B400FAD9
      B500FAD9B500FAD9B500FAD9B500FADAB600FADAB600FADAB600FADAB700EABA
      9500FDF9F5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000091919100E3E5E400E0E7E300A2706E008B6E6E0096808000967F
      7F00957E7E00947D7D00937C7C00937C7C00927B7B00917A7A00907979009078
      7800928282009A97940097999800868A8800787C7A00757775007C7675008472
      71008A71710089707000886F6F00886E6E00876D6D00866C6C00856A6A00856A
      6A008469690067373700AF8D8B00B5B9B700BCC0BE00E1E6E300BABBBA009D9D
      9D00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2E4DB004EA17C006EC9
      B10077D8C40070CAB100439F79006CAD8D00D6E7E00000000000000000000000
      00000000000000000000000000000000000000000000000000009FB5EE00719B
      ED0088BAF80086B6F70085B4F60082B2F50081B0F50080AFF30080AFF5006890
      E70098ACE90000000000000000000000000095A9E7005C7FE2006F99ED006D96
      ED00719BEE007CA9F2004562D4000000000000000000415DD1005E82E6005D80
      E6005C80E5005F83E7004C6BD8008796DF000000000000000000000000008491
      DC003A53D0004562DC004360DC00415DDB00415CDB004865DD005678E300658B
      E9003F5AD0007A84D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E9B89200FAD8B200FAD9B400FAD9B400FAD9B400FAD9B400FAD9
      B400FADAB500FADAB600FADAB600FADAB600FADAB600FADAB700FADAB700FADB
      B700FADBB800FADBB900FADCB900FADCB900FADCB900FADCB900FADCBB00E9B8
      9300FDF9F5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000092939200DEE1DF00E1E8E500AD84810083434300884D4C00874C
      4C00874C4B00874C4B0084474600824544008245440081444300814443008143
      4300814242008142420081424200804141008041410081444400844646008446
      46008446460083464500834444008440400084404000843F3F00843F3F00843F
      3F00833F3F007B232200BEAAA700CCD4D000CDD3D100E3E7E500B0B2B100A7A7
      A700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E3D9004EA17C0074CB
      B3006FC8AF003F9C750076B29600000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A0B6EE00729DEE008BBD
      F80089BAF80087B8F70086B6F70085B4F60082B2F50083B3F6006A93E8009AAE
      EA00000000000000000000000000000000000000000095A9E7005C7FE2006F99
      ED00739DEF007DAAF2004664D5000000000000000000425FD2006186E7005F83
      E6006389E8004F6FDA008998E000000000000000000000000000000000000000
      00008491DC003A53D0004562DC004360DC00415DDB00415CDB004865DD005678
      E300658BE9003F5AD0007A84D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E8B69000FBDBB700FBDBB800FBDBB800FBDBB800FBDBB800FBDB
      B800FBDCB900FBDCBA00FBDCBA00FBDCBA00FBDCBA00FBDDBB00FBDDBB00FBDD
      BB00FBDDBC00FBDDBD00FBDEBD00FBDEBD00FBDEBE00FBDEBE00FBDEBE00E8B6
      9100FDF9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000095959500D9DCDB00E5EAE700DAE1DD00D5D4D000D5D4D000D5D4
      D000D5D4D000D5D4D000D5D4D000D4D3CF00D4D3CF00D4D2CF00D3D2CE00D3D2
      CE00D7DBD800D8E0DC00D8E0DC00D8DFDC00D7DFDB00D7DFDB00D7DEDB00D6DE
      DA00D6DEDA00D6DDDA00D5DDD900D5DDD900D5DCD900D4DCD800D4DCD800D4DB
      D800D3DBD700D3DBD700D3DAD700D2DAD600D2DAD600E6EAE900A7A9A800B0B0
      B000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E3D9004C9F7A0050B0
      8E003E9A730086BBA10000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2B8EF00749FEE008DC0F9008BBD
      F9008ABCF80089BAF80087B8F70086B6F70087B6F7006C95E9009BB0EB000000
      000000000000000000000000000000000000000000000000000095A9E7005C7F
      E200759FEF007EABF3004967D50000000000000000004361D300648AE800688D
      E9005273DB008B9BE10000000000000000000000000000000000000000000000
      0000000000008491DC003A53D0004562DC004360DC00415DDB00415CDB004865
      DD005678E300658BE9003F5AD0007A84D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7B48F00FBDDBB00FBDDBC00FBDDBC00FBDEBC00FBDEBD00FBDE
      BD00FBDEBE00FBDEBE00FBDEBE00FBDEBE00FBDFBF00FBDFBF00FBDFBF00FBDF
      C000FBDFC100FBE0C100FBE0C100FBE0C200FBE1C300FBE1C300FBE1C300E7B5
      8F00FDF9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A6A6A600B6B7B600EFF1F000F1F3F200F1F3F200F1F3F200F1F3
      F200F1F3F200F1F3F200F1F3F200F1F3F200F0F3F100F0F3F100F0F3F100EFF2
      F100EFF2F100EFF2F100EFF2F000EFF2F000EFF2F000EFF1F000EFF1F000EFF1
      F000EEF1F000EEF1F000EEF1F000EEF1EF00EEF0EF00EEF0EF00EDF0EF00EDF0
      EF00EDF0EF00EDF0EF00EDF0EE00EDF0EE00EDEFEE00E6E9E80082828200CFCF
      CF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1E4DA003D926900318E
      620096C4AD000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000638AE7008FC3FA008DC0FA008CBF
      F9008BBDF9008ABCF80089BAF80088BAF8006E99EC009CB3EC00000000000000
      00000000000000000000000000000000000000000000000000000000000095A9
      E7005E82E2006C95E9008A9DE3000000000000000000869AE2005E81E3005677
      DD008E9FE2000000000000000000000000000000000000000000000000000000
      000000000000000000008491DC003A53D0004562DC004360DC00415DDB00415C
      DB004865DD00587BE4006890EB002234BD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E6B28D00FBE0C000FBE0C000FBE0C000FBE0C100FBE0C200FBE0
      C200FBE0C200FBE0C200FBE1C300FBE1C400FBE1C400FBE2C400FBE2C400FBE2
      C500FBE2C500FBE2C500FBE2C700FBE3C800FBE3C800FBE3C800FBE3C900E6B3
      8D00FDF9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000929392008F8F8F008E8E8E008E8E8E008E8E8E008E8E
      8E008E8E8E008E8E8E008E8E8E008E8E8E008E8E8E008E8E8E008E8E8E008E8E
      8E008E8E8E008E8E8E008C8C8C008B8B8B008A8A8A0089898900878787008686
      86008585850084848400838383008181810080808000808080007F7F7F007E7E
      7E007C7C7C007B7B7B007A7A7A007979790078787800797979009E9E9E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDEBE5006EAE8F00A1C9
      B400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2B9EF0075A1EF008FC2FA008DC0
      FA008CBFF9008BBDF9008BBDF800719BED009FB3ED0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B7C3EE008C9FE500D4DCF4000000000000000000D3DAF300869AE200B3BF
      EB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008491DC003A53D0004562DC004360DC00415D
      DB00425EDB005072E1003F59D0007D87D7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B18B00E5B1
      8B00FDFAF6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A2B9EF0075A1EF008FC2
      FA008DC0FA008DC0F900729DEE009FB5EE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008491DC003A53D0004562DC004562
      DD004B6ADF003C57D0007D89D800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A2B9EF0075A1
      EF008FC3FA00749FEE00A0B6EE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008491DC003A54D0004F6E
      E100405AD100818CDA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A2B9
      EF00638AE700A2B8EF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008491DC002F45
      C600818EDB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AC957300754D0F00744C1100744B
      0F00754C1100754D0F00754C0F00754D0F00754D1200764D1200754C0F00754C
      0E00754C0F00754D0F00754B0F00754D0F00754C0F00754D1100744C1100AB95
      7100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CBE8D4009ED3A7007DC17D007CC07C007EC27E0082C48300ACD4AC00CEE1
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754C110081571700825717008157
      1700815616008157170080561600815717008156140082571700815717008157
      170080561600815717008056160081571700825716008157160081561600744C
      0E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEEFE20047BC
      8F002BA96700209A400008850F0000800100018203002E98300080B48100589D
      5A005EA06000DDEBDD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754D12008D5E19008D5E19008D5F
      1A008D5F1A008C5D1A008D5E19008E5F1A008D5E1C008D5F1A008E5F1A008D5E
      1B008D5E1A008C5D1A008D5F1A008D5F1A008C5F1A008D5F1C008D5E1900754C
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DFEEE1002A9B5B002DAE
      8C002BA77700259B530009801000017B0200027D0400208C22007CAC7D005696
      5700368839002B872E00E0EDE100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754D12009B691E009C691E009A69
      1E009B691E009A691D009B681E009A691D009C6A1E009B691E009A691D009B68
      1E009B6A1E009B691E009B691D009B691E009B681E009B691D009A681C00754C
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003F944A00218C4A00289A
      6C002AA58600289B65000477090001770300027904001282150078A879005392
      5500358438001D7E2200459B4900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754C0F00AA742200AA732100AA74
      2100AB742200A9742200AA742200AB742300B4792200BD802600BC812600B279
      2400AA732300AA742200AA732300A9732100AA732100AA732100AA732100754D
      1200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CBDFCB000A6C1200197D3200238B
      5200289A70001A904000067F0700067F07000681070007820800429543004D8F
      5000338237001D7C22000F7F1600D2E9D3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000858BB900232985005D5DA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006666B9005353C9009797
      D000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754C0F00B97F2700BA7F2700BA7E
      2700BA7E2700B97D2500BA7F2700BB802700C58628009B691B00A56F1F00C586
      2900BB7F2700BA7F2700BA7F2700BA7E2600BA7E2600BB7F2700BB7F2700744C
      1100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008EB98E00015F01000E6D1900197B
      36001C8D37000086000000880000008A0000008B0000008B0000008A00002D8E
      2F002B7E3000197B1F000E7E15009ECFA3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008E8E
      C600263479001C2577000E1370003D3D8E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003E3E90002B2BA1006868E2002B2B
      AA008E8EC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754D0F00C98B2C00C88A2A00C989
      2A00C9892A00C98A2C00C8892900C98B2C00C58828008A5C170094641A00C98B
      2C00C8892900C98A2B00C98B2C00C98A2C00C88A2A00C98A2C00C98A2B00754D
      1200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006AA26A00005D0000006000000062
      00000F890F00028E04000490070005920A0006930C0007940D0007930D001594
      1900077C0D00087F0F00098311007CBD81000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008E8EC6000000
      81000A0E7F004E59A00012167400000061003939860000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003939870000006500111282008186E4000B0B
      8B00000081008E8EC60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754C1100D7932F00D6932F00D693
      2F00D7932E00D6932F00D6932D00D7932E00D7932F00D2902C00D3902D00D693
      2F00D7932E00D7932F00D7932E00D7932F00D7932F00D7933000D6932D00754C
      1100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000689F6800005B0000005E00000061
      000018901B0008950F000A9814000D9B19000F9D1D00109E1F00119F20001F9C
      2B00077B0E00087E10000982130079BB7F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008E8EC600000081000000
      81005860BD00636AF2007F89D800070868000000550037378100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000363681000000580007086D007F89D900656CF3005860
      BD0000008100000081008E8EC600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754B0F00E29C3100E39B3100E29C
      3100E39B3100E39C3300E39C3300E29C3200E29B3100E39B3100E39C3200E39C
      3300E39C3100E39C3300E39C3300E39C3200E39C3100E29B3100E39C3300744B
      0F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000679F6700005A0000005D0000005F
      01001F9526000E9C1C0012A0220015A4280017A72D0019A930001BAA33002BA6
      3D00077A0F00097D10000A8113007ABC80000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008E8EC60000008100000081005860
      BD004E54F1000000F5001B1DF5007E88D50007085B0000004B0034347C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000034347B0000004B00070860007E88D7001B1DF5000000F5004E54
      F1005860BD0000008100000081008E8EC6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000754D0F00FFC43D00FFDE4100FFDD
      4200FFDE4100FEDD4200FEDD4200FFDD4200FFDE4100FFDD4300FFDE4100FFDE
      4100FFDD4100FFDE4200FFDD4200FFDD4200FFDE4100FFDE4100FFC33B00754C
      1000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000679F6700005A0000005D0000015F
      0100289D320014A3260018A72D001BAB34001FB03C0023B4430026B7480038B2
      500008780F00097C12000B7F140079BB80000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008D8DC5000000810000008100585FBD004D54
      F1000000F5000000F5000000F5001A1DF5007E87D30007084E00000042003232
      7900000000000000000000000000000000000000000000000000000000000000
      00003232780000004200070852007E88D4001B1DF5000000F5000000F5000000
      F5004E54F1005860BD0000008100000081008E8EC60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AC957100744C1100754D1100754B
      0F00754D0F00754C0F00754D1100744C1100744C0E00754D1200754B0F00754D
      0F00754D1100754D0F00754D1200754C1100754B0F00744C0E00754D1200AB93
      7000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000679F6700005A0000015C0100015F
      020030A13C0019A82F001DAD370022B2400026B849002BBD510030C2590044BC
      6200097811000A7C12000B7F15007CBB80000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009696CA001B257C000B0F7F00555EBD004B53F1000000
      F5000000F5000000F5000000F5000000F5001A1CF4007B86CE00070840000000
      3C00323279000000000000000000000000000000000000000000000000003131
      770000003A00070844007D87D0001A1DF5000000F5000000F5000000F5000000
      F5000000F5004E54F1005860BD001111890016168C009898CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000054737B00FBFCFC00EDF3
      F100EFF3F300EEF3F300EDF2F100EBF1EF00E9F0EF00E6EFEF00E4EDEC00E1EB
      EB00DDE9E900DCE8E700D9E7E600D7E6E500DDEAE900EBF3F20054747D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000679F6700005A0100015C0100015F
      030036A645001CAC350021B23E0026B748002BBD510030C25A0035C863004CC2
      6E00097811000A7C14000B7F16007ABA81000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000022278900212D7B004F5EA2005C67F2000000F5000000
      F5000000F5000000F5000000F5000000F5000000F500191CF4007785CA000607
      3500000038003131790000000000000000000000000000000000313177000000
      3500060737007A85CC001A1CF4000000F5000000F5000000F5000000F5000000
      F5000000F5000000F500636BF4006369B90039399D002C2C9500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000055747D00FBFCFC00EEF2
      F200EEF3F200EEF3F200EDF3F200ECF2F000EAF1EF00E8EFEF00E6EFEE00E3ED
      EB00E1EBEA00DEEAE800DBE7E700DAE8E700D7E5E400E3EFED0054737C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000679F6700015B0100015E02000260
      03003CA94B001FAF3A0023B4430028BA4C002DBF540031C45C0035C864004FC3
      6F00097813000A7B15000C7F17007AB982000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005C60A800212D7B00161E83007384DA00181CF1000000
      EF000000EF000000EF000000EF000000EF000000EF000000EE00181CEE007382
      C40006062B000000360031317900000000000000000030307600000032000606
      2D007683C600191CEE000000EE000000EF000000EF000000EF000000EF000000
      EF000000EF00191CF1007C89DB004949A5005959AD006C6CB500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000054737C00FBFCFC00FBFB
      FB00FBFDFD00FBFCFC00FBFBFB00FBFCFC00FAFDFB00F9FBFA00F9FBFB00F9FA
      FB00F8F9F900F7FBFB00F7FAFA00F6F9F900F4F8F900F4F8F70054747D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000068A06900015C0200015F03000261
      040042AD510020B13D0025B6450029BA4D002DBF550031C35B0034C7610051C2
      6F000A7914000B7C15000C7F18007BBA82000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000044479E000000810006078500697ED900161B
      EC000000E9000000E9000000E9000000E9000000E9000000E9000000E800171B
      E8006E7FBE0006062500000034002C2C6D002D2D6F0000003000060625007181
      C000181CE8000000E8000000E9000000E9000000E9000000E9000000E9000000
      E900191CEC007685D90007078500030382006868B40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5B6BA006A868E006886
      8C006A868E0069878D0069868E006A878E0069878D006A878E0069868C006986
      8D0069868E006A868E0069868C006A878D0069868D0069878D00A5B6BB000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000069A26900015E0300025F04000362
      050046AF550021B13E0025B6450029BA4D002CBE53002FC1580031C45D0054C1
      71000B7914000C7C17000D8019007AB980000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004545A20000008100060785005F7B
      D800141AE6000000E3000000E3000000E3000000E3000000E3000000E3000000
      E100151AE100647BB90006061F00000023000000210006061E00687CBB00161B
      E2000000E1000000E3000000E3000000E3000000E3000000E3000000E300171B
      E6007082D80006078500000081004545A2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BFDDF20050A4DF00309F
      F5002FA3FA0030A2FA002FA2FA002FA2FB002FA2FB0030A2FA002FA2FA002FA2
      FA002FA2FA0030A2FA0031A3FB002FA2FB002F9CEE0074B7E400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CCE0CC0092BB92006CA36C006AA06A00689F6800689F6800689F
      6800689F6800689F680068A0680068A269002E7C2F00015F0300026105000363
      05004AB15A0020B13D0024B5440027B94B002ABC50002DBF55002FC1580055C1
      70000B7916000D7D18000E801A00419C4B007CBC83007DBE85007FC0870081C2
      8A0081C38B0083C68E0085C9910087CA920087CD9400A8DBB300D6EEDB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AFAFD0008989B7007F7FAF007F7FAE007F7FAE007F7FAE007F7F
      AF007F7FAF007F7FB0007F7FB1007F7FB2007F7FB3007F7FB5007F7FB6007F7F
      B7007F7FB9007F7FBB007F7FBC007F7FBE007F7FC0007F7FC1007F7FC3007F7F
      C5007F7FC7007F7FC9007F7FCB007F7FCD007F7FCF007F7FD1007F7FD3007F7F
      D6007F7FD8007F7FDA007F7FDD007F7FDF007F7FE1008A8AE500AFAFEE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004545A200000081000507
      85005577D6001219E0000000DC000000DC000000DC000000DC000000DC000000
      DB000000D9001319DA005A77B50005061300050613005C78B600141ADB000000
      DA000000DB000000DC000000DC000000DC000000DC000000DC00151AE000657C
      D60006078500000081004545A200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DBEBF70081BEE7002F97E0002E9EF00030A5
      FB0030A5FB0031A5FB0031A5FB0030A5FB002FA4FA0030A5FB0031A5FB0030A5
      FB0030A5FB0030A5FA0031A4FB002FA4FA002E9EF10068B1E200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003D8F3D000066000000610000005E0000005C0000005B0000005A0000005A
      0000005A0000005B0000005C0000015D0100015F030002600400036205000364
      06004FB35E0020B03C0023B4420026B7470028BA4C002ABC50002CBE520058C1
      70000C7B17000D7E19000E811B000F841D0011871F00128A2200148D25001590
      270016942A0017972D00199B30001A9E33001DA236001EA6390020AA3D005BC1
      710000000000000000000000000000000000000000000000000000000000BFBF
      DC004B4B9A001515770002026700000065000000630000006300000063000000
      640000006500000066000000680000006A0000006D0000006F00000071000000
      74000000780000007B0000007E0000008100000085000000880000008C000000
      8F00000093000000970000009B0000009F000000A3000000A8000000AC000000
      B0000000B5000000B9000000BE000000C3000000C7000202CA001515D3004B4B
      E000C0C0F4000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004444A3000000
      8300050687004D74D6001118DB000000D6000000D6000000D6000000D6000000
      D6000000D5000000D2001219D4005073B1005274B2001219D4000000D3000000
      D5000000D6000000D6000000D6000000D6000000D6001219DB005A79D6000607
      8700000083004444A30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDDCF2002C92D9002F9EEC0030A6FB0030A6FB0032A6
      FA0031A6FB0031A6FA0030A6FB0031A6FB0032A7FB0031A6FB0031A6FB0030A6
      FA0032A6FA0033A7FB0030A6FB0030A6FB0030A1F3005CAAE100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000127E
      1200006E0000006700000063000000600000005E0000005D0000005C0000005C
      0000005D0000005E0000015F0100015F02000161030002620500036406000365
      070052B460001EAF3A0021B23F0024B5440026B7480028B94B0029BA4D005AC0
      71000C7B17000D7F1A000E811C0010841E0011882100128A2300148E26001691
      290017952B0018982E00199B30001B9E34001EA338001FA73B0021AA3F0023AF
      420035B653000000000000000000000000000000000000000000C0C0DF002828
      8D000000720000006E0000006800000065000000640000006400000064000000
      650000006600000067000000690000006B0000006D0000007000000072000000
      75000000780000007B0000007E0000008200000085000000880000008C000000
      9000000093000000970000009B0000009F000000A3000000A7000000AC000000
      B0000000B4000000B9000000BE000000C3000000C7000000CA000000D1000000
      D4002828DE00C0C0F50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004343
      A40000008500040688004B72D5001018D5000000D0000000D0000000D0000000
      D0000000D0000000CE000000CB000D13CC000D13CD000000CB000000CE000000
      D0000000D0000000D0000000D0000000D0001118D5004E73D500050688000000
      85004343A4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6E9F6002D95DB0030A2F20031A8FB0032A6FA0032A7FA0032A7
      FA0032A8FB0032A7FB0032A7FB0032A7FB0032A6FA0031A8FB0032A7FB0032A7
      FA0032A7FB0032A7FB0032A7FB0032A7FB0031A6F8002C95DB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000439F43000075
      0000006E000000690000006500000063000000610000005F0000005F0000005F
      0000005F00000060010001600100016103000262040003640600036507000467
      080056B664001DAD37001FB03C0022B2400024B4430025B6460026B747005DC0
      71000D7D19000E7F1B000F821E001085200012892200138B2500148F27001692
      290017952D0019982F001A9C32001C9F36001DA3390020A83C0021AB400024AF
      430026B3470060C97900000000000000000000000000000000004D4DA6000000
      79000000730001017000111175001B1B78001C1C77001C1C77001C1C77001C1C
      78001C1C79001C1C7A001B1B7B001B1B7D001B1B7F001B1B81001B1B83001B1B
      86001C1C88001C1C8B001C1C8D001C1C91001B1B94001B1B96001B1B99001B1B
      9D001B1BA0001B1BA4001B1BA7001B1BAA001B1BAE001B1BB1001B1BB5001B1B
      B9001B1BBD001B1BC1001B1BC5001B1BC9001A1ACD001111D1000101D2000000
      D4000000D9004D4DE70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004343A5000000870004068A004770D4001018D0000000CB000000CB000000
      CB000000CA000000CA000000C7000000C5000000C5000000C8000000CA000000
      CB000000CB000000CB000000CB001018D0004B71D40004068A00000087004343
      A500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007EBCE7002FA0EE0034A9FA0034ABFC0033A9FA0032AAFB0033AA
      FB0033AAFB0032A9FB0032AAFB0034A9FA0033A9FA0033AAFB0032AAFB0033AB
      FB0034A9FA0034AAFA0034A9FA0032AAFA0032AAFB00319EE90090C5E9000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D4E9D400007D00000075
      0000006F00001F7D1F00549F54004DA14D004CA24C004CA44C004CA54C004CA7
      4D004DA950004FAA520050AB540052AD580054B05B0056B25E0057B4620059B5
      640038B04A001BAB33001DAD38001FB03B0021B23E0022B3410023B442003FBA
      58005FC072005FC071005FC070005DBE6E005BBD6B005ABC690058BA660056B8
      620055B75E0052B45A0051B257004FB1540050B055005DB768003AB4550025B0
      450026B4490028B84C00DBF2E0000000000000000000B0B0D80015158B000000
      7B0001017600262687003C3C9A0032329B0032329E003232A2003333A5003434
      AA003636AE003636B1003737B5003838B9003838BC003838BF003636C2003434
      C4003333C7003131C9003030CB003030CE003030D0003232D1003232D2003333
      D3003333D4003333D3003333D2003333D1003333CF003333CC003333CA003333
      C7003232C3003232BF003232BB003232B7003232B3003C3CBC002525D3000101
      D8000000DC001515E100B0B0F500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004242A6000000890004068C00446FD4000F18CA000000C4000000
      C4000000C4000000C3000000C2000000C1000000C1000000C3000000C4000000
      C4000000C4000000C4001018CA004770D40004068C00000089004242A6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000098C8EB00309AE30034ACFA0034ABF90034ACFA0034ADFC0034ACFA0034AC
      F90034ACFA0034ACFA0034ACF90034ADFB0034ADFC0034ACFA0034ACF90034AC
      F90034ACFA0034ACF90032ACFA0034ACF90034ACFA0034AAF8002D96DB000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A1CEA100007C00000075
      0000007100005CA65C000B840C000F8A11001490160018961B001E9A210024A1
      28002CA7310032AC3A0040B2480047B7520047B7520047B9550046B9550047BA
      560047BB59004ABD5C0055C1680048BE5D0029B242001FB03B0020B03C0020B0
      3C001FB03C001EAF3A001DAD37001BAA330018A82E0016A5290012A123000F9D
      1C000B98150007940D0003900700008C0000008700000082000063BA6D0025B1
      460027B44A0029B94E00AFE4BD0000000000000000008B8BC700020282000000
      7C00131380004949A5001F1F9C001818A1001E1EAA002323B2002828BA002C2C
      C0003131C6003232CA003535CE003737D2003939D6003A3AD8003A3ADB003A3A
      DD003A3ADE003636DF003131E0002A2AE0002222DF001F1FDE001919DC000F0F
      DA000606D8000202D6000303D3000404D0000505CC000505C7000505C0000505
      BA000505B3000505AB000505A30005059A000505910010108D003F3FBC001212
      DC000000DD000202E0008B8BF100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004141A70000008B0004068E00416DD3000E17C5000000
      BE000000BE000000BE000000BE000000BD000000BD000000BE000000BE000000
      BE000000BE000F18C500436FD30004068E0000008B004141A700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D7E9
      F6002E95DC0034ABF60036AEFA0036AEFB0035ADF90036AEFA0036AEFA0035AD
      FA0036AEFA0035ADFA0035ADFA0036AEFA0034AEFA0035AEFA0036AEFB0036AE
      FA0035AEFB0035ADF90035AEFA0036AEFA0036AEFB0036AEFB00319FE9009DCC
      EC00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007FBE7F00007D00000077
      00000072000076BD7A003FAA46003FAB46003FAD46003FAF46003FB146003FB2
      460040B4490042B54B0043B64D0044B8500045BA520047BB550048BD570049BE
      5A004BBF5C004BC05E004CC15F0057C569006DCE7F0072CF82006DCC7F0069CB
      7A0064C8760042BA57001AAA330018A82E0016A52A0014A22500119F20000D9B
      1A000A97130006930C00038F0600008C000000870000008200005AB05F0026B2
      480028B54C002ABA500094DCA70000000000000000008080C300000085000000
      7D001F1F89005C5CBC003F3FBE004040C2004141C6004141C9004141CC004141
      CF004141D1004040D4004040D6004040D9004040DB004141DD004141DF004242
      E2004444E3004747E5004A4AE7004E4EE8005252E9005454E8005252E8004C4C
      E5004141E1003333DC001F1FD4000909CD000101C7000101C2000000BD000101
      B8000000B1000000AA000000A30000009A0001019300050589003939B0001E1E
      DD000000DE000000E3008080F100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004141A90000008D00040690003E6CD2000A12
      BF000000B8000000B8000000B8000000B8000000B8000000B8000000B8000000
      B8000B12BF00416DD2000406900000008D004141A90000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007EBC
      E60031A5ED0037B0FA0036B0FA0035AFF90037B1FB0037B0FB0037B1FB0036AF
      FA0036B0FA0036B0FA0036AFFA0037B0FA0036AFFA0035AFFA0037B0FA0037B1
      FA0037B1FB0036B0FA0036AFFA0036B0FA0037B1FB0037B0FB0035ADF600479F
      DD00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007EBE7E00007E00000079
      0000007400007AC07E0042AD4B0042AE4B0042B04B0042B14B0042B34B0042B5
      4B0043B64D0044B74F0045B8510046BA530048BB560049BC58004ABE5A004BBF
      5C004CC05E004DC160004EC261004EC362004FC363004FC364004FC464004FC4
      640051C366006FCE800083D4900079CF86005DC169001EA42D000F9D1C000C99
      16000895100005920A00028F0500008B000000860000008200005EB2630027B3
      4A0029B64D002BBB510095DCA90000000000000000007F7FC200000086000000
      7F0021218C006363C1004444C2004242C4004242C7004242CA004242CC004242
      CF004242D1004242D4004242D6004242D8004242DB004242DD004242DF004242
      E0004242E2004141E3004040E4004040E6004141E6004343E7004646E8004B4B
      E8005252E8005959E8005E5EE6005B5BE1004A4AD8002828CA000E0EBD000404
      B4000000AE000000A8000000A10000009A0000009200060689003C3CB1002020
      DF000000DF000000E4007F7FF100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004141AA0000008F0003069200537BD700191F
      BE000000B2000000B2000000B2000000B2000000B2000000B2000000B2000000
      B2000A12B9003E6CD2000406920000008F004141AA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000083BFE80031A0
      E70038B1FA0038B2FB0038B2FB0039B2FC0039B2FB0038B2FB0038B2FB0038B2
      FB0038B2FB0038B3FA0038B1FA0037B1FA0039B3FA0038B2FB0039B1FB0038B2
      FB0039B3FB0039B2FB0038B2FB0038B2FB0039B1FB0038B2FB0038B2FB002F99
      E000D8EAF6000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007EBE7E00007F0000007B
      00000076000080C3840045B04F0045B24F0045B34F0045B54F0045B64F0045B8
      4F0045B9500046BA520047BB540048BC56004ABD58004BBF5A004CC05C004DC1
      5E004EC260004EC361004FC4620050C4630050C5640050C5640050C5650050C5
      650050C5640050C463004FC462004FC361005EC86E007FD38B007CCD850065C1
      6C00159A1B0004910800018E0300008A0000008600000081000062B4680027B4
      4B002AB84F002CBC530095DCAA0000000000000000007F7FC300000088000000
      810022228E006767C4004747C5004545C7004545C9004545CC004545CE004545
      D1004545D3004545D5004545D7004545D9004545DB004545DD004545DF004545
      E1004545E2004545E3004545E4004545E5004545E6004545E6004444E6004444
      E6004444E5004444E5004545E4004B4BE3005757E4006464E3006262DC004747
      CC002424B8000C0CA80000009E000000980001019200060688003F3FB2002121
      E0000000E0000000E6007F7FF200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004141AB000000910003069400416DD7003139CB003A3A
      C9003938C7002C2CC3002121BE003434C3004F4FCA006666CF005251C7000606
      AD000000AB000D17B5003B6AD20003069400000091004141AB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E0EEF80092C6EA002D94DA0031A4EB0039B5
      FC003AB4FB0039B4FB003AB5FB003AB5FB003AB5FB0039B4FB003AB5FB003AB4
      FB003AB5FB0038B4FB0038B5FA003AB5FB003AB5FB003AB4FB0039B5FA003AB5
      FB0039B4FB0038B5FA0039B6FA003AB4FB003AB5FC003AB5FB003AB5FC0031A2
      E900A2CEED000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007FBF7F0000800000007C
      00000179010090CC950049B2540049B4540049B5540049B6540049B7540049B9
      540049BA54004ABB56004BBC58004CBD5A004DBE5B004EBF5D004FC05F004FC1
      600050C2620051C3630052C4650052C4650052C4660053C5670053C5670053C5
      670052C4660052C4650051C4640051C3630050C261004FC160004EBF5E0056C1
      640081D0890070C574001C991E000089000000850000008100006AB76F0029B5
      4E002BB951002CBC540095DEAA0000000000000000008080C40001018A000303
      86002D2D96007171CA004949C7004949C9004949CB004949CD004949D0004949
      D2004949D4004949D6004949D8004949DA004949DC004949DE004949DF004949
      E0004949E2004949E3004949E3004949E5004949E5004949E5004949E5004949
      E5004949E5004949E4004949E3004949E1004747E0004646DE004E4EDE005D5D
      DE006464DA005555CB003434B3000D0D9A0001018F00060687004646B4002B2B
      E3000303E2000101E7008080F300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004242AD000000930003069600436ED700373ECB003030C5003030
      C5003030C5003030C5003030C5003030C4003030C4003232C5005858D2008B8B
      E1008C8BDD004040BE000C17AF00396BD10003069600000093004242AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000081BDE7002F99DE0034A6EB0038B2F8003AB6FB003AB8
      FB003AB8FB003AB7FA003AB6FB003AB7FB003AB7FB003AB6FB003AB8FB003AB8
      FB003AB8FB003AB7FB003AB6FB003AB8FB003AB7FB003AB6FB003AB7FB003AB7
      FA003AB7FA003AB7FB003AB8FB003AB6FA0039B6FB003AB6FB003BB7FB0032A5
      EB0090C5EA000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A3D2A300098609002A93
      2A004FA34F00AAD7AD0054B960004DB659004DB859004DB959004DBA59004DBB
      59004DBC59004EBD5A004EBE5C004FBF5D0050C05F0051C1600052C2620052C3
      640053C4650054C4660054C5670054C5670055C5680055C6680055C6680055C6
      680055C5680054C5670054C4670054C4650053C3640052C2620051C1600050C0
      5E004FBF5D0050BD5D0077CC7F0070C27500319D31000080000097CE9C0069CA
      82004EC56D0034C05C00B4E8C40000000000000000008B8BCB000D0D90002323
      97005353AC009797D6005D5DCF004D4DCC004D4DCF004D4DD0004D4DD2004D4D
      D5004D4DD7004D4DD8004D4DDA004D4DDC004D4DDD004D4DDF004D4DE1004D4D
      E2004D4DE3004D4DE4004D4DE4004D4DE5004D4DE5004D4DE5004D4DE5004D4D
      E5004D4DE5004D4DE4004D4DE3004D4DE2004D4DE0004D4DDF004C4CDD004A4A
      DA004D4DD9005757D9006262D6005C5CC7003232A60019198F006C6CC7005151
      E9002222E4000D0DE7008B8BF400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004343AF000000950003069800456FD8003E45CD003737C7003737C7003737
      C7003737C7003737C6003737C4003737C1003737C1003737C4003737C6003737
      C7003B3BC7007A79DC009C9BE4008C90DC00507AD60003069800000095004343
      AF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A7D1EE0032A3E7003CB8FA00499FD2003FB4F4003BB9FA003CBA
      FB003CB9FD003EC2FF003FCAFF003FD3FF003FCAFF003DC2FF003BB9FB003DB9
      FB003DB9FB003DB9FC003DB9FB003BB9FB003DB9FB003CB8FB003BB7FB003CBA
      FB003CB8FB003CB8FA003CB9FC003DB9FB003DB9FB003DB9FC003CBAFB0035AA
      ED0081BDE7000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D4E9D4002093200042A1
      420064AF640098CA9800AFD9B100A2D6A6009FD6A4009FD6A4009ED6A3009DD6
      A2009DD6A1009BD6A1009AD69F0098D69E0097D59E0096D59E0095D59D0094D5
      9C0072CD800056C6670057C7680057C7690057C8690058C8690058C8690071CF
      810095D89F0099D9A2009CDAA4009EDBA700A1DCA900A4DDAC00A7DEAF00AAE0
      B100ADE0B400B1E1B600B4E2B900B8E3BD00C8E9CC00AFDCB400A0D9AE007BD0
      910062CB7F0048C86D00DBF3E2000000000000000000B3B3DD003131A2004242
      A9006565B7009898CF009B9BD9007878D0007474D0007474D1007474D2007474
      D3007474D4007474D5007474D6007474D7007474D8007474D9007474DA007474
      DB007474DC007474DD007474DE007474DF007474DF007474E0007474E0007474
      E1007474E1007474E1007373E1007373E1007373E1007373E1007373E1007373
      E1007373E0007272DF007272DF007979E1008F8FE2008D8DD8008A8AE7006262
      EA004040EB003131EB00B3B3F700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004343
      B1000000970003069A004770D900444ACF003E3EC9003E3EC9003E3EC9003E3E
      C9003E3EC9003E3EC7003E3EC2006065CC006064CB003E3EC1003E3EC6003E3E
      C8003E3EC9003E3EC9003E3EC9005554D0009B9DE9007A96E30003069A000000
      97004343B1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006DB3E30036AFF1003EBCFA0045AFE90041BEFD003FC5FF0041D0
      FF0044DAFF0042E0FF0037C4F10033B2E80035B9EE0042D1FF003FC4FF003EBC
      FC003EBCFB003CBBFA003DBCFB003EBCFB003DBBFB003DBBFB003CBBFA003DBB
      FB003EBCFA003EBCFB003FBDFC003DBBFA003DBCFB003FBCFB003FBDFD0033A8
      EC008EC4E9000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000064B3640058AC
      58007ABC7A007CBC7C0055A65500007800000077010001760300027604000275
      0500037506000475080005750A0006750B0006760D0007780E00097810000A7A
      120096D79F0058C76A0059C86B0059C86C0059C86C0059C86C0059C86C0099DA
      A300138B2500148D270016902A0017922C0018952F001A9731001B9B34001D9E
      37001FA13A0021A53E0022A9410024AB440025AE47006BC783008CD49F008CD7
      A10075D28F007FD79900000000000000000000000000000000007070C0005555
      B2007575BF007878C1005C5CB20030309B002828960028289600282896002828
      96002828960028289700282898002828990028289A0028289C0028289D002828
      9F002828A1002828A3002828A5002828A7002828AA002828AC002828AE002828
      B1002828B4002828B7002828B9002828BC002828C0002828C3002828C6002828
      CA002828CD002828D0002828D4002727D7002E2EDC005A5AE5007575E9007272
      EA005454EC007070F20000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004444B3000000
      990004069C004971DA004C51D1004646CB004646CB004646CB004646CB004646
      CB004646CA004646C6006B6FD000708FD6006D8DD400686DCD004646C5004646
      CA004646CB004646CB004646CB004646CB004646CB004F54D2006586DF000406
      9C00000099004444B30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AED4EF0033ABE90046E7FF0049EDFF0045E8FF0040DBFB003DD1
      F70032B0E6002D97DB0086C0E800B3D7F00081BDE70037B2F00042CAFF0041BE
      FD0040BFFD003FBEFB0041BEFD003FBEFB003FBDFB0040BDFC0040BFFC003FBE
      FC0040BFFD0040BFFC0040BFFC0040BFFC0041BEFC003FBEFC003FBEFC0034A7
      EA009BCAEB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E5F2E50071B9
      710088C4880066B2660044A04400037D0300017A020002780300037705000377
      0600047707000577090006770B0006780C0007790E0008790F00097B12000A7C
      13009BDAA4005BCA6E005BCA6F005CCA6F005CCB6F005CCB70005CCB70009FDC
      A900148D26001590290016912B0018942E00199730001B9A33001C9E36001FA0
      390020A33D0022A63F0023A9420025AD460029B14B005FC579007BD0930099DC
      AB0089D9A100E9F7ED0000000000000000000000000000000000CFCFE9007E7E
      C5007C7CC4006565B8003B3BA40009098C000000860000008600000086000000
      870000008700000088000000890000008A0000008B0000008D0000008F000000
      910000009300000096000000980000009B0000009D000000A0000000A3000000
      A6000000A9000000AC000000B0000000B3000000B7000000BB000000BE000000
      C2000000C6000000CA000000CE000000D2000909D8003838DF006262E7007979
      EE007D7DF000D0D0F90000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004545B40000009B000406
      9E004A72DA005358D4004E4ECF004E4ECF004E4ECF004E4ECF004E4ECF004E4E
      CE004E4ECC007377D6007A96DA0003066700030663007693D5006F73D2004E4E
      CB004E4ECE004E4ECF004E4ECF004E4ECF004E4ECF004E4ECF006064D7007D99
      E50004069E0000009B004545B400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000086C0E8002F93D8002E93DA003797DA0055A7DF006CB3
      E300B8D9F000000000000000000000000000E0EEF800309ADF0043C3FF0042C1
      FB0043C1FD0042C1FC0042C1FB0041C1FC0042C2FC0042C2FC0042C1FC0043C1
      FC0042C2FC0042C2FC0042C2FC0042C2FC0042C1FC0042C1FB0042C1FD0033A5
      E600ACD3EF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E4F0
      E4008DC68D0051A951002E962E0003800400017C0300027B0400037B0600037A
      080004790800057A0A00067A0C00077A0D00087B0F00097B11000A7D13000B7E
      14009FDBA7005ECB72005ECC72005FCC72005FCC73005FCC73005FCC7300A5DE
      AE00148F280016902A0017942C0018962F001A9932001B9C35001E9F380020A1
      3B0021A53E0023A9410024AB450026AE48002BB34E004EC16C006CCC8700A0DE
      B200E9F7ED00000000000000000000000000000000000000000000000000D8D8
      EC009595CF005B5BB5002B2BA0000808900000008C0000008C0000008C000000
      8C0000008D0000008D0000008E00000090000000910000009200000094000000
      96000000980000009A0000009D0000009F000000A2000000A5000000A7000000
      AA000000AE000000B0000000B4000000B7000000BA000000BE000000C2000000
      C6000000C9000000CD000000D1000000D6000808DA002929E0005B5BE7009696
      F100DADAFA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004545B60000009D000406A0004D73
      DB00595DD8005656D3005656D3005656D3005656D3005656D3005656D3005656
      D3007A7DDE00849DDF00030672000000660000006100030664007E99DA007477
      DA005656D2005656D3005656D3005656D3005656D3005656D3005656D3007276
      E0008EA5EA000406A00000009D004545B6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DBEBF700319DDF0043C2FC0043C2
      FB0045CAFF0045CBFF0045CCFF0044C4FF0045C7FF0044CDFF0046CDFF0045CA
      FF0044C3FC0046D1FF0045D1FF0045D0FF0043C2FC0044C3FC0044C3FC0032A1
      E400C2DFF3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D3E8D3009FCE9F007ABB7B0078B9790078B8790078B87A0078B7
      790077B7780077B7790077B77C0077B77C003D984300097F12000B8014000C81
      1600A0DCA80061CD740061CD740061CD750061CD750061CD750061CD7500A8DF
      B0001591290017932B0018962E004AAD5D0085C9920085CA950087CC950088CD
      98008BCF9A008BD09B008DD29E008ED49F0091D7A200B1E4BF00DDF3E2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EDEDF500C1C1E2009595D0008080C6007F7FC5007F7FC4007F7FC5007F7F
      C5007F7FC5007F7FC5007F7FC6007F7FC6007F7FC7007F7FC8007F7FC9007F7F
      CA007F7FCB007F7FCC007F7FCD007F7FCE007F7FD0007F7FD1007F7FD2007F7F
      D4007F7FD5007F7FD7007F7FD8007F7FDA007F7FDC007F7FDE007F7FE0007F7F
      E1007F7FE3007F7FE5007F7FE7007F7FE9008080EB009494F000C1C1F600EEEE
      FB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004646B80000009F000406A2004F74DD006164
      DC005D5DD8005D5DD8005D5DD8005D5DD8005D5DD8005D5DD8005D5DD8008285
      E4008CA3E5000306830000007A00353595003434900000006B0003067100849D
      E0007A7DE0005D5DD8005D5DD8005D5DD8005D5DD8005D5DD8005D5DD8005D5D
      D800888BE8009AADEC000406A20000009F004646B80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600319DE00045C6FC0045C5
      FC0047D0FF003CBBF20044CCFE0045CBFF0046CBFF0042C8FA003BBBF30047D0
      FF0045C6FC0048D1FF0032A1E10047D2FF0045C5FC0045C6FD0045C5FC00309D
      DF00D6E9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000078B97D000A8113000B8216000D84
      1700A0DDAA0064D0790064D0790064D0790064D0790064D0790064D07900A9E1
      B30017932A0018952D001998300084C892000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006161C3000000A1000407A4005076DD00676AE2006565
      DF006565DF006565DF006565DF006565DF006565DF006565DF00898BE90094A8
      EA000306960000008A003A3AA400000000000000000037379B00000079000306
      860089A1E6007F81E7006565DF006565DF006565DF006565DF006565DF006565
      DF006565DF009D9FF100A3B4EE000407A4000000A1006161C300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600309CDF0046C8FB0048C9
      FD0048CBFF0033A5E5003DBCF50049CBFF0048C9FD003EB9F30033A8E70048CA
      FE0046C7FB0045C5FA002D94DA0046C8FB0048C9FE004ACFFF0047CCFF002E97
      DD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000079BB7F000B8415000C8517000D86
      1A00A2DEAC0067D27E0068D27E0068D27E0068D27E0068D27E0068D27E00ACE2
      B50018952D0019982F001B9B320085C994000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000001C1CAB000000A3001F36BD00707BEC006D6DE6006D6D
      E6006D6DE6006D6DE6006D6DE6006D6DE6006D6DE6009091EF009AADED000306
      A30000009B003E3EAF00000000000000000000000000000000003B3BA5000000
      8F0003069C008FA4EA008486ED006D6DE6006D6DE6006D6DE6006D6DE6006D6D
      E6006D6DE6006D6DE600CBCDFB001B33BA000000A3001C1CAB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329DE00048C9FC0048C9
      FC0048C9FD0032A2E4003CB4EF004ACCFF0049C9FD003EBAF20034A6E50049C9
      FE0049C9FD0046C7FB003597D9004AD0FF0051E3FF0053EBFF003BBEF00086C0
      E800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007BBC81000C8717000D8719000E88
      1B00A3DFAD006BD582006BD582006BD582006BD582006BD582006BD58200AFE3
      B80018982E001A9A31001B9D340087CA94000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009898D9000000A5000000A5004360D0007B80EF007474
      ED007474ED007474ED007474ED007474ED009596F400A0B1EF000406A8000000
      A4004242B8000000000000000000000000000000000000000000000000004040
      B2000000A0000406A70094A8ED008989F2007474ED007474ED007474ED007474
      ED007474ED00AAADF700697ED9000000A5000000A5009898D900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600319EE0004CCDFE004CCC
      FD004BCDFD00329EE20038AFEA004BCDFE004BCCFC0041BCF20035A8E5004CCC
      FD004BCDFE004ACAFC002F93D9003EC4F10044D4F20038B6E80081BEE700E0EE
      F800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007BBE82000C8919000E8A1A000F8B
      1C00A5E0B0006ED887006ED887006ED887006ED887006ED887006ED88700B2E5
      BC00199A30001B9C33001C9F350087CC94000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008E8ED6000000A7000000A700536CD500878B
      F4007B7BF4007B7BF4007B7BF4009B9BF900A4B5F1000406AA000000A7004343
      BC00000000000000000000000000000000000000000000000000000000000000
      00004242BA000000A7000406AA0097A9EE008E8FF7007B7BF4007B7BF4007B7B
      F400ADB0F9006B81DB000000A7000000A7008E8ED60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329FE0004DCEFD004ECF
      FE004DCEFD00329EE00037ADEA0052DEFF0054E2FF0046CBF70033ABE30057EE
      FF005CF9FF004FDEFF003C9ADB0088C1E9007EBCE600B5D8F000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007DC084000D8C1A000E8E1C00108E
      1E00A7E0B20073DA8E0073DA8E0073DA8E0073DA8E0073DA8E0073DA8E00B4E6
      BE001A9D32001C9F35001DA2380089CE97000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008E8ED7000000A9000000A9005E76
      D9009295F8008383FC009F9FFD00AAB8F2000406AC000000A9004444BE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004444BE000000A9000406AC009AABEF009292FC008383FC00B0B2
      FB006D83DC000000A9000000A9008E8ED7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329DE0004FD1FD004FD1
      FD004FD0FD00309BDE00309DDE0048D7F40047D7F30036B1E50081BEE7002F9D
      DD0035B1E4002F9CDD0092C6EA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007FC285000E8F1B000F8F1E001091
      1F00A8E2B40077DD940077DE940077DE940077DE940077DE940077DE9400B7E8
      C2001CA034001DA237001EA43A008CCF99000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008E8ED8000000AB000000
      AB00687EDC00B4B5FD00ADBAF3000406AE000000AB004545C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004545C0000000AB000406AE009CAEF000BBBCFD007084
      DE000000AB000000AB008E8ED800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329FDF0051D3FD0051D3
      FD0050D3FD002E98DD00CAE3F40074B6E5007EBCE600BDDCF100000000000000
      0000C7E1F3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000080C4880010921D0010921F001193
      2100AAE3B7007BE19A007BE19A007BE19A007BE19A007BE19A007BE19A00BBE9
      C5001DA237001EA5390020A73C008CD09B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008E8ED9000000
      AC000101AC008EA3E5001215B1000000AC004545C10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004545C1000000AC000B0EB000AABEEF000808
      AC000000AC008E8ED90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329FE00054D6FD0053D4
      FD0054D6FD003098DC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000081C5890010951F00119520001397
      2300ABE5B9007FE3A0007FE3A0007FE3A0007FE3A0007FE3A0007FE3A000BDEA
      C8001EA538001FA73B0021A93E008BD29C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008E8E
      D9000D0DAC00A7A7DF002C2CB2004646C3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004646C3002020AF00C2C2EC001B1B
      AE008E8ED9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329FDF0055D6FD0056D8
      FE0055D6FD002C93DA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084C88B00169924001A9C2B0030A4
      4000BEEBCA0083E8A60083E8A60083E8A60083E8A60083E8A60083E8A600C0EC
      CC001FA73B0020AA3E0022AC41008ED59E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009696DA005959C7006262CA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006161C9006B6BCF009595
      DB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF60032A0E00057D8FE0056D9
      FE0055D6FC003193D90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5D7AB001D9E2C0055B561008CCA
      9400D0EDD70098EFB70088ECAC0088ECAC0088ECAC0088ECAC0088ECAC00C5EA
      CD0083CD920069C57D003FB85A00AFE1BA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600329EDE0058D6FB0059DC
      FF0055D6FC003796D90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6EDD9003BAC49007AC68300B1DB
      B600E2F0E300D5F0DC00CAF0D600C5EFD100C5EFD100C5EFD100C9ECD200EAF4
      EB00C7E7CD0093D5A20060C67900D8F0DE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8EAF600339DDA005AC2E4005ADC
      FF0054D3FA003E9ADA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000064BE6E009ED7A600D7EB
      DA00E4F0E600ACDCB40039B04D001BA733001CA836001FAA39009DD8A800D4EC
      D900E9F3EB00B6E3C00087D49A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEECF700349BDA005CC3E40061ED
      FF0053D4FB0048A0DD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000077C88500FCFC
      FC00C4E7CA008DD2980038B34D001DAA35001EAB380025AF40007ED08F00B5E3
      BF00EAF6ED00B3E2BE00E8F6EC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002E94DB004DD9F4006BFB
      F6003EBEEB00A0CDEC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008ED3
      98008FD59A006BC77B0028B040001DAD38001FAE3B0022B140005AC571008ED8
      9E00A5E0B300E9F6EB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CFE5F50077B8E5003496
      D90089C1E9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D7EFDC00ACDEB4008CD499008DD59B008ED59C008ED79D00AEE2B800D9F1
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000C0000000900000000100010000000000800D00000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF80000000
      0001000000000000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      FFFFFFFFFFFFFFFFFF1FFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FF03FFFF000000000000000000000000FFFFFFFFFFFFFFFFE001FFFF00000000
      0000000000000000FF003FFFFFFFFFFF8000FFFF000000000000000000000000
      E00007FFFFFFFFFE00007FFF000000000000000000000000C00007FFFFFFFFFC
      0000FFFF000000000000000000000000C000007FFFFFFFF80001CFFF00000000
      0000000000000000C000003F7FFFFFF8000387FF000000000000000000000000
      C00000021E3FFFF0078707FF000000000000000000000000C0000000001FFFF0
      0F8E07FF000000000000000000000000C0000000000FFFF01F9E03FF00000000
      0000000000000000C00000000007FFFFFFFE03FF000000000000000000000000
      C00000000003FFFFFFFF03FF000000000000000000000000C10000000003FFFF
      FFFF03FF000000000000000000000000C00000000003FFE007FE03FF00000000
      0000000000000000C00000000007FFE007FE03FF000000000000000000000000
      C0000000000FFFE00FFE03FF000000000000000000000000C0000000001FFFE0
      1FE007FF000000000000000000000000C0000000003FFFE00FE007FF00000000
      0000000000000000C00003FFFFFFFFE00FE00FFF000000000000000000000000
      C00003FFFFFFFFE007F00FFF000000000000000000000000E00003FFFFFFFFE0
      00F003FF000000000000000000000000FC0007FFFFFFFFFE003007FF00000000
      0000000000000000FFC07FFFFFFFFFFE00300FFF000000000000000000000000
      FFFFFFFFFFFFFFFF00383FFF000000000000000000000000FFFFFFFFFFFFFFFF
      80387FFF000000000000000000000000FFFFFFFFFFFFFFFFE03FFFFF00000000
      0000000000000000FFFFFFFFFFFFFFFFFC3FFFFF000000000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF800000000001000000000000FFFFFFFFFFFFE3FFFFFFFFC7FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFC1FFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF80FFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFFFFF1FFFFFFFF007F
      F18FFE00FFE0000007FFFFFFFFFFFFFFFFE1FFFFFFFF003FE187FC00FFF80000
      0FFFFFFFF80FFFFFFFC1FFFFFFFF001FC183F800FFF800000FFFFFFF0000FFFF
      FF81FFFFFFFF800F8181F001FFD800000BFFFFFC00001FFFFE01FFFFFFFFC007
      0180E003800000000001FFF800000FFFFC001FFFFFFFE00201804007E0000000
      0007FFF0000007FFF8000000FFFFF0000180000FE00000000003FFE0000003FF
      F00000007FFFF8000180001FE00000000003FFE0000003FFE0000000FFFFFC00
      0180003FE00000000003FFE0000003FFC0000000FFFFFE000180007FE0000000
      0003FFF0000007FF80000000FFFFFF00018000FFE00000000003FFC0000001FF
      00000000FFFFFF80018001FFE00000000003F8000000000F00000000FFFFFF00
      018000FFE00000000003F0000000000780000000FFFFFE000180007FE0000000
      0003F00000000007C0000000FFFFFC000180003FE00000000003F00000000007
      E0000000FFFFF8000180001FE00000000003F00000000007F00000007FFFF000
      0180000FE00000000003F00000000007F8000000FFFFE00001800007E0000000
      0003F00000000007FC003FFF8FFFE00001800007E00000000003F00000000007
      FE01FFFF87FFE00001800007E00000000003F00000000007FF81FFFF83FFFFFF
      FFFFFFFFE00000000003F00000000007FFC1FFFF81FFFFFFFFFFFFFFE0000000
      0003F00000000007FFE1FFFF807FE00001800007E00000000003F00000000007
      FFF1FFF8003FE00001800007E00000000003F00000000007FFFF0000001FE000
      01800007E00000000003F0000000000FFFFE0000000FF0000180000FE0000000
      0003F0000000000FFFFF00000007F8000180001FE00000000003F0000000000F
      FFFF00000003FC000180003FF00000000007F0000000000FFFFF00000001FE00
      0180007FF80000000007F0000000000FFFFF00000000FF00018000FFFED80000
      07FFF8000000000FFFFF00000000FF80018001FFFFF800000FFFF8000000000F
      FFFF00000001FF00018000FFFFF800000FFFF8000000000FFFFF00000003FE00
      0180007FFFF800000FFFF8000000000FFFFF00000007FC000180003FFFF80000
      0FFFF8000000000FFFFE0000000FF8000180001FFFF8000007FFF8000000000F
      FFFE0000001FF0000180000FFFF8000007FFF8000000000FFFFFFFF8003FE002
      01804007FFF8000007FFF8000000000FFFFFFFFF807FC0070180E003FFF80000
      07FFF8000000000FFFFFFFFF81FF800F8181F001FFF8000007FFF8000000000F
      FFFFFFFF83FF001FC183F800FFF8000007FFF8000000000FFFFFFFFF87FF003F
      E187FC00FFF8000007FFFC000000001FFFFFFFFF8FFF007FF18FFE00FFF80000
      07FFFFFFFFFFFFFFFFFFFFFFFFFF80FFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFC1FFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FF
      FFFFFFC7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFFFFF00FFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF00000FFFFFFFC003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      00000FFFFFFF8001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFFFF8001
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFF
      FFF1FFFF8FFFFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFFFFE0FFFF07FFFFFF
      00000FFFFFFF0000FFFFFFFFFFFFFFFFFFC07FFE03FFFFFF00000FFFFFFF0000
      FFFFFFFFFFFFFFFFFF803FFC01FFFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFF
      FF001FF800FFFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFFFE000FF0007FFFFF
      00000FFFFFFF0000FFFFFFFFFFFFFFFFFC0007E0003FFFFF80001FFFFFFF0000
      FFFFFFFFFFFFFFFFFC0003C0003FFFFF80001FFFFFFF0000FFFFFFFFFFFFFFFF
      FC000180003FFFFF80001FFFFFFF0000FFFFFFFFFFFFFFFFFE000000007FFFFF
      80001FFFFFFF0000FFFFFFFFFFFFFFFFFF00000000FFFFFF80003FFFF8000000
      001FF8000000001FFF80000001FFFFFE00003FFFF0000000000FE00000000007
      FFC0000003FFFFFC00003FFFE00000000007C00000000003FFE0000007FFFFF8
      00003FFFC00000000003C00000000003FFF000000FFFFFF800001FFF80000000
      0001800000000001FFF800001FFFFFF000001FFF800000000001800000000001
      FFFC00003FFFFFE000000FFF800000000001800000000001FFFE00007FFFFFE0
      00000FFF800000000001800000000001FFFE00007FFFFFC0000007FF80000000
      0001800000000001FFFC00003FFFFE00000007FF800000000001800000000001
      FFF800001FFFFC00000007FF800000000001800000000001FFF000000FFFF800
      000007FF800000000001800000000001FFE0000007FFF800000007FFC0000000
      0003C00000000003FFC0000003FFF800000007FFC00000000003C00000000003
      FF80000001FFFC07000007FFE00000000007E00000000007FF00000000FFFFFF
      000007FFF8000000001FF0000000000FFE000000007FFFFF000007FFFFFF0000
      FFFFFFFFFFFFFFFFFC000180003FFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFF
      FC0003C0003FFFFF00000FFFFFFF0000FFFFFFFFFFFFFFFFFC0007E0003FFFFF
      00000FFFFFFF0000FFFFFFFFFFFFFFFFFE000FF0007FFFFF00003FFFFFFF0000
      FFFFFFFFFFFFFFFFFF001FF800FFFFFF0001FFFFFFFF0000FFFFFFFFFFFFFFFF
      FF803FFC01FFFFFF0037FFFFFFFF0000FFFFFFFFFFFFFFFFFFC07FFE03FFFFFF
      03FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFE0FFFF07FFFFFF03FFFFFFFFFF0000
      FFFFFFFFFFFFFFFFFFF1FFFF8FFFFFFF03FFFFFFFFFF0000FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF03FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      03FFFFFFFFFF8001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF03FFFFFFFFFFC001
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83FFFFFFFFFFE003FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF87FFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object POS_PQT: TADOCommand
    CommandText = 'POS_PQT;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pINPUT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@pMATHE'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@pMENHGIA'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end>
    Left = 281
    Top = 346
  end
  object QrTT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTTBeforeOpen
    AfterInsert = QrTTAfterInsert
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM_TTOAN'
      ' where'#9'KHOA =:KHOA')
    Left = 84
    Top = 460
    object QrTTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrTTPTTT: TWideStringField
      FieldName = 'PTTT'
      Size = 2
    end
    object QrTTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrTTTypeInput: TWideStringField
      FieldName = 'TypeInput'
    end
    object QrTTTypeCode: TWideStringField
      FieldName = 'TypeCode'
      Size = 30
    end
    object QrTTTypeBalance: TFloatField
      FieldName = 'TypeBalance'
    end
    object QrTTTypeID: TGuidField
      FieldName = 'TypeID'
      FixedChar = True
      Size = 38
    end
    object QrTTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrTTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTTTTOAN_CASH: TFloatField
      FieldName = 'TTOAN_CASH'
    end
    object QrTTTTOAN_CARD: TFloatField
      FieldName = 'TTOAN_CARD'
    end
    object QrTTTTOAN_TRAHANG: TFloatField
      FieldName = 'TTOAN_TRAHANG'
    end
    object QrTTTTOAN_VOUCHER: TFloatField
      FieldName = 'TTOAN_VOUCHER'
    end
    object QrTTTTOAN_EWALLET: TFloatField
      FieldName = 'TTOAN_EWALLET'
    end
    object QrTTTHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrTTCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrTTCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrTTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrTTRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrTT_id: TLargeintField
      FieldName = '_id'
    end
  end
end
