﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThongtinCN;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, wwdblook, Mask, wwdbedit,
  ActnList, RzButton;

type
  TFrmThongtinCN = class(TForm)
    Panel1: TPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    MyActionList: TActionList;
    CmdOK: TAction;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CmdReturn: TRzBitBtn;
    CmdClose: TAction;
    Panel2: TPanel;
    CmdCancel: TAction;
    Label16: TLabel;
    wwDBEdit5: TwwDBEdit;
    CmdInvoiceOK: TRzBitBtn;
    CmdInvoiceCancel: TRzBitBtn;
    CmdDeliveryOK: TRzBitBtn;
    CmdDeliveryCancel: TRzBitBtn;
    BtClose: TRzBitBtn;
    Label11: TLabel;
    wwDBEdit2: TwwDBEdit;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdOKExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdInvoiceCancelClick(Sender: TObject);
    procedure CmdInvoiceOKClick(Sender: TObject);
    procedure CmdDeliveryOKClick(Sender: TObject);
    procedure CmdDeliveryCancelClick(Sender: TObject);
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);

  public
    mShowInvoice: Boolean;
  	function Execute(pShowInvoice: Boolean = True) : Boolean;
    function ExecuteInvoice() : Boolean;
    function ExecuteDelivery() : Boolean;
  end;

var
  FrmThongtinCN: TFrmThongtinCN;

implementation

uses
	isLib, PosMain, MainData, isDb, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdCancelExecute(Sender: TObject);
begin
    with FrmMain.QrBH do
    begin
        if FieldByName('DELIVERY').AsBoolean then
        begin
            if not YesNo('Hủy giao hàng. Tiếp tục?') then
                Exit;

            FieldByName('CN_LIENHE').Clear;
            FieldByName('CN_DIACHI').Clear;
            FieldByName('CN_DTHOAI').Clear;
            FieldByName('TINHTRANG').Clear;
            FieldByName('DELIVERY').Clear;
        end;
        ModalResult := mrOk;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdDeliveryCancelClick(Sender: TObject);
begin
    with (FrmMain.QrBH) do
    begin
         FieldByName('DELIVERY').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdDeliveryOKClick(Sender: TObject);
begin
    with (FrmMain.QrBH) do
    begin
         FieldByName('DELIVERY').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdInvoiceCancelClick(Sender: TObject);
begin
    with (FrmMain.QrBH) do
    begin
         FieldByName('CO_HOADON').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdInvoiceOKClick(Sender: TObject);
begin
    with (FrmMain.QrBH) do
    begin
         FieldByName('CO_HOADON').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdOKExecute(Sender: TObject);
begin
    CmdReturn.SetFocus;
    with (FrmMain.QrBH) do
    begin
          if mShowInvoice then
            ExecuteInvoice
          else
            ExecuteDelivery;
          Post;
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN.Execute;
begin
    mShowInvoice := pShowInvoice;
    with FrmMain.QrBH do
    begin

        SetEditState(FrmMain.QrBH);
        if FieldByName('MAVIP').AsString <> '' then
        begin
            FieldByName('CN_DTHOAI').AsString := FieldByName('LK_VIP_DTHOAI').AsString;
            FieldByName('CN_DIACHI').AsString := FieldByName('LK_VIP_DCHI').AsString;
            FieldByName('CN_LIENHE').AsString := FieldByName('LK_TENVIP').AsString;
        end;
    end;
	Result := ShowModal = mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormShow(Sender: TObject);
var
    bTk, bHd: Boolean;
begin
    TMyForm(Self).Init;

    bHd := mShowInvoice;
    GroupBox1.Visible := bHd;

    if bHd then
        Self.Height := Panel2.Height +  GroupBox1.Height + 30;

    bTk := not mShowInvoice;
    GroupBox2.Visible := bTk;

    if bTk then
        Self.Height := Panel2.Height + GroupBox2.Height + 30;

    with FrmMain.QrBH do
    begin
        if not FieldByName('CO_HOADON').AsBoolean and bHd then
             FieldByName('CO_HOADON').AsBoolean := True;

        if not FieldByName('DELIVERY').AsBoolean and bTk then
             FieldByName('DELIVERY').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    with FrmMain.QrBH do
    begin
        CmdInvoiceOK.Enabled := not FieldByName('CO_HOADON').AsBoolean;
        CmdInvoiceCancel.Enabled := FieldByName('CO_HOADON').AsBoolean;
        CmdDeliveryOK.Enabled := not FieldByName('DELIVERY').AsBoolean;
        CmdDeliveryCancel.Enabled := FieldByName('DELIVERY').AsBoolean;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN.ExecuteInvoice() : Boolean;
begin
    with FrmMain.QrBH do
    begin
        if FieldByName('CO_HOADON').AsBoolean then
        begin
            if BlankConfirm(FrmMain.QrBH, ['CN_TENDV']) then
                Abort;

            FieldByName('THUCTRA').AsFloat := FieldByName('THANHTOAN').AsFloat;
            FieldByName('CALC_TIEN_THUE').AsFloat := FieldByName('TIEN_THUE').AsFloat;
        end
        else
        begin
            FieldByName('CN_TENDV').Clear;
            FieldByName('CN_MST').Clear;
            FieldByName('CN_DIACHI_HD').Clear;
            FieldByName('CN_EMAIL').Clear;

            FieldByName('THUCTRA').AsFloat := FieldByName('THANHTIEN').AsFloat;
            FieldByName('CALC_TIEN_THUE').AsFloat := 0;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN.ExecuteDelivery() : Boolean;
begin
    with FrmMain.QrBH do
    begin
        if FieldByName('DELIVERY').AsBoolean then
        begin
            if BlankConfirm(FrmMain.QrBH, ['CN_LIENHE', 'CN_DIACHI', 'CN_DTHOAI']) then
                Abort;

        end
        else
        begin
            FieldByName('CN_LIENHE').Clear;
            FieldByName('CN_DIACHI').Clear;
            FieldByName('CN_DTHOAI').Clear;
        end;
    end;
end;

end.
