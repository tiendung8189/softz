﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExCommon;

interface

uses
	Classes, Db, Windows, Forms, ADODb, wwfltdlg, Graphics, ComCtrls, ActnList,
    fctreecombo, fctreeview, Controls, SysUtils, wwDBGrid2, Messages, ADOInt,
    ShellAPI, wwdblook, wwDataInspector;

resourcestring
	{ Softz }
	// Import
    RS_IM_NUM_REC			= 'Đã import %d mẫu tin.';
    RS_IM_NONE				= 'Không có mẫu tin nào được import.';
    RS_IM_ERR_REC			= 'Có %d mẫu tin không hợp lệ.';

    // File
	RS_FILE_IO				= 'Lỗi truy xuất file.';
    RS_EXPORT_ERR			= 'Lỗi xuất dữ liệu.';

    // Common
    RS_CONNECT_FAIL     	= 'Không kết nối được với cơ sở dữ liệu.';
    RS_NOTE_CAP         	= 'Lưu ý';

    RS_VAT_NOTSAME       	= 'Mặt hàng sai loại thuế.';
    RS_NO_FOLDEREXPORT		= 'Chưa chọn thư mục lưu dữ liệu export.';
    RS_EXPORTED_COMPLETE 	= 'Đã xuất xong dữ liệu vào thư mục "%s"';

    RS_DSN_CONFIG			= 'Chưa cấu hình text DSN.';
    RS_ERROR_COPY_FILE		= 'Lỗi khi copy file vào thư mục hệ thống.';
    RS_INVALID_DATA			= 'Dữ liệu không hợp lệ.';
    RS_INVALID_CARD			= 'Mã thẻ không hợp lệ.';
    RS_ITEM_CODE_DUPLICATE	= 'Trùng %s.';

	RS_ITEM_CODE_FAIL1		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc mã hàng đã ngừng kinh doanh';
	RS_ITEM_CODE_FAIL2		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc nhà cung cấp.';
    RS_BOOKCLOSED			= 'Đã khóa sổ đến ngày %s.';

    RS_INVALID_QTY 			= 'Số lượng không hợp lệ.';
	RS_INVALID_DISCOUNT 	= 'Chiết khấu không hợp lệ.';
    RS_INVALID_INTEREST 	= 'Tỷ lệ lãi không hợp lệ.';
	RS_POS_NOT_PRICE 		= 'Mặt hàng chưa có giá bán.';
    RS_SCT_CHANGE           = 'Ngày lập phiếu thay đổi. Số phiếu mới là: "%s".';

	RS_DA_THUCHI			= 'Đã phát sinh Thu / Chi, nên không thể chỉnh sửa được.';
    RS_XOA_CHITIET			= 'Xóa toàn bộ chi tiết chứng từ?';
    RS_CONFIRM_XOAPHIEU		= 'Xóa phiếu sẽ không thể hồi phục. Tiếp tục?';
    RS_PHUCHOI              = 'Phục hồi';
    RS_DAXOA                = 'Đã xóa';

    RS_EDIT_BY              = 'Phiếu đang được sửa bởi "%s" từ lúc "%s", các dữ liệu đang sửa trước đó sẽ bị bỏ qua. Tiếp tục?';

    RS_ADMIN_CONTACT		= 'Xin liên hệ với người quản trị hệ thống.';

    // Cr Frame
    RS_THESAME_MONTH 		= 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME	= 'Thời gian báo cáo không hợp lệ.';

	{ POSz }
    RS_PRINT_ERR            = 'Lỗi in bill.';
    RS_NHACNHO_THANHTOAN    = 'Xác nhận nhân đơn vị tiền thối';
	RS_INVALID_ACTION       = 'Thao tác không hợp lệ.';
	RS_POS_CODE_FAIL        = 'Nhập sai mã hàng hóa.';
    RS_CONFIRM_PRICE        = 'Hàng có giá bán là %s, giá đã được giảm là %s. Giảm giá?';
    RS_CUT_PRICE_QTY        = 'Chỉ còn lại %f suất giảm giá.';


	{ Chung }

    RS_ORDER_TABLE_CANCEL	= 'Xác nhận hủy bàn';
    RS_START_SESSION        = 'Xác nhận kết thúc ca bán hàng.';
    RS_END_SESSION          = 'Không có tiền dự phòng, vẫn thực hiện Nhận Ca. Tiếp tục?';
    RS_INVALID_MONEY        = 'Số tiền không hợp lệ';
    RS_CONFIRM_PAYMENT		= 'Xóa thanh toán sẽ không thể hồi phục. Tiếp tục?';

const
    REP_ENCODE_KEY: Word = 119;
    TXT_DSN: String 		= 'SOFTZ_TEXT';

var
    sysCurRound, sysNumRound: Integer;

    sysYear, sysMon: Integer;
    sysBegYear, sysBegMon: Integer;
    sysCloseHH, sysCloseCN: TDateTime;

    sysLateDay, sysKhttLe, sysKhttSi: Integer;
    sysDefKho, sysLoc, sysHTGia: String;

    sysPrinter: string;

    mTrigger, mTriggerCK, mExDot,
    cfRecordDate, sysIsBarcode: Boolean;

    // ePos global variables
    posQuay0,			// Quầy gốc tương ứng tên máy trong "Danh sách quầy thu ngân"
    posQuay,			// Quầy hiện tại, mặc định bằng mQuay0 and could be changed
    posMaQuay,
    posMakho,			// Kho hiện tại, mặc định bằng sysDefKho and could be changed
    lastSct,
    sysHinhThucGia,
    sysFolderImageWeb,
    posCa,
    posSct: String;
    posKhoa: TGUID;	// Hóa đơn hiện tại

    (*
    ** License
    *)
//function SetLicense(filename: String): Boolean;
function IsLicense(s: String): Boolean;
function IsLicenseCheck: Boolean;
    (*
    ** Message
    *)
procedure exValidClosing(const dt: TDateTime; const subsys: Integer = 1);
procedure exValidRecordDate(const dt: TDateTime);

	(*
    ** Utils
    *)

function  exDefaultWarehouse: Boolean;
function  exCanChange(Sender: TPageControl; DataSet: TCustomADODataSet): Boolean;

function  exValidMatchGroup(pNganh, pNhom, pNhom2, pNhom3, pNhom4: String): Boolean;
procedure FlexGroupCombo(Sender: TfcTreeCombo);
function  exGetFlexDesc(Sender: TObject): WideString;

function  exCanEditVoucher(ds: TDataSet; pField: String = 'MAKHO'; pAbort: Boolean = True): Boolean;
function  exDrcValidate(ds: TDataSet): Boolean;

function  exVNDRound(_pExtended: Extended; _pDigit: Integer = -1000; _pRoundNum: Integer = -1000): Extended; //_pRoundNum: 0, 100, 500, 1000

function  exIsChecked(ds: TDataSet; pAbort: Boolean = True): Boolean;
procedure exChecked(pDataset: TDataSet; pRight: String = 'SZ');
function  exGetCheckedCaption(pDataSet: TDataSet): String;
function exCheckNhomthue(pNhomthue: String; pDataset: TCustomADODataSet): Boolean;
function exCheckLoc(pDataset: TDataSet; pAbort: Boolean = True; AFIELD: string = 'LOC'): Boolean;

function  exIsCheckSL(pSl: Double; pMSG: Boolean = True): Boolean;

function IsDuplicateCode(pQuery: TCustomADODataSet;
	pValue: TField; pShowMgs: Boolean = True): Boolean; overload;
function IsDuplicateCode2(pQuery: TCustomADODataSet;
	pValue: TField; var pBm: TBytes): Boolean; overload;

procedure exReSyncRecord(DataSet: TCustomADODataSet; pAll: Boolean = False);
procedure exDbCopyFields(pDataSet, pSource: TCustomADODataSet;
    pOveride: Boolean = False; pExceptField: String = '');

procedure exSaveDetails(DataSet: TCustomADODataSet);
procedure exEmptyDetails(DataSet: TDataSet; ActiveControl: TWinControl = Nil);

function  exConfigInspector(pIns: TwwDataInspector; pFieldNames: String): Integer;

function CanEditLockedBy(Dataset: TDataSet; pShowMessage: Boolean = True): Boolean;
procedure SetLockedBy(Dataset: TDataSet; b: Boolean);
(*
    **
    *)
function  exExecSQL(const sqlStr: String): Integer;
function  exGetImportFile(const pType: Integer = 2): String;
procedure exViewLog(const LogName, Content: String);

procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo);

function ShowReport (pTitle, pRepName : String; Args: array of Variant): Boolean; overload;
function ShowReport (pTitle, pRepName : String): Boolean; overload;

function PrintReport (pRepName : String; Args: array of Variant;
                            const PrinterName: String = '';
                            const pCopy: Integer = 1): Boolean; overload;
	(*
    ** Refs. functions
    *)
function exDotUSER(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotUSER(Sender: TField): Boolean; overload;

function exDotMadt(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMadt(Sender: TField): Boolean; overload;
function  exDotMadt(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMadt(var s: String): Boolean; overload;

function  exDotMakh(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMakh(Sender: TField): Boolean; overload;
function  exDotMakh(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMakh(var s: String): Boolean; overload;

function  exDotMancc(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMancc(Sender: TField): Boolean; overload;
function  exDotMancc(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMancc(var s: String): Boolean; overload;

function  exDotVip(var s: String): Boolean; overload;

procedure exInitDotMavt;
function  exDotMavt(const Group: Integer; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

procedure exInitDotFBMavt;
function  exDotFBMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotFBMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

function cloneDataDetail(curKhoa: TGUID): Boolean; overload;

implementation

uses
	isMsg, isStr, Printers, MainData, IniFiles, isCommon, Math, isFile,
    Rights, isDb, GuidEx, isLib, isNameVal, RepEngine, isEnCoding, SetLicense;

    (*
    ** License
    *)
function IsLicenseCheck: Boolean;
var
    lic: String;
    mRet: Boolean;
begin
    mRet := True;
    if not IsLicense(GetSysParam('LIC_CAL')) then
        if FrmSetLicense.Execute(lic) then
        begin
            DataMain.SetSysParam('LIC_CAL', lic)
        end else
            mRet := False;
    Result := mRet;
end;

function IsLicense(s: String): Boolean;
begin
    Result := s = EncryptLicense(GetSysParam('HEADER1'));
end;
	(*
    ** Message
    *)
(*==============================================================================
** Arguments:
**	dt:			Ngay can kiem tra
**	subsys:		1 	Hang hoa
**				2 	Cong no
**------------------------------------------------------------------------------
*)
procedure exValidClosing(const dt: TDateTime; const subsys: Integer);
var
	closing: TDateTime;
begin
	case subsys of
    1:	// Hang hoa
    	closing := sysCloseHH;
    2:	// Cong no
    	closing := sysCloseCN;
    else
    	Exit;
    end;

    if dt <= closing then
	begin
		ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(closing)]));
    	Abort;
    end;
end;

(*==============================================================================
** Arguments:
**		dt:		Record date
**------------------------------------------------------------------------------
*)
procedure exValidRecordDate(const dt: TDateTime);
begin
//	// License expired
//    if IsLicenseExpired(dt) then
//    	Abort;

    // Suspicious date
    if not cfRecordDate then
        Exit;

    if (dt <= Date + sysLateDay) and (dt + sysLateDay >= Date) then
		Exit;

    if YesNo('Xác nhận đã nhập đúng ngày. Tiếp tục?') then
        Exit;

    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_WAREHOUSE_NOT_DEFAULT = 'Chưa chỉ định mã kho.';

function exDefaultWarehouse: Boolean;
begin
	Result := sysDefKho <> '';
	if Result then
    else
    	Msg(RS_WAREHOUSE_NOT_DEFAULT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_FINISH_INPUT = 'Phải nhập liệu hoàn tất chứng từ này.';

function exCanChange(Sender: TPageControl; DataSet: TCustomADODataSet): Boolean;
begin
	with Sender do
		if ActivePageIndex = 0 then
        	Result := not DataSet.IsEmpty
        else
        begin
        	Result := DataSet.State in [dsBrowse];
        	if not Result then
	        	Msg(RS_FINISH_INPUT);
		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exValidMatchGroup(pNganh, pNhom, pNhom2, pNhom3, pNhom4: String): Boolean;
var
    s: String;
begin
    if pNhom4 <> '' then
        s := Format(
            'select 1 from DM_NHOM4 where MANHOM4=''%s'' and MANHOM3=''%s''' +
            ' and MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom4, pNhom3, pNhom2, pNhom, pNganh])
    else if pNhom3 <> '' then
        s := Format(
            'select 1 from DM_NHOM3 where MANHOM3=''%s''' +
            ' and MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom3, pNhom2, pNhom, pNganh])
    else if pNhom2 <> '' then
        s := Format(
            'select 1 from DM_NHOM2 where MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom2, pNhom, pNganh])
    else
        s := Format(
            'select 1 from DM_NHOM where MANHOM=''%s'' and MANGANH=''%s''', [pNhom, pNganh]);

    with TADOQuery.Create(Nil) do
    begin
        Connection  := DataMain.Conn;
        LockType    := ltReadOnly;

        SQL.Text := s;

        Open;
        Result := not IsEmpty;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure FlexGroupCombo;
var
    mId: String;
	Qr1, Qr2, Qr3, Qr4, Qr5: TADOQuery;
    it1, it2, it3, it4, it5: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 15;
    end;

    Qr1 := TADOQuery.Create(Nil);
    Qr2 := TADOQuery.Create(Nil);
    Qr3 := TADOQuery.Create(Nil);
    Qr4 := TADOQuery.Create(Nil);
    Qr5 := TADOQuery.Create(Nil);

    Qr1.Connection := DataMain.Conn;
    Qr2.Connection := DataMain.Conn;
    Qr3.Connection := DataMain.Conn;
    Qr4.Connection := DataMain.Conn;
    Qr5.Connection := DataMain.Conn;

    Qr1.LockType := ltReadOnly;
    Qr2.LockType := ltReadOnly;
    Qr3.LockType := ltReadOnly;
    Qr4.LockType := ltReadOnly;
    Qr5.LockType := ltReadOnly;

    with Qr1 do
    begin
        SQL.Text := 'select MANGANH, TENNGANH from DM_NGANH order by MANGANH';
        Open;
        while not Eof do
        begin
            mId := FieldByName('MANGANH').AsString;
            it1 := Sender.Items.Add(Nil, Format('[%s] %s', [FieldByName('MANGANH').AsString, FieldByName('TENNGANH').AsString]));
            it1.StringData := mId;
            // it1.ImageIndex := 0;

            with Qr2 do
            begin
                SQL.Text := Format('select MA, MANHOM, TENNHOM from DM_NHOM where MANGANH=''%s'' order by MANHOM', [mId]);
                Open;
                while not Eof do
                begin
                    mId := FieldByName('MANHOM').AsString;
                    it2 := Sender.Items.AddChild(it1, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM').AsString]));
                    it2.StringData := mId;
		            // it2.ImageIndex := 1;

                    with Qr3 do
                    begin
                        SQL.Text := Format('select MA, MANHOM2, TENNHOM2 from DM_NHOM2 where MANHOM=''%s'' order by MANHOM2', [mId]);
                        Open;
                        while not Eof do
                        begin
                            mId := FieldByName('MANHOM2').AsString;
                            it3 := Sender.Items.AddChild(it2, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM2').AsString]));
                            it3.StringData := mId;
				            // it3.ImageIndex := 2;

                            with Qr4 do
                            begin
                                SQL.Text := Format('select MA, MANHOM3, TENNHOM3 from DM_NHOM3 where MANHOM2=''%s'' order by MANHOM3', [mId]);
                                Open;
                                while not Eof do
                                begin
                                    mId := FieldByName('MANHOM3').AsString;
                                    it4 := Sender.Items.AddChild(it3, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM3').AsString]));
                                    it4.StringData := mId;
                                    // it4.ImageIndex := 3;

                                    with Qr5 do
                                    begin
                                        SQL.Text := Format('select MA, MANHOM4, TENNHOM4 from DM_NHOM4 where MANHOM3=''%s'' order by MANHOM4', [mId]);
                                        Open;
                                        while not Eof do
                                        begin
                                            mId := FieldByName('MANHOM4').AsString;
                                            it5 := Sender.Items.AddChild(it4, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM4').AsString]));
                                            it5.StringData := mId;
                                            // it5.ImageIndex := 4;

                                            Next;
                                        end;
                                        Close;
                                    end;
                                    Next;
                                end;
                                Close;
                            end;
                            Next;
                        end;
                        Close;
                    end;
                    Next;
                end;
                Close;
            end;
            Next;
        end;
        Close;
    end;

    Qr1.Free;
    Qr2.Free;
    Qr3.Free;
    Qr4.Free;
    Qr5.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
    		SetSelectedNode(Items[0]);
	    	Text := Items[0].Text;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_LEV0 = 'Tất cả ngành';
    RS_LEV1 = 'Ngành';
    RS_LEV2 = 'Nhóm';
    RS_LEV3 = 'Nhóm 2';
    RS_LEV4 = 'Nhóm 3';
    RS_LEV5 = 'Nhóm 4';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetFlexDesc(Sender: TObject): WideString;
var
    s: String;
begin
    with Sender as TfcTreeCombo do
    begin
        if Text = '' then
            s := RS_LEV0
        else if SelectedNode <> Nil then
        begin
            case SelectedNode.Level of
            0:
                s := RS_LEV1;
            1:
                s := RS_LEV2;
            2:
                s := RS_LEV3;
            3:
                s := RS_LEV4;
            4:
                s := RS_LEV5;
            end;
        end;
    end;
    Result := s
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CAN_NOT_EDIT = 'Không được phép chỉnh sửa chứng từ.';

function exCanEditVoucher;
begin
	Result := True;
//    with ds do
//    	if (FieldByName(pField).AsString <> sysDefKho) then
//        	if FindField('DRC_STATUS') <> nil then
//            begin
//                Result := FieldByName('DRC_STATUS').AsString <> '3';
//            	if Result and pAbort then
//                begin
//                	Msg(RS_CAN_NOT_EDIT);
//                	Abort;
//                end;
//    		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_DRC_INVALID = 'Trạng thái chứng từ phải là 1 hoặc 2.';

function exDrcValidate;
begin
    Result := True;
//    Result := ds.FindField('DRC_STATUS') = nil;
//    if not Result then
//        Result := ds.FieldByName('DRC_STATUS').AsString <> '3';
//        if not Result then
//        begin
//            ErrMsg(RS_DRC_INVALID);
//            Abort;
//        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exVNDRound(_pExtended: Extended; _pDigit: Integer = -1000; _pRoundNum: Integer = -1000): Extended;
begin
    if _pRoundNum = -1000 then
        _pRoundNum := sysNumRound;

    if _pDigit = -1000 then
        _pDigit := sysCurRound;

    Result := RoundTo(_pExtended, _pDigit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CAN_NOT_EDIT_CHECKED = 'Dữ liệu đã %s, không thể chỉnh sửa.';

function  exIsChecked(ds: TDataSet; pAbort: Boolean = True): Boolean;
begin
    Result := ds.FieldByName('CHECKED').AsBoolean;
    if Result then
    begin
        ErrMsg(Format(RS_CAN_NOT_EDIT_CHECKED, ['Khóa']));
        if pAbort then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exChecked(pDataset: TDataSet; pRight: String);
begin
    with pDataset do
    begin
        if not FieldByName('CHECKED').AsBoolean then
        begin
            if not rCanEdit(GetRights(pRight + '_CHECKED')) then
                Exit;
            if not YesNo(Format('Dữ liệu sau khi %s không thể chỉnh sửa. Tiếp tục?', ['Khóa']), 1) then
                Exit;
        end
        else
        begin
            if not rCanEdit(GetRights(pRight + '_UNCHECKED')) then
                Exit;
            if not YesNo('Cho phép chỉnh sửa dữ liệu. Tiếp tục?', 1) then
                Exit;
        end;

        try
            mTrigger := True;
            SetEditState(pDataset);
            FieldByName('CHECKED').AsBoolean := not FieldByName('CHECKED').AsBoolean;
//            SetAudit(pDataset);
            Post;
        finally
            mTrigger := False;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetCheckedCaption(pDataSet: TDataSet): String;
begin
    with pDataSet.FieldByName('CHECKED') do
        if AsBoolean then
            Result := 'Mở khóa'
        else
            Result := 'Khóa';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exCheckNhomthue(pNhomthue: String; pDataset: TCustomADODataSet): Boolean;
begin
    Result := True;
    if pDataset.IsEmpty then
        Exit;

    with pDataset do
    begin
        First;
        if pNhomthue = '' then
            pNhomthue := FieldByName('LOAITHUE').AsString;
        while not Eof do
        begin
            if pNhomthue <> FieldByName('LOAITHUE').AsString then
            begin
                Msg(RS_VAT_NOTSAME);
                Result := False;
                Break;
            end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exCheckLoc(pDataset: TDataSet; pAbort: Boolean = True; AFIELD: string = 'LOC'): Boolean;
begin
    Result := SameText(pDataset.FieldByName(AFIELD).AsString, sysLoc);
    if (not Result) and pAbort then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  exIsCheckSL(pSl: Double; pMSG: Boolean = True): Boolean;
begin
    Result := (pSl >= soluongMin) and (pSl <= soluongMax);

    if not Result then
        if pMSG then
            ErrMsg(RS_INVALID_QTY);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure AddAllFields(pDataSet: TCustomADODataSet;
        pTable: String = ''; pIndex: Integer = 0);
var
    i, n: Integer;
    fld: TField;
begin
    with TADOQuery.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select * from [%s] where 1=0', [pTable]);
        Open;

        if pIndex = -1 then
            pDataSet.FieldDefs.Update
        else
        begin
            n := pDataSet.FieldDefs.Count;
            for i := pIndex to FieldCount - 1 do
            begin
                fld := Fields[i];
                if pDataSet.Fields.FindField(fld.FieldName) = nil then
                    pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;

            for i := n to pDataSet.FieldDefs.Count - 1 do
                pDataSet.FieldDefs[i].CreateField(pDataSet);
        end;

        Close;
        Free;
    end;

    for i := 0 to pDataSet.FieldDefs.Count - 1 do
        if pDataSet.FindField(pDataSet.FieldDefs[i].Name) = nil then
            pDataSet.FieldDefs[i].CreateField(pDataSet);
end;

procedure AddFields(pDataSet: TCustomADODataSet;
        pTable, pFields: String);
var
    i: Integer;
    fld: TField;
    ls: TStrings;
begin
    ls := TStringList.Create;
    isStrBreak(pFields, ';', ls);
    with TADOQuery.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select * from [%s] where 1=0', [pTable]);
        Open;
        if pFields = '' then
        begin
            for i := 0 to FieldCount - 1 do
            begin
                fld := Fields[i];
                if pDataSet.Fields.FindField(fld.FieldName) = nil then
                    pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;
        end
        else
        begin
            for i := 0 to ls.Count - 1 do
            begin
                fld := Fields.FindField(ls[i]);
                if fld <> nil then
                    if pDataSet.Fields.FindField(fld.FieldName) = nil then
                        pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;
        end;

        Close;
        Free;
    end;
    ls.Free;

    for i := 0 to pDataSet.FieldDefs.Count - 1 do
        if pDataSet.FindField(pDataSet.FieldDefs[i].Name) = nil then
            pDataSet.FieldDefs[i].CreateField(pDataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function IsDuplicateCode(pQuery: TCustomADODataSet;
	pValue: TField; pShowMgs: Boolean): Boolean;
begin
	Result := False;
	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
  		Clone(pQuery);

        if Locate(pValue.FieldName, pValue.Value, []) then
        begin
        	Result := RecNo <> pQuery.RecNo;
        	if Result and pShowMgs then
        		Msg(Format(RS_ITEM_CODE_DUPLICATE, [pValue.DisplayLabel]));
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function IsDuplicateCode2(pQuery: TCustomADODataSet;
	pValue: TField; var pBm: TBytes): Boolean;
begin
	Result := False;
	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
  		Clone(pQuery);

        if Locate(pValue.FieldName, pValue.Value, []) then
        begin
        	Result := RecNo <> pQuery.RecNo;
            pBm := Bookmark;
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetImportFile(const pType: Integer = 2): String;
begin
    case pType of
    1:	// Recorder data
    	Result := isGetOpenFileName('ALL', 1, GetSysParam('FOLDER_RECORDER'));
    2:	// Import data
    	Result := isGetOpenFileName('CSV;TXT;ALL', 1, GetSysParam('FOLDER_IMPORT'));
    3:	// Import data
    	Result := isGetOpenFileName('XLS;ALL', 1, GetSysParam('FOLDER_IMPORT'));
    else
    	Result := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exViewLog(const LogName, Content: String);
var
    s: String;
begin
	s := sysAppTempPath+ LogName + '.log';
    with TStringList.Create do
    begin
        Text := Content;
        SaveToFile(s);
        Free;
    end;
    ShellExecute(0, 'Open', PChar(s), nil,  nil, SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
begin
    Sender.LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo);
begin
    Sender.LookupTable.Filter := 'PLOAI=''NB''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exExecSQL(const sqlStr: String): Integer;
begin
	Result := DataMain.ExecSQL(sqlStr);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exReSyncRecord(DataSet: TCustomADODataSet; pAll: Boolean = False);
begin
    with DataSet do
    begin
        UpdateCursorPos;
        Recordset.Resync(Iif(pAll, adAffectAll, adAffectCurrent), adResyncAllValues);
        Resync([]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exDbCopyFields(pDataSet, pSource: TCustomADODataSet;
    pOveride: Boolean; pExceptField: String);
var
	i: Integer;
    fld: TField;
begin
    SetEditState(pDataSet);

    with pDataSet do
    for i := 0 to pSource.FieldCount - 1 do
    begin
        fld := FindField(pSource.Fields[i].FieldName);
        if (fld <> Nil) and (fld.FieldKind in [fkData]) and not fld.ReadOnly then
        begin
            if (pExceptField <> '') and (Pos(fld.FieldName + ';', pExceptField + ';') > 0) then
            else if pOveride or (FieldByName(fld.FieldName).Value <> pSource.FieldByName(fld.FieldName).Value) then
                    FieldByName(fld.FieldName).Value := pSource.FieldByName(fld.FieldName).Value;
        end;
    end;
end;


(*==============================================================================
** Đánh lại STT cho chi tiết phiếu trước khi lưu
**------------------------------------------------------------------------------
*)
procedure exSaveDetails(DataSet: TCustomADODataSet);
var
	bm: TBytes;
    n: Integer;
begin
    with DataSet do
    begin
        bm := Bookmark;
        DisableControls;

        First;
        mTrigger := True;
        while not Eof do
        begin
        	n := RecNo;
            if FieldByName('STT').AsInteger <> n then
            begin
                Edit;
                FieldByName('STT').AsInteger := n;
            end;
            Next;
        end;

        Bookmark := bm;
        EnableControls;
        UpdateBatch;
        mTrigger := False;
    end;
end;


(*==============================================================================
** Xóa chi tiết phiếu
**------------------------------------------------------------------------------
*)
procedure exEmptyDetails(DataSet: TDataSet; ActiveControl: TWinControl);
begin
	if not YesNo(RS_XOA_CHITIET, 1) then
    	Exit;
    DeleteConfirm(False);
	EmptyDataset(DataSet);
    DeleteConfirm(True);

    if Assigned(ActiveControl) then
    	ActiveControl.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exConfigInspector(pIns: TwwDataInspector; pFieldNames: String): Integer;
var
    i, ret: Integer;
    fld: TField;
	ls: TStrings;
    it: TwwInspectorItem;

begin
    ls := TStringList.Create;
    isStrBreak(pFieldNames, ';', ls);

    // Update list of fields
    for i := 0 to ls.Count - 1 do
    begin
        it := pIns.GetItemByFieldName(ls[i]);
        if it <> Nil then
        begin
            it.Caption := it.Field.DisplayName;
            it.Tag := 1;
        end
        else
            with pIns do
            begin
                fld := DataSource.DataSet.FindField(ls[i]);

                if fld <> Nil then
                begin

                    it := Items.Add;
                    it.DataField := ls[i];
                    it.Caption := fld.DisplayLabel;
		            it.Tag := 1;

                    // Checkbox
                    if fld.DataType = ftBoolean then
                        with it.PickList do
                        begin
                            DisplayAsCheckbox := True;
                            Items.Add('True');
                            Items.Add('False');
                        end
                end;
            end;
        it.Index := i;
    end;

	ret := 0;
    if ls.Count > 0 then
        with pIns do
        begin
            for i := 0 to Items.Count - 1 do
            begin
                Items.Items[i].Visible := Items.Items[i].Tag = 1;
                if Items.Items[i].Visible then
                	Inc(ret);
            end;
            Invalidate;
        end;

    ls.Free;
    Result := ret;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function CanEditLockedBy(Dataset: TDataSet; pShowMessage: Boolean): Boolean;
begin
    Result := True;
    with (Dataset as TADOQuery) do
    begin
        if FieldByName('LOCKED_BY').AsInteger <> 0 then
        begin
            Result := False;
            if pShowMessage then
                ErrMsg(Format(RS_EDIT_BY, [FieldByName('LK_LOCKED_NAME').AsString,
                            DateTimeToStr(FieldByName('LOCKED_DATE').AsDateTime)]));
//                if YesNo(Format(RS_EDIT_BY, [FieldByName('LK_LOCKED_NAME').AsString,
//                            DateTimeToStr(FieldByName('LOCKED_DATE').AsDateTime)]), 1) then
//                    Result := GetRights('PHIEU_BAOGIA_EDIT_LOCK') <> R_DENY
        end;
    end;
end;

procedure SetLockedBy;
begin
    with (Dataset as TADOQuery) do
    begin
        mTrigger := True;
        Edit;
        if b then
        begin
            FieldByName('LOCKED_BY').AsInteger := sysLogonUID;
            FieldByName('LOCKED_DATE').AsDateTime := Now;
        end else
        begin
            FieldByName('LOCKED_BY').Clear;
            FieldByName('LOCKED_DATE').Clear;
        end;
        Post;
        mTrigger := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function ShowReport (pTitle, pRepName : String; Args: array of Variant): Boolean; overload;
begin
    Result := FrmRep.Execute(pTitle, pRepName, Args);
end;
function ShowReport (pTitle, pRepName : String): Boolean; overload;
begin
    Result := ShowReport(pTitle, pRepName, []);
end;

function PrintReport ( pRepName : String; Args: array of Variant; const PrinterName: String; const pCopy: Integer): Boolean; overload;
begin
    Result := FrmRep.Execute('', pRepName, Args, True, True, PrinterName, pCopy);
end;

	(*
    ** Refs. functions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_USER =
        'USERNAME'#9'20'#9'Tên đăng nhập'#13 +
        'FULLNAME'#9'30'#9'Tên đầy đủ';

function exDotUSER(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_USER, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotUSER(Sender: TField): Boolean;
begin
	Result := exDotUSER(DataMain.QrUSER, Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MADT =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMadt(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MADT, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMadt(Sender: TField): Boolean;
begin
	Result := exDotMadt(DataMain.QrDM_KH_NCC, Sender);
end;

function exDotMadt(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MADT);
end;
function exDotMadt(var s: String): Boolean;
begin
    Result := exDotMadt(DataMain.QrDM_KH_NCC, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MAKH =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMakh(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MAKH, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMakh(Sender: TField): Boolean;
begin
	Result := exDotMakh(DataMain.QrDMKH, Sender);
end;

function exDotMakh(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MAKH);
end;
function exDotMakh(var s: String): Boolean;
begin
    Result := exDotMadt(DataMain.QrDMKH, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MANCC =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMancc(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MANCC, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMancc(Sender: TField): Boolean;
begin
    Result := exDotMancc(DataMain.QrDMNCC, Sender);
end;

function exDotMancc(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MANCC);
end;
function exDotMancc(var s: String): Boolean;
begin
    Result := exDotMancc(DataMain.QrDMNCC, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_VIP =
		'MAVIP'#9'15'#9'Mã VIP'#13 +
      	'HOTEN'#9'30'#9'Họ tên'#13 +
      	'MASO'#9'15'#9'Thẻ VIP';

function exDotVip(var s: String): Boolean;
begin
    Result := QuickSelect3(DataMain.QrDMVIP, s, DOT_SEARCH_VIP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_MAVT: array[1..4] of String = (
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp'
	);

procedure exInitDotMavt;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 4 do
		SetCustomGrid('REF_MAVT_' + IntToStr(i), DOT_SEARCH_MAVT[i]);
end;

function exDotMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond <> '' then
        exCond := exCond + ' and TINHTRANG=''01'''
    else
        exCond := 'TINHTRANG=''01''';
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_MAVT[Group], exCond);
end;

function exDotMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotMavt(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMavt(const Group: Integer; Sender: TField; exCond: String): Boolean;
begin
    Result := exDotMavt(Group, DataMain.QrDMVT, Sender, exCond);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_FB_MAVT: array[1..6] of String = (
        //Thành phẩm
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT',

        //Nguyên liệu
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT'#13 +
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13,

        //FB_DM_HH
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT'#13 +
        'GIAVON'#9'15'#9'Giá vốn'+
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13 +
        'GIABAN_CHUA_VAT'#9'15'#9'Giá bán chưa VAT'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        //Phiếu nhập, Xuất trả, ĐĐH Ncc
        'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'SKU'#13 +
        'QuyDoi'#9'8'#9'QĐ'#13 +
        'TenDvtLon'#9'8'#9'Hộp'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'TENLT'#9'15'#9'Thuế'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        'MANGANH'#9'15'#9'Mã'#13 +
        'TENNGANH'#9'30'#9'Tên'#13,

        'MANHOM'#9'15'#9'Mã'#13 +
        'TENNHOM'#9'30'#9'Tên'#13
	);

procedure exInitDotFBMavt;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 6 do
		SetCustomGrid('REF_FB_MAVT_' + IntToStr(i), DOT_SEARCH_FB_MAVT[i]);
end;

function exDotFBMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond = 'OTHER' then
        exCond := ''
    else
    begin
    if exCond <> '' then
        exCond := exCond + ' and MaNganTinhTrang =''01'''
    else
        exCond := 'MaNganTinhTrang =''01''';
    end;
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_FB_MAVT[Group], exCond);
end;

function exDotFBMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotFBMavt(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function cloneDataDetail(curKhoa: TGUID): Boolean;
var
    isCloned :Boolean;
begin

    with DataMain.CLONE_ORDER_TO_DETAIL do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(curKhoa);
        try
            Execute;
            isCloned := (Parameters[0].Value = 0)
        except
            DbeMsg;
        end;
    end;

    Result := isCloned;
end;

	(*
    ** End: Refs. functions
    *)


begin
	cfRecordDate := True;
end.

