﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CashierTableOrders;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Variants, Types,
  Db, wwDBGrid2, ADODB, Buttons, StdCtrls, Grids, Wwdbigrd, Wwdbgrid,
  AdvSmoothTileList, AdvSmoothTileListEx, ExtCtrls, HTMLabel, AppEvnts,
  Vcl.ActnList, RzButton, AdvSmoothTileListImageVisualizer,
  AdvSmoothTileListHTMLVisualizer, AdvGlowButton, AdvSmoothPanel,
  AdvSmoothScrollBar, AdvMetroScrollBox, DBAdvSmoothLabel, AdvSmoothLabel
  ,DateUtils, GDIPFill, GDIPAPI, GDIPOBJ,
   Graphics, Menus, Mask, wwdblook, isEnv, AdvMenus, fcStatusBar, RzPanel, wwdbedit,
    ImgList, pngimage, AdvGlassButton, AdvSmoothButton, AdvMetroButton,
    AdvSelectors;

type
  TFrmCashierTableOrders = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    QrDmKhuVuc: TADOQuery;
    MyActionList: TActionList;
    CmdTableOrders: TAction;
    CmdOrders: TAction;
    CmdChangeTable: TAction;
    CmdMergeTable: TAction;
    CmdSplitTable: TAction;
    CmdClose: TAction;
    PaBoard: TPanel;
    Panel3: TPanel;
    spDmBan: TADOStoredProc;
    PaMain: TPanel;
    PaBottom: TPanel;
    Panel1: TPanel;
    AdvGlowButton3: TAdvGlowButton;
    Panel2: TPanel;
    AdvGlowButton1: TAdvGlowButton;
    Panel6: TPanel;
    AdvGlowButton2: TAdvGlowButton;
    Panel9: TPanel;
    Panel8: TPanel;
    TtKhuVuc: TAdvSmoothTileListEx;
    TtKhuVucBan: TAdvSmoothTileListEx;
    spMergeTable: TADOStoredProc;
    spChangeTable: TADOStoredProc;
    Panel5: TPanel;
    AdvGlowButton4: TAdvGlowButton;
    QrBHSplit: TADOQuery;
    QrBHSplitCALC_TONGCK: TFloatField;
    QrBHSplitCALC_TIENTHOI: TFloatField;
    QrBHSplitLK_TENKHO: TWideStringField;
    QrBHSplitLCT: TWideStringField;
    QrBHSplitNGAY: TDateTimeField;
    QrBHSplitQUAY: TWideStringField;
    QrBHSplitCA: TWideStringField;
    QrBHSplitSCT: TWideStringField;
    QrBHSplitMAVIP: TWideStringField;
    QrBHSplitMAKHO: TWideStringField;
    QrBHSplitCHIETKHAU_MH: TFloatField;
    QrBHSplitSOTIEN: TFloatField;
    QrBHSplitSOLUONG: TFloatField;
    QrBHSplitCHUATHOI: TFloatField;
    QrBHSplitTHANHTOAN: TFloatField;
    QrBHSplitDGIAI: TWideMemoField;
    QrBHSplitPRINTED: TBooleanField;
    QrBHSplitCREATE_BY: TIntegerField;
    QrBHSplitCREATE_DATE: TDateTimeField;
    QrBHSplitKHOA: TGuidField;
    QrBHSplitTINHTRANG: TWideStringField;
    QrBHSplitCN_TENDV: TWideStringField;
    QrBHSplitCN_MST: TWideStringField;
    QrBHSplitCN_DIACHI: TWideStringField;
    QrBHSplitCN_DIACHI_HD: TWideStringField;
    QrBHSplitCN_LIENHE: TWideStringField;
    QrBHSplitCN_DTHOAI: TWideStringField;
    QrBHSplitCN_EMAIL: TWideStringField;
    QrBHSplitCN_MATK: TWideStringField;
    QrBHSplitLK_TENTK: TWideStringField;
    QrBHSplitLK_DAIDIEN: TWideStringField;
    QrBHSplitLK_NGANHANG: TWideStringField;
    QrBHSplitPRINT_NO: TIntegerField;
    QrBHSplitLOC: TWideStringField;
    QrBHSplitDELIVERY: TBooleanField;
    QrBHSplitTIENTHOI: TFloatField;
    QrBHSplitLK_TENVIP: TWideStringField;
    QrBHSplitLK_VIP_DTHOAI: TWideStringField;
    QrBHSplitLK_VIP_DCHI: TWideStringField;
    QrBHSplit_id: TLargeintField;
    QrBHSplitMAQUAY: TWideStringField;
    QrBHSplitMABAN: TWideStringField;
    QrBHSplitSOLUONG_KHACH: TFloatField;
    QrBHSplitLK_BAN: TWideStringField;
    QrBHSplitHINHTHUC_GIA: TWideStringField;
    QrBHSplitUPDATE_DATE: TDateTimeField;
    QrBHSplitUPDATE_BY: TIntegerField;
    QrCTBHSplit: TADOQuery;
    QrCTBHSplitSTT: TIntegerField;
    QrCTBHSplitMAVT: TWideStringField;
    QrCTBHSplitTENVT: TWideStringField;
    QrCTBHSplitTENTAT: TWideStringField;
    QrCTBHSplitLK_TENVT: TWideStringField;
    QrCTBHSplitLK_TENVT_KHONGDAU: TWideStringField;
    QrCTBHSplitLK_TENTAT: TWideStringField;
    QrCTBHSplitDVT: TWideStringField;
    QrCTBHSplitDONGIA: TFloatField;
    QrCTBHSplitSOLUONG: TFloatField;
    QrCTBHSplitSOTIEN: TFloatField;
    QrCTBHSplitGHICHU: TWideStringField;
    QrCTBHSplitRSTT: TIntegerField;
    QrCTBHSplitCALC_SOTIEN_SAUCK: TFloatField;
    QrCTBHSplitKHOA: TGuidField;
    QrCTBHSplitKHOACT: TGuidField;
    QrCTBHSplitLOC: TWideStringField;
    QrCTBHSplitBO: TBooleanField;
    DsBHSplit: TDataSource;
    DsCTBHSplit: TDataSource;
    PaLeft: TPanel;
    PaRight: TPanel;
    Panel12: TPanel;
    LbTable: TAdvSmoothLabel;
    EdTable: TDBAdvSmoothLabel;
    DBAdvSmoothLabel1: TDBAdvSmoothLabel;
    lbDbState: TAdvSmoothLabel;
    GrDetail: TwwDBGrid2;
    Panel13: TPanel;
    LbTableSplit: TAdvSmoothLabel;
    EdTableSplit: TDBAdvSmoothLabel;
    DBAdvSmoothLabel2: TDBAdvSmoothLabel;
    AdvSmoothLabel1: TAdvSmoothLabel;
    GrDetailSplit: TwwDBGrid2;
    CmdIns: TAction;
    CmdDel: TAction;
    QrCTBHSplitDVT2: TWideStringField;
    Panel14: TPanel;
    QrCTBHSplitCREATE_BY: TIntegerField;
    QrCTBHSplitUPDATE_BY: TIntegerField;
    QrCTBHSplitCREATE_DATE: TDateTimeField;
    QrCTBHSplitUPDATE_DATE: TDateTimeField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    CmdInsAll: TAction;
    CmdDelAll: TAction;
    BitBtn4: TBitBtn;
    QrCTBHSplitLK_BO: TBooleanField;
    QrBHSplitSOLUONG_DAT: TFloatField;
    QrCTBHSplitSOLUONG_DAT: TFloatField;
    QrCTBHSplitORDER_NO: TIntegerField;
    QrBHSplitNGAYVAO: TDateTimeField;
    QrBHSplitQUAY_ORDER: TWideStringField;
    QrBHSplitMAQUAY_ORDER: TWideStringField;
    QrCTBHSplitMAKHO: TWideStringField;
    QrCTBHSplitTHOIGIAN_CHEBIEN: TFloatField;
    QrCTBHSplitORDER_BY: TIntegerField;
    QrCTBHSplitORDER_DATE: TDateTimeField;
    BtnPrev: TBitBtn;
    BitBtn5: TBitBtn;
    Panel19: TPanel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    CmdAreaPrev: TAction;
    CmdAreaNext: TAction;
    CmdAreaTablePrev: TAction;
    CmdAreaTableNext: TAction;
    QrCTBHSplit_id: TLargeintField;
    QrBHSplitCO_HOADON: TBooleanField;
    QrBHSplitCN_GHICHU: TWideStringField;
    QrBHSplitMAVIP_HOTEN: TWideStringField;
    QrBHSplitTL_PHUTHU: TFloatField;
    QrBHSplitORDER_BY: TIntegerField;
    QrBHSplitORDER_DATE: TDateTimeField;
    QrBHSplitTHUCTRA: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
    procedure TtKhuVucTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure CmdMergeTableExecute(Sender: TObject);
    procedure CmdChangeTableExecute(Sender: TObject);
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TtKhuVucBanTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure QrBHSplitBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHSplitBeforeOpen(DataSet: TDataSet);
    procedure QrCTBHSplitMAVTChange(Sender: TField);
    procedure QrCTBHSplitSOLUONGChange(Sender: TField);
    procedure QrCTBHSplitAfterInsert(DataSet: TDataSet);
    procedure QrBHSplitAfterInsert(DataSet: TDataSet);
    procedure QrCTBHSplitAfterPost(DataSet: TDataSet);
    procedure QrCTBHSplitBeforePost(DataSet: TDataSet);
    procedure QrCTBHSplitSOTIENChange(Sender: TField);
    procedure QrCTBHSplitCalcFields(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdInsAllExecute(Sender: TObject);
    procedure CmdInsOneExecute(Sender: TObject);
    procedure CmdDelAllExecute(Sender: TObject);
    procedure CmdDelOneExecute(Sender: TObject);
    procedure TtKhuVucBanTileFill(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState; Fill: TGDIPFill);
    procedure TtKhuVucBanTileFont(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState; AFont: TFont);
    procedure QrBHSplitBeforePost(DataSet: TDataSet);
    procedure QrCTBHSplitSOLUONG_GIAOChange(Sender: TField);
    procedure QrCTBHSplitSOLUONG_DATChange(Sender: TField);
    procedure CmdAreaPrevExecute(Sender: TObject);
    procedure CmdAreaNextExecute(Sender: TObject);
    procedure CmdAreaTablePrevExecute(Sender: TObject);
    procedure CmdAreaTableNextExecute(Sender: TObject);
    procedure GrDetailCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure GrDetailSplitCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrCTBHSplitAfterDelete(DataSet: TDataSet);
  private
    procedure tbAddUnit;
    procedure tbAddGroup;
    procedure OpenQueries;
    procedure TotalList(pReOrder: Boolean = False);
    procedure MoveIns(pMoveAll: Boolean = True);
    procedure MoveDel(pMoveAll: Boolean = True);
  public
    mMAKV, mMABAN, mMABAN_NEW, mTENBAN_NEW, mACTION : String;
    curKhoa, toKhoa: TGUID;
    toStt, toIndex: Integer;
    tyLePhuThu: Double;
    function Execute(sMAKV, sMABAN, sTENBAN: string; sACTION: string; sKhoa: TGUID): Boolean;
    function  updateContentTable(fIndex: Integer): Boolean;

  end;

var
  FrmCashierTableOrders: TFrmCashierTableOrders;

implementation

{$R *.DFM}

uses
    isLib, ExCommon, isDb, GuidEx, isCommon, PosMain, isMsg, KeypadNumeric,
    CashierMain, MainData, isStr;
const
    FORM_CODE = 'FB_POS_BANHANG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
   RS_CONFIRM_PHUTHU = 'Phụ thu có thay đổi, tỷ lệ phụ thu mới là "%s". Tiếp tục?';
procedure TFrmCashierTableOrders.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);

begin
    //
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmCashierTableOrders.CmdAreaNextExecute(Sender: TObject);
begin
    FrmCashierMain.smoothTitleBackNext(TtKhuVuc);
    //FrmCashierMain.enabledButtonBackNext(TtKhuVuc, CmdAreaPrev, CmdAreaNext);
end;

procedure TFrmCashierTableOrders.CmdAreaPrevExecute(Sender: TObject);
begin
    FrmCashierMain.smoothTitleBackNext(TtKhuVuc, False);
    //FrmCashierMain.enabledButtonBackNext(TtKhuVuc, CmdAreaPrev, CmdAreaNext);
end;

procedure TFrmCashierTableOrders.CmdAreaTableNextExecute(Sender: TObject);
begin
     FrmCashierMain.smoothTitleBackNext(TtKhuVucBan);
     //FrmCashierMain.enabledButtonBackNext(TtKhuVucBan, CmdAreaTablePrev, CmdAreaTableNext);
end;

procedure TFrmCashierTableOrders.CmdAreaTablePrevExecute(Sender: TObject);
begin
     FrmCashierMain.smoothTitleBackNext(TtKhuVucBan, False);
     //FrmCashierMain.enabledButtonBackNext(TtKhuVucBan, CmdAreaTablePrev, CmdAreaTableNext);
end;

procedure TFrmCashierTableOrders.CmdChangeTableExecute(Sender: TObject);
var
    aContinute: Boolean;
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin
        aContinute := False;
        if (tyLePhuThu <> FrmCashierMain.QrBH.FieldByName('TL_PHUTHU').AsFloat) then
        begin
            aContinute := True;
            if YesNo(Format(RS_CONFIRM_PHUTHU, [FloatToStrF(tyLePhuThu, ffNumber, 10, 1) + ' %']), 1) then
            begin
                with FrmCashierMain.QrBH do
                begin
                    Edit;
                    FieldByName('TL_PHUTHU').AsFloat := tyLePhuThu;
                    Post;
                end;
                //noted
                cloneDataDetail(curKhoa);
                aContinute := False;
            end;
        end;

        if not aContinute then
        begin
            with spChangeTable do
            begin
                Prepared := True;
                Parameters[1].Value := TGuidEx.ToString(curKhoa);
                Parameters[2].Value := mMABAN_NEW;
                Parameters[3].Value := sysLogonUID;
                ExecProc;

                if Parameters[0].Value = 1 then
                begin
                    ErrMsg('Lỗi chuyển bàn. Vui lòng liên hệ với người quản trị hệ thống');
                    Exit;
                end;
            end;

            FrmCashierMain.mMABAN := mMABAN_NEW;
            FrmCashierMain.CmdRefreshExecute(Nil);
            Close;
        end;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.CmdCloseExecute(Sender: TObject);
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin
        FrmCashierMain.CmdRefreshExecute(Nil);
        FrmCashierMain.RefreshSelectTable(mMABAN);
    end;
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.CmdDelExecute(Sender: TObject);
begin
    MoveDel(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


 procedure TFrmCashierTableOrders.CmdDelAllExecute(Sender: TObject);
begin
     MoveDel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.CmdDelOneExecute(Sender: TObject);
begin
    MoveDel(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmCashierTableOrders.CmdInsAllExecute(Sender: TObject);
begin
    MoveIns;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.CmdInsOneExecute(Sender: TObject);
begin
    MoveIns(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.CmdInsExecute(Sender: TObject);
begin
    MoveIns(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierTableOrders.CmdMergeTableExecute(Sender: TObject);
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin
        with spMergeTable do
        begin
            Prepared := True;
            Parameters[1].Value := TGuidEx.ToString(curKhoa);
            Parameters[2].Value := TGuidEx.ToString(toKhoa);
            Parameters[3].Value := sysLogonUID;
            ExecProc;

            if Parameters[0].Value = 1 then
            begin
                ErrMsg('Lỗi gộp bàn. Vui lòng liên hệ với người quản trị hệ thống');
                Exit;
            end;
        end;

        FrmCashierMain.CmdRefreshExecute(Nil);
        FrmCashierMain.RefreshSelectTable(mMABAN);
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCashierTableOrders.Execute(sMAKV, sMABAN, sTENBAN: string; sACTION: string; sKhoa: TGUID): Boolean;
begin
    mMAKV := sMAKV;
    mMABAN := sMABAN;
    mACTION := sACTION;
    curKhoa := sKhoa;
    EdTable.Caption.Text := sTENBAN;

    Caption := 'BÀN ' + sTENBAN;

    if sACTION = 'MERGE' then
        Panel2.Visible := True;

    if sACTION = 'CHANGE' then
        Panel1.Visible := True;

    if sACTION = 'SPLIT' then
    begin
        //Panel7.Visible := False;
        PaRight.Visible := True;
    end;

    Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierTableOrders.FormShow(Sender: TObject);
var
    selectedIndex: Integer;
begin
    TMyForm(Self).Init;

    with spDmBan do
    begin
        if Active then
            Close;

        ExecProc;
    end;

    OpenDatasets([QrDmKhuVuc, spDmBan]);

     // Tuy bien luoi
    SetCustomGrid(FORM_CODE + '_MAIN', GrDetail);

    SetCustomGrid(FORM_CODE + '_MAIN', GrDetailSplit);

    FrmCashierMain.spDmBanTemp.Requery;


    SetDisplayFormat(QrBHSplit, sysCurFmt);
    SetDisplayFormat(QrBHSplit,['SOLUONG', 'SOLUONG_DAT'], sysQtyFmt);
    SetShortDateFormat(QrBHSplit);

    with QrCTBHSplit do
    begin
		SetDisplayFormat(QrCTBHSplit, sysCurFmt);
        SetDisplayFormat(QrCTBHSplit,['SOLUONG', 'SOLUONG_DAT'], sysQtyFmt);
    end;


    EdTableSplit.Caption.Text := '';
    DBAdvSmoothLabel2.Caption.Text := '';

    CmdInsAll.Enabled := False;
    CmdIns.Enabled := False;
    CmdDelAll.Enabled := False;
    CmdDel.Enabled := False;

    // Touch
    with TtKhuVuc do
    begin
        if (mACTION = 'SPLIT') then
            Columns := 4
        else
            Columns := 5;
        Rows := 1;
    end;

    with TtKhuVucBan do
    begin
        if (mACTION = 'SPLIT') then
            Columns := 4
        else
            Columns := 5;

        Rows :=  6;
    end;

    FrmCashierMain.updateArea(TtKhuVuc, clWindowText, clNone, clWindowText, $0000000f, clWhite, 10903848);

    FrmCashierMain.updateAreaTable(TtKhuVucBan, clWindowText, clWhite, clGray , clWindowText, clWhite, clTeal, clWhite, clTeal, clTeal);

    tbAddGroup;
    tbAddUnit;

    selectedIndex := FrmCashierMain.getIndexTileListByValue(mMAKV, TtKhuVuc);

    TtKhuVuc.SelectTile(selectedIndex);
    TtKhuVucTileClick(TtKhuVuc, TtKhuVuc.Tiles[selectedIndex], tsNormal);

    //FrmCashierMain.enabledButtonBackNext(TtKhuVuc, CmdAreaPrev, CmdAreaNext);

    //FrmCashierMain.enabledButtonBackNext(TtKhuVucBan, CmdAreaTablePrev, CmdAreaTableNext);
end;

procedure TFrmCashierTableOrders.GrDetailCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if ((Field.DataSet.FieldByName('ORDER_NO').IsNull) or (Field.DataSet.FieldByName('ORDER_NO').AsInteger = 0)) and
    (not Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').IsNull) and (Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').AsFloat > 0) then
    begin
        GrDetail.Canvas.Font.Color := 10903848;
    end;
end;

procedure TFrmCashierTableOrders.GrDetailSplitCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if ((Field.DataSet.FieldByName('ORDER_NO').IsNull) or (Field.DataSet.FieldByName('ORDER_NO').AsInteger = 0)) and
    (not Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').IsNull) and (Field.DataSet.FieldByName('THOIGIAN_CHEBIEN').AsFloat > 0) then
    begin
        GrDetailSplit.Canvas.Font.Color := 10903848;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdMergeTable.Enabled := (mMABAN_NEW <> '');
    CmdChangeTable.Enabled := (mMABAN_NEW <> '');
    CmdSplitTable.Enabled := (mMABAN_NEW <> '');

    with FrmCashierMain.QrCTBH do
    begin
    	if not Active then
        	Exit;

        CmdInsAll.Enabled := (not IsEmpty) and (mMABAN_NEW <> '');
        CmdIns.Enabled := (not IsEmpty) and (mMABAN_NEW <> '');
    end;

    with QrCTBHSplit do
    begin
    	if not Active then
        	Exit;

        CmdDelAll.Enabled := (not IsEmpty) and (mMABAN_NEW <> '') ;
        CmdDel.Enabled := (not IsEmpty) and (mMABAN_NEW <> '');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmCashierTableOrders.QrBHSplitAfterInsert(DataSet: TDataSet);
begin
    toStt := 0;
	with QrBHSplit do
    begin

		TGuidField(FieldByName('KHOA')).AsGuid := toKhoa;
        FieldByName('_id').Value := NewIntID;
		//FieldByName('NGAY').AsDateTime := Now;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
        FieldByName('CREATE_DATE').AsDateTime := Now;
		FieldByName('CREATE_BY').AsInteger := sysLogonUID;

        FieldByName('LCT').AsString := FrmCashierMain.mLCT;
		//FieldByName('QUAY').AsString := posQuay;
        //FieldByName('MAQUAY').AsString := posMaQuay;
		//FieldByName('CA').AsString := '1';
        FieldByName('QUAY_ORDER').AsString := posQuay;
        FieldByName('MAQUAY_ORDER').AsString := posMaQuay;
        FieldByName('NGAYVAO').AsDateTime := Now;
		FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('HINHTHUC_GIA').AsString := sysHinhThucGia;
        FieldByName('DELIVERY').AsBoolean := False;
        FieldByName('CO_HOADON').AsBoolean := False;
        FieldByName('MABAN').AsString := mMABAN_NEW;
        FieldByName('SOLUONG_KHACH').AsInteger := 1;
        FieldByName('TL_PHUTHU').AsFloat := tyLePhuThu;
        FieldByName('ORDER_DATE').AsDateTime := Now;
		FieldByName('ORDER_BY').AsInteger := sysLogonUID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrBHSplitBeforeOpen(DataSet: TDataSet);
begin
   with QrBHSplit do
   begin
        Parameters[0].Value := TGuidEx.ToString(toKhoa);
   end;
end;

procedure TFrmCashierTableOrders.QrBHSplitBeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierTableOrders.QrCTBHSplitAfterDelete(DataSet: TDataSet);
var
    result: Boolean;
begin
    result := cloneDataDetail(toKhoa);
    if result then
    begin
         exReSyncRecord(QrBHSplit);
    end;
end;

procedure TFrmCashierTableOrders.QrCTBHSplitAfterInsert(DataSet: TDataSet);
begin
    with QrCTBHSplit do
    begin
    	Inc(toStt);
        FieldByName('_id').Value := NewIntID;
    	FieldByName('STT').AsInteger := toStt;
        TGuidField(FieldByName('KHOA')).AsVariant := QrBHSplit.FieldByName('KHOA').AsVariant;
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
        FieldByName('LOC').AsString := QrBHSplit.FieldByName('LOC').AsString;
        FieldByName('MAKHO').AsString := posMakho;
        FieldByName('ORDER_BY').AsInteger := sysLogonUID;
    end;

    // lay ngay gio de tinh chiet khau, khuyen mai.
    if Is1Record(QrCTBHSplit) and (not sysIsCentral) then
    begin
        SetEditState(QrBHSplit);
        QrBHSplit.FieldByName('NGAY').AsDateTime := Now;
    end;
end;

procedure TFrmCashierTableOrders.QrCTBHSplitAfterPost(DataSet: TDataSet);
var
    result: Boolean;
begin
    if mTrigger then
    	Exit;

    TotalList;

    // Reread DMVT
    with QrCTBHSplit do
    begin
        Edit;
        if FieldByName('LK_TENVT').AsString = '' then
        begin
            FieldByName('LK_TENVT').RefreshLookupList;
            FieldByName('LK_TENVT_KHONGDAU').RefreshLookupList;
        end;

        result := cloneDataDetail(toKhoa);
        if result then
        begin
             exReSyncRecord(QrBHSplit);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitBeforeOpen(DataSet: TDataSet);
begin
    with QrCTBHSplit do
    begin
    	Parameters[0].Value := TGuidEx.ToString(toKhoa);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

    SetAudit(DataSet);

	with QrBHSplit do
    if State in [dsInsert] then
    begin
        Post;
        Edit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitMAVTChange(Sender: TField);
begin
    with QrCTBHSplit do
    begin
        FieldByName('TENVT').AsString := FieldByName('LK_TENVT').AsString;
        FieldByName('TENTAT').AsString := FieldByName('LK_TENTAT').AsString;
        FieldByName('DVT').AsString := FieldByName('LK_DVT').AsString;
        FieldByName('BO').AsBoolean := FieldByName('LK_BO').AsBoolean;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitSOLUONGChange(Sender: TField);
var
	sl, dg, st: Double;
begin
    if mTrigger then
    	Exit;

	with QrCTBHSplit do
    begin
        if (Sender <> Nil) then
        begin
            dg  := FieldByName('DONGIA').AsFloat;
            sl  := FieldByName('SOLUONG').AsFloat;
            st := exVNDRound(sl*dg);
            FieldByName('SOTIEN').AsFloat := st;
        end;
        GrDetailSplit.InvalidateCurrentRow;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitSOLUONG_DATChange(Sender: TField);
begin
    if mTrigger then
    	Exit;

	with QrCTBHSplit do
    begin
        if (Sender <> Nil) then
        begin
            if FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG_DAT').AsFloat then
            begin
                Edit;
                FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG_DAT').AsFloat;
            end;
        end;
        GrDetailSplit.InvalidateCurrentRow;
	end;
end;

procedure TFrmCashierTableOrders.QrCTBHSplitSOLUONG_GIAOChange(Sender: TField);
begin
    if mTrigger then
    	Exit;

	with QrCTBHSplit do
    begin
        if (Sender <> Nil) then
        begin
            if FieldByName('SOLUONG').AsFloat > FieldByName('SOLUONG_DAT').AsFloat then
            begin
                Edit;
                FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG_DAT').AsFloat;
            end;
        end;
        GrDetailSplit.InvalidateCurrentRow;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.QrCTBHSplitSOTIENChange(Sender: TField);
begin
//    with QrCTBHSplit do
//    begin
//        if FieldByName('SOTIEN').AsFloat <= 0 then
//        begin
//            Delete;
//        end;
//
//        if IsEmpty and not QrBHSplit.IsEmpty then
//        begin
//            QrBHSplit.Delete;
//        end;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
    	CloseDataSets([QrBHSplit, QrCTBHSplit, QrDmKhuVuc]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    //CanClose := CheckBrowseDataSet(QrBHSplit, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierTableOrders.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then
        Close
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCashierTableOrders.tbAddGroup;
var
    mTiles: TAdvSmoothTile;
begin
    TtKhuVuc.Tiles.Clear;
    TtKhuVuc.BeginUpdate;
    with QrDmKhuVuc do
    begin
        First;
        while not Eof do
        begin
            mTiles := TtKhuVuc.Tiles.Add;
            with mTiles do
            begin
                Data := FieldByName('MAKV').AsString;
                Content.Text := FieldByName('TENKV').AsString;
                Content.TextPosition := tpCenterCenter;
                Tag := 0;
            end;
            Next;
        end;
    end;
    TtKhuVuc.EndUpdate;
    TtKhuVuc.FirstPage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.tbAddUnit;
var
    fIndex: Integer;
begin
    TtKhuVucBan.Tiles.Clear;
    TtKhuVucBan.BeginUpdate;
    with spDmBan do
    begin
        First;
        fIndex := 0;
        while not Eof do
        begin
            with TtKhuVucBan.Tiles.Add do
            begin
                FrmCashierMain.drawContentTable(spDmBan, TtKhuVucBan, fIndex);
                Inc(fIndex);
            end;
            Next;
        end;
    end;
    TtKhuVucBan.EndUpdate;
    TtKhuVucBan.FirstPage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.TtKhuVucBanTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    aContinute: Boolean;
begin
    mMABAN_NEW := Tile.Data;
    toIndex := Tile.Index;
    with spDmBan do
    if Locate('MABAN', mMABAN_NEW, []) then
    begin
        aContinute := False;
        tyLePhuThu := FieldByName('TL_PHUTHU').AsFloat;
        if (mACTION = 'SPLIT') and (tyLePhuThu <> FrmCashierMain.QrBH.FieldByName('TL_PHUTHU').AsFloat) then
        begin
            aContinute := True;
            if YesNo(Format(RS_CONFIRM_PHUTHU, [FloatToStrF(tyLePhuThu, ffNumber, 10, 1) + ' %']), 1) then
            begin
                aContinute := False;
            end;
        end;

        if not aContinute then
        begin
            mTENBAN_NEW := FieldByName('TENBAN').AsString;
            EdTableSplit.Caption.Text := mTENBAN_NEW;
            toKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
            OpenQueries;
        end
        else
        begin
            mMABAN_NEW := '';
            toIndex := -1;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.TtKhuVucBanTileFill(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState; Fill: TGDIPFill);
begin
    with FrmCashierMain.spDmBanTemp do
    begin
        if Locate('MABAN', Tile.Data, []) then
        begin
            if FieldByName('TINHTRANG').AsInteger = 1 then
            begin
                Fill.Color :=  FrmCashierMain.activeBackground;
                Fill.BorderColor := FrmCashierMain.activeBackground;
            end
            else if FieldByName('TINHTRANG').AsInteger = 2 then
            begin
                Fill.Color := FrmCashierMain.orderBackground;
                Fill.BorderColor := FrmCashierMain.orderBackground;
            end
            else
            begin
                Fill.Color := FrmCashierMain.emptyBackground;

            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.TtKhuVucBanTileFont(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState; AFont: TFont);
begin
    with FrmCashierMain.spDmBanTemp do
    begin
        if Locate('MABAN', Tile.Data, []) then
        begin
            if FieldByName('TINHTRANG').AsInteger = 1 then
            begin
                AFont.Color := FrmCashierMain.activeColor;
                updateContentTable(toIndex);
            end
            else if FieldByName('TINHTRANG').AsInteger = 2 then
            begin
                AFont.Color := FrmCashierMain.orderColor;
            end
            else
            begin
                 AFont.Color := FrmCashierMain.emptyColor;
            end;
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.TtKhuVucTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Data;
    if (s <> '') and (s <> 'ALL') then
      sf := 'MAKV=' + QuotedStr(s);


    if mACTION = 'MERGE' then
      sf := sf + ' and (TINHTRANG  < 2) and (MABAN <> ' + QuotedStr(mMABAN) + ')';

    if (mACTION = 'CHANGE') then
      sf := sf + ' and (TINHTRANG = 0)';

    if (mACTION = 'SPLIT') then
      sf := sf + ' and (TINHTRANG = 0)';

    if (mACTION = 'SPLIT') and (mMABAN_NEW <> '') then
      sf := sf + ' or ((TINHTRANG = 1) and (MABAN = ' + QuotedStr(mMABAN_NEW) + ')) ';

    mMAKV := s;
    spDmBan.Filter := sf;
    tbAddUnit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmCashierTableOrders.OpenQueries;
begin
    CloseDataSets([QrBHSplit, QrCTBHSplit]);
    OpenDataSets([QrBHSplit, QrCTBHSplit]);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.TotalList;
var
    bm: TBytes;
    xSotien, mSotien, mSoluong, mSoluongdat: Double;
begin
    if mTrigger then
    	Exit;

	mSotien := 0;
    mSoluong := 0;
    mSoluongdat := 0;

	with QrCTBHSplit do
    begin
        DisableControls;
        bm := BookMark;

        First;
        while not Eof do
        begin
            if pReOrder and (FieldByName('STT').AsInteger <> FieldByName('RSTT').AsInteger)then
            begin
                try
                    mTrigger := True;
                    SetEditState(QrCTBHSplit);
                    FieldByName('STT').AsInteger := FieldByName('RSTT').AsInteger;
                    Post;
                finally
                    mTrigger := False;
                end;
            end;

            xSotien := FieldByName('SOTIEN').AsFloat;
            mSotien := mSotien + xSotien;
            mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
            mSoluongdat := mSoluongdat + FieldByName('SOLUONG_DAT').AsFloat;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

    with QrBHSplit do
    begin
        if State in [dsBrowse] then
        	Edit;

    	FieldByName('SOTIEN').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
        FieldByName('SOLUONG_DAT').AsFloat := mSoluongdat;

        CheckBrowseMode;
    end;

    with QrCTBHSplit do
    begin
        if IsEmpty then
        begin
           FrmCashierMain.deleteOrderByKey(toKhoa);
           QrBHSplit.Delete;
        end;
    end;

    updateContentTable(toIndex);
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.MoveIns(pMoveAll: Boolean = True);
var
    sMAVT: string;
    sSOLUONG, sSOLUONG_THUC, sSOLUONG_GIAO_DAT, sSOLUONG_DAT, sDONGIA: Double;
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin
        sSOLUONG_THUC := 1;
        sSOLUONG_DAT := 1;

        with QrBHSplit do
        begin
            if IsEmpty then
            begin
                toKhoa := DataMain.GetNewGuid;
                Append;
            end
            else
                Edit;
        end;

        with FrmCashierMain.QrCTBH do
        begin

           sMAVT := FieldByName('MAVT').AsString;
           sDONGIA := FieldByName('DONGIA').AsFloat;
           sSOLUONG := FieldByName('SOLUONG').AsFloat;
           sSOLUONG_GIAO_DAT := FieldByName('SOLUONG_DAT').AsFloat;

           if pMoveAll then
           begin
              sSOLUONG_THUC := sSOLUONG;
              sSOLUONG_DAT := sSOLUONG_GIAO_DAT;
              sSOLUONG := 0;
              sSOLUONG_GIAO_DAT := 0;
           end
           else
           begin
              sSOLUONG := sSOLUONG - 1;
              if sSOLUONG < 1 then
                  sSOLUONG_DAT := sSOLUONG_GIAO_DAT
              else
                  sSOLUONG_GIAO_DAT := sSOLUONG_GIAO_DAT - 1;
              
              if sSOLUONG < 0 then
              begin
                  sSOLUONG_THUC := 0;
              end;
           end;

           if sSOLUONG_GIAO_DAT < 1 then
           begin
               sSOLUONG_GIAO_DAT := 0;
           end;

           if sSOLUONG < 1 then
           begin
               sSOLUONG := 0;
           end
           else
           begin
               Edit;
               FieldByName('SOLUONG').AsFloat := sSOLUONG;
               FieldByName('SOLUONG_DAT').AsFloat := sSOLUONG_GIAO_DAT;
           end;

           with QrCTBHSplit do
           begin
               if Locate('MAVT;ORDER_NO;ORDER_BY', VarArrayOf([sMAVT, FrmCashierMain.QrCTBH.FieldByName('ORDER_NO').AsInteger, FrmCashierMain.QrCTBH.FieldByName('ORDER_BY').AsInteger]), []) then
               begin
                    Edit;
               end
               else
               begin
                    Append;
                    FieldByName('MAVT').AsString := sMAVT;
                    FieldByName('DONGIA').AsFloat := sDONGIA;
                    FieldByName('ORDER_NO').AsInteger := FrmCashierMain.QrCTBH.FieldByName('ORDER_NO').AsInteger;
                    FieldByName('THOIGIAN_CHEBIEN').AsInteger := FrmCashierMain.QrCTBH.FieldByName('THOIGIAN_CHEBIEN').AsInteger;

               end;
               FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG').AsFloat + sSOLUONG_THUC;
               FieldByName('SOLUONG_DAT').AsFloat := FieldByName('SOLUONG_DAT').AsFloat + sSOLUONG_DAT;
           end;

           if sSOLUONG = 0 then
           begin
                with FrmCashierMain.QrCTBH do
                begin
                    Delete;
                end;
           end;

           FrmCashierMain.Total(True);
           TotalList(true);

           FrmCashierMain.mMABAN := mMABAN;
           FrmCashierMain.CmdRefreshExecute(Nil);

        end;
    end;
end;

(* ==============================================================================
  **------------------------------------------------------------------------------
*)

procedure TFrmCashierTableOrders.MoveDel(pMoveAll: Boolean = True);
var
    sMAVT: string;
    sSOLUONG, sSOLUONG_THUC, sSOLUONG_GIAO_DAT, sSOLUONG_DAT, sDONGIA: Double;
begin
    if (mMABAN_NEW <> '') and ( mMABAN <> '') then
    begin
        with FrmCashierMain.QrBH do
        begin
            if IsEmpty then
            begin
                Append;
            end
            else
                Edit;
        end;

        sSOLUONG_THUC := 1;
        sSOLUONG_DAT := 1;

        with QrCTBHSplit do
        begin
           sMAVT := FieldByName('MAVT').AsString;
           sDONGIA := FieldByName('DONGIA').AsFloat;
           sSOLUONG := FieldByName('SOLUONG').AsFloat;
           sSOLUONG_GIAO_DAT := FieldByName('SOLUONG_DAT').AsFloat;


           if pMoveAll then
           begin
              sSOLUONG_THUC := sSOLUONG;
              sSOLUONG_DAT := sSOLUONG_GIAO_DAT;
              sSOLUONG := 0;
              sSOLUONG_GIAO_DAT := 0;
           end
           else
           begin
              sSOLUONG := sSOLUONG - 1;
              if sSOLUONG < 1 then
                sSOLUONG_DAT := sSOLUONG_GIAO_DAT
              else
                sSOLUONG_GIAO_DAT := sSOLUONG_GIAO_DAT - 1;

              if sSOLUONG < 0 then
              begin
                  sSOLUONG_THUC := 0;
              end;
           end;

           if sSOLUONG_GIAO_DAT < 1 then
           begin
               sSOLUONG_GIAO_DAT := 0;
           end;

           if sSOLUONG < 1 then
           begin
               sSOLUONG := 0;
           end
           else
           begin
               Edit;
               FieldByName('SOLUONG').AsFloat := sSOLUONG;
               FieldByName('SOLUONG_DAT').AsFloat := sSOLUONG_GIAO_DAT;
           end;

           with  FrmCashierMain.QrCTBH do
           begin
               if Locate('MAVT;ORDER_NO;ORDER_BY', VarArrayOf([sMAVT, QrCTBHSplit.FieldByName('ORDER_NO').AsInteger, QrCTBHSplit.FieldByName('ORDER_BY').AsInteger]), []) then
               begin
                    Edit;
               end
               else
               begin
                    Append;
                    FieldByName('MAVT').AsString := sMAVT;
                    FieldByName('DONGIA').AsFloat := sDONGIA;
                    FieldByName('ORDER_NO').AsInteger := QrCTBHSplit.FieldByName('ORDER_NO').AsInteger;
               end;
               FieldByName('SOLUONG').AsFloat := FieldByName('SOLUONG').AsFloat + sSOLUONG_THUC;
               FieldByName('SOLUONG_DAT').AsFloat := FieldByName('SOLUONG_DAT').AsFloat + sSOLUONG_DAT;
           end;

           if sSOLUONG = 0 then
           begin
                with QrCTBHSplit do
                begin
                    Delete;
                end;
           end;

           FrmCashierMain.Total(True);
           TotalList(true);

           FrmCashierMain.mMABAN := mMABAN;
           FrmCashierMain.CmdRefreshExecute(Nil);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmCashierTableOrders.updateContentTable(fIndex: Integer): Boolean;

begin
    FrmCashierMain.spDmBanTemp.Requery;
    with FrmCashierMain.spDmBanTemp do
    if Locate('MABAN', mMABAN_NEW, []) then
    begin
       FrmCashierMain.drawContentTable(FrmCashierMain.spDmBanTemp, TtKhuVucBan, fIndex);
    end;
    Result := True;
end;

end.
