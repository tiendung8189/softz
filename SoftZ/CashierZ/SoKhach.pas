﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SoKhach;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Vcl.Mask, wwdbedit, DBAdvSmoothLabel,
  AdvSmoothLabel, AdvSmoothTouchKeyBoard, RzButton, Vcl.ActnList;

type
  TFrmSoKhach = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdSoKhach: TwwDBEdit;
    BtClose: TRzBitBtn;
    LbTable: TAdvSmoothLabel;
    EdTable: TDBAdvSmoothLabel;
    lbDbState: TAdvSmoothLabel;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    MyActionList: TActionList;
    CmdWinKeyboard: TAction;
    procedure FormShow(Sender: TObject);
    procedure AdvSmoothTouchKeyBoard1KeyClick(Sender: TObject; Index: Integer);
    procedure CmdWinKeyboardExecute(Sender: TObject);
   
  private
    mMABAN: string;
  public
    function Execute(sMABAN, sTENBAN: string): Boolean;
  end;

var
  FrmSoKhach: TFrmSoKhach;

implementation

{$R *.DFM}

uses
	isLib, isStr, isMsg, CashierMain;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmSoKhach.AdvSmoothTouchKeyBoard1KeyClick(Sender: TObject;
  Index: Integer);
begin
   case Index of
    14:
        CmdWinKeyboard.Execute;
    end;
end;

procedure TFrmSoKhach.CmdWinKeyboardExecute(Sender: TObject);
begin

    with FrmCashierMain.QrBH do
    begin
        Edit;
        Post;
    end;
    FrmCashierMain.mMABAN := mMABAN;
    FrmCashierMain.CmdRefreshExecute(Nil);
    Close;
end;

function TFrmSoKhach.Execute;
begin
    mMABAN := sMABAN;
    EdTable.Caption.Text := sTENBAN;
    Caption := 'BÀN ' + sTENBAN;

    with FrmCashierMain.QrBH do
    begin
        if IsEmpty then
        begin
            Append;
            FrmCashierMain.GrDetail.InvalidateCurrentRow;
        end;
    end;

   	Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmSoKhach.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;
    EdSoKhach.SelectAll;
    EdSoKhach.SetFocus;
end;
end.
