﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThongtinCN_View;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, wwdblook, Mask, wwdbedit,
  ActnList, RzButton;

type
  TFrmThongtinCN_View = class(TForm)
    Panel1: TPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    MyActionList: TActionList;
    CmdOK: TAction;
    Label15: TLabel;
    wwDBEdit4: TwwDBEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    BtClose: TRzBitBtn;
    CmdClose: TAction;
    Panel2: TPanel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdCloseExecute(Sender: TObject);
  private
  public
  	function Execute : Boolean;
  end;

var
  FrmThongtinCN_View: TFrmThongtinCN_View;

implementation

uses
	isLib, MainData, isDb, isMsg, BanleCN;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN_View.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN_View.Execute;
begin
	Result := ShowModal = mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN_View.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN_View.FormShow(Sender: TObject);
var
    bHd: Boolean;
begin
    TMyForm(Self).Init;

    bHd := False;
    if not bHd then
        Self.Height := Self.Height - GroupBox1.Height;
    GroupBox1.Visible := bHd;

end;

end.
