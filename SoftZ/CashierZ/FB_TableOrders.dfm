object FrmFB_TableOrders: TFrmFB_TableOrders
  Left = 135
  Top = 87
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #272#7863't B'#224'n'
  ClientHeight = 573
  ClientWidth = 859
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    859
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 859
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 859
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 66
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object TntToolButton2: TToolButton
      Left = 66
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 74
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 140
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 206
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 214
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 280
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 288
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton5: TToolButton
      Left = 354
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object TntToolButton3: TToolButton
      Left = 362
      Top = 0
      Cursor = 1
      Action = CmdDatBan
      ImageIndex = 12
    end
    object ToolButton6: TToolButton
      Left = 428
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 436
      Top = 0
      Action = CmdHuyBan
      ImageIndex = 9
    end
    object BtnBarCode2: TToolButton
      Left = 502
      Top = 0
      Width = 8
      Caption = 'BtnBarCode2'
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 510
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 859
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 851
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 851
        Height = 483
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'LYDO'#9'30'#9'L'#253' do'#9'F'
          'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsKM
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrBrowseCalcCellColors
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 851
        Height = 195
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        DesignSize = (
          851
          195)
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 51
          Top = 62
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#7885' v'#224' t'#234'n'
        end
        object Label5: TLabel
          Left = 30
          Top = 13
          Width = 75
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y, gi'#7901' '#273#7863't'
        end
        object Label10: TLabel
          Left = 55
          Top = 134
          Width = 49
          Height = 16
          Alignment = taRightJustify
          Caption = 'Di'#7877'n gi'#7843'i'
        end
        object Label3: TLabel
          Left = 317
          Top = 85
          Width = 62
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' b'#224'n '#273#7863't'
        end
        object Label1: TLabel
          Left = 47
          Top = 85
          Width = 58
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7879'n tho'#7841'i'
        end
        object Label2: TLabel
          Left = 46
          Top = 108
          Width = 59
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#236'nh tr'#7841'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 5
          Top = 37
          Width = 100
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y, gi'#7901' h'#7871't h'#7841'n'
        end
        object Label6: TLabel
          Left = 210
          Top = 85
          Width = 51
          Height = 16
          Alignment = taRightJustify
          Caption = 'SL kh'#225'ch'
        end
        object Label8: TLabel
          Left = 256
          Top = 13
          Width = 124
          Height = 16
          Alignment = taRightJustify
          Caption = ' Th'#7901'i gian gi'#7919' ch'#7895' (p)'
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 111
          Top = 10
          Width = 145
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdLyDo: TwwDBEdit
          Left = 111
          Top = 58
          Width = 321
          Height = 22
          Ctl3D = False
          DataField = 'HOTEN'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit1: TDBMemo
          Left = 111
          Top = 130
          Width = 321
          Height = 55
          Ctl3D = False
          DataField = 'DGIAI'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
        end
        object EdGiaban: TwwDBEdit
          Left = 384
          Top = 82
          Width = 48
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'SOLUONG_BAN'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit3: TwwDBEdit
          Left = 111
          Top = 82
          Width = 95
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'DTHOAI'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbTinhTrang: TwwDBLookupCombo
          Left = 111
          Top = 106
          Width = 321
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'27'#9'Tr'#7841'ng th'#225'i'#9'F')
          DataField = 'TINHTRANG'
          DataSource = DsKM
          LookupTable = QrTINHTRANG
          LookupField = 'MA_HOTRO'
          Options = [loColLines, loSearchOnBackspace]
          Style = csDropDownList
          Color = clBtnFace
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ImageList = DataMain.ImageMark
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object CbNGAYHETHAN: TwwDBDateTimePicker
          Left = 111
          Top = 34
          Width = 145
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY_HETHAN'
          DataSource = DsKM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 2
        end
        object wwDBEdit2: TwwDBEdit
          Left = 266
          Top = 82
          Width = 48
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'SOLUONG_KHACH'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit4: TwwDBEdit
          Left = 385
          Top = 10
          Width = 47
          Height = 22
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'THOIGIAN_GIUCHO'
          DataSource = DsKM
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object Panel1: TPanel
          Left = 438
          Top = 2
          Width = 411
          Height = 191
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 9
          object isPanel2: TisPanel
            Left = 1
            Top = 1
            Width = 409
            Height = 189
            Align = alClient
            BevelOuter = bvNone
            Color = 16119285
            Ctl3D = True
            ParentBackground = False
            ParentCtl3D = False
            TabOrder = 0
            HeaderCaption = ' :: Danh s'#225'ch b'#224'n '#273#227' '#273#7863't'
            HeaderColor = 16119285
            ImageSet = 4
            RealHeight = 0
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = DEFAULT_CHARSET
            HeaderFont.Color = clBlue
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            ExplicitWidth = 335
            ExplicitHeight = 103
            object GrDetail2: TwwDBGrid2
              Left = 0
              Top = 16
              Width = 409
              Height = 173
              DittoAttributes.ShortCutDittoField = 16397
              DittoAttributes.Options = [wwdoSkipReadOnlyFields]
              Selected.Strings = (
                'MABAN'#9'19'#9'M'#227' b'#224'n'#9'F'
                'LK_TENBAN'#9'17'#9'T'#234'n b'#224'n'#9'F'
                'LK_SOLUONG'#9'15'#9'S'#7889' kh'#225'ch'#9'F')
              IniAttributes.Delimiter = ';;'
              TitleColor = 13360356
              FixedCols = 0
              ShowHorzScrollBar = True
              EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
              Align = alClient
              DataSource = DsDatBan
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              KeyOptions = []
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgWordWrap, dgShowCellHint, dgProportionalColResize]
              ParentFont = False
              ParentShowHint = False
              ShowHint = False
              TabOrder = 1
              TitleAlignment = taCenter
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = 8404992
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = [fsBold]
              TitleLines = 1
              TitleButtons = False
              UseTFields = False
              ImageList = DataMain.ImageMark
              TitleImageList = DataMain.ImageSort
              FooterColor = 13360356
              FooterCellColor = 13360356
              PadColumnStyle = pcsPadHeader
              GroupFieldName = 'LK_KHUVUC'
              ExplicitWidth = 405
              ExplicitHeight = 160
            end
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 195
        Width = 851
        Height = 288
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 851
          Height = 272
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'RSTT'#9'8'#9'STT'#9'T'
            'MAVT'#9'44'#9'M'#227' h'#224'ng'#9'F'
            'LK_TenDvt'#9'8'#9#272'VT'#9'F'
            'GIABAN'#9'37'#9'Gi'#225' b'#225'n'#9'T'
            'TL_CK'#9'16'#9'%C.K'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint, dgProportionalColResize]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
      object Status2: TStatusBar
        Left = 0
        Top = 483
        Width = 851
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 717
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 717
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 146
    Top = 250
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdDatBan: TAction
      Caption = 'Ch'#7885'n b'#224'n'
      Hint = 'Ch'#7885'n b'#224'n t'#7915' danh s'#225'ch b'#224'n'
      OnExecute = CmdDatBanExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChonNhom: TAction
      Caption = 'Ch'#7885'n nh'#243'm h'#224'ng'
    end
    object CmdEmpty: TAction
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyExecute
    end
    object CmdChonNCC: TAction
      Caption = 'Ch'#7885'n nh'#224' cung c'#7845'p'
    end
    object CmdChonphieuKM: TAction
      Caption = 'Ch'#7885'n phi'#7871'u khuy'#7871'n m'#227'i'
    end
    object CmdChonNhomNCC: TAction
      Caption = 'Ch'#7885'n nh'#224' cung c'#7845'p v'#224' nh'#243'm h'#224'ng'
    end
    object CmdIntem: TAction
      Caption = 'In tem'
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdImportExcel: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
    end
    object CmdDMTD: TAction
      Caption = 'Th'#7921'c '#273#417'n'
    end
    object CmdDMNPL: TAction
      Caption = 'Nguy'#234'n ph'#7909' li'#7879'u F&&B'
    end
    object CmdHuyBan: TAction
      Caption = 'H'#7911'y b'#224'n'
      OnExecute = CmdHuyBanExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsKM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 114
    Top = 250
  end
  object QrDMHH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.*, c.TENDT, t.TENLT, e.DGIAI TenDvt, f.DGIAI TenDvtLon,' +
        ' h.MA MaNganTinhTrang, h.TEN_HOTRO TenTinhTrang'
      '  from'#9'FB_DM_HH a '
      #9#9'left join DM_KH_NCC c on a.MADT = c.MADT '
      #9#9'left join DM_LOAITHUE t on a.LOAITHUE = t.MALT'
      #9#9'left join DM_DVT e on a.MaDvt = e.MA'
      #9#9'left join DM_DVT f on a.MaDvtLon = f.MA'
      
        '        left join V_FB_DMVT_TINHTRANG h on a.TINHTRANG = h.MA_HO' +
        'TRO'
      
        ' where'#9'IsNull(a.BO, 0) = 0 and a.MANGANH in (select MANGANH from' +
        ' DM_NGANH where LOAI = '#39'FBTP'#39')'
      'order by  a.MAVT'
      '')
    Left = 480
    Top = 291
  end
  object QrKM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKMBeforeOpen
    BeforeInsert = QrKMBeforeInsert
    AfterInsert = QrKMAfterInsert
    BeforeEdit = QrKMBeforeEdit
    BeforePost = QrKMBeforePost
    AfterCancel = QrKMAfterCancel
    AfterScroll = QrKMAfterScroll
    OnCalcFields = QrKMCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_DATBAN'
      ' where     LCT = :LCT')
    Left = 418
    Top = 291
    object QrKMXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrKMIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrKMNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
      OnChange = QrKMNGAYChange
    end
    object QrKMIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Calculated = True
    end
    object QrKMSCT: TWideStringField
      DisplayLabel = 'S'#7889' ch'#7913'ng t'#7915
      FieldName = 'SCT'
    end
    object QrKMDGIAI: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrKMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKMLYDO: TWideStringField
      FieldName = 'LYDO'
      Size = 200
    end
    object QrKMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKMDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrKMDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrKMLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrKMLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrKMKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrKMCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrKMCHECKED_BY: TIntegerField
      FieldName = 'CHECKED_BY'
    end
    object QrKMCHECKED_DATE: TDateTimeField
      FieldName = 'CHECKED_DATE'
    end
    object QrKMCHECKED_DESC: TWideMemoField
      FieldName = 'CHECKED_DESC'
      BlobType = ftWideMemo
    end
    object QrKMLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_KM
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrKMHOTEN: TWideStringField
      FieldName = 'HOTEN'
      Size = 200
    end
    object QrKMDTHOAI: TWideStringField
      FieldName = 'DTHOAI'
      Size = 200
    end
    object QrKMDS_MABAN: TWideStringField
      FieldName = 'DS_MABAN'
      Size = 200
    end
    object QrKMSOLUONG_KHACH: TFloatField
      FieldName = 'SOLUONG_KHACH'
    end
    object QrKMTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      OnChange = QrKMTINHTRANGChange
      Size = 5
    end
    object QrKMTINHTRANG_DATE: TDateTimeField
      FieldName = 'TINHTRANG_DATE'
    end
    object QrKMTHOIGIAN_GIUCHO: TFloatField
      FieldName = 'THOIGIAN_GIUCHO'
      OnChange = QrKMNGAYChange
    end
    object QrKMNGAY_HETHAN: TDateTimeField
      FieldName = 'NGAY_HETHAN'
    end
    object QrKMSOLUONG_BAN: TFloatField
      FieldName = 'SOLUONG_BAN'
    end
    object QrKMCAL_NGAYHIENTAI: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CAL_NGAYHIENTAI'
      Calculated = True
    end
    object QrKMLK_TEN_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TEN_TINHTRANG'
      LookupDataSet = QrTINHTRANG
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'TINHTRANG'
      Lookup = True
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterInsert
    BeforeEdit = QrCTBeforeEdit
    BeforePost = QrCTBeforePost
    BeforeDelete = QrCTBeforeDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from FB_DATBAN_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 446
    Top = 291
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTMAVT: TWideStringField
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      Size = 15
    end
    object QrCTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTGIABAN: TFloatField
      FieldName = 'GIABAN'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTTL_CK: TFloatField
      FieldName = 'TL_CK'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrCTCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrCTUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrCTUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTLK_GIABAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_GIABAN'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'GIABAN_CHUA_VAT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTTINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'TINHTRANG'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNganTinhTrang'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTLK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSOLUONG: TFloatField
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrCTLK_TenDvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenDvt'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_QuyDoi: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_QuyDoi'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'QuyDoi'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_TenDvtLon: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenDvtLon'
      LookupDataSet = QrDMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvtLon'
      KeyFields = 'MAVT'
      Lookup = True
    end
  end
  object DsKM: TDataSource
    DataSet = QrKM
    Left = 418
    Top = 319
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 446
    Top = 319
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 276
    Top = 286
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PopItem1: TMenuItem
      Tag = 1
      Caption = 'Phi'#7871'u c'#243' hi'#7879'u l'#7921'c'
      OnClick = PopItemAllClick
    end
    object PopItem2: TMenuItem
      Tag = 2
      Caption = 'Phi'#7871'u h'#7871't hi'#7879'u l'#7921'c'
      OnClick = PopItemAllClick
    end
    object PopItemAll: TMenuItem
      Caption = 'T'#7845't c'#7843' c'#225'c phi'#7871'u'
      OnClick = PopItemAllClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 340
    Top = 286
  end
  object Filter2: TwwFilterDialog2
    SortBy = fdSortByFieldNo
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 116
    Top = 312
  end
  object QrTemp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'MAVT'
      '  from'#9'FB_DM_HH'
      ' where'#9'IsNull(a.BO, 0) = 0'
      '  ')
    Left = 512
    Top = 291
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 228
    Top = 286
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
  object QrTemp2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_DATBAN_CT'
      ' where  '#9'1 = 1')
    Left = 560
    Top = 291
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from FB_DM_HH'
      'order by TENVT')
    Left = 340
    Top = 384
    object QrDMMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrDMTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
  end
  object MemoDate: TwwMemoDialog
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -16
    Font.Name = 'Courier'
    Font.Style = []
    Caption = 'Danh S'#225'ch C'#225'c L'#7847'n Kh'#243'a Phi'#7871'u'
    DlgLeft = 0
    DlgTop = 0
    DlgWidth = 561
    DlgHeight = 396
    OnInitDialog = MemoDateInitDialog
    Left = 176
    Top = 404
  end
  object QrList: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'FB_TableOrders_DANHSACH;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@TYPE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@STRING'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 688
    Top = 324
  end
  object QrTINHTRANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'V_FB_DATBAN_TINHTRANG'
      '  ')
    Left = 392
    Top = 387
  end
  object DsDatBan: TDataSource
    AutoEdit = False
    DataSet = QrDatBan
    Left = 524
    Top = 384
  end
  object QrBan: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'FB_DM_BAN'
      'order by MAKV')
    Left = 452
    Top = 388
  end
  object QrKhuVuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'MAKV, TENKV, VIP, TL_PHUTHU, GHICHU'
      '  from '#9'FB_DM_BAN_KHUVUC')
    Left = 584
    Top = 368
  end
  object QrDatBan: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrDatBanBeforeOpen
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'FB_DATBAN_MABAN'
      'where KHOA =:KHOA')
    Left = 520
    Top = 344
    object GuidField1: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object WideStringField1: TWideStringField
      FieldName = 'MABAN'
      Size = 5
    end
    object BooleanField1: TBooleanField
      FieldName = 'MARK'
    end
    object WideStringField2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENBAN'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'TENBAN'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrDatBanLK_MAKV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MAKV'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'MAKV'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrDatBanLK_KHUVUC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_KHUVUC'
      LookupDataSet = QrKhuVuc
      LookupKeyFields = 'MAKV'
      LookupResultField = 'TENKV'
      KeyFields = 'LK_MAKV'
      Lookup = True
    end
    object IntegerField1: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_SOLUONG'
      LookupDataSet = QrBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'SOLUONG'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object WideStringField3: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
  end
end
