object FrmCashierMain: TFrmCashierMain
  Left = 306
  Top = 173
  Caption = 'SoftZ Solutions'
  ClientHeight = 744
  ClientWidth = 1018
  Color = 16119285
  Constraints.MinWidth = 1024
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = TntFormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaMain: TPanel
    Left = 0
    Top = 0
    Width = 1018
    Height = 744
    Align = alClient
    TabOrder = 0
    object PaRight: TPanel
      Left = 1
      Top = 1
      Width = 636
      Height = 742
      Align = alClient
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 634
        Height = 440
        Align = alTop
        BiDiMode = bdLeftToRight
        Ctl3D = False
        ParentBiDiMode = False
        ParentCtl3D = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 632
          Height = 60
          Align = alTop
          BevelInner = bvRaised
          Color = clActiveCaption
          ParentBackground = False
          TabOrder = 0
          object TtKhuVuc: TAdvSmoothTileListEx
            Left = 44
            Top = 2
            Width = 544
            Height = 56
            Cursor = 1
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Transparent = True
            AnimationFactor = 2.000000000000000000
            Fill.Color = clNone
            Fill.ColorTo = clNone
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtVertical
            Fill.Opacity = 0
            Fill.OpacityTo = 0
            Fill.OpacityMirror = 0
            Fill.OpacityMirrorTo = 0
            Fill.BorderColor = clSilver
            Fill.BorderWidth = 0
            Fill.Rounding = 0
            Fill.ShadowColor = 13408767
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Tiles = <
              item
                Content.Text = '$$$'
                Content.TextPosition = tpBottomCenter
                ContentMaximized.Text = 'Description for Tile 1'
                ContentMaximized.Extra = '$$$'
                StatusIndicator = '$$$$'
                DeleteIndicator = 'X'
                StatusIndicatorLeft = -20
                StatusIndicatorTop = 1
                SubTiles = <>
                Tag = 0
                Extra = '$$$'
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end>
            Columns = 5
            Rows = 1
            TileAppearance.LargeViewFill.Color = 15581579
            TileAppearance.LargeViewFill.ColorTo = 12889148
            TileAppearance.LargeViewFill.ColorMirror = clNone
            TileAppearance.LargeViewFill.ColorMirrorTo = clNone
            TileAppearance.LargeViewFill.GradientType = gtSolid
            TileAppearance.LargeViewFill.GradientMirrorType = gtNone
            TileAppearance.LargeViewFill.Opacity = 180
            TileAppearance.LargeViewFill.BorderColor = 10987431
            TileAppearance.LargeViewFill.Rounding = 0
            TileAppearance.LargeViewFill.ShadowOffset = 0
            TileAppearance.LargeViewFill.Glow = gmNone
            TileAppearance.SmallViewFill.Color = clNone
            TileAppearance.SmallViewFill.ColorTo = clNone
            TileAppearance.SmallViewFill.ColorMirror = clNone
            TileAppearance.SmallViewFill.ColorMirrorTo = clNone
            TileAppearance.SmallViewFill.GradientType = gtSolid
            TileAppearance.SmallViewFill.GradientMirrorType = gtNone
            TileAppearance.SmallViewFill.BorderColor = clNone
            TileAppearance.SmallViewFill.Rounding = 5
            TileAppearance.SmallViewFill.ShadowOffset = 1
            TileAppearance.SmallViewFill.Glow = gmNone
            TileAppearance.SmallViewFillSelected.Color = 10903848
            TileAppearance.SmallViewFillSelected.ColorTo = 10903848
            TileAppearance.SmallViewFillSelected.ColorMirror = clNone
            TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillSelected.GradientType = gtSolid
            TileAppearance.SmallViewFillSelected.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillSelected.BorderColor = clNone
            TileAppearance.SmallViewFillSelected.BorderWidth = 3
            TileAppearance.SmallViewFillSelected.Rounding = 5
            TileAppearance.SmallViewFillSelected.ShadowColor = clNone
            TileAppearance.SmallViewFillSelected.ShadowOffset = 0
            TileAppearance.SmallViewFillSelected.Glow = gmNone
            TileAppearance.SmallViewFillSelected.GlowGradientColor = clNone
            TileAppearance.SmallViewFillSelected.GlowRadialColor = clNone
            TileAppearance.SmallViewFillDisabled.Color = clWhite
            TileAppearance.SmallViewFillDisabled.ColorTo = clWhite
            TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
            TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
            TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillDisabled.BorderColor = clBlue
            TileAppearance.SmallViewFillDisabled.BorderOpacity = 128
            TileAppearance.SmallViewFillDisabled.BorderWidth = 3
            TileAppearance.SmallViewFillDisabled.Rounding = 5
            TileAppearance.SmallViewFillDisabled.ShadowOffset = 0
            TileAppearance.SmallViewFillDisabled.Glow = gmNone
            TileAppearance.SmallViewFillHover.ColorTo = clSilver
            TileAppearance.SmallViewFillHover.ColorMirror = clNone
            TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillHover.GradientType = gtSolid
            TileAppearance.SmallViewFillHover.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillHover.Opacity = 154
            TileAppearance.SmallViewFillHover.OpacityTo = 154
            TileAppearance.SmallViewFillHover.BorderColor = clNone
            TileAppearance.SmallViewFillHover.BorderOpacity = 0
            TileAppearance.SmallViewFillHover.BorderWidth = 3
            TileAppearance.SmallViewFillHover.Rounding = 5
            TileAppearance.SmallViewFillHover.ShadowColor = clNone
            TileAppearance.SmallViewFillHover.ShadowOffset = 0
            TileAppearance.SmallViewFillHover.Glow = gmNone
            TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
            TileAppearance.LargeViewFont.Color = clMaroon
            TileAppearance.LargeViewFont.Height = -16
            TileAppearance.LargeViewFont.Name = 'Tahoma'
            TileAppearance.LargeViewFont.Style = []
            TileAppearance.SmallViewFont.Charset = 163
            TileAppearance.SmallViewFont.Color = clWindowText
            TileAppearance.SmallViewFont.Height = -15
            TileAppearance.SmallViewFont.Name = 'Tahoma'
            TileAppearance.SmallViewFont.Style = [fsBold]
            TileAppearance.SmallViewFontSelected.Charset = 163
            TileAppearance.SmallViewFontSelected.Color = clWindowText
            TileAppearance.SmallViewFontSelected.Height = -15
            TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
            TileAppearance.SmallViewFontSelected.Style = [fsBold]
            TileAppearance.SmallViewFontDisabled.Charset = 163
            TileAppearance.SmallViewFontDisabled.Color = clWindowText
            TileAppearance.SmallViewFontDisabled.Height = -15
            TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
            TileAppearance.SmallViewFontDisabled.Style = [fsBold]
            TileAppearance.SmallViewFontHover.Charset = 163
            TileAppearance.SmallViewFontHover.Color = clWindowText
            TileAppearance.SmallViewFontHover.Height = -15
            TileAppearance.SmallViewFontHover.Name = 'Tahoma'
            TileAppearance.SmallViewFontHover.Style = [fsBold]
            TileAppearance.VerticalSpacing = 0
            TileAppearance.HorizontalSpacing = 0
            TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
            TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
            TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = clRed
            TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 128
            TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 8
            TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
            TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
            TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
            TileAppearance.StatusIndicatorAppearance.Font.Color = clMaroon
            TileAppearance.StatusIndicatorAppearance.Font.Height = -11
            TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
            TileAppearance.StatusIndicatorAppearance.Font.Style = []
            TileAppearance.StatusIndicatorAppearance.Glow = False
            TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
            TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
            TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
            TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
            TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
            TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
            TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
            TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
            TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
            TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
            TileAppearance.DeleteIndicatorAppearance.Font.Color = clWhite
            TileAppearance.DeleteIndicatorAppearance.Font.Height = -11
            TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
            TileAppearance.DeleteIndicatorAppearance.Font.Style = []
            TileMargins.Left = 0
            TileMargins.Top = 0
            TileMargins.Right = 0
            TileMargins.Bottom = 0
            Header.Visible = False
            Header.Fill.Color = clNone
            Header.Fill.ColorTo = clNone
            Header.Fill.ColorMirror = clNone
            Header.Fill.ColorMirrorTo = clNone
            Header.Fill.GradientType = gtVertical
            Header.Fill.GradientMirrorType = gtVertical
            Header.Fill.BorderColor = clNone
            Header.Fill.Rounding = 0
            Header.Fill.ShadowColor = clNone
            Header.Fill.ShadowOffset = 0
            Header.Fill.Glow = gmNone
            Header.Height = 10
            Header.BulletSize = 6
            Header.ArrowNavigation = False
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Footer.Visible = False
            Footer.Fill.Color = 10655892
            Footer.Fill.ColorTo = 8749179
            Footer.Fill.ColorMirror = 8156785
            Footer.Fill.ColorMirrorTo = 6775131
            Footer.Fill.GradientType = gtVertical
            Footer.Fill.GradientMirrorType = gtVertical
            Footer.Fill.BorderColor = 6973542
            Footer.Fill.Rounding = 0
            Footer.Fill.ShadowOffset = 0
            Footer.Fill.Glow = gmNone
            Footer.BulletSize = 15
            Footer.ArrowSize = 0
            Footer.ArrowRectangleSize = 0
            Footer.ArrowNavigation = False
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Options = []
            OnTileClick = TtKhuVucTileClick
            Align = alClient
            Ctl3D = True
            TabOrder = 0
            BevelInner = bvNone
            BevelOuter = bvNone
            DoubleBuffered = True
            ParentCtl3D = False
            ParentFont = False
            TMSStyle = 0
          end
          object BtnPrev: TBitBtn
            Left = 588
            Top = 2
            Width = 42
            Height = 56
            Cursor = 1
            Action = CmdAreaNext
            Align = alRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000101000189551390F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000101000189551390F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000101000189551390F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000101000189551390F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000101000189551390F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000010100018955
              1390F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000008250
              1288F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000082501288F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000082501288F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000082501288F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000082501288F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000082501288F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentFont = False
            Spacing = -4
            TabOrder = 1
          end
          object BitBtn1: TBitBtn
            Left = 2
            Top = 2
            Width = 42
            Height = 56
            Cursor = 1
            ParentCustomHint = False
            Action = CmdAreaPrev
            Align = alLeft
            BiDiMode = bdLeftToRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF9159149801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF915914980101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF9159
              1498010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF915914980101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF91591498010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF9159149801010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF8854138F01010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF8854138F010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF8854138F0101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF8854
              138F010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF8854138F0101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF8854138F01010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentBiDiMode = False
            ParentFont = False
            Spacing = -4
            TabOrder = 2
          end
        end
        object TtKhuVucBan: TAdvSmoothTileListEx
          Left = 1
          Top = 61
          Width = 632
          Height = 328
          Cursor = 1
          Transparent = True
          AnimationFactor = 1.000000000000000000
          Fill.Color = clNone
          Fill.ColorTo = clNone
          Fill.ColorMirror = clNone
          Fill.ColorMirrorTo = clNone
          Fill.GradientType = gtVertical
          Fill.GradientMirrorType = gtVertical
          Fill.BackGroundPictureMode = pmInsideFill
          Fill.PictureSize = psCustom
          Fill.PictureWidth = 1008
          Fill.PictureHeight = 428
          Fill.PictureAspectRatio = True
          Fill.PictureAspectMode = pmNormal
          Fill.Opacity = 98
          Fill.OpacityTo = 98
          Fill.BorderColor = clWhite
          Fill.BorderOpacity = 0
          Fill.BorderWidth = 0
          Fill.Rounding = 0
          Fill.ShadowColor = clNone
          Fill.ShadowOffset = 0
          Fill.Glow = gmNone
          Tiles = <
            item
              Content.Text = '$$$ 4444 555 666 777 888 999 1000 222'
              Content.TextPosition = tpHTMLAlign
              ContentMaximized.ImageIndex = 74
              ContentMaximized.Text = 'Description for Tile 1'
              ContentMaximized.Extra = '$$$'
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              StatusIndicator = '$$$$'
              DeleteIndicator = 'X'
              StatusIndicatorLeft = -20
              StatusIndicatorTop = 10
              CanDelete = False
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer1
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end>
          Columns = 5
          TileAppearance.LargeViewFill.Color = 16776697
          TileAppearance.LargeViewFill.ColorTo = 16775920
          TileAppearance.LargeViewFill.ColorMirror = clNone
          TileAppearance.LargeViewFill.ColorMirrorTo = clNone
          TileAppearance.LargeViewFill.GradientType = gtSolid
          TileAppearance.LargeViewFill.GradientMirrorType = gtVertical
          TileAppearance.LargeViewFill.PicturePosition = ppTopCenter
          TileAppearance.LargeViewFill.PictureSize = psCustom
          TileAppearance.LargeViewFill.Opacity = 180
          TileAppearance.LargeViewFill.BorderColor = 16577242
          TileAppearance.LargeViewFill.Rounding = 0
          TileAppearance.LargeViewFill.ShadowOffset = 0
          TileAppearance.LargeViewFill.Glow = gmNone
          TileAppearance.SmallViewFill.Color = clWhite
          TileAppearance.SmallViewFill.ColorTo = clWhite
          TileAppearance.SmallViewFill.ColorMirror = clNone
          TileAppearance.SmallViewFill.ColorMirrorTo = clNone
          TileAppearance.SmallViewFill.GradientType = gtSolid
          TileAppearance.SmallViewFill.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFill.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFill.PictureSize = psCustom
          TileAppearance.SmallViewFill.Opacity = 180
          TileAppearance.SmallViewFill.BorderColor = clGray
          TileAppearance.SmallViewFill.BorderOpacity = 173
          TileAppearance.SmallViewFill.Rounding = 5
          TileAppearance.SmallViewFill.ShadowColor = clWhite
          TileAppearance.SmallViewFill.ShadowOffset = 0
          TileAppearance.SmallViewFill.Glow = gmNone
          TileAppearance.SmallViewFillSelected.Color = 10066329
          TileAppearance.SmallViewFillSelected.ColorTo = 10066329
          TileAppearance.SmallViewFillSelected.ColorMirror = clNone
          TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillSelected.GradientType = gtSolid
          TileAppearance.SmallViewFillSelected.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillSelected.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillSelected.PictureSize = psCustom
          TileAppearance.SmallViewFillSelected.Opacity = 180
          TileAppearance.SmallViewFillSelected.BorderColor = clGray
          TileAppearance.SmallViewFillSelected.Rounding = 5
          TileAppearance.SmallViewFillSelected.ShadowColor = clNone
          TileAppearance.SmallViewFillSelected.ShadowOffset = 1
          TileAppearance.SmallViewFillSelected.Glow = gmNone
          TileAppearance.SmallViewFillSelected.GlowGradientColor = clNone
          TileAppearance.SmallViewFillSelected.GlowRadialColor = clNone
          TileAppearance.SmallViewFillDisabled.Color = 16513786
          TileAppearance.SmallViewFillDisabled.ColorTo = 15132390
          TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
          TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
          TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillDisabled.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillDisabled.PictureSize = psCustom
          TileAppearance.SmallViewFillDisabled.BorderColor = 14277081
          TileAppearance.SmallViewFillDisabled.BorderOpacity = 0
          TileAppearance.SmallViewFillDisabled.BorderWidth = 2
          TileAppearance.SmallViewFillDisabled.Rounding = 0
          TileAppearance.SmallViewFillDisabled.ShadowOffset = 1
          TileAppearance.SmallViewFillDisabled.Glow = gmNone
          TileAppearance.SmallViewFillHover.Color = clNone
          TileAppearance.SmallViewFillHover.ColorTo = clNone
          TileAppearance.SmallViewFillHover.ColorMirror = clNone
          TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillHover.GradientType = gtSolid
          TileAppearance.SmallViewFillHover.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillHover.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillHover.PictureSize = psCustom
          TileAppearance.SmallViewFillHover.Opacity = 178
          TileAppearance.SmallViewFillHover.BorderColor = 8421440
          TileAppearance.SmallViewFillHover.BorderOpacity = 173
          TileAppearance.SmallViewFillHover.BorderWidth = 2
          TileAppearance.SmallViewFillHover.Rounding = 5
          TileAppearance.SmallViewFillHover.ShadowColor = clNone
          TileAppearance.SmallViewFillHover.ShadowOffset = 1
          TileAppearance.SmallViewFillHover.Glow = gmNone
          TileAppearance.SmallViewFillHover.GlowGradientColor = clNone
          TileAppearance.SmallViewFillHover.GlowRadialColor = clNone
          TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
          TileAppearance.LargeViewFont.Color = 5275647
          TileAppearance.LargeViewFont.Height = -16
          TileAppearance.LargeViewFont.Name = 'Tahoma'
          TileAppearance.LargeViewFont.Style = []
          TileAppearance.SmallViewFont.Charset = 163
          TileAppearance.SmallViewFont.Color = clWindowText
          TileAppearance.SmallViewFont.Height = -13
          TileAppearance.SmallViewFont.Name = 'Tahoma'
          TileAppearance.SmallViewFont.Style = []
          TileAppearance.SmallViewFontSelected.Charset = 163
          TileAppearance.SmallViewFontSelected.Color = clWindowText
          TileAppearance.SmallViewFontSelected.Height = -13
          TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
          TileAppearance.SmallViewFontSelected.Style = []
          TileAppearance.SmallViewFontDisabled.Charset = 163
          TileAppearance.SmallViewFontDisabled.Color = clWhite
          TileAppearance.SmallViewFontDisabled.Height = -16
          TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
          TileAppearance.SmallViewFontDisabled.Style = []
          TileAppearance.SmallViewFontHover.Charset = 163
          TileAppearance.SmallViewFontHover.Color = clWindowText
          TileAppearance.SmallViewFontHover.Height = -13
          TileAppearance.SmallViewFontHover.Name = 'Tahoma'
          TileAppearance.SmallViewFontHover.Style = []
          TileAppearance.VerticalSpacing = 7
          TileAppearance.HorizontalSpacing = 7
          TileAppearance.TargetTileColor = clPurple
          TileAppearance.MovingTileColor = 42495
          TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
          TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
          TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = 13434828
          TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 0
          TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 10
          TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
          TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
          TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
          TileAppearance.StatusIndicatorAppearance.Font.Color = clBlue
          TileAppearance.StatusIndicatorAppearance.Font.Height = -13
          TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
          TileAppearance.StatusIndicatorAppearance.Font.Style = []
          TileAppearance.StatusIndicatorAppearance.Glow = False
          TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
          TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
          TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
          TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
          TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
          TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
          TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
          TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
          TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
          TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
          TileAppearance.DeleteIndicatorAppearance.Font.Color = clBlue
          TileAppearance.DeleteIndicatorAppearance.Font.Height = -13
          TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
          TileAppearance.DeleteIndicatorAppearance.Font.Style = []
          TileMargins.Left = 0
          TileMargins.Top = 0
          TileMargins.Right = 0
          TileMargins.Bottom = 0
          Header.Visible = False
          Header.Fill.Color = 16579058
          Header.Fill.ColorTo = 16248537
          Header.Fill.ColorMirror = clNone
          Header.Fill.ColorMirrorTo = clNone
          Header.Fill.GradientType = gtVertical
          Header.Fill.GradientMirrorType = gtNone
          Header.Fill.BorderColor = 16374166
          Header.Fill.Rounding = 0
          Header.Fill.ShadowColor = 3355443
          Header.Fill.ShadowOffset = 0
          Header.Fill.Glow = gmNone
          Header.Height = 34
          Header.BulletSelectedColor = clTeal
          Header.BulletSize = 24
          Header.ArrowSize = 24
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clBlack
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Footer.Visible = False
          Footer.Fill.Color = 16579058
          Footer.Fill.ColorTo = 16248537
          Footer.Fill.ColorMirror = clNone
          Footer.Fill.ColorMirrorTo = clNone
          Footer.Fill.GradientType = gtVertical
          Footer.Fill.GradientMirrorType = gtNone
          Footer.Fill.BorderColor = 16374166
          Footer.Fill.Rounding = 0
          Footer.Fill.ShadowOffset = 0
          Footer.Fill.Glow = gmNone
          Footer.BulletSize = 24
          Footer.ArrowSize = 0
          Footer.ArrowRectangleSize = 0
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clBlack
          Footer.Font.Height = -11
          Footer.Font.Name = 'Tahoma'
          Footer.Font.Style = []
          Visualizer = AdvSmoothTileListHTMLVisualizer1
          OnTileFill = TtKhuVucBanTileFill
          OnTileFont = TtKhuVucBanTileFont
          OnTileClick = TtKhuVucBanTileClick
          OnTileDblClick = TtKhuVucBanTileDblClick
          OnTileDeleteIndicator = TtKhuVucBanTileDeleteIndicator
          Align = alClient
          Ctl3D = True
          TabOrder = 1
          ParentShowHint = False
          ShowHint = False
          BevelInner = bvNone
          BevelOuter = bvNone
          DoubleBuffered = True
          ParentCtl3D = False
          ParentFont = False
          TMSStyle = 0
        end
        object Panel19: TPanel
          Left = 1
          Top = 389
          Width = 632
          Height = 50
          ParentCustomHint = False
          Align = alBottom
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          Ctl3D = False
          DoubleBuffered = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          object BitBtn2: TBitBtn
            Left = 0
            Top = 0
            Width = 42
            Height = 50
            Cursor = 1
            ParentCustomHint = False
            Action = CmdAreaTablePrev
            Align = alLeft
            BiDiMode = bdLeftToRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF9159149801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF915914980101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF9159
              1498010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF915914980101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF91591498010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF9159149801010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF8854138F01010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF8854138F010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF8854138F0101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF8854
              138F010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF8854138F0101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF8854138F01010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentBiDiMode = False
            ParentFont = False
            Spacing = -4
            TabOrder = 0
          end
          object BitBtn3: TBitBtn
            Left = 590
            Top = 0
            Width = 42
            Height = 50
            Cursor = 1
            Action = CmdAreaTableNext
            Align = alRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000101000189551390F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000101000189551390F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000101000189551390F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000101000189551390F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000101000189551390F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000010100018955
              1390F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000008250
              1288F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000082501288F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000082501288F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000082501288F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000082501288F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000082501288F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentFont = False
            Spacing = -4
            TabOrder = 1
          end
        end
      end
      object Panel12: TPanel
        Left = 1
        Top = 676
        Width = 634
        Height = 65
        Align = alBottom
        BiDiMode = bdLeftToRight
        Ctl3D = False
        FullRepaint = False
        ParentBiDiMode = False
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 1
        object Panel6: TPanel
          Left = 183
          Top = 1
          Width = 91
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 0
          TabStop = True
          object AdvGlowButton3: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 87
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdTableOrders
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F8000000097048597300000B1300000B1301009A9C18000002E149444154789C
              DD96DF6F0C5114C74711123FE247C4831F0DDDB967BA4A50F5847A20F1E4479B
              4652DD3967B7D5D204A541523F3612152F6848849412A5A9ED9C336D68132144
              E2C19B277F0FCEECCEDABD8317491F3AC97999CFB9E7DCB9DFEFBD771CC7711C
              C378C80BB1BB3220A0F6DA67B8D829B4CDF7845A6CAEEF9C7CBE467334D7E65A
              536B3B46E808307E374CBDD5091400E31D103A07425F40F054B939538F11FA08
              8C6741E89E617C638DED05A66FC0FE619D7D1F84488EF5A40AD935C0146A0108
              71BFCD0D5353C4984233D5B1C9E620D4A6B5A30646B21D76426D882BE20646B2
              7B6CEE86B83D6E90E2AEF589098478748E3400A61E101C6E7CDCBDF00F6B3866
              186F19C66BEAA632CCE76B20C0FE121BF384D0F9E1CC8B71F3A7FC02C338A482
              3BA99913CB8DE00808BEAF0AC6C9FA097F8BCE0E04C7135C70DC2DF8EB3CCE35
              68AECDB5A6D6765233671699102F81E0EDCA3042794F32AB6132B70C04AFD85C
              DFA50BBD4B1BA63AD702D38DC478CE9E8F56C548F634307DF0980EBA41F6401C
              A5CF1FD2E53182AF2A59C4A3AFA6AB86F12908DEB539084E47DA464901666C91
              D285CE55C038AA42BA81BFCFE61ED3CE58E47421B331C1855A8A226B83BFBA08
              47FFDF45B3D220482ED1D6B7ED2BCB4B14D25E9BA726323BE20675216D484C80
              FD63C58DA6220B4EBBE2EFF6426C8C231297CB228FC0446E572507C687B1C846
              70B07A2C3519468E56466D0A82179336C5EB2AB45A1198061236651AF8B74DB1
              2FB2A92619A1FB2054B0E2457D406EA9C09324C761659E648CE6DA5C6B6E7BD7
              B1C471054F02D3EBF45467CA0DB29BE3D033DD08BD344C378B56FDCD222E38A8
              4C73F4D8A86429F1EB0CE373C3D435074E53331B0D3C3D6EAD273AE8B8D8C093
              6C7382AB554B0DEA835CADCD21C4D6E2460BB11518BF9A808EEB1D1087117C14
              B92BC07EDD2795AC1462842E18C6072A68D5D862ADCFD19F875E14C56E74B932
              F4B2509BA9975DF67D9BEB3BBD5834A7F427513D5E8BFFBA847E02618AA6BE88
              4BC1F80000000049454E44AE426082}
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 91
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 1
          object AdvGlowButton2: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 87
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdSelect
            Caption = 'Ch'#7885'n m'#243'n'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F8000000097048597300000B1300000B1301009A9C180000010149444154789C
              63601805A400AFB581DE3EEB029EF8AC0FF84F1EF67FECB32EC013A705600564
              1B1E00C6DEEB021EE1B1807403A75D98F9DF7F63108A18C516B49DE884E313CF
              4EFEDF7867E3FF904D11A45B10BF3D09AB052043D1F1E2AB4B49B320776FE1FF
              038F0F62B5A0EFEC04380619BEE1CE26D27C5077B4E1FF896727305CE983C5B2
              A9E7A7FDF7DB40621C802C388E25187C888C238A83C887403C511CC93E4818E4
              089063A89A4C4F6009BEA6E32DA459008A385004D2C4025092DB78771358631B
              52A6A25A1081320D3657FA502B92C13EB8B3116C681F52A6A26A32C517073EE4
              5B40EBE27A5D8027259678AF0B78E4B536D003A705A380010B00009C3E21C9A5
              5D5AB40000000049454E44AE426082}
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel1: TPanel
          Left = 274
          Top = 1
          Width = 95
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 2
          object AdvGlowButton11: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 91
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdSendOrder
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F8000000097048597300000B1300000B1301009A9C18000002A749444154789C
              AD945B48146114C7A71B45560F3BF39D6F152922E966053DE6430F81F5582FE5
              2E99EEB6E6CACE7756CA4428C35E8A5E2A227A29EAA507295C304B56F3B26A1A
              B9DF59DBCA82EE0442451788C0A27427666B6B941DDA65F70FC3C070F8FDCF9C
              9BA2E4A09E1A6DF56DAFE636DF4A3ED5EB83F28E4A6DA25B2F4D0CF89DC6909F
              4D920E1BF3020F7B1C473A7C2BA79FDF386DBC686D3228E83408B941C80EE70C
              EFF3B0B2F6FDAB12AFC3178CB1C6923FE0DF8F14AC2A6783CE7D5AFFC3AB478D
              FB4D6B67C049C0E4A85EA4E66C70B34A7BFCB4EDC44C78327B38698D8B059C5B
              4703BC346B83F6BDDA87E140F12C38EF8DB428F3FFC205AF20E45312F9C5ACE0
              DD5EA86C73A94634003F49F09724F89814D0FC0C4B16A66248C02E42FE831062
              74A850CB181EF6A83B436E75AAD7C706ED6A4DC8B64B01DF2582BC7BB0D89131
              BCCBA355845CEA748F970D1A8A32272D5CF06D66A325C2D070A3BA34ABB2845C
              EA749F8F0D182DCADC7431311DB648E45F0921321E604B328687ABD5BA36979A
              E8AF81116B13AD8AA2B659227C26019D91EA158B328677557334E1911A18A15A
              6541DACC83B089047C92023AAC8DFEAFBA3D8EE6905B4D0CF959D42EAB687DD1
              1A12FC1D0968B5FBBBB40A7BD9F124BC16E2F1065E9036F37AE73A42FE96045C
              BEBE5B999739DCA39E35E177FCEC81B559F1065E403ADB41C8F790805324F817
              12FC92DD44A55597473D13726BC6B09F3DBA878E65A9EF52877289F07ED6DD79
              63D7175BDDAAD2A28307E0897541CCF123846F84F071C66940B8A2E4AA645910
              5E11C2797373A560E3FF4CE05CCE06127993093714652E093836864565C9FB82
              DC308F59CE06845C4FC1A341BE2116602552F00942DE63B7D1D99B243377AE4F
              C1A5806B545BB8382FF0A441B07079128ED0678E68DEC056C5EB386435EF69F4
              0B6A085CA685B1D0CF0000000049454E44AE426082}
            Position = bpLeft
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel8: TPanel
          Left = 369
          Top = 1
          Width = 95
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 3
          object AdvGlowButton13: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 91
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdChangeTable
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C0864880000000970485973000000B1000000
              B101C62D498D0000001974455874536F667477617265007777772E696E6B7363
              6170652E6F72679BEE3C1A000001FC494441544889DD944D6B135114869F7367
              6A14A185D62A444114BBE8A2E0CAAE846815846C337E442CB8D54575E74F0822
              08160BBA347416B5AB82BB62052B6EC52A28E946B14252A194529A4832D7C57C
              F46698CC4871530F0C9C39F79DE73DF7CE9C81FD1E12654B059BF513F7415F04
              EC98AE0154B83ABB02C0DCCD31E0017034A66B83BC66F8C7632EBC69D305F2E1
              0F539A9960CE3913E48B09F020F4151A790D3C0250C6C2A51438C031B0C7FCAB
              173C0891CB616A1EC5810C03102BE7F7A2B394114BA5A9FE45EC7F03F31DFCCC
              548BB7068096749D96B53035775001EABDE13CA5E4D628B935343329F83A5A2A
              BB8F99F1E2D66172DE59D087BAEA16DF28B9B5AEDA7C79840E27635DECD0521F
              98AC6E8795EE893DD814B025F30880C07075D0191F056BA0D3A7BE6CBACB1B71
              D92EC91FFFDE13AA99E1DAECDDF0F68873AE88A869E054506A6BA84AD39AFAB5
              F06E2BD499EF20E9DF62B67287F9F288015F30E000B6C06D72DE2B1CC74A32C8
              F78447BB50C70141594FE8F5898B3E3FC4F77292C15FC5A0333E8AD6A7D334A2
              A4B8670314FD991ACDC09E0D3A56DF57A09D2A12BD9264F03BBBB34E6BD35DDE
              D0504D51B5C453CF130C6431035FA7697F0490A6358596B78970985C7FF97E35
              A2464B4B059B46FE1E2213C407508B3FFED7AB9FA25AA1600F0DEFDC1025453C
              FA11F92C9A6726FCFF883FA37C8C36CDAA33B00000000049454E44AE426082}
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel20: TPanel
          Left = 542
          Top = 1
          Width = 91
          Height = 63
          Align = alRight
          BevelWidth = 2
          TabOrder = 4
          object btnMore: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 87
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdMore
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel23: TPanel
          Left = 92
          Top = 1
          Width = 91
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 5
          object AdvGlowButton10: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 87
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdCustomer
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F8000000097048597300000B1300000B1301009A9C180000041349444154789C
              DD92E9531A671CC7695FF77D673AD3E91FD1E34D3BED3FA08EA9513C32B6B5D5
              34623C6247242A0844A8E249249E1C8B96F5400E03821767159B484463827124
              E39862BC629CD8446B846F67B75551709ABC6C7F339FD9679FFD3CDFDFF3EC2E
              83F1BF2B4D61D227064E6A9B819D32305072E1E2791E188C77C8FCC4382387D9
              6D6033357D45899FFD6BB88ECD6C986AC85BDB196AC46B672B665A8AB7FB8B2F
              5477B2123E88F4783CDEBB7A76F2F0830EF6CEAEB5097FDA5AE010FF10240B13
              73FA72BF7A2F66784F61FCE75E59C133FCDA8923C2EE0EAC1BAA611365ADF75D
              4BFCE2782325299C951EC17EA47B606F45B0BF2A6CAEC80868F2E3DF8F6A602C
              65B6BF1A6D3E5E40F16AAC190BAAEB0839DB61AA48771FB9666EFA64A447B16D
              AEC332C9C70B6B13F41C6653540303274DB33F7E0B8B5DE5F84D968FC96616EE
              280A10B009D15DCB0D734537F73875C432BB5A795FC12F784CB90F946C4CC9AE
              62E2260BBE9E12CC69D9F02B396163692A197D020EB372889F115ABD23C1E106
              016C77D1588715685219A077CCC3E09CA7AFC206E5FECF22090277A5086F9DB8
              148315CC430327991BD5C05C1F6719AA4A0D47CAA12D35246D041D7C96FE311F
              24AD9D083F3BF1292CE2F4B0A93E6128AAC168475CBABD2D231429AF05549012
              03C7A103B639C8750E886572B468CC90125AAC0754A71A501996F6F8B4987F92
              479B36138A38F28B2704EA3A346890F7A045A5808A9463D22DC7C1A61ABD3A39
              2A1B15B4737262021E6DFA3DC679E5547DFDE5ECD0E53F429B7FEFEAF5860A57
              794DF0FB94A77649B1F384406E79030EFF71A935B3969C5D3B71F23BC72C3799
              E4BF67FE0E0B8E02DCD55F42599518A1ADD3E147948B44B443B9D41A3779D17F
              6E70B6A2E2D33C926FB668D25E06674BB132FD1356E7CB60252E85F757DBA2C2
              F783EDB0109961CAA15C6ACD6D22EB2553683327570C7F7C2A3C8FE48B8416E9
              9669C98A9187FD701AB2B1E429C084F17B8C4FD6E3A18B1DD560CE5E06954E07
              9B3617014F21467B59909AFC10DFDE428ED4BB9952357E830EFFB1AB3CA17AAC
              657B64791CC72C59601BE160E49181BE77988B707F8485BD601BF682ADF478B0
              8F0F8965078DA600B47DB5A833AFD1F747E4DEF26D2709AC718C7C52E0B53E1E
              3B098F81D526C1A29B059F258B861AABB55DA702CF52637E8E54A1CDCBC826CA
              9E5E374A02B128EB2537053D9E0385D11872E9AFC03721A471E9AEA0536F0AF1
              BA670EF264531BDFD67A02B148E20EAF9EFBD179EA6941A76365C7F91470AD01
              AE478B28AA51A148A2A4C7D41CF5ACC3B1F29C47782B196F53C5CD131FD51917
              56E9E0086EF4CFD29C9DAF372E04AFC99C1FBE71036EF7B478D0BF1B15945963
              4766B53D6A9E722BD5DEAA376FD0E5BD2CF865663412AE6ADAF18DD8F63B0535
              3EFB9CA7F6E6BCD56BFACFD45FB7C52949085203A10000000049454E44AE4260
              82}
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
        object Panel21: TPanel
          Left = 464
          Top = 1
          Width = 91
          Height = 63
          Align = alLeft
          BevelWidth = 2
          TabOrder = 6
          object AdvGlowButton8: TAdvGlowButton
            Left = 2
            Top = 2
            Width = 87
            Height = 59
            Cursor = 1
            Align = alClient
            Action = CmdShowImage
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
            NotesFont.Charset = DEFAULT_CHARSET
            NotesFont.Color = clWindowText
            NotesFont.Height = -11
            NotesFont.Name = 'Tahoma'
            NotesFont.Style = []
            ParentFont = False
            Picture.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F8000000097048597300000B1300000B1301009A9C180000006149444154789C
              636018058306F0FBF724F0077637E0C5FEFD0AEF19DC15DE327836E0C36F18DC
              13302CE00BEC3EC01FD8F31F2FF6EF7678CDE0EEF096C1F33F3EFC86C1F300FD
              2DE0A775100D7DC03F9A8A18465311A5603415BD1D4D4523170000A0F5A11CE6
              1291630000000049454E44AE426082}
            TabOrder = 0
            Appearance.Color = cl3DLight
            Appearance.ColorTo = clScrollBar
            Appearance.ColorChecked = cl3DLight
            Appearance.ColorCheckedTo = cl3DLight
            Appearance.ColorDisabled = clMenuBar
            Appearance.ColorDisabledTo = clMenuBar
            Appearance.ColorDown = cl3DLight
            Appearance.ColorDownTo = cl3DLight
            Appearance.ColorHot = cl3DLight
            Appearance.ColorHotTo = cl3DLight
            Appearance.ColorMirror = cl3DLight
            Appearance.ColorMirrorTo = clScrollBar
            Appearance.ColorMirrorHot = cl3DLight
            Appearance.ColorMirrorHotTo = cl3DLight
            Appearance.ColorMirrorDown = cl3DLight
            Appearance.ColorMirrorDownTo = cl3DLight
            Appearance.ColorMirrorChecked = cl3DLight
            Appearance.ColorMirrorCheckedTo = cl3DLight
            Appearance.ColorMirrorDisabled = clMenuBar
            Appearance.ColorMirrorDisabledTo = clMenuBar
          end
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 674
        Width = 634
        Height = 2
        Align = alBottom
        TabOrder = 2
      end
      object Panel10: TPanel
        Left = 1
        Top = 441
        Width = 634
        Height = 233
        Align = alClient
        BevelEdges = []
        BiDiMode = bdLeftToRight
        BorderWidth = 1
        Ctl3D = True
        ParentBiDiMode = False
        ParentCtl3D = False
        ShowCaption = False
        TabOrder = 3
        object Panel11: TPanel
          Left = 2
          Top = 2
          Width = 630
          Height = 60
          Align = alTop
          BevelInner = bvRaised
          Color = clActiveCaption
          ParentBackground = False
          TabOrder = 0
          object TtNganhhang: TAdvSmoothTileListEx
            Left = 44
            Top = 2
            Width = 542
            Height = 56
            Cursor = 1
            Transparent = True
            AnimationFactor = 2.000000000000000000
            Fill.Color = clNone
            Fill.ColorTo = clNone
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtVertical
            Fill.Opacity = 0
            Fill.OpacityTo = 0
            Fill.OpacityMirror = 0
            Fill.OpacityMirrorTo = 0
            Fill.BorderColor = clSilver
            Fill.BorderWidth = 0
            Fill.Rounding = 0
            Fill.ShadowColor = 13408767
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Tiles = <
              item
                Content.Text = '$$$'
                Content.TextPosition = tpBottomCenter
                ContentMaximized.Text = 'Description for Tile 1'
                ContentMaximized.Extra = '$$$'
                StatusIndicator = '$$$$'
                DeleteIndicator = 'X'
                StatusIndicatorLeft = -20
                StatusIndicatorTop = 1
                SubTiles = <>
                Tag = 0
                Extra = '$$$'
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end
              item
                DeleteIndicator = 'X'
                SubTiles = <>
                Tag = 0
              end>
            Columns = 10
            Rows = 1
            TileAppearance.LargeViewFill.Color = 15581579
            TileAppearance.LargeViewFill.ColorTo = 12889148
            TileAppearance.LargeViewFill.ColorMirror = clNone
            TileAppearance.LargeViewFill.ColorMirrorTo = clNone
            TileAppearance.LargeViewFill.GradientType = gtSolid
            TileAppearance.LargeViewFill.GradientMirrorType = gtNone
            TileAppearance.LargeViewFill.Opacity = 180
            TileAppearance.LargeViewFill.BorderColor = 10987431
            TileAppearance.LargeViewFill.Rounding = 0
            TileAppearance.LargeViewFill.ShadowOffset = 0
            TileAppearance.LargeViewFill.Glow = gmNone
            TileAppearance.SmallViewFill.Color = clWhite
            TileAppearance.SmallViewFill.ColorTo = clWhite
            TileAppearance.SmallViewFill.ColorMirror = clNone
            TileAppearance.SmallViewFill.ColorMirrorTo = clNone
            TileAppearance.SmallViewFill.GradientType = gtSolid
            TileAppearance.SmallViewFill.GradientMirrorType = gtNone
            TileAppearance.SmallViewFill.Opacity = 180
            TileAppearance.SmallViewFill.BorderColor = clBlue
            TileAppearance.SmallViewFill.BorderOpacity = 128
            TileAppearance.SmallViewFill.Rounding = 5
            TileAppearance.SmallViewFill.ShadowOffset = 1
            TileAppearance.SmallViewFill.Glow = gmNone
            TileAppearance.SmallViewFillSelected.Color = clGray
            TileAppearance.SmallViewFillSelected.ColorTo = 12889148
            TileAppearance.SmallViewFillSelected.ColorMirror = clNone
            TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillSelected.GradientType = gtSolid
            TileAppearance.SmallViewFillSelected.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillSelected.BorderColor = clGray
            TileAppearance.SmallViewFillSelected.BorderWidth = 3
            TileAppearance.SmallViewFillSelected.Rounding = 5
            TileAppearance.SmallViewFillSelected.ShadowOffset = 0
            TileAppearance.SmallViewFillSelected.Glow = gmNone
            TileAppearance.SmallViewFillDisabled.Color = clWhite
            TileAppearance.SmallViewFillDisabled.ColorTo = clWhite
            TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
            TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
            TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillDisabled.BorderColor = clBlue
            TileAppearance.SmallViewFillDisabled.BorderOpacity = 128
            TileAppearance.SmallViewFillDisabled.BorderWidth = 3
            TileAppearance.SmallViewFillDisabled.Rounding = 5
            TileAppearance.SmallViewFillDisabled.ShadowOffset = 0
            TileAppearance.SmallViewFillDisabled.Glow = gmNone
            TileAppearance.SmallViewFillHover.Color = clWhite
            TileAppearance.SmallViewFillHover.ColorTo = clWhite
            TileAppearance.SmallViewFillHover.ColorMirror = clNone
            TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
            TileAppearance.SmallViewFillHover.GradientType = gtSolid
            TileAppearance.SmallViewFillHover.GradientMirrorType = gtNone
            TileAppearance.SmallViewFillHover.Opacity = 180
            TileAppearance.SmallViewFillHover.BorderColor = clBlue
            TileAppearance.SmallViewFillHover.BorderOpacity = 128
            TileAppearance.SmallViewFillHover.BorderWidth = 3
            TileAppearance.SmallViewFillHover.Rounding = 5
            TileAppearance.SmallViewFillHover.ShadowOffset = 0
            TileAppearance.SmallViewFillHover.Glow = gmNone
            TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
            TileAppearance.LargeViewFont.Color = clMaroon
            TileAppearance.LargeViewFont.Height = -16
            TileAppearance.LargeViewFont.Name = 'Tahoma'
            TileAppearance.LargeViewFont.Style = []
            TileAppearance.SmallViewFont.Charset = 163
            TileAppearance.SmallViewFont.Color = clWindowText
            TileAppearance.SmallViewFont.Height = -15
            TileAppearance.SmallViewFont.Name = 'Tahoma'
            TileAppearance.SmallViewFont.Style = [fsBold]
            TileAppearance.SmallViewFontSelected.Charset = 163
            TileAppearance.SmallViewFontSelected.Color = clWindowText
            TileAppearance.SmallViewFontSelected.Height = -15
            TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
            TileAppearance.SmallViewFontSelected.Style = [fsBold]
            TileAppearance.SmallViewFontDisabled.Charset = 163
            TileAppearance.SmallViewFontDisabled.Color = clWindowText
            TileAppearance.SmallViewFontDisabled.Height = -15
            TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
            TileAppearance.SmallViewFontDisabled.Style = [fsBold]
            TileAppearance.SmallViewFontHover.Charset = 163
            TileAppearance.SmallViewFontHover.Color = clWindowText
            TileAppearance.SmallViewFontHover.Height = -15
            TileAppearance.SmallViewFontHover.Name = 'Tahoma'
            TileAppearance.SmallViewFontHover.Style = [fsBold]
            TileAppearance.VerticalSpacing = 0
            TileAppearance.HorizontalSpacing = 0
            TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
            TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
            TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
            TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = clRed
            TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 128
            TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 8
            TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
            TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
            TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
            TileAppearance.StatusIndicatorAppearance.Font.Color = clMaroon
            TileAppearance.StatusIndicatorAppearance.Font.Height = -11
            TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
            TileAppearance.StatusIndicatorAppearance.Font.Style = []
            TileAppearance.StatusIndicatorAppearance.Glow = False
            TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
            TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
            TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
            TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
            TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
            TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
            TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
            TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
            TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
            TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
            TileAppearance.DeleteIndicatorAppearance.Font.Color = clWhite
            TileAppearance.DeleteIndicatorAppearance.Font.Height = -11
            TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
            TileAppearance.DeleteIndicatorAppearance.Font.Style = []
            TileMargins.Left = 0
            TileMargins.Top = 0
            TileMargins.Right = 0
            TileMargins.Bottom = 0
            Header.Visible = False
            Header.Fill.Color = clNone
            Header.Fill.ColorTo = clNone
            Header.Fill.ColorMirror = clNone
            Header.Fill.ColorMirrorTo = clNone
            Header.Fill.GradientType = gtVertical
            Header.Fill.GradientMirrorType = gtVertical
            Header.Fill.BorderColor = clNone
            Header.Fill.Rounding = 0
            Header.Fill.ShadowColor = clNone
            Header.Fill.ShadowOffset = 0
            Header.Fill.Glow = gmNone
            Header.Height = 10
            Header.BulletSize = 6
            Header.ArrowNavigation = False
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Footer.Visible = False
            Footer.Fill.Color = 10655892
            Footer.Fill.ColorTo = 8749179
            Footer.Fill.ColorMirror = 8156785
            Footer.Fill.ColorMirrorTo = 6775131
            Footer.Fill.GradientType = gtVertical
            Footer.Fill.GradientMirrorType = gtVertical
            Footer.Fill.BorderColor = 6973542
            Footer.Fill.Rounding = 0
            Footer.Fill.ShadowOffset = 0
            Footer.Fill.Glow = gmNone
            Footer.BulletSize = 15
            Footer.ArrowSize = 0
            Footer.ArrowRectangleSize = 0
            Footer.ArrowNavigation = False
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Options = []
            OnTileClick = TtNganhhangTileClick
            Align = alClient
            Ctl3D = False
            TabOrder = 0
            OnDblClick = TtNganhhangDblClick
            DoubleBuffered = True
            Color = clWhite
            ParentCtl3D = False
            ParentFont = False
            TMSStyle = 0
          end
          object BitBtn4: TBitBtn
            Left = 586
            Top = 2
            Width = 42
            Height = 56
            Cursor = 1
            Action = CmdGroupMenuNext
            Align = alRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000101000189551390F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000101000189551390F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000101000189551390F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000101000189551390F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000101000189551390F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000010100018955
              1390F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000008250
              1288F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000082501288F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000082501288F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000082501288F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000082501288F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000082501288F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentFont = False
            Spacing = -4
            TabOrder = 1
          end
          object BitBtn5: TBitBtn
            Left = 2
            Top = 2
            Width = 42
            Height = 56
            Cursor = 1
            ParentCustomHint = False
            Action = CmdGroupMenuPrev
            Align = alLeft
            BiDiMode = bdLeftToRight
            Caption = '  '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF9159149801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF915914980101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF9159
              1498010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF915914980101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF91591498010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF9159149801010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF8854138F01010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF8854138F010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF8854138F0101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF8854
              138F010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF8854138F0101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF8854138F01010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentBiDiMode = False
            ParentFont = False
            Spacing = -4
            TabOrder = 2
          end
        end
        object TtVattu: TAdvSmoothTileListEx
          Left = 2
          Top = 62
          Width = 630
          Height = 119
          Cursor = 1
          Transparent = True
          AnimationFactor = 1.000000000000000000
          Fill.Color = 14145495
          Fill.ColorTo = clNone
          Fill.ColorMirror = clNone
          Fill.ColorMirrorTo = clNone
          Fill.GradientType = gtVertical
          Fill.GradientMirrorType = gtVertical
          Fill.BackGroundPictureMode = pmInsideFill
          Fill.PictureSize = psCustom
          Fill.PictureWidth = 1008
          Fill.PictureHeight = 428
          Fill.PictureAspectRatio = True
          Fill.PictureAspectMode = pmNormal
          Fill.Opacity = 98
          Fill.OpacityTo = 98
          Fill.BorderColor = clWhite
          Fill.BorderOpacity = 0
          Fill.BorderWidth = 0
          Fill.Rounding = 0
          Fill.ShadowOffset = 0
          Fill.Glow = gmNone
          Tiles = <
            item
              Content.Text = '$$$ 4444 555 666 777 888 999 1000 222'
              Content.TextPosition = tpBottomCenter
              Content.TextTop = 1
              Content.ImageStretch = True
              ContentMaximized.ImageIndex = 74
              ContentMaximized.Text = 'Description for Tile 1'
              ContentMaximized.Extra = '$$$'
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              StatusIndicator = '$$$$'
              DeleteIndicator = 'X'
              StatusIndicatorLeft = -20
              StatusIndicatorTop = 1
              SubTiles = <>
              Tag = 0
              Extra = '$$$'
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end
            item
              Visualizer = AdvSmoothTileListHTMLVisualizer2
              DeleteIndicator = 'X'
              SubTiles = <>
              Tag = 0
            end>
          Columns = 6
          Rows = 1
          TileAppearance.LargeViewFill.Color = 16776697
          TileAppearance.LargeViewFill.ColorTo = 16775920
          TileAppearance.LargeViewFill.ColorMirror = clNone
          TileAppearance.LargeViewFill.ColorMirrorTo = clNone
          TileAppearance.LargeViewFill.GradientType = gtSolid
          TileAppearance.LargeViewFill.GradientMirrorType = gtVertical
          TileAppearance.LargeViewFill.PicturePosition = ppTopCenter
          TileAppearance.LargeViewFill.PictureSize = psCustom
          TileAppearance.LargeViewFill.Opacity = 180
          TileAppearance.LargeViewFill.BorderColor = 16577242
          TileAppearance.LargeViewFill.Rounding = 0
          TileAppearance.LargeViewFill.ShadowOffset = 0
          TileAppearance.LargeViewFill.Glow = gmNone
          TileAppearance.SmallViewFill.Color = clWhite
          TileAppearance.SmallViewFill.ColorTo = clWhite
          TileAppearance.SmallViewFill.ColorMirror = clNone
          TileAppearance.SmallViewFill.ColorMirrorTo = clNone
          TileAppearance.SmallViewFill.GradientType = gtSolid
          TileAppearance.SmallViewFill.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFill.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFill.PictureSize = psCustom
          TileAppearance.SmallViewFill.Opacity = 180
          TileAppearance.SmallViewFill.BorderColor = clWhite
          TileAppearance.SmallViewFill.Rounding = 5
          TileAppearance.SmallViewFill.ShadowColor = clWhite
          TileAppearance.SmallViewFill.ShadowOffset = 0
          TileAppearance.SmallViewFill.Glow = gmNone
          TileAppearance.SmallViewFillSelected.Color = clWhite
          TileAppearance.SmallViewFillSelected.ColorTo = clWhite
          TileAppearance.SmallViewFillSelected.ColorMirror = clNone
          TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillSelected.GradientType = gtSolid
          TileAppearance.SmallViewFillSelected.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillSelected.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillSelected.PictureSize = psCustom
          TileAppearance.SmallViewFillSelected.Opacity = 180
          TileAppearance.SmallViewFillSelected.BorderColor = clGray
          TileAppearance.SmallViewFillSelected.Rounding = 5
          TileAppearance.SmallViewFillSelected.ShadowColor = clNone
          TileAppearance.SmallViewFillSelected.ShadowOffset = 1
          TileAppearance.SmallViewFillSelected.Glow = gmNone
          TileAppearance.SmallViewFillSelected.GlowGradientColor = clNone
          TileAppearance.SmallViewFillSelected.GlowRadialColor = clNone
          TileAppearance.SmallViewFillDisabled.Color = 16513786
          TileAppearance.SmallViewFillDisabled.ColorTo = 15132390
          TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
          TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
          TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillDisabled.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillDisabled.PictureSize = psCustom
          TileAppearance.SmallViewFillDisabled.BorderColor = 14277081
          TileAppearance.SmallViewFillDisabled.BorderOpacity = 0
          TileAppearance.SmallViewFillDisabled.BorderWidth = 2
          TileAppearance.SmallViewFillDisabled.Rounding = 0
          TileAppearance.SmallViewFillDisabled.ShadowOffset = 1
          TileAppearance.SmallViewFillDisabled.Glow = gmNone
          TileAppearance.SmallViewFillHover.Color = clWhite
          TileAppearance.SmallViewFillHover.ColorTo = clWhite
          TileAppearance.SmallViewFillHover.ColorMirror = clNone
          TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
          TileAppearance.SmallViewFillHover.GradientType = gtSolid
          TileAppearance.SmallViewFillHover.GradientMirrorType = gtVertical
          TileAppearance.SmallViewFillHover.PicturePosition = ppTopCenter
          TileAppearance.SmallViewFillHover.PictureSize = psCustom
          TileAppearance.SmallViewFillHover.Opacity = 178
          TileAppearance.SmallViewFillHover.BorderColor = clTeal
          TileAppearance.SmallViewFillHover.Rounding = 5
          TileAppearance.SmallViewFillHover.ShadowColor = clNone
          TileAppearance.SmallViewFillHover.ShadowOffset = 1
          TileAppearance.SmallViewFillHover.Glow = gmNone
          TileAppearance.SmallViewFillHover.GlowGradientColor = clNone
          TileAppearance.SmallViewFillHover.GlowRadialColor = clNone
          TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
          TileAppearance.LargeViewFont.Color = 5275647
          TileAppearance.LargeViewFont.Height = -16
          TileAppearance.LargeViewFont.Name = 'Tahoma'
          TileAppearance.LargeViewFont.Style = []
          TileAppearance.SmallViewFont.Charset = 163
          TileAppearance.SmallViewFont.Color = clWindowText
          TileAppearance.SmallViewFont.Height = -11
          TileAppearance.SmallViewFont.Name = 'Tahoma'
          TileAppearance.SmallViewFont.Style = []
          TileAppearance.SmallViewFontSelected.Charset = 163
          TileAppearance.SmallViewFontSelected.Color = clWindowText
          TileAppearance.SmallViewFontSelected.Height = -11
          TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
          TileAppearance.SmallViewFontSelected.Style = []
          TileAppearance.SmallViewFontDisabled.Charset = 163
          TileAppearance.SmallViewFontDisabled.Color = clWhite
          TileAppearance.SmallViewFontDisabled.Height = -11
          TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
          TileAppearance.SmallViewFontDisabled.Style = []
          TileAppearance.SmallViewFontHover.Charset = 163
          TileAppearance.SmallViewFontHover.Color = clWindowText
          TileAppearance.SmallViewFontHover.Height = -11
          TileAppearance.SmallViewFontHover.Name = 'Tahoma'
          TileAppearance.SmallViewFontHover.Style = []
          TileAppearance.VerticalSpacing = 7
          TileAppearance.HorizontalSpacing = 7
          TileAppearance.TargetTileColor = clPurple
          TileAppearance.MovingTileColor = 42495
          TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
          TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
          TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
          TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = 13434828
          TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 0
          TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 10
          TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
          TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
          TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
          TileAppearance.StatusIndicatorAppearance.Font.Color = 8404992
          TileAppearance.StatusIndicatorAppearance.Font.Height = -13
          TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
          TileAppearance.StatusIndicatorAppearance.Font.Style = [fsBold]
          TileAppearance.StatusIndicatorAppearance.Glow = False
          TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
          TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
          TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
          TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
          TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
          TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
          TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
          TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
          TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
          TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
          TileAppearance.DeleteIndicatorAppearance.Font.Color = clWhite
          TileAppearance.DeleteIndicatorAppearance.Font.Height = -11
          TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
          TileAppearance.DeleteIndicatorAppearance.Font.Style = []
          TileMargins.Left = 0
          TileMargins.Top = 0
          TileMargins.Right = 0
          TileMargins.Bottom = 0
          Header.Visible = False
          Header.Fill.Color = 16579058
          Header.Fill.ColorTo = 16248537
          Header.Fill.ColorMirror = clNone
          Header.Fill.ColorMirrorTo = clNone
          Header.Fill.GradientType = gtVertical
          Header.Fill.GradientMirrorType = gtNone
          Header.Fill.BorderColor = 16374166
          Header.Fill.Rounding = 0
          Header.Fill.ShadowColor = 3355443
          Header.Fill.ShadowOffset = 0
          Header.Fill.Glow = gmNone
          Header.Height = 34
          Header.BulletSelectedColor = clTeal
          Header.BulletSize = 24
          Header.ArrowSize = 24
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clBlack
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Footer.Visible = False
          Footer.Fill.Color = 16579058
          Footer.Fill.ColorTo = 16248537
          Footer.Fill.ColorMirror = clNone
          Footer.Fill.ColorMirrorTo = clNone
          Footer.Fill.GradientType = gtVertical
          Footer.Fill.GradientMirrorType = gtNone
          Footer.Fill.BorderColor = 16374166
          Footer.Fill.Rounding = 0
          Footer.Fill.ShadowOffset = 0
          Footer.Fill.Glow = gmNone
          Footer.BulletSize = 24
          Footer.ArrowSize = 0
          Footer.ArrowRectangleSize = 0
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clBlack
          Footer.Font.Height = -11
          Footer.Font.Name = 'Tahoma'
          Footer.Font.Style = []
          Visualizer = AdvSmoothTileListHTMLVisualizer2
          Options = []
          OnTileFill = TtVattuTileFill
          OnTileDblClick = TtVattuTileDblClick
          Align = alClient
          Ctl3D = True
          TabOrder = 1
          BevelInner = bvNone
          BevelOuter = bvNone
          DoubleBuffered = True
          ParentCtl3D = False
          ParentFont = False
          TMSStyle = 0
        end
        object Panel15: TPanel
          Left = 2
          Top = 181
          Width = 630
          Height = 50
          ParentCustomHint = False
          Align = alBottom
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          Ctl3D = False
          DoubleBuffered = False
          FullRepaint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          object BitBtn6: TBitBtn
            Left = 0
            Top = 0
            Width = 42
            Height = 50
            Cursor = 1
            ParentCustomHint = False
            Action = CmdMenuPrev
            Align = alLeft
            BiDiMode = bdLeftToRight
            Caption = ' '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF9159149801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF915914980101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF9159
              1498010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF915914980101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF91591498010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF9159149801010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF915914980101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF91591498010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000000000000000000007A4B1180F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000007B4C
              1181F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00007C4C1182F39621FFF39621FFF39621FF8854138F01010001000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000007C4C1182F39621FFF39621FFF39621FF8854138F010100010000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000007D4D1183F39621FFF39621FFF39621FF8854138F0101
              0001000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000007E4E1184F39621FFF39621FFF39621FF8854
              138F010100010000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000007E4E1184F39621FFF39621FFF396
              21FF8854138F0101000100000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000007F4E1185F39621FFF396
              21FFF39621FF8854138F01010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000804F1186F396
              21FFF39621FFF39621FF8854138F010100010000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000804F
              1186F39621FFF39621FFF39621FF8854138F0101000100000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00008653128DF39621FFF39621FFF39621FF633D0D6800000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFB87219C10A06010B00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000001010001633D0D68090501090000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentBiDiMode = False
            ParentFont = False
            Spacing = -4
            TabOrder = 0
          end
          object BitBtn7: TBitBtn
            Left = 588
            Top = 0
            Width = 42
            Height = 50
            Cursor = 1
            Action = CmdMenuNext
            Align = alRight
            Caption = ' '
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Glyph.Data = {
              36100000424D3610000000000000360000002800000020000000200000000100
              2000000000000010000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000101000189551390F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000101000189551390F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000101000189551390F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000101000189551390F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000101000189551390F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000010100018955
              1390F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000101
              000189551390F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000101000189551390F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF7A4B1180000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8351128900000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000008250
              1288F39621FFF39621FFF39621FF8451128A0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000082501288F396
              21FFF39621FFF39621FF8451128A000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000082501288F39621FFF396
              21FFF39621FF8452128B00000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000000000000000000000000000082501288F39621FFF39621FFF396
              21FF8552128C0000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000082501288F39621FFF39621FFF39621FF8552
              128C000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000082501288F39621FFF39621FFF39621FF8653128D0000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000082501288F39621FFF39621FFF39621FF8754128E000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000082501288F39621FFF39621FFF39621FF8754128E00000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000000633D
              0D68F39621FFF39621FFF39621FF8653128D0000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000905
              0109B16E17BBF39621FF8854138F000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000009050109633D0D6801010001000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000}
            ParentFont = False
            Spacing = -4
            TabOrder = 1
          end
        end
      end
    end
    object PaLeft: TPanel
      Left = 637
      Top = 1
      Width = 380
      Height = 742
      Align = alRight
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object GrDetail: TwwDBGrid2
        Left = 1
        Top = 137
        Width = 378
        Height = 365
        TabStop = False
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        LineStyle = glsSingle
        ControlType.Strings = (
          'SOLUONG4;CustomEdit;;F')
        Selected.Strings = (
          'TENVT'#9'19'#9'T'#234'n m'#243'n'#9'T'
          'SOLUONG'#9'4'#9#272#7863't'#9'F'#9'S'#7889' l'#432#7907'ng'
          'DVT'#9'5'#9#272'VT'#9'T'
          'SOTIEN'#9'10'#9'Th'#224'nh ti'#7873'n'#9'T')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCTBH
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowCellHint]
        ParentFont = False
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = False
        UseTFields = False
        LineColors.DataColor = clScrollBar
        LineColors.HighlightColor = clWhite
        LineColors.ShadowColor = clWhite
        LineColors.FixedColor = clWhite
        OnCalcCellColors = GrDetailCalcCellColors
        OnDrawDataCell = GrDetailDrawDataCell
        FooterColor = 13360356
        FooterCellColor = 13360356
        PadColumnStyle = pcsPadHeader
        PaintOptions.BackgroundBitmap.Data = {
          07544269746D61700E090100424D0E0901000000000036000000280000009600
          0000960000000100180000000000D8080100120B0000120B0000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFE
          FEFBFCFCF9FAFBF9FAFAFAFBFBFAFBFBFBFBFBFCFCFDFEFEFEFFFFFFFFFFFFFF
          FFFFFFFFFFFEFEFEF5F8F7EBF0EDE5ECE8E4EDE8E9F1EDE7EFECE3ECE8E2EBE6
          E1EBE6E5EDE9EBF2EFE3EBE7E2EAE6E9EEECF3F6F5FEFEFEFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFCF9
          FAFAF6F7F8F8F9FAF9FBFBFAFCFCFBFCFDFBFCFDFAFBFBF8F9F9F7F8F8F8F9F9
          FEFEFEFFFFFFFAFBFAE2E9E6DBE4E0DEE8E3E2ECE7E8F1EDF1F7F4EBF3EFE2ED
          E7E0EBE6DFEAE4E5EFEAF9FEFCE6EEEADEE7E3DCE5E0DAE2DEDFE6E3F6F8F7FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F5F6F6F8F9FAFCFEFEFEFFFFFDFDFEFBFDFDFAFBFCF9FBFBFCFDFDFDFEFFFAFB
          FCF4F5F6F8F8F9FFFFFFECF1EED9E2DEDBE5E0DEE8E3E2ECE7E7F1ECEFF6F2EA
          F3EEE2EDE7E0EBE6DFEAE5E5EFEAF8FDFBE5EDE9DEE7E2DDE5E1DBE3DFD8E0DC
          E4E9E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFCFDFDFAFAFBFEFEFEFFFFFFFDFDFDFAFBFCF9FAFBF8F9F9FCFDFDFF
          FFFFF9FBFBF4F5F6FCFCFCFFFFFFECF0EED8E2DDDFE7E3E7EDEAEFF4F1F5F8F6
          F9FAF9F6F8F7EEF1F0E2EBE7DEE9E4E6EFEAF9FDFCE6EEEAE0E8E4DEE6E2DBE3
          DFD8E0DCE5EAE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFEFEFBFCFCFCFDFDFEFEFEFCFDFEFBFDFDFBFCFC
          F9FAFAF8F8F9F9F9FAFEFEFEFFFFFFFFFFFFEDF1EFECF0EEF4F8F8F9FCFDFAFE
          FFF2F9FDEEF7FAEBF2F6F2F3F3E2EBE7E2ECE7E9F1EDF9FDFCE7EFEBE0E8E4DE
          E6E2DCE4E0D8E0DCE5EAE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFBFCFCFAFAFB
          F9FAFAFAFBFBFAFBFBFBFBFBFCFCFDFEFEFEFFFFFFFFFFFFFEFEFEFCFDFEFBFD
          FEFBFCFCFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F5F5ECF4F8EFF7FAF7
          FBFDFAFDFEF8FBFDF9FBFCE8F1F4F0F1F2E5EDE9E5EEE9EAF2EEFAFEFCE7EFEB
          E0E8E4DEE6E2DCE4E0D7DFDBE4E9E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFDF9FAFAF6F7F8F7F9
          F9F9FBFBFAFCFCFBFCFDFBFCFDFAFBFBF8F9F9F6F8F8F8F9F9FEFEFEFFFFFFFD
          FEFEFCFEFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F5F6F5F7F7
          FCFBFBFEFDFDFEFDFDFEFEFDFDFDFCEAF2F5F0F1F2E5EEEAE3ECE8E8F1ECFAFE
          FDE7EFEBE0E8E4DEE6E2DBE4DFD9E1DDE9EDEBFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F6F6F7F9F9FC
          FDFEFEFFFFFCFDFEFCFDFDFAFBFCF9FAFBFBFCFDFDFEFFFAFCFCF4F5F6F7F8F8
          FFFFFFFDFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F6
          F6F8F8F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1F2E5EEEAE3EDE8E8
          F1EDFBFEFDE7EFEBE0E8E4DEE6E2DBE3E0D9E1DDE9EEECFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFD
          F9FAFAFEFEFEFFFFFFFDFDFDFAFBFCF9FAFBF8F9F9FBFCFCFFFFFFFAFBFCF4F5
          F5FBFBFBFFFFFFF9FBFEFEFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF5F6F6F8F8F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1F2E5EEEA
          E3EDE8E8F1EDFBFEFDE7EFEBE0E8E4DEE6E2DBE3DFDAE2DEEAEEECFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFDFDFDFCFCFCFCFDFDFEFEFEFCFDFEFBFDFDFBFCFCFAFBFBF8F9F9F8
          F9F9FDFDFDFFFFFFFFFFFFFCFDFEFDFDFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF5F6F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1
          F2E5EEEAE3EDE8E8F1EDFBFEFDE7EFEBE0E8E4DEE6E2DBE3E0D9E1DDE7ECEAFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCFDFEFBFDFEFBFBFCFEFEFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FAFDFEFEFEFEFEFEFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF5F7F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEA
          F2F5F0F1F2E5EEE9E3EDE8E8F1ECFAFEFDE7EFEBE0E8E4DEE6E2DBE3DFD7DFDB
          E4E9E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFCFEFFFEFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFBFDFEFEFEFEFEFEFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F6F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFD
          FDFCFCEAF2F5F0F1F2E5EEE9E3EDE8E8F0ECFAFEFCE7EFEBE0E8E4DEE6E2DFE6
          E3DCE3E0EBEFEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FAFCFEFEFFFDFDFD
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7F6F8F7F8FBFBFBFDFDFDFDFD
          FDFDFDFDFDFCFCEAF2F5F0F1F2E5EEE9E3EDE8E8F0ECF9FEFCE7EFEBE0E8E4DE
          E6E2DFE6E2DDE4E1E5EAE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F9FBFEFEFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9FAFFFF
          FFFAFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7F6F8F7F8FBFBFBFD
          FDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1F2E5EDE9E3EDE8E7F0ECF8FDFBE7EEEB
          E0E8E4DEE6E2DBE3DFD7DFDBE5E9E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFBFCFCF8F9F9F7F7F7F5F6F6F5F7F7EFF1F4EAECF1
          ECEDF1F3F4F5F7F9FAF8F9FAFCFCFDFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFBFCFDFDFDFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFEF7
          F8F9FFFFFFF5F6F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7F6F8F7F8
          FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1F2E5EDE9E3ECE8E7F0ECF7FD
          FBE6EEEAE0E8E4DEE6E2DCE4E0D8DFDCE5EAE7FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFDFEFEF7F8F9F6F7F7F4F6F7F6F8F8F8F9F9F9FAFAF9FBFBF9FA
          FBF8F9FAF7F8FAF7F9FAF7F9FAF7F9F9F7F8F8F6F7F7F8F9F9FBFCFCFFFFFFFF
          FFFFFFFFFFFFFFFFF8FAFCFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FAFAFBF7F7F8FFFFFFF1F2F2FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7
          F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5EFF1F2E7EEEBE3EDE8E7
          EFEBF6FCFAE6EEEAE0E8E4DEE6E2DCE4E0D8DFDCE5EAE7FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFEF2F4F5F4F6F7F8FAFBFBFCFDFBFCFDFBFCFDFBFCFDFB
          FCFDFAFCFDFAFCFDFBFCFDFAFCFDFAFBFCF9FBFCF8FBFCF7FAFBF6F8F9F4F6F7
          F6F7F8FEFDFDFFFFFFFFFFFFFAFBFDFDFEFEFDFDFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF5F7F8F9FCFCFEFFFFF8FBFCF5F6F7FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF6F7F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1F2E8EFEC
          E4EDE9E6EFEBF5FCF8E6EEEAE0E8E4DEE6E2DCE4E0D8DFDCE5E9E7FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFAFBFBF8FAFBF9FBFCFBFCFCFBFCFDFBFCFDFBFCFD
          FBFCFDFAFBFCF9FBFCF8FAFBFAFBFCFAFBFCFAFCFCFCFCFDFAFCFDF9FBFCF8FB
          FCF8FAFCF6F7F8F8F7F7FFFFFFFFFFFFF9FAFCFEFEFEFCFCFDFFFFFFFFFFFFFF
          FFFFFFFFFFF9FCFDF1FAFDF8FDFEFDFFFFF8FFFFF2FDFEFAFBFBFFFFFFFFFFFF
          FFFFFFFFFFFFF6F7F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEAF2F5F0F1
          F2E5EDE9E3ECE8E6EFEBF4FAF7E5EEE9E0E8E4DEE6E2DCE4E0D7DFDBE5EAE8FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFCFDFDF6F9FAF8FAFBF9FBFCFBFCFCFAFBFCF9FB
          FCFAFCFCFDFDFDFAFBFCF8FBFCF9FBFCF9FBFCFAFBFCFBFCFDFDFDFEFDFDFDFB
          FCFDF8FBFCF7FAFBF7FAFCF8FBFBFFFFFEFFFFFFF9F9FAFFFFFFFAFAFBFFFFFF
          FFFFFFFFFFFFF6FAFDEDF9FCF0FAFCF8FDFEFDFFFFF8FFFFF0FEFEEDF9FBFDFE
          FEFFFFFFFFFFFFFFFFFFF6F7F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFDFDFCFCEA
          F2F5F0F1F2E5EDE9E3ECE8E6EEEAF3F9F6E5EDE9E0E8E4DEE6E2DBE3DFD6DEDA
          E9EEECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEF6F9FAF7FAFBF9FBFCFBFCFDFA
          FBFCF9FBFCFBFCFDFEFEFEFCFDFDFBFCFDFBFCFDFAFCFDFBFCFDFBFCFDFCFDFD
          FBFCFDFBFCFDF9FBFCF8FBFCF8FBFCFBFCFDFDFEFEFEFEFEF8F8F9FFFFFFF6F6
          F7FFFFFFFFFFFFF9FBFEEBF7FBE8F7FBF0FAFCF8FDFEFDFFFFF8FFFFF1FDFEEA
          FAFDF3F9FBFFFFFFFFFFFFFFFFFFF6F7F6F8F7F8FBFBFBFDFDFDFDFDFDFDFDFD
          FDFDFCEAF2F5F0F1F2E5EDE9E3ECE8E6EEEAF1F8F5E5EDE9E0E8E4DEE6E2E1E8
          E4D8E0DCE6EAE8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FBFCF7F9FBF7FAFB
          FAFCFCFAFCFCF9FBFCFAFBFCFCFCFDFBFCFDFDFDFEFEFDFEFEFEFEFEFEFEFCFC
          FDF5F8F9F9FBFBFBFCFDFBFCFDFBFCFDFBFCFDFCFDFEFBFCFCFBFBFBF7F7F7FF
          FFFFF2F2F3FCFCFBFFFFFFF0F6FCE5F6FAE9F7FBF0FAFCF8FDFEFDFFFFF8FFFF
          F1FDFEEAFBFDE8F7FAFDFDFEFFFFFFFFFFFFF6F7F6F8F8F7FCFBFBFEFDFDFDFD
          FDFDFDFDFCFCFCE9F1F5F0F1F2E5EDE9E3ECE8E5EEEAF0F7F3E4EDE9E0E8E4DE
          E6E2E2E9E6D8E0DCE4E9E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF7FA
          FBF7FAFBF8FAFBFBFCFCFBFCFCFAFBFCFCFCFDFAFBFCFAFCFDFBFCFDFCFDFDFC
          FDFDFBFCFCF9FAFBFBFCFDFCFDFDFCFDFDFCFDFDFDFDFEFBFCFDFEFEFEF6F8F9
          F8FBFCFEFFFFF9FCFCF7F7F7FCFDFEE8F5FBE1F5FAE9F8FBF0FAFCF8FDFEFDFF
          FFF8FFFFF1FDFEEBFBFDE4F7FBF7FAFBFFFFFFFFFFFFF6F7F6F2F6F7F0F7FAF4
          FBFDF8FDFEF3F9FCF3F8FBF0F5F7F3F3F3E5EDE9E3ECE7E5EDE9EFF6F2E4ECE8
          E0E8E4DEE6E2DBE3DFD7DFDCE4E9E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFFFFF9FBFCF7FAFBF8FAFBFAFBFCFCFDFDFEFEFEFBFCFDFAFCFCFBFCFD
          FCFDFDFCFDFDFDFEFEFCFCFDFCFDFEFCFDFEFDFDFEFDFDFEFCFDFDFEFEFFFAFC
          FDF1FAFDF7FDFEFDFFFFF8FFFFF4FEFEF3F7FAE3F4F9E1F5FAE9F8FBF0FAFCF8
          FDFEFDFFFFF8FFFFF1FDFEEBFBFDE3F8FBF0F7FAFFFFFFFFFFFFF3F5F6F0F5F8
          F6F9FBF9FBFBFAFCFBF9FBFAFAFBFAF7F8F7EEF2F0E5EDE9E3ECE7E5EDE9EEF5
          F1E4ECE8E2EAE6E3EAE7DBE3DFD7DFDCE4E9E6FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFAFCFDF8FBFCFBFDFDFFFFFFFDFEFEFAFC
          FCF9FCFCFBFCFDFEFEFEFFFFFFFDFDFEFCFDFEFDFDFEFCFDFDFCFDFEFEFFFFF6
          FAFDEDF9FCEFFAFCF8FCFEFDFFFFF8FFFFF0FEFEEEF9FBDEF3F9E1F5FAE9F8FB
          F0FAFCF8FDFEFDFFFFF8FFFFF1FDFEEBFBFDE3F8FBE8F5F9FDFEFEFFFFFFF6F7
          F6EAEEEBE4EAE7E3EBE7E5EDE9E8F1EDEEF5F2E9F1EEE4EEE9E3ECE8E2EBE7E4
          EDE9ECF3F0E4ECE8E1E9E5E1E8E4DBE3DFD7DFDCE4E9E6FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFEFEFFFFFFFF
          FEFFFEFEFEFEFEFEFFFEFFFFFFFFFFFFFFFDFEFEFDFEFEFEFEFEFEFFFFFFFFFF
          FAFCFEEDF7FBE8F7FBF0FAFCF8FCFEFDFFFFF8FFFFF1FEFEEBF8FBDBF2F9E2F5
          FAE9F8FBF0FAFCF8FDFEFDFFFFF8FFFFF1FDFEEBFBFDE3F8FBE6F4F9FAFCFDFF
          FFFFF5F8F7D9E2DEDBE4E0DFE8E4E3ECE8E8F0ECEEF5F2E9F1EDE5EEE9E3ECE8
          E2EBE7E4EDE9EBF2EFE4ECE8E0E8E4DEE6E2DBE3E0D7DFDCE6EAE8FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF2F7FCE5F6FAE8F7FBF0FAFCF8FCFEFDFFFFF8FFFFF2FEFEEAF7FBDA
          F2F8E2F5FAE9F7FBF0FAFCF9FDFEFEFFFFF9FFFFF1FEFEEBFBFDE4F8FBE5F4F9
          F8FAFCFFFFFFF7F9F8D9E2DEDCE5E1E0E8E4E4ECE8E8F0ECEEF5F1E9F1EDE5EE
          E9E3ECE8E2EBE7E4EDE9EBF2EEE4ECE7E0E8E4DEE6E2DBE3DFD7DFDCEAEEECFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFEFFFFEBF5FBE1F5FAE9F8FBF0FAFCF8FCFEFDFFFFF8FFFFF2FEFE
          E7F6FAD8F1F8E1F5FAEAF8FBEFFAFDF5FBFDF9FDFEF4FDFEEFFCFDEBFAFCE3F8
          FBE3F5FAF3F6FAFFFFFFFCFCFCDCE4E1DCE5E0E5EDEAE4ECE8EBF2EFEEF5F1E9
          F1EDE5EDE9E3ECE8E2EBE7E5EEE8E8EFEEDFE6E9E0E8E4DEE6E2DBE3DFD7DFDC
          F0F3F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFEFEFFFFFFFFFEFEFEFDFEFEFDFEFEFCFDFEFDFEFEFFFFFF
          FFFFFFFFFFFFFFFFFFF9FCFEE4F4FAE0F5FAE9F8FBF0FAFCF8FCFEFDFFFFF8FF
          FFF2FEFEE9F5FAD8ECF4DDF0F8F5FDFFE5F6FEDDF1FCDBF0FADAF0FADFF2F9E2
          F1F7D9EDF4E0EFF6F2F4F7FFFFFFFFFFFFE1E7E7DBE4DFE1EAE6E4ECE8E8F0EC
          EDF4F1E9F1EDE5EDE9E4EDE7E2EBE7DBE2EDD0D4F5D7DDECE1EAE4DEE6E2DBE3
          DFD8E0DCF7F9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFDFDFDFBFCFCF6F7F9F6F7F9F8F9F9
          F8F9FAF9FBFBFDFEFEFCFDFEFCFDFDFAFBFCF7FAFBF1F3F7ECEEF6EEF0F5F0F2
          F4F1F4F5F2F4F5F3F5F6F5F7F8EEF4F8DBF0F7DFF4F9E9F8FBF0FAFCF8FCFEFD
          FFFFF8FFFFF1FEFEF0F8FCF3F9FCF6FBFCFCFEFFF7FCFFF4FBFEF4FAFDF4FAFD
          F6FBFDF6FAFBF4F7F9F0F5F8F3F5F7FFFFFFFFFFFFEAEEEFDAE3DFDFE8E4E3EC
          E8E8F0ECEFF5F2E9F1EDE5EDE8E1E9EBD6DBF5CCCEF9CCCCF8DADFECE1E9E4DE
          E6E2DAE3DFE1E8E4FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFFFFFDFDFDFAFBFBF6F6F7F2F3F3F2F3F3F4F5F6F4F6F9F6F7
          F9F8F9FAF9FBFBF9FBFCF5F6FAF5F7F9FBFCFDFEFEFFF8FAFCF3F5F9F0F2F7F1
          F3F8F4F7F8F6F8F8F5F7F7F3F5F5F3F5F5E5EEF3D3EAF2D8EEF3E2F3F6ECF7FA
          F6FCFDFDFFFFF8FFFFF1FEFEEFF8FCF4F9FDFCFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFDFDFDF8FAFBF1F5F9F5F7F9FFFFFFFFFFFFF6F8F8D5DCE5E1
          E9E3E3ECE8E7EFEBEDF4F1E9F1ECE6EDF2DCDFFDD1D1FCCCCCF9CCCCF8CDD0F5
          D6DDEBDEE6E2D8E1DCF1F5F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFDFEFEFCFCFBF9F9F8F9F9F7F7F7F7F7F8F9F9FAFBFBFCFDF7F9FCF2
          F4F8EEF0F6EEF0F8F9FAFDF3F4FAE9EBF5E9ECF3EBEEF4F1F2F7EAECF4E8EAF3
          E8EAF4E8EAF5EBEDF7F0F1F8F4F5FAF2F4FAF0F2F9E0EAF5CFE8F3D7ECF4E1F2
          F6EAF5F8F2F8F9FAFDFDF7FEFEF1FEFEEEF9FBF1F5F8F7FAFAFBFDFCFDFEFDFC
          FDFDFBFDFCFCFDFCFDFFFEF8FAFAF3F6F8EEF2F5F7F8F9FFFFFFFFFFFFFEFEFE
          D8DBEEDAE3E6E4EDE7E7EFEBECF4EFEFF4F8EBECFFDCDCFFD1D0FDCCCDFBD1D6
          F5DBE2ECDFE6E5DEE6E2D9E1DDF7F9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFBFBFCF7F8F9F9F9F9FAFAFAF5F6F9F3F4F7F0F2F6EFF0F5EEF0F8
          E5E8F3E3E5F2E2E4F2E2E4F2E2E4F1E6E8F3E6E8F3E6E8F3E7E9F1E7E9F1E9EB
          F3EBECF4EAECF5EBEDF6EDEFF7EDEEF6EEEEF6EFF0F8EFF0F9DFE9F6CEE7F5D5
          EAF6DFEEF7E5F2F8ECF4F9F2F7FAEFF9FAECFAFBE9F9FBE5F3F5E9F4F6F3F7F7
          F9FCFAF8FAF9F7F9F8F7F9F8F7F9F9F3F6F5F0F4F3F1F4F3FCFCFCFFFFFFFFFF
          FFFFFFFFE7E8F7CFD3EFE1EAE8E8F0EAECF0F9F3F3FFECEBFFDCDCFFD2D6FBDA
          E1F0E6EEEAE4ECE6E0E8E4DCE4E1E2E9E6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCFDFEF6F8FAF4F6F8F5F7F9F8F9FAF8FAFBF3F5F8F0F1F5EEF0F4EFF0
          F5F0F1F8EEEFF7EDEEF7EDEFF8ECEEF7E8E9F3EBEDF5F1F3F8F0F2F8EAEBF4E2
          E3F0E1E1F0DCD9EDD8D5EDD9D8EEDCDCF1D7D4ECD6D2EBDAD8EFDBD9F0D0D6EF
          C2D5ECC8DBF0E3EDF7D9EAF6D5EAF7D7ECF8D4EBF7D8EBF6DDEDF5D7EDF4DCEE
          F5F2F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFF8F9FCCECEF1D6DAF2DEE5F2E6E6FFF2F2FFEBEBFFDFE4F7
          E0E8E9E4EDE6E6EEE9E3EBE7E0E8E4DCE5E1EDF1EFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFEFEFEFCFDFDF6F9FAECEFF4E5E7F0E2E3EEEEF0F5EEEFF6D9D8EAD7D5E7D4
          D1E5D4D1E6DEDBF1D9D8F0DBDAF1DBDAF1DEDEF2DDDAEEDCDAEEDEDEF0DFDFF0
          D6D4E9D1CDE3D6D9E7D5D2EAD8D7EFD8D9F0D7D6F0D3CFE9D2CEE8D3CFEBD3CF
          EBCBCDECD7DAF1DCDFF3DDDEF2D6D9ECD6DBEFDAE0F1DEE5F2E9EEF6F1F5F8F1
          F5F7EFF4F8F3F5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E5F1D1D2F5D4DAF5E3E3FEF1F1FFE9EF
          F4E3EBE7E3EBE6E3EBE7E6EDEAE2EAE6E1E9E5E6EDE9FAFBFAFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFBFDFCFAFCFCFDFEFEEDEFF4DADBEBD7D6E9D7D6E9D6D4E8D7D4E7D2CEE5
          D7D5E7DBDAE9DAD9E8E0DCF0DFE0F5E2E4F7DDDEF4D5D2EED3D1E6D3CFE5D7D5
          EBD8D6EAD2CEE2D4D6E1D5D7E5D6D3E9DEDFF3DCDCF4D4D0EBD2CFE1D3D1E4D5
          D2EBDDDCF0D3D9F0D8D9F2E0E0F4DEDBF0DBD7ECDDD9EDDDDAEEDFDDEFE0DFF0
          E9EAF4F6F7F9EEF1F4F2F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F6F6D6DFF1E3F4FAE9F2FFEC
          F0F8E8F0EBE4ECE7E3EBE7E3EBE7E6EDE9E2EAE7DFE7E3E8EEEBFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCFEFEFCFDFEFFFFFFFFFFFFEBEAF3D8D4E8D3CFE8D5D2E8D4D0E6D1CE
          E1D2CEE1D2CFE0D4D1E2D9D9E7D3D0E5D9D7EEE2E3F6DEDEF4D3D0E9D3CFE1D2
          D0DED1CEDDD1CEDFD3D1E2D9E1E8D2D0E2D1CFDCD2D0DFD3CFE2D2CFE0D2CFE3
          D6D3E5D5D7E7D9DDECDADFF3D7DAEED6D4ECD8D6EAD9D7EBDAD9EBD9D7EBDCDC
          EDE0E0F0DCDFEDE9EDF5EDF1F5F1F3F3F8F9F8FEFFFEFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFDDDE8F0EDFEFF
          F0F8FFEDEEFEEAF0F4E3EBE7E3EBE6E3EBE7E5EDE9E2EAE7DEE6E2F1F4F3FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFAFBFBF9FBFBFFFFFFFFFFFFFFFFFFF6F6F9E6E1EFD8D4EBD5D2E9D3
          D0E6D1CEE1D5D3E4D3D0E3D1CEDFD1CEDED2CFDFD1CEE0D2D0E3D2CFE4D2CFE2
          D3D0E3D2CFDED1CFDAD6D2E5DBDDEAD6D8E6D2CEE4D2CFE2D2CFD9D2CFDDD2CE
          E5D2CEE5D2CEE5D4D6E5D2D0E7D5D2ECCED0E3C9CADDCDCEDED5D4E4DBDEE9DC
          DFEAD5D6E7D3D3E7D3D5E6DBDFEAF1F4F3FCFCFCFCFCFBF9FAF8FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDF
          F2DFEAF8E8EAFEEBEAFFEDEEFFE6EBF8E1E9EBE3EBE6E6EDE8E2EAE6E1E9E5FB
          FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFF4F6F5F9FBFBFFFFFFFFFFFFFFFFFFF9FAFDE0DDEFDCDBEE
          DBDAEED2CEE6D6D5E7DADAEAD9D8EAD4D1E8D3D0E2D2D0DBD2CFDFD4D2E4D3D0
          E5D2CEE4D2CFE4D1CEDFD2CFE1D4D0E7D5D8E7D2CFE6D4D1E7D4D1E7D1CEE0D3
          D0E6D4D0E8D1CDE6D5D4E8D8DBE9D4CFE8D2CEE0D1CEDED1CFE0D1CDE1D3D2E3
          D4D4E5D4D1E8D4D1E8D7D5EBD5D4EBE8EAF5FFFFFEFEFEFEFFFFFFFDFCFCFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE6E6F6CDD2EEE0E7FBECECFFE9E8FFE4E4FFDCE0FEDAE0F3E1E9EAE4ECE7
          EBF0EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF8FAFBFCFDFEFFFFFFFFFFFFFEFEFFE6E3
          F3D8D7EEDDDDEFDBDAEBD8D8E8D4D1E6D3D0E8D5D2EAD3D0E9D2CFE1D3D0E2D2
          CEE7D5D3E9D7D6E9D3CFE7D1CEE3D2CEE6D3D1E9D4D5E8D1CCE7D5D2EAD5D3EA
          D1CDE7D2CEE9D3CEE9D2CDE9D6DAEAD9DAEDD3CEE9D1CEDDD2CFE0D2CEE4D2D1
          E4D4D7E6D2CEE7D6D3EAD4D0E7D7D6EBD6D3EADFE3EFF8FAFAF6F7F9F8FAFCF9
          FAF9FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEFEFF9CDD6EDCEE8F2E2ECFDEAE9FFE1E0FFD8D8FCD0D1FACED3
          F6D6DDEEF0F4F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFAFCFDFBFDFEFDFEFEFF
          FFFFF6F7FBE2E3F2E3E4F2D7D7EAD3D0E6D2CEE5D2CEE8D4D1EBD5D3EDD2CEE6
          D2CEE3D4D0E7DDDDEEDBDBEDD2CEE6D0CDDDDDD9EADCDDF0DADEEDDAD8EED7D6
          EED3CFEAD1CDE5D3D0EADEDEF3E0E1F3D8DBEDD3D0EDD2CEE6D1CEDFD2CEE6D2
          CFE6D4D6E6D4D2E8D5D2EBDDDEF0D5D3E9D7D6EAD6D4EBE8E7F4F8FAFAF7F9FA
          F4F6F7F8F9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF9F9FDD2DDEECEEAF2CDEBF7DCECFDE2E3FDD8D7F8CF
          CEF6CCCCF5CCCDF3EFF0F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFEFE
          FAFCFDFCFEFEF7F9FAF8F9F9F4F4FAD5D2EBD3CFE8D4D0E9D9D7EDDBDBF0D6D3
          EDD3D0E8D5D2E8D3D0E7D3D2E5D1CEE1D1CEE1D1CEDED5D1E2DCDDEEE0E8F2DF
          DEF0D5D3EBD1CDE6D1CDE1D2CFE4D8D7EBDCDFEED6D6EAD2CDE8D3D1DED1CEE1
          D2CFE7D3D5E5D3D0E7D3CFE8D7D5ECDBDBF0D8D7EDDCDEEFE8EBF5FEFEFFFFFF
          FFFFFFFFF2F3F2FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBE4F1D1EBF3CCECF7CCF1FDD9F3FE
          D7E2F9CFD2F4CCCCF2CECEF1F8F8FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFDFEFEF4F6F6FAFCFCEDEDF4E3E2F1DBDAF0E0E1F1D9DAEDD5
          D9E7D3D0E3D7D4E8DADAEADADDEBD5D3E8D2CEE6D4D1E7D3CFE6D0CCE3D5D6E5
          DAE4EDD0CCE2D1CDE5D1CDE6D1CEE5D2CEE3D1CFDFD3D2E0D3D3DED3D6DAD3D9
          DCD3D7E0D3D7E6D2CFE6D2CFE6D4D2E8D8D6EDD6D3EDD5D1ECD9D9EEF2F4F9FF
          FFFFFFFFFFFFFFFFFEFEFEF7F7F6F8F9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E9F3CCEAF2CCECF8CDF1
          FDD7F9FFCFF8FFD3EEFAD2E2F6D4DBF2FEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFEFEF8FAFBFEFEFFEDEDF4D6D3E7DBDBF0DBDAF0
          D4D1EAD5D6E5D6D5E6DAD9EAD9DAE7DFE0EDDAD7EBD2CEE8D3CFE7D5D2E7D7D4
          E8D5D5E7D5D8E9D1CEE4D3CEE9D5D2EBD3CFE8D2CEE5D2D0E0D3DAE0D4E0DFD6
          E2E0D4DDE4D6DCE3E0E7E7D6D7E6D3D4E6D5D3EAD6D2ECD4D0EAD5D1EBD6D5EE
          E9EAF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7F6F5F6F5FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F3F7CCE8F2CC
          ECF8CDF1FDD6F8FFCFF8FFD0F7FED0F5FEE5F0F8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF5F7F7FCFDFEFDFDFEE5E4EFD7DB
          EADDDDEFDADAEBD7D6E9D8DAE9DFE2ECDFE1EEDAD9EAD6D3E9D8D5ECDAD9EBD6
          D6E6D6D4E8D3D0E7D0CCE1D4D2E7DCDBEFD9D9EADBDAECD5D3E6D3D9DDD5E3DD
          D8E2E1D8E5E5D2D4E5D6D3E7F4F5F1E5ECE9DAE2E5D4D5E7D3CFE9DBDAEED9D7
          EED1CCE9E7E7F3FFFFFFFEFEFFFFFFFFFEFEFFFEFFFFFEFEFEF2F2F0FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F8FA
          CCE8F2CCECF7CDF1FDD8F8FFCFF8FFCCF7FFCCF3FFF1F9FDFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF8F9FAFBFDFDF0
          F2F7DEE3EEE0E0EFDFE2EEE0E4EFE2E4F0D5DBE4D7D7EAD7D6E9D9D6EBE4E1F2
          D3CFEAD3D3E7D3D3E8D4D1E8D1CFE1D3D0E5D7D4EBD4D4E5D5D5E7D5DCE1D6E1
          DEDBE4E2DDE7E4D9E2E4D2D0E9D2CFEBE1E2ECF1F1EDE9ECE8DFE8E4DAE1E6D8
          DBEAD9D9ECE1DFF0F5F8FAFEFEFFFEFEFFFEFEFFFEFEFFFCFDFEFCFCFDF4F4F4
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFF9FCFDD1EAF3CCECF7CDF0FCD7F8FFCFF7FFCCF6FFCDF2FEF8FDFEFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
          FEFEFEF7FAFBF1F6F8FAFAFCFEFEFFF8FAFBF4F6F9D9DBECD9D7F0D3D0E8D1CD
          E5D7D2EBD9D8EFD9DBEDD6D5E8D5D4E8D8DDEAD1CEE5D1CDE4D2D1E5D5DAE5D9
          E4E2DFE8E6E0E8E7DCE3E6D6D5E8DCD8F1D8D6F0D9D7F1E0E2E9E6EAE5E0E6E2
          DBE6E1DDE8E5EEF3F5FEFFFFFFFFFFFEFFFFFDFEFFFBFDFEFCFDFEFBFCFDF8FA
          FBFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFBFDFED2EBF3CCEBF6CDF0FCD6F6FFCFF6FFCCF5FFD2F3FEFDFEFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFCFDFEF4F8FAF3F7F9FAFCFDEAEFF2DDDEEBDCDDEED8
          D6ECD3CFE9D1CDE9D3CFEAD5D3EBD5D2EAD6D4EAD4D2E9D2CFE6D4D2E7D2D0E7
          D5D9E8D6D8E7D5D4E9D7D6EADAD8EDD9D6ECD9D6EFD9DAF0DCDFF0E1E2F5D9DD
          EADBE2E6ECF2F0F9FBFBF8FBFCF5FAFBFAFCFDF9FBFCF7FAFBFAFBFCFEFEFEFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFCFDFED8EDF5CCEAF5CDEFFAD5F5FFCFF5FFCCF3FFDAF4
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEF7F9FAECF0F3E0E7ED
          DADFEDD6D2EED3CEEBD3CEEBD6D3EED4D0ECD2CEE7D2CFE8D5D4E9DFDFEDDBDE
          E9DDDDEED5D7EAD4D0EAD6D3ECDDDDEFDADBECDADBECD8D6ECD5D7EADDDBF1DF
          DDF3DBDBF1EEEFF9FDFEFFF8FAFBF8FAFBFCFDFDFCFDFEFEFEFEFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCEFF5CCEAF4CDEEF9D5F3FECFF3FFCC
          F1FEE2F5FCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFDFFFF
          FFFBFCFDECEFF6E4E1F5D6D3F0D8D6F2D8D8F2D7D6F0EEEEF6EFEFF8D6D2ECDE
          DFEED7DCE9DADBEEDBDAF0D5D2EDF7F7FBF2F2F9F0F0F7DEDFEDD2D0E8D2CFE9
          D3CFEBD6D5ECF1F3F8FFFFFFFFFFFFFCFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6F3F7D4ECF5CDECF7D5F2FC
          CEF1FDCCF0FCEAF8FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFD
          FEFEFFFFFFFFFFFFFBFCFDE7E7F4DEDEF4E0E3F6D5D4EFECECF6FEFFFFFAFAFD
          D9D7EFD8DAEED7D9EDD8DAEDDBDAF2F0EFF9FFFFFFFFFFFFFFFFFFF3F5F8DDDB
          F0D6D4EED5D3EDD5D4EBF5F8FBFFFFFFFFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEEBF4F7CDE8F2CDEB
          F5D4F0FACEEFFACCEEF9EFF9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFAFAFCEDF0F6F5F5FAFFFFFFFFFF
          FFF4F6F8DADFEEDCDCF5DADBEDDDDDF0F6F6FBFFFFFFFFFFFFFFFFFFFAFCFCFC
          FDFDEBEBF5E0E2F5DADAF1E7E7F3F5F8FAFFFFFFFEFEFEFEFEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFBFBEEF2F4CD
          E8F1CCE9F3D1EDF7CCECF7CCEBF7EAF7FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFFFFFFFF
          FFFFFDFDFEF9FBFCF2F4F9E6E6F4EEEFF5FCFDFDFFFFFFFFFFFFFFFFFFFEFFFF
          FEFFFFF8F9FAFBFCFCFAFAFDF9F9FDFDFDFEFBFDFDFFFFFFFDFDFEFEFEFEFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFD
          EBEEECDFEDF1E4F1F6DEF1F8D6EEF7D8EDF4E3EFF2FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFEF9FAFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFF8FAFBFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEFFFFFFF8F9FBFE
          FEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFE0E7E4E2E7E4E6EBE7E6ECE9E7ECEAE7ECE8E8ECE9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FBFCF6F9FBFFFFFFFEFEFEFF
          FFFFFFFFFFFFFFFFFEFEFEF9FBFBFFFFFFFFFFFFFFFFFFFEFEFFFEFEFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFF9FAF9DEE6E2E2EAE6DEE6E2DDE6E2DFE7E2E0E7E3E2E8E5FDFDFD
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFDFEFEFFFFFFFFFFFFFDFDFEFCFDFDFFFFFFFFFFFFFFFFFFFEFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFDFDFDF4F7F5E7ECEAE5EBE8E4EBE8E7ECE9ECF1EEF3F6
          F5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFEFEFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFDFDFEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFAFBFCF1F5F8FAFCFCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFBFAFA
          F7F7F6F5F4F3F2F2F1F1F0EFEFEEECEEEDEBEEEDEBEDEDEBEEEDEBEEEDEBEEED
          EBEEEDEBEFEEEDF1F1EFF2F2F1F5F5F4F7F7F6FBFBFAFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFDF7F9FBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF7F7F6F2F1F0EFEE
          ECEEEDEBEEEDEBEEEDEBEEEDEBEEEDEBEFEDEBEFEDEBEFEDEBEFEDECEFEDEBEF
          EDEBEFEDEBEFEDECEFEDECEEEDEBEEEDEBEEEDEBEEEDEBEEEDEBEFEEECF2F1F0
          F7F6F5FBFBFBFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF8F8F7F2F1F0EEEDEBEEEDEBEF
          EDEBF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEEC
          F0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECF0EEECEFEEECEFED
          ECEFEDECEFEDEBEEEDEBF1F0EEF6F6F5FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF6F5F4EFEEEDEFEDECEFEEECF0EEEC
          F0EEECF0EEECF0EEEDF0EEEDF0EEECF0EEECF0EEECF0EEECF0EEEDF0EEEDF0EE
          EDF0EEEDF0EEEDF1EFEDF0EEEDF0EEEDF0EEEDF0EEEDF0EEEDF0EEEDF0EEECF0
          EEECF0EEEDF0EEEDF0EEECF0EEECEFEEECEFEEECEFEEECF4F3F2FBFBFBFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFCFCFBF5F4F3EFEEECEFEEECF0EEEDF1EFEDF1EF
          EDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF2EFEDF2EFEEF2
          F0EEF3F0EEF3F0EEF3F0EEF3F0EEF3F0EEF2F0EEF2EFEEF1EFEDF1EFEDF1EFED
          F1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF1EFEDF0EEECF0EE
          ECF4F3F2FCFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFDFDFDF6F5F4F0EEECF0EEECF1EFEDF1EFEDF1EFEDF1
          EFEDF1EFEDF2EFEEF2EFEEF2EFEEF4F1EFF5F2F0F6F3F1F7F4F2F7F4F2F6F4F3
          F5F3F2F4F2F2F3F1F2F3F1F2F3F0F2F3F1F2F4F1F2F5F3F3F7F4F3F7F5F3F8F5
          F3F7F4F2F6F3F1F4F1F0F3F0EFF2EFEEF2EFEEF2EFEEF1EFEEF1EFEDF1EFEEF1
          EFEDF1EFEDF0EFEDF0EEEDF7F6F5FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEF9F8F8F0EFEDF0EEECF1EFEDF1EFEDF1EFEDF1EFEE
          F2EFEEF2EFEEF3F1EFF6F3F1F7F5F2F7F4F3F3F0F1EDEAEFE8E5EDE2E1EBDDDC
          E9DAD9E8D9D9E7D8D7E5D6D4E4D5D5E5D6D9E7D5D5E4D5D3E3D6D3E3D9D6E4DC
          D9E6E1DFE8E6E4ECECEAEEF2F0F1F7F4F3F8F5F4F7F4F3F5F2F0F3F0EFF2EFEE
          F2EFEEF2EFEEF1EFEEF2F0EEF2F0EEF1EFEDF2F1EFFBFAFAFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFDFDFCF4F3F2F0EEEDF1EFEDF2EFEEF1EFEEF2EFEEF2F0
          EEF4F1EFF6F3F1F7F4F2F4F1F2EBE9EFE1DFEAD9D7E6D4D1E2D1CEDED0CCDDCF
          CCDECFCDDED1CEE0D3CFDFD3CEDED2CEDED3D7E6D5DEE9D4D9E7D3D4E5D2CFE3
          D2CFE3D1CDE0D1CDDCD1CCD9D1CDDAD3D1DCD9D9E0E1DFE4EAE8EAF3F0F0F8F6
          F5F9F6F4F6F3F1F3F0EFF2F0EFF2F0EEF2F0EEF2F0EEF2F0EEF1EFEEF8F7F7FF
          FEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFAF9F9F2F0EEF1EFEEF2F0EEF2F0EEF2F0EEF2F0EEF4
          F2F0F7F4F2F4F1F3E9E6EFDEE0EBD6DBE7D1D3E4D1D1E3D3D6E5D5DAE7D4D9E6
          D3D3E2D2CFDCD1CDD8D1CED8D1CEDDD2CEDCD3CFDED5DAEAD6DFEBD5DEEAD4D8
          E8D3D3E7D3D1E6D3D3E5D4D5E3D4D0D6D3D0D8D4DAE3D4DDE2D5DDDED5DBDCD6
          D6DADBD9DEE6E4E7F3F1F1F9F7F5F8F5F3F4F1F0F3F1EFF3F0EFF2F0EFF2F0EF
          F2F0EEF6F5F4FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFAF9F8F1EFEDF2F0EEF2F0EFF2F0EFF2F0EEF4F2F0
          F8F5F3F5F2F2E7E5EAD7D9E8D3D9E8D7E0E9D5DEE9D3D8E7D2D6E6D6DCE8D7DF
          E9D5DCE8D5DBE8D5D9E6D3D3E0D1CFD9D1D0DFD1CEE0D2D1E3D4DBEBD5DEEBD5
          DCEBD4DAEAD4DAEAD3D6E8D4DAE8D5DBE7D3D2DCD3D0D7D4D6DED6D9DDD6DBDC
          D8DDDFD7DEDFD6DBDCD3D3D7D5D3D7DEDCE0EDEBECF9F6F5F9F6F4F5F2F0F3F1
          F0F3F1EFF3F1EFF3F1EFF5F3F2FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFAF9F8F2F0EEF3F0EFF3F1EFF3F0EFF4F1F0F8F5
          F3F6F4F3ECEAEBE1DEE2D8D6E1D6E0EBD7E3ECD7E2EBD5DDEAD3D8E9D3D7E9D4
          D8E9D6DEE9D6DEE9D5DCE9D6DEE8D5DBE7D3D5E4D2D0E0D2CFE2D3D7E8D4D9EB
          D4D6E9D4D5E9D3D2E8D3D4E9D4D7EAD4DAEAD5DDEAD4DAE9D3D3E0D2CFD9D3D1
          D6D6D9DBD5DAE0D4D6DCD4D4DDD6D8DDD6D8DBD5D6D9D4D4D8DADADEEBE9ECF9
          F7F6F9F6F4F4F2F0F3F1F0F4F2F0F3F1EFF5F3F2FEFDFDFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFBFAF9F3F0EFF3F1EFF3F1EFF4F1EFF7F4F2F8
          F6F5EFECEDE4E1E5DDDBE2D6D4E0D5D7E7D7E3EDD7E3EDD7E2ECD5DEEBD4DAEA
          D3D6E9D2D2E8D4D7E9D5DEEAD5DCE9D5DDE9D5DCE8D4D9E6D3D4E3D2D1E2D2D2
          E5D3D2E4D3D2E8D2D0E7D3D0E7D4D4E9D4D4EAD5D9EBD5DCEAD5DCE9D5DAE6D4
          D5E3D3D2E0D3D0D6D5D9DBD6E1E2D5DBE0D4D5DED4D6DCD4D6DBD5D8DDD4DADE
          D3D5DADAD9DFEEEDF0FBF9F7F7F4F2F4F2F0F4F2F0F4F2F0F6F4F3FFFEFEFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF4F1F0F4F1F0F4F1EFF5F2F0FAF7F5
          F4F2F2E4E2E6DAD8E0D6D4DED2D0DCD2D0E0D6DEECD7E3EDD8E3EDD6E2ECD5DF
          EBD3D8EAD3D6E9D2D3E9D5DBEAD5DDEAD5DDE9D6DEE9D4DAE8D4D8E6D4D6E5D3
          D1E3D1CDE1D2D2DFD5DDECD4D7E9D1CFE6D3D0E5D5D3E5D4D5E8D5DAEBD4DDEB
          D5DEEAD4D7E9D2D1E6D2D1E1D3D0D7D4D3DDD6DCE1D7DFE1D5DCE2D5DBE0D5DD
          E0D5DEE0D6DFE0D5D5D9D4D1D6E4E4E7F9F8F7F9F6F5F5F2F1F4F2F1F4F2F0F9
          F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F4F3F4F2F0F4F1EFF6F3F1FAF8
          F6ECEAECDAD8E0D4D1DDD2D0DDD2D1DDD1CFDED3D5E6D7E4EED6E2EDD5E1ECD5
          E1ECD4D7EAD2CEE7D3D1E8D2D0E8D5DAEAD5DEEAD5DCEAD4DAE9D4D7E7D2D1E5
          D2D0E4D2CDE2D2D2E5D5DFEDD6E2EED5DCECD4D6EAD3D6E8D1D0DDD1CDD9D2D0
          DFD3D6E4D5DBE9D4D8E9D4D2E9D4D4E8D4D5E4D3D2E2D3D1E0D3D7DFD2D4E2D3
          D3E1D7DCDFDBE5E5DBE7E6D8E2E2D5D5DED3D1DCE1DFE2F8F7F5FBF8F6F6F3F1
          F5F3F1F5F3F1FDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFAF9F4F2F0F3F1EFF7F4F2F9
          F7F6E5E4E8D4D3DFD2D4E1D3D6E3D4D8E5D4D8E6D2D1E5D4DBEBD8E5EFD6E2EE
          D4E0EDD4DEECD3D2E9D2CCE6D2D1E7D3D6E9D5DBEAD4D9EAD3D8E9D3D8E9D3D4
          E7D2D0E5D1CDE3D2D0E3D3D9EBD5DDEDD7E0ECD4D6EBD6DCEBD7E3EDD4DDECD2
          D2E5D1CFDED1CCD8D2CED8D2D0D9D2D0DCD3D4E4D5D5E5D4D3E4D3D4E5D3D4E0
          D4D8DFD4D6E2D4D4DCD4D4D9D8D9DEDEE2E4D8DDE7D5D8E6D4D7DBE0E0E4F8F6
          F5FBF9F8F7F3F2F5F3F2F7F5F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF6F3F2F4F1F0F8F4F3
          F8F6F6E5E5E8D3D6E3D2D6E5D3D7E7D3D9E8D4DBE9D2D4E7D2D0E5D6E0EDD7E5
          F0D6E2EFD6E2EED5DFECD3D7EBD3D2E9D3D4E9D3D5EAD3D8EAD5DDEAD5DDEAD4
          D9E9D2D3E7D1D0E5D1CEE4D4DCEBD5DFEDD4D9ECD5DBECD3D2EAD5DAEBD5DDEC
          D5DDECD4D5EAD3D3E9D2D4E3D1CFD9D1CED5D2CFD7D1CED9D2CFDCD3D1DFD3D2
          E2D3D4E2D5D7E3D6D9E2D7D9E2D6D5DAD5D1D8D7D3DBD4D2DFD7DAE2D8DBDFD8
          DFE1E8E9E9F7F6F4FCF9F8F7F4F2F6F3F2FCFBFBFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9F8F4F1F0F8F4
          F3F9F7F6E8E8E9DCDEE5D3DAE8D3D9E9D2D0E8D3D7E9D7DFEAD3D4E8D2CFE6D4
          D6E9D8E4F0D7E4F0D7E3EED4DEEDD3D6ECD3D8ECD3D1EAD3D5EAD5DDEBD6DFEB
          D5DEEAD3D8E9D1D2E7D2D4E6D3D4E8D3D7ECD4D9ECD5DDECD5D9EAD3CFE8D5D8
          EBD5DAEBD4D8EBD4D5EAD4D7EAD7E1ECD4DAE8D2D1DCD2CED5D2CED7D2CED8D2
          CFD7D2CED5D2D0D8D4D2DCD4D5DCD6D8DED3D0D6D2CED4D2CED5D2CED4D7DADB
          D9DADDDADEDFDDE4E4F0EFEEF6F5F3FCF9F8F7F4F2F8F6F4FFFFFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF5F3F2F6
          F3F2FAF8F7ECECEDE4E4E7DAE0E9D5DFEAD6DFEAD4DBEAD5DDEAD4DBEAD4DAE9
          D3D4E8D2D2E7D5DBEAD8E3EFD8E5F0DAE6F1DBE5F1DCE5F0DADEEED9E1EDD7E2
          EDD5DFEBD5DEEAD3D8E9D2D5E7D4D5E8D3CFE8D3CFE8D4D2E9D3D2E8D3D0E8D4
          D4E9D5D8EBD5D7EBD3D3EAD4D6EBD4DAEBD5DAEBD5DCEBD4D9EAD3D6E6D2CFDA
          D2CED2D3CFD6D3D0D6D4D0D5D3CFDBD4D1DFD4D1D6D1CED3D2D0D4D3D2D7D3D1
          D7D5D3D7D9DADDD8D7DAD8DFDFE7EAEAF3F1EFF7F5F4FCF9F8F7F4F2FCFBFBFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFAF9
          F5F2F0FBF8F7F1F0F0E9E9EBE6E6EAD9E2EBD7E2EBD7E2EBD7E2EBD6E1EBD5DA
          EAD4D8EAD4DBEAD4DBEAD3D8E9D3D5E8D4D8E9D8DEECDAE1EDDCE3EEDDE4EFDA
          E2EDD5DEEBD4DAEAD3D7E9D3D7E8D3D5E6D2D4E8D3D1E7D2CEE6D3D0E7D2CEE6
          D4D3E8D5D8EAD5DBEBD4D6EAD3D1E9D4D5EAD5DDEBD3D6EBD4D7EBD4D9EAD5DD
          EBD3D8E8D2CFDCD3CFD6D3D0D5D4D1D5D2CFD9D3D0D9D4D0D4D3D3D8D4D7DCD5
          D5DCD4D2DAD5D1D9D7D6D9D8D6DCDADDDFE1E7E7F2F0EEF1EFEDF9F7F6FBF7F6
          F9F7F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFF6F4F2FAF7F5F6F4F4EDECEDECECEDE6E8ECD7E1ECD7E2ECD8E3ECD7E2ECD7
          E2ECD7E1ECD5DCEBD3D9EAD3D8EAD2D6E9D2D3E8D2D3E8D1D4E7D2D4E7D2D4E7
          D1D1E6D2D4E6D2D4E6D3D5E6D2D4E6D2D3E5D4D9E8D4DBEAD4DAEAD2D1E5D1CD
          E3D1CCE2D1CDE3D2D0E5D4D7E9D3D5E9D5D5E9D5D8EBD5DCEBD7DFEDD5D9EBD4
          D6EAD5DCEBD6DEEBD3D4E7D2CFDBD2CFD3D3CFD3D3D0D5D3D1D7D4D3DAD5D5DA
          D4D2D9D4D0D8D4D0D8D4D0D7D4D2D6D8D7D9D9D9DADBDFE0EEEDEDF1F0EEF1EF
          EEFCF9F8F9F5F4FEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFCFCFCF7F4F2FAF8F7F0EEEFEEEDEDEFEDEEE2E7EDD5E1ECD7E3EDD8E4ED
          D7E2EDD7E2ECD5DEECD5DDECD4DDEBD4DBEBD3D5EAD2D2E8D1D2E8D2D4E9D3D8
          E8D3D8E8D2D5E7D2D3E7D1D1E6D3D7E7D4D9E7D3D6E6D5DEEAD6E1ECD5E1ECD4
          DBEAD3D5E7D2D1E4D2CFE2D1CDE2D3D1E7D3D0E5D3D3E7D4DBEBD7E2ECD5DEEB
          D4D7EAD4D4EAD3D3E9D4D9EAD4D6E5D3D0E3D3D0DFD3D0D5D4D1D9D4D2D9D4D4
          DCD3D2D8D3CFD6D2CED4D2CED4D2CFD4D2CFD3D4D1D3D8D5D7D9DADBE8E9E9F2
          F0EFEFEDECF6F4F2FCF9F7FBF9F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF8F6F6FAF7F6F4F2F2EFEDEDEEEDEDF0EDEDDEE5EDD6E2EED8E5
          EFD8E5EFD8E5EED6E1EDD4D9EBD4D8EBD4DBEBD3D8EAD3D4E9D3D5E9D3D7E9D3
          D8E9D5DBE9D3D8E8D2D6E8D3D7E8D3D7E7D5DBE8D4D9E7D3D5E7D5DDEBD5E0ED
          D6E2EED5E0EDD3DAEBD3D7EAD3D7E8D4D5E5D2D0E4D2CEE2D1CDDFD3D1E5D4D7
          E8D4D7EAD3D3EAD4D4EAD4D2E9D4D2E6D3D1E0D4D3E2D5D4E2D5D3DED4D1DDD3
          D4DBD3D3D8D2D2DAD4D5DCD2CFD6D2CFD7D3CFD7D3CFD6D2CED3D6D5D7DADBDC
          E0E2E2F1EFEDEFEEECF0EEEDFBF9F7FAF7F5FEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFEFEF7F5F3FAF7F7F0EEEEEEEDEDEEECEDEEEDEDDDE6EFD4
          DAEED8E3F0DBE8F1DCE8F1D7E1EED4D6EBD3D1E9D2D2EAD2D3E9D2D1E9D2D1E8
          D3D4E8D3D5E7D3D6E7D2D3E6D3D3E6D3D4E7D4D8E8D5DBE7D3D7E6D3D8E8D4DC
          EBD6E1EDD8E4EFD6DFEED4D7ECD3D8EBD4D9EBD5DDEBD4DAE9D4D8E7D2D1E1D1
          CDDFD2CDE1D2CFE7D5D5EBD7DCEDD4D5E9D2CFE2D3D1DFD4D1DED4D1E0D4D2E0
          D3D5DED4D8DCD4DADDD6E0E2D5DAE0D3D2DED3D2DFD3D3E0D2D3E1D2D5E2D5D7
          DDD9D8DADCDDDEEDECEBF0EEEDEEEDEBF6F4F3FCF9F7FDFDFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFDFDF9F7F5F5F3F3EEEDEDEDECECEDECECEEECEC
          E1EAF0D8E0F2D8DEF1DCE9F2DCE9F2D5DEEFD4D5ECD5D5ECD5D3EAD4D2E9D3D0
          E8D2CEE6D2CFE5D2D0E6D1D0E6D1CEE5D1CFE5D2D0E5D2D1E4D3D6E6D3D4E6D3
          D3E9D4D9EBD7E3EED7E3EFD5DDEDD4D7ECD5D7ECD4D5EBD3D6EBD4DBECD6E0EC
          D6DDEAD2CFE5D2CDE1D2CFE1D3D4E5D4D9E9D6D5E7D5D2E5D3D1E2D4D4E1D5D4
          DFD5D7E0D6DEE1D7E0E3D7E0E2D6DADED3D4DED3D4E1D4D2E4D7D7E8D9E0EADA
          E5EED7DAE1D6D2D8DADDE0E9EAE9EFEEECEEEDEBF1EFEEFCF9F8FCFBFAFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFAFAFAF8F6F1EFEFEDECECECEBEBECEB
          EBECEBEAE9E9EAE0E9F1DAE7F4D9E5F3D6E3F1D4DBEFD6DAEFDADEEFDBDBEDD5
          D4E9D3D1E7D4D4E7D3D7E9D4DAEAD4D9E9D2D0E7D2D0E6D1CFE6D2D1E4D2D2E5
          D3D3E9D4D5EBD4DCEDD6E2EFD4DFEED3D5EBD3D3EBD5D7ECD4D5EBD4D1EAD6DC
          EDD7E3EED5DEEDD3D9EBD3D7EAD4D7E7D2D2E2D1D0DED3D1DDD5D4E4D2D0E3D3
          D4E4D3D3DFD3D6DFD4D8E0D4D7DFD3D3DAD2CFDAD4D0D6D3D1D7D3CFDBD4D2E0
          D6DBE6DAE0E7D9D6DBD6D6E1D8DCE1E4E7E6EFEDEBEEECEBEFEDEBFAF7F6FCFA
          F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF8F7FAF8F7EFEDEEECEBEBEB
          EAEAEBEAEAEBE9EAE9E7E8E6E4E5DFE4EBD9E4F3D5E3F4D9E5F5DBE5F5D7DEF1
          D4D4EBD3D0E7D6DAE9D5DEECD4D9ECD4D9EBD6DDEBD4D4E8D3CFE7D1CDE5D2D1
          E5D4D6E8D4DAEDD5DEEED6E2EFD5E1F0D4DEEFD3D5ECD4D2EAD5D5ECD6D8EDD5
          D4ECD6DBEED7E1EED5DCEDD5DBEDD5DEECD4DCEBD3D7E9D2D2E5D1D0DFD2D2E1
          D3D9EBD4DEEAD7E2E8D8E2E8D6E1E7D7E3E8D6E3E7D6DEE8D4D5DCD5D2D5D6D4
          D8D6D6DCD4D7E2D4D2D7D7D2D3D8D7DFD7D9DDE0E5E5EEEDEBEDECEAEDECEAF7
          F4F3FDFAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF8F7F8F6F5EEECEC
          ECEBEBEBE9EAEAE9EAE9E8E9E7E6E8E5E3E5E1DDE2D7DAE5D9E7F3DCE9F4DFEA
          F3E2ECF5DCDEEFD4D5E9D7E0EDD5DEEDD4D7EBD3D4EAD5DBEAD7DCEAD3D1E7D1
          CDE5D1CEE4D4DDECD7E4F1D9E6F2DAE7F2DBE8F3DBE7F3D7DEF1D3D2ECD5D5EC
          D9E0EFD9DEEFD7DCEDD4D5EBD5D7ECD6DBEDD6DCEDD4D7EBD3D7EBD3D4E9D2D1
          E6D2D2E4D4D8E8D4D6E0D7D6DDD8D8DED6D6E1D9DCE8D5DAE4DBE1E6DBE2E8D5
          D9E0D4D3DCD4D3DAD8DDE2D5D8DDD9D3D5D9D5D6DBDADDE1E5E5EDECEAECECEA
          EEECEAF4F2F1FDFAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F8F7F5
          F4ECEBEBEBEAEAEAE9E9E9E8E9E8E7E8E6E5E7E4E1E4DFDDE3D3D5E1D5D8E2D5
          D6DEDEDEE1EBEAE8E1E1EBD3D9ECD5DBECD4D7EBD3D4EAD2D4EAD3D8EAD5DBEA
          D3D1E8D1CEE6D0CCE4D5DDEEDCECF7DEEBF6DEE9F6E1EBF6E0EBF6E2EBF6DFE3
          F4D7DDF0D7E4F0D7E0EED3D3EAD4D0E9D5D2EAD6D3EAD8DFEED7D8EDD5D5EBD5
          D5EBD4D4EAD3D5E9D2D4E7D2D3E3D1CEDED1CDDCD4D4E3DADCE9E0E5ECDAE0E3
          D4D4DAD7D9DDD7DDE1D6DCE2D5D7DED7D7E1D8D3D7D9D6D8E1E1E2EBEAE8ECEB
          E9EDEBE9EDECEAF3F1EFFCFAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
          FAF8F6F4F3ECEBEBEBE9EAEAE8E9E8E7E8E7E6E8E5E4E6E3E0E4DEDCE2D4D9E6
          D1D3E1D9DBE4E6E5E9E5E4E6DADDEAD4D6ECD4D4EAD4D3EAD3D3EAD2D2EAD3D7
          EBD5DCEBD4D4EAD3D2EAD2CFE8D1D1E9DDE8F5E4F0FAE1EAF8EBF0FAE7EEF9E5
          EDF8E8EFF9DCE3F4DAE7F4D8E0F1D2CEE9D5D4ECDADDF0DADBEED8DFEFDBE2F0
          D6DBEDD4D6ECD5D8ECD5D7EBD4D8EBD3D9EAD3D6E8D1D1E7D6DFEDDBE5EBD9E1
          E5DCE1E5DBE3E6D4D5D7D6D2D4D8D7DBD7D6DBD6D3D8D5D1D5DAD8D9E6E5E5EB
          E9E8ECEAE8EDEBE9EDEBE9F2F0EEFCFAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCFAF8F6F4F3ECEAEAEAE9E9E9E8E9E8E6E8E6E5E7E4E3E6E2DFE4DCDA
          E1D3DAE9D2D8E9D4DAE8D4D7E8D5D6E7D6D9E9D5D8EED4D5EBD4D8ECD2D1E9D2
          D1E9D3D5EAD4DCEBD5D9EBDADBEDD8DAEDDAE2F1D7DFF0DBE4F3E7EEF9EAF1FB
          EDF2FCEAEFFAEAEFFAE2E8F7E6EDF9ECF1FAE2E2F4DBE1F3DAE7F4DCE6F3D7DA
          EFD5DBEED4D5EDD5D6EDD5DAEDD5D6EBD4D5EBD5DBECD5DFEDD5DFEED8E3EFE4
          EBEDE1EDECD8E0E2DDE0E3DEE7E8D9E0E1DAD9DFD7D3DAD7D4D7D4D6D9E2E2E1
          EAE7E6EAE8E6EBE9E7ECEAE8EDEBE9F2F0EEFCF9F8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFCFAF9F7F5F4ECEBEBEAE9E9E9E8E9E8E6E8E6E5E7E4E3E6E1
          DEE3DADAE3D3DCEAD3DAEAD4DAE9D4DBE9D4DAE9D4D7E8D5DDEED4D6ECD6DBEF
          D5D4ECD5D4EBD2D3EAD2D9ECD8E2EED9E2EED6DDEDDDE4F0DBE3F0D7E0EDD8E0
          EEDCE4F3E5EAF7E6EBF8EAEDFAE7EBF9E9EEFAEAF1FBEDF3FDECF0FBE2E8F7DC
          E4F5DDE2F4DDE0F3DDDFF1D3D6EED3D7EED2CFEAD4D1EBD6DCEFD8E5F1D7E5F1
          D6D8E1DCE0E5E9F0F0E3EFEDD6E0E5D7DBE5DDE3E8D6D5D9D4D0D2D6D3D5D6DB
          DDE3E2E2E7E4E4E9E6E6EBE8E7ECEAE8EDEBE9F2F0EEFCF9F8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFDFBFAF9F6F5EDEBEBE9E8E9E9E7E9E8E6E8E6E5E7
          E4E3E6E0DDE3D8DDE6D4E1ECD5DEEBD5DBEBD5DBEBD4DAEAD3D4E9D5DBEED3D5
          EDD6D8ECD7DAEDD4D4EBD3D3EBD5DAEED7E3F0D7E4F0DBE7F0D9E5F0DBE6F1DA
          E6F1DAE6F1D9E5F2DAE7F3DEE9F5E0EBF7E7EFF9E5ECF8DDE2F4DDE2F3DFE2F3
          E3E6F4DEE4F4E5E8F6EAEDF9E7EAF8DFE5F6DDDFF3DBDBF1D9DBF0D9E2F2DBE9
          F5D7DFEBD1D1DFD3D6E3D9DDE7EAF0F0DDEBEAD3DBE6D2D0DAD2CECED5D0D2D5
          D9DCD8DDDFE1DFE2E6E3E4E9E6E5EAE8E7EBE9E8EDEBE9F4F1EFFDFAF9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFBF8F6EEEDEDE9E8E9E8E7E9E8E6
          E8E6E5E7E5E4E7DFDDE3D5DCE8D6E3EFD4DEEDD4DBECD4D8E9D5D7EAD4D3E9D4
          D7EBD5D9EED4D1EAD6D7EDD6D8EEDAD9EFDBDDF0DEE7F4E4EEF7E5EFF8E2ECF7
          E3EDF7E6F0F9E5F0F9E0EBF8E6EFFAE4ECF9DCE7F5DAE5F1D7E0EED5DCECD4D9
          EBD3D9EBD3DBEBD3DBEAD2D6EAD7DAEDDDE1F0E8EDF8F6F9FEEEF1FBE0E8F7DE
          EBF8DEEAF7D7DAE0D2D8DDD2DAE1D1D9E0D9E3E7E6EFEEDEEBE9D5D9DAD4CECF
          D4D5D8D6DDDFDCDADDE4E0E1E7E4E3E8E6E5EAE8E6EBE9E8EDEBE9F6F3F1FEFC
          FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFAF8F2F0EFE9E9EAE8
          E7E9E7E6E9E6E5E8E5E4E7E2E1E6DAE1EDDCE7F3D9E1F1D8DEEFD3D4EAD5D5EB
          D4D6ECD3D1E9D4D5ECD6D6EDDDDDF0E3E9F6E8EDF8E6EAF7E3E9F7E3EAF6E4EB
          F7E3EBF8E5EDF8EAF2FBE7F2FBDDE7F5DADDF0D6D8EED4DAECD7E0EED5DCEDD3
          D8ECD4D8ECD6DAEDD6DFEDD4DDECD3D9EBD3D9EAD1D7E9D2D9E9DEE5F1ECF2F9
          EDF4FBE3ECF6D9DBECDBDCEADBE1E4DEE8E8DBE7E9D6E3E4D8E1E4E4EDECDFEB
          EAD5DBDCD5D8DADAD6D8E1DADCE5E1E2E7E4E3E8E5E5EAE8E7EBE9E8EEECEAF9
          F6F4FFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCFBF6F4F3
          EBEAEBE9E8EAE8E7E9E7E6E9E6E6E8E7E6E8E0E7F0DDE7F5DAE1F3D7E0F3DBE0
          F3DEE0F2DADDF0D7D4ECD6D8EFD8E0F3E3E6F5F1F2FAEFF3FBE9EFF9EBF1FAE9
          ECF9E8EBF9E6E9F8E1E3F5DCE0F3DDE3F3D8DAEFDBDBEFDDDFF1D9DCEFD5D8ED
          D5D5ECDADAEED8D8EDD5D5EBD4D6EAD2D6EBD3D6EBD5DBECD5DDEDD3D7EBD1D6
          E9D2DAEBD5DCECD5DDEAD4DBE9D5DDE9D3D2DFD4D2DFD9D9E0DADDDED7DCDCDA
          DDDEE2E9EADDECEBD6D7D8DBD2D4E2DCDDE5E1E2E6E4E3E8E6E5EAE8E7EBE9E8
          F0EDECFCF9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
          FEFBF8F7EEECECE9E8EAE8E7EAE7E7EAE7E7E9E7E7E9E5E6EAE1E4EDDCE2F1DE
          E5F5E2EAF7E1E9F8E1E7F7E3E8F8E2E7F7E6EBF9E2E7F5E3E5F5DFE3F4DCE2F4
          E9ECF9EBEDF9E5E7F6E9EAF7D9D8EDD2D0E7D5D6EBDCDDEFDDDEF0DFE1F1DCDE
          F0D8D7EDD9D9EEE1E3F2E2E5F3DDE1F0D9DCEDD7D7EDD7D9EDD8DFEED8DEEFD6
          D7ECD5D8ECD8E0EDD6DFECD6E1EDD6E3EDD6E2EBD4D8E6D3D4E0D2D0D8D2CFD3
          D4CFD1D8D3D9D7D5D8DFE7E6DCE6E5D9D5D7E5DFE0E6E2E2E7E4E4E9E6E6EBE8
          E7EBEAE8F4F1EFFEFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFEFDFCF4F2F1E9E9EAE8E8EAE7E7EAE7E7EAE6E7EAE6E6E9E7E6E9
          E8E7EAE7E8ECE7E9ECE5E8EEE4E7EFE6EAF0E6EAF1E3E9F4D9E1F0D7DCEFDDDE
          F2E2E3F3DEDEF1D8D7EDD2D0E8D2CFE7D1CDE7D3CFE7D0CDE6D0CDE6D2D0E7D1
          D4E8D2D6E9D3D3E8D2CFE6D2D0E6D3D2E8D5D4E9D8D6EBD8D8ECD7D7EBD8DAED
          DBE1F0DADDF1D5D5EED2D4ECD2D2EBD6DCEDD9E5EFD7E1EED8DEE9D5DAE2D3D6
          DFD4D4DCD4D1D7D4D0D4D1D1D3D7E1E1DEE3E5DBD8DAE6E2E2E6E3E3E8E5E5E9
          E7E7EBE9E7EDECEAF9F6F4FFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFCF9F8EEECECE8E8EAE7E7EAE7E7EAE6E6E9E6E5
          E9E5E5E9E6E6E9E9E8E9EBE9EAECEAEAECEAEAEBE9E9ECEAE9ECEBEAD7DDEED2
          D4EDD2D0EAD1D1EAD4D4EAD4D3EADAD9ECD8D7ECD9D9EDD6D7EAD5D4EAD4D1E9
          D2D0E7D2D2E8D2D5EAD3D4E9D1CDE7D1CEE7D2D0E8D2D0E9D4D0EAD5D3EBD1CE
          E6D2CFE6D3D2E2D8D9E5DADBE9DFE1F0D9DAEFD5D6EDDCE4F1DAE5ECD4DDE3D4
          DDE2D3D7DDD5D1D5D3D2D6D2DAD9D6E5E2DEE4E4E0DCDFE4E1E1E6E3E4E7E5E4
          E8E6E6EAE8E7EBE9E8F3F0EEFEFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEF7F4F3EAE9EAE7E7EAE7E7EAE6
          E6EAE6E5E9E5E5E9E5E5E9E5E5E9E7E6E9E9E8EAEAEAEAEBEAEBEBEAEAE3E6ED
          D4DDF1D7DCF0DBDDF0DCDBEFD6D7ECD7D9EDD6D9EDD8DFEFD8DEEFD6DDEED6D9
          EDD4D5E7D3D1DED5D7E9D3D9EDD2D8EDD5D6EED4D7EED3D8EDD8DFEFD8DEEFD6
          DAEDD4D5EBD3D4EAD3D2E5D3D2DDD2D2DED5D6E3D8DCE9DADFEDD8E0E4DFEAE9
          DFEEECDBEBEAD9E5E4D8E1DFD9E7E5DEEBE8E5E7E6E7E3E4E7E4E4E6E4E4E6E4
          E4E7E5E5E9E7E7E9E8E8EEECEAFAF7F5FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCF3F0F0E8E8EA
          E7E7EAE6E6EAE6E6E9E5E5E9E4E5E9E4E5E9E5E5E9E5E5E9E7E7E9E9E9EAEBEA
          EAE8E9EBDAE1F1DAE2F5E1E7F6DEE2F2D6D7EDD7DAEED9E1F2DCE8F5DDE9F6DA
          E5F4D6DDF1DAE0EEDADCE3DADEEBDEE7F4E4EDF8E9F0FAE7EEF8DFE7F5E2ECF6
          E6EFF8E1E8F4E0E3F1DCDEEDD5D4E8D4D3E6DCDCEAE2E3EDE0E3EDDAE2EAD8E7
          E8D8E6E8DDE9EBDFEBEEDFEDEFDEE7ECE7E8E9E8E5E6E7E4E4E6E4E4E6E4E4E6
          E5E5E7E5E5E8E6E6E9E7E7EBEAE8F6F3F0FFFEFDFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFC
          FBF0EEEEE8E7E9E7E7EAE6E6E9E6E6E9E5E5E9E5E5E9E4E5E9E5E5E9E5E5E9E6
          E6E9E8E7EAE9E9EAE9E9EBE5E7EEE1E8F5E2EBF9E8ECF9E3E9F8EBF2FCF0F6FD
          F0F6FDF1F4FCE9EBF8ECEDF8E4E7F3E0E3F4E1E4F5EBEFFAEEF3FCE5EAF8E7EB
          F9EDF3FDEBF3FCE6ECF8DFE2F1E3E3F1DDE0F0D6DBEAD8DFE8DFE6EEE0E8EFE4
          E9EDE8ECECE8EBEBE8EBEAE6E9EAE1E6E8E1E3E7E6E4E5E6E5E5E6E4E5E6E5E5
          E6E5E5E7E6E6E7E6E6E9E7E7EAE9E8F2EFEDFEFCFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFDFBF9EFEDEDE7E7E9E6E6E9E6E6E9E5E6E9E5E5E9E5E5E9E4E5E9
          E5E5E9E5E5E9E5E5E9E6E6E9E7E7EAE9E8E9E8E8EBE7EAF2E7EDF6ECEFF4ECEE
          F2E7E8F0E8E8EEEAEAEDECEAECECEBEBE9E9EDE8EAF1E7E8EDE7E9ECE8EAEDE6
          E9EFEDEEF2EBEFF4EAEEF3EAEDF1E7E9EEEAEBEFE9EAEFE3E9ECDFE8EAE3E8E9
          E8E9E9EBE9E9EBE9E9EAE8E9E8E7E8E7E5E7E7E5E6E7E5E6E6E5E6E6E5E6E6E5
          E6E6E5E6E7E6E6E8E7E7E9E7E7EAE9E8F0EEECFCFAF8FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFCFBF9F0EEEEE7E7E9E6E6E9E6E6E9E5E6E9E5E5
          E9E5E5E9E4E5E9E4E5E9E4E5E9E5E5E9E5E5E9E5E6E9E6E6E9E7E7E9E8E8EAE9
          E8E9E8E8E8E9E8E9E9E8E9E9E8E9E9E8E9E9E8E9EAE8E9EAE8E8EAE8E9E9E8E9
          E9E8E8E9E8E8E9E8E8E9E8E8E9E8E8E9E8E8EAE8E8E9E8E8EAE8E8E9E8E9EAE9
          E9EAE8E9E9E8E9E9E8E9E7E6E8E6E5E8E5E5E7E5E5E6E5E5E6E5E5E6E6E5E6E6
          E5E6E6E6E7E7E6E7E8E7E7E9E8E8E9E9E8F0EDECFBF9F7FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCFBF2F0EFE8E8EAE6E6E9E6
          E6E9E5E6E9E5E6E9E5E6E9E5E5E9E5E5E9E4E5E9E5E5E9E5E5E9E5E6E9E6E6E9
          E6E6E9E7E7E9E7E7E9E7E7E9E8E7E9E8E7E9E8E7E9E8E7E9E8E7E9E8E7E9E8E7
          E9E8E7E8E9E7E9E8E7E9E8E7E9E8E7E8E8E7E8E8E7E9E8E8E9E8E8E9E8E7E9E8
          E7E9E7E7E9E7E6E8E6E6E8E6E6E8E5E5E8E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7
          E6E6E7E7E6E7E7E7E8E8E7E8E8E7E8EAE9E8F0EEECFBF8F7FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCF6F3F2
          EAEAEBE6E7EAE6E6E9E6E6E9E5E6EAE5E6E9E5E6E9E5E6E9E5E5E9E5E5E9E5E5
          E9E5E5E9E5E5E9E5E6E9E5E6E9E6E6E9E5E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6
          E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E5E6E8E5E6E8E5E6E8E6E6E9E6E6E8
          E5E6E8E6E6E8E5E5E8E5E5E8E5E5E8E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5
          E7E6E6E7E6E6E8E7E7E8E8E7E8E8E8E8EAE9E9F2F0EEFDFBFAFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFEFAF8F7EFEDEEE8E8EAE6E7EAE6E6EAE5E6EAE5E6EAE5E6E9E5E6E9E5
          E6E9E5E5E9E5E5E9E5E5E9E5E5E9E5E5E9E5E6E9E5E6E9E5E6E9E5E6E8E5E5E8
          E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E4E5E8E5E5E8E5E5
          E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E5E5E7E6
          E6E7E6E6E8E7E6E8E7E7E8E8E7E8E9E8E8EDEBEBF6F3F1FEFDFCFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFEFDFCF5F3F2ECEBECE7E7EAE6E7EAE6E7EAE6E6EA
          E6E6EAE5E6E9E5E6E9E5E6E9E5E5E9E5E5E9E5E5E9E5E5E9E5E5E9E5E5E8E5E5
          E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5
          E6E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E6E6E8E6E6E8E6E6E8
          E6E6E8E7E6E8E7E7E8E7E7E8E8E7E8EBEAE9F2EFEEFBF8F7FFFEFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCFAF9F2F0F0EBEAEBE7E7
          EAE6E7EAE6E7EAE6E7EAE6E6E9E6E6EAE5E6E9E5E6E9E5E6E9E5E6E9E5E5E9E5
          E5E9E5E5E9E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8E5E5E8
          E5E6E8E5E5E8E5E6E8E5E6E8E5E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6
          E8E7E6E8E7E6E8E7E6E8E7E7E8EAE9E9F0EEECF8F6F5FEFDFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFB
          FAF9F3F1F1ECEBECE8E8EAE6E7EAE6E7EAE6E6EAE6E6EAE6E6EAE6E6E9E5E6E9
          E5E6E9E5E5E9E5E5E9E5E5E9E5E5E9E5E5E8E5E5E8E5E5E8E5E6E9E5E6E9E5E6
          E9E5E6E9E5E6E9E5E6E9E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E8E6E6E9E7
          E6E8E7E7E8E7E7E8E8E7E9EBEAEAF0EEEDF9F6F5FEFDFCFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFCFBFAF7F4F3F0EEEEEBEAEBE8E8EAE6E7EAE6E6E9E6E6
          E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E5E6E9E5E6E9E5E6E9E6
          E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E7E9E6E7E9E6E7E9E7E7E9E6E7E9
          E7E7E9E8E7E9EAE9EAEDECECF3F0EFFAF7F6FEFDFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFCFBF9F8F6F4F3F1EFEEEC
          ECECEAEAEAE8E8EAE7E7E9E6E7E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9
          E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E6E9E6E7E9E6E7E9E7E7E9E8E7E9E9E8
          E9EBEAEBEFEDEDF3F1EFF8F6F4FDFBFAFEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
          FEFDFDFDFBFAFAF7F6F6F4F2F2F0EFF0EEEEEDECECEBEAEBEAEAEBE9E9EAE9E8
          E9E9E8E9E9E8E9E9E8E9E9E8E9E9E9EAEAE9EAEBEAEBECEBEBEEEDECF0EEEEF4
          F1F0F7F5F4FBF9F8FDFCFBFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFDFDFCFBFAFDFBFBFAF8F7F9
          F7F6F8F7F6F6F5F4F6F4F3F6F4F4F8F6F5F8F7F6F9F7F6FBF9F9FCFBFAFCFBFA
          FEFDFDFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF91BDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5DA9FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE172}
      end
      object Panel13: TPanel
        Left = 1
        Top = 1
        Width = 378
        Height = 136
        Align = alTop
        TabOrder = 1
        object LbPanel: TAdvSmoothLabel
          Left = 1
          Top = 1
          Width = 376
          Height = 55
          Fill.Color = clNone
          Fill.ColorTo = clNone
          Fill.ColorMirror = clNone
          Fill.ColorMirrorTo = clNone
          Fill.GradientType = gtVertical
          Fill.GradientMirrorType = gtSolid
          Fill.BorderColor = clNone
          Fill.Rounding = 10
          Fill.ShadowOffset = 0
          Fill.Glow = gmNone
          Caption.Text = 'SOFTZ SOLUTIONS'
          Caption.Font.Charset = DEFAULT_CHARSET
          Caption.Font.Color = clWindowText
          Caption.Font.Height = -32
          Caption.Font.Name = 'Tahoma'
          Caption.Font.Style = [fsBold]
          Caption.ColorStart = clSkyBlue
          Caption.ColorEnd = clBlue
          CaptionShadow.Text = 'SOFTZ SOLUTIONS'
          CaptionShadow.Font.Charset = DEFAULT_CHARSET
          CaptionShadow.Font.Color = clWindowText
          CaptionShadow.Font.Height = -27
          CaptionShadow.Font.Name = 'Tahoma'
          CaptionShadow.Font.Style = []
          Version = '1.6.1.0'
          Transparent = False
          Align = alTop
          ExplicitLeft = -15
          ExplicitTop = -23
          ExplicitWidth = 347
        end
        object RzPanel1: TRzPanel
          Left = 1
          Top = 56
          Width = 376
          Height = 77
          Align = alTop
          BorderOuter = fsNone
          BorderSides = [sdLeft, sdTop, sdBottom]
          TabOrder = 0
          Transparent = True
          DesignSize = (
            376
            77)
          object LbTable: TAdvSmoothLabel
            Left = 0
            Top = 0
            Width = 376
            Height = 37
            Fill.Color = clTeal
            Fill.ColorTo = clTeal
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtSolid
            Fill.BorderColor = clNone
            Fill.Rounding = 0
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Caption.Text = '  B'#224'n:'
            Caption.Location = plCenterLeft
            Caption.Font.Charset = DEFAULT_CHARSET
            Caption.Font.Color = clWindowText
            Caption.Font.Height = -27
            Caption.Font.Name = 'Tahoma'
            Caption.Font.Style = [fsBold]
            Caption.ColorStart = clWhite
            Caption.ColorEnd = clWhite
            CaptionShadow.Text = 'AdvSmoothLabel'
            CaptionShadow.Font.Charset = DEFAULT_CHARSET
            CaptionShadow.Font.Color = clWindowText
            CaptionShadow.Font.Height = -27
            CaptionShadow.Font.Name = 'Tahoma'
            CaptionShadow.Font.Style = []
            Version = '1.6.1.0'
            Transparent = False
            TextRendering = tClearType
            Align = alTop
            ExplicitLeft = -3
            ExplicitTop = -6
            ExplicitWidth = 351
          end
          object EdTable: TDBAdvSmoothLabel
            Left = 84
            Top = 0
            Width = 257
            Height = 33
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtSolid
            Fill.BorderColor = clNone
            Fill.Rounding = 0
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Caption.Text = 'EdTable'
            Caption.Location = plCenterLeft
            Caption.Font.Charset = DEFAULT_CHARSET
            Caption.Font.Color = clWindowText
            Caption.Font.Height = -27
            Caption.Font.Name = 'Tahoma'
            Caption.Font.Style = [fsBold]
            Caption.ColorStart = clWhite
            Caption.ColorEnd = clWhite
            CaptionShadow.Text = 'EdTable'
            CaptionShadow.Font.Charset = DEFAULT_CHARSET
            CaptionShadow.Font.Color = clWindowText
            CaptionShadow.Font.Height = -27
            CaptionShadow.Font.Name = 'Tahoma'
            CaptionShadow.Font.Style = []
            Version = '1.6.1.0'
            Anchors = [akLeft, akTop, akRight]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 228
          end
          object DBAdvSmoothLabel1: TDBAdvSmoothLabel
            Left = 32
            Top = 37
            Width = 344
            Height = 40
            Fill.Color = clNone
            Fill.ColorTo = clNone
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtSolid
            Fill.BorderColor = 3355443
            Fill.Rounding = 0
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Caption.Text = 'DBAdvSmoothLabel1'
            Caption.Font.Charset = DEFAULT_CHARSET
            Caption.Font.Color = clWindowText
            Caption.Font.Height = -35
            Caption.Font.Name = 'Tahoma'
            Caption.Font.Style = [fsBold]
            Caption.ColorStart = clRed
            Caption.ColorEnd = clRed
            CaptionShadow.Text = 'DBAdvSmoothLabel1'
            CaptionShadow.Font.Charset = DEFAULT_CHARSET
            CaptionShadow.Font.Color = clWindowText
            CaptionShadow.Font.Height = -27
            CaptionShadow.Font.Name = 'Tahoma'
            CaptionShadow.Font.Style = []
            Version = '1.6.1.0'
            Align = alClient
            DataField = 'THUCTRA'
            DataSource = DsBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ExplicitLeft = 2
            ExplicitTop = 34
          end
          object lbDbState: TAdvSmoothLabel
            Left = 0
            Top = 6
            Width = 20
            Height = 22
            Fill.Color = clTeal
            Fill.ColorTo = clTeal
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtSolid
            Fill.BorderColor = clNone
            Fill.Rounding = 0
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            Caption.Text = '*'
            Caption.Font.Charset = DEFAULT_CHARSET
            Caption.Font.Color = clWindowText
            Caption.Font.Height = -27
            Caption.Font.Name = 'Tahoma'
            Caption.Font.Style = [fsBold]
            Caption.ColorStart = clRed
            Caption.ColorEnd = clRed
            CaptionShadow.Text = '*'
            CaptionShadow.Font.Charset = DEFAULT_CHARSET
            CaptionShadow.Font.Color = clWindowText
            CaptionShadow.Font.Height = -27
            CaptionShadow.Font.Name = 'Tahoma'
            CaptionShadow.Font.Style = []
            Version = '1.6.1.0'
            Transparent = False
            TextRendering = tClearType
            Align = alCustom
          end
        end
      end
      object Panel14: TPanel
        Left = 1
        Top = 537
        Width = 378
        Height = 204
        Align = alBottom
        TabOrder = 2
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 215
          Height = 202
          Align = alClient
          TabOrder = 0
          object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
            Left = 1
            Top = 1
            Width = 213
            Height = 200
            AllowAutoZoom = False
            AutoCompletion.Font.Charset = DEFAULT_CHARSET
            AutoCompletion.Font.Color = clWhite
            AutoCompletion.Font.Height = -19
            AutoCompletion.Font.Name = 'Tahoma'
            AutoCompletion.Font.Style = []
            AutoCompletion.Color = clBlack
            Fill.Color = clNone
            Fill.ColorTo = clNone
            Fill.ColorMirror = clNone
            Fill.ColorMirrorTo = clNone
            Fill.GradientType = gtVertical
            Fill.GradientMirrorType = gtSolid
            Fill.BorderColor = clNone
            Fill.Rounding = 0
            Fill.ShadowColor = clNone
            Fill.ShadowOffset = 0
            Fill.Glow = gmNone
            HighlightCaps = clWhite
            HighlightAltGr = clWhite
            KeyboardType = ktNUMERIC
            Keys = <
              item
                Caption = 'Back Space'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skBackSpace
                Color = 10526880
                X = 1
                Y = 2
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '/'
                KeyValue = 111
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skDivide
                X = 53
                Y = 2
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '*'
                KeyValue = 106
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skMultiply
                X = 105
                Y = 2
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '-'
                KeyValue = 109
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skSubstract
                X = 157
                Y = 2
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '7'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 1
                Y = 42
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '8'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 53
                Y = 42
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '9'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 105
                Y = 42
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '+'
                KeyValue = 107
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skAdd
                X = 157
                Y = 42
                Height = 80
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '4'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 1
                Y = 82
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '5'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 53
                Y = 82
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '6'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 105
                Y = 82
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '1'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 1
                Y = 122
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '2'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 53
                Y = 122
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '3'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 105
                Y = 122
                Width = 52
                SubKeys = <>
              end
              item
                Caption = 'Enter'
                KeyValue = 13
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skReturn
                Color = 10526880
                X = 157
                Y = 122
                Height = 80
                Width = 52
                SubKeys = <>
              end
              item
                Caption = '0'
                KeyValue = -1
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skNone
                X = 1
                Y = 162
                Width = 104
                SubKeys = <>
              end
              item
                Caption = '.'
                KeyValue = 110
                ShiftKeyValue = -1
                AltGrKeyValue = -1
                SpecialKey = skDecimal
                X = 105
                Y = 162
                Width = 52
                SubKeys = <>
              end>
            SmallFont.Charset = DEFAULT_CHARSET
            SmallFont.Color = clWindowText
            SmallFont.Height = -16
            SmallFont.Name = 'Tahoma'
            SmallFont.Style = []
            Version = '1.8.5.2'
            OnKeyClick = AdvSmoothTouchKeyBoard1KeyClick
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = []
          end
        end
        object Panel3: TPanel
          Left = 216
          Top = 1
          Width = 161
          Height = 202
          Align = alRight
          TabOrder = 1
          object Panel16: TPanel
            Left = 1
            Top = 67
            Width = 159
            Height = 67
            Align = alBottom
            BevelWidth = 2
            TabOrder = 0
            object AdvGlowButton1: TAdvGlowButton
              Left = 2
              Top = 2
              Width = 155
              Height = 63
              Cursor = 1
              Align = alClient
              Action = CmdPayment
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = []
              NotesFont.Charset = DEFAULT_CHARSET
              NotesFont.Color = clWindowText
              NotesFont.Height = -11
              NotesFont.Name = 'Tahoma'
              NotesFont.Style = []
              ParentFont = False
              Picture.Data = {
                89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
                F4000000097048597300000B1300000B1301009A9C18000001E849444154789C
                6360180504C0FF7A7B96A75D16D64FBA2C4389C18F3BCC839EB499AA315003BC
                EEB4E27DDC6971EA71A7E57F52F0A30ECB7F8F3B2C1B2976C0C30EF36A9081CF
                A6F9FE7FB3A69038BC2AF7FF935E3B88233ACD552972C0E30ECB1520077C3DBB
                F4FFFFCF0F88C66F56E54042A3C33C8822073CEAB05C0532E8DB851518965C3D
                B3F7FF811DABFFBFBA7F0ED3016B0AC10E00A5099A39C0DDDDEDBF9393D3FFD6
                C6AA81718076B5D17FE30ECBFF167D364461AA3B206C59ECFFA26D25FF1BF735
                63C559EB7369EB80D09531FFDB8E75FE5F7A6D39563CEBDCECFFF6939D68E700
                ED6AE3FF76935DFEE76D2AFCBFE4EA321C8E98030F099A38C0769233D8709023
                708504283A68EE00EB89F6FFE75D5C00B670C28949FF1357A6FEAFDE5D473F07
                58F4D9FC6F3F04490F6ED33DE1627DC7270E80036678C1C5FA8F4FA25F14CCBF
                B4101A0593FF27AD4AFB5FB3AB9E7E5190BFA9686012A1DD6417B0E5B8B3E16C
                DA65C3D0812E88C206BA28D61EE8CAC87DA0ABE3ABF46C907C25B3494635073C
                23A9519A036E9452C5018F3B2D96E36A7A5F6D34FBBFBFD8E0FF83760BDC4D74
                4A1BA58F3B2DAA70190EB27C598AD6FF2B0D6638FB068FDA2C542872C0CB7A7B
                9EC79D9627B15900F2395ECB3B2CEA19A8D5357BD26169454AD78C629F338C24
                00005FDE7A4E45AF38B30000000049454E44AE426082}
              Position = bpLeft
              TabOrder = 0
              Appearance.Color = cl3DLight
              Appearance.ColorTo = clScrollBar
              Appearance.ColorChecked = cl3DLight
              Appearance.ColorCheckedTo = cl3DLight
              Appearance.ColorDisabled = clMenuBar
              Appearance.ColorDisabledTo = clMenuBar
              Appearance.ColorDown = cl3DLight
              Appearance.ColorDownTo = cl3DLight
              Appearance.ColorHot = cl3DLight
              Appearance.ColorHotTo = cl3DLight
              Appearance.ColorMirror = cl3DLight
              Appearance.ColorMirrorTo = clScrollBar
              Appearance.ColorMirrorHot = cl3DLight
              Appearance.ColorMirrorHotTo = cl3DLight
              Appearance.ColorMirrorDown = cl3DLight
              Appearance.ColorMirrorDownTo = cl3DLight
              Appearance.ColorMirrorChecked = cl3DLight
              Appearance.ColorMirrorCheckedTo = cl3DLight
              Appearance.ColorMirrorDisabled = clMenuBar
              Appearance.ColorMirrorDisabledTo = clMenuBar
              Layout = blGlyphLeftAdjusted
            end
          end
          object Panel17: TPanel
            Left = 1
            Top = 134
            Width = 159
            Height = 67
            Align = alBottom
            BevelWidth = 2
            TabOrder = 1
            object AdvGlowButton5: TAdvGlowButton
              Left = 2
              Top = 2
              Width = 155
              Height = 63
              Cursor = 1
              Align = alClient
              Action = CmdClose
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = []
              NotesFont.Charset = DEFAULT_CHARSET
              NotesFont.Color = clWindowText
              NotesFont.Height = -11
              NotesFont.Name = 'Tahoma'
              NotesFont.Style = []
              ParentFont = False
              Picture.Data = {
                89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
                F4000000097048597300000B1300000B1301009A9C180000017C49444154789C
                ED96CF4AC34010C6F352BB17457117AC104F22BD554A0BA22888076FD6835510
                C18B67057D0CF1A87705135F40ACD83F5EBA6B36923AC2A6C5C426D9B4D91C84
                0CCC6D99EFB7DFCC2C6B18451411118CE21AA3C8EE13E4318A214BFA3590CD08
                AAA61327B89E553436D34030829E7303A0D85202F435D89ED40EB50334B7DBCB
                9C1A80974D70CFCF9402CEEE26B0D28C5E005E36616059009D0FF8BABE8C2D2E
                9AFB00ED2E78B737C04AB3FA009CED3AC0EB9B0490105717E3E2470D292ECFB4
                BBBE133A5BE06CD5E0FBA5F50B11706274735FBC07EEC9A1FE19603110938867
                06601110D0E9A516D702C08610D07A0F41A411D70620A4EDC39B47CC44AE00E2
                4FCF834EA481C8042022062E693BB4028884699F04622A00D1D80B3D32E2F860
                7C3077D6438F957BDAD407C0571661F0F0A85CB59113DEFD1DF0E579BD2DE0AB
                4BD209D5907D6E54809B73F9AE21CB90FFE14382ECDC1C20F8490D4050353780
                055C510204202C7DDF726C318AD764F1228A30C2F103D2E0D26991A4E0990000
                000049454E44AE426082}
              Position = bpLeft
              TabOrder = 0
              Appearance.Color = cl3DLight
              Appearance.ColorTo = clScrollBar
              Appearance.ColorChecked = cl3DLight
              Appearance.ColorCheckedTo = cl3DLight
              Appearance.ColorDisabled = clMenuBar
              Appearance.ColorDisabledTo = clMenuBar
              Appearance.ColorDown = cl3DLight
              Appearance.ColorDownTo = cl3DLight
              Appearance.ColorHot = cl3DLight
              Appearance.ColorHotTo = cl3DLight
              Appearance.ColorMirror = cl3DLight
              Appearance.ColorMirrorTo = clScrollBar
              Appearance.ColorMirrorHot = cl3DLight
              Appearance.ColorMirrorHotTo = cl3DLight
              Appearance.ColorMirrorDown = cl3DLight
              Appearance.ColorMirrorDownTo = cl3DLight
              Appearance.ColorMirrorChecked = cl3DLight
              Appearance.ColorMirrorCheckedTo = cl3DLight
              Appearance.ColorMirrorDisabled = clMenuBar
              Appearance.ColorMirrorDisabledTo = clMenuBar
              Layout = blGlyphLeftAdjusted
            end
          end
          object Panel18: TPanel
            Left = 1
            Top = 0
            Width = 159
            Height = 67
            Align = alBottom
            BevelWidth = 2
            TabOrder = 2
            object AdvGlowButton7: TAdvGlowButton
              Left = 2
              Top = 2
              Width = 155
              Height = 63
              Cursor = 1
              Align = alClient
              Action = CmdCalTemp
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = []
              NotesFont.Charset = DEFAULT_CHARSET
              NotesFont.Color = clWindowText
              NotesFont.Height = -11
              NotesFont.Name = 'Tahoma'
              NotesFont.Style = []
              ParentFont = False
              Picture.Data = {
                89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
                F4000000097048597300000B1300000B1301009A9C180000020E49444154789C
                ED95CB4BDC5014C6F357B8485C7467E94AB43B77EEA4538D05575D75A1D0855D
                B912DA5597EDA6E06832492D74535FD0FA00C107820BF101D645CD646292B9A3
                93143BA2C5CEB48C99F9CADC91383AA5A2991B413CF02D7293707E9CF39D7339
                EE2E2E894E1775A283279D2EBAAEA2D37FEAB8A0213A888B0E50A98895C763E2
                A175E3D0579BFEFBDC37257538D06A01808BC91FC4BEA2653E0DFEE582AFFBF2
                66154049350710AFA8C000829A4210DD01702C5BC00FEA5437D3022589B72B69
                BC59494388D921022804F5433A9E7E36B14C7E626DEF18AF9652888C1810E41D
                F6000D92864DF717FE154DC33A638098858F5BFB34D9F68F2CFAE6085ECCDA78
                B7EA209EC9A151D5D802F083097CDBCF5280DE599BB6A30C66D377826C310690
                4CCC99871460C33946F37B8D9E8537050A41D7B801AF58A410278522BEE80778
                F429015EDA09690A6413E258025BDFCF1B7142CBE09EC4DA84EA59254A3D6F1F
                3530BE9D41A15C10BA172EEE84DA03C42CBA842A7DF17A79D7F7053F64B01C43
                1BFD0B49F42FA6F0F0830E3E1A8710D5105D7729C052F2A8CA943506B030A91F
                F87DCFE50BC8E60BFEF3B349F36C34594D4164C4A01099DC899F981CFDC1F319
                8B1A3494BB40904D5AFE52E907D6DD722B2A7C11D26D48D0336DA27BAABAECE1
                00A86553FEEF2A660FA05EAE5B00A010EFDA000AF10203F02A91AF05A1108F57
                52526000EEB6C75FD736E1F482504BB30000000049454E44AE426082}
              TabOrder = 0
              Appearance.Color = cl3DLight
              Appearance.ColorTo = clScrollBar
              Appearance.ColorChecked = cl3DLight
              Appearance.ColorCheckedTo = cl3DLight
              Appearance.ColorDisabled = clMenuBar
              Appearance.ColorDisabledTo = clMenuBar
              Appearance.ColorDown = cl3DLight
              Appearance.ColorDownTo = cl3DLight
              Appearance.ColorHot = cl3DLight
              Appearance.ColorHotTo = cl3DLight
              Appearance.ColorMirror = cl3DLight
              Appearance.ColorMirrorTo = clScrollBar
              Appearance.ColorMirrorHot = cl3DLight
              Appearance.ColorMirrorHotTo = cl3DLight
              Appearance.ColorMirrorDown = cl3DLight
              Appearance.ColorMirrorDownTo = cl3DLight
              Appearance.ColorMirrorChecked = cl3DLight
              Appearance.ColorMirrorCheckedTo = cl3DLight
              Appearance.ColorMirrorDisabled = clMenuBar
              Appearance.ColorMirrorDisabledTo = clMenuBar
              Layout = blGlyphLeftAdjusted
            end
          end
        end
      end
      object Panel22: TPanel
        Left = 1
        Top = 502
        Width = 378
        Height = 35
        Align = alBottom
        TabOrder = 3
        object isPanel1: TisPanel
          Left = 2
          Top = -1
          Width = 128
          Height = 36
          ParentCustomHint = False
          BiDiMode = bdLeftToRight
          Color = 16119285
          Ctl3D = False
          DoubleBuffered = False
          FullRepaint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentBackground = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowCaption = False
          ShowHint = False
          TabOrder = 0
          HeaderCaption = 'Chuy'#7875'n ch'#7871' bi'#7871'n'
          HeaderColor = clBtnFace
          ImageSet = 4
          RealHeight = 41
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clGrayText
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = []
          object DBText4: TDBText
            Left = 1
            Top = 17
            Width = 126
            Height = 18
            ParentCustomHint = False
            Align = alClient
            Alignment = taCenter
            BiDiMode = bdLeftToRight
            Color = clBtnFace
            DataField = 'TEXT_THOIGIAN_CHUYENCHEBIEN'
            DataSource = DsCTBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGrayText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            ExplicitWidth = 47
            ExplicitHeight = 16
          end
        end
        object isPanel3: TisPanel
          Left = 256
          Top = -1
          Width = 120
          Height = 36
          ParentCustomHint = False
          BiDiMode = bdLeftToRight
          Color = 16119285
          Ctl3D = False
          DoubleBuffered = False
          FullRepaint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentBackground = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowCaption = False
          ShowHint = False
          TabOrder = 1
          HeaderCaption = 'C'#242'n l'#7841'i'
          HeaderColor = clBtnFace
          ImageSet = 4
          RealHeight = 41
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clGrayText
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = []
          object DBText5: TDBText
            Left = 1
            Top = 17
            Width = 118
            Height = 18
            ParentCustomHint = False
            Align = alClient
            Alignment = taCenter
            BiDiMode = bdLeftToRight
            Color = clBtnFace
            DataField = 'TEXT_THOIGIAN_CONLAI'
            DataSource = DsCTBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGrayText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            ExplicitLeft = 48
            ExplicitTop = 15
            ExplicitWidth = 65
            ExplicitHeight = 17
          end
        end
        object isPanel2: TisPanel
          Left = 133
          Top = -1
          Width = 120
          Height = 36
          ParentCustomHint = False
          BiDiMode = bdLeftToRight
          Color = 16119285
          Ctl3D = False
          DoubleBuffered = False
          FullRepaint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentBackground = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowCaption = False
          ShowHint = False
          TabOrder = 2
          HeaderCaption = 'Ch'#7871' bi'#7871'n'
          HeaderColor = clBtnFace
          ImageSet = 4
          RealHeight = 41
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clGrayText
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = []
          object DBText1: TDBText
            Left = 1
            Top = 17
            Width = 118
            Height = 18
            ParentCustomHint = False
            Align = alClient
            Alignment = taCenter
            BiDiMode = bdLeftToRight
            Color = clBtnFace
            DataField = 'TEXT_THOIGIAN_CHEBIEN'
            DataSource = DsCTBH
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGrayText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            Transparent = True
            ShowHint = False
            ExplicitLeft = 48
            ExplicitTop = 15
            ExplicitWidth = 65
            ExplicitHeight = 17
          end
        end
      end
    end
  end
  object MyActionList: TActionList
    OnUpdate = MyActionListUpdate
    Left = 452
    Top = 232
    object CmdTableOrders: TAction
      Caption = #272#7863't b'#224'n'
      OnExecute = CmdTableOrdersExecute
    end
    object CmdOrders: TAction
      Caption = #272#417'n h'#224'ng'
    end
    object CmdChangeTable: TAction
      Caption = 'Chuy'#7875'n b'#224'n'
      OnExecute = CmdChangeTableExecute
    end
    object CmdMergeTable: TAction
      Caption = 'G'#7897'p b'#224'n'
      OnExecute = CmdMergeTableExecute
    end
    object CmdSplitTable: TAction
      Caption = 'T'#225'ch b'#224'n'
      OnExecute = CmdSplitTableExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      Caption = 'T'#7843'i l'#7841'i'
      OnExecute = CmdRefreshExecute
    end
    object CmdSendOrder: TAction
      Caption = 'B'#225'o ch'#7871' bi'#7871'n'
      OnExecute = CmdSendOrderExecute
    end
    object CmdPayment: TAction
      Caption = 'Thanh to'#225'n'
      OnExecute = CmdPaymentExecute
    end
    object CmdSelect: TAction
      Caption = 'Ch'#7885'n b'#224'n'
      OnExecute = CmdSelectExecute
    end
    object CmdWinKeyboard: TAction
      Caption = 'Screen Keyboard'
      OnExecute = CmdWinKeyboardExecute
    end
    object CmdCustomer: TAction
      Caption = #272#7893'i s'#7889' kh'#225'ch'
      OnExecute = CmdCustomerExecute
    end
    object CmdShowImage: TAction
      Caption = 'H'#236'nh '#7843'nh'
      OnExecute = CmdShowImageExecute
    end
    object CmdRefreshMenu: TAction
      Caption = 'T'#7843'i l'#7841'i'
      OnExecute = CmdRefreshMenuExecute
    end
    object CmdMore: TAction
      Caption = '...'
      OnExecute = CmdMoreExecute
    end
    object CmdPrintMore: TAction
      Caption = 'H'#243'a '#273#417'n'
      OnExecute = CmdPrintMoreExecute
    end
    object CmdCalTemp: TAction
      Caption = 'T'#7841'm t'#237'nh'
      OnExecute = CmdCalTempExecute
    end
    object CmdListTableOrders: TAction
      Caption = 'Danh s'#225'ch '#273#7863't b'#224'n'
      OnExecute = CmdListTableOrdersExecute
    end
    object CmdAreaPrev: TAction
      Caption = '  '
      OnExecute = CmdAreaPrevExecute
    end
    object CmdAreaNext: TAction
      Caption = '  '
      OnExecute = CmdAreaNextExecute
    end
    object CmdAreaTablePrev: TAction
      Caption = '  '
      OnExecute = CmdAreaTablePrevExecute
    end
    object CmdAreaTableNext: TAction
      Caption = '  '
      OnExecute = CmdAreaTableNextExecute
    end
    object CmdGroupMenuNext: TAction
      Caption = '  '
      OnExecute = CmdGroupMenuNextExecute
    end
    object CmdGroupMenuPrev: TAction
      Caption = '  '
      OnExecute = CmdGroupMenuPrevExecute
    end
    object CmdMenuNext: TAction
      Caption = ' '
      OnExecute = CmdMenuNextExecute
    end
    object CmdMenuPrev: TAction
      Caption = ' '
      OnExecute = CmdMenuPrevExecute
    end
    object CmdEndOfShift: TAction
      Caption = 'K'#7871't ca'
      OnExecute = CmdEndOfShiftExecute
    end
    object CmdScanVIP: TAction
      Caption = 'VIP'
      Hint = 'Nh'#7853'p th'#7867' VIP'
      OnExecute = CmdScanVIPExecute
    end
    object CmdBillList: TAction
      Caption = 'H'#243'a '#273#417'n'
      OnExecute = CmdBillListExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 420
    Top = 232
  end
  object QrDmKhuVuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from FB_DM_BAN_KHUVUC_CASHIER'
      '  order by SORT')
    Left = 464
    Top = 308
  end
  object AdvSmoothTileListHTMLVisualizer1: TAdvSmoothTileListHTMLVisualizer
    Left = 184
    Top = 344
  end
  object QrNhomCamUng: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from FB_DM_CAMUNG_CASHIER'
      'order by SORT')
    Left = 552
    Top = 324
  end
  object QrCamUng: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'Vw_FB_DM_HH'
      'where '#9'isnull(CAMUNG_NHOM, '#39#39') <> '#39#39
      'order by  TENVT')
    Left = 540
    Top = 372
  end
  object QrBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrBHBeforeOpen
    AfterInsert = QrBHAfterInsert
    BeforePost = QrBHBeforePost
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM'
      ' where'#9'KHOA =:KHOA')
    Left = 572
    Top = 260
    object QrBHCALC_TONGCK: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TONGCK'
      Calculated = True
    end
    object QrBHCALC_TIENTHOI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TIENTHOI'
      Calculated = True
    end
    object QrBHLK_TENKHO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 200
      Lookup = True
    end
    object QrBHLCT: TWideStringField
      FieldName = 'LCT'
      Size = 5
    end
    object QrBHNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrBHQUAY: TWideStringField
      FieldName = 'QUAY'
      Size = 2
    end
    object QrBHCA: TWideStringField
      FieldName = 'CA'
      Size = 1
    end
    object QrBHSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrBHMAVIP: TWideStringField
      FieldName = 'MAVIP'
      Size = 15
    end
    object QrBHMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 2
    end
    object QrBHCHIETKHAU_MH: TFloatField
      FieldName = 'CHIETKHAU_MH'
    end
    object QrBHSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrBHSOLUONG: TFloatField
      FieldName = 'SOLUONG'
    end
    object QrBHCHUATHOI: TFloatField
      FieldName = 'CHUATHOI'
    end
    object QrBHTHANHTOAN: TFloatField
      FieldName = 'THANHTOAN'
    end
    object QrBHDGIAI: TWideMemoField
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrBHPRINTED: TBooleanField
      FieldName = 'PRINTED'
    end
    object QrBHCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrBHCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrBHTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      Size = 2
    end
    object QrBHCN_TENDV: TWideStringField
      DisplayLabel = 'T'#234'n '#273#417'n v'#7883
      FieldName = 'CN_TENDV'
      Size = 200
    end
    object QrBHCN_MST: TWideStringField
      FieldName = 'CN_MST'
      Size = 50
    end
    object QrBHCN_DIACHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881' giao h'#224'ng'
      FieldName = 'CN_DIACHI'
      Size = 200
    end
    object QrBHCN_DIACHI_HD: TWideStringField
      FieldName = 'CN_DIACHI_HD'
      Size = 200
    end
    object QrBHCN_LIENHE: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i li'#234'n h'#7879
      FieldName = 'CN_LIENHE'
      Size = 200
    end
    object QrBHCN_DTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'CN_DTHOAI'
      Size = 50
    end
    object QrBHCN_EMAIL: TWideStringField
      FieldName = 'CN_EMAIL'
      Size = 100
    end
    object QrBHCN_MATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'CN_MATK'
      Size = 30
    end
    object QrBHLK_TENTK: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_DAIDIEN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DAIDIEN'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'DAIDIEN'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH'
      KeyFields = 'CN_MATK'
      Size = 200
      Lookup = True
    end
    object QrBHPRINT_NO: TIntegerField
      FieldName = 'PRINT_NO'
    end
    object QrBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
    object QrBHDELIVERY: TBooleanField
      FieldName = 'DELIVERY'
    end
    object QrBHTIENTHOI: TFloatField
      FieldName = 'TIENTHOI'
    end
    object QrBHLK_TENVIP: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVIP'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'HOTEN'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBHLK_VIP_DTHOAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_VIP_DTHOAI'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'DTHOAI'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBHLK_VIP_DCHI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_VIP_DCHI'
      LookupDataSet = DataMain.QrDMVIP
      LookupKeyFields = 'MAVIP'
      LookupResultField = 'DCHI'
      KeyFields = 'MAVIP'
      Size = 200
      Lookup = True
    end
    object QrBH_id: TLargeintField
      FieldName = '_id'
    end
    object QrBHMAQUAY: TWideStringField
      FieldName = 'MAQUAY'
    end
    object QrBHMABAN: TWideStringField
      FieldName = 'MABAN'
    end
    object QrBHSOLUONG_KHACH: TFloatField
      FieldName = 'SOLUONG_KHACH'
    end
    object QrBHLK_BAN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_BAN'
      LookupDataSet = spDmBan
      LookupKeyFields = 'MABAN'
      LookupResultField = 'TENBAN'
      KeyFields = 'MABAN'
      Lookup = True
    end
    object QrBHHINHTHUC_GIA: TWideStringField
      FieldName = 'HINHTHUC_GIA'
    end
    object QrBHUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrBHUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrBHCKHD_BY: TIntegerField
      FieldName = 'CKHD_BY'
    end
    object QrBHTL_CKVIPDM: TFloatField
      FieldName = 'TL_CKVIPDM'
    end
    object QrBHTL_CKHD: TFloatField
      FieldName = 'TL_CKHD'
    end
    object QrBHSOLUONG_DAT: TFloatField
      FieldName = 'SOLUONG_DAT'
    end
    object QrBHNGAYVAO: TDateTimeField
      FieldName = 'NGAYVAO'
    end
    object QrBHQUAY_ORDER: TWideStringField
      FieldName = 'QUAY_ORDER'
    end
    object QrBHMAQUAY_ORDER: TWideStringField
      FieldName = 'MAQUAY_ORDER'
    end
    object QrBHTL_PHUTHU: TFloatField
      FieldName = 'TL_PHUTHU'
    end
    object QrBHCO_HOADON: TBooleanField
      FieldName = 'CO_HOADON'
    end
    object QrBHCN_GHICHU: TWideStringField
      FieldName = 'CN_GHICHU'
      Size = 200
    end
    object QrBHMAVIP_HOTEN: TWideStringField
      FieldName = 'MAVIP_HOTEN'
      Size = 200
    end
    object QrBHORDER_BY: TIntegerField
      FieldName = 'ORDER_BY'
    end
    object QrBHORDER_DATE: TDateTimeField
      FieldName = 'ORDER_DATE'
    end
    object QrBHPHUTHU: TFloatField
      FieldName = 'PHUTHU'
    end
    object QrBHTHUCTRA: TFloatField
      FieldName = 'THUCTRA'
    end
  end
  object QrCTBH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrCTBHBeforeOpen
    AfterInsert = QrCTBHAfterInsert
    BeforePost = QrCTBHBeforePost
    AfterPost = QrCTBHAfterPost
    AfterDelete = QrCTBHAfterDelete
    OnCalcFields = QrCTBHCalcFields
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'FB_BANLE_TAM_ORDER'
      ' where'#9' KHOA =:KHOA'
      'order by KHOA, STT')
    Left = 604
    Top = 260
    object QrCTBHSTT: TIntegerField
      DisplayWidth = 3
      FieldName = 'STT'
    end
    object QrCTBHMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 17
      FieldName = 'MAVT'
      OnChange = QrCTBHMAVTChange
      Size = 15
    end
    object QrCTBHTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 200
    end
    object QrCTBHTENTAT: TWideStringField
      FieldName = 'TENTAT'
      Size = 200
    end
    object QrCTBHLK_TENVT: TWideStringField
      DisplayLabel = 'T'#234'n h'#224'ng'
      DisplayWidth = 37
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 50
      Lookup = True
    end
    object QrCTBHLK_TENVT_KHONGDAU: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT_KHONGDAU'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT_KHONGDAU'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTBHLK_TENTAT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTAT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENTAT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTBHDVT: TWideStringField
      DisplayLabel = #272'VT'
      DisplayWidth = 7
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TenDvt'
      KeyFields = 'MAVT'
      Size = 10
      Lookup = True
    end
    object QrCTBHSOLUONG: TFloatField
      DisplayLabel = 'S'#7889' l'#432#7907'ng'
      DisplayWidth = 8
      FieldName = 'SOLUONG'
      OnChange = QrCTBHSOLUONGChange
      OnValidate = QrCTBHSOLUONGValidate
    end
    object QrCTBHDONGIA: TFloatField
      DisplayLabel = #272#417'n gi'#225
      DisplayWidth = 12
      FieldName = 'DONGIA'
      Visible = False
    end
    object QrCTBHSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrCTBHSOTIENChange
    end
    object QrCTBHGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTBHRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTBHCALC_SOTIEN_SAUCK: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN_SAUCK'
      Calculated = True
    end
    object QrCTBHKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTBHKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTBHLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTBHBO: TBooleanField
      FieldName = 'BO'
    end
    object QrCTBHDVT2: TWideStringField
      FieldName = 'DVT'
    end
    object QrCTBHCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrCTBHUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrCTBHCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrCTBHUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrCTBHLK_BO: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_BO'
      LookupDataSet = QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'BO'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTBHORDER_NO: TIntegerField
      FieldName = 'ORDER_NO'
    end
    object QrCTBHSOLUONG_DAT: TFloatField
      FieldName = 'SOLUONG_DAT'
      OnChange = QrCTBHSOLUONG_DATChange
    end
    object QrCTBHMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 5
    end
    object QrCTBHTHOIGIAN_CHEBIEN: TFloatField
      FieldName = 'THOIGIAN_CHEBIEN'
    end
    object QrCTBHORDER_BY: TIntegerField
      FieldName = 'ORDER_BY'
    end
    object QrCTBHORDER_DATE: TDateTimeField
      FieldName = 'ORDER_DATE'
    end
    object QrCTBH_id: TLargeintField
      FieldName = '_id'
    end
    object QrCTBHCALC_THOIGIAN_CONLAI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_THOIGIAN_CONLAI'
      Calculated = True
    end
    object QrCTBHTEXT_THOIGIAN_CHUYENCHEBIEN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXT_THOIGIAN_CHUYENCHEBIEN'
      Calculated = True
    end
    object QrCTBHTEXT_THOIGIAN_CHEBIEN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXT_THOIGIAN_CHEBIEN'
      Calculated = True
    end
    object QrCTBHTEXT_THOIGIAN_CONLAI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXT_THOIGIAN_CONLAI'
      Calculated = True
    end
  end
  object DsCTBH: TDataSource
    DataSet = QrCTBH
    Left = 604
    Top = 288
  end
  object POS_GIABAN: TADOCommand
    CommandText = 'FB_POS_GIABAN;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@MAVIP'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@GIA'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@TL_CK7'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@TL_CK3'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@VAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@LTHUE'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end
      item
        Name = '@MABO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@TL_CK4'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@SL_BO'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@UPDATE_DATE'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 569
    Top = 210
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    CommandTimeout = 0
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.MAVT MACHUNG, a.MAVT MAVT, a.TENVT, a.TENVT_KHONGDAU, a' +
        '.TENTAT, a.MaDvt, b.DGIAI TenDvt, a.QuyDoi, a.MaDvtLon, c.DGIAI ' +
        ' TenDvtLon, a.BO, a.MADT,  a.TINHTRANG,'
      
        #9'a.GIABAN_CHUA_VAT GIANHAP, a.GIANHAP GIANHAPVAT, a.GIABAN_CHUA_' +
        'VAT GIABAN, a.GIABAN GIABANVAT,  0 GIASI, 0 GIASIVAT, a.TL_LAI, ' +
        'a.UPDATE_DATE,'
      #9'e.TENDT'
      '  from'#9'FB_DM_HH a '
      #9#9'left join DM_KH_NCC e on e.MADT = a.MAVT_NCC'
      #9#9'left join DM_DVT b on a.MaDvt = b.MA'
      #9#9'left join DM_DVT c on a.MaDvtLon = c.MA'
      'order by a.TENVT')
    Left = 324
    Top = 348
  end
  object GetUPDATE_DATE: TADOCommand
    CommandText = 'select isnull(max(UPDATE_DATE), 0) UPDATE_DATE from FB_DM_HH'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <>
    Left = 497
    Top = 214
  end
  object DsBH: TDataSource
    DataSet = QrBH
    Left = 572
    Top = 288
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrBH
    DetailDataSet = QrCTBH
    MasterFields.Strings = (
      'SOLUONG'
      'SOLUONG_GIAO'
      'SOTIEN')
    DetailFields.Strings = (
      'SOLUONG'
      'SOLUONG_GIAO'
      'SOTIEN')
    Left = 306
    Top = 248
  end
  object AdvSmoothTileListHTMLVisualizer2: TAdvSmoothTileListHTMLVisualizer
    Left = 208
    Top = 384
  end
  object spDmBan: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    ProcedureName = 'spW_Dm_Ban_Status;1'
    Parameters = <>
    Left = 182
    Top = 244
  end
  object isOneInstance1: TisOneInstance
    SwitchToPrevious = True
    Left = 344
    Top = 384
  end
  object spDmBanTemp: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    ProcedureName = 'spW_Dm_Ban_Status;1'
    Parameters = <>
    Left = 222
    Top = 308
  end
  object GET_ORDER_NO: TADOCommand
    CommandText = 'FB_ORDER_NO;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 441
    Top = 374
  end
  object FB_CASHIER_DELETE_ORDER: TADOCommand
    CommandText = 'spFB_CASHIER_DELETE_ORDER;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 377
    Top = 294
  end
  object POS_VIP: TADOCommand
    CommandText = 'POS_VIP;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MAVIP'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TEN'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@TLCK'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end>
    Left = 281
    Top = 298
  end
end
