object FrmPosKetca: TFrmPosKetca
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'K'#7871't Ca'
  ClientHeight = 584
  ClientWidth = 877
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  GlassFrame.Enabled = True
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PgMain: TPageControl
    Left = 0
    Top = 0
    Width = 622
    Height = 584
    Cursor = 1
    ActivePage = TabSheet2
    Align = alLeft
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    Style = tsButtons
    TabHeight = 41
    TabOrder = 0
    TabWidth = 167
    OnChange = PgMainChange
    object TabSheet1: TTabSheet
      Caption = 'K'#7871't Ca'
      object VertGridEhGroupCa: TDBVertGridEh
        Left = 0
        Top = 0
        Width = 614
        Height = 533
        Align = alClient
        AllowedOperations = []
        AllowedSelections = []
        RowCategories.Active = True
        RowCategories.CategoryProps = <
          item
            Name = 'Fields_Group_Ca'
            DisplayText = 'Ca'
            DefaultExpanded = True
          end
          item
            Name = 'Fields_Group_TienMat'
            DisplayText = 'Ti'#7873'n m'#7863't'
            DefaultExpanded = True
          end>
        RowCategories.Font.Charset = DEFAULT_CHARSET
        RowCategories.Font.Color = clWindow
        RowCategories.Font.Height = -16
        RowCategories.Font.Name = 'Tahoma'
        RowCategories.Font.Style = [fsBold]
        RowCategories.ParentFont = False
        LabelColParams.Color = clWhite
        LabelColParams.FillStyle = cfstThemedEh
        LabelColParams.Font.Charset = DEFAULT_CHARSET
        LabelColParams.Font.Color = clDefault
        LabelColParams.Font.Height = -16
        LabelColParams.Font.Name = 'Tahoma'
        LabelColParams.Font.Style = []
        LabelColParams.HorzLines = True
        LabelColParams.ParentFont = False
        PrintService.ColorSchema = pcsFullColorEh
        DataColParams.RowHeight = 23
        DataSource = DsKETCA
        DrawGraphicData = True
        DrawMemoText = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        Options = [dgvhAlwaysShowEditor, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
        OptionsEh = [dgvhHighlightFocusEh, dgvhEnterToNextRowEh, dgvhHotTrackEh]
        ParentFont = False
        RowsDefValues.Layout = tlCenter
        RowsDefValues.RowLabel.ToolTips = True
        TabOrder = 0
        LabelColWidth = 413
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'B'#7843'ng k'#234' ti'#7873'n m'#7863't '#273#7847'u ca'
      ImageIndex = 1
      object GrNHAN: TDBGridEh
        Left = 12
        Top = 12
        Width = 599
        Height = 509
        AllowedOperations = [alopUpdateEh]
        DataSource = DsMENHGIA
        DynProps = <>
        EditActions = [geaDeleteEh]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        GridLineParams.DataHorzLines = True
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        ParentFont = False
        RowHeight = 29
        SumList.Active = True
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -20
        TitleParams.Font.Name = 'Tahoma'
        TitleParams.Font.Style = []
        TitleParams.ParentFont = False
        TitleParams.RowHeight = 26
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'MENHGIA'
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'M'#7879'nh gi'#225
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SoLuongNhan'
            Footers = <>
            Layout = tlCenter
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' t'#7901
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'TienMatNhan'
            Footer.ValueType = fvtSum
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' ti'#7873'n'
            Width = 143
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'B'#7843'ng k'#234' ti'#7873'n th'#7921'c t'#7871
      ImageIndex = 2
      object GrNOP: TDBGridEh
        Left = 12
        Top = 12
        Width = 605
        Height = 509
        AllowedOperations = [alopUpdateEh]
        DataSource = DsMENHGIA
        DynProps = <>
        EditActions = [geaDeleteEh]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        ParentFont = False
        RowHeight = 29
        SumList.Active = True
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -20
        TitleParams.Font.Name = 'Tahoma'
        TitleParams.Font.Style = []
        TitleParams.ParentFont = False
        TitleParams.RowHeight = 26
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'MENHGIA'
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'M'#7879'nh gi'#225
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'SoLuongNop'
            Footers = <>
            Layout = tlCenter
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' t'#7901
            Width = 143
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'TienMatNop'
            Footer.ValueType = fvtSum
            Footers = <>
            Layout = tlCenter
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = 'S'#7889' ti'#7873'n'
            Width = 143
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object BtnXn: TRzBitBtn
    Left = 671
    Top = 462
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdXacNhan
    Caption = 'X'#225'c Nh'#7853'n'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    ImageIndex = 9
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object RzBitBtn3: TRzBitBtn
    Left = 671
    Top = 518
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdClose
    Caption = 'Tho'#225't'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 3
    TabStop = False
    ImageIndex = 5
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
    Left = 642
    Top = 59
    Width = 213
    Height = 208
    AllowAutoZoom = False
    AutoCompletion.Font.Charset = DEFAULT_CHARSET
    AutoCompletion.Font.Color = clWhite
    AutoCompletion.Font.Height = -19
    AutoCompletion.Font.Name = 'Tahoma'
    AutoCompletion.Font.Style = []
    AutoCompletion.Color = clBlack
    Fill.Color = clNone
    Fill.ColorTo = clNone
    Fill.ColorMirror = clNone
    Fill.ColorMirrorTo = clNone
    Fill.GradientType = gtVertical
    Fill.GradientMirrorType = gtSolid
    Fill.BorderColor = clNone
    Fill.Rounding = 0
    Fill.ShadowColor = clNone
    Fill.ShadowOffset = 0
    Fill.Glow = gmNone
    HighlightCaps = clWhite
    HighlightAltGr = clWhite
    KeyboardType = ktNUMERIC
    Keys = <
      item
        Caption = 'Back Space'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skBackSpace
        Color = 10526880
        X = 1
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '/'
        KeyValue = 111
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDivide
        X = 53
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '*'
        KeyValue = 106
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skMultiply
        X = 105
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '-'
        KeyValue = 109
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skSubstract
        X = 157
        Y = 2
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '7'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '8'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '9'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 42
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '+'
        KeyValue = 107
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skAdd
        X = 157
        Y = 42
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '4'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '5'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '6'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 82
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '1'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '2'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 53
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '3'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 105
        Y = 122
        Width = 52
        SubKeys = <>
      end
      item
        Caption = 'Enter'
        KeyValue = 13
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skReturn
        Color = 10526880
        X = 157
        Y = 122
        Height = 80
        Width = 52
        SubKeys = <>
      end
      item
        Caption = '0'
        KeyValue = -1
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skNone
        X = 1
        Y = 162
        Width = 104
        SubKeys = <>
      end
      item
        Caption = '.'
        KeyValue = 110
        ShiftKeyValue = -1
        AltGrKeyValue = -1
        SpecialKey = skDecimal
        X = 105
        Y = 162
        Width = 52
        SubKeys = <>
      end>
    SmallFont.Charset = DEFAULT_CHARSET
    SmallFont.Color = clWindowText
    SmallFont.Height = -16
    SmallFont.Name = 'Tahoma'
    SmallFont.Style = []
    Version = '1.8.5.2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object BtnTKC: TRzBitBtn
    Left = 671
    Top = 406
    Width = 162
    Height = 50
    Cursor = 1
    FrameColor = 7617536
    ShowFocusRect = False
    Action = CmdTongKetCa
    Caption = 'T'#7893'ng k'#7871't ca'
    Color = 15791348
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    ImageIndex = 37
    Images = DataMain.ImageNavi
    Margin = 1
    Spacing = 1
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 242
    Top = 174
    object CmdNhan: TAction
      Caption = 'B'#7843'ng k'#234' Nh'#7853'n ti'#7873'n'
    end
    object CmdNop: TAction
      Caption = 'B'#7843'ng k'#234' N'#7897'p ti'#7873'n'
    end
    object CmdIn: TAction
      Caption = 'In'
      OnExecute = CmdInExecute
    end
    object CmdXacNhan: TAction
      Caption = 'X'#225'c Nh'#7853'n'
      OnExecute = CmdXacNhanExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      OnExecute = CmdCloseExecute
    end
    object CmdTongKetCa: TAction
      Caption = 'T'#7893'ng k'#7871't ca'
      OnExecute = CmdTongKetCaExecute
    end
  end
  object QrKETCA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrKETCABeforeOpen
    AfterInsert = QrKETCAAfterInsert
    BeforePost = QrKETCABeforePost
    OnCalcFields = QrKETCACalcFields
    Parameters = <
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'SCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      '  select *  from KETCA where LOC=:LOC and SCT=:SCT')
    Left = 428
    Top = 159
    object QrKETCALK_THUNGAN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_THUNGAN'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'NhanVienThuNgan'
      Lookup = True
    end
    object QrKETCACREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrKETCAUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrKETCACREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrKETCAUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrKETCASct: TWideStringField
      FieldName = 'Sct'
    end
    object QrKETCAMaLoc: TWideStringField
      FieldName = 'MaLoc'
      Size = 5
    end
    object QrKETCAMaKho: TWideStringField
      FieldName = 'MaKho'
      Size = 5
    end
    object QrKETCAMaQuay: TWideStringField
      FieldName = 'MaQuay'
    end
    object QrKETCAQuay: TWideStringField
      FieldName = 'Quay'
    end
    object QrKETCANhanVienThuNgan: TIntegerField
      FieldName = 'NhanVienThuNgan'
    end
    object QrKETCANgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
    end
    object QrKETCANgayKetThuc: TDateTimeField
      FieldName = 'NgayKetThuc'
    end
    object QrKETCASctBatDau: TWideStringField
      FieldName = 'SctBatDau'
    end
    object QrKETCASctKetThuc: TWideStringField
      FieldName = 'SctKetThuc'
    end
    object QrKETCASoLuongSct: TIntegerField
      FieldName = 'SoLuongSct'
    end
    object QrKETCATongSoTienNhan: TFloatField
      FieldName = 'TongSoTienNhan'
    end
    object QrKETCAThanhToanTienMat: TFloatField
      FieldName = 'ThanhToanTienMat'
    end
    object QrKETCAThanhToanKhac: TFloatField
      FieldName = 'ThanhToanKhac'
    end
    object QrKETCATienMatNhan: TFloatField
      FieldName = 'TienMatNhan'
    end
    object QrKETCATienMatNop: TFloatField
      FieldName = 'TienMatNop'
    end
    object QrKETCATienMatChenhLech: TFloatField
      FieldName = 'TienMatChenhLech'
    end
    object QrKETCACHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrKETCALOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrKETCACALC_NHANNOP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_NHANNOP'
      Calculated = True
    end
    object QrKETCAKHOA: TLargeintField
      FieldName = 'KHOA'
    end
    object QrKETCAThanhToanTheNganHang: TFloatField
      FieldName = 'ThanhToanTheNganHang'
    end
    object QrKETCAThanhToanChuyenKhoan: TFloatField
      FieldName = 'ThanhToanChuyenKhoan'
    end
    object QrKETCAThanhToanViDienTu: TFloatField
      FieldName = 'ThanhToanViDienTu'
    end
    object QrKETCAThanhToanViDienTu_VNPAY: TFloatField
      FieldName = 'ThanhToanViDienTu_VNPAY'
    end
    object QrKETCAThanhToanViDienTu_MOMO: TFloatField
      FieldName = 'ThanhToanViDienTu_MOMO'
    end
    object QrKETCAGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrKETCACALC_ThanhToanTienMat: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_ThanhToanTienMat'
      Calculated = True
    end
    object QrKETCAThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrKETCAThanhToanNTBL: TFloatField
      FieldName = 'ThanhToanNTBL'
    end
  end
  object DsKETCA: TDataSource
    DataSet = QrKETCA
    Left = 426
    Top = 208
  end
  object QrMENHGIA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrMENHGIABeforeOpen
    AfterInsert = QrMENHGIAAfterInsert
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paSigned, paNullable]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '    from  KETCA_MENHGIA'
      'where KHOA = :KHOA'
      'order by MENHGIA desc')
    Left = 326
    Top = 92
    object QrMENHGIAMENHGIA: TIntegerField
      FieldName = 'MENHGIA'
    end
    object QrMENHGIAGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrMENHGIALOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrMENHGIASTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'STT'
      Calculated = True
    end
    object QrMENHGIASoLuongNhan: TIntegerField
      FieldName = 'SoLuongNhan'
      OnChange = QrMENHGIASOLUONG_NHANChange
    end
    object QrMENHGIATienMatNhan: TFloatField
      FieldName = 'TienMatNhan'
    end
    object QrMENHGIASoLuongNop: TIntegerField
      FieldName = 'SoLuongNop'
      OnChange = QrMENHGIASOLUONG_NHANChange
    end
    object QrMENHGIATienMatNop: TFloatField
      FieldName = 'TienMatNop'
    end
    object QrMENHGIAKhoa: TLargeintField
      FieldName = 'Khoa'
    end
    object QrMENHGIAKhoaCT: TLargeintField
      FieldName = 'KhoaCT'
    end
  end
  object DsMENHGIA: TDataSource
    DataSet = QrMENHGIA
    Left = 326
    Top = 144
  end
  object QrPayment: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrPaymentBeforeOpen
    Parameters = <
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'CA'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'UID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select      concat(a.LOC,'#39'.'#39', a.CA, '#39'.'#39', a.NhanVienThuNgan) PAYM' +
        'ENT,  '
      
        '               sum(isnull(a.ThanhToanTienMat, 0)) - sum(isnull(a' +
        '.ThoiLaiTienMat, 0)) ThanhToanTienMat,'
      
        '               sum(isnull(a.ThanhToanTheNganHang, 0))  ThanhToan' +
        'TheNganHang,'
      
        '               sum(isnull(a.ThanhToanTraHang, 0))  ThanhToanTraH' +
        'ang,'
      
        '               sum(isnull(a.ThanhToanPhieuQuaTang, 0)) ThanhToan' +
        'PhieuQuaTang,'
      
        '               sum(isnull(a.ThanhToanViDienTu, 0)) ThanhToanViDi' +
        'enTu,'
      
        '               sum(isnull(a.ThanhToanChuyenKhoan, 0)) ThanhToanC' +
        'huyenKhoan,'
      
        '               sum(isnull(a.ThanhToanDiemTichLuy, 0)) ThanhToanD' +
        'iemTichLuy,'
      
        '              sum(case when a.ThanhToanViDienTu_Loai = '#39'VNPAY'#39' t' +
        'hen isnull(a.ThanhToanViDienTu, 0) else 0 end) ThanhToanViDienTu' +
        '_VNPAY,'
      
        '              sum(case when a.ThanhToanViDienTu_Loai = '#39'MOMO'#39' th' +
        'en isnull(a.ThanhToanViDienTu, 0) else 0 end) ThanhToanViDienTu_' +
        'MOMO,'
      
        '              sum(case when a.ThanhToanViDienTu_Loai = '#39'ZALOPAY'#39 +
        ' then isnull(a.ThanhToanViDienTu, 0) else 0 end) ThanhToanViDien' +
        'Tu_ZALOPAY'
      '  from      T_BANLE a'
      ' where'#9'a.LCT = '#39'BLE'#39
      '    and     a.LOC = :LOC'
      '     and'#9'a.CA = :CA'
      '     and'#9'a.NhanVienThuNgan = :UID'
      'group by  a.LOC, a.CA, a.NhanVienThuNgan')
    Left = 502
    Top = 132
    object QrPaymentPAYMENT: TWideStringField
      FieldName = 'PAYMENT'
      ReadOnly = True
      Size = 39
    end
    object QrPaymentThanhToanTienMat: TFloatField
      FieldName = 'ThanhToanTienMat'
      ReadOnly = True
    end
    object QrPaymentThanhToanTheNganHang: TFloatField
      FieldName = 'ThanhToanTheNganHang'
      ReadOnly = True
    end
    object QrPaymentThanhToanTraHang: TFloatField
      FieldName = 'ThanhToanTraHang'
      ReadOnly = True
    end
    object QrPaymentThanhToanPhieuQuaTang: TFloatField
      FieldName = 'ThanhToanPhieuQuaTang'
      ReadOnly = True
    end
    object QrPaymentThanhToanViDienTu: TFloatField
      FieldName = 'ThanhToanViDienTu'
      ReadOnly = True
    end
    object QrPaymentThanhToanChuyenKhoan: TFloatField
      FieldName = 'ThanhToanChuyenKhoan'
      ReadOnly = True
    end
    object QrPaymentThanhToanDiemTichLuy: TFloatField
      FieldName = 'ThanhToanDiemTichLuy'
      ReadOnly = True
    end
    object QrPaymentThanhToanViDienTu_VNPAY: TFloatField
      FieldName = 'ThanhToanViDienTu_VNPAY'
      ReadOnly = True
    end
    object QrPaymentThanhToanViDienTu_MOMO: TFloatField
      FieldName = 'ThanhToanViDienTu_MOMO'
      ReadOnly = True
    end
    object QrPaymentThanhToanViDienTu_ZALOPAY: TFloatField
      FieldName = 'ThanhToanViDienTu_ZALOPAY'
      ReadOnly = True
    end
  end
end
