﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosCommon;

interface

uses
	Classes, Db, Windows, Forms, ADODb, wwfltdlg, Graphics, ComCtrls, ActnList,
    fctreecombo, fctreeview, Controls, SysUtils, wwDBGrid2, Messages, ADOInt,
    ShellAPI, wwdblook, wwDataInspector;

resourcestring
	{ Softz }
	// Import
    RS_IM_NUM_REC			= 'Đã import %d mẫu tin.';
    RS_IM_NONE				= 'Không có mẫu tin nào được import.';
    RS_IM_ERR_REC			= 'Có %d mẫu tin không hợp lệ.';

    // File
	RS_FILE_IO				= 'Lỗi truy xuất file.';
    RS_EXPORT_ERR			= 'Lỗi xuất dữ liệu.';

    // Common
    RS_CONNECT_FAIL     	= 'Không kết nối được với cơ sở dữ liệu.';
    RS_NOTE_CAP         	= 'Lưu ý';

    RS_VAT_NOTSAME       	= 'Mặt hàng sai loại thuế.';
    RS_NO_FOLDEREXPORT		= 'Chưa chọn thư mục lưu dữ liệu export.';
    RS_EXPORTED_COMPLETE 	= 'Đã xuất xong dữ liệu vào thư mục "%s"';

    RS_DSN_CONFIG			= 'Chưa cấu hình text DSN.';
    RS_ERROR_COPY_FILE		= 'Lỗi khi copy file vào thư mục hệ thống.';
    RS_INVALID_DATA			= 'Dữ liệu không hợp lệ.';
    RS_INVALID_CARD			= 'Mã thẻ không hợp lệ.';
    RS_ITEM_CODE_DUPLICATE	= 'Trùng %s.';

	RS_ITEM_CODE_FAIL1		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc mã hàng đã ngừng kinh doanh';
	RS_ITEM_CODE_FAIL2		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc nhà cung cấp.';
    RS_BOOKCLOSED			= 'Đã khóa sổ đến ngày %s.';

    RS_INVALID_QTY 			= 'Số lượng không hợp lệ.';
	RS_INVALID_DISCOUNT 	= 'Chiết khấu không hợp lệ.';
    RS_INVALID_INTEREST 	= 'Tỷ lệ lãi không hợp lệ.';
	RS_POS_NOT_PRICE 		= 'Mặt hàng chưa có giá bán.';
    RS_SCT_CHANGE           = 'Ngày lập phiếu thay đổi. Số phiếu mới là: "%s".';

	RS_DA_THUCHI			= 'Đã phát sinh Thu / Chi, nên không thể chỉnh sửa được.';
    RS_XOA_CHITIET			= 'Xóa toàn bộ chi tiết chứng từ?';
    RS_CONFIRM_XOAPHIEU		= 'Xóa phiếu sẽ không thể hồi phục. Tiếp tục?';
    RS_PHUCHOI              = 'Phục hồi';
    RS_DAXOA                = 'Đã xóa';

    RS_EDIT_BY              = 'Phiếu đang được sửa bởi "%s" từ lúc "%s", các dữ liệu đang sửa trước đó sẽ bị bỏ qua. Tiếp tục?';

    RS_ADMIN_CONTACT		= 'Xin liên hệ với người quản trị hệ thống.';

    // Cr Frame
    RS_THESAME_MONTH 		= 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME	= 'Thời gian báo cáo không hợp lệ.';

	{ POSz }
    RS_PRINT_ERR            = 'Lỗi in bill.';
    RS_NHACNHO_THANHTOAN    = 'Xác nhận nhân đơn vị tiền thối';
	RS_INVALID_ACTION       = 'Thao tác không hợp lệ.';
	RS_POS_CODE_FAIL        = 'Nhập sai mã hàng hóa.';
    RS_CONFIRM_PRICE        = 'Hàng có giá bán là %s, giá đã được giảm là %s. Giảm giá?';
    RS_CUT_PRICE_QTY        = 'Chỉ còn lại %f suất giảm giá.';

    NOT_CONFIG_YET          = 'Chưa cấu hình số hiệu máy bán hàng.';
    EXIST_HELD_INVOICE      = 'Có hóa đơn chưa lưu. Tiếp tục?';


	{ Chung }


const
    REP_ENCODE_KEY: Word = 119;
    TXT_DSN: String 		= 'SOFTZ_TEXT';

var
    sysPrinter: string;

    // ePos global variables
    posCa: String;

    posQuay0,			// Quầy gốc tương ứng tên máy trong "Danh sách quầy thu ngân"
    posMaQuay0,

    posQuay,			// Quầy hiện tại, mặc định bằng mQuay0 and could be changed
    posMaQuay,
    posMakho,			// Kho hiện tại, mặc định bằng sysDefKho and could be changed
    lastSct,
    posSct: String;
    posKhoa: TGUID;	// Hóa đơn hiện tại


    (*
    ** Message
    *)

function InitParamPos: Boolean;
function  AllocRetailBillNumber(const counter: String; buydate: TDateTime): String;
function  PosKetCa(const cmd: Integer; nhanCa: Integer = 0): Boolean;

	(*
    ** Utils
    *)
function  exDefaultWarehouse: Boolean;

procedure exInitDotMavtPos;
function  exDotMavtPos(const Group: Integer; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavtPos(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavtPos(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

implementation

uses
	isMsg, isStr, Printers, MainData, IniFiles, isCommon, Math, isFile,
    Rights, isDb, GuidEx, isLib, isNameVal, RepEngine, isEnCoding, SetLicense, ExCommon;


function InitParamPos: Boolean;
begin
    Result := True;
    // Get outlet info.
    with TADOQuery.Create(nil) do
    begin
        Connection := sysDbConnection;
        LockType := ltReadOnly;
        SQL.Text := 'select top 1 a.QUAY, a.MAKHO, a.MAQUAY, a.TENMAY, a.LOC  from DM_QUAYTN a, DM_KHO b ' +
                        ' where a.MAKHO = b.MAKHO and a.TENMAY = :TENMAY and a.LOC = :LOC ' +
                        ' order by case when a.MAKHO = (select top 1 DEFAULT_MAKHO from SYS_CONFIG) then 0 else 1 end';
        Parameters[0].Value := isGetComputerName;
        Parameters[1].Value := sysLoc;
        Open;
        posQuay0 := Fields[0].AsString;
        sysDefKho := Fields[1].AsString;
        posMaQuay0 := Fields[2].AsString;
        Close;
        Free;
    end;

    if posQuay0 = '' then
    begin
        ErrMsg(NOT_CONFIG_YET + #13 + RS_ADMIN_CONTACT);
        Result := False;
        Application.Terminate;
        Exit;
    end;

    posQuay := posQuay0;
    posMaQuay := posMaQuay0;
    posMakho := sysDefKho;

    // Validate access right
    if GetRights('POS_BANHANG') = R_DENY then
    begin
        Result := False;
        Exit;
    end;

    // Kiem tra xem co phieu chua in khong
    posKhoa := TGuidEx.EmptyGuid;;
    with TADOQuery.Create(nil) do
    begin
        Connection := sysDbConnection;
        LockType := ltReadOnly;
        SQL.Text :=
            'select	KHOA, SCT from BANLE_TAM where LCT = ''BLE''' +
            ' and isnull(PRINTED, 0) = 0' +
            ' and QUAY = ''' + posQuay + '''' +
            ' and MAKHO = ''' + posMakho + '''';
        Open;

        if not IsEmpty then
            if YesNo(EXIST_HELD_INVOICE) then
            begin
                posKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
                lastSct := FieldByName('SCT').AsString;
            end
            else
            begin
                Close;
                Exit;
            end;
        Close;
        Free;
    end;

    // Get maximum bill number
    posSct := AllocRetailBillNumber(posMaQuay, Date);

end;

function AllocRetailBillNumber(const counter: String; buydate: TDateTime): String;
var
    _msg: string;
begin
    Result := '';
    try
        with TADOCommand.Create(nil) do
        begin
            Connection := sysDbConnection;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spBANLE_Create_Sct;1';

            Parameters.Refresh;
            Parameters.ParamByName('@MaQuay').Value := counter;
            Parameters.ParamByName('@Ngay').Value := buydate;
            Execute;

            if Parameters.ParamValues['@RETURN_VALUE'] = 0 then
            begin
                Result := Parameters.ParamValues['@SCT'];
            end;

            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function PosKetCa(const cmd: Integer; nhanCa: Integer = 0): Boolean;
begin
    with TADOCommand.Create(nil) do
    begin
        Connection := sysDbConnection;
        CommandType := cmdStoredProc;
        CommandTimeout := 1200;
        CommandText := 'spKETCA_Get_Sct;1';

        Parameters.Refresh;
        Parameters.ParamByName('@pUID').Value := sysLogonUID;
        Parameters.ParamByName('@pCMD').Value := cmd;
        Parameters.ParamByName('@pNhanCa').Value := nhanCa;
        Parameters.ParamByName('@pMaQuay').Value := posMaQuay;
        Execute;

        if cmd = 0 then
            posCa := Parameters.ParamValues['@pSct'];

        Result := (Parameters.ParamValues['@RETURN_VALUE'] = 0);
        Free;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_WAREHOUSE_NOT_DEFAULT = 'Chưa chỉ định mã kho.';

function exDefaultWarehouse: Boolean;
begin
	Result := sysDefKho <> '';
	if Result then
    else
    	Msg(RS_WAREHOUSE_NOT_DEFAULT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_MAVT: array[1..4] of String = (
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp'
	);

procedure exInitDotMavtPos;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 4 do
		SetCustomGrid('REF_MAVT_' + IntToStr(i), DOT_SEARCH_MAVT[i]);
end;

function exDotMavtPos(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond <> '' then
        exCond := exCond + ' and TINHTRANG=''01'''
    else
        exCond := 'TINHTRANG=''01''';
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_MAVT[Group], exCond);
end;

function exDotMavtPos(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotMavtPos(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMavtPos(const Group: Integer; Sender: TField; exCond: String): Boolean;
begin
    Result := exDotMavtPos(Group, DataMain.QrDMVT, Sender, exCond);
end;
	(*
    ** End: Refs. functions
    *)

begin
	cfRecordDate := True;
end.

