object FrmPosVIP: TFrmPosVIP
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin VIP'
  ClientHeight = 426
  ClientWidth = 821
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Padding.Left = 8
  Padding.Top = 8
  Padding.Right = 8
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    821
    426)
  PixelsPerInch = 96
  TextHeight = 16
  object CmdReturn: TBitBtn
    Left = 714
    Top = 310
    Width = 97
    Height = 51
    Cursor = 1
    Action = CmdSave
    Anchors = [akRight, akBottom]
    Caption = 'Ti'#7871'p t'#7909'c'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ParentFont = False
    TabOrder = 4
  end
  object BtnCancel: TBitBtn
    Left = 714
    Top = 367
    Width = 97
    Height = 51
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'K'#7871't th'#250'c'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 5
  end
  object Panel1: TPanel
    Left = 8
    Top = 11
    Width = 697
    Height = 407
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object PaSearch: TPanel
      Left = 1
      Top = 1
      Width = 695
      Height = 80
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      DesignSize = (
        695
        80)
      object Label18: TLabel
        Left = 9
        Top = 8
        Width = 114
        Height = 16
        Caption = '&Nh'#7853'p chu'#7895'i c'#7847'n t'#236'm:'
        FocusControl = EdSearch
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdSearch: TAdvEdit
        Left = 9
        Top = 30
        Width = 677
        Height = 43
        EmptyText = 'H'#7885' t'#234'n, '#272'i'#7879'n tho'#7841'i, '#272#7883'a ch'#7881', M'#227' VIP...'
        EmptyTextStyle = [fsItalic]
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Anchors = [akLeft, akTop, akRight]
        Color = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -29
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Visible = True
        OnExit = EdSearchExit
        OnKeyPress = EdSearchKeyPress
        Version = '3.5.0.1'
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 81
      Width = 695
      Height = 168
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 1
      object isPanel1: TisPanel
        Left = 0
        Top = 0
        Width = 441
        Height = 168
        Align = alLeft
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Ctl3D = True
        ParentBackground = False
        ParentCtl3D = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin c'#225' nh'#226'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label6: TLabel
          Left = 26
          Top = 58
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#7885' t'#234'n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 24
          Top = 110
          Width = 39
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7883'a ch'#7881
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 5
          Top = 136
          Width = 58
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7879'n tho'#7841'i'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 236
          Top = 136
          Width = 31
          Height = 16
          Alignment = taRightJustify
          Caption = 'Email'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 29
          Top = 32
          Width = 34
          Height = 16
          Alignment = taRightJustify
          Caption = 'M'#227' s'#7889
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel3: TLabel
          Left = 8
          Top = 84
          Width = 55
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y sinh'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 196
          Top = 84
          Width = 71
          Height = 16
          Alignment = taRightJustify
          Caption = 'CMND/CCCD'
          FocusControl = wwDBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit6: TwwDBEdit
          Left = 272
          Top = 132
          Width = 159
          Height = 22
          Ctl3D = False
          DataField = 'EMAIL'
          DataSource = DsDMVIP
          ParentCtl3D = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit9: TwwDBEdit
          Tag = 1
          Left = 69
          Top = 28
          Width = 197
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'MAVIP'
          DataSource = DsDMVIP
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 69
          Top = 80
          Width = 121
          Height = 24
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAYSINH'
          DataSource = DsDMVIP
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 3
        end
        object wwDBEdit1: TwwDBEdit
          Left = 272
          Top = 80
          Width = 159
          Height = 22
          Ctl3D = False
          DataField = 'CMND'
          DataSource = DsDMVIP
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit2: TwwDBEdit
          Tag = 1
          Left = 272
          Top = 28
          Width = 87
          Height = 22
          TabStop = False
          Color = 15794175
          Ctl3D = False
          DataField = 'MASO'
          DataSource = DsDMVIP
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdHo: TDBAdvEdit
          Left = 69
          Top = 54
          Width = 290
          Height = 22
          EmptyText = '--B'#7855't bu'#7897'c nh'#7853'p--'
          EmptyTextStyle = [fsItalic]
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          AutoSelect = False
          Color = clWindow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          Visible = True
          Version = '3.5.0.1'
          DataField = 'HOTEN'
          DataSource = DsDMVIP
        end
        object EdDchi: TDBAdvEdit
          Left = 69
          Top = 107
          Width = 362
          Height = 22
          EmptyText = '--B'#7855't bu'#7897'c nh'#7853'p--'
          EmptyTextStyle = [fsItalic]
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          AutoSelect = False
          Color = clWindow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          Visible = True
          Version = '3.5.0.1'
          DataField = 'DCHI'
          DataSource = DsDMVIP
        end
        object EdDienThoai: TDBAdvEdit
          Left = 69
          Top = 132
          Width = 161
          Height = 22
          EmptyText = '--B'#7855't bu'#7897'c nh'#7853'p--'
          EmptyTextStyle = [fsItalic]
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Font.Charset = DEFAULT_CHARSET
          Lookup.Font.Color = clWindowText
          Lookup.Font.Height = -11
          Lookup.Font.Name = 'Arial'
          Lookup.Font.Style = []
          Lookup.Separator = ';'
          AutoSelect = False
          Color = clWindow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          Visible = True
          Version = '3.5.0.1'
          DataField = 'DTHOAI'
          DataSource = DsDMVIP
        end
      end
      object PD2: TisPanel
        Left = 441
        Top = 0
        Width = 254
        Height = 168
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Th'#244'ng tin VIP'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object TntLabel1: TLabel
          Left = 6
          Top = 58
          Width = 81
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#237'nh '#273#7871'n ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label25: TLabel
          Left = 18
          Top = 84
          Width = 69
          Height = 16
          Alignment = taRightJustify
          Caption = 'D.s'#7889' t'#237'ch l'#361'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel4: TLabel
          Left = 41
          Top = 112
          Width = 46
          Height = 16
          Alignment = taRightJustify
          Caption = 'Lo'#7841'i VIP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 5
          Top = 32
          Width = 82
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y tham gia'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object wwDBDateTimePicker3: TwwDBDateTimePicker
          Left = 94
          Top = 54
          Width = 151
          Height = 24
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = 15794175
          DataField = 'NGAYDSO'
          DataSource = DsDMVIP
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 4
        end
        object DBEdit11: TwwDBEdit
          Tag = 1
          Left = 94
          Top = 80
          Width = 151
          Height = 24
          TabStop = False
          BorderStyle = bsNone
          Color = 15794175
          Ctl3D = False
          DataField = 'DOANHSO'
          DataSource = DsDMVIP
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8404992
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbLoai: TwwDBLookupCombo
          Left = 94
          Top = 106
          Width = 151
          Height = 24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DGIAI'#9'0'#9'DGIAI'#9'F')
          DataField = 'LOAI'
          DataSource = DsDMVIP
          LookupTable = DataMain.QrLOAI_VIP_LE
          LookupField = 'MA'
          Color = 15794175
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 1
          AutoDropDown = False
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 94
          Top = 28
          Width = 151
          Height = 24
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = 15794175
          DataField = 'NGAY_THAMGIA'
          DataSource = DsDMVIP
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 0
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 249
      Width = 695
      Height = 157
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object GrList: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 695
        Height = 157
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'HOTEN'#9'25'#9'H'#7885' t'#234'n'#9'F'
          'DTHOAI'#9'19'#9#272'i'#7879'n tho'#7841'i'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsDMVIP
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
        ParentFont = False
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -24
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
    end
  end
  object BtSearch: TBitBtn
    Left = 714
    Top = 11
    Width = 97
    Height = 51
    Cursor = 1
    Action = CmdSearch
    Anchors = [akTop, akRight]
    Caption = '     T'#236'm     '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FF9D9995FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FF8C8C8C6363639E9E9EFF00FF4F4737FF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF746B61F6B564F6B564F5B4
      64646361FF00FFFF00FFFF00FF55D3FD28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF5D696DF6B564F6B564F6B564F6B564F5B4649C9D9EFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF616161F6B564F6B564F6B564F6B5
      64F6B564616363FF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF606263F6B564F8C98EF6B564F9D2A3F6B56478878DFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF30BBE8967F62F7BD75F9D09CF7B8
      6B746B61B5EAFCFF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF30BBE86062636161615D686C2AC6F9B5EBFDFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CA
      FF28CAFFB5EBFDFF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFFA3E4FDFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CA
      FF28CAFF7CD5FDFF00FFFF00FF25C8FF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFFDFF3FDFF00FFFF00FF00A0FF
      00A0FF00A0FF00A0FF00A0FF0AA2FDFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FF57BFFD54BEFD54BEFD54BEFDFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    TabOrder = 1
  end
  object BtNew: TBitBtn
    Left = 713
    Top = 119
    Width = 97
    Height = 51
    Cursor = 1
    Action = CmdNew
    Anchors = [akTop, akRight]
    Caption = ' C'#7845'p m'#7899'i '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4B974B11
      77111177114B974BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF147A1455BB5555BB55147A14FF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF187E1858
      BE5858BE58187E18FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF1D831D5CC25C5CC25C1D831DFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF23892361
      C76161C761238923FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF5DA95D
      2D932D329832389E383FA53F44AA4466CC6666CC6644AA443FA53F389E383298
      322D932D5DA95DFF00FFFF00FF3096306CD26C6CD26C6CD26C6CD26C6CD26C6C
      D26C6CD26C6CD26C6CD26C6CD26C6CD26C6CD26C309630FF00FFFF00FF369C36
      88EE8888EE8888EE8888EE887DE37D71D77171D7717DE37D88EE8888EE8888EE
      8888EE88369C36FF00FFFF00FF6CB86C40A64046AC464CB24C51B75156BC5677
      DD7777DD7756BC5651B7514CB24C46AC4640A6406CB86CFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF43A9437CE27C7CE27C43A943FF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF49AF4981
      E78181E78149AF49FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF4EB44E85EB8585EB854EB44EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF52B85288
      EE8888EE8852B852FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF7ECA7E55BB5555BB557ECA7EFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    TabOrder = 3
  end
  object BtClear: TBitBtn
    Left = 714
    Top = 65
    Width = 97
    Height = 51
    Cursor = 1
    Action = CmdClear
    Anchors = [akTop, akRight]
    Caption = 'X'#243'a chu'#7895'i'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFBEB0999C90789C90789C90789C90789C90789C90789C90789C9078FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAE5CAFDF2E3FDF2E3FDF2E3FD
      F2E3FDF2E3FDF2E3FDF2E3FBE5CAFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFF9DCBAFDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FCEAD4FF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFBDEBBFDF2E3FDF2E3FDF2E3FD
      F2E3FCEEDDFDF2E3FDF2E3FCF0DEFCF6EEFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFADFBFFDF2E3FAE8D2C06515E9C39EE9993CE5881EFDF2E3FDF2E3FBED
      DDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFCE6CAFDF2E3E68A21F5CEA1FD
      F2E3FAE5CAC1691AF2DBBFFDF2E3FAE4CBFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFDEBD4FDF2E3EEAD64EFB675FDF2E3FDF2E3CC803DFDF2E3FDF2E3F9DC
      BAFF00FFFF00FFFF00FFFF00FFFF00FFFCF5EEFDF0DEFDF2E3FDF2E3C16718D9
      9F69E79434FAE6CEFDF2E3FDF2E3FBDEBBFF00FFFF00FFFF00FFFF00FFFF00FF
      FBEDDDFDF2E3FDF2E3FDF2E3F1D7BCDD8E39EDAD63FDF2E3FDF2E3FDF2E3FADF
      C0FF00FFFF00FFFF00FFFF00FFFF00FFFAE5CCFDF2E3FDF2E3FDF2E3FDF2E3FD
      F2E3FDF2E3FDF2E3FDF2E3FDF2E3FCE6CAFF00FFFF00FFFF00FFFF00FFFF00FF
      F9DCB9FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDF2E3FDEB
      D4FF00FFFF00FFFF00FFFF00FFFF00FFDBC4A5DDCDB6DDCDB6DDCDB6DDCDB6DD
      CDB6DDCDB6DDCDB6DDCDB6DDCDB6DCCCB4EAE1D7FF00FFFF00FFFF00FFFF00FF
      BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3A3BBB3
      A3C9C2B6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 713
    Top = 176
    Width = 97
    Height = 51
    Cursor = 1
    Action = CmdSaveCapMoi
    Anchors = [akTop, akRight]
    Caption = 'L'#432'u'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000E09165E3FCA3
      72FFFCB087FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFEAE4DFFFEAE4
      DFFFEAE4DFFFEAE4DFFFEAE4DFFFF9AC61FFFC9228FFF9922FFCFCA372FFFCA3
      72FFFCB087FFF9F9F9FFF9F9F9FFE7F5F9FFE5F4F9FFE5F4F9FFC2DCE5FFC2DC
      E5FFC7DDE5FFEAE4DFFFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFECF6F9FF78DCFAFF75DCFAFF75DCFAFF38B8FBFF38B8
      FBFF3AB9FAFFD0DFE3FFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFF9F9F9FFD4F1F9FFD0F0F9FFD0F0F9FF9ED4ECFF9ED4
      ECFFA4D6EAFFEAE4DFFFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFEDF6F9FF8CE1FAFF89E0FAFF89E0FAFF47BEF9FF47BE
      F9FF49BFF8FFD2DFE3FFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFF6F8F9FFBFECF9FFBCEBF9FFBCEBF9FF80CDF0FF80CD
      F0FF84CFF0FFE5E3DFFFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFEFF7F9FFA1E5FAFF9EE5FAFF9EE5FAFF5AC4F6FF5AC4
      F6FF5DC5F6FFD6E0E3FFEAE4DFFFF9AC61FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCB087FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFEAE4DFFFEAE4
      DFFFEAE4DFFFEAE4DFFFEAE4DFFFF9AC5FFFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCA473FFFBC4A6FFFBC9ADFFFBC9ADFFFBC9ADFFFBC9ADFFF4CFACFFF4CF
      ACFFF4CFACFFF4CFACFFF4CAA1FFFC942BFFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCA372FFFCA372FFFCA372FFFCA372FFFCA372FFFCA372FFFC9228FFFC92
      28FFFC9228FFFC9228FFFC9228FFFC9228FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFCA372FFFCA372FFFCA372FFFCA372FFFCA372FFFCA372FFFC9228FFFC92
      28FFFC9228FFFC9228FFFC9228FFFC9228FFFC9228FFFC9228FFFCA372FFFCA3
      72FFFA9C6FFFEB5C55FFEA5854FFEA5854FFEA5854FFEA5854FFE34535FFE345
      35FFE34535FFF57B31FFFC9228FFFC9228FFFC9228FFFC9228FFFCA372FFFCA3
      72FFF9976DFFEA5854FFEA5854FFEA5854FFEA5854FFEA5854FFE34535FFE345
      35FFE34535FFF17134FFFC9228FFFC9228FFFC9228FFFC9228FFFCA372FFFCA3
      72FFF9976DFFEA5854FFEA5854FFEA5854FFEA5854FFEA5854FFE34535FFE345
      35FFE34535FFF17134FFFC9228FFFC9228FFFC9228FFE68934E9FCA372FFFCA3
      72FFF9976DFFEA5854FFEA5854FFEA5854FFEA5854FFEA5854FFE34535FFE345
      35FFE34535FFF17134FFFC9228FFFC9228FFE98A34EB10090510E09165E3FCA3
      72FFF9976DFFEA5854FFEA5854FFEA5854FFEA5854FFEA5854FFE34535FFE345
      35FFE34535FFF17134FFFC9228FFE68934E91009051000000000}
    ParentFont = False
    TabOrder = 6
  end
  object QrDMVIP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDMVIPBeforeInsert
    BeforeEdit = QrDMVIPBeforeEdit
    BeforePost = QrDMVIPBeforePost
    OnPostError = exDbError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DM_VIP'
      ' where'#9'1 = 1')
    Left = 232
    Top = 48
    object QrDMVIPMASO: TWideStringField
      DisplayLabel = 'M'#227' s'#7889
      FieldName = 'MASO'
      Size = 15
    end
    object QrDMVIPDCHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881
      FieldName = 'DCHI'
      Size = 250
    end
    object QrDMVIPEMAIL: TWideStringField
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Size = 250
    end
    object QrDMVIPTL_CK: TFloatField
      DisplayLabel = 'T'#7927' l'#7879' chi'#7871't kh'#7845'u'
      FieldName = 'TL_CK'
      Visible = False
    end
    object QrDMVIPMAVIP: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MAVIP'
      Visible = False
      Size = 13
    end
    object QrDMVIPCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMVIPUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMVIPDOANHSO: TFloatField
      FieldName = 'DOANHSO'
    end
    object QrDMVIPNGAYDSO: TDateTimeField
      FieldName = 'NGAYDSO'
    end
    object QrDMVIPNGAYSINH: TDateTimeField
      FieldName = 'NGAYSINH'
    end
    object QrDMVIPCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDMVIPUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDMVIPLOAI: TWideStringField
      FieldName = 'LOAI'
      Size = 15
    end
    object QrDMVIPNGAY_THAMGIA: TDateTimeField
      FieldName = 'NGAY_THAMGIA'
    end
    object QrDMVIPLOC_THAMGIA: TWideStringField
      FieldName = 'LOC_THAMGIA'
      Size = 2
    end
    object QrDMVIPHOTEN: TWideStringField
      FieldName = 'HOTEN'
      Size = 200
    end
    object QrDMVIPSOTIEN_MUA: TFloatField
      FieldName = 'SOTIEN_MUA'
    end
    object QrDMVIPSOTIEN_TRA: TFloatField
      FieldName = 'SOTIEN_TRA'
    end
    object QrDMVIPDOANHSO_DAU: TFloatField
      FieldName = 'DOANHSO_DAU'
    end
    object QrDMVIPDOANHSO_MUA: TFloatField
      FieldName = 'DOANHSO_MUA'
    end
    object QrDMVIPTINHTRANG: TWideStringField
      FieldName = 'TINHTRANG'
      Size = 2
    end
    object QrDMVIPCMND: TWideStringField
      FieldName = 'CMND'
      Size = 100
    end
    object QrDMVIPDTHOAI: TWideStringField
      DisplayLabel = 'S'#7889' '#273'i'#7879'n tho'#7841'i'
      FieldName = 'DTHOAI'
      Size = 12
    end
  end
  object DsDMVIP: TDataSource
    DataSet = QrDMVIP
    Left = 260
    Top = 48
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 124
    Top = 44
    object CmdSave: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdRefesh: TAction
      OnExecute = CmdRefeshExecute
    end
    object CmdNew: TAction
      Caption = 'C'#7845'p m'#7899'i'
      OnExecute = CmdNewExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm'
      OnExecute = CmdSearchExecute
    end
    object CmdClear: TAction
      Caption = 'X'#243'a chu'#7895'i'
      OnExecute = CmdClearExecute
    end
    object CmdSaveCapMoi: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      OnExecute = CmdSaveCapMoiExecute
    end
  end
  object spGET_VIP_NEW: TADOCommand
    CommandText = 'spGET_VIP_NEW;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MAVIP'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end
      item
        Name = '@CONLAI'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 356
    Top = 312
  end
end
