﻿program PosZ;

uses
  Forms,
  CkMa in 'CkMa.pas' {FrmCkma},
  SoBill in 'SoBill.pas' {FrmSoBill},
  PosCommoInfo in 'PosCommoInfo.pas' {FrmCommoInfo},
  Printer in 'Printer.pas' {FrmPrinter},
  TempReceipt in 'TempReceipt.pas' {FrmTempReceipt},
  ReceiptDesc in 'ReceiptDesc.pas' {FrmReceiptDesc},
  exResStr in 'exResStr.pas',
  PosMain in 'PosMain.pas' {FrmMain},
  PosVIP in 'PosVIP.pas' {FrmPosVIP},
  CkBill in 'CkBill.pas' {FrmChietkhau},
  Retailer in 'Retailer.pas' {FrmRetailer},
  Payment in 'Payment.pas' {FrmPayment},
  TheLenh in 'TheLenh.pas' {FrmThelenh},
  CR208U in 'CR208U.pas',
  exCOMPort in 'exCOMPort.pas',
  SystemCriticalU in 'SystemCriticalU.pas',
  PosChitiet in 'PosChitiet.pas' {FrmPosChitiet},
  PosCamung in 'PosCamung.pas' {FrmPosCamung},
  KeypadNumeric in 'KeypadNumeric.pas' {FrmKeypadNumeric},
  SetGhichu in 'SetGhichu.pas' {FrmSetGhichu},
  BanleCN in 'BanleCN.pas' {FrmBanleCN},
  ThongtinCN_View in 'ThongtinCN_View.pas' {FrmThongtinCN_View},
  PosCommon in 'PosCommon.pas',
  ExCommon in '..\Common\ExCommon.pas',
  exPrintBill in '..\Common\exPrintBill.pas',
  FastReport in '..\Common\FastReport.pas' {FrmFastReport: TDataModule},
  frameNavi in '..\Common\frameNavi.pas' {frNavi: TFrame},
  GuidEx in '..\Common\GuidEx.pas',
  RepEngine in '..\Common\RepEngine.pas' {FrmRep: TDataModule},
  MainData in '..\Common\MainData.pas' {DataMain: TDataModule},
  ChonDsma in '..\SoftZ\ChonDsma.pas' {FrmChonDsma},
  Banle in '..\SoftZ\Banle.pas' {FrmBanle},
  frameKho in '..\SoftZ\frameKho.pas' {frKHO: TFrame},
  ThongtinCN in 'ThongtinCN.pas' {FrmThongtinCN},
  NhaptraBanleTimPhieu in '..\SoftZ\NhaptraBanleTimPhieu.pas' {FrmNhaptraBanleTimPhieu},
  NhapTraBanLe in '..\SoftZ\NhapTraBanLe.pas' {FrmNhapTraBanLe},
  frameNgay in '..\SoftZ\frameNgay.pas' {frNGAY: TFrame},
  Tienthue in '..\SoftZ\Tienthue.pas' {FrmTienthue},
  OfficeData in '..\Common\OfficeData.pas' {DataOffice: TDataModule},
  PosKetCa in 'PosKetCa.pas' {FrmPosKetca};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Bán lẻ';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TFrmFastReport, FrmFastReport);
  Application.CreateForm(TFrmRep, FrmRep);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TDataOffice, DataOffice);
  Application.CreateForm(TFrmChonDsma, FrmChonDsma);
  Application.CreateForm(TFrmPosKetca, FrmPosKetca);
  if not DataMain.Logon(1) then
  begin
      Application.Terminate;
      Exit;
  end;

  if  not PosCommon.InitParamPos then
  begin
      Application.Terminate;
      Exit;
  end;

  if not PosCommon.PosKetCa(0, 0) then
    begin
        if not FrmPosKetca.Execute(True) then
        begin
            Application.Terminate;
            Exit;
        end;
    end;

  Application.Run;
end.
