﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosCommoInfo;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Db, ADODB,
  wwDataInspector, Grids;

type
  TFrmCommoInfo = class(TForm)
    EdMA: TEdit;
    Inspect: TwwDataInspector;
    DsDMVT: TDataSource;
    CHECK_TONKHO: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CHECK_TONKHOAfterOpen(DataSet: TDataSet);
  private
  	RefDmvt: TADOQuery;
  public
  	procedure Execute(Sender: TADOQuery);
  end;

var
  FrmCommoInfo: TFrmCommoInfo;

implementation

uses
	PosCommon, isDb, isLib, isCommon, GuidEx, ExCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.Execute;
begin
    RefDmvt := Sender;
	with RefDmvt do
        if not Active then
            Open;
	ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormShow(Sender: TObject);
begin
    SetDisplayFormat(RefDmvt, sysCurFmt);
	EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CHECK_TONKHO.Close;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.CHECK_TONKHOAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(CHECK_TONKHO, sysCurFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.EdMAKeyPress(Sender: TObject; var Key: Char);
var
	n: Integer;
	s, mavt, makho: String;
    ngay: TDateTime;

begin
		// Is CRLF keystroke
	if Key <> #13 then
    	Exit;

    // Is empty
    s := Trim(EdMA.Text);
    if s = '' then
        Exit;

    Key := #0;

    // Quick select
    n := IsDotSelect(s);
    if n <> 0 then
    begin
        // Show dialog
        if not exDotMavt(4, RefDmvt, s) then
        begin
            EdMA.Text := '';
            Exit;
        end;
        EdMA.Text := s;
    end;

    mavt := edMA.Text;
    makho := posMakho;
    ngay := Now;
    with CHECK_TONKHO do
    begin
        if Active then
            Close;

        if mavt <> '' then
        begin
            Parameters[1].Value := mavt;
            Parameters[2].Value := makho;
            Parameters[3].Value := ngay;
            Parameters[4].Value := TGuidEx.ToString(TGUID.Empty);
            Open;
        end;
    end;
    EdMa.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCommoInfo.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

end.
