﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosCamung;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants, Types, Windows,
  Db, wwDBGrid2, ADODB, Buttons, StdCtrls, Grids, Wwdbigrd, Wwdbgrid,
  AdvSmoothTileList, AdvSmoothTileListEx, ExtCtrls, HTMLabel, AppEvnts,
  Vcl.ActnList, RzButton;

type
  TFrmPosCamung = class(TForm)
    Panel20: TPanel;
    Panel1: TPanel;
    TtNganhhang: TAdvSmoothTileListEx;
    QrNhom: TADOQuery;
    Panel2: TPanel;
    LbCurSoluong: TLabel;
    HtmlCurSoluong: THTMLabel;
    ApplicationEvents1: TApplicationEvents;
    TtVattu: TAdvSmoothTileListEx;
    ActionList1: TActionList;
    CmdSoluong: TAction;
    CmdClose: TAction;
    BtReturn: TRzBitBtn;
    BtClose: TRzBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure TtNganhhangTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure TtVattuTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure TtVattuTileDblClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdSoluongExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
  private
    procedure tbAddUnit;
    procedure tbAddGroup;
  public
    function Execute(pQuery: TCustomADODataSet): Boolean;
  end;

var
  FrmPosCamung: TFrmPosCamung;

implementation

{$R *.DFM}

uses
    isLib, PosCommon, isDb, GuidEx, isCommon, PosMain, isMsg, KeypadNumeric;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
var
	s: String;
begin
	if FrmMain.mDefSoluong > 1 then
		s := FloatToStr(FrmMain.mDefSoluong)
	else
		s := '';

    if LbCurSoluong.Caption <> s then
    begin
	    LbCurSoluong.Caption := s;
        if s = '' then
	        HtmlCurSoluong.HTMLText.Text := ''
        else
            HtmlCurSoluong.HTMLText.Text := Format(
                '<P align="left"><FONT face="Tahoma" size="13" color="#004080"><b>(SL)</b>  ' +
                '<FONT size="18"><b>%s</b></FONT></FONT></P>',
                [s]);
    end;
end;

procedure TFrmPosCamung.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.CmdSoluongExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmKeypadNumeric, FrmKeypadNumeric);
    FrmKeypadNumeric.Execute(FrmMain.mDefSoluong);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosCamung.Execute(pQuery: TCustomADODataSet): Boolean;
begin
    Result := ShowModal = mrOk;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.FormShow(Sender: TObject);
var
    r: TRect;
    x: Integer;
begin
    TMyForm(Self).Init;

    SystemParametersInfo(SPI_GETWORKAREA, 0, r, 0);
    Left := r.Right - Width;
    Top := r.Bottom - Height;

    QrNhom.Open;

    x := QrNhom.RecordCount;

    if x = 0 then
        ErrMsg('Chưa cấu hình nhóm hàng Cảm ứng. Vui lòng liên hệ Quản trị phần mềm.');

    if x > round(Width/100) then
        x :=  round(Width/100);
    //Touch
    with TtNganhhang do
    begin
        Columns := x;
        Rows := 1;
    end;

    with TtVattu do
    begin
        Columns := round(Width/150);
        Rows := Round(Height/52);
    end;

    tbAddGroup;
    tbAddUnit;

    TtNganhhang.SelectTile(0);
    TtNganhhangTileClick(TtNganhhang, TtNganhhang.Tiles[0], tsNormal);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then
        Close
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.tbAddGroup;
var
    mTiles: TAdvSmoothTile;

    procedure AddGroup2(pTiles: TAdvSmoothTile);
    begin
//        pTiles.SubTiles.Clear;
//        with pTiles.SubTiles.Add do
//        begin
//            //Content.Image := PicBan.Items[2].Picture;
//            Data := pTiles.Data;
//            Content.Text := '* ' + #13 + pTiles.Content.Text;
//            Content.TextPosition := tpTopCenter;
//
//            Tag := 0;
//        end;

        with QrNhom do
        begin
            First;

            while not Eof do
            begin
                with pTiles.SubTiles.Add do
                begin
                    // Dai dien
                    //Content.Image := PicBan.Items[2].Picture;
                    Data := FieldByName('MACAMUNG').AsString;
                    Content.Text := FieldByName('TENCAMUNG').AsString;
                    Content.TextPosition := tpTopCenter;
                    // Chi tiet
                    ContentMaximized.Text :=
                        'Mã:   ' + FieldByName('MACAMUNG').AsString + #13 +
                        'Tên:  ' + FieldByName('TENCAMUNG').AsString;

                    ContentMaximized.TextPosition := tpCenterCenter;
                    Tag := 1;
                end;

                Next;
            end;
        end;
    end;
begin
    TtNganhhang.BeginUpdate;
    TtNganhhang.GoBack;
    TtNganhhang.Tiles.Clear;
//    mTiles := TtNganhhang.Tiles.Add;
//    with mTiles do
//    begin
//        //Content.Image := PicBan.Items[2].Picture;
//        Data := '';
//        Content.Text := '*';
//        Content.TextPosition := tpTopCenter;
//        ContentMaximized.Text := '*';
//        Tag := 0;
//    end;

    with QrNhom do
    begin
        First;

        while not Eof do
        begin
            mTiles := TtNganhhang.Tiles.Add;
            with mTiles do
            begin
                // Dai dien
                //Content.Image := PicBan.Items[2].Picture;
                Data := FieldByName('MACAMUNG').AsString;
                Content.Text := FieldByName('TENCAMUNG').AsString;
                Content.TextPosition := tpTopCenter;
                // Chi tiet
                ContentMaximized.Text :=
                    'Mã nhóm:   ' + FieldByName('MACAMUNG').AsString + #13 +
                    'Tên nhóm:  ' + FieldByName('TENCAMUNG').AsString;

                ContentMaximized.TextPosition := tpCenterCenter;
                Tag := 0;
            end;
//            AddGroup2(mTiles);
            Next;
        end;
    end;
    TtNganhhang.FirstPage;
    TtNganhhang.EndUpdate;
end;

procedure TFrmPosCamung.tbAddUnit;
var
  sStr: TStream;
  bBlob: TBlobField;
begin
    TtVattu.Tiles.Clear;
    with FrmMain.QrDMVT_CAMUNG do
    begin
        First;

        while not Eof do
        begin
            with TtVattu.Tiles.Add do
            begin
                // Dai dien
//                if bImage then
//                begin
//                    bBlob := FieldByName('HINH') as TBlobField;
//                    sStr := CreateBlobStream(bBlob, bmRead);
//                    try
//                        sStr.Position := 0;
//                        if bBlob.IsNull then
//                            Content.Image := PicBan.Items[0].Picture
//                        else
//                            Content.Image.LoadFromStream(sStr);
//                    finally
//                        sStr.Free;
//                    end;
//                end;

                Data := FieldByName('MAVT').AsString;
                Content.Text := FieldByName('TENVT_CAMUNG').AsString;
                Content.TextPosition := tpTopCenter;
                //Content.TextTop := mTop;

                StatusIndicator := FloatToStrF(FieldByName('GiaBanChuaThue').AsFloat, ffNumber, 10, 0);

                StatusIndicatorLeft :=
                    -5 - Length(FloatToStrF(FieldByName('GiaBanChuaThue').AsFloat, ffNumber, 10, 0))*6 div 2;
                StatusIndicatorTop := 36;

                // Chi tiet1
                ContentMaximized.Text :=
                    'Mã:       ' + FieldByName('MAVT').AsString + #13 +
                    'Giá bán:  ' + FloatToStrF(FieldByName('GiaBanChuaThue').AsFloat, ffNumber, 10, 0)+#13 +
                    'Tên: ' + FieldByName('TENVT').AsString + #13;
            end;
            Next;
        end;
    end;

    TtVattu.FirstPage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.TtNganhhangTileClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s, sf: string;
begin
    s := Tile.Data;
    if s <> '' then
        sf := 'CAMUNG_NHOM=' + QuotedStr(s);

    FrmMain.QrDMVT_CAMUNG.Filter := sf;
    FrmMain.QrDMVT_CAMUNG.Filtered := True;
    tbAddUnit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.TtVattuTileClick(Sender: TObject; Tile: TAdvSmoothTile;
  State: TTileState);
begin
    FrmMain.QrDMVT_CAMUNG.Locate('MAVT', Tile.Data, []);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosCamung.TtVattuTileDblClick(Sender: TObject;
  Tile: TAdvSmoothTile; State: TTileState);
var
    s: String;
begin
    s := FrmMain.QrDMVT_CAMUNG.FieldByName('MAVT').AsString;
    if not (State in [tsMaximized]) then
    begin
        with FrmMain.QrCTBH do
        begin
        // Mã cũ
            if Locate('MAVT;MABO', VarArrayOf([s, Null]), []) then
            begin
                FrmMain.GetSalingPrice(s);

                FrmMain.ItemSetQty(FieldByName('SOLUONG').AsFloat + 1)
            end
            // Mã mới
            else
            begin
                // Sai ma
                if not FrmMain.GetSalingPrice(s) then
                begin
                    FrmMain.InvalidAction(RS_POS_CODE_FAIL);
                    Exit;
                end;

                // Chua co gia ban
                if FrmMain.curGiaban = 0 then
                begin
//                    Beep;
                    ErrMsg(RS_POS_NOT_PRICE);
                    Exit;
                end;

                FrmMain.ItemAddNew(s)
            end;
        end;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
