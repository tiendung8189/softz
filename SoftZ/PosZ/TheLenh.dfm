﻿object FrmThelenh: TFrmThelenh
  Left = 360
  Top = 483
  HelpContext = 1
  ActiveControl = EdMA
  BorderStyle = bsDialog
  Caption = 'Th'#7867' L'#7879'nh'
  ClientHeight = 86
  ClientWidth = 352
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    352
    86)
  PixelsPerInch = 96
  TextHeight = 16
  object EdMA: TEdit
    Left = 8
    Top = 8
    Width = 335
    Height = 45
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -56
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    MaxLength = 50
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
    Text = '*****'
    OnEnter = EdMAEnter
    OnKeyPress = EdMAKeyPress
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 64
    Width = 352
    Height = 22
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Alignment = taCenter
        Width = 50
      end>
    UseSystemFont = False
  end
  object UPDATE_THELENH: TADOCommand
    CommandText = 'Update DM_LOCATION set THELENH = :THELENH where LOC = :LOC'
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'THELENH'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 255
        Value = Null
      end
      item
        Name = 'LOC'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    Left = 200
    Top = 4
  end
end
