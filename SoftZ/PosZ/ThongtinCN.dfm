object FrmThongtinCN: TFrmThongtinCN
  Left = 561
  Top = 513
  HelpContext = 1
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Th'#244'ng Tin Giao H'#224'ng'
  ClientHeight = 665
  ClientWidth = 775
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 605
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 224
      Width = 775
      Height = 162
      Align = alBottom
      Caption = ' Th'#244'ng tin h'#243'a '#273#417'n '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        775
        162)
      object Label6: TLabel
        Left = 122
        Top = 28
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'T'#234'n '#273#417'n v'#7883
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label7: TLabel
        Left = 760
        Top = 24
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object Label1: TLabel
        Left = 80
        Top = 116
        Width = 149
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' h'#243'a '#273#417'n'
        FocusControl = wwDBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 17
      end
      object Label4: TLabel
        Left = 126
        Top = 72
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'M'#227' s'#7889' thu'#7871
        FocusControl = DBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 63
      end
      object EdTEN: TwwDBEdit
        Tag = 1
        Left = 245
        Top = 24
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        AutoSize = False
        Color = 15794175
        Ctl3D = False
        DataField = 'CN_TENDV'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TwwDBEdit
        Left = 245
        Top = 112
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI_HD'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit1: TwwDBEdit
        Left = 245
        Top = 68
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_MST'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 775
      Height = 224
      Align = alClient
      Caption = ' Th'#244'ng tin giao h'#224'ng '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        775
        224)
      object Label15: TLabel
        Left = 178
        Top = 167
        Width = 51
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Email'
        FocusControl = wwDBEdit4
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 115
      end
      object Label2: TLabel
        Left = 49
        Top = 74
        Width = 180
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' giao h'#224'ng'
        FocusControl = DBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 92
        Top = 29
        Width = 137
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#432#7901'i li'#234'n h'#7879
        FocusControl = DBEdit2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 122
        Top = 122
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272'i'#7879'n tho'#7841'i'
        FocusControl = DBEdit5
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label10: TLabel
        Left = 760
        Top = 114
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label8: TLabel
        Left = 760
        Top = 26
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label9: TLabel
        Left = 760
        Top = 70
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object DBEdit1: TwwDBEdit
        Left = 245
        Top = 70
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit2: TwwDBEdit
        Left = 245
        Top = 26
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_LIENHE'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit5: TwwDBEdit
        Left = 245
        Top = 114
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DTHOAI'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit4: TwwDBEdit
        Left = 245
        Top = 158
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_EMAIL'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 386
      Width = 775
      Height = 219
      Align = alBottom
      Caption = ' Th'#244'ng tin t'#224'i kho'#7843'n ng'#226'n h'#224'ng '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 2
      DesignSize = (
        775
        219)
      object Label12: TLabel
        Left = 99
        Top = 76
        Width = 130
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ch'#7911' t'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 36
      end
      object Label13: TLabel
        Left = 151
        Top = 124
        Width = 78
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7841'i di'#7879'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 88
      end
      object Label14: TLabel
        Left = 126
        Top = 180
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#226'n h'#224'ng'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 63
      end
      object Label30: TLabel
        Left = 126
        Top = 33
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'T'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 63
      end
      object Label11: TLabel
        Left = 760
        Top = 29
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object EdTENDT: TwwDBEdit
        Left = 245
        Top = 72
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_TENTK'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit2: TwwDBEdit
        Left = 245
        Top = 120
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_DAIDIEN'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit3: TwwDBEdit
        Left = 245
        Top = 165
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_NGANHANG'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBLookupCombo11: TwwDBLookupCombo
        Left = 245
        Top = 29
        Width = 312
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'TENTK'#9'28'#9'TENTK'#9'F'
          'MATK'#9'11'#9'MATK'#11#9)
        DataField = 'CN_MATK'
        DataSource = FrmMain.DsBH
        LookupTable = DataMain.QrDMTK
        LookupField = 'MATK'
        Options = [loColLines]
        Style = csDropDownList
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
      end
      object wwDBLookupCombo12: TwwDBLookupCombo
        Left = 560
        Top = 29
        Width = 194
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        BorderStyle = bsNone
        DropDownAlignment = taRightJustify
        Selected.Strings = (
          'MATK'#9'11'#9'MATK'#11'F'
          'TENTK'#9'28'#9'TENTK'#9#9)
        DataField = 'CN_MATK'
        DataSource = FrmMain.DsBH
        LookupTable = DataMain.QrDMTK
        LookupField = 'MATK'
        Options = [loColLines]
        Style = csDropDownList
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 605
    Width = 775
    Height = 60
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      775
      60)
    object BtClose: TRzBitBtn
      Left = 660
      Top = 4
      Width = 106
      Height = 50
      Cursor = 1
      FrameColor = 7617536
      ModalResult = 2
      ShowFocusRect = False
      Anchors = [akRight, akBottom]
      Caption = 'Tho'#225't'#13'Esc'
      Color = 15791348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      HotTrack = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      Glyph.Data = {
        C2040000424DC204000000000000420000002800000018000000180000000100
        10000300000080040000202E0000202E00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
        924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
        6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
        FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
        261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
        FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
        F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
        E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
        8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
        271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
        A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
        0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
        2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F}
      Margin = 1
      Spacing = 1
    end
    object CmdReturn: TRzBitBtn
      Left = 548
      Top = 4
      Width = 106
      Height = 50
      Cursor = 1
      Hint = 'Ti'#7871'p t'#7909'c'
      FrameColor = 7617536
      ShowFocusRect = False
      Anchors = [akRight, akBottom]
      Caption = 'Ti'#7871'p t'#7909'c'#13'F3'
      Color = 15791348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      HotTrack = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = CmdOKExecute
      Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000004280B3D09631B950C92
        29DE0EA62EFF0FAE31FF0FAE31FF0EA62EFF0C9229DE09631B9504280B3D0000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000031D082C0DA02AFD11B133FF18C344FF1FCE
        4AFF20D14EFF20D04DFF20D04DFF20D14EFF1FCE4AFF18C344FF11B133FF0DA0
        2AFD031D082C0000000000000000000000000000000000000000000000000000
        00000000000000000000085D17950EA32AFF1DC945FF20D04DFF1FCF4CFF1ECE
        4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECC4BFF1ECE4BFF1FCF4CFF20D14DFF1DC9
        45FF0EA32AFF085D179500000000000000000000000000000000000000000000
        000000000000076919AB10AE31FF1FD150FF1FCF50FF1ECD4FFF1DCF51FF1FD2
        4EFF1FD24EFF1DD04CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECD50FF20D0
        51FF1FD251FF10AE31FF076919AB000000000000000000000000000000000000
        00000759149511AA30FF20D353FF1ECE4FFF1ECD4EFF1CD052FF2BC04BFF0093
        0FFF009311FF2BC04CFF1DD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE4AFF1ECE
        4AFF1ECE4FFF21D454FF11AA30FF07591495000000000000000000000000021A
        062C0D9922FF1FD150FF1ECE50FF1ECD4FFF1FD052FF14BA3DFF008F01FFFFFF
        FFFFBFEBCBFF009008FF2BC04CFF1FD14CFF1ECE4BFF1ECE4BFF1ECE4BFF1ECE
        4AFF1ECD4FFF1ECE50FF1FD251FF0D9922FF021A062C00000000000000000B8C
        19FD19C748FF1ECF52FF1DCD50FF1ED153FF16BF42FF008E01FFFFFDFFFFFBF7
        F9FFFFFFFFFFBBE9C8FF009108FF12BB3EFF1FD14EFF1ECE4BFF1ECE4BFF1ECE
        4AFF1DCD51FF1DCD51FF1FD053FF19C748FF0B8C19FD000000000322063D0F9F
        25FF1FD459FF1DCE53FF1ED358FF0FA72CFF008F01FFFFFBFFFFEFF1F1FFF2F4
        F2FFFAF8FAFFFFFFFFFFB5E6C0FF00910AFF13BB3EFF1FD14DFF1ECE4AFF1ECE
        4AFF1ECE4AFF1ECE49FF1DCD53FF20D55AFF0F9F25FF0322063D064F0D9515BB
        3EFF1CD058FF1CD159FF0FAD32FF009103FFFFF7FFFFEEEDEEFFF1F1F1FFF5F4
        F5FFF8F7F8FFFCFAFCFFFFFFFFFFAEE2BAFF00910AFF13BB3DFF1DD25DFF1CCF
        58FF1CCF58FF1CD25AFF1CCF57FF1CD159FF15BB3EFF064F0D9507700BDE31D4
        6AFF17CD53FF15BB42FF009102FFFFF2FFFFEAE9E9FFEFECEFFFFFF8FFFFFFF7
        FFFFFFFDFFFFFDFBFDFFFEFEFEFFFFFFFFFFABE2B8FF00910AFF14BB3CFF17D5
        5FFF16D25BFF16D15AFF1ED25BFF13CC4FFF31D46AFF07700BDE027C08FF77EA
        AAFF32D56FFF039913FFFFF1FFFFE2E3E1FFEAE8EAFFFFF7FFFF008D00FF00B5
        2BFF008B00FFFFFFFFFFFEFCFEFFFFFFFFFFFFFFFFFFABE2B8FF00910AFF14BB
        3CFF17D45FFF16D15AFF11D056FF2DD46BFF77EAAAFF027C08FF0A8712FF6CE8
        A6FF6BE09FFF019406FFFFEDFFFFF1E6F1FFFFF5FFFF008C00FF1CD860FF1ED4
        5DFF1CD860FF008B00FFFFFFFFFFFFFCFEFFFFFEFFFFFFFFFFFFABE2B8FF0091
        0AFF13BB3CFF1CD45BFF30DB74FF6BE09FFF6CE8A6FF0A800FFF037909FF6CE8
        A9FF63DE9BFF66E1A2FF008C00FFE8DFE2FF008D00FF13CB4DFF18D256FF18D0
        54FF18D155FF17D759FF008A00FFFFFFFFFFFEFCFEFFFEFEFEFFFFFFFFFFABE2
        B9FF009109FF2ABF48FF6CE4A6FF63DE9BFF6CE8A9FF037909FF006800FF68E8
        A6FF61DF9EFF67E5A8FF64E8B2FF29BC59FF50D791FF57D99AFF56D898FF55D8
        98FF56D898FF56D999FF58DFA1FF008900FFFFFFFFFFFBFAFBFFFBFBFAFFFFFF
        FFFFABE3B9FF008F05FF67E4A5FF61DF9EFF68E8A6FF006800FF005F00DE71DC
        9EFF5CE19DFF62E1A2FF54DB99FF61E4AAFF60E2A8FF5FE2A7FF5EDEA0FF68DE
        A3FF5FE2A6FF5FE2A6FF60E2A8FF67EAB6FF008700FFFFFFFFFFF9F8F9FFF7F7
        F8FFFFFDFFFFADE4BAFF009006FF5DE4A1FF71DC9EFF005F00DE0342019544B3
        61FF8DEDC2FF58E19DFF62E4A5FF5EE1A6FF5EE1A5FF5EE1A6FF5EE1A6FF5DE1
        A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF61E5ABFF008900FFFFFFFFFFF6F4
        F6FFF2F3F2FFFFFFFFFF089712FF92F0C8FF44B361FF03420195021C023D0878
        11FFA2F0D1FF74E5B1FF55DF9CFF5EE1A0FF60E1A1FF5FE1A1FF5EE1A5FF5EE1
        A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A8FF63E6AFFF008900FFFFFF
        FFFFFFF6FFFFFFFDFFFF37B956FFA2F0D1FF087811FF021C023D000000000066
        00FD52C278FFA9F1D6FF85E8BCFF54E09DFF5AE0A2FF5FE1A6FF5EE1A5FF5EE1
        A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5EE1A5FF5FE2A7FF68EAB6FF008B
        00FF23BF5DFF2AAA3BFFADF3DBFF52C278FF006600FD00000000000000000113
        012C006600FF79E2AEFFA2EFD1FF96EBC9FF6EE3B2FF4AE09BFF5BE0A3FF5CE0
        A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE1A4FF5CE0A4FF5BE0A3FF4CE19DFF73E8
        BBFF9DEED0FFA6F1D5FF79E2AEFF006600FF0113012C00000000000000000000
        000003400195107D18FF7FE9BAFF9DEFD2FF9BECCBFF97EBCAFF7DEAC2FF6DE3
        B0FF6DE3B1FF6DE3B1FF6DE3B2FF6DE3B2FF6DE3B1FF7DEAC2FF99EBCBFF9DED
        CCFF9EEFD3FF7FE9BAFF107D18FF034001950000000000000000000000000000
        000000000000004400AB0F7817FF6DDDA7FF98EED3FF99ECCFFF98EBCDFF9EEC
        D2FFA2EED6FFA4EED8FF9EEDCCFFA3EED8FF9DEDCDFF99EBCDFF9AECD0FF98EE
        D3FF6DDDA7FF0F7817FF004400AB000000000000000000000000000000000000
        00000000000000000000043E0095006000FF46BC74FF81E7C1FF97EFD4FF9AEE
        D5FF97ECD1FF96ECCFFF96ECCFFF97ECD1FF9AEED5FF97EFD4FF81E7C1FF46BC
        74FF006000FF043E009500000000000000000000000000000000000000000000
        00000000000000000000000000000113012C005B00FD0B730FFF31A554FF5BCE
        90FF75E1B4FF86EAC4FF86EAC4FF75E1B4FF5BCE90FF31A554FF0B730FFF005B
        00FD0113012C0000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000011A013D013C0095004D
        00DE005000FF006400FF006400FF005000FF004D00DE013C0095011A013D0000
        0000000000000000000000000000000000000000000000000000}
      Margin = 1
      Spacing = 1
    end
    object BtnDelivery: TRzBitBtn
      Left = 435
      Top = 4
      Width = 106
      Height = 50
      Cursor = 1
      FrameColor = 7617536
      ShowFocusRect = False
      Action = CmdCancel
      Anchors = [akRight, akBottom]
      Caption = 'H'#7911'y giao h'#224'ng'
      Color = 15791348
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      HotTrack = True
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000A0C7CBD1011BAFD1011B9FD1011B9FD1011B9FD1011B9FD1011B9FD1011
        B9FD1011B9FD1011B9FD1011B9FD1011B9FD1011B9FD1011B9FD1011B9FD1011
        B9FD1011B9FD1011B9FD1011BAFD0A0B76B40000000000000000000000000000
        02031011BDFF1314DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314
        DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314DFFF1314
        DFFF1314DFFF1314DFFF1314DFFF1011BAFD0000000000000000000000000000
        02041011BDFF1314DEFF1314DEFF1314DEFF1314DEFF1314DEFF1314DEFF1314
        DEFF1314DEFF1314DEFF1314DEFF1314DEFF1314DDFF1314DDFF1314DDFF1314
        DDFF1314DDFF1314DDFF1314DDFF1011B9FD0000000000000000000000000000
        02041011BCFF1314DCFF1314DCFF1314DCFF1314DCFF3131E0FF1314DBFF1314
        DBFF1314DBFF1314DBFF1314DBFF1314DBFF1314DBFF1314DBFF3131E0FF1314
        DBFF1314DBFF1314DBFF1314DBFF1011B8FD0000000000000000000000000000
        02041011BAFF1314D9FF1314D9FF1314D9FF4E4FE3FFF5F5FCFF8889EAFF1213
        D9FF1213D9FF1213D9FF1213D9FF1213D9FF1213D9FF8C8CECFFF4F4FCFF4D4D
        E2FF1213D8FF1213D8FF1213D8FF0F10B6FD0000000000000000000000000000
        02041011B9FF1213D7FF1213D7FF3333DCFFF5F5FCFFFFFFFFFFFFFFFFFF8889
        E9FF1213D6FF1213D6FF1213D6FF1213D6FF8C8CEAFFFFFFFFFFFFFFFFFFF4F4
        FCFF3031DBFF1213D6FF1213D6FF0F10B5FD0000000000000000000000000000
        02041011B8FF1213D4FF1213D4FF1212CCFF8E8ED0FFFFFFFFFFFFFFFFFFFFFF
        FFFF8889E8FF1213D4FF1213D4FF8C8CE9FFFFFFFFFFFFFFFFFFFFFFFFFF898A
        CEFF1213CCFF1213D3FF1213D3FF0F10B4FD0000000000000000000000000000
        02041011B7FF1213D2FF1213D2FF1213D2FF1112C3FF8E8ED0FFFFFFFFFFFFFF
        FFFFFFFFFFFF8889E7FF8C8CE8FFFFFFFFFFFFFFFFFFFFFFFFFF8A8ACEFF1012
        C2FF1213D1FF1213D1FF1213D1FF0F10B3FD0000000000000000000000000000
        02041011B5FF1213CFFF1213CFFF1213CFFF1213CFFF1112C0FF8E8ECFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8A8ACEFF1012C1FF1213
        CFFF1213CFFF1213CFFF1213CEFF0F10B1FD0000000000000000000000000000
        02040F10B4FF1112CDFF1112CDFF1112CDFF1112CDFF1112CDFF1011BFFF8E8E
        CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8A8ACEFF0F11BFFF1112CCFF1112
        CCFF1112CCFF1112CCFF1112CCFF0F10B0FD0000000000000000000000000000
        02040F10B3FF1112CBFF1112CBFF1112CAFF1112CAFF1112CAFF1112CAFF8B8B
        DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8788DDFF1112CAFF1112CAFF1112
        CAFF1112CAFF1112CAFF1112CAFF0F10AFFD0000000000000000000000000000
        02040F10B1FF1112C8FF1112C8FF1112C8FF1112C8FF1112C8FF8B8CE3FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8888E2FF1112C7FF1112
        C7FF1112C7FF1112C7FF1112C7FF0F10AEFD0000000000000000000000000000
        02040F10B0FF1112C6FF1112C6FF1112C6FF1112C5FF8B8CE2FFFFFFFFFFFFFF
        FFFFFFFFFFFF8A8ACEFF8D8ECFFFFFFFFFFFFFFFFFFFFFFFFFFF8888E1FF1112
        C5FF1112C5FF1112C5FF1112C5FF0F10ADFD0000000000000000000000000000
        02040F10AFFF1112C3FF1112C3FF1112C3FF8B8CE1FFFFFFFFFFFFFFFFFFFFFF
        FFFF8A8ACEFF0F11B8FF1011B8FF8E8ECFFFFFFFFFFFFFFFFFFFFFFFFFFF8888
        E0FF1112C2FF1112C2FF1112C2FF0F10ABFD0000000000000000000000000000
        02040F10AEFF1011C1FF1011C1FF3132C8FFF6F7FCFFFFFFFFFFFFFFFFFF8A8A
        CEFF0F10B6FF1011C0FF1011C0FF1010B6FF8E8ECFFFFFFFFFFFFFFFFFFFF5F5
        FBFF2E2FC7FF1011C0FF1011C0FF0E0FAAFD0000000000000000000000000000
        02040F10ACFF1011BEFF1011BEFF1010BBFF4E4FB9FFF6F6FBFF8A8ACEFF0F10
        B4FF1011BEFF1011BEFF1011BEFF1011BEFF1010B4FF8E8ECFFFF5F5FBFF4B4C
        B7FF1011BAFF1011BDFF1011BDFF0E0FA9FD0000000000000000000000000000
        02040F10ABFF1011BCFF1011BCFF1011BCFF0F10B9FF2F2FACFF0F10B3FF1011
        BBFF1011BBFF1011BBFF1011BBFF1011BBFF1011BBFF1010B2FF2F2FADFF1010
        B8FF1011BBFF1011BBFF1011BBFF0E0FA8FD0000000000000000000000000000
        02040F10AAFF1011B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011
        B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011B9FF1011
        B9FF1011B8FF1011B8FF1011B8FF0E0FA7FD0000000000000000000000000000
        02030F10A9FF1011B7FF1011B7FF1011B7FF1011B7FF1011B7FF1011B7FF1011
        B7FF1011B6FF1011B6FF1011B6FF1011B6FF1011B6FF1011B6FF1011B6FF0F10
        B6FF0F10B6FF0F10B6FF0F10B6FF0E0FA6FD0000000000000000000000000000
        00000A0A7AC30E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0F
        A7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0FA7FF0E0F
        A7FF0E0FA7FF0E0FA6FF0E0FA6FF090A74BD0000000000000000000000000000
        0000000000000000020300000204000002040000020400000204000002040000
        0204000002040000020400000204000002040000020400000204000002040000
        0204000002040000020400000203000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      Margin = 1
      Spacing = 1
    end
  end
  object MyActionList: TActionList
    Left = 316
    Top = 328
    object CmdOK: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      ShortCut = 114
      OnExecute = CmdOKExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      OnExecute = CmdCloseExecute
    end
    object CmdCancel: TAction
      Caption = 'H'#7911'y giao h'#224'ng'
      OnExecute = CmdCancelExecute
    end
  end
end
