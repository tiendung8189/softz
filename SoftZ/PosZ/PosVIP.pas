﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosVIP;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  DB, ADODB, StdCtrls, Buttons, wwdblook, wwdbdatetimepicker,
  DBCtrls, ExtCtrls, isPanel, wwdbedit, ActnList, Mask, pngimage,
  Grids, Wwdbigrd, Wwdbgrid, wwDBGrid2, AdvEdit, DBAdvEd;

type
  TFrmPosVIP = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    QrDMVIP: TADOQuery;
    QrDMVIPMASO: TWideStringField;
    QrDMVIPDCHI: TWideStringField;
    QrDMVIPEMAIL: TWideStringField;
    QrDMVIPTL_CK: TFloatField;
    QrDMVIPMAVIP: TWideStringField;
    QrDMVIPCREATE_DATE: TDateTimeField;
    QrDMVIPUPDATE_DATE: TDateTimeField;
    DsDMVIP: TDataSource;
    isPanel1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    DBEdit6: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    PD2: TisPanel;
    TntLabel1: TLabel;
    Label25: TLabel;
    TntLabel4: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBEdit11: TwwDBEdit;
    CbLoai: TwwDBLookupCombo;
    ActionList1: TActionList;
    CmdSave: TAction;
    QrDMVIPDOANHSO: TFloatField;
    QrDMVIPNGAYDSO: TDateTimeField;
    QrDMVIPNGAYSINH: TDateTimeField;
    QrDMVIPCREATE_BY: TIntegerField;
    QrDMVIPUPDATE_BY: TIntegerField;
    QrDMVIPLOAI: TWideStringField;
    QrDMVIPNGAY_THAMGIA: TDateTimeField;
    QrDMVIPLOC_THAMGIA: TWideStringField;
    QrDMVIPHOTEN: TWideStringField;
    QrDMVIPSOTIEN_MUA: TFloatField;
    QrDMVIPSOTIEN_TRA: TFloatField;
    QrDMVIPDOANHSO_DAU: TFloatField;
    QrDMVIPDOANHSO_MUA: TFloatField;
    QrDMVIPTINHTRANG: TWideStringField;
    QrDMVIPCMND: TWideStringField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label5: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Panel1: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    EdSearch: TAdvEdit;
    Panel3: TPanel;
    Panel4: TPanel;
    GrList: TwwDBGrid2;
    CmdRefesh: TAction;
    CmdNew: TAction;
    wwDBEdit2: TwwDBEdit;
    spGET_VIP_NEW: TADOCommand;
    BtSearch: TBitBtn;
    BtNew: TBitBtn;
    BtClear: TBitBtn;
    CmdSearch: TAction;
    CmdClear: TAction;
    EdHo: TDBAdvEdit;
    EdDchi: TDBAdvEdit;
    QrDMVIPDTHOAI: TWideStringField;
    BitBtn1: TBitBtn;
    CmdSaveCapMoi: TAction;
    EdDienThoai: TDBAdvEdit;
    procedure QrDMVIPBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure exDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMVIPBeforeEdit(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure EdSearchExit(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure QrDMVIPBeforeInsert(DataSet: TDataSet);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSaveCapMoiExecute(Sender: TObject);
  private
    mSQL, mSearch, mMa: String;
    mNew, fixCode: Boolean;
  public
  	function  Execute(var id: String; act: Integer): String;
  end;

var
  FrmPosVIP: TFrmPosVIP;

implementation

uses
	isDB, isLib, PosCommon, isCommon, MainData, isMsg, isStr, exCommon;

const
    FORM_CODE = 'POS_DM_VIP';
{$R *.dfm}

(*==============================================================================
** Arguments:
**	id		MAVIP
**	act		0:	register; 1: view / edit
**------------------------------------------------------------------------------
** Return value:
**	VIP full name = last + ' ' + first name
** or
**	'' if not registered
**------------------------------------------------------------------------------
*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosVIP.Execute(var id: String; act: Integer): String;
begin
    EdSearch.Text := id;
    if ShowModal = mrOk then
    begin
        with QrDMVIP do
        begin
            id := FieldByName('MAVIP').AsString;
            Result := FieldByName('HOTEN').AsString;
        end;
    end;

    mNew := False;
    CloseDataSets([QrDMVIP, DataMain.QrLOAI_VIP_LE]);

    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrDMVIP, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormCreate(Sender: TObject);
begin
    mSQL := QrDMVIP.SQL.Text;
     SetCustomGrid([FORM_CODE], [GrList]);
    fixCode := setCodeLength( FORM_CODE, QrDMVIP.FieldByName('DTHOAI'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.FormShow(Sender: TObject);
begin
    OpenDataSets([DataMain.QrLOAI_VIP_LE]);

	SetDisplayFormat(QrDMVIP, sysCurFmt);
    SetDisplayFormat(QrDMVIP, ['NGAYDSO'], ShortDateFormat + ' hh:nn');

    SetDictionary(QrDMVIP, 'DM_VIP', Nil);

    CbLoai.ReadOnly := sysKhttLe > 1;
    mNew := False;
    CmdRefesh.Execute;
    EdSearch.SetFocus;
    EdSearch.SelectAll;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPBeforeEdit(DataSet: TDataSet);
var
	s: String;
begin
	with QrDMVIP do
    begin
    	s := FieldByName('TINHTRANG').AsString;
        if (s <> '01') or (FieldByName('MAVIP').AsString = '') then
        	Abort;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPBeforeInsert(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.QrDMVIPBeforePost(DataSet: TDataSet);
begin
	with QrDMVIP do
	begin
        if BlankConfirm(QrDMVIP, ['HOTEN', 'DCHI', 'DTHOAI']) then
            Abort;

        FieldByName('LOC_THAMGIA').AsString := sysLoc;

//        if DataMain.VIP_BarcodeIsUsed(FieldByName('DTHOAI').AsString) then
//            Abort;

    end;
	SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdSaveCapMoiExecute(Sender: TObject);
begin
    with QrDMVIP do
    begin
        if not (State in [dsBrowse]) then
        	Post;
    end;
end;

procedure TFrmPosVIP.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
	b, bBrowse, bInsert: Boolean;
begin
    with QrDMVIP do
    begin
        if not Active then
        	Exit;
        bBrowse := (State in [dsBrowse]);
        b := (FieldByName('MAVIP').AsString = '')
                and (FieldByName('HOTEN').AsString <> '')
    end;
    CmdSave.Enabled :=bBrowse or b;
    CmdNew.Enabled := bBrowse;
    CmdSearch.Enabled := bBrowse;
    CmdSaveCapMoi.Enabled := not bBrowse;
    if bBrowse then
        CmdClear.Caption := 'Xóa chuỗi'
    else
        CmdClear.Caption := ' Bỏ qua  ';
    EdSearch.ReadOnly := not bBrowse;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdClearExecute(Sender: TObject);
begin
    mMa := '';
    EdSearch.Text := '';
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_LIMIT_MAVIP = 'Đã hết Mã số có thể cấp mới. #13Vui lòng liên hệ Trung tâm thẻ hoặc Quản trị hệ thống.';
    RS_LIMIT_MAVIP_SOON = 'Số mã có thể cấp mới còn lại "%d" mã. #13Vui lòng liên hệ Trung tâm thẻ hoặc Quản trị hệ thống.';
procedure TFrmPosVIP.CmdNewExecute(Sender: TObject);
var
    s, ma: String;
    n: Integer;
begin
    s := EdSearch.Text;
    if s <> '' then
        ma := s
    else
    begin
        with spGET_VIP_NEW do
        begin
            Parameters[1].Value := sysLoc;
            Execute;
            if Parameters[0].Value = 1 then
            begin
                ErrMsg(RS_LIMIT_MAVIP);
                Exit;
            end else
            begin
                ma := Parameters[2].Value;
                n := Parameters[3].Value;
                if n < 6 then
                    Msg(Format(RS_LIMIT_MAVIP_SOON, [n]));
            end;
        end;
    end;

    mMa := ma;
    EdSearch.Text := '';
    CmdRefesh.Execute;

    with QrDMVIP do
    begin
        if IsEmpty or (QrDMVIP.FieldByName('TINHTRANG').AsString <> '01') then
        begin
            EdSearch.Text := mMa;
            EdSearch.SelectAll;

            ErrMsg(RS_INVALID_CARD);
            Abort;
        end else
        begin
            Edit;
            FieldByName('NGAY_THAMGIA').AsDateTime := Date;
            FieldByName('TINHTRANG').AsString := '02';

            EdHo.SetFocus;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdRefeshExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;

    mSearch := DataMain.StripToneMark(Trim(EdSearch.Text));
	with QrDMVIP do
    begin
    	Close;

        if mSearch = '' then
        begin
            if mMa <> '' then
                sSQL := sSQL + ' and MAVIP = ''' + mMa + ''''
            else
                sSQL := sSQL + ' and 1=0';
        end else
            sSQL := sSQL + ' and isnull(TINHTRANG, '''') = ''02''' +
                    ' and (dbo.fnStripToneMark(HOTEN) like ''%'+ mSearch +'%''' +
                        ' or dbo.fnStripToneMark(MAVIP) like ''%' + mSearch + '%''' +
                        ' or dbo.fnStripToneMark(CMND) like ''%' + mSearch + '%''' +
                        ' or dbo.fnStripToneMark(DCHI) like ''%' + mSearch + '%''' +
                        ' or dbo.fnStripToneMark(DTHOAI) like ''%' + mSearch + '%''' +
                        ' or dbo.fnStripToneMark(EMAIL) like ''%' + mSearch + '%''' +
                    ')';
        SQL.Text := sSQL;
        SQL.Add(' order by 	MASO');
        Open;
	end;

    EdSearch.SetFocus;
    EdSearch.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdSaveExecute(Sender: TObject);
begin
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.CmdSearchExecute(Sender: TObject);
begin
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.EdSearchExit(Sender: TObject);
begin
    if EdSearch.Text = mSearch then
        Exit;

    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosVIP.exDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    Action := DbeMsg;
end;

end.
