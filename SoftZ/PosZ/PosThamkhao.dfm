﻿object FrmPosThamkhao: TFrmPosThamkhao
  Left = 204
  Top = 157
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin H'#224'ng H'#243'a'
  ClientHeight = 221
  ClientWidth = 400
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object EdMA: TEdit
    Left = 12
    Top = 12
    Width = 377
    Height = 45
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -32
    Font.Name = 'VNI-Avo'
    Font.Style = [fsBold]
    MaxLength = 13
    ParentFont = False
    TabOrder = 0
    OnKeyPress = EdMAKeyPress
  end
  object Inspect: TwwDataInspector
    Left = 15
    Top = 76
    Width = 377
    Height = 137
    TabStop = False
    DisableThemes = False
    Ctl3D = True
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    DataSource = DsDMVT
    Items = <
      item
        DataSource = DsDMVT
        DataField = 'TENVT'
        Caption = 'T'#234'n m'#7863't h'#224'ng'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'TENTAT'
        Caption = 'T'#234'n t'#7855't'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'DVT'
        Caption = #272#417'n v'#7883' t'#237'nh'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'GIABAN'
        Caption = 'Gi'#225' b'#225'n'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'GIASI'
        Caption = 'Gi'#225' b'#225'n s'#7881
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'VAT_RA'
        Caption = 'Thu'#7871' VAT'
        WordWrap = False
      end>
    CaptionWidth = 120
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    PaintOptions.BackgroundOptions = [coFillDataCells]
    CaptionFont.Charset = ANSI_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
    ReadOnly = True
  end
  object DsDMVT: TDataSource
    DataSet = QrDMVT
    Left = 252
    Top = 108
  end
  object QrDMVT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DM_VT_FULL'
      ' where'#9'MAVT = :MAVT'
      ' order by case when MAVT = :MAVT then 0 else 1 end')
    Left = 220
    Top = 108
    object QrDMVTTENVT: TWideStringField
      FieldName = 'TENVT'
      Size = 100
    end
    object QrDMVTTENTAT: TWideStringField
      FieldName = 'TENTAT'
      Size = 25
    end
    object QrDMVTDVT: TWideStringField
      FieldName = 'DVT'
      Size = 10
    end
    object QrDMVTGIABAN: TFloatField
      FieldName = 'GIABAN'
    end
    object QrDMVTMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrDMVTDVT1: TWideStringField
      FieldName = 'DVT1'
      Size = 10
    end
    object QrDMVTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrDMVTGIASIVAT: TFloatField
      FieldName = 'GIASIVAT'
    end
    object QrDMVTVAT_VAO: TFloatField
      FieldName = 'VAT_VAO'
    end
    object QrDMVTVAT_RA: TFloatField
      FieldName = 'VAT_RA'
    end
  end
end
