﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Printer;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, wwdblook, Db, ADODB, Buttons, ActnList,
  Wwdbcomb, Printers, Mask, wwdbedit, Wwdotdot, Graphics;

type
  TFrmPrinter = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    CbPrinter: TwwDBLookupCombo;
    QrPRINTER: TADOQuery;
    QrDMQUAY: TADOQuery;
    QrDMQUAYTENMAY: TWideStringField;
    QrDMQUAYGHICHU: TWideStringField;
    QrDMQUAYQUAY: TWideStringField;
    QrDMQUAYPRINTER: TWideStringField;
    CmdOK: TBitBtn;
    BitBtn2: TBitBtn;
    DsDMQUAY: TDataSource;
    Action: TActionList;
    CmdSave: TAction;
    Image1: TImage;
    CbPrinter2: TwwDBComboBox;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  public
  	function Execute: Boolean;
  end;

var
  FrmPrinter: TFrmPrinter;

implementation

uses
    isDb, isLib,  PosCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPrinter.Execute: Boolean;
begin
	Result := ShowModal = mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinter.FormCreate(Sender: TObject);
begin
    CbPrinter2.Items := Printers.Printer.Printers;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinter.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;
    QrPRINTER.Open;
    with QrDMQUAY do
    begin
        Parameters[0].Value := isGetComputerName;
        Open;
    end;

    CbPrinter2.Value := sysPrinter;
	CbPrinter.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinter.CmdSaveExecute(Sender: TObject);
begin
	QrDMQUAY.CheckBrowseMode;
    if CbPrinter2.Value <> sysPrinter then
    begin
        sysPrinter := CbPrinter2.Value;
        RegWrite('COMMON', 'PosPrinter', sysPrinter);
    end;
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinter.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    CloseDataSets([QrDMQUAY, QrPRINTER]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPrinter.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdSave.Enabled := not (QrDMQUAY.State in [dsBrowse]) or (not SameStr(CbPrinter2.Value, sysPrinter));
end;

end.
