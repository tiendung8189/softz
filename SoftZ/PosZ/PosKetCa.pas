﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosKetCa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, htmlbtns, ActnList, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBVertGridsEh, DB, ADODB, RzButton, ComCtrls, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, ImgList, DBGridEh, ExtCtrls, AdvTouchKeyboard,
  AdvSmoothTouchKeyBoard, Vcl.Mask, Vcl.DBCtrls, wwdblook;

type
  TFrmPosKetca = class(TForm)
    ActionList1: TActionList;
    CmdNhan: TAction;
    CmdNop: TAction;
    CmdIn: TAction;
    CmdXacNhan: TAction;
    CmdClose: TAction;
    QrKETCA: TADOQuery;
    DsKETCA: TDataSource;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    VertGridEhGroupCa: TDBVertGridEh;
    GrNHAN: TDBGridEh;
    TabSheet3: TTabSheet;
    BtnXn: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    QrMENHGIA: TADOQuery;
    DsMENHGIA: TDataSource;
    GrNOP: TDBGridEh;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    QrKETCALK_THUNGAN: TWideStringField;
    QrMENHGIAMENHGIA: TIntegerField;
    QrMENHGIAGHICHU: TWideStringField;
    QrMENHGIALOC: TWideStringField;
    QrMENHGIASTT: TIntegerField;
    QrKETCACREATE_BY: TIntegerField;
    QrKETCAUPDATE_BY: TIntegerField;
    QrKETCACREATE_DATE: TDateTimeField;
    QrKETCAUPDATE_DATE: TDateTimeField;
    CmdTongKetCa: TAction;
    BtnTKC: TRzBitBtn;
    QrKETCASct: TWideStringField;
    QrKETCAMaLoc: TWideStringField;
    QrKETCAMaKho: TWideStringField;
    QrKETCAMaQuay: TWideStringField;
    QrKETCAQuay: TWideStringField;
    QrKETCANhanVienThuNgan: TIntegerField;
    QrKETCANgayBatDau: TDateTimeField;
    QrKETCANgayKetThuc: TDateTimeField;
    QrKETCASctBatDau: TWideStringField;
    QrKETCASctKetThuc: TWideStringField;
    QrKETCASoLuongSct: TIntegerField;
    QrKETCATongSoTienNhan: TFloatField;
    QrKETCAThanhToanTienMat: TFloatField;
    QrKETCAThanhToanKhac: TFloatField;
    QrKETCATienMatNhan: TFloatField;
    QrKETCATienMatNop: TFloatField;
    QrKETCATienMatChenhLech: TFloatField;
    QrKETCACHECKED: TBooleanField;
    QrKETCALOC: TWideStringField;
    QrMENHGIASoLuongNhan: TIntegerField;
    QrMENHGIATienMatNhan: TFloatField;
    QrMENHGIASoLuongNop: TIntegerField;
    QrMENHGIATienMatNop: TFloatField;
    QrKETCACALC_NHANNOP: TFloatField;
    QrPayment: TADOQuery;
    QrPaymentPAYMENT: TWideStringField;
    QrPaymentThanhToanTienMat: TFloatField;
    QrPaymentThanhToanTheNganHang: TFloatField;
    QrPaymentThanhToanTraHang: TFloatField;
    QrPaymentThanhToanPhieuQuaTang: TFloatField;
    QrPaymentThanhToanViDienTu: TFloatField;
    QrPaymentThanhToanChuyenKhoan: TFloatField;
    QrPaymentThanhToanDiemTichLuy: TFloatField;
    QrKETCAKHOA: TLargeintField;
    QrMENHGIAKhoa: TLargeintField;
    QrMENHGIAKhoaCT: TLargeintField;
    QrKETCAThanhToanTheNganHang: TFloatField;
    QrKETCAThanhToanChuyenKhoan: TFloatField;
    QrKETCAThanhToanViDienTu: TFloatField;
    QrKETCAThanhToanViDienTu_VNPAY: TFloatField;
    QrKETCAThanhToanViDienTu_MOMO: TFloatField;
    QrKETCAGhiChu: TWideMemoField;
    QrPaymentThanhToanViDienTu_VNPAY: TFloatField;
    QrPaymentThanhToanViDienTu_MOMO: TFloatField;
    QrPaymentThanhToanViDienTu_ZALOPAY: TFloatField;
    QrKETCACALC_ThanhToanTienMat: TFloatField;
    QrKETCAThanhToan: TFloatField;
    QrKETCAThanhToanNTBL: TFloatField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrKETCABeforeOpen(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure QrKETCACalcFields(DataSet: TDataSet);
    procedure CmdInExecute(Sender: TObject);
    procedure CmdXacNhanExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure QrMENHGIASOLUONG_NHANChange(Sender: TField);
    procedure QrKETCAAfterInsert(DataSet: TDataSet);
    procedure QrKETCABeforePost(DataSet: TDataSet);
    procedure QrMENHGIABeforeOpen(DataSet: TDataSet);
    procedure QrMENHGIAAfterInsert(DataSet: TDataSet);
    procedure CmdTongKetCaExecute(Sender: TObject);
    procedure QrPaymentBeforeOpen(DataSet: TDataSet);
  
  private
    { Private declarations }
    bRet, bFlag: Boolean;
    mLCT: string;
    khoa: Largeint;
    procedure TongKetCa;
  public
    { Public declarations }
    function Execute(const xflag: Boolean = False): Boolean; // xflag: Tiền dự phòng

    function totalByFieldName(fieldName: string):  Double;
    function OpenQueries():  Boolean;
    function VertGridEhGroupByName(VertGridEhGroup: TDBVertGridEh; groupName: string):  Boolean;
  end;

var
  FrmPosKetca: TFrmPosKetca;

implementation

uses
	MainData, PosCommon, GuidEx, PosMain, isDb, isLib, isCommon, isMsg, exResStr,
    ExCommon, isStr;


{$R *.dfm}

const
	FORM_CODE = 'POS_KETCA';

(*==============================================================================
** xflag:
**	Tiền dự phòng
** Return value:
**	Kết ca or not
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.Execute(const xflag: Boolean): Boolean;
begin
	// Chỉ nhập tiền dự phòng?
    TabSheet1.TabVisible := not xflag;
    TabSheet3.TabVisible := not xflag;
    BtnTKC.Visible := not xflag;

    bFlag := xflag;

    if bFlag then
    begin
        BtnXn.Caption := 'Nhận ca';
    	Caption := 'Nhận Ca';
	    PgMain.ActivePageIndex := 1;
    end
    else
    begin
        BtnXn.Caption := 'Kết thúc ca';
    	Caption := 'Kết Thúc Ca';
	    PgMain.ActivePageIndex := 0;
    end;

	// Go ahead
    bRet := False;
	ShowModal;
    Result := bRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	//CmdXacNhan.Enabled := PgMain.ActivePageIndex = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdCloseExecute(Sender: TObject);
begin
    QrMENHGIA.CancelBatch;
	QrKETCA.Cancel;
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdInExecute(Sender: TObject);
begin
	TongKetCa;
	ShowReport(Caption, 'RP_POSZ_KETCA', [sysLogonUID, QrKETCA.FieldByName('KHOA').AsLargeInt]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdTongKetCaExecute(Sender: TObject);
begin
    if posCa <> '' then
    begin
        SetEditState(QrKETCA);
        QrMENHGIA.CheckBrowseMode;
        QrKETCA.Post;
        exSaveDetails(QrMENHGIA);
        TongKetCa;
        QrKETCA.Requery;
        QrMENHGIA.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.CmdXacNhanExecute(Sender: TObject);
var
	b, bEmpty: Boolean;
    sum: Double;
begin
    if posCa = '' then
    begin
        sum := totalByFieldName('TienMatNhan');
        if (sum = 0) and  not YesNo('Không có tiền mặt đầu ca, vẫn thực hiện Nhận Ca. Tiếp tục?') then
            Exit;

        b := False;
    end
    else
    begin
        with TADOCommand.Create(nil) do
        begin
            Connection := sysDbConnection;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spKETCA_Check_BanLeTam;1';

            Parameters.Refresh;
            Parameters.ParamByName('@pUID').Value := sysLogonUID;
            Parameters.ParamByName('@pMaQuay').Value := posMaQuay;
            Execute;

            bEmpty := (Parameters.ParamValues['@RETURN_VALUE'] <> 0);
            Free;
        end;

        if bEmpty then
        begin
            ErrMsg('Phải giải quyết tất cả hóa đơn còn treo lại.');
            Exit;
        end;

        // Confirm
        if not YesNo('Xác nhận kết thúc ca bán hàng.') then
            Exit;

        SetEditState(QrKETCA);
        b := True;
    end;

    QrMENHGIA.CheckBrowseMode;
    QrKETCA.Post;
    exSaveDetails(QrMENHGIA);

    if b then
    begin
        PosCommon.PosKetCa(2);
    	ShowReport(Caption, 'RP_POSZ_KETCA', [sysLogonUID, QrKETCA.FieldByName('KHOA').AsLargeInt]);
    end
    else
    begin
        PosCommon.PosKetCa(0, 1);
    end;

    bRet := True;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrKETCA, QrMENHGIA, QrPayment]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.TongKetCa;
begin
	// Tổng kết ca
    if posCa <> '' then
    begin
       PosCommon.PosKetCa(1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.FormShow(Sender: TObject);
var
    r: TRect;
    mMENHGIA: Double;
begin
    mLCT := 'KCA';

	SystemParametersInfo(SPI_GETWORKAREA, 0, r, 0);
//	with Sender do
//    begin
//	    Left := (r.Right - Width) div 2;
//        Top := r.Top;
//    end;


	SetDictionary(QrKETCA, FORM_CODE);
	SetDictionary(QrMENHGIA, FORM_CODE + '_MENHGIA');

    SetDisplayFormat(QrPayment, sysCurFmt);
    SetDisplayFormat(QrMENHGIA, sysCurFmt);

    SetDisplayFormat(QrKETCA, sysCurFmt);
    SetDisplayFormat(QrKETCA, ['NgayBatDau', 'NgayKetThuc'], ShortDateFormat + ' hh:nn');

    VertGridEhGroupByName(VertGridEhGroupCa, 'Fields_Group_Ca');
    VertGridEhGroupByName(VertGridEhGroupCa, 'Fields_Group_TienMat');

	if bFlag then
    begin
        TongKetCa;
    end;

    OpenDataSets([QrPayment, QrKETCA]);

    OpenQueries();

    PgMain.OnChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.PgMainChange(Sender: TObject);
begin
    if QrMENHGIA.Active then
        QrMENHGIA.CheckBrowseMode;

	case PgMain.ActivePageIndex of
    0:
    begin
        CmdTongKetCa.Execute;
    end;
    1:
       	GrNHAN.SetFocus;
    2:
       	GrNOP.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCAAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrKETCA do
    begin
        khoa := GetBigint; //getIdentityCurrentByTable('KETCA');
        FieldByName('KHOA').Value                   := khoa;
		FieldByName('NhanVienThuNgan').AsInteger    := sysLogonUID;
		FieldByName('NgayBatDau').AsDateTime        := d;
        FieldByName('LOC').AsString                 := sysLoc;
        FieldByName('MAKHO').AsString               := posMakho;
        FieldByName('QUAY').AsString                := posQuay;
        FieldByName('MAQUAY').AsString              := posMaQuay;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCABeforeOpen(DataSet: TDataSet);
begin
	with QrKETCA do
    begin
        Parameters[0].Value := sysLoc;
        Parameters[1].Value := posCa;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCABeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrKETCACalcFields(DataSet: TDataSet);
begin
	with QrKETCA do
    begin
    	FieldByName('CALC_NHANNOP').AsFloat :=
    		FieldByName('TienMatNop').AsFloat -
	    	FieldByName('TienMatNhan').AsFloat;

       FieldByName('CALC_ThanhToanTienMat').AsFloat := FieldByName('ThanhToanTienMat').AsFloat;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIAAfterInsert(DataSet: TDataSet);
begin
    with QrMENHGIA do
    begin
        FieldByName('KhoaCT').Value := GetBigint;
        FieldByName('KHOA').Value := QrKETCA.FieldByName('KHOA').Value;
        FieldByName('LOC').AsString := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIABeforeOpen(DataSet: TDataSet);
begin
    QrMENHGIA.Parameters[0].Value := QrKETCA.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrMENHGIASOLUONG_NHANChange(Sender: TField);
var
	x: Integer;
begin
    with QrMENHGIA do
    begin
    	x := FieldByName('MENHGIA').AsInteger;
        FieldByName('TienMatNhan').AsFloat := x * FieldByName('SoLuongNhan').AsInteger;
        FieldByName('TienMatNop').AsFloat := x * FieldByName('SoLuongNop').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPosKetca.QrPaymentBeforeOpen(DataSet: TDataSet);
begin
    with QrPayment do
    begin
        Parameters[0].Value := sysLoc;
        Parameters[1].Value := posCa;
        Parameters[2].Value := sysLogonUID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.totalByFieldName(fieldName: string):  Double;
var
    sum: Double;
begin
    with QrMENHGIA do
    begin
        sum := 0;
        First;
        while not Eof do
        begin
            if not FieldByName(fieldName).IsNull then
            begin
                sum := sum  + FieldByName(fieldName).AsFloat
            end;
            Next;
        end;
    end;
    Result := sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.OpenQueries():  Boolean;
var
    mMENHGIA: Double;
begin
    if posCa <> '' then
        QrMENHGIA.Open
    else
    begin
        QrKETCA.Append;
        with QrMENHGIA do
        begin
            Close;
            Open;
        end;

        with DataMain.QrTEMP do
        begin
            SQL.Text := 'select * from dbo.fnLoaiMenhgia()';
            Open;
            First;
            while not Eof do
            begin
                mMENHGIA := FieldByName('MENHGIA').AsFloat;
                with QrMENHGIA do
                begin
                    Append;
                    FieldByName('MENHGIA').AsFloat := mMENHGIA;
                end;
                Next;
            end;
            QrMENHGIA.CheckBrowseMode;
        end;
    end;

    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPosKetca.VertGridEhGroupByName(VertGridEhGroup: TDBVertGridEh; groupName: string):  Boolean;
var
    fld: TField;
    it: TFieldRowEh;
	ls: TStrings;
    s: String;
    i, ret: Integer;
begin
      s := FlexConfigString(FORM_CODE, groupName);
    if s <> '' then
    begin
        ls := TStringList.Create;
        isStrBreak(s, ';', ls);

        if ls.Count > 0 then
        begin
            for i := 0 to ls.Count - 1 do
            begin
                with VertGridEhGroup do
                begin
                    fld := DataSource.DataSet.FindField(ls[i]);
                    it := VertGridEhGroup.FindFieldRow(ls[i]);
                    if it <> Nil then
                    begin
                        it.RowLabel.Caption := '+ ' +  it.Field.DisplayName;
                    end
                    else if fld <> Nil then
                    begin
                        it := Rows.Add;
                        it.CategoryName := groupName;
                        it.Alignment := taRightJustify;
                        it.FieldName := ls[i];
                        it.RowLabel.Caption := '+ ' + fld.DisplayLabel;
                        it.Font.Name := 'Tahoma';
                        it.Font.Style := [fsBold];
                    end;
                    if (fld <> Nil) then
                    begin
                        // Checkbox
                        if (fld.DataType = ftBoolean) then
                            it.DblClickNextVal := True
                        else if (fld.DataType = ftDateTime) or (fld.FieldKind = fkLookup) then
                            it.EditButton.Width := 20
                        else if (fld.DataType = ftFloat) then
                            (fld as TFloatField).EditFormat := '###0.##;-###0.##;#';
                    end;

                    it.Index := i;
                end;
            end;
        end;
    end;
    Result := True;
end;

end.
