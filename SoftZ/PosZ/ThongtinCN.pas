﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThongtinCN;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, wwdblook, Mask, wwdbedit,
  ActnList, RzButton;

type
  TFrmThongtinCN = class(TForm)
    Panel1: TPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label30: TLabel;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdTENDT: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MyActionList: TActionList;
    CmdOK: TAction;
    Label15: TLabel;
    wwDBEdit4: TwwDBEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    CmdReturn: TRzBitBtn;
    BtClose: TRzBitBtn;
    CmdClose: TAction;
    Panel2: TPanel;
    BtnDelivery: TRzBitBtn;
    CmdCancel: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdOKExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
  private
  public
  	function Execute(pShowCancel: Boolean = True; viewDB: Boolean = False) : Boolean;
  end;

var
  FrmThongtinCN: TFrmThongtinCN;

implementation

uses
	isLib, PosMain, MainData, isDb, isMsg, BanleCN;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdCancelExecute(Sender: TObject);
begin
    with FrmMain.QrBH do
    begin
        if FieldByName('DELIVERY').AsBoolean then
        begin
            if not YesNo('Hủy giao hàng. Tiếp tục?') then
                Exit;

            FieldByName('CN_LIENHE').Clear;
            FieldByName('CN_DIACHI').Clear;
            FieldByName('CN_DTHOAI').Clear;
            FieldByName('TINHTRANG').Clear;
            FieldByName('DELIVERY').Clear;
        end;
        ModalResult := mrOk;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdOKExecute(Sender: TObject);
begin
    CmdReturn.SetFocus;
    with (FrmMain.QrBH) do
    begin
//        SetDefButton(1);
        if BlankConfirm(FrmMain.QrBH, ['CN_DIACHI', 'CN_LIENHE', 'CN_DTHOAI']) then
            Abort;

        FieldByName('TINHTRANG').AsString := 'T01';
        FieldByName('DELIVERY').AsBoolean := True;
//        SetDefButton(0);
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN.Execute;
begin
    CmdCancel.Visible := pShowCancel;

    with FrmMain.QrBH do
    begin
        SetEditState(FrmMain.QrBH);
        if viewDB then
        begin
            FieldByName('CN_DTHOAI').AsString := FrmBanleCN.QrBH.FieldByName('CN_DTHOAI').AsString;
            FieldByName('CN_DIACHI').AsString := FrmBanleCN.QrBH.FieldByName('CN_DIACHI').AsString;
            FieldByName('CN_LIENHE').AsString := FrmBanleCN.QrBH.FieldByName('CN_LIENHE').AsString;
            FieldByName('CN_EMAIL').AsString := FrmBanleCN.QrBH.FieldByName('CN_EMAIL').AsString;
        end
        else
        begin
            if FieldByName('MAVIP').AsString <> '' then
            begin
                FieldByName('CN_DTHOAI').AsString := FieldByName('LK_VIP_DTHOAI').AsString;
                FieldByName('CN_DIACHI').AsString := FieldByName('LK_VIP_DCHI').AsString;
                FieldByName('CN_LIENHE').AsString := FieldByName('LK_TENVIP').AsString;
            end;
        end;
    end;
	Result := ShowModal = mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormShow(Sender: TObject);
var
    bTk, bHd: Boolean;
begin
    TMyForm(Self).Init;

    bTk := False;
    if not bTk then
        Self.Height := Self.Height - GroupBox3.Height;
    GroupBox3.Visible := bTk;

    bHd := False;
    if not bHd then
        Self.Height := Self.Height - GroupBox1.Height;
    GroupBox1.Visible := bHd;

end;

end.
