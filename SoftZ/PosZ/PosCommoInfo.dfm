object FrmCommoInfo: TFrmCommoInfo
  Left = 204
  Top = 157
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin H'#224'ng H'#243'a'
  ClientHeight = 329
  ClientWidth = 598
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    598
    329)
  PixelsPerInch = 96
  TextHeight = 16
  object EdMA: TEdit
    Left = 9
    Top = 12
    Width = 580
    Height = 47
    Anchors = [akLeft, akTop, akRight]
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    MaxLength = 13
    ParentFont = False
    TabOrder = 0
    OnKeyPress = EdMAKeyPress
  end
  object Inspect: TwwDataInspector
    Left = 9
    Top = 73
    Width = 580
    Height = 244
    TabStop = False
    DisableThemes = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    Ctl3D = True
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    DataSource = DsDMVT
    Items = <
      item
        DataSource = DsDMVT
        DataField = 'MACHUNG'
        Caption = 'M'#227' h'#224'ng'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'MAVT'
        Caption = 'M'#227' barcode'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'MaNoiBo'
        Caption = 'M'#227' n'#7897'i b'#7897
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'TENVT'
        Caption = 'T'#234'n m'#7863't h'#224'ng'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'DVT'
        Caption = #272#417'n v'#7883' t'#237'nh'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'GIABAN'
        Caption = 'Gi'#225' b'#225'n'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'VAT_VAO'
        Caption = 'Thu'#7871' VAT (%)'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'TenKho'
        Caption = 'Kho x'#233't t'#7891'n'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'NgayXetTon'
        Caption = 'Th'#7901'i gian x'#233't t'#7891'n'
        WordWrap = False
      end
      item
        DataSource = DsDMVT
        DataField = 'LG_CUOI'
        Caption = 'S'#7889' l'#432#7907'ng t'#7891'n'
        WordWrap = False
      end>
    CaptionWidth = 120
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    PaintOptions.BackgroundOptions = [coFillDataCells]
    CaptionFont.Charset = ANSI_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
    ReadOnly = True
  end
  object DsDMVT: TDataSource
    DataSet = CHECK_TONKHO
    Left = 252
    Top = 108
  end
  object CHECK_TONKHO: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterOpen = CHECK_TONKHOAfterOpen
    ProcedureName = 'spCHECK_TONKHO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 320
    Top = 86
  end
end
