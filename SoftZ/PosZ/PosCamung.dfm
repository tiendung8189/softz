object FrmPosCamung: TFrmPosCamung
  Left = 108
  Top = 129
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Danh S'#225'ch H'#224'ng H'#243'a C'#7843'm '#7912'ng'
  ClientHeight = 383
  ClientWidth = 908
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel20: TPanel
    Left = 0
    Top = 0
    Width = 908
    Height = 383
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 362
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 906
      Height = 64
      Align = alTop
      BevelInner = bvRaised
      Color = clActiveCaption
      ParentBackground = False
      TabOrder = 0
      object TtNganhhang: TAdvSmoothTileListEx
        Left = 2
        Top = 2
        Width = 902
        Height = 60
        Transparent = True
        AnimationFactor = 2.000000000000000000
        Fill.Color = clNone
        Fill.ColorTo = clNone
        Fill.ColorMirror = clNone
        Fill.ColorMirrorTo = clNone
        Fill.GradientType = gtVertical
        Fill.GradientMirrorType = gtVertical
        Fill.Opacity = 0
        Fill.OpacityTo = 0
        Fill.OpacityMirror = 0
        Fill.OpacityMirrorTo = 0
        Fill.BorderColor = clSilver
        Fill.BorderWidth = 0
        Fill.Rounding = 0
        Fill.ShadowColor = 13408767
        Fill.ShadowOffset = 0
        Fill.Glow = gmNone
        Tiles = <
          item
            Content.Text = '$$$'
            Content.TextPosition = tpBottomCenter
            ContentMaximized.Text = 'Description for Tile 1'
            ContentMaximized.Extra = '$$$'
            StatusIndicator = '$$$$'
            DeleteIndicator = 'X'
            StatusIndicatorLeft = -20
            StatusIndicatorTop = 1
            SubTiles = <>
            Tag = 0
            Extra = '$$$'
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end
          item
            DeleteIndicator = 'X'
            SubTiles = <>
            Tag = 0
          end>
        Columns = 10
        Rows = 1
        TileAppearance.LargeViewFill.Color = 15581579
        TileAppearance.LargeViewFill.ColorTo = 12889148
        TileAppearance.LargeViewFill.ColorMirror = clNone
        TileAppearance.LargeViewFill.ColorMirrorTo = clNone
        TileAppearance.LargeViewFill.GradientType = gtSolid
        TileAppearance.LargeViewFill.GradientMirrorType = gtNone
        TileAppearance.LargeViewFill.Opacity = 180
        TileAppearance.LargeViewFill.BorderColor = 10987431
        TileAppearance.LargeViewFill.Rounding = 0
        TileAppearance.LargeViewFill.ShadowOffset = 0
        TileAppearance.LargeViewFill.Glow = gmNone
        TileAppearance.SmallViewFill.Color = clWhite
        TileAppearance.SmallViewFill.ColorTo = clWhite
        TileAppearance.SmallViewFill.ColorMirror = clNone
        TileAppearance.SmallViewFill.ColorMirrorTo = clNone
        TileAppearance.SmallViewFill.GradientType = gtSolid
        TileAppearance.SmallViewFill.GradientMirrorType = gtNone
        TileAppearance.SmallViewFill.Opacity = 180
        TileAppearance.SmallViewFill.BorderColor = clBlue
        TileAppearance.SmallViewFill.BorderOpacity = 128
        TileAppearance.SmallViewFill.Rounding = 5
        TileAppearance.SmallViewFill.ShadowOffset = 1
        TileAppearance.SmallViewFill.Glow = gmNone
        TileAppearance.SmallViewFillSelected.Color = clGray
        TileAppearance.SmallViewFillSelected.ColorTo = 12889148
        TileAppearance.SmallViewFillSelected.ColorMirror = clNone
        TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
        TileAppearance.SmallViewFillSelected.GradientType = gtSolid
        TileAppearance.SmallViewFillSelected.GradientMirrorType = gtNone
        TileAppearance.SmallViewFillSelected.Opacity = 180
        TileAppearance.SmallViewFillSelected.BorderColor = clGray
        TileAppearance.SmallViewFillSelected.BorderOpacity = 128
        TileAppearance.SmallViewFillSelected.BorderWidth = 3
        TileAppearance.SmallViewFillSelected.Rounding = 5
        TileAppearance.SmallViewFillSelected.ShadowOffset = 0
        TileAppearance.SmallViewFillSelected.Glow = gmNone
        TileAppearance.SmallViewFillDisabled.Color = clWhite
        TileAppearance.SmallViewFillDisabled.ColorTo = clWhite
        TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
        TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
        TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
        TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtNone
        TileAppearance.SmallViewFillDisabled.BorderColor = clBlue
        TileAppearance.SmallViewFillDisabled.BorderOpacity = 128
        TileAppearance.SmallViewFillDisabled.BorderWidth = 3
        TileAppearance.SmallViewFillDisabled.Rounding = 5
        TileAppearance.SmallViewFillDisabled.ShadowOffset = 0
        TileAppearance.SmallViewFillDisabled.Glow = gmNone
        TileAppearance.SmallViewFillHover.Color = clWhite
        TileAppearance.SmallViewFillHover.ColorTo = clWhite
        TileAppearance.SmallViewFillHover.ColorMirror = clNone
        TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
        TileAppearance.SmallViewFillHover.GradientType = gtSolid
        TileAppearance.SmallViewFillHover.GradientMirrorType = gtNone
        TileAppearance.SmallViewFillHover.Opacity = 180
        TileAppearance.SmallViewFillHover.BorderColor = clBlue
        TileAppearance.SmallViewFillHover.BorderOpacity = 128
        TileAppearance.SmallViewFillHover.BorderWidth = 3
        TileAppearance.SmallViewFillHover.Rounding = 5
        TileAppearance.SmallViewFillHover.ShadowOffset = 0
        TileAppearance.SmallViewFillHover.Glow = gmNone
        TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
        TileAppearance.LargeViewFont.Color = clMaroon
        TileAppearance.LargeViewFont.Height = -16
        TileAppearance.LargeViewFont.Name = 'Tahoma'
        TileAppearance.LargeViewFont.Style = []
        TileAppearance.SmallViewFont.Charset = 163
        TileAppearance.SmallViewFont.Color = clWindowText
        TileAppearance.SmallViewFont.Height = -15
        TileAppearance.SmallViewFont.Name = 'Tahoma'
        TileAppearance.SmallViewFont.Style = [fsBold]
        TileAppearance.SmallViewFontSelected.Charset = 163
        TileAppearance.SmallViewFontSelected.Color = clWindowText
        TileAppearance.SmallViewFontSelected.Height = -15
        TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
        TileAppearance.SmallViewFontSelected.Style = [fsBold]
        TileAppearance.SmallViewFontDisabled.Charset = 163
        TileAppearance.SmallViewFontDisabled.Color = clWindowText
        TileAppearance.SmallViewFontDisabled.Height = -15
        TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
        TileAppearance.SmallViewFontDisabled.Style = [fsBold]
        TileAppearance.SmallViewFontHover.Charset = 163
        TileAppearance.SmallViewFontHover.Color = clWindowText
        TileAppearance.SmallViewFontHover.Height = -15
        TileAppearance.SmallViewFontHover.Name = 'Tahoma'
        TileAppearance.SmallViewFontHover.Style = [fsBold]
        TileAppearance.VerticalSpacing = 5
        TileAppearance.HorizontalSpacing = 5
        TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
        TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
        TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
        TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
        TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
        TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
        TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = clRed
        TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 128
        TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 8
        TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
        TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
        TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
        TileAppearance.StatusIndicatorAppearance.Font.Color = clMaroon
        TileAppearance.StatusIndicatorAppearance.Font.Height = -11
        TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
        TileAppearance.StatusIndicatorAppearance.Font.Style = []
        TileAppearance.StatusIndicatorAppearance.Glow = False
        TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
        TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
        TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
        TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
        TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
        TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
        TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
        TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
        TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
        TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
        TileAppearance.DeleteIndicatorAppearance.Font.Color = clWhite
        TileAppearance.DeleteIndicatorAppearance.Font.Height = -11
        TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
        TileAppearance.DeleteIndicatorAppearance.Font.Style = []
        TileMargins.Left = 0
        TileMargins.Top = 0
        TileMargins.Right = 0
        TileMargins.Bottom = 0
        Header.Visible = False
        Header.Fill.Color = clNone
        Header.Fill.ColorTo = clNone
        Header.Fill.ColorMirror = clNone
        Header.Fill.ColorMirrorTo = clNone
        Header.Fill.GradientType = gtVertical
        Header.Fill.GradientMirrorType = gtVertical
        Header.Fill.BorderColor = clNone
        Header.Fill.Rounding = 0
        Header.Fill.ShadowColor = clNone
        Header.Fill.ShadowOffset = 0
        Header.Fill.Glow = gmNone
        Header.Height = 10
        Header.BulletSize = 6
        Header.ArrowNavigation = False
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Footer.Visible = False
        Footer.Fill.Color = 10655892
        Footer.Fill.ColorTo = 8749179
        Footer.Fill.ColorMirror = 8156785
        Footer.Fill.ColorMirrorTo = 6775131
        Footer.Fill.GradientType = gtVertical
        Footer.Fill.GradientMirrorType = gtVertical
        Footer.Fill.BorderColor = 6973542
        Footer.Fill.Rounding = 0
        Footer.Fill.ShadowOffset = 0
        Footer.Fill.Glow = gmNone
        Footer.BulletSize = 15
        Footer.ArrowSize = 0
        Footer.ArrowRectangleSize = 0
        Footer.ArrowNavigation = False
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Options = []
        OnTileClick = TtNganhhangTileClick
        Align = alClient
        Ctl3D = True
        TabOrder = 0
        BevelInner = bvNone
        BevelOuter = bvNone
        DoubleBuffered = True
        ParentCtl3D = False
        ParentFont = False
        ExplicitHeight = 52
        TMSStyle = 0
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 323
      Width = 906
      Height = 59
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 1
      DesignSize = (
        906
        59)
      object LbCurSoluong: TLabel
        Left = 120
        Top = 11
        Width = 180
        Height = 29
        Anchors = [akTop, akRight]
        AutoSize = False
        BiDiMode = bdLeftToRight
        Caption = '~'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8421440
        Font.Height = -24
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object HtmlCurSoluong: THTMLabel
        Left = 114
        Top = 7
        Width = 180
        Height = 35
        Anchors = [akTop, akRight]
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        AutoSizing = True
        HTMLText.Strings = (
          
            '<P align="left"><FONT face="Tahoma" size="8" color="#004080"><FO' +
            'NT size="18"><b>7</b></FONT></FONT></P>')
        Transparent = True
        Version = '2.2.1.2'
      end
      object BtReturn: TRzBitBtn
        Left = 12
        Top = 9
        Width = 95
        Height = 42
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdSoluong
        Anchors = [akRight, akBottom]
        Caption = 'S'#7889' l'#432#7907'ng'#13'F3'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000908070F3E3A30664E483C804E483C804E483C804E483C804E48
          3C804E483C804E483C804E483C804E483C804E483C804E483C804E483C804E48
          3C804E483C804E483C804E483C804E483C804B45397A24211C3B000000000000
          0000000000000B0A081A00000000000000000000000000000000000000000000
          00001412111C9C9381E9A1967FFFA59A84FFA59A84FFA59A84FFA59A84FFA59A
          84FFA59A84FFA59A84FFA59A84FFA59A84FFA59A84FFA59A84FFA59A84FFA59A
          84FFA59A84FFA59A84FFA59A84FFA59A84FFA49983FF9E927BFF423D336C0000
          0000000000004C463AB500000000000000000000000000000000000000000000
          0000716C60A0B0A693FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFABA08BFF8D826CE60000
          000012100D2D6F6857FF110F0C2B000000000000000000000000000000000000
          00008A8376C0AFA592FFC7C2B8FFCBCBCBFFCECECEFFBFBDB8FFAEA490FFC7C2
          B8FFCCCCCBFFCECECEFFC0BEB9FFAEA490FFC7C2B8FFCCCCCBFFCECECEFFC0BE
          B9FFAEA490FFC7C2B8FFCCCCCBFFCECECEFFC0BEB9FFAEA490FF9C9078FF0000
          0000444037976D6453FF322D2380000000000000000000000000000000000000
          00008A8376C0AFA592FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FFDBD8
          D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECE
          CEFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          00006F6A5FE6675D49FF544B3AD5000000000000000000000000000000000000
          00008A8376C0AFA592FFC6C0B5FFEFEFEFFFEFEFEFFFD0CECAFFAEA490FFC6C0
          B5FFEFEFEFFFEFEFEFFFD0CECAFFAEA490FFC6C0B5FFEFEFEFFFEFEFEFFFD0CE
          CAFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFB4AB9AFFB5AFA4FFB6B1A7FFB2AB9CFFAEA490FFB4AB
          9AFFB5AFA4FFB6B1A7FFB2AB9CFFAEA490FFB4AB9AFFB5AFA4FFB6B1A7FFB2AB
          9CFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFD8D4CEFFDEDEDEFFE0E0E0FFCBCBCBFFAEA490FFD8D4
          CEFFDFDFDFFFE0E0E0FFCCCCCBFFAEA490FFD8D4CEFFDFDFDFFFE0E0E0FFCCCC
          CBFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFD6D3CDFFE0E0E0FFE0E0E0FFCBCBCBFFAEA490FFD6D3
          CDFFE0E0E0FFE0E0E0FFCCCCCBFFAEA490FFD6D3CDFFE0E0E0FFE0E0E0FFCCCC
          CBFFAEA490FFD6D3CDFFE0E0E0FFE0E0E0FFCCCCCBFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFB4AB99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FFB4AB
          99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FFB4AB99FFD6D3CDFFDBD8D3FFC7C2
          B8FFAEA490FFB4AB99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFC7C2B8FFCBCBCBFFCECECEFFBFBDB8FFAEA490FFC7C2
          B8FFCCCCCBFFCECECEFFC0BEB9FFAEA490FFC7C2B8FFCCCCCBFFCECECEFFC0BE
          B9FFAEA490FFC7C2B8FFCCCCCBFFCECECEFFC0BEB9FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FFDBD8
          D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECE
          CEFFAEA490FFDBD8D3FFE0E0E0FFE0E0E0FFCECECEFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFC6C0B5FFEFEFEFFFEFEFEFFFD0CECAFFAEA490FFC6C0
          B5FFEFEFEFFFEFEFEFFFD0CECAFFAEA490FFC6C0B5FFEFEFEFFFEFEFEFFFD0CE
          CAFFAEA490FFC6C0B5FFEFEFEFFFEFEFEFFFD0CECAFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFB4AB9AFFB5AFA4FFB6B1A7FFB2AB9CFFAEA490FFB4AB
          9AFFB5AFA4FFB6B1A7FFB2AB9CFFAEA490FFB4AB9AFFB5AFA4FFB6B1A7FFB2AB
          9CFFAEA490FFB4AB9AFFB5AFA4FFB6B1A7FFB2AB9CFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF000000000000000000000000000000000000
          00008A8376C0AFA592FFD8D4CEFFDEDEDEFFE0E0E0FFCBCBCBFFAEA490FFD8D4
          CEFFDFDFDFFFE0E0E0FFCCCCCBFFAEA490FFD8D4CEFFDFDFDFFFE0E0E0FFCCCC
          CBFFAEA490FFD8D4CEFFDFDFDFFFE0E0E0FFCCCCCBFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF0D0B09200000000000000000000000000000
          00008A8376C0AFA592FFD6D3CDFFE0E0E0FFE0E0E0FFCBCBCBFFAEA490FFD6D3
          CDFFE0E0E0FFE0E0E0FFCCCCCBFFAEA490FFD6D3CDFFE0E0E0FFE0E0E0FFCCCC
          CBFFAEA490FFD6D3CDFFE0E0E0FFE0E0E0FFCCCCCBFFAEA490FF9C9078FF0000
          00005C6CA2FF5465A2FF5465A2FF322D23800000000000000000000000000000
          00008A8376C0AFA592FFB4AB99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FFB4AB
          99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FFB4AB99FFD6D3CDFFDBD8D3FFC7C2
          B8FFAEA490FFB4AB99FFD6D3CDFFDBD8D3FFC7C2B8FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA4
          90FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FF857B69FF60584AFF60584AFF5B5445FF534B3CFF6058
          4AFF534B3CFF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FF4F4737FF4F4737FF4F4737FF544B3BFFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FF7F7664FF60584AFF60584AFF60584AFF554D3DFF5A53
          44FF5D5547FF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FFBFDC5CFF95A34EFFB7D159FF4F4737FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FF7F7664FF5E5748FF60584AFF60584AFF5E5748FF5149
          3BFF5E5748FF574F40FF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FF94A24DFF4F4737FF94A24DFF4F4737FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FF7F7664FF564E3FFF60584AFF60584AFF60584AFF5951
          42FF564E3FFF5F5849FF514939FF4F4737FF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FF94A24DFF4F4737FF94A24DFF4F4737FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0AFA592FF7F7664FF4F4737FF5C5546FF60584AFF60584AFF6058
          4AFF524B3BFF5C5546FF5B5344FF4F4737FF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FFBFDC5CFF95A34EFFB7D159FF4F4737FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          00008A8376C0B4AB99FF7F7664FF4F4737FF524A3AFF60584AFF60584AFF6058
          4AFF5D5547FF524A3AFF60584AFF554D3DFF4F4737FF4F4737FF4F4737FF4F47
          37FF4F4737FF4F4737FF4F4737FF4F4737FF4F4737FFAEA490FF9C9078FF0000
          0000547DFFFF4370FFFF4370FFFF322D23800000000000000000000000000000
          0000736E63A3BEB6A7FF9B927FFF7F7664FF7F7664FF7F7664FF7F7664FF7F76
          64FF7F7664FF7F7664FF7F7664FF7F7664FF7F7664FF7F7664FF7F7664FF7F76
          64FF7F7664FF7F7664FF7F7664FF7F7664FF857B69FFABA08BFF8D826CE60000
          0000547DFFFF4370FFFF4370FFFF504837CD0000000000000000000000000000
          00001412111CBDB5A6FABEB6A7FFB9B1A0FFB9B1A0FFAFA592FFAFA592FFAFA5
          92FFAFA592FFAFA592FFAFA592FFAFA592FFAFA592FFAFA592FFAFA592FFAFA5
          92FFAFA592FFAFA592FFAFA592FFAFA592FFAFA692FFAFA693FF4F4A3F7A0000
          00005077F1F63B57B3CD31437EA3080705140000000000000000000000000000
          0000000000001412111C736E63A38A8376C08A8376C08A8376C08A8376C08A83
          76C08A8376C08A8376C08A8376C08A8376C08A8376C08A8376C08A8376C08A83
          76C08A8376C08A8376C08A8376C08A8376C0857E71B946423B65000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Margin = 1
        Spacing = 1
      end
      object BtClose: TRzBitBtn
        Left = 794
        Top = 9
        Width = 106
        Height = 42
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdClose
        Anchors = [akRight, akBottom]
        Caption = 'Tho'#225't'#13'Esc'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        Glyph.Data = {
          C2040000424DC204000000000000420000002800000018000000180000000100
          1000030000008004000000000000000000000000000000000000007C0000E003
          00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
          924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
          6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
          FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
          261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
          FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
          F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
          E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
          8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
          271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
          A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
          0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
          D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
          2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F}
        Margin = 1
        Spacing = 1
      end
    end
    object TtVattu: TAdvSmoothTileListEx
      Left = 1
      Top = 65
      Width = 906
      Height = 258
      Transparent = True
      AnimationFactor = 1.000000000000000000
      Fill.Color = 14145495
      Fill.ColorTo = clNone
      Fill.ColorMirror = clNone
      Fill.ColorMirrorTo = clNone
      Fill.GradientType = gtVertical
      Fill.GradientMirrorType = gtVertical
      Fill.BackGroundPictureMode = pmInsideFill
      Fill.PictureSize = psCustom
      Fill.PictureWidth = 1008
      Fill.PictureHeight = 428
      Fill.PictureAspectRatio = True
      Fill.PictureAspectMode = pmNormal
      Fill.Opacity = 98
      Fill.OpacityTo = 98
      Fill.BorderColor = clWhite
      Fill.BorderOpacity = 0
      Fill.BorderWidth = 0
      Fill.Rounding = 0
      Fill.ShadowOffset = 0
      Fill.Glow = gmNone
      Tiles = <
        item
          Content.Image.Data = {
            424D360C00000000000036000000280000002000000020000000010018000000
            0000000C000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9BAB937493A596D5DA0A39BDCDCDB
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBEBEB41543931523964CDD257ACA75E8574
            7F8B7BB5B6B0E8E9E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF7B867B295A2133472967D3E164F2FF5DE2FB
            5FCED85AA49C557461777E70B8B9B7EEEEEDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFBCBEBC2D5C2E3C723A2F3E1D61B0C367DCFE5CD5F5
            54DCFD4EDEFF4ED3EC41A8AF2F6A5E445B46848880C7C9C6FEFEFDFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFEBEBEB4863483A733A51875135462057929E6DD4F65FC7E6
            5AC9EA52CCEE4BD2F848DBFF43D8FC3ABDD62D8A8C2C5C494E5F4B939690D7DA
            D7FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF7E887E316731487C48649C66486135537F8471CBEE65C5E7
            5DC7EC55C5EB50C3E94BC3E944C7EF3ECDFA3CD3FF3DCBF239ABBC2E78703152
            3A5B6955ACB0ABFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFBCBEBC2F5C2F437B43578B5776AE7866895A48656473C1E54F7A8F
            5A6A6B5F99A951B2D749B9E645BBE643BCE542BEE743C4EE44CEFA44D2FC45C4
            E43E9DA253695FF2F2F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBEBEB4B664B3E773E528552689B6884B9858BBB895E90675C786644190F
            5870657A88707C83676D968F519FB944ADD847BBE94ABDE74ABCE249BEE24AC9
            F14BCEF498A8ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFF7E897E376D374E824E6396637BAE7B91C4918AC18B61A06D7942116C2500
            4EA8AE62F1FF63CBD27DB8A287987270715C58737C4E96B74EB6E150BCE654BD
            E64FB4D7A1AFB4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1
            C1BF346034487F485C8F5C73A6738DC08D91C4916DB07A56552DB04A1081460B
            4A7E7E69E2FF5CE0FD4DDFFF50D7F05EB7BB657465613E2D604A455D798856A6
            C959C0EA94A7ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDED4B
            6E5E437E47568A566DA16D87BA8794C6937EBC8651774B863708C8732D986420
            43635A6BD3F263CDEC5CCEEE51D3F748DBFF42DCFF44C2DA4F898F5D5043673A
            2D695C59787A7FF5F4F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81939313
            949E508756689A6580B38093C6938BC28C5C97676C3D13B25517CD8E43B68B3E
            4659466BC4E365C5E55DC3E457C5E651C5E84BC9ED43D1F83AD9FF36D1F940AB
            C4557476664231815041AF908BDBD9D8FDFDFCFFFFFFFFFFFFC2C1BF177D8509
            ADD25E95657BAB7590C39091C5906FAF7A56522AA5440CBE7832D2A353DFBE63
            565F4065B1D369BEE262BADC5ABADC55BBDD4FBCE149BCE342BEE63EC5EE3CCF
            FA3AD4FF40CBED50A7BB656C656B3C28A69B97FFFFFFEFEEEE49746E179FAA05
            B3E263A98490BC8492C59381BF895073488A3B0ABD6B27C89247DCB863F6EC88
            89824A6C83806EADC260B2D559B5DF56B6E051B5DD4CB3DB47B3DB46B6DD47BA
            E147BEE645C4EB43CFF746E0FF5796A4C1C0C0FFFFFF7E91900C9CB41DA4AF00
            BEF55CBEAA9CC3888CC48F5B95656A3A10B75A1BC38940D5AC5AECD87BF7F491
            D2B569CB9847A66A2E7F573C6D7774598CA24C9EC748AAD94AAFDE4EB1DB4FB0
            D64EB1D74EB5DB4DB8DD53CDF56A9EAEE8E8E8FFFFFF30787508B2DC25ABB30E
            CDF97AD5C996C18773B37E524E24A7470EC07C36D0A052E3C66EFCF690DDCC7B
            CBA159C68A40AA4C10B55915C07629AF6F2D916B416F7064547E904C93B94BA3
            D151ADDB54AFD955B0D75CBFE56997A8DADBDBFFFFFF3D807902BCE92EB9C476
            E8F09FE8DE85BF88506F40863C0CC06F2BC9964ADDBB65F5EA87EFE88DCCAB65
            CA9649AC5B1EA54D13C1823CCA9247D49E4DDDA54EDAA047C79344A8844B8279
            5F6579805588A75199C45DB5E3619CB9C2C6C8FFFFFF5D857C05C5F541CCD9A7
            EDE48BD6B865915A2F54498D704ACE8638D5AE5CECDA7CFAF794D6BF76CEA055
            B8702DA2450DB7712EC4863FC89046CF9D4FD4AA5ADCBA65E9CA6EF1D371F1D1
            6DE2C063C2A55D9B8B5F78817A5D7C8D99A0A4FFFFFF81958D26D6F37EE6EC90
            CDA87BAE754055320C8CB27F9577E19E44E4CC72FBF690E5D888CEA760C28339
            A34911AE5E20C1823BC58A41CB964BD1A353D7AE5CDCB965E2C66EE8D478F1E2
            84FBF38EFFFF94FFF891F4E483DFC573A3875FFFFFFF9BA49F88EAE79CEEEB8A
            C48F516B3A4F574602ACDE71A893F0B959F5EB88F4ED92D4B972C99146AC581C
            A54E15C07730CA8335CD8B3BD09947D5A755D9B360DFBE69E6CA72EBD77AF0E2
            82F7EE8CFDFA95FFFF9DFFFFAACCC077A79C8EFFFFFFBABCB88FD4C191DCBB66
            8E57235A56458F930DAFDB4EBEC2FCDA73FBF693DFCD84CEA054B46A28A2460F
            BD6A24B181427A9F88A9AB7DC6A861D6A955E3B45AE9C367EAD075EDDC7FF3E8
            88F9F490FFFE9AFFFFA6C3B270B2ADA9FFFEFEFFFFFFE2E3E276987A89C38744
            562D158AAA3CA8B125B4D140D3E3FFF58BF0E390D4AF68BD7D35A34811AF5A1C
            C7762A5A968F5879779C9D85AFCAB1A7CEB0AFC79AC8C782EDD374FBE17CF5EC
            8BFDFB97FFFFA7BEAD6FB9B6B3FFFFFFFFFFFFFFFFFFFFFFFFC7C8C7667E654F
            543220A1C22FB5CA4CC6CA78F1FCF0F2A6E3C378C68B42A95418A44D14C96A1D
            7B8F7230C7ED9DC9C4C0B89BBBB48BBEC799B6DFB5A3E8C87EDECFABDEACFFF6
            8CFFFFA3B5A46DC1BFBEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEA0
            9E9233A8BD1CC3E4A1E3CEA9F8F5DCD99DD3A050B16524A14610BD601AA37F4A
            418D95819587C0E4D5B7FFFF91F8FD7EE4EC8FE1D09EEFD06CE4DFBBE9AAFFFF
            96B1A16CCBC9C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6
            C1BE46ADAF29DAFDC6F2C8D2EABADEB568BA752FA3470FAF5516BA6C2845A8B2
            62C2D7BDAB8DBFA87DC5BF8AC0E0B29EF2DF4CE7FF41D9EECEE59AFFFC8AA899
            69D3D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDA
            D8D66FB2A17DFAFFD9F4C3EFD080C3863EA85117A54A10BB651F64958855888C
            AAA58BC6E7D7AEF5F292E6E398E1CFA6EFCC66DEDFCEE298FFF081A49267DCDB
            DBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEC
            EBE9A4AF96B8FFF8E1E1A3D39E50AE5F1FA1440DB15C1CB26C2B72968386AB9E
            AEAF92B1B691C0D0A0C2EEC180F1EF4DD9EAD0DB90FFE57BA08D6AEBE8E7FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
            FCFCA7AA8FFEFFC3E5BA6DB9712CA2460FA75115B16424B86E2BC5772BC18841
            B9A46AABBA9299C9B097DEC488DFCBD1D98FFBDC749A886AEAE8E7FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFC8C8C4B1A070C4873EB05316B04D10B86422B96C2ABA7531BF7F39C7883D
            CF9241D89D46DAAB54C9B86FDACE7EF7D26E988870F1F0EFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFE3E3E2B4AAA1987D6E8A5D43945020B06323C67C33D28F41D39B4B
            D5A353D5AB5BD8B460EECB6CEEC869968872F5F4F4FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F2DCD7D4B6AEAA958072936E4EA87845
            C59047DFB05BEFCA6EDFBB66988C7BF8F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7D9D6D5
            B4ADA89C8E80A082599C9182FDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          Content.Text = '$$$ 4444 555 666 777 888 999 1000 222'
          Content.TextPosition = tpBottomLeft
          Content.TextTop = 45
          ContentMaximized.ImageIndex = 74
          ContentMaximized.Text = 'Description for Tile 1'
          ContentMaximized.Extra = '$$$'
          StatusIndicator = '$$$$'
          DeleteIndicator = 'X'
          StatusIndicatorLeft = -20
          StatusIndicatorTop = 1
          SubTiles = <>
          Tag = 0
          Extra = '$$$'
        end
        item
          DeleteIndicator = 'X'
          SubTiles = <>
          Tag = 0
        end
        item
          DeleteIndicator = 'X'
          SubTiles = <>
          Tag = 0
        end
        item
          DeleteIndicator = 'X'
          SubTiles = <>
          Tag = 0
        end
        item
          DeleteIndicator = 'X'
          SubTiles = <>
          Tag = 0
        end
        item
          DeleteIndicator = 'X'
          SubTiles = <>
          Tag = 0
        end>
      Columns = 6
      TileAppearance.LargeViewFill.Color = 16776697
      TileAppearance.LargeViewFill.ColorTo = 16775920
      TileAppearance.LargeViewFill.ColorMirror = clNone
      TileAppearance.LargeViewFill.ColorMirrorTo = clNone
      TileAppearance.LargeViewFill.GradientType = gtSolid
      TileAppearance.LargeViewFill.GradientMirrorType = gtVertical
      TileAppearance.LargeViewFill.PicturePosition = ppTopCenter
      TileAppearance.LargeViewFill.PictureSize = psCustom
      TileAppearance.LargeViewFill.Opacity = 180
      TileAppearance.LargeViewFill.BorderColor = 16577242
      TileAppearance.LargeViewFill.Rounding = 0
      TileAppearance.LargeViewFill.ShadowOffset = 0
      TileAppearance.LargeViewFill.Glow = gmNone
      TileAppearance.SmallViewFill.Color = clWhite
      TileAppearance.SmallViewFill.ColorTo = clWhite
      TileAppearance.SmallViewFill.ColorMirror = clNone
      TileAppearance.SmallViewFill.ColorMirrorTo = clNone
      TileAppearance.SmallViewFill.GradientType = gtSolid
      TileAppearance.SmallViewFill.GradientMirrorType = gtVertical
      TileAppearance.SmallViewFill.PicturePosition = ppTopCenter
      TileAppearance.SmallViewFill.PictureSize = psCustom
      TileAppearance.SmallViewFill.Opacity = 180
      TileAppearance.SmallViewFill.BorderColor = clWhite
      TileAppearance.SmallViewFill.Rounding = 5
      TileAppearance.SmallViewFill.ShadowColor = clWhite
      TileAppearance.SmallViewFill.ShadowOffset = 0
      TileAppearance.SmallViewFill.Glow = gmNone
      TileAppearance.SmallViewFillSelected.Color = 10066329
      TileAppearance.SmallViewFillSelected.ColorTo = 10066329
      TileAppearance.SmallViewFillSelected.ColorMirror = clNone
      TileAppearance.SmallViewFillSelected.ColorMirrorTo = clNone
      TileAppearance.SmallViewFillSelected.GradientType = gtSolid
      TileAppearance.SmallViewFillSelected.GradientMirrorType = gtVertical
      TileAppearance.SmallViewFillSelected.PicturePosition = ppTopCenter
      TileAppearance.SmallViewFillSelected.PictureSize = psCustom
      TileAppearance.SmallViewFillSelected.Opacity = 180
      TileAppearance.SmallViewFillSelected.BorderColor = 10066329
      TileAppearance.SmallViewFillSelected.Rounding = 5
      TileAppearance.SmallViewFillSelected.ShadowColor = clNone
      TileAppearance.SmallViewFillSelected.ShadowOffset = 1
      TileAppearance.SmallViewFillSelected.Glow = gmNone
      TileAppearance.SmallViewFillSelected.GlowGradientColor = clNone
      TileAppearance.SmallViewFillSelected.GlowRadialColor = clNone
      TileAppearance.SmallViewFillDisabled.Color = 16513786
      TileAppearance.SmallViewFillDisabled.ColorTo = 15132390
      TileAppearance.SmallViewFillDisabled.ColorMirror = clNone
      TileAppearance.SmallViewFillDisabled.ColorMirrorTo = clNone
      TileAppearance.SmallViewFillDisabled.GradientType = gtSolid
      TileAppearance.SmallViewFillDisabled.GradientMirrorType = gtVertical
      TileAppearance.SmallViewFillDisabled.PicturePosition = ppTopCenter
      TileAppearance.SmallViewFillDisabled.PictureSize = psCustom
      TileAppearance.SmallViewFillDisabled.BorderColor = 14277081
      TileAppearance.SmallViewFillDisabled.BorderOpacity = 0
      TileAppearance.SmallViewFillDisabled.BorderWidth = 2
      TileAppearance.SmallViewFillDisabled.Rounding = 0
      TileAppearance.SmallViewFillDisabled.ShadowOffset = 1
      TileAppearance.SmallViewFillDisabled.Glow = gmNone
      TileAppearance.SmallViewFillHover.Color = 12369084
      TileAppearance.SmallViewFillHover.ColorTo = clSilver
      TileAppearance.SmallViewFillHover.ColorMirror = clNone
      TileAppearance.SmallViewFillHover.ColorMirrorTo = clNone
      TileAppearance.SmallViewFillHover.GradientType = gtSolid
      TileAppearance.SmallViewFillHover.GradientMirrorType = gtVertical
      TileAppearance.SmallViewFillHover.PicturePosition = ppTopCenter
      TileAppearance.SmallViewFillHover.PictureSize = psCustom
      TileAppearance.SmallViewFillHover.Opacity = 178
      TileAppearance.SmallViewFillHover.BorderColor = clSilver
      TileAppearance.SmallViewFillHover.Rounding = 5
      TileAppearance.SmallViewFillHover.ShadowColor = clNone
      TileAppearance.SmallViewFillHover.ShadowOffset = 1
      TileAppearance.SmallViewFillHover.Glow = gmNone
      TileAppearance.SmallViewFillHover.GlowGradientColor = clNone
      TileAppearance.SmallViewFillHover.GlowRadialColor = clNone
      TileAppearance.LargeViewFont.Charset = DEFAULT_CHARSET
      TileAppearance.LargeViewFont.Color = 5275647
      TileAppearance.LargeViewFont.Height = -16
      TileAppearance.LargeViewFont.Name = 'Tahoma'
      TileAppearance.LargeViewFont.Style = []
      TileAppearance.SmallViewFont.Charset = 163
      TileAppearance.SmallViewFont.Color = clWindowText
      TileAppearance.SmallViewFont.Height = -13
      TileAppearance.SmallViewFont.Name = 'Tahoma'
      TileAppearance.SmallViewFont.Style = []
      TileAppearance.SmallViewFontSelected.Charset = 163
      TileAppearance.SmallViewFontSelected.Color = clWindowText
      TileAppearance.SmallViewFontSelected.Height = -13
      TileAppearance.SmallViewFontSelected.Name = 'Tahoma'
      TileAppearance.SmallViewFontSelected.Style = []
      TileAppearance.SmallViewFontDisabled.Charset = 163
      TileAppearance.SmallViewFontDisabled.Color = clWhite
      TileAppearance.SmallViewFontDisabled.Height = -16
      TileAppearance.SmallViewFontDisabled.Name = 'Tahoma'
      TileAppearance.SmallViewFontDisabled.Style = []
      TileAppearance.SmallViewFontHover.Charset = 163
      TileAppearance.SmallViewFontHover.Color = clWindowText
      TileAppearance.SmallViewFontHover.Height = -13
      TileAppearance.SmallViewFontHover.Name = 'Tahoma'
      TileAppearance.SmallViewFontHover.Style = []
      TileAppearance.VerticalSpacing = 7
      TileAppearance.HorizontalSpacing = 7
      TileAppearance.TargetTileColor = clPurple
      TileAppearance.MovingTileColor = 42495
      TileAppearance.StatusIndicatorAppearance.Fill.Color = clNone
      TileAppearance.StatusIndicatorAppearance.Fill.ColorTo = clNone
      TileAppearance.StatusIndicatorAppearance.Fill.ColorMirror = clNone
      TileAppearance.StatusIndicatorAppearance.Fill.ColorMirrorTo = clNone
      TileAppearance.StatusIndicatorAppearance.Fill.GradientType = gtSolid
      TileAppearance.StatusIndicatorAppearance.Fill.GradientMirrorType = gtSolid
      TileAppearance.StatusIndicatorAppearance.Fill.BorderColor = 13434828
      TileAppearance.StatusIndicatorAppearance.Fill.BorderOpacity = 0
      TileAppearance.StatusIndicatorAppearance.Fill.Rounding = 10
      TileAppearance.StatusIndicatorAppearance.Fill.ShadowOffset = 0
      TileAppearance.StatusIndicatorAppearance.Fill.Glow = gmNone
      TileAppearance.StatusIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
      TileAppearance.StatusIndicatorAppearance.Font.Color = 8404992
      TileAppearance.StatusIndicatorAppearance.Font.Height = -13
      TileAppearance.StatusIndicatorAppearance.Font.Name = 'Tahoma'
      TileAppearance.StatusIndicatorAppearance.Font.Style = [fsBold]
      TileAppearance.StatusIndicatorAppearance.Glow = False
      TileAppearance.DeleteIndicatorAppearance.Fill.Color = clBlack
      TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirror = clNone
      TileAppearance.DeleteIndicatorAppearance.Fill.ColorMirrorTo = clNone
      TileAppearance.DeleteIndicatorAppearance.Fill.GradientType = gtSolid
      TileAppearance.DeleteIndicatorAppearance.Fill.GradientMirrorType = gtSolid
      TileAppearance.DeleteIndicatorAppearance.Fill.BorderColor = clWhite
      TileAppearance.DeleteIndicatorAppearance.Fill.Rounding = 0
      TileAppearance.DeleteIndicatorAppearance.Fill.ShadowOffset = 0
      TileAppearance.DeleteIndicatorAppearance.Fill.Glow = gmNone
      TileAppearance.DeleteIndicatorAppearance.Font.Charset = DEFAULT_CHARSET
      TileAppearance.DeleteIndicatorAppearance.Font.Color = clWhite
      TileAppearance.DeleteIndicatorAppearance.Font.Height = -11
      TileAppearance.DeleteIndicatorAppearance.Font.Name = 'Tahoma'
      TileAppearance.DeleteIndicatorAppearance.Font.Style = []
      TileMargins.Left = 0
      TileMargins.Top = 0
      TileMargins.Right = 0
      TileMargins.Bottom = 0
      Header.Visible = False
      Header.Fill.Color = 16579058
      Header.Fill.ColorTo = 16248537
      Header.Fill.ColorMirror = clNone
      Header.Fill.ColorMirrorTo = clNone
      Header.Fill.GradientType = gtVertical
      Header.Fill.GradientMirrorType = gtNone
      Header.Fill.BorderColor = 16374166
      Header.Fill.Rounding = 0
      Header.Fill.ShadowColor = 3355443
      Header.Fill.ShadowOffset = 0
      Header.Fill.Glow = gmNone
      Header.Height = 34
      Header.BulletSelectedColor = clTeal
      Header.BulletSize = 24
      Header.ArrowSize = 24
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clBlack
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Footer.Visible = False
      Footer.Fill.Color = 16579058
      Footer.Fill.ColorTo = 16248537
      Footer.Fill.ColorMirror = clNone
      Footer.Fill.ColorMirrorTo = clNone
      Footer.Fill.GradientType = gtVertical
      Footer.Fill.GradientMirrorType = gtNone
      Footer.Fill.BorderColor = 16374166
      Footer.Fill.Rounding = 0
      Footer.Fill.ShadowOffset = 0
      Footer.Fill.Glow = gmNone
      Footer.BulletSize = 24
      Footer.ArrowSize = 0
      Footer.ArrowRectangleSize = 0
      Footer.Font.Charset = DEFAULT_CHARSET
      Footer.Font.Color = clBlack
      Footer.Font.Height = -11
      Footer.Font.Name = 'Tahoma'
      Footer.Font.Style = []
      Options = []
      OnTileClick = TtVattuTileClick
      OnTileDblClick = TtVattuTileDblClick
      Align = alClient
      Ctl3D = True
      TabOrder = 2
      BevelInner = bvNone
      BevelOuter = bvNone
      DoubleBuffered = True
      ParentCtl3D = False
      ParentFont = False
      ExplicitTop = 57
      ExplicitHeight = 241
      TMSStyle = 0
    end
  end
  object QrNhom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from DM_CAMUNG'
      'order by MACAMUNG')
    Left = 272
    Top = 4
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 236
    Top = 240
  end
  object ActionList1: TActionList
    Left = 53
    Top = 121
    object CmdSoluong: TAction
      Caption = 'S'#7889' l'#432#7907'ng'
      ShortCut = 114
      OnExecute = CmdSoluongExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      ShortCut = 27
      OnExecute = CmdCloseExecute
    end
  end
end
