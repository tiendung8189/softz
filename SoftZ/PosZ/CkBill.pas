﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CkBill;

interface

uses
  Windows, Classes, Controls, Forms, System.SysUtils,
  StdCtrls, Buttons, ExtCtrls, DB, DBCtrls, kbmMemTable, Mask, Graphics,
  wwdbedit, RzButton;

type
  TFrmChietkhau = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    EdCK: TwwDBEdit;
    Label5: TLabel;
    EdGhichu: TwwDBEdit;
    Image1: TImage;
    DataSource1: TDataSource;
    tbTemp: TkbmMemTable;
    tbTempTL_CK: TFloatField;
    tbTempDGIAI: TWideStringField;
    Label2: TLabel;
    wwDBEdit1: TwwDBEdit;
    tbTempCHIETKHAU: TFloatField;
    BtClose: TRzBitBtn;
    BtReturn: TRzBitBtn;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CmdOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tbTempTL_CKChange(Sender: TField);
    procedure tbTempTL_CKValidate(Sender: TField);
    procedure tbTempCHIETKHAUValidate(Sender: TField);
  private
    mSotien: Double;
  public
  	function Get(sotien: Double; var ck: Double; var ghichu: String): Boolean;
  end;

var
  FrmChietkhau: TFrmChietkhau;

implementation

{$R *.DFM}

uses
	isLib, isMsg, exResStr, PosCommon, isCommon, exCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChietkhau.Get(sotien: Double; var ck: Double; var ghichu: String): Boolean;
begin
    mSotien := sotien;
	with tbTemp do
    begin
        Open;
        Append;
        FieldByName('TL_CK').AsFloat := ck;
        FieldByName('DGIAI').AsString := ghichu;
    end;

   	Result := ShowModal = mrOK;
    if Result then
		with tbTemp do
    	begin
        	ck := FieldByName('TL_CK').AsFloat;
            ghichu := FieldByName('DGIAI').AsString;
        end;

    tbTemp.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.tbTempCHIETKHAUValidate(Sender: TField);
var
    x: Double;
begin
    x := Sender.AsFloat;
    if (x < 0) or (x > mSotien ) or (x > 99999999) then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.tbTempTL_CKChange(Sender: TField);
var
    b: Boolean;
    tlck, mCk: Double;
begin
    if mTrigger then
        Exit;

    b := mTrigger;
    with tbTemp do
    begin
        mTrigger := True;
        if Sender.FieldName = 'TL_CK' then
        begin
            tlck := FieldByName('TL_CK').AsFloat;
            FieldByName('CHIETKHAU').AsFloat := exVNDRound(mSotien * tlck / 100.0, sysCurRound)
        end
        else
        begin
            mCk := FieldByName('CHIETKHAU').AsFloat;
            FieldByName('TL_CK').AsFloat := Iif(mCk=0, 0, 100 * (SafeDiv(mCk, mSotien)))
        end;
        mTrigger := b;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.tbTempTL_CKValidate(Sender: TField);
var
    x: Double;
begin
    x := Sender.AsFloat;
    if (x < 0) or (x > 100 ) then
    begin
        ErrMsg(RS_INVALID_DISCOUNT);
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    TFloatField(tbTempTL_CK).DisplayFormat := sysPerFmt;
    TFloatField(tbTempCHIETKHAU).DisplayFormat := sysCurFmt;
    EdCk.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.CmdOKClick(Sender: TObject);
begin
    tbTemp.CheckBrowseMode;
    ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChietkhau.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

end.
