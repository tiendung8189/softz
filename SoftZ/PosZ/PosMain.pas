﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PosMain;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, ActnList, Menus, ExtCtrls,
  StdCtrls, Wwdbigrd, Wwdbgrid2, Mask, Db, ADODB, wwdblook, isEnv, AppEvnts,
  AdvMenus, fcStatusBar, RzPanel, Controls, wwdbedit, HTMLabel,
  RzButton, isDb, ImgList, Grids, Wwdbgrid, pngimage, isOneInstance, exPrintBill,
  AdvGlassButton, AdvGlowButton, AdvSmoothButton, AdvMetroButton, AdvSelectors,
  AdvOfficeSelectors, wwdbdatetimepicker, RzSplit, AdvSmoothTileList,
  AdvSmoothTileListEx, GDIPPictureContainer, AdvAlertWindow,
  AdvSmoothTileListImageVisualizer, AdvSmoothTileListHTMLVisualizer,
  AdvOfficeStatusBar, AdvOfficeStatusBarStylers, rImprovedComps;

type
  TFrmMain = class(TForm)
    MyActionList: TActionList;
    CmdQuit: TAction;
    CmdSetpass: TAction;
    ImgLarge: TImageList;
    CmdSave: TAction;
    Bevel1: TBevel;
    QrBH: TADOQuery;
    DsBH: TDataSource;
    QrCTBH: TADOQuery;
    DsCTBH: TDataSource;
    QrCTBHMAVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    CmdReprint: TAction;
    CmdDiscount: TAction;
    QrTAM: TADOQuery;
    QrINLAI: TADOQuery;
    CmdScanReturnQty: TAction;
    QrCTBHSTT: TIntegerField;
    Bevel4: TBevel;
    CmdPrint: TAction;
    Popup: TAdvPopupMenu;
    CmdScanVIP: TAction;
    N1: TMenuItem;
    imtkhu1: TMenuItem;
    Hthng1: TMenuItem;
    CmdLock: TAction;
    CmdCommInfo: TAction;
    CmdSetPrinter: TAction;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    CmdSave2: TAction;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHRSTT: TIntegerField;
    PaHeader: TPanel;
    HTMLabel15: THTMLabel;
    EdCounter: TwwDBEdit;
    HTMLabel17: THTMLabel;
    EdCashier: TwwDBEdit;
    LbCode: TLabel;
    EdCode: TwwDBEdit;
    HTMLabel3: THTMLabel;
    HTMLabel6: THTMLabel;
    HTMLabel8: THTMLabel;
    wwDBEdit13: TwwDBEdit;
    wwDBEdit27: TwwDBEdit;
    wwDBEdit28: TwwDBEdit;
    Status: TfcStatusBar;
    ImageList1: TImageList;
    HTMLabel13: THTMLabel;
    EdCus: TwwDBEdit;
    CmdScanSku: TAction;
    CmdScanQty: TAction;
    HTMLabel1: THTMLabel;
    EdRno: TwwDBEdit;
    QrBHLK_TENKHO: TWideStringField;
    CmdChangeRetailer: TAction;
    QrBHLCT: TWideStringField;
    QrBHNGAY: TDateTimeField;
    QrBHQUAY: TWideStringField;
    QrBHCA: TWideStringField;
    QrBHSCT: TWideStringField;
    QrBHMAVIP: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHQUAYKE: TWideStringField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_BY: TIntegerField;
    QrBHCREATE_DATE: TDateTimeField;
    QrCTBHMABO: TWideStringField;
    spBANLE_CT_Invalid_Mavt: TADOCommand;
    QrDMVT_BO: TADOQuery;
    CmdReconnect: TAction;
    Ktnicsdliu1: TMenuItem;
    spBANLE_Paid: TADOCommand;
    QrDMVT: TADOQuery;
    CmdReLoad: TAction;
    GetUPDATE_DATE: TADOCommand;
    QrBHKHOA: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    CmdCongnobl: TAction;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHLK_TENTK: TWideStringField;
    QrBHLK_DAIDIEN: TWideStringField;
    QrBHLK_NGANHANG: TWideStringField;
    CmdDiscount1: TAction;
    isOneInstance1: TisOneInstance;
    QrBHPRINT_NO: TIntegerField;
    QrBHLOC: TWideStringField;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    CmdChitiet: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    AdvGlowButton12: TAdvGlowButton;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    AdvGlowButton15: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvGlowButton17: TAdvGlowButton;
    AdvGlowButton18: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    AdvGlowButton20: TAdvGlowButton;
    Panel11: TPanel;
    Panel12: TPanel;
    HtmlCurSoluong: THTMLabel;
    LbCurSoluong: TLabel;
    HTMLabel2: THTMLabel;
    HTMLabel5: THTMLabel;
    EdLocation: TwwDBLookupCombo;
    EdDate: TwwDBDateTimePicker;
    QrDMKHO: TADOQuery;
    CmdBanle: TAction;
    CmdTrahang: TAction;
    rhng1: TMenuItem;
    Hanbnl1: TMenuItem;
    QrCTBHTENVT: TWideStringField;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    RzSizePanel1: TRzSizePanel;
    Panel20: TPanel;
    AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler;
    AdvSmoothTileListHTMLVisualizer1: TAdvSmoothTileListHTMLVisualizer;
    AdvSmoothTileListImageVisualizer1: TAdvSmoothTileListImageVisualizer;
    AlFillter: TAdvAlertWindow;
    PicBan: TGDIPPictureContainer;
    Image48: TImageList;
    TtVattu: TAdvSmoothTileListEx;
    QrDMVT_CAMUNG: TADOQuery;
    CmdCamung: TAction;
    Panel19: TPanel;
    AdvGlowButton3: TAdvGlowButton;
    Panel21: TPanel;
    QrBHDELIVERY: TBooleanField;
    Panel22: TPanel;
    AdvGlowButton4: TAdvGlowButton;
    Panel23: TPanel;
    AdvGlowButton5: TAdvGlowButton;
    CmdPhieuquatang: TAction;
    CmdGiaohang: TAction;
    N3: TMenuItem;
    QrBHLK_TENVIP: TWideStringField;
    QrBHLK_VIP_DTHOAI: TWideStringField;
    QrBHLK_VIP_DCHI: TWideStringField;
    QrBH_id: TLargeintField;
    spCHECK_BILL_TRAHANG: TADOStoredProc;
    spPHIEUQUATANG_CT_Invalid: TADOStoredProc;
    N2: TMenuItem;
    Chnmyinhan1: TMenuItem;
    QrBHMAQUAY: TWideStringField;
    QrBHTinhTrang: TWideStringField;
    QrBHTyLeCKHD: TFloatField;
    QrBHTyLeCKVip: TFloatField;
    QrBHNhanVienCKHD: TIntegerField;
    QrBHSoLuong: TFloatField;
    QrBHThanhTien: TFloatField;
    QrBHSoTienCK: TFloatField;
    QrBHThanhTienSauCK: TFloatField;
    QrBHThanhToanTinhThue: TFloatField;
    QrBHThanhToanChuaThue: TFloatField;
    QrBHThanhToanChuaCL: TFloatField;
    QrBHSoTienCL: TFloatField;
    QrBHLoaiThue: TWideStringField;
    QrBHThueSuat: TFloatField;
    QrBHSoTienThue: TFloatField;
    QrBHSoTienThue_5: TFloatField;
    QrBHSoTienThue_10: TFloatField;
    QrBHSoTienThue_Khac: TFloatField;
    QrBHThanhToan: TFloatField;
    QrBHKhachDuaTienMat: TFloatField;
    QrBHThoiLaiTienMat: TFloatField;
    QrBHTongSoTienNhan: TFloatField;
    QrBHThanhToanTienMat: TFloatField;
    QrBHThanhToanTienMat_Lan1: TFloatField;
    QrBHThanhToanTienMat_Lan2: TFloatField;
    QrBHThanhToanTheNganHang: TFloatField;
    QrBHThanhToanTheNganHang_Lan1: TFloatField;
    QrBHThanhToanTheNganHang_Lan2: TFloatField;
    QrBHThanhToanTheNganHang_SoChuanChi: TWideStringField;
    QrBHThanhToanTraHang: TFloatField;
    QrBHThanhToanTraHang_SCT: TWideStringField;
    QrBHThanhToanTraHang_Khoa: TGuidField;
    QrBHThanhToanPhieuQuaTang: TFloatField;
    QrBHThanhToanPhieuQuaTang_MaThe: TWideStringField;
    QrBHThanhToanViDienTu: TFloatField;
    QrBHThanhToanViDienTu_Loai: TWideStringField;
    QrBHThanhToanChuyenKhoan: TFloatField;
    QrBHThanhToanDiemTichLuy: TFloatField;
    QrBHGhiChu: TWideMemoField;
    QrBHNhanVienThuNgan: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDEBT_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDEBT_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHMADT: TWideStringField;
    QrCTBHThanhTien: TFloatField;
    QrCTBHTyLeCKMH: TFloatField;
    QrCTBHTyLeCKHD: TFloatField;
    QrCTBHTyLeCKVipNhomHang: TFloatField;
    QrCTBHTyLeCKVip: TFloatField;
    QrCTBHTyLeCKPhieu: TFloatField;
    QrCTBHTyLeCKDaChieu: TFloatField;
    QrCTBHTyLeCKBo: TFloatField;
    QrCTBHTyLeCK_Max: TFloatField;
    QrCTBHTyLeCK_Them: TFloatField;
    QrCTBHTyLeCK: TFloatField;
    QrCTBHDonGiaCK: TFloatField;
    QrCTBHSoTienCK: TFloatField;
    QrCTBHThanhTienSauCK: TFloatField;
    QrCTBHDonGiaTinhThue: TFloatField;
    QrCTBHThanhToanTinhThue: TFloatField;
    QrCTBHThanhToanChuaCL: TFloatField;
    QrCTBHSoTienCL: TFloatField;
    QrCTBHDonGiaChuaThue: TFloatField;
    QrCTBHThanhToanChuaThue: TFloatField;
    QrCTBHLoaiThue: TWideStringField;
    QrCTBHThueSuat: TFloatField;
    QrCTBHSoTienThue: TFloatField;
    QrCTBHSoTienThue_5: TFloatField;
    QrCTBHSoTienThue_10: TFloatField;
    QrCTBHSoTienThue_Khac: TFloatField;
    QrCTBHThanhToan: TFloatField;
    QrCTBHNhanVienCKMH: TIntegerField;
    QrCTBHLOC: TWideStringField;
    QrCTBH_id: TLargeintField;
    QrCTBHDonGia: TFloatField;
    QrCTBHSoLuong: TFloatField;
    QrCTBHSoTienThueChuaRound: TFloatField;
    QrBHSoTienThueChuaRound: TFloatField;
    Panel24: TPanel;
    AdvGlowButton6: TAdvGlowButton;
    CmdKetCa: TAction;
    QrBHThanhToanChuyenKhoan_SoTaiKhoan: TWideStringField;
    QrBHThanhToanTraHang_TriGia: TFloatField;
    QrBHThanhToanPhieuQuaTang_TriGia: TFloatField;
    QrBHThanhToanChuyenKhoan_LoaiHinh: TWideStringField;
    QrTaiKhoan_LoaiHinh: TADOQuery;
    QrBHLK_TaiKhoan_LoaiHinh: TWideStringField;
    Panel25: TPanel;
    AdvGlowButton7: TAdvGlowButton;
    QrCTBHMaNoiBo: TWideStringField;
    procedure CmdQuitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdSetpassExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdReprintExecute(Sender: TObject);
    procedure QrBHAfterPost(DataSet: TDataSet);
    procedure MyActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrBHAfterInsert(DataSet: TDataSet);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure QrCTBHAfterPost(DataSet: TDataSet);
    procedure CmdDiscountExecute(Sender: TObject);
    procedure QrDBError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure CmdScanReturnQtyExecute(Sender: TObject);
    procedure QrBHBeforeOpen(DataSet: TDataSet);
    procedure CmdHelpExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
      FooterCellRect: TRect; Field: TField; FooterText: String;
      var DefaultDrawing: Boolean);
    procedure GrListUpdateFooter(Sender: TObject);
    procedure GrListKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAboutExecute(Sender: TObject);
    procedure CmdScanVIPExecute(Sender: TObject);
    procedure CmdLockExecute(Sender: TObject);
    procedure CmdCommInfoExecute(Sender: TObject);
    procedure CmdFaqExecute(Sender: TObject);
    procedure QrCTBHSoLuongChange(Sender: TField);
    procedure CmdSetPrinterExecute(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure TntFormResize(Sender: TObject);
    procedure CmdSave2Execute(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure QrCTBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHAfterDelete(DataSet: TDataSet);
    procedure EdCodeEnter(Sender: TObject);
    procedure EdCodeKeyPress(Sender: TObject; var Key: Char);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdScanSkuExecute(Sender: TObject);
    procedure CmdScanQtyExecute(Sender: TObject);
    procedure QrTAMBeforeOpen(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdCodeExit(Sender: TObject);
    procedure QrBHTTOAN1Change(Sender: TField);
    procedure QrCTBHTyLeCKMHChange(Sender: TField);
    procedure QrBHMAKHOChange(Sender: TField);
    procedure CmdChangeRetailerExecute(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure CmdReLoadExecute(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCongnoblExecute(Sender: TObject);
    procedure CmdDiscount1Execute(Sender: TObject);
    procedure QrBHCN_DIACHI_HDChange(Sender: TField);
    procedure CmdChitietExecute(Sender: TObject);
    procedure CmdBanleExecute(Sender: TObject);
    procedure CmdTrahangExecute(Sender: TObject);
    procedure QrCTBHTyLeCK_MAXChange(Sender: TField);
    procedure TtVattuTileClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure TtVattuTileDblClick(Sender: TObject; Tile: TAdvSmoothTile;
      State: TTileState);
    procedure CmdCamungExecute(Sender: TObject);
    procedure QrBHTTOAN3_SCTValidate(Sender: TField);
    procedure QrBHTTOAN3_SCTChange(Sender: TField);
    procedure CmdPhieuquatangExecute(Sender: TObject);
    procedure CmdGiaohangExecute(Sender: TObject);
    procedure QrBHKhachDuaTienMatChange(Sender: TField);
    procedure QrBHTTOAN4_INPUTValidate(Sender: TField);
    procedure QrBHTTOAN4_INPUTChange(Sender: TField);
    procedure QrCTBHThanhTienChange(Sender: TField);
    procedure QrCTBHThanhTienSauCKChange(Sender: TField);
    procedure QrCTBHThanhToanTinhThueChange(Sender: TField);
    procedure QrCTBHThanhToanChange(Sender: TField);
    procedure CmdKetCaExecute(Sender: TObject);
    procedure QrBHThanhToanChuyenKhoan_SoTaiKhoanChange(Sender: TField);
  private
  	mVATMode: Integer;
    mLastDate, mLastUpDate: TDateTime;
    mNhanTT: Double;
    mNhacnhoTT: Boolean;

    mPrinter: TexPrintBill;

    // Hóa đơn hiện tại
    curKhoa: TGUID;

    function  IsEnableFunc(const pFunc: String): Boolean;
    procedure OpenQueries;
    procedure Total(pReOrder: Boolean= False);

    procedure GetCustomerName;

    procedure Cleanup;
	procedure ScanType;

    function  ValidBarcode(pma:String; var bo: Boolean): Boolean;

    procedure ProcScanCode;
    procedure ProcScanQty;
    procedure ProcScanVIP;
    procedure ProcScanRetQty;

    function IsChangeDMHH: Boolean;
    procedure posPaid;
  protected
    procedure tbAddUnit;
  public
    mSqlTam: String;
    dThanhtoan : Boolean;
    // Thông tin mặt hàng scan sau cùng
    curStt: Integer;
    curSoluong, curGiaban, curTyLeCKVipNhomHang, curTyLeCKPhieu, curTyLeCKBo, curTyLeCKDaChieu,
    curThueSuat, curSoLuongChiTietBo: Double;
    curLoaiThue, curTenVT, curDvt, curMaNoiBo, mVipTen : String;
    mDefSoluong: Double;

    procedure InvalidAction(msg: String = '');
    function  GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
    procedure ItemAddNew(const ma: String; const pMabo: String = '');
    procedure ItemSetQty(const pSoluong: Double);

    procedure GoLast;
    function  SaveBill(pLoai: Integer; pDesc: String = ''): Boolean;
    procedure PrintBill(pKhoa: TGUID);
    Function exThelenh: Boolean;
  end;

var
  FrmMain: TFrmMain;

implementation

uses
	MainData, PosCommon, isMsg, isStr, SoBill, Rights, CkBill, isDba, isOdbc,
    isLib, PosCommoInfo, RepEngine, Printer, TempReceipt, CkMa,
    ReceiptDesc, isCommon, exResStr, PosVIP, Retailer, Payment,
    Variants, ADOInt, GuidEx, TheLenh, exCOMPort, SystemCriticalU, ExCommon,
    PosChitiet, Banle, PosCamung, SetGhichu, ThongtinCN, BanleCN, NhapTraBanLe,
  PosKetCa;

{$R *.DFM}

const
	FORM_CODE = 'POS';

{$REGION '<< Form events >>'}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormCreate(Sender: TObject);
const
	imgext: array[1..3] of String = ('png', 'jpg', 'bmp');
var
	i: Integer;
begin

    // Smart load logo
//    for i := 1 to Length(imgext) do
//    begin
//	    s := sysAppPath + 'System\POSz.' + imgext[i];
//	    if FileExists(s) then
//        begin
//    	    Image.Picture.LoadFromFile(s);
//            Break;
//        end;
//    end;

//    GrList.PaintOptions.AlternatingRowColor := clNone;
    mSqlTam := QrTAM.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

procedure TFrmMain.FormShow(Sender: TObject);
const
	imgext: array[1..3] of String = ('png', 'jpg', 'bmp');
var
	i, h: Integer;
	s: String;
begin
	TMyForm(Self).Init2;
    LoadCustomIcon;
//    Status.Panels[5].Text := 'Build ' + GetModuleVersion;
    DbeInitial;

    // Smart load logo
    for i := 1 to Length(imgext) do
    begin
	    s := sysAppPath + 'System\PosZ.' + imgext[i];
	    if FileExists(s) then
        begin
//    	    Image.Picture.LoadFromFile(s);
//            Break;
        end;
    end;

    // Other init
    SetDefButton(1);

    ShortTimeFormat := 'hh:nn';
	mTrigger := False;
    mLastDate := Now;

	    (*
    	** Optional settings
	    *)
    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger(FORM_CODE, 'VAT Form');

    dThanhtoan := FlexConfigBool(FORM_CODE, 'Default Thanh Toan', True);
    mNhanTT := GetSysParam('BL_DONVI_TIENTHOI');
    mNhacnhoTT := GetSysParam('BL_NHACNHO_TIENTHOI');

    // Init bill printer
    exBillInitial(mPrinter);

    CmdChangeRetailer.Visible := IsEnableFunc('POS_CHANGE_RETAILER');
    CmdScanVIP.Enabled := GetRights('POS_VIP', False) <> R_DENY;

//    EdDate.ReadOnly := not sysIsCentral;
//    EdDate.ShowButton := sysIsCentral;
//    EdLocation.ReadOnly := not sysIsCentral;
//    EdLocation.ShowButton := sysIsCentral;
//
    // Tuy bien luoi
	SetCustomGrid(FORM_CODE, GrList);

	// Cleanup empty receipt
	Cleanup;

    // Open database
    with DataMain do
    begin
    	OpenDatasets([QrDMKHO, QrUser, QrDMTK, QrLOAI_VIDIENTU, QrBANLE_TAIKHOAN]);
    end;
    OpenDataSets([QrDMKHO]);

    // OpenDataSets([QrDMVT]);
    IsChangeDMHH;
	OpenQueries;

    // Display format
    SetShortDateFormat(QrBH);
    SetDisplayFormat(QrBH,['Ngay'], ShortDateFormat + ' ' + ShortTimeFormat);
	SetDisplayFormat(QrBH, sysCurFmt);
    SetDisplayFormat(QrBH,['SoLuong'], sysQtyFmt);

    SetDisplayFormat(QrDmvt, sysCurFmt);
    SetDisplayFormat(QrDmvt, ['TyLeLai'], sysPerFmt);

    with QrCTBH do
    begin
		SetDisplayFormat(QrCTBH, sysCurFmt);
		SetDisplayFormat(QrCTBH, ['SoLuong'], sysQtyFmt);
		SetDisplayFormat(QrCTBH, ['TyLeCK', 'TyLeCKMH', 'TyLeCKHD', 'TyLeCKBo',
                        'TyLeCKVip', 'TyLeCKVipNhomHang', 'TyLeCKPhieu'], sysPerFmt);
        SetDisplayFormat(QrCTBH, ['ThueSuat'], sysTaxFmt);
    end;

 	// Form initial
    // SetApplicationVersion;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'SoftZ', sysAppPath + '\External Data');

    //Touch
    with TtVattu do
    begin
        Columns := round(Width/150);
        Rows := Round(Height/50);
    end;
    tbAddUnit;

    EdCashier.Text := sysLogonFullName;
	MyActionList.Tag := 1;
    TExComPort.IcdComCreate(GetSysParam('ICD_STR1'), GetSysParam('ICD_STR2'));
    SystemCritical.IsCritical := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
	b: Boolean;
begin
    // Cho close nếu thấy connection failured
	b := False;
	{try
		GetDbServerDate;
    except
		b := True;
    end;   }

    if b and (DataMain.Conn.Errors.Count > 0) then
    begin
    	CanClose := True;
        Exit;
    end;

    // Confirm for bill complete
    CanClose := QrCTBH.IsEmpty;
    if not CanClose then
    begin
    	ErrMsg('Phải hoàn tất hóa đơn.');
        Exit;
    end;

    with QrBH do
    if (State in [dsInsert]) and (QrBH.FieldByName('ThanhToan').Value = 0) then
    begin
        Cancel
    end else
    	SaveBill(2);

    // Clean
	Cleanup;

	with QrTAM do
    begin
		Close;

        SQL.Text := mSqlTam;
    	Open;
        CanClose := IsEmpty;
    end;

    if not CanClose then
        ErrMsg('Phải giải quyết tất cả hóa đơn còn treo lại.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    exBillFinal(mPrinter);

    try
//        DataMain.isLogon.Logoff;
    except
    end;

    DbeFinal;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if ActiveControl = EdCode then
    	Exit;
	Enter2Tab(Self, Key);
end;
{$ENDREGION}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetpassExecute(Sender: TObject);
begin
	DataMain.isLogon.ResetPassword;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLockExecute(Sender: TObject);
begin
	DataMain.isLogon.Lock;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHelpExecute(Sender: TObject);
begin
	ShowHelp;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKetCaExecute(Sender: TObject);
var
    b: Boolean;
begin
    b := not PosCommon.PosKetCa(0, 0);

    if FrmPosKetca.Execute(b) then
        Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdAboutExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmAbout, FrmAbout);
//    FrmAbout.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBanleExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_SUABILL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBanle, FrmBanle);
    FrmBanle.Execute(r, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
    if not YesNo(RS_RECONNECT) then
        Exit;
//    DbConnect;
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReLoadExecute(Sender: TObject);
begin
    Wait(DATAREADING);
    with QrDMVT do
        if Active then
            Requery
        else
            Open;

    with QrDMVT_CAMUNG do
        if Active then
            Requery
        else
            Open;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bEmpty, bDelivery: Boolean;
begin
    with QrBH do
    begin
        if not Active then
            Exit;

        bDelivery := FieldByName('DELIVERY').AsBoolean;
    end;
	with QrCTBH do
    begin
        if not Active then
            Exit;

		bEmpty := IsEmpty;
    end;

    CmdChangeRetailer.Enabled :=  bEmpty;
    CmdScanReturnQty.Enabled := not bEmpty;
    CmdReprint.Enabled := bEmpty;
//    CmdSetPrinter.Enabled := mPrinter.PrintType = 0;
//    CmdSetPrinter.Visible := mPrinter.PrintType = 0;

    CmdDiscount.Enabled := not bEmpty;
    CmdDiscount1.Enabled := not bEmpty;
    CmdPrint.Enabled := not bEmpty;

    CmdSave.Enabled := not bDelivery;
//    CmdSave2.Enabled := not bEmpty;
    CmdChitiet.Enabled :=  not bEmpty;

    CmdPhieuquatang.Enabled := bEmpty;
    CmdGiaohang.Enabled := bEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCamungExecute(Sender: TObject);
begin
//    mDefSoluong := 1;
    Application.CreateForm(TFrmPosCamung, FrmPosCamung);
    FrmPosCamung.Execute(QrCTBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChangeRetailerExecute(Sender: TObject);
var
    n, mUser: Integer;
    sRole: String;
begin
    with QrTAM do
    begin
		Close;

        SQL.Text := mSqlTam;
    	Open;
        n := RecordCount;
        if (n > 1) or ((n = 1) and
            (not TGuidEx.EqualGuids(
            	TGuidField(FieldByName('KHOA')).AsGuid,
            	TGuidField(QrBH.FieldByName('KHOA')).AsGuid))) then
        begin
            ErrMsg('Phải giải quyết tất cả hóa đơn còn treo lại.');
            Exit;
        end;
    end;

    sRole := 'POS_CHANGE_RETAILER';
    if GetRights(sRole, False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, sRole) = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    Application.CreateForm(TFrmRetailer, FrmRetailer);
    if FrmRetailer.execute(posMakho, posMaQuay, posQuay, posQuay0) then
    with QrBH do
    begin
        FieldByName('MaKho').AsString := posMakho;
        FieldByName('MaQuay').AsString := posMaQuay;
        FieldByName('Quay').AsString := posQuay;
        posSct := AllocRetailBillNumber(posMaQuay, Date);
        FieldByName('SCT').AsString := posSct;
        posSct := isSmartInc(posSct, 2);
    end;
    FrmRetailer.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCommInfoExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmCommoInfo, FrmCommoInfo);
    FrmCommoInfo.Execute(DataMain.QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongnoblExecute(Sender: TObject);
begin
//    r := GetRights('SZ_SUABILL');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmBanleCN, FrmBanleCN);
    FrmBanleCN.Execute(R_FULL, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSaveExecute(Sender: TObject);
var
    s: string;
begin
    s := '';
    if QrCTBH.IsEmpty then
    begin
        if isSmartInc(QrBH.FieldByName('SCT').AsString, 2) = posSct then
            Exit;

        Application.CreateForm(TFrmSetGhichu, FrmSetGhichu);
        if not FrmSetGhichu.Execute(s) then
            Exit;
    end
    else if not YesNo('Chỉ lưu và không in hóa đơn. Tiếp tục?') then
        Exit;

    with QrBH do
    begin
        SetEditState(QrBH);
        FieldByName('KhachDuaTienMat').AsFloat := FieldByName('ThanhToan').AsFloat;
    end;

    SaveBill(0);
    curKhoa := TGuidEx.EmptyGuid;
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanQtyExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 1;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanSkuExecute(Sender: TObject);
begin
	with EdCode do
    begin
    	Tag := 0;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PrintBill(pKhoa: TGUID);
begin
    try
        if mPrinter.BillPrint(pKhoa) then
            DataMain.UpdatePrintNo(pKhoa);
    except
        on E: Exception do
            ErrMsg(E.Message, 'Lỗi in Bill');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPrintExecute(Sender: TObject);
begin
    if QrCTBH.IsEmpty then
    	Exit;

    Application.CreateForm(TFrmPayment, FrmPayment);
    if not FrmPayment.Execute(1) then
    	Exit;

	PrintBill(curKhoa);
    curKhoa := TGuidEx.EmptyGuid;
    OpenQueries;
    EdCode.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_RECEIPT_NO = 'Số hóa đơn không đúng.';

procedure TFrmMain.CmdReprintExecute(Sender: TObject);
var
    s: String;
    m, y: Integer;
    dd, mm, yy: WORD;
    k: TGUID;
begin
	EdCode.SetFocus;
   	DecodeDate(Date, yy, mm, dd);
    m := mm;
    y := yy;
	s := lastSct;

    Application.CreateForm(TFrmSoBill, FrmSoBill);
	if not FrmSoBill.GetBillNo(m, y, s) then
        Exit;

	// Lay khoa chung tu
    with QrINLAI do
    begin
    	Parameters[0].Value := m;
    	Parameters[1].Value := y;
    	Parameters[2].Value := posMaQuay;
        Parameters[3].Value := s;
    	Open;
        if IsEmpty then
        begin
        	Msg(RS_RECEIPT_NO);
            Close;
            Exit;
        end;
        k := TGuidField(FieldByName('KHOA')).AsGuid;
        Close;
    end;

    // In lai
    PrintBill(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscount1Execute(Sender: TObject);
var
	mMa, sRole: String;
    mTyLeCKMH: Double;
    bm: TBytes;
    mUser: Integer;
begin
	if QrCTBH.FieldByName('DONGIA').AsFloat = 0.0 then
		Exit;

    sRole := 'POS_CHIETKHAU_MH';

    if GetRights(sRole, False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, sRole) = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

	with QrCTBH do
    begin
    	if IsEmpty then
        	Exit;

        mTyLeCKMH := FieldByName('TyLeCKMH').AsFloat;

        Application.CreateForm(TFrmCkma, FrmCkma);
        if not FrmCkma.GetDiscount(FieldByName('TenVT').AsString, FieldByName('ThanhTien').AsFloat, mTyLeCKMH) then
        	Exit;

        mMa := FieldByName('MAVT').AsString;
        bm := BookMark;
        DisableControls;

        First;
        while not Eof do
        begin
        	if (FieldByName('MAVT').AsString = mMa) and
               (FieldByName('DonGia').AsFloat <> 0.0) then
            begin
		        Edit;
				FieldByName('TyLeCKMH').AsFloat := mTyLeCKMH;
				FieldByName('NhanVienCKMH').AsInteger := mUser;
            end;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;
	end;
    EdCode.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDiscountExecute(Sender: TObject);
var
	mUser: Integer;
    ghiChu, sRole: String;
    mTyLeCKHD: Double;
begin
    EdCode.SetFocus;
    sRole := 'POS_CHIETKHAU';

    if GetRights(sRole, False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, sRole) = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    with QrBH do
    begin
        mTyLeCKHD := FieldByName('TyLeCKHD').AsFloat;
        ghiChu := FieldByName('GhiChu').AsString;
    end;
    // Get discount info.
    Application.CreateForm(TFrmChietkhau, FrmChietkhau);
    if not FrmChietkhau.Get(QrBH.FieldByName('ThanhTien').AsFloat, mTyLeCKHD, ghiChu) then
    	Exit;

    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('TyLeCKHD').AsFloat := mTyLeCKHD;
        FieldByName('NhanVienCKHD').AsInteger := mUser;
        FieldByName('GhiChu').AsString := ghiChu;
    end;
    Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdScanReturnQtyExecute(Sender: TObject);
var
	mUser: Integer;
begin
	EdCode.SetFocus;
    if GetRights('POS_TRAHANG', False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
        if DataMain.GetPassCode(posMakho) <> '' then  //test. Will remove
        begin
            if not exThelenh then
                Exit;
            mUser := sysLogonUID;
        end
        else
        begin
            // Logon
            if not DataMain.isLogon.Logon3(mUser) then
                Exit;

            if GetRights(mUser, 'POS_TRAHANG') = R_DENY then
            begin
                DenyMsg;
                Exit;
            end;
        end;
    end;

	with EdCode do
    begin
    	Tag := 3;
        Text := '';
		SetFocus;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_VIP = 'Sai mã khách hàng thân thiết.';

procedure TFrmMain.CmdScanVIPExecute(Sender: TObject);
begin
//	with EdCode do
//    begin
//    	Tag := 2;
//        Text := '';
//		SetFocus;
//    end;
//    ScanType;
    ProcScanVIP;
end;
	(*
    ** Functions
    *)
(*==============================================================================
**------------------------------------------------------------------------------\
pLoai:
    0: Save           --> paid
    1: Save and Print --> paid
    2: Save temp      --> not paid
    3: Save and delivery    --> paid
*)
function TFrmMain.SaveBill(pLoai: Integer; pDesc: String): Boolean;
var
    bPrinted, bPosPaid: Boolean;
    candoi: Double;
begin
   	Result := False;
	with QrCTBH do
    begin
    	CheckBrowseMode;
    	if IsEmpty and (isSmartInc(QrBH.FieldByName('SCT').AsString) = posSct) then
            Exit;
	end;

    candoi := 0;
    with QrCTBH do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('SoTienCL').AsFloat <> 0 then
            begin
                Edit;
                FieldByName('SoTienCL').AsFloat := 0;
                CheckBrowseMode;
            end;
            Next;
        end;
    end;

    with QrBH do
    begin
        if (FieldByName('ThanhToan').AsFloat <> 0) then
        begin
            candoi := Round(FieldByName('ThanhToan').AsFloat
                        - FieldByName('SoTienThue').AsFloat
                        - FieldByName('ThanhToanChuaThue').AsFloat)
        end;
    end;

    if candoi <> 0 then
    with QrCTBH do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('SoTienThue').AsFloat <> 0 then
            begin
                Edit;
                FieldByName('SoTienCL').AsFloat := candoi;
                CheckBrowseMode;
                Break;
            end;
            Next;
        end;
    end;

    Total(True);
    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

        case pLoai of
            1:  begin
                    bPrinted := True;
                    bPosPaid := True;
                end;
            2:  begin
                    bPrinted := False;
                    bPosPaid := False;

                    FieldByName('GhiChu').AsString := pDesc;
                end;
            3:  begin
                    bPrinted := True;
                    bPosPaid := True;

                end;
        else    begin
                    bPrinted := False;
                    bPosPaid := True;

                    mTrigger := True;
                    FieldByName('KhachDuaTienMat').AsFloat := FieldByName('ThanhTien').AsFloat;
                    mTrigger := False;
                end;
        end;

        FieldByName('PRINTED').AsBoolean := bPrinted;
        Post;

        if bPosPaid then
            posPaid;
    end;

    // Done
   	Result := True;
    icdComPort.IcdThanks;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.Total;
var
    bm: TBytes;
    dSoLuong, dThanhTien, dSoTienCk, dThanhTienSauCK, dThanhToanTinhThue, dThanhToanChuaThue,
    dThanhToanChuaCL, dSoTienCL, dSoTienThue, dSoTienThue_5, dSoTienThue_10, dSoTienThue_Khac, dThanhToan: Double;
	mSoLuong, mThanhTien, mSoTienCk, mThanhTienSauCK, mThanhToanTinhThue, mThanhToanChuaThue,
    mThanhToanChuaCL, mSoTienCL, mSoTienThueChuaRound, mSoTienThue, mSoTienThue_5, mSoTienThue_10, mSoTienThue_Khac, mThanhToan: Double;
    mTyLeCKHD, mTyLeCKVip: Double;
begin
	if mTrigger then
    	Exit;

	mSoLuong := 0;
    mThanhTien := 0;
    mSoTienCk := 0;
    mThanhTienSauCK := 0;
    mThanhToanTinhThue := 0;
    mThanhToanChuaThue := 0;
    mThanhToanChuaCL := 0;
    mSoTienCL := 0;
    mSoTienThueChuaRound := 0;
    mSoTienThue := 0;
    mSoTienThue_5 := 0;
    mSoTienThue_10 := 0;
    mSoTienThue_Khac := 0;
    mThanhToan  := 0;

    mTyLeCKHD := QrBH.FieldByName('TyLeCKHD').AsFloat;
    mTyLeCKVip := QrBH.FieldByName('TyLeCKVip').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
            if pReOrder and (FieldByName('STT').AsInteger <> FieldByName('RSTT').AsInteger)then
            begin
                try
                    mTrigger := True;
                    SetEditState(QrCTBH);
                    FieldByName('STT').AsInteger := FieldByName('RSTT').AsInteger;
                    Post;
                finally
                    mTrigger := False;
                end;
            end;

            if FieldByName('TyLeCKHD').AsFloat <> mTyLeCKHD then
            begin
                SetEditState(QrCTBH);
            	FieldByName('TyLeCKHD').AsFloat := mTyLeCKHD;
            end;

            // CK VIP theo danh muc VIP
        	if FieldByName('TyLeCKVip').AsFloat <> mTyLeCKVip then
            begin
                SetEditState(QrCTBH);
            	FieldByName('TyLeCKVip').AsFloat := mTyLeCKVip;
            end;

            // Xoa chiet khau khi xoa VIP  - theo ck nhom hang
            if (QrBH.FieldByName('MAVIP').AsString = '') and (FieldByName('TyLeCKVipNhomHang').AsFloat > 0) then
            begin
                SetEditState(QrCTBH);
                FieldByName('TyLeCKVipNhomHang').Clear;
            end;

            dSoLuong := FieldByName('SoLuong').AsFloat;
            dThanhTien := FieldByName('ThanhTien').AsFloat;
            dSoTienCk := FieldByName('SoTienCk').AsFloat;
            dThanhTienSauCK := FieldByName('ThanhTienSauCK').AsFloat;
            dThanhToanTinhThue := FieldByName('ThanhToanTinhThue').AsFloat;
            dThanhToanChuaThue := FieldByName('ThanhToanChuaThue').AsFloat;
            dThanhToanChuaCL := FieldByName('ThanhToanChuaCL').AsFloat;
            dSoTienCL := FieldByName('SoTienCL').AsFloat;
            dSoTienThue := FieldByName('SoTienThue').AsFloat;
            dSoTienThue_5 := FieldByName('SoTienThue_5').AsFloat;
            dSoTienThue_10 := FieldByName('SoTienThue_10').AsFloat;
            dSoTienThue_Khac := FieldByName('SoTienThue_Khac').AsFloat;
            dThanhToan  := FieldByName('ThanhToan').AsFloat;


	        mSoluong := mSoluong + dSoLuong;
	        mThanhTien := mThanhTien + dThanhTien;
            mSoTienCk := mSoTienCk + dSoTienCk;
            mThanhTienSauCK := mThanhTienSauCK + dThanhTienSauCK;
            mThanhToanTinhThue := mThanhToanTinhThue + dThanhToanTinhThue;
            mThanhToanChuaThue := mThanhToanChuaThue + dThanhToanChuaThue;
            mThanhToanChuaCL := mThanhToanChuaCL + dThanhToanChuaCL;
            mSoTienCL := mSoTienCL + dSoTienCL;
            mSoTienThueChuaRound := mSoTienThueChuaRound + dSoTienThue;

            mSoTienThue_5 := mSoTienThue_5 + dSoTienThue_5;
            mSoTienThue_10 := mSoTienThue_10 + dSoTienThue_10;
            mSoTienThue_Khac := mSoTienThue_Khac + dSoTienThue_Khac;
            mThanhToan := mThanhToan + dThanhToan;

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('SoLuong').AsFloat := mSoLuong;
        FieldByName('ThanhTien').AsFloat  := mThanhTien;
        FieldByName('SoTienCk').AsFloat  := mSoTienCk;
        FieldByName('ThanhTienSauCK').AsFloat  := mThanhTienSauCK;
        FieldByName('ThanhToanTinhThue').AsFloat  := mThanhToanTinhThue;
        FieldByName('ThanhToanChuaThue').AsFloat  := mThanhToanChuaThue;
        FieldByName('ThanhToanChuaCL').AsFloat  := mThanhToanChuaCL;
        FieldByName('SoTienCL').AsFloat  := mSoTienCL;
        FieldByName('SoTienThueChuaRound').AsFloat := mSoTienThueChuaRound;
        mSoTienThue := exVNDRound(mSoTienThueChuaRound, 0);
        FieldByName('SoTienThue').AsFloat := mSoTienThue;
        FieldByName('SoTienThue_5').AsFloat := mSoTienThue_5;
        FieldByName('SoTienThue_10').AsFloat := mSoTienThue_10;
        FieldByName('SoTienThue_Khac').AsFloat := mSoTienThue_Khac;
        FieldByName('ThanhToan').AsFloat := mThanhToan;;

        CheckBrowseMode;
    end;

    GrListUpdateFooter(GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TtVattuTileClick(Sender: TObject; Tile: TAdvSmoothTile;
  State: TTileState);
begin
    QrDMVT_CAMUNG.Locate('MAVT', Tile.Data, []);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TtVattuTileDblClick(Sender: TObject; Tile: TAdvSmoothTile;
  State: TTileState);
var
    s: String;
begin
    s := QrDMVT_CAMUNG.FieldByName('MAVT').AsString;
    if not (State in [tsMaximized]) then
    begin
        with QrCTBH do
        begin
        // Mã cũ
            if Locate('MAVT;MABO', VarArrayOf([s, Null]), []) then
            begin
                GetSalingPrice(s);

                if mDefSoluong > 0 then
                    ItemSetQty(FieldByName('SoLuong').AsFloat + mDefSoluong)
                else
                    ItemSetQty(FieldByName('SoLuong').AsFloat + 1)
            end
            // Mã mới
            else
            begin
                // Sai ma
                if not GetSalingPrice(s) then
                begin
                    InvalidAction(RS_POS_CODE_FAIL);
                    Exit;
                end;

                // Chua co gia ban
                if curGiaban = 0 then
                begin
                    Beep;
                    ErrMsg(RS_POS_NOT_PRICE);
                    Exit;
                end;

                ItemAddNew(s)
            end;
        end;
    end;

    mDefSoluong := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.ValidBarcode(pma: String; var bo: Boolean): Boolean;
begin
    with spBANLE_CT_Invalid_Mavt do
    begin
        Prepared := True;
        Parameters[1].Value := pma;
        Parameters[2].Value := Now;
        try
            Execute;
            bo := Parameters[3].Value;
        except
        end;
        if DataMain.Conn.Errors.Count > 0 then
        begin
            InvalidAction(DataMain.Conn.Errors[0].Description);
            EdCode.Text := '';
            EdCode.SetFocus;
        end;

        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.OpenQueries;
begin
	CloseDataSets([QrCTBH, QrBH]);
    OpenDataSets([QrBH, QrCTBH]);

    with QrBH do
    begin
	    if IsEmpty then
		    Append
        else
        begin
            with QrCTBH do
            begin
            	curStt := RecordCount;
                Last;
            end;
            Edit;
        end;
    end;

    mDefSoluong := 1;
    GetCustomerName;

    EdCode.Text := '';
    EdCodeExit(EdCode);
    EdCode.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.posPaid;
begin
    with spBANLE_Paid do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrBH.FieldByName('KHOA'));
        try
            Execute;
        except
            if DataMain.Conn.Errors.Count > 0 then
                ErrMsg(DataMain.Conn.Errors[0].Description + #13 + RS_ADMIN_CONTACT);
            Exit;
        end;
    end;
end;

(*==============================================================================
** Last record
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GoLast;
begin
    with QrCTBH do
    begin
        Last;
        curSoluong := FieldByName('SOLUONG').AsFloat;
    end;
end;

(*==============================================================================
	@MAVT		nvarchar(15),

	@GIA		float out,	-- Gia ban
	@TL_CK		float out,	-- Chiet khau
	@VAT		float out
**------------------------------------------------------------------------------
*)
function TFrmMain.GetSalingPrice(mavt: String; mabo: String = ''): Boolean;
var
    _maKho, _maVIP: String;
    _ngay: TDateTime;
begin
    with QrBH do
    begin
        _ngay := FieldByName('Ngay').AsDateTime;
        _maKho := FieldByName('MaKho').AsString;
        _maVIP := FieldByName('MaVip').AsString;
    end;

    try
        with TADOStoredProc.Create(nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            CommandTimeout := 1200;
            ProcedureName := 'spBANLE_CT_Get_DonGia;1';

            Parameters.Refresh;
            Parameters.ParamByName('@MaKho').Value := _maKho;
            Parameters.ParamByName('@Ngay').Value := _ngay;
            Parameters.ParamByName('@MaVT').Value := mavt;
            Parameters.ParamByName('@MaVip').Value := _maVIP;
            Parameters.ParamByName('@MaBo').Value := mabo;
            ExecProc;
            Active := True;
            if Parameters.ParamValues['@RETURN_VALUE'] = 0 then
            begin
                Result := not IsEmpty;
                if Result then
                begin
                    curTenVT  := FieldByName('TenVT').AsString;
                    curMaNoiBo := FieldByName('MaNoiBo').AsString;
                    curDvt  := FieldByName('Dvt').AsString;

                    curGiaban := FieldByName('DonGia').AsFloat;
                    curSoLuongChiTietBo   := FieldByName('SoLuongChiTietBo').AsFloat;
                    curTyLeCKBo   := FieldByName('TyLeCKBo').AsFloat;
                    curTyLeCKVipNhomHang  := FieldByName('TyLeCKVipNhomHang').AsFloat;
                    curTyLeCKPhieu := FieldByName('TyLeCKPhieu').AsFloat;
                    curTyLeCKDaChieu := FieldByName('TyLeCKDaChieu').AsFloat;
                    curLoaiThue  := FieldByName('LoaiThue').AsString;
                    curThueSuat := FieldByName('ThueSuat').AsFloat;


                    if QrDMVT.Locate('MAVT', mavt, []) then
                    begin
                        if QrDMVT.FieldByName('UPDATE_DATE').Value <> FieldByName('UPDATE_DATE').Value  then
                        begin
                            exReSyncRecord(QrDMVT);
                            if mLastUpDate < QrDMVT.FieldByName('UPDATE_DATE').AsDateTime then
                                mLastUpDate := QrDMVT.FieldByName('UPDATE_DATE').AsDateTime;
                        end;
                    end else if curGiaban <> 0 then
                        QrDMVT.Requery;
                end;

            end;
            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GetCustomerName;
var
	mavip: String;
begin
	mavip := QrBH.FieldByName('MaVip').AsString;
    if mavip = '' then
        EdCus.Text := ''
    else
        with DataMain.QrTEMP do
        begin
            SQL.Text := 'select HOTEN from DM_VIP where MAVIP=''' + mavip + '''';
            Open;
            EdCus.Text := FieldByName('HOTEN').AsString;
            Close;
        end;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterPost(DataSet: TDataSet);
begin
	with QrBH do
		lastSct := FieldByName('SCT').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHAfterInsert(DataSet: TDataSet);
begin
    curStt := 0;
	curKhoa := DataMain.GetNewGuid;

	with QrBH do
    begin
		TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        FieldByName('_id').Value := NewIntID;
		FieldByName('Ngay').AsDateTime := Now;
		FieldByName('SCT').AsString := posSct;
        FieldByName('PRINTED').AsBoolean := False;
        FieldByName('PRINT_NO').AsInteger := 0;
        FieldByName('NhanVienThuNgan').AsInteger := sysLogonUID;
		FieldByName('CREATE_BY').AsInteger := sysLogonUID;

        FieldByName('LCT').AsString := 'BLE';
		FieldByName('QUAY').AsString := posQuay;
        FieldByName('MAQUAY').AsString := posMaQuay;
		FieldByName('CA').AsString := posCa;
		FieldByName('MAKHO').AsString := posMakho;
        FieldByName('LOC').AsString := sysLoc;
        FieldByName('DELIVERY').AsBoolean := False;
		posSct := isSmartInc(posSct, 2);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHBeforePost(DataSet: TDataSet);
var
	mDate: TDateTime;
begin
	mDate := Now;
    if (Trunc(mDate) <> Trunc(mLastDate)) or (mLastDate > mDate) then
    begin
		ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
    	Abort;
    end;

    with DataSet do
    begin
    	// Kiểm tra tiền khách trả

//    	if FieldByName('PRINTED').AsBoolean then
//        begin
//            if FieldByName('CHUATHOI').AsFloat < FieldByName('THANHTOAN').AsFloat then
//            begin
//                ErrMsg(RS_POS_INVALID_PAID, 'Thông báo',1);
//
//                if (FieldByName('CHUATHOI').AsFloat = 0.0) then
//				    FieldByName('TTOAN1_1').AsFloat :=
//                        FieldByName('THANHTOAN').AsFloat;
//                Abort;
//            end;
//
//            if (FieldByName('CHUATHOI').AsFloat <> FieldByName('THANHTOAN').AsFloat) then
//            begin
//               ErrMsg(RS_POS_INVALID_PAID, 'Thông báo',1);
//               Abort;
//            end
//        end;

        if FieldByName('NGAY').IsNull then //
		    FieldByName('NGAY').AsDateTime := mDate;
		FieldByName('CREATE_DATE').AsDateTime := mDate;
        mLastDate := mDate;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrDBError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
    // For TEST Hoang phuc Mail: 2012-08-02
    with DataMain.Conn.Errors[0] do
        if (NativeError = 32) and (Pos('CANNOT BE LOCATED', UpperCase(Description)) < 1) then
        begin
            ErrMsg(Description);
            Action := daAbort;
        end
        else
            Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHThanhToanChuyenKhoan_SoTaiKhoanChange(Sender: TField);
begin
    with QrBH do
    begin
        if Sender.AsString = '' then
            FieldByName('ThanhToanChuyenKhoan_LoaiHinh').Clear
        else
            FieldByName('ThanhToanChuyenKhoan_LoaiHinh').AsString := FieldByName('LK_TaiKhoan_LoaiHinh').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN1Change(Sender: TField);
begin
    with QrBH do
    begin
        if not mTrigger then
        begin
            if mNhacnhoTT and (Sender.AsFloat <> 0) then
                if YesNo(RS_NHACNHO_THANHTOAN) then
                begin
                    mTrigger := True;
                    if Sender.FieldName = 'KhachDuaTienMat' then
                        FieldByName('KhachDuaTienMat').AsFloat := Sender.AsFloat * mNhanTT
                    else
                        FieldByName('ThanhToanTheNganHang').AsFloat := Sender.AsFloat * mNhanTT;
                    mTrigger := False;
                end;
        end;

        if Sender.FieldName = 'ThanhToanTheNganHang' then
            if Sender.AsFloat = 0 then
                FieldByName('ThanhToanTheNganHang_SoChuanChi').Clear;

    	FieldByName('TongSoTienNhan').AsFloat :=
	    	FieldByName('KhachDuaTienMat').AsFloat  + FieldByName('ThanhToanTheNganHang').AsFloat +
            FieldByName('ThanhToanTraHang').AsFloat  + FieldByName('ThanhToanPhieuQuaTang').AsFloat +
            FieldByName('ThanhToanViDienTu').AsFloat  + FieldByName('ThanhToanChuyenKhoan').AsFloat +
            FieldByName('ThanhToanDiemTichLuy').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHKhachDuaTienMatChange(Sender: TField);
var
    thanhtoan, tongtiennhan, khachdua, tienmat, tienmat_1, tienmat_2, tongtiennhan_chua2,
    theNganHang, traHang, phieuQuaTang, viDienTu, chuyenKhoan, diemTichLuy,
    coTheThoiLai, khongTheThoiLai, tienThoiLai
    : Double;
    isDelivery: Boolean;
begin
    tienmat_2 := 0;
    with QrBH do
    begin
        thanhtoan := exVNDRound(FieldByName('ThanhToan').AsFloat, 0);
        khachdua := exVNDRound(FieldByName('KhachDuaTienMat').AsFloat, 0);
        theNganHang := exVNDRound(FieldByName('ThanhToanTheNganHang').AsFloat, 0);
        traHang := exVNDRound(FieldByName('ThanhToanTraHang').AsFloat, 0);
        phieuQuaTang := exVNDRound(FieldByName('ThanhToanPhieuQuaTang').AsFloat, 0);
        viDienTu := exVNDRound(FieldByName('ThanhToanViDienTu').AsFloat, 0);
        chuyenKhoan := exVNDRound(FieldByName('ThanhToanChuyenKhoan').AsFloat, 0);
        diemTichLuy := exVNDRound(FieldByName('ThanhToanDiemTichLuy').AsFloat, 0);

        isDelivery := FieldByName('DELIVERY').AsBoolean;

//            if not mTrigger then
//            begin
//                if mNhacnhoTT and (Sender.AsFloat <> 0) then
//                    if YesNo(RS_NHACNHO_THANHTOAN) then
//                    begin
//                        mTrigger := True;
//                        if Sender.FieldName = 'KhachDuaTienMat' then
//                            FieldByName('KhachDuaTienMat').AsFloat := Sender.AsFloat * mNhanTT
//                        else
//                            FieldByName('ThanhToanTheNganHang').AsFloat := Sender.AsFloat * mNhanTT;
//                        mTrigger := False;
//                    end;
//            end;

        if Sender.FieldName = 'ThanhToanTheNganHang' then
            if Sender.AsFloat = 0 then
                FieldByName('ThanhToanTheNganHang_SoChuanChi').Clear;

        coTheThoiLai := khachdua + theNganHang + viDienTu + chuyenKhoan;
        khongTheThoiLai := traHang + phieuQuaTang + diemTichLuy;
        tongtiennhan := coTheThoiLai + khongTheThoiLai;

        if khongTheThoiLai > thanhtoan then
            tienThoiLai := coTheThoiLai
        else
        begin
            if isDelivery then
            begin
                tongtiennhan_chua2 := tongtiennhan;
                if tongtiennhan_chua2 - thanhtoan < 0 then
                begin
                    tienmat_2 := thanhtoan - tongtiennhan_chua2;
                    tongtiennhan := tongtiennhan_chua2 + tienmat_2;
                end;
            end;

            tienThoiLai := tongtiennhan - thanhtoan;
        end;

        if tienmat_2 > 0 then
            tienmat_1 := khachdua
        else
        begin
            tienmat_1 := khachdua - tienThoiLai
        end;

        tienmat := tienmat_1 + tienmat_2;

        FieldByName('TongSoTienNhan').AsFloat := tongtiennhan;
        FieldByName('ThoiLaiTienMat').AsFloat := tienThoiLai;

        FieldByName('ThanhToanTienMat_Lan1').AsFloat := tienmat_1;
        FieldByName('ThanhToanTienMat_Lan2').AsFloat := tienmat_2;
        FieldByName('ThanhToanTienMat').AsFloat := tienmat;
//
//        FieldByName('ThanhToanTienMat').AsFloat :=
//            FieldByName('ThanhToanTienMat_Lan1').AsFloat + FieldByName('ThanhToanTienMat_Lan2').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN3_SCTChange(Sender: TField);
begin
    with QrBH do
    if Sender.AsString = '' then
    begin
        FieldByName('ThanhToanTraHang_Khoa').Clear;
        FieldByName('ThanhToanTraHang').AsFloat := 0;
        FieldByName('ThanhToanTraHang_TriGia').AsFloat := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN3_SCTValidate(Sender: TField);
var
    s: String;
    sotien, thanhToan: Double;
    pKhoa: TGUID;
begin
    s := Sender.AsString;
    if s = '' then
        Exit;
    try
        with spCHECK_BILL_TRAHANG do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@pS1').Value := s;

            ExecProc;
            if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
            begin
                QrBH.FieldByName('ThanhToanTraHang_Khoa').Clear;
                QrBH.FieldByName('ThanhToanTraHang').AsFloat := 0;
                QrBH.FieldByName('ThanhToanTraHang_TriGia').AsFloat := 0;
                s := Parameters[1].Value;
                ErrMsg(s);
                Exit;
            end
            else
            begin
                Active := True;

                pKhoa := TGuidField(FieldByName('KHOA')).AsGuid;
                sotien := FieldByName('ThanhToan').AsFloat;
                TGuidField(QrBH.FieldByName('ThanhToanTraHang_Khoa')).AsGuid := pKhoa;

                thanhToan := QrBH.FieldByName('ThanhToan').AsFloat;

                if (sotien >  thanhToan) then
                    QrBH.FieldByName('ThanhToanTraHang').AsFloat := thanhToan
                else
                    QrBH.FieldByName('ThanhToanTraHang').AsFloat := sotien;

                QrBH.FieldByName('ThanhToanTraHang_TriGia').AsFloat := sotien;
            end;
        end;
    except on E: Exception do
        begin
            ClearWait;
            ErrMsg('For IT: ' + e.Message);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHTTOAN4_INPUTChange(Sender: TField);
begin
    with QrBH do
    if Sender.AsString = '' then
    begin
        FieldByName('ThanhToanPhieuQuaTang_MATHE').Clear;
        FieldByName('ThanhToanPhieuQuaTang').AsFloat := 0;
        FieldByName('ThanhToanPhieuQuaTang_TriGia').AsFloat := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_PQT = 'Phiếu Quà Tặng không hợp lệ.';

procedure TFrmMain.QrBHTTOAN4_INPUTValidate(Sender: TField);
var
	s, ma, mathe: String;
    sotien, thanhToan: Double;
    bm: TBytes;
begin
    ma := Sender.AsString;

    if ma = '' then
        Exit;

    // Lấy thông tin PQT
    try
        with spPHIEUQUATANG_CT_Invalid do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@pS1').Value := ma;

            ExecProc;
            if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
            begin
                //QrBH.FieldByName('ThanhToanPhieuQuaTang_MATHE').Clear;
                QrBH.FieldByName('ThanhToanPhieuQuaTang').AsFloat := 0;
                QrBH.FieldByName('ThanhToanPhieuQuaTang_TriGia').AsFloat := 0;
                s := Parameters[1].Value;
                ErrMsg(s);
                Exit;
            end
            else
            begin
                Active := True;

                mathe := FieldByName('MATHE').AsString;
                sotien := FieldByName('MENHGIA').AsFloat;
                thanhToan := QrBH.FieldByName('ThanhToan').AsFloat;
                //QrBH.FieldByName('ThanhToanPhieuQuaTang_MATHE').AsString := mathe;

                if (sotien > thanhToan) then
                    QrBH.FieldByName('ThanhToanPhieuQuaTang').AsFloat := thanhToan
                else
                    QrBH.FieldByName('ThanhToanPhieuQuaTang').AsFloat := sotien;

                QrBH.FieldByName('ThanhToanPhieuQuaTang_TriGia').AsFloat := sotien;
            end;
        end;
    except on E: Exception do
        begin
            ClearWait;
            ErrMsg('For IT: ' + e.Message);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterDelete(DataSet: TDataSet);
begin
	Total;
    if QrCTBH.IsEmpty then
    begin
        with QrBH do
        begin
            SetEditState(QrBH);
            FieldByName('CN_LIENHE').Clear;
            FieldByName('CN_DIACHI').Clear;
            FieldByName('CN_DTHOAI').Clear;
            FieldByName('TINHTRANG').Clear;
            FieldByName('DELIVERY').Clear;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	with QrCTBH do
    begin
    	Inc(curStt);
    	FieldByName('STT').AsInteger := curStt;
        TGuidField(FieldByName('KHOA')).AsGuid := curKhoa;
        TGuidField(FieldByName('KHOACT')).AsGuid := DataMain.GetNewGuid;
    end;

    // lay ngay gio de tinh chiet khau, khuyen mai.
    if Is1Record(QrCTBH) and (not sysIsCentral) then
    begin
        SetEditState(QrBH);
        QrBH.FieldByName('NGAY').AsDateTime := Now;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHBeforeOpen(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    	Parameters[0].Value := TGuidEx.ToString(curKhoa);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHCN_DIACHI_HDChange(Sender: TField);
var
    s: String;
begin
    with QrBH do
    begin
        s := Sender.AsString;
        if s <> '' then
            if YesNo('Mặc định địa chỉ giao hàng?') then
                FieldByName('CN_DIACHI').AsString := s
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrBHMAKHOChange(Sender: TField);
begin
    with QrBH do
    begin
//        if FieldByName('MAKHO').AsString <> sysDefKho then
//            EdRetailer.Font.Color := clRed
//        else
//            EdRetailer.Font.Color := clPurple;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHAfterPost(DataSet: TDataSet);
var
    sTenvtKhongDau, sDonGia, sThanhtoan: String;
begin
	if mTrigger then
    	Exit;

	Total;

    // Reread DMVT
    with QrCTBH do
    begin
        sTenvtKhongDau := isStripToneMarkUtf8(FieldByName('TenVT').AsString);
        sDonGia := FormatFloat(ctCurFmt, FieldByName('DonGia').AsFloat);
        sThanhtoan := FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat) + ' VND';
    end;

    icdComPort.IcdScan(sTenvtKhongDau, sDonGia, sThanhtoan);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrBH do
    	if State in [dsInsert] then
    	begin
        	Post;
            Edit;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTyLeCKMHChange(Sender: TField);
begin
    with QrCTBH do
		FieldByName('TyLeCK_Max').AsFloat :=
                max(
                    max(FieldByName('TyLeCKMH').AsFloat,
                        max(FieldByName('TyLeCKHD').AsFloat, FieldByName('TyLeCKBo').AsFloat)),
                    max(FieldByName('TyLeCKVip').AsFloat,
                        Max(FieldByName('TyLeCKVipNhomHang').AsFloat, FieldByName('TyLeCKPhieu').AsFloat))
                );
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHTyLeCK_MAXChange(Sender: TField);
begin
    with QrCTBH do
		FieldByName('TyLeCK').AsFloat :=
                    FieldByName('TyLeCK_Max').AsFloat + FieldByName('TyLeCK_Them').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrTAMBeforeOpen(DataSet: TDataSet);
begin
	with QrTAM do
    begin
    	Parameters[0].Value := posQuay;
        Parameters[1].Value := posMakho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeEnter(Sender: TObject);
begin
	EdCode.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeExit(Sender: TObject);
begin
	with EdCode do
	begin
    	if Text <> '' then
        	Exit;
		Tag := 0;
	end;
	ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.InvalidAction;
begin
    Beep;
    if msg = '' then
		ErrMsg(RS_INVALID_ACTION, 'Lỗi nhập liệu', 1)
    else
		ErrMsg(msg, 'Lỗi nhập liệu', 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.IsChangeDMHH: Boolean;
begin
    Result := False;
    with GetUPDATE_DATE do
    begin
        Prepared := True;
        try
            with Execute do
            begin
                Result := mLastUpDate < Fields[0].Value;
                if Result then
                begin
                    mLastUpDate := Fields[0].Value;
                    CmdReLoad.Execute;
                end;
            end;
        except
            DbeMsg;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.IsEnableFunc(const pFunc: String): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := format('select FUNC_CODE from SYS_FUNC where isnull(ENABLED, 0)=1 and FUNC_CODE = %s',
            [Quotedstr(pFunc)]);
        Open;
        Result := not IsEmpty;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanCode;
var
	s: String;
    gio: TDateTime;
    bo: Boolean;
begin
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Quick select
    if IsDotSelect(s) <> 0 then
    begin
        IsChangeDMHH;

    	if not exDotMavt(4, QrDmvt, s) then
        	Exit;
		EdCode.Text := s;
	end;

    gio := Now;
    if (Trunc(gio) <> Trunc(mLastDate)) or (mLastDate > gio) then
    begin
        ErrMsg('Máy bị sai ngày giờ.' + RS_ADMIN_CONTACT);
        Exit;
    end;

    if not ValidBarcode(s, bo) then
        Abort;
    if bo then
    begin
        with QrDMVT_BO do
        begin
            Close;
            Parameters[0].Value := s;
            Open;
            while not eof do
            begin
                if GetSalingPrice(FieldByName('MABH').AsString, s) then
                begin
                    if QrCTBH.Locate('MAVT;MABO',
                        VarArrayOf([FieldByName('MABH').AsString, s]), []) then
                        if mDefSoluong > 0 then
                            ItemSetQty(QrCTBH.FieldByName('SoLuong').AsFloat + mDefSoluong * curSoLuongChiTietBo)
                        else
                            ItemSetQty(QrCTBH.FieldByName('SoLuong').AsFloat + curSoLuongChiTietBo)
                    else
                        ItemAddNew(FieldByName('MABH').AsString, s);
                end;
                Next;
            end;
        end;
    end
    else
    begin
        GoLast;
        with QrCTBH do
        begin
            // Mã cũ
            if Locate('MAVT;MABO', VarArrayOf([s, Null]), []) then
            begin
                GetSalingPrice(s);

                // Resync GIABAN
//				if FieldByName('DONGIA').AsFloat <> curGiaban then
//                begin
//                    QrRefDMVT.Locate('MAVT', s, []);
//                    ReSyncRecord(QrRefDMVT);
//                end;
                // End: resync

                if mDefSoluong > 0 then
                    ItemSetQty(FieldByName('SoLuong').AsFloat + mDefSoluong)
                else
                    ItemSetQty(FieldByName('SoLuong').AsFloat + 1)
            end
            // Mã mới
            else
            begin
                // Sai ma
                if not GetSalingPrice(s) then
                begin
                    InvalidAction(RS_POS_CODE_FAIL);
                    EdCode.Text := '';
                    EdCode.SetFocus;
                    Exit;
                end;

                // Chua co gia ban
                if curGiaban = 0 then
                begin
                    Beep;
                    ErrMsg(RS_POS_NOT_PRICE);
                    InvalidAction(RS_POS_CODE_FAIL);
                    EdCode.Text := '';
                    EdCode.SetFocus;
                    Exit;
                end;

                ItemAddNew(s);
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanQty;
var
	b: Boolean;
    s: String;
begin
    s := Trim(EdCode.Text);
    b := Length(s) > 10;
    if not b then
        try
            mDefSoluong := StrToFloat(s);
        except
            mDefSoluong := 1;
            b := True;
        end;

    if b then
        ErrMsg(RS_POS_ERROR_QTY);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanVIP;
var
	ma, ten: String;
    ck: Double;
    bm: TBytes;
begin
    with QrBH do
        ma := FieldByName('MaVip').AsString;

    Application.CreateForm(TFrmPosVIP, FrmPosVIP);
    ten := FrmPosVIP.Execute(ma, 0);

//	ma := Trim(EdCode.Text);
//
    // Xóa mã VIP
    if ma = '' then
    begin
	    SetEditState(QrBH);
    	with QrBH do
	    begin
    	    FieldByName('MaVip').Clear;
	        FieldByName('TyLeCKVip').Clear;
            EdCus.Text := '';
            Total;
	    end;
        Exit;
    end;

    if ten = '' then
        Exit;

    // Lấy thông tin VIP
    with TADOStoredProc.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        CommandTimeout := 1200;
        ProcedureName := 'spMA_VIP_Invalid;1';

        Parameters.Refresh;
        Parameters.ParamByName('@pMaVip').Value := ma;

        ExecProc;
        Active := True;
        if Parameters.ParamValues['@RETURN_VALUE'] = 0 then
        begin
            mVipTen := FieldByName('HoTen').AsString;
            ck := FieldByName('TyLeCkVip').AsFloat;
        end
        else
        begin
            ErrMsg(RS_INVALID_VIP);
            Exit;
        end;

        Free;
	end;

//     EdCus.Text := ten +' - '+ ma;
    EdCus.Text := mVipTen + ' - ' + ma;
    SetEditState(QrBH);
    with QrBH do
    begin
        FieldByName('MaVip').AsString := ma;
        FieldByName('TyLeCKVip').AsFloat := ck;
    end;
    with QrCTBH do
    begin
        DisableControls;
        bm := Bookmark;
        First;
        while not eof do
        begin
            GetSalingPrice(FieldByName('MAVT').AsString);
            SetEditState(QrCTBH);
            FieldByName('TyLeCKVipNhomHang').AsFloat := curTyLeCKVipNhomHang;
            Next;
        end;
        Bookmark := bm;
        EnableControls;
    end;
    Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ProcScanRetQty;
var
	s: String;
    soluong: Double;
begin
    s := Trim(EdCode.Text);
    if s = '' then
        Exit;

    // Lấy số lượng trả
    try
        soluong := StrToFloat(s);
    except
        soluong := -1;
    end;

	with QrCTBH do
    begin
        // Kiểm tra số lượng
		if (soluong <= 0) or (soluong > FieldByName('SoLuong').AsFloat) then
        begin
            InvalidAction('Số lượng không hợp lệ.');
			EdCode.SetFocus;
            Exit;
        end;

		if soluong = FieldByName('SoLuong').AsFloat then
        	Delete
        else
    	begin
	        Edit;
    	    FieldByName('SoLuong').AsFloat := FieldByName('SoLuong').AsFloat - soluong;
            Post;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.EdCodeKeyPress(Sender: TObject; var Key: Char);
begin
	// Is CRLF keystroke
	if Key <> #13 then
    	Exit;

    Key := #0;

	case EdCode.Tag of
    // Mã hàng
    0:
    	ProcScanCode;

	// Số lượng
    1:
    	ProcScanQty;

	// VIP
    2:
    	ProcScanVIP;

	// Trả hàng
    3:
    	ProcScanRetQty;
    end;

    if EdCode.Tag <> 1 then
		mDefSoluong := 1;

    with EdCode do
    begin
    	Tag := 0;
	    Text := '';
		SelectAll;
    end;
    ScanType;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMain.exThelenh: Boolean;
begin
    Application.CreateForm(TFrmTheLenh, FrmTheLenh);
    Result := FrmTheLenh.Execute(0, posMakho);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListDrawFooterCell(Sender: TObject; Canvas: TCanvas;
  FooterCellRect: TRect; Field: TField; FooterText: String;
  var DefaultDrawing: Boolean);
begin
	with Canvas.Font do
    begin
    	Size := 10;
		Style := [fsBold];

        if (Field.FullName = 'MAVT') or (Field.FullName = 'TENVT') then
			Color := $00804000
        else
			Color := $002222B2;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListUpdateFooter(Sender: TObject);
begin
	with GrList, QrBH do
    begin
    	ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(sysQtyFmt, FieldByName('SoLuong').AsFloat);
    	ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('SoTienCK').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SoTienCK').AsFloat);
    	ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('ThanhToan').AsFloat);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.GrListKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    begin
    	Key := #0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmMain.ItemAddNew(const ma: String; const pMabo: String);
begin
    with QrCTBH do
    begin
	    Append;
        FieldByName('MAVT').AsString := ma;
        FieldByName('TenVT').AsString := curTenVT;
        FieldByName('MaNoiBo').AsString := curMaNoiBo;
        FieldByName('Dvt').AsString := curDvt;
        FieldByName('DonGia').AsFloat := curGiaban;
        FieldByName('LoaiThue').AsString := curLoaiThue;
        FieldByName('ThueSuat').AsFloat := curThueSuat;

        FieldByName('TyLeCKVipNhomHang').AsFloat := curTyLeCKVipNhomHang;
        FieldByName('TyLeCKPhieu').AsFloat := curTyLeCKPhieu;

        if pMabo <> '' then
        begin
            FieldByName('MABO').AsString := pMabo;
            FieldByName('TyLeCKBo').AsFloat := curTyLeCKBo;
            FieldByName('SoLuong').AsFloat := mDefSoluong * curSoLuongChiTietBo;
        end
        else
            FieldByName('SoLuong').AsFloat := mDefSoluong;

        Post;
    end;

    mDefSoluong := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemSetQty(const pSoluong: Double);
begin
    with QrCTBH do
    begin
        if State in [dsBrowse] then
            Edit;
//		FieldByName('DonGia').AsFloat := curGiaban;
//
//        FieldByName('LoaiThue').AsString := curLoaiThue;
//        FieldByName('ThueSuat').AsFloat := curThueSuat;
//
//        FieldByName('TyLeCKVipNhomHang').AsFloat := curTyLeCKVipNhomHang;
//        FieldByName('TyLeCKPhieu').AsFloat := curTyLeCKPhieu;
//
//        // CK mặt hàng thuộc bộ!
//        FieldByName('TyLeCKBo').AsFloat := curTyLeCKBo;

		FieldByName('SoLuong').AsFloat := pSoluong;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHSoLuongChange(Sender: TField);
var
	dongia, soluong, thanhtien: Double;
begin
	with QrCTBH do
    begin
    	dongia  := FieldByName('DonGia').AsFloat;
    	soluong  := FieldByName('SoLuong').AsFloat;
        thanhtien := exVNDRound(dongia * soluong, sysCurRound);

		FieldByName('ThanhTien').AsFloat := thanhtien;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHThanhTienChange(Sender: TField);
var
    dongia, dongiaCK, thanhtien, ck, tlck, thanhtienSauCK: Double;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCTBH do
    begin
        dongia := FieldByName('DonGia').AsFloat;
        dongiaCK := FieldByName('DonGiaCK').AsFloat;
        tlck := FieldByName('TyLeCK').AsFloat;
        Ck := FieldByName('SoTienCK').AsFloat;
        thanhtien := FieldByName('ThanhTien').AsFloat;

        if Sender.FieldName = 'DonGiaCK' then
        begin
            tlck := (1 - SafeDiv(dongiaCK, dongia))*100;
            Ck := exVNDRound(thanhtien * tlck / 100.0, sysCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCK').AsFloat := tlck;
            FieldByName('SoTienCK').AsFloat := ck;
            mTrigger := pTrigger;
        end
        else if Sender.FieldName = 'SoTienCK' then
        begin
            tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, thanhtien)));
            dongiaCK := dongia * (1 - tlck/100);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCK').AsFloat := tlck;
            FieldByName('DonGiaCK').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end else
        begin
            dongiaCK := dongia * (1 - tlck/100);
            Ck := exVNDRound(thanhtien * tlck / 100.0, sysCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('SoTienCK').AsFloat := ck;
            FieldByName('DonGiaCK').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end;

        thanhtienSauCK := thanhtien - ck;
        FieldByName('ThanhTienSauCK').AsFloat := thanhtienSauCK;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHThanhTienSauCKChange(Sender: TField);
begin
    with QrCTBH do
    begin
        FieldByName('DonGiaTinhThue').AsFloat := FieldByName('DonGiaCK').AsFloat;
        FieldByName('ThanhToanTinhThue').AsFloat := FieldByName('ThanhTienSauCK').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHThanhToanChange(Sender: TField);
begin
    Total;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.QrCTBHThanhToanTinhThueChange(Sender: TField);
var
    mHinhthuc: String;
    donGiaTinhThue, dgChuaVAT, ts, sl,
    thanhToanTinhThue, thanhtoanChuaVATcl, cl, thanhtoanChuaVAT, thue, thanhtoan : Double;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    mHinhthuc := sysGiaCoVAT;

    with QrCTBH do
    begin
        donGiaTinhThue := FieldByName('DonGiaTinhThue').AsFloat;
        ts := FieldByName('ThueSuat').AsFloat;
        sl := FieldByName('SoLuong').AsFloat;
        thanhToanTinhThue := FieldByName('ThanhToanTinhThue').AsFloat;
        cl := FieldByName('SoTienCL').AsFloat;

        if (mHinhthuc = sysGiaCoVAT) then
        begin
            thanhtoan := thanhToanTinhThue;

            thanhtoanChuaVATcl := exVNDRound((donGiaTinhThue /(1 + ts/100)) * sl, -2);
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl + cl, sysCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);
        end
        else
        begin
            thanhtoanChuaVATcl := thanhToanTinhThue;
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl, sysCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);

            thanhtoan := exVNDRound(thanhtoanChuaVATcl + thue + cl, sysCurRound);
        end;

        pTrigger := mTrigger;
        mTrigger := True;
        FieldByName('DonGiaChuaThue').AsFloat := dgChuaVAT;
        FieldByName('ThanhToanChuaCL').AsFloat := thanhtoanChuaVATcl ;
        FieldByName('SoTienCL').AsFloat := cl;
        FieldByName('ThanhToanChuaThue').AsFloat := thanhtoanChuaVAT;
        FieldByName('SoTienThueChuaRound').AsFloat := thue;

        if Ts = 5 then
        begin
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_5').AsFloat := Thue;
        end
        else if Ts = 10 then
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := Thue;
        end else
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := Thue;
        end;

        FieldByName('SoTienThue').AsFloat := Thue;
        FieldByName('ThanhToan').AsFloat := thanhtoan;
        mTrigger := pTrigger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFaqExecute(Sender: TObject);
begin
	Faqs
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetPrinterExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPrinter, FrmPrinter);
    if FrmPrinter.Execute then
    begin
    	exBillFinal(mPrinter);
		exBillInitial(mPrinter);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTrahangExecute(Sender: TObject);
var
    mUser: Integer;
    sRole: string;
    r: WORD;
begin
    sRole := 'POS_NHAPTRA';

    if GetRights(sRole, False) <> R_DENY then
    	mUser := sysLogonUID
    else
    begin
		// Logon
        if not DataMain.isLogon.Logon3(mUser) then
            Exit;

        if GetRights(mUser, sRole) = R_DENY then
        begin
            DenyMsg;
            Exit;
        end;
    end;

    Application.CreateForm(TFrmNhaptraBanle, FrmNhaptraBanle);
    FrmNhaptraBanle.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPhieuquatangExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChitietExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmPosChitiet, FrmPosChitiet);
    FrmPosChitiet.Execute(QrCTBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	Handled := (MyActionList.Tag = 0) and Self.Active;
//    if Handled then
//    	TestDbConnection; //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSave2Execute(Sender: TObject);
var
	b: Boolean;
    k: TGUID;
    n: Integer;
    s: String;
begin
	if QrCTBH.IsEmpty then		// Restore saved receipt
    begin
        with QrBH do //Truong hop xoa chi tiet phieu o luu tam
            if (State in [dsEdit]) and (FieldByName('ThanhToan').OldValue <> FieldByName('ThanhToan').Value) then
                CheckBrowseMode;

        // Exist temp. receipt?
        with QrTAM do
        begin
            Close;

            SQL.Text := mSqlTam;

            Open;
            if IsEmpty then
                Exit
            else if RecordCount = 1 then
                curKhoa := TGuidField(FieldByName('KHOA')).AsGuid
            else
            begin
                Application.CreateForm(TFrmTempReceipt, FrmTempReceipt);
                curKhoa := FrmTempReceipt.Execute(posMakho, posQuay);
            end;
        end;
    end
    (*
    ** Temporarily save receipt
    *)
    else
    begin
	    // Temporarily save
        s := QrBH.FieldByName('GhiChu').AsString;
        if s = '' then
        begin
            Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
            FrmReceiptDesc.Execute(s)
        end;

        SaveBill(2, s);

        // New receipt
        curKhoa := TGuidEx.EmptyGuid;
    end;
    posSct := AllocRetailBillNumber(posMaQuay, Date);
	OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGiaohangExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmBanleCN, FrmBanleCN);
    FrmBanleCN.Execute(R_FULL, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
	s: String;
begin
	if mDefSoluong > 1 then
		s := FloatToStr(mDefSoluong)
	else
		s := '';

    if LbCurSoluong.Caption <> s then
    begin
	    LbCurSoluong.Caption := s;
        if s = '' then
	        HtmlCurSoluong.HTMLText.Text := ''
        else
            HtmlCurSoluong.HTMLText.Text := Format(
                '<P align="left"><FONT face="Tahoma" size="13" color="#004080"><b>(SL)</b>  ' +
                '<FONT size="18"><b>%s</b></FONT></FONT></P>',
                [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.Cleanup;
begin
    try
        with TADOCommand.Create(nil) do
        begin
            Connection := DataMain.Conn;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spBANLE_Clean;1';

            Parameters.Refresh;
            Parameters.ParamByName('@MaQuay').Value := posMaQuay;
            Execute;

            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ScanType;
begin
	case EdCode.Tag of
    0:
        begin
            LbCode.Caption := 'NHẬP MÃ HÀNG';
            LbCode.Font.Color := RGB(0, 64, 128);
            EdCode.Font.Color := clGray;
        end;
    1:
        begin
            LbCode.Caption := 'NHẬP SỐ LƯỢNG';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    2:
        begin
            LbCode.Caption := 'NHẬP THẺ VIP';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    3:
        begin
            LbCode.Caption := 'NHẬP SỐ LƯỢNG TRẢ';
            LbCode.Font.Color := clPurple;
            EdCode.Font.Color := clTeal;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.tbAddUnit;
var
  sStr: TStream;
  bBlob: TBlobField;
  x: Integer;
begin
    TtVattu.Tiles.Clear;
    with QrDMVT_CAMUNG do
    begin
        x := 1;
        First;

        while (not Eof) and (x < 30) do
        begin
            with TtVattu.Tiles.Add do
            begin
                // Dai dien
//                if bImage then
//                begin
//                    bBlob := FieldByName('HINH') as TBlobField;
//                    sStr := CreateBlobStream(bBlob, bmRead);
//                    try
//                        sStr.Position := 0;
//                        if bBlob.IsNull then
//                            Content.Image := PicBan.Items[0].Picture
//                        else
//                            Content.Image.LoadFromStream(sStr);
//                    finally
//                        sStr.Free;
//                    end;
//                end;

                Data := FieldByName('MAVT').AsString;
                Content.Text := FieldByName('TENVT_CAMUNG').AsString;
                Content.TextPosition := tpTopCenter;
                //Content.TextTop := mTop;

                StatusIndicator := FloatToStrF(FieldByName('GiaBan').AsFloat, ffNumber, 10, 0);

                StatusIndicatorLeft :=
                    -5 - (Length(FloatToStrF(FieldByName('GiaBan').AsFloat, ffNumber, 10, 0))*6 div 2);
                StatusIndicatorTop := 38;

                // Chi tiet1
                ContentMaximized.Text :=
                    'Mã:       ' + FieldByName('MAVT').AsString + #13 +
                    'Giá bán:  ' + FloatToStrF(FieldByName('GiaBan').AsFloat, ffNumber, 10, 0)+#13 +
                    'Tên:  ' + FieldByName('TENVT_CAMUNG').AsString + #13;
                Inc(x);
            end;

            Next;
        end;
    end;

    TtVattu.FirstPage;
end;

initialization
end.
