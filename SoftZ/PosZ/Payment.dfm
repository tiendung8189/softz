object FrmPayment: TFrmPayment
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Thanh To'#225'n'
  ClientHeight = 547
  ClientWidth = 928
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Padding.Left = 8
  Padding.Top = 8
  Padding.Right = 8
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    928
    547)
  PixelsPerInch = 96
  TextHeight = 16
  object Panel5: TPanel
    Left = 8
    Top = 8
    Width = 915
    Height = 533
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 500
      Height = 468
      Align = alLeft
      TabOrder = 0
      object PD1: TisPanel
        Left = 1
        Top = 1
        Width = 498
        Height = 56
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = '  .: Ti'#7873'n m'#7863't - Cash'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          56)
        object HTMLabel4: THTMLabel
          Left = 146
          Top = 20
          Width = 95
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>Thanh to'#225'n l'#7847'n 1' +
              '</B><br/> First Pay</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Visible = False
          Version = '2.2.1.2'
        end
        object HTMLabel5: THTMLabel
          Left = 125
          Top = 55
          Width = 95
          Height = 29
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>Thanh to'#225'n l'#7847'n 2' +
              '</B><br/> Second Pay</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Visible = False
          Version = '2.2.1.2'
        end
        object EdSotien1: TwwDBEdit
          Left = 247
          Top = 15
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clWhite
          DataField = 'KhachDuaTienMat'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit4: TwwDBEdit
          Left = 247
          Top = 52
          Width = 229
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'ThanhToanTienMat_Lan2'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          Visible = False
          WantReturns = False
          WordWrap = False
        end
      end
      object PD2: TisPanel
        Left = 1
        Top = 112
        Width = 498
        Height = 67
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = '  .: Th'#7867' ng'#226'n h'#224'ng - Bank card'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          67)
        object HTMLabel1: THTMLabel
          Left = 41
          Top = 20
          Width = 161
          Height = 19
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' chu'#7849'n chi</B>' +
              ' (Auth. Code)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdChuanchi: TwwDBEdit
          Left = 41
          Top = 35
          Width = 200
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'ThanhToanTheNganHang_SoChuanChi'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSotien2: TwwDBEdit
          Left = 247
          Top = 26
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'ThanhToanTheNganHang'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD3: TisPanel
        Left = 1
        Top = 234
        Width = 498
        Height = 67
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 4
        HeaderCaption = '  .: Phi'#7871'u nh'#7853'p tr'#7843' b'#225'n l'#7867' - Pos return'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          67)
        object HTMLabel2: THTMLabel
          Left = 41
          Top = 20
          Width = 179
          Height = 19
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>S'#7889' phi'#7871'u/M'#227' v'#7841'ch' +
              '</B> (Number/ID)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdTTOAN3_MA: TwwDBEdit
          Left = 41
          Top = 35
          Width = 200
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'ThanhToanTraHang_SCT'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit2: TwwDBEdit
          Left = 247
          Top = 26
          Width = 229
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'ThanhToanTraHang'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD4: TisPanel
        Left = 1
        Top = 301
        Width = 498
        Height = 67
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 5
        HeaderCaption = '  .: Phi'#7871'u qu'#224' t'#7863'ng - Gift voucher'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          67)
        object HTMLabel6: THTMLabel
          Left = 41
          Top = 20
          Width = 161
          Height = 19
          Anchors = [akTop, akRight]
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="8" color="clGreen"><B>M'#227' phi'#7871'u</B> (Co' +
              'de)</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object wwDBEdit5: TwwDBEdit
          Left = 41
          Top = 35
          Width = 200
          Height = 24
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CharCase = ecUpperCase
          DataField = 'ThanhToanPhieuQuaTang_MaThe'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit6: TwwDBEdit
          Left = 247
          Top = 26
          Width = 229
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          Color = clBtnFace
          DataField = 'ThanhToanPhieuQuaTang'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object PD6: TisPanel
        Left = 1
        Top = 57
        Width = 498
        Height = 55
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = '  .: Chuy'#7875'n kho'#7843'n - Transfer'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          55)
        object EdSotien5: TwwDBEdit
          Left = 247
          Top = 15
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'ThanhToanChuyenKhoan'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdChuyenKhoan: TwwDBLookupCombo
          Left = 41
          Top = 21
          Width = 200
          Height = 27
          TabStop = False
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MATK'#9'15'#9'S'#7889' t'#224'i kho'#7843'n'#9'F'
            'TENTK'#9'25'#9'T'#234'n t'#224'i kho'#7843'n'#9'F')
          DataField = 'ThanhToanChuyenKhoan_SoTaiKhoan'
          DataSource = FrmMain.DsBH
          LookupTable = DataMain.QrBANLE_TAIKHOAN
          LookupField = 'MATK'
          Style = csDropDownList
          Color = clBtnFace
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = False
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object PD5: TisPanel
        Left = 1
        Top = 179
        Width = 498
        Height = 55
        Align = alTop
        Anchors = [akLeft, akBottom]
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 3
        HeaderCaption = '  .: V'#237' '#273'i'#7879'n t'#7917' - E.Wallet'
        HeaderColor = cl3DLight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = 9109504
        HeaderFont.Height = -12
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          498
          55)
        object wwDBEdit1: TwwDBEdit
          Left = 247
          Top = 15
          Width = 229
          Height = 34
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          DataField = 'ThanhToanViDienTu'
          DataSource = FrmMain.DsBH
          Font.Charset = ANSI_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentFont = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBLookupCombo1: TwwDBLookupCombo
          Left = 41
          Top = 21
          Width = 200
          Height = 27
          TabStop = False
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'15'#9'TEN_HOTRO'#9'F')
          DataField = 'ThanhToanViDienTu_Loai'
          DataSource = FrmMain.DsBH
          LookupTable = DataMain.QrLOAI_VIDIENTU
          LookupField = 'MA_HOTRO'
          Style = csDropDownList
          Color = clBtnFace
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = False
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 468
      Width = 915
      Height = 65
      Align = alBottom
      TabOrder = 1
      DesignSize = (
        915
        65)
      object BtClose: TRzBitBtn
        Left = 777
        Top = 7
        Width = 106
        Height = 50
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdClose
        Anchors = [akRight, akBottom]
        Caption = 'Tho'#225't'#13'Esc'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        Glyph.Data = {
          C2040000424DC204000000000000420000002800000018000000180000000100
          10000300000080040000202E0000202E00000000000000000000007C0000E003
          00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          BD77344FDB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF556861AF75FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F92468002F34FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBF7FE821E106D34FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F196744164517D557FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32A516251BF967
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7F71462516E71E4A2FFE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1A6B071EEA2AA416
          924FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FBF7F4B2EA822681EE926FE7BFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F4C32661E6A2A
          6412D65BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9A6B565B
          FE7BFF7FFF7FFF7FDF7F2B2A261A4A2A25162C33FF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FDE7BF24A45167557FF7FFF7FFF7FFF7F3863C51549264A2A
          261EC822FD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FBB6F14536B2EC0010E3BFF7F
          FF7FFF7FFF7FDE7B2B2AC41149264926271E661EDB6FFF7F7A6BBA6BFC737863
          F24A6C32061AE411E4156722FD77FF7FFF7FFF7F79672A2AE411492228222822
          E619651AFB6FFF7F1863A71D82090516E515E515061A2922282248268B2E8D36
          8C324A2AE619E5152822082208222926C5158722FE7BFF7FFF7F3967EB2D2105
          271E4A2A2822282228220822C515C411C415E515E61D2826082208220926E721
          A1095353FF7FFF7FFF7FFF7FFF7F94522105C515292607220822082207220722
          0722082207260722E721E721A5156001AD36FF7FFF7FFF7FFF7FFF7FFF7FFF7F
          D65A230DC4152826C61D830D620D620D83118311830D82096209630DE821F452
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75A0001A4118B32B24A4E362C32
          2B2E2B2E2B2E2C326F3EF452BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7F9452A000CB2EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8925A40DFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F5B6FC929BB6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F9B6FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F}
        Margin = 1
        Spacing = 1
      end
      object BtReturn: TRzBitBtn
        Left = 667
        Top = 6
        Width = 106
        Height = 50
        Cursor = 1
        Hint = 'L'#432'u v'#224' in bill'
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdReturn
        Anchors = [akRight, akBottom]
        Caption = 'In'#13'F4'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        Glyph.Data = {
          C2040000424DC204000000000000420000002800000018000000180000000100
          10000300000080040000202E0000202E00000000000000000000007C0000E003
          00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F5B67
          54423967DD7BFE7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FBE6F583AB225B21DF3295432553EB74E19639C6FBD7B
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBE7718327219B842
          1C53BA46793A5832F525D321F329F22D54467C6BFF7FFF7FFF7F9C77984AB952
          3A6B9B77FD7FDE7BDC46FB297C6BFF7FBE77BE739D6F9D6B5D633C5BDB4EDA4A
          352E1A57FF7FFF7FFF7F554A1405D915D81DF6251632BB4EBF46DF4AFF7BFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7F7C6BFB52FF7BFF7FFF7F9B775315F9217F53
          1F3BFF32BD2A5C3ADD529E6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE775B32
          BD46FF7FFF7FFE7F754A7705DB4AFF7FFF7F7B671536BB46BF6FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FBE739C46DE4E9F6FFF7FFF7F7A7395157C265B67FF7F
          FF7F35468E083632B93E1C4B5E579F63DF6BFF77FF7BFF7FBD6FB9465D2EBB42
          FD7FFF7FFF7FF85EFA113E53396BFF77FF7F594278119021E8140A150C114F11
          931DF725592E9B3A783A3B5F7F5F5A369C73FF7FFF7FFA5ABC32BD6FFA5AFF6F
          FF7F7B42390D3D2E5C321932F631F32DB0294D210C198A00F72DFF7FFF7FFA52
          5C67FF7FFF7FFA5A5D579C731B57BF67FF777C3E3C11DF21DF1D1F1E1F263F2E
          7F325E325B32B721DA25DF7BFF7FDB4A7C6BFF7FFF7F3A67BD7B3C673E539F5B
          BF67DC2D7E111F263F2E5F321F2ADF1DBF1DDF211F2A9F19DC2DFF7FDF73BB4A
          9D73FF7FFF7F5A6B9C7B1D533F43BF5F9B3E180D1C2A7E3ABE46BE4ABE465E32
          DE21DD25DD21D9009B4AFF7F5E5BDB52FF7FFF7FFF7F7B731B5F1F3F5F437F57
          97255615192E593A7A429A469B425A3ADA25B91D16093511BE67DF67BD425C67
          FF7FFF7FFF7FFF7F3B631C533D4FF9253C3ADE4E7C3E5A3A393238363732F62D
          B525F10CD0083C53DF5FDD421B5BFF7FFF7FFF7FFF7FFF7FFF7FDE7FBD77994A
          F62D18323A3A3A367C3E7C427C3E5B36F9297419DB429F4FDD3A3B5BFE7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F5642D829182EDA4EF95EB525192E9B42
          BD52DB529E631C535C67FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDC7B
          373EBF46BF4AFD7FB6523C327C3E9D6FFF7FFF7FDE7FDE7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7F5A6B9321392E9B42954AF7317E36BC4AFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
          7746F42D15329421D625D6293C63FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE779D737E6F5C639D73FF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
          FF7FFF7FFF7F}
        Margin = 1
        Spacing = 1
      end
      object BtnDelivery: TRzBitBtn
        Left = 555
        Top = 7
        Width = 106
        Height = 50
        Cursor = 1
        FrameColor = 7617536
        ShowFocusRect = False
        Action = CmdDelivery
        Anchors = [akRight, akBottom]
        Caption = 'Giao h'#224'ng'#13'F3'
        Color = 15791348
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrack = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000008080817383838AB494949F1414141D40D0D0D2800000000000000000000
          00000000000000000000000000000000000000000000000000000C0C0C233D3D
          3DBB494949F33D3D3DC707070716000000000000000000000000000000000000
          0000424242C0575757FF575757FF4F4F4FFF474747EA00000000000000000000
          00000000000000000000000000000000000000000000020202064B4B4BDB5757
          57FF565656FF4E4E4EFF424242D4000000000000000000000000000000000606
          0611575757FF5A5A5AFF6C6C6CFF5A5A5AFF4D4D4DFF1C181235332917403329
          174033291740332917403329174033291740332917403A322465575757FF5C5C
          5CFF6B6B6BFF575757FF4D4C4CFD000000000000000000000000000000000101
          0104535353F3585858FF606060FF585858FF5E5A52F7BD9855EDCCA45CFFCCA4
          5CFFCCA45CFFCCA45CFFCCA45CFFCCA45CFFCCA45CFFC19D5CFF585857FF5858
          58FF606060FF575757FF615D54FF000000000000000000000000000000006E58
          31896E5C3CA9595856FC575757FF5E5B57FCA1834ED7CCA45CFF765F3594392D
          194C8E6926E7966C21F4916921EECCA45CFFCCA45CFFCCA45CFF927E5AFF5757
          57FF575757FF5B5A57FFAB8E5BFF040302050000000000000000000000009276
          42B7CCA45CFFCCA45CFFCCA45CFFCCA45CFFCCA45CFF947743B9010100011A14
          09359B6503FF9D6500FF9C6400FFB28E4BEACCA45CFFCCA45CFFCCA45CFFB796
          5BFFA68B5AFFBF9C5BFFCCA45CFF6F59338C120E071600000000000000000101
          00014D3E2360AA894CD4B6914EEAB6904FEAA08046CA130F0918000000000000
          00000E0B051B4F483BCF60594CFFB59252E4CCA45CFFCCA45CFFCCA45CFFCCA4
          5CFFCCA45CFFCCA45CFFCCA45CFFBC9754FFA7864BEC00000000000000000000
          000000000000130F0819BE8C34FFB58631FFA27930E800000000000000000000
          000000000000424242C0575757FF8F6D31C19F7833D09F7833D09F7833D09F78
          33D09F7833D0A07833D2A27930E9A57D36F69E7834E900000000000000000000
          00000000000000000000A87E36DCBF8D33FFBC913FF502010102000000000000
          000000000000404040BC575757FF665538C78A692FD4826632DC8B692CCD9079
          21D9937C21E69B7A26F39D7928F69C7928F56751199B00000000000000000000
          0000000000000000000054401D6DC49543FFCBA45BFF3229173F000000000000
          00000000000024242469575757FF575757FF575757FF535353FF4A4C47F76699
          00FF669900FF699D00FF72AB00FF77B200FF639400D300000000000000000000
          0000000000000000000000000000AE8C4EDACCA45CFF77603695000000000000
          000000000000000000000C0C0C232C2C2C82575757FF575757FF50583FEF6699
          00FF83AD2DFFC0DB87FFC4DE8EFF91C12FFF699D00E000000000000000000000
          000000000000000000000000000079613697CCA45CFFB99553E7020101020000
          000000000000000000000000000012100F266E5B49E0715A3FE56A612FE1689C
          00FF8DBF2AFFD5E8B1FFCEE4A3FF7DB60AFF699D00E000000000000000000000
          0000000000000000000014100A19B79353E5CCA45CFFAC99A8FFA18A8CF59A63
          00E09A6300E09A6300E0905D00D25D40118C9D6500FF966100FF875B04FE72A9
          00FF78B300FF90C02CFF82B814FF78B300FF699D00E000000000000000000000
          00000000000000000000B09E80C5DFC496FFCCA45CFFB59885FFA48878F08455
          00C3845500C3855600C5A56A00F1AF7100FFA66B00FF9D6500FF8D6C0EFC76B0
          00FF78B300FF78B300FF78B300FF78B300FF699D00E000000000000000000000
          00000000000000000000B6A587CCEFE2CBFFD5B67BFFCCA45CFFC9A46BFF9D66
          00FF9D6500FF9D6500FF9D6600FFAA7008FFAF7100FFA66B00FF8F7714FB6CA1
          00FF6CA100FF6CA100FF6CA100FF6CA100FF6BA000FA00000000000000000000
          000000000000000000002B261D31D1C0A1E7D3BB91F0B39051E09E804CC6130C
          001F130C001F130C001F130C001F1E150630A87111FCAF7100FF886739DB496E
          00A7547E00C0547E00C0547E00C0547E00C03853007E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000016171E229897DFED9F8376FF362E31550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000292B40419DBC78FCA1B1C0FF97869BFF7F560DD00504
          00082C2A252FB3A998BFB3A998BF867F728F0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000062639899A2A4F6FF8EA28CFF9F731CFF956000FF412A
          0067000000003B38323F7771657F7771657F3B38323F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000021150030422B0060715A48AD8E7876E4987757EFAF7202FFA36800FF462D
          0070000000001D1B191F3B38323F3B38323F1D1B191F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000211500308E5C00CFAF7100FFAF7100FFAF7100FFBC892BFFB07507FF3C27
          0060000000000000000000000000B3A998BFB3A998BF2C2A252F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000002F1E00448F5D00D19A6300E09A6300E0996300DF704800A30100
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Margin = 1
        Spacing = 1
      end
    end
    object Panel3: TPanel
      Left = 500
      Top = 0
      Width = 415
      Height = 468
      Align = alClient
      TabOrder = 2
      object Panel4: TPanel
        Left = 76
        Top = 163
        Width = 280
        Height = 287
        Align = alCustom
        BevelOuter = bvNone
        TabOrder = 0
        object AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard
          Left = 0
          Top = 0
          Width = 280
          Height = 287
          AutoCompletion.Font.Charset = DEFAULT_CHARSET
          AutoCompletion.Font.Color = clWhite
          AutoCompletion.Font.Height = -19
          AutoCompletion.Font.Name = 'Tahoma'
          AutoCompletion.Font.Style = []
          AutoCompletion.Color = clBlack
          Fill.ColorMirror = clNone
          Fill.ColorMirrorTo = clNone
          Fill.GradientType = gtVertical
          Fill.GradientMirrorType = gtSolid
          Fill.BorderColor = clNone
          Fill.Rounding = 0
          Fill.ShadowOffset = 0
          Fill.Glow = gmNone
          KeyboardType = ktNUMERIC
          Keys = <
            item
              Caption = 'Back Space'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skBackSpace
              Color = 10526880
              X = 2
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '/'
              KeyValue = 111
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skDivide
              X = 71
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '*'
              KeyValue = 106
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skMultiply
              X = 140
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '-'
              KeyValue = 109
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skSubstract
              X = 209
              Y = 2
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '7'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '8'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '9'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 58
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '+'
              KeyValue = 107
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skAdd
              X = 209
              Y = 58
              Height = 113
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '4'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '5'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '6'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 115
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '1'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '2'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 71
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '3'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 140
              Y = 172
              Height = 57
              Width = 69
              SubKeys = <>
            end
            item
              Caption = 'Enter'
              KeyValue = 13
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skReturn
              Color = 10526880
              X = 209
              Y = 172
              Height = 113
              Width = 69
              SubKeys = <>
            end
            item
              Caption = '0'
              KeyValue = -1
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skNone
              X = 2
              Y = 229
              Height = 57
              Width = 139
              SubKeys = <>
            end
            item
              Caption = '.'
              KeyValue = 110
              ShiftKeyValue = -1
              AltGrKeyValue = -1
              SpecialKey = skDecimal
              X = 140
              Y = 229
              Height = 57
              Width = 69
              SubKeys = <>
            end>
          SmallFont.Charset = DEFAULT_CHARSET
          SmallFont.Color = clWindowText
          SmallFont.Height = -16
          SmallFont.Name = 'Tahoma'
          SmallFont.Style = []
          Version = '1.8.5.2'
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
        end
      end
      object isPanel3: TisPanel
        Left = 1
        Top = 1
        Width = 413
        Height = 144
        Align = alTop
        BiDiMode = bdLeftToRight
        Color = cl3DLight
        ParentBiDiMode = False
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = '  :: T'#7893'ng c'#7897'ng - Total'
        HeaderColor = clScrollBar
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clPurple
        HeaderFont.Height = -15
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          413
          144)
        object HTMLabel9: THTMLabel
          Left = 37
          Top = 102
          Width = 136
          Height = 50
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>S'#7889' ti'#7873'n th'#7889'i l'#7841'i' +
              '</B><br/>Change</FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel7: THTMLabel
          Left = 33
          Top = 28
          Width = 145
          Height = 50
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>T'#7893'ng ti'#7873'n nh'#7853'n</' +
              'B><br/>Advance deposit</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object HTMLabel3: THTMLabel
          Left = 22
          Top = 66
          Width = 158
          Height = 50
          Anchors = [akTop, akRight]
          AutoSizing = True
          AutoSizeType = asBoth
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HTMLText.Strings = (
            
              '<FONT face="Tahoma" size="9" color="#004080"><B>T'#7893'ng thanh to'#225'n<' +
              '/B><br/>Total amount</FONT></FONT>')
          ParentFont = False
          Transparent = True
          Version = '2.2.1.2'
        end
        object EdChange: TwwDBEdit
          Left = 141
          Top = 102
          Width = 250
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = 15794160
          Ctl3D = False
          DataField = 'ThoiLaiTienMat'
          DataSource = FrmMain.DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit23: TwwDBEdit
          Left = 141
          Top = 30
          Width = 250
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = 15794175
          Ctl3D = False
          DataField = 'TongSoTienNhan'
          DataSource = FrmMain.DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit3: TwwDBEdit
          Left = 141
          Top = 66
          Width = 250
          Height = 34
          TabStop = False
          Anchors = [akTop, akRight]
          Color = 15794175
          Ctl3D = False
          DataField = 'ThanhToan'
          DataSource = FrmMain.DsBH
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -23
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 589
    Top = 265
    object CmdReturn: TAction
      Caption = 'In'
      ShortCut = 115
      OnExecute = CmdReturnExecute
    end
    object CmdClose: TAction
      Caption = 'Tho'#225't'
      ShortCut = 27
      OnExecute = CmdCloseExecute
    end
    object CmdDelivery: TAction
      Caption = 'Giao h'#224'ng'
      ShortCut = 114
      Visible = False
      OnExecute = CmdDeliveryExecute
    end
    object CmdTTOAN1: TAction
      Hint = 'Thanh to'#225'n - Ti'#7873'n m'#7863't'
      OnExecute = CmdTTOAN1Execute
    end
    object CmdTTOAN50: TAction
      Hint = 'Thanh to'#225'n VNPAY'
    end
  end
end
