﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Payback;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TFrmPayBack = class(TForm)
    PaST: TPanel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
  public
  	procedure Proc(x : Double);
  end;

var
  FrmPayBack: TFrmPayBack;

implementation

{$R *.DFM}

uses
    isLib, exCommon, isCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayBack.Proc(x : Double);
begin
	PaST.Caption := FormatFloat('#,##0;-#,##0;#', x) + ' ';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayBack.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if (Key = #27) or (Key = #13) then
    	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayBack.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init
end;

end.
