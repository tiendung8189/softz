object FrmPrinter: TFrmPrinter
  Left = 293
  Top = 193
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n M'#225'y In H'#243'a '#208#417'n'
  ClientHeight = 143
  ClientWidth = 371
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    371
    143)
  PixelsPerInch = 96
  TextHeight = 16
  object Image1: TImage
    Left = 12
    Top = 103
    Width = 32
    Height = 32
    Anchors = [akLeft, akBottom]
    AutoSize = True
    ExplicitTop = 76
  end
  object Panel1: TPanel
    Left = 12
    Top = 12
    Width = 347
    Height = 84
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 51
      Height = 16
      Caption = 'Lo'#7841'i m'#225'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 12
      Top = 50
      Width = 50
      Height = 16
      Caption = 'T'#234'n m'#225'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbPrinter: TwwDBLookupCombo
      Left = 70
      Top = 16
      Width = 264
      Height = 24
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'PRINTER'#9'1'#9'PRINTER'#9#9)
      DataField = 'PRINTER'
      DataSource = DsDMQUAY
      LookupTable = QrPRINTER
      LookupField = 'PRINTER'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbPrinter2: TwwDBComboBox
      Left = 70
      Top = 46
      Width = 264
      Height = 24
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = True
      AutoDropDown = True
      ShowMatchText = True
      Ctl3D = True
      DropDownCount = 8
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ItemHeight = 0
      ParentCtl3D = False
      ParentFont = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
    end
  end
  object CmdOK: TBitBtn
    Left = 162
    Top = 107
    Width = 97
    Height = 25
    Cursor = 1
    Action = CmdSave
    Anchors = [akRight, akBottom]
    Caption = 'Ch'#7885'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ParentFont = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 262
    Top = 107
    Width = 97
    Height = 25
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Tho'#225't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFEEEDF8A6A5E06765CE4946CB4946CB6765CEA6A5E0EEEDF8FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A6E20B07CD100CED120EFE13
      0EFF130EFF120EFE100CEE0B07CDA7A6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      8887E0100EE91412FC1313FC110FE90F0DE00F0DE0110FEA1211F71312FB100E
      EA8180DFFFFFFFFFFFFFFFFFFFA7A6E41214EB1619FC1416F21F1ED4AAA9ECAE
      AEE4B5B4E73C39C71315EF1416F61518F91214EBA7A6E4FFFFFFEEEEFA1013DB
      171FFB171DF18C8CECF6F6FDFFFFFFFFFFFF918FDF1A1FE3161EF7161DF4161E
      F6171EFA1013DBEEEEFAA8A7EC151EF11B27FB2224D9FFFFFFFFFFFFFFFFFF88
      85E0141BE61924F91923F61924F5151AE71A25F8151EF2A8A7EC7373E52739FD
      1521F1AAA9EDFFFFFFFFFFFF817EE1141BE31B2CF91B2AF61B2BF81113D93B38
      CF1521F02739FD7373E56062E6536CFF202FECB4B3F1FFFFFF8885E6161EE41E
      32F81C31F61E34F9161DE4918EE5C0BFF1202FEC546CFF5F61E35F61EC607BFF
      414FF1C1BFF4928EEA1820E62139F92037F62139F81821E98985E7FFFFFFB5B4
      F2414FF1607AFF5D5FE07978F15F77FF4C60F73D37E21318E5253FFA243CF826
      41FA1B26EC837EE8FFFFFFFFFFFFADABF54C60F95C75F87675E4ABA8F55C6CFD
      5E7CFE4251F43650FA223EF82845FA202FF28A86EEFFFFFFFFFFFFFFFFFF3B40
      EE6281FF5869E9A9A7EEEEEDFD4042F57490FF5E7BFD5C78FC3D5CFB2637F393
      90F2FFFFFFFFFFFFF6F6FF9B9EF9556DFC7591FD393BD2EEEDFDFFFFFFA8A6F8
      646EFA7A94FE617EFC5A73FB4843EFB6B3F9B1AEFAB4B5FB4047F35873FC7F9D
      FF5B65DFA6A4ECFFFFFFFFFFFFFFFFFF8D89F86772F98CA8FE7695FF5A72FC4F
      64FA4E63F95870FC7D9DFF8EAAFE636EE48887DAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFA8A6F84445F17A88F793A9FD98B1FF98B2FF92A9FB7988EF4042DFA6A4
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEDFEAEACF67E7CF369
      69EF6869EE7C7CEEADABEDEEEDFDFFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
  end
  object QrPRINTER: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from PRINTERS')
    Left = 88
    Top = 4
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'TENMAY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_QUAYTN'
      ' where TENMAY= :TENMAY')
    Left = 120
    Top = 4
    object QrDMQUAYTENMAY: TWideStringField
      DisplayLabel = 'T'#234'n m'#225'y'
      FieldName = 'TENMAY'
      Size = 50
    end
    object QrDMQUAYGHICHU: TWideStringField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrDMQUAYQUAY: TWideStringField
      DisplayLabel = 'S'#7889' hi'#7879'u qu'#7847'y thu ng'#226'n'
      FieldName = 'QUAY'
      Size = 2
    end
    object QrDMQUAYPRINTER: TWideStringField
      DisplayLabel = 'M'#225'y in'
      FieldName = 'PRINTER'
    end
  end
  object DsDMQUAY: TDataSource
    DataSet = QrDMQUAY
    Left = 152
    Top = 4
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 184
    Top = 4
    object CmdSave: TAction
      Caption = 'Ch'#7885'n'
      OnExecute = CmdSaveExecute
    end
  end
end
