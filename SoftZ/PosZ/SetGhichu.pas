﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SetGhichu;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, wwdblook, Wwdbdlg, wwidlg;

type
  TFrmSetGhichu	 = class(TForm)
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    EdLic: TEdit;
    LbText: TLabel;
    procedure CmdReturnKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
  public
    function Execute(var pLic: String): Boolean;
  end;

var
  FrmSetGhichu: TFrmSetGhichu;

implementation

{$R *.DFM}

uses
	isDb, isLib, SysUtils, PosCommon, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmSetGhichu.Execute;

begin
    if ShowModal = mrOK then
    begin
    	pLic := EdLic.Text;
    	Result := True;
	end
    else
    	Result := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetGhichu.CmdReturnClick(Sender: TObject);
var
    s: String;
begin
    s := Trim(EdLic.Text);
    if s = '' then
    begin
        ErrMsg('Phải nhập ghi chú.');
        Abort
    end;

    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetGhichu.CmdReturnKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSetGhichu.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
