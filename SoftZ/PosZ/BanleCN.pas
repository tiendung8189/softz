﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BanleCN;

interface

uses
  SysUtils, Classes, Controls, Forms, Vcl.Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit, DBGridEh, DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh, System.Variants;

type
  TFrmBanleCN = class(TForm)
    ToolMain: TToolBar;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    CmdTotal: TAction;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    QrBHNGAY: TDateTimeField;
    QrBHCA: TWideStringField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHTHUNGAN: TWideStringField;
    QrBHXOA: TWideStringField;
    QrBHNG_CK: TWideStringField;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    LbHH1: TLabel;
    EdHH1: TwwDBEdit;
    EdCK: TwwDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    Label24: TLabel;
    Label1: TLabel;
    CmdDel: TAction;
    QrBHNG_HUY: TWideStringField;
    CmdSearch: TAction;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    QrBHSCT: TWideStringField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrBHIMG: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    QrBHMAMADT: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHTHANHTOAN: TFloatField;
    QrBHLK_TENDT: TWideStringField;
    CmdReRead: TAction;
    QrBHLK_TENKHO: TWideStringField;
    CmdFilterCom: TAction;
    QrBHQUAY: TWideStringField;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    Bevel1: TBevel;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    QrCTBHLOAITHUE: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrCTBHMABO: TWideStringField;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrBHTINHTRANG: TWideStringField;
    QrBHLK_TINHTRANG: TWideStringField;
    QrBHLOC: TWideStringField;
    QrDMKHOLOC: TWideStringField;
    EdTENDT: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBDateTimePicker;
    Label4: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    QrBHLCT: TWideStringField;
    QrBHDELIVERY: TBooleanField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHDEBT_BY: TIntegerField;
    QrBHDEBT_DATE: TDateTimeField;
    Label6: TLabel;
    CbTINHTRANG: TwwDBLookupCombo;
    QrBHLK_DEBT_TEN: TWideStringField;
    QrBHLK_FULLNAME: TWideStringField;
    CmdThongtinCN_View: TAction;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    wwDBEdit7: TwwDBEdit;
    Label7: TLabel;
    N2: TMenuItem;
    Item1: TMenuItem;
    Item2: TMenuItem;
    ItemAll: TMenuItem;
    QrBHMaQuay: TWideStringField;
    QrBHMaVIP: TWideStringField;
    QrBHTyLeCKHD: TFloatField;
    QrBHTyLeCKVip: TFloatField;
    QrBHNhanVienCKHD: TIntegerField;
    QrBHThanhTien: TFloatField;
    QrBHSoTienCK: TFloatField;
    QrBHThanhTienSauCK: TFloatField;
    QrBHThanhToanTinhThue: TFloatField;
    QrBHThanhToanChuaThue: TFloatField;
    QrBHThanhToanChuaCL: TFloatField;
    QrBHSoTienCL: TFloatField;
    QrBHLoaiThue: TWideStringField;
    QrBHThueSuat: TFloatField;
    QrBHSoTienThue: TFloatField;
    QrBHSoTienThue_5: TFloatField;
    QrBHSoTienThue_10: TFloatField;
    QrBHSoTienThue_Khac: TFloatField;
    QrBHKhachDuaTienMat: TFloatField;
    QrBHThoiLaiTienMat: TFloatField;
    QrBHTongSoTienNhan: TFloatField;
    QrBHThanhToanTienMat: TFloatField;
    QrBHThanhToanTienMat_Lan1: TFloatField;
    QrBHThanhToanTienMat_Lan2: TFloatField;
    QrBHThanhToanTheNganHang: TFloatField;
    QrBHThanhToanTheNganHang_Lan1: TFloatField;
    QrBHThanhToanTheNganHang_Lan2: TFloatField;
    QrBHThanhToanTheNganHang_SoChuanChi: TWideStringField;
    QrBHThanhToanTraHang: TFloatField;
    QrBHThanhToanTraHang_SCT: TWideStringField;
    QrBHThanhToanTraHang_Khoa: TGuidField;
    QrBHThanhToanPhieuQuaTang: TFloatField;
    QrBHThanhToanPhieuQuaTang_MaThe: TWideStringField;
    QrBHThanhToanViDienTu: TFloatField;
    QrBHThanhToanViDienTu_Loai: TWideStringField;
    QrBHThanhToanChuyenKhoan: TFloatField;
    QrBHThanhToanDiemTichLuy: TFloatField;
    QrBHQUAYKE: TWideStringField;
    QrBHGhiChu: TWideMemoField;
    QrBHNhanVienThuNgan: TIntegerField;
    QrBHPRINT_NO: TIntegerField;
    QrCTBHThanhTien: TFloatField;
    QrCTBHTyLeCKMH: TFloatField;
    QrCTBHTyLeCKHD: TFloatField;
    QrCTBHTyLeCKVipNhomHang: TFloatField;
    QrCTBHTyLeCKVip: TFloatField;
    QrCTBHTyLeCKPhieu: TFloatField;
    QrCTBHTyLeCKDaChieu: TFloatField;
    QrCTBHTyLeCKBo: TFloatField;
    QrCTBHTyLeCK_Max: TFloatField;
    QrCTBHTyLeCK_Them: TFloatField;
    QrCTBHTyLeCK: TFloatField;
    QrCTBHDonGiaCK: TFloatField;
    QrCTBHSoTienCK: TFloatField;
    QrCTBHThanhTienSauCK: TFloatField;
    QrCTBHDonGiaTinhThue: TFloatField;
    QrCTBHThanhToanTinhThue: TFloatField;
    QrCTBHThanhToanChuaCL: TFloatField;
    QrCTBHSoTienCL: TFloatField;
    QrCTBHDonGiaChuaThue: TFloatField;
    QrCTBHThanhToanChuaThue: TFloatField;
    QrCTBHThueSuat: TFloatField;
    QrCTBHSoTienThue: TFloatField;
    QrCTBHSoTienThue_5: TFloatField;
    QrCTBHSoTienThue_10: TFloatField;
    QrCTBHSoTienThue_Khac: TFloatField;
    QrCTBHThanhToan: TFloatField;
    QrCTBHNhanVienCKMH: TIntegerField;
    QrCTBHLOC: TWideStringField;
    QrBHSoLuong: TFloatField;
    QrCTBHSoLuong: TFloatField;
    QrCTBHDonGia: TFloatField;
    QrCTBHGhiChu: TWideStringField;
    EdMaKho: TDBEditEh;
    cbbKho: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrBHBeforeEdit(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrBHTL_CKChange(Sender: TField);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrBHTINHTRANGValidate(Sender: TField);
    procedure CmdThongtinCN_ViewExecute(Sender: TObject);
    procedure ItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure cbbKhoExit(Sender: TObject);
    procedure cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
    procedure cbbKhoDropDown(Sender: TObject);
  private
    mCanEdit, mClose: Boolean;
    mVATMode: Integer;
    mPrinter: TexPrintBill;

    // List filter
    fType, mFilter: Integer;
   	fTungay, fDenngay: TDateTime;
    fKho, fSQL, fStr: String;

    procedure Total(fUpdate: Boolean);
  public
  	procedure Execute(r: WORD; bClose: Boolean = True);
  end;

var
  FrmBanleCN: TFrmBanleCN;

implementation

uses
	isDb, PosCommon, MainData, Rights, RepEngine, ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, isCommon, ThongtinCN_View, ThongtinCN, ExCommon;

{$R *.DFM}

const
	FORM_CODE = 'HOADON_BANLE_CONGNO';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.Execute(r: WORD; bClose: Boolean);
begin
	mCanEdit := rCanEdit(r);
    mClose := bClose;
    mFilter := 1;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    frNavi.DataSet := QrBH;

    exBillInitial(mPrinter, 'RP_POSZ.RPT');
    mPrinter.PrintPreview := True;
    
    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrBH.SQL.Text;
	EdFrom.Date := Date - sysLateDay;
	EdTo.Date := Date;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrLOC, QrDMKHO, DataMain.QrTT_BANLE]);

    cbbKho.Value := sysDefKho;
    EdMaKho.Text := sysDefKho;
	QrDMQUAY.Open;

    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger('POS', 'VAT Form');

    with QrBH do
    begin
	    SetDisplayFormat(QrBH, sysCurFmt);
    	SetShortDateFormat(QrBH);
	    SetDisplayFormat(QrBH, ['NGAY'], 'dd/mm/yyyy hh:nn');
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
    	SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);
    
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exBillFinal(mPrinter);
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrBH, QrCTBH, QrDMKHO, QrDMQUAY]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.PopupMenu1Popup(Sender: TObject);
begin
    Item1.Checked := mFilter = 1;
    Item2.Checked := mFilter = 2;
    ItemAll.Checked := mFilter = 0;
end;

(*
    ** End: Forms events
	*)

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdRefreshExecute(Sender: TObject);
var
	s, sKho: String;
begin
    if not VarIsNull(cbbKho.Value) then
        sKho := cbbKho.Value
    else
        sKho := '';

   	if (EdFrom.Date <> fTungay) or (EdTo.Date <> fDenngay)or (sKho <> fKho) then
    begin
		fTungay := EdFrom.Date;
		fDenngay := EdTo.Date;
        fKho := sKho;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ sKho + '''');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add(' and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add(' and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add(' and KHOA in (select KHOA from BANLE_CT where KHOA = BANLE.KHOA and MAVT in (' + fStr + '))');
				end;

            case mFilter of
                1:
                    SQL.Add(' and isnull(TINHTRANG, '''') <> ''T03''');
                2:
                    SQL.Add(' and isnull(TINHTRANG, '''') = ''T03''')
            end;

			SQL.Add('order by NGAY desc, SCT desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CbMaKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
	with QrDMQUAY do
    begin
    	Close;
        Open;
    end;
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdSaveExecute(Sender: TObject);
begin
	QrCTBH.UpdateBatch;
	QrBH.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdCancelExecute(Sender: TObject);
begin
	QrCTBH.CancelBatch;
    QrBH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdPrintExecute(Sender: TObject);
var
	k: TGUID;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  TGuidField(FieldByName('KHOA')).AsGuid;
	end;

    if mPrinter.BillPrint(k) then
        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdDelExecute(Sender: TObject);
var
    s: String;
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
    s := QrBH.FieldByName('DGIAI').AsString;
    if QrBH.FieldByName('DELETE_BY').AsInteger = 0 then
    begin
        Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
        if FrmReceiptDesc.Execute(s) then
        begin
            if s = '' then
            begin
                ErrMsg('Phải nhập ghi chú mới được xóa phiếu.');
                Exit;
            end;
        end;
    end;

   	MarkDataSet(QrBH);
    with QrBH do
    begin
    	Edit;
        FieldByName('DGIAI').AsString := s;
//        FieldByName('DRC_STATUS').AsString := '2';
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdThongtinCN_ViewExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmThongtinCN_View, FrmThongtinCN_View);
//    if not FrmThongtinCN_View.Execute then
//        Exit;

    Application.CreateForm(TFrmThongtinCN, FrmThongtinCN);
    if not FrmThongtinCN.Execute(False, True) then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
    Total(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrBH);
    CmdThongtinCN_View.Enabled := not bEmpty;

    CmdPrint.Enabled := not bEmpty;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse; 
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHBeforeEdit(DataSet: TDataSet);
begin
    with QrBH do
    begin
        if FieldByName('TINHTRANG').AsString = 'T03' then
        begin
            ErrMsg('Phiếu đã thanh toán, không thể chỉnh sửa.');
            Abort;
        end;
    end;

	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHTL_CKChange(Sender: TField);
begin
	Total(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM_THANHTOAN = 'Lưu phiếu rồi sẽ không chỉnh sửa được. Tiếp tục?'#13
                        + 'Thu ngân: "%s". Ngày: "%s".';
procedure TFrmBanleCN.QrBHTINHTRANGValidate(Sender: TField);
var
    d: TDateTime;
begin
    d := Now;
    if Sender.AsString = 'T03' then
    begin
        if not YesNo(Format(RS_CONFIRM_THANHTOAN, [sysLogonFullName, DateTimeToStr(d)])) then
            Abort;
        with QrBH do
        begin
            FieldByName('DEBT_BY').AsInteger := sysLogonUID;
            FieldByName('DEBT_DATE').AsDateTime := d;
        end;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := '';
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;

    with QrDMQUAY do
    begin
        Close;
        Open;
    end;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.cbbKhoDropDown(Sender: TObject);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.cbbKhoExit(Sender: TObject);
begin
    EdMaKho.Text := cbbKho.Value;
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrBH do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clGray;
            Exit;
        end;

        if FieldByName('TINHTRANG').AsString = 'T01' then
        begin
            AFont.Color := clRed;
            Exit;
        end;

        if FieldByName('TINHTRANG').AsString = 'T02' then
        begin
            AFont.Color := clPurple;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.ItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh, xCkhd: Double;
	mSotien, mSoluong, mCkmh, mCkhd, mTlckhd, mThue: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mCkhd := 0;
    mThue := 0;
	mTlckhd := QrBH.FieldByName('TyLeCKHD').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
        	xSotien := FieldByName('ThanhTien').AsFloat;		// Thanh tien chua chiet khau
            xThsuat := FieldByName('ThueSuat').AsFloat;	// Thue suat
            xCkmh := FieldByName('SoTienCK').AsFloat;		// Tien CKMH
            if xCkmh = 0.0 then								// Tinh tien CKHD
	            xCkhd := xSotien * mTlckhd / 100.0
            else
            	xCkhd := 0.0;

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SoLuong').AsFloat;
	        mCkmh := mCkmh + xCkmh;
	        mCkhd := mCkhd + xCkhd;

            if mVATMode = 0 then	// Tinh thue truoc chiet khau
	            mThue := mThue + xSotien * xThsuat / (100.0 + xThsuat)
	    	else					// Tinh thue sau chiet khau
	            mThue := mThue + (xSotien - xCkmh - xCkhd) * xThsuat / (100.0 + xThsuat);

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('ThanhTien').AsFloat  := mSotien;
    	FieldByName('SoLuong').AsFloat := mSoluong;
    	FieldByName('SoTienCK').AsFloat := mCkmh;
    	FieldByName('SoTienCK').AsFloat := mCkhd;
    	FieldByName('SoTienThue').AsFloat := mThue;
    	FieldByName('ThanhToan').AsFloat := mSotien - mCkmh - mCkhd;

        if fUpdate then
	        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanleCN.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
