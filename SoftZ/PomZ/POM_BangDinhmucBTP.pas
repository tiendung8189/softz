﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_BangDinhmucBTP;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin;

type
  TFrmPOM_BangDinhmucBTP = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    CmdRefresh: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    TntToolButton4: TToolButton;
    QrListRSTT: TIntegerField;
    QrListNAM: TIntegerField;
    QrListTHANG: TIntegerField;
    QrListMAVT: TWideStringField;
    QrListMAVT_NVL: TWideStringField;
    QrListDINHMUC: TFloatField;
    QrListDONGIA: TFloatField;
    QrListTHANHTIEN: TFloatField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrListLK_TENVT: TWideStringField;
    QrListLK_DVT: TWideStringField;
    QrListLK_CO: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    CmdDmvtNVL: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    GrGroup: TwwDBGrid2;
    QrGroup: TADOQuery;
    DsGroup: TDataSource;
    QrGroupNAM: TIntegerField;
    QrGroupTHANG: TIntegerField;
    QrGroupMAVT: TWideStringField;
    QrGroupLK_TENVT: TWideStringField;
    QrGroupCHIPHI_NVL: TFloatField;
    QrGroupTONG_CHIPHI_NVL: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure QrListPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrListBeforeEdit(DataSet: TDataSet);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListBeforeInsert(DataSet: TDataSet);
    procedure CmdDmvtNVLExecute(Sender: TObject);
    procedure QrGroupBeforeOpen(DataSet: TDataSet);
    procedure QrGroupAfterScroll(DataSet: TDataSet);
    procedure QrListDINHMUCChange(Sender: TField);
    procedure CmdRefreshExecute(Sender: TObject);
  private
  	mCanEdit, mBookClosed: Boolean;
  	mThang, mNam: Integer;

    procedure OpenQueries;
  public
  	procedure Execute(r: WORD; pNam, pThang: Integer);
  end;

var
  FrmPOM_BangDinhmucBTP: TFrmPOM_BangDinhmucBTP;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon,
  POM_DmvtNVL;

{$R *.DFM}

const
	FORM_CODE = 'POM_BANG_DINHMUC_BTP';
    TABLE_NAME  = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.Execute;
begin
    mThang := pThang;
    mNam := pNam;
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrList]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

    AddAllFields(QrList, TABLE_NAME);
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdDmvtNVLExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_NVL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtNVL, FrmPOM_DmvtNVL);
    if FrmPOM_DmvtNVL.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListBeforeOpen(DataSet: TDataSet);
begin
    with QrList do
    begin
        Parameters[0].Value := mNam;
        Parameters[1].Value := mThang;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.FormShow(Sender: TObject);
begin
    LbMA.Caption := IntToStr(mThang);
    LbTEN.Caption := IntToStr(mNam);

    with DataMain do
        OpenDataSets([QrPOM_DM_CHIPHI, QrPOM_DM_CHIPHI_NHOM]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetDisplayFormat(QrList, ['DINHMUC'], sysFloatFmtFour);
    SetDisplayFormat(QrList, ['DONGIA'], sysFloatFmtTwo);

    SetDisplayFormat(QrGroup, sysCurFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_GROUP'], [GrList, GrGroup]);
    SetDictionary([QrList, QrGroup], [FORM_CODE, FORM_CODE + '_GROUP'], [Filter, nil]);

    OpenQueries;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.OpenQueries;
begin
    with QrList do
    begin
        Close;
        Open;
    end;

    with QrGroup do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListBeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrGroupAfterScroll(DataSet: TDataSet);
var
    s: String;
begin
    if not QrList.Active then
        Exit;

    Screen.Cursor := crSQLWait;
    QrList.Filter := 'MAVT=' + QuotedStr(QrGroup.FieldByName('MAVT').AsString);
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrGroupBeforeOpen(DataSet: TDataSet);
begin
    with QrGroup do
    begin
        Parameters[0].Value := mNam;
        Parameters[1].Value := mThang;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListBeforeDelete(DataSet: TDataSet);
begin
//    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdSaveExecute(Sender: TObject);
begin
	QrList.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdCancelExecute(Sender: TObject);
begin
	QrList.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not QrList.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListBeforeInsert(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdRefreshExecute(Sender: TObject);
begin
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdReReadExecute(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdCalcExecute(Sender: TObject);
begin
	if DataMain.POM_UpdateGiathanh(sysMon, sysYear) <> 0 then
    begin
    	ErrMsg(Format(RS_POM_GEN_ERROR, [sysMon, sysYear]));
		Exit;
    end;
    CmdReRead.Execute;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListCalcFields(DataSet: TDataSet);
begin
   with QrList do
   	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucBTP.QrListDINHMUCChange(Sender: TField);
var
    dinhmuc, dongia, thanhtien: Double;
begin
    with QrList do
    begin
        dinhmuc :=  FieldByName('DINHMUC').AsFloat;
        dongia :=  FieldByName('DONGIA').AsFloat;

        thanhtien := dinhmuc * dongia;

        FieldByName('THANHTIEN').AsFloat := thanhtien;
    end;

    GrList.InvalidateCurrentRow;
end;

end.
