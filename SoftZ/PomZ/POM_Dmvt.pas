﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_Dmvt;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons,
  AdvEdit;

type
  TFrmPOM_Dmvt = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    QrDMVTGIANHAP: TFloatField;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTDVT_BOX: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    ItmNHOM: TMenuItem;
    N2: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopPrint: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    CmdExport: TAction;
    BtExport: TToolButton;
    ToolButton12: TToolButton;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label12: TLabel;
    EdTen: TwwDBEdit;
    CbDVT1: TwwDBLookupCombo;
    PD2: TisPanel;
    Label18: TLabel;
    EdVAT_VAO: TwwDBEdit;
    PD5: TisPanel;
    EdGHICHU: TDBMemo;
    PD6: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    Label10: TLabel;
    wwDBLookupCombo6: TwwDBLookupCombo;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    CmdSwitch: TAction;
    QrDMVTLOAITHUE: TWideStringField;
    TntLabel6: TLabel;
    CbLoaithue: TwwDBLookupCombo;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    TntLabel7: TLabel;
    CmdAudit: TAction;
    N3: TMenuItem;
    CmdImport: TAction;
    ToolButton9: TToolButton;
    ToolButton13: TToolButton;
    RzLauncher: TRzLauncher;
    PopExport: TAdvPopupMenu;
    Export1: TMenuItem;
    CmdDmdvt: TAction;
    SpeedButton1: TSpeedButton;
    CmdDetailDinhmuc: TAction;
    ToolButton8: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    PopTinhtrang1: TMenuItem;
    PD7: TisPanel;
    LbGianhap: TLabel;
    wwDBEdit10: TwwDBEdit;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang: TMenuItem;
    PopTinhtrang2: TMenuItem;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    QrDMVTTL_CK_NCC: TFloatField;
    QrDMVTGIANHAP_CHUA_VAT: TFloatField;
    QrDMVTXUATXU: TWideStringField;
    QrDMVTTINHCHAT_NVL: TWideStringField;
    QrDMVTMA_PREFIX: TWideStringField;
    QrDMVTMA: TWideStringField;
    QrDMVTCHIPHI_DM_CHUA_HAOHUT: TFloatField;
    QrDMVTCHIPHI_CD_CHUA_HAOHUT: TFloatField;
    QrDMVTCHIPHI_CHUA_HAOHUT: TFloatField;
    QrDMVTCHIPHI_DM: TFloatField;
    QrDMVTCHIPHI_CD: TFloatField;
    QrDMVTCHIPHI: TFloatField;
    Label14: TLabel;
    EdMA: TwwDBEdit;
    QrQuocgia: TADOQuery;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrDMVTMAVT_NCC: TWideStringField;
    QrDMVTMADT: TWideStringField;
    QrDMVTKHAC_NCC: TBooleanField;
    PopImport: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdDetailCongdoan: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    Cngon1: TMenuItem;
    Label1: TLabel;
    wwDBLookupCombo1: TwwDBLookupCombo;
    QrDMVTLK_TEN_CONGTY: TWideStringField;
    Label2: TLabel;
    wwDBLookupCombo2: TwwDBLookupCombo;
    QrDMVTMA_DUNGTICH: TWideStringField;
    QrDMVTMA_CONGTY: TWideStringField;
    QrDMVTLK_HESO_DUNGTICH: TFloatField;
    wwDBEdit1: TwwDBEdit;
    ImportBnthnhphmnhmctfileExcel1: TMenuItem;
    Label3: TLabel;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    PD0: TisPanel;
    Label15: TLabel;
    Label16: TLabel;
    CbMANGANH: TwwDBLookupCombo;
    CbNGANH: TwwDBLookupCombo;
    CbMANHOM: TwwDBLookupCombo;
    CbNHOM: TwwDBLookupCombo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure CmdDetailDinhmucExecute(Sender: TObject);
    procedure PopTinhtrang1Click(Sender: TObject);
    procedure PopMainPopup(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDMVTDVTChange(Sender: TField);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTMAVTValidate(Sender: TField);
    procedure QrDMVTMAChange(Sender: TField);
    procedure CmdDetailCongdoanExecute(Sender: TObject);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure CbMANHOMBeforeDropDown(Sender: TObject);
  private
  	mCanEdit, mEANConfirm, mRet: Boolean;
    mObsolete, defDvt : Integer;

    defTinhtrang, defTinhchat, defThue: String;
  	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4, mPrefix, mSearch: String;

    mLoaiNganh: String;

  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmPOM_Dmvt: TFrmPOM_Dmvt;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, Scan, Variants,
    CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
  TimBarcode2, Dmkhac, DmvtCT, OfficeData, POM_DmvtBTP_Dinhmuc,
  POM_Dmvt_Dinhmuc, DmHotro;

{$R *.DFM}

const
	FORM_CODE = 'POM_DM_TP';
    TABLE_NAME  = 'POM_DM_HH';
    REPORT_NAME = FORM_CODE;
    EXPORT_EXCEL = 'POM_RP_DM_TP_EXCEL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmPOM_Dmvt.Execute;
begin
	mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.FormCreate(Sender: TObject);
var
    x: Integer;
begin
    TMyForm(Self).Init2;

    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
	    SetDisplayFormat(QrDMVT, ['TL_CK_NCC'], sysPerFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
        SetDisplayFormat(QrDMVT, ['LK_HESO_DUNGTICH'], sysFloatFmtTwo);
    	SetShortDateFormat(QrDMVT);
    end;

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE,'EAN Confirm');

    // Default unit
	defDvt := GetSysParam('DEFAULT_DVT');
    defThue := GetSysParam('DEFAULT_LOAITHUE');

    defTinhtrang := '01';
    defTinhchat := '01';

    // Initial
    mTrigger := False;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    mObsolete     := 0;
    mLoaiNganh := 'TP';
    // Tree Combo
    FlexGroupCombo(CbNhomhang, mLoaiNganh);

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PD5.Collapsed := RegReadBool('PD5');
    PD6.Collapsed := RegReadBool('PD6');
    PD7.Collapsed := RegReadBool('PD7');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.FormShow(Sender: TObject);
begin
//	DsDMVT.AutoEdit := mCanEdit;
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    QrQuocgia.Open;
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrDMLOAITHUE, QrPOM_DMHH,
                QrV_POM_DMVT_TINHTRANG, QrV_POM_DM_CONGTY,
                QrPOM_DM_HESO_DUNGTICH]);
    end;
    OpenDataSets([QrNGANH, QrNHOM]);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('TINHTRANG').AsString <> '01' then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    RegWrite(Name, ['PD2', 'PD5', 'PD6', 'PD7'],
    	[PD2.Collapsed, PD5.Collapsed, PD6.Collapsed, PD7.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdNewExecute(Sender: TObject);
begin
	QrDMVT.Append;
    PD0.Enabled := True;
    CbMANGANH.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdSaveExecute(Sender: TObject);
begin
    QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdDelExecute(Sender: TObject);
begin
    if DataMain.POM_BarcodeIsUsed(QrDMVT.FieldByName('MAVT').AsString) then
        Abort;
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdDetailCongdoanExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdDetailDinhmucExecute(Sender: TObject);
begin
    with QrDMVT do
    begin
        Application.CreateForm(TFrmPOM_Dmvt_Dinhmuc, FrmPOM_Dmvt_Dinhmuc);
        if FrmPOM_Dmvt_Dinhmuc.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString) then
        begin
            exReSyncRecord(QrDMVT);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdPrintExecute(Sender: TObject);
var
    REP_NAME: String;
begin
    REP_NAME := REPORT_NAME;

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, b, bInsert: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
        bInsert := State in [dsInsert];
	end;

    CmdNew.Enabled := bBrowse and mCanEdit and sysIsCentral;
    CmdEdit.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;

    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdDetailDinhmuc.Enabled := bBrowse;
    CmdDetailCongdoan.Enabled := False;

    PD0.Enabled := bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.PopMainPopup(Sender: TObject);
begin
    PopTinhtrang.Checked := mObsolete = 0;
    PopTinhtrang1.Checked := mObsolete = 1;
    PopTinhtrang2.Checked := mObsolete = 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.PopTinhtrang1Click(Sender: TObject);
begin
    mObsolete :=  (Sender as TComponent).Tag;
    mNganh := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4, s1 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) or
        (EdSearch.Text <> mSearch) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mNhom2 := sNhom2;
        mNhom3 := sNhom3;
        mNhom4 := sNhom4;
        mSearch := EdSearch.Text;

        with QrDMVT do
        begin
			Close;
            sSql := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''', [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            case mObsolete of
            0:
                sSql := sSql + ' and TINHTRANG in (''01'')';
            1:
                sSql := sSql + ' and TINHTRANG in (''02'')';
            end;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSql := sSql + ' and (' +
                            'dbo.fnStripToneMark([MAVT]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([TENVT]) like ''%' + s1 + '%'''
                            + ')';
            end;

            SQL.Text := sSQL;
            Open;
            First;
        end;

//        with Filter do
//        begin
//            RefreshOriginalSQL; //NTD
//            ApplyFilter;
//        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
        FieldByName('LOAITHUE').AsString := defThue;
        FieldByName('QD1').AsInteger := 1;
//        FieldByName('GIABAN').AsFloat := mGia;
        FieldByName('DVT').AsInteger := defDvt;
		FieldByName('TINHTRANG').AsString := defTinhtrang;

        FieldByName('MA_PREFIX').AsString := mPrefix;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';

procedure TFrmPOM_Dmvt.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM', 'TENVT', 'DVT', 'LOAITHUE']) then
			Abort;

        if FieldByName('DVT_BOX').AsString = '' then
        begin
           FieldByName('DVT_BOX').AsString := FieldByName('DVT').AsString ;
           FieldByName('QD1').Value := 1;
        end;

	    SetNull(DataSet, ['MADT', 'TINHCHAT_NVL']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTGIANHAPChange(Sender: TField);
var
	mGiavon, mGianhapchuaVAT, mGianhap, mTL_CK_NCC: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDmvt do
    begin
        mGiavon := FieldByName('GIAVON').AsFloat;
        mTL_CK_NCC := FieldByName('TL_CK_NCC').AsFloat;
		mGianhapchuaVAT := FieldByName('GIANHAP_CHUA_VAT').AsFloat;
        mGianhap := FieldByName('GIANHAP').AsFloat;
        mLoaithue := FieldByName('LOAITHUE').AsString;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters[1].Value := mLoaithue;
        Parameters[2].Value := Sender.FieldName;
    	Parameters[3].Value := mGianhapchuaVAT;
        Parameters[4].Value := mGianhap;
        Parameters[5].Value := mTL_CK_NCC;
        Parameters[6].Value := mGiavon;
        Execute;
    	mGianhapchuaVAT := Parameters[3].Value;
        mGianhap := Parameters[4].Value;
        mTL_CK_NCC := Parameters[5].Value;
        mGiavon := Parameters[6].Value;
    end;

    mTrigger := True;
	with QrDmvt do
    begin
        FieldByName('GIANHAP_CHUA_VAT').AsFloat := mGianhapchuaVAT;
        FieldByName('GIANHAP').AsFloat := mGianhap;

        FieldByName('GIAVON').AsFloat := mGiavon;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmPOM_Dmvt.QrDMVTTENVTChange(Sender: TField);
var
	b: Boolean;
begin
	with QrDMVT do
    begin
        FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);

        FieldByName('TENVT_KHONGDAU').AsString := DataMain.StripToneMark(FieldByName('TENVT').AsString)
	end;
end;

procedure TFrmPOM_Dmvt.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmPOM_Dmvt.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTMAVTValidate(Sender: TField);
begin
    with QrDMVT do
    if FieldByName('MAVT').OldValue <> null then
        if DataMain.BarcodeIsUsed(FieldByName('MAVT').OldValue, True) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTMAChange(Sender: TField);
var
    n: Integer;
	mMa, s, mavt: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;

    with QrDMVT do
    begin
        mavt := FieldByName('MA_PREFIX').AsString + FieldByName('MA').AsString;
        FieldByName('MAVT').AsString := mavt;
        FieldByName('MAVT_NCC').AsString := mavt;
    end;
end;

procedure TFrmPOM_Dmvt.QrDMVTMANGANHChange(Sender: TField);
begin
    QrDMVT.FieldByName('MANHOM').AsString := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdExportExecute(Sender: TObject);
begin
    DataOffice.CreateReport2('XLSX\' + EXPORT_EXCEL + '.xlsx',
        [sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4], EXPORT_EXCEL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

procedure TFrmPOM_Dmvt.CbMANHOMBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdImportExecute(Sender: TObject);
var
    sFile, iSection, iTable, iStore, iKeyField: String;
    b: Boolean;
    rs: Integer;
begin
	sFile := isGetOpenFileName('XLS');
    if sFile = '' then
        Exit;

    rs := YesNoCancel(RS_IM_EXCEL_YESNO);
    if rs = mrCancel then
        Exit;

    b := rs = mrYes;

    if (Sender as TComponent).Tag = 1 then
    begin
        iSection := 'IMP_' + FORM_CODE + '_DINHMUC';
        iTable := 'IMP_DM_HH_BO';
        iStore := 'spIMPORT_POM_DM_HH_DINHMUC;1';
        iKeyField := ''
    end else
    begin
        iSection := 'IMP_' + FORM_CODE;
        iTable := 'IMP_DM_HH';
        iStore := 'spIMPORT_POM_DM_HH;1';
        iKeyField := 'MAVT'
    end;

    DataExcel.ExcelImport(iSection, sFile, iTable, iStore, iKeyField,
                                [sysLogonUID, mPrefix, mLoaiNganh, b], 0);
    CmdReRead.Execute;
    mRet := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_Dmvt.QrDMVTDVTChange(Sender: TField);
begin
    with QrDMVT do
    begin
        FieldByName('DVT_BOX').AsString := FieldByName('DVT').AsString;
    end;
end;

end.
