﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_BangChiphi;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin;

type
  TFrmPOM_BangChiphi = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    CmdRefresh: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    BtIn: TToolButton;
    QrListRSTT: TIntegerField;
    QrListNAM: TIntegerField;
    QrListTHANG: TIntegerField;
    QrListMACP: TWideStringField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrListLK_TENCP: TWideStringField;
    QrListTENCP_NHOM: TWideStringField;
    QrListSOTIEN1: TFloatField;
    QrListSOTIEN2: TFloatField;
    QrListSOTIEN: TFloatField;
    QrListTAIKHOAN1: TWideStringField;
    QrListTAIKHOAN2: TWideStringField;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrList2: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    GuidField1: TGuidField;
    WideStringField3: TWideStringField;
    WideStringField5: TWideStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    WideStringField6: TWideStringField;
    WideStringField7: TWideStringField;
    DsList2: TDataSource;
    GrList2: TwwDBGrid2;
    Filter2: TwwFilterDialog2;
    QrListCODINH: TBooleanField;
    QrList2CODINH: TBooleanField;
    CmdDmChiphi: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrListMACP_NHOM: TWideStringField;
    QrListPLOAI: TWideStringField;
    QrListTILE1: TFloatField;
    QrListTILE2: TFloatField;
    QrList2MACP_NHOM: TWideStringField;
    QrList2PLOAI: TWideStringField;
    QrList2TILE1: TFloatField;
    QrList2TILE2: TFloatField;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    PopIn: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure QrListPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrListBeforeEdit(DataSet: TDataSet);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListBeforeInsert(DataSet: TDataSet);
    procedure CmdDmChiphiExecute(Sender: TObject);
    procedure QrListTILE1Validate(Sender: TField);
    procedure BtInClick(Sender: TObject);
    procedure QrListSOTIENChange(Sender: TField);
    procedure QrListTAIKHOAN1Change(Sender: TField);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FloatField1Change(Sender: TField);
  private
  	mCanEdit, mBookClosed: Boolean;
  	mThang, mNam: Integer;
  public
  	procedure Execute(r: WORD; pNam, pThang: Integer);
  end;

var
  FrmPOM_BangChiphi: TFrmPOM_BangChiphi;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon,
  POM_DmChiphi, OfficeData;

{$R *.DFM}

const
	FORM_CODE = 'POM_BANG_CHIPHI_CHUNG';
    FORM_CODE2 = 'POM_BANG_CHIPHI_CHUNG_PX';
    TABLE_NAME  = FORM_CODE;
    TABLE_NAME2  = FORM_CODE;
    REP_NAME    = FORM_CODE;
    REP_NAME2   = FORM_CODE2;
    REP_NAME_EXCEL  = 'POM_RP_BANG_CHIPHI_CHUNG_EXCEL';
    REP_NAME_EXCEL2  = 'POM_RP_BANG_CHIPHI_CHUNG_PX_EXCEL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.Execute;
begin
    mThang := pThang;
    mNam := pNam;
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.FloatField1Change(Sender: TField);
begin
    with QrList2 do
    begin
        FieldByName('SOTIEN').AsFloat := FieldByName('SOTIEN1').AsFloat + FieldByName('SOTIEN2').AsFloat
    end;

    GrList2.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrList, QrList2]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddAllFields(QrList, TABLE_NAME);
    AddAllFields(QrList2, TABLE_NAME2);
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdDmChiphiExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('POM_DM_CHIPHI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmChiphi, FrmPOM_DmChiphi);
    FrmPOM_DmChiphi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := mNam;
        Parameters[1].Value := mThang;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.FormShow(Sender: TObject);
begin
    LbMA.Caption := IntToStr(mThang);
    LbTEN.Caption := IntToStr(mNam);

    with DataMain do
        OpenDataSets([QrPOM_DM_CHIPHI, QrPOM_DM_CHIPHI_NHOM]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetDisplayFormat(QrList2, sysCurFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE2], [GrList, GrList2]);
    SetDictionary([QrList, QrList2], [FORM_CODE, FORM_CODE2], [Filter, Filter2]);

    CmdRefresh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListBeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListSOTIENChange(Sender: TField);
var
    tile1, tile2, sotien1, sotien2, sotien: Double;
begin
    with QrList do
    begin
        sotien := FieldByName('SOTIEN').AsFloat;
        tile1 := FieldByName('TILE1').AsFloat;
        tile2 := 100 - tile1;

        sotien1 := sotien * tile1/100;
        sotien2 := sotien - sotien1;

        FieldByName('TILE2').AsFloat := tile2;
        FieldByName('SOTIEN1').AsFloat := sotien1;
        FieldByName('SOTIEN2').AsFloat := sotien2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListTAIKHOAN1Change(Sender: TField);
begin
    with QrList do
    begin
        FieldByName('TAIKHOAN2').AsString := FieldByName('TAIKHOAN1').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListTILE1Validate(Sender: TField);
var
    tile: Double;
begin
    tile := Sender.AsFloat;
    if (tile < 0) or (tile > 100)  then
    begin
        ErrMsg('Tỷ lệ không hợp lệ');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListBeforeDelete(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdSaveExecute(Sender: TObject);
begin
	QrList.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdCancelExecute(Sender: TObject);
begin
	QrList.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not QrList.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    if (DataSet as TADOQuery).FieldByName('MACP').AsString = '9000' then //Mã này là sum của Phân bổ chi phí chung, không sửa được
        Abort;

    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListBeforeInsert(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.BtInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdRefreshExecute(Sender: TObject);
begin
    with QrList do
    begin
        Close;
        Open;
    end;

    with QrList2 do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdReReadExecute(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdCalcExecute(Sender: TObject);
begin
	if DataMain.POM_UpdateChiphiChung(sysMon, sysYear) = 0 then
    begin
		CmdReRead.Execute;
        MsgDone;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.CmdPrintExecute(Sender: TObject);
var
    repname, filename: String;
    n: Integer;
begin
	CmdSave.Execute;
    repname := REP_NAME_EXCEL;
    filename := 'XLSX\' + REP_NAME_EXCEL + '.xlsx';

    n := (Sender as TComponent).Tag;
    case n of
        1:
            begin
                repname := REP_NAME_EXCEL2;
                filename := 'XLSX\' + REP_NAME_EXCEL2 + '.xlsx';
            end
    end;

    with QrList do
        DataOffice.CreateReport2(filename, [sysLogonUID, mThang, mNam], repname);
//	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangChiphi.QrListCalcFields(DataSet: TDataSet);
begin
   with DataSet as TADOQuery do
   	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

end.
