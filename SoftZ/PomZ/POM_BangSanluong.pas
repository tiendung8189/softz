﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_BangSanluong;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, ComCtrls, ActnList,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, wwdblook, Db, ADODB, wwfltdlg,
  Menus, AdvMenus, AppEvnts, fctreecombo, wwFltDlg2, wwDBGrid2,
  wwDialog, ToolWin, Mask, wwdbedit, Wwdotdot, Wwdbcomb, frameMonthYear;

type
  TFrmPOM_BangSanluong = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    ToolBar1: TToolBar;
    BtnXuly: TToolButton;
    ToolButton3: TToolButton;
    ToolButton11: TToolButton;
    QrCongty: TADOQuery;
    CmdRefresh: TAction;
    DsCongty: TDataSource;
    QrList: TADOQuery;
    DsList: TDataSource;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    QrEmp: TADOQuery;
    PopChitiet: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    frMonthYear: TfrMonthYear;
    CmdParams: TAction;
    PopGroup: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    Lcdliu1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N5: TMenuItem;
    Item3: TMenuItem;
    CmdRep: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdSwitch: TAction;
    CmdCalc1: TAction;
    Tbsungdliuqunqutth1: TMenuItem;
    CmdDetail: TAction;
    Kimtratnhngcadliu1: TMenuItem;
    CmdReload: TAction;
    CmdUpdate: TAction;
    N10: TMenuItem;
    Status: TStatusBar;
    N4: TMenuItem;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    GrGroup: TwwDBGrid2;
    GrList: TwwDBGrid2;
    Splitter1: TSplitter;
    PopList: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Bevel1: TBevel;
    CmdChiphiChung: TAction;
    Cpnhtdliutdliucngth1: TMenuItem;
    TS_RECORDER_LOG: TADOCommand;
    TS_PAYROLL_PERIOD: TADOCommand;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    spHR_XOACONG: TADOCommand;
    CmdBOMTP: TAction;
    QrListNAM: TIntegerField;
    QrListTHANG: TIntegerField;
    QrListMAVT: TWideStringField;
    QrListMAVT_CHITIET_CHINH: TWideStringField;
    QrListSANLUONG: TFloatField;
    QrListMA_DUNGTICH: TWideStringField;
    QrListDUNGTICH: TFloatField;
    QrListSANLUONG_DUNGTICH: TFloatField;
    QrListHESO_DUNGTICH: TFloatField;
    QrListSANLUONG_HESO_DUNGTICH: TFloatField;
    QrListMA_DODAM: TWideStringField;
    QrListHESO_DODAM: TFloatField;
    QrListSANLUONG_HESO_DODAM: TFloatField;
    QrListCHIPHI_BAOBI: TFloatField;
    QrListTONG_CHIPHI_BAOBI: TFloatField;
    QrListTL_HAOHUT: TFloatField;
    QrListSOTIEN_HAOHUT: TFloatField;
    QrListTONG_SOTIEN_HAOHUT: TFloatField;
    QrListCHIPHI_NVL: TFloatField;
    QrListTONG_CHIPHI_NVL: TFloatField;
    QrListSOTIEN_QUYRD: TFloatField;
    QrListTONG_SOTIEN_QUYRD: TFloatField;
    QrListCHIPHI_CHEBIEN: TFloatField;
    QrListTONG_CHIPHI_CHEBIEN: TFloatField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrCongtyMANHOM_HOTRO: TWideStringField;
    QrCongtyMA_HOTRO: TWideStringField;
    QrCongtyTEN_HOTRO: TWideStringField;
    CmdBOMBTP: TAction;
    TabSheet2: TTabSheet;
    GrList2: TwwDBGrid2;
    QrList2: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    WideStringField6: TWideStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    GuidField1: TGuidField;
    DsList2: TDataSource;
    QrList2MAVT: TWideStringField;
    QrList2CONGTY: TWideStringField;
    QrList2THETICH_THUHOI: TFloatField;
    QrList2TONG_CHIPHI_NVL: TFloatField;
    QrList2CHIPHI_NVL_BQ: TFloatField;
    QrList2TL_QUYRD: TFloatField;
    QrList2SOTIEN_QUYRD: TFloatField;
    QrList2CHIPHI_NVL: TFloatField;
    QrList2MA_DODAM: TWideStringField;
    QrList2HESO_DODAM: TFloatField;
    QrList2CHIPHI_CHEBIEN_BQ: TFloatField;
    QrList2GIATHANH_BTP_BQ: TFloatField;
    Panel1: TPanel;
    Filter2: TwwFilterDialog2;
    QrList2LK_TENVT: TWideStringField;
    QrListMA_CONGTY: TWideStringField;
    CmdDmvtBTP: TAction;
    CmdDmvtNVL: TAction;
    ToolButton6: TToolButton;
    ToolButton12: TToolButton;
    CmdDmvtTP: TAction;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    CmdClear: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure BtnXulyClick(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdParamsExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRepExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrCongtyAfterScroll(DataSet: TDataSet);
    procedure PS_DAILYAfterScroll(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure CmdChiphiChungExecute(Sender: TObject);
    procedure CmdBOMBTPExecute(Sender: TObject);
    procedure CmdBOMTPExecute(Sender: TObject);
    procedure CmdDmvtBTPExecute(Sender: TObject);
    procedure CmdDmvtNVLExecute(Sender: TObject);
    procedure CmdDmvtTPExecute(Sender: TObject);
    procedure QrList2BeforePost(DataSet: TDataSet);
  private
  	mCanEdit, mTrigger, bClose: Boolean;
    fStr: String;
    fLevel: Integer;
	sqlGroup, sqlList, sqlList2: String;

    fsDateFormat: TFormatSettings;

    procedure GetPeriod;

    procedure OpenTimeSheet;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmPOM_BangSanluong: TFrmPOM_BangSanluong;

implementation

{$R *.DFM}

uses
	isLib, Rights, isDb, MainData, ExCommon, isStr, isMsg, Tuden,
  	POM_BangThongso, Variants, isFile, isCommon, GmsRep, POM_BangChiphi,
  POM_BangDinhmucBTP, POM_BangDinhmucTP, POM_DmvtBTP, POM_DmvtNVL, POM_Dmvt;

const
	FORM_CODE = 'POM_BANG_SANLUONG';
    TABLE_NAME = FORM_CODE;

    FORM_CODE2 = 'POM_BANG_DONGIA_BTP';
    TABLE_NAME2 = FORM_CODE2;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.Execute;
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets(DataMain.Conn);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
	frMonthYear.Initial(sysMon, sysYear);
    mTrigger := False;

    sqlGroup := QrCongty.SQL.Text;
    sqlList := QrList.SQL.Text;
    sqlList2 := QrList2.SQL.Text;

    AddAllFields(QrList, TABLE_NAME, 0);
    AddAllFields(QrList2, TABLE_NAME2, 0);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.FormShow(Sender: TObject);
begin
    // Open database
	OpenDataSets([QrEmp]);

    //Format Date.
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fsDateFormat);

    // Display format

	SetDisplayFormat(QrCongty, sysFloatFmtOne);

    SetDisplayFormat(QrList, sysCurFmt);
    SetDisplayFormat(QrList, ['HESO_DUNGTICH'], sysFloatFmtTwo);

    SetDisplayFormat(QrList2, sysCurFmt);
    SetDisplayFormat(QrList2, ['HESO_DODAM'], sysFloatFmtOne);

    // Customize
	SetCustomGrid([FORM_CODE + '_CONGTY', FORM_CODE, FORM_CODE2],
                    [GrGroup, GrList, GrList2]);

    SetDictionary([QrList, QrCongty, QrList2],
                    [FORM_CODE, FORM_CODE + '_CONGTY', FORM_CODE2],
                    [Filter, nil, Filter2]);

    CmdReload.Execute;

    // Go ahead
	GrGroup.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdDmvtBTPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_BTP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtBTP, FrmPOM_DmvtBTP);
    if FrmPOM_DmvtBTP.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

//        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdDmvtNVLExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_NVL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtNVL, FrmPOM_DmvtNVL);
    if FrmPOM_DmvtNVL.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

//        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdDmvtTPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_TP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_Dmvt, FrmPOM_Dmvt);
    if FrmPOM_Dmvt.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

//        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.GetPeriod;
begin
	sysMon  := StrToInt(frMonthYear.CbMon.Text);
    sysYear := StrToInt(frMonthYear.CbYear.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdRefreshExecute(Sender: TObject);
var
	s, mSQL: String;
    mMon, mYear: Integer;
begin
    mMon := StrToInt(frMonthYear.CbMon.Text);
    mYear := StrToInt(frMonthYear.CbYear.Text);

	if	(sysMon  = mMon)  and
        (sysYear = mYear) then
		Exit;

    Wait(DATAREADING);
    if (mMon <> sysMon) or (mYear <> sysYear) then
    begin
        GetPeriod;
    end;

    with QrCongty do
    begin
		s := Sort;
    	Close;
        mSQL := ' 1=1';

        SQL.Text := sqlGroup + ' and ' + mSQL;
        Parameters.Refresh;
        Open;

        if s <> '' then
        	Sort := s;
    end;

    OpenTimeSheet;

    ClearWait;

    PgMainChange(PgMain);

    bClose := DataMain.GetPeriodStatus(sysMon, sysYear, 0);
    if bClose then
    	Msg(RS_POM_CLOSED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.OpenTimeSheet;
var
    s, mSQL: String;
begin
    with QrList do
    begin
		s := Sort;
    	Close;
        mSQL := ' and 1=1';

        SQL.Text := sqlList + mSQL;
        SQL.Add(' Order by MAVT');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;

        if s <> '' then
        	Sort := s;
    end;

    with QrList2 do
    begin
		s := Sort;
    	Close;

        SQL.Text := sqlList2;
        SQL.Add(' Order by MAVT');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;

        if s <> '' then
        	Sort := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        exSearch(Name + '_2', DsList2)
    else
        exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        Filter2.Execute
    else
        Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdUpdateExecute(Sender: TObject);
begin
    QrList.CheckBrowseMode;
	GetPeriod;

	if DataMain.POM_UpdateGiathanh(sysMon, sysYear) = 0 then
    begin
        CmdReload.Execute;
        MsgDone;
    end;
end;

{$HINTS OFF}
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.BtnXulyClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := $00CBDCE4;
            AFont.Color  := clBlack;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 1 then
        Status.Panels[0].Text := exRecordCount(QrList2, Filter)
    else
	    Status.Panels[0].Text := exRecordCount(QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.PS_DAILYAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdParamsExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('POM_BANG_THONGSO'); //NTD
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_BangThongso, FrmPOM_BangThongso);
    FrmPOM_BangThongso.Execute(R_FULL, 1, StrToInt(frMonthYear.cbMon.Text), StrToInt(frMonthYear.CbYear.Text));
    bClose := DataMain.POM_ThongsoTinhtrang(sysMon, sysYear, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdBOMBTPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_GIATHANH_BTP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmPOM_BangDinhmucBTP, FrmPOM_BangDinhmucBTP);
    FrmPOM_BangDinhmucBTP.Execute(r, StrToInt(frMonthYear.CbYear.Text), StrToInt(frMonthYear.cbMon.Text));

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdBOMTPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_GIATHANH_TP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmPOM_BangDinhmucTP, FrmPOM_BangDinhmucTP);
    FrmPOM_BangDinhmucTP.Execute(r, StrToInt(frMonthYear.CbYear.Text), StrToInt(frMonthYear.cbMon.Text));

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdChiphiChungExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_GIATHANH_CPC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmPOM_BangChiphi, FrmPOM_BangChiphi);
    FrmPOM_BangChiphi.Execute(r, StrToInt(frMonthYear.CbYear.Text), StrToInt(frMonthYear.cbMon.Text));

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdClearFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
    begin
        with Filter2 do
        begin
            FieldInfo.Clear;
            ClearFilter;
            ApplyFilter;
        end;
    end else
    begin
        with Filter do
        begin
            FieldInfo.Clear;
            ClearFilter;
            ApplyFilter;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, b1, b2: Boolean;
    n: Integer;
    bBrowseTime: Boolean;
begin
    n := PgMain.ActivePageIndex;
    CmdFilter.Enabled := (n=0);
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    b := mCanEdit and (not bClose);
    CmdUpdate.Enabled := b;

    with QrList do
    begin
        if not Active then
            Exit;
        b1 := IsEmpty;
        bBrowseTime := State in [dsBrowse];
    end;

    GrList.ReadOnly := b1;

    with QrList2 do
    begin
        if not Active then
            Exit;
        b2 := IsEmpty;
        bBrowseTime := State in [dsBrowse];
    end;

    GrList.ReadOnly := b2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdRepExecute(Sender: TObject);
var
	r: WORD;
begin
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('SanXuat') (*40*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrGroup then
    	GrList.SetFocus
	else if ActiveControl = GrList then
    	GrGroup.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.CmdReloadExecute(Sender: TObject);
begin
    QrEmp.Requery;

	sysMon := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.PgMainChange(Sender: TObject);
var
    s: String;
begin
    if not QrList.Active then
        Exit;

    Screen.Cursor := crSQLWait;
    QrList.Filter := 'MA_CONGTY=' + QuotedStr(QrCongty.FieldByName('MA_HOTRO').AsString);
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.QrList2BeforePost(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        //
    end;
    SetAudit(DataSet as TADOQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.QrListBeforePost(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    begin
        //
    end;
    SetAudit(DataSet as TADOQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.QrCongtyAfterScroll(DataSet: TDataSet);
begin
    if QrCongty.Tag <> 99 then
        PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.ApplicationEvents1Hint(Sender: TObject);
begin
	if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangSanluong.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	try
		QrList.CheckBrowseMode;
        CanClose := True;
    except
        CanClose := False;
    end;
end;

end.
