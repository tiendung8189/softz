﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_DmvtBTP_Dinhmuc;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid, wwdblook, Vcl.Menus,
  AdvMenus;

type
  TFrmPOM_DmvtBTP_Dinhmuc = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    QrDetail: TADOQuery;
    Image1: TImage;
    CmdAudit: TAction;
    QrDetailMAVT: TWideStringField;
    QrDetailMAVT_NVL: TWideStringField;
    QrDetailRSTT: TIntegerField;
    QrDetailLK_TENVT: TWideStringField;
    QrDetailLK_GIANHAP: TFloatField;
    QrDetailLK_DVT: TWideStringField;
    QrDMHH: TADOQuery;
    QrDetailLK_MADT: TWideStringField;
    QrDetailLK_TENDT: TWideStringField;
    QrDetailDINHMUC: TFloatField;
    QrDetailHAOHUT: TFloatField;
    QrDetailGIAVON: TFloatField;
    QrDetailTHANHTIEN_CHUA_HAOHUT: TFloatField;
    QrDetailTHANHTIEN: TFloatField;
    QrDetailGHICHU: TWideStringField;
    QrDetailLK_GIAVON: TFloatField;
    QrDetailDONGIA: TFloatField;
    CmdDmvtNPL: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    QrDetailCREATE_BY: TIntegerField;
    QrDetailUPDATE_BY: TIntegerField;
    QrDetailCREATE_DATE: TDateTimeField;
    QrDetailUPDATE_DATE: TDateTimeField;
    CmdImportExcel: TAction;
    PopDetail: TAdvPopupMenu;
    LydliutfileExcel1: TMenuItem;
    QrDetailCO: TWideStringField;
    QrDetailLK_CO: TWideStringField;
    ToolButton12: TToolButton;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDetailCalcFields(DataSet: TDataSet);
    procedure QrDetailMAVT_NVLChange(Sender: TField);
    procedure QrDetailDINHMUCChange(Sender: TField);
    procedure QrDetailTHANHTIENChange(Sender: TField);
    procedure QrDetailAfterPost(DataSet: TDataSet);
    procedure QrDetailAfterDelete(DataSet: TDataSet);
    procedure CmdDmvtNPLExecute(Sender: TObject);
    procedure QrDMHHAfterOpen(DataSet: TDataSet);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure GrListUpdateFooter(Sender: TObject);
  private
    mCanEdit: Boolean;
    mMavt: String;
  	mRet: Boolean;
  public
  	function Execute(pCanEdit: Boolean; pMa, pTen: String): Boolean;
  end;

var
  FrmPOM_DmvtBTP_Dinhmuc: TFrmPOM_DmvtBTP_Dinhmuc;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, isCommon, Rights, POM_DmvtNVL,
    ImportExcel, OfficeData, isFile, POM_DmvtBTP;

{$R *.DFM}

const
	FORM_CODE = 'POM_DM_BTP_DINHMUC';
    TABLE_NAME  = 'POM_DM_HH_DINHMUC';
    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPOM_DmvtBTP_Dinhmuc.Execute(pCanEdit: Boolean; pMa, pTen: String): Boolean;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;

    mMavt := pMa;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;

    ShowModal;

    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdNewExecute(Sender: TObject);
begin
	QrDetail.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdDmvtNPLExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_NVL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtNVL, FrmPOM_DmvtNVL);
    if FrmPOM_DmvtNVL.Execute(r, False) then
    begin
        QrDMHH.Requery;
        DataMain.QrPOM_DMHH.Requery;

        QrDetail.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrDetail, '', False, 'MAVT_NVL');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT_NVL') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrPOM_DMHH.Locate('MAVT', s, []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        if (not QrDetail.Locate('MAVT_NVL', s, []))  then
                        begin
                            QrDetail.Append;
                            QrDetail.FieldByName('MAVT_NVL').AsString := s
                        end;

                        for i := 0 to n do
                            if i <> k then
                                QrDetail.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        GrList.InvalidateCurrentRow;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);

var
    bBrowse, bEmpty: Boolean;
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);

    with QrDetail do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdDmvtNPL.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//	HideAudit;
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDetail, TABLE_NAME);
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.FormShow(Sender: TObject);
begin
//    with DataMain do
        OpenDataSets([QrDMHH]);

	OpenDataSets([QrDetail]);

    with (QrDetail) do
    begin
        SetShortDateFormat(QrDetail);
	    SetDisplayFormat(QrDetail, sysCurFmt);
        SetDisplayFormat(QrDetail, ['DONGIA', 'GIAVON'], sysPriceFmt);
	    SetDisplayFormat(QrDetail, ['DINHMUC', 'HAOHUT'], sysPerFmt);
    end;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDetail, FORM_CODE);

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.GrListUpdateFooter(Sender: TObject);
begin
    with GrList, FrmPOM_DmvtBTP.QrDMVT do
    begin
		ColumnByName('THANHTIEN').FooterValue :=
        	FormatFloat(sysFloatFmtTwo, FieldByName('CHIPHI_DM').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailBeforePost(DataSet: TDataSet);
begin
	with QrDetail do
	begin
		if BlankConfirm(QrDetail, ['MAVT_NVL']) then
			Abort;

        if FieldByName('LK_TENVT').AsString = '' then
        begin
            ErrMsg('Lỗi nhập liệu. Sai mã hàng hóa.');
            Abort;
        end;

		FieldByName('MAVT').AsString := mMavt;
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailTHANHTIENChange(Sender: TField);
begin
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDMHHAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(Dataset as TADOQuery, sysCurFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailAfterDelete(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailAfterPost(DataSet: TDataSet);
begin
    mRet := True;
    DataMain.POM_UpdateBTP(mMavt);
    exReSyncRecord(FrmPOM_DmvtBTP.QrDMVT);
    GrListUpdateFooter(GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := mMavt;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailCalcFields(DataSet: TDataSet);
begin
    with QrDetail do
        FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailDINHMUCChange(Sender: TField);
var
    dongia, dinhmuc, haohut, thanhtienchuaHH, thanhtien: Extended;
begin
    with QrDetail do
    begin
        dongia := FieldByName('DONGIA').AsFloat;
        dinhmuc := FieldByName('DINHMUC').AsFloat;
        haohut := FieldByName('HAOHUT').AsFloat;

        thanhtienchuaHH := exVNDRound(dongia * dinhmuc, sysCurPomRound);
        thanhtien := exVNDRound(thanhtienchuaHH + (thanhtienchuaHH * haohut/100), sysCurPomRound);

        FieldByName('THANHTIEN_CHUA_HAOHUT').AsFloat := thanhtienchuaHH;
        FieldByName('THANHTIEN').AsFloat := thanhtien;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_DmvtBTP_Dinhmuc.QrDetailMAVT_NVLChange(Sender: TField);
var
    x: Extended;
begin
    exDotMavt(2, QrDMHH, Sender);
    with QrDetail do
    begin
        x := FieldByName('LK_GIAVON').AsFloat;

        FieldByName('GIAVON').AsFloat := x;
        FieldByName('DONGIA').AsFloat := x;

        FieldByName('CO').AsString := FieldByName('LK_CO').AsString;
    end;
    GrList.InvalidateCurrentRow;
end;

end.
