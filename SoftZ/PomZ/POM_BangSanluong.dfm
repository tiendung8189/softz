object FrmPOM_BangSanluong: TFrmPOM_BangSanluong
  Left = 309
  Top = 169
  Caption = 'Gi'#225' Th'#224'nh'
  ClientHeight = 573
  ClientWidth = 1159
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = TntFormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 92
    Width = 1159
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitLeft = 12
    ExplicitWidth = 50
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1159
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 69
    Caption = 'ToolBar1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton5: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdUpdate
      ImageIndex = 30
    end
    object ToolButton8: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 77
      Top = 0
      Cursor = 1
      Action = CmdParams
      ImageIndex = 34
    end
    object ToolButton9: TToolButton
      Left = 146
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnXuly: TToolButton
      Left = 154
      Top = 0
      Cursor = 1
      Caption = 'Chi ti'#7871't'
      DropdownMenu = PopChitiet
      ImageIndex = 8
      ParentShowHint = False
      ShowHint = True
      Style = tbsDropDown
      OnClick = BtnXulyClick
    end
    object ToolButton3: TToolButton
      Left = 238
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 246
      Top = 0
      Cursor = 1
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 25
      Style = tbsDropDown
      OnClick = BtnXulyClick
    end
    object ToolButton12: TToolButton
      Left = 330
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 338
      Top = 0
      Cursor = 1
      Action = CmdRep
      ImageIndex = 4
    end
    object ToolButton1: TToolButton
      Left = 407
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 415
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
      ParentShowHint = False
      ShowHint = True
    end
  end
  inline frMonthYear: TfrMonthYear
    Left = 0
    Top = 44
    Width = 1159
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 44
    ExplicitWidth = 1159
    inherited Panel1: TPanel
      Width = 1159
      ExplicitWidth = 1159
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 1159
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end>
    UseSystemFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 94
    Width = 1159
    Height = 458
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object PgMain: TPageControl
      Left = 0
      Top = 0
      Width = 1159
      Height = 458
      ActivePage = TabSheet1
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnChange = PgMainChange
      object TabSheet1: TTabSheet
        Caption = ' S'#7843'n l'#432#7907'ng'
        object Splitter1: TSplitter
          Left = 0
          Top = 0
          Width = 1
          Height = 430
          ExplicitLeft = 275
          ExplicitHeight = 258
        end
        object GrList: TwwDBGrid2
          Left = 1
          Top = 0
          Width = 875
          Height = 430
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'LK_TENCA;CustomEdit;CbShift;F')
          Selected.Strings = (
            'MAVT'#9'13'#9'Code'#9'T'#9'Th'#224'nh ph'#7849'm'
            'MAVT_NVL_CHNH'#9'13'#9'Code'#9'T'#9'B'#225'n th'#224'nh ph'#7849'm'
            'SANLUONG'#9'10'#9'S'#7843'n l'#432#7907'ng'#9'F'
            'DUNGTICH'#9'10'#9'L'#237't'#9'F'#9'Dung t'#237'ch'
            'SANLUONG_DUNGTICH'#9'10'#9'S'#7843'n l'#432#7907'ng'#9'T'#9'Dung t'#237'ch'
            'HESO_DUNGTICH'#9'7'#9'H'#7879' s'#7889#9'T'#9'Quy '#273#7893'i dung t'#237'ch'
            'SANLUONG_HESO_DUNGTICH'#9'10'#9'S'#7843'n l'#432#7907'ng'#9'T'#9'Quy '#273#7893'i dung t'#237'ch'
            'HESO_DODAM'#9'7'#9'H'#7879' s'#7889#9'T'#9'Quy '#273#7893'i '#273#7897' '#273#7841'm'
            'SANLUONG_HESO_DODAM'#9'10'#9'S'#7843'n l'#432#7907'ng'#9'T'#9'Quy '#273#7893'i '#273#7897' '#273#7841'm'
            'CHIPHI_BAOBI'#9'10'#9#272#417'n v'#7883#9'T'#9'Chi ph'#237' bao b'#236
            'TONG_CHIPHI_BAOBI'#9'13'#9'T'#7893'ng'#9'T'#9'T'#7893'ng chi ph'#237
            'TONG_CHIPHI_CHEBIEN'#9'13'#9'Ch'#7871' bi'#7871'n'#9'T'#9'T'#7893'ng chi ph'#237
            'SOTIEN_HAOHUT'#9'8'#9'0.9%'#9'T'#9'Qu'#7929' b'#236'nh '#7893'n + hao h'#7909't'
            'TONG_SOTIEN_HAOHUT'#9'13'#9'T'#7893'ng'#9'T'#9'Qu'#7929' b'#236'nh '#7893'n + hao h'#7909't'
            'TONG_SOTIEN_QUYRD'#9'10'#9'1%'#9'T'#9'Qu'#7929' R&&D')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopList
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          LineColors.ShadowColor = clSilver
          OnCalcCellColors = GrListCalcCellColors
          OnEnter = CmdRefreshExecute
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = clWindow
        end
        object GrGroup: TwwDBGrid2
          Left = 876
          Top = 0
          Width = 275
          Height = 430
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'TEN_HOTRO'#9'35'#9'C'#244'ng ty'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alRight
          DataSource = DsCongty
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopGroup
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnEnter = CmdRefreshExecute
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = 14474460
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' '#272#417'n gi'#225' BTP'
        ImageIndex = 1
        object GrList2: TwwDBGrid2
          Left = 0
          Top = 0
          Width = 1151
          Height = 430
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'LK_TENCA;CustomEdit;CbShift;F')
          Selected.Strings = (
            'MAVT'#9'13'#9'Code'#9'T'#9'B'#225'n th'#224'nh ph'#7849'm'
            'LK_TENVT'#9'30'#9'T'#234'n'#9'T'#9'B'#225'n th'#224'nh ph'#7849'm'
            'THETICH_THUHOI'#9'10'#9'thu h'#7891'i'#9'F'#9'Th'#7875' t'#237'ch'
            'TONG_CHIPHI_NVL'#9'13'#9'T'#7893'ng'#9'T'#9'Chi ph'#237' NVL'
            'CHIPHI_NVL_BQ'#9'11'#9'BQ 1L'#9'T'#9'Chi ph'#237' NVL'
            'SOTIEN_QUYRD'#9'7'#9'1%'#9'T'#9'Qu'#7929' R&&D'
            'CHIPHI_NVL'#9'10'#9'NVL'#9'T'#9'Chi ph'#237
            'HESO_DODAM'#9'10'#9'H'#7879' s'#7889#9'T'#9'Q'#272' '#273#7897' '#273#7841'm'
            'CHIPHI_CHEBIEN_BQ'#9'13'#9'BQ 1L'#9'T'#9'Chi ph'#237' ch'#7871' bi'#7871'n'
            'GIATHANH_BTP_BQ'#9'13'#9'BQ 1L'#9'T'#9'Gi'#225' th'#224'nh BTP')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsList2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
          ParentFont = False
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          LineColors.ShadowColor = clSilver
          OnCalcCellColors = GrListCalcCellColors
          OnEnter = CmdRefreshExecute
          PadColumnStyle = pcsPadHeader
          PaintOptions.AlternatingRowColor = clWindow
        end
      end
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 36
    Top = 388
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = '1'
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Category = '1'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdCalc1: TAction
      Category = '1'
      Caption = 'T'#7893'ng h'#7907'p c'#244'ng th'#225'ng'
      ShortCut = 16502
    end
    object CmdParams: TAction
      Category = 'XULY'
      Caption = 'Th'#244'ng s'#7889
      OnExecute = CmdParamsExecute
    end
    object CmdClearFilter: TAction
      Category = '1'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdRep: TAction
      Caption = 'B'#225'o c'#225'o'
      Hint = 'C'#225'c b'#225'o c'#225'o'
      OnExecute = CmdRepExecute
    end
    object CmdSwitch: TAction
      ShortCut = 117
      Visible = False
      OnExecute = CmdSwitchExecute
    end
    object CmdDetail: TAction
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdUpdate: TAction
      Category = 'XULY'
      Caption = 'C'#7853'p nh'#7853't'
      Hint = 'Th'#234'm m'#7899'i/C'#7853'p nh'#7853't danh s'#225'ch...'
      ShortCut = 120
      OnExecute = CmdUpdateExecute
    end
    object CmdChiphiChung: TAction
      Category = 'XULY'
      Caption = 'Chi ph'#237' chung'
      OnExecute = CmdChiphiChungExecute
    end
    object CmdBOMTP: TAction
      Category = 'XULY'
      Caption = #272#7883'nh m'#7913'c th'#224'nh ph'#7849'm'
      OnExecute = CmdBOMTPExecute
    end
    object CmdBOMBTP: TAction
      Category = 'XULY'
      Caption = #272#7883'nh m'#7913'c b'#225'n th'#224'nh ph'#7849'm'
      OnExecute = CmdBOMBTPExecute
    end
    object CmdDmvtBTP: TAction
      Category = 'DANHMUC'
      Caption = 'B'#225'n th'#224'nh ph'#7849'm'
      OnExecute = CmdDmvtBTPExecute
    end
    object CmdDmvtNVL: TAction
      Category = 'DANHMUC'
      Caption = 'Nguy'#234'n v'#7853't li'#7879'u'
      OnExecute = CmdDmvtNVLExecute
    end
    object CmdDmvtTP: TAction
      Category = 'DANHMUC'
      Caption = 'Th'#224'nh ph'#7849'm'
      OnExecute = CmdDmvtTPExecute
    end
    object CmdClear: TAction
      Category = '1'
      Caption = 'X'#243'a to'#224'n b'#7897' d'#7919' li'#7879'u tr'#234'n l'#432#7899'i'
    end
  end
  object QrCongty: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = QrCongtyAfterScroll
    Parameters = <>
    SQL.Strings = (
      'select * from V_POM_DM_CONGTY'
      'where 1=1')
    Left = 996
    Top = 220
    object QrCongtyMANHOM_HOTRO: TWideStringField
      FieldName = 'MANHOM_HOTRO'
      Size = 50
    end
    object QrCongtyMA_HOTRO: TWideStringField
      FieldName = 'MA_HOTRO'
    end
    object QrCongtyTEN_HOTRO: TWideStringField
      FieldName = 'TEN_HOTRO'
      Size = 200
    end
  end
  object DsCongty: TDataSource
    DataSet = QrCongty
    Left = 996
    Top = 252
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrListBeforePost
    Parameters = <
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      'from'#9'POM_BANG_SANLUONG'
      'where'#9'THANG = :THANG'
      '   and'#9'NAM = :NAM')
    Left = 172
    Top = 260
    object QrListNAM: TIntegerField
      FieldName = 'NAM'
    end
    object QrListTHANG: TIntegerField
      FieldName = 'THANG'
    end
    object QrListMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrListMAVT_CHITIET_CHINH: TWideStringField
      FieldName = 'MAVT_CHITIET_CHINH'
      Size = 15
    end
    object QrListSANLUONG: TFloatField
      FieldName = 'SANLUONG'
    end
    object QrListMA_DUNGTICH: TWideStringField
      FieldName = 'MA_DUNGTICH'
    end
    object QrListDUNGTICH: TFloatField
      FieldName = 'DUNGTICH'
    end
    object QrListSANLUONG_DUNGTICH: TFloatField
      FieldName = 'SANLUONG_DUNGTICH'
    end
    object QrListHESO_DUNGTICH: TFloatField
      FieldName = 'HESO_DUNGTICH'
    end
    object QrListSANLUONG_HESO_DUNGTICH: TFloatField
      FieldName = 'SANLUONG_HESO_DUNGTICH'
    end
    object QrListMA_DODAM: TWideStringField
      FieldName = 'MA_DODAM'
    end
    object QrListHESO_DODAM: TFloatField
      FieldName = 'HESO_DODAM'
    end
    object QrListSANLUONG_HESO_DODAM: TFloatField
      FieldName = 'SANLUONG_HESO_DODAM'
    end
    object QrListCHIPHI_BAOBI: TFloatField
      FieldName = 'CHIPHI_BAOBI'
    end
    object QrListTONG_CHIPHI_BAOBI: TFloatField
      FieldName = 'TONG_CHIPHI_BAOBI'
    end
    object QrListTL_HAOHUT: TFloatField
      FieldName = 'TL_HAOHUT'
    end
    object QrListSOTIEN_HAOHUT: TFloatField
      FieldName = 'SOTIEN_HAOHUT'
    end
    object QrListTONG_SOTIEN_HAOHUT: TFloatField
      FieldName = 'TONG_SOTIEN_HAOHUT'
    end
    object QrListCHIPHI_NVL: TFloatField
      FieldName = 'CHIPHI_NVL'
    end
    object QrListTONG_CHIPHI_NVL: TFloatField
      FieldName = 'TONG_CHIPHI_NVL'
    end
    object QrListSOTIEN_QUYRD: TFloatField
      FieldName = 'SOTIEN_QUYRD'
    end
    object QrListTONG_SOTIEN_QUYRD: TFloatField
      FieldName = 'TONG_SOTIEN_QUYRD'
    end
    object QrListCHIPHI_CHEBIEN: TFloatField
      FieldName = 'CHIPHI_CHEBIEN'
    end
    object QrListTONG_CHIPHI_CHEBIEN: TFloatField
      FieldName = 'TONG_CHIPHI_CHEBIEN'
    end
    object QrListGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrListCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrListUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrListCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrListUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrListROWGUID: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object QrListMA_CONGTY: TWideStringField
      FieldName = 'MA_CONGTY'
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 172
    Top = 292
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Name'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'LK_TENNV'
      'MANV'
      'TENPB')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdDayMonthYear
    SQLTables = <>
    Left = 68
    Top = 452
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'MANV, TENNV'
      '  from'#9'HR_DM_NHANVIEN'
      'where'#9'isnull([COMAT_BANGCONG], 0) <> 0'
      'order by  MANV')
    Left = 356
    Top = 204
  end
  object PopChitiet: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 284
    Top = 332
    object Cpnhtdliutdliucngth1: TMenuItem
      Action = CmdChiphiChung
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Tbsungdliuqunqutth1: TMenuItem
      Action = CmdBOMBTP
    end
    object Kimtratnhngcadliu1: TMenuItem
      Action = CmdBOMTP
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 36
    Top = 324
  end
  object PopGroup: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 164
    Top = 356
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Item3: TMenuItem
      Caption = 'X'#243'a d'#7919' li'#7879'u gi'#7901' c'#244'ng'
    end
  end
  object PopList: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 164
    Top = 404
    object MenuItem1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
  end
  object TS_RECORDER_LOG: TADOCommand
    CommandText = 'spHR_CAPNHAT_DULIEU_CONG;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TUNGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DENNGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 112
    Top = 184
  end
  object TS_PAYROLL_PERIOD: TADOCommand
    CommandText = 'spHR_THONGSO_CHUKY;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNAM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTHANG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTU'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@pDEN'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 192
    Top = 184
  end
  object spHR_XOACONG: TADOCommand
    CommandText = 'spHR_XOACONG;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNAM'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTHANG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 272
    Top = 184
  end
  object QrList2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforePost = QrList2BeforePost
    Parameters = <
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      'from'#9'POM_BANG_DONGIA_BTP'
      'where'#9'THANG = :THANG'
      '   and'#9'NAM = :NAM')
    Left = 228
    Top = 260
    object IntegerField1: TIntegerField
      FieldName = 'NAM'
    end
    object IntegerField2: TIntegerField
      FieldName = 'THANG'
    end
    object WideStringField6: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object IntegerField3: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object IntegerField4: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object GuidField1: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object QrList2MAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrList2CONGTY: TWideStringField
      FieldName = 'CONGTY'
      Size = 70
    end
    object QrList2THETICH_THUHOI: TFloatField
      FieldName = 'THETICH_THUHOI'
    end
    object QrList2TONG_CHIPHI_NVL: TFloatField
      FieldName = 'TONG_CHIPHI_NVL'
    end
    object QrList2CHIPHI_NVL_BQ: TFloatField
      FieldName = 'CHIPHI_NVL_BQ'
    end
    object QrList2TL_QUYRD: TFloatField
      FieldName = 'TL_QUYRD'
    end
    object QrList2SOTIEN_QUYRD: TFloatField
      FieldName = 'SOTIEN_QUYRD'
    end
    object QrList2CHIPHI_NVL: TFloatField
      FieldName = 'CHIPHI_NVL'
    end
    object QrList2MA_DODAM: TWideStringField
      FieldName = 'MA_DODAM'
    end
    object QrList2HESO_DODAM: TFloatField
      FieldName = 'HESO_DODAM'
    end
    object QrList2CHIPHI_CHEBIEN_BQ: TFloatField
      FieldName = 'CHIPHI_CHEBIEN_BQ'
    end
    object QrList2GIATHANH_BTP_BQ: TFloatField
      FieldName = 'GIATHANH_BTP_BQ'
    end
    object QrList2LK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrPOM_DM_BTP
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
  end
  object DsList2: TDataSource
    DataSet = QrList2
    Left = 228
    Top = 292
  end
  object Filter2: TwwFilterDialog2
    DataSource = DsList2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Name'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'LK_TENNV'
      'MANV'
      'TENPB')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdDayMonthYear
    SQLTables = <>
    Left = 156
    Top = 452
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 284
    Top = 372
    object MenuItem2: TMenuItem
      Action = CmdDmvtNVL
    end
    object MenuItem4: TMenuItem
      Action = CmdDmvtBTP
    end
    object MenuItem5: TMenuItem
      Action = CmdDmvtTP
    end
  end
end
