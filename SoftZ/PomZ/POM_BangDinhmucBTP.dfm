object FrmPOM_BangDinhmucBTP: TFrmPOM_BangDinhmucBTP
  Left = 622
  Top = 216
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #272#7883'nh M'#7913'c B'#225'n Th'#224'nh Ph'#7849'm'
  ClientHeight = 762
  ClientWidth = 1316
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1316
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 57
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object TntToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdCalc
      ImageIndex = 30
    end
    object TntToolButton2: TToolButton
      Left = 57
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
      Visible = False
    end
    object TntToolButton4: TToolButton
      Left = 65
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object ToolButton2: TToolButton
      Left = 122
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton1: TToolButton
      Left = 130
      Top = 0
      Cursor = 1
      Action = CmdDmvtNVL
      ImageIndex = 27
    end
    object ToolButton6: TToolButton
      Left = 187
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 195
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 741
    Width = 1316
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 77
    Width = 1316
    Height = 664
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 832
      Height = 664
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'RSTT'#9'3'#9'STT'#9'F'
        'MAVT_NVL'#9'15'#9'M'#227#9'T'#9'Nguy'#234'n v'#7853't li'#7879'u'
        'LK_TENVT'#9'35'#9'T'#234'n'#9'T'#9'Nguy'#234'n v'#7853't li'#7879'u'
        'LK_DVT'#9'7'#9#272'vt'#9'T'#9'Nguy'#234'n v'#7853't li'#7879'u'
        'LK_CO'#9'6'#9'CO'#9'T'#9'Nguy'#234'n v'#7853't li'#7879'u'
        'DINHMUC'#9'10'#9#272#7883'nh m'#7913'c'#9'F'
        'DONGIA'#9'11'#9#272#417'n gi'#225#9'F'
        'THANHTIEN'#9'13'#9'Th'#224'nh ti'#7873'n'#9'T'
        'GHICHU'#9'35'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 1
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      GroupFieldName = 'LK_TENCP_NHOM'
    end
    object GrGroup: TwwDBGrid2
      Left = 832
      Top = 0
      Width = 484
      Height = 664
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'MAVT'#9'15'#9'M'#227#9'F'#9'B'#225'n th'#224'nh ph'#7849'm'
        'LK_TENVT'#9'35'#9'T'#234'n'#9'F'#9'B'#225'n th'#224'nh ph'#7849'm'
        'CHIPHI_NVL'#9'13'#9'Nguy'#234'n v'#7853't li'#7879'u'#9'F'#9'Chi ph'#237)
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alRight
      DataSource = DsGroup
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 14474460
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 1316
    Height = 41
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object Label4: TLabel
      Left = 45
      Top = 12
      Width = 41
      Height = 16
      Alignment = taRightJustify
      Caption = 'Th'#225'ng:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbMA: TLabel
      Left = 92
      Top = 12
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 158
      Top = 12
      Width = 31
      Height = 16
      Alignment = taRightJustify
      Caption = 'N'#259'm:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbTEN: TLabel
      Left = 196
      Top = 12
      Width = 40
      Height = 16
      Caption = 'HOTEN'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 48
    Top = 132
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdCalc: TAction
      Caption = 'C'#7853'p nh'#7853't'
      ShortCut = 116
      OnExecute = CmdCalcExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      ShortCut = 16464
      Visible = False
      OnExecute = CmdPrintExecute
    end
    object CmdDmvtNVL: TAction
      Caption = 'NVL'
      OnExecute = CmdDmvtNVLExecute
    end
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = QrListBeforeOpen
    BeforeInsert = QrListBeforeInsert
    BeforeEdit = QrListBeforeEdit
    BeforePost = QrListBeforePost
    BeforeDelete = QrListBeforeDelete
    OnCalcFields = QrListCalcFields
    OnDeleteError = QrListPostError
    OnEditError = QrListPostError
    OnPostError = QrListPostError
    Parameters = <
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'POM_BANG_DINHMUC_BTP'
      ' where '#9'NAM = :NAM'
      '    and '#9'THANG = :THANG'
      'order by '#9'SORT')
    Left = 48
    Top = 164
    object QrListRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrListNAM: TIntegerField
      FieldName = 'NAM'
    end
    object QrListTHANG: TIntegerField
      FieldName = 'THANG'
    end
    object QrListMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 30
    end
    object QrListMAVT_NVL: TWideStringField
      FieldName = 'MAVT_NVL'
      Size = 30
    end
    object QrListDINHMUC: TFloatField
      FieldName = 'DINHMUC'
      OnChange = QrListDINHMUCChange
    end
    object QrListDONGIA: TFloatField
      FieldName = 'DONGIA'
      OnChange = QrListDINHMUCChange
    end
    object QrListTHANHTIEN: TFloatField
      FieldName = 'THANHTIEN'
    end
    object QrListGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrListCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrListUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrListCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrListUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrListROWGUID: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object QrListLK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrPOM_DMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT_NVL'
      Size = 200
      Lookup = True
    end
    object QrListLK_DVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DVT'
      LookupDataSet = DataMain.QrPOM_DMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DVT'
      KeyFields = 'MAVT_NVL'
      Size = 200
      Lookup = True
    end
    object QrListLK_CO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CO'
      LookupDataSet = DataMain.QrPOM_DMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'CO'
      KeyFields = 'MAVT_NVL'
      Size = 200
      Lookup = True
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 48
    Top = 196
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAVT'
      'TENVT'
      'DVT'
      'SL'
      'DG'
      'ST'
      'TENNHOM'
      'MANHOM')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 273
    Top = 134
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 218
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 304
    Top = 218
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrGroup: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrGroupBeforeOpen
    AfterScroll = QrGroupAfterScroll
    Parameters = <
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'POM_BANG_DONGIA_BTP'
      ' where '#9'NAM = :NAM'
      '    and '#9'THANG = :THANG'
      'order by '#9'MAVT')
    Left = 1076
    Top = 220
    object QrGroupNAM: TIntegerField
      FieldName = 'NAM'
    end
    object QrGroupTHANG: TIntegerField
      FieldName = 'THANG'
    end
    object QrGroupMAVT: TWideStringField
      FieldName = 'MAVT'
      Size = 15
    end
    object QrGroupLK_TENVT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENVT'
      LookupDataSet = DataMain.QrPOM_DMHH
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrGroupCHIPHI_NVL: TFloatField
      FieldName = 'CHIPHI_NVL'
    end
    object QrGroupTONG_CHIPHI_NVL: TFloatField
      FieldName = 'TONG_CHIPHI_NVL'
    end
  end
  object DsGroup: TDataSource
    DataSet = QrGroup
    Left = 1076
    Top = 252
  end
end
