﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_BangThongso;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid, ADODb,
  AppEvnts, Menus, AdvMenus, wwDBGrid2, DBCtrls, Grids, Wwdbigrd, ToolWin,
  wwdblook;

type
  TFrmPOM_BangThongso = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    Ds1: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Qr1: TADOQuery;
    ToolButton8: TToolButton;
    Qr1NAM: TIntegerField;
    Qr1CREATE_BY: TIntegerField;
    Qr1UPDATE_BY: TIntegerField;
    Qr1CREATE_DATE: TDateTimeField;
    Qr1UPDATE_DATE: TDateTimeField;
    GrList: TwwDBGrid2;
    PopCommon: TAdvPopupMenu;
    CmdCountWD: TAction;
    Tnhsngycngthct1: TMenuItem;
    COUNT_WD: TADOCommand;
    CmdReload: TAction;
    PaPsComment: TPanel;
    PDComment: TGroupBox;
    EdComment: TDBMemo;
    Qr1GHICHU: TWideStringField;
    Qr1TUNGAY: TDateTimeField;
    Qr1DENNGAY: TDateTimeField;
    CmdRefresh: TAction;
    Qr1THANG: TIntegerField;
    Qr1KHOASO: TBooleanField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Qr1BeforePost(DataSet: TDataSet);
    procedure Qr1BeforeDelete(DataSet: TDataSet);
    procedure Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure Qr1BeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdCountWDExecute(Sender: TObject);
    procedure Qr1AfterInsert(DataSet: TDataSet);
    procedure Qr1NAMChange(Sender: TField);
    procedure CmdReloadExecute(Sender: TObject);
    procedure Qr1BeforeEdit(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
  private
    mMonth, mYear: Integer;
  	mCanEdit, mTrigger: Boolean;
    d1, d2: TDateTime;
    i1: Integer;
    fSearch: string;
  public
  	procedure Execute(r: WORD; pType: Integer; pMonth: Integer = 0; pYear: Integer = 0);
  end;

var
  FrmPOM_BangThongso: TFrmPOM_BangThongso;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, Variants, MainData, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'POM_BANG_THONGSO';
    TABLE_NAME = FORM_CODE;
    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Execute;
begin
	mCanEdit := rCanEdit(r);

    mMonth := pMonth;
    mYear  := pYear;

    EdComment.ReadOnly := not mCanEdit;
	ShowModal;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdNewExecute(Sender: TObject);
begin
	with Qr1 do
    begin
    	Append;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdSaveExecute(Sender: TObject);
begin
	Qr1.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdCancelExecute(Sender: TObject);
begin
	Qr1.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdDelExecute(Sender: TObject);
begin
	Qr1.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([Qr1]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    mTrigger := False;
    AddAllFields(Qr1, TABLE_NAME);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    with DataMain do
    begin
//        OpenDataSets([QrDM_HOCKY]);
    end;

    OpenDataSets([Qr1]);

    with Qr1 do
    begin
        SetShortDateFormat(Qr1);
	    SetDisplayFormat(Qr1, sysCurFmt);
    end;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(Qr1, FORM_CODE);

    if not Qr1.Locate('NAM;THANG', VarArrayOf([mYear, mMonth]), []) then
        Qr1.Last;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, Qr1, mCanEdit);
    CmdCountWD.Enabled := (not Qr1.IsEmpty) and mCanEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(Qr1, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1BeforePost(DataSet: TDataSet);
begin
	if BlankConfirm(DataSet, ['THANG', 'NAM']) then
		Abort;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1BeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

//    if Qr1.FieldByName('KHOASO').AsBoolean then
//        Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1BeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

//    if Qr1.FieldByName('KHOASO').AsBoolean then
//        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1BeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    with Qr1 do
    begin
        if IsEmpty then
        begin
            mMonth := sysMon;
            mYear  := sysYear;
        end
        else
        begin
            Last;
	        mMonth := FieldByName('THANG').AsInteger;
    	    mYear := FieldByName('NAM').AsInteger;
		end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(Qr1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_MONTH = 'Kỳ không hợp lệ.';

procedure TFrmPOM_BangThongso.CmdCountWDExecute(Sender: TObject);
var
	m, y, n: Integer;
begin
	with Qr1 do
    begin
		m := FieldByName('THANG').AsInteger;
        if (m < 1) then
        begin
        	Msg(RS_INVALID_MONTH);
        	Exit;
        end;
		y := FieldByName('NAM').AsInteger;
    end;

    with COUNT_WD do
    begin
    	Prepared := True;
        Parameters[1].Value := y;
        Parameters[2].Value := m;
        Execute;
        n := Parameters[0].Value;
    end;

	with Qr1 do
    begin
    	if State in [dsBrowse] then
        	Edit;
//		FieldByName('KHOANGCACH_TUAN').AsInteger := n;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1AfterInsert(DataSet: TDataSet);
begin
    with Qr1 do
    begin
        Inc(mMonth);
        if mMonth > 12 then
        begin
            mMonth := 1;
            Inc(mYear);
        end;
        FieldByName('THANG').AsInteger := mMonth;
        FieldByName('NAM').AsInteger := mYear;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.Qr1NAMChange(Sender: TField);
var
    dd, mm, yy: WORD;
    firstDate, lastDate: TDateTime;
begin
    with Qr1 do
    begin
        mm := FieldByName('THANG').AsInteger;
        yy := FieldByName('NAM').AsInteger;
        if not ((mm > 0) and (yy > 0)) then
            Exit;

        firstDate := EncodeDate(yy, mm, 1);
        lastDate := IncMonth(firstDate) - 1;

        FieldByName('TUNGAY').AsDateTime := firstDate;
        FieldByName('DENNGAY').AsDateTime := lastDate;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdRefreshExecute(Sender: TObject);
var
    s: String;
begin
    if fSearch <> '' then
    begin
        fSearch := '';
        with Qr1 do
        begin
            s := Sort;
            Close;
            Open;

            if s <> '' then
                Sort := s;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangThongso.CmdReloadExecute(Sender: TObject);
begin
    fSearch := '~';
    CmdRefresh.Execute;
end;

end.

