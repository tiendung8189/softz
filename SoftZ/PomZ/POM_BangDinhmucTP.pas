﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit POM_BangDinhmucTP;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin;

type
  TFrmPOM_BangDinhmucTP = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    CmdRefresh: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    TntToolButton4: TToolButton;
    QrListRSTT: TIntegerField;
    QrListNAM: TIntegerField;
    QrListTHANG: TIntegerField;
    QrListMAVT: TWideStringField;
    QrListMAVT_NVL: TWideStringField;
    QrListDINHMUC: TFloatField;
    QrListDONGIA: TFloatField;
    QrListTHANHTIEN: TFloatField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrListLK_TENVT: TWideStringField;
    QrListLK_DVT: TWideStringField;
    QrListLK_CO: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    CmdDmvtNVL: TAction;
    CmdDmvtBTP: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    QrGroup: TADOQuery;
    QrGroupNAM: TIntegerField;
    QrGroupTHANG: TIntegerField;
    DsGroup: TDataSource;
    GrGroup: TwwDBGrid2;
    QrGroupMAVT: TWideStringField;
    QrGroupCHIPHI_BAOBI: TFloatField;
    QrGroupLK_TENVT: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure QrListPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMANotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrListBeforeEdit(DataSet: TDataSet);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListBeforeInsert(DataSet: TDataSet);
    procedure CmdDmvtNVLExecute(Sender: TObject);
    procedure CmdDmvtBTPExecute(Sender: TObject);
    procedure QrGroupBeforeOpen(DataSet: TDataSet);
    procedure QrGroupAfterScroll(DataSet: TDataSet);
    procedure QrListDINHMUCChange(Sender: TField);
    procedure CmdRefreshExecute(Sender: TObject);
  private
  	mCanEdit, mBookClosed: Boolean;
  	mThang, mNam: Integer;

    procedure OpenQueries;
  public
  	procedure Execute(r: WORD; pNam, pThang: Integer);
  end;

var
  FrmPOM_BangDinhmucTP: TFrmPOM_BangDinhmucTP;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon,
  POM_DmvtBTP, POM_DmvtNVL;

{$R *.DFM}

const
	FORM_CODE = 'POM_BANG_DINHMUC_TP';
    TABLE_NAME  = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.Execute;
begin
    mThang := pThang;
    mNam := pNam;
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrList]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

    AddAllFields(QrList, TABLE_NAME);
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdDmvtBTPExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_BTP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtBTP, FrmPOM_DmvtBTP);
    if FrmPOM_DmvtBTP.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdDmvtNVLExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('POM_DM_NVL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtNVL, FrmPOM_DmvtNVL);
    if FrmPOM_DmvtNVL.Execute(r, False) then
    begin
        DataMain.QrPOM_DMHH.Requery;

        QrList.Requery;
    end;
    GrList.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListBeforeOpen(DataSet: TDataSet);
begin
    with QrList do
    begin
        Parameters[0].Value := mNam;
        Parameters[1].Value := mThang;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.FormShow(Sender: TObject);
begin
    LbMA.Caption := IntToStr(mThang);
    LbTEN.Caption := IntToStr(mNam);

    with DataMain do
        OpenDataSets([QrPOM_DM_CHIPHI, QrPOM_DM_CHIPHI_NHOM]);

    SetDisplayFormat(QrList, sysCurFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_GROUP'], [GrList, GrGroup]);
    SetDictionary([QrList, QrGroup], [FORM_CODE, FORM_CODE + '_GROUP'], [Filter, nil]);

    OpenQueries;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.OpenQueries;
begin
    with QrList do
    begin
        Close;
        Open;
    end;

    with QrGroup do
    begin
        Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListBeforePost(DataSet: TDataSet);
begin
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrGroupAfterScroll(DataSet: TDataSet);
var
    s: String;
begin
    if not QrList.Active then
        Exit;

    Screen.Cursor := crSQLWait;
    QrList.Filter := 'MAVT=' + QuotedStr(QrGroup.FieldByName('MAVT').AsString);
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrGroupBeforeOpen(DataSet: TDataSet);
begin
    with QrGroup do
    begin
        Parameters[0].Value := mNam;
        Parameters[1].Value := mThang;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListBeforeDelete(DataSet: TDataSet);
begin
//    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdSaveExecute(Sender: TObject);
begin
	QrList.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdCancelExecute(Sender: TObject);
begin
	QrList.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not QrList.IsEmpty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListBeforeEdit(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    if mBookClosed then
    begin
	    ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListBeforeInsert(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdRefreshExecute(Sender: TObject);
begin
    OpenQueries;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdReReadExecute(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CbMANotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdCalcExecute(Sender: TObject);
begin
	if DataMain.POM_UpdateGiathanh(sysMon, sysYear) <> 0 then
    begin
    	ErrMsg(Format(RS_POM_GEN_ERROR, [sysMon, sysYear]));
		Exit;
    end;
    CmdReRead.Execute;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListCalcFields(DataSet: TDataSet);
begin
   with QrList do
   	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPOM_BangDinhmucTP.QrListDINHMUCChange(Sender: TField);
var
    dinhmuc, dongia, thanhtien: Double;
begin
    with QrList do
    begin
        dinhmuc :=  FieldByName('DINHMUC').AsFloat;
        dongia :=  FieldByName('DONGIA').AsFloat;

        thanhtien := dinhmuc * dongia;

        FieldByName('THANHTIEN').AsFloat := thanhtien;
    end;

    GrList.InvalidateCurrentRow;
end;

end.
