object FrmPOM_BangChiphi: TFrmPOM_BangChiphi
  Left = 622
  Top = 216
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Chi Ph'#237' Chung'
  ClientHeight = 574
  ClientWidth = 1085
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000000040000000000000000000000000000000000000000
    0000C57A2A4DC0782678BA73219FB46D1CC0AF6717DAAB6414EEAA6413FBAA64
    13FBAA6213EEA96013DAA85C13C0A758149FA5531478A44D144D000000000000
    0000AA6413FFBC7323FFC27828FFC4792BFFC5782CFFBF6927FFB55820FFB155
    1DFFAC501AFFA74E17FFA54D15FFA44F14FFA55413FFAA6413FF000000000000
    0000C073274DB1671BE5B86527FFBA6628FFC3702CFFBF6928FFE1BAA0FFFFFF
    FFFFAC511CFFA24519FF9F4117FFA24617FFA75A14E5A44D144D000000000000
    000000000000AB6414ECB45E24FFBC6629FFCB7E3DFFF7ECE3FFFFFFFFFFFAF5
    F1FFCD916BFFAB511EFFA24619FFA44818FFAA6213EC00000000000000000000
    000000000000AA6413FDC7702DFFC8752FFFCB7831FFD28F58FFC26F2CFFC87F
    46FFFFFFFFFFBF7341FFAF561FFFAB511CFFAA6413FD00000000000000000000
    000000000000AC6515ECCA7930FFCE7D34FFCD7D33FFCB7B33FFC87731FFD498
    65FFFFFFFFFFBF7136FFB76124FFB25C20FFAA6314EC00000000000000000000
    000000000000B1691ACDCA7D30FFD28438FFD18538FFD99C5FFFF5E6D7FFFFFF
    FFFFDCAC82FFC16F2CFFBD6828FFB66221FFAC6216CD00000000000000000000
    000000000000B87021A2C57C2CFFD68C3CFFD68D3DFFFCF7F2FFEDD0B0FFD18A
    44FFCB7C34FFC77630FFC26F2CFFB76720FFB0631AA200000000000000000000
    000000000000C27B2A6EBD7726FFD99340FFDA9441FFFFFFFFFFE1AD71FFD38A
    3BFFD59352FFCB7C33FFC7752FFFB4681DFFB5651E6E00000000000000000000
    000000000000CF873632B36E1CEBD99540FFDD9B45FFE8BB82FFFDF8F2FFFFFF
    FFFFF6E7D8FFCF8136FFC8772FFFAF6718EB12F7FEFF00000000000000000000
    00000000000000000000BF7A2893C98530FFE0A048FFDF9E46FFFFFFFFFFEFD1
    ACFFD58C3CFFD18438FFBE7225FF12F7FEFF0282BCFF00000000000000000000
    00000000000000000000D28F3A38B6711FCED7983FFFE0A048FFDD9B45FFDA94
    41FFD68D3DFFCB7F32FF12F7FEFF0282BCFF0000000000000000000000000000
    0000000000000000000000000000CD8A3557B87421D70282BCFF0282BCFF0282
    BCFF0282BCFF0282BCFF0282BCFF000000000000000000000000000000000000
    0000000000000000000000000000000000000282BCFF12F7FEFF12F7FEFF12F7
    FEFF12F7FEFF0282BCFF00000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000BE7827FFDCA464FFBD74
    22FFB56E1EF60000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000BD7625FFB56C1FFFB56C1FFFB56B
    1DFFB46A1CFFB46A1CF600000000000000000000000000000000000000008001
    00008001000080010000C0030000C0030000C0030000C0030000C0030000C003
    0000C0030000E0030000E0070000F00F0000F81F0000FC3F0000F81F0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1085
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 71
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    ExplicitWidth = 1086
    object TntToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdCalc
      ImageIndex = 30
    end
    object TntToolButton2: TToolButton
      Left = 71
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object ToolButton1: TToolButton
      Left = 79
      Top = 0
      Cursor = 1
      Action = CmdDmChiphi
      ImageIndex = 25
    end
    object ToolButton2: TToolButton
      Left = 150
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtIn: TToolButton
      Left = 158
      Top = 0
      Cursor = 1
      Hint = 'In phi'#7871'u'
      Caption = 'In'
      DropdownMenu = PopIn
      ImageIndex = 38
      Style = tbsDropDown
      OnClick = BtInClick
    end
    object ToolButton6: TToolButton
      Left = 244
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 252
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 553
    Width = 1085
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitTop = 552
    ExplicitWidth = 1086
  end
  object Panel2: TPanel
    Left = 0
    Top = 85
    Width = 1085
    Height = 468
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1086
    ExplicitHeight = 467
    object PgMain: TPageControl
      Left = 0
      Top = 0
      Width = 1085
      Height = 468
      ActivePage = TabSheet2
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 1086
      ExplicitHeight = 467
      object TabSheet1: TTabSheet
        Caption = ' Chi ph'#237' chung'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GrList: TwwDBGrid2
          Left = 0
          Top = 0
          Width = 1078
          Height = 439
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'CODINH;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'LK_TENCP'#9'30'#9'N'#7897'i dung chi ph'#237#9'T'
            'SOTIEN'#9'13'#9'Chi ph'#237#9'F'#9'T'#7893'ng'
            'TAIKHOAN1'#9'20'#9'T'#224'i kho'#7843'n'#9'F'#9'T'#7893'ng'
            'SOTIEN1'#9'13'#9'PX1'#9'F'#9'Ph'#226'n b'#7893
            'SOTIEN2'#9'13'#9'PX2'#9'F'#9'Ph'#226'n b'#7893
            'CODINH'#9'5'#9't'#7915'ng th'#225'ng'#9'F'#9'C'#7889' '#273#7883'nh'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopupMenu1
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
          GroupFieldName = 'LK_TENCP_NHOM'
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Chi ph'#237' t'#7915'ng ph'#226'n x'#432#7903'ng'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 1078
        ExplicitHeight = 439
        object GrList2: TwwDBGrid2
          Left = 0
          Top = 0
          Width = 1077
          Height = 440
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'CODINH;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'LK_TENCP'#9'30'#9'N'#7897'i dung chi ph'#237#9'T'
            'SOTIEN1'#9'13'#9'S'#7889' ti'#7873'n'#9'F'#9'Ph'#226'n x'#432#7903'ng 1'
            'TAIKHOAN1'#9'12'#9'T'#224'i kho'#7843'n'#9'F'#9'Ph'#226'n x'#432#7903'ng 1'
            'SOTIEN2'#9'13'#9'S'#7889' ti'#7873'n'#9'F'#9'Ph'#226'n x'#432#7903'ng 2'
            'TAIKHOAN2'#9'12'#9'T'#224'i kho'#7843'n'#9'F'#9'Ph'#226'n x'#432#7903'ng 2'
            'SOTIEN'#9'13'#9'Chi ph'#237#9'T'#9'T'#7893'ng'
            'CODINH'#9'5'#9't'#7915'ng th'#225'ng'#9'F'#9'C'#7889' '#273#7883'nh'
            'GHICHU'#9'35'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsList2
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopupMenu1
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
          GroupFieldName = 'LK_TENCP_NHOM'
          ExplicitWidth = 1078
          ExplicitHeight = 439
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 44
    Width = 1085
    Height = 41
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    ExplicitWidth = 1086
    object Label4: TLabel
      Left = 45
      Top = 12
      Width = 41
      Height = 16
      Alignment = taRightJustify
      Caption = 'Th'#225'ng:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbMA: TLabel
      Left = 92
      Top = 12
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 158
      Top = 12
      Width = 31
      Height = 16
      Alignment = taRightJustify
      Caption = 'N'#259'm:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbTEN: TLabel
      Left = 196
      Top = 12
      Width = 40
      Height = 16
      Caption = 'HOTEN'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 48
    Top = 132
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdCalc: TAction
      Caption = 'C'#7853'p nh'#7853't'
      ShortCut = 116
      OnExecute = CmdCalcExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdDmChiphi: TAction
      Caption = 'Dm chi ph'#237
      OnExecute = CmdDmChiphiExecute
    end
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrListBeforeOpen
    BeforeInsert = QrListBeforeInsert
    BeforeEdit = QrListBeforeEdit
    BeforePost = QrListBeforePost
    BeforeDelete = QrListBeforeDelete
    OnCalcFields = QrListCalcFields
    OnDeleteError = QrListPostError
    OnEditError = QrListPostError
    OnPostError = QrListPostError
    Parameters = <
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'POM_BANG_CHIPHI_CHUNG'
      ' where '#9'NAM = :NAM'
      '    and '#9'THANG = :THANG'
      '    and'#9'PLOAI = '#39'01'#39
      'order by MACP')
    Left = 48
    Top = 164
    object QrListRSTT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrListNAM: TIntegerField
      FieldName = 'NAM'
    end
    object QrListTHANG: TIntegerField
      FieldName = 'THANG'
    end
    object QrListMACP: TWideStringField
      FieldName = 'MACP'
    end
    object QrListGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrListCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrListUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrListCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrListUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrListROWGUID: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object QrListLK_TENCP: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENCP'
      LookupDataSet = DataMain.QrPOM_DM_CHIPHI
      LookupKeyFields = 'MACP'
      LookupResultField = 'TENCP'
      KeyFields = 'MACP'
      Size = 200
      Lookup = True
    end
    object QrListTENCP_NHOM: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENCP_NHOM'
      LookupDataSet = DataMain.QrPOM_DM_CHIPHI_NHOM
      LookupKeyFields = 'MACP_NHOM'
      LookupResultField = 'TENCP_NHOM'
      KeyFields = 'MACP_NHOM'
      Size = 200
      Lookup = True
    end
    object QrListSOTIEN1: TFloatField
      FieldName = 'SOTIEN1'
    end
    object QrListSOTIEN2: TFloatField
      FieldName = 'SOTIEN2'
    end
    object QrListSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrListSOTIENChange
    end
    object QrListTAIKHOAN1: TWideStringField
      FieldName = 'TAIKHOAN1'
      OnChange = QrListTAIKHOAN1Change
    end
    object QrListTAIKHOAN2: TWideStringField
      FieldName = 'TAIKHOAN2'
    end
    object QrListCODINH: TBooleanField
      FieldName = 'CODINH'
    end
    object QrListMACP_NHOM: TWideStringField
      FieldName = 'MACP_NHOM'
    end
    object QrListPLOAI: TWideStringField
      FieldName = 'PLOAI'
    end
    object QrListTILE1: TFloatField
      FieldName = 'TILE1'
      OnChange = QrListSOTIENChange
      OnValidate = QrListTILE1Validate
    end
    object QrListTILE2: TFloatField
      FieldName = 'TILE2'
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 48
    Top = 196
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAVT'
      'TENVT'
      'DVT'
      'SL'
      'DG'
      'ST'
      'TENNHOM'
      'MANHOM')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 49
    Top = 254
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 218
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 304
    Top = 218
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrList2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrListBeforeOpen
    BeforeInsert = QrListBeforeInsert
    BeforeEdit = QrListBeforeEdit
    BeforePost = QrListBeforePost
    BeforeDelete = QrListBeforeDelete
    OnCalcFields = QrListCalcFields
    OnDeleteError = QrListPostError
    OnEditError = QrListPostError
    OnPostError = QrListPostError
    Parameters = <
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'POM_BANG_CHIPHI_CHUNG'
      ' where '#9'NAM = :NAM'
      '    and '#9'THANG = :THANG'
      '    and'#9'PLOAI = '#39'02'#39
      'order by MACP')
    Left = 128
    Top = 164
    object IntegerField1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'NAM'
    end
    object IntegerField3: TIntegerField
      FieldName = 'THANG'
    end
    object WideStringField1: TWideStringField
      FieldName = 'MACP'
    end
    object WideStringField2: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object IntegerField4: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object IntegerField5: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object GuidField1: TGuidField
      FieldName = 'ROWGUID'
      FixedChar = True
      Size = 38
    end
    object WideStringField3: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENCP'
      LookupDataSet = DataMain.QrPOM_DM_CHIPHI
      LookupKeyFields = 'MACP'
      LookupResultField = 'TENCP'
      KeyFields = 'MACP'
      Size = 200
      Lookup = True
    end
    object WideStringField5: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENCP_NHOM'
      LookupDataSet = DataMain.QrPOM_DM_CHIPHI_NHOM
      LookupKeyFields = 'MACP_NHOM'
      LookupResultField = 'TENCP_NHOM'
      KeyFields = 'MACP_NHOM'
      Size = 200
      Lookup = True
    end
    object FloatField1: TFloatField
      FieldName = 'SOTIEN1'
      OnChange = FloatField1Change
    end
    object FloatField2: TFloatField
      FieldName = 'SOTIEN2'
      OnChange = FloatField1Change
    end
    object FloatField3: TFloatField
      FieldName = 'SOTIEN'
    end
    object WideStringField6: TWideStringField
      FieldName = 'TAIKHOAN1'
    end
    object WideStringField7: TWideStringField
      FieldName = 'TAIKHOAN2'
    end
    object QrList2CODINH: TBooleanField
      FieldName = 'CODINH'
    end
    object QrList2MACP_NHOM: TWideStringField
      FieldName = 'MACP_NHOM'
    end
    object QrList2PLOAI: TWideStringField
      FieldName = 'PLOAI'
    end
    object QrList2TILE1: TFloatField
      FieldName = 'TILE1'
    end
    object QrList2TILE2: TFloatField
      FieldName = 'TILE2'
    end
  end
  object DsList2: TDataSource
    DataSet = QrList2
    Left = 128
    Top = 196
  end
  object Filter2: TwwFilterDialog2
    DataSource = DsList2
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAVT'
      'TENVT'
      'DVT'
      'SL'
      'DG'
      'ST'
      'TENNHOM'
      'MANHOM')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 121
    Top = 254
  end
  object PopIn: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 304
    Top = 298
    object MenuItem1: TMenuItem
      Caption = 'Chi ph'#237' chung'
      OnClick = CmdPrintExecute
    end
    object MenuItem2: TMenuItem
      Tag = 1
      Caption = 'Chi ph'#237' t'#7915'ng ph'#226'n x'#432#7903'ng'
      OnClick = CmdPrintExecute
    end
  end
end
