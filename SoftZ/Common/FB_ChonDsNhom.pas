﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FB_ChonDsNhom;

interface

uses
  Classes, Controls, Forms, System.Variants,
  StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg, MemTableDataEh, MemTableEh, DBGridEh, DBCtrlsEh, Vcl.Mask,
  DBLookupEh, DbLookupComboboxEh2;

type
  TFrmFB_ChonDsNhom = class(TForm)
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    RgLoai: TRadioGroup;
    paNhom: TPanel;
    QrNHOM2: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    btnAdd: TBitBtn;
    btnRemove: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    DsNHOM: TDataSource;
    DsNHOM2: TDataSource;
    TbDummyEh: TMemTableEh;
    TbDummyEhMaNganh: TStringField;
    TbDummyEhMaNhom: TStringField;
    TbDummyEhMaNganNhom: TStringField;
    TbDummyEhMaNhom2: TStringField;
    TbDummyEhMaNganNhom2: TStringField;
    DsDummyEh: TDataSource;
    CbNganh: TDbLookupComboboxEh2;
    EdMaNganh: TDBEditEh;
    EdMaNhom: TDBEditEh;
    EdMaNhom2: TDBEditEh;
    CbNhom2: TDbLookupComboboxEh2;
    CbNhom: TDbLookupComboboxEh2;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CbNhomDropDown(Sender: TObject);
    procedure CbNhom2DropDown(Sender: TObject);
    procedure TbDummyEhMaNganhChange(Sender: TField);
    procedure TbDummyEhMaNhomChange(Sender: TField);
    procedure TbDummyEhMaNhom2Change(Sender: TField);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmFB_ChonDsNhom: TFrmFB_ChonDsNhom;

implementation

uses
	isDb, isLib, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormShow(Sender: TObject);
var
    n, i: Integer;
begin
    TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrNGANH, QrNHOM, QrNHOM2, DataMain.QrDM_NHOM, DataMain.QrDM_NHOM2]);
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
	mTrigger := False;

    n := FlexConfigInteger('FB_DM_HH_NGANH', 'Depth') + 1;
    with RgLoai do
    begin
        for i := Items.Count downto n do
            Items.Delete(i-1);

        Columns := Items.Count;
    end;

    CbNhom2.Visible := n > 3;
    EdMaNhom2.Visible := n > 3;

    i := Iif(n > 3, 0, 28);

    paNhom.Height := 105 - i;
    btnAdd.Top := 168 - i;
    btnRemove.Top := 168 - i;

    GrList.Top := 200 - i;
    GrList.Height := 103 + i;

    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFB_ChonDsNhom.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    CbNhom.Enabled := n > 0;
    CbNhom2.Enabled := n > 1;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbNganh.SetFocus;
        1:
            CbNhom.SetFocus;
        2:
            CbNhom2.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.TbDummyEhMaNganhChange(Sender: TField);
begin
    with TbDummyEh do
    begin
        FieldByName('MaNhom').AsString := '';
        FieldByName('MaNhom2').AsString := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.TbDummyEhMaNhom2Change(Sender: TField);
var
    s, s1 : string;
begin
    s1 := Sender.AsString;
    s :=  TbDummyEh.FieldByName('MaNganh').AsString;
    with QrNHOM2 do
    if Locate('MaNhom;MaNganh', VarArrayOf([s1, s]), []) then
    begin
        s1 := FieldByName('MA').AsString;
    end;
    TbDummyEh.FieldByName('MaNganNhom2').AsString := s1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.TbDummyEhMaNhomChange(Sender: TField);
var
    s : string;
begin
    s := Sender.AsString;
    with QrNHOM do
    if Locate('MaNhom', s, []) then
    begin
        s := FieldByName('MA').AsString;
    end;
    with TbDummyEh do
    begin
        FieldByName('MaNhom2').AsString := '';
        FieldByName('MaNganNhom').AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
        	s1 := EdMaNganh.Text;
        	s2 := CbNGANH.Text;
        end;
    1:
    	begin
        	s1 := CbNhom.Value;
        	s2 := CbNhom.Text;
        end;
    2:
    	begin
        	s1 := CbNhom2.Value;
            s2 := CbNhom2.Text;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;

//        EdMAVT.Text := '';
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := EdMaNganh.Text;
    1:
       	s := EdMaNhom.Text;
    2:
       	s := EdMaNhom2.Text;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
//	    CloseDataSets([QrNGANH, QrNHOM, QrNHOM2]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CbNhom2DropDown(Sender: TObject);
var
    s, s1: string;
begin
    s := TbDummyEh.FieldByName('MaNganh').AsString;
    s1 := TbDummyEh.FieldByName('MaNhom').AsString;
    QrNHOM2.Filter := 'MaNhom=''' + s1 + ''' and MaNganh=''' + s + '''';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CbNhomDropDown(Sender: TObject);
var
    s: string;
begin
    s := TbDummyEh.FieldByName('MaNganh').AsString;
    QrNHOM.Filter := 'MaNganh=''' + s + '''';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFB_ChonDsNhom.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
