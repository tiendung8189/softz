﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit GmsRep;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ActnList, StdCtrls, Buttons, ComCtrls, ExtCtrls, Grids,
  Wwdbgrid2, Db, ADODB,
  Shellapi, crCommon, Wwdbigrd, Wwdbgrid, ImgList;

type
  TFrmGmsRep = class(TForm)
    ActionList1: TActionList;
    CmdRun: TAction;
    DsRep: TDataSource;
    Panel4: TPanel;
    PgGroup: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet11: TTabSheet;
    GrList: TwwDBGrid2;
    Bevel4: TBevel;
    Panel21: TPanel;
    PaParam: TPanel;
    Bevel8: TBevel;
    Bevel1: TBevel;
    PaCMD: TPanel;
    Image1: TImage;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BtnCalc: TBitBtn;
    CmdCalc: TAction;
    TabSheet1: TTabSheet;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    CmdChonDsma: TAction;
    QrREP: TADOStoredProc;
    CmdReread: TAction;
    CmdRuntimeEdit: TAction;
    CmdChonDsNCC: TAction;
    CmdChonDsKH: TAction;
    CmdTestAll: TAction;
    cmdChonDsHH: TAction;
    CmdChonDsNgNh: TAction;
    CmdChonDsKho: TAction;
    ImageList1: TImageList;
    spKIEMKE_Get_Ngay_Max: TADOCommand;
    procedure CmdRunExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure QrREPAfterScroll(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure PgGroupChange(Sender: TObject);
    procedure CmdCalcExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdRereadExecute(Sender: TObject);
    procedure CmdRuntimeEditExecute(Sender: TObject);
    procedure CmdTestAllExecute(Sender: TObject);
    procedure QrREPFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    mCats, mGroupName: string;
//    mGroup: Integer;
  	mStockControl, mMustCalc, mFixLogon, mIsTemplate: Boolean;
    mFields: TStrings;			// List of criteria fields
    mFrames: array of TCrFrame;	// List of criteria frames

    procedure OpenReports;
    function  Proc(fs: String = ''): Boolean;
  public
//  	function  Execute(group: Integer = 0): Boolean;
    function  Execute(groupName: String = ''): Boolean;
    function  IsStockControl: Boolean;
    procedure SetMustCalc(pValue: Boolean);

  end;

var
  FrmGmsRep: TFrmGmsRep;

implementation

{$R *.DFM}

uses
	MainData, isDb, RepEngine, ExCommon, isStr, isMsg, Rights, GuidEx,
    isLib, isFile,
    isCommon,crKho, crDs_Kho, crTuden, crTon, crUID,
    crNgay, crQuy, crNam, crThoigian, crMANCC, crMAKH, crDS_NCC, crMAVIP,
    crDS_KH, crCT_TH, crTOP, crMAVT, crSILE, crDS_NHOMVT, crMAHH,
    crNGANHNHOM, crTHANGNAM, OfficeData, crDS_LYDO_NK, crDS_LYDO_XK, crDS_USER,
    crDS_LOC, crDS_NV, crSOTHANG, crLOC, crSONGAY, crCHIETKHAU, crBANLE_TT, crDS_VIP,
    crDS_LYDO_THU, crDS_LYDO_THUKHAC, crDS_LYDO_CHI, crDS_LYDO_CHIKHAC, crLichSuThanhToanChungTu,

    // FB
    crFB_DS_NHOMVT, crFB_DS_LYDO_NK, crFB_DS_LYDO_XK, crFB_MAVT, crFB_MAVT_NPL, crFB_MAVT_BO,
    crFB_MAVT_TD, crFB_MAVT_NPL_TD,

    //HR
    crDS_NhanVien, crDS_ChucDanh, crGioiTinh, crDS_VanHoa, crDS_QuocTich, crDS_DanToc,
    crDS_TonGiao, crDS_NguonTD, crDS_HopDong, crDS_QuaTrinhLV, crQuanHeGiaDinh,
    crKThuongKLuat, crDoiTuong, crTinhTrangNhanVien, crLoaiNhanVien, crDS_ChiNhanh,
    crDS_ChiNhanhNotCN, crHonNhan, crDS_LyDoThoiViec, crDS_PhongBan, crDS_BoPhan,
    crDS_KThuong, crDS_KLuat, crDS_LyDoVangMat;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGmsRep.Execute;
var
	i, n, m: Integer;
begin
  	mTrigger := False;
    mFixLogon := False;
    mGroupName := groupName;
    mCats := FlexConfigString('Admin', 'Rp Group ' + mGroupName);

    OpenReports;
    if QrREP.IsEmpty then
	begin
    	Result := False;
		CloseDataSets([QrREP]);
    end
    else
    begin
    	Result := True;
        // List of criteria fields
        mFields := TStringList.Create;
        mFields.Text :=
            'LOC'#13 +
            'DS_LOC'#13 +
            'KHO'#13 +
            'DS_KHO'#13 +
            'HR_DS_ChiNhanh'#13 +
            'NGAY'#13 +
            'TUNGAY'#13 +
            'GIA'#13 +
            'THANGNAM'#13 +
            'NAM'#13 +
            'QUY'#13 +
            'THOIGIAN'#13 +
            'SOTHANG'#13 +
            'SONGAY'#13 +
            'MANCC'#13 +
            'MAKH'#13 +
            'DS_NCC'#13 +
            'DS_KH'#13 +
            'DS_NV'#13 +
            'SILE'#13+
            'CT_TH'#13 +
            'TOP10'#13 +
            'CHIETKHAU'#13 +
            'NHOMVT'#13 +
            'DS_NHOMVT'#13 +
            'MAVIP'#13 +
            'DS_VIP'#13 +
            'BANLE_TT'#13 +
            'DS_LYDO_NK'#13 +
            'DS_LYDO_XK'#13 +
            'DS_LYDO_THU'#13 +
            'DS_LYDO_THUKHAC'#13 +
            'DS_LYDO_CHI'#13 +
            'DS_LYDO_CHIKHAC'#13 +
            'LichSuThanhToanChungTu'#13 +
            'DS_USER'#13 +
            'MAVT'#13 +
            'MAHH'#13 +
            'FB_DS_NHOMVT'#13 +
            'FB_DS_LYDO_NK'#13 +
            'FB_DS_LYDO_XK'#13 +
            'FB_DS_MAVT_NPL'#13 +
            'FB_DS_MAVT_TD'#13 +
            'FB_DS_MAVT_NPL_TD'#13 +
            'FB_DS_MAVT_BO'#13 +
            'FB_DS_MAVT'#13 +
            'HR_DS_PhongBan'#13 +
            'HR_DS_BoPhan'#13 +
            'HR_DS_NhanVien'#13 +
            'HR_DS_ChucDanh'#13 +
            'HR_GioiTinh'#13 +
            'HR_HonNhan'#13 +
            'HR_DS_VanHoa'#13 +
            'HR_DS_QuocTich'#13 +
            'HR_DS_DanToc'#13 +
            'HR_DS_TonGiao'#13 +
            'HR_DS_NguonTD'#13 +
            'HR_DS_QuaTrinhLV'#13 +
            'HR_DS_HopDong'#13 +
            'HR_QuanHeGiaDinh'#13 +
            'HR_KThuongKLuat'#13 +
            'HR_DS_KThuong'#13 +
            'HR_DS_KLuat'#13 +
            'HR_DoiTuong'#13 +
            'HR_LoaiNhanVien'#13 +
            'HR_DS_LyDoVangMat'#13 +
            'HR_DS_LyDoThoiViec'#13 +
            'HR_TinhTrangNhanVien'#13;

        // Create criteria frames
        n := mFields.Count;

        SetLength(mFrames, n);
        mFrames[mFields.IndexOf('LOC')]     := TframeCrLOC.Create(Self);
        mFrames[mFields.IndexOf('DS_LOC')]  := TframeDS_LOC.Create(Self);
        mFrames[mFields.IndexOf('KHO')]     := TframeCrKho.Create(Self);
        mFrames[mFields.IndexOf('DS_KHO')]  := TframeDs_Kho.Create(Self);
        if sysIsHrChinhanh then
            mFrames[mFields.IndexOf('HR_DS_ChiNhanh')]    := TframeHR_DS_ChiNhanh.Create(Self)
        else
            mFrames[mFields.IndexOf('HR_DS_ChiNhanh')]    := TFrameHR_DS_ChiNhanhNotCN.Create(Self);
        mFrames[mFields.IndexOf('NGAY')]    := TframeCrNgay.Create(Self);
        mFrames[mFields.IndexOf('TUNGAY')]  := TframeTuden.Create(Self);
        mFrames[mFields.IndexOf('GIA')]     := TframeTON.Create(Self);
        mFrames[mFields.IndexOf('THANGNAM')]:= TframeTHANGNAM.Create(Self);
        mFrames[mFields.IndexOf('NAM')]     := TframeNam.Create(Self);
        mFrames[mFields.IndexOf('QUY')]     := TframeQuy.Create(Self);
        mFrames[mFields.IndexOf('THOIGIAN')]:= TframeThoigian.Create(Self);
        mFrames[mFields.IndexOf('SOTHANG')]  := TframeSOTHANG.Create(Self);
        mFrames[mFields.IndexOf('SONGAY')]  := TframeSongay.Create(Self);
        mFrames[mFields.IndexOf('MANCC')]   := TframeMANCC.Create(Self);
        mFrames[mFields.IndexOf('MAKH')]    := TframeMAKH.Create(Self);
        mFrames[mFields.IndexOf('DS_NCC')]  := TframeDS_NCC.Create(Self);
        mFrames[mFields.IndexOf('DS_KH')]   := TframeDS_KH.Create(Self);
        mFrames[mFields.IndexOf('DS_NV')]   := TframeDS_NV.Create(Self);
        mFrames[mFields.IndexOf('SILE')]    := TframeSILE.Create(Self);
        mFrames[mFields.IndexOf('CT_TH')]   := TframeCT_TH.Create(Self);
        mFrames[mFields.IndexOf('TOP10')]   := TframeTOP.Create(Self);
        mFrames[mFields.IndexOf('CHIETKHAU')]   := TframeCHIETKHAU.Create(Self);
        mFrames[mFields.IndexOf('NHOMVT')]  := TframeNGANHNHOM.Create(Self);
        mFrames[mFields.IndexOf('DS_NHOMVT')]:= TframeDS_NHOMVT.Create(Self);
        mFrames[mFields.IndexOf('MAVIP')]    := TframeMAVIP.Create(Self);
        mFrames[mFields.IndexOf('DS_VIP')]    := TframeDS_VIP.Create(Self);
        mFrames[mFields.IndexOf('BANLE_TT')]    := TframeBANLE_TT.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_NK')]    := TframeDS_LYDO_NK.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_XK')]    := TframeDS_LYDO_XK.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_THU')]    := TframeDS_LYDO_THU.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_THUKHAC')]    := TframeDS_LYDO_THUKHAC.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_CHI')]    := TframeDS_LYDO_CHI.Create(Self);
        mFrames[mFields.IndexOf('DS_LYDO_CHIKHAC')]    := TframeDS_LYDO_CHIKHAC.Create(Self);
        mFrames[mFields.IndexOf('LichSuThanhToanChungTu')]    := TframeLichSuThanhToanChungTu.Create(Self);
        mFrames[mFields.IndexOf('DS_USER')]    := TframeDS_USER.Create(Self);
        mFrames[mFields.IndexOf('MAVT')]    := TframeMAVT.Create(Self);
        mFrames[mFields.IndexOf('MAHH')]    := TframeMAHH.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_NHOMVT')]:= TFrameFB_DS_NHOMVT.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_LYDO_NK')]    := TframeFB_DS_LYDO_NK.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_LYDO_XK')]    := TframeFB_DS_LYDO_XK.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_MAVT_NPL')]    := TframeFB_MAVT_NPL.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_MAVT_TD')]    := TframeFB_MAVT_TD.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_MAVT_NPL_TD')]    := TframeFB_MAVT_NPL_TD.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_MAVT_BO')]    := TframeFB_MAVT_BO.Create(Self);
        mFrames[mFields.IndexOf('FB_DS_MAVT')]    := TframeFB_MAVT.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_PhongBan')]    := TFrameHR_DS_PhongBan.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_BoPhan')]    := TframeHR_DS_BoPhan.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_NhanVien')]    := TframeHR_DS_NhanVien.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_ChucDanh')]    := TframeHR_DS_ChucDanh.Create(Self);
        mFrames[mFields.IndexOf('HR_GioiTinh')]    := TframeHR_GioiTinh.Create(Self);
        mFrames[mFields.IndexOf('HR_HonNhan')]    := TframeHR_HonNhan.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_VanHoa')]    := TFrameHR_DS_VanHoa.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_QuocTich')]    := TFrameHR_DS_QuocTich.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_DanToc')]    := TFrameHR_DS_DanToc.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_TonGiao')]    := TFrameHR_DS_TonGiao.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_NguonTD')]    := TFrameHR_DS_NguonTD.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_QuaTrinhLV')]    := TframeHR_DS_QuaTrinhLV.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_HopDong')]    := TframeHR_DS_HopDong.Create(Self);
        mFrames[mFields.IndexOf('HR_QuanHeGiaDinh')]    := TframeHR_QuanHeGiaDinh.Create(Self);
        mFrames[mFields.IndexOf('HR_KThuongKLuat')]    := TframeHR_KThuongKLuat.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_KThuong')]    := TFrameHR_DS_KThuong.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_KLuat')]    := TFrameHR_DS_KLuat.Create(Self);
        mFrames[mFields.IndexOf('HR_DoiTuong')]    := TframeHR_DoiTuong.Create(Self);
        mFrames[mFields.IndexOf('HR_LoaiNhanVien')]    := TframeHR_LoaiNhanVien.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_LyDoVangMat')]    := TFrameHR_DS_LyDoVangMat.Create(Self);
        mFrames[mFields.IndexOf('HR_DS_LyDoThoiViec')]    := TframeHR_DS_LyDoThoiViec.Create(Self);
        mFrames[mFields.IndexOf('HR_TinhTrangNhanVien')]    := TframeHR_TinhTrangNhanVien.Create(Self);

        n := Length(mFrames) - 1;
        m := PaParam.Width;
        for i := 0 to n do
            with mFrames[i] do
            begin
                Visible := False;	// Make it surely
                Parent := PaParam;
                Left := 0;
                Width := m;
                Anchors := Anchors + [akLeft, akRight];
            end;
        mIsTemplate := QrREP.FindField('TEMPLATE') <> nil;
        ShowModal;

        mFields.Free;
        SetLength(mFrames, 0);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.SetMustCalc(pValue: Boolean);
begin
    mMustCalc := pValue;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGmsRep.IsStockControl: Boolean;
begin
    Result := mStockControl;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	// Final
    RegWrite('Rps\Rp Group ' + mGroupName, ['Page', 'Id'],
    	[PgGroup.ActivePageIndex, QrRep.FieldByName('IDX').AsInteger]);

	// Done
    try
	    CloseDataSets([QrREP]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    ResetRowColor([GrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.OpenReports;
begin
    with QrREP do
    begin
    	Close;
    	Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := sysEnglish;
        Parameters[3].Value := mCats;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormShow(Sender: TObject);
var
	m, n, i: Integer;
    txt, ls: TStrings;
begin
    // Stock control
	mStockControl := rCanEdit(GetRights('SZ_TONTT', False));

    // Show / hide groups
    txt := TStringList.Create;
    if sysEnglish then
	    txt.Text := isStrReplace(FlexConfigString('Admin', 'Report English Categories'), ';', #13)
    else
	    txt.Text := isStrReplace(FlexConfigString('Admin', 'Report Categories'), ';', #13);

    ls := TStringList.Create;
    if mCats <> '' then
		ls.Text := isStrReplace(mCats, ';', #13)
	else
        for i := 0 to txt.Count - 1 do
			ls.Add(txt.Names[i]);

    n := 0;
    with QrRep, PgGroup do
    begin
    	for i := 0 to ls.Count - 1 do
        begin
        	m := StrToInt(ls[i]);
		    if Locate('CAT', m, []) and (n < PageCount) then
            begin
	            Pages[n].Tag := m;
	            Pages[n].Caption := txt.Values[ls[i]];
                Inc(n);
            end;
        end;
        txt.Free;
	    ls.Free;

		while n < PageCount do
	    begin
		    Pages[n].TabVisible := False;
	    	Inc(n);
    	end;
    end;
    ClearWait;

    // Seek to the last called report
    m := RegRead('Rps\Rp Group ' + mGroupName , 'Page', 0);
    n := RegRead('Id', -1);

    with PgGroup do
    begin
	    if PageCount <= m then
	    	m := 0;

//		while (m < PageCount) and (not Pages[m].TabVisible) do
//        	Inc(m);
        if not Pages[m].TabVisible then
            m := 0;

		ActivePageIndex := m;
	end;
    PgGroup.OnChange(Nil);

    with QrRep do
    begin
    	Tag := 1;
    	if not Locate('IDX', n, []) then
        	First;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.QrREPAfterScroll(DataSet: TDataSet);
var
    i, n1, n2, y: Integer;
begin
	if QrRep.Tag = 0 then
    	Exit;

	n1 := Length(mFrames) - 1;
    n2 := mFields.Count - 1;
    y := 0;
	with QrRep do
    begin
    	if FieldByName('SYS').AsBoolean then
        begin
	    	for i := 0 to n1 do
                mFrames[i].Visible := False;
        	Exit;
        end;

    	for i := 0 to n2 do
        	if i <= n1 then
                with mFrames[i] do
                begin
                    Visible := FieldByName(mFields[i]).AsBoolean;
                    if Visible then
                    begin
                        Top := y;
                        Inc(y, Height);
                    end;
                end;
    end;
end;

procedure TFrmGmsRep.QrREPFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
    if (mGroupName <> '') then
        Exit;

    Accept := DataSet.FieldByName('GIA').AsBoolean or
                (DataSet.FieldByName('LCT').AsString = 'SYS_GIA')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if QrRep.FieldByName('SYS').AsBoolean then
    	with AFont do
        begin
            Style := [fsBold];
            if Highlight then
	        	Color := clWhite
            else
    	    	Color := clGreen;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_THESAME_MONTH = 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME = 'Thời gian báo cáo không hợp lệ.';
    RS_INVALID_REPORTDATE = 'Ngày báo cáo cần > ngày chốt tồn kho';
function TFrmGmsRep.Proc(fs: String = ''): Boolean;
const
    mVarFields: array[0..9] of String = ('DS_DT', 'DS_NV', 'DS_VIP','MAVT', 'MAHH', 'FB_DS_MAVT_NPL', 'FB_DS_MAVT_TD', 'FB_DS_MAVT_NPL_TD', 'FB_DS_MAVT_BO', 'FB_DS_MAVT');
var
	sRep, sCap, mTemplate, s, stemp1, stemp2, sLCT, sMessage: String;
    i, i1, n, n1, def, posIndex: Integer;
    mVar, mVarValues, mVarStockFirstDate: array of Variant;
    luot: TGUID;
    sResult: Boolean;
begin
    // Prepare
	with QrRep do
    begin
    	sRep := Trim(FieldByName('REP_NAME').AsString);
        sLCT := Trim(FieldByName('LCT').AsString);
        if fs = '' then
	    	//sCap := isProperCase(isStripToneMark(Trim(FieldByName('DESC').AsString)));
            sCap := '';//((Trim(FieldByName('DESC').AsString)));

	    // Within month
        if FieldByName('TRONGTHANG').AsBoolean then
        begin
            if FieldByName('TUNGAY').AsBoolean then
                if not mFrames[mFields.IndexOf('TUNGAY')].Validate then
                    Abort;

            if FieldByName('THOIGIAN').AsBoolean then
                if not mFrames[mFields.IndexOf('THOIGIAN')].Validate then
                    Abort;
        end;
    end;

    sResult := True;
    posIndex := Pos('THEKHO', UpperCase(sLCT));
    if posIndex > 0 then
    begin
        n1 := Length(mFrames) - 1;
        SetLength(mVarStockFirstDate, 1);
        SetLength(mVarValues, Length(mVarFields));

        mVarStockFirstDate[0] := UpperCase(sLCT);
        for i := 0 to n1 do
        with mFrames[i] do
        if Visible and
          ((mFields.IndexOf('KHO') = i) or
          (mFields.IndexOf('TUNGAY') = i)) then
        begin
            SetLength(mVarStockFirstDate, Length(mVarStockFirstDate)
              + GetParamNo);
            GetCriteria(mVarStockFirstDate, mVarFields, mVarValues);
            if mFields.IndexOf('TUNGAY') = i then
            begin
               i1 := Length(mVarStockFirstDate);

               mVarStockFirstDate[i1-1] := isStrReplace(mVarStockFirstDate[i1-1], ',', '-');
               mVarStockFirstDate[i1-2] := isStrReplace(mVarStockFirstDate[i1-2], ',', '-');
            end;
        end;
        n1 := Length(mVarStockFirstDate) - 1;
        if n1 > 0 then
        begin
            with spKIEMKE_Get_Ngay_Max do
            begin
                for i1 := 0 to n1 do
                begin
                   Parameters[(i1 + 1)].Value := mVarStockFirstDate[i1];
                end;
                Execute;
                sMessage := Parameters[5].Value;
                Result := Parameters[0].Value <> 0;
            end;
            if Result then
            begin
                sResult := False;
                ErrMsg(sMessage);
            end;
        end;

    end;

    if sResult then
    begin
        if sRep = '' then
        begin
            GrList.SetFocus;
            Result := False;
            Exit;
        end;

        // Stock report
        if QrRep.FieldByName('GIA').AsBoolean then
        begin
            if mMustCalc then
                CmdCalc.Execute;
        end;

        // Get Criteria
        n := Length(mFrames) - 1;
        luot := TGuidEx.NewGuid;
        SetLength(mVar, 3);
        SetLength(mVarValues, Length(mVarFields));

        mVar[0] := TGuidEx.ToString(luot);
        mVar[1] := sysLogonUID;
        mVar[2] := QrRep.FieldByName('LCT').AsString;
        for i := 0 to n do
        with mFrames[i] do
        if Visible then
        begin
            SetLength(mVar, Length(mVar) + GetParamNo);
            GetCriteria(mVar, mVarFields, mVarValues);
        end;

        //Add vào sys_report_param với các chuỗi có thể >256 ký tự
        DataMain.SetSysRepParam(luot, mVarFields, mVarValues);

        // Report template
        if not mIsTemplate then
            mTemplate := ''
        else
            mTemplate := Trim(QrRep.FieldByName('TEMPLATE').AsString);
        if mTemplate  <> '' then
        begin
            Result := DataOffice.CreateReport2(mTemplate, mVar, sRep);
            Exit;
        end;

        // Proc
        if fs = '' then		// To window
            Result := ShowReport(sCap, sRep, mVar)
    //    else				// Export for testing
    //	    Result := ExportReport(fs, sRep, 'RPT', mVar)
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRunExecute(Sender: TObject);
begin
	Proc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmGmsRep.PgGroupChange(Sender: TObject);
begin
    with QrRep do
    begin
    	Filter := Format('CAT=%d', [PgGroup.ActivePage.Tag]);
        while not Eof do
        	if FieldByName('SYS').AsBoolean then
	        	Next
            else
            	Break;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdCalcExecute(Sender: TObject);
begin
    mFrames[mFields.IndexOf('GIA')].Action(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	with QrRep do
    begin
		CmdCalc.Enabled := FieldByName('GIA').AsBoolean and mStockControl;
    	CmdRun.Enabled := not FieldByName('SYS').AsBoolean;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRereadExecute(Sender: TObject);
var
	n: Integer;
begin
	n := QrRep.FieldByName('IDX').AsInteger;
    OpenReports;
    QrRep.Locate('IDX', n, []);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdRuntimeEditExecute(Sender: TObject);
var
	mPath, mName: String;
begin
	if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
	end;

    with QrRep do
    begin
		if FieldByName('SYS').AsBoolean then
        	Exit;
		mName := Trim(FieldByName('REP_NAME').AsString);
        mName := Iif(sysEnglish, 'EN_' + mName, mName);
    end;
//    mPath := GetReportFullPath(mName);
    FrmRep.BuildFullPath(mName, mPath);
    if mPath = '' then
    	Exit;

    //  Open target report for editing
    ShellExecute(Handle, 'Open', PChar(mPath), nil, nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGmsRep.CmdTestAllExecute(Sender: TObject);
var
	s, fs: String;
    ls: TStrings;
begin
	if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
	end;

	if not YesNo('Chạy kiểm tra các báo cáo trong danh sách hiện tại. Tiếp tục?') then
    	Exit;
	fs := isGetTempFileName;
    ls := TStringList.Create;

    with QrREP do
    begin
    	First;
        while not Eof do
        begin
        	if not FieldByName('SYS').AsBoolean then
            begin
        		s := Trim(FieldByName('REP_NAME').AsString);
	            if s <> '' then
					if not Proc(fs) then
        	        	ls.Add(FieldByName('DESC').AsString + #9#9 + s);
            end;

            Next;
        end;
    end;
    DeleteFile(fs);

    // Done
    if ls.Count > 0 then
    begin
    	ls.Insert(0, '---------------------');
    	ls.Insert(0, 'List of error reports');
        ls.SaveToFile(sysTempPath + 'errors.log');
	    ShellExecute(Handle, 'Open', PChar(sysTempPath + 'errors.log'), nil, nil, SW_NORMAL);
    end
    else
    	Msg('Không có lỗi nào được phát hiện.');
    ls.Free;
end;

end.
