﻿unit exPrintBill;

interface
uses
	Classes, ADODb;

type
    arrVariant = array of Variant;
type
  TexPrintBill = class(TObject)
    private
        FType: Integer;
    protected
        FPrinterI: Integer;
        FPrinterName: String;
        FPreview: Boolean;
    public
        property PrintType: Integer read FType write FType;
        property PrintPreview: Boolean read FPreview write FPreview;
        procedure BillInit;
        function  BillPrint(const pKhoa: TGUID; const pCopy: Integer = 1): Boolean; virtual;
        function  BillSetPrinter(const pPrinterName: String): Boolean;
    end;

  //
  TexPrintRpt = class(TexPrintBill)
    private
        FFileName: String;
    public
        constructor Create(const pFileName: String); overload;
        function  BillPrint(const pKhoa: TGUID; const pCopy: Integer = 1): Boolean; override;
    end;

  //Fast report
  TexPrintFRpt = class(TexPrintBill)
    private
        FFileName: String;
    public
        constructor Create(const pFileName: String); overload;
        function  BillPrint(const pKhoa: TGUID; const pCopy: Integer = 1): Boolean; override;
    end;

  TexPrintDirect = class(TexPrintBill)
    procedure BillInitial(const pSpMaster: String = 'RP_POSZ_TEXT_MASTER';
        const pSpDetail: String =  'RP_POSZ_TEXT_DETAIL';
        const pSQL: String = '');
    procedure BillFinal;
  private
    mCopy: Integer;
    mBillFile: TextFile;
    mBillHeader, mBillDetail, mBillFooter, mAccContent: TStrings;

    POS_BILL_MASTER, POS_BILL_DETAIL: TADOStoredProc;
    mBillHeaderValues: array of Variant;
    mBillDetailValues: array of Variant;

    function  BillHeader(Values: array of Variant): Boolean; overload;
    procedure BillDetail(Values: array of Variant); overload;
    procedure BillDetail(); overload;
    procedure BillFooter(Values: array of Variant); overload;

    procedure SetNullVariant(var Values: arrVariant; pInc: Integer = 2);
    procedure PrintBlock(Block: TStrings; Values: array of Variant);
    function  GetBillHeaderValues(const khoa: TGUID): Boolean;
    function  GetBillDetail(const khoa: TGUID): Integer;

  public
    constructor Create(const pSpMaster: String; const pSpDetail: String); overload;
    destructor Destroy(); overload; override;

    procedure PrintAccu(Values: array of Variant);
    function  BillHeader(const khoa: TGUID): Boolean; overload;
    procedure BillDetail(const khoa: TGUID); overload;
    procedure BillFooter; overload;

    function  BillPrint(const khoa: TGUID; const pCopy: Integer = 1): Boolean; override;
  end;

  procedure exBillInitial(var pPrintBill: TexPrintBill; pFileName: String = '');
  procedure exBillFinal(pPrintBill: TexPrintBill);

implementation
uses
	Forms, SysUtils, Printers, MainData, isNameVal, IniFiles, isLib,  isStr,
    isCommon, RepEngine, GuidEx, ExCommon, isMsg;

    (*
    ** Print bill
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintDirect.BillInitial;
var
    i, k, n: Integer;
begin
    // Ini Params.
    with TIniFile.Create(sysAppPath + '\ConfigZ.ini') do
    begin
    	PrintPreview := ReadInteger('COMMON', 'Debug', 0) = 1;
        Free;
    end;

	mBillHeader  := TStringList.Create;
	mBillDetail := TStringList.Create;
	mBillFooter := TStringList.Create;
    mAccContent := TStringList.Create;

	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
        if pSQL <> '' then
            SQL.Text := pSQL
        else
            SQL.Text :=
        	    'select b.* from [DM_QUAYTN] a, PRINTERS b, DM_KHO k where a.MAKHO=k.MAKHO and a.PRINTER = b.PRINTER and a.TENMAY=' +
                    QuotedStr(isGetComputerName);

        try
            Open;

            if IsEmpty then
                raise Exception.Create('Chưa cấu hình máy in.');
        except
            on E: Exception do
                raise Exception.Create('PRINTER message: ' + E.Message);
        end;

	   	mBillHeader.Text := FieldByName('BILL_HEADER').AsString;
   		mBillDetail.Text := FieldByName('BILL_DETAIL').AsString;
		mBillFooter.Text := FieldByName('BILL_FOOTER').AsString;
        try
			mAccContent.Text := FieldByName('ACCU_CONTENT').AsString;
        except
        end;

        Close;
        Free;
    end;

    //Master.
    POS_BILL_MASTER := TADOStoredProc.Create(nil);
    with POS_BILL_MASTER do
    begin
    	Connection := DataMain.Conn;
        CommandTimeout := 1200;
        ProcedureName :=  pSpMaster;
        try
            Parameters.Refresh;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := TGuidEx.ToString(TGuidEx.EmptyGuid);
            Parameters[3].Value := sysLoc;
            ExecProc;
            Active := True;
       except
            on E: Exception do
                raise Exception.Create(pSpMaster + ' message: ' + E.Message);
        end;

        // Number of fields
        n := FieldCount;
        k := 2;

        // Alloc buffer
        SetLength(mBillHeaderValues, 2 * (n + 1));	// (field count) + Copy
        mBillHeaderValues[0] := 'COPY';

        for i := 0 to n - 1 do
        begin
			mBillHeaderValues[k] := Fields[i].FullName;
            Inc(k, 2);
        end;

        Close;
    end;

    //Detail
    POS_BILL_DETAIL := TADOStoredProc.Create(nil);
    with POS_BILL_DETAIL do
    begin
    	Connection := DataMain.Conn;
        CommandTimeout := 1200;
        ProcedureName := pSpDetail;
        try
            Parameters.Refresh;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := TGuidEx.ToString(TGuidEx.EmptyGuid);
            Parameters[3].Value := sysLoc;
            ExecProc;
            Active := True;
        except
            on E: Exception do
                raise Exception.Create(pSpDetail + ' message: ' + E.Message);
        end;

        // Number of fields
        n := FieldCount;

        // Alloc buffer
        SetLength(mBillDetailValues, 2 * n);

        // Get fields' name
        k := 0;
        for i := 0 to n - 1 do
        begin
			mBillDetailValues[k] := Fields[i].FullName;
            Inc(k, 2);
        end;

        Close;
    end;
end;

function TexPrintDirect.BillPrint(const khoa: TGUID; const pCopy: Integer): Boolean;
var
    i: Integer;
begin
    mCopy := 1;
    try
        Result := GetBillHeaderValues(khoa);
        if Result then
        begin
            if (FPrinterI >= 0) and (Printers.Printer.PrinterIndex <> FPrinterI) then
                    Printers.Printer.PrinterIndex := FPrinterI;

            GetBillDetail(khoa);
            for i := 1 to pCopy do
            while mCopy <= pCopy do
            begin
                mBillHeaderValues[1] := mCopy;
                BillHeader(mBillHeaderValues);
                BillDetail;
                BillFooter(mBillHeaderValues);
                inc(mCopy);
            end;
            POS_BILL_DETAIL.Close;
            SetNullVariant(arrVariant(mBillHeaderValues));
        end;
    except
        on E: Exception do
        begin
            Raise Exception.Create(E.Message);
            Result := False;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TexPrintDirect.Create(const pSpMaster, pSpDetail: String);
begin
    inherited Create;
    BillInit;
    BillInitial(pSpMaster, pSpDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TexPrintDirect.Destroy;
begin
    BillFinal;
    inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintDirect.SetNullVariant(var Values: arrVariant; pInc: Integer = 2);
var
    n, k: Integer;
begin
    k := Low(Values) + (pInc - 1);
    n := High(Values) + 1;

    while k < n do
    begin
        VarClear(Values[k]);
        inc(k, pInc);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintDirect.BillFinal;
begin
	mBillHeader.Free;
	mBillDetail.Free;
	mBillFooter.Free;
    mAccContent.Free;

    SetLength(mBillHeaderValues, 0);
    SetLength(mBillDetailValues, 0);
    POS_BILL_MASTER.Free;
    POS_BILL_DETAIL.Free;
end;

(*==============================================================================
** Print As Format
**------------------------------------------------------------------------------
*)
procedure TexPrintDirect.PrintBlock(Block: TStrings; Values: array of Variant);
var
	i : Integer;
    s : String;
    x : TisValueList;

    (*==========================================================================
    ** Change Bill font
    ** Result:
    **			0: Normal
    **			1: New line
    **			2: Printer code
    **--------------------------------------------------------------------------
    *)
    function ControlCode(var s : String) : Boolean;
    var
        i, n: Integer;
        sBuf, sName, sVal: String;
    begin
        Result := False;

        if s = '' then
            Exit;
        if s[1] <> '@' then
            Exit;

        (*
        ** IF command
        ** Syntax:	IF(Name;Compare value; Select value)
        *)
        if SameText(Trim(isSubStr(s, 2, 2)), 'IF') then
        begin
	        i := Pos('(', s);
    	    if i <= 0 then
        	    Exit;
	        sBuf := isSubStr(s, i + 1);

            // Name
	        i := Pos(';', sBuf);
    	    if i <= 0 then
        	    Exit;
            sName := isLeft(sBuf, i - 1);
	        sBuf := isSubStr(sBuf, i + 1);

            // Compare Value
	        i := Pos(';', sBuf);
    	    if i <= 0 then
        	    Exit;
            sVal := isLeft(sBuf, i - 1);
	        sBuf := isSubStr(sBuf, i + 1);
            sBuf := copy(sBuf, 1, Length(sBuf) - 1);

            // Compare
            if x.ValueByName(sName).AsString = sVal then
			    Writeln(mBillFile, x.Format(sBuf));
		end;
        //else
        (*
        ** NIF command
        ** Syntax:	NIF(Name;Compare value; Select value)
        *)
        if SameText(Trim(isSubStr(s, 2, 3)), 'NIF') then
        begin
	        i := Pos('(', s);
    	    if i <= 0 then
        	    Exit;
	        sBuf := isSubStr(s, i + 1);

            // Name
	        i := Pos(';', sBuf);
    	    if i <= 0 then
        	    Exit;
            sName := isLeft(sBuf, i - 1);
	        sBuf := isSubStr(sBuf, i + 1);

            // Compare Value
	        i := Pos(';', sBuf);
    	    if i <= 0 then
        	    Exit;
            sVal := isLeft(sBuf, i - 1);
	        sBuf := isSubStr(sBuf, i + 1);
            sBuf := copy(sBuf, 1, Length(sBuf) - 1);

            // Compare
            if x.ValueByName(sName).AsString <> sVal then
			    Writeln(mBillFile, x.Format(sBuf));
		end;
        //else
        (*
        ** New line
        *)
        if SameText(Trim(isSubStr(s, 2)), 'NEWLINE') then
        begin
			Writeln(mBillFile);
        end;
        //else
        begin
            // Printer Code
            i := Pos(';', s);
            if i <= 0 then
                Exit;

            n := StrToIntDef(isSubStr(s, i + 1), 0);
            s := isSubStr(s, 2, i - 2);

            with Printer.Canvas.Font do
            begin
                Name := s;
                Size := n;
            end;
        end;

        Result := True;
    end;

begin
    x := TisValueList.Create(Values);
    with Block do
	    for i := 0 to Count - 1 do
        begin
        	s := Strings[i];
            if not ControlCode(s) then
			    Writeln(mBillFile, x.Format(s));
        end;
    x.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintDirect.GetBillDetail(const khoa: TGUID): Integer;
begin
    with POS_BILL_DETAIL do
    begin
    	if Active then
            Close;

        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := TGuidEx.ToString(khoa);
        Parameters[3].Value := sysLoc;
        Open;

        Result := FieldCount;
    end;
end;

function TexPrintDirect.GetBillHeaderValues(const khoa: TGUID): Boolean;
var
	i, k, n: Integer;
begin
    mCopy := 1;
	with POS_BILL_MASTER do
    begin
    	if Active then
            Close;

        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := TGuidEx.ToString(khoa);
        Parameters[3].Value := sysLoc;
        ExecProc;
        Result := Parameters[0].Value = 0;
        if not Result then
        begin
            Close;
            Exit;
        end;
        Active := True;

        // Number of fields
        n := FieldCount;
        k := 3;
        mBillHeaderValues[1] := mCopy;
        for i := 0 to n - 1 do
        begin
			mBillHeaderValues[k] := Fields[i].Value;
            Inc(k, 2);
        end;

        Close;
    end;
end;

(*==============================================================================
** Billing engine old method
**------------------------------------------------------------------------------
*)
function TexPrintDirect.BillHeader(Values: array of Variant): Boolean;
begin
	if PrintPreview then
		AssignFile(mBillFile, sysAppPath + 'tam.txt')
    else
		AssignPrn(mBillFile);

    Rewrite(mBillFile);

    try
	    PrintBlock(mBillHeader, Values);
        Result := True;
    except
        Result := False;
    end;
end;

procedure TexPrintDirect.BillDetail(Values: array of Variant);
begin
    try
	    PrintBlock(mBillDetail, Values)
    except
        on E: Exception do
            Raise Exception.Create('Print BillDetail Error: ' + E.Message);
    end;
end;

procedure TexPrintDirect.BillFooter(Values: array of Variant);
begin
    try
        PrintBlock(mBillFooter, Values);
    finally
        CloseFile(mBillFile);
    end;
end;

(*==============================================================================
** Billing engine new method
**------------------------------------------------------------------------------
*)
	(*
    ** Header
    *)
function TexPrintDirect.BillHeader(const khoa: TGUID): Boolean;
begin
	 Result := GetBillHeaderValues(khoa);
     if Result then
	    Result := BillHeader(mBillHeaderValues);
end;

	(*
    ** Detail
    *)
procedure TexPrintDirect.BillDetail;
var
	i, k, n: Integer;
begin
    with POS_BILL_DETAIL do
    begin
        n := FieldCount;
        First;
        // Scan and print
        while not Eof do
        begin
            // Get fields' values
            k := 1;
            for i := 0 to n - 1 do
            begin
				mBillDetailValues[k] := Fields[i].Value;
            	Inc(k, 2);
            end;

            // Print
			BillDetail(mBillDetailValues);

            // Next record
        	Next;
        end;
    end;
    SetNullVariant(arrVariant(mBillDetailValues));
end;

procedure TexPrintDirect.BillDetail(const khoa: TGUID);
begin
    GetBillDetail(khoa);
    BillDetail;
	POS_BILL_DETAIL.Close;
end;

	(*
    ** Footer
    *)
procedure TexPrintDirect.BillFooter;
begin
	BillFooter(mBillHeaderValues);
    SetNullVariant(arrVariant(mBillHeaderValues));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintDirect.PrintAccu(Values: array of Variant);
begin
	if PrintPreview then
		AssignFile(mbillFile, sysAppPath + 'doanhso.txt')
    else
		AssignPrn(mbillFile);

    Rewrite(mbillFile);
	PrintBlock(mAccContent, Values);
    CloseFile(mbillFile);
end;

{ TexPrintBill }
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TexPrintBill.BillInit;
var
    s: string;
begin
    try
        FPreview := GetSysParam('BILL_PREVIEW');
    except
        FPreview := False;
    end;
    FPrinterName := '';
    FPrinterI := -1;
    //Lay chon may in.
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        s := ReadString('COMMON', 'PosPrinter_' + isGetComputerName, '');
        if s = '' then
            s := ReadString('COMMON', 'PosPrinter', '');
        Free;
    end;
    if s <> '' then
        BillSetPrinter(FPrinterName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintBill.BillPrint(const pKhoa: TGUID; const pCopy: Integer): Boolean;
begin
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintBill.BillSetPrinter(const pPrinterName: string): Boolean;
var
    i: Integer;
begin
    i := Printer.Printers.IndexOf(pPrinterName);
    Result := i >= 0;
    if not Result then
        Exit;

    FPrinterI := i;
    FPrinterName := pPrinterName;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exBillInitial(var pPrintBill: TexPrintBill; pFileName: String);
var
    mType: Integer;
    mFile, s, mComName, mRepDef: String;
begin
    mType := -1;
    if pFileName <> '' then
        mFile := pFileName
    else
        with TADOQuery.Create(Nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text :=
                'select b.* from DM_QUAYTN a, PRINTERS b where a.PRINTER = b.PRINTER and a.TENMAY=' +
                    QuotedStr(isGetComputerName);

            try
                Open;

                if IsEmpty then
                    raise Exception.Create('Chưa cấu hình máy in.');

                mFile := Trim(FieldByName('BILL_HEADER').AsString);

                if FindField('PRINTER_TYPE') <> nil then
                    mType := FieldByName('PRINTER_TYPE').AsInteger;
            except
                on E: Exception do
                    raise Exception.Create('PRINTER message: ' + E.Message);
            end;

            Close;
            Free;
        end;
    try

        if mType < 0 then
        begin
            s := ExtractFileExt(mFile);
            if SameText(s, '.RPT') or SameText(s, '.SZR') then
                mType := 1
            else if SameText(s, '.FR3') then
                mType := 2
            else
                mType := 0;
        end;

        case mType of
        0:
            pPrintBill := TexPrintDirect.Create('RP_POSZ_TEXT_MASTER', 'RP_POSZ_TEXT_DETAIL');
        1:
        begin
            pPrintBill := TexPrintRpt.Create(ChangeFileExt(mFile, ''));
        end;
        2:
        begin
            pPrintBill := TexPrintFRpt.Create(ChangeFileExt(mFile, ''));
        end
        else
            pPrintBill := TexPrintBill.Create;
        end;
        pPrintBill.PrintType := mType;
    except

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exBillFinal;
begin
    pPrintBill.Free;
end;

{ TexPrintRpt }
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintRpt.BillPrint(const pKhoa: TGUID; const pCopy: Integer): Boolean;
begin
    if FPreview then
        Result := ShowReport('Bill', FFileName, [sysLogonUID, TGuidEx.ToStringEx(pKhoa), sysLoc])
    else
        Result := PrintReport(FFileName, [sysLogonUID, TGuidEx.ToStringEx(pKhoa), sysLoc], FPrinterName, pCopy);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TexPrintRpt.Create(const pFileName: String);
begin
    BillInit;
    FFileName := pFileName;
end;

{ TexPrintFRpt }
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TexPrintFRpt.BillPrint(const pKhoa: TGUID; const pCopy: Integer): Boolean;
begin
    Result := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TexPrintFRpt.Create(const pFileName: String);
begin
    BillInit;
    FFileName := pFileName;
end;

end.
