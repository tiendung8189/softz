object FrmChonDsVIP: TFrmChonDsVIP
  Left = 847
  Top = 241
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n Danh S'#225'ch Kh'#225'ch H'#224'ng Th'#226'n Thi'#7871't'
  ClientHeight = 414
  ClientWidth = 502
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 114
    Height = 16
    Caption = '&Nh'#7853'p chu'#7895'i c'#7847'n t'#236'm:'
    FocusControl = EdMaSearch
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object CmdReturn: TBitBtn
    Left = 296
    Top = 380
    Width = 97
    Height = 25
    Cursor = 1
    Caption = 'Ti'#7871'p t'#7909'c'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 1
    ParentFont = False
    TabOrder = 5
  end
  object BtnCancel: TBitBtn
    Left = 396
    Top = 380
    Width = 97
    Height = 25
    Cursor = 1
    Cancel = True
    Caption = 'K'#7871't th'#250'c'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 6
  end
  object EdMaSearch: TwwIncrementalSearch
    Left = 8
    Top = 24
    Width = 485
    Height = 24
    DataSource = DsDM
    SearchField = 'MAVIP'
    OnPerformCustomSearch = EdMaSearchPerformCustomSearch
    TabOrder = 0
    OnKeyPress = EdMaSearchKeyPress
  end
  object GrListSearch: TwwDBGrid2
    Left = 8
    Top = 50
    Width = 485
    Height = 163
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MAVIP'#9'15'#9'M'#227' VIP'#9'F'
      'HOTEN'#9'23'#9'H'#7885' t'#234'n'#9'F'
      'DTHOAI'#9'15'#9#272'i'#7879'n tho'#7841'i'#9'F'
      'MASO'#9'10'#9'M'#227' s'#7889#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = DsDM
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnDblClick = CmdInsExecute
    OnColumnMoved = GrListSearchColumnMoved
    TitleImageList = DataMain.ImageSort
  end
  object BitBtn1: TBitBtn
    Left = 186
    Top = 218
    Width = 53
    Height = 25
    Cursor = 1
    Action = CmdIns
    Glyph.Data = {
      36060000424D3606000000000000360400002800000020000000100000000100
      08000000000000020000530B0000530B00000001000000000000000000003300
      00006600000099000000CC000000FF0000000033000033330000663300009933
      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
      000000990000339900006699000099990000CC990000FF99000000CC000033CC
      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
      330000333300333333006633330099333300CC333300FF333300006633003366
      33006666330099663300CC663300FF6633000099330033993300669933009999
      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
      66006600660099006600CC006600FF0066000033660033336600663366009933
      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
      660000996600339966006699660099996600CC996600FF99660000CC660033CC
      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
      990000339900333399006633990099339900CC339900FF339900006699003366
      99006666990099669900CC669900FF6699000099990033999900669999009999
      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
      000000808000800000008000800080800000C0C0C00080808000191919004C4C
      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
      E809E8E8E8E8E8E8E8E8E8E8E8E8E8E8E881E8E8E8E8E8E8E8E8E8E8E8E8E8E8
      091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
      1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
      101010101009E8E8E8E8E8E8E881ACACACACACACAC81E8E8E8E8E8E809101010
      10101010101009E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E809101010
      10101010101009E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E809090909
      10101009090909E8E8E8E8E881818181ACACAC81818181E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      09090909E8E8E8E8E8E8E8E8E8E8E88181818181E8E8E8E8E8E8E8E8E8E8E8E8
      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
    NumGlyphs = 2
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 278
    Top = 218
    Width = 53
    Height = 25
    Cursor = 1
    Action = CmdDel
    Glyph.Data = {
      36060000424D3606000000000000360400002800000020000000100000000100
      08000000000000020000530B0000530B00000001000000000000000000003300
      00006600000099000000CC000000FF0000000033000033330000663300009933
      0000CC330000FF33000000660000336600006666000099660000CC660000FF66
      000000990000339900006699000099990000CC990000FF99000000CC000033CC
      000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
      0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
      330000333300333333006633330099333300CC333300FF333300006633003366
      33006666330099663300CC663300FF6633000099330033993300669933009999
      3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
      330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
      66006600660099006600CC006600FF0066000033660033336600663366009933
      6600CC336600FF33660000666600336666006666660099666600CC666600FF66
      660000996600339966006699660099996600CC996600FF99660000CC660033CC
      660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
      6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
      990000339900333399006633990099339900CC339900FF339900006699003366
      99006666990099669900CC669900FF6699000099990033999900669999009999
      9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
      990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
      CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
      CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
      CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
      CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
      CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
      FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
      FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
      FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
      FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
      000000808000800000008000800080800000C0C0C00080808000191919004C4C
      4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
      6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809
      09090909E8E8E8E8E8E8E8E8E8E8E88181818181E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E809090909
      10101009090909E8E8E8E8E881818181ACACAC81818181E8E8E8E8E809101010
      10101010101009E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E809101010
      10101010101009E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8091010
      101010101009E8E8E8E8E8E8E881ACACACACACACAC81E8E8E8E8E8E8E8E80910
      1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
      10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8E8E8
      091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8E8E8E8E8E8
      E809E8E8E8E8E8E8E8E8E8E8E8E8E8E8E881E8E8E8E8E8E8E8E8E8E8E8E8E8E8
      E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
    NumGlyphs = 2
    TabOrder = 4
  end
  object GrList: TStringGrid
    Left = 8
    Top = 248
    Width = 485
    Height = 123
    Hint = 'X'#243'a danh s'#225'ch (F3)'
    ColCount = 2
    DefaultColWidth = 120
    DefaultRowHeight = 19
    FixedCols = 0
    RowCount = 50
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    ScrollBars = ssVertical
    TabOrder = 2
    OnDblClick = CmdDelExecute
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_VIP'
      'where isnull(TINHTRANG, '#39'01'#39') <> '#39'01'#39
      'order by HOTEN')
    Left = 100
    Top = 120
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 128
    Top = 120
    object CmdIns: TAction
      Hint = 'Th'#234'm v'#224'o danh s'#225'ch'
      ShortCut = 113
      OnExecute = CmdInsExecute
    end
    object CmdDel: TAction
      OnExecute = CmdDelExecute
    end
    object CmdClear: TAction
      Caption = 'X'#243'a danh s'#225'ch'
      ShortCut = 114
      OnExecute = CmdClearExecute
    end
  end
  object DsDM: TDataSource
    DataSet = QrDM
    Left = 99
    Top = 148
  end
end
