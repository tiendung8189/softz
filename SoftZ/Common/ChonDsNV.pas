(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsNV;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Db, ADODB, Buttons, Grids,
  Wwdbigrd, Wwdbgrid2, ActnList, Wwkeycb, Wwdbgrid;

type
  TFrmChonDsNV = class(TForm)
    QrDM: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClear: TAction;
    DsDM: TDataSource;
    Label1: TLabel;
    EdMaSearch: TwwIncrementalSearch;
    GrListSearch: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GrList: TStringGrid;
    GrListSearchIButton: TwwIButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure GrListSearchColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure EdMaSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EdMaSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);
  private

  public
  	function  Get(var pLst: String): Boolean;
  end;

var
  FrmChonDsNV: TFrmChonDsNV;

implementation

uses
	isDb, isLib, isStr;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DSNV', GrListSearch);
	OpenDataSets([QrDM]);
    EdMaSearch.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsNV.Get;
var
	i: Integer;
begin
    Result := ShowModal = mrOK;
    if Result then
    begin
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
    with QrDM do
    begin
        s1 := FieldByName('MANV').AsString;
        s2 := FieldByName('TENNV').AsString
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
    s := EdMaSearch.Text;
    CmdIns.Enabled := not QrDM.IsEmpty;
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.FormCreate(Sender: TObject);
begin
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDM]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.GrListSearchColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
    s: String;
begin
    s := GrListSearch.Columns[0].FieldName;
    EdMaSearch.SearchField := s;
    QrDM.Sort := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.EdMaSearchKeyPress(Sender: TObject; var Key: Char);
var
	n: Integer;
    s: String;
begin

    if Key = #13 then
    begin
        s := EdMaSearch.Text;

        // Quick select?
        n := IsDotSelect(s);
        if n <> 0 then
            s := isDoubleChr(isSubStr(EdMaSearch.Text, 2), '''');

        with QrDM do
        begin
            if s = '' then
                Filter := ''
            else
                case n of
                2:
                	Filter := GrListSearch.Columns[1].FieldName + ' like ''%' + s + '%''';
                1:
                	Filter := GrListSearch.Columns[0].FieldName + ' like ''' + s + '%''';
                end;
//            Refresh;
            First;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNV.EdMaSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

end.
