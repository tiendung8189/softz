﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit MainData;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Db, ADODB, Wwintl, isEnv,
  AdvMenuStylers, AdvMenus, ImgList, isLogin, NativeXml;

type
  TDataMain = class(TDataModule)
    Conn: TADOConnection;
    QrDMKHO: TADOQuery;
    QrUSER: TADOQuery;
    QrDMKH: TADOQuery;
    QrTEMP: TADOQuery;
    QrPTNHAP: TADOQuery;
    QrLYDO_NK: TADOQuery;
    QrLYDO_XK: TADOQuery;
    QrPTXUAT: TADOQuery;
    QrDM_DVT: TADOQuery;
    QrLOAI_NCC: TADOQuery;
    QrLYDO_CHIPHI: TADOQuery;
    QrLOAI_KH: TADOQuery;
    QrLYDO_THU: TADOQuery;
    QrLYDO_CHI: TADOQuery;
    QrDMVIP: TADOQuery;
    QrMAU: TADOQuery;
    QrSIZE: TADOQuery;
    QrTT_DDH: TADOQuery;
    QrDMVT_TINHTRANG: TADOQuery;
    CALC_STOCK: TADOCommand;
    QrNGOAITE: TADOQuery;
    ALLOC_MADT: TADOCommand;
    QrDMNXB: TADOQuery;
    QrDM_NHOM: TADOQuery;
    QrDM_NHOM2: TADOQuery;
    QrDMTT_NHAPTRABL: TADOQuery;
    QrPTTT: TADOQuery;
    QrDMLOAITHUE: TADOQuery;
    ALLOC_SCT: TADOCommand;
    QrLABEL: TADOQuery;
    EXPORT_LABEL: TADOStoredProc;
    QrDMVT: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    CheckPrintStamp: TADOCommand;
    isEnv1: TisEnv;
    ImageMark: TImageList;
    ImageSort: TImageList;
    ImageSmall: TImageList;
    wwIntl: TwwIntl;
    ImageNavi: TImageList;
    ImageOrg: TImageList;
    QrLOAI_VIP_LE: TADOQuery;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    QrDMTACGIA: TADOQuery;
    QrHINHTHUC_GIA: TADOQuery;
    SYS_SEQ_GUID: TADOCommand;
    QrTT_BANLE: TADOQuery;
    QrPTNHAPTRA: TADOQuery;
    QrPTXUATTRA: TADOQuery;
    QrDMTK: TADOQuery;
    spUPDATE_PRINT_NO: TADOCommand;
    QrDM_NGANH: TADOQuery;
    QrLOC: TADOQuery;
    GETACCESS: TADOCommand;
    GETSEQ: TADOCommand;
    isLogon: TisLogon;
    GET_LOGON: TADOStoredProc;
    SYS_LOGOUT: TADOStoredProc;
    SETPASS: TADOStoredProc;
    QrLOAI_DT: TADOQuery;
    QrLYDO_TK: TADOQuery;
    QrLYDO_CK: TADOQuery;
    QrDMPB: TADOQuery;
    QrLOAI_KH_NCC: TADOQuery;
    QrLOAI_VIP_SI: TADOQuery;
    ALLOC_BARCODE: TADOCommand;
    DMVT_EXISTS_CHUNGTU: TADOCommand;
    QrDMNCC: TADOQuery;
    QrDMMENHGIA: TADOQuery;
    QrSql: TADOQuery;
    QrPTKETCHUYEN: TADOQuery;
    SYS_SEQ_SHA1: TADOCommand;
    QrDMTK_DT: TADOQuery;
    QrDMTK_NV: TADOQuery;
    QrDMTK_NB: TADOQuery;
    QrLOAI_KHUYENMAI: TADOQuery;
    QrNganhang: TADOQuery;
    QrNganhangCN: TADOQuery;
    QrLYDO_KM: TADOQuery;
    fnStripToneMark: TADOCommand;
    QrDM_CAMUNG: TADOQuery;
    QrDMNSX: TADOQuery;
    QrTT_VIP: TADOQuery;
    fnStripTentat: TADOCommand;
    spCHUNGTU_GET_REF: TADOStoredProc;
    fnTinhTL_LAI: TADOCommand;
    spCHECK_INSERT: TADOCommand;
    spCHECK_EDIT: TADOCommand;
    spCHECK_DELETE: TADOCommand;
    spPQT_PHATHANH_CT: TADOCommand;
    spPQT_HUYTHE_CT: TADOCommand;
    ALLOC_UID: TADOCommand;
    QrV_POM_DM_CONGTY: TADOQuery;
    QrV_POM_DMVT_TINHTRANG: TADOQuery;
    QrV_POM_NVL_TINHCHAT: TADOQuery;
    QrPOM_DM_NVL: TADOQuery;
    QrPOM_DM_BTP: TADOQuery;
    QrPOM_DMHH: TADOQuery;
    spPOM_UPDATE_BTP: TADOCommand;
    QrDM_MAYCC: TADOQuery;
    QrQuocgia: TADOQuery;
    QrTinh: TADOQuery;
    QrHuyen: TADOQuery;
    QrSite: TADOQuery;
    QrPOM_DM_HESO_DODAM: TADOQuery;
    QrPOM_DM_HESO_DUNGTICH: TADOQuery;
    spPOM_CAPNHAT_BANG_GIATHANH: TADOCommand;
    QrPOM_DM_CHIPHI: TADOQuery;
    QrPOM_DM_CHIPHI_NHOM: TADOQuery;
    QrV_POM_CHIPHI_LOAI: TADOQuery;
    spPOM_CAPNHAT_BANG_CHIPHI_CHUNG: TADOCommand;
    spPOM_DMVT_EXISTS_CHUNGTU: TADOCommand;
    QrREP_PARAM: TADOQuery;
    QrTS_LOAI_DT: TADOQuery;
    QrV_FB_DMVT_TINHTRANG: TADOQuery;
    spFB_DMVT_EXISTS_CHUNGTU: TADOCommand;
    QrFB_DM_CHIPHINHOM: TADOQuery;
    QrFB_DM_CHIPHI: TADOQuery;
    QrDM_CHIPHI: TADOQuery;
    QrDM_CHIPHINHOM: TADOQuery;
    spFB_CHUNGTU_GET_REF: TADOStoredProc;
    QrFB_DM_NPL: TADOQuery;
    QrFB_DM_THUCDON: TADOQuery;
    QrFB_DM_HH: TADOQuery;
    QrFB_LYDO_NK: TADOQuery;
    QrFB_LYDO_XK: TADOQuery;
    FB_CALC_STOCK: TADOCommand;
    spFB_UPDATE_HH: TADOCommand;
    QrFB_DM_NPL_THUCDON: TADOQuery;
    QrFB_TT_DDH: TADOQuery;
    ALLOC_SCT2: TADOCommand;
    sp_CATEGORY_ACCOUNT_CHARGE_POS_USED: TADOCommand;
    spLAY_1BANLE_FB: TADOStoredProc;
    spAfterPostPhieu: TADOCommand;
    QrDmQuocGia: TADOQuery;
    ImageEditButton: TImageList;
    DsV_QuocGia: TDataSource;
    QrSYS_APP: TADOQuery;
    ImageStatus: TImageList;
    DsTINH: TDataSource;
    DsHUYEN: TDataSource;
    QrPhuongXa: TADOQuery;
    DsPHUONGXA: TDataSource;
    DsNGOAITE: TDataSource;
    DsQuocGia: TDataSource;
    DsNganhang: TDataSource;
    DsNganhangCN: TDataSource;
    QrResource: TADOQuery;
    DsDM_CAMUNG: TDataSource;
    DsDMNCC: TDataSource;
    DsDM_DVT: TDataSource;
    DsDMVT_TINHTRANG: TDataSource;
    DsDMLOAITHUE: TDataSource;
    DsDM_KH_NCC: TDataSource;
    DsDMKHO: TDataSource;
    DsLYDO_NK: TDataSource;
    DsLYDO_XK: TDataSource;
    DsPTNHAP: TDataSource;
    DsHINHTHUC_GIA: TDataSource;
    DsPTXUAT: TDataSource;
    DsDMKH: TDataSource;
    DsPTNHAPTRA: TDataSource;
    DsPTXUATTRA: TDataSource;
    DsTT_DDH: TDataSource;
    DsPTTT: TDataSource;
    DsLYDO_THU: TDataSource;
    DsLOC: TDataSource;
    DsDMTK_NB: TDataSource;
    DsLYDO_TK: TDataSource;
    DsLYDO_CHI: TDataSource;
    DsLYDO_CK: TDataSource;
    DsDM_CHIPHI: TDataSource;
    DsPTKETCHUYEN: TDataSource;
    QrLOAI_VIDIENTU: TADOQuery;
    QrDMQUAY: TADOQuery;
    DsUser: TDataSource;
    DsLOAI_VIP_SI: TDataSource;
    DsLOAI_VIP_LE: TDataSource;
    DsTT_VIP: TDataSource;
    DsV_FB_DMVT_TINHTRANG: TDataSource;
    DsLOAI_KHUYENMAI: TDataSource;
    DsLYDO_KM: TDataSource;
    QrV_PUB_PHANHE_VIP_ALL: TADOQuery;
    DsV_PUB_PHANHE_VIP_ALL: TDataSource;
    QrV_PUB_PHANHE_VIP_FB: TADOQuery;
    DsV_PUB_PHANHE_VIP_FB: TDataSource;
    QrV_PUB_PHANHE_VIP_SZ: TADOQuery;
    DsV_PUB_PHANHE_VIP_SZ: TDataSource;
    DsV_PUB_PHANHE_VIP_KH: TDataSource;
    QrV_PUB_PHANHE_VIP_KH: TADOQuery;
    DsV_PUB_PHANHE_VIP: TDataSource;
    QrV_PUB_PHANHE_VIP: TADOQuery;
    DsFB_LYDO_XK: TDataSource;
    DsFB_LYDO_NK: TDataSource;
    DsLOAI_VIDIENTU: TDataSource;
    QrBANLE_TAIKHOAN: TADOQuery;
    DsBANLE_TAIKHOAN: TDataSource;
    DsDM_NHOM2: TDataSource;
    QrDDH_LyDoMoDuyet: TADOQuery;
    DsDDH_LyDoMoDuyet: TDataSource;
    QrDM_LYDO_DIEUCHINH: TADOQuery;
    DsDM_LYDO_DIEUCHINH: TDataSource;
    DsDM_LYDO_DONGPHIEU: TDataSource;
    QrDM_LYDO_DONGPHIEU: TADOQuery;
    QrTT_DieuKho: TADOQuery;
    procedure ConnWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrPOM_DM_HESO_DUNGTICHAfterOpen(DataSet: TDataSet);
    procedure QrPOM_DM_HESO_DODAMAfterOpen(DataSet: TDataSet);
    procedure QrFB_DM_NPLAfterOpen(DataSet: TDataSet);
    procedure QrFB_DM_HHAfterOpen(DataSet: TDataSet);
  private
  public
    procedure InitParams(const subsys: Integer = 0);
    function  Logon(const subsys: Integer = 0): Boolean;
    procedure CheckUpdate;
    function  GetLatestVersion: String;

    function  ExecSQL(const sqlStr: String): Integer;
    function  GetNewGuid: TGuid;
    function  GetNewSHA1: String;
    function  GetOutletInfo(var pQuay, pMakho: String): Boolean;

    procedure SetSysRepParam(const luot: TGUID; const ListField: array of String; const ListValue: array of Variant);
    function  GetSeqValue(seq: String): Integer;

    function  GetUIDByUserName(pUserName: String ): Integer;
    function  GetUserNameByUID(uid: Integer): String;
    function  GetFullNameByUID(uid: Integer): String;
    function  GetManvByUID(uid: Integer): String;

    // In tem - lable
    procedure LabelOpen;
    procedure LabelAdd(mabh: String; soluong: Double; ngay: TDateTime);
    function  LabelClose(filename: String; addhdr, overwrite: Boolean): Boolean;
    function  LabelClose2: Boolean;
    function  LabelCloseFR(filename: String): Boolean;
    function  LabelExport(khoa: String; fTable: String = 'CHUNGTU'; fName: String = 'SOLUONG'): Boolean;

    function  IsPrintStamp(pMavt: String): Boolean;

    //SoftZ
    function  AllocEAN(mPrefix, nhom: String): String;
    function  BarcodeIsUsed(pMABH: String; bDMHH: Boolean = False): Boolean;
    function  CheckSlKho(pMsg: Boolean = True): Boolean;
	procedure CalcStock(pDate: TDateTime);
    function  StripToneMark(s: String): String;
    function  StripTentat(s: String): String;
    function  TinhTL_LAI(_pGianhap, _pGiaban: Double): Double;

    function  AllocMADT(pLen, pType: Integer; pPrefix: String = ''): String;
    procedure AllocSCT(lct: String; qr: TADOQuery; AFIELDNAME: String = 'LOC');
    procedure AllocSCT2(lct: String; qr: TADOQuery; column: String = 'SCT2');
    function  AllocUID(pSEQ_NAME: String = 'SYS_USER'): Integer;

    procedure UpdatePrintNo(pKhoa: TGUID);

    //Hr
    function  GetPassCode(pMakho: String): String;
    function  GetPeriodStatus(m, y: Integer; t: Integer): Boolean;
    function  GetPeriodDate(pYear, pMon: Integer; var pFromDate: TDateTime; var pToDate: TDateTime): Boolean;
   

    //POM
    function  POM_BarcodeIsUsed(pMABH: String): Boolean;
    procedure POM_UpdateBTP(pMavt: string);
    function  POM_ThongsoTinhtrang(m, y: Integer; t: Integer = 0): Boolean;
    function  POM_UpdateGiathanh(m, y: Integer): Integer;
    function  POM_UpdateChiphiChung(m, y: Integer): Integer;

    //Xml
    function  ListTables(pType: Integer; ls: TStrings): Boolean;
    procedure CopyData(qrSource, qrDest: TADOQuery);

    function  IsCheckDelete(_pKhoa: TGUID; _pFORM_CODE: String; _pMsg: Boolean = True): Boolean;
    function  IsCheckInsert(_pKhoa: TGUID; _pFORM_CODE: String; _pMsg: Boolean = True): Boolean;
    function  IsCheckEdit(_pKhoa: TGUID; _pFORM_CODE: String; _pMsg: Boolean = True): Boolean;

    function  PhathanhPQT_CT(_pMathe: String; _pMsg: Boolean = True): Boolean;
    function  HuyPhathanhPQT_CT(_pMathe: String; _pMsg: Boolean = True): Boolean;


    //FB
    function  FB_BarcodeIsUsed(pMABH: String): Boolean;
    procedure FB_CalcStock(pDate: TDateTime);
    procedure FB_UpdateHH(pMavt: string);

    { Chung }
    function CategoryAccountChargePosUsed(pPLOAI, pMATK: String): Boolean;
    procedure UpdateAfterPostPhieu(pKhoa: TGUID; pLCT: string);
  end;

var
  DataMain: TDataMain;

implementation

uses
	ExCommon, Rights, isMsg, isDb, Variants, isCommon, GuidEx, RepEngine, isSystem,
    isOdbc, isStr, isType, isLib, isEnCoding, isFile, FastReport, isLic;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    NOT_CONFIG_YET = 'Chưa cấu hình số hiệu máy bán hàng.';
    NOT_CONFIG_SYSLOC = 'Chưa cấu hình Chinh nhánh. Kết thúc chương trình.';
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

function TDataMain.Logon(const subsys: Integer): Boolean;
begin
	Result := False;
//    if not exGetDba then
//        Exit;

//    if not DbConnect then
//        Exit;

//    if not ServerLicenseCheck then
//    	Exit;

//    if CheckSlKho(True) then
//        Exit;

    DataMain.Conn.Close;

  // Logon
    with isLogon do
    begin
        if not Logon2(RegReadString('', Iif(subsys = 1, 'LastLogonUserPOS' ,'LastLogonUser'), '')) then
        begin
            Exit;
        end;
    end;

    sysDbConnection := Conn;

    Screen.Cursor := crHourGlass;
    CheckUpdate;
    Screen.Cursor := crDefault;

    if not IsLicenseCheck then
        Exit;

    InitParams(subsys);
    InitCommonVariables;
    sysIsAdmin := sysLogonUser = 'ADMIN';
    sysLogonManv := GetManvByUID(sysLogonUID);
//    if sysIsAdmin then
//        DataAdmin.Initial;

    if sysLoc = '' then
    begin
        ErrMsg(NOT_CONFIG_SYSLOC);
        Exit;
    end;

    if not sysIsAdmin  then
        RegWrite('', Iif(subsys = 1, 'LastLogonUserPOS' ,'LastLogonUser'), sysLogonUser);

    sysIsChecked := GetFuncState('SZ_CHECKED');
    exInitDotMavt;
    exInitDotFBMavt;
    exInitDotPomMavt;
	wwIntl.Connected := not sysEnglish;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.PhathanhPQT_CT(_pMathe: String; _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spPQT_PHATHANH_CT do
    begin
        Prepared := True;
        Parameters[1].Value := _pMathe;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[2].Value;
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.POM_UpdateBTP(pMavt: string);
begin
    with spPOM_UPDATE_BTP do
    begin
        Prepared := True;
        Parameters[1].Value := pMavt;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CAPNHAT_DULIEU_TU_THANGTRUOC = 'Có cập nhật dữ liệu tháng trước qua không?';
function TDataMain.POM_UpdateChiphiChung(m, y: Integer): Integer;
var
    b: Boolean;
begin
    b := YesNo(RS_CAPNHAT_DULIEU_TU_THANGTRUOC, 1);

    try
        with spPOM_CAPNHAT_BANG_CHIPHI_CHUNG do
        begin
            Parameters.Refresh;
            Parameters[1].Value := y;
            Parameters[2].Value := m;
            Parameters[3].Value := sysLogonUID;
            Parameters[4].Value := b;
            Execute;
            Result := Parameters[0].Value
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.POM_UpdateGiathanh(m, y: Integer): Integer;
begin
    try
        with spPOM_CAPNHAT_BANG_GIATHANH do
        begin
            Parameters.Refresh;
            Parameters[1].Value := y;
            Parameters[2].Value := m;
            Parameters[3].Value := sysLogonUID;
            Execute;
            Result := Parameters[0].Value
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.QrFB_DM_HHAfterOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        SetDisplayFormat(DataSet as TADOQuery, sysCurFmt);
    end;
end;

procedure TDataMain.QrFB_DM_NPLAfterOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        SetDisplayFormat(DataSet as TADOQuery, sysCurFmt);
    end;
end;

procedure TDataMain.QrPOM_DM_HESO_DODAMAfterOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        SetDisplayFormat(DataSet as TADOQuery, sysQtyFmt);
        SetDisplayFormat(DataSet as TADOQuery, ['HESO'], sysFloatFmtOne);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.QrPOM_DM_HESO_DUNGTICHAfterOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        SetDisplayFormat(DataSet as TADOQuery, sysQtyFmt);
        SetDisplayFormat(DataSet as TADOQuery, ['HESO'], sysFloatFmtOne);
        SetDisplayFormat(DataSet as TADOQuery, ['DUNGTICH'], sysFloatFmtTwo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.UpdatePrintNo(pKhoa: TGUID);
begin
    with spUPDATE_PRINT_NO do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.InitParams;
var
	s: String;
    yy, mm, dd: WORD;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select * from SYS_CONFIG';
        Open;

        sysDesc1 := FieldByName('HEADER1').AsString;
		sysDesc2 := FieldByName('HEADER2').AsString;
		sysDesc3 := FieldByName('HEADER3').AsString;

        if subsys = 0 then
        begin
            sysCurFmt := FieldByName('FMT_GMS_CUR').AsString;
			sysQtyFmt := FieldByName('FMT_GMS_QTY').AsString;
            sysPriceFmt := FieldByName('FMT_GMS_PRICE').AsString;

	        s := FieldByName('FMT_GMS_THOUSAND_SEP').AsString;
            if s <> '' then
				ThousandSeparator := s[1];

            s := FieldByName('FMT_GMS_DECIMAL_SEP').AsString;
            if s <> '' then
				DecimalSeparator := s[1];
        end
        else
        begin
			sysCurFmt := FieldByName('FMT_POS_CUR').AsString;
			sysQtyFmt := FieldByName('FMT_POS_QTY').AsString;

	        s := FieldByName('FMT_POS_THOUSAND_SEP').AsString;
            if s <> '' then
				ThousandSeparator := s[1];

            s := FieldByName('FMT_POS_DECIMAL_SEP').AsString;
            if s <> '' then
				DecimalSeparator := s[1];
        end;
        sysPerFmt := FieldByName('FMT_PER').AsString;
        sysTaxFmt := FieldByName('FMT_TAX').AsString;

        sysCurRound := FieldByName('ROUNDING_CUR').AsInteger;
        sysCurPomRound := -14;
        sysCurHrRound := sysCurRound;
        sysNumRound := FieldByName('ROUNDING_NUM').AsInteger;

        sysTaxRound := FieldByName('ROUNDING_TAX').AsInteger;
        sysPerRound := FieldByName('ROUNDING_PER').AsInteger;
        sysFloatRoundOne := -1;

        sysIntFmt := '#,##0;-#,##0;#';
        sysFloatFmtOne := '#,##0.0;-#,##0.0;#';
        sysFloatFmtTwo := '#,##0.00;-#,##0.00;#';
        sysFloatFmtThree := '#,##0.000;-#,##0.000;#';
        sysFloatFmtFour := '#,##0.0000;-#,##0.0000;#';

        curHaohutRound := -2;
        curHaohutFmt := '#,##0.00;-#,##0.00;#';

        sysTinhlai := FieldByName('TINH_LAI').AsInteger;

        // Date format
        if subsys = 0 then
			s := FieldByName('FMT_GMS_DATE').AsString
        else
			s := FieldByName('FMT_POS_DATE').AsString;

        ShortDateFormat := s;
        TimeFormatSQL := 'hh:nn:ss';

        DateTimeFmt := s + ' ' + TimeFormatSQL;
        ShortDateFmtSQL := 'yyyy/mm/dd';
        DateTimeFmtSQL := ShortDateFmtSQL + ' ' + TimeFormatSQL;
        sysMinFmt := 'HH:nn';
        sysAppSource := 'DEK';
        sysLang := 'vi';

        InitFmtAndRoud;

        sysCloseHH := Trunc(FieldByName('KS_HH').AsDateTime);
        sysCloseCN := Trunc(FieldByName('KS_CN').AsDateTime);
        sysCloseFBHH := Trunc(FieldByName('FB_KS_HH').AsDateTime);

        sysDefKho := FieldByName('DEFAULT_MAKHO').AsString;
        sysLoc  := FieldByName('DEFAULT_LOC').AsString;
        sysSite  := FieldByName('DEFAULT_CHINHANH').AsString;
        sysIsCentral := FieldByName('IS_CENTRAL').AsBoolean;
        sysIsThue := FieldByName('IS_THUE_VAT').AsBoolean;
        sysRepPath := FieldByName('FOLDER_REPORT').AsString;
        sysHTGia := FieldByName('DEFAULT_HINHTHUC_GIA').AsString;
        sysHTGiaBL := FieldByName('DEFAULT_HINHTHUC_GIA_BL').AsString;

        sysLateDay := FieldByName('DEFAULT_LATE_DAY').AsInteger;
        sysPTTT := FieldByName('DEFAULT_PTTT').AsString;

        sysGiaCoVAT := '03';
        
        // Begin month year user Softz
        DecodeDate(Date, yy, mm, dd);
        sysMon  := mm;
        sysYear := yy;

        sysBegMon  := FieldByName('BEGIN_MONTH').AsInteger;
        sysBegYear := FieldByName('BEGIN_YEAR').AsInteger;
        sysBegDate := EncodeDate(sysBegYear, sysBegMon, 1);

        sysIsDrc   := FieldByName('IS_DRC').AsBoolean;
        sysIsHrChinhanh := FieldByName('IS_HR_CHINHANH').AsBoolean;
        sysIsDataAccess := FieldByName('IS_DATA_ACCESS').AsBoolean;

        if FindField('KHTT_LOAIVIP_LE') <> nil then
            sysKhttLe := FieldByName('KHTT_LOAIVIP_LE').AsInteger
        else
            sysKhttLe := 0;

        if FindField('KHTT_LOAIVIP_SI') <> nil then
            sysKhttSi := FieldByName('KHTT_LOAIVIP_SI').AsInteger
        else
            sysKhttSi := 0;

        soluongMin := 0;
        soluongMax := 99999;

        sysDDH_TT_NEW := 'DONDH_TINHTRANG.NEW';
        sysDDH_TT_WAIT := 'DONDH_TINHTRANG.WAIT';
        sysDDH_TT_APPR := 'DONDH_TINHTRANG.APPR';
        sysDDH_TT_PART := 'DONDH_TINHTRANG.PART';
        sysDDH_TT_DONE := 'DONDH_TINHTRANG.DONE';
        sysDDH_TT_CLOSE := 'DONDH_TINHTRANG.CLOSE';

        sysDIEUKHO_TT_NEW := 'DIEUKHO_TINHTRANG.NEW';
        sysDIEUKHO_TT_DONE := 'DIEUKHO_TINHTRANG.DONE';


        // Done
       	Close;
        Free;
    end;

    sysFuncTK := GetFuncState('ACC_DM_TAIKHOAN_NH');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.IsCheckDelete(_pKhoa: TGUID; _pFORM_CODE: String;
  _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spCHECK_DELETE do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(_pKhoa);
        Parameters[2].Value := _pFORM_CODE;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.IsCheckEdit(_pKhoa: TGUID; _pFORM_CODE: String;
  _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spCHECK_EDIT do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(_pKhoa);
        Parameters[2].Value := _pFORM_CODE;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.IsCheckInsert(_pKhoa: TGUID; _pFORM_CODE: String;
  _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spCHECK_INSERT do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(_pKhoa);
        Parameters[2].Value := _pFORM_CODE;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.IsPrintStamp(pMavt: String): Boolean;
begin
    with CheckPrintStamp do
    begin
        Parameters[0].Value := pMavt;
        Result := Execute.RecordCount > 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ConnWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
//	UserID := dbUser;
//    Password := dbPasswd;
//	ConnectionString := BuildConStr(ConnectionString);
    ConnectionString := GetConnectionString(isNameServer, isUserServer, isPasswordServer, isDatabaseServer);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleCreate(Sender: TObject);
begin
	CloseDataSets(Conn);
    Conn.Connected := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleDestroy(Sender: TObject);
begin
//	FreeReportEngine;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ExecSQL(const sqlStr: String): Integer;
var
	n: Integer;
begin
	with Conn do
        Execute(sqlStr, n);
    Result := n;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetLatestVersion: String;
var
    sVer: String;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select top 1 Version, VersionKey from SYS_VERSION where VersionKey = %s order by Version desc, VersionDate desc',
				[QuotedStr(sysAppName)]);

        Open;
        if IsEmpty then
            sVer := ''
        else
            sVer  := Fields[0].AsString;
		Result := sVer;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewGuid: TGuid;
begin
    with SYS_SEQ_GUID do
    begin
        Prepared := True;
        Execute;
        Result := TGuidEx.FromString(Parameters[1].Value);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewSHA1: String;
begin
    with SYS_SEQ_SHA1 do
    begin
        Prepared := True;
        Execute;
        Result := Parameters[1].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetOutletInfo;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select a.QUAY, a.MAKHO from DM_QUAYTN a, DM_KHO b where a.MAKHO = b.MAKHO and a.TENMAY = %s and b.LOC = %s',
				[QuotedStr(isGetComputerName), QuotedStr(sysLoc)]);

            SQL.Add(' order by case when a.MAKHO = (select top 1 DEFAULT_MAKHO from SYS_CONFIG) then 0 else 1 end');
        Open;
        pQuay  := Fields[0].AsString;
        pMakho := Fields[1].AsString;
		Result := (not IsEmpty) and (pQuay <> '') and (pMakho <> '');
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetPassCode(pMakho: String): String;
begin
    with TADOQuery.Create(Nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select THELENH from DM_LOCATION where LOC = ' + QuotedStr(pMakho);
        Open;
        Result := Fields[0].AsString;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.StripTentat(s: String): String;
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.StripToneMark(s: String): String;
begin
    with fnStripToneMark do
    begin
    	Prepared := True;
    	Parameters[1].Value := s;
        Execute;
    	Result := Parameters[0].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.TinhTL_LAI(_pGianhap, _pGiaban: Double): Double;
begin
    with fnTinhTL_LAI do
    begin
    	Prepared := True;
    	Parameters.ParamByName('@GIANHAP').Value := _pGianhap;
        Parameters.ParamByName('@GIABAN').Value := _pGiaban;
        Execute;
    	Result := Parameters.ParamValues['@RETURN_VALUE'];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUIDByUserName(pUsername: String): Integer;
begin
    with QrUSER do
    begin
        if not Active then
            Open;

        if Locate('UserName', pUsername, []) then
            Result := FieldByName('UID').AsInteger
        else
            Result := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUserNameByUID(uid: Integer): String;
begin
    with QrUSER do
    begin
        if not Active then
            Open;

        if Locate('UID', uid, []) then
            Result := FieldByName('UserName').AsString
        else
            Result := '';
    end;
end;

function TDataMain.GetFullNameByUID(uid: Integer): String;
begin
    with QrUSER do
    begin
        if not Active then
            Open;

        if Locate('UID', uid, []) then
            Result := FieldByName('FullName').AsString
        else
            Result := '';
    end;
end;

function TDataMain.GetManvByUID(uid: Integer): String;
begin
    with QrUSER do
    begin
        if not Active then
            Open;

        if Locate('UID', uid, []) then
        begin
            if FindField('Manv') <> nil then
                Result := FieldByName('Manv').AsString
            else
                Result := '';
        end
        else
            Result := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.HuyPhathanhPQT_CT(_pMathe: String; _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spPQT_HUYTHE_CT do
    begin
        Prepared := True;
        Parameters[1].Value := _pMathe;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[2].Value;
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.SetSysRepParam(const luot: TGUID;
  const ListField: array of String; const ListValue: array of Variant);
var
    n, i: Integer;
begin
    n := Length(ListValue);

    with QrREP_PARAM do
    begin
        if Active then
            Close;
        Open;

        Append;
        TGuidField(FieldByName('LUOT')).AsGuid := luot;
        for i := 0 to n - 1 do
        begin
            FieldByName(ListField[i]).AsVariant := ListValue[i];
        end;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetSeqValue;
begin
	with GETSEQ do
    begin
    	Prepared := True;
    	Parameters[1].Value := seq;
        Parameters[3].Value := True;
        Execute;
    	Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TDataMain.AllocEAN(mPrefix, nhom: String): String;
begin
    with ALLOC_BARCODE do
    begin
    	Prepared := True;
        Parameters.ParamByName('@PREFIX').Value := mPrefix;
        Parameters.ParamByName('@MANHOM').Value := nhom;
        Execute;
		Result := Parameters.ParamValues['@BARCODE'];
    end;
end;

(*==============================================================================
** Cap ma khach hang, NCC
** Args:
**		pLen 	length of code
**		pType	0: Khach hang; 1: NCC
**------------------------------------------------------------------------------
*)
function TDataMain.AllocMADT(pLen, pType: Integer; pPrefix: String): String;
begin
    with ALLOC_MADT do
    begin
        Prepared := True;
        Parameters[1].Value := pLen;
        Parameters[2].Value := pType;
        Parameters[4].Value := pPrefix;
        Execute;
        Result := Parameters[3].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CalcStock;
begin
	Wait(PROCESSING);
    try
        with CALC_STOCK do
        begin
            Prepared := True;
            Parameters[1].Value := pDate;
            Execute;
        end;
    except on E: Exception do
        begin
            ClearWait;
            ErrMsg(e.Message);
        end;
    end;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_SL_KHO   = 'Dữ liệu có nhiều hơn 2 kho hàng. Kết thúc chương trình.';
function TDataMain.CheckSlKho(pMsg: Boolean): Boolean;
begin
    Result := False;
    with QrDMKHO do
    begin
        if Active then
            Close;
        Open;
        if RecordCount > 2 then
        begin
            if pMsg then
                ErrMsg(RS_SL_KHO);
            Result := True;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CheckUpdate;
var
    sCurVer, sLastVer: String;
begin
    sLastVer := GetLatestVersion;
    if sLastVer = '' then
        Exit;

    sCurVer := GetVersionInfo('FileVersion');
    if sCurVer < sLastVer then
    begin
        Msg('Có phiên bản mới.');
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.AllocSCT(lct: String; qr: TADOQuery; AFIELDNAME: String);
var
    s, mLoc: String;
    d: TDateTime;
    x, m1, m2, y1, y2: Word;
    b: Boolean;

        (*
        **
        *)
    function Proc: String;
    begin
        with ALLOC_SCT do
        begin
            Prepared := True;
            Parameters.ParamByName('@SCT').Value := Qr.FieldByName('SCT').AsString;
            Parameters.ParamByName('@LOAI').Value := lct;     // Loai chung tu
            Parameters.ParamByName('@NGAY').Value := d;       // Ngay phieu
            Parameters.ParamByName('@MAKHO').Value := sysLoc;  // Location
            Execute;

            Result := Parameters.ParamValues['@SCT'];
        end
    end;
begin
    with qr do
    begin
        // Ngay phieu
        d := FieldByName('NGAY').AsDateTime;

        // Chung tu moi
        if State in [dsInsert] then
            s := Proc
        // Chinh sua
        else if State in [dsEdit] then
        begin
            DecodeDate(FieldByName('NGAY').OldValue, y1, m1, x);
            DecodeDate(d, y2, m2, x);
            
            if (m1 <> m2) or (y1 <> y2) then    // Thang, nam khac or Kho khac
            begin
                s := Proc;
                Msg('Số phiếu thay đổi. Số cũ: ' + FieldByName('SCT').AsString +
                    ' * Số mới: ' + s);
            end
            else
                Exit
        end
        else
            Exit;

        // SCT moi
        FieldByName('SCT').AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.AllocUID(pSEQ_NAME: String): Integer;
begin
    with ALLOC_UID do
    begin
        Prepared := True;
        Parameters[1].Value := pSEQ_NAME;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_BARCODE_USED = 'Đã có dữ liệu phát sinh.';
function TDataMain.BarcodeIsUsed(pMABH: String; bDMHH: Boolean): Boolean;
begin
    with DMVT_EXISTS_CHUNGTU do
    begin
        Prepared := True;
        Parameters.ParamByName('@MABH').Value := pMABH;
        Parameters.ParamByName('@DM_HH').Value := bDMHH;
        Execute;
        Result := (Parameters.ParamValues['@RETURN_VALUE'] <> 0);
    end;
    if Result then
        ErrMsg(RS_BARCODE_USED);
end;


(*
    **  In tem - lable
    *)
var
	labelTransNo: Integer;
    labelUtf8: Boolean;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.LabelOpen;
begin
	labelTransNo := GetSeqValue('IN_BARCODE');
    labelUtf8 := GetSysParam('STAMP_UTF8');
	with QrLABEL do
    begin
    	Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.LabelAdd(mabh: String; soluong: Double; ngay: TDateTime);
begin
	with QrLABEL do
    begin
        Append;
        FieldByName('LUOT').AsInteger := labelTransNo;
        FieldByName('MABH').AsString := mabh;
        FieldByName('SOLUONG').AsInteger := RoundUp(soluong);
        FieldByName('NGAY').AsDateTime := ngay;
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    // Print Stamp
    RS_LABEL_FILE_ERROR = 'Lỗi ghi ra file.';
    RS_LABEL_NOTHING 	= 'Không có nhãn nào được xuất in.';
    RS_LABEL_COUNT 		= 'Đã xuất in %d nhãn.';
    RS_LABEL_COUNT2		= 'Đã xuất in %d nhãn theo định dạng sau:'#13#13;

function TDataMain.LabelClose(filename: String; addhdr, overwrite: Boolean): Boolean;
var
    s: String;
	ls: TStrings;
    i, n, k, cnt: Integer;
begin
	QrLABEL.Close;
    Result := True;

    ls := TStringList.Create;
    if (not overwrite) and FileExists(filename) then
    	ls.LoadFromFile(filename);		// Load existing file

    with EXPORT_LABEL do
    begin
        if Active then
            Close;

    	Prepared := True;
        Parameters[1].Value := labelTransNo;
        Open;
        n := FieldCount;

        // Export header
		if addhdr and (ls.Count = 0) then
        begin
        	s := '"' + Fields[1].FieldName + '"';	// Skip column SOLUONG
        	for i := 2 to n - 1 do
            	s := s + ',"' + Fields[i].FieldName + '"';
            ls.Add(s);
        end;

        // Export labels
        cnt := 0;
        while not Eof do
        begin
        	k := Fields[0].AsInteger;
        	s := '"' + Fields[1].AsString + '"';
        	for i := 2 to n - 1 do
            	s := s + ',"' + Fields[i].AsString + '"';

            if not labelUtf8 then
	            s := UpperCase(isStripToneMark(s));

	        cnt := cnt + k;
            while k > 0 do
            begin
	            ls.Add(s);
                Dec(k);
            end;
        	Next;
        end;
        Close;
    end;

    // Write to file
    try
        if labelUtf8 then
            ls.SaveToFile(filename, TEncoding.UTF8)
        else
            ls.SaveToFile(filename)
    except
    	ErrMsg(RS_LABEL_FILE_ERROR);
	    Result := False;
    end;

    // Final
    if Result then
        if cnt = 0 then
            Msg(RS_LABEL_NOTHING)
        else if addhdr and overwrite then
            Msg(Format(RS_LABEL_COUNT, [cnt]))
        else
            Msg(Format(RS_LABEL_COUNT2 + ls[0], [cnt]));

    ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.LabelClose2: Boolean;
begin
	QrLABEL.Close;
//    Result := ShowReport('In tem', 'EX_LABEL_TEM',[sysLogonUID, labelTransNo]);
    Result := FrmFastReport.ShowReport('In tem', 'EX_LABEL_TEM',[sysLogonUID, labelTransNo]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.LabelCloseFR(filename: string): Boolean;
begin
	QrLABEL.Close;
    Result := FrmFastReport.ShowReport('In tem', filename ,[sysLogonUID, labelTransNo]);
end;

(*==============================================================================
** Arguments:
**	khoa	: Khoa nhap xuat
**	fName	: So luong tem can in. (default: 'SOLUONG')
**------------------------------------------------------------------------------
*)
function TDataMain.LabelExport(khoa: String; fTable: String; fName: String): Boolean;
var
    mFilename: String;
begin
    LabelOpen;

    DataMain.Conn.Execute(
        Format(
            'insert into IN_LABEL (LUOT, MABH, SOLUONG, NGAY)' +
            ' select %d, b.MAVT, ceiling(b.%s), a.NGAY' +
            ' from  %s a, %s b' +
            ' where	a.KHOA = %s' +
            ' and	a.KHOA = b.KHOA' +
            ' and b.B1 = 1' +
            ' and IsNull(b.%s,0) <> 0', [labelTransNo, fName, fTable, fTable + '_CT',  QuotedStr(khoa), fName])
    );

    Result := LabelClose(
    	IncludeTrailingPathDelimiter(GetSysParam('FOLDER_BARCODE')) + 'Label.csv',
    	GetSysParam('STAMP_CSV_HEADER'),
        GetSysParam('STAMP_OVERWRITE'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetPeriodDate(pYear, pMon: Integer; var pFromDate: TDateTime; var pToDate: TDateTime): Boolean;
var
	_msg: String;
begin
    Result := False;
    try
        with TADOCommand.Create(nil) do
        begin
            Connection := DataMain.Conn;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spHR_THONGSO_CHUKY_CONGLUONG;1';

            Parameters.Refresh;
            Parameters[1].Value := pYear;
            Parameters[2].Value := pMon;
            Parameters[3].Value := date;
            Parameters[4].Value := date;
            Execute;

            Result := Parameters[0].Value = 0;
            if Result then
            begin
                pFromDate := Parameters[3].Value;
                pToDate := Parameters[4].Value;
            end else
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_ERROR_ACTION;

                ErrMsg(_msg);
            end;

            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetPeriodStatus(m, y, t: Integer): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format(
        	'select [KhoaSoCong], [KhoaSoLuong], [KhoaSoLichLV], [DuyetLuong] from HR_BANG_THONGSO where [NAM]=%d and [THANG]=%d', [y, m]);
        Open;
        Result := Fields[t].AsBoolean;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.POM_BarcodeIsUsed(pMABH: String): Boolean;
begin
    with spPOM_DMVT_EXISTS_CHUNGTU do
    begin
        Prepared := True;
        Parameters[1].Value := pMABH;
        Execute;
        Result := Parameters[0].Value <> 0;
    end;
    if Result then
        ErrMsg(RS_BARCODE_USED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.POM_ThongsoTinhtrang(m, y, t: Integer): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format(
        	'select [KHOASO] from POM_BANG_THONGSO where [NAM]=%d and [THANG]=%d', [y, m]);
        Open;
        Result := Fields[t].AsBoolean;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.FB_BarcodeIsUsed(pMABH: String): Boolean;
begin
    with spFB_DMVT_EXISTS_CHUNGTU do
    begin
        Prepared := True;
        Parameters[1].Value := pMABH;
        Execute;
        Result := Parameters[0].Value <> 0;
    end;
    if Result then
        ErrMsg(RS_BARCODE_USED);
end;

(*==============================================================================
** Init XML
**------------------------------------------------------------------------------
*)
function TDataMain.ListTables(pType: Integer; ls: TStrings): Boolean;
begin
    Result := False;

    // OtherX
    if (pType and 64) <> 0 then
    begin
        ls.Add('DM_KHACX')
    end;

    // Dbe
    if (pType and 32) <> 0 then
    begin
        ls.Add('SYS_DBE_OBJ');
        ls.Add('SYS_DBE_MSG');
    end;

    // Reports
    if (pType and 16) <> 0 then
        ls.Add('SYS_REPORT');

    // Functions
    if (pType and 8) <> 0 then
        ls.Add('SYS_FUNC');

    // Flexible
    if (pType and 4) <> 0 then
        ls.Add('SYS_FLEXCONFIG');

    // Dictionary
    if (pType and 2) <> 0 then
        ls.Add('SYS_DICTIONARY');

    // Grid
    if (pType and 1) <> 0 then
        ls.Add('SYS_GRID');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CopyData(qrSource, qrDest: TADOQuery);
var
    i, n: Integer;
begin
    // Export
    with qrDest do
    begin
        n := qrSource.FieldCount - 1;
        while not qrSource.Eof do
        begin
            Append;
            for i := 0 to n do
                if Fields[i].DataType <> ftAutoInc then
                begin
                    FieldByName(qrSource.Fields[i].FullName).Value := qrSource.Fields[i].Value;
                end;
            Post;
            qrSource.Next;
        end;
        Close;
    end;
    qrSource.Close;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.FB_CalcStock;
begin
	Wait(PROCESSING);
    try
        with FB_CALC_STOCK do
        begin
            Prepared := True;
            Parameters[1].Value := pDate;
            Execute;
        end;
    except on E: Exception do
        begin
            ClearWait;
            ErrMsg(e.Message);
        end;
    end;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.FB_UpdateHH(pMavt: string);
begin
    with spFB_UPDATE_HH do
    begin
        Prepared := True;
        Parameters[1].Value := pMavt;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.AllocSCT2(lct: String; qr: TADOQuery; column: String = 'SCT2');
var
    s, mLoc: String;
    d: TDateTime;
    x, m1, m2, y1, y2: Word;
    b: Boolean;

        (*
        **
        *)
    function Proc: String;
    begin
        with ALLOC_SCT2 do
        begin
            Prepared := True;
            Parameters[1].Value := qr.FieldByName(column).AsString;
            Parameters[2].Value := lct;     // Loai chung tu
            Parameters[3].Value := d;       // Ngay phieu
            Parameters[4].Value := sysLoc;  // Location
            Execute;
            Result := Parameters[1].Value;
        end
    end;
begin
    with qr do
    begin
        // Ngay phieu
        d := FieldByName('NGAY').AsDateTime;

        // Chung tu moi
        if State in [dsInsert] then
            s := Proc
        // Chinh sua
        else if State in [dsEdit] then
        begin
            DecodeDate(FieldByName('NGAY').OldValue, y1, m1, x);
            DecodeDate(d, y2, m2, x);

            if (m1 <> m2) or (y1 <> y2) then    // Thang, nam khac or Kho khac
            begin
                s := Proc;
                Msg('Số phiếu thay đổi. Số cũ: ' + FieldByName(column).AsString +
                    ' * Số mới: ' + s);
            end
            else
                Exit
        end
        else
            Exit;

        // SCT moi
        FieldByName(column).AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.CategoryAccountChargePosUsed(pPLOAI, pMATK: String): Boolean;
begin
    with sp_CATEGORY_ACCOUNT_CHARGE_POS_USED do
    begin
        Prepared := True;
        Parameters[1].Value := pPLOAI;
        Parameters[2].Value := pMATK;
        Execute;
        Result := Parameters[0].Value <> 0;
    end;
    if Result then
        ErrMsg(RS_INVAILD_POS_USED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.UpdateAfterPostPhieu(pKhoa: TGUID; pLCT: string);
begin
    with spAfterPostPhieu do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKhoa);
        Parameters[2].Value := pLCT;
        Execute;
    end;
end;

end.
