﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsHH;

interface

uses
  Classes, Controls, Forms,
  Wwdbdlg, StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh,
  DbLookupComboboxEh2, MemTableDataEh, MemTableEh;

type
  TFrmChonDsHH = class(TForm)
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
    RgLoai: TRadioGroup;
    Panel1: TPanel;
    QrDMVT: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    DsDMVT: TDataSource;
    DsNHOM: TDataSource;
    CbNganh: TDbLookupComboboxEh2;
    EdMaNganh: TDBEditEh;
    EdMaNhom: TDBEditEh;
    EdMavt: TDBEditEh;
    CbMavt: TDbLookupComboboxEh2;
    CbNhom: TDbLookupComboboxEh2;
    TbDummyEh: TMemTableEh;
    TbDummyEhMaNganh: TStringField;
    TbDummyEhMaNhom: TStringField;
    TbDummyEhMaNganNhom: TStringField;
    DsDummyEh: TDataSource;
    TbDummyEhMavt: TStringField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CbNhomDropDown(Sender: TObject);
    procedure TbDummyEhMaNhomChange(Sender: TField);
    procedure TbDummyEhMaNganhChange(Sender: TField);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmChonDsHH: TFrmChonDsHH;

implementation

uses
	isDb, isLib, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.FormShow(Sender: TObject);
begin
     TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrNGANH, QrNHOM, QrDMVT, DataMain.QrDM_NHOM]);
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
	mTrigger := False;
    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsHH.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    CbNganh.Enabled := n <> 2;
    CbNhom.Enabled := n = 1;
    CbMavt.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbNganh.SetFocus;
        1:
            CbNhom.SetFocus;
        2:
            CbMavt.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.TbDummyEhMaNganhChange(Sender: TField);
begin
    with TbDummyEh do
    begin
        FieldByName('MaNhom').AsString := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.TbDummyEhMaNhomChange(Sender: TField);
var
    s : string;
begin
    s := Sender.AsString;
    with QrNHOM do
    if Locate('MaNhom', s, []) then
    begin
        s := FieldByName('MA').AsString;
    end;
    TbDummyEh.FieldByName('MaNganNhom').AsString := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
        	s1 := EdMaNganh.Text;
        	s2 := CbNganh.Text;
        end;
    1:
    	begin
        	s1 := CbNhom.Value;
        	s2 := CbNhom.Text;
        end;
    2:
    	begin
        	s1 := EdMavt.Text;
            s2 := CbMavt.Text;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;

        EdMAVT.Text := '';
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := EdMaNganh.Text;
    1:
       	s := EdMaNhom.Text;
    2:
       	s := EdMavt.Text;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
//	    CloseDataSets([QrNGANH, QrNHOM, QrDMVT]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.CbNhomDropDown(Sender: TObject);
var
    s: string;
begin
    s := TbDummyEh.FieldByName('MaNganh').AsString;
    QrNHOM.Filter := 'MaNganh=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHH.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
