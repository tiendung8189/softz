﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExCommon;

interface

uses
	Classes, Db, Windows, Forms, ADODb, wwfltdlg, Graphics, ComCtrls, ActnList,
    fctreecombo, fctreeview, Controls, SysUtils, wwDBGrid2, Messages, ADOInt,
    ShellAPI, wwdblook, wwDataInspector, Variants, rDBComponents, fcCombo, Wwdotdot, Wwdbcomb,
    System.RegularExpressions, DBLookupEh, DBVertGridsEh;

resourcestring
	{ Softz }
	// Import
    RS_IM_NUM_REC			= 'Đã import %d mẫu tin.';
    RS_IM_NONE				= 'Không có mẫu tin nào được import.';
    RS_IM_ERR_REC			= 'Có %d mẫu tin không hợp lệ.';
    RS_IM_EXCEL_YESNO       =  'Có cập nhật lại dữ liệu hiện tại không?';

    // File
	RS_FILE_IO				= 'Lỗi truy xuất file.';
    RS_EXPORT_ERR			= 'Lỗi xuất dữ liệu.';
    RS_ERROR_ACTION		    = 'Đã xảy ra lỗi khi thực hiện.';

    // Common
    RS_CONNECT_FAIL     	= 'Không kết nối được với cơ sở dữ liệu.';
    RS_NOTE_CAP         	= 'Lưu ý';

    RS_NGANH_EMPTY          = 'Phải chọn "Ngành hàng".';
    RS_NHOM_EMPTY           = 'Phải chọn "Nhóm hàng".';
    RS_NHOM_DT_EMPTY        = 'Phải chọn "Nhóm đối tác".';

    RS_VAT_NOTSAME       	= 'Mặt hàng sai loại thuế.';
    RS_NO_FOLDEREXPORT		= 'Chưa chọn thư mục lưu dữ liệu export.';
    RS_EXPORTED_COMPLETE 	= 'Đã xuất xong dữ liệu vào thư mục "%s"';

    RS_DSN_CONFIG			= 'Chưa cấu hình text DSN.';
    RS_ERROR_COPY_FILE		= 'Lỗi khi copy file vào thư mục hệ thống.';
    RS_INVALID_DATA			= 'Dữ liệu không hợp lệ.';
    RS_INVALID_CARD			= 'Mã thẻ không hợp lệ.';
    RS_ITEM_CODE_DUPLICATE	= 'Trùng "%s".';

	RS_ITEM_CODE_FAIL1		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc mã hàng đã ngừng kinh doanh';
	RS_ITEM_CODE_FAIL2		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc nhà cung cấp.';
    RS_BOOKCLOSED			= 'Đã khóa sổ đến ngày %s.';
    RS_CANT_INPUT_DATE      = 'Không được nhập liệu trước ngày bắt đầu'#13'sử dụng chương trình %s ';

    RS_INVALID_QTY 			= 'Số lượng không hợp lệ.';
	RS_INVALID_DISCOUNT 	= 'Chiết khấu không hợp lệ.';
    RS_INVALID_INTEREST 	= 'Tỷ lệ lãi không hợp lệ.';
	RS_POS_NOT_PRICE 		= 'Mặt hàng chưa có giá bán.';
    RS_SCT_CHANGE           = 'Ngày lập phiếu thay đổi. Số phiếu mới là: "%s".';

	RS_DA_THUCHI			= 'Đã phát sinh Thu / Chi, nên không thể chỉnh sửa được.';
    RS_XOA_CHITIET			= 'Xóa toàn bộ chi tiết chứng từ?';
    RS_CONFIRM_XOAPHIEU		= 'Xóa phiếu sẽ không thể hồi phục. Tiếp tục?';
    RS_PHUCHOI              = 'Phục hồi';
    RS_DAXOA                = 'Đã xóa';
    RS_CLEAR_DATA_GRID      = 'Xóa toàn bộ dữ liệu có trên lưới. Tiếp tục?';

    RS_EDIT_BY              = 'Phiếu đang được sửa bởi "%s" từ lúc "%s", các dữ liệu đang sửa trước đó sẽ bị bỏ qua. Tiếp tục?';

    RS_ADMIN_CONTACT		= 'Xin liên hệ với người quản trị hệ thống.';

    // Cr Frame
    RS_THESAME_MONTH 		= 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME	= 'Thời gian báo cáo không hợp lệ.';

	{ POSz }
    RS_PRINT_ERR            = 'Lỗi in bill.';
    RS_NHACNHO_THANHTOAN    = 'Xác nhận nhân đơn vị tiền thối';
	RS_INVALID_ACTION       = 'Thao tác không hợp lệ.';
	RS_POS_CODE_FAIL        = 'Nhập sai mã hàng hóa.';
    RS_CONFIRM_PRICE        = 'Hàng có giá bán là %s, giá đã được giảm là %s. Giảm giá?';
    RS_CUT_PRICE_QTY        = 'Chỉ còn lại %f suất giảm giá.';

    //HrZ
    {Hrms}
	RS_SEPARATION_DATE		= 'Ngày thôi việc không hợp lệ. Xem lại quá trình làm việc.';
	RS_EMP_ID				= 'Mã nhân viên không hợp lệ.';
	RS_PERIOD				= 'Thời gian không hợp lệ.';
    RS_SESSION_VALIED   	= 'Buổi nghỉ không hợp lệ.';
    RS_SESSION2_VALIED   	= 'Buổi nghỉ thứ 2 chỉ được nhập "Nửa buổi cuối ngày".';
    RS_PERIOD_BIRTHDAY		= 'Ngày sinh con không hợp lệ.';

    //Register Status
    RS_REG_STATUS1   	    = '"Duyệt" yêu cầu.';
    RS_REG_STATUS2   	    = '"Từ chối" yêu cầu.';
    RS_REG_APPROVE   	    = 'Duyệt yêu cầu. Tiếp tục?';
    RS_REG_CONFIRM   	    = 'Xác nhận yêu cầu. Tiếp tục?';
    RS_REG_CANCEL   	    = 'Từ chối yêu cầu. Tiếp tục?';
    RS_NOT_PERMISSION   	= 'Không có quyền thao tác';

    // Leave Type
	RS_LEAVE_TYPE_INVALID	= 'Lỗi nhập liệu. Loại nghỉ không hợp lệ.';
    RS_LEAVE_TYPE_AND_AMOUNT	= 'Lỗi nhập liệu. Phải nhập đủ Lý do và Số ngày.';
    RS_LEAVE_TYPE_DUPLICATE	= 'Lỗi nhập liệu. Loại nghỉ không được trùng nhau.';
    RS_LEAVE_TYPE_2ML		= 'Lỗi nhập liệu. Trùng loại nghỉ thai sản.';
    RS_LEAVE_MAX_DAY	    = 'Lỗi nhập liệu. Số ngày nghỉ không được lớn hơn 1.';

    // Leave Balance
	RS_CFM_CARRY_FWD		= 'Mặc định cột "Chuyển năm sau"'#13 +
    						  'bằng cột "Còn lại". Tiếp tục?';

    // Organization
	RS_CONGTY				= '&Công ty';
	RS_CHINHANH				= '&Chi nhánh';

	RS_ORG0					= '&Toàn bộ';
	RS_ORG1					= '&Phòng ban';
	RS_ORG2					= '&Bộ phận';
	RS_ORG3					= '&Tổ';
	RS_ORGANIZATION			= 'Sai tổ chức phòng ban / bộ phận...';

    RS_CODE_DEF             = 'Mặc định Mã chấm công từ Mã nhân viên?';
    RS_LOG_INVALID_CARD		= 'Các Thẻ Không Hợp Lệ';
    RS_LOG_INVALID_EMP		= 'Các Mã Không Hợp Lệ';
	RS_LOG_INVALID			= 'Dữ Liệu Không Hợp Lệ';
    RS_LOG_DUP_CARD			= 'Các Mã Thẻ Bị Trùng';

    // Timesheet
	RS_TIMESHEET_CFM_CLOSED	= 'Bảng công tháng đã khóa sổ. Bạn không thể chỉnh sửa được.';
	RS_IMPORT_ERROR_AT		= 'Dữ liệu lỗi ở dòng: %d, Emp ID: %s - Date: %s. Tiếp tục?';
	RS_TIMESHEET_CALC1		= 'Tổng hợp công tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CALC2		= 'Tổng hợp công tháng %d/%d của tất cả nhân viên. Tiếp tục?';
	RS_TIMESHEET_AUTO1		= 'Tự động bổ sung giờ vào, giờ ra bị thiếu của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_AUTO2		= 'Tự động bổ sung giờ vào, giờ ra bị thiếu của'#13'tất cả nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_CLEAR1		= 'Xóa dữ liệu công tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CLEAR2		= 'Xóa toàn bộ dữ liệu công tháng %d/%d của'#13'tất cả nhân viên. Tiếp tục?';
    RS_TIMESHEET_CLEAR3		= 'Xóa dữ liệu ca làm việc ngày %s với ca thứ %d của'#13'nhân viên "%s". Tiếp tục?';
    RS_TIMESHEET_CLEAR4		= 'Xóa toàn bộ dữ liệu lịch làm việc %d/%d của'#13'tất cả nhân viên. Tiếp tục?';
    RS_TIMESHEET_CLEAR_M1	= 'Xóa dữ liệu suất ăn tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CLEAR_M2	= 'Xóa toàn bộ dữ liệu suất ăn tháng %d/%d của'#13'tất cả nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_VALID		= 'Dữ liệu công đã hợp lệ.';
    RS_TIMESHEET_VALID1		= 'Dữ liệu lịch làm việc đã hợp lệ.';
	RS_TIMESHEET_ERASE		= 'Xóa dữ liệu công bổ sung tháng %d/%d của tất cả'#13'nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_POPUP		= 'Tổng hợp công tháng cho';
    RS_TIMESHEET_POPUP2		= 'Cập nhật dữ liệu công cho';
    RS_TIMESHEET_POPUP3		= 'Xóa dữ liệu giờ công của';
	RS_TIMESHEET_RECALC1	= 'Tính lại OT đã duyệt.';
    RS_TIMESHEET_GEN_ERROR	= 'Chưa cập nhật các thông số chấm công,'#13'tính lương cho tháng %d/%d.';
    RS_TIMESHEET_CALC_ERROR	= 'Có xảy ra lỗi khi thực hiện tính công.';
    RS_TIMESHEET_SCHEDULE_POPUP2    = 'Cập nhật dữ liệu lịch làm việc cho';
    RS_TIMESHEET_SCHEDULE_POPUP3	= 'Xóa dữ liệu lịch làm việc của';
    RS_TIMESHEET_SCHEDULE_POPUP4	= 'Xóa ca làm việc đang chọn của';

    // Payroll Advanced
	RS_ADVANCED_CALC1		= 'Tính lại tạm ứng cho nhân viên "%s". Tiếp tục?';
	RS_ADVANCED_CALC		= 'Tính lại tạm ứng tháng %d/%d của tất cả'#13 +
							  'nhân viên có trên màn hình. Tiếp tục?';

    // Payroll
	RS_PAYROLL_CFM_CLOSED	= 'Bảng lương tháng đã khóa sổ. Bạn không thể chỉnh sửa được.';
    RS_PAYROLL_ERROR_CALC	= 'Đã xảy ra lỗi khi tính lương.';
	RS_PAYROLL_ERROR_CALC1	= 'Lỗi tính lương nhân viên "%s".';
    RS_PAYROLL_CFM_CALC1	= 'Tính lại lương cho nhân viên "%s". Tiếp tục?';
    RS_PAYROLL_CFM_CALC2	= 'Tính lại lương tháng %d/%d của tất cả nhân viên. Tiếp tục?';
	RS_PAYROLL_CFM_IMPORT	= 'Cập nhật dữ liệu vào bảng lương tháng %d/%d. Tiếp tục?';
	RS_PAYROLL_CFM_ERASE	= 'Xóa dữ liệu lương bổ sung tháng %d/%d của tất cả'#13 +
    						  'nhân viên có trên màn hình. Tiếp tục?';
    // Separation
	RS_CFM_REWORK			= 'Nhân viên này sẽ chuyển từ tình trạng'#13 +
			    			  'thôi việc sang làm việc. Tiếp tục?';
	RS_SEPARATED			= 'Nhân viên này đã thôi việc.';

    RS_ROSTER_CFM_CLOSED	= 'Lịch làm việc đã khóa sổ. Bạn không thể chỉnh sửa được.';
	RS_EDIT_NOT_ALLOWED		= 'Dữ liệu đã được xác nhận là đúng. Không được phép chỉnh sửa.';

    RS_CFM_DATE_FORMAT		= 'Định dạng ngày của Windows đang là "%s".'#13'Dữ liệu ngày cũng phải được định dạng đúng như vậy. Tiếp tục?';
	RS_MUST_NODOT			= 'Lỗi nhập liệu. Mã không được chứa dấu ".".';
	RS_NO_SKIN_DATA			= 'Không có dữ liệu hiệu ứng.';
	RS_POPUP_RECALC			= 'Tính lại cho';
	RS_RECALC_COLS			= 'Tính Lại Các Cột';

    //POM
    RS_POM_CLOSED	        = 'Dữ liệu Giá thành/Sản xuất đã khóa sổ. Bạn không thể chỉnh sửa được.';
    RS_POM_GEN_ERROR	    = 'Chưa cập nhật các thông số Giá thành/Sản xuất cho tháng %d/%d.';


	{ Chung }

    RS_INVAILD_COLS			= 'Cột dữ liệu không tồn tại';
    RS_INVAILD_PREFIX		= 'Tiếp đầu ngữ chứa ký tự đặt biệt';
    RS_INVAILD_POS_USED		= 'Máy POS đã được sử dụng';
    RS_CONFIRM_DELETED_BILL	= 'Xóa thông tin bill bán lẻ. Tiếp tục?';
    RS_INVAILD_FORMAT		= '"%s" không hợp lệ. Email phải đúng định dạng và không chứa ký tự đặc biệt.';
    RS_INVAILD_VALIDATION	= '"%s" không hợp lệ. Dữ liệu phải đúng định dạng.';
    RS_CONFIRM_ATTACH       = 'Xoá file đính kèm. Tiếp tục?';
    RS_INVAILD_CONTENT		= 'Lỗi trích xuất nội dung.';
    RS_INVAILD_NOTFOUND		= 'Không tìm thấy nội dung file.';
    RS_FILE_NOTFOUND_FORMAT = 'Không tìm thấy file mẫu hoặc không đúng định dạng (Word).';
    RS_YEAR_INVALID         = 'Năm không hợp lệ';

const
    REP_ENCODE_KEY: Word = 119;
    TXT_DSN: String 		= 'SOFTZ_TEXT';

    RP_LABEL_35x25x3    = 'RP_LABEL_35x25x3';
    RP_LABEL_35x22x3    = 'RP_LABEL_35x22x3';
    RP_LABEL_35x15x3    = 'RP_LABEL_35x15x3';
    RP_LABEL_25x15x4    = 'RP_LABEL_25x15x4';
    RP_LABEL_20x10x2    = 'RP_LABEL_20x10x2';


var
    sysCurRound, sysNumRound, sysCurPomRound, sysCurHrRound: Integer;

    sysYear, sysMon: Integer;
    sysBegYear, sysBegMon: Integer;
    sysBegDate, sysCloseHH, sysCloseCN, sysCloseFBHH: TDateTime;

    sysLateDay, sysKhttLe, sysKhttSi, sysFloatRoundOne: Integer;
    sysDefKho, sysLoc, sysHTGia, sysHTGiaBL, sysGiaCoVAT, sysLang: String;

    sysDDH_TT_NEW, sysDDH_TT_WAIT, sysDDH_TT_APPR, sysDDH_TT_PART, sysDDH_TT_DONE, sysDDH_TT_CLOSE: String;
    sysDIEUKHO_TT_NEW, sysDIEUKHO_TT_DONE: String;

    curHaohutRound: Integer;
    curHaohutFmt: String;

    //Hr
    sysSite, sysAppSource: String;

    (*
        May cham cong
    *)
     devLogFromDate: TDateTime;
     devLogAll: Boolean;

    mTrigger, mTriggerCK, mTriggerMaster, mTriggerDetail, mExDot,
    cfRecordDate, sysIsBarcode, sysFuncTK: Boolean;
    sysPTTT: String;

    (*
    ** Message
    *)
procedure exValidClosing(const dt: TDateTime; const subsys: Integer = 1);
procedure exValidRecordDate(const dt: TDateTime; bEdit: Boolean = False);

	(*
    ** Utils
    *)

function  exDefaultWarehouse: Boolean;
function  exCanChange(Sender: TPageControl; DataSet: TCustomADODataSet): Boolean;
procedure exCompleteConfirm;
function  GetBigint: LargeInt;

function  exValidMatchGroup(pNganh, pNhom, pNhom2, pNhom3, pNhom4: String): Boolean;
procedure FlexGroupCombo(Sender: TfcTreeCombo; loai: string = 'HH');
function  exGetFlexDesc(Sender: TObject): WideString;
procedure FlexGroupComboDT(Sender: TfcTreeCombo);
procedure FlexGroupComboTS(Sender: TfcTreeCombo);

function  exVNDRound(_pExtended: Extended; _pDigit: Integer = -1000; _pRoundNum: Integer = -1000): Extended; //_pRoundNum: 0, 100, 500, 1000

function  exIsChecked(ds: TDataSet; pAbort: Boolean = True): Boolean;
procedure exChecked(pDataset: TDataSet; pRight: String = 'SZ');
function  exGetCheckedCaption(pDataSet: TDataSet): String;
function exCheckNhomthue(pNhomthue: String; pDataset: TCustomADODataSet): Boolean;
function exCheckLoc(pDataset: TDataSet; pAbort: Boolean = True; AFIELD: string = 'LOC'): Boolean;

function  exIsCheckSL(pSl: Double; pMSG: Boolean = True): Boolean;

function IsDuplicateCode(pQuery: TCustomADODataSet;
	pValue: TField; pShowMgs: Boolean = True): Boolean; overload;
function IsDuplicateCode2(pQuery: TCustomADODataSet;
	pValue: TField; var pBm: TBytes): Boolean; overload;

function IsDuplicateCode3(pQuery: TCustomADODataSet;
	pDisplayField: TField; pConditionFields: string; pConditionValues: array of Variant;  pShowMgs: Boolean = True): Boolean; overload;
function IsDuplicateCode3(pQuery: TCustomADODataSet;
	pDisplayLabel: String; pConditionFields: string; pConditionValues: array of Variant;  pShowMgs: Boolean = True): Boolean; overload;

procedure exReSyncRecord(DataSet: TCustomADODataSet; pAll: Boolean = False);
procedure exDbCopyFields(pDataSet, pSource: TCustomADODataSet;
    pOveride: Boolean = False; pExceptField: String = '');

procedure exSaveDetails(DataSet: TCustomADODataSet);
procedure exEmptyDetails(DataSet: TDataSet; ActiveControl: TWinControl = Nil);

function  exConfigInspector(pIns: TwwDataInspector; pFieldNames: String): Integer;
function  exConfigVertGrid(pVGrid: TDBVertGridEh; pFieldNames: String): Integer;

function CanEditLockedBy(Dataset: TDataSet; pShowMessage: Boolean = True): Boolean;
procedure SetLockedBy(Dataset: TDataSet; b: Boolean);
(*
    **
    *)
function  exExecSQL(const sqlStr: String): Integer;
function  exGetImportFile(const pType: Integer = 2): String;
procedure exViewLog(const LogName, Content: String);
procedure ToggleMarkDataSet(DataSet: TCustomADODataSet);
function  GetToggleMarkCaption(DataSet: TCustomADODataSet): String;
function  TransFieldDesc(ds: TDataSet; flst: String): String;
function  MakeEraseSQL(ds: TDataSet; tn, flst: String): String;

procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo);

function ShowReport (pTitle, pRepName : String; Args: array of Variant): Boolean; overload;
function ShowReport (pTitle, pRepName : String): Boolean; overload;

function PrintReport (pRepName : String; Args: array of Variant;
                            const PrinterName: String = '';
                            const pCopy: Integer = 1): Boolean; overload;
	(*
    ** Refs. functions
    *)
function exDotUSER(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotUSER(Sender: TField): Boolean; overload;

function exDotMadt(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMadt(Sender: TField): Boolean; overload;
function  exDotMadt(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMadt(var s: String): Boolean; overload;

function  exDotMakh(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMakh(Sender: TField): Boolean; overload;
function  exDotMakh(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMakh(var s: String): Boolean; overload;

function  exDotMancc(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMancc(Sender: TField): Boolean; overload;
function  exDotMancc(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMancc(var s: String): Boolean; overload;

//FB
procedure exInitDotFBMavt;
function  exDotFBMavt(const Group: Integer; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotFBMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotFBMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

function  exDotMACP(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMACP(Sender: TField): Boolean; overload;
function  exDotMACP(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotMACP(var s: String): Boolean; overload;

function  exDotVip(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotVip(DataSet: TDataSet; var s: String;
    exCond: String = ''): Boolean; overload;
function  exDotVip(var s: String; exCond: String = ''): Boolean; overload;

procedure exInitDotMavt;
function  exDotMavt(const Group: Integer; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

procedure exInitDotPomMavt;
function  exDotPomMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotPomMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String = ''): Boolean; overload;

//HR
procedure FlexOrgCombo(Sender: TfcTreeCombo; const pDep: String = '');
procedure FlexOrgComboDataAccess(Sender: TfcTreeCombo; const pDep: String = '');
function  exGetFlexOrgDesc(Sender: TObject): String;

function  exClearListConfirm: Boolean;

function  openImportFile(pHandle: HWND; pName: String) : Boolean;

function getSizeOfFile(FileName: String ): Int64;

function initMonthList(Sender : TwwDBComboBox; curMonth : Integer): Boolean;
function initYearList(Sender : TwwDBComboBox; curYear : Integer): Boolean;
function validateEmail(const emailAddress: string): Boolean;
function validateUrl(const url: string): Boolean;
function isFileInUse(FileName: TFileName): Boolean;
function toNonAccentVietnamese(content: string): string;
function lookupComboboxEhShowImages(adoQuery: TADOQuery; CbbLookup: TDBLookupComboboxEh;
fieldName: string; iColumnImage: Integer): Boolean;
function  exStatusAudit(pDataSet: TDataSet): String;
function deleteImageByFileIdx(mFileIdx: TGUID): Boolean;
function getIdentityCurrentByTable(sTableName: string): Integer;
function getTranslateByResource(sResourceName: string): String;

implementation

uses
	isMsg, isStr, Printers, MainData, IniFiles, isCommon, Math, isFile,
    Rights, isDb, GuidEx, isLib, isNameVal, RepEngine, isEnCoding, isType;


	(*
    ** Message
    *)
(*==============================================================================
** Arguments:
**	dt:			Ngay can kiem tra
**	subsys:		1 	Hang hoa
**				2 	Cong no
**------------------------------------------------------------------------------
*)
procedure exValidClosing(const dt: TDateTime; const subsys: Integer);
var
	closing: TDateTime;
begin
	case subsys of
    1:	// Hang hoa
    	closing := sysCloseHH;
    2:	// Cong no
    	closing := sysCloseCN;
    else
    	Exit;
    end;

    if dt <= closing then
	begin
		ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(closing)]));
    	Abort;
    end;
end;

(*==============================================================================
** Arguments:
**		dt:		Record date
**------------------------------------------------------------------------------
*)
procedure exValidRecordDate(const dt: TDateTime; bEdit: Boolean);
begin
//	// License expired
//    if IsLicenseExpired(dt) then
//    	Abort;

    if dt < sysBegDate then
    begin
        ErrMsg(Format(RS_CANT_INPUT_DATE, [DateToStr(sysBegDate)]));
        Abort;
    end;

    if bEdit then
    begin
        if not YesNo('Ngày tạo phiếu có thay đổi. Tiếp tục?') then
            Abort;
    end;

    // Suspicious date
    if not cfRecordDate then
        Exit;

    if (dt <= Date + sysLateDay) and (dt + sysLateDay >= Date) then
		Exit;

    if YesNo(Format('Ngày tạo phiếu quá định mức nhập liệu (%s ngày), xác nhận đã nhập đúng ngày. Tiếp tục?', [floattostr(sysLateDay)])) then
        Exit;

    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_WAREHOUSE_NOT_DEFAULT = 'Chưa chỉ định mã kho.';

function exDefaultWarehouse: Boolean;
begin
	Result := sysDefKho <> '';
	if Result then
    else
    	Msg(RS_WAREHOUSE_NOT_DEFAULT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_FINISH_INPUT = 'Phải nhập liệu hoàn tất chứng từ này.';

function exCanChange(Sender: TPageControl; DataSet: TCustomADODataSet): Boolean;
begin
	with Sender do
		if ActivePageIndex = 0 then
        	Result := not DataSet.IsEmpty
        else
        begin
        	Result := DataSet.State in [dsBrowse];
        	if not Result then
	        	Msg(RS_FINISH_INPUT);
		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_MUST_COMPLETED = 'Phải hoàn tất nhập liệu.';

procedure exCompleteConfirm;
begin
	Msg(RS_MUST_COMPLETED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exValidMatchGroup(pNganh, pNhom, pNhom2, pNhom3, pNhom4: String): Boolean;
var
    s: String;
begin
    if pNhom4 <> '' then
        s := Format(
            'select 1 from DM_NHOM4 where MANHOM4=''%s'' and MANHOM3=''%s''' +
            ' and MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom4, pNhom3, pNhom2, pNhom, pNganh])
    else if pNhom3 <> '' then
        s := Format(
            'select 1 from DM_NHOM3 where MANHOM3=''%s''' +
            ' and MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom3, pNhom2, pNhom, pNganh])
    else if pNhom2 <> '' then
        s := Format(
            'select 1 from DM_NHOM2 where MANHOM2=''%s'' and MANHOM=''%s'' and MANGANH=''%s''',
            [pNhom2, pNhom, pNganh])
    else
        s := Format(
            'select 1 from DM_NHOM where MANHOM=''%s'' and MANGANH=''%s''', [pNhom, pNganh]);

    with TADOQuery.Create(Nil) do
    begin
        Connection  := DataMain.Conn;
        LockType    := ltReadOnly;

        SQL.Text := s;

        Open;
        Result := not IsEmpty;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure FlexGroupCombo;
var
    mId, _Sql: String;
	Qr1, Qr2, Qr3, Qr4, Qr5: TADOQuery;
    it1, it2, it3, it4, it5: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 15;
    end;

    Qr1 := TADOQuery.Create(Nil);
    Qr2 := TADOQuery.Create(Nil);
    Qr3 := TADOQuery.Create(Nil);
    Qr4 := TADOQuery.Create(Nil);
    Qr5 := TADOQuery.Create(Nil);

    Qr1.Connection := DataMain.Conn;
    Qr2.Connection := DataMain.Conn;
    Qr3.Connection := DataMain.Conn;
    Qr4.Connection := DataMain.Conn;
    Qr5.Connection := DataMain.Conn;

    Qr1.LockType := ltReadOnly;
    Qr2.LockType := ltReadOnly;
    Qr3.LockType := ltReadOnly;
    Qr4.LockType := ltReadOnly;
    Qr5.LockType := ltReadOnly;

    with Qr1 do
    begin
        if loai <> '' then
        begin
            _Sql := 'select MANGANH, TENNGANH from DM_NGANH';

            _Sql := _Sql + ' where LOAI in (''' + loai + ''')';
            _Sql := _Sql + ' order by MANGANH';
        end else
            _Sql := 'select MANGANH, TENNGANH from DM_NGANH order by MANGANH';

        SQL.Text := _Sql;
        Open;
        while not Eof do
        begin
            mId := FieldByName('MANGANH').AsString;
            it1 := Sender.Items.Add(Nil, Format('[%s] %s', [FieldByName('MANGANH').AsString, FieldByName('TENNGANH').AsString]));
            it1.StringData := mId;
            // it1.ImageIndex := 0;

            with Qr2 do
            begin
                SQL.Text := Format('select MA, MANHOM, TENNHOM from DM_NHOM where MANGANH=''%s'' order by MANHOM', [mId]);
                Open;
                while not Eof do
                begin
                    mId := FieldByName('MANHOM').AsString;
                    it2 := Sender.Items.AddChild(it1, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM').AsString]));
                    it2.StringData := mId;
		            // it2.ImageIndex := 1;

                    with Qr3 do
                    begin
                        SQL.Text := Format('select MA, MANHOM2, TENNHOM2 from DM_NHOM2 where MANHOM=''%s'' order by MANHOM2', [mId]);
                        Open;
                        while not Eof do
                        begin
                            mId := FieldByName('MANHOM2').AsString;
                            it3 := Sender.Items.AddChild(it2, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM2').AsString]));
                            it3.StringData := mId;
				            // it3.ImageIndex := 2;

                            with Qr4 do
                            begin
                                SQL.Text := Format('select MA, MANHOM3, TENNHOM3 from DM_NHOM3 where MANHOM2=''%s'' order by MANHOM3', [mId]);
                                Open;
                                while not Eof do
                                begin
                                    mId := FieldByName('MANHOM3').AsString;
                                    it4 := Sender.Items.AddChild(it3, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM3').AsString]));
                                    it4.StringData := mId;
                                    // it4.ImageIndex := 3;

                                    with Qr5 do
                                    begin
                                        SQL.Text := Format('select MA, MANHOM4, TENNHOM4 from DM_NHOM4 where MANHOM3=''%s'' order by MANHOM4', [mId]);
                                        Open;
                                        while not Eof do
                                        begin
                                            mId := FieldByName('MANHOM4').AsString;
                                            it5 := Sender.Items.AddChild(it4, Format('[%s] %s', [FieldByName('MA').AsString, FieldByName('TENNHOM4').AsString]));
                                            it5.StringData := mId;
                                            // it5.ImageIndex := 4;

                                            Next;
                                        end;
                                        Close;
                                    end;
                                    Next;
                                end;
                                Close;
                            end;
                            Next;
                        end;
                        Close;
                    end;
                    Next;
                end;
                Close;
            end;
            Next;
        end;
        Close;
    end;

    Qr1.Free;
    Qr2.Free;
    Qr3.Free;
    Qr4.Free;
    Qr5.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
    		SetSelectedNode(Items[0]);
	    	Text := Items[0].Text;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_LEV0 = 'Tất cả ngành';
    RS_LEV1 = 'Ngành';
    RS_LEV2 = 'Nhóm';
    RS_LEV3 = 'Nhóm 2';
    RS_LEV4 = 'Nhóm 3';
    RS_LEV5 = 'Nhóm 4';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetFlexDesc(Sender: TObject): WideString;
var
    s: String;
begin
    with Sender as TfcTreeCombo do
    begin
        if Text = '' then
            s := RS_LEV0
        else if SelectedNode <> Nil then
        begin
            case SelectedNode.Level of
            0:
                s := RS_LEV1;
            1:
                s := RS_LEV2;
            2:
                s := RS_LEV3;
            3:
                s := RS_LEV4;
            4:
                s := RS_LEV5;
            end;
        end;
    end;
    Result := s
end;

procedure FlexGroupComboDT;
var
    mId, _Sql: String;
	Qr1: TADOQuery;
    it1: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 4;
    end;

    Qr1 := TADOQuery.Create(Nil);
    Qr1.Connection := DataMain.Conn;
    Qr1.LockType := ltReadOnly;

    with Qr1 do
    begin
        _Sql := 'select MA, DGIAI from	DM_LOAI_DT order by  MA';

        SQL.Text := _Sql;
        Open;
        while not Eof do
        begin
            mId := FieldByName('MA').AsString;
            it1 := Sender.Items.Add(Nil, Format('%s', [FieldByName('DGIAI').AsString]));
            it1.StringData := mId;
            // it1.ImageIndex := 0;

            Next;
        end;
        Close;
    end;

    Qr1.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
    		//SetSelectedNode(Items[0]);
	    	Text := '';//Items[0].Text;
    	end;
end;

procedure FlexGroupComboTS;
var
    mId, _Sql: String;
	Qr1: TADOQuery;
    it1: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 4;
    end;

    Qr1 := TADOQuery.Create(Nil);
    Qr1.Connection := DataMain.Conn;
    Qr1.LockType := ltReadOnly;

    with Qr1 do
    begin
        _Sql := 'select MA, DGIAI from	V_TS_DM_LOAI_DT order by  MA';

        SQL.Text := _Sql;
        Open;
        while not Eof do
        begin
            mId := FieldByName('MA').AsString;
            it1 := Sender.Items.Add(Nil, Format('%s', [FieldByName('DGIAI').AsString]));
            it1.StringData := mId;
            // it1.ImageIndex := 0;

            Next;
        end;
        Close;
    end;

    Qr1.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
    		SetSelectedNode(Items[0]);
	    	Text := Items[0].Text;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exVNDRound(_pExtended: Extended; _pDigit: Integer = -1000; _pRoundNum: Integer = -1000): Extended;
begin
    if _pRoundNum = -1000 then
        _pRoundNum := sysNumRound;

    if _pDigit = -1000 then
        _pDigit := sysCurRound;

    Result := RoundTo(_pExtended, _pDigit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CAN_NOT_EDIT_CHECKED = 'Dữ liệu đã %s, không thể chỉnh sửa.';

function  exIsChecked(ds: TDataSet; pAbort: Boolean = True): Boolean;
begin
    Result := ds.FieldByName('CHECKED').AsBoolean;
    if Result then
    begin
        ErrMsg(Format(RS_CAN_NOT_EDIT_CHECKED, ['Khóa']));
        if pAbort then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exChecked(pDataset: TDataSet; pRight: String);
begin
    with pDataset do
    begin
        if not FieldByName('CHECKED').AsBoolean then
        begin
            if not rCanEdit(GetRights(pRight + '_CHECKED')) then
                Exit;
            if not YesNo(Format('Dữ liệu sau khi %s không thể chỉnh sửa. Tiếp tục?', ['Khóa']), 1) then
                Exit;
        end
        else
        begin
            if not rCanEdit(GetRights(pRight + '_UNCHECKED')) then
                Exit;
            if not YesNo('Cho phép chỉnh sửa dữ liệu. Tiếp tục?', 1) then
                Exit;
        end;

        try
            mTrigger := True;
            CheckedDataSet(pDataSet as TADOQuery);
        finally
            mTrigger := False;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetCheckedCaption(pDataSet: TDataSet): String;
begin
    with pDataSet.FieldByName('CHECKED') do
        if AsBoolean then
            Result := 'Mở khóa'
        else
            Result := 'Khóa';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exCheckNhomthue(pNhomthue: String; pDataset: TCustomADODataSet): Boolean;
begin
    Result := True;
    if pDataset.IsEmpty then
        Exit;

    with pDataset do
    begin
        First;
        if pNhomthue = '' then
            pNhomthue := FieldByName('LOAITHUE').AsString;
        while not Eof do
        begin
            if pNhomthue <> FieldByName('LOAITHUE').AsString then
            begin
                Msg(RS_VAT_NOTSAME);
                Result := False;
                Break;
            end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exCheckLoc(pDataset: TDataSet; pAbort: Boolean = True; AFIELD: string = 'LOC'): Boolean;
begin
    Result := SameText(pDataset.FieldByName(AFIELD).AsString, sysLoc);
    if (not Result) and pAbort then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  exIsCheckSL(pSl: Double; pMSG: Boolean = True): Boolean;
begin
    Result := (pSl >= soluongMin) and (pSl <= soluongMax);

    if not Result then
        if pMSG then
            ErrMsg(RS_INVALID_QTY);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure AddAllFields(pDataSet: TCustomADODataSet;
        pTable: String = ''; pIndex: Integer = 0);
var
    i, n: Integer;
    fld: TField;
begin
    with TADOQuery.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select * from [%s] where 1=0', [pTable]);
        Open;

        if pIndex = -1 then
            pDataSet.FieldDefs.Update
        else
        begin
            n := pDataSet.FieldDefs.Count;
            for i := pIndex to FieldCount - 1 do
            begin
                fld := Fields[i];
                if pDataSet.Fields.FindField(fld.FieldName) = nil then
                    pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;

            for i := n to pDataSet.FieldDefs.Count - 1 do
                pDataSet.FieldDefs[i].CreateField(pDataSet);
        end;

        Close;
        Free;
    end;

    for i := 0 to pDataSet.FieldDefs.Count - 1 do
        if pDataSet.FindField(pDataSet.FieldDefs[i].Name) = nil then
            pDataSet.FieldDefs[i].CreateField(pDataSet);
end;

procedure AddFields(pDataSet: TCustomADODataSet;
        pTable, pFields: String);
var
    i: Integer;
    fld: TField;
    ls: TStrings;
begin
    ls := TStringList.Create;
    isStrBreak(pFields, ';', ls);
    with TADOQuery.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select * from [%s] where 1=0', [pTable]);
        Open;
        if pFields = '' then
        begin
            for i := 0 to FieldCount - 1 do
            begin
                fld := Fields[i];
                if pDataSet.Fields.FindField(fld.FieldName) = nil then
                    pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;
        end
        else
        begin
            for i := 0 to ls.Count - 1 do
            begin
                fld := Fields.FindField(ls[i]);
                if fld <> nil then
                    if pDataSet.Fields.FindField(fld.FieldName) = nil then
                        pDataSet.FieldDefs.Add(fld.FieldName, fld.DataType, fld.Size);
            end;
        end;

        Close;
        Free;
    end;
    ls.Free;

    for i := 0 to pDataSet.FieldDefs.Count - 1 do
        if pDataSet.FindField(pDataSet.FieldDefs[i].Name) = nil then
            pDataSet.FieldDefs[i].CreateField(pDataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function IsDuplicateCode(pQuery: TCustomADODataSet;
	pValue: TField; pShowMgs: Boolean): Boolean;
begin
	Result := False;
	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
  		Clone(pQuery);

        if Locate(pValue.FieldName, pValue.Value, []) then
        begin
        	Result := RecNo <> pQuery.RecNo;
        	if Result and pShowMgs then
        		ErrMsg(Format(RS_ITEM_CODE_DUPLICATE, [pValue.DisplayLabel]));
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function IsDuplicateCode2(pQuery: TCustomADODataSet;
	pValue: TField; var pBm: TBytes): Boolean;
begin
	Result := False;
	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
  		Clone(pQuery);

        if Locate(pValue.FieldName, pValue.Value, []) then
        begin
        	Result := RecNo <> pQuery.RecNo;
            pBm := Bookmark;
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function IsDuplicateCode3(pQuery: TCustomADODataSet;
	pDisplayLabel: String; pConditionFields: string; pConditionValues: array of Variant; pShowMgs: Boolean = True): Boolean; overload;
begin
	Result := False;
	with TADOQuery.Create(Nil) do
    begin
    	Connection := DataMain.Conn;
  		Clone(pQuery);

        if Locate(pConditionFields, VarArrayOf(pConditionValues), []) then
        begin
        	Result := RecNo <> pQuery.RecNo;
        	if Result then
        		ErrMsg(Format(RS_ITEM_CODE_DUPLICATE, [pDisplayLabel]));
        end;
        Close;
        Free;
    end;
end;

function IsDuplicateCode3(pQuery: TCustomADODataSet;
	pDisplayField: TField; pConditionFields: string; pConditionValues: array of Variant; pShowMgs: Boolean = True): Boolean; overload;
begin
    Result := IsDuplicateCode3(pQuery, pDisplayField.AsString, pConditionFields, pConditionValues, pShowMgs)
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)

function exGetImportFile(const pType: Integer = 2): String;
begin
    case pType of
    1:	// Recorder data
    	Result := isGetOpenFileName('ALL', 1, GetSysParam('FOLDER_RECORDER'));
    2:	// Import data
    	Result := isGetOpenFileName('CSV;TXT;ALL', 1, GetSysParam('FOLDER_IMPORT'));
    3:	// Import data
    	Result := isGetOpenFileName('XLS;ALL', 1, GetSysParam('FOLDER_IMPORT'));
    else
    	Result := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exViewLog(const LogName, Content: String);
var
    s: String;
begin
	s := sysAppTempPath+ LogName + '.log';
    with TStringList.Create do
    begin
        Text := Content;
        SaveToFile(s);
        Free;
    end;
    ShellExecute(0, 'Open', PChar(s), nil,  nil, SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function GetToggleMarkCaption(DataSet: TCustomADODataSet) : String;
begin
	Result := 'Xóa';
	with DataSet do
		if Active then
			if FieldByName('DELETE_BY').AsString = '' then
            else
        		Result := 'Phục hồi'
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure ToggleMarkDataSet(DataSet: TCustomADODataSet);
begin
    with DataSet do
    begin
        Edit;

        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            FieldByName('DELETE_BY').Clear;
            FieldByName('DELETE_DATE').Clear;
        end
        else
        begin
            FieldByName('DELETE_BY').AsInteger := sysLogonUID;
            FieldByName('DELETE_DATE').AsDateTime := Date;
        end;

        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TransFieldDesc(ds: TDataSet; flst: String): String;
var
	i: Integer;
    ls: TStrings;
begin
    ls := TStringList.Create;
    ls.Text := flst;

    Result := '';
	for i := 0 to ls.Count - 1 do
    	Result := Result + ds.FieldByName(ls.Strings[i]).DisplayName + #13;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function MakeEraseSQL(ds: TDataSet; tn, flst: String): String;
var
	i: Integer;
    ls: TStrings;

    (*
    **
    *)
    function SmartValue(fn: String): String;
    begin
    	case ds.FieldByName(fn).DataType of
        ftFloat, ftInteger:
        	Result := '0';
		else
        	Result := 'NULL';
        end;
    end;

begin
   	ls := TStringList.Create;
	ls.Text := flst;

    Result := Format('update %s set [%s]=%s', [tn, ls.Strings[0], SmartValue(ls.Strings[0])]);
    for i := 1 to ls.Count - 1 do
    	Result := Result + Format(', [%s]=%s', [ls.Strings[i], SmartValue(ls.Strings[i])]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
begin
    Sender.LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo);
begin
    Sender.LookupTable.Filter := 'PLOAI=''NB''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exExecSQL(const sqlStr: String): Integer;
begin
	Result := DataMain.ExecSQL(sqlStr);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exReSyncRecord(DataSet: TCustomADODataSet; pAll: Boolean = False);
begin
    with DataSet do
    begin
        UpdateCursorPos;
        Recordset.Resync(Iif(pAll, adAffectAll, adAffectCurrent), adResyncAllValues);
        Resync([]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exDbCopyFields(pDataSet, pSource: TCustomADODataSet;
    pOveride: Boolean; pExceptField: String);
var
	i: Integer;
    fld: TField;
begin
    SetEditState(pDataSet);

    with pDataSet do
    for i := 0 to pSource.FieldCount - 1 do
    begin
        fld := FindField(pSource.Fields[i].FieldName);
        if (fld <> Nil) and (fld.FieldKind in [fkData]) and not fld.ReadOnly then
        begin
            if (pExceptField <> '') and (Pos(fld.FieldName + ';', pExceptField + ';') > 0) then
            else if pOveride or (FieldByName(fld.FieldName).Value <> pSource.FieldByName(fld.FieldName).Value) then
                    FieldByName(fld.FieldName).Value := pSource.FieldByName(fld.FieldName).Value;
        end;
    end;
end;


(*==============================================================================
** Đánh lại STT cho chi tiết phiếu trước khi lưu
**------------------------------------------------------------------------------
*)
procedure exSaveDetails(DataSet: TCustomADODataSet);
var
	bm: TBytes;
    n: Integer;
begin
    with DataSet do
    begin
        bm := Bookmark;
        DisableControls;

        First;
        mTrigger := True;
        while not Eof do
        begin
        	n := RecNo;
            if FieldByName('STT').AsInteger <> n then
            begin
                Edit;
                FieldByName('STT').AsInteger := n;
            end;
            Next;
        end;

        Bookmark := bm;
        EnableControls;
        UpdateBatch;
        mTrigger := False;
    end;
end;


(*==============================================================================
** Xóa chi tiết phiếu
**------------------------------------------------------------------------------
*)
procedure exEmptyDetails(DataSet: TDataSet; ActiveControl: TWinControl);
begin
	if not YesNo(RS_XOA_CHITIET, 1) then
    	Exit;
    DeleteConfirm(False);
	EmptyDataset(DataSet);
    DeleteConfirm(True);

    if Assigned(ActiveControl) then
    	ActiveControl.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exConfigInspector(pIns: TwwDataInspector; pFieldNames: String): Integer;
var
    i, ret: Integer;
    fld: TField;
	ls: TStrings;
    it: TwwInspectorItem;

begin
    ls := TStringList.Create;
    isStrBreak(pFieldNames, ';', ls);

    // Update list of fields
    for i := 0 to ls.Count - 1 do
    begin
        with pIns do
        begin
            fld := DataSource.DataSet.FindField(ls[i]);
            it := GetItemByFieldName(ls[i]);
            if it <> Nil then
            begin
                it.Caption := it.Field.DisplayName;
                it.Tag := 1;
            end
            else if fld <> Nil then
            begin
                it := Items.Add;
                it.DataField := ls[i];
                it.Caption := fld.DisplayLabel;
                it.Tag := 1;
            end;

            // Checkbox
            if (fld <> Nil) and (fld.DataType = ftBoolean) then
            with it.PickList do
            begin
                DisplayAsCheckbox := True;
                Items.Add('True');
                Items.Add('False');
            end;
            it.Index := i;
        end;
    end;

	ret := 0;
    if ls.Count > 0 then
        with pIns do
        begin
            for i := 0 to Items.Count - 1 do
            begin
                Items.Items[i].Visible := Items.Items[i].Tag = 1;
                if Items.Items[i].Visible then
                	Inc(ret);
            end;
            Invalidate;
        end;

    ls.Free;
    Result := ret;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exConfigVertGrid(pVGrid: TDBVertGridEh; pFieldNames: String): Integer;
var
    i, ret: Integer;
    fld: TField;
	ls: TStrings;
    it: TFieldRowEh;

begin
    ls := TStringList.Create;
    isStrBreak(pFieldNames, ';', ls);

    // Update list of fields
    for i := 0 to ls.Count - 1 do
    begin
        with pVGrid do
        begin
            fld := DataSource.DataSet.FindField(ls[i]);
            it := pVGrid.FindFieldRow(ls[i]);
            if it <> Nil then
            begin
                it.RowLabel.Caption := it.Field.DisplayName;
                it.Tag := 1;
            end
            else if fld <> Nil then
            begin
                it := Rows.Add;
                it.FieldName := ls[i];
                it.RowLabel.Caption := fld.DisplayLabel;
                it.Tag := 1;
            end;

            if (fld <> Nil) then
            begin
                // Checkbox
                if (fld.DataType = ftBoolean) then
                    it.DblClickNextVal := True
                else if (fld.DataType = ftDateTime) or (fld.FieldKind = fkLookup) then
                    it.EditButton.Width := 20
                else if (fld.DataType = ftFloat) then
                    (fld as TFloatField).EditFormat := '###0.##;-###0.##;#';
            end;

            it.Index := i;
        end;
    end;

	ret := 0;
    if ls.Count > 0 then
        with pVGrid do
        begin
            for i := 0 to RowCount - 1 do
            begin
                Rows[i].Visible := Rows[i].Tag = 1;
                if Rows[i].Visible then
                	Inc(ret);
            end;
//            Invalidate;
        end;

    ls.Free;
    Result := ret;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function CanEditLockedBy(Dataset: TDataSet; pShowMessage: Boolean): Boolean;
begin
    Result := True;
    with (Dataset as TADOQuery) do
    begin
        if FieldByName('LOCKED_BY').AsInteger <> 0 then
        begin
            Result := False;
            if pShowMessage then
                ErrMsg(Format(RS_EDIT_BY, [FieldByName('LK_LOCKED_NAME').AsString,
                            DateTimeToStr(FieldByName('LOCKED_DATE').AsDateTime)]));
//                if YesNo(Format(RS_EDIT_BY, [FieldByName('LK_LOCKED_NAME').AsString,
//                            DateTimeToStr(FieldByName('LOCKED_DATE').AsDateTime)]), 1) then
//                    Result := GetRights('PHIEU_BAOGIA_EDIT_LOCK') <> R_DENY
        end;
    end;
end;

procedure SetLockedBy;
begin
    with (Dataset as TADOQuery) do
    begin
        mTrigger := True;
        Edit;
        if b then
        begin
            FieldByName('LOCKED_BY').AsInteger := sysLogonUID;
            FieldByName('LOCKED_DATE').AsDateTime := Now;
        end else
        begin
            FieldByName('LOCKED_BY').Clear;
            FieldByName('LOCKED_DATE').Clear;
        end;
        Post;
        mTrigger := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function ShowReport (pTitle, pRepName : String; Args: array of Variant): Boolean; overload;
begin
    Result := FrmRep.Execute(pTitle, pRepName, Args);
end;
function ShowReport (pTitle, pRepName : String): Boolean; overload;
begin
    Result := ShowReport(pTitle, pRepName, []);
end;

function PrintReport ( pRepName : String; Args: array of Variant; const PrinterName: String; const pCopy: Integer): Boolean; overload;
begin
    Result := FrmRep.Execute('', pRepName, Args, True, True, PrinterName, pCopy);
end;

	(*
    ** Refs. functions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_USER =
        'USERNAME'#9'20'#9'Tên đăng nhập'#13 +
        'FULLNAME'#9'30'#9'Tên đầy đủ';

function exDotUSER(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_USER, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotUSER(Sender: TField): Boolean;
begin
	Result := exDotUSER(DataMain.QrUSER, Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MACP =
    	'MACP'#9'12'#9'Mã'#13 +
        'TENCP'#9'30'#9'Tên'#13 +
    	'TENCP_NHOM'#9'30'#9'Tên chí phí nhóm'#13 +
    	'TINHTRANG'#9'12'#9'Ngừng sử dụng'#13 +
    	'GHICHU'#9'20'#9'Ghi chú';

function exDotMACP(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MACP, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMACP(Sender: TField): Boolean;
begin
	Result := exDotMACP(DataMain.QrDM_CHIPHI, Sender);
end;

function exDotMACP(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MACP);
end;
function exDotMACP(var s: String): Boolean;
begin
    Result := exDotMACP(DataMain.QrDM_CHIPHI, s);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MADT =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMadt(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MADT, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMadt(Sender: TField): Boolean;
begin
	Result := exDotMadt(DataMain.QrDM_KH_NCC, Sender);
end;

function exDotMadt(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MADT);
end;
function exDotMadt(var s: String): Boolean;
begin
    Result := exDotMadt(DataMain.QrDM_KH_NCC, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MAKH =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMakh(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MAKH, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMakh(Sender: TField): Boolean;
begin
	Result := exDotMakh(DataMain.QrDMKH, Sender);
end;

function exDotMakh(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MAKH);
end;
function exDotMakh(var s: String): Boolean;
begin
    Result := exDotMadt(DataMain.QrDMKH, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_MANCC =
    	'MADT'#9'12'#9'Mã'#13 +
        'TENDT'#9'30'#9'Tên'#13 +
    	'MST'#9'12'#9'MST'#13 +
    	'DCHI'#9'30'#9'Địa chỉ'#13 +
    	'DTHOAI'#9'20'#9'Điện thoại';

function exDotMancc(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MANCC, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMancc(Sender: TField): Boolean;
begin
    Result := exDotMancc(DataMain.QrDMNCC, Sender);
end;

function exDotMancc(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_MANCC);
end;
function exDotMancc(var s: String): Boolean;
begin
    Result := exDotMancc(DataMain.QrDMNCC, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_SEARCH_VIP =
		'MAVIP'#9'15'#9'Mã VIP'#13 +
      	'HOTEN'#9'30'#9'Họ tên'#13 +
      	'MASO'#9'15'#9'Thẻ VIP';

function exDotVip(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_VIP, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;
function exDotVip(DataSet: TDataSet; var s: String; exCond: String): Boolean;
begin
    Result := QuickSelect3(DataSet, s, DOT_SEARCH_VIP, exCond);
end;

function exDotVip(var s: String; exCond: String): Boolean;
begin
    Result := exDotVip(DataMain.QrDMVIP, s, exCond);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_MAVT: array[1..4] of String = (
        //Phiếu nhập, Xuất trả, ĐĐH Ncc
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        //Phiếu Xuất, Nhập trả, ĐĐH KH
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        //Nhập khác, xuất khác, chuyển kho, kiểm kê
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        //Bán lẻ, khuyến mãi, phân bổ
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'TL_LAI'#9'8'#9'Tỷ lệ lãi'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'GIASI'#9'15'#9'Giá sỉ'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp'
	);

procedure exInitDotMavt;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 4 do
		SetCustomGrid('REF_MAVT_' + IntToStr(i), DOT_SEARCH_MAVT[i]);
end;

function exDotMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond <> '' then
        exCond := exCond + ' and TINHTRANG=''01'''
    else
        exCond := 'TINHTRANG=''01''';
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_MAVT[Group], exCond);
end;

function exDotMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotMavt(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotMavt(const Group: Integer; Sender: TField; exCond: String): Boolean;
begin
    Result := exDotMavt(Group, DataMain.QrDMVT, Sender, exCond);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_POM_MAVT: array[1..3] of String = (
        //Thành phẩm
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT',

        //Nguyên liệu
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIAVON'#9'15'#9'Giá vốn',

        //Bán thành phẩm
    	'MAVT'#9'15'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'DVT'#9'8'#9'ĐVT'#13 +
        'GIAVON'#9'15'#9'Giá vốn'
	);

procedure exInitDotPomMavt;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 4 do
		SetCustomGrid('REF_POM_MAVT_' + IntToStr(i), DOT_SEARCH_POM_MAVT[i]);
end;

function exDotPomMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond <> '' then
        exCond := exCond + ' and TINHTRANG=''01'''
    else
        exCond := 'TINHTRANG=''01''';
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_POM_MAVT[Group], exCond);
end;

function exDotPomMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotPomMavt(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
var
    DOT_SEARCH_FB_MAVT: array[1..6] of String = (
        //Thành phẩm
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT',

        //Nguyên liệu
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT'#13 +
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13,

        //FB_DM_HH
    	'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'ĐVT'#13 +
        'GIAVON'#9'15'#9'Giá vốn'+
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13 +
        'GIABAN_CHUA_VAT'#9'15'#9'Giá bán chưa VAT'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        //Phiếu nhập, Xuất trả, ĐĐH Ncc
        'MAVT'#9'16'#9'Mã'#13 +
        'TENVT'#9'30'#9'Tên'#13 +
        'TenDvt'#9'8'#9'SKU'#13 +
        'QuyDoi'#9'8'#9'QĐ'#13 +
        'TenDvtLon'#9'8'#9'Hộp'#13 +
        'GIANHAP'#9'15'#9'Giá nhập'#13 +
        'GIANHAP_CHUA_VAT'#9'15'#9'Giá nhập chưa VAT'#13 +
        'GIABAN'#9'15'#9'Giá lẻ'#13 +
        'TENLT'#9'15'#9'Thuế VAT'#13 +
        'TENDT'#9'30'#9'Nhà cung cấp',

        'MANGANH'#9'15'#9'Mã'#13 +
        'TENNGANH'#9'30'#9'Tên'#13,

        'MANHOM'#9'15'#9'Mã'#13 +
        'TENNHOM'#9'30'#9'Tên'#13
	);

procedure exInitDotFBMavt;
var
    i: Integer;
begin
    mExDot := False;
	for i := 1 to 6 do
		SetCustomGrid('REF_FB_MAVT_' + IntToStr(i), DOT_SEARCH_FB_MAVT[i]);
end;

function exDotFBMavt(const Group: Integer; DataSet: TDataSet; var Sender: String;
	exCond: String): Boolean;
begin
    if exCond = 'OTHER' then
        exCond := ''
    else
    begin
    if exCond <> '' then
        exCond := exCond + ' and MaNganTinhTrang=''01'''
    else
        exCond := 'MaNganTinhTrang=''01''';
    end;
    Result := QuickSelect3(DataSet, Sender , DOT_SEARCH_FB_MAVT[Group], exCond);
end;

function exDotFBMavt(const Group: Integer; DataSet: TDataSet; Sender: TField;
	exCond: String): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := exDotFBMavt(Group, DataSet, s, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotFBMavt(const Group: Integer; Sender: TField; exCond: String): Boolean;
begin
    Result := exDotFBMavt(Group, DataMain.QrFB_DM_HH, Sender, exCond);
end;

	(*
    ** End: Refs. functions
    *)

procedure FlexOrgCombo(Sender: TfcTreeCombo; const pDep: String = '');
var
    mId: String;
	Qr0, Qr1, Qr2, Qr3: TADOQuery;
    it0, it1, it2, it3: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 15;
    end;

    Qr0 := TADOQuery.Create(Nil);
    Qr1 := TADOQuery.Create(Nil);
    Qr2 := TADOQuery.Create(Nil);
    Qr3 := TADOQuery.Create(Nil);

    Qr0.Connection := DataMain.Conn;
    Qr1.Connection := DataMain.Conn;
    Qr2.Connection := DataMain.Conn;
    Qr3.Connection := DataMain.Conn;

    Qr0.LockType := ltReadOnly;
    Qr1.LockType := ltReadOnly;
    Qr2.LockType := ltReadOnly;
    Qr3.LockType := ltReadOnly;

    // Site
    with Qr0 do
	begin
    	if pDep = '' then
			SQL.Text := 'select * from HR_DM_CHINHANH order by [MaChiNhanh]'
        else
			SQL.Text := 'select * from HR_DM_CHINHANH where [MaChiNhanh] in (select [MaChiNhanh] from DM_PHONGBAN where [MaPhongBan] in (' + pDep + ')) order by [MaPhongBan]';
        Open;

        while not Eof do
        begin
            mId := FieldByName('MaChiNhanh').AsString;
            it0 := Sender.Items.Add(Nil, Format('[%s] %s', [FieldByName('MaChiNhanh').AsString, FieldByName('TenChiNhanh').AsString]));
            it0.StringData := mId;
            it0.ImageIndex := 0;
//            if not sysManySites then
//            	Sender.Tag := 1;

            // Department
            with Qr1 do
            begin
            	if pDep = '' then
					SQL.Text := 'select * from HR_DM_PHONGBAN where [MaChiNhanh]=''' + mId + ''' order by [MaPhongBan]'
                else
					SQL.Text := 'select * from HR_DM_PHONGBAN where [MaChiNhanh]=''' + mId + ''' and [MaPhongBan] in (' + pDep + ') order by [MaPhongBan]';
                Open;

                while not Eof do
                begin
                    mId := FieldByName('MaPhongBan').AsString;
                    it1 := Sender.Items.AddChild(it0, Format('[%s] %s', [FieldByName('Ma').AsString, FieldByName('TenPhongBan').AsString]));
                    it1.StringData := mId;
                    it1.ImageIndex := 1;

    	            Next;
        	    end;
            	Close;
        	end;
            Next;
    	end;
        Close;
    end;


    Qr0.Free;
    Qr1.Free;
    Qr2.Free;
    Qr3.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
        	SetSelectedNode(Items[0]);
	    	//Text := Items[0].Text;
            Text := '';
		end;
end;

procedure FlexOrgComboDataAccess(Sender: TfcTreeCombo; const pDep: String = '');
var
    mId: String;
	Qr0, Qr1, Qr2, Qr3: TADOQuery;
    it0, it1, it2, it3: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 15;
    end;

    Qr0 := TADOQuery.Create(Nil);
    Qr1 := TADOQuery.Create(Nil);
    Qr2 := TADOQuery.Create(Nil);
    Qr3 := TADOQuery.Create(Nil);

    Qr0.Connection := DataMain.Conn;
    Qr1.Connection := DataMain.Conn;
    Qr2.Connection := DataMain.Conn;
    Qr3.Connection := DataMain.Conn;

    Qr0.LockType := ltReadOnly;
    Qr1.LockType := ltReadOnly;
    Qr2.LockType := ltReadOnly;
    Qr3.LockType := ltReadOnly;

    // Site
    with Qr0 do
	begin
    	if pDep = '' then
			SQL.Text := 'select * from HR_DM_CHINHANH cn where 1=1'
        else
			SQL.Text := 'select * from HR_DM_CHINHANH cn where [MaChiNhanh] in (select [MaChiNhanh] from DM_PHONGBAN where [MaPhongBan] in (' + pDep + '))';

        SQL.Add(' and exists(select 1 ' +
                ' from ( ' +
                   ' select	a.DATA_CODE ' +
                     ' from	( ' +
                       ' select	DATA_CODE, isnull([RIGHT], 0) [RIGHT] ' +
                         ' from	SYS_RIGHT_DATA ' +
                        ' where	UID = :UID ' +
                       ' union all ' +
                       ' select	b.DATA_CODE, isnull(b.[RIGHT], 0) ' +
                         ' from	SYS_GROUP a, SYS_RIGHT_DATA b ' +
                        ' where	a.MEMBER  = :UID ' +
                          ' and	a.[GROUP] = b.UID ' +
                       ' ) a ' +
                   ' group by a.DATA_CODE ' +
                   ' having max(a.[RIGHT]) > 0) r ' +
               ' where	cn.MaChiNhanh = left(r.DATA_CODE, len(cn.MaChiNhanh)) ' +
               ' )');
        SQL.Add(' order by MaChiNhanh');

        Parameters[0].value := sysLogonUID;
        Parameters[1].value := sysLogonUID;

        Open;

        while not Eof do
        begin
            mId := FieldByName('MaChiNhanh').AsString;
            it0 := Sender.Items.Add(Nil, Format('[%s] %s', [FieldByName('MaChiNhanh').AsString, FieldByName('TenChiNhanh').AsString]));
            it0.StringData := mId;
            it0.ImageIndex := 0;
//            if not sysManySites then
//            	Sender.Tag := 1;

            // Department
            with Qr1 do
            begin
            	if pDep = '' then
					SQL.Text := 'select * from HR_DM_PHONGBAN pb where [MaChiNhanh]=''' + mId + ''''
                else
					SQL.Text := 'select * from HR_DM_PHONGBAN pb where [MaChiNhanh]=''' + mId + ''' and [MaPhongBan] in (' + pDep + ')';

                SQL.Add(' and exists(select 1 ' +
                        ' from ( ' +
                           ' select	a.DATA_CODE ' +
                             ' from	( ' +
                               ' select	DATA_CODE, isnull([RIGHT], 0) [RIGHT] ' +
                                 ' from	SYS_RIGHT_DATA ' +
                                ' where	UID = :UID ' +
                               ' union all ' +
                               ' select	b.DATA_CODE, isnull(b.[RIGHT], 0) ' +
                                 ' from	SYS_GROUP a, SYS_RIGHT_DATA b ' +
                                ' where	a.MEMBER  = :UID ' +
                                  ' and	a.[GROUP] = b.UID ' +
                               ' ) a ' +
                           ' group by a.DATA_CODE ' +
                           ' having max(a.[RIGHT]) > 0) r ' +
                       ' where	pb.MaChiNhanh = r.DATA_CODE ' +
                           ' or pb.MaPhongBan = left(r.DATA_CODE, len(pb.MaPhongBan))' +
                       ' )');
                SQL.Add(' order by MaPhongBan');

                Parameters[0].value := sysLogonUID;
                Parameters[1].value := sysLogonUID;
                Open;

                while not Eof do
                begin
                    mId := FieldByName('MaPhongBan').AsString;
                    it1 := Sender.Items.AddChild(it0, Format('[%s] %s', [FieldByName('Ma').AsString, FieldByName('TenPhongBan').AsString]));
                    it1.StringData := mId;
                    it1.ImageIndex := 1;

    	            Next;
        	    end;
            	Close;
        	end;
            Next;
    	end;
        Close;
    end;


    Qr0.Free;
    Qr1.Free;
    Qr2.Free;
    Qr3.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
        	SetSelectedNode(Items[0]);
	    	Text := Items[0].Text;
            //Text := '';
		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetFlexOrgDesc(Sender: TObject): String;
var
    s: String;
begin
    with Sender as TfcTreeCombo do
    begin
        if Text = '' then
            s := RS_ORG0
        else if SelectedNode <> Nil then
        begin
            case SelectedNode.Level of
            0:
//            	if (Sender as TComponent).Tag = 1 then
	                s := RS_CONGTY;
//                else
//	                s := RS_CHINHANH;
            1:
                s := RS_ORG1;
            2:
                s := RS_ORG2;
            3:
                s := RS_ORG3;
            end;
        end;
    end;
    Result := s;
end;

function  GetBigint: LargeInt;
begin
    with TADOCommand.Create(nil) do
    begin
        Connection := sysDbConnection;
        CommandType := cmdStoredProc;
        CommandTimeout := 1200;
        CommandText := 'SYS_SEQ_ID;1';

        Parameters.Refresh;
        Execute;
        if Parameters.ParamValues['@RETURN_VALUE'] = 0 then
            Result := Parameters.ParamValues['@ID']
        else
            Result := 0;

        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CFM_CLEARLIST = 'Toàn bộ dữ liệu trên màn hình sẽ bị xóa. Tiếp tục?';

function exClearListConfirm: Boolean;
begin
	Result := YesNo(RS_CFM_CLEARLIST, 1);
end;


function openImportFile(pHandle: HWND; pName: String) : Boolean;
var
	sn, sFile: String;
    Qr1: TADOQuery;
begin
    Qr1 := TADOQuery.Create(Nil);
    Qr1.Connection := DataMain.Conn;
    Qr1.LockType := ltReadOnly;

    with Qr1 do
    begin
        SQL.Text := 'select * from SYS_DOCUMENT WHERE [NAME]=''' + pName + '''';
        Open;
        while not Eof do
        begin
            sn := isStripToneMark(FieldByName('FILENAME').AsString);
            sFile := sysAppData + sn + FieldByName('EXT').AsString;
            try
                TBlobField(FieldByName('CONTENT')).SaveToFile(sFile);
            except
                ErrMsg('Lỗi trích xuất nội dung.');
                Exit;
            end;
            Next;
        end;
        Close;
    end;

    ShellExecute(pHandle, 'Open', PChar(sFile), Nil, Nil, SW_SHOWMAXIMIZED);

    Result := True;
end;

function getSizeOfFile(FileName: String ): Int64;
var
  Rec : TSearchRec;
begin
  Result := 0;
  if (FindFirst(FileName, faAnyFile, Rec) = 0) then
  begin
    Result := Rec.Size;
    FindClose(Rec);
  end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function initMonthList(Sender : TwwDBComboBox; curMonth : Integer) : Boolean;
var
    i : Integer;
    dd, yy, mm  : WORD;
begin
  with Sender do
  begin
    if curMonth = 0 then
    begin
      DecodeDate (Date, yy, mm, dd);
    end;

    Items.Clear;
    for i := 1 to 12 do
      Items.Add (isPadLeft(IntToStr(i), 2));
  end;

   Result := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function initYearList(Sender : TwwDBComboBox; curYear : Integer) : Boolean;
var
	i : Integer;
    dd, yy, mm  : WORD;
begin
  with Sender do
  begin
     if curYear = 0 then
        DecodeDate (Date, yy, mm, dd)
     else
      yy := curYear;

    Items.Clear;
    for i := yy - 10 to yy + 10 do
      Items.Add (isPadLeft(IntToStr(i), 4));
  end;

   Result := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function validateEmail(const emailAddress: string): Boolean;
var
  RegEx: TRegEx;
begin
  RegEx := TRegex.Create('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]*[a-zA-Z0-9]+$');
  Result := RegEx.Match(emailAddress).Success;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function validateUrl(const url: string): Boolean;
var
  RegEx: TRegEx;
begin
  RegEx := TRegex.Create('^((http|https|ftp)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]{2,}\.([a-zA-Z0-9\&\.\/\?\:@\-_=#]){2,}$');
  Result := RegEx.Match(url).Success;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function isFileInUse(FileName: TFileName): Boolean;
var
     hf: THandle;
begin
     hf := CreateFile(PChar(FileName),
        GENERIC_READ or GENERIC_WRITE,
        0,
        nil,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0);
      if hf <> INVALID_HANDLE_VALUE then begin
        CloseHandle(hf);
        Result := False;
      end else begin
        Result := (GetLastError() = ERROR_SHARING_VIOLATION);
      end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function toNonAccentVietnamese(content: string): string;
var
  RegEx: TRegEx;
  body: string;
begin
    body := content;
    body := RegEx.Replace(body, '(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)', 'A');
    body := RegEx.Replace(body, '(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)', 'a');
    body := RegEx.Replace(body, '(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)', 'E');
    body := RegEx.Replace(body, '(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)', 'e');
    body := RegEx.Replace(body, '(Ì|Í|Ị|Ỉ|Ĩ)', 'I');
    body := RegEx.Replace(body, '(ì|í|ị|ỉ|ĩ)', 'i');
    body := RegEx.Replace(body, '(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)', 'O');
    body := RegEx.Replace(body, '(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)', 'o');
    body := RegEx.Replace(body, '(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)', 'U');
    body := RegEx.Replace(body, '(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)', 'u');
    body := RegEx.Replace(body, '(Ỳ|Ý|Ỵ|Ỷ|Ỹ)', 'Y');
    body := RegEx.Replace(body, '(ỳ|ý|ỵ|ỷ|ỹ)', 'y');
    body := RegEx.Replace(body, '(Đ)', 'D');
    body := RegEx.Replace(body, '(đ)', 'd');
    body := RegEx.Replace(body, '\W', '-');
    Result := body;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function lookupComboboxEhShowImages(adoQuery: TADOQuery; CbbLookup: TDBLookupComboboxEh;
fieldName: string; iColumnImage: Integer): Boolean;
var
  sStatus: TStringList;
begin
    sStatus := TStringList.Create;
    sStatus.Clear;
    with adoQuery do
    begin
        adoQuery.Open;
        First;
        while not Eof do
        begin
            sStatus.Add(FieldByName(fieldName).AsString);
            Next;
        end;
    end;
    if CbbLookup.DropDownBox.Columns.Count >= iColumnImage then
    begin
        CbbLookup.DropDownBox.Columns.Items[iColumnImage].ImageList := CbbLookup.Images;
        CbbLookup.DropDownBox.Columns.Items[iColumnImage].ShowImageAndText := True;
        CbbLookup.DropDownBox.Columns.Items[iColumnImage].PickList := sStatus;
    end;
     Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  exStatusAudit(pDataSet: TDataSet): String;
var
    s: string;
begin
    with pDataSet do
    begin
        s := '';
        if FindField('LK_CREATE_FULLNAME') <> nil then
        begin
            if FieldByName('CREATE_BY').AsInteger > 0 then
            begin
                s := 'Tạo: ' + FieldByName('LK_CREATE_FULLNAME').AsString;

                if FindField('CREATE_DATE') <> nil then
                    s := s + ' (' + DateTimeToStr(FieldByName('CREATE_DATE').AsDateTime) + ')';
            end;
        end;

        if (FindField('LK_UPDATE_FULLNAME') <> nil) and (FindField('UPDATE_DATE') <> nil) then
        begin
            if (FieldByName('CREATE_BY').AsInteger <> FieldByName('UPDATE_BY').AsInteger) or
                (FieldByName('CREATE_DATE').AsDateTime <> FieldByName('UPDATE_DATE').AsDateTime)
            then
                s := s + ' - Cập nhật: ' + FieldByName('LK_UPDATE_FULLNAME').AsString +
                     ' (' + DateTimeToStr(FieldByName('UPDATE_DATE').AsDateTime) + ')';
        end;
    end;
    Result := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function deleteImageByFileIdx(mFileIdx: TGUID): Boolean;
begin
    if mFileIdx <> TGUID.Empty then
    begin
       with DataMain.QrTEMP do
       begin
            if Active then
                Close;

            SQL.Text := 'select  * from SYS_FILE_CONTENT where IdxKey=:FileIdx';
            Parameters[0].Value := TGuidEx.ToString(mFileIdx);
            Open;

            if not IsEmpty then
                Delete;
       end;
    end;

   Result := True;
end;
 (*==============================================================================
**------------------------------------------------------------------------------
*)
function getTranslateByResource(sResourceName: string): String;
var
    sMessage, sLang: string;
begin
   if mvlLanguage = ENGLISH then
       sLang := 'en'
   else
       sLang := 'vi';

    if sResourceName <> '' then
    begin
       sMessage := sResourceName;
       with DataMain.QrTEMP do
       begin
            if Active then
                Close;

            SQL.Text := 'select  dbo.fnGetResourceByLang(:ResourceName,:Lang) Caption';
            Parameters[0].Value := sResourceName;
            Parameters[1].Value := sLang;
            Open;

            if not IsEmpty then
                sMessage := FieldByName('Caption').AsString;
       end;
    end;

   Result := sMessage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function getIdentityCurrentByTable(sTableName: string): Integer;
var
    index: Integer;
begin
   index := 1;
   with DataMain.QrTEMP do
   begin
        if Active then
            Close;

        SQL.Text := 'select IDENT_CURRENT(:TableName) VALUE';
        Parameters[0].Value := sTableName;
        Open;
        Result := (index + FieldByName('VALUE').AsInteger);
   end;
end;

begin
	cfRecordDate := True;
end.

