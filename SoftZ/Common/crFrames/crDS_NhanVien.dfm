inherited frameHR_DS_NhanVien: TframeHR_DS_NhanVien
  Height = 89
  ExplicitHeight = 89
  object PaMAVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 89
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbMa: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 158
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch nh'#226'n vi'#234'n >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbMaClick
    end
    object EdMa: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 55
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
