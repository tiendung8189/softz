﻿unit crLOC;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook;

type
  TframeCrLOC = class(TCrFrame)
    PaKHO: TPanel;
    Label27: TLabel;
    CbMAKHO: TwwDBLookupCombo;
    CbTENKHO: TwwDBLookupCombo;
    procedure CbMAKHOChange(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameCrLOC: TframeCrLOC;

implementation
uses
    SysUtils, Excommon, isCommon;

{$R *.dfm}

{ TframeCrLOC }

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrLOC.CbMAKHOBeforeDropDown(Sender: TObject);
begin
  inherited;
  if not sysIsCentral then
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrLOC.CbMAKHOChange(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).DisplayValue = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    1:
        CbTENKHO.LookupValue := s;
    2:
        CbMAKHO.LookupValue := s;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrLOC.GetCode: Variant;
begin
    if CbMAKHO.DisplayValue <> '' then
        Result := CbMAKHO.LookupValue
    else
        Result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrLOC.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrLOC.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrLOC.Init;
begin
  inherited;
  with CbMAKHO.LookupTable do
    if not Active then
        Open;
  CbMAKHO.LookupValue := sysLoc;
end;

end.
