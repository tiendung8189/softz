﻿unit crCHIETKHAU;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms, crCommon, StdCtrls, ExtCtrls,
  Mask, wwdbedit, DB, kbmMemTable;

type
  TframeCHIETKHAU = class(TCrFrame)
    Pa1: TPanel;
    Label2: TLabel;
    Ed1: TwwDBEdit;
    Label1: TLabel;
    Ed2: TwwDBEdit;
    kbmTemp: TkbmMemTable;
    kbmTempF1: TFloatField;
    DsTemp: TDataSource;
    kbmTempF2: TFloatField;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameCHIETKHAU: TframeCHIETKHAU;

implementation

uses
	isCommon;

{$R *.dfm}

{ TframeCHIETKHAU }

procedure TframeCHIETKHAU.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := kbmTemp.FieldByName('F1').AsFloat;
    Inc(n);
    cr[n] := kbmTemp.FieldByName('F2').AsFloat;

    kbmTemp.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCHIETKHAU.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCHIETKHAU.Init;
begin
    inherited;
    with kbmTemp do
    begin
        TFloatField(kbmTempF1).DisplayFormat := sysPerFmt;
        TFloatField(kbmTempF2).DisplayFormat := sysCurFmt;

        Open;
        Append;
        FieldByName('F1').AsFloat := 0;
        FieldByName('F2').AsFloat := 0;
    end;
end;

end.
