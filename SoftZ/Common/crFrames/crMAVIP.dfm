inherited frameMAVIP: TframeMAVIP
  Height = 57
  ExplicitHeight = 57
  object PaMADT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 57
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label36: TLabel
      Left = 16
      Top = 4
      Width = 124
      Height = 16
      Caption = 'Kh'#225'ch h'#224'ng th'#226'n thi'#7871't'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdMaDT: TEdit
      Tag = 3
      Left = 16
      Top = 20
      Width = 124
      Height = 24
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnExit = EdMaDTExit
    end
    object EdTenDT: TEdit
      Tag = 4
      Left = 142
      Top = 20
      Width = 240
      Height = 24
      TabStop = False
      Color = 15794175
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
  end
  object QrDMVIP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DM_VIP'
      'where isnull(TINHTRANG, '#39'01'#39') <> '#39'01'#39
      'order by MASO')
    Left = 224
    Top = 8
  end
end
