﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit crTuden;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, crCommon;

type
  TframeTuden = class(TCrFrame)
    PaTUDEN: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    EdDen: TwwDBDateTimePicker;
    EdTu: TwwDBDateTimePicker;
    procedure PaTUDENExit(Sender: TObject);
    procedure EdTuExit(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure Init2(pTu, pDen: TDateTime);
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function Validate(pType: Integer = 0): Boolean; override;

  end;
var
	frameTuden: TframeTuden;

implementation

{$R *.dfm}

uses
    ExCommon, isMSG, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.EdTuExit(Sender: TObject);
begin
    if EdTu.Date < sysBegDate then
    begin
		ErrMsg(Format('"Từ ngày" tối thiểu là "%s"', [DateToStr(sysBegDate)]));
        EdTu.SelectAll;
        EdTu.SetFocus;
    end;
end;

procedure TframeTuden.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr) - GetParamNo;
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdTu.DateTime);
    inc(n);
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdDen.DateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTuden.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.Init;
var
    pTu, pDen: TDateTime;
begin
    EdDen.DisplayFormat :=  ShortDateFormat;
    EdTu.DisplayFormat := ShortDateFormat;
    pDen := Date;
    pTu := pDen - sysLateDay;
    Init2(pTu, pDen);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.Init2(pTu, pDen: TDateTime);
begin
    EdTu.Date := pTu;
    EdDen.DateTime := pDen;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTuden.PaTUDENExit(Sender: TObject);
begin
    if EdDen.DateTime < EdTu.DateTime then
    begin
        ErrMsg(RS_PERIOD);
        EdTu.SelectAll;
        EdTu.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTuden.Validate(pType: Integer): Boolean;
begin
    case pType of
    0:
    begin
        Result := SameMonth(EdTu.Date, EdDen.Date);
        if not Result then
        begin
            ErrMsg(RS_THESAME_MONTH);
            EdTu.SelectAll;
            EdTu.SetFocus;
        end;
    end;
    else
        Result := True;
    end;
end;

end.

