﻿unit crLOAI_HD;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, wwdblook, ExtCtrls;

type
  TframeLOAI_HD = class(TCrFrame)
    PaLHD: TPanel;
    Label38: TLabel;
    CbLHD: TwwDBLookupCombo;
    CbTENLHD: TwwDBLookupCombo;
    procedure CbLHDChange(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameLOAI_HD: TframeLOAI_HD;

implementation
uses
    ExCommon;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeLOAI_HD.CbLHDChange(Sender: TObject);
var
	s : String;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

	if (Sender as TwwDbLookupCombo).DisplayValue = '' then
    	s := ''
    else
		s := (Sender as TwwDbLookupCombo).LookupValue;

	case (Sender as TComponent).Tag of
    7:
        CbTENLHD.LookupValue := s;
    8:
        CbLHD.LookupValue := s;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeLOAI_HD.GetCode: Variant;
begin
    if CbLHD.DisplayValue <> '' then
        Result := CbLHD.LookupValue
    else
        Result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TframeLOAI_HD.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeLOAI_HD.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeLOAI_HD.Init;
begin
    inherited;
    with CbLHD.LookupTable do
    if not Active then
        Open;
end;

end.
