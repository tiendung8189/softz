﻿unit crNAM;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeNAM = class(TCrFrame)
    PaNAM: TPanel;
    Label9: TLabel;
    CbNam: TComboBox;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;

  end;

var
  frameNAM: TframeNAM;

implementation

{$R *.dfm}
uses
    isStr, ExCommon;

{ TframeNAM }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeNAM.GetCode: Variant;
begin
    Result := StrToIntDef(CbNam.Text, 0)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeNAM.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeNAM.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeNAM.Init;
begin
    inherited;
    isYearList(CbNam, sysYear);
end;

end.
