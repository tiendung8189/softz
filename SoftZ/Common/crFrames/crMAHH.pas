﻿unit crMAHH;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TFrameMAHH = class(TCrFrame)
    PaMaHH: TPanel;
    lbMaHH: TLabel;
    EdMaHH: TMemo;
    procedure lbMaHHClick(Sender: TObject);

  private
  protected
    procedure Init; override;

  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  FrameMAHH: TFrameMAHH;

implementation

{$R *.dfm}
uses
    ChonDsHH;
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo ngành hàng';
    RS_TIT1 = 'Theo nhóm hàng';
    RS_TIT_CAP = '<< %s >>';
    RS_TIT22 = 'Theo mã hàng hóa chung';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameMAHH.lbMaHHClick(Sender: TObject);
const
	TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT22
    );
var
    n: Integer;
    s: String;
begin
	n := EdMaHH.Tag;
    if not FrmChonDsHH.Get(n, s) then
    	Exit;

    LbMaHH.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMaHH do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameMAHH.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := EdMaHH.Text;

    n := Length(cr) - GetParamNo;
    cr[n] := EdMaHH.Tag;
    inc(n);
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'MAHH' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrameMAHH.GetParamNo: Integer;
begin
    Result:= 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameMAHH.Init;
begin
  inherited;

end;

end.
