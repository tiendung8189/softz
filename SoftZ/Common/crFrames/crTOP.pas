﻿unit crTOP;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, Mask, wwdbedit;

type
  TframeTOP = class(TCrFrame)
    PaTOP: TPanel;
    Label2: TLabel;
    EdTOP: TwwDBEdit;
    RbTOP1: TRadioGroup;
    RbTOP2: TRadioGroup;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameTOP: TframeTOP;

implementation

{$R *.dfm}

{ TframeTOP }

procedure TframeTOP.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := StrToIntDef(EdTOP.Text, 1);
    Inc(n);
    cr[n] := RbTOP1.ItemIndex;
    Inc(n);
    cr[n] := RbTOP2.ItemIndex;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTOP.GetParamNo: Integer;
begin
    Result := 3;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTOP.Init;
begin
    inherited;
    EdTOP.Text := '10';
end;

end.
