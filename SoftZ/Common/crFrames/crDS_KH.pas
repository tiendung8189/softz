﻿unit crDS_KH;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_KH = class(TCrFrame)
    PaDS_KH: TPanel;
    Label4: TLabel;
    EdDS_KH: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected

  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_KH: TframeDS_KH;

implementation
uses
    ChonDsKH;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KH.GetCode: Variant;
begin
    Result := Trim(EdDS_KH.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := GetCode;

    n := Length(cr) - GetParamNo;
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'DS_DT' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KH.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KH.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsKH.Get(s) then
    	Exit;

    with EdDS_KH do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
