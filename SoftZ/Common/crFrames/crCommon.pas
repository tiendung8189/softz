﻿unit crCommon;

interface

uses
  Variants, Classes, Controls, Forms;

type
  TCrFrame = class(TFrame)
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  protected
    procedure Init; virtual; //abstract;
    procedure crFinal; virtual;
  public
    function  GetParamNo: Integer; virtual;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); virtual;
    function  GetCode: Variant; virtual;
    function  Action(Sender: TObject): Boolean; virtual;
    function  Validate(pType: Integer = 0): Boolean; virtual;

  end;

implementation

{$R *.dfm}

{ TCrFrame }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCrFrame.Action(Sender: TObject): Boolean;
begin
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TCrFrame.Create(AOwner: TComponent);
begin
    inherited;
    Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TCrFrame.Destroy;
begin
    crFinal;
    inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TCrFrame.crFinal;
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCrFrame.GetCode: Variant;
begin
    Result := Null;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TCrFrame.GetCriteria;
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCrFrame.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TCrFrame.Init;
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCrFrame.Validate(pType: Integer): Boolean;
begin
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

end.
