﻿unit crHonNhan;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook, DBGridEh, Vcl.Mask, DBCtrlsEh,
  DBLookupEh, DbLookupComboboxEh2;

type
  TframeHR_HonNhan = class(TCrFrame)
    PaBoard: TPanel;
    CbHonNhan: TDbLookupComboboxEh2;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameHR_HonNhan: TframeHR_HonNhan;

implementation
uses
    SysUtils, Excommon, isCommon, isDb, MainData, HrData;

{$R *.dfm}

{ TframeHR_HonNhan }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_HonNhan.GetCode: Variant;
begin
    if (CbHonNhan.Text = '') or (VarIsNull(CbHonNhan.Value)) or (VarIsEmpty(CbHonNhan.Value)) then
        Result := ''
    else
        Result := CbHonNhan.Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_HonNhan.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_HonNhan.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_HonNhan.Init;
begin
  inherited;
  OpenDataSets([HrDataMain.QrV_HONNHAN]);
end;

end.
