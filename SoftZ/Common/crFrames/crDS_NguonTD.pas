﻿unit crDS_NguonTD;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TFrameHR_DS_NguonTD = class(TCrFrame)
    PaDS_NHOMVT: TPanel;
    LbText: TLabel;
    EdDSText: TMemo;
    procedure LbTextClick(Sender: TObject);

  private
  protected
    procedure Init; override;

  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  FrameHR_DS_NguonTD: TFrameHR_DS_NguonTD;

implementation

{$R *.dfm}
    uses
    ChonDsNguonTD;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrameHR_DS_NguonTD.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameHR_DS_NguonTD.LbTextClick(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsNguonTD.Get(s) then
    	Exit;

    with EdDSText do
    begin
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
 procedure TFrameHR_DS_NguonTD.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := Trim(EdDSText.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameHR_DS_NguonTD.Init;
begin
  inherited;

end;

end.
