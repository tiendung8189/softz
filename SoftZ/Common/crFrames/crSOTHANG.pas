﻿unit crSOTHANG;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, Mask, wwdbedit;

type
  TframeSOTHANG = class(TCrFrame)
    Pa1: TPanel;
    Label2: TLabel;
    EdTOP: TwwDBEdit;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameSOTHANG: TframeSOTHANG;

implementation

{$R *.dfm}

{ TframeSOTHANG }

procedure TframeSOTHANG.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := StrToIntDef(EdTOP.Text, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSOTHANG.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSOTHANG.Init;
begin
    inherited;
    EdTOP.Text := '0';
end;

end.
