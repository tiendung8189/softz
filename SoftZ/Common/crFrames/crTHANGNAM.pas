﻿unit crTHANGNAM;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeTHANGNAM = class(TCrFrame)
    PaTHANGNAM: TPanel;
    Label34: TLabel;
    Label35: TLabel;
    CbThang: TComboBox;
    CbNam: TComboBox;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;

  end;

var
  frameTHANGNAM: TframeTHANGNAM;

implementation

{$R *.dfm}
uses
    isStr, ExCommon;

{ TframeTHANGNAM }

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTHANGNAM.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := StrToIntDef(CbThang.Text, 0);
    inc(n);
    cr[n] := StrToIntDef(CbNam.Text, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTHANGNAM.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTHANGNAM.Init;
begin
    inherited;
    // Init month, year combobox
    isMonthList(CbTHANG, sysMon);
    isYearList(CbNAM, sysYear);
end;

end.
