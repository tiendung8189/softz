﻿unit crDS_USER;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_USER = class(TCrFrame)
    PaDS_KHO: TPanel;
    Label7: TLabel;
    EdDS: TMemo;
    procedure Label7Click(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_USER: TframeDS_USER;

implementation

{$R *.dfm}

uses
    ChonDsUser;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_USER.GetCode: Variant;
begin
    Result := EdDS.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_USER.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_USER.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_USER.Init;
begin
  inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_USER.Label7Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsUser.Get(s) then
    	Exit;

    with EdDS do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
