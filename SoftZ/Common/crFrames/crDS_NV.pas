﻿unit crDS_NV;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_NV = class(TCrFrame)
    PaDS_KH: TPanel;
    Label4: TLabel;
    EdDS: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected

  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_NV: TframeDS_NV;

implementation
uses
    ChonDsNV;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NV.GetCode: Variant;
begin
    Result := Trim(EdDS.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NV.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := GetCode;

    n := Length(cr) - GetParamNo;
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'DS_NV' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NV.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NV.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NV.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsNV.Get(s) then
    	Exit;

    with EdDS do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
