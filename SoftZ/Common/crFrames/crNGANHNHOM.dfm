inherited frameNGANHNHOM: TframeNGANHNHOM
  Height = 81
  ExplicitHeight = 81
  object PaNHOM: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 81
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 4
      Width = 76
      Height = 16
      Caption = 'Ng'#224'nh, nh'#243'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbNGANH: TwwDBLookupCombo
      Tag = 10
      Left = 68
      Top = 20
      Width = 313
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENNGANH'#9'26'#9'TENNGANH'#9'F'
        'MANGANH'#9'4'#9'MANGANH'#9'F')
      LookupTable = QrNGANH
      LookupField = 'MANGANH'
      Options = [loColLines]
      Style = csDropDownList
      Color = clWhite
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbNHOM: TwwDBLookupCombo
      Tag = 12
      Left = 68
      Top = 44
      Width = 313
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENNHOM'#9'26'#9'TENNHOM'#9'F'
        'MANHOM'#9'4'#9'MANHOM'#9'F')
      LookupTable = QrNHOM
      LookupField = 'MANHOM'
      Options = [loColLines]
      Style = csDropDownList
      Color = clWhite
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbMANGANH: TwwDBLookupCombo
      Tag = 9
      Left = 16
      Top = 20
      Width = 49
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MANGANH'#9'4'#9'MANGANH'#9'F'
        'TENNGANH'#9'32'#9'TENNGANH'#9'F')
      LookupTable = QrNGANH
      LookupField = 'MANGANH'
      Options = [loColLines]
      Color = clWhite
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbMANHOM: TwwDBLookupCombo
      Tag = 11
      Left = 16
      Top = 44
      Width = 49
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'4'#9'MA'#9'F'
        'TENNHOM'#9'32'#9'TENNHOM'#9'F')
      LookupTable = QrNHOM
      LookupField = 'MANHOM'
      Options = [loColLines]
      Color = clWhite
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
  end
  object QrNHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsNGANH
    Parameters = <
      item
        Name = 'MANGANH'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'MA, MANHOM, TENNHOM'
      '  from '#9'DM_NHOM'
      ' where '#9'MANGANH =:MANGANH'
      'order by MANHOM')
    Left = 212
    Top = 7
  end
  object QrNGANH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MANGANH, TENNGANH'
      '  from DM_NGANH'
      'order by MANGANH')
    Left = 180
    Top = 6
  end
  object DsNGANH: TDataSource
    DataSet = QrNGANH
    Left = 180
    Top = 49
  end
end
