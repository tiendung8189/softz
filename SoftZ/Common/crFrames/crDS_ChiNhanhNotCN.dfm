inherited FrameHR_DS_ChiNhanhNotCN: TFrameHR_DS_ChiNhanhNotCN
  Height = 0
  ExplicitHeight = 0
  object PaDS_NHOMVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 0
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    ExplicitHeight = 55
    object LbText: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 157
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch chi nh'#225'nh >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbTextClick
    end
    object EdDSText: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
