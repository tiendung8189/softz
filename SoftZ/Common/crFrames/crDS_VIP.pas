﻿unit crDS_VIP;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_VIP = class(TCrFrame)
    PaDS_VIP: TPanel;
    Label4: TLabel;
    EdDS_VIP: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected

  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_VIP: TframeDS_VIP;

implementation
uses
    ChonDsVIP;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_VIP.GetCode: Variant;
begin
    Result := Trim(EdDS_VIP.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_VIP.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := GetCode;

    n := Length(cr) - GetParamNo;
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'DS_VIP' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_VIP.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_VIP.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_VIP.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsVIP.Get(s) then
    	Exit;

    with EdDS_VIP do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
