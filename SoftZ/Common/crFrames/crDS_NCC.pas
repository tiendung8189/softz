﻿unit crDS_NCC;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_NCC = class(TCrFrame)
    PaDS_NCC: TPanel;
    Label4: TLabel;
    EdDS_MA: TMemo;
    procedure Label4Click(Sender: TObject);
  private
  protected
  public
    procedure Init; override;
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_NCC: TframeDS_NCC;

implementation
uses
    ChonDsNCC;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NCC.GetCode: Variant;
begin
    Result := Trim(EdDS_MA.Text)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := GetCode;

    n := Length(cr) - GetParamNo;
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'DS_DT' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_NCC.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.Init;
begin
    inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_NCC.Label4Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsNCC.Get(s) then
    	Exit;

    with EdDS_MA do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
