﻿unit crNGANHNHOM;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, wwdblook, ExtCtrls, DB, ADODB;

type
  TframeNGANHNHOM = class(TCrFrame)
    PaNHOM: TPanel;
    Label3: TLabel;
    CbNGANH: TwwDBLookupCombo;
    CbNHOM: TwwDBLookupCombo;
    CbMANGANH: TwwDBLookupCombo;
    CbMANHOM: TwwDBLookupCombo;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    DsNGANH: TDataSource;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;

  end;

var
  frameNGANHNHOM: TframeNGANHNHOM;

implementation

{$R *.dfm}

procedure TframeNGANHNHOM.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := CbNGANH.LookupValue;
    inc(n);
    cr[n] := CbNHOM.LookupValue;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeNGANHNHOM.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeNGANHNHOM.Init;
begin
    inherited;
    with QrNGANH do
    if not Active then
        Open;
    with QrNHOM do
    if not Active then
        Open;
end;

end.
