﻿unit crFB_DS_LYDO_NK;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeFB_DS_LYDO_NK = class(TCrFrame)
    PaDS_KHO: TPanel;
    Label7: TLabel;
    EdDS: TMemo;
    procedure Label7Click(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameFB_DS_LYDO_NK: TframeFB_DS_LYDO_NK;

implementation

{$R *.dfm}

uses
    FB_ChonDsLydoNK;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeFB_DS_LYDO_NK.GetCode: Variant;
begin
    Result := EdDS.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_DS_LYDO_NK.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeFB_DS_LYDO_NK.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_DS_LYDO_NK.Init;
begin
  inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_DS_LYDO_NK.Label7Click(Sender: TObject);
var
    s: String;
begin
    if not FrmFB_ChonDsLydoNK.Get(s) then
    	Exit;

    with EdDS do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
