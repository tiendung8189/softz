inherited frameHR_DS_HopDong: TframeHR_DS_HopDong
  Height = 74
  ExplicitHeight = 74
  object PaMAVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbMa: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 178
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch lo'#7841'i h'#7907'p '#273#7891'ng >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbMaClick
    end
    object EdMa: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 40
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
