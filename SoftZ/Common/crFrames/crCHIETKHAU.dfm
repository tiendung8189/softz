inherited frameCHIETKHAU: TframeCHIETKHAU
  Height = 41
  ExplicitHeight = 41
  object Pa1: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 41
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 12
      Width = 67
      Height = 16
      Caption = '% '#273'ang C.K'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 189
      Top = 12
      Width = 91
      Height = 16
      Caption = #272#417'n gi'#225' sau C.K'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Ed1: TwwDBEdit
      Left = 98
      Top = 7
      Width = 61
      Height = 24
      DataField = 'F1'
      DataSource = DsTemp
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object Ed2: TwwDBEdit
      Left = 287
      Top = 7
      Width = 91
      Height = 24
      DataField = 'F2'
      DataSource = DsTemp
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object kbmTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'F1'
        DataType = ftFloat
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 0
    LocaleID = 0
    Left = 325
    Top = 8
    object kbmTempF1: TFloatField
      FieldName = 'F1'
    end
    object kbmTempF2: TFloatField
      FieldName = 'F2'
    end
  end
  object DsTemp: TDataSource
    DataSet = kbmTemp
    Left = 324
    Top = 44
  end
end
