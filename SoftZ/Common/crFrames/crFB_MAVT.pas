﻿unit crFB_MAVT;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeFB_MAVT = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameFB_MAVT: TframeFB_MAVT;

implementation

{$R *.dfm}

uses
    FB_ChonDsma_MAVT;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo ngành hàng F&&B';
    RS_TIT1 = 'Theo nhóm hàng F&&B';
    RS_TIT2 = 'Theo mã hàng';
    RS_TIT_CAP = '<< %s >>';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT.LbMaClick(Sender: TObject);
const
    TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT2
    );
var
    n: Integer;
    s: String;
begin
	n := EdMa.Tag;
    if not FrmFB_ChonDsma_MAVT.Get(n, s) then
    	Exit;

    LbMa.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMa do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := EdMa.Text;

    n := Length(cr) - GetParamNo;
    cr[n] := EdMa.Tag;
    inc(n);
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'FB_DS_MAVT' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeFB_MAVT.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeFB_MAVT.Init;
begin
  inherited;

end;

end.
