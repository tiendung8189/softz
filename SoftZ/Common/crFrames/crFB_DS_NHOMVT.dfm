inherited FrameFB_DS_NHOMVT: TFrameFB_DS_NHOMVT
  Height = 55
  ExplicitHeight = 55
  object PaDS_NHOMVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 55
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbNganhNhom: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 155
      Height = 13
      Cursor = 1
      Caption = '<< Ng'#224'nh h'#224'ng b'#225'o c'#225'o >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbNganhNhomClick
    end
    object EdDSNHOM: TMemo
      Left = 16
      Top = 20
      Width = 365
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
