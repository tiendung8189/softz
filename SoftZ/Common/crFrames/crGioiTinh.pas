﻿unit crGioiTinh;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook, DBGridEh, Vcl.Mask, DBCtrlsEh,
  DBLookupEh, DbLookupComboboxEh2;

type
  TframeHR_GioiTinh = class(TCrFrame)
    PaBoard: TPanel;
    CbGioiTinh: TDbLookupComboboxEh2;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameHR_GioiTinh: TframeHR_GioiTinh;

implementation
uses
    SysUtils, Excommon, isCommon, isDb, MainData, HrData;

{$R *.dfm}

{ TframeHR_GioiTinh }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_GioiTinh.GetCode: Variant;
begin
    if (CbGioiTinh.Text = '') or (VarIsNull(CbGioiTinh.Value)) or (VarIsEmpty(CbGioiTinh.Value)) then
        Result := '0'
    else
        Result := CbGioiTinh.Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_GioiTinh.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_GioiTinh.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_GioiTinh.Init;
begin
  inherited;
  OpenDataSets([HrDataMain.QrLOAI_GIOITINH]);
end;

end.
