inherited frameHR_DS_ChucDanh: TframeHR_DS_ChucDanh
  Height = 74
  ExplicitHeight = 74
  object PaMAVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbMa: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 211
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch ch'#7913'c danh, ch'#7913'c v'#7909' >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbMaClick
    end
    object EdMa: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 40
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
