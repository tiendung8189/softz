object DataOffice: TDataOffice
  OldCreateOrder = False
  Height = 344
  Width = 449
  object WordReport: TARWordReport
    TableCaptionsBold = True
    SavePrompt = False
    Marks.Prefix = '<#'
    Marks.Postfix = '>'
    Steps = [rs10_CustTagValues, rs20_DatasetList, rs30_FillTables, rs40_DataPool, rs50_Conditions, rs60_Numerators, rs70_OnTag]
    OnTag = WordReportTag
    OnNeedSQL = WordReportNeedSQL
    DataSetList = <
      item
        DSName = 'DS'
        Dataset = SP_EXPORT
      end>
    AutoTables = True
    Aggregation = True
    Version = 'TARWordReport 2.1'
    TextTables = True
    NeedHeaderFooterProc = True
    RunMacro = True
    Left = 192
    Top = 48
  end
  object SP_EXPORT: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    Left = 120
    Top = 48
  end
  object QrDataPool: TADOQuery
    Connection = DataMain.Conn
    Parameters = <>
    Left = 112
    Top = 216
  end
  object XLSReport: TEXLReport
    About = 'EMS Advanced Excel Report(tm) Component Suite for Delphi(R)'
    Bands.BandCount = 4
    Bands.GroupHeaderBandCount = 1
    Bands.Items = <
      item
        Bands.Range = 'A1:D3'
        Bands.BandType = 0
      end
      item
        Bands.Range = 'A5:D5'
        Bands.BandType = 2
      end
      item
        Bands.Range = 'A6:D9'
        Bands.BandType = 7
      end
      item
        Bands.Range = 'A4:D4'
        Bands.FieldName = 'Department'
        Bands.BandType = 1
        Bands.GroupID = 1
      end>
    DataSet = SP_EXPORT
    Dictionary = <>
    Options = [xlroTotalsAsFormula, xlroVisibleWhenError, xlroHourCursor]
    _Version = '1.9.3.1'
    Left = 192
    Top = 104
  end
  object USER_REP_PRINT: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    ProcedureName = 'USER_REP_PRINT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EN'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FORMCODE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    Left = 120
    Top = 104
  end
  object USER_REP_PRINT2: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    ProcedureName = 'USER_REP_PRINT2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EN'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FORMCODE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@REP_CODE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@KEYVALUE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    Left = 120
    Top = 152
  end
end
