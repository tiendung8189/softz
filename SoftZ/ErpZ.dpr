program ErpZ;



uses
  Forms,
  Main in 'SoftZ\Main.pas' {FrmMain},
  TonkhoDK in 'SoftZ\TonkhoDK.pas' {FrmTonkhoDK},
  Params in 'SoftZ\Params.pas' {FrmParams},
  Dmkhac3 in 'SoftZ\Dmkhac3.pas' {FrmDmkhac3},
  Dmvt in 'SoftZ\Dmvt.pas' {FrmDmvt},
  Banle in 'SoftZ\Banle.pas' {FrmBanle},
  Soluong in 'SoftZ\Soluong.pas' {FrmSoluong},
  Xuat in 'SoftZ\Xuat.pas' {FrmXuat},
  DondhNCC in 'SoftZ\DondhNCC.pas' {FrmDondhNCC},
  Dmkhac in 'SoftZ\Dmkhac.pas' {FrmDmkhac},
  Khoaso in 'SoftZ\Khoaso.pas' {FrmKhoaso},
  ChonNgay in 'SoftZ\ChonNgay.pas' {FrmChonNgay},
  Kiemke in 'SoftZ\Kiemke.pas' {FrmKiemke},
  ChuyenKho in 'SoftZ\ChuyenKho.pas' {FrmChuyenKho},
  Dmkho in 'SoftZ\Dmkho.pas' {FrmDmkho},
  Inlabel in 'SoftZ\Inlabel.pas' {FrmInlabel},
  ChonMavt in 'SoftZ\ChonMavt.pas' {FrmChonMavt},
  Scan in 'SoftZ\Scan.pas' {FrmScan},
  Tuden in 'SoftZ\Tuden.pas' {FrmTuden},
  Dmkh in 'SoftZ\Dmkh.pas' {FrmDmkh},
  Dmncc in 'SoftZ\Dmncc.pas' {FrmDmncc},
  CayNganhNhom in 'SoftZ\CayNganhNhom.pas' {FrmCayNganhNhom},
  BkgrOption in 'SoftZ\BkgrOption.pas' {FrmBkgrOption},
  NXKhac in 'SoftZ\NXKhac.pas' {FrmNXKhac},
  Mamoi in 'SoftZ\Mamoi.pas' {FrmMamoi},
  DmVIP in 'SoftZ\DmVIP.pas' {FrmDmVIP},
  ExVIP in 'SoftZ\ExVIP.pas' {FrmExVIP},
  Tygia in 'SoftZ\Tygia.pas' {FrmTygia},
  Dmsize in 'SoftZ\Dmsize.pas' {FrmDmsize},
  ChonPhieunhap in 'SoftZ\ChonPhieunhap.pas' {FrmChonPhieunhap},
  Dmquaytn in 'SoftZ\Dmquaytn.pas' {FrmDmquaytn},
  ChonKh in 'SoftZ\ChonKh.pas' {FrmChonKh},
  ChonDondh in 'SoftZ\ChonDondh.pas' {FrmChonDondh},
  frameNgay in 'SoftZ\frameNgay.pas' {frNGAY: TFrame},
  DondhKH in 'SoftZ\DondhKH.pas' {FrmDondhKH},
  Lienhe in 'SoftZ\Lienhe.pas' {FrmLienhe: TForm},
  DmhhNhom in 'SoftZ\DmhhNhom.pas' {FrmDmhhNhom: TForm},
  Thu in 'SoftZ\Thu.pas' {FrmThu: TForm},
  Chi in 'SoftZ\Chi.pas' {FrmChi: TForm},
  DieuchinhXuat in 'SoftZ\DieuchinhXuat.pas' {FrmDieuchinhXuat: TForm},
  DmvtBo in 'SoftZ\DmvtBo.pas' {FrmDmvtBo: TForm},
  HoadonGTGT in 'SoftZ\HoadonGTGT.pas' {FrmHoadonGTGT: TForm},
  HoadonGTGTGetBill in 'SoftZ\HoadonGTGTGetBill.pas' {FrmHoadonGTGTGetBill: TForm},
  Dmmau in 'SoftZ\Dmmau.pas' {FrmDmmau: TForm},
  DieuchinhNhap in 'SoftZ\DieuchinhNhap.pas' {FrmDieuchinhNhap: TForm},
  NhapTra in 'SoftZ\NhapTra.pas' {FrmNhapTra: TForm},
  ChonPhieuXuat in 'SoftZ\ChonPhieuXuat.pas' {FrmChonPhieuXuat: TForm},
  XuatTra in 'SoftZ\XuatTra.pas' {FrmXuatTra: TForm},
  Ketchuyentien in 'SoftZ\Ketchuyentien.pas' {FrmKetchuyentien: TForm},
  TimBarcode in 'SoftZ\TimBarcode.pas' {FrmTimBarcode: TForm},
  ThuThungan in 'SoftZ\ThuThungan.pas' {FrmThuThungan: TForm},
  Chikhac in 'SoftZ\Chikhac.pas' {FrmChikhac: TForm},
  ChiThungan in 'SoftZ\ChiThungan.pas' {FrmChiThungan: TForm},
  Thukhac in 'SoftZ\Thukhac.pas' {FrmThukhac: TForm},
  ChonPhieuKM in 'SoftZ\ChonPhieuKM.pas' {FrmChonPhieuKM: TForm},
  Khuyenmai in 'SoftZ\Khuyenmai.pas' {FrmKhuyenmai: TForm},
  Khuyenmai2 in 'SoftZ\Khuyenmai2.pas' {FrmKhuyenmai2: TForm},
  frameKho in 'SoftZ\frameKho.pas' {frKHO: TFrame},
  Tonkho in 'SoftZ\Tonkho.pas' {FrmTonkho},
  FlexRep in 'SoftZ\FlexRep.pas' {FrmFlexRep},
  FlexRep3 in 'SoftZ\FlexRep3.pas' {FrmFlexRep3},
  DmKhuyenmai in 'SoftZ\DmKhuyenmai.pas' {FrmDmKhuyenmai},
  Khuyenmai3 in 'SoftZ\Khuyenmai3.pas' {FrmKhuyenmai3},
  frameScanCode in 'SoftZ\frameScanCode.pas' {frScanCode: TFrame},
  Nhaptrabl2 in 'SoftZ\Nhaptrabl2.pas' {FrmNhaptrabl2},
  frameScanCode2 in 'SoftZ\frameScanCode2.pas' {frScanCode2: TFrame},
  ChiHT in 'SoftZ\ChiHT.pas' {FrmChiHT},
  ThuHT in 'SoftZ\ThuHT.pas' {FrmThuHT},
  DmLoaiVipCT in 'SoftZ\DmLoaiVipCT.pas' {FrmDmLoaiVipCT},
  ChonDondh3 in 'SoftZ\ChonDondh3.pas' {FrmChonDondh3},
  TheodoiGia in 'SoftZ\TheodoiGia.pas' {FrmTheodoiGia},
  DmTK in 'SoftZ\DmTK.pas' {FrmDmTK},
  Thu2 in 'SoftZ\Thu2.pas' {FrmThu2},
  Chi2 in 'SoftZ\Chi2.pas' {FrmChi2},
  ChonPhieuNX in 'SoftZ\ChonPhieuNX.pas' {FrmChonPhieuNX},
  ChonDsPN in 'SoftZ\ChonDsPN.pas' {FrmChonDsPN},
  ChonDsPX in 'SoftZ\ChonDsPX.pas' {FrmChonDsPX},
  CardReader in 'SoftZ\CardReader.pas' {FrmCardReader},
  TheLenh in 'SoftZ\TheLenh.pas' {FrmThelenh},
  HoadonGTGTCT in 'SoftZ\HoadonGTGTCT.pas' {FrmHoadonGTGTCT},
  TimBarcode2 in 'SoftZ\TimBarcode2.pas' {FrmTimBarcode2},
  DmKho2 in 'SoftZ\DmKho2.pas' {FrmDmKho2},
  AddList in '..\Softz.Admin\AddList.pas' {FrmAddList},
  ExAdminCommon in '..\Softz.Admin\ExAdminCommon.pas',
  ListUser in '..\Softz.Admin\ListUser.pas' {FrmListUser},
  Users in '..\Softz.Admin\Users.pas' {FrmUser},
  ListGroup in '..\Softz.Admin\ListGroup.pas' {FrmListGroup},
  Groups in '..\Softz.Admin\Groups.pas' {FrmGroup},
  FunctionAccess in '..\Softz.Admin\FunctionAccess.pas' {FrmFuncAccess},
  ReportAccess in '..\Softz.Admin\ReportAccess.pas' {FrmRepAccess},
  DmKhNcc in 'SoftZ\DmKhNcc.pas' {FrmDmKhNcc},
  DmLoaiVip in 'SoftZ\DmLoaiVip.pas' {FrmDmLoaiVip},
  DmvtCT in 'SoftZ\DmvtCT.pas' {FrmDmvtCT},
  DmvtPB in 'SoftZ\DmvtPB.pas' {FrmDmvtPB},
  ChonDsNhomCk in 'SoftZ\ChonDsNhomCk.pas' {FrmChonDsNhomCk},
  ChonDsNhomNCC in 'SoftZ\ChonDsNhomNCC.pas' {FrmChonDsNhomNCC},
  Nhap in 'SoftZ\Nhap.pas' {FrmNhap},
  ChonDondh2 in 'SoftZ\ChonDondh2.pas' {FrmChonDondh2},
  Tienthue in 'SoftZ\Tienthue.pas' {FrmTienthue},
  frameLoc in 'SoftZ\frameLoc.pas' {frLoc: TFrame},
  CongnoKH in 'SoftZ\CongnoKH.pas' {FrmCongnoKH},
  CongnoNCC in 'SoftZ\CongnoNCC.pas' {FrmCongnoNCC},
  ChonDsHHKM in 'SoftZ\ChonDsHHKM.pas' {FrmChonDsHHKM},
  DmNganhang in 'SoftZ\DmNganhang.pas' {FrmDmNganhang},
  Params2 in 'SoftZ\Params2.pas' {FrmParams2},
  DmvtThuoctinh in 'SoftZ\DmvtThuoctinh.pas' {FrmDmvtThuoctinh},
  DmvtTTSach in 'SoftZ\DmvtTTSach.pas' {FrmDmvtTTSach},
  DmHotro in 'SoftZ\DmHotro.pas' {FrmDmHotro},
  InlabelChungtu in 'SoftZ\InlabelChungtu.pas' {FrmInlabelChungtu},
  PhieuQuatang in 'SoftZ\PhieuQuatang.pas' {FrmPhieuQuatang},
  CheckTonkho in 'SoftZ\CheckTonkho.pas' {FrmCheckTonkho},
  TudenTon in 'SoftZ\TudenTon.pas' {FrmTudenTon},
  frameD2D in 'HrZ\frameD2D.pas' {frD2D: TFrame},
  frameEmp in 'HrZ\frameEmp.pas' {frEmp: TFrame},
  frameListField in 'HrZ\frameListField.pas' {frListField: TFrame},
  frameMonthYear in 'HrZ\frameMonthYear.pas' {frMonthYear: TFrame},
  frameMY in 'HrZ\frameMY.pas' {frMY: TFrame},
  frameYear in 'HrZ\frameYear.pas' {frYear: TFrame},
  Bangcong in 'HrZ\Bangcong.pas' {FrmBangcong},
  BangThongso in 'HrZ\BangThongso.pas' {FrmBangThongso},
  BosungCong in 'HrZ\BosungCong.pas' {FrmBosungCong},
  BosungLuong in 'HrZ\BosungLuong.pas' {FrmBosungLuong},
  DangkyThaisan in 'HrZ\DangkyThaisan.pas' {FrmDangkyThaisan},
  DevFP in 'HrZ\DevFP.pas' {FrmDevFP},
  DevFP2 in 'HrZ\DevFP2.pas' {FrmDevFP2},
  DevList in 'HrZ\DevList.pas' {FrmDevList},
  DevLog in 'HrZ\DevLog.pas' {FrmDevLog},
  DevOption in 'HrZ\DevOption.pas' {FrmDevOption},
  DevRAW in 'HrZ\DevRAW.pas' {FrmDevRAW},
  devResStr in 'HrZ\devResStr.pas',
  DmCalamviec in 'HrZ\DmCalamviec.pas' {FrmDmCalamviec},
  DmChucvu in 'HrZ\DmChucvu.pas' {FrmDmChucvu},
  DmHopdong in 'HrZ\DmHopdong.pas' {FrmDmHopdong},
  DmNgayle in 'HrZ\DmNgayle.pas' {FrmDmNgayle},
  DmNhomlv in 'HrZ\DmNhomlv.pas' {FrmDmNhomlv},
  DmNhomlvDetail in 'HrZ\DmNhomlvDetail.pas' {FrmNhomlvDetail},
  DmNoichuabenh in 'HrZ\DmNoichuabenh.pas' {FrmDmNoichuabenh},
  Dmnv in 'HrZ\Dmnv.pas' {FrmDmnv},
  DmnvThoiviec in 'HrZ\DmnvThoiviec.pas' {FrmDmnvThoiviec},
  DmPhongban in 'HrZ\DmPhongban.pas' {FrmDmPhongban},
  DmPhongbanNotCN in 'HrZ\DmPhongbanNotCN.pas' {FrmDmPhongbanNotCN},
  DmPhucap in 'HrZ\DmPhucap.pas' {FrmDmPhucap},
  DmQTLV in 'HrZ\DmQTLV.pas' {FrmDmQTLV},
  DmTK_NV in 'HrZ\DmTK_NV.pas' {FrmDmTK_NV},
  DmVangmat in 'HrZ\DmVangmat.pas' {FrmDmVangmat},
  HopdongLaodong in 'HrZ\HopdongLaodong.pas' {FrmHopdongLaodong},
  LogReader in 'HrZ\LogReader.pas',
  Phepnam in 'HrZ\Phepnam.pas' {FrmPhepnam},
  PhepnamThongbao in 'HrZ\PhepnamThongbao.pas' {FrmPhepnamThongbao},
  Vantay in 'HrZ\Vantay.pas' {FrmVantay},
  zkem in 'HrZ\zkem.pas',
  SelectFields in 'HrZ\SelectFields.pas' {FrmSelectFields},
  Bangluong in 'HrZ\Bangluong.pas' {FrmBangluong},
  WaitProgress in 'HrZ\WaitProgress.pas' {FrmWaitProgress},
  POM_BangThongso in 'PomZ\POM_BangThongso.pas' {FrmPOM_BangThongso},
  POM_BangSanluong in 'PomZ\POM_BangSanluong.pas' {FrmPOM_BangSanluong},
  POM_BangChiphi in 'PomZ\POM_BangChiphi.pas' {FrmPOM_BangChiphi},
  POM_BangDinhmucBTP in 'PomZ\POM_BangDinhmucBTP.pas' {FrmPOM_BangDinhmucBTP},
  POM_BangDinhmucTP in 'PomZ\POM_BangDinhmucTP.pas' {FrmPOM_BangDinhmucTP},
  UnLockPhieu in 'SoftZ\UnLockPhieu.pas' {FrmUnLockPhieu},
  POM_DmChiphi in 'PomZ\POM_DmChiphi.pas' {FrmPOM_DmChiphi},
  POM_DmHesoDD in 'PomZ\POM_DmHesoDD.pas' {FrmPOM_DmHesoDD},
  POM_DmHesoDT in 'PomZ\POM_DmHesoDT.pas' {FrmPOM_DmHesoDT},
  POM_DmhhNhom in 'PomZ\POM_DmhhNhom.pas' {FrmPOM_DmhhNhom},
  POM_Dmvt in 'PomZ\POM_Dmvt.pas' {FrmPOM_Dmvt},
  POM_Dmvt_Dinhmuc in 'PomZ\POM_Dmvt_Dinhmuc.pas' {FrmPOM_Dmvt_Dinhmuc},
  POM_DmvtBTP in 'PomZ\POM_DmvtBTP.pas' {FrmPOM_DmvtBTP},
  POM_DmvtBTP_Dinhmuc in 'PomZ\POM_DmvtBTP_Dinhmuc.pas' {FrmPOM_DmvtBTP_Dinhmuc},
  POM_DmvtNVL in 'PomZ\POM_DmvtNVL.pas' {FrmPOM_DmvtNVL},
  TS_DmhhNhom in 'AssetZ\TS_DmhhNhom.pas' {FrmTS_DmhhNhom},
  TS_DmNcc in 'AssetZ\TS_DmNcc.pas' {FrmTS_DmNcc},
  TS_Dmvt in 'AssetZ\TS_Dmvt.pas' {FrmTS_Dmvt},
  TS_DactinhKT in 'AssetZ\TS_DactinhKT.pas' {FrmTS_DactinhKT},
  TS_DmvtFile in 'AssetZ\TS_DmvtFile.pas' {FrmTS_DmvtFile},
  FB_DmhhNhom in 'FBZ\FB_DmhhNhom.pas' {FrmFB_DmhhNhom},
  FB_DmChiphi in 'FBZ\FB_DmChiphi.pas' {FrmFB_DmChiphi},
  FB_DmBan in 'FBZ\FB_DmBan.pas' {FrmFB_DmBan},
  FB_DmvtNVL in 'FBZ\FB_DmvtNVL.pas' {FrmFB_DmvtNVL},
  FB_Dmhh in 'FBZ\FB_Dmhh.pas' {FrmFB_Dmhh},
  DmChiphi in 'SoftZ\DmChiphi.pas' {FrmDmChiphi},
  FB_ChuyenKho in 'FBZ\FB_ChuyenKho.pas' {FrmFB_ChuyenKho},
  FB_Kiemke in 'FBZ\FB_Kiemke.pas' {FrmFB_Kiemke},
  FB_NXKhac in 'FBZ\FB_NXKhac.pas' {FrmFB_NXKhac},
  FB_PhieuNhap in 'FBZ\FB_PhieuNhap.pas' {FrmFB_PhieuNhap},
  FB_ChonDsma in 'FBZ\FB_ChonDsma.pas' {FrmFB_ChonDsma},
  FB_ChonDondh2 in 'FBZ\FB_ChonDondh2.pas' {FrmFB_ChonDondh2},
  FB_ChonPhieunhap in 'FBZ\FB_ChonPhieunhap.pas' {FrmFB_ChonPhieunhap},
  FB_XuatTra in 'FBZ\FB_XuatTra.pas' {FrmFB_Xuattra},
  FB_DondhNCC in 'FBZ\FB_DondhNCC.pas' {FrmFB_DondhNCC},
  FB_CayNganhNhom in 'FBZ\FB_CayNganhNhom.pas' {FrmFB_CayNganhNhom},
  FB_Tonkho in 'FBZ\FB_Tonkho.pas' {FrmFB_Tonkho},
  FB_TudenTon in 'FBZ\FB_TudenTon.pas' {FrmFB_TudenTon},
  FB_DmvtBo in 'FBZ\FB_DmvtBo.pas' {FrmFB_DmvtBo},
  FB_Khuyenmai in 'FBZ\FB_Khuyenmai.pas' {FrmFB_Khuyenmai},
  FB_ChonDsNhomNCC in 'FBZ\FB_ChonDsNhomNCC.pas' {FrmFB_ChonDsNhomNCC},
  FB_ChonDsHHKM in 'FBZ\FB_ChonDsHHKM.pas' {FrmFB_ChonDsHHKM},
  FB_Dmhh_Dinhmuc in 'FBZ\FB_Dmhh_Dinhmuc.pas' {FrmFB_Dmhh_Dinhmuc},
  FB_frameScanCode in 'FBZ\FB_frameScanCode.pas' {FB_frScanCode: TFrame},
  FB_Khuyenmai2 in 'FBZ\FB_Khuyenmai2.pas' {FrmFB_Khuyenmai2},
  FB_ChonPhieuKM in 'FBZ\FB_ChonPhieuKM.pas' {FrmFB_ChonPhieuKM},
  FB_CheckTonkho in 'FBZ\FB_CheckTonkho.pas' {FrmFB_CheckTonkho},
  FB_TheodoiGia in 'FBZ\FB_TheodoiGia.pas' {FrmFB_TheodoiGia},
  FB_ChonDsPN in 'FBZ\FB_ChonDsPN.pas' {FrmFB_ChonDsPN},
  DmKhNccDauKy in 'SoftZ\DmKhNccDauKy.pas' {FrmDmKhNccDauKy},
  FB_Dmhh_File in 'FBZ\FB_Dmhh_File.pas' {FrmFB_Dmhh_File},
  DmnccCty in 'SoftZ\DmnccCty.pas' {FrmDmnccCty},
  FB_CheBien in 'FBZ\FB_CheBien.pas' {FrmFB_CheBien},
  FB_Khuyenmai3 in 'FBZ\FB_Khuyenmai3.pas' {FrmFB_Khuyenmai3},
  FB_Scan in 'FBZ\FB_Scan.pas' {FrmFB_Scan},
  FB_DmLoaiVipCT in 'FBZ\FB_DmLoaiVipCT.pas' {FrmFB_DmLoaiVipCT},
  FB_ChonDsNhomCk in 'FBZ\FB_ChonDsNhomCk.pas' {FrmFB_ChonDsNhomCk},
  FB_Banle in 'FBZ\FB_Banle.pas' {FrmFB_Banle},
  FB_Nhaptrabl in 'FBZ\FB_Nhaptrabl.pas' {FrmFB_Nhaptrabl},
  FB_Sapthutu in 'FBZ\FB_Sapthutu.pas' {FrmFB_Sapthutu},
  DmBacLuong in 'HrZ\DmBacLuong.pas' {FrmDmBacLuong},
  DmDiaDiemLamViec in 'HrZ\DmDiaDiemLamViec.pas' {FrmDmDiaDiemLamViec},
  DmNguonTuyenDung in 'HrZ\DmNguonTuyenDung.pas' {FrmDmNguonTuyenDung},
  DmThietBiTaiSanLaoDong in 'HrZ\DmThietBiTaiSanLaoDong.pas' {FrmDmThietBiTaiSanLaoDong},
  DmHotro_HR in 'HrZ\DmHotro_HR.pas' {FrmDmHotro_HR},
  DmCanhBao in 'HrZ\DmCanhBao.pas' {FrmDmCanhBao},
  DmBieuMau in 'HrZ\DmBieuMau.pas' {FrmDmBieuMau},
  DangkyVangmat in 'HrZ\DangkyVangmat.pas' {FrmDangkyVangmat},
  KhenThuongKyLuat in 'HrZ\KhenThuongKyLuat.pas' {FrmKhenThuongKyLuat},
  DmLyDoKTKL in 'HrZ\DmLyDoKTKL.pas' {FrmDmLyDoKTKL},
  DataAccess in '..\Softz.Admin\DataAccess.pas' {FrmDataAccess},
  HoSonv in 'HrZ\HoSonv.pas' {FrmHoSonv},
  HoSonvThoiviec in 'HrZ\HoSonvThoiviec.pas' {FrmHoSonvThoiviec},
  HoSonvQuanHeGiaDinh in 'HrZ\HoSonvQuanHeGiaDinh.pas' {FrmHoSonvQuanHeGiaDinh},
  HoSonvKienThucTaiLieu in 'HrZ\HoSonvKienThucTaiLieu.pas' {FrmHoSonvKienThucTaiLieu},
  LichSuCongTac in 'HrZ\LichSuCongTac.pas' {FrmLichSuCongTac},
  HosonvTaiKhoanTruyCap in 'HrZ\HosonvTaiKhoanTruyCap.pas' {FrmHosonvTaiKhoanTruyCap},
  PhepnamBangThongso in 'HrZ\PhepnamBangThongso.pas' {FrmPhepnamBangThongso},
  HosonvDesc in 'HrZ\HosonvDesc.pas' {FrmHosonvDesc},
  HoSonvTruocKhiVaoCongTy in 'HrZ\HoSonvTruocKhiVaoCongTy.pas' {FrmHoSonvTruocKhiVaoCongTy},
  ThietBiTaiSan in 'HrZ\ThietBiTaiSan.pas' {FrmThietBiTaiSan},
  HoSonvNgayKyNiem in 'HrZ\HoSonvNgayKyNiem.pas' {FrmHoSonvNgayKyNiem},
  HosonvThongTinKhac in 'HrZ\HosonvThongTinKhac.pas' {FrmHosonvThongTinKhac},
  QuaTrinhLamViec in 'HrZ\QuaTrinhLamViec.pas' {FrmQuaTrinhLamViec},
  frameYearNow in 'HrZ\frameYearNow.pas' {frYearNow: TFrame},
  AdminData in '..\Softz.Admin\AdminData.pas' {DataAdmin: TDataModule},
  ScanAvatar in 'HrZ\ScanAvatar.pas' {FrmScanAvatar},
  HosonvCCCD in 'HrZ\HosonvCCCD.pas' {FrmHosonvCCCD},
  HosonvHoChieu in 'HrZ\HosonvHoChieu.pas' {FrmHosonvHoChieu},
  DangkyTangCa in 'HrZ\DangkyTangCa.pas' {FrmDangkyTangCa},
  KhenThuongKyLuatDangKy in 'HrZ\KhenThuongKyLuatDangKy.pas' {FrmKhenThuongKyLuatDangKy},
  KhenThuongKyLuatDanhSach in 'HrZ\KhenThuongKyLuatDanhSach.pas' {FrmKhenThuongKyLuatDanhSach},
  DangkyVangmatDanhSach in 'HrZ\DangkyVangmatDanhSach.pas' {FrmDangkyVangmatDanhSach},
  DangkyVangmatTools in 'HrZ\DangkyVangmatTools.pas' {FrmDangkyVangmatTools},
  ImportExcelToTable in 'HrZ\ImportExcelToTable.pas' {FrmImportExcelToTable},
  DangkyTangCaTools in 'HrZ\DangkyTangCaTools.pas' {FrmDangkyTangCaTools},
  DangkyTangCaDanhSach in 'HrZ\DangkyTangCaDanhSach.pas' {FrmDangkyTangCaDanhSach},
  HoSonvPhuCapDangky in 'HrZ\HoSonvPhuCapDangky.pas' {FrmHoSonvPhuCapDangky},
  HoSonvPhuCapDanhSach in 'HrZ\HoSonvPhuCapDanhSach.pas' {FrmHoSonvPhuCapDanhSach},
  SystemCriticalU in 'Common\SystemCriticalU.pas',
  MainData in 'Common\MainData.pas' {DataMain: TDataModule},
  OfficeData in 'Common\OfficeData.pas' {DataOffice: TDataModule},
  GmsRep in 'Common\GmsRep.pas' {FrmGmsRep},
  FastReport in 'Common\FastReport.pas' {FrmFastReport: TDataModule},
  GuidEx in 'Common\GuidEx.pas',
  CR208U in 'Common\CR208U.pas',
  ExCommon in 'Common\ExCommon.pas',
  exPrintBill in 'Common\exPrintBill.pas',
  exThread in 'Common\exThread.pas',
  RepEngine in 'Common\RepEngine.pas' {FrmRep: TDataModule},
  crBANLE_TT in 'Common\crFrames\crBANLE_TT.pas' {frameBANLE_TT: TFrame},
  crCHIETKHAU in 'Common\crFrames\crCHIETKHAU.pas' {frameCHIETKHAU: TFrame},
  crCommon in 'Common\crFrames\crCommon.pas' {CrFrame: TFrame},
  crCT_TH in 'Common\crFrames\crCT_TH.pas' {frameCT_TH: TFrame},
  crDoiTuong in 'Common\crFrames\crDoiTuong.pas' {frameHR_DoiTuong: TFrame},
  crDS_BoPhan in 'Common\crFrames\crDS_BoPhan.pas' {frameHR_DS_BoPhan: TFrame},
  crDS_ChiNhanh in 'Common\crFrames\crDS_ChiNhanh.pas' {FrameHR_DS_ChiNhanh: TFrame},
  crDS_ChiNhanhNotCN in 'Common\crFrames\crDS_ChiNhanhNotCN.pas' {FrameHR_DS_ChiNhanhNotCN: TFrame},
  crDS_ChucDanh in 'Common\crFrames\crDS_ChucDanh.pas' {frameHR_DS_ChucDanh: TFrame},
  crDS_DanToc in 'Common\crFrames\crDS_DanToc.pas' {FrameHR_DS_DanToc: TFrame},
  crDS_HopDong in 'Common\crFrames\crDS_HopDong.pas' {frameHR_DS_HopDong: TFrame},
  crDS_KH in 'Common\crFrames\crDS_KH.pas' {frameDS_KH: TFrame},
  crDS_KHO in 'Common\crFrames\crDS_KHO.pas' {frameDS_KHO: TFrame},
  crDS_KLuat in 'Common\crFrames\crDS_KLuat.pas' {FrameHR_DS_KLuat: TFrame},
  crDS_KThuong in 'Common\crFrames\crDS_KThuong.pas' {FrameHR_DS_KThuong: TFrame},
  crDS_LOC in 'Common\crFrames\crDS_LOC.pas' {frameDS_LOC: TFrame},
  crDS_LYDO_CHI in 'Common\crFrames\crDS_LYDO_CHI.pas' {frameDS_LYDO_CHI: TFrame},
  crDS_LYDO_CHIKHAC in 'Common\crFrames\crDS_LYDO_CHIKHAC.pas' {frameDS_LYDO_CHIKHAC: TFrame},
  crDS_LYDO_NK in 'Common\crFrames\crDS_LYDO_NK.pas' {frameDS_LYDO_NK: TFrame},
  crDS_LYDO_THU in 'Common\crFrames\crDS_LYDO_THU.pas' {frameDS_LYDO_THU: TFrame},
  crDS_LYDO_THUKHAC in 'Common\crFrames\crDS_LYDO_THUKHAC.pas' {frameDS_LYDO_THUKHAC: TFrame},
  crDS_LYDO_XK in 'Common\crFrames\crDS_LYDO_XK.pas' {frameDS_LYDO_XK: TFrame},
  crDS_LyDoThoiViec in 'Common\crFrames\crDS_LyDoThoiViec.pas' {frameHR_DS_LyDoThoiViec: TFrame},
  crDS_LyDoVangMat in 'Common\crFrames\crDS_LyDoVangMat.pas' {FrameHR_DS_LyDoVangMat: TFrame},
  crDS_NCC in 'Common\crFrames\crDS_NCC.pas' {frameDS_NCC: TFrame},
  crDS_NguonTD in 'Common\crFrames\crDS_NguonTD.pas' {FrameHR_DS_NguonTD: TFrame},
  crDS_NhanVien in 'Common\crFrames\crDS_NhanVien.pas' {frameHR_DS_NhanVien: TFrame},
  crDS_NHOMVT in 'Common\crFrames\crDS_NHOMVT.pas' {FrameDS_NHOMVT: TFrame},
  crDS_NV in 'Common\crFrames\crDS_NV.pas' {frameDS_NV: TFrame},
  crDS_PhongBan in 'Common\crFrames\crDS_PhongBan.pas' {FrameHR_DS_PhongBan: TFrame},
  crDS_QuaTrinhLV in 'Common\crFrames\crDS_QuaTrinhLV.pas' {frameHR_DS_QuaTrinhLV: TFrame},
  crDS_QuocTich in 'Common\crFrames\crDS_QuocTich.pas' {FrameHR_DS_QuocTich: TFrame},
  crDS_TonGiao in 'Common\crFrames\crDS_TonGiao.pas' {FrameHR_DS_TonGiao: TFrame},
  crDS_USER in 'Common\crFrames\crDS_USER.pas' {frameDS_USER: TFrame},
  crDS_VanHoa in 'Common\crFrames\crDS_VanHoa.pas' {FrameHR_DS_VanHoa: TFrame},
  crDS_VIP in 'Common\crFrames\crDS_VIP.pas' {frameDS_VIP: TFrame},
  crFB_DS_LYDO_NK in 'Common\crFrames\crFB_DS_LYDO_NK.pas' {frameFB_DS_LYDO_NK: TFrame},
  crFB_DS_LYDO_XK in 'Common\crFrames\crFB_DS_LYDO_XK.pas' {frameFB_DS_LYDO_XK: TFrame},
  crFB_DS_NHOMVT in 'Common\crFrames\crFB_DS_NHOMVT.pas' {FrameFB_DS_NHOMVT: TFrame},
  crFB_MAVT in 'Common\crFrames\crFB_MAVT.pas' {frameFB_MAVT: TFrame},
  crFB_MAVT_BO in 'Common\crFrames\crFB_MAVT_BO.pas' {frameFB_MAVT_BO: TFrame},
  crFB_MAVT_NPL in 'Common\crFrames\crFB_MAVT_NPL.pas' {frameFB_MAVT_NPL: TFrame},
  crFB_MAVT_NPL_TD in 'Common\crFrames\crFB_MAVT_NPL_TD.pas' {frameFB_MAVT_NPL_TD: TFrame},
  crFB_MAVT_TD in 'Common\crFrames\crFB_MAVT_TD.pas' {frameFB_MAVT_TD: TFrame},
  crGioiTinh in 'Common\crFrames\crGioiTinh.pas' {frameHR_GioiTinh: TFrame},
  crHonNhan in 'Common\crFrames\crHonNhan.pas' {frameHR_HonNhan: TFrame},
  crKho in 'Common\crFrames\crKho.pas' {frameCrKho: TFrame},
  crKThuongKLuat in 'Common\crFrames\crKThuongKLuat.pas' {frameHR_KThuongKLuat: TFrame},
  crLOAI_HD in 'Common\crFrames\crLOAI_HD.pas' {frameLOAI_HD: TFrame},
  crLoaiNhanVien in 'Common\crFrames\crLoaiNhanVien.pas' {frameHR_LoaiNhanVien: TFrame},
  crLOC in 'Common\crFrames\crLOC.pas' {frameCrLOC: TFrame},
  crMAHH in 'Common\crFrames\crMAHH.pas' {FrameMAHH: TFrame},
  crMAKH in 'Common\crFrames\crMAKH.pas' {frameMAKH: TFrame},
  crMANCC in 'Common\crFrames\crMANCC.pas' {frameMANCC: TFrame},
  crMAVIP in 'Common\crFrames\crMAVIP.pas' {frameMAVIP: TFrame},
  crMAVT in 'Common\crFrames\crMAVT.pas' {frameMAVT: TFrame},
  crNAM in 'Common\crFrames\crNAM.pas' {frameNAM: TFrame},
  crNGANHNHOM in 'Common\crFrames\crNGANHNHOM.pas' {frameNGANHNHOM: TFrame},
  crNgay in 'Common\crFrames\crNgay.pas' {frameCrNgay: TFrame},
  crQuanHeGiaDinh in 'Common\crFrames\crQuanHeGiaDinh.pas' {frameHR_QuanHeGiaDinh: TFrame},
  crQUY in 'Common\crFrames\crQUY.pas' {frameQUY: TFrame},
  crSILE in 'Common\crFrames\crSILE.pas' {frameSILE: TFrame},
  crSONGAY in 'Common\crFrames\crSONGAY.pas' {frameSONGAY: TFrame},
  crSOTHANG in 'Common\crFrames\crSOTHANG.pas' {frameSOTHANG: TFrame},
  crTHANGNAM in 'Common\crFrames\crTHANGNAM.pas' {frameTHANGNAM: TFrame},
  crTHOIGIAN in 'Common\crFrames\crTHOIGIAN.pas' {frameTHOIGIAN: TFrame},
  crTinhTrangNhanVien in 'Common\crFrames\crTinhTrangNhanVien.pas' {frameHR_TinhTrangNhanVien: TFrame},
  crTon in 'Common\crFrames\crTon.pas' {frameTon: TFrame},
  crTOP in 'Common\crFrames\crTOP.pas' {frameTOP: TFrame},
  crTuden in 'Common\crFrames\crTuden.pas' {frameTuden: TFrame},
  crUID in 'Common\crFrames\crUID.pas' {frameUID: TFrame},
  ChonDsBoPhan in 'Common\ChonDsBoPhan.pas' {FrmChonDsBoPhan},
  ChonDsChiNhanh in 'Common\ChonDsChiNhanh.pas' {FrmChonDsChiNhanh},
  ChonDsChucDanh in 'Common\ChonDsChucDanh.pas' {FrmChonDsChucDanh},
  ChonDsDanToc in 'Common\ChonDsDanToc.pas' {FrmChonDsDanToc},
  ChonDsHopDong in 'Common\ChonDsHopDong.pas' {FrmChonDsHopDong},
  ChonDsHH in 'Common\ChonDsHH.pas' {FrmChonDsHH},
  ChonDsKH in 'Common\ChonDsKH.pas' {FrmChonDsKH},
  ChonDsKho in 'Common\ChonDsKho.pas' {FrmChonDsKho},
  ChonDsKLuat in 'Common\ChonDsKLuat.pas' {FrmChonDsKLuat},
  ChonDsKThuong in 'Common\ChonDsKThuong.pas' {FrmChonDsKThuong},
  ChonDsLoc in 'Common\ChonDsLoc.pas' {FrmChonDsLoc},
  ChonDsLydoCHI in 'Common\ChonDsLydoCHI.pas' {FrmChonDsLydoCHI},
  ChonDsLydoCHIKHAC in 'Common\ChonDsLydoCHIKHAC.pas' {FrmChonDsLydoCHIKHAC},
  ChonDsLydoNK in 'Common\ChonDsLydoNK.pas' {FrmChonDsLydoNK},
  ChonDsLyDoThoiViec in 'Common\ChonDsLyDoThoiViec.pas' {FrmChonDsLyDoThoiViec},
  ChonDsLyDoVangMat in 'Common\ChonDsLyDoVangMat.pas' {FrmChonDsLyDoVangMat},
  ChonDsLydoTHU in 'Common\ChonDsLydoTHU.pas' {FrmChonDsLydoTHU},
  ChonDsLydoTHUKHAC in 'Common\ChonDsLydoTHUKHAC.pas' {FrmChonDsLydoTHUKHAC},
  ChonDsLydoXK in 'Common\ChonDsLydoXK.pas' {FrmChonDsLydoXK},
  ChonDsma in 'Common\ChonDsma.pas' {FrmChonDsma},
  ChonDsNCC in 'Common\ChonDsNCC.pas' {FrmChonDsNCC},
  ChonDsNguonTD in 'Common\ChonDsNguonTD.pas' {FrmChonDsNguonTD},
  ChonDsNhanVien in 'Common\ChonDsNhanVien.pas' {FrmChonDsNhanVien},
  ChonDsNhom in 'Common\ChonDsNhom.pas' {FrmChonDsNhom},
  ChonDsNV in 'Common\ChonDsNV.pas' {FrmChonDsNV},
  ChonDsPhongBan in 'Common\ChonDsPhongBan.pas' {FrmChonDsPhongBan},
  ChonDsQuaTrinhLV in 'Common\ChonDsQuaTrinhLV.pas' {FrmChonDsQuaTrinhLV},
  ChonDsQuocTich in 'Common\ChonDsQuocTich.pas' {FrmChonDsQuocTich},
  ChonDsTonGiao in 'Common\ChonDsTonGiao.pas' {FrmChonDsTonGiao},
  ChonDsUser in 'Common\ChonDsUser.pas' {FrmChonDsUser},
  ChonDsVIP in 'Common\ChonDsVIP.pas' {FrmChonDsVIP},
  ChonDsVanHoa in 'Common\ChonDsVanHoa.pas' {FrmChonDsVanHoa},
  FB_ChonDsLydoNK in 'Common\FB_ChonDsLydoNK.pas' {FrmFB_ChonDsLydoNK},
  FB_ChonDsLydoXK in 'Common\FB_ChonDsLydoXK.pas' {FrmFB_ChonDsLydoXK},
  FB_ChonDsNhom in 'Common\FB_ChonDsNhom.pas' {FrmFB_ChonDsNhom},
  FB_ChonDsma_MAVT in 'Common\FB_ChonDsma_MAVT.pas' {FrmFB_ChonDsma_MAVT},
  FB_ChonDsma_MAVT_BO in 'Common\FB_ChonDsma_MAVT_BO.pas' {FrmFB_ChonDsma_MAVT_BO},
  FB_ChonDsma_MAVT_NPL in 'Common\FB_ChonDsma_MAVT_NPL.pas' {FrmFB_ChonDsma_MAVT_NPL},
  FB_ChonDsma_MAVT_NPL_TD in 'Common\FB_ChonDsma_MAVT_NPL_TD.pas' {FrmFB_ChonDsma_MAVT_NPL_TD},
  FB_ChonDsma_MAVT_TD in 'Common\FB_ChonDsma_MAVT_TD.pas' {FrmFB_ChonDsma_MAVT_TD},
  ReceiptDesc in 'Common\ReceiptDesc.pas' {FrmReceiptDesc},
  Dmdl in 'Common\Dmdl.pas' {FrmDmdl},
  HrData in 'HrZ\HrData.pas' {HrDataMain: TDataModule},
  HrExCommon in 'HrZ\HrExCommon.pas',
  ExcelData in 'Common\ExcelData.pas' {DataExcel: TDataModule},
  HoSonvBieuMau in 'HrZ\HoSonvBieuMau.pas' {FrmHoSonvBieuMau},
  NghiBu in 'HrZ\NghiBu.pas' {FrmNghiBu},
  ImportExcel in 'Common\ImportExcel.pas' {FrmImportExcel},
  ImportExcelRev in 'Common\ImportExcelRev.pas' {FrmImportExcelRev},
  HoSonvDanhSachImport in 'HrZ\HoSonvDanhSachImport.pas' {FrmHoSonvDanhSachImport},
  DaoTao in 'HrZ\DaoTao.pas' {FrmDaoTao},
  Sapthutu in 'Common\Sapthutu.pas' {FrmSapthutu},
  frameNavi in 'Common\frameNavi.pas' {frNavi: TFrame},
  LichLamViec in 'HrZ\LichLamViec.pas' {FrmLichLamViec},
  LichLamViecTools in 'HrZ\LichLamViecTools.pas' {FrmLichLamViecTools},
  HoSonvLichSuBieuMau in 'HrZ\HoSonvLichSuBieuMau.pas' {FrmHoSonvLichSuBieuMau},
  DangkyMangThai in 'HrZ\DangkyMangThai.pas' {FrmDangkyMangThai},
  NhapTraBanLe in 'SoftZ\NhapTraBanLe.pas' {FrmNhapTraBanLe},
  NhaptraBanleTimPhieu in 'SoftZ\NhaptraBanleTimPhieu.pas' {FrmNhaptraBanleTimPhieu},
  DmTKKeToan in 'SoftZ\DmTKKeToan.pas' {FrmDmTKKeToan},
  DmvtDanhSachImport in 'SoftZ\DmvtDanhSachImport.pas' {FrmDmvtDanhSachImport},
  DondhEdit in 'SoftZ\DondhEdit.pas' {FrmDondhEdit},
  DondhClose in 'SoftZ\DondhClose.pas' {FrmDondhClose},
  KetCa in 'SoftZ\KetCa.pas' {FrmKetCa},
  crLichSuThanhToanChungTu in 'SoftZ\crFrames\crLichSuThanhToanChungTu.pas' {frameLichSuThanhToanChungTu: TFrame},
  ChuyenDvt in 'SoftZ\ChuyenDvt.pas' {FrmChuyenDvt};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'ErpZ Solutions';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TFrmSoluong, FrmSoluong);
  Application.CreateForm(TFrmChonNgay, FrmChonNgay);
  Application.CreateForm(TFrmTuden, FrmTuden);
  Application.CreateForm(TFrmMamoi, FrmMamoi);
  Application.CreateForm(TDataAdmin, DataAdmin);
  Application.CreateForm(TFrmChonDsNhomCk, FrmChonDsNhomCk);
  Application.CreateForm(TFrmChonDsNhomNCC, FrmChonDsNhomNCC);
  Application.CreateForm(TFrmSelectFields, FrmSelectFields);
  Application.CreateForm(TFrmWaitProgress, FrmWaitProgress);
  Application.CreateForm(TFrmFB_ChonDsNhomNCC, FrmFB_ChonDsNhomNCC);
  Application.CreateForm(TFrmFB_CheckTonkho, FrmFB_CheckTonkho);
  Application.CreateForm(TFrmFB_ChonDsma, FrmFB_ChonDsma);
  Application.CreateForm(TFrmFB_ChonDsNhomCk, FrmFB_ChonDsNhomCk);
  Application.CreateForm(TFrmHosonvTaiKhoanTruyCap, FrmHosonvTaiKhoanTruyCap);
  Application.CreateForm(TFrmHosonvDesc, FrmHosonvDesc);
  Application.CreateForm(TFrmHosonvThongTinKhac, FrmHosonvThongTinKhac);
  Application.CreateForm(TDataAdmin, DataAdmin);
  Application.CreateForm(TFrmImportExcelToTable, FrmImportExcelToTable);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TDataOffice, DataOffice);
  Application.CreateForm(TFrmGmsRep, FrmGmsRep);
  Application.CreateForm(TFrmFastReport, FrmFastReport);
  Application.CreateForm(TFrmRep, FrmRep);
  Application.CreateForm(TFrmChonDsChiNhanh, FrmChonDsChiNhanh);
  Application.CreateForm(TFrmChonDsDanToc, FrmChonDsDanToc);
  Application.CreateForm(TFrmChonDsHopDong, FrmChonDsHopDong);
  Application.CreateForm(TFrmChonDsHH, FrmChonDsHH);
  Application.CreateForm(TFrmChonDsKH, FrmChonDsKH);
  Application.CreateForm(TFrmChonDsKho, FrmChonDsKho);
  Application.CreateForm(TFrmChonDsKLuat, FrmChonDsKLuat);
  Application.CreateForm(TFrmChonDsKThuong, FrmChonDsKThuong);
  Application.CreateForm(TFrmChonDsLoc, FrmChonDsLoc);
  Application.CreateForm(TFrmChonDsLydoCHI, FrmChonDsLydoCHI);
  Application.CreateForm(TFrmChonDsLydoCHIKHAC, FrmChonDsLydoCHIKHAC);
  Application.CreateForm(TFrmChonDsLydoNK, FrmChonDsLydoNK);
  Application.CreateForm(TFrmChonDsLyDoThoiViec, FrmChonDsLyDoThoiViec);
  Application.CreateForm(TFrmChonDsLyDoVangMat, FrmChonDsLyDoVangMat);
  Application.CreateForm(TFrmChonDsLydoTHU, FrmChonDsLydoTHU);
  Application.CreateForm(TFrmChonDsLydoTHUKHAC, FrmChonDsLydoTHUKHAC);
  Application.CreateForm(TFrmChonDsLydoXK, FrmChonDsLydoXK);
  Application.CreateForm(TFrmChonDsma, FrmChonDsma);
  Application.CreateForm(TFrmChonDsNCC, FrmChonDsNCC);
  Application.CreateForm(TFrmChonDsNguonTD, FrmChonDsNguonTD);
  Application.CreateForm(TFrmChonDsNhom, FrmChonDsNhom);
  Application.CreateForm(TFrmChonDsNV, FrmChonDsNV);
  Application.CreateForm(TFrmChonDsPhongBan, FrmChonDsPhongBan);
  Application.CreateForm(TFrmChonDsQuaTrinhLV, FrmChonDsQuaTrinhLV);
  Application.CreateForm(TFrmChonDsQuocTich, FrmChonDsQuocTich);
  Application.CreateForm(TFrmChonDsTonGiao, FrmChonDsTonGiao);
  Application.CreateForm(TFrmChonDsUser, FrmChonDsUser);
  Application.CreateForm(TFrmChonDsVIP, FrmChonDsVIP);
  Application.CreateForm(TFrmChonDsVanHoa, FrmChonDsVanHoa);
  Application.CreateForm(TFrmFB_ChonDsLydoNK, FrmFB_ChonDsLydoNK);
  Application.CreateForm(TFrmFB_ChonDsLydoXK, FrmFB_ChonDsLydoXK);
  Application.CreateForm(TFrmFB_ChonDsNhom, FrmFB_ChonDsNhom);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT, FrmFB_ChonDsma_MAVT);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_BO, FrmFB_ChonDsma_MAVT_BO);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_NPL, FrmFB_ChonDsma_MAVT_NPL);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_NPL_TD, FrmFB_ChonDsma_MAVT_NPL_TD);
  Application.CreateForm(TFrmFB_ChonDsma_MAVT_TD, FrmFB_ChonDsma_MAVT_TD);
  Application.CreateForm(THrDataMain, HrDataMain);
  Application.CreateForm(TDataExcel, DataExcel);
  Application.CreateForm(TFrmImportExcel, FrmImportExcel);
  Application.CreateForm(TFrmImportExcelRev, FrmImportExcelRev);
  if not DataMain.Logon then
  begin
      Application.Terminate;
      Exit;
  end;

  (*==============================================================================
   ** Form su dung LookupComboboxEh tu dong ket noi va open DataSource
   *)
  Application.CreateForm(TFrmChonDsChucDanh, FrmChonDsChucDanh);
  Application.CreateForm(TFrmChonDsNhanVien, FrmChonDsNhanVien);
  Application.CreateForm(TFrmKhenThuongKyLuatDangKy, FrmKhenThuongKyLuatDangKy);
  Application.CreateForm(TFrmDangkyVangmatTools, FrmDangkyVangmatTools);
  Application.CreateForm(TFrmDangkyTangCaTools, FrmDangkyTangCaTools);
  Application.CreateForm(TFrmHoSonvPhuCapDangky, FrmHoSonvPhuCapDangky);
  Application.CreateForm(TFrmFB_TheodoiGia, FrmFB_TheodoiGia);
  Application.CreateForm(TFrmFB_DmLoaiVipCT, FrmFB_DmLoaiVipCT);
  Application.CreateForm(TFrmChonDsBoPhan, FrmChonDsBoPhan);
  Application.CreateForm(TFrmChonDsChucDanh, FrmChonDsChucDanh);
  Application.CreateForm(TFrmChonDsNhanVien, FrmChonDsNhanVien);

  Application.Run;
end.
