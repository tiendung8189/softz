(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameYearNow;

interface

uses
  Windows, Classes, Controls, Forms,
  fctreecombo, StdCtrls, ExtCtrls,
  ActnList, fcCombo, wwdbdatetimepicker;

type
  TfrYearNow = class(TFrame)
    Panel1: TPanel;
    LbOrg: TLabel;
    CbOrg: TfcTreeCombo;
    Label5: TLabel;
    CbYear: TComboBox;
    Label1: TLabel;
    EdNow: TwwDBDateTimePicker;
    procedure CbOrgChange(Sender: TObject);
    procedure CbOrgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CbOrgCloseUp(Sender: TObject; Select: Boolean);
  private
  	FAction: TAction;
  public
  	procedure Initial(y: Integer; date: TDateTime; ac: TAction);
  end;

implementation

uses
	ExCommon, isStr, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYearNow.Initial;
begin
    isYearList(CbYear, y);
    EdNow.Date := date;
    FAction := ac;
  	if sysIsDataAccess then
        FlexOrgComboDataAccess(CbOrg)
    else
      	FlexOrgCombo(CbOrg);
    CbOrg.OnChange(CbOrg);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYearNow.CbOrgChange(Sender: TObject);
begin
    LbOrg.Caption := exGetFlexOrgDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYearNow.CbOrgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearOrg}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYearNow.CbOrgCloseUp(Sender: TObject; Select: Boolean);
begin
	if Assigned(FAction) then
	    if Select then
    	    FAction.Execute;
end;

end.
