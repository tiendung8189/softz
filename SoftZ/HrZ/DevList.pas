﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevList;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  wwdbedit, Wwdbcomb, isPanel, AppEvnts,
  RzSplit, Buttons, zkemkeeper_TLB, wwDialog, wwfltdlg, OleCtrls, Grids,
  Wwdbigrd, Wwdbgrid, Mask, Wwdotdot, ExtCtrls, RzPanel, ToolWin, DBCtrlsEh;

type
  TFrmDevList = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    Status: TStatusBar;
    QrListRec: TADOQuery;
    DsListRec: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    QrListRecCOM_TYPE: TStringField;
    QrListRecBAUD_RATE: TIntegerField;
    QrListRecIP4: TStringField;
    QrListRecCREATE_BY: TIntegerField;
    QrListRecUPDATE_BY: TIntegerField;
    QrListRecCREATE_DATE: TDateTimeField;
    QrListRecUPDATE_DATE: TDateTimeField;
    QrListRecIO_TYPE: TStringField;
    Label3: TLabel;
    EdTENMAY: TDBEditEh;
    CbComType: TwwDBComboBox;
    Label4: TLabel;
    CbIO: TwwDBComboBox;
    EdIP: TDBEditEh;
    EdSocket: TDBEditEh;
    LbRate: TLabel;
    LbPort: TLabel;
    CbRate: TwwDBComboBox;
    CbPort: TwwDBComboBox;
    QrListRecPORT: TIntegerField;
    QrListRecSOCKET: TIntegerField;
    CmdRestoreDevData: TAction;
    CmdConnect: TAction;
    QrListRecCALC_IO_TYPE: TStringField;
    CmdOption: TAction;
    CmdBackupDevData: TAction;
    BitBtn1: TSpeedButton;
    BitBtn2: TSpeedButton;
    BitBtn3: TSpeedButton;
    BitBtn4: TSpeedButton;
    GroupBox1: TGroupBox;
    MeInfo: TMemo;
    RestoreFrom: TOpenDialog;
    SaveTo: TSaveDialog;
    QrListRecTENMAY: TWideStringField;
    QrListRecIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrListRecBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrListRecBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrListRecBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrListRecAfterInsert(DataSet: TDataSet);
    procedure CmdConnectExecute(Sender: TObject);
    procedure QrListRecCalcFields(DataSet: TDataSet);
    procedure QrListRecAfterScroll(DataSet: TDataSet);
    procedure QrListRecCOM_TYPEChange(Sender: TField);
    procedure QrListRecAfterEdit(DataSet: TDataSet);
    procedure CmdOptionExecute(Sender: TObject);
    procedure CmdBackupDevDataExecute(Sender: TObject);
    procedure CmdRestoreDevDataExecute(Sender: TObject);
  private
  	mCanEdit, mTrigger, fixCode: Boolean;

    procedure SetUI;
	procedure DevConnect;
	procedure DevDisconnect;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDevList: TFrmDevList;

implementation

uses
	ExCommon , isDb, isLib, isMsg, Rights, MainData, zkem, DevOption;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_MAYCC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrListRec, FORM_CODE, Filter);

    mTrigger := False;

    // Reposition
//    LbRate.Top := LbIP.Top;
//    CbRate.Top := EdIP.Top;
//    LbPort.Top := LbSocket.Top;
//    CbPort.Top := EdSocket.Top;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.FormShow(Sender: TObject);
var
    k, n: Integer;
begin
    QrListRec.Open;
    GrList.SetFocus;

    n:= 0;
    for k := 1 to 12 do
    begin
        n := n + 9600;
        CbRate.Items.Add(IntToStr(n));
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    CloseDataSets([QrListRec]);
    zkDisconnect;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrListRec, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdCloseExecute(Sender: TObject);
begin
    if CmdConnect.Tag = 1 then
    begin
        zkDisconnect;
        Close;    
    end
    else
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdNewExecute(Sender: TObject);
begin
    QrListRec.Append;
    EdTENMAY.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdSaveExecute(Sender: TObject);
begin
    QrListRec.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdCancelExecute(Sender: TObject);
begin
    QrListRec.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdDelExecute(Sender: TObject);
begin
    QrListRec.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CONNECT = 'Kết nối';
	RS_DISCONNECT = 'Ngắt kết nối';

procedure TFrmDevList.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
    exActionUpdate(ActionList, QrListRec, Filter, mCanEdit);

    b := QrListRec.State in [dsBrowse];
    CmdConnect.Enabled := b;

    b := CmdConnect.Tag = 1;
    if b then
	    CmdConnect.Caption := (RS_DISCONNECT)
    else
	    CmdConnect.Caption := (RS_CONNECT);

    CmdBackupDevData.Enabled := b;
    CmdRestoreDevData.Enabled := b;
    CmdOption.Enabled := b;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecBeforePost(DataSet: TDataSet);
begin
	with (DataSet) do
    begin
	    if BlankConfirm(DataSet, ['TENMAY']) then
   			Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrListRec, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsListRec);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecAfterInsert(DataSet: TDataSet);
begin
	with QrListRec do
    begin
    	FieldByName('COM_TYPE').AsString := 'E';
    	FieldByName('IP4').AsString := '192.168.0.0';	// PQT insert code here
    	FieldByName('SOCKET').AsInteger := 4370;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.DevConnect;
begin
	Screen.Cursor := crHourGlass;
//	with QrListRec do
//        if zkConnect(CZKEM1,
//            FieldByName('COM_TYPE').AsString,
//            FieldByName('IP4').AsString,
//            FieldByName('SOCKET').AsInteger,
//            FieldByName('PORT').AsInteger,
//            FieldByName('ID').AsInteger,
//            FieldByName('BAUD_RATE').AsInteger) then
//        begin
//            CmdConnect.Tag := 1;
//            with MeInfo do
//            begin
//                zkReadInfo(Lines);
//                SelStart := 0;
//                SelLength := 0;
//            end;
//        end
//        else
//            CmdConnect.Tag := 0;
	Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.DevDisconnect;
begin
    if CmdConnect.Tag <> 0 then
    	zkDisconnect;
    CmdConnect.Tag := 0;
    MeInfo.Lines.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdConnectExecute(Sender: TObject);
begin
	if CmdConnect.Tag = 1 then
    begin
    	DevDisconnect;
        Exit;
    end;
	DevConnect;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecCalcFields(DataSet: TDataSet);
var
	s: String;
begin
	with QrListRec do
    begin
        s := FieldByName('IO_TYPE').AsString + 'X';
        case s[1] of
        'I':
	    	FieldByName('CALC_IO_TYPE').AsString := 'Máy vào';
        'O':
	    	FieldByName('CALC_IO_TYPE').AsString := 'Máy ra';
        else
	    	FieldByName('CALC_IO_TYPE').AsString := 'Máy vào ra'
		end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.SetUI;
begin
    LbRate.Visible := False;
//    LbIP.Visible := False;
    CbRate.Visible := False;
    EdIP.Visible := False;
    LbPort.Visible := False;
//    LbSocket.Visible := False;
    CbPort.Visible := False;
    EdSocket.Visible := False;

    if QrListRec.FieldByName('COM_TYPE').AsString = 'E' then
    begin
//        LbIP.Visible := True;
        EdIP.Visible := True;
//        LbSocket.Visible := True;
        EdSocket.Visible := True;
    end;

    if QrListRec.FieldByName('COM_TYPE').AsString = 'S' then
    begin
        LbRate.Visible := True;
        CbRate.Visible := True;
        LbPort.Visible := True;
        CbPort.Visible := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecAfterScroll(DataSet: TDataSet);
begin
	DevDisconnect;
    SetUI;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecCOM_TYPEChange(Sender: TField);
begin
    SetUI;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.QrListRecAfterEdit(DataSet: TDataSet);
begin
	DevDisconnect;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdOptionExecute(Sender: TObject);
var
	b: Boolean;
begin
	Application.CreateForm(TFrmDevOption, FrmDevOption);
    b := FrmDevOption.Execute;
    if b then
    begin
		CmdConnect.Tag := 0;
        MeInfo.Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevList.CmdBackupDevDataExecute(Sender: TObject);
begin
    if SaveTo.Execute then
        zkGetAllUserInfo(SaveTo.FileName);
end;

procedure TFrmDevList.CmdRestoreDevDataExecute(Sender: TObject);
begin
    if RestoreFrom.Execute then
        zkSetAllUserInfo(RestoreFrom.FileName);
end;

end.
