(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Tuden;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls;

type
  TFrmTuden = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdTu: TwwDBDateTimePicker;
    Label2: TLabel;
    EdDen: TwwDBDateTimePicker;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
  public
  	function Execute(var ngayd, ngayc : TDateTime) : Boolean;
  end;

var
  FrmTuden: TFrmTuden;

implementation

{$R *.DFM}

uses
	isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTuden.Execute(var ngayd, ngayc : TDateTime) : Boolean;
begin
   	EdTu.Date  := ngayd;
   	EdDen.Date := ngayc;

	Result := ShowModal = mrOK;
	if Result then
    begin
    	ngayd := EdTu.Date;
    	ngayc := EdDen.Date;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTuden.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTuden.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
	EdTu.SetFocus;
end;

end.
