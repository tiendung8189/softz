(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BosungLuong;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, DBCtrls, ActnList, Db, ExtCtrls, Grids, RzPanel, RzDBNav;

type
  TFrmBosungLuong = class(TForm)
    Inspect: TwwDataInspector;
    Action: TActionList;
    CmdNext: TAction;
    CmdPrior: TAction;
    DBNavigator1: TRzDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdNextExecute(Sender: TObject);
    procedure CmdPriorExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TntFormCreate(Sender: TObject);
  private
  	mDataSource: TDataSource;
  public
  	procedure Execute(pDataSource: TDataSource; pStr: String);
  end;

var
  FrmBosungLuong: TFrmBosungLuong;

implementation

uses
	ExCommon, isLib, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	mDataSource.DataSet.CheckBrowseMode;
	Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.CmdNextExecute(Sender: TObject);
begin
	mDataSource.DataSet.Next;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.CmdPriorExecute(Sender: TObject);
begin
	mDataSource.DataSet.Prior;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	CmdNext.Enabled  := not mDataSource.DataSet.Eof;
	CmdPrior.Enabled := not mDataSource.DataSet.Bof;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.Execute;
var
	n, h: Integer;
begin
	mDataSource := pDataSource;
    Inspect.DataSource := pDataSource;
    DBNavigator1.DataSource := pDataSource;

	n := exConfigInspector(Inspect, pStr);
    if n < 12 then
    	with Inspect do
        begin
            h := Height;
            Height := Trunc(n * (h - 4) / 12.0) + 4;
            ApplySettings;
            Self.Height := Self.Height + Height - h;
        end;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungLuong.TntFormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
