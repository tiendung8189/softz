﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmNgayle;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin, StdCtrls, ExtCtrls, wwdblook;

type
  TFrmDmNgayle = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    CmdClearFilter: TAction;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    QrDMNGAY: TDateTimeField;
    Panel1: TPanel;
    Label1: TLabel;
    CbYear: TComboBox;
    QrDMTHU: TWideStringField;
    CmdEveryYear: TAction;
    SET_DEFAULT: TADOCommand;
    QrDMTenLeTet: TWideStringField;
    QrDMTenLeTet_TA: TWideStringField;
    QrDMLoai: TIntegerField;
    CbLoai: TwwDBLookupCombo;
    QrDMLK_TenLoai: TWideStringField;
    QrDMHeSoOT: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CbYearChange(Sender: TObject);
    procedure QrDMCalcFields(DataSet: TDataSet);
    procedure CmdEveryYearExecute(Sender: TObject);
    procedure QrDMBeforeOpen(DataSet: TDataSet);
    procedure QrDMAfterInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmNgayle: TFrmDmNgayle;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    isStr, HrData;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NGAYLETET';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDM.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    isYearList(CbYear);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.FormShow(Sender: TObject);
begin
    SetDisplayFormat(QrDM, sysCurFmt);
    SetDisplayFormat(QrDM, ['HeSoOT'],sysFloatFmtOne);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDM, FORM_CODE, Filter);

    OpenDataSets([HrDataMain.QrV_HR_LOAI_NGAY_TINHCONG, QrDM]);

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

//	s := Field.FullName;
//    with (Sender as TwwDbGrid) do
//    begin
//        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
//        begin
//            ABrush.Color := $00CBDCE4;
//            AFont.Color  := clBlack;
//        end;
//
//        if (UpperCase(s) = 'NGAY') then
//        begin
//            case DataSource.DataSet.FieldByName('Loai').AsInteger of
//            2:
//                begin
//                    AFont.Style := [fsBold];
//                    AFont.Color := clBlue;
//                end;
//            3:
//                 begin
//                    AFont.Style := [fsBold];
//                    AFont.Color := clRed;
//                 end
//            else
//            end;
//        end;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
	GrList.SetFocus;
end;

procedure TFrmDmNgayle.CmdEveryYearExecute(Sender: TObject);
var
	n: Integer;
begin
    n := StrToInt(CbYear.Text);
	with SET_DEFAULT do
    begin
    	Parameters[0].Value := sysLogonUID;
    	Parameters[1].Value := sysLogonUID;
    	Parameters[2].Value := n;
    	Parameters[3].Value := n;
        Execute;
    end;
    QrDM.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID, CbYear.Text]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMAfterInsert(DataSet: TDataSet);
begin
    with QrDM do
    begin
        FieldByName('Loai').AsInteger := 3;
        FieldByName('HeSoOT').AsFloat := 3.0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDM, ['NGAY', 'TenLeTet', 'Loai']) then
   		Abort;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMCalcFields(DataSet: TDataSet);
const
	DAY_OF_WEEK: array[1..14] of String = (
    	'Chủ nhật',
    	'Hai',
    	'Ba',
    	'Tư',
    	'Năm',
    	'Sáu',
    	'Bảy',
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat'
    );
var
	n: Integer;
begin
	n := Iif(sysEnglish, 7, 0);
	with QrDM do
    	FieldByName('THU').AsString :=
	    	DAY_OF_WEEK[n + DayOfWeek(FieldByName('NGAY').AsDateTime)];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.QrDMBeforeOpen(DataSet: TDataSet);
begin
    with QrDM do
    	Parameters[0].Value := CbYear.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CbYearChange(Sender: TObject);
begin
    with QrDM do
    begin
    	Close;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNgayle.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

end.
