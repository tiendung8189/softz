object frD2D: TfrD2D
  Left = 0
  Top = 0
  Width = 451
  Height = 48
  Align = alTop
  AutoSize = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 48
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    OnExit = Panel1Exit
    DesignSize = (
      451
      48)
    object Label65: TLabel
      Left = 34
      Top = 16
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label66: TLabel
      Left = 211
      Top = 16
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbOrg: TLabel
      Left = 386
      Top = 16
      Width = 77
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Ph'#242'ng ban'
      FocusControl = CbOrg
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdFrom: TwwDBDateTimePicker
      Left = 90
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
    object EdTo: TwwDBDateTimePicker
      Left = 273
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 1
    end
    object CbOrg: TfcTreeCombo
      Left = 471
      Top = 12
      Width = 291
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      AllowClearKey = True
      ButtonStyle = cbsDownArrow
      Color = 15794175
      Text = 'CbOrg'
      DropDownCount = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Images = DataMain.ImageOrg
      Items.StreamVersion = 1
      Items.Data = {00000000}
      Options = [icoExpanded]
      ReadOnly = False
      ShowMatchText = True
      Sorted = False
      Style = csDropDownList
      TabOrder = 2
      TreeOptions = [tvoHideSelection, tvoRowSelect, tvoShowButtons, tvoShowLines, tvoShowRoot, tvoToolTips]
      OnChange = CbOrgChange
      OnCloseUp = CbOrgCloseUp
      OnExit = CbOrgChange
      OnKeyDown = CbOrgKeyDown
    end
  end
end
