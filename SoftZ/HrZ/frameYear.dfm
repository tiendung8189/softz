object frYear: TfrYear
  Left = 0
  Top = 0
  Width = 451
  Height = 48
  Align = alTop
  AutoSize = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 48
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      451
      48)
    object LbOrg: TLabel
      Left = 374
      Top = 16
      Width = 77
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Ph'#242'ng ban'
      FocusControl = CbOrg
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 46
      Top = 16
      Width = 26
      Height = 16
      Alignment = taRightJustify
      Caption = '&N'#259'm'
      FocusControl = CbYear
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbOrg: TfcTreeCombo
      Left = 460
      Top = 12
      Width = 321
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      AllowClearKey = True
      ButtonStyle = cbsDownArrow
      Color = 15794175
      Text = 'CbOrg'
      DropDownCount = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Images = DataMain.ImageOrg
      Items.StreamVersion = 1
      Items.Data = {00000000}
      Options = [icoExpanded]
      ReadOnly = False
      ShowMatchText = True
      Sorted = False
      Style = csDropDownList
      TabOrder = 1
      TreeOptions = [tvoHideSelection, tvoRowSelect, tvoShowButtons, tvoShowLines, tvoShowRoot, tvoToolTips]
      OnChange = CbOrgChange
      OnCloseUp = CbOrgCloseUp
      OnExit = CbOrgChange
      OnKeyDown = CbOrgKeyDown
    end
    object CbYear: TComboBox
      Left = 80
      Top = 12
      Width = 65
      Height = 24
      Style = csDropDownList
      Color = 15794175
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
