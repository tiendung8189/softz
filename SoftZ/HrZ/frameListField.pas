(*==============================================================================
** List of Field from Table or Query
**------------------------------------------------------------------------------
*)
unit frameListField;

interface

uses
  Variants, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, RzSplit, ADODB,
  ComCtrls, Buttons, RzPanel;

type
  TfrListField = class(TFrame)
    RzSplitter1: TRzSplitter;
    Panel2: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label5: TLabel;
    Panel5: TPanel;
    TntLabel1: TLabel;
    LstListField: TListView;
    LstSelect: TListView;
    CmdClear: TSpeedButton;
    CmdRemove: TSpeedButton;
    CmdAdd: TSpeedButton;
    CmdAddAll: TSpeedButton;
    procedure CmdAddClick(Sender: TObject);
    procedure CmdAddAllClick(Sender: TObject);
    procedure CmdRemoveClick(Sender: TObject);
    procedure CmdClearClick(Sender: TObject);
    procedure LstSelectDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure LstSelectDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure LstSelectDblClick(Sender: TObject);
    procedure LstListFieldDblClick(Sender: TObject);
  private
	procedure GetListField;
    procedure MoveSelectedItem(pSource, pDest: TListView);
    procedure MoveAllItem(pSource, pDest: TListView);
  public
	procedure Initial(pTable: string); overload;
    procedure Initial(pQuery: TADOQuery; pFORMCODE: String = ''); overload;
    procedure Initial2(pQuery: TADOQuery); overload;
    procedure Initial2(pQuery: TADOStoredProc); overload;
    Function GetSelectFields: TStrings;
  end;

var
	mLstList, mFields: TStrings;
implementation

uses
	MainData, DB, isCommon, isType;
{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.Initial(pTable: string);
var
	QrField: TADOQuery;
begin
	QrField := TADOQuery.Create(Nil);
    with QrField do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select top 0 * from [' + pTable + ']';
        Open;
        GetFieldNames(mLstList);
        Close;
        Free;
    end;
    GetListField;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.Initial2(pQuery: TADOQuery);
var
	n, j: Integer;
    mItem: TListItem;
begin
	n := pQuery.FieldCount - 1;
    for j := 0 to n do
    with pQuery.Fields[j] do
    	if Visible then
        begin
            mItem := LstListField.Items.Add;
            mItem.Caption := FieldName;
            mItem.SubItems.Add(DisplayLabel);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.Initial2(pQuery: TADOStoredProc);
var
	n, j: Integer;
    mItem: TListItem;
begin
	with pQuery do
    begin

    	Parameters.Refresh;
        n := Parameters.Count - 1;

        for j := 0 to n do
    		if Parameters[j].DataType = ftString then
            	Parameters[j].Value := ''
            else
                Parameters[j].Value := 0;

    	pQuery.ExecProc;
        Open;
        n := pQuery.FieldCount - 1;
    end;

    for j := 0 to n do
    with pQuery.Fields[j] do
    	if Visible then
        begin
            mItem := LstListField.Items.Add;
            mItem.Caption := FieldName;
            mItem.SubItems.Add(DisplayLabel);
        end;
    pQuery.Close;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.Initial(pQuery: TADOQuery; pFORMCODE: String = '');
var
    mItem: TListItem;
    fDesc: String;
begin
	if pFORMCODE <> '' then
    begin
    	with TADOQuery.Create(Nil) do
    	begin
        	// Language
//            case sysLanguage of
//            ENGLISH:
//                fdesc := 'DESCRIPTION1'
//            else
                fdesc := 'DESCRIPTION';
//            end;

            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text := 'select * from SYS_DICTIONARY where [TABLE]=''' +
            	pFORMCODE + ''' and isnull(['+ fdesc +'],'''') <> ''''';

            Open;
            Sort := '[FIELD]';

            // Add to List view
        	First;
	        while not Eof do
    	    begin
				with LstListField do
                begin
                    mItem := Items.Add;
                    mItem.Caption := FieldByName('FIELD').AsString;
                    mItem.SubItems.Add(FieldByName(fDesc).AsString);
                end;
                Next;
        	end;
        
            // Done
            Close;
            Free;
        end;
    end;

    // Add all Field
    if LstListField.Items.Count < 1 then
    begin
  		pQuery.GetFieldNames(mLstList);
		GetListField;
    end;
end;

(*==============================================================================
** Get List Field
**------------------------------------------------------------------------------
*)
procedure TfrListField.GetListField;
var
	j: Integer;
    mItem: TListItem;
begin
	with LstListField do
    begin
        for j := 0 to mLstList.Count - 1 do
        begin
        	mItem := Items.Add;
        	mItem.Caption := mLstList[j];
            mItem.SubItems.Add(mLstList[j]);
        end;
    end;
end;

(*==============================================================================
**	Move selected Item from pSource to pDest ListView
**------------------------------------------------------------------------------
*)
procedure TfrListField.MoveSelectedItem(pSource, pDest: TListView);
var
    i, j: Integer;
    mItem: TListItem;
begin
	i := pDest.Items.Count;
	with pSource do
    begin
    	if SelCount > 1 then
        begin
            for j := -1 + pSource.Items.Count downto 0 do
                if Items.Item[j].Selected then
                begin
                	mItem := pDest.Items.Insert(i);
                    mItem.Assign(Items.Item[j]);
                    Items.Item[j].Delete;
                end;
        end
        else if SelCount = 1 then
        begin
        	mItem := pDest.Items.Add;
            mItem.Assign(Selected);
            Selected.Delete;
        end;
    end;
end;

(*==============================================================================
**	Move All Item from pSource to pDest ListView
**------------------------------------------------------------------------------
*)
procedure TfrListField.MoveAllItem(pSource, pDest: TListView);
var
    j, n: Integer;
    mItem: TListItem;
begin
	n := pDest.Items.Count;
	with pSource do
    begin
        for j := -1 + pSource.Items.Count downto 0 do
        begin
        	mItem := pDest.Items.Insert(n);
            mItem.Assign(Items.Item[j]);
            Items.Item[j].Delete;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.CmdAddClick(Sender: TObject);
begin
	MoveSelectedItem(LstListField, LstSelect);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.CmdAddAllClick(Sender: TObject);
begin
	MoveAllItem(LstListField, LstSelect);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.CmdRemoveClick(Sender: TObject);
begin
	MoveSelectedItem(LstSelect, LstListField);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.CmdClearClick(Sender: TObject);
begin
	MoveAllItem(LstSelect, LstListField);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.LstSelectDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
	Accept := Sender = LstSelect;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.LstSelectDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  currentItem, nextItem, dragItem, dropItem : TListItem;
begin
	if Sender = Source then
  begin
    with TListView(Sender) do
    begin
      dropItem := GetItemAt(X, Y) ;
      currentItem := Selected;
      while currentItem <> nil do
      begin
        nextItem := GetNextItem(currentItem, SdAll, [IsSelected]) ;
        if Assigned(dropItem) then
          dragItem := Items.Insert(dropItem.Index)
        else
          dragItem := Items.Add;
        dragItem.Assign(currentItem) ;
        currentItem.Free;
        currentItem := nextItem;
      end;
    end;
  end;
end;
  
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.LstSelectDblClick(Sender: TObject);
begin
	CmdRemove.Click;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrListField.LstListFieldDblClick(Sender: TObject);
begin
	CmdAdd.Click;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TfrListField.GetSelectFields: TStrings;
var
	j: Integer;
begin
	Result := Nil;

	with LstSelect do
    if Items.Count > 0 then
    begin
        mFields.Clear;
        for j := 0 to Items.Count - 1 do
            mFields.Add(Items.Item[j].Caption);

        Result := mFields;
    end;
end;

initialization
	mLstList := TStringList.Create;
    mFields := TStringList.Create;


finalization
	mLstList.Free;
    mFields.Free;
end.
