﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ImportExcelToTable;

interface

uses
  Classes, Controls, Forms, SysUtils, wwDataInspector, ActnList, Db,
  ExtCtrls, StdCtrls, ADODB, Buttons, kbmMemTable,
  ExportDS, SME2OLE, SMEEngine, ShellApi,
  Graphics, MainData, XLSDbRead4,
  XLSReadWriteII4, SME2Cell, Grids, XLSFonts4, System.RegularExpressions, System.Variants;

type
  TFrmImportExcelToTable = class(TForm)
    Inspect: TwwDataInspector;
    Action: TActionList;
    CmdRefresh: TAction;
    CmdImport: TAction;
    Conn: TADOConnection;
    QrExcel: TADOQuery;
    cbFields: TComboBox;
    QrFields: TkbmMemTable;
    dsFields: TDataSource;
    Panel1: TPanel;
    Label3: TLabel;
    cbSheet: TComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CmdReview: TAction;
    dsExcel: TDataSource;
    QrIMPORT: TADOQuery;
    SP_PROCESS: TADOCommand;
    ExToExcel: TSMExportToExcel;
    CmdClose: TAction;
    XLSReadWriteII41: TXLSReadWriteII4;
    XLSDbRead41: TXLSDbRead4;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdReviewExecute(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure ExToExcelGetCellParams(Sender: TObject; Field: TField;
      var Text: WideString; AFont: TFont; var Alignment: TAlignment;
      var Background: TColor; var CellType: TCellType);
    procedure ExToExcelProgress(Sender: TObject; CurValue, MaxValue: Integer;
      var Abort: Boolean);
    procedure CmdCloseExecute(Sender: TObject);
  private
  	mDataSet: TDataSet;
    mKeyField, mRequiredFld, mFileName, mSection: string;
    mRequired: Boolean;
    mIniFile: string;

    function  ConfigInspector(pFieldNames: String): Integer; overload;
    function  ConfigInspector(pFieldNames: TStrings): Integer; overload;
    function  CheckField(pAuto: Boolean = True): Boolean;
    procedure ConnectToExcel;

    function GetIniFile: String;
  public
    function ExcelImport(const pProfileSection, pSqlTable, pSqlProc: String;
        pProcParamValues: array of Variant; pTransNo: Integer = 0): Boolean;
    function ExcelImportToTable(const pProfileSection, pSqlTable, pSqlProc: String;
        pProcParamValues: array of Variant; pTransNo: Integer = 0): Boolean;
    procedure ExportDataToFile(pFile: String; pLstField: TStrings; pDB: TCustomADODataSet;
    	pSheet: String; pDesc: String = 'Exporting'; pErrField: String = '';
        pOveride: Boolean = True;  pAddTitle: Boolean = True; pOpen: Boolean = True;
        p1StDataRow: Integer = 0);

    function Execute(const pFile, pSec: String; pDataSet: TDataSet;
        pLstField: TStrings; pRequired: Boolean = False; pKeyField: String = ''; pRequiredFld: String=''): Boolean; overload;
  	function Execute(const pFile, pSec: String; pDataSet: TDataSet;
        pStr: String; pRequired: Boolean = False; pKeyField: String = ''; pRequiredFld: String=''): Boolean; overload;
    function Execute(const pSec: String; pDataSet: TDataSet;
        pStr: String; pRequired: Boolean = False; pKeyField: String = ''; pRequiredFld: String=''): Boolean; overload;
    procedure DisConnectToExcel;
  end;

var
  FrmImportExcelToTable: TFrmImportExcelToTable;

implementation

uses
	Windows, islib, isStr, isMsg, IniFiles, isDb, isFile, isCommon,
    ImportExcelRev;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    QrFields.Close;
//	Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.GetIniFile: String;
var
    mFileHandle: Integer;
    s: String;
begin
    if mIniFile = '' then
    begin
        s := isGetComputerName;
        mIniFile := sysAppPath + '\Temp\excelconfig' + s + '.ini';
        mFileHandle := FileOpen(mIniFile, fmOpenWrite or fmShareDenyNone);
        if mFileHandle < 1 then
        begin
            mIniFile := sysAppPath + 'Temp\excelconfig' + s + '.ini';
            if not FileExists(mIniFile) then
            begin
                with TMemIniFile.Create(mIniFile, TEncoding.UTF8) do
                begin
                    WriteString('COMMON', 'Export', '');
                    UpdateFile;
                    Free;
                end;
            end;
        end
        else
            CloseHandle(mFileHandle);
    end;

    Result := mIniFile;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.CmdImportExecute(Sender: TObject);
var
    i: Integer;
begin
    if mRequired then
    begin
        with QrFields do
        for i := 0 to FieldCount - 1 do
                if BlankConfirm(Fields[i]) then
                    Abort;
    end
    else
        if mKeyField <> '' then
            if BlankConfirm(QrFields, mRequiredFld) then
                Abort;

    if not CheckField(False) then
        Exit;

    try
        with TMemIniFile.Create(GetIniFile, TEncoding.UTF8) do
        begin
            EraseSection(mSection);
            for i := 0 to QrFields.FieldCount - 1 do
                WriteString(mSection, QrFields.Fields[i].FieldName, QrFields.Fields[i].AsString);
            UpdateFile;
            Free;
        end;
    except
    end;

    with Inspect do
        for i := 0 to Items.Count - 1 do
            with Items[i] do
            begin
                if Field.AsString <> '' then
                begin
                    if Tag = 1 then
                        QrExcel.FieldByName(Field.AsString).Tag := 1
                    else
                        QrExcel.FieldByName(Field.AsString).Tag := 4;
                    QrExcel.FieldByName(Field.AsString).DisplayLabel := Field.FieldName;
                end;
            end;


    with QrExcel do
    begin
        i := FieldCount;
        while i > 0 do
        begin
            dec(i);
            if Fields[i].Tag = 0 then
                Fields.Remove(Fields[i]);
        end;
    end;

    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.CheckField;
var
    i: Integer;
begin
    Result := False;
    with Inspect do
    begin
        for i := 0 to Items.Count - 1 do
        with Items.Items[i] do
            if not ReadOnly then
            begin
                PickList.Items.Clear;
                PickList.Items := cbFields.Items;

                if pAuto and (cbFields.Items.IndexOf(DataField) >= 0) then
                    Field.Value := DataField
                else if (Field.AsString <> '')  and (cbFields.Items.IndexOf(Field.AsString) < 0) then
                    Field.Clear;

                if not Result then
                    Result := cbFields.Items.IndexOf(Field.AsString) >= 0;
            end;
        Invalidate;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.CmdRefreshExecute(Sender: TObject);
var
    sSheet: string;
begin
    if not Conn.Connected then ConnectToExcel;
    if (cbSheet.Text = '') and (cbSheet.Items.Count > 0) then
        cbSheet.ItemIndex := 1;

    sSheet := cbSheet.Items[cbSheet.ItemIndex];
    if sSheet = '' then
        Exit;
    cbFields.Items.Clear;
    with QrExcel do
    begin
        Close;
        SQL.Text := 'SELECT * FROM [' + sSheet + ']';
        try
            Open;
        except
            ErrMsg('Unable to read data from Excel.');
            raise;
        end;
        GetFieldNames(cbFields.Items);
    end;
    CheckField;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.CmdReviewExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmImportExcelRev, FrmImportExcelRev);
    FrmImportExcelRev.Execute(QrExcel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.ConfigInspector(pFieldNames: String): Integer;
var
	ls: TStrings;
begin
    ls := TStringList.Create;
    isStrBreak(pFieldNames, ';', ls);
    Result := ConfigInspector(ls);
    ls.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.ConfigInspector(pFieldNames: TStrings): Integer;
var
    i, ret: Integer;
    fld: TField;
    it: TwwInspectorItem;
    s: String;
begin
    Inspect.Items.Clear;
    ret := 0;
    with QrFields do
    begin
        if Active then Close;
        FieldDefs.Clear;
    end;

    s := mKeyField + ';';
    // Update list of fields
    for i := 0 to pFieldNames.Count - 1 do
        with Inspect do
        begin
            fld := mDataSet.FindField(pFieldNames[i]);
            if fld <> Nil then
            begin
                QrFields.FieldDefs.Add(fld.FieldName, ftWideString, 255);

                it := Items.Add;
                it.Caption := fld.DisplayLabel;
                it.DataField := fld.FieldName;
                it.Index := i;

                if (fld.ReadOnly or not (fld.FieldKind in [fkData])) or (Pos(fld.FieldName+';', s) > 0) then
                    it.Tag := 1;
//                    it.ReadOnly := True;
                inc(ret);
            end;
        end;

    with QrFields do
    begin
        Open;
        Append;
    end;

    with TMemIniFile.Create(GetIniFile, TEncoding.UTF8) do
    begin
        for i := 0 to QrFields.FieldCount - 1 do
        begin
    	    QrFields.Fields[i].Value := ReadString(mSection, QrFields.Fields[i].FieldName, '');
            QrFields.Fields[i].DisplayLabel := Inspect.Items[i].Caption;
        end;

        Free;
    end;
    Result := ret;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.ConnectToExcel;
var
    strConn :  widestring;
begin
    if SameText(ExtractFileExt(mFileName), '.XLS') then
        strConn:='Provider=Microsoft.Jet.OLEDB.4.0;' +
           'Data Source="%s";' +
           'Extended Properties=Excel 8.0;'
    else
        strConn := 'Provider=Microsoft.ACE.OLEDB.12.0;'
            + 'Data Source="%s";'
            + 'Extended Properties="Excel 12.0 Xml;HDR=YES;IMEX=1";';

    strConn := Format(strConn, [mFileName]);

    Conn.Connected:=False;
    Conn.ConnectionString:=strConn;
    try
        Conn.Open;
        Conn.GetTableNames(cbSheet.Items,True);
        if cbSheet.Items.Count > 0 then
            cbSheet.ItemIndex := (cbSheet.Items.Count - 1);
    except
        on E: Exception do
            ErrMsg(E.Message)
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.DisConnectToExcel;
begin
    try
    QrExcel.Close;
    Conn.Close;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.ExcelImport(const pProfileSection, pSqlTable,
  pSqlProc: String; pProcParamValues: array of Variant;
  pTransNo: Integer): Boolean;
var
    mLastPath: String;
    mFieldsName: TStrings;
    mCount: Integer;
    b: Boolean;

        (*
        ** Excel --> SQL
        *)
    procedure Proc1();
    var
        i, n: Integer;
    begin
        with QrExcel do
        begin
            n := FieldCount;
            while not eof do
            begin
                QrIMPORT.Append;
                for i := 0 to n - 1 do
                    if Fields[i].DisplayLabel <> '' then
                    begin
                        with QrIMPORT.FieldByName(Fields[i].DisplayLabel) do
                        begin

                            case DataType of
                            ftWideString, ftString:
                                Value := isLeft(Fields[i].AsString, Size);
                            else
                                Value := Fields[i].Value;
                            end;

                            DisplayLabel := Fields[i].FieldName;
                        end;
                    end;

                if pTransNo <> 0 then
                    QrIMPORT.FieldByName('TransNo').AsInteger := pTransNo;

                QrIMPORT.CheckBrowseMode;
                Next;
            end;
        end;
    end;

        (*
        ** Process SQL
        *)
    function Proc2: Integer;
    var
        n: Integer;
        mParam: TParameter;
    begin
        Wait(PROCESSING);
        try
            with SP_PROCESS do
            begin
                CommandType := cmdStoredProc;
                CommandText := pSqlProc;
                Parameters.Refresh;
                n := 0;
                // Set parameters
                while n < Length(pProcParamValues) do
                begin
                    Parameters[n + 1].Value := pProcParamValues[n];
                    Inc(n);
                end;

                if pTransNo > 0 then
                    Parameters.FindParam('@TRANSNO').Value := pTransNo;
                mParam := Parameters.FindParam('@TRANSNO');
                if mParam <> nil then
                    mParam.Value := pTransNo;
                mParam := Parameters.FindParam('@UID');
                if mParam <> nil then
                    mParam.Value := sysLogonUID;
                try
                    Execute;
                except
                    on E: Exception do
                        ErrMsg(E.Message);
                end;
                Result := Parameters[0].Value;
            end;
        finally
            ClearWait;
        end;
    end;

        (*
        ** SQL --> Excel as log
        *)
    procedure Proc3;
    var
        sLogFile: String;
    begin
        sLogFile := ExtractFilePath(mFileName) +'Temp\' + ChangeFileExt(ExtractFileName(mFileName), '') + '_log.xls';
        DeleteFile(PChar(sLogFile));

        // Add data
        with QrIMPORT do
        begin
            if not IsEmpty then
            begin
                XLSReadWriteII41.Clear;
                with XLSDbRead41 do
                begin
                    XLSDbRead41.Dataset := QrIMPORT;
                    IncludeFields.Clear;
                    IncludeFields.AddStrings(mFieldsName);
                    IncludeFields.Add('ErrCode');
                    Read;
                end;
                XLSReadWriteII41.Filename := sLogFile;
                XLSReadWriteII41.Write;
                ShellExecute(0, 'Open', PChar(sLogFile), nil,  nil, SW_NORMAL)
            end;
//                ExportDataToFile(sLogFile, mFieldsName, QrIMPORT, 'Sheet1', 'Write Log', 'ErrCode');
        end;
    end;

    procedure GetListFields;
    begin
        with TADOQuery.Create(nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text := 'select [FIELD] from SYS_DICTIONARY d where isnull(VISIBLE,0)=1 and d.[TABLE] =:TABLE';
            SQL.Add(' and (isnull(d.[DESCRIPTION], '''') <> '''' or isnull(d.DESCRIPTION1, '''') <> '''')');
            SQL.Add(' order by d.FILTER');
            Parameters[0].Value := pProfileSection;
            Open;
            while not eof do
            begin
                mFieldsName.Add(Fields[0].AsString);
                QrIMPORT.FieldByName(Fields[0].AsString).Index := RecNo;
                Next;
            end;
            Close;
            Free;
        end;
    end;
begin
    Result := False;
    mLastPath := RegReadString(Name, 'FilePath');
    mFileName := isGetOpenFileName('XLSX;XLS', 1, mLastPath);
    if mFileName = '' then
        Exit;
    RegWrite(Name, 'FilePath', ExtractFilePath(mFileName));

    b := True;
    if pSqlProc <> '' then
        with SP_PROCESS do
        begin
            CommandType := cmdStoredProc;
            CommandText := pSqlProc;
            Parameters.Refresh;
            b := Parameters.FindParam('@TRANSNO') <> nil;
        end;

    // Clean up
    if b and (pTransNo = 0) then
        pTransNo := DataMain.GetSeqValue(pSqlTable);

    if pTransNo > 0 then
    	DataMain.Conn.Execute('delete ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo))
    else
    	DataMain.Conn.Execute('delete ' + pSqlTable);

    with QrIMPORT do
    begin
		// Excel --> SQL
        if pTransNo > 0 then
        	SQL.Text := 'select * from ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo)
        else
    		SQL.Text := 'select * from ' + pSqlTable;
        Open;
        SetDictionary(QrIMPORT, pProfileSection);
        mFieldsName := TStringList.Create;
        GetListFields;
        if mFieldsName.Count < 1 then
        begin
            ErrMsg(Format('Lỗi cấu hình "%s"', [pProfileSection]));
            mFieldsName.Free;
            Close;
            Exit;
        end;

        if Execute(mFileName, pProfileSection, QrIMPORT, mFieldsName) then
        begin
            //Excel --> SQL
            Proc1;
            DisConnectToExcel;

            // Process SQL
            mCount := Proc2;
            Requery;

            // SQL --> Excel Log
            Proc3;

            if mCount > 0 then
                Result := True;
            Msg(Format('%d Record affected.',[mCount]));
        end
        else
            DisConnectToExcel;
        Close;
        mFieldsName.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.Execute(const pSec: String; pDataSet: TDataSet;
  pStr: String; pRequired: Boolean; pKeyField, pRequiredFld: String): Boolean;
var
    mLastPath: String;
begin
    Result := False;
    mLastPath := RegReadString(Name, 'FilePath');
    mFileName := isGetOpenFileName('XLSX;XLS', 1, mLastPath);
    if mFileName = '' then
        Exit;
    RegWrite(Name, 'FilePath', mLastPath);
    Result := Execute(mFileName, pSec, pDataSet, pStr, pRequired, pKeyField, pRequiredFld);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.Execute(const pFile, pSec: String; pDataSet: TDataSet;
  pLstField: TStrings; pRequired: Boolean; pKeyField, pRequiredFld: String): Boolean;
var
	n, h: Integer;
begin
    QrExcel.Close;
	mDataSet := pDataSet;
    mSection := pSec;
    mRequired := pRequired;
    mKeyField := pKeyField;
    if pRequiredFld <> '' then
        mRequiredFld := pRequiredFld
    else
        mRequiredFld := pKeyField;

    mFileName := pFile;
    ConnectToExcel;

	n := ConfigInspector(pLstField) + 1;

    h := Inspect.Top + (n * 24) + 20;
    if h > 600 then
        h := 600;
    Self.Height := h;
    Result := ShowModal = mrOk;
    if not Result then
        DisConnectToExcel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.ExportDataToFile(pFile: String; pLstField: TStrings;
  pDB: TCustomADODataSet; pSheet, pDesc, pErrField: String; pOveride, pAddTitle,
  pOpen: Boolean; p1StDataRow: Integer);
var
    i: Integer;
begin
    // Initial
    with pDB do
	    for i := 0 to FieldCount - 1 do
        	Fields[i].Visible := False;
    if pLstField.Names[0] = '' then
        for i := 0 to pLstField.Count - 1 do
            with pDB.FieldByName(pLstField[i]) do
            begin
                Visible := True;
                Index := i;
            end
    else
        for i := 0 to pLstField.Count - 1 do
            with pDB.FieldByName(pLstField.Names[i]) do
            begin
                Visible := True;
                Index := i;
            end;

	if pErrField <> '' then
    	pDB.FieldByName(pErrField).Visible := True;

    with ExToExcel do
    begin
        FileName := pFile;
        Columns.Clear;
        KeyGenerator := pSheet;
        StartRow := max(p1StDataRow - Iif(pAddTitle, 1, 0), 0);
        AddTitle := pAddTitle;
        ActionAfterExport := Iif(pOpen, aeOpenView, aeNone);

        if pOveride then
        begin
            Options := Options - [soMergeData];
            DeleteDefaultSheets := True;
        end
        else
        begin
        	Options := Options + [soMergeData];
            DeleteDefaultSheets := False;
        end;

        DataSet := pDB;
		StartProgress(pDB.RecordCount, 'Exporting');
    	SetProgressDesc(pDesc);
        Execute;
        StopProgress;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.ExToExcelGetCellParams(Sender: TObject; Field: TField;
  var Text: WideString; AFont: TFont; var Alignment: TAlignment;
  var Background: TColor; var CellType: TCellType);
begin
	if ExToExcel.AddTitle and not Assigned(Field) and
    	(TSMExportToExcel(Sender).Statistic.CurrentRow = 0) then
	begin
		//AFont.Size := 10;
        AFont.Style := AFont.Style + [fsBold];
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.ExToExcelProgress(Sender: TObject; CurValue,
  MaxValue: Integer; var Abort: Boolean);
begin
    SetProgress(CurValue);
    if CurValue = MaxValue then
        ShowProgress(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelToTable.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    b: Boolean;
begin
    with QrExcel do
        b := Active and (not IsEmpty);

    CmdReview.Enabled := b;
    CmdImport.Enabled := b;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.Execute(const pFile, pSec: String; pDataSet: TDataSet;
        pStr: String; pRequired: Boolean; pKeyField, pRequiredFld: String): Boolean;
var
	ls: TStrings;
begin
    if pStr = '' then
        pStr := FlexConfigString(pSec, 'Import Excel Fields');

    Result := False;
    if pStr = '' then
    begin
        ErrMsg(Format('Lỗi cấu hình "%s"', [pSec]));
        ModalResult := mrAbort;
        Exit;
    end;
    ls := TStringList.Create;

    isStrBreak(pStr, ';', ls);
    try
        Result := Execute(pFile, pSec, pDataSet, ls, pRequired, pKeyField, pRequiredFld);
    finally
        ls.Free;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmImportExcelToTable.ExcelImportToTable(const pProfileSection, pSqlTable,
  pSqlProc: String; pProcParamValues: array of Variant;
  pTransNo: Integer): Boolean;
var
    s, mLastPath: String;
    mFieldsName: TStrings;
    mCount: Integer;
    fld: TField;
    b: Boolean;
        (*
        ** Excel --> SQL
        *)
    procedure Proc1();
    var
        i, n: Integer;
        sText: string;
        Matches: TMatchCollection;
        RegEx: TRegEx;
    begin

        with QrExcel do
        begin
            n := FieldCount;
            while not eof do
            begin
                try
                    QrIMPORT.Append;

                    if pTransNo <> 0 then
                        QrIMPORT.FieldByName('TransNo').AsInteger := pTransNo;

                    for i := 0 to n - 1 do
                        if Fields[i].DisplayLabel <> '' then
                        begin
                            with QrIMPORT.FieldByName(Fields[i].DisplayLabel) do
                            begin
                                sText := '';
                                Matches := TRegEx.Matches(Fields[i].AsString, '\(([^()]*)\)[^()]*$');
                                if(Matches.Count > 0) then
                                begin
                                    sText := Matches[0].Value;
                                    sText := isStrReplace(sText, '(', '');
                                    sText := isStrReplace(sText, ')', '');
                                end;

                                if DataType in [ftWideString, ftString] then
                                begin
                                    if(sText <> '') then
                                        Value := isLeft(sText, Size)
                                    else
                                        Value := isLeft(Fields[i].AsString, Size);
                                end
                                else
                                begin
                                    if(sText <> '') then
                                        Value := sText
                                    else
                                        Value := Fields[i].Value;
                                end;
                                DisplayLabel := Fields[i].FieldName;
                            end;
                        end;
                    QrIMPORT.CheckBrowseMode;
                except
                end;
                Next;
            end;
        end;
    end;

    (*
        ** Process SQL
        *)
    function Proc2: Integer;
    var
        n: Integer;
        mParam: TParameter;
    begin
        Wait(PROCESSING);
        try
            with SP_PROCESS do
            begin
                CommandType := cmdStoredProc;
                CommandText := pSqlProc;
                Parameters.Refresh;
                n := 0;
                // Set parameters
                while n < Length(pProcParamValues) do
                begin
                    Parameters[n + 1].Value := pProcParamValues[n];
                    Inc(n);
                end;

                try
                    Execute;
                except
                    on E: Exception do
                        ErrMsg(E.Message);
                end;
                s := Parameters[6].Value;
                Result := Parameters[0].Value;
            end;
        finally
            ClearWait;
        end;
    end;


    procedure GetListFields;
    begin
        with TADOQuery.Create(nil) do
        begin
            Connection := DataMain.Conn;
            LockType := ltReadOnly;
            SQL.Text := 'select [FIELD] from SYS_DICTIONARY d where isnull(VISIBLE,0)=1 and d.[TABLE] =:TABLE';
            SQL.Add(' and (isnull(d.[DESCRIPTION], '''') <> '''' or isnull(d.DESCRIPTION1, '''') <> '''')');
            SQL.Add(' order by d.FILTER');
            Parameters[0].Value := pProfileSection;
            Open;
            while not eof do
            begin
                mFieldsName.Add(Fields[0].AsString);
                QrIMPORT.FieldByName(Fields[0].AsString).Index := RecNo;
                Next;
            end;
            Close;
            Free;
        end;
    end;
begin
    Result := False;
    mLastPath := RegReadString(Name, 'FilePath');
    mFileName := isGetOpenFileName('XLSX;XLS', 1, mLastPath);
    if mFileName = '' then
        Exit;
    RegWrite(Name, 'FilePath', ExtractFilePath(mFileName));

    b := True;
    if pSqlProc <> '' then
        with SP_PROCESS do
        begin
            CommandType := cmdStoredProc;
            CommandText := pSqlProc;
            Parameters.Refresh;
            b := Parameters.FindParam('@TRANSNO') <> nil;
        end;

    // Clean up
    if b and (pTransNo = 0) then
        pTransNo := DataMain.GetSeqValue(pSqlTable);

    if pTransNo > 0 then
    	DataMain.Conn.Execute('delete ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo))
    else
    	DataMain.Conn.Execute('delete ' + pSqlTable);



    with QrIMPORT do
    begin
		// Excel --> SQL
        if pTransNo > 0 then
        	SQL.Text := 'select * from ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo)
        else
    		SQL.Text := 'select * from ' + pSqlTable;
        Open;
        SetDictionary(QrIMPORT, pProfileSection);
        mFieldsName := TStringList.Create;
        GetListFields;
        if mFieldsName.Count < 1 then
        begin
            ErrMsg(Format('Lỗi cấu hình "%s"', [pProfileSection]));
            mFieldsName.Free;
            Close;
            Exit;
        end;

        if Execute(mFileName, pProfileSection, QrIMPORT, mFieldsName) then
        begin

            //Excel --> SQL
            Proc1;
            DisConnectToExcel;

             // Process SQL
            mCount := Proc2;

            if mCount >= 0 then
                Result := True
            else
                ErrMsg(s);

        end
        else
            DisConnectToExcel;
        Close;
        mFieldsName.Free;
    end;
end;

end.
