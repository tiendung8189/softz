object FrmHosonvCCCD: TFrmHosonvCCCD
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7891' S'#417' Nh'#226'n Vi'#234'n - Danh S'#225'ch CCCD/ CMND'
  ClientHeight = 492
  ClientWidth = 1008
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1008
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton10: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 471
    Width = 1008
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 541
    Top = 77
    Width = 467
    Height = 394
    Align = alRight
    BorderOuter = fsLowered
    HotSpotHighlight = 11855600
    HotSpotIgnoreMargins = True
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 2
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    ExplicitTop = 36
    ExplicitHeight = 435
    object PD1: TisPanel
      Left = 10
      Top = 2
      Width = 455
      Height = 102
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = ' :: Th'#244'ng tin'
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      DesignSize = (
        455
        102)
      object Label57: TLabel
        Left = 288
        Top = 24
        Width = 52
        Height = 16
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#224'y c'#7845'p'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 268
      end
      object EdMA: TDBEditEh
        Tag = 1
        Left = 114
        Top = 22
        Width = 154
        Height = 22
        AutoSize = False
        BevelKind = bkFlat
        BorderStyle = bsNone
        CharCase = ecUpperCase
        ControlLabel.Width = 94
        ControlLabel.Height = 16
        ControlLabel.Caption = 'S'#7889' CCCD/ CMND'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'MaSo'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <>
        EmptyDataInfo.Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 0
        Visible = True
      end
      object DBEditEh4: TDBEditEh
        Left = 369
        Top = 70
        Width = 77
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clBtnFace
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'LK_NoiCap_Ma'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        Visible = True
      end
      object wwDBDateTimePicker16: TwwDBDateTimePicker
        Left = 345
        Top = 21
        Width = 101
        Height = 22
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NgayCap'
        DataSource = DsDanhmuc
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = True
        TabOrder = 1
      end
      object CbNoiCap: TDbLookupComboboxEh2
        Left = 114
        Top = 70
        Width = 252
        Height = 22
        ControlLabel.Width = 42
        ControlLabel.Height = 16
        ControlLabel.Caption = 'N'#417'i c'#7845'p'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        Anchors = [akLeft, akTop, akRight]
        DynProps = <>
        DataField = 'MaNoiCap'
        DataSource = DsDanhmuc
        DropDownBox.Columns = <
          item
            FieldName = 'TEN_HOTRO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 200
          end
          item
            FieldName = 'MA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'M'#227
            Width = 40
          end>
        DropDownBox.ListSource = HrDataMain.DsV_CCCD_NOICAP
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        EmptyDataInfo.Color = clInfoBk
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MA_HOTRO'
        ListField = 'TEN_HOTRO'
        ListSource = HrDataMain.DsV_CCCD_NOICAP
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 3
        Visible = True
      end
      object CbGiayToLoai: TDbLookupComboboxEh2
        Left = 114
        Top = 46
        Width = 154
        Height = 22
        ControlLabel.Width = 102
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Lo'#7841'i CCCD/ CMND'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        DynProps = <>
        DataField = 'GiayTo_Nhom'
        DataSource = DsDanhmuc
        DropDownBox.Columns = <
          item
            FieldName = 'TEN_HOTRO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 200
          end>
        DropDownBox.ListSource = HrDataMain.DsV_HR_CCCD_NHOM
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        EmptyDataInfo.Color = clInfoBk
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MA_HOTRO'
        ListField = 'TEN_HOTRO'
        ListSource = HrDataMain.DsV_HR_CCCD_NHOM
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 2
        Visible = True
      end
    end
    object PD2: TisPanel
      Left = 10
      Top = 179
      Width = 455
      Height = 213
      Align = alClient
      Color = 16119285
      ParentBackground = False
      TabOrder = 2
      HeaderCaption = ' .: Ghi ch'#250
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 99
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      ExplicitHeight = 254
      object DBMemo1: TDBMemo
        Left = 1
        Top = 17
        Width = 453
        Height = 195
        Align = alClient
        BorderStyle = bsNone
        DataField = 'GHICHU'
        DataSource = DsDanhmuc
        TabOrder = 1
        ExplicitHeight = 236
      end
    end
    object PaDinhKem: TisPanel
      Left = 10
      Top = 104
      Width = 455
      Height = 75
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 1
      HeaderCaption = ' .: File '#273#237'nh k'#232'm'
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 0
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      DesignSize = (
        455
        75)
      object EdFileAttach: TDBEditEh
        Tag = 1
        Left = 114
        Top = 22
        Width = 332
        Height = 22
        TabStop = False
        Alignment = taLeftJustify
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clBtnFace
        ControlLabel.Width = 76
        ControlLabel.Height = 16
        ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'Calc_FileName'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <
          item
            Action = CmdFilePlus
            Images.NormalImages = DataMain.ImageEditButton
            Images.HotImages = DataMain.ImageEditButton
            Images.PressedImages = DataMain.ImageEditButton
            Images.DisabledImages = DataMain.ImageEditButton
            Style = ebsGlyphEh
            Width = 30
            DrawBackTime = edbtWhenHotEh
          end
          item
            Action = CmdFileMinus
            Images.NormalImages = DataMain.ImageEditButton
            Images.HotImages = DataMain.ImageEditButton
            Images.PressedImages = DataMain.ImageEditButton
            Images.DisabledImages = DataMain.ImageEditButton
            Images.NormalIndex = 1
            Images.HotIndex = 1
            Images.PressedIndex = 1
            Images.DisabledIndex = 1
            Style = ebsGlyphEh
            Width = 30
            DrawBackTime = edbtWhenHotEh
          end
          item
            Action = CmdFileView
            Images.NormalImages = DataMain.ImageEditButton
            Images.HotImages = DataMain.ImageEditButton
            Images.PressedImages = DataMain.ImageEditButton
            Images.DisabledImages = DataMain.ImageEditButton
            Images.NormalIndex = 2
            Images.HotIndex = 2
            Images.PressedIndex = 2
            Images.DisabledIndex = 2
            Style = ebsGlyphEh
            Width = 30
            DrawBackTime = edbtWhenHotEh
          end>
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        Visible = True
      end
      object DBEdit6: TDBEditEh
        Left = 114
        Top = 46
        Width = 332
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 50
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Url cloud'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'Link'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <
          item
            Action = CmdOpenLink
            DefaultAction = False
            Style = ebsEllipsisEh
            Width = 20
            DrawBackTime = edbtWhenHotEh
          end>
        ParentCtl3D = False
        ShowHint = True
        TabOrder = 1
        Visible = True
      end
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 77
    Width = 541
    Height = 394
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MaSo'#9'11'#9'CCCD/CMND'#9'F'#9'S'#7889
      'NgayCap'#9'11'#9'Ng'#224'y c'#7845'p'#9'F'
      'LK_TenNoiCap'#9'30'#9'N'#417'i c'#7845'p'#9'F'
      'GhiChu'#9'20'#9'Ghi ch'#250#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDanhmuc
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
    ExplicitTop = 36
    ExplicitHeight = 435
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 36
    Width = 1008
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    ExplicitLeft = -236
    ExplicitTop = 36
    ExplicitWidth = 1244
    inherited Panel1: TPanel
      Width = 1008
      ExplicitWidth = 1244
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdOpenLink: TAction
      Caption = 'M'#7903' li'#234'n k'#7871't'
      OnExecute = CmdOpenLinkExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 196
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = QrDanhmucBeforeOpen
    BeforeInsert = QrDanhmucBeforeInsert
    AfterInsert = QrDanhmucAfterInsert
    BeforePost = QrDanhmucBeforePost
    AfterPost = QrDanhmucAfterPost
    BeforeDelete = QrDanhmucBeforeDelete
    AfterDelete = QrDanhmucAfterDelete
    OnCalcFields = QrDanhmucCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from HR_DM_NHANVIEN_GIAYTO'
      'where Manv =:Manv'
      'and GiayTo_Loai=1'
      'order by NgayCap')
    Left = 148
    Top = 196
    object QrDanhmucManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrDanhmucMaSo: TWideStringField
      FieldName = 'MaSo'
    end
    object QrDanhmucMaNoiCap: TWideStringField
      FieldName = 'MaNoiCap'
      Size = 70
    end
    object QrDanhmucNoiCap: TWideStringField
      FieldName = 'NoiCap'
      Size = 200
    end
    object QrDanhmucNgayCap: TDateTimeField
      FieldName = 'NgayCap'
    end
    object QrDanhmucNgayHetHan: TDateTimeField
      FieldName = 'NgayHetHan'
    end
    object QrDanhmucGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDanhmucLK_TenNoiCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNoiCap'
      LookupDataSet = HrDataMain.QrV_HR_CCCD_NOICAP
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'MaNoiCap'
      Lookup = True
    end
    object QrDanhmucLK_NoiCap_Ma: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NoiCap_Ma'
      LookupDataSet = HrDataMain.QrV_HR_CCCD_NOICAP
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'MA'
      KeyFields = 'MaNoiCap'
      Lookup = True
    end
    object QrDanhmucGiayTo_Loai: TIntegerField
      FieldName = 'GiayTo_Loai'
    end
    object QrDanhmucGiayTo_Nhom: TWideStringField
      FieldName = 'GiayTo_Nhom'
      Size = 70
    end
    object QrDanhmucLK_GiayTo_TenNhom: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_GiayTo_TenNhom'
      LookupDataSet = HrDataMain.QrV_HR_CCCD_NHOM
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'GiayTo_Nhom'
      Lookup = True
    end
    object QrDanhmucFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
    object QrDanhmucFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrDanhmucFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrDanhmucLink: TWideStringField
      FieldName = 'Link'
      OnValidate = QrDanhmucLinkValidate
      Size = 300
    end
    object QrDanhmucCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Calculated = True
    end
    object QrDanhmucCacl_FileExt: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cacl_FileExt'
      Calculated = True
    end
    object QrDanhmucFileDesc: TWideStringField
      FieldName = 'FileDesc'
      Size = 200
    end
    object QrDanhmucIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 148
    Top = 224
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 148
    Top = 256
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 212
    Top = 224
  end
  object spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap: TADOCommand
    CommandText = 'spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NgayCap'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@GiayTo_Loai'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 180
    Top = 116
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'Idx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :Idx')
    Left = 324
    Top = 244
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
end
