﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PayNet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, Grids, wwDataInspector, ActnList,
  ImgList, ComCtrls, ToolWin, OleCtrls, SHDocVw_EWB, EwbCore, EmbeddedWB, Menus,
  AdvMenus, IEAddress, MSHTML, IniFiles, kbmMemTable, wwdblook;

type
  TFrmPayNet = class(TForm)
    Panel2: TPanel;
    RemuWorking: TwwDataInspector;
    PS_PAYNET_MASTER: TADOStoredProc;
    DsList: TDataSource;
    PS_PAYNET_DETAIL: TADOStoredProc;
    ActionList1: TActionList;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolBtnRefresh: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdFillData: TAction;
    QrParam: TADOQuery;
    DsParam: TDataSource;
    QrParamPAYNET_VALUE: TDateTimeField;
    QrParamPAYNET_DATE: TDateTimeField;
    QrTemp: TkbmMemTable;
    QrTempPNET_EMP_TYPE: TWideStringField;
    QrTempLK_PNET_EMP_TYPE: TWideStringField;
    DsTemp: TDataSource;
    SaveDlg: TSaveDialog;
    CbEmpType2: TwwDBLookupCombo;
    QrTempPNET_BANK_CODE: TWideStringField;
    CbBank: TwwDBLookupCombo;
    QrTempPNET_REFERENCE: TWideStringField;
    CmdPrint: TAction;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure emBrowserWindowClosing(ASender: TObject; IsChildWindow: WordBool;
      var Cancel: WordBool);
    procedure emBrowserCommandStateChange(ASender: TObject; Command: Integer;
      Enable: WordBool);
    procedure CmdFillDataExecute(Sender: TObject);
    procedure PS_PAYNET_MASTERAfterOpen(DataSet: TDataSet);
    procedure OpenMaster(pYear, pMon: Integer; pEmpType, pBankCode, pRef: String; pLoai: Integer);
    procedure QrTempPNET_EMP_TYPEChange(Sender: TField);
    procedure CmdPrintExecute(Sender: TObject);
  private
    mYear, mMon: Integer;
    mStr: String;
    mInputLst: TStrings;
    mInputFileIndex: Integer;
    mFileStream: TFileStream;
    mKeepFile: Boolean;

    function FillFormData(pFormName: string): Boolean;

    function ExportDataToFile(): string;
    procedure DeleteIFile(pFile: String);
  public
    procedure Execute(pYear, pMon: Integer; pStr: string = '');
  end;

var
  FrmPayNet: TFrmPayNet;

implementation

uses
	Vlib, vlMsg, vlStr, ExCommon, MainData, vlCommon, vlFile, vlCrpe,
    ShellApi, vlDb;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NOT_DATA = 'Không có dữ liệu.';
    RS_COUNT_DATA = 'Có %d dòng dữ liệu.';

procedure TFrmPayNet.CmdCloseExecute(Sender: TObject);
begin
    ModalResult := mrClose;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.CmdFillDataExecute(Sender: TObject);
begin
    if BlankConfirm(QrParam.FieldByName('PAYNET_VALUE')) then
        Abort;
    QrParam.CheckBrowseMode;
    try
        FillFormData(mInputLst.Values['Form_Name']);
        DataMain.Conn.Execute(Format('update PAYROLL_PERIOD set PAYNET_DATE = getdate() where [year]=%d and [Month]=%d', [mYear, mMon]));
    finally
        ReSyncRecord(QrParam);
    end;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.CmdPrintExecute(Sender: TObject);
begin
    with QrTemp do
    begin
        ShowReport(Caption, 'PAY_NET',
            [mYear, mMon, 4, FieldByName('PNET_EMP_TYPE').AsString,
            FieldByName('PNET_BANK_CODE').AsString, FieldByName('PNET_REFERENCE').AsString])
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.DeleteIFile(pFile: String);
//var
//    FileOpStruc: TSHFileOpStruct;
begin
//    with FileOpStruc do
//    begin
//        Wnd := 0;
//        wFunc := FO_DELETE;
//        pFrom := PWideChar(pFile);
//        fFlags := (FOF_NOCONFIRMATION or FOF_SILENT);
//    end;
//    SHFileOperation(FileOpStruc);
    FileSetReadOnly(pFile, False);
    DeleteFile(pFile);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.emBrowserCommandStateChange(ASender: TObject; Command: Integer;
  Enable: WordBool);
begin
//    case Command of
//    CSC_NAVIGATEBACK:
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.emBrowserWindowClosing(ASender: TObject; IsChildWindow: WordBool;
  var Cancel: WordBool);
begin
    CmdClose.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.Execute(pYear, pMon: Integer; pStr: string);
begin
    mMon := pMon;
    mYear := pYear;
    mStr := pStr;
    Caption := Caption + ' - ' + IntToStr(mMon) + '/' + IntToStr(mYear);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPayNet.ExportDataToFile: string;
var
    s, mPath: String;
    ls: TStrings;
    i, n: Integer;
begin
    Result := '';
    with PS_PAYNET_DETAIL do
    begin
        Close;
        Parameters[1].Value := mYear;
        Parameters[2].Value := mMon;
        Parameters[3].Value := 4;
        Parameters[4].Value := mStr;
        Parameters[5].Value := QrTemp.FieldByName('PNET_EMP_TYPE').AsString;
        Parameters[6].Value := QrTemp.FieldByName('PNET_BANK_CODE').AsString;
        Open;

        if IsEmpty then
        begin
            ErrMsg(RS_NOT_DATA);
            Exit;
        end;
        n := RecordCount;
        if not YesNo(Format(RS_COUNT_DATA, [n]) + ' Tiếp tục?') then
            Exit;
    end;
    ls := TStringList.Create;
    with QrTemp do
        OpenMaster(mYear, mMon, FieldByName('PNET_EMP_TYPE').AsString,
            FieldByName('PNET_BANK_CODE').AsString, FieldByName('PNET_REFERENCE').AsString, 4);
    with PS_PAYNET_MASTER do
    begin
        s := Fields[4].AsString;
        i := 5;
        while i < FieldCount do
        begin
            if 'C_BR_' = Copy(Fields[i].FieldName, 0, 5) then
            begin
                ls.Add(s);
                if i + 1 < FieldCount then
                begin
                    s := Fields[i + 1].AsString;
                    inc(i);
                end;
            end
            else
                s := s + ',' + Fields[i].AsString;
            inc(i);
        end;
        ls.Add(s);
    end;

    with PS_PAYNET_DETAIL do
    begin
        First;
        n := FieldCount;
        while not Eof do
        begin
            s := Fields[0].AsString;
            i := 1;
            while i < n do
            begin
                s := s + ',' + Fields[i].AsString;
                inc(i);
            end;

            ls.Add(s);
            Next;
        end;
    end;
    if Assigned(mFileStream) then
    begin
        try
            s := mFileStream.FileName;
            mFileStream.Free;
            if not mKeepFile then
                DeleteIFile(s);
        except
        end;
    end;


    if mKeepFile then
    begin
        s := IncludeTrailingBackslash(sysTempPath) + PS_PAYNET_MASTER.FieldByName('IFILENAME').AsString;
        if (QrTemp.FieldByName('PNET_EMP_TYPE').AsString <> 'PER')then
            s := s + QrTemp.FieldByName('PNET_EMP_TYPE').AsString;
        s := s + '.txt';

        with SaveDlg do
        begin
            FileName := s;
            if not Execute then
                Exit;
            s := FileName;
        end;

        if s <> '' then
            mPath := ExtractFilePath(s);
        Application.ProcessMessages;
    end
    else
        s := IncludeTrailingBackslash(sysTempPath) + PS_PAYNET_MASTER.FieldByName('IFILENAME').AsString + '.txt';

    ls.SaveToFile(s);
    ls.Free;

    FileSetReadOnly(s, True);
    mFileStream := TFileStream.Create(s, fmOpenRead or fmShareDenyWrite);
    Result := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    C_FORM_BY_ACTION = 'ACTION=';
    C_FORM_BY_ITEM = 'ITEM=';
resourcestring
    RS_INVALID_WEB_FORM = 'Nội dung không hợp lệ.';
function TFrmPayNet.FillFormData(pFormName: string): Boolean;
var
    s:String;
    i: Integer;
begin
    Result := False;
    s := ExportDataToFile;
    if s = '' then
        Exit;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.FormClose(Sender: TObject; var Action: TCloseAction);
var
    s: String;
begin
    try
        QrParam.CheckBrowseMode;
        QrParam.Close;
        PS_PAYNET_MASTER.Close;
        PS_PAYNET_DETAIL.Close;
    except
    end;
    if Assigned(mFileStream) then
    begin
        try
            s := mFileStream.FileName;
            mFileStream.Free;
            if not mKeepFile then
                DeleteIFile(s);
        except
        end;
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.FormShow(Sender: TObject);
begin
    with DataMain do
    begin
        OpenDataSets([QrBank2, QrPNET_EMP_TYPE]);
        QrBank2.First;
    end;
    mKeepFile := not FlexConfigBool('Pay_Net', 'Remove File');
    with QrTemp do
    begin
        if not Active then
            Open;
        Edit;
        FieldByName('PNET_EMP_TYPE').AsString := 'PER';
        FieldByName('PNET_BANK_CODE').AsString := DataMain.QrBank2.FieldByName('Bank Code').AsString;
        FieldByName('PNET_REFERENCE').AsString := 'Salary BV';
        OpenMaster(mYear, mMon, FieldByName('PNET_EMP_TYPE').AsString,
            FieldByName('PNET_BANK_CODE').AsString, FieldByName('PNET_REFERENCE').AsString, 4);
    end;
    with QrParam do
    begin
        Parameters[0].Value := mYear;
        Parameters[1].Value := mMon;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.OpenMaster(pYear, pMon: Integer; pEmpType, pBankCode, pRef: String;
  pLoai: Integer);
begin
    with PS_PAYNET_MASTER do
    begin
        if Active then
            Close;
        Prepared := True;
        Parameters[1].Value := pYear;
        Parameters[2].Value := pMon;
        Parameters[3].Value := pLoai;
        Parameters[4].Value := pEmpType;
        Parameters[5].Value := pBankCode;
        Parameters[6].Value := pRef;
//        ExecProc;
//        Active := True;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.PS_PAYNET_MASTERAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(PS_PAYNET_MASTER, sysCurFmt);
    SetDisplayFormat(PS_PAYNET_MASTER, ['NO_OF_TRANS'], '#,##0.0;-#,##0.0;#');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.QrTempPNET_EMP_TYPEChange(Sender: TField);
begin
    with QrTemp do
    begin
        OpenMaster(mYear, mMon, FieldByName('PNET_EMP_TYPE').AsString,
            FieldByName('PNET_BANK_CODE').AsString, FieldByName('PNET_REFERENCE').AsString, 4);
//        FieldByName('PNET_REFERENCE').AsString := PS_PAYNET_MASTER.FieldByName('PNET_REFERENCE').AsString;
    end;
//    RemuWorking.Refresh;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPayNet.FormCreate(Sender: TObject);
var
    i: Integer;
begin
	TMyForm(Self).Init1(IDI_AIRPLANE);
    mInputLst := TStringList.Create;

    for i := 0 to mInputLst.Count - 1 do
        if mInputLst.ValueFromIndex[i] = '@FILE' then
        begin
            mInputFileIndex := i;
            Break;
        end;
end;

end.
