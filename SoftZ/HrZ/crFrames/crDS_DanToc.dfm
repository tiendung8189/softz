inherited FrameHR_DS_DanToc: TFrameHR_DS_DanToc
  Height = 74
  ExplicitHeight = 74
  object PaDS_NHOMVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    ExplicitHeight = 55
    object LbText: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 145
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch d'#226'n t'#7897'c >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbTextClick
    end
    object EdDSText: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 40
      DoubleBuffered = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 0
    end
  end
end
