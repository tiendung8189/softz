﻿unit crDS_LyDoThoiViec;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook, Vcl.Mask, DBCtrlsEh;

type
  TframeHR_DS_LyDoThoiViec = class(TCrFrame)
    PaBoard: TPanel;
    LbText: TLabel;
    EdDSText: TMemo;
    procedure LbTextClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  frameHR_DS_LyDoThoiViec: TframeHR_DS_LyDoThoiViec;

implementation
uses
    SysUtils, Excommon, isCommon, isDb, MainData, ChonDsLyDoThoiViec;

{$R *.dfm}

{ TframeHR_DS_LyDoThoiViec }

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TframeHR_DS_LyDoThoiViec.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := Trim(EdDSText.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DS_LyDoThoiViec.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_LyDoThoiViec.Init;
begin
  inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_LyDoThoiViec.LbTextClick(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsLyDoThoiViec.Get(s) then
    	Exit;

    with EdDSText do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
