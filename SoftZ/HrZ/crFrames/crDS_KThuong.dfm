inherited FrameHR_DS_KThuong: TFrameHR_DS_KThuong
  Height = 74
  ExplicitHeight = 74
  object PaDS_NHOMVT: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object LbText: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 205
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch l'#253' do khen th'#432#7903'ng >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = LbTextClick
    end
    object EdDSText: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 40
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
