﻿unit crDS_ChucDanh;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeHR_DS_ChucDanh = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameHR_DS_ChucDanh: TframeHR_DS_ChucDanh;

implementation

{$R *.dfm}

uses
    ChonDsChucDanh;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo chức vụ';
    RS_TIT1 = 'Theo chức danh';
    RS_TIT_CAP = '<< %s >>';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_ChucDanh.LbMaClick(Sender: TObject);
const
    TIT: array [0..1] of String = (
    	RS_TIT0,
        RS_TIT1
    );
var
    n: Integer;
    s: String;
begin
	n := EdMa.Tag;
    if not FrmChonDsChucDanh.Get(n, s) then
    	Exit;

    LbMa.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMa do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_ChucDanh.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := Trim(EdMa.Text);

    n := Length(cr) - GetParamNo;
    cr[n] := EdMa.Tag;
    inc(n);
    cr[n] := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DS_ChucDanh.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_ChucDanh.Init;
begin
  inherited;

end;

end.
