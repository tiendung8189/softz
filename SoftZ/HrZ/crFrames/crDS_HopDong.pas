﻿unit crDS_HopDong;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeHR_DS_HopDong = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameHR_DS_HopDong: TframeHR_DS_HopDong;

implementation

{$R *.dfm}

uses
    ChonDsHopDong;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_HopDong.LbMaClick(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsHopDong.Get(s) then
    	Exit;

    with EdMa do
    begin
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_HopDong.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := Trim(EdMa.Text);

    n := Length(cr) - GetParamNo;
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'HR_DS_HopDong' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DS_HopDong.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_HopDong.Init;
begin
  inherited;

end;

end.
