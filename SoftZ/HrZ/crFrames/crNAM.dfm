inherited frameNAM: TframeNAM
  Height = 49
  ExplicitHeight = 49
  object PaNAM: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label9: TLabel
      Left = 141
      Top = 16
      Width = 26
      Height = 16
      Alignment = taRightJustify
      Caption = 'N'#259'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbNam: TComboBox
      Left = 174
      Top = 12
      Width = 81
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
