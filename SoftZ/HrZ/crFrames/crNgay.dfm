inherited frameCrNgay: TframeCrNgay
  Height = 49
  ExplicitHeight = 49
  object PaNGAY: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label11: TLabel
      Left = 143
      Top = 16
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y'
    end
    object EdNgay: TwwDBDateTimePicker
      Left = 181
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
  end
end
