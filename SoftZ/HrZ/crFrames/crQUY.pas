﻿unit crQUY;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeQUY = class(TCrFrame)
    PaQuy: TPanel;
    TntLabel3: TLabel;
    TntLabel4: TLabel;
    CbQuy: TComboBox;
    CbNam: TComboBox;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameQUY: TframeQUY;

implementation

{$R *.dfm}
uses
    isStr;

{ TframeQUY }

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeQUY.GetCriteria(var cr: array of Variant);
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := StrToInt(CbQuy.Text);
    inc(n);
    cr[n] := StrToInt(CbNam.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeQUY.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeQUY.Init;
    procedure SetPeriodList(Sender : TComboBox);
    var
	i : Integer;
    yy, mm: WORD;
    begin
        with Sender do
        begin
            DecodeDate (Date, yy, mm, yy);

            Items.Clear;
            for i := 1 to 4 do
                Items.Add(isPadLeft(IntToStr(i), 2));

            if (mm >= 1) and (mm <= 3) then
                i := 0
            else if (mm >= 4) and (mm <= 6) then
                i := 1
            else if (mm >= 7) and (mm <= 9) then
                i := 2
            else
                i := 3;
            ItemIndex := i;
        end;
    end;
begin
    inherited;
    SetPeriodList(CbQuy);
    isYearList(CbNam);
end;

end.
