﻿unit crDoiTuong;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, wwdblook, Vcl.Mask, DBCtrlsEh;

type
  TframeHR_DoiTuong = class(TCrFrame)
    rgObsolete: TRadioGroup;
  private
  protected
    procedure Init; override;
  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetCode: Variant; override;
  end;

var
  frameHR_DoiTuong: TframeHR_DoiTuong;

implementation
uses
    SysUtils, Excommon, isCommon, isDb;

{$R *.dfm}

{ TframeHR_DoiTuong }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DoiTuong.GetCode: Variant;
begin
    Result := rgObsolete.ItemIndex;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DoiTuong.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr)- GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DoiTuong.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DoiTuong.Init;
begin
  inherited;
end;

end.
