inherited frameQUY: TframeQUY
  Height = 49
  ExplicitHeight = 49
  object PaQuy: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object TntLabel3: TLabel
      Left = 90
      Top = 16
      Width = 22
      Height = 16
      Alignment = taRightJustify
      Caption = 'Qu'#253
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TntLabel4: TLabel
      Left = 198
      Top = 16
      Width = 26
      Height = 16
      Alignment = taRightJustify
      Caption = 'N'#259'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbQuy: TComboBox
      Left = 120
      Top = 12
      Width = 53
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Items.Strings = (
        '1'
        '2'
        '3'
        '4')
    end
    object CbNam: TComboBox
      Left = 232
      Top = 12
      Width = 73
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
end
