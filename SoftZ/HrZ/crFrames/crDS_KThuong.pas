﻿unit crDS_KThuong;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TFrameHR_DS_KThuong = class(TCrFrame)
    PaDS_NHOMVT: TPanel;
    LbText: TLabel;
    EdDSText: TMemo;
    procedure LbTextClick(Sender: TObject);

  private
  protected
    procedure Init; override;

  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  FrameHR_DS_KThuong: TFrameHR_DS_KThuong;

implementation

{$R *.dfm}
    uses
    ChonDsKThuong;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrameHR_DS_KThuong.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameHR_DS_KThuong.LbTextClick(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsKThuong.Get(s) then
    	Exit;

    with EdDSText do
    begin
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
 procedure TFrameHR_DS_KThuong.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := Trim(EdDSText.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameHR_DS_KThuong.Init;
begin
  inherited;

end;

end.
