﻿unit crDS_NhanVien;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeHR_DS_NhanVien = class(TCrFrame)
    PaMAVT: TPanel;
    LbMa: TLabel;
    EdMa: TMemo;
    procedure LbMaClick(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameHR_DS_NhanVien: TframeHR_DS_NhanVien;

implementation

{$R *.dfm}

uses
    ChonDsNhanVien;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Theo phòng ban';
    RS_TIT1 = 'Theo bộ phận';
    RS_TIT2 = 'Danh sách nhân viên';
    RS_TIT_CAP = '<< %s >>';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_NhanVien.LbMaClick(Sender: TObject);
const
    TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT2
    );
var
    n: Integer;
    s: String;
begin
	n := EdMa.Tag;
    if not FrmChonDsNhanVien.Get(n, s) then
    	Exit;

    LbMa.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdMa do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_NhanVien.GetCriteria;
var
    n, n1, i: Integer;
    s: String;
begin
    inherited;
    s := EdMa.Text;

    n := Length(cr) - GetParamNo;
    cr[n] := EdMa.Tag;
    inc(n);
    cr[n] := s;

    n1 := Length(fieldList);
    for i := 0 to n1 - 1 do
    begin
        if fieldList[i] = 'DS_NV' then
            valuesList[i] := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeHR_DS_NhanVien.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeHR_DS_NhanVien.Init;
begin
  inherited;

end;

end.
