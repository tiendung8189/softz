﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExcelData;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, ShellApi, Windows,
  Graphics, XLSReadWriteII4, SheetData4, ExportDS, SME2Cell, SMEEngine, SME2OLE,
  SME2TXT, XLSFonts4, FormulaHandler4, BIFFRecsII4;

type
  TDataExcel = class(TDataModule)
    SP_PROCESS: TADOCommand;
    SP_EXPORT: TADOStoredProc;
    QrIMPORT: TADOQuery;
    QrCheck: TADOQuery;
    QrImport2: TADOQuery;
    XLSReadWrite: TXLSReadWriteII4;
    ExToExcel: TSMExportToExcel;
    ExToCSV: TSMExportToText;
    QrCheck2: TADOQuery;
    QrCheck3: TADOQuery;
    procedure ExToExcelGetCellParams(Sender: TObject; Field: TField;
      var Text: WideString; AFont: TFont; var Alignment: TAlignment;
      var Background: TColor; var CellType: TCellType);
    procedure ExToExcelProgress(Sender: TObject; CurValue, MaxValue: Integer;
      var Abort: Boolean);

  private
    function  RefToCell(RowID, ColID: Integer): string;
    procedure AddHeaderRow(pSheet: TSheet; pLstField: TStrings; pError: String = '');

  	procedure CopyDataToSheet(LstField: TStrings; pDb: TCustomADODataSet; pSheets: TSheets;
    	pDesc: String = 'Exporting'; pErrField: String = '');

    procedure Worksheet2DB(pWorksheet: TSheet; p1stDataRow: Integer;
    	LstField: TStrings; pDb: TCustomADODataSet; pTransNo: Integer = 0);
    procedure Worksheet2DB2(pWorksheet: TSheet; p1stDataRow: Integer;
    	LstField: TStrings; pDb: TCustomADODataSet;
        pKeyFeild: String = ''; dbCheck: TCustomADODataSet = Nil; pFile: String = '');
    procedure Worksheet2DB3(pWorksheet: TSheet; p1stDataRow: Integer;
    	LstField: TStrings; pDb: TCustomADODataSet;
        p1NCC: Boolean; pCond: array of Variant;
        pKeyFeild: String = ''; dbCheck: TCustomADODataSet = Nil; pFile: String = '');

    function IsEmptyRow(pWorksheet: TSheet; pRow, pColFrom, pColTo: Integer): Boolean;
    function IsValidHeaders(pSheet: TSheet; pColHeaders: TStrings; p1StDataRow: Integer): Boolean;

    function IsValidData(Values: Variant; pDataSet: TCustomADODataSet): Boolean;
    function IsValidData2(Values: Variant; pDataSet: TCustomADODataSet; p1NCC: Boolean; pCond: array of Variant): Boolean;
  public
  	Function ExcelExport(pExcelFile: String; pQuery: TCustomADODataSet;
        pLstField: TStrings = Nil; pSheetName: String = 'Sheet 1'; pOpen: Boolean = False): Integer; overload;
    Function ExcelExport(pExcelFile: String; pProcedureName: String;
    	pParams: array of Variant; pSheetName: String = 'Sheet 1'; pOpen: Boolean = False): Integer; overload;

    function ExcelImport(const pProfileSection, pExcelFile, pSqlTable, pSqlProc, pKeyName: String;
        pProcParamValues: array of Variant; pTransNo: Integer): Boolean; overload;
    function ExcelImport(const pProfileSection, pExcelFile: String;
        dbQuery: TCustomADODataSet; pKeyName: String = ''; dbCheck: TCustomADODataSet = Nil): Boolean; overload;

    function ExcelImport2(const pProfileSection, pExcelFile: String;
        p1NCC: Boolean; pCond: array of Variant;
        dbQuery: TCustomADODataSet; pKeyName: String = ''; dbCheck: TCustomADODataSet = Nil): Boolean; overload;

    procedure ExportDataToFile(pFile: String; pLstField: TStrings; pDB: TCustomADODataSet;
    	pSheet: String; pDesc: String = 'Exporting'; pErrField: String = '';
        pOveride: Boolean = True;  pAddTitle: Boolean = True; pOpen: Boolean = True;
        p1StDataRow: Integer = 0);

    procedure ExportDataToCSV(pFile: String; pProcedureName: String; pParams: array of Variant;
        pHeader: TStrings = Nil; pDesc: String = 'Exporting'; pOveride: Boolean = True;
        pAddTitle: Boolean = True; pShow: Boolean = False); overload;
    procedure ExportDataToCSV(pFile: String; pDB: TCustomADODataSet;  pHeader: TStrings = Nil;
        pDesc: String = 'Exporting'; pOveride: Boolean = True; pAddTitle: Boolean = True;
        pShow: Boolean = True); overload;
  end;

var
  DataExcel: TDataExcel;

const
    cSep: Char = #9;

implementation

uses
	IniFiles, MainData, Variants, isMsg, isDB, isStr, isLib, isCommon, isFile,
    isEnCoding;

{$R *.dfm}
{ TDataExcel }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.RefToCell(RowID, ColID: Integer): string;
var
    ACount, APos: Integer;
begin
    ACount := ColID div 26;
    APos := ColID mod 26;
    if APos = 0 then
    begin
        ACount := ACount - 1;
        APos := 26;
    end;

    if ACount = 0 then
        Result := Chr(Ord('A') + ColID - 1) + IntToStr(RowID)
    else if ACount = 1 then
        Result := 'A' + Chr(Ord('A') + APos - 1) + IntToStr(RowID)
    else if ACount > 1 then
        Result := Chr(Ord('A') + ACount - 1) + Chr(Ord('A') + APos - 1) + IntToStr(RowID);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.AddHeaderRow;
var
    i, n: Integer;
begin
    // Add header row
    n := pLstField.Count - 1;
    with pSheet do
    begin
        for i := 0 to n do
        begin
            AsString[i,0] := pLstField.Names[i];
//            Cell[i,1].FontStyle := TXFontStyle;
        end;
        if pError <> '' then
        begin
        	// Error Code
            AsString[n + 1,0] := pError;
//            Cell[n + 1,1].FontStyle := TXFontStyle;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.IsValidData(Values: Variant;
  pDataSet: TCustomADODataSet): Boolean;
var
    i, n: Integer;
begin
    // Database check
    with pDataSet as TADOQuery do
    begin
        if Active then
            Close;

        // Array of values
        if VarType(Values) and varArray <> 0 then
        begin
            n := Min(Length(Values), Parameters.Count);
            for i := 0 to n - 1 do
                Parameters[i].Value := Values[i];
        end
        // 1 value
        else if Parameters.Count > 0 then
            Parameters[0].Value := Values;

        Open;
        Result := not IsEmpty;
        Close;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.IsValidData2(Values: Variant;
  pDataSet: TCustomADODataSet; p1NCC: Boolean; pCond: array of Variant): Boolean;
var
    i, n: Integer;
begin
    // Database check
    with pDataSet as TADOQuery do
    begin
        if Active then
            Close;

        // Array of values
        if VarType(Values) and varArray <> 0 then
        begin
            n := Min(Length(Values), Parameters.Count);
            for i := 0 to n - 1 do
                Parameters[i].Value := Values[i];
        end
        // 1 value
        else if Parameters.Count > 0 then
        begin
            Parameters[0].Value := Values;
            Parameters[1].Value := pCond[0];
            if p1NCC then
                Parameters[2].Value := pCond[1];
        end;

        Open;
        Result := not IsEmpty;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.CopyDataToSheet;
var
    i, n, nSheet, iRow, iCol, nCol, nRow, totalRow, remainRow: Integer;
    bm: TBytes;
	mSheet: TSheet;

begin
    nCol := LstField.Count;
    with pDb do
    begin
        DisableControls;
        bm := Bookmark;
        First;
	    totalRow := RecordCount;
        remainRow := totalRow;
        i := 0;
	    nSheet := 0;
        mSheet := Nil;

        Forms.Application.ProcessMessages;
        StartProgress(totalRow);

		while remainRow > 0 do
        begin
        	if remainRow > 50000 then
                nRow := 50000
            else
            	nRow := remainRow;
			remainRow := remainRow - nRow;

            // Sheet preparing
            n := pSheets.Count;
            Inc(nSheet);
            if nSheet > n then
                pSheets.Add;
            mSheet := pSheets.Items[nSheet - 1];

            // Format sheet
            with mSheet.Range[1, 1, nCol, nRow + 1] do
            begin
                FontName := 'Arial';
                NumberFormat := '@';
            end;

            // Header
            AddHeaderRow(mSheet, LstField, pErrField);

            // Content
            iRow := 1;
            while nRow > 0 do
            begin
                Inc(i);
                SetProgressDesc(pDesc + Format(': %d/%d row(s)', [i, totalRow]));
                IncProgress;

                with mSheet do
                begin
                    for iCol := 0 to nCol - 1 do
                        case FieldByName(LstField.Names[iCol]).DataType of
                    	ftDateTime:
                                if FieldByName(LstField.Names[iCol]).AsFloat <> 0 then
                                    AsDateTime[iCol, iRow] := FieldByName(LstField.Names[iCol]).AsDateTime;
                        ftWideString, ftString, ftMemo, ftWideMemo:
                            AsString[iCol, iRow] := '''' + FieldByName(LstField.Names[iCol]).AsString
                        else
                            AsVariant[iCol, iRow] := FieldByName(LstField.Names[iCol]).Value;
                        end;
                    // Error for write log
                    if pErrField <> '' then
                    	AsVariant[nCol + 1, iRow] := FieldByName(pErrField).Value;
				end;

                Next;
                Inc(iRow);
                Dec(nRow);
            end;
        end;

        // Sheet cleaning
        while pSheets.Count > nSheet do
        begin
            pSheets.Delete(pSheets.Count);
        end;

        mSheet.Free;

        StopProgress;
		Bookmark := bm;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function TDataExcel.ExcelExport(pExcelFile: String;  pQuery: TCustomADODataSet;
    pLstField: TStrings; pSheetName: String; pOpen: Boolean): Integer;
begin
    OpenDataSets([pQuery]);
    with pQuery do
    begin

        if pLstField = Nil then
        begin
            pLstField := TStringList.Create;
            GetFieldNames(pLstField);
        end;

        if not IsEmpty then
        begin
            ExportDataToFile(pExcelFile, pLstField, pQuery, pSheetName, 'Exporting', '', True, True, pOpen);
            Result := 1;
        end
        else
            Result := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.ExcelExport(pExcelFile, pProcedureName: String;
  pParams: array of Variant; pSheetName: String; pOpen: Boolean): Integer;
var
	mListField: TStrings;
    i: Integer;
begin
	mListField := TStringList.Create;
	With SP_EXPORT do
    begin
        Close;
    	ProcedureName := pProcedureName;
        Parameters.Refresh;
        for i := 1 to Parameters.Count - 1 do
        	Parameters[i].Value := pParams[i-1];
        Open;
        GetFieldNames(mListField);
        if IsEmpty then
        	Result := 0
        else
        	Result := ExcelExport(pExcelFile, SP_EXPORT, mListField, pSheetName, pOpen);
    end;
    mListField.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.Worksheet2DB;
var
    i, j, num: Integer;
    cellVal: Variant;
    s, sVal: String;
    b: Boolean;
begin
	num := LstField.Count;
    with pWorksheet do
    begin
        i := p1stDataRow;
        StartProgress(0);

        while not IsEmptyRow(pWorksheet, i, 1, num) do
        begin
            with pDb do
            begin
                SetProgressDesc(Format('Import  "' + pWorksheet.Name + '":' + ' %d row(s)', [i-1]));
                Append;

                for j := 1 to num do
                begin
                	s := LstField.Names[j - 1];
                    cellVal := AsVariantRef [LstField.ValueFromIndex[j - 1] + IntToStr(i)];

                    case varType(cellVal) of
                    // Blank string
                    varString, varOleStr:
                    	begin
	                    	if Trim(cellVal) = '' then
                            begin
    	                    	cellVal := Null
                            end
                            else

		                    // Undefined
		                    if (cellVal = '#N/A') or (cellVal = '#VALUE!') then
                            begin
		                    	try
				                    sVal := cellVal;
									FieldByName('ErrCode').Value := s + ':' + sVal;
                        		except
		                        end;
    	                    	cellVal := Null;
                            end
                            else
                            if (Length(cellVal) > FieldByName(s).Size) and
                                (FieldByName(s).Size <> 0) then
                            begin
                                cellVal := isLeft(cellVal, FieldByName(s).Size);
                            end;
                    	end;

					// Zero value
                    varSmallInt, varInteger, varSingle, varDouble, varCurrency,
                    varByte, varWord, varLongWord, varInt64:
                    	if cellVal = 0 then
                        	cellVal := Null;
                    end;

                    if VarIsNull(cellVal) or VarIsEmpty(cellVal) then
                    else
                    begin
                        b := False;
                        try
                            FieldByName(s).Value := cellVal;
                        except
                            b := True;
                        end;

                        if b then
                            try
			                    sVal := cellVal;
                                FieldByName('ErrCode').Value := s + ':' + sVal;
                            finally
                            end
                    end;
                end;

                // Sheet name
                FieldByName('Sheet').AsString := pWorksheet.Name;
                if pTransNo > 0 then
                	FieldByName('TransNo').AsInteger := pTransNo;

                Post;
                Forms.Application.ProcessMessages;
            end;

            Inc(i);
        end;
        StopProgress;
    end;

    VarClear(cellVal);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.Worksheet2DB2(pWorksheet: TSheet;
  p1stDataRow: Integer; LstField: TStrings; pDb: TCustomADODataSet;
  pKeyFeild: String = ''; dbCheck: TCustomADODataSet = Nil; pFile: String = '' );
var
    i, j, num: Integer;
    cellVal: Variant;
    mFieldName: String;
begin
	num := LstField.Count;
    Application.ProcessMessages;
    with pWorksheet do
    begin
        i := p1stDataRow;
        StartProgress(0);

        while not IsEmptyRow(pWorksheet, i, 1, num) do
        begin
            with pDb do
            begin
                SetProgressDesc(Format('Analysing  "' + pWorksheet.Name + '":' + ' %d row(s)', [i-1]));
                Append;

                for j := 0 to num - 1 do
                begin
                    cellVal := pWorksheet.AsVariantRef[LstField.ValueFromIndex[j] + IntToStr(i)];
                    mFieldName := LstField.Names[j];
                    try
                    	if cellVal <> Null then
                        begin
                        	case FieldByName(mFieldName).DataType of
                            ftString, ftWideString, ftMemo, ftWideMemo:
                                if trim(cellVal) = '' then
                                	cellVal := Null;
                            end;

                            FieldByName(mFieldName).Value := cellVal;
                        end;

                    except
                          FieldByName('ErrCode').Value := mFieldName + ' #N/A'
                    end;
                end;

                if (dbCheck <> nil) and (pKeyFeild <> '') and not IsValidData(FieldByName(pKeyFeild).Value, dbCheck) then
                	FieldByName('ErrCode').Value := pKeyFeild;
                if Assigned(FieldByName('Sheet')) then
                	FieldByName('Sheet').Value := pWorksheet.Name;

                Post;
            end;
            IncProgress;
            Inc(i);
        end;
        //
        StopProgress;
    end;

    Forms.Application.ProcessMessages;
    VarClear(cellVal);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.Worksheet2DB3(pWorksheet: TSheet;
  p1stDataRow: Integer; LstField: TStrings; pDb: TCustomADODataSet;
  p1NCC: Boolean; pCond: array of Variant;
  pKeyFeild: String = ''; dbCheck: TCustomADODataSet = Nil; pFile: String = '' );
var
    i, j, num: Integer;
    cellVal: Variant;
    mFieldName: String;
begin
	num := LstField.Count;
    Application.ProcessMessages;
    with pWorksheet do
    begin
        i := p1stDataRow;
        StartProgress(0);

        while not IsEmptyRow(pWorksheet, i, 1, num) do
        begin
            with pDb do
            begin
                SetProgressDesc(Format('Analysing  "' + pWorksheet.Name + '":' + ' %d row(s)', [i-1]));
                Append;

                for j := 0 to num - 1 do
                begin
                    cellVal := pWorksheet.AsVariantRef[LstField.ValueFromIndex[j] + IntToStr(i)];
                    mFieldName := LstField.Names[j];
                    try
                    	if cellVal <> Null then
                        begin
                        	case FieldByName(mFieldName).DataType of
                            ftString, ftWideString, ftMemo, ftWideMemo:
                                if trim(cellVal) = '' then
                                	cellVal := Null;
                            end;

                            FieldByName(mFieldName).Value := cellVal;
                        end;

                    except
                          FieldByName('ErrCode').Value := mFieldName + ' #N/A'
                    end;
                end;

                if (dbCheck <> nil) and (pKeyFeild <> '') and not IsValidData2(FieldByName(pKeyFeild).Value, dbCheck, p1NCC, pCond) then
                	FieldByName('ErrCode').Value := pKeyFeild;
                if Assigned(FieldByName('Sheet')) then
                	FieldByName('Sheet').Value := pWorksheet.Name;

                Post;
            end;
            IncProgress;
            Inc(i);
        end;
        //
        StopProgress;
    end;

    Forms.Application.ProcessMessages;
    VarClear(cellVal);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.ExcelImport(const pProfileSection, pExcelFile,
  pSqlTable, pSqlProc, pKeyName: String;
  pProcParamValues: array of Variant; pTransNo: Integer): Boolean;
var
	mCount: Integer;
    mSheetName: String;
	colHeaders: TStrings;
    m1stDataRow: Integer;
    mSheetFrom, mSheetEnd: Integer;
    mFontName: String;

        (*
        ** Excel --> SQL
        *)
    procedure Proc1;
    var
		mSheet: TSheet;
        i, num: Integer;
    begin
        mSheetName := '';
        mSheet := Nil;
        with XLSReadWrite do
        begin
            Filename := pExcelFile;
            Read;
            with Sheets do
            begin
                num := Min(Sheets.Count, mSheetEnd);
                for i := Min(Sheets.Count, mSheetFrom) to num do
                begin
                    mSheet := Items[i - 1];
//                    if IsValidHeaders(mSheet, colHeaders, m1stDataRow) then
                    begin
                        Worksheet2DB(mSheet, m1stDataRow, colHeaders, QrIMPORT, pTransNo);
                        mSheetName := mSheetName + mSheet.Name + '#';
                    end;
                end
            end;

//            mFontName := mSheet.Cell[1,1].FontName;
        end;
        mSheet.Free;
    end;

    	(*
        ** Process SQL
        *)
    function Proc2: Integer;
    var
        n: Integer;
    begin
        Wait(PROCESSING);
        try
            with SP_PROCESS do
            begin
                CommandType := cmdStoredProc;
                CommandText := pSqlProc;
                Parameters.Refresh;

                // Set parameters
                n := 0;
                while n < Length(pProcParamValues) do
                begin
                    Parameters[n + 1].Value := pProcParamValues[n];
                    Inc(n);
                end;
                Execute;
                Result := Parameters[0].Value;
            end;
        finally
            ClearWait;
        end;
    end;

    	(*
        ** SQL --> Excel as log
        *)
    procedure Proc3;
    var
        s, sLogFile: String;
        i, n: Integer;
    begin
        sLogFile := isLeft(pExcelFile, Length(pExcelFile) - 5) + '_log.xls';
        DeleteFile(PChar(sLogFile));
       with XLSReadWrite do
        begin
            i := 0;
            // Add data
            while mSheetName <> '' do
            begin
            	inc(i);
                n := Pos('#', mSheetName);
                s := isLeft(mSheetName, n - 1);
                mSheetName := isSubStr(mSheetName, n + 1);

                with QrIMPORT do
                begin
                    Filter := Format('Sheet = ''%s''', [s]);
                    Filtered := True;

                    if not IsEmpty then
                    	ExportDataToFile(sLogFile, colHeaders, QrIMPORT, s ,'Write Log', 'ErrCode',
                    		Iif(i=1, True, False), True, True, m1stDataRow - 1);

                    Filter := '';
                end;
            end;
        end;
    end;
begin
	Result := False;
    colHeaders := TStringList.Create;

	// Read column headers
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        if not SectionExists(pProfileSection) then
        begin
            ErrMsg('Chưa cài đặt thông số Import dữ liệu. Liên hệ với quản trị hệ thống.');
            Free;
            Exit;
        end else
        begin
            ReadSectionValues(pProfileSection, colHeaders);
            Free;
        end;
    end;

    try
        with colHeaders do
        begin
            m1stDataRow := StrToIntDef(colHeaders.Values['1StDataRow'], 2);
            mSheetFrom	:= StrToIntDef(colHeaders.Values['SheetFrom'], 1);
            mSheetEnd	:= StrToIntDef(colHeaders.Values['SheetEnd'], 1);
            mCount := IndexOfName('1StDataRow');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetFrom');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetEnd');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
        end;
    except
    	m1stDataRow := 2;
        mSheetFrom	:= 1;
        mSheetEnd	:= 1;
    end;

    // Clean up
    if pTransNo <> 0 then
    	DataMain.Conn.Execute('delete ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo))
    else
    	DataMain.Conn.Execute('delete ' + pSqlTable);

    with QrIMPORT do
    begin
		// Excel --> SQL
        if pTransNo <> 0 then
        	SQL.Text := 'select * from ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo)
        else
    		SQL.Text := 'select * from ' + pSqlTable;
        Open;
        Proc1;

        // Process SQL
        mCount := Proc2;
        Requery;

        // SQL --> Excel Log
        Proc3;
        Close;

        if mCount > 0 then
            Result := True;
        Msg(Format('%d Record affected.',[mCount]));
    end;

    colHeaders.Free;

//    if pTransNo <> 0 then
//    	DataMain.Conn.Execute('delete ' + pSqlTable + ' where TransNo=' + IntToStr(pTransNo))
//    else
//    	DataMain.Conn.Execute('delete ' + pSqlTable);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.ExcelImport(const pProfileSection, pExcelFile: String;
        dbQuery: TCustomADODataSet; pKeyName: String = ''; dbCheck: TCustomADODataSet = Nil): Boolean;
var
	mCount: Integer;
    mSheetName: String;
	colHeaders: TStrings;
    m1stDataRow, mSheetFrom, mSheetEnd: Integer;

     procedure Proc1;
     var
		//mSheet: ExcelWorksheet;
        mSheet: TSheet;
        i, num: Integer;
     begin
        mSheetName := '';
        mSheet := Nil;
        with XLSReadWrite do
        begin
        	Filename := pExcelFile;
            Read;
            num := min(Sheets.Count, max(mSheetEnd, mSheetFrom));
            for i := min(Sheets.Count, mSheetFrom) to num do
            begin
            	mSheet := Sheets.Items[i - 1];
            	Worksheet2DB2(mSheet, m1stDataRow, colHeaders, dbQuery,
                pKeyName, dbCheck, pExcelFile);
            	mSheetName := mSheetName + mSheet.Name + '#';
            end;
        end;
        mSheet.Free;
    end;

    (*
        ** SQL --> Excel as log
        *)
    procedure Proc3;
    var
        s, sLogFile: String;
        n, i: Integer;
    begin
    	sLogFile := isLeft(pExcelFile, Length(pExcelFile) - 4) + '_log.xls';
        DeleteFile(PChar(sLogFile));
        i := 0;
    	while mSheetName <> '' do
        begin
        	Inc(i);
            n := Pos('#', mSheetName);
            s := isLeft(mSheetName, n - 1);
            mSheetName := isSubStr(mSheetName, n + 1);

            with dbQuery do
            begin
                Filter := Format('[ErrCode] <> Null and [Sheet] = ''%s''', [s]);
                Filtered := True;

                if not IsEmpty then
                    ExportDataToFile(sLogFile, colHeaders, dbQuery, s , 'Write Log', 'ErrCode',
                    	Iif(i=1, True, False), True, True, m1stDataRow - 1);

                Filter := '[ErrCode] = Null or [ErrCode] = ''''';
            end;
        end;
    end;
begin
	Result := False;
    colHeaders := TStringList.Create;
    CloseDataSets([dbQuery]);
    OpenDataSets([dbQuery]);

	// Read column headers
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        ReadSectionValues(pProfileSection, colHeaders);
        Free;
    end;

    try
        with colHeaders do
        begin
            m1stDataRow := StrToIntDef(colHeaders.Values['1StDataRow'], 2);
            mSheetFrom	:= StrToIntDef(colHeaders.Values['SheetFrom'], 1);
            mSheetEnd	:= StrToIntDef(colHeaders.Values['SheetEnd'], 1);
            mCount := IndexOfName('1StDataRow');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetFrom');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetEnd');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
        end;
    except
    	m1stDataRow := 2;
        mSheetFrom	:= 1;
        mSheetEnd	:= 1;
    end;

    Proc1;

    // Log.
	Proc3;

    colHeaders.Free;
    if Assigned(dbQuery.FieldByName('STT')) then
    	dbQuery.Sort := 'STT';
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.ExcelImport2(const pProfileSection, pExcelFile: String;
        p1NCC: Boolean; pCond: array of Variant;
        dbQuery: TCustomADODataSet; pKeyName: String = ''; dbCheck: TCustomADODataSet = Nil): Boolean;
var
	mCount: Integer;
    mSheetName: String;
	colHeaders: TStrings;
    m1stDataRow, mSheetFrom, mSheetEnd: Integer;

     procedure Proc1;
     var
		//mSheet: ExcelWorksheet;
        mSheet: TSheet;
        i, num: Integer;
     begin
        mSheetName := '';
        mSheet := Nil;
        with XLSReadWrite do
        begin
        	Filename := pExcelFile;
            Read;
            num := min(Sheets.Count, max(mSheetEnd, mSheetFrom));
            for i := min(Sheets.Count, mSheetFrom) to num do
            begin
            	mSheet := Sheets.Items[i - 1];
            	Worksheet2DB3(mSheet, m1stDataRow, colHeaders, dbQuery,
                p1NCC, pCond,
                pKeyName, dbCheck, pExcelFile);
            	mSheetName := mSheetName + mSheet.Name + '#';
            end;
        end;
        mSheet.Free;
    end;

    (*
        ** SQL --> Excel as log
        *)
    procedure Proc3;
    var
        s, sLogFile: String;
        n, i: Integer;
    begin
    	sLogFile := isLeft(pExcelFile, Length(pExcelFile) - 4) + '_log.xls';
        DeleteFile(PChar(sLogFile));
        i := 0;
    	while mSheetName <> '' do
        begin
        	Inc(i);
            n := Pos('#', mSheetName);
            s := isLeft(mSheetName, n - 1);
            mSheetName := isSubStr(mSheetName, n + 1);

            with dbQuery do
            begin
                Filter := Format('[ErrCode] <> Null and [Sheet] = ''%s''', [s]);
                Filtered := True;

                if not IsEmpty then
                    ExportDataToFile(sLogFile, colHeaders, dbQuery, s , 'Write Log', 'ErrCode',
                    	Iif(i=1, True, False), True, True, m1stDataRow - 1);

                Filter := '[ErrCode] = Null or [ErrCode] = ''''';
            end;
        end;
    end;
begin
	Result := False;
    colHeaders := TStringList.Create;
    CloseDataSets([dbQuery]);
    OpenDataSets([dbQuery]);

	// Read column headers
    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        ReadSectionValues(pProfileSection, colHeaders);
        Free;
    end;

    try
        with colHeaders do
        begin
            m1stDataRow := StrToIntDef(colHeaders.Values['1StDataRow'], 2);
            mSheetFrom	:= StrToIntDef(colHeaders.Values['SheetFrom'], 1);
            mSheetEnd	:= StrToIntDef(colHeaders.Values['SheetEnd'], 1);
            mCount := IndexOfName('1StDataRow');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetFrom');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
            mCount := IndexOfName('SheetEnd');
            if mCount >= 0 then
            	colHeaders.Delete(mCount);
        end;
    except
    	m1stDataRow := 2;
        mSheetFrom	:= 1;
        mSheetEnd	:= 1;
    end;

    Proc1;

    // Log.
	Proc3;

    colHeaders.Free;
    if Assigned(dbQuery.FieldByName('STT')) then
    	dbQuery.Sort := 'STT';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.ExportDataToCSV(pFile, pProcedureName: String;
  pParams: array of Variant; pHeader: TStrings; pDesc: String; pOveride,
  pAddTitle: Boolean; pShow: Boolean);
var
    i: Integer;
begin
    With SP_EXPORT do
    begin
    	ProcedureName := pProcedureName;
        Parameters.Refresh;
        for i := 1 to Parameters.Count - 1 do
        	Parameters[i].Value := pParams[i-1];
		Close;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;
    end;
    ExportDataToCSV(pFile, SP_EXPORT, pHeader, pDesc, pOveride, pAddTitle, pShow);
    SP_EXPORT.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.ExportDataToCSV(pFile: String; pDB: TCustomADODataSet;
  pHeader: TStrings; pDesc: String; pOveride, pAddTitle: Boolean; pShow: Boolean);
    // Add Columns for Header
    procedure AddHeaderColumns;
    var
        n, j, k: Integer;
        ls: TStrings;
    begin
        n := pDB.FieldCount - 1;
        ls := TStringList.Create;
        for j := 0 to pHeader.Count - 1 do
        begin
            isStrBreak(pHeader[j], cSep, ls);
            if ls.Count < n then
            begin
                for k := ls.Count to n do
                    pHeader[j] := pHeader[j] + cSep;
            end;
        end;
        ls.Free;
    end;
begin
    with ExToCSV do
    begin
        FileName := pFile;
        Columns.Clear;
        if pHeader <> Nil then
        begin
            AddHeaderColumns;
            Header := pHeader;
        end;

        AddTitle := pAddTitle;
        KeyGenerator := pDesc;

        if pOveride then
            Options := Options - [soMergeData]
        else
        	Options := Options + [soMergeData];

        DataSet := pDB;
		StartProgress(pDB.RecordCount, pDesc);
    	SetProgressDesc(pDesc);
        Execute;
        StopProgress;
    end;

    if pShow and FileExists(pFile) then
        ShellExecute(0, Nil, PChar(pFile), Nil, Nil, SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.ExportDataToFile(pFile: String; pLstField: TStrings;
	pDB: TCustomADODataSet; pSheet, pDesc, pErrField: String;
    pOveride, pAddTitle, pOpen: Boolean; p1StDataRow: Integer);
var
    i: Integer;
begin
    // Initial
    with pDB do
	    for i := 0 to FieldCount - 1 do
        	Fields[i].Visible := False;
    if pLstField.Names[0] = '' then
        for i := 0 to pLstField.Count - 1 do
            with pDB.FieldByName(pLstField[i]) do
            begin
                Visible := True;
                Index := i;
            end
    else
        for i := 0 to pLstField.Count - 1 do
            with pDB.FieldByName(pLstField.Names[i]) do
            begin
                Visible := True;
                Index := i;
            end;

	if pErrField <> '' then
    	pDB.FieldByName(pErrField).Visible := True;

    with ExToExcel do
    begin
        FileName := pFile;
        Columns.Clear;
        KeyGenerator := pSheet;
        StartRow := max(p1StDataRow - Iif(pAddTitle, 1, 0), 0);
        AddTitle := pAddTitle;
        ActionAfterExport := Iif(pOpen, aeOpenView, aeNone);

        if pOveride then
        begin
            Options := Options - [soMergeData];
            DeleteDefaultSheets := True;
        end
        else
        begin
        	Options := Options + [soMergeData];
            DeleteDefaultSheets := False;
        end;

        DataSet := pDB;
		StartProgress(pDB.RecordCount, 'Exporting');
    	SetProgressDesc(pDesc);
        Execute;
        StopProgress;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.ExToExcelGetCellParams(Sender: TObject; Field: TField;
  var Text: WideString; AFont: TFont; var Alignment: TAlignment;
  var Background: TColor; var CellType: TCellType);
begin
	if ExToExcel.AddTitle and not Assigned(Field) and
    	(TSMExportToExcel(Sender).Statistic.CurrentRow = 0) then
	begin
		//AFont.Size := 10;
        AFont.Style := AFont.Style + [fsBold];
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataExcel.ExToExcelProgress(Sender: TObject; CurValue,
  MaxValue: Integer; var Abort: Boolean);
begin
    SetProgress(CurValue);
end;

	(*
    ** Header check
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.IsValidHeaders(pSheet: TSheet; pColHeaders: TStrings;
  p1StDataRow: Integer): Boolean;
var
    i: Integer;
begin
    Result := True;
    for i := 0 to pColHeaders.Count - 1 do
    begin
        if not SameText(
            Trim(pSheet.AsStringRef[pColHeaders.ValueFromIndex[i] + IntToStr(p1StDataRow)]), pColHeaders.Names[i]) then
        begin
            Result := False;
            Msg('Thứ tự cột không đúng.');
            Break;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataExcel.IsEmptyRow(pWorksheet: TSheet; pRow, pColFrom,
  pColTo: Integer): Boolean;
var
    i: Integer;
begin
    Result := True;
    for i := pColFrom to pColTo do
        if Trim(pWorksheet.AsString[i-1, pRow-1]) <> '' then
        begin
            Result := False;
            Break;
        end;
end;

end.
