﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonv;

interface

uses
  SysUtils, Classes, Controls, Forms, Graphics, System.Variants,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Grids, Wwdbigrd,
  Wwdbgrid, Mask, RzPanel, ToolWin, wwdblook, Buttons, wwDataInspector, fcImager,
  AdvEdit, DBAdvEd, rDBComponents, fcCombo, fctreecombo, Wwdotdot, Wwdbcomb,
  wwcheckbox, DBCtrlsEh, GuidEx, DBGridEh, DBLookupEh, ToolCtrlsEh, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBVertGridsEh, DbLookupComboboxEh2;

type
  TFrmHoSonv = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    BtnPrint: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    QrDMNV: TADOQuery;
    DsDMNV: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton10: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PaCaNhan: TisPanel;
    Label2: TLabel;
    EdTen: TDBEditEh;
    DBEdit2: TDBEditEh;
    DBEdit5: TDBEditEh;
    EdMa: TDBEditEh;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    CmdReload: TAction;
    QrDMNVMANV: TWideStringField;
    QrDMNVTENNV: TWideStringField;
    QrDMNVDTHOAI: TWideStringField;
    QrDMNVDCHI: TWideStringField;
    QrDMNVEMAIL: TWideStringField;
    QrDMNVCREATE_BY: TIntegerField;
    QrDMNVUPDATE_BY: TIntegerField;
    QrDMNVCREATE_DATE: TDateTimeField;
    QrDMNVUPDATE_DATE: TDateTimeField;
    QrDMNVGHICHU: TWideMemoField;
    ALLOC_MANV: TADOCommand;
    QrDMNVNgayThoiViec: TDateTimeField;
    QrDMNVMaPhongBan: TWideStringField;
    QrDMNVNGAYSINH: TDateTimeField;
    QrDMNVMaChucDanh: TWideStringField;
    QrDMNVLUONG_COBAN: TFloatField;
    QrDMNVCOMAT_BANGCONG: TBooleanField;
    QrDMNVCOMAT_BANGLUONG: TBooleanField;
    wwDBEdit1: TDBEditEh;
    wwDBEdit2: TDBEditEh;
    CmdDmtk: TAction;
    QrDMNVNGAY_VAOLAM: TDateTimeField;
    QrDMNVMACC: TWideStringField;
    QrDMNVMA_NHOMLV: TWideStringField;
    QrDMNVMST: TWideStringField;
    QrDMNVMABP: TWideStringField;
    QrDMNVMA_NOI_LAMVIEC: TWideStringField;
    QrDMNVLUONG_BAOHIEM: TFloatField;
    QrDMNVPTTT: TWideStringField;
    QrDMNVNGAY_LAMDON_THOIVIEC: TDateTimeField;
    QrDMNVNGAY_TRATAISAN: TDateTimeField;
    QrDMNVNGAY_QUYETTOAN_LUONG: TDateTimeField;
    QrDMNVGHICHU_THOIVIEC: TWideStringField;
    QrDMNVGIOITINH: TIntegerField;
    PaTuyenDung: TisPanel;
    QrDMNVNGAY_THAMNIEN: TDateTimeField;
    PaToChuc: TisPanel;
    QrDMNVBIDANH: TWideStringField;
    QrDMNVNAMSINH: TIntegerField;
    QrDMNVNOISINH: TWideStringField;
    QrDMNVQUOCTICH: TWideStringField;
    QrDMNVDANTOC: TWideStringField;
    QrDMNVTONGIAO: TWideStringField;
    QrDMNVVANHOA: TWideStringField;
    QrDMNVDCHI_THUONGTRU: TWideStringField;
    wwDBEdit4: TDBEditEh;
    wwDBEdit5: TDBEditEh;
    CmdEdit: TAction;
    ToolButton8: TToolButton;
    ToolButton12: TToolButton;
    PopMore: TAdvPopupMenu;
    CmdThoiviec: TAction;
    BtnChitiet: TToolButton;
    ToolButton14: TToolButton;
    QrDMNVHO: TWideStringField;
    QrDMNVTEN: TWideStringField;
    wwDBEdit6: TDBEditEh;
    PaDangDoanThe: TisPanel;
    PaThongTinChung: TPanel;
    paPhoto: TPanel;
    Photo: TfcDBImager;
    PaProEmp: TPanel;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaSearch: TPanel;
    QrDMNVHuuTriNam: TIntegerField;
    QrDMNVHuuTriThang: TIntegerField;
    QrDMNVNgayHuuTri: TDateTimeField;
    QrDMNVNoiSinh_MaTinh: TWideStringField;
    QrDMNVHonNhan: TWideStringField;
    QrDMNVChuyenMon: TWideStringField;
    QrDMNVChuyenNganh: TWideStringField;
    QrDMNVCuTru_MaTinh: TWideStringField;
    QrDMNVCuTru_MaHuyen: TWideStringField;
    QrDMNVThuongTru_MaTinh: TWideStringField;
    QrDMNVThuongTru_MaHuyen: TWideStringField;
    QrDMNVEmailCaNhan: TWideStringField;
    QrDMNVLienHe1_HoTen: TWideStringField;
    QrDMNVLienHe1_QuanHe: TWideStringField;
    QrDMNVLienHe1_DienThoai: TWideStringField;
    QrDMNVLienHe1_DiaChi: TWideStringField;
    QrDMNVLienHe2_HoTen: TWideStringField;
    QrDMNVLienHe2_QuanHe: TWideStringField;
    QrDMNVLienHe2_DienThoai: TWideStringField;
    QrDMNVLienHe2_DiaChi: TWideStringField;
    QrDMNVDangVien_NgayThamGia: TDateTimeField;
    QrDMNVDangVien_NgayChinhThuc: TDateTimeField;
    QrDMNVDangVien_TinhTrang: TWideStringField;
    QrDMNVDangVien_GhiChu: TWideStringField;
    QrDMNVDoanVien_NgayThamGia: TDateTimeField;
    QrDMNVDoanVien_TinhTrang: TWideStringField;
    QrDMNVCongDoan_NgayThamGia: TDateTimeField;
    QrDMNVCongDoan_Phi: TFloatField;
    QrDMNVMaChiNhanh: TWideStringField;
    QrDMNVMaNguonTD: TWideStringField;
    QrDMNVNgayBatDauThuViec: TDateTimeField;
    QrDMNVNgayKetThucThuViec: TDateTimeField;
    QrDMNVMaQTLV_VaoLam: TWideStringField;
    QrDMNVNgayBienChe: TDateTimeField;
    QrDMNVDoiTuong: TIntegerField;
    QrDMNVMaBacLuong: TWideStringField;
    QrDMNVBacLuong: TIntegerField;
    QrDMNVNgayBacLuong: TDateTimeField;
    QrDMNVBHXH_So: TWideStringField;
    QrDMNVBHXH_NgayCap: TDateTimeField;
    QrDMNVBHXH_NoiCap: TWideStringField;
    QrDMNVBHYT_So: TWideStringField;
    QrDMNVBHYT_NgayBatDau: TDateTimeField;
    QrDMNVBHYT_NgayKetThuc: TDateTimeField;
    QrDMNVBHYT_NoiCap: TWideStringField;
    QrDMNVMaKCB: TWideStringField;
    QrDMNVTinhTrang: TIntegerField;
    QrDMNVMaQTLV_ThoiViec: TWideStringField;
    QrEmp: TADOQuery;
    QrDMNVLK_XacNhan_Tennv: TWideStringField;
    TabSheet3: TTabSheet;
    PaThongTinLuong: TisPanel;
    QrDMNVLuongNgay: TFloatField;
    QrDMNVLuongGio: TFloatField;
    QrDMNVCo_BHXH: TBooleanField;
    QrDMNVCo_BHYT: TBooleanField;
    QrDMNVCo_BHTN: TBooleanField;
    QrDMNVCo_ChuyenKhoan: TBooleanField;
    PaNoiSinhHuuTri: TisPanel;
    Label48: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    wwDBEdit12: TDBEditEh;
    wwDBDateTimePicker9: TwwDBDateTimePicker;
    DBNumberEditEh9: TDBNumberEditEh;
    DBNumberEditEh10: TDBNumberEditEh;
    DBNumberEditEh11: TDBNumberEditEh;
    wwDBDateTimePicker10: TwwDBDateTimePicker;
    ScrollPro: TScrollBox;
    ScrollCV: TScrollBox;
    CmdNoiCapBHXH: TAction;
    CmdNoiCapBHYT: TAction;
    PaDiaChiLienLac: TisPanel;
    GroupBox3: TGroupBox;
    EdThuongTruDiaChi: TDBEditEh;
    GroupBox4: TGroupBox;
    EdTamTruDiaChi: TDBEditEh;
    CmdNoiCapCCCD: TAction;
    CmdHoChieuLoai: TAction;
    CmdDangTinhTrang: TAction;
    CmdDoanTinhTrang: TAction;
    PopDanhMuc: TAdvPopupMenu;
    QrHuyen: TADOQuery;
    CmdQuaTrinhLamViec: TAction;
    CmdHopDong: TAction;
    CmdKhenThuongKyLuat: TAction;
    CmdQuanHeGiaDinh: TAction;
    CmdTaiSanThietBi: TAction;
    CmdDiCongTac: TAction;
    CmdKienThucTaiLieu: TAction;
    CmdTruocVaoCongTy: TAction;
    Qutrnhlmvic1: TMenuItem;
    Hpnglaong1: TMenuItem;
    Khenthngklut1: TMenuItem;
    Linlc1: TMenuItem;
    Gitisnthitb1: TMenuItem;
    icngtc1: TMenuItem;
    Kinthctiliu1: TMenuItem;
    rckhivocngty1: TMenuItem;
    hivic1: TMenuItem;
    CmdPhongBan: TAction;
    CmdChucDanh: TAction;
    CmdNoiLamViec: TAction;
    CmdNguonTuyenDung: TAction;
    CmdNoiDangKyKhamBenh: TAction;
    Phngban1: TMenuItem;
    Chcdanhchcv1: TMenuItem;
    aimlmvic1: TMenuItem;
    Nguntuyndng1: TMenuItem;
    Ningkkhmbnh1: TMenuItem;
    PaOrg: TisPanel;
    CbOrg: TfcTreeCombo;
    QrDMNVLK_TenPhongBan: TWideStringField;
    QrDMNVLK_TenBoPhan: TWideStringField;
    QrDMNVLK_TenChucDanh: TWideStringField;
    QrDMNVLK_TenNoiLV: TWideStringField;
    QrDMNVLK_TenNhomLV: TWideStringField;
    QrDMNVLK_TenDoiTuong: TWideStringField;
    QrDMNVLK_TenNguonTD: TWideStringField;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSite: TDataSource;
    DsDep: TDataSource;
    DsSec: TDataSource;
    ScrollThuNhap: TScrollBox;
    PaPhuCap: TisPanel;
    QrAllowance: TADOQuery;
    DsAllowance: TDataSource;
    GrAllowance: TwwDBGrid2;
    QrAllowanceManv: TWideStringField;
    QrAllowanceMaPhuCap: TWideStringField;
    QrAllowanceCo_SuDung: TBooleanField;
    QrAllowanceNgayXetDuyet: TDateTimeField;
    QrAllowanceThangBatDau: TIntegerField;
    QrAllowanceNamBatDau: TIntegerField;
    QrAllowanceThangKetThuc: TIntegerField;
    QrAllowanceNamKetThuc: TIntegerField;
    QrAllowanceCo_GioiHan: TBooleanField;
    QrAllowanceSoThang: TFloatField;
    QrAllowanceSoTien: TFloatField;
    QrAllowanceGhiChu: TWideStringField;
    QrAllowanceCREATE_BY: TIntegerField;
    QrAllowanceUPDATE_BY: TIntegerField;
    QrAllowanceCREATE_DATE: TDateTimeField;
    QrAllowanceUPDATE_DATE: TDateTimeField;
    QrAllowanceLK_TenPhuCap: TWideStringField;
    QrAllowanceLK_PhuCapLoai: TWideStringField;
    QrAllowanceLK_TenPhanLoai: TWideStringField;
    CbMaPhuCap: TwwDBLookupCombo;
    CbPhuCap: TwwDBLookupCombo;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    PopDetail: TAdvPopupMenu;
    Xachitit1: TMenuItem;
    CmdDelDetail: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    QrTaiKhoanNV: TADOQuery;
    DsTaiKhoanNV: TDataSource;
    QrDMNVLK_MaNH: TWideStringField;
    QrDMNVLK_MaNH_CN: TWideStringField;
    QrDMNVLK_TenNH: TWideStringField;
    QrDMNVLK_TenNH_CN: TWideStringField;
    CmdTaiKhoanTruyCap: TAction;
    ikhontruycpdliu1: TMenuItem;
    QrDMNVBHYT_Ngay5Nam: TDateTimeField;
    CmdQuocTich: TAction;
    CmdTonGiao: TAction;
    CmdHonNhan: TAction;
    CmdDanToc: TAction;
    CmdVanHoa: TAction;
    CmdLoaiQuaTrinhLamViec: TAction;
    CmdLoaiPhuCap: TAction;
    CmdViTriDiaLy: TAction;
    Loiqutrnhlmvic1: TMenuItem;
    Loiphcp1: TMenuItem;
    Vtral1: TMenuItem;
    QrDMNVCuTru_MaXa: TWideStringField;
    QrDMNVThuongTru_MaXa: TWideStringField;
    QrDMNVMaTK: TWideStringField;
    QrPhuongXa: TADOQuery;
    wwDBEdit22: TDBEditEh;
    wwDBEdit23: TDBEditEh;
    wwDBEdit25: TDBEditEh;
    QrDMNVNhomMau: TWideStringField;
    QrDMNVTinhTrangSucKhoe: TWideStringField;
    QrDMNVCanNang: TFloatField;
    QrDMNVDangVien_ChiBo: TWideStringField;
    QrDMNVCapUy: TWideStringField;
    QrDMNVCapUyKiem: TWideStringField;
    QrDMNVChucVuDangVien: TWideStringField;
    QrDMNVChucVuChinhQuyen: TWideStringField;
    QrDMNVNamQuyHoach: TIntegerField;
    QrDMNVNamQuyHoachBoSung: TIntegerField;
    QrDMNVBoNhiem_MaChucDanh: TWideStringField;
    QrDMNVBoNhiem_NgayKetThuc: TDateTimeField;
    QrDMNVBietPhai_MaChucDanh: TWideStringField;
    QrDMNVBietPhai_NgayKetThuc: TDateTimeField;
    QrDMNVPhepNam_LuatDinh: TFloatField;
    QrDMNVPhepNam_ChucDanh: TFloatField;
    QrDMNVPhepNam_ThamNien: TFloatField;
    QrDMNVXacNhan_Manv: TWideStringField;
    QrDMNVHeSoLuong: TFloatField;
    QrDMNVHeSoPhuCap: TFloatField;
    QrDMNVTyLeLuongThuViec: TFloatField;
    QrDMNVLK_XetDuyet_Tennv: TWideStringField;
    QrDMNVCongViecChinh: TWideStringField;
    QrDMNVThongTinKhac: TWideMemoField;
    CmdThongTinKhac: TAction;
    PaChinhQuyen: TisPanel;
    wwDBEdit24: TDBEditEh;
    Label74: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    DBNumberEditEh7: TDBNumberEditEh;
    DBNumberEditEh8: TDBNumberEditEh;
    Label78: TLabel;
    DpBoNhiemTuNgay: TwwDBDateTimePicker;
    Label80: TLabel;
    DpBietPhaiTuNgay: TwwDBDateTimePicker;
    DBNumberEditEh5: TDBNumberEditEh;
    wwDBEdit29: TDBEditEh;
    DBNumberEditEh6: TDBNumberEditEh;
    CmdNhomMau: TAction;
    CmdLienLacKhan: TAction;
    GroupBox7: TGroupBox;
    Label27: TLabel;
    wwDBDateTimePicker12: TwwDBDateTimePicker;
    QrDMNVWebsite: TWideStringField;
    Label35: TLabel;
    wwDBDateTimePicker13: TwwDBDateTimePicker;
    CmdChiBoDang: TAction;
    GroupBox8: TGroupBox;
    Label7: TLabel;
    Label53: TLabel;
    wwDBDateTimePicker14: TwwDBDateTimePicker;
    wwDBDateTimePicker15: TwwDBDateTimePicker;
    wwDBEdit7: TDBEditEh;
    wwDBEdit8: TDBEditEh;
    wwDBEdit9: TDBEditEh;
    PaHeSoLuong: TisPanel;
    QrDMNVLK_TenBacLuong: TWideStringField;
    GroupBox9: TGroupBox;
    QrDMNVNgayPhongVan: TDateTimeField;
    QrDMNVNgayKiemTraKyNang: TDateTimeField;
    GroupBox10: TGroupBox;
    Label15: TLabel;
    DpTuNgay: TwwDBDateTimePicker;
    Label18: TLabel;
    wwDBDateTimePicker20: TwwDBDateTimePicker;
    Label86: TLabel;
    DpChinhThucTuNgay: TwwDBDateTimePicker;
    lbThamNien: TLabel;
    CbThamNien: TwwDBDateTimePicker;
    rgObsolete: TRadioGroup;
    TabSheet4: TTabSheet;
    PaBaoHiem: TisPanel;
    GroupBox1: TGroupBox;
    Label39: TLabel;
    wwDBDateTimePicker6: TwwDBDateTimePicker;
    wwDBEdit10: TDBEditEh;
    GroupBox2: TGroupBox;
    Label42: TLabel;
    Label44: TLabel;
    Label65: TLabel;
    wwDBDateTimePicker7: TwwDBDateTimePicker;
    wwDBEdit11: TDBEditEh;
    wwDBDateTimePicker8: TwwDBDateTimePicker;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    PaGiaoDich: TisPanel;
    EdTenNH: TDBEditEh;
    EdTenNH_CN: TDBEditEh;
    PaGiayToTuyThan: TisPanel;
    GroupBox5: TGroupBox;
    Label57: TLabel;
    dpCCCDNgayCap: TwwDBDateTimePicker;
    EdCCCDGhiChu: TDBEditEh;
    GroupBox6: TGroupBox;
    Label61: TLabel;
    Label66: TLabel;
    dpHoChieuNgayCap: TwwDBDateTimePicker;
    EdHoChieuNoiCap: TDBEditEh;
    dpHoChieuNgayHetHan: TwwDBDateTimePicker;
    ScrollGiayTo: TScrollBox;
    GroupBox11: TGroupBox;
    Label20: TLabel;
    Label32: TLabel;
    wwDBDateTimePicker19: TwwDBDateTimePicker;
    wwDBDateTimePicker22: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    QrDMNVCALC_TongPhepNam: TFloatField;
    QrDMNVCo_PhepNamMotThang: TBooleanField;
    QrDMNVLK_PhepNam_LuatDinh: TFloatField;
    QrDMNVLK_PhepNam_ChucDanh: TFloatField;
    QrDMNVLK_PhepNam_ThamNien: TFloatField;
    QrDMNVLK_Co_PhepNamMotThang: TBooleanField;
    rDBCheckBox1: TrDBCheckBox;
    CmdRefresh: TAction;
    CmdNgayKyNiem: TAction;
    Ccngyknim1: TMenuItem;
    QrDMNVChieuCao: TFloatField;
    CbChucDanh: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    DBEditEh2: TDBEditEh;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaBoPhan_BanDau: TDBEditEh;
    QrDMNVLK_MaBoPhan: TWideStringField;
    CbLyDoTuyen: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    CbNguonTuyenDung: TDbLookupComboboxEh2;
    DBEditEh5: TDBEditEh;
    CbNoiLamViec: TDbLookupComboboxEh2;
    DBEditEh6: TDBEditEh;
    CbNhomLamViec: TDbLookupComboboxEh2;
    DBEditEh7: TDBEditEh;
    CbDoiTuong: TDbLookupComboboxEh2;
    CbNhomMau: TDbLookupComboboxEh2;
    CbQuocTich: TDbLookupComboboxEh2;
    CbTonGiao: TDbLookupComboboxEh2;
    CbDanToc: TDbLookupComboboxEh2;
    CbVanHoa: TDbLookupComboboxEh2;
    CbGioiTinh: TDbLookupComboboxEh2;
    CbHonNhan: TDbLookupComboboxEh2;
    CbDangVien_TinhTrang: TDbLookupComboboxEh2;
    CbChiBoDang: TDbLookupComboboxEh2;
    CbDoanVien_TinhTrang: TDbLookupComboboxEh2;
    CbThuongTruTinhThanhPho: TDbLookupComboboxEh2;
    CbCuTruTinhThanhPho: TDbLookupComboboxEh2;
    DsHuyen: TDataSource;
    DsPhuongXa: TDataSource;
    CbThuongTruQuanHuyen: TDbLookupComboboxEh2;
    CbThuongTruPhuongXa: TDbLookupComboboxEh2;
    CbCuTruQuanHuyen: TDbLookupComboboxEh2;
    CbCuTruPhuongXa: TDbLookupComboboxEh2;
    CbNoiCapBHXH: TDbLookupComboboxEh2;
    CbNoiCapBHYT: TDbLookupComboboxEh2;
    CbNoiDangKyKhamBenh: TDbLookupComboboxEh2;
    DBEditEh8: TDBEditEh;
    EdCCCDMaNoiCap: TDBEditEh;
    CbBoNhiem_MaChucDanh: TDbLookupComboboxEh2;
    CbBietPhai_MaChucDanh: TDbLookupComboboxEh2;
    Status: TStatusBar;
    CbSoTaiKhoan: TDbLookupComboboxEh2;
    QrDMNVLK_MaPhongBan: TWideStringField;
    EdXacNhan_ManvQL: TDBEditEh;
    CbXacNhan_Manv: TDbLookupComboboxEh2;
    EdXetDuyet_ManvQL: TDBEditEh;
    CbXetDuyet_Manv: TDbLookupComboboxEh2;
    DsEmp: TDataSource;
    DBEditEh11: TDBEditEh;
    DBEditEh12: TDBEditEh;
    EdThongTinKhac: TDBMemoEh;
    QrDMNVBoNhiem_NgayBatDau: TDateTimeField;
    QrDMNVBietPhai_NgayBatDau: TDateTimeField;
    wwDBDateTimePicker17: TwwDBDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    wwDBDateTimePicker18: TwwDBDateTimePicker;
    TabSheet5: TTabSheet;
    PaGhiChu: TisPanel;
    EdGhichu: TDBMemo;
    PaLienLacKhan: TisPanel;
    DBEditEh13: TDBEditEh;
    DBEditEh14: TDBEditEh;
    DBEditEh15: TDBEditEh;
    wwDBEdit16: TDBEditEh;
    wwDBEdit17: TDBEditEh;
    wwDBEdit18: TDBEditEh;
    wwDBEdit19: TDBEditEh;
    wwDBEdit20: TDBEditEh;
    ScrollBox1: TScrollBox;
    grThongTinLuong: TDBVertGridEh;
    grHeSoLuong: TDBVertGridEh;
    CbBacLuong: TDbLookupComboboxEh2;
    QrDMNVXetDuyet_Manv: TWideStringField;
    spHR_LICHSU_THIETBI_Invalid_NgayTraTaiSan: TADOCommand;
    spIMP_HR_DM_NHANVIEN_DeleteList: TADOCommand;
    QrDMNVNgayVaoLamChinhThuc: TDateTimeField;
    QrDMNVCo_MacDinhCong: TBooleanField;
    QrDMNVCo_CongDoan: TBooleanField;
    Label4: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBEditEh16: TDBEditEh;
    QrDMNVManvQL: TWideStringField;
    QrDMNVNgayBatDauHocViec: TDateTimeField;
    QrDMNVNgayKetThucHocViec: TDateTimeField;
    Label5: TLabel;
    Label6: TLabel;
    DpHocViecTuNgay: TwwDBDateTimePicker;
    DpHocViecDenNgay: TwwDBDateTimePicker;
    QrDMNVLK_XetDuyet_ManvQL: TWideStringField;
    QrDMNVLK_XacNhan_ManvQL: TWideStringField;
    spHR_TAIKHOAN_NGANHANG_NGAYSUDUNGMAX: TADOCommand;
    QrDMNVLK_UserName: TWideStringField;
    QrDMNVLK_CREATE_FULLNAME: TWideStringField;
    QrDMNVLK_UPDATE_FULLNAME: TWideStringField;
    ToolButton13: TToolButton;
    CmdHinhAnh: TAction;
    ToolButton15: TToolButton;
    QrV_HR_DM_NHANVIEN_CCCD_MANV: TADOQuery;
    DsV_HR_DM_NHANVIEN_CCCD_MANV: TDataSource;
    CmdCCCD: TAction;
    QrDMNVLK_CCCD_GhiChu: TWideStringField;
    QrDMNVLK_CCCD_MaNoiCap: TWideStringField;
    QrDMNVLK_CCCD_NgayCap: TDateTimeField;
    QrDMNVLK_CCCD_TenNoiCap: TWideStringField;
    EdCCCDNoiCap: TDBEditEh;
    QrDMNVLK_CCCD_MaNoiCap_Ma: TWideStringField;
    CbCCCD: TDbLookupComboboxEh2;
    CbHoChieu: TDbLookupComboboxEh2;
    QrV_HR_DM_NHANVIEN_HOCHIEU_MANV: TADOQuery;
    DsV_HR_DM_NHANVIEN_HOCHIEU_MANV: TDataSource;
    CmdHoChieu: TAction;
    QrDMNVLK_HoChieu_NoiCap: TWideStringField;
    EdHoChieuTenNhom: TDBEditEh;
    QrDMNVLK_HoChieu_NgayCap: TDateTimeField;
    QrDMNVLK_HoChieu_NgayHetHan: TDateTimeField;
    spHR_CCCD_NGAYCAPMAX: TADOCommand;
    EdCCCDTenNhom: TDBEditEh;
    QrDMNVLK_CCCD_GiayTo_TenNhom: TWideStringField;
    QrDMNVLK_HoChieu_GiayTo_TenNhom: TWideStringField;
    QrTaiKhoanNV_MANV: TADOQuery;
    DsTaiKhoanNV_MANV: TDataSource;
    QrAllowanceNgayBatDau: TDateTimeField;
    Panel1: TPanel;
    EdDmPhuCap: TDBEditEh;
    CmdNewList: TAction;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    PopIn: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdPrintForm: TAction;
    N1: TMenuItem;
    MenuItem2: TMenuItem;
    BtnImport: TToolButton;
    ToolButton18: TToolButton;
    CmdImportExcel: TAction;
    CmdSampleExcel: TAction;
    PopImport: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    CmdLichSuBieuMau: TAction;
    BinbnHSGiyt1: TMenuItem;
    QrAllowanceCo_ThueTNCN: TBooleanField;
    QrAllowanceLK_Co_ThueTNCN: TBooleanField;
    QrDMNVCCCD_Idx: TIntegerField;
    QrDMNVHoChieu_Idx: TIntegerField;
    QrDMNVMaTK_Idx: TIntegerField;
    QrDMNVIdxQTLV_VaoLam: TIntegerField;
    QrDMNVIdxQTLV_ThoiViec: TIntegerField;
    QrDMNVIdxQTLV: TIntegerField;
    QrAllowanceIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMNVBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMNVBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMNVBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMNVAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDMNVAfterInsert(DataSet: TDataSet);
    procedure QrDMNVMANVChange(Sender: TField);
    procedure CmdDmtkExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMNVNGAY_VAOLAMChange(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure CmdThoiviecExecute(Sender: TObject);
    procedure BtnChitietClick(Sender: TObject);
    procedure QrDMNVHOChange(Sender: TField);
    procedure QrDMNVNGAYSINHChange(Sender: TField);
    procedure QrDMNVHuuTriNamChange(Sender: TField);
    procedure CmdNoiCapBHYTExecute(Sender: TObject);
    procedure CmdNoiCapBHXHExecute(Sender: TObject);
    procedure CmdNoiCapCCCDExecute(Sender: TObject);
    procedure CmdHoChieuLoaiExecute(Sender: TObject);
    procedure CmdDangTinhTrangExecute(Sender: TObject);
    procedure CmdDoanTinhTrangExecute(Sender: TObject);
    procedure CmdPhongBanExecute(Sender: TObject);
    procedure CmdChucDanhExecute(Sender: TObject);
    procedure CmdNoiLamViecExecute(Sender: TObject);
    procedure CmdNguonTuyenDungExecute(Sender: TObject);
    procedure CmdNoiDangKyKhamBenhExecute(Sender: TObject);
    procedure CmdQuaTrinhLamViecExecute(Sender: TObject);
    procedure CbTinh1NotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure CbTinh2NotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure QrDMNVThuongTru_MaTinhChange(Sender: TField);
    procedure QrDMNVCuTru_MaTinhChange(Sender: TField);
    procedure CbOrgChange(Sender: TObject);
    procedure QrDMNVMaPhongBanChange(Sender: TField);
    procedure QrDMNVAfterScroll(DataSet: TDataSet);
    procedure CmdHopDongExecute(Sender: TObject);
    procedure CmdKhenThuongKyLuatExecute(Sender: TObject);
    procedure CmdQuanHeGiaDinhExecute(Sender: TObject);
    procedure CmdKienThucTaiLieuExecute(Sender: TObject);
    procedure CmdDiCongTacExecute(Sender: TObject);
    procedure QrAllowanceBeforePost(DataSet: TDataSet);
    procedure QrAllowanceAfterInsert(DataSet: TDataSet);
    procedure QrAllowanceSoThangChange(Sender: TField);
    procedure QrAllowanceBeforeInsert(DataSet: TDataSet);
    procedure QrAllowanceBeforeDelete(DataSet: TDataSet);
    procedure QrAllowanceBeforeEdit(DataSet: TDataSet);
    procedure QrAllowanceBeforeOpen(DataSet: TDataSet);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrDMNVMaTK_IdxChange(Sender: TField);
    procedure CmdTaiKhoanTruyCapExecute(Sender: TObject);
    procedure CmdQuocTichExecute(Sender: TObject);
    procedure CmdTonGiaoExecute(Sender: TObject);
    procedure CmdHonNhanExecute(Sender: TObject);
    procedure CmdDanTocExecute(Sender: TObject);
    procedure CmdVanHoaExecute(Sender: TObject);
    procedure CmdLoaiQuaTrinhLamViecExecute(Sender: TObject);
    procedure CmdLoaiPhuCapExecute(Sender: TObject);
    procedure CmdViTriDiaLyExecute(Sender: TObject);
    procedure QrDMNVThuongTru_MaHuyenChange(Sender: TField);
    procedure QrDMNVCuTru_MaHuyenChange(Sender: TField);
    procedure CmdThongTinKhacExecute(Sender: TObject);
    procedure CmdNhomMauExecute(Sender: TObject);
    procedure CmdChiBoDangExecute(Sender: TObject);
    procedure QrDMNVMaBacLuongChange(Sender: TField);
    procedure QrDMNVMaChucDanhChange(Sender: TField);
    procedure CmdTruocVaoCongTyExecute(Sender: TObject);
    procedure CmdTaiSanThietBiExecute(Sender: TObject);
    procedure QrDMNVCalcFields(DataSet: TDataSet);
    procedure QrDMNVEMAILValidate(Sender: TField);
    procedure QrDMNVNgayThoiViecChange(Sender: TField);
    procedure QrDMNVMaQTLV_VaoLamChange(Sender: TField);
    procedure QrDMNVMaQTLV_ThoiViecChange(Sender: TField);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdNgayKyNiemExecute(Sender: TObject);
    procedure QrDMNVMABPChange(Sender: TField);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CbCuTruQuanHuyenDropDown(Sender: TObject);
    procedure CbCuTruPhuongXaDropDown(Sender: TObject);
    procedure CbThuongTruQuanHuyenDropDown(Sender: TObject);
    procedure CbThuongTruPhuongXaDropDown(Sender: TObject);
    procedure CbBoPhanDropDown(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrDMNVMA_NHOMLVChange(Sender: TField);
    procedure QrDMNVXetDuyet_ManvChange(Sender: TField);
    procedure QrDMNVXacNhan_ManvChange(Sender: TField);
    procedure CmdHinhAnhExecute(Sender: TObject);
    procedure CmdCCCDExecute(Sender: TObject);
    procedure QrDMNVCCCD_IdxChange(Sender: TField);
    procedure CmdHoChieuExecute(Sender: TObject);
    procedure QrDMNVHoChieu_IdxChange(Sender: TField);
    procedure CmdNewListExecute(Sender: TObject);
    procedure BtnPrintClick(Sender: TObject);
    procedure CmdPrintFormExecute(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdSampleExcelExecute(Sender: TObject);
    procedure CmdLichSuBieuMauExecute(Sender: TObject);
    procedure QrAllowanceMaPhuCapChange(Sender: TField);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, bComboboxThamNien: Boolean;
    mFilter, mNhomDacBiet, mManv_Length: Integer;
    mSQL, fStr, mManv: String;
    rProfile: WORD;
    function  AllocMANV(pLen: Integer): String;
    procedure expendCollapsed(bExpend: Boolean);
    function DeleteAllRecordByTransNo(pTransNo: Integer; pIsError: Boolean = false): Boolean;
  public
  	function Execute(r: WORD; pManv: string = ''): Boolean;
    function kiemTraNgayTraHopLe: Boolean;
    function layNgaySuDungMaxTaiKhoanNganHang: Boolean;
  end;

var

  FrmHoSonv: TFrmHoSonv;

implementation

uses
	ExCommon, isDb, isStr,  isMsg, Rights, MainData, RepEngine, isLib, isCommon, DmTK_NV,
    DmnvThoiviec, DmHotro_HR, DmPhongban, DmPhongbanNotCN, DmChucvu, DmDiaDiemLamViec,
    DmNhomlv, DmNguonTuyenDung, DmNoichuabenh, HopdongLaodong, KhenThuongKyLuat,
    HoSonvThoiviec, HoSonvQuanHeGiaDinh, HoSonvKienThucTaiLieu, HosonvTaiKhoanTruyCap, DmQTLV,
    DmPhucap, Dmdl, HosonvDesc, HoSonvTruocKhiVaoCongTy,
    HoSonvNgayKyNiem, HosonvThongTinKhac, ThietBiTaiSan,
    LichSuCongTac, QuaTrinhLamViec, ScanAvatar, HosonvCCCD, HosonvHoChieu,
    HoSonvPhuCapDangky, HoSonvPhuCapDanhSach, HrData, OfficeData, HoSonvBieuMau,
    ExcelData, isFile, HoSonvDanhSachImport, HoSonvLichSuBieuMau;

{$R *.DFM}

const
    TABLE_NAME = 'HR_DM_NHANVIEN';
    FORM_CODE = 'HR_DM_NHANVIEN';
    REPORT_NAME = FORM_CODE;
(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
function TFrmHoSonv.Execute;
begin
    rProfile := r;
	mCanEdit := rCanEdit(r);

    mManv := pManv;
	EdGhichu.ReadOnly := not mCanEdit;

    mRet := False;
    ShowModal;
    Result := mRet;
    CloseDataSets([QrDMNV]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmHoSonv.FormCreate(Sender: TObject);
var
    bThongTinLuong, bThongTinHeSoLuong: Boolean;
begin
    TMyForm(Self).Init2;
    SetTab0(Self);
    AddFields(QrDMNV, TABLE_NAME);

    mTrigger := False;

    PaGhiChu.Collapsed := RegReadBool(name, 'PD5');
    PaBaoHiem.Collapsed := RegReadBool(name, 'PD2');
    mFilter := 0;
    mNhomDacBiet := -1;

    bThongTinLuong := GetRights('HR_NV_LUONG', False) <> R_DENY;
    PaThongTinLuong.Visible := bThongTinLuong;

    bThongTinHeSoLuong := FlexConfigBool(FORM_CODE, 'Panel_HeSoLuong');
    PaHeSoLuong.Visible := bThongTinLuong and bThongTinHeSoLuong;

    mAutoCode := FlexConfigBool(FORM_CODE, 'ManvQL_AutoCode');
    if mAutoCode then
    begin
        EdMa.ReadOnly := True;
        EdMa.Color := clBtnFace;
        EdMa.EmptyDataInfo.Color := clBtnFace;
    end;

    fixCode := SetCodeLength(FORM_CODE, QrDMNV.FieldByName('ManvQL'));
    mManv_Length := FlexConfigInteger(FORM_CODE, 'Manv_Length');
    bComboboxThamNien := FlexConfigBool(FORM_CODE, 'Combobox_ThamNien');
    lbThamNien.Visible := bComboboxThamNien;
    CbThamNien.Visible := bComboboxThamNien;

    PaChinhQuyen.Visible := FlexConfigBool(FORM_CODE, 'Panel_ChinhQuyen');

    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);

    if sysIsDataAccess then
        FlexOrgComboDataAccess(CbOrg)
    else
      	FlexOrgCombo(CbOrg);
    CbOrg.OnChange(CbOrg);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.FormShow(Sender: TObject);
var
	s, s2: String;
begin

    with DataMain do
        OpenDataSets([QrSite, QrNganhang, QrNganhangCN, QrTinh, QrHuyen, QrPhuongXa]);


    with HrDataMain do
        OpenDataSets([QrDMPHONGBAN, QrDM_CHUCDANH, QrDM_NHOMLV,
            QrLOAI_GIOITINH, QrV_QUOCTICH, QrV_DANTOC, QrV_TONGIAO,
            QrV_VANHOA, QrDMBOPHAN,QrDM_NOILV, QrV_HR_NHANVIEN_DOITUONG,
            QrDM_NGUONTUYENDUNG, QrDM_LYDO_TUYENDUNG, QrV_HONNHAN,
            QrDMNOIKHAMBENH, QrV_BHXH_NOICAP, QrV_BHYT_NOICAP,
            QrV_HR_CCCD_NOICAP, QrV_HR_DANG_TINHTRANG, QrV_HR_DOAN_TINHTRANG,
            QrDM_HR_PHUCAP, QrV_HR_NHOMMAU, QrV_HR_DANG_CHIBO,QrDMTK_HR, QrV_HR_DM_NHANVIEN_CCCD,
            QrV_HR_DM_NHANVIEN_HOCHIEU]);

    OpenDataSets([QrEmp, QrSite, QrDep, QrSec,  QrHuyen, QrPhuongXa, QrTaiKhoanNV, QrTaiKhoanNV_MANV,
    QrV_HR_DM_NHANVIEN_CCCD_MANV, QrV_HR_DM_NHANVIEN_HOCHIEU_MANV]);

    SetDisplayFormat(HrDataMain.QrDM_CHUCDANH, sysCurFmt);

    SetDisplayFormat(QrDMNV, sysCurFmt);
    SetShortDateFormat(QrDMNV, ShortDateFormat);
    SetDisplayFormat(QrDMNV, ['HeSoLuong', 'HeSoPhuCap'], sysFloatFmtTwo);

    SetDisplayFormat(QrAllowance, sysCurFmt);
    SetDisplayFormat(QrAllowance, ['SoThang'], '#,##0;-#,##0;0');
    SetShortDateFormat(QrAllowance);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_PHUCAP'], [GrList, GrAllowance]);
    SetDictionary([QrDMNV, QrAllowance], [FORM_CODE, FORM_CODE + '_PHUCAP'], [Filter, nil]);

    s := FlexConfigString('HR_NHANVIEN_LUONG', 'Fields');
    exConfigVertGrid(grThongTinLuong, s);

    s2 := FlexConfigString('HR_NHANVIEN_LUONG', 'Fields_HeSoLuong');
    exConfigVertGrid(grHeSoLuong, s2);

	mSQL := QrDMNV.SQL.Text;
    if sysIsDataAccess then
        mSQL := mSQL + Format(' where Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID])
    else
        mSQL := mSQL + ' where 1=1 ';

    if mManv <> '' then
    begin
        CbOrg.Text := '';
        rgObsolete.ItemIndex := 2;

        PaOrg.Visible := False;
        PaSearch.Visible := False;
        mSQL := mSQL + ' and [Manv]=''' + mManv + '''';
    end;

    CmdReload.Execute;
    GrList.SetFocus;

    if mManv <> '' then
    begin
        QrDMNV.Locate('Manv', mManv, []);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrDMNV.FieldByName('NgayThoiViec').AsFloat > 10 then
    begin
	    AFont.Color := clRed;
        Exit;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;

	//Save state panel
    RegWrite(Name, ['PD5','PD2'],
        [PaGhiChu.Collapsed,PaBaoHiem.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMNV, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNewExecute(Sender: TObject);
begin
    QrDMNV.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
    PgMain.ActivePageIndex := 0;
    expendCollapsed(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNewListExecute(Sender: TObject);
var
    mTransNo: Integer;
begin
    mTransNo := 0;
    Application.CreateForm(TFrmHoSonvPhuCapDangky, FrmHoSonvPhuCapDangky);
    if not FrmHoSonvPhuCapDangky.Execute(rProfile, FORM_CODE, mTransNo) then
        Exit;

    if mTransNo <> 0 then
    begin
        QrAllowance.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNgayKyNiemExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_NV_KYNIEM');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvNgayKyNiem, FrmHoSonvNgayKyNiem);
    FrmHoSonvNgayKyNiem.Execute (r, mEmpID, mEmpIDLabel, mEmpName);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNguonTuyenDungExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_NGUON_TUYENDUNG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNguonTuyenDung, FrmDmNguonTuyenDung);
    FrmDmNguonTuyenDung.Execute(r);
    HrDataMain.QrDM_NGUONTUYENDUNG.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNhomMauExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_NHOMMAU');

    HrDataMain.QrV_HR_NHOMMAU.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNoiCapBHXHExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_BHXH_NOICAP');

    HrDataMain.QrV_BHXH_NOICAP.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNoiCapBHYTExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_BHYT_NOICAP');

    HrDataMain.QrV_BHYT_NOICAP.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNoiCapCCCDExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_CCCD_NOICAP');

    HrDataMain.QrV_HR_CCCD_NOICAP.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNoiDangKyKhamBenhExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_DK_KHAMBENH');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmNoichuabenh, FrmDmNoichuabenh);
    FrmDmNoichuabenh.Execute(r);
    HrDataMain.QrDMNOIKHAMBENH.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdNoiLamViecExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_NOI_LAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmDiaDiemLamViec, FrmDmDiaDiemLamViec);
    FrmDmDiaDiemLamViec.Execute(r);
    HrDataMain.QrDM_NOILV.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdSampleExcelExecute(Sender: TObject);
begin
     openImportFile(Handle, 'IMP_HR_DM_NHANVIEN_EXCEL');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdSaveExecute(Sender: TObject);
begin
    QrDMNV.Post;
    with QrAllowance do
    if Active then
    begin
        CheckBrowseMode;
        UpdateBatch;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdCancelExecute(Sender: TObject);
begin
    with QrAllowance do
    if Active then
        CancelBatch;
    QrDMNV.Cancel;
    mNhomDacBiet := -1;
end;

procedure TFrmHoSonv.CmdCCCDExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
begin
    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;


    Application.CreateForm(TFrmHosonvCCCD, FrmHosonvCCCD);
    if FrmHosonvCCCD.Execute(rProfile, mEmpID, mEmpIDLabel, mEmpName, False) then
    begin
        QrV_HR_DM_NHANVIEN_CCCD_MANV.Requery;
        HrDataMain.QrV_HR_DM_NHANVIEN_CCCD.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdChiBoDangExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_DANG_CHIBO');

    HrDataMain.QrV_HR_DANG_CHIBO.Requery;
end;

procedure TFrmHoSonv.CmdChucDanhExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('SZ_PUB_DM_CHUCVU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChucvu, FrmDmChucvu);
    FrmDmChucvu.Execute(r);

    HrDataMain.QrDM_CHUCDANH.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDangTinhTrangExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_DANG_TINHTRANG');

    HrDataMain.QrV_HR_DANG_TINHTRANG.Requery;
end;

procedure TFrmHoSonv.CmdDanTocExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_DANTOC');
    HrDataMain.QrV_DANTOC.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDelDetailExecute(Sender: TObject);
begin
    QrAllowance.Delete;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDelExecute(Sender: TObject);
begin
    QrDMNV.Delete;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDiCongTacExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_CONGTAC');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmLichSuCongTac, FrmLichSuCongTac);
    FrmLichSuCongTac.Execute (r, mEmpID, mEmpIDLabel, mEmpName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDmtkExecute(Sender: TObject);
var
    pMatk, pManv: String;
begin
    with QrDMNV do
    begin
        pManv := FieldByName('Manv').AsString;
        Application.CreateForm(TFrmDmTK_NV, FrmDmTK_NV);
        if FrmDmTK_NV.Execute(rProfile, pManv, False) then
        begin
            QrTaiKhoanNV.Requery;
            QrTaiKhoanNV_MANV.Requery;
        end;
        if State in [dsEdit] then
        begin
            layNgaySuDungMaxTaiKhoanNganHang;
        end;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdDoanTinhTrangExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_DOAN_TINHTRANG');

    HrDataMain.QrV_HR_DOAN_TINHTRANG.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdEditExecute(Sender: TObject);
begin
	QrDMNV.Edit;
    if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
    expendCollapsed(False);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdPhongBanExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('SZ_PUB_DM_PHONGBAN');
    if r = R_DENY then
    	Exit;

	if sysIsHrChinhanh then
    begin
        Application.CreateForm(TFrmDmPhongban, FrmDmPhongban);
        FrmDmPhongban.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmPhongbanNotCN, FrmDmPhongbanNotCN);
        FrmDmPhongbanNotCN.Execute(r);
    end;

    QrDep.Requery;
    QrSec.Requery;
    HrDataMain.QrDMBOPHAN.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdPrintExecute(Sender: TObject);
begin
    ShowReport(Caption, REPORT_NAME, [sysLogonUID, mFilter, fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdPrintFormExecute(Sender: TObject);
var
    mEmpID, EmpIDLabel, mName: String;
begin
    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        EmpIDLabel := FieldByName('ManvQL').AsString;
        mName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvBieuMau, FrmHoSonvBieuMau);
    FrmHoSonvBieuMau.Execute(rProfile, mEmpID, EmpIDLabel, mName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdQuanHeGiaDinhExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_KHENTHUONG_KL');
    if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

    Application.CreateForm(TFrmHoSonvQuanHeGiaDinh, FrmHoSonvQuanHeGiaDinh);
    FrmHoSonvQuanHeGiaDinh.Execute(r, mEmpID, mEmpIDLabel, mEmpName);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdQuaTrinhLamViecExecute(Sender: TObject);
var
    mEmpID, EmpIDLabel, mName: String;
    r: Word;
begin
    r := GetRights('HR_QUATRINH_LV');
	if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        EmpIDLabel := FieldByName('ManvQL').AsString;
        mName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmQuaTrinhLamViec, FrmQuaTrinhLamViec);
    FrmQuaTrinhLamViec.Execute(r, mEmpID, EmpIDLabel, mName);

    exReSyncRecord(QrDMNV);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdQuocTichExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_QUOCTICH');
    HrDataMain.QrV_QUOCTICH.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMNV)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdTaiKhoanTruyCapExecute(Sender: TObject);
var
	r: WORD;
    mEmpID, mEmpIDLabel, mName, mEmail: String;
begin
	r := GetRights('HR_NV_TAIKHOAN');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mName := FieldByName('Tennv').AsString;
        mEmail := FieldByName('Email').AsString;
    end;

	Application.CreateForm(TFrmHosonvTaiKhoanTruyCap, FrmHosonvTaiKhoanTruyCap);
    FrmHosonvTaiKhoanTruyCap.Execute(mEmpID, mEmpIDLabel, mName, mEmail);
    DataMain.QrUSER.Requery;
    exReSyncRecord(QrDMNV);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdTaiSanThietBiExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_THIETBI_DUNGCU');
	if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmThietBiTaiSan, FrmThietBiTaiSan);
    FrmThietBiTaiSan.Execute(r, mEmpID, mEmpIDLabel, mEmpName);
end;
 (*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdThoiviecExecute(Sender: TObject);
var
	r: WORD;
    mEmpID, mEmpIDLabel, mEmpName: String;
begin
	r := GetRights('HR_NV_NGHIVIEC');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvThoiviec, FrmHoSonvThoiviec);
    if not FrmHoSonvThoiviec.Execute (r, mEmpID, mEmpIDLabel, mEmpName) then
    	Exit;

    exReSyncRecord(QrDMNV);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdThongTinKhacExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
begin
	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHosonvThongTinKhac, FrmHosonvThongTinKhac);
    if not FrmHosonvThongTinKhac.Execute (rProfile, mEmpID, mEmpIDLabel, mEmpName) then
    	Exit;

    exReSyncRecord(QrDMNV);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdTonGiaoExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_TONGIAO');
    HrDataMain.QrV_TONGIAO.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdTruocVaoCongTyExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_NV_CONGTY_TRUOC');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvTruocKhiVaoCongTy, FrmHoSonvTruocKhiVaoCongTy);
    FrmHoSonvTruocKhiVaoCongTy.Execute (r, mEmpID, mEmpIDLabel, mEmpName);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdVanHoaExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_VANHOA');
    HrDataMain.QrV_VANHOA.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdViTriDiaLyExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r);

    DataMain.QrTinh.Requery;
    DataMain.QrHuyen.Requery;
    DataMain.QrPhuongXa.Requery;
    QrHuyen.Requery;
    QrPhuongXa.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdHinhAnhExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmScanAvatar, FrmScanAvatar);
    FrmScanAvatar.Execute (DsDMNV, 'Manv');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdHoChieuExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
begin
    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

    Application.CreateForm(TFrmHosonvHoChieu, FrmHosonvHoChieu);
    if FrmHosonvHoChieu.Execute(rProfile, mEmpID, mEmpIDLabel, mEmpName, False) then
    begin
        QrV_HR_DM_NHANVIEN_HOCHIEU_MANV.Requery;
        HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdHoChieuLoaiExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_HOCHIEU_LOAI');

    HrDataMain.QrV_HR_HOCHIEU_LOAI.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdHonNhanExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_HONNHAN');
    HrDataMain.QrV_HONNHAN.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdKienThucTaiLieuExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_NV_TAILIEU');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvKienThucTaiLieu, FrmHoSonvKienThucTaiLieu);
    FrmHoSonvKienThucTaiLieu.Execute (r, mEmpID, mEmpIDLabel, mEmpName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdLichSuBieuMauExecute(Sender: TObject);
var
    mEmpID, EmpIDLabel, mName: String;
begin
    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        EmpIDLabel := FieldByName('ManvQL').AsString;
        mName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmHoSonvLichSuBieuMau, FrmHoSonvLichSuBieuMau);
    FrmHoSonvLichSuBieuMau.Execute(rProfile, mEmpID, EmpIDLabel, mName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdLoaiPhuCapExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);

    HrDataMain.QrDM_HR_PHUCAP.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdLoaiQuaTrinhLamViecExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_DM_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmQTLV, FrmDmQTLV);
    FrmDmQTLV.Execute(r);

    HrDataMain.QrDM_LYDO_TUYENDUNG.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdHopDongExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_HOPDONG_LD');
	if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;


	Application.CreateForm(TFrmHopdongLaodong, FrmHopdongLaodong);
    FrmHopdongLaodong.Execute(r, mEmpID, mEmpIDLabel, mEmpName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdImportExcelExecute(Sender: TObject);
var
    sFile, mTableNameImport: String;
    b: Boolean;
    rs, mTransNo: Integer;
begin
    mTableNameImport := 'IMP_HR_DM_NHANVIEN';
    mTransNo := DataMain.GetSeqValue(mTableNameImport);

	sFile := isGetOpenFileName('XLSX;XLS');
    if sFile = '' then
        Exit;

    if not DataExcel.ExcelImport('IMP_' + FORM_CODE, sFile, mTableNameImport,
        'spIMP_HR_DM_NHANVIEN_ImportListExcel;1', 'ManvQL',
        [sysLogonUID, TGuidEx.ToString(DataMain.GetNewGuid), FORM_CODE + '_EXCEL', sysAppSource, mTransNo, '', 0], mTransNo, False) then
    begin
        //xóa dữ liệu excel đã tự động insert
        DeleteAllRecordByTransNo(mTransNo);
        mTransNo := 0;
    end;

    if mTransNo <> 0 then
    begin
        Application.CreateForm(TFrmHoSonvDanhSachImport, FrmHoSonvDanhSachImport);
        if not FrmHoSonvDanhSachImport.Execute(rProfile, mTransNo) then
            Exit;


        HrDataMain.QrV_HR_DM_NHANVIEN_CCCD.Requery;
        HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU.Requery;
        QrTaiKhoanNV.Requery;
        CmdReload.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdKhenThuongKyLuatExecute(Sender: TObject);
var
    mEmpID, mEmpIDLabel, mEmpName: String;
    r: Word;
begin
    r := GetRights('HR_KHENTHUONG_KL');
    if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mEmpIDLabel := FieldByName('ManvQL').AsString;
        mEmpName := FieldByName('Tennv').AsString;
    end;

    Application.CreateForm(TFrmKhenThuongKyLuat, FrmKhenThuongKyLuat);
    FrmKhenThuongKyLuat.Execute(r, mEmpID, mEmpIDLabel, mEmpName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bInsert, bEmptyAllowance: Boolean;
begin
    exActionUpdate(ActionList, QrDMNV, Filter, mCanEdit);

	with QrDMNV do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bEmpty := IsEmpty;

    end;
    CmdHinhAnh.Enabled := bBrowse;
    BtnChitiet.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
    CmdReload.Enabled := bBrowse;
    CmdContact.Enabled := bBrowse and (not bEmpty);

    CmdThoiviec.Enabled := not bEmpty;
    CmdDmtk.Enabled := not bInsert and (not bEmpty);
    CmdCCCD.Enabled := not bInsert and (not bEmpty);
    CmdHoChieu.Enabled := not bInsert and (not bEmpty);
    CmdPrintForm.Enabled := not bEmpty;
//    CmdThongTinKhac.Enabled := not bBrowse;
//    CmdLienLacKhan.Enabled := not bBrowse;
//    CmdDmtk.Enabled := bBrowse and (not bEmpty);

    with QrAllowance do
    begin
    	if not Active then
        	Exit;
        bEmptyAllowance := IsEmpty;
    end;

    CmdDelDetail.Enabled := (not bEmptyAllowance) and mCanEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMNV);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_PHEPNAM_INVALID = 'Số ngày tính phép năm phải >= %f';
    RS_INVALID_HOCVIEC = 'Học việc từ ngày không hợp lệ.';
    RS_INVALID_THUVIEC = 'Thử việc từ ngày không hợp lệ.';
    RS_INVALID_NGAYVAOLAM = 'Ngày vào làm không hợp lệ.';
    RS_INVALID_NGAYLAMVIEC = 'Ngày chính thức không hợp lệ.';
    RS_INVALID_BONHIEM = 'Bổ nhiệm từ ngày không hợp lệ.';
    RS_INVALID_BIETPHAI = 'Biệt phái từ ngày không hợp lệ.';
procedure TFrmHoSonv.QrDMNVBeforePost(DataSet: TDataSet);
var
    bThoiViec: Boolean;
begin
	with QrDMNV do
    begin
    	if not mAutoCode then
        begin
            if BlankConfirm(DataSet, ['ManvQL']) then
        	    Abort;

            if fixCode then
            	if LengthConfirm(FieldByName('ManvQL')) then
                	Abort;
        end;

        if BlankConfirm(DataSet, ['Ho', 'Ten', 'MaPhongBan', 'MaChucDanh',
            'MaQTLV_VaoLam', 'NgayVaoLam', 'NgayThamNien', 'MaNhomLV', 'DoiTuong']) then
        	Abort;

        if ((FieldByName('NgayKetThucHocViec').AsFloat > 10) and
             (FieldByName('NgayKetThucHocViec').AsDateTime < FieldByName('NgayBatDauHocViec').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_HOCVIEC);
            try
                DpHocViecTuNgay.SelectAll;
                DpHocViecTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('NgayKetThucHocViec').AsFloat > 10) and  (FieldByName('NgayBatDauThuViec').AsFloat > 10) and
             (FieldByName('NgayBatDauThuViec').AsDateTime < FieldByName('NgayKetThucHocViec').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_THUVIEC);
            try
                DpTuNgay.SelectAll;
                DpTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('NgayKetThucThuViec').AsFloat > 10) and
             (FieldByName('NgayKetThucThuViec').AsDateTime < FieldByName('NgayBatDauThuViec').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_THUVIEC);
            try
                DpTuNgay.SelectAll;
                DpTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('NgayVaoLamChinhThuc').AsFloat > 10) and  (FieldByName('NgayKetThucThuViec').AsFloat > 10) and
             (FieldByName('NgayKetThucThuViec').AsDateTime > FieldByName('NgayVaoLamChinhThuc').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_NGAYLAMVIEC);
            try
                DpChinhThucTuNgay.SelectAll;
                DpChinhThucTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('NgayVaoLamChinhThuc').AsFloat > 10) and  (FieldByName('NgayKetThucHocViec').AsFloat > 10) and
             (FieldByName('NgayKetThucHocViec').AsDateTime > FieldByName('NgayVaoLamChinhThuc').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_NGAYLAMVIEC);
            try
                DpChinhThucTuNgay.SelectAll;
                DpChinhThucTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('BoNhiem_NgayKetThuc').AsFloat > 10) and
                (FieldByName('BoNhiem_NgayKetThuc').AsDateTime < FieldByName('BoNhiem_NgayBatDau').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_BONHIEM);
            try
                DpBoNhiemTuNgay.SelectAll;
                DpBoNhiemTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('BietPhai_NgayKetThuc').AsFloat > 10) and
                (FieldByName('BietPhai_NgayKetThuc').AsDateTime < FieldByName('BietPhai_NgayBatDau').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_BIETPHAI);
            try
                DpBietPhaiTuNgay.SelectAll;
                DpBietPhaiTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if FieldByName('NgayThoiViec').AsFloat > 10 then
        begin
            bThoiViec := True;
            if BlankConfirm(FrmHoSonv.QrDMNV, ['MaQTLV_ThoiViec']) then
                Abort;

            if not kiemTraNgayTraHopLe then
                Abort;
        end;

        if FieldByName('MaQTLV_ThoiViec').AsString <> '' then
        begin
            bThoiViec := True;
            if BlankConfirm(FrmHoSonv.QrDMNV, ['NgayThoiViec']) then
                Abort;
        end;

        if bThoiViec and
            (FieldByName('NgayVaoLam').AsFloat > 10) and
            (not HrDataMain.kiemTraNgayHieuLucHopLe(FieldByName('Manv').AsString, 1, FieldByName('NgayVaoLam').AsDateTime)) then
            Abort;

        if bThoiViec and
            (FieldByName('NgayThoiViec').AsFloat > 10) and
            (not HrDataMain.kiemTraNgayHieuLucHopLe(FieldByName('Manv').AsString, 2, FieldByName('NgayThoiViec').AsDateTime)) then
            Abort;


        if mAutoCode then
        begin
            if State in [dsInsert] then
            	FieldByName('ManvQL').AsString := HrDataMain.AllocManvQL(FieldByName('MaNhomLV').AsString);
        end;

        if State in [dsInsert] then
            	FieldByName('Manv').AsString := AllocMANV(mManv_Length);
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVCalcFields(DataSet: TDataSet);
begin
    with QrDMNV do
    begin
        FieldByName('CALC_TongPhepNam').AsFloat := exVNDRound(
                FieldByName('PhepNam_LuatDinh').AsFloat +
                FieldByName('PhepNam_ChucDanh').AsFloat +
                FieldByName('PhepNam_ThamNien').AsFloat, sysCurHrRound);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVCCCD_IdxChange(Sender: TField);
begin
    with QrDMNV do
    begin
        EdCCCDTenNhom.Text := EdCCCDTenNhom.Field.AsString;
        dpCCCDNgayCap.Text := dpCCCDNgayCap.Field.AsString;
        EdCCCDNoiCap.Text := EdCCCDNoiCap.Field.AsString;
        EdCCCDMaNoiCap.Text := EdCCCDMaNoiCap.Field.AsString;
        EdCCCDGhiChu.Text := EdCCCDGhiChu.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVCuTru_MaHuyenChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('CuTru_MaXa').Clear;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVCuTru_MaTinhChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('CuTru_MaHuyen').Clear;
        FieldByName('CuTru_MaXa').Clear;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVEMAILValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
    begin
        if not validateEmail(Sender.AsString) then
        begin
            ErrMsg(Format(RS_INVAILD_FORMAT, [Sender.DisplayLabel]));
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVHOChange(Sender: TField);
var
    _bTrigger: Boolean;
    _ho, _ten: String;
begin
    if mTrigger then
        Exit;

    _bTrigger := mTrigger;
    mTrigger := True;
    with QrDMNV do
    begin
        _ho := Trim(FieldByName('Ho').AsString);
        _ten := Trim(FieldByName('Ten').AsString);

        FieldByName('Ho').AsString := _ho;
        FieldByName('Ten').AsString := _ten;

        FieldByName('Tennv').AsString := _ho + ' ' + _ten;
    end;
    mTrigger := _bTrigger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVHoChieu_IdxChange(Sender: TField);
begin
    with QrDMNV do
    begin
        EdHoChieuTenNhom.Text := EdHoChieuTenNhom.Field.AsString;
        dpHoChieuNgayCap.Text := dpHoChieuNgayCap.Field.AsString;
        dpHoChieuNgayHetHan.Text := dpHoChieuNgayHetHan.Field.AsString;
        EdHoChieuNoiCap.Text := EdHoChieuNoiCap.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVHuuTriNamChange(Sender: TField);
var
    yy, dd: WORD;
    soNam, soThang: Integer;
    ngaySinh, ngayHuuTri: TDateTime;
begin
    with QrDMNV do
    begin
        ngaySinh := FieldByName('NgaySinh').AsDateTime;
        soNam := FieldByName('HuuTriNam').AsInteger;
        soThang := FieldByName('HuuTriThang').AsInteger;
        if (ngaySinh = 0) or (soNam * 12 + soThang = 0) then
        begin
            FieldByName('NgayHuuTri').Clear;
        end else
        begin
            DecodeDate(ngaySinh, yy, dd, dd);
            FieldByName('NamSinh').AsInteger := yy;

            ngayHuuTri := IncMonth(ngaySinh, soNam * 12 + soThang);
            FieldByName('NgayHuuTri').AsDateTime := ngayHuuTri;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaBacLuongChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('HeSoLuong').AsFloat := HrDataMain.GetHeSoLuong(Sender);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMABPChange(Sender: TField);
begin
    with QrDMNV do
    begin
        EdMaBoPhan_BanDau.Text := EdMaBoPhan_BanDau.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMANVChange(Sender: TField);
var
    mMa, s: String;
    b: Boolean;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	with QrDMNV do
    begin
		b := FieldByName('MaCC').AsString = '';
    	if not b then
        	b := YesNo(RS_CODE_DEF);
		if b then
			FieldByName('MaCC').AsString := FieldByName('Manv').AsString;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaPhongBanChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('MaBoPhan').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaQTLV_ThoiViecChange(Sender: TField);
begin
    if mNhomDacBiet < 2  then
        mNhomDacBiet := 2
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaQTLV_VaoLamChange(Sender: TField);
begin
    if mNhomDacBiet < 1  then
        mNhomDacBiet := 1
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaTK_IdxChange(Sender: TField);
begin
    EdTenNH.Text := EdTenNH.Field.AsString;
    EdTenNH_CN.Text := EdTenNH_CN.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMA_NHOMLVChange(Sender: TField);
begin
    with QrDMNV do
        if (State in [dsInsert]) and mAutoCode then
            	FieldByName('ManvQL').AsString := HrDataMain.AllocManvQL(FieldByName('MaNhomLV').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVMaChucDanhChange(Sender: TField);
begin
    with QrDMNV do
    begin
        if Sender.AsString <> '' then
        begin
           FieldByName('PhepNam_LuatDinh').AsFloat := FieldByName('LK_PhepNam_LuatDinh').AsFloat;
           FieldByName('PhepNam_ChucDanh').AsFloat := FieldByName('LK_PhepNam_ChucDanh').AsFloat;
           FieldByName('PhepNam_ThamNien').AsFloat := FieldByName('LK_PhepNam_ThamNien').AsFloat;
           FieldByName('Co_PhepNamMotThang').AsBoolean := FieldByName('LK_Co_PhepNamMotThang').AsBoolean;
        end
        else
        begin
           FieldByName('PhepNam_LuatDinh').Clear;
           FieldByName('PhepNam_ChucDanh').Clear;
           FieldByName('PhepNam_ThamNien').Clear;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVNGAYSINHChange(Sender: TField);
var
    yy, xx: WORD;
begin
    DecodeDate(Sender.AsDateTime, yy, xx, xx);
    QrDMNV.FieldByName('NamSinh').AsInteger := yy;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVNgayThoiViecChange(Sender: TField);
begin
    QrDMNVMaQTLV_ThoiViecChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVNGAY_VAOLAMChange(Sender: TField);
begin
    with QrDMNV do
    begin
        if (not bComboboxThamNien) or (FieldByName('NgayThamNien').IsNull) then
            FieldByName('NgayThamNien').AsDateTime := FieldByName('NgayVaoLam').AsDateTime;
    end;
    QrDMNVMaQTLV_VaoLamChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVThuongTru_MaHuyenChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('ThuongTru_MaXa').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVThuongTru_MaTinhChange(Sender: TField);
begin
    with QrDMNV do
    begin
        FieldByName('ThuongTru_MaHuyen').Clear;
        FieldByName('ThuongTru_MaXa').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVXacNhan_ManvChange(Sender: TField);
begin
    EdXacNhan_ManvQL.Text := EdXacNhan_ManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVXetDuyet_ManvChange(Sender: TField);
begin
    EdXetDuyet_ManvQL.Text := EdXetDuyet_ManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 567;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbThuongTruPhuongXaDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDMNV.FieldByName('ThuongTru_MaHuyen').AsString;
    QrPhuongXa.Filter := 'MaHuyen=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbThuongTruQuanHuyenDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDMNV.FieldByName('ThuongTru_MaTinh').AsString;
    QrHuyen.Filter := 'MATINH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbTinh1NotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbTinh2NotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVAfterPost(DataSet: TDataSet);
begin
	mRet := True;
    with QrDMNV do
    begin
        HrDataMain.SyncQuaTrinh(FieldByName('Manv').AsString, mNhomDacBiet, 0);
    end;
    mNhomDacBiet := -1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVAfterScroll(DataSet: TDataSet);
begin
    with QrAllowance do
    begin
        if not Active then
            Open
        else if FieldByName('Manv').AsString <> QrDMNV.FieldByName('Manv').AsString then
        begin
            Close;
            Open;
        end;
    end;
end;

(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint

end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.Panels[0].Text := exRecordCount(QrDMNV, Filter);
    Status.Panels[2].Text := exStatusAudit(QrDMNV);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.BtnChitietClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.BtnImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.BtnPrintClick(Sender: TObject);
begin
     (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbOrgChange(Sender: TObject);
begin
    PaOrg.HeaderCaption := exGetFlexOrgDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbBoPhanDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDMNV.FieldByName('MaPhongBan').AsString;
    QrSec.Filter := 'MaPhongBan=''' + s + '''';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbCuTruPhuongXaDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDMNV.FieldByName('CuTru_MaHuyen').AsString;
    QrPhuongXa.Filter := 'MaHuyen=''' + s + '''';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CbCuTruQuanHuyenDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDMNV.FieldByName('CuTru_MaTinh').AsString;
    QrHuyen.Filter := 'MATINH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmHoSonv.AllocMANV(pLen: Integer): String;
begin
    with ALLOC_MANV do
    begin
        Prepared := True;
        Parameters[1].Value := pLen;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s, sSearch: String;
    fLevel, fFilter: Integer;
begin
    if CbOrg.Text = '' then
        s := ''
    else
    begin
        s := CbOrg.SelectedNode.StringData;
        fLevel := CbOrg.SelectedNode.Level;
	end;
    fFilter := rgObsolete.ItemIndex;

//    sSearch := DataMain.StripToneMark(EdSearch.Text);

    if (fStr <> s) or
        (fFilter <> mFilter) then
    begin
        fStr := s;
        mFilter := fFilter;
        with QrDMNV do
        begin
            s := Sort;
            Close;
            sSQL := mSQL;

            if mFilter = 0 then
                sSQL := sSQL + ' and isnull(NgayThoiViec, 0) <= 0'
            else if mFilter = 1 then
                sSQL := sSQL + ' and isnull(NgayThoiViec, 0) > 0';

            if fStr <> '' then
                case fLevel of
                0:
                    sSQL := sSQL + ' and [MaChiNhanh]=''' + fStr + '''';
                1:
                    sSQL := sSQL + ' and [MaPhongBan]=''' + fStr + '''';
                2:
                    sSQL := sSQL + ' and [MaBoPhan]=''' + fStr + '''';
            end;

            SQL.Text := sSQL;
            SQL.Add(' order by 	Manv');
            Open;
        end;
        if s = '' then
            s := 'ManvQL';

        SortDataSet(QrDMNV, s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.CmdReloadExecute(Sender: TObject);
begin
    fStr := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceAfterInsert(DataSet: TDataSet);
begin
    with QrAllowance do
    begin
        FieldByName('Co_GioiHan').AsBoolean := False;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
        FieldByName('Manv').AsString := QrDMNV.FieldByName('Manv').AsString;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;

    SetEditState(QrDMNV);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceBeforeEdit(DataSet: TDataSet);
begin
    with QrDMNV do
    if (State in [dsBrowse]) then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceBeforeInsert(DataSet: TDataSet);
begin
    with QrDMNV do
    if (State in [dsBrowse]) then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceBeforeOpen(DataSet: TDataSet);
begin
    QrAllowance.Parameters[0].Value := QrDMNV.FieldByName('Manv').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceBeforePost(DataSet: TDataSet);
var
    _thangBatDau, _namBatDau: Integer;
begin
    with QrAllowance do
    begin
        if BlankConfirm(QrAllowance, ['MaPhuCap']) then
            Abort;

        _thangBatDau := FieldByName('ThangBatDau').AsInteger;
        _namBatDau := FieldByName('NamBatDau').AsInteger;

        if (_thangBatDau <= 0) or (_namBatDau <= 0) then
        begin
            ErrMsg('"Tháng/ Năm bắt đầu" phải > 0');
            Abort;
        end;

        if IsDuplicateCode3(QrAllowance,
                'Trùng "Mã phụ cấp" và "Tháng/ Năm bắt đầu".',
                'MaPhuCap;ThangBatDau;NamBatDau',
                [FieldByName('MaPhuCap').AsString, _thangBatDau, _namBatDau],
                True) then
            Abort;

        if FieldByName('Co_GioiHan').AsBoolean then
        begin
            if FieldByName('SoThang').AsInteger <= 0 then
            begin
                ErrMsg('"Số tháng áp dụng" phải > 0');
                Abort;
            end;
        end else
        begin
            FieldByName('SoThang').Clear;
            FieldByName('ThangKetThuc').Clear;
            FieldByName('NamKetThuc').Clear;
        end;
    end;
    SetAudit(DataSet);
end;
procedure TFrmHoSonv.QrAllowanceMaPhuCapChange(Sender: TField);
begin
    with QrAllowance do
	begin
        FieldByName('Co_ThueTNCN').AsBoolean := FieldByName('LK_Co_ThueTNCN').AsBoolean;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrAllowanceSoThangChange(Sender: TField);
var
    mDay,mThang, mNam, soThang: Integer;
    nextYear, nextMonth, nextDay: Word;
    mDate, mFromDate, mToDate: TDateTime;
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrAllowance do
	begin
        mThang := FieldByName('ThangBatDau').AsInteger;
        mNam := FieldByName('NamBatDau').AsInteger;

        if FieldByName('Co_GioiHan').AsBoolean then
        begin
            if not (FieldByName('ThangBatDau').IsNull)
            and not (FieldByName('NamBatDau').IsNull) then
            begin
                soThang := FieldByName('SoThang').AsInteger;

                mDate := IncMonth(EncodeDate(mNam, mThang, 1), Iif(soThang<=0,1,soThang)) - 1;
                DecodeDate(mDate, nextYear, nextMonth, nextDay);
                FieldByName('ThangKetThuc').AsInteger := nextMonth;
                FieldByName('NamKetThuc').AsInteger := nextYear;
                GrAllowance.InvalidateCurrentRow;
            end;
        end
        else
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            FieldByName('SoThang').Clear;
            FieldByName('ThangKetThuc').Clear;
            FieldByName('NamKetThuc').Clear;
            mTrigger := bTrigger;
        end;

        if (Sender.FieldName = 'ThangBatDau') or (Sender.FieldName = 'NamBatDau') then
        begin
            if (mNam <> 0) and (mThang <> 0) then
            begin
                if HrDataMain.GetPeriodDate(mNam, mThang, mFromDate, mToDate) then
                begin
                    FieldByName('NgayBatDau').AsDateTime := mFromDate;
                end;
            end;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.QrDMNVAfterInsert(DataSet: TDataSet);
begin
	with QrDMNV do
	begin
        FieldByName('Co_MacDinhCong').AsBoolean := False;
        FieldByName('Co_BHXH').AsBoolean := False;
        FieldByName('Co_BHYT').AsBoolean := False;
        FieldByName('Co_BHTN').AsBoolean := False;
        FieldByName('Co_CongDoan').AsBoolean := False;
        FieldByName('Co_BangCong').AsBoolean := False;
        FieldByName('Co_BangLuong').AsBoolean := False;
        FieldByName('Co_ChuyenKhoan').AsBoolean := False;
        FieldByName('MaChiNhanh').AsString := sysSite;
        FieldByName('MaPhongBan').AsString := QrDep.FieldByName('MaPhongBan').AsString;
//        FieldByName('MaNhomLV').AsString := HrDataMain.QrDM_NHOMLV.FieldByName('MaNhomLV').AsString;
        FieldByName('MaChucDanh').AsString := HrDataMain.QrDM_CHUCDANH.FieldByName('MaChucDanh').AsString;
//        FieldByName('GioiTinh').AsInteger := HrDataMain.QrLOAI_GIOITINH.FieldByName('MA_HOTRO').AsInteger;
        FieldByName('Co_PhepNamMotThang').AsBoolean := False;
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonv.expendCollapsed(bExpend: Boolean);
begin
    PaToChuc.Collapsed := bExpend;
    PaTuyenDung.Collapsed := bExpend;
    PaChinhQuyen.Collapsed := bExpend;
    PaGhiChu.Collapsed := bExpend;
    PaCaNhan.Collapsed := bExpend;
    PaNoiSinhHuuTri.Collapsed := bExpend;
    PaDiaChiLienLac.Collapsed := bExpend;
    PaDangDoanThe.Collapsed := bExpend;
    PaBaoHiem.Collapsed := bExpend;
    PaGiaoDich.Collapsed := bExpend;
    PaGiayToTuyThan.Collapsed := bExpend;
    PaThongTinLuong.Collapsed := bExpend;
    PaHeSoLuong.Collapsed := bExpend;
    PaPhuCap.Collapsed := bExpend;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  TFrmHoSonv.kiemTraNgayTraHopLe: Boolean;
var
    s: String;
begin
    with spHR_LICHSU_THIETBI_Invalid_NgayTraTaiSan do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := QrDMNV.FieldByName('Manv').AsString;

        Execute;
        Result := (Parameters.ParamValues['@RETURN_VALUE'] = 0);

        if not Result then
        begin
            if (Parameters.FindParam('@pMSG') <> nil) then
                s := Parameters.ParamValues['@pMSG'];

            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonv.layNgaySuDungMaxTaiKhoanNganHang: Boolean;
begin
    with spHR_TAIKHOAN_NGANHANG_NGAYSUDUNGMAX do
    begin
        Prepared := True;
        Parameters[2].Value := QrDMNV.FieldByName('Manv').AsString;
        Execute;
        QrDMNV.FieldByName('MaTK_Idx').Value := Parameters[1].Value;
    end;
    Result := True;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonv.DeleteAllRecordByTransNo;
begin
    with spIMP_HR_DM_NHANVIEN_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;


End.