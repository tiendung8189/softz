object FrmHoSonvTruocKhiVaoCongTy: TFrmHoSonvTruocKhiVaoCongTy
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7891' S'#417' Nh'#226'n Vi'#234'n - Tr'#432#7899'c Khi V'#224'o C'#244'ng Ty'
  ClientHeight = 573
  ClientWidth = 1244
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1244
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton13: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 1244
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 77
    Width = 754
    Height = 475
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'Manv'#9'12'#9'M'#227' nh'#226'n vi'#234'n'#9'F'
      'LK_Tennv'#9'20'#9'H'#7885' t'#234'n nh'#226'n vi'#234'n'#9'F'
      'CongTy'#9'25'#9'C'#244'ng ty'#9'F'
      'ChucDanh'#9'15'#9'Ch'#7913'c danh'#9'F'
      'KinhNghiem'#9'15'#9'Kinh nghi'#7879'm'#9'F'
      'GhiChu'#9'25'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 36
    Width = 1244
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    ExplicitTop = 36
    ExplicitWidth = 1244
    inherited Panel1: TPanel
      Width = 1244
      ExplicitWidth = 1244
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 754
    Top = 77
    Width = 490
    Height = 475
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 4
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 482
      Height = 475
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PaThongTin: TisPanel
        Left = 2
        Top = 2
        Width = 478
        Height = 126
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          478
          126)
        object Label4: TLabel
          Left = 63
          Top = 26
          Width = 47
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#7915' ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 309
          Top = 26
          Width = 54
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = #272#7871'n ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 354
        end
        object EdCongTy: TDBEditEh
          Left = 116
          Top = 46
          Width = 354
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 43
          ControlLabel.Height = 16
          ControlLabel.Caption = 'C'#244'ng ty'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'CongTy'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object CbToDate: TwwDBDateTimePicker
          Left = 116
          Top = 22
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayBatDau'
          DataSource = DsMaster
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 369
          Top = 22
          Width = 101
          Height = 22
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayKetThuc'
          DataSource = DsMaster
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 1
        end
        object wwDBEdit2: TDBEditEh
          Left = 116
          Top = 70
          Width = 354
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 80
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ch'#7913'c v'#7909' ch'#237'nh'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'ChucDanh'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object wwDBEdit1: TDBEditEh
          Left = 116
          Top = 94
          Width = 354
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 104
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kinh nghi'#7879'm ch'#237'nh'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'KinhNghiem'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
      end
      object PaGhiChu: TisPanel
        Left = 2
        Top = 128
        Width = 478
        Height = 345
        Align = alClient
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitHeight = 296
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 474
          Height = 325
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
          ExplicitHeight = 276
        end
      end
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
    end
    object CmdLoaiPhuCap: TAction
      OnExecute = CmdLoaiPhuCapExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Manv'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_CONGTY'
      ' where'#9'Manv =:Manv')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrMasterLK_TENNV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterCongTy: TWideStringField
      FieldName = 'CongTy'
      Size = 200
    end
    object QrMasterChucDanh: TWideStringField
      FieldName = 'ChucDanh'
      Size = 200
    end
    object QrMasterNgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
    end
    object QrMasterNgayKetThuc: TDateTimeField
      FieldName = 'NgayKetThuc'
    end
    object QrMasterKinhNghiem: TWideStringField
      FieldName = 'KinhNghiem'
      Size = 200
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterIDX: TAutoIncField
      FieldName = 'IDX'
      ReadOnly = True
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
end
