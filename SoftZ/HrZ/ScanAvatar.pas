(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ScanAvatar;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs, ExtDlgs, Menus, ActnList, Db,
  ADODB, ExtCtrls, AdvMenus, ImgList, fcImager, EhLibVCL, DBCtrlsEh, rImageZoom,
  rDBComponents, Vcl.StdCtrls, Vcl.Buttons;

type
  TFrmScanAvatar = class(TForm)
    Action: TActionList;
    CmdAssign: TAction;
    CmdClear: TAction;
    OpenDlg: TOpenPictureDialog;
    CmdGray: TAction;
    ImageList1: TImageList;
    QrHINH: TADOQuery;
    DsHINH: TDataSource;
    Gnhnh1: TMenuItem;
    Xahnh1: TMenuItem;
    N1: TMenuItem;
    GrayScale1: TMenuItem;
    PaPic: TPanel;
    CmdExport: TAction;
    Export1: TMenuItem;
    N2: TMenuItem;
    SaveDlg: TSaveDialog;
    PopPic: TAdvPopupMenu;
    ItemDrawStyle: TMenuItem;
    ItemDraw0: TMenuItem;
    ItemDraw1: TMenuItem;
    ItemDraw2: TMenuItem;
    ItemDraw3: TMenuItem;
    ItemDraw4: TMenuItem;
    ItemDraw5: TMenuItem;
    N3: TMenuItem;
    QrHINHManv: TWideStringField;
    QrHINHFileContent: TBlobField;
    QrHINHFileExt: TWideStringField;
    Img: TrDBImage;
    BtnUpdate: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BtnZoomIn: TBitBtn;
    BtnClose: TBitBtn;
    BtnZoomOut: TBitBtn;
    BtnZoomFit: TBitBtn;
    CmdZoomIn: TAction;
    CmdZoomOut: TAction;
    CmdZoomFit: TAction;
    CmdCancel: TAction;
    BtnCancel: TBitBtn;
    BtnOK: TBitBtn;
    CmdSave: TAction;
    CmdClose: TAction;
    QrHINHFileName: TWideStringField;
    QrHINHGhiChu: TWideMemoField;
    QrHINHCREATE_BY: TIntegerField;
    QrHINHUPDATE_BY: TIntegerField;
    QrHINHCREATE_DATE: TDateTimeField;
    QrHINHUPDATE_DATE: TDateTimeField;
    QrHINHIDX: TAutoIncField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAssignExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ImgCalcPictureType(ImageControl: TfcDBImager;
      var PictureType: TfcImagerPictureType; var GraphicClassName: String);
    procedure FormCreate(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure CmdZoomInExecute(Sender: TObject);
    procedure CmdZoomOutExecute(Sender: TObject);
    procedure CmdZoomFitExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrHINHAfterInsert(DataSet: TDataSet);
    procedure QrHINHBeforePost(DataSet: TDataSet);
  private
  	nDrawStyle: Integer;
  public
  	procedure Execute(master: TDataSource; ma: String = 'Manv');
  end;

var
  FrmScanAvatar: TFrmScanAvatar;

implementation

{$R *.DFM}

uses
	jpeg, isLib, Rights, Excommon, isMsg, isDb, MainData;

const
  	cDrawStyle: array[0..5] of TfcImagerDrawStyle = (dsCenter, dsNormal, dsProportional, dsProportionalCenter, dsStretch, dsTile);

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.Execute;
begin
    with QrHINH do
    begin
    	DataSource := master;
		Open;
    end;

    // Restore saved parameters
    Left   := RegRead('GoodsPhoto', Left);
    Top    := RegRead('GoodsPhoto', Top);
    Width  := RegRead('GoodsPhoto', Width);
    Height := RegRead('GoodsPhoto', Height);

	nDrawStyle := RegRead('DrawStyle', 1);
    Show;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdAssignExecute(Sender: TObject);
var
    ext: String;
begin
   	FormStyle := fsNormal;
	if not OpenDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
        Exit;
	end;

    ext := UpperCase(ExtractFileExt(OpenDlg.FileName));
    if ext = '.JPEG' then
    	ext := '.JPG';

	with QrHINH do
    begin
    	if IsEmpty then
        	Append
		else
	        Edit;

		TBlobField(FieldByName('FileContent')).LoadFromFile(OpenDlg.FileName);
        FieldByName('FileExt').AsString := ext;
	end;

   	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdCancelExecute(Sender: TObject);
begin
     QrHINH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdClearExecute(Sender: TObject);
begin
	with QrHINH do
    begin
        Edit;
        FieldByName('FileContent').Clear;
        FieldByName('FileExt').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.ImgCalcPictureType(ImageControl: TfcDBImager;
  var PictureType: TfcImagerPictureType; var GraphicClassName: String);
var
	ext : String;
begin
	ext := QrHINH.FieldByName('FileExt').AsString;
    if ext = '.JPG' then
		PictureType := fcptJpg
    else if ext = '.BMP' then
		PictureType := fcptBitmap
    else if ext = '.WMF' then
		PictureType := fcptMetafile
    else
		PictureType := fcptBitmap;
end;

procedure TFrmScanAvatar.QrHINHAfterInsert(DataSet: TDataSet);
begin
//    with QrHINH do
//	begin
//        TGuidField(FieldByName('IDX')).AsGuid := DataMain.GetNewGuid;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.QrHINHBeforePost(DataSet: TDataSet);
begin
    with QrHINH do
    begin
        if BlankConfirm(QrHINH, ['Manv']) then
            Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdZoomInExecute(Sender: TObject);
begin
     Img.ZoomIn;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdZoomOutExecute(Sender: TObject);
begin
    Img.ZoomOut;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, bEmpty, bBrowse: Boolean;
begin
    with QrHINH do
    begin
        if not Active then
            Exit;

        b := IsEmpty;
        bBrowse := State in [dsBrowse];
        bEmpty := FieldByName('FileContent').IsNull;
    end;

	CmdClear.Enabled := not b and not bEmpty and bBrowse;
	CmdExport.Enabled := not b and not bEmpty and bBrowse;
    CmdSave.Enabled := not b and not bBrowse;
    CmdCancel.Enabled := not b and not bBrowse;
    CmdZoomIn.Enabled := not bEmpty;
    CmdZoomOut.Enabled := not bEmpty;
    CmdZoomFit.Enabled := not bEmpty;
    CmdAssign.Enabled := not (QrHINH.DataSource.DataSet.State in [dsInsert]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    RegWrite('GoodsPhoto', ['Left', 'Top', 'Width', 'Height', 'DrawStyle'],
    	[Left, Top, Width, Height, nDrawStyle]);
    try
	    QrHINH.Close;
    finally
    end;
    FrmScanAvatar := Nil;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.FormResize(Sender: TObject);
begin
	Img.Repaint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdExportExecute(Sender: TObject);
begin
    FormStyle := fsNormal;
	if not SaveDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
    	Exit;
    end;

    with QrHINH do
    begin
	    if FieldByName('FileContent').IsNull then
        begin
	    	FormStyle := fsStayOnTop;
    		Exit;
        end;

	    TBlobField(FieldByName('FileContent')).SaveToFile(
        	ChangeFileExt(SaveDlg.FileName, FieldByName('FileExt').AsString));
    end;
	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdSaveExecute(Sender: TObject);
begin
     QrHINH.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScanAvatar.CmdZoomFitExecute(Sender: TObject);
begin
    Img.Fit;
end;

end.
