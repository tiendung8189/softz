object FrmDmPhucap: TFrmDmPhucap
  Left = 169
  Top = 135
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c Lo'#7841'i Ph'#7909' C'#7845'p'
  ClientHeight = 583
  ClientWidth = 871
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 562
    Width = 871
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 871
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 36
    Width = 871
    Height = 526
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 871
      Height = 526
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'Co_BaoHiem;CheckBox;True;False'
        'Co_ThueTNCN;CheckBox;True;False'
        'PhuCapLoai;CustomEdit;CbType;F'
        'LK_TenNhomPhuCap;CustomEdit;CbNhom;F'
        'LK_PhuCapLoai;CustomEdit;CbType;F')
      Selected.Strings = (
        'LK_TenNhomPhuCap'#9'200'#9'LK_TenNhomPhuCap'#9'F'
        'LK_PhuCapLoai'#9'20'#9'LK_PhuCapLoai'#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDanhmuc
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopCommon
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      GroupFieldName = 'LK_PhuCapLoai'
    end
    object CbType: TwwDBLookupCombo
      Left = 294
      Top = 167
      Width = 93
      Height = 22
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN_HOTRO'#9'20'#9'DGIAI'#9'F')
      DataField = 'PhuCapLoai'
      DataSource = DsDanhmuc
      LookupTable = HrDataMain.QrV_HR_PHUCAP_LOAI
      LookupField = 'MA_HOTRO'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
  end
  object CbNhom: TwwDBLookupCombo
    Left = 558
    Top = 151
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TEN_HOTRO'#9'25'#9'DGIAI'#9'F')
    DataField = 'MaNhom_PhuCap'
    DataSource = DsDanhmuc
    LookupTable = HrDataMain.QrNHOM_PHUCAP
    LookupField = 'MA_HOTRO'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 3
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 84
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDanhmucBeforeInsert
    AfterInsert = QrDanhmucAfterInsert
    BeforeEdit = QrDanhmucBeforeEdit
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_PHUCAP'
      'order by'#9'PhuCapLoai, MaPhuCap')
    Left = 20
    Top = 168
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDanhmucLK_TEN_NHOMPC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhomPhuCap'
      LookupDataSet = HrDataMain.QrNHOM_PHUCAP
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'MaNhom_PhuCap'
      Size = 200
      Lookup = True
    end
    object QrDanhmucMaPhuCap: TWideStringField
      FieldName = 'MaPhuCap'
    end
    object QrDanhmucTenPhuCap: TWideStringField
      FieldName = 'TenPhuCap'
      Size = 200
    end
    object QrDanhmucTenPhuCap_TA: TWideStringField
      FieldName = 'TenPhuCap_TA'
      Size = 200
    end
    object QrDanhmucMaNhom_PhuCap: TWideStringField
      FieldName = 'MaNhom_PhuCap'
    end
    object QrDanhmucPhuCapLoai: TIntegerField
      FieldName = 'PhuCapLoai'
    end
    object QrDanhmucCo_BaoHiem: TBooleanField
      FieldName = 'Co_BaoHiem'
    end
    object QrDanhmucCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrDanhmucSoTien: TFloatField
      FieldName = 'SoTien'
    end
    object QrDanhmucLK_PhuCapLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PhuCapLoai'
      LookupDataSet = HrDataMain.QrV_HR_PHUCAP_LOAI
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'PhuCapLoai'
      Lookup = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 20
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 52
    Top = 168
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 116
    Top = 168
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
  end
end
