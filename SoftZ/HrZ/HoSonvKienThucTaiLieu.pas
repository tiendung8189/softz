﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvKienThucTaiLieu;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  frameEmp, DBCtrlsEh, isPanel, Vcl.Buttons, RzPanel, RzSplit, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2;

type
  TFrmHoSonvKienThucTaiLieu = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    Label1: TLabel;
    CbToDate: TwwDBDateTimePicker;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    PaThongTin: TisPanel;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    spHR_TINH_HETHAN_HDLD: TADOCommand;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    EdTenTaiLieu: TDBEditEh;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    frEmp1: TfrEmp;
    CmdTaiLieu: TAction;
    CmdChungChi: TAction;
    QrMasterManv: TWideStringField;
    QrMasterTenTaiLieu: TWideStringField;
    QrMasterNoiDungTaiLieu: TWideStringField;
    QrMasterTaiLieuLoai: TWideStringField;
    QrMasterNgayCap: TDateTimeField;
    QrMasterNoiCap: TWideStringField;
    QrMasterMaQuocGia: TWideStringField;
    QrMasterChungChi: TWideStringField;
    QrMasterNgayBatDau: TDateTimeField;
    QrMasterNgayKetThuc: TDateTimeField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    Label7: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    wwDBEdit2: TDBEditEh;
    QrMasterLK_TenKienThuc: TWideStringField;
    QrMasterLK_TenQuocGia: TWideStringField;
    QrMasterLK_TenChungChi: TWideStringField;
    QrMasterGhiChu: TWideMemoField;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    RzSizePanel1: TRzSizePanel;
    QrMasterUPDATE_NAME: TWideStringField;
    CmdQuocGia: TAction;
    QrMasterCalc_FileName: TWideStringField;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    CbQuocGiaCap: TDbLookupComboboxEh2;
    CbKienThuc: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    CbChungChi: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    QrMasterLK_MaKienThuc: TWideStringField;
    QrMasterLK_MaChungChi: TWideStringField;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    QrMasterFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    QrMasterIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdTaiLieuExecute(Sender: TObject);
    procedure CmdChungChiExecute(Sender: TObject);
    procedure CmdQuocGiaExecute(Sender: TObject);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterAfterDelete(DataSet: TDataSet);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel: Integer;
    mSQL, fStr, mManv: String;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String);
  end;

var
  FrmHoSonvKienThucTaiLieu: TFrmHoSonvKienThucTaiLieu;

const
    TABLE_NAME = 'HR_LICHSU_TAILIEU';
    FORM_CODE = 'HR_LICHSU_NHANVIEN_TAILIEU';
    REPORT_NAME = 'HR_RP_LICHSU_NHANVIEN_TAILIEU';

implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmHotro_HR, Dmdl, HrData; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := EmpID;
    frEmp1.Initial(EmpID, EmpIDLabel, EmpName);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrMaster, TABLE_NAME);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.FormShow(Sender: TObject);
begin
    with DataMain do
        OpenDataSets([QrQuocgia]);

    with HrDataMain do
        OpenDataSets([QrDMNV, QrV_HR_TAILIEU, QrV_HR_CHUNGCHI]);

    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, sysCurFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrMaster.SQL do
    begin
        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayBatDau], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    EdTenTaiLieu.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
	    EdTenTaiLieu.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdChungChiExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_CHUNGCHI');

    HrDataMain.QrV_HR_CHUNGCHI.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdPrintExecute(Sender: TObject);
var
    d: TDateTime;
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    d := Date;
    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        1, //0: Lich su, 1: HSNV/Lich Su
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        mManv]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdQuocGiaExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r, True);
    DataMain.QrQuocgia.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdRefreshExecute(Sender: TObject);
var
    s: String;
begin
    Wait(DATAREADING);
    with QrMaster do
    begin
        s := Sort;
        Close;
        Parameters[0].Value := mManv;
        Open;
    end;
    if s = '' then
        s := 'NgayBatDau';

    SortDataSet(QrMaster, s);
    ClearWait;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdFileMinusExecute(Sender: TObject);
begin
   if  not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bViewFile: Boolean;
begin
    bViewFile := False;
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
        if Active then
            bViewFile := FieldByName('FileExt').AsString <> '';

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdFileMinus.Enabled := bViewFile and not bBrowse;

    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVAILD_DATA			= '%s. Dữ liệu không tồn tại';
procedure TFrmHoSonvKienThucTaiLieu.QrMasterBeforePost(DataSet: TDataSet);
var
    s: string;
begin
    if BlankConfirm(QrMaster, ['Manv', 'TenTaiLieu', 'NgayCap', 'NoiCap', 'KienThuc', 'ChungChi']) then
        Abort;

    with QrMaster do
    begin
        if not HrDataMain.QrV_HR_TAILIEU.Locate('MA_HOTRO', FieldByName('KienThuc').AsString , []) then
        begin
            s := FieldByName('KienThuc').DisplayLabel;
            ErrMsg(Format(RS_INVAILD_DATA, [s]));
            Abort;
        end;

        if not HrDataMain.QrV_HR_CHUNGCHI.Locate('MA_HOTRO', FieldByName('ChungChi').AsString , []) then
        begin
            s := FieldByName('ChungChi').DisplayLabel;
            ErrMsg(Format(RS_INVAILD_DATA, [s]));
            Abort;
        end;
    end;

    SetAudit(DataSet);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterCalcFields(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.RzSizePanel1ConstrainedResize(
  Sender: TObject; var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 535;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Manv').AsString := mManv;
        FieldByName('MaQuocGia').AsString := '084';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrList then
    	EdTenTaiLieu.SetFocus;

    GrList.SetFocus
end;

procedure TFrmHoSonvKienThucTaiLieu.CmdTaiLieuExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_LOAI_KIENTHUC');

    HrDataMain.QrV_HR_TAILIEU.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrMaster do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.Dettach();
begin
     with QrMaster do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvKienThucTaiLieu.ViewAttachment();
var
	s, sn: String;
begin
	with QrMaster do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

end.
