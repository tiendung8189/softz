﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvBieuMau;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdblook, AdvEdit, RzEdit, RzDBEdit, RzDBBnEd,
  Winapi.ShellAPI, Winapi.Windows, Vcl.Buttons, DBCtrlsEh, frameEmp;

type
  TFrmHoSonvBieuMau = class(TForm)
    ActionList: TActionList;
    CmdPrint: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMBieuMau: TADOQuery;
    DsDMBieuMau: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrDMBieuMauCREATE_BY: TIntegerField;
    QrDMBieuMauUPDATE_BY: TIntegerField;
    QrDMBieuMauCREATE_DATE: TDateTimeField;
    QrDMBieuMauUPDATE_DATE: TDateTimeField;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMBieuMauFileName: TWideStringField;
    QrDMBieuMauFileExt: TWideStringField;
    QrDMBieuMauFileChecksum: TWideStringField;
    QrDMBieuMauFileGroup: TWideStringField;
    QrDMBieuMauFileDesc: TWideStringField;
    QrDMBieuMauComment: TWideMemoField;
    QrDMBieuMauCacl_FileExt: TWideStringField;
    RefNhom: TADOQuery;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    Panel1: TPanel;
    QrDMBieuMauLK_FileGroup: TWideStringField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrDMBieuMauCalc_FileName: TWideStringField;
    CmdRefresh: TAction;
    frEmp1: TfrEmp;
    QrFileContentIdx: TAutoIncField;
    QrDMBieuMauIdx: TAutoIncField;
    QrDMBieuMauFileIdx: TGuidField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBieuMauCalcFields(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrDMBieuMauAfterInsert(DataSet: TDataSet);
  private
    r: WORD;
  	mCanEdit: Boolean;
    mSQL, mSearch, mEmpID, mEmpIDLabel, mEmpName: String;
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmHoSonvBieuMau: TFrmHoSonvBieuMau;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    isStr, isFile, DmHotro_HR, GuidEx, OfficeData;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHANVIEN_BIEUMAU';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
	DsDMBieuMau.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMBieuMau, FORM_CODE, Filter);
    mSQL := QrDMBieuMau.SQL.Text;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.FormShow(Sender: TObject);
begin
    frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
    OpenDataSets([QrDMBieuMau, RefNhom]);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDMBieuMau]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMBieuMau, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdPrintExecute(Sender: TObject);
var
	s, sName, sExt, sManv : String;
    Idx: Integer;
begin
	with QrDMBieuMau do
    begin
        try
            sManv := mEmpID;
            Idx := FieldByName('Idx').AsInteger;

            sName := isStripToneMark(FieldByName('FileName').AsString);
            sExt := isStripToneMark(FieldByName('FileExt').AsString);
            s := sysAppData + sName + sExt;

            if (not SameText(sExt, '.doc')) and (not SameText(sExt, '.docx')) then
            begin
                ErrMsg(RS_FILE_NOTFOUND_FORMAT);
                Exit;
            end;

            with QrFileContent do
            begin
                if Active then
                    Close;

                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                if not FileExists(s) then
                begin
                    TBlobField(FieldByName('FileContent')).SaveToFile(s);
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
                DataOffice.CreateDoc3(s, 'HR_RP_BIEUMAU_WORD', [sysLogonUID, sManv, Idx]);
            end;
        except
            ErrMsg(RS_INVAILD_CONTENT);
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdRefreshExecute(Sender: TObject);
var
	sSQL, s1 : String;
begin
   	if (EdSearch.Text <> mSearch) then
	begin
        mSearch := EdSearch.Text;
        with QrDMBieuMau do
        begin
			Close;
            sSQL := mSQL;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSQL := sSQL + ' and (' +
                            'dbo.fnStripToneMark([FileName]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([FileDesc]) like ''%' + s1 + '%'''
                            + ')';
            end;

            SQL.Text := sSQL;
            Open;
            First;
        end;

		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMBieuMau)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDMBieuMau, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.QrDMBieuMauAfterInsert(DataSet: TDataSet);
begin
    with QrDMBieuMau do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
    end;
end;

procedure TFrmHoSonvBieuMau.QrDMBieuMauCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrDMBieuMau do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrDMBieuMau.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMBieuMau, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvBieuMau.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMBieuMau);
end;


end.
