﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonNgay;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls;

type
  TFrmChonNgay = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    CbNgay: TwwDBDateTimePicker;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  	function Execute (prompt : String = 'Chọn Ngày') : TDateTime;
  end;

var
  FrmChonNgay: TFrmChonNgay;

implementation

uses
	isLib;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonNgay.FormCreate(Sender: TObject);
begin
	CbNgay.Date := Date;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonNgay.Execute;
begin
	Caption := prompt;
	if ShowModal = mrOK then
    	Result := CbNgay.Date
    else
    	Result := 0
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonNgay.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
	CbNgay.SetFocus
end;

end.
