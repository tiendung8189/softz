(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmDiaDiemLamViec;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  wwdblook, AppEvnts, Menus, AdvMenus, wwfltdlg, wwFltDlg2, wwDBGrid2, wwDialog,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbigrd, ToolWin;

type
  TFrmDmDiaDiemLamViec = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsKhuVuc: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    QrKhuVuc: TADOQuery;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PgMain: TPageControl;
    tsBehaviour: TTabSheet;
    tsDep: TTabSheet;
    GrDiaDiem: TwwDBGrid2;
    GrKhuVuc: TwwDBGrid2;
    QrDiaDiem: TADOQuery;
    DsDiaDiem: TDataSource;
    Bevel1: TBevel;
    CbDiaDiemKhuVuc: TwwDBLookupCombo;
    RefKhuVuc: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Filter: TwwFilterDialog2;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrKhuVucCREATE_BY: TIntegerField;
    QrKhuVucUPDATE_BY: TIntegerField;
    QrKhuVucCREATE_DATE: TDateTimeField;
    QrKhuVucUPDATE_DATE: TDateTimeField;
    QrDiaDiemCREATE_BY: TIntegerField;
    QrDiaDiemUPDATE_BY: TIntegerField;
    QrDiaDiemCREATE_DATE: TDateTimeField;
    QrDiaDiemUPDATE_DATE: TDateTimeField;
    QrDiaDiemLK_NHOM: TWideStringField;
    CmdReload: TAction;
    QrKhuVucMaNoiLV_KhuVuc: TWideStringField;
    QrKhuVucTenNoiLV_KhuVuc: TWideStringField;
    QrKhuVucTenNoiLV_KhuVuc_TA: TWideStringField;
    QrDiaDiemMaNoiLV: TWideStringField;
    QrDiaDiemMaNoiLV_KhuVuc: TWideStringField;
    QrDiaDiemTenNoiLV: TWideStringField;
    QrDiaDiemTenNoiLV_TA: TWideStringField;
    QrDiaDiemDiaChi: TWideStringField;
    QrDiaDiemPhuCap_NoiLV: TFloatField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrKhuVucBeforePost(DataSet: TDataSet);
    procedure QrKhuVucBeforeDelete(DataSet: TDataSet);
    procedure QrKhuVucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKhuVucBeforeInsert(DataSet: TDataSet);
    procedure GrDiaDiemCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure GrKhuVucCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrDiaDiemBeforePost(DataSet: TDataSet);
    procedure CbDiaDiemKhuVucNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrKhuVucAfterPost(DataSet: TDataSet);
  private
  	mCanEdit, mTrigger, fixCodeLd, fixCodeNhom, mRefresh: Boolean;
    mQuery: TADOQuery;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmDiaDiemLamViec: TFrmDmDiaDiemLamViec;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, isCommon, MainData;

{$R *.DFM}

const
    FORM_CODE = 'HR_DM_NOI_LAMVIEC';
    FORM_CODE2 = 'HR_DM_NOI_LAMVIEC_KHUVUC';

    TABLE_NAME = FORM_CODE;
    TABLE_NAME2 = FORM_CODE2;

    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrDiaDiem, TABLE_NAME);
    AddFields(QrKhuVuc, TABLE_NAME2);

    mRefresh := False;
    mTrigger := False;
    mQuery := QrDiaDiem;

    SetCustomGrid(
    	[FORM_CODE, FORM_CODE2], [GrDiaDiem, GrKhuVuc]);
    SetDictionary([QrDiaDiem, QrKhuVuc], [FORM_CODE, FORM_CODE2], [Filter, nil]);

    // Ly do
    fixCodeLd := setCodeLength(FORM_CODE, QrDiaDiem.FieldByName('MaNoiLV'));

    // Nhom
    fixCodeNhom := SetCodeLength(FORM_CODE2, QrKhuVuc.FieldByName('MaNoiLV_KhuVuc'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.FormShow(Sender: TObject);
begin
	GrDiaDiem.ReadOnly := not mCanEdit;
    GrKhuVuc.ReadOnly := not mCanEdit;
	OpenDataSets([QrDiaDiem, QrKhuVuc]);

    SetDisplayFormat(QrDiaDiem, sysCurFmt);
    ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrDiaDiem, QrKhuVuc]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
    n: Integer;
begin
	b := mQuery.State in [dsBrowse];
    n := PgMain.ActivePageIndex;
    CmdNew.Enabled 	  := b and mCanEdit;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdDel.Enabled	  := b and mCanEdit and (not mQuery.IsEmpty);
    CmdSearch.Enabled := b;
    CmdClearFilter.Enabled := b and (Filter.FieldInfo.Count > 0);

    CmdFilter.Enabled := b and (n = 0);
    CmdFilter.Visible := n = 0;
    CmdClearFilter.Visible := n = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrDiaDiemBeforePost(DataSet: TDataSet);
begin
    begin
		if BlankConfirm(QrDiaDiem, ['MaNoiLV']) then
    		Abort;

        if fixCodeLd then
            if LengthConfirm(QrDiaDiem, ['MaNoiLV']) then
                Abort;

		if BlankConfirm(QrDiaDiem, ['TenNoiLV', 'MaNoiLV_KhuVuc']) then
    		Abort;

		SetNull(QrDiaDiem, ['MaNoiLV_KhuVuc']);
    end;

    SetAudit(DataSet);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrKhuVucBeforePost(DataSet: TDataSet);
begin
    begin
	    if BlankConfirm(QrKhuVuc, ['MaNoiLV_KhuVuc']) then
    	    Abort;

	    if fixCodeNhom then
    	    if LengthConfirm(QrKhuVuc, ['MaNoiLV_KhuVuc']) then
        	    Abort;

	    if BlankConfirm(QrKhuVuc, ['TenNoiLV_KhuVuc']) then
    	    Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrKhuVucAfterPost(DataSet: TDataSet);
begin
    mRefresh := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrKhuVucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrKhuVucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
        exSearch(Name + '_0', DsDiaDiem)
    else
        exSearch(Name + '_1', DsKhuVuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.QrKhuVucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.GrDiaDiemCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MaNoiLV') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.GrKhuVucCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if (Field.FullName = 'MaNoiLV_KhuVuc') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 0 then
    	Status.SimpleText := exRecordCount(mQuery, Filter)
    else
    	Status.SimpleText := RecordCount(mQuery);    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, REPORT_NAME, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := mQuery.State in [dsBrowse];
    if not AllowChange then
        exCompleteConfirm;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.PgMainChange(Sender: TObject);
begin
	case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrDiaDiem;
			GrDiaDiem.SetFocus;
            RefKhuVuc.Requery;

            if mRefresh then
            begin
                exReSyncRecord(mQuery);
                mRefresh := False;
            end;
	    end;
    1:
	    begin
    	    mQuery := QrKhuVuc;
        	GrKhuVuc.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CbDiaDiemKhuVucNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmDiaDiemLamViec.CmdReloadExecute(Sender: TObject);
begin
    QrDiaDiem.Requery;
    QrKhuVuc.Requery;
    RefKhuVuc.Requery;
end;

end.
