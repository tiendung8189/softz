﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HosonvHoChieu;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, System.Variants,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, Wwdbgrid, RzSplit, wwDialog, Wwdbigrd, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdbdatetimepicker, wwcheckbox, wwdblook, DBCtrlsEh,
  DBGridEh, DBLookupEh, DbLookupComboboxEh2, Winapi.ShellAPI, Winapi.Windows,
  frameEmp;

type
  TFrmHosonvHoChieu = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    EdMA: TDBEditEh;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    QrDanhmucManv: TWideStringField;
    QrDanhmucMaSo: TWideStringField;
    QrDanhmucMaNoiCap: TWideStringField;
    QrDanhmucNoiCap: TWideStringField;
    QrDanhmucNgayCap: TDateTimeField;
    QrDanhmucNgayHetHan: TDateTimeField;
    QrDanhmucGhiChu: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucLK_TenNoiCap: TWideStringField;
    QrDanhmucLK_NoiCap_Ma: TWideStringField;
    EdHoChieuNoiCap: TDBEditEh;
    Label61: TLabel;
    dpHoChieuNgayCap: TwwDBDateTimePicker;
    Label66: TLabel;
    dpHoChieuNgayHetHan: TwwDBDateTimePicker;
    CbHoChieuLoai: TDbLookupComboboxEh2;
    CmdHoChieuLoai: TAction;
    QrDanhmucGiayTo_Loai: TIntegerField;
    QrDanhmucGiayTo_Nhom: TWideStringField;
    QrDanhmucLK_GiayTo_TenNhom: TWideStringField;
    spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap: TADOCommand;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    DBEdit6: TDBEditEh;
    CmdOpenLink: TAction;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrDanhmucFileIdx: TGuidField;
    QrDanhmucFileName: TWideStringField;
    QrDanhmucFileExt: TWideStringField;
    QrDanhmucLink: TWideStringField;
    CmdFileView: TAction;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    QrDanhmucCacl_FileExt: TWideStringField;
    QrDanhmucCalc_FileName: TWideStringField;
    frEmp1: TfrEmp;
    QrFileContentIdx: TAutoIncField;
    QrDanhmucIdx: TAutoIncField;
    QrDanhmucFileDesc: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrDanhmucBeforeOpen(DataSet: TDataSet);
    procedure QrDanhmucBeforeEdit(DataSet: TDataSet);
    procedure CmdHoChieuLoaiExecute(Sender: TObject);
    procedure CmdOpenLinkExecute(Sender: TObject);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure QrDanhmucCalcFields(DataSet: TDataSet);
    procedure QrDanhmucAfterDelete(DataSet: TDataSet);
    procedure QrDanhmucLinkValidate(Sender: TField);
  private
  	mCanEdit, mClose, mRet: Boolean;
    mSql, mManv: String;
    mFileIdx: TGUID;
    r: WORD;
    function ReleaseDateValid(pManv: String; pGiayToLoai: Integer; pNgay: TDateTime; pIdx: Integer): Boolean;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	function Execute(r: WORD; pEmpID, pEmpIDLabel, pEmpName: string; pClose: Boolean = True): Boolean;
  end;

var
  FrmHosonvHoChieu: TFrmHosonvHoChieu;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon, GuidEx,
  DmHotro_HR, HrData, isFile, isStr;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHANVIEN_HOCHIEU';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvHoChieu.Execute;
begin
	mCanEdit := rCanEdit(r);
	DsDanhmuc.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    mManv := pEmpID;
    frEmp1.Initial(pEmpID, pEmpIDLabel, pEmpName);
    mClose := pClose;
    ShowModal;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDanhmuc, 'HR_DM_NHANVIEN_GIAYTO');

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');
    mSql := QrDanhmuc.SQL.Text;

    mTrigger := False;
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.FormShow(Sender: TObject);
begin
    with HrDataMain do
        OpenDataSets([QrV_HR_HOCHIEU_NHOM]);

    OpenDataSets([QrDanhmuc]);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDanhmuc]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdOpenLinkExecute(Sender: TObject);
begin
    with QrDanhmuc do
    if FieldByName('Link').AsString <> '' then
    begin
         ShellExecute(0, 'Open', PChar(FieldByName('Link').AsString), nil,  nil, SW_NORMAL);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdFileMinusExecute(Sender: TObject);
begin
    if  not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;

procedure TFrmHosonvHoChieu.CmdFilePlusExecute(Sender: TObject);
begin
     Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdFileViewExecute(Sender: TObject);
begin
   ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdHoChieuLoaiExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_HOCHIEU_NHOM');

    HrDataMain.QrV_HR_HOCHIEU_NHOM.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Manv').AsString := mManv;
        FieldByName('GiayTo_Loai').AsInteger := 2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucAfterPost(DataSet: TDataSet);
begin
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrDanhmuc.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

procedure TFrmHosonvHoChieu.QrDanhmucBeforeEdit(DataSet: TDataSet);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_TUNGAY = 'Ngày cấp không hợp lệ.';
procedure TFrmHosonvHoChieu.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
	    if BlankConfirm(QrDanhmuc, ['MaSo', 'NgayCap', 'GiayTo_Nhom', 'NgayHetHan']) then
   			Abort;

        if ((FieldByName('NgayHetHan').AsFloat > 10) and
                (FieldByName('NgayHetHan').AsDateTime < FieldByName('NgayCap').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_TUNGAY);
            try
                dpHoChieuNgayCap.SelectAll;
                dpHoChieuNgayCap.SetFocus;
            except
            end;
            Abort;
        end;

        if not ReleaseDateValid(FieldByName('Manv').AsString,
            FieldByName('GiayTo_Loai').AsInteger,
            FieldByName('NgayCap').AsDateTime,
            FieldByName('Idx').AsInteger) then
            Abort;

    end;
    SetAudit(DataSet);
end;

procedure TFrmHosonvHoChieu.QrDanhmucCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrDanhmuc do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucLinkValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
    begin
        if not validateUrl(Sender.AsString) then
        begin
            ErrMsg(Format(RS_INVAILD_VALIDATION, [Sender.DisplayLabel]));
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrDanhmuc.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 454;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

procedure TFrmHosonvHoChieu.QrDanhmucBeforeOpen(DataSet: TDataSet);
begin
      with DataSet as TADOQuery do
        Parameters[0].Value := mManv;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaSo' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvHoChieu.ReleaseDateValid;
var
    s: String;
begin
    with spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap do
    begin
        Prepared := True;
        Parameters.ParamByName('@Idx').Value := pIdx;
        Parameters.ParamByName('@Manv').Value := pManv;
        Parameters.ParamByName('@NgayCap').Value := pNgay;
        Parameters.ParamByName('@GiayTo_Loai').Value := pGiayToLoai;
        try
            Execute;
        except
        end;

        if Parameters.FindParam('@STR') <> nil then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrDanhmuc do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrDanhmuc.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.Dettach();
begin
     with QrDanhmuc do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvHoChieu.ViewAttachment();
var
	s, sn: String;
begin
	with QrDanhmuc do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;



end.
