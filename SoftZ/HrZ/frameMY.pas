(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameMY;

interface

uses
  Windows, Classes, Controls, Forms, fctreecombo, StdCtrls, ExtCtrls, ActnList,
  fcCombo;

type
  TfrMY = class(TFrame)
    Panel1: TPanel;
    LbOrg: TLabel;
    CbOrg: TfcTreeCombo;
    Label4: TLabel;
    Label5: TLabel;
    CbMon: TComboBox;
    CbYear: TComboBox;
    procedure CbOrgChange(Sender: TObject);
    procedure CbOrgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CbOrgCloseUp(Sender: TObject; Select: Boolean);
  private
  	FAction: TAction;
  public
  	procedure Initial(m, y: Integer; ac: TAction; pDep: String = '');
  end;

implementation

uses
	ExCommon, isStr, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMY.Initial;
begin
    isMonthList(CbMon, m);
    isYearList(CbYear, y);
    FAction := ac;
  	if sysIsDataAccess then
        FlexOrgComboDataAccess(CbOrg)
    else
      	FlexOrgCombo(CbOrg);
    CbOrg.OnChange(CbOrg);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMY.CbOrgChange(Sender: TObject);
begin
    LbOrg.Caption := exGetFlexOrgDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMY.CbOrgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearOrg}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMY.CbOrgCloseUp(Sender: TObject; Select: Boolean);
begin
	if Assigned(FAction) then
	    if Select then
    	    FAction.Execute;
end;

end.
