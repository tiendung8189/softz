﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvThoiviec;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, wwdbdatetimepicker, ExtCtrls,
  ActnList, ComCtrls, db, wwdblook, DBCtrls, frameEmp, Buttons, isPanel, Mask,
  ToolWin, wwdbedit, DBCtrlsEh, DBGridEh, DBLookupEh, Vcl.Grids, Wwdbigrd,
  Wwdbgrid, wwDBGrid2, Data.Win.ADODB, DbLookupComboboxEh2;

type
  TFrmHoSonvThoiviec = class(TForm)
    PaTerminated: TisPanel;
    DpNgayDangKyThoiViec: TwwDBDateTimePicker;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    DpNgayThoiViec: TwwDBDateTimePicker;
    wwDBDateTimePicker4: TwwDBDateTimePicker;
    Label1: TLabel;
    Label6: TLabel;
    frEmp: TfrEmp;
    Panel2: TisPanel;
    CmdChecked: TAction;
    Label4: TLabel;
    ToolButton1: TToolButton;
    CmdEdit: TAction;
    ToolButton2: TToolButton;
    CbLyDo: TDbLookupComboboxEh2;
    isPanel1: TisPanel;
    DBMemo1: TDBMemo;
    PaLydo2: TPanel;
    CmdThietBiTaiSan: TAction;
    Label2: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    DsMaster: TDataSource;
    CmdRefresh: TAction;
    spMaster: TADOStoredProc;
    GrList: TwwDBGrid2;
    EdMaThietBi: TDBEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
    procedure CmdEditExecute(Sender: TObject);
    procedure CmdThietBiTaiSanExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeOpen(DataSet: TDataSet);
    procedure spMasterAfterOpen(DataSet: TDataSet);
  private
    r: WORD;
  	mCanEdit, mChanged: Boolean;
    mSQL, mEmpID, mEmpIDLabel, mName: String;
  public
  	function  Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String): Boolean;
  end;

var
  FrmHoSonvThoiviec: TFrmHoSonvThoiviec;

implementation

{$R *.DFM}

uses
    Rights, isLib, isMsg,  isDb, ExCommon, isCommon, MainData,  HoSonv,
    ThietBiTaiSan, HrData;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonvThoiviec.Execute;
begin
	mCanEdit := rCanEdit(r);
    mChanged := False;
    mEmpID := EmpID;
    mName := EmpName;
    mEmpIDLabel := EmpIDLabel;
    frEmp.Initial(EmpID, EmpIDLabel, mName);
    ShowModal;
    Result := mChanged;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.FormShow(Sender: TObject);
begin
    // Open Database
    with HrDataMain do
        OpenDataSets([QrLOAI_THOIVIEC]);

    CmdRefresh.Execute;
end;

procedure TFrmHoSonvThoiviec.QrMasterBeforeOpen(DataSet: TDataSet);
begin
     with DataSet as TADOQuery do
        Parameters[0].Value := mEmpID;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.spMasterAfterOpen(DataSet: TDataSet);
begin
    SetShortDateFormat(spMaster, ShortDateFormat);

    SetCustomGrid(['HR_DM_NHANVIEN_THOIVIEC'], [GrList]);
    SetDictionary([spMaster], ['HR_DM_NHANVIEN_THOIVIEC'], [nil]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    FrmHoSonv.DsDMNV.AutoEdit := False;
	CloseDataSets([HrDataMain.QrLOAI_THOIVIEC]);
    Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(FrmHoSonv.QrDMNV, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.CmdSaveExecute(Sender: TObject);
begin
    FrmHoSonv.QrDMNV.Post;
    mChanged := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.CmdThietBiTaiSanExecute(Sender: TObject);
begin
    r := GetRights('HR_NV_THIEBI_DUNGCU');
	if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThietBiTaiSan, FrmThietBiTaiSan);
    FrmThietBiTaiSan.Execute(r, mEmpID, mEmpIDLabel, mName);
    CmdRefresh.Execute;
    exReSyncRecord(FrmHoSonv.QrDMNV);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.CmdCancelExecute(Sender: TObject);
begin
	FrmHoSonv.QrDMNV.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.CmdCloseExecute(Sender: TObject);
begin
    Close
end;

procedure TFrmHoSonvThoiviec.CmdEditExecute(Sender: TObject);
begin
    FrmHoSonv.QrDMNV.Edit;
    DpNgayDangKyThoiViec.SetFocus;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    Wait(DATAREADING);
    with spMaster do
    begin
        Close;
    	Prepared := True;
        Parameters[1].Value := mEmpID;
        Parameters[2].Value := FrmHoSonv.QrDMNV.FieldByName('NgayDangKyThoiViec').AsDateTime;
        ExecProc;
        Open;
    end;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
begin
	b := FrmHoSonv.QrDMNV.State in [dsBrowse];
    CmdEdit.Enabled := b;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdCancel.Enabled := not b;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvThoiviec.TntFormCreate(Sender: TObject);
begin
    TMyForm(Self).Init();
end;


end.
