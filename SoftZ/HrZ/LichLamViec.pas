﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit LichLamViec;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, ComCtrls, ActnList,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, wwdblook, Db, ADODB, wwfltdlg,
  Menus, AdvMenus, AppEvnts, fctreecombo, wwFltDlg2, wwDBGrid2,
  frameMY, wwDialog, ToolWin, Mask, wwdbedit, Wwdotdot, Wwdbcomb, RzPanel,
  RzSplit;

type
  TFrmLichLamViec = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    QrPayroll: TADOQuery;
    CmdRefresh: TAction;
    DsPayroll: TDataSource;
    QrTimeSheet: TADOQuery;
    DsTimeSheet: TDataSource;
    FilterPayroll: TwwFilterDialog2;
    QrPayrollMANV: TWideStringField;
    QrPayrollMaPhongBan: TWideStringField;
    QrPayrollCREATE_BY: TIntegerField;
    QrPayrollUPDATE_BY: TIntegerField;
    QrPayrollCREATE_DATE: TDateTimeField;
    QrPayrollUPDATE_DATE: TDateTimeField;
    QrTimeSheetMANV: TWideStringField;
    QrTimeSheetUPDATE_BY: TIntegerField;
    QrTimeSheetUPDATE_DATE: TDateTimeField;
    CmdFilter: TAction;
    QrEmp: TADOQuery;
    QrPayrollName: TWideStringField;
    CmdCalc2: TAction;
    ApplicationEvents1: TApplicationEvents;
    TS_RECORDER_IN: TADOCommand;
    TS_RECORDER_OUT: TADOCommand;
    CmdParams: TAction;
    Pop1: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    Lcdliu1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N5: TMenuItem;
    CmdClear1: TAction;
    Item3: TMenuItem;
    QrTimeSheetMACA: TWideStringField;
    QrShift: TADOQuery;
    QrTimeSheetLK_TENCA: TWideStringField;
    QrLeave: TADOQuery;
    CLEAR_DAILY_DATA: TADOCommand;
    CmdClear2: TAction;
    CmdRep: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdSwitch: TAction;
    CmdDayCsv: TAction;
    CmdAutoIO2: TAction;
    CmdAutoIO1: TAction;
    TS_AUTO_IO: TADOCommand;
    QrCsv: TADOQuery;
    TS_DAILY_CSV: TADOCommand;
    CmdDetail: TAction;
    CmdValidate: TAction;
    CmdValidateNext: TAction;
    CmdReload: TAction;
    CmdUpdate: TAction;
    N10: TMenuItem;
    TS_DAILY_CHECK: TADOCommand;
    QrPayrollLK_TenPhongBan: TWideStringField;
    frMY: TfrMY;
    Status: TStatusBar;
    TS_RECORDER_DETECT: TADOCommand;
    CmdDayhst: TAction;
    QrTimeSheetName: TWideStringField;
    Bevel1: TBevel;
    CmdGetRecLog: TAction;
    TS_RECORDER_LOG: TADOCommand;
    TS_PAYROLL_PERIOD: TADOCommand;
    QrTimeSheetLOAINGAY: TIntegerField;
    QrPayrollTHANG: TIntegerField;
    QrPayrollNAM: TIntegerField;
    QrTimeSheetTHU: TWideStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    spHR_XOALICHLV: TADOCommand;
    Item2: TMenuItem;
    CmdGetRecLog1NV: TAction;
    N1: TMenuItem;
    QrPayrollMaChiNhanh: TWideStringField;
    QrPayrollMaBoPhan: TWideStringField;
    QrPayrollMaNhomLV: TWideStringField;
    QrPayrollMaChucDanh: TWideStringField;
    QrTimeSheetCREATE_BY: TIntegerField;
    QrTimeSheetCREATE_DATE: TDateTimeField;
    QrTimeSheetSoGio_Ca: TFloatField;
    QrTimeSheetLK_TenPhongBan: TWideStringField;
    QrTimeSheetLK_TenBoPhan: TWideStringField;
    QrTimeSheetLK_TenChiNhanh: TWideStringField;
    QrPayrollLK_TenChiNhanh: TWideStringField;
    QrPayrollLK_TenChucDanh: TWideStringField;
    QrPayrollLK_TenNhomLV: TWideStringField;
    BtnDangKy: TToolButton;
    ToolButton12: TToolButton;
    CmdLichLamViec: TAction;
    QrTimeSheetGioVao_Ca: TDateTimeField;
    QrTimeSheetGioRa_Ca: TDateTimeField;
    QrTimeSheetGioVao1_Ca: TDateTimeField;
    QrTimeSheetGioRa1_Ca: TDateTimeField;
    QrTimeSheetDiTre_Ca: TDateTimeField;
    QrTimeSheetVeSom_Ca: TDateTimeField;
    QrTimeSheetLK_ManvQL: TWideStringField;
    QrTimeSheetManvQL: TWideStringField;
    QrDaily: TADOQuery;
    DsDaily: TDataSource;
    QrDailyNgay: TDateTimeField;
    QrDailyThang: TIntegerField;
    QrDailyNam: TIntegerField;
    QrDailyThu: TWideStringField;
    QrTimeSheetLK_MaChiNhanh: TWideStringField;
    QrTimeSheetLK_MaPhongBan: TWideStringField;
    QrTimeSheetLK_MaBoPhan: TWideStringField;
    QrTimeSheetLK_MaNhomLV: TWideStringField;
    Filter: TwwFilterDialog2;
    QrPayrollCalc_NgayVaoLam: TDateTimeField;
    QrPayrollNgayVaoLam: TDateTimeField;
    QrPayrollNgayBatDauHocViec: TDateTimeField;
    QrPayrollNgayKetThucHocViec: TDateTimeField;
    QrPayrollNgayBatDauThuViec: TDateTimeField;
    QrPayrollNgayKetThucThuViec: TDateTimeField;
    QrPayrollNgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollNgayThoiViec: TDateTimeField;
    QrPayrollCalc_NgayBatDauHocViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucHocViec: TDateTimeField;
    QrPayrollCalc_NgayBatDauThuViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucThuViec: TDateTimeField;
    QrPayrollCalc_NgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollCalc_NgayThoiViec: TDateTimeField;
    PopTime2: TAdvPopupMenu;
    MenuItem10: TMenuItem;
    QrTimeSheetSoCa: TIntegerField;
    QrTimeSheetOTDauCa_TuGio: TDateTimeField;
    QrTimeSheetOTDauCa_DenGio: TDateTimeField;
    QrTimeSheetOTGiuaCa_TuGio: TDateTimeField;
    QrTimeSheetOTGiuaCa_DenGio: TDateTimeField;
    QrTimeSheetOTCuoiCa_TuGio: TDateTimeField;
    QrTimeSheetOTCuoiCa_DenGio: TDateTimeField;
    PgMain: TPageControl;
    TaPayroll: TTabSheet;
    Splitter1: TSplitter;
    GrTime: TwwDBGrid2;
    CbShift: TwwDBLookupCombo;
    RzSizePaPayroll: TRzSizePanel;
    GrPayroll: TwwDBGrid2;
    TabSheet1: TTabSheet;
    GrTime2: TwwDBGrid2;
    RzSizePaDaily: TRzSizePanel;
    GrDaily: TwwDBGrid2;
    QrTimeSheetThang: TIntegerField;
    QrTimeSheetNgay: TDateTimeField;
    QrTimeSheetNam: TIntegerField;
    QrPayrollManvQL: TWideStringField;
    spHR_BANG_LICHLV_Invalid_GioTangCa: TADOCommand;
    CmdNewList: TAction;
    QrTimeSheetLK_TenLoaiNgay: TWideStringField;
    CbLoaiNgay: TwwDBLookupCombo;
    Item4: TMenuItem;
    N2: TMenuItem;
    CmdClear3: TAction;
    spHR_XOALICHLV_THEOSOCA: TADOCommand;
    BtnXuly: TToolButton;
    ToolButton6: TToolButton;
    PopXuly: TAdvPopupMenu;
    Kimtratnhngcadliu1: TMenuItem;
    MenuItem1: TMenuItem;
    Xatonbdliucng1: TMenuItem;
    QrPayrollCo_MacDinhCong: TBooleanField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure GrTimeCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrTimeSheetCalcFields(DataSet: TDataSet);
    procedure CmdParamsExecute(Sender: TObject);
    procedure CmdClear1Execute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdClear2Execute(Sender: TObject);
    procedure CmdRepExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdDayCsvExecute(Sender: TObject);
    procedure CmdAutoIO2Execute(Sender: TObject);
    procedure CmdAutoIO1Execute(Sender: TObject);
    procedure QrShiftAfterOpen(DataSet: TDataSet);
    procedure CmdDetailExecute(Sender: TObject);
    procedure CmdValidateExecute(Sender: TObject);
    procedure CmdValidateNextExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure Pop1Popup(Sender: TObject);
    procedure QrTimeSheetBeforePost(DataSet: TDataSet);
    procedure QrTimeSheetMACAChange(Sender: TField);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDayhstExecute(Sender: TObject);
    procedure QrPayrollAfterScroll(DataSet: TDataSet);
    procedure QrDailyAfterScroll(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure GrTime2CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdGetRecLogExecute(Sender: TObject);
    procedure CmdGetRecLog1NVExecute(Sender: TObject);
    procedure QrDailyCalcFields(DataSet: TDataSet);
    procedure QrPayrollCalcFields(DataSet: TDataSet);
    procedure QrTimeSheetOTDauCa_TuGioValidate(Sender: TField);
    procedure QrTimeSheetOTGiuaCa_TuGioValidate(Sender: TField);
    procedure QrTimeSheetOTCuoiCa_TuGioValidate(Sender: TField);
    procedure CmdNewListExecute(Sender: TObject);
    procedure CmdClear3Execute(Sender: TObject);
    procedure BtnXulyClick(Sender: TObject);
  private
    r: WORD;
  	mCanEdit, mTrigger, bClose: Boolean;
    fStr: String;
    fLevel, mMonth, mYear, fFilterThaiSan: Integer;
	sqlPayroll, sqlTimeSheet, mManv: String;

    fsDateFormat: TFormatSettings;

    procedure GetPeriod;
    procedure Clear1;
    procedure AutoIO1;
    procedure ValidationCheck(nx: Boolean);

    function DetectRec(pID: String; pDate, pTime: TDateTime): Boolean;
    function InRec(pID: String; pDate, pTime: TDateTime): Boolean;
    function OutRec(pID: String; pDate, pTime: TDateTime): Boolean;

	function StdConvert(pModel, pFile, pTemp: String): Boolean;

    procedure CapnhatDulieuLichLamViec(manv: String = '');
    procedure XoaDulieuLichLamViec(manv: String = '');
    procedure XoaDulieuLichLamViecDongChon(manv: String; pDate: TDateTime; pSoCa: Integer);
    procedure OpenTimeSheet;

    function overTimeDateValid(pManv, pMaCa: String; pDate, pDateFrom, pDateTo: TDateTime; pLoaiCa: Integer): Boolean;

  public
  	procedure Execute(r: WORD; manv: String = ''; month: Integer = 0; year: Integer = 0);
  end;

var
  FrmLichLamViec: TFrmLichLamViec;

implementation

{$R *.DFM}

uses
	isLib, Rights, isDb, MainData, ExCommon, isStr, isMsg, Tuden,
  	BangThongso, BosungCong, Variants, isFile, isCommon, GmsRep, DangkyVangmat,
    GuidEx, DangkyTangCa, HrData, LichLamViecTools;

const
	FORM_CODE = 'HR_BANG_LICHLV';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := manv;
    mMonth := month;
    mYear := year;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    frMY.Initial(sysMon, sysYear, CmdRefresh);
    mTrigger := False;
    sqlPayroll := QrPayroll.SQL.Text;
    sqlTimeSheet := QrTimeSheet.SQL.Text;
    AddAllFields(QrPayroll, 'HR_BANG_LUONG', 0);
    AddAllFields(QrTimesheet, 'HR_BANG_LICHLV', 0);
    AddAllFields(QrDaily, 'HR_BANG_THONGSO_NGAY', 0);

    //Format Date.
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fsDateFormat);

	SetDisplayFormat(QrPayroll, '0.0#;-0.0#;#');
    SetDisplayFormat(QrTimeSheet, ['NGAY'], 'dd');
//    SetDisplayFormat(QrDaily, ['NGAY'], 'dd');
    SetShortDateFormat(QrDaily);
    SetDisplayFormat(QrTimeSheet, [ 'GioVao_Ca', 'GioVao1_Ca', 'GioRa1_Ca', 'GioRa_Ca', 'OTDauCa_TuGio', 'OTDauCa_DenGio',
    'OTGiuaCa_TuGio', 'OTGiuaCa_DenGio', 'OTCuoiCa_TuGio', 'OTCuoiCa_DenGio'], sysMinFmt);


    // Customize
	SetCustomGrid([FORM_CODE + '_DSNV', FORM_CODE + '_NV'],
        [GrPayroll, GrTime]);
    SetCustomGrid([FORM_CODE + '_DSNGAY', FORM_CODE + '_NGAY'],
        [GrDaily, GrTime2]);

    SetDictionary([QrPayroll, QrTimeSheet], [FORM_CODE + '_DSNV', FORM_CODE + '_NV'], [FilterPayroll, Filter]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.FormShow(Sender: TObject);
begin
    // Open database
	OpenDataSets([QrShift, QrLeave, QrEmp, HrDataMain.QrV_HR_LOAI_NGAY_TINHCONG]);
    if mManv <> '' then
    begin
        sysMon := mMonth;
        sysYear := mYear;
    end;

    fFilterThaiSan := 0;
    CmdReload.Execute;

    if mManv <> '' then
    begin
        QrPayroll.Locate('Manv', mManv, []);
    end;
    // Go ahead
	GrPayroll.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrShift, QrLeave, QrEmp, HrDataMain.QrV_HR_LOAI_NGAY_TINHCONG]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.GetPeriod;
begin
	sysMon  := StrToInt(frMY.CbMon.Text);
    sysYear := StrToInt(frMY.CbYear.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdRefreshExecute(Sender: TObject);
var
    s: String;
	mSQL: String;
    mMon, mYear: Integer;
begin
    if frMY.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frMY.CbOrg.SelectedNode.StringData;
        fLevel := frMY.CbOrg.SelectedNode.Level;
	end;

    mMon := StrToInt(frMY.CbMon.Text);
    mYear := StrToInt(frMY.CbYear.Text);

	if	(fStr    = s) and
		(sysMon  = mMon)  and
        (sysYear = mYear) then
		Exit;

    Wait(DATAREADING);
    fStr := s;
//    if (mMon <> sysMon) or (mYear <> sysYear) or (PS_DAILY.IsEmpty) then
    if (mMon <> sysMon) or (mYear <> sysYear) then
    begin
        GetPeriod;

        with QrDaily do
        begin
            Close;
            Parameters[0].Value := sysMon;
            Parameters[1].Value := sysYear;
            Open;
        end;
    end;

    with QrPayroll do
    begin
		s := Sort;
    	Close;
        mSQL := ' 1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := sqlPayroll + ' and ' + mSQL;
        Parameters.Refresh;
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;

    end;
    if s = '' then
        s := 'ManvQL';
//
    SortDataSet(QrPayroll, s);

    OpenTimeSheet;

    ClearWait;

    PgMainChange(PgMain);
//
    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 0);
    if bClose then
    	Msg(RS_TIMESHEET_CFM_CLOSED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.OpenTimeSheet;
var
    s, mSQL: String;
begin
    with QrTimeSheet do
    begin
        s := Sort;
    	Close;
        mSQL := '1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := Format(sqlTimeSheet, [mSQL]);
        SQL.Add(' Order by Manv, [Ngay]');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;
    end;
    if s = '' then
        s := 'ManvQL';

    SortDataSet(QrTimeSheet, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdSearchExecute(Sender: TObject);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        1: exSearch(Name + '_1', DsTimeSheet);
        else exSearch(Name, DsPayroll);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdFilterExecute(Sender: TObject);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        0: FilterPayroll.Execute;
//        else FilterPayroll.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdGetRecLog1NVExecute(Sender: TObject);
begin
    CapnhatDulieuLichLamViec(QrPayroll.FieldByName('Manv').AsString);
    exReSyncRecord(QrTimeSheet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdGetRecLogExecute(Sender: TObject);
begin
    CapnhatDulieuLichLamViec;
    exReSyncRecord(QrTimeSheet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdUpdateExecute(Sender: TObject);
var
    bm: TBytes;
begin
    QrTimeSheet.CheckBrowseMode;
	GetPeriod;
    with QrPayroll do
    begin
        bm := BookMark;
        if HrDataMain.UpdateScheduleSheet(sysMon, sysYear, True) then
        begin
            MsgDone;
            CmdReload.Execute;
        end;
        BookMark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CapnhatDulieuLichLamViec(manv: String);
var
    ngayd, ngayc: TDateTime;
    b: Boolean;
begin
    with TS_PAYROLL_PERIOD do
    begin
        Prepared := True;
        Parameters[1].Value := sysYear;
        Parameters[2].Value := sysMon;
        Execute;
        if Parameters[0].Value <> 0 then
        begin
            MsgDone(False);
            Exit;
        end;
        ngayd := Parameters[3].Value;
        ngayc := Parameters[4].Value;
    end;

    if not FrmTuden.Execute(ngayd, ngayc) then
    	Exit;

    Wait(PROCESSING);
    try
        with TS_RECORDER_LOG do
        begin
            Prepared := True;
            Parameters[1].Value := ngayd;
            Parameters[2].Value := ngayc;
            Parameters[3].Value := manv;
            try
                Execute;
                b := Parameters[0].Value = 0;
                if b then
                begin
                    QrTimeSheet.Requery;
                end;
            except
                on E: Exception do
                    ErrMsg(E.Message);
            end;
        end;
    finally
        ClearWait;
        if b then
            MsgDone;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.AutoIO1;
begin
	with TS_AUTO_IO do
    begin
    	Prepared := True;
        Parameters[1].Value := QrPayroll.FieldByName('MANV').AsString;
        Parameters[2].Value := sysYear;
        Parameters[3].Value := sysMon;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.BtnXulyClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdAutoIO1Execute(Sender: TObject);
begin
	if not YesNo(Format(RS_TIMESHEET_AUTO1, [QrPayroll.FieldByName('LK_TENNV').AsString]), 1) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
	AutoIO1;
	QrTimeSheet.Requery;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
{$HINTS OFF}
procedure TFrmLichLamViec.CmdAutoIO2Execute(Sender: TObject);
begin
	if not YesNo(RS_TIMESHEET_AUTO2, 1) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
	CmdRefresh.Execute;

    with QrPayroll do
    begin
    	StartProgress(RecordCount);
    	DisableControls;
        First;
        Tag := 99;
        try
            while not Eof do
            begin
                SetProgressDesc(FieldByName('MANV').AsString);
                AutoIO1;
                Next;
                IncProgress;
            end;
            First;
        finally
            Tag := 0;
            EnableControls;
            StopProgress;
        end;
    end;

	QrTimeSheet.Requery;
    MsgDone;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdDayCsvExecute(Sender: TObject);
var
	b: Boolean;
    n: Integer;
    mIn, mOut, mDate: TDateTime;
	sFmt, sEmp, mFile: String;
	mLog: TStrings;
begin
	// Regional setting confirm
    SetLength(sFmt, 100);
    n := GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SSHORTDATE, @sFmt[1], 100);
    SetLength(sFmt, n - 1);

    if not YesNo(Format(RS_CFM_DATE_FORMAT, [sFmt])) then
    	Exit;

	// Get file name
	mFile := exGetImportFile;
    if mFile = '' then
    	Exit;

    // File preparing
//    if not CsvPrepareFile(mFile, 'Daily') then
//		Exit;
    Exit; //NTD

    QrTimeSheet.CheckBrowseMode;

    // Open data file
    with QrCsv do
    begin
    	b := True;
    	SQL.Text := 'select * from "Daily.csv"';
        try
	        Open;
        	sEmp := FieldByName('MANV').AsString;
		    mIn := FieldByName('IN').AsDateTime;
            mOut := FieldByName('OUT').AsDateTime;
            mDate := FieldByName('DATE').AsDateTime;
        except
        	ErrMsg(RS_INVALID_DATA);
            Close;
            b := False;
        end;

		if not b then
			Exit;

	    mLog := TStringList.Create;
        StartProgress(RecordCount);

        while not Eof do
        begin
        	sEmp := FieldByName('MANV').AsString;
		    mIn := Frac(FieldByName('IN').AsDateTime);
            mOut := Frac(FieldByName('OUT').AsDateTime);
            mDate := Trunc(FieldByName('DATE').AsDateTime);
        	SetProgressDesc(sEmp);

			with TS_DAILY_CSV do
            begin
            	Prepared := True;
                Parameters[1].Value := sEmp;
                Parameters[2].Value := mDate;
                if (mIn = 0.0) and (mOut = 0.0) then
                begin
	                Parameters[3].Value := NULL;
    	            Parameters[4].Value := NULL;
                end
                else
                begin
	                Parameters[3].Value := mIn;
    	            Parameters[4].Value := mOut;
                end;
                Execute;

                if Parameters[0].Value <> 0 then
                	mLog.Add(sEmp);
            end;

            IncProgress;
        	Next;
        end;
        Close;
        StopProgress;
    end;

    if mLog.Count > 0 then
    	exViewLog(RS_LOG_INVALID_EMP, mLog.Text);

    mLog.Free;
    CmdReload.Execute;
//    GrTime.SetFocus;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViec.StdConvert(pModel, pFile, pTemp: String): Boolean;
var
	ls, rs, temp: TStrings;
    i: Integer;
begin
	Result := True;
    if not FileExists(pFile) then
    begin
    	Result := False;
        Exit;
    end;

	ls := TStringList.Create;		// Rows buffer
    rs := TStringList.Create;		// Fields buffer / row
    temp := TStringList.Create;		// Data
    ls.LoadFromFile(pFile);
    try
        for i := 0 to ls.Count - 1 do
        begin
            if Pos(',Granted,', ls[i]) > 0 then
            begin
                isStrBreak(ls[i], ',', rs);
                temp.Add('0,'+ rs[5] + ',X,' + rs[1] + ',' + rs[2])
            end;
        end;
    except
    	Result := False;
    end;
    temp.SaveToFile(pTemp);
    ls.Free;
    rs.Free;
    temp.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdDayhstExecute(Sender: TObject);
//var
//	mFile, mTempFile, mModel: String;
//    bDate: TDateTime;
//
//    b: Boolean;
//    i, cRecId, cId, cFunc, cDate, cTime: Integer;
//    lsIn, lsOut, lsX, lsIn1, lsOut1, log, ls, rs: TStrings;
//
//    s, mRecId, mId, mFunc, mDelim, mIO: String;
//    mDate, mTime: TDateTime;
//
//    fs: TFormatSettings;
begin
	// Get params
//    Application.CreateForm(TFrmTimeRecOption, FrmTimeRecOption);
//    if not FrmTimeRecOption.Get(mModel, mFile, bDate) then
//        Exit;
//    // Convert to specified format
//	mTempFile := vlGetTempDir + 'Hrms.hst';
//    if not StdConvert(mModel, mFile, mTempFile) then
//    	Exit;
//
//    // Init
//    ls := TStringList.Create;		// Rows buffer
//    rs := TStringList.Create;		// Fields buffer / row
//    log := TStringList.Create;		// Invalid Id
//
//    lsIn := TStringList.Create;		// List of In recorders
//    lsOut := TStringList.Create;	// List of Out recorders
//    lsX := TStringList.Create;		// List of Detect recorders
//
//    lsIn1 := TStringList.Create;	// List of In functions
//    lsOut1 := TStringList.Create;	// List of Out functions
//
//    // I/O settings
//    vlStrBreak(FlexReadString(mModel, 'In'), ',', lsIn);
//    vlStrBreak(FlexReadString('Out'), ',', lsOut);
//    vlStrBreak(FlexReadString('Detect'), ',', lsX);
//
//    vlStrBreak(FlexReadString('In1'), ',', lsIn1);
//    vlStrBreak(FlexReadString('Out1'), ',', lsOut1);
//
//    mDelim := FlexReadString(mModel, 'Delim', ' ');
//    if mDelim[1] = '#' then
//    	mDelim := Chr(StrToInt(vlSubStr(mDelim, 2)));
//
//	// Locale format settings
//	GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fs);
//    fs.ShortDateFormat := FlexReadString(mModel, 'DateFormat', fs.ShortDateFormat);
//    fs.ShortTimeFormat := FlexReadString(mModel, 'TimeFormat', fs.ShortTimeFormat);
//    s := FlexReadString(mModel, 'DateSeparator', fs.DateSeparator);
//    if s <> '' then
//        fs.DateSeparator := s[1];
//    s := FlexReadString(mModel, 'TimeSeparator', fs.TimeSeparator);
//    if s <> '' then
//        fs.TimeSeparator := s[1];
//
//    // Data columns order
//    cRecId := FlexReadInteger(mModel, 'MacID', -1);		// Recorder
//    cId := FlexReadInteger('ID');							// Emp/Card ID
//    cFunc := FlexReadInteger('Function');					// Function
//    cDate := FlexReadInteger('Date');						// Date
//    cTime := FlexReadInteger('Time');						// Time
//
//    // Load data
//	ls.LoadFromFile(mTempFile);
//
//    // Proc
//    StartProgress(ls.Count, 'Analizing');
//    for i := 0 to ls.Count - 1 do
//    begin
//    	// Parse
//    	vlStrBreak(ls[i], mDelim, rs);
//		IncProgress;
//
//        b := False;
//        try
//        	// Recorder
//            if cRecId < 0 then
//            	mRecId := ''
//            else
//            	mRecId := rs[cRecId];
//
//        	// Emp/Card ID
//        	mId := rs[cId];
//
//            // Function
//            if cFunc < 0 then
//            	mFunc := ''
//            else
//            	mFunc := rs[cFunc];
//
//            if cTime < 0 then		// Date and time contigous
//            begin
//	            mDate := StrToDateTime(rs[cDate], fs);
//    	        mTime := Frac(mDate);
//                mDate := Trunc(mDate);
//            end
//            else
//            begin
//	            mDate := StrToDate(rs[cDate], fs);
//    	        mTime := StrToTime(rs[cTime]);
//            end;
//        except
//            b := True;
//        end;
//
//        if b then
//        begin
//        	ShowProgress(False);
//        	if not YesNo(Format(RS_IMPORT_ERROR_AT, [i])) then
//            	Break;
//        	ShowProgress;
//            Continue;
//        end;
//
//        // Proc
//	    SetProgressDesc(mId);
//
//        if mDate >= bDate then
//        begin
//        	mIO := '';
//        	if mRecId <> '' then
//            begin
//	        	if lsIn.IndexOf(mRecId) >= 0 then
//                	mIO := 'I'
//                else if lsOut.IndexOf(mRecId) >= 0 then
//                	mIO := 'O'
//                else if lsX.IndexOf(mRecId) >= 0 then
//                	mIO := 'X'
//            end;
//
//            if mIO = '' then
//                if mFunc <> '' then
//                begin
//                    if lsIn1.IndexOf(mFunc) >= 0 then
//                        mIO := 'I'
//                    else if lsOut1.IndexOf(mFunc) >= 0 then
//                        mIO := 'O'
//		            else
//                        mIO := 'X'
//                end
//                else
//					mIO := 'X';
//
//            if mIO = 'I' then
//            begin
//                if not InRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//            else if mIO = 'O' then
//            begin
//                if not OutRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//            else if mIO = 'X' then
//            begin
//                if not DetectRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//		end;
//    end;
//    StopProgress;
//
//    if log.Count > 0 then
//    	exViewLog(RS_LOG_INVALID_CARD, log.Text);
//
//    ls.Free;
//    rs.Free;
//    log.Free;
//    lsIn.Free;
//    lsOut.Free;
//    lsX.Free;
//    lsIn1.Free;
//    lsOut1.Free;
//
//    // Done
//	// DeleteFile(mTempFile);
//    CmdReload.Execute;
//    GrTime.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.GrTimeCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (UpperCase(s) = 'NGAY') or (UpperCase(s) = 'THU') then
        begin
            AFont.Size  := 10;
            AFont.Style := [];
            AFont.Color  := clBlack;

            case DataSource.DataSet.FieldByName('LoaiNgay').AsInteger of
            2:
            begin
                AFont.Color := clBlue;
                AFont.Style := [fsBold];
            end;
            3:
            begin
                AFont.Color := clRed;
                AFont.Style := [fsBold];
            end
            else
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.GrTime2CalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(s).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (gdFixed in State) then
        begin
            AFont.Size  := 10;
            AFont.Style := [];
            AFont.Color  := clBlack;
            case DataSource.DataSet.FieldByName('LoaiNgay').AsInteger of
                2:
                    AFont.Color := clBlue;
                3:
                    AFont.Color := clRed;
            else
            end;
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        1: Status.Panels[0].Text := exRecordCount(QrTimeSheet, Filter);
        else Status.Panels[0].Text := exRecordCount(QrPayroll, FilterPayroll);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
	WD_NAMES: array[1..14] of String = (
    	'CN',
    	'T2',
    	'T3',
    	'T4',
    	'T5',
    	'T6',
    	'T7',
        'Su',
        'Mo',
        'Tu',
        'We',
        'Th',
        'Fr',
        'Sa'
    );
procedure TFrmLichLamViec.QrTimeSheetCalcFields(DataSet: TDataSet);
var
	n: Integer;
begin
	n := Iif(sysEnglish, 7, 0);
	with DataSet do
       	FieldByName('THU').AsString := WD_NAMES[n + DayOfWeek(FieldByName('NGAY').AsDateTime)];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrDailyAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdParamsExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_BANG_THONGSO'); //NTD
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBangThongso, FrmBangThongso);
    FrmBangThongso.Execute(R_FULL, 3, StrToInt(frMY.cbMon.Text), StrToInt(frMY.CbYear.Text));
    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.Clear1;
begin
	with CLEAR_DAILY_DATA do
    begin
    	Prepared := True;
        Parameters[1].Value := QrPayroll.FieldByName('Manv').AsString;
        Parameters[2].Value := sysYear;
        Parameters[3].Value := sysMon;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdClear1Execute(Sender: TObject);
begin
	if not YesNo(Format(RS_TIMESHEET_CLEAR1, [sysMon, sysYear, QrPayroll.FieldByName('LK_Tennv').AsString]), 1) then
    	Exit;

    XoaDulieuLichLamViec(QrPayroll.FieldByName('Manv').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdClear2Execute(Sender: TObject);
begin
    if not YesNo(Format(RS_TIMESHEET_CLEAR4, [sysMon, sysYear]), 1) then
    	Exit;

    XoaDulieuLichLamViec;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdClear3Execute(Sender: TObject);
begin
   if not YesNo(Format(RS_TIMESHEET_CLEAR3, [FormatDateTime('DD/MM/YYYY', QrTimeSheet.FieldByName('Ngay').AsDateTime), QrTimeSheet.FieldByName('SoCa').AsInteger, QrPayroll.FieldByName('LK_Tennv').AsString]), 1) then
    	Exit;

   XoaDulieuLichLamViecDongChon(QrPayroll.FieldByName('Manv').AsString, QrTimeSheet.FieldByName('Ngay').AsDateTime, QrTimeSheet.FieldByName('SoCa').AsInteger);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdClearFilterExecute(Sender: TObject);
begin
    with FilterPayroll do
    begin
        FieldInfo.Clear;
		ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdNewListExecute(Sender: TObject);
var
    mTransNo: Integer;
begin
    mTransNo := 0;
    Application.CreateForm(TFrmLichLamViecTools, FrmLichLamViecTools);
    if not FrmLichLamViecTools.Execute(r, FORM_CODE, mTransNo, sysMon, sysYear) then
        Exit;

    QrTimeSheet.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, b2: Boolean;
    n: Integer;
    bBrowseTime: Boolean;
begin

    n := PgMain.ActivePageIndex;
    CmdClearFilter.Enabled := FilterPayroll.FieldInfo.Count > 0;

    b := mCanEdit and (not bClose);
    CmdUpdate.Enabled := b;
//    CmdTimeRec.Enabled := b;
    CmdDayCsv.Enabled := b;
    CmdDayhst.Enabled := b;

    CmdClear1.Enabled := b and (n = 0);
    CmdAutoIO1.Enabled := b and (n = 0);

    CmdCalc2.Enabled := b;
    CmdClear2.Enabled := b;
    CmdAutoIO2.Enabled := b;

    CmdValidate.Enabled := (n = 0);
    CmdValidateNext.Enabled := (n = 0);

    DsTimeSheet.AutoEdit := b;

    with QrTimeSheet do
    begin
        if not Active then
            Exit;
        b2 := IsEmpty;
        bBrowseTime := State in [dsBrowse];
    end;

    GrTime.ReadOnly := b2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdRepExecute(Sender: TObject);
var
	r: WORD;
begin
//	r := GetRights('COLEX_REPORT');
//    if r = R_DENY then
//    	Exit;
//
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('CongLuong') (*3*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrPayroll then
    	GrTime.SetFocus
	else if ActiveControl = GrTime then
    	GrPayroll.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrShiftAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrShift, ['GIOVAO', 'GIORA', 'GIOVAO1', 'GIORA1'], 'hh:mm');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdDetailExecute(Sender: TObject);
var
	p: TPoint;
    s: String;
begin
    if (Sender as TwwDBGrid2).GetActiveField.FieldName = 'VAORA_THO'  then
        Exit;

	// Field list
   	s := FlexConfigString(FORM_CODE, 'Editable');
    if s = '' then
    	Exit;

	p.x := GrTime.Left + GrTime.Width;
    p.y := GrTime.Top;
    p := ClientToScreen(p);

    GrTime.Enabled := False;
	Application.CreateForm(TFrmBosungCong, FrmBosungCong);
    with FrmBosungCong do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y + 2;
    	Execute(DsTimeSheet, s);
    end;
    GrTime.Enabled := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.ValidationCheck;
var
	b: Boolean;
    s: String;
    d: TDateTime;
    soCa: Integer;
begin
	with QrTimeSheet do
    begin
    	CheckBrowseMode;
    	DisableControls;
	end;

    if nx then
		QrTimeSheet.Next
    else
    	QrPayroll.First;

    b := False;
    while (not b) and (not QrPayroll.Eof) do
    begin
    	s := QrPayroll.FieldByName('Manv').AsString;

		with QrTimeSheet do
    	begin
            while (not b) and (not Eof) do
            begin
            	d := FieldByName('Ngay').AsDateTime;
                soCa := FieldByName('SoCa').AsInteger;
                try
	            	with TS_DAILY_CHECK do
    	            begin
        	        	Parameters[1].Value := s;
            	    	Parameters[2].Value := d;
                        Parameters[3].Value := soCa;
                	    Execute;
	                end;
					b := False;
                except
					s := DataMain.Conn.Errors[0].Description;
					b := True;
                end;

                if b then
                begin
					EnableControls;
                    Refresh;
					PopMsg(s);
                end
                else
	            	Next;
            end;
	    end;
        if not b then
	        QrPayroll.Next;
    end;

	QrTimeSheet.EnableControls;
    if b then
        GrTime.SetFocus
    else
	    Msg(RS_TIMESHEET_VALID1);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.XoaDulieuLichLamViec(manv: String);
var
    b: Boolean;
begin
    Wait(PROCESSING);
    try
        with spHR_XOALICHLV do
        begin
            Prepared := True;
            Parameters[1].Value := sysYear;
            Parameters[2].Value := sysMon;
            Parameters[3].Value := manv;
            try
                Execute;
                b := Parameters[0].Value = 0;
                if b then
                begin
//                    exReSyncRecord(QrPayroll, True);
//                    exReSyncRecord(QrTimeSheet, True);
                    QrTimeSheet.Requery;
//                    CmdReload.Execute;
                end;
            except
                on E: Exception do
                    ErrMsg(E.Message);
            end;
        end;
    finally
        ClearWait;
        if b then
            MsgDone;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdValidateExecute(Sender: TObject);
begin
	ValidationCheck(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdValidateNextExecute(Sender: TObject);
begin
	ValidationCheck(True);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViec.DetectRec;
begin
	with TS_RECORDER_DETECT do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViec.InRec;
begin
	with TS_RECORDER_IN do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViec.OutRec;
begin
	with TS_RECORDER_OUT do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.CmdReloadExecute(Sender: TObject);
begin
    QrEmp.Requery;
    QrShift.Requery;
    QrLeave.Requery;

	sysMon := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.PgMainChange(Sender: TObject);
var
    s, manv: String;
    n: Integer;
begin
    if not QrPayroll.Active then
        Exit;

    n := PgMain.ActivePageIndex;
    manv := QrPayroll.FieldByName('Manv').AsString;
    Screen.Cursor := crSQLWait;
    case n of
        0:
        begin
            if QrTimeSheet.Active then
                QrTimeSheet.Filter := 'Manv=' + QuotedStr(manv);
        end;
        1:
        begin
            if QrTimeSheet.Active then
            begin
                s := '[Ngay]>=''' + DateToStr(QrDaily.FieldByName('Ngay').AsDateTime, fsDateFormat) + '''' +
                ' and [Ngay]<''' + DateToStr(QrDaily.FieldByName('Ngay').AsDateTime + 1, fsDateFormat) + '''';

                QrTimeSheet.Filter := s;
            end;
        end;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.Pop1Popup(Sender: TObject);
var
	s: String;
    soCa: Integer;
begin
	s := QrPayroll.FieldByName('LK_Tennv').AsString;
    if s <> '' then
    begin
        Item2.Caption := RS_TIMESHEET_SCHEDULE_POPUP2 + ' "' + s + '"';
        Item3.Caption := RS_TIMESHEET_SCHEDULE_POPUP3 + ' "' + s + '"'
    end
    else
    begin
        Item2.Caption := (Item2.Action as TAction).Caption;
        Item3.Caption := (Item3.Action as TAction).Caption;
    end;

    soCa := QrTimeSheet.FieldByName('SoCa').AsInteger;
    if soCa > 1 then
    begin
        Item4.Caption := RS_TIMESHEET_SCHEDULE_POPUP4 + ' "' + s + '"';
        Item4.Visible := True;
        N2.Visible := True;
    end
    else
    begin
        Item4.Caption := (Item4.Action as TAction).Caption;
        Item4.Visible := False;
        N2.Visible := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrTimeSheetBeforePost(DataSet: TDataSet);
begin
	with (QrTimeSheet) do
    begin
        FieldByName('UPDATE_BY').AsInteger := sysLogonUID;
        FieldByName('UPDATE_DATE').AsDateTime := Now;
		SetNull(QrTimeSheet, ['MaCa']);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrTimeSheetMACAChange(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString = '' then
        begin
            FieldByName('GioVao_Ca').Clear;
            FieldByName('GioVao1_Ca').Clear;
            FieldByName('GioRa1_Ca').Clear;
            FieldByName('GioRa_Ca').Clear;
            FieldByName('SoGio_Ca').Clear;
        end
        else
        begin
            FieldByName('GioVao_Ca').Value  := QrShift.FieldByName('GioVao').Value;
            FieldByName('GioVao1_Ca').Value := QrShift.FieldByName('GioVao1').Value;
            FieldByName('GioRa1_Ca').Value := QrShift.FieldByName('GioRa1').Value;
            FieldByName('GioRa_Ca').Value := QrShift.FieldByName('GioRa').Value;
            FieldByName('SoGio_Ca').Value := QrShift.FieldByName('SoGio').Value;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrTimeSheetOTCuoiCa_TuGioValidate(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString <> '' then
        begin
            if  (not FieldByName('OTCuoiCa_TuGio').IsNull) and
                (not FieldByName('OTCuoiCa_DenGio').IsNull) and
                not overTimeDateValid(
                    FieldByName('Manv').AsString,
                    FieldByName('MaCa').AsString,
                    FieldByName('Ngay').AsDateTime,
                    FieldByName('OTCuoiCa_TuGio').AsDateTime,
                    FieldByName('OTCuoiCa_DenGio').AsDateTime,
                    3) then
            begin
                Abort;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrTimeSheetOTDauCa_TuGioValidate(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString <> '' then
        begin
            if  (not FieldByName('OTDauCa_TuGio').IsNull) and
                (not FieldByName('OTDauCa_DenGio').IsNull) and
                not overTimeDateValid(
                    FieldByName('Manv').AsString,
                    FieldByName('MaCa').AsString,
                    FieldByName('Ngay').AsDateTime,
                    FieldByName('OTDauCa_TuGio').AsDateTime,
                    FieldByName('OTDauCa_DenGio').AsDateTime,
                    1) then
            begin
                Abort;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrTimeSheetOTGiuaCa_TuGioValidate(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString <> '' then
        begin
            if  (not FieldByName('OTGiuaCa_TuGio').IsNull) and
                (not FieldByName('OTGiuaCa_DenGio').IsNull) and
                not overTimeDateValid(
                    FieldByName('Manv').AsString,
                    FieldByName('MaCa').AsString,
                    FieldByName('Ngay').AsDateTime,
                    FieldByName('OTGiuaCa_TuGio').AsDateTime,
                    FieldByName('OTGiuaCa_DenGio').AsDateTime,
                    2) then
            begin
                Abort;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrDailyCalcFields(DataSet: TDataSet);
var
	n: Integer;
begin
	n := Iif(sysEnglish, 7, 0);
	with DataSet do
       	FieldByName('Thu').AsString := WD_NAMES[n + DayOfWeek(FieldByName('Ngay').AsDateTime)];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrPayrollAfterScroll(DataSet: TDataSet);
begin
    if QrPayroll.Tag <> 99 then
        PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.QrPayrollCalcFields(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    with QrPayroll do
    begin
        DecodeDate(FieldByName('NgayVaoLam').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLam').AsDateTime := FieldByName('NgayVaoLam').AsDateTime
        else
            FieldByName('Calc_NgayVaoLam').Clear;

        DecodeDate(FieldByName('NgayBatDauHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauHocViec').AsDateTime := FieldByName('NgayBatDauHocViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauHocViec').Clear;

        DecodeDate(FieldByName('NgayKetThucHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucHocViec').AsDateTime := FieldByName('NgayKetThucHocViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucHocViec').Clear;

        DecodeDate(FieldByName('NgayBatDauThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauThuViec').AsDateTime := FieldByName('NgayBatDauThuViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauThuViec').Clear;

        DecodeDate(FieldByName('NgayKetThucThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucThuViec').AsDateTime := FieldByName('NgayKetThucThuViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucThuViec').Clear;

        DecodeDate(FieldByName('NgayVaoLamChinhThuc').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLamChinhThuc').AsDateTime := FieldByName('NgayVaoLamChinhThuc').AsDateTime
        else
            FieldByName('Calc_NgayVaoLamChinhThuc').Clear;

        DecodeDate(FieldByName('NgayThoiViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayThoiViec').AsDateTime := FieldByName('NgayThoiViec').AsDateTime
        else
            FieldByName('Calc_NgayThoiViec').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.ApplicationEvents1Hint(Sender: TObject);
begin
	if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	try
		QrTimeSheet.CheckBrowseMode;
        CanClose := True;
    except
        CanClose := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViec.overTimeDateValid;
var
    s: String;
begin
    with spHR_BANG_LICHLV_Invalid_GioTangCa do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := pManv;
        Parameters.ParamByName('@MaCa').Value := pMaCa;
        Parameters.ParamByName('@Ngay').Value := pDate;
        Parameters.ParamByName('@TuGio').Value := pDateFrom;
        Parameters.ParamByName('@DenGio').Value := pDateTo;
        Parameters.ParamByName('@LoaiCa').Value := pLoaiCa;
        try
            Execute;
        except
        end;

        if Parameters.FindParam('@STR') <> nil then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;



(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViec.XoaDulieuLichLamViecDongChon;
var
    ngayd, ngayc: TDateTime;
    b: Boolean;
begin
    Wait(PROCESSING);
    try
        with spHR_XOALICHLV_THEOSOCA do
        begin
            Prepared := True;
            Parameters[1].Value := sysYear;
            Parameters[2].Value := sysMon;
            Parameters[3].Value := manv;
            Parameters[4].Value := pDate;
            Parameters[5].Value := pSoCa;
            try
                Execute;
                b := Parameters[0].Value = 0;
                if b then
                begin
                    QrTimeSheet.Requery;
                end;
            except
                on E: Exception do
                    ErrMsg(E.Message);
            end;
        end;
    finally
        ClearWait;
        if b then
            MsgDone;
    end;

end;

end.
