object FrmDmTK_NV: TFrmDmTK_NV
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c T'#224'i Kho'#7843'n - Nh'#226'n Vi'#234'n'
  ClientHeight = 409
  ClientWidth = 727
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 727
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton10: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 388
    Width = 727
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 260
    Top = 36
    Width = 467
    Height = 352
    Align = alRight
    BorderOuter = fsLowered
    HotSpotHighlight = 11855600
    HotSpotIgnoreMargins = True
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 2
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PD1: TisPanel
      Left = 10
      Top = 2
      Width = 455
      Height = 132
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = ' :: Th'#244'ng tin'
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      DesignSize = (
        455
        132)
      object TntLabel3: TLabel
        Left = 8
        Top = 100
        Width = 78
        Height = 16
        Alignment = taRightJustify
        Caption = 'Ng'#224'y s'#7917' d'#7909'ng'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdMA: TDBEditEh
        Tag = 1
        Left = 92
        Top = 24
        Width = 354
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BevelKind = bkFlat
        BorderStyle = bsNone
        CharCase = ecUpperCase
        ControlLabel.Width = 71
        ControlLabel.Height = 16
        ControlLabel.Caption = 'S'#7889' t'#224'i kho'#7843'n'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'MATK'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <>
        EmptyDataInfo.Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 0
        Visible = True
      end
      object CbNGAY: TwwDBDateTimePicker
        Left = 92
        Top = 96
        Width = 101
        Height = 22
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NGAY'
        DataSource = DsDanhmuc
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = True
        TabOrder = 6
      end
      object CbNganHang: TDbLookupComboboxEh2
        Left = 92
        Top = 48
        Width = 274
        Height = 22
        ControlLabel.Width = 61
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        Anchors = [akLeft, akTop, akRight]
        DynProps = <>
        DataField = 'MANH'
        DataSource = DsDanhmuc
        DropDownBox.Columns = <
          item
            FieldName = 'TENNH'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 200
          end
          item
            FieldName = 'MANH'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'M'#227
            Width = 40
          end>
        DropDownBox.ListSource = DataMain.DsNganhang
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        EmptyDataInfo.Color = clInfoBk
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MANH'
        ListField = 'TENNH'
        ListSource = DataMain.DsNganhang
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 1
        Visible = True
      end
      object DBEditEh4: TDBEditEh
        Left = 369
        Top = 48
        Width = 77
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clBtnFace
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'MANH'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        Visible = True
      end
      object CbChinhanh: TDbLookupComboboxEh2
        Left = 92
        Top = 72
        Width = 274
        Height = 22
        ControlLabel.Width = 57
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Chi nh'#225'nh'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        Anchors = [akLeft, akTop, akRight]
        DynProps = <>
        DataField = 'MANH_CN'
        DataSource = DsDanhmuc
        DropDownBox.Columns = <
          item
            FieldName = 'TENNH_CN'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 200
          end
          item
            FieldName = 'MA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'M'#227
            Width = 40
          end>
        DropDownBox.ListSource = DsNganhangCN
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MANH_CN'
        ListField = 'TENNH_CN'
        ListSource = DataMain.DsNganhangCN
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 3
        Visible = True
        OnDropDown = CbChinhanhDropDown
      end
      object EdChiNhanh: TDBEditEh
        Left = 369
        Top = 72
        Width = 77
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clBtnFace
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'LK_MANH_CN_MA'
        DataSource = DsDanhmuc
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        Visible = True
      end
    end
    object PD2: TisPanel
      Left = 10
      Top = 134
      Width = 455
      Height = 216
      Align = alClient
      Color = 16119285
      ParentBackground = False
      TabOrder = 1
      HeaderCaption = ' .: Ghi ch'#250
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 99
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWhite
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object DBMemo1: TDBMemo
        Left = 1
        Top = 17
        Width = 453
        Height = 198
        Align = alClient
        BorderStyle = bsNone
        DataField = 'GHICHU'
        DataSource = DsDanhmuc
        TabOrder = 1
      end
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 260
    Height = 352
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MATK'#9'20'#9'S'#7889#9'F'
      'NGAY'#9'12'#9'Ng'#224'y s'#7917' d'#7909'ng'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDanhmuc
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MATK'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 196
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDanhmucBeforeInsert
    AfterInsert = QrDanhmucAfterInsert
    BeforePost = QrDanhmucBeforePost
    AfterPost = QrDanhmucAfterPost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.*, b.TENNH'
      '  from'#9'HR_DM_TAIKHOAN a'
      '                join DM_NGANHANG b on b.MANH = a.MaNH'
      'where 1 = 1')
    Left = 148
    Top = 196
    object QrDanhmucMATK: TWideStringField
      DisplayLabel = 'S'#7889' t'#224'i kho'#7843'n'
      FieldName = 'MATK'
      OnChange = QrDanhmucMATKChange
      Size = 30
    end
    object QrDanhmucGHICHU: TWideMemoField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GHICHU'
      BlobType = ftWideMemo
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDanhmucNGAY: TDateTimeField
      FieldName = 'NGAY'
      Visible = False
    end
    object QrDanhmucMANV: TWideStringField
      FieldName = 'MANV'
      Visible = False
      Size = 15
    end
    object QrDanhmucMANH: TWideStringField
      FieldName = 'MANH'
      OnChange = QrDanhmucMANHChange
      Size = 10
    end
    object QrDanhmucMANH_CN: TWideStringField
      FieldName = 'MANH_CN'
      Size = 26
    end
    object QrDanhmucLK_MANH_CN_MA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH_CN_MA'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MANH_CN'
      LookupResultField = 'MA'
      KeyFields = 'MANH_CN'
      Lookup = True
    end
    object QrDanhmucIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
    object QrDanhmucLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'MANH'
      Lookup = True
    end
    object QrDanhmucLK_CHINHANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CHINHANH'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MANH_CN'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'MANH_CN'
      Lookup = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 148
    Top = 224
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 148
    Top = 256
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 212
    Top = 224
  end
  object DsNganhangCN: TDataSource
    AutoEdit = False
    DataSet = QrNganhangCN
    Left = 84
    Top = 224
  end
  object QrNganhangCN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_NGANHANG_CHINHANH')
    Left = 80
    Top = 182
  end
  object spHR_TAIKHOAN_NGANHANG_NGAYSUDUNG_HOPLE: TADOCommand
    CommandText = 'spHR_TAIKHOAN_NGANHANG_NGAYSUDUNG_HOPLE;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MaNH'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 200
        Value = Null
      end
      item
        Name = '@Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 132
    Top = 84
  end
end
