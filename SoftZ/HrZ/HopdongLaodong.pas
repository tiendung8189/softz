﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HopdongLaodong;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  RzPanel, RzSplit, isPanel, Vcl.Buttons, DBCtrlsEh, DBGridEh, DBLookupEh,
  frameEmp, DbLookupComboboxEh2;

type
  TFrmHopdongLaodong = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    btnHopDong: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    ToolButton15: TToolButton;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    PaThongTin: TisPanel;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    spHR_TINH_HETHAN_HDLD: TADOCommand;
    PopIn: TAdvPopupMenu;
    Hpngcngtcvin1: TMenuItem;
    Hpnghcvic1: TMenuItem;
    Hpngthvic1: TMenuItem;
    Hpnglaong1: TMenuItem;
    N4: TMenuItem;
    Phlchpnghcvic1: TMenuItem;
    Phlchpngthvic1: TMenuItem;
    Phlchpnglaong1: TMenuItem;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterSoHopDong: TWideStringField;
    QrMasterMaHopDong: TWideStringField;
    QrMasterSoThang: TFloatField;
    QrMasterSoHopDong_Goc: TWideStringField;
    QrMasterNgayKy: TDateTimeField;
    QrMasterNgayHieuLuc: TDateTimeField;
    QrMasterNgayHetHan: TDateTimeField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    QrMasterLK_TENNV: TWideStringField;
    QrMasterLK_TEN_HDLD: TWideStringField;
    QrMasterLK_TEN_HDLD_TA: TWideStringField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrMasterGhiChu: TWideMemoField;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterUPDATE_NAME: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    QrMasterLK_SoThang: TIntegerField;
    CmdLoaiHopDong: TAction;
    CmdHopDong: TAction;
    QrDMHopDong: TADOQuery;
    QrDMHopDongFileName: TWideStringField;
    QrDMHopDongFileExt: TWideStringField;
    QrDMHopDongFileContent: TBlobField;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    QrMasterCalc_FileName: TWideStringField;
    DsEmp: TDataSource;
    QrMasterLK_ManvQL: TWideStringField;
    frEmp1: TfrEmp;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    QrMasterMaLoai_HopDong: TWideStringField;
    QrHopDongGoc: TADOQuery;
    DsHopDongGoc: TDataSource;
    PaNhanVien: TPanel;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    PaInfo: TPanel;
    CbMaLoaiHopDong: TDbLookupComboboxEh2;
    EdSoHopDong: TDBEditEh;
    CbLoaiHopDong: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    DBNumberEditEh1: TDBNumberEditEh;
    Label8: TLabel;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    Label1: TLabel;
    CbToDate: TwwDBDateTimePicker;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    CbHopDongGoc: TDbLookupComboboxEh2;
    PaTop: TPanel;
    spHR_LICHSU_HOPDONG_Get_Last_By_Manv_Ngay: TADOCommand;
    QrMasterLK_MaLoai_HopDong_Ma: TWideStringField;
    QrMasterFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    QrMasterIdx_HopDong: TIntegerField;
    QrMasterIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure QrMasterApprovalChange(Sender: TField);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure QrMasterNgayKyChange(Sender: TField);
    procedure QrMasterMaHopDongChange(Sender: TField);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdLoaiHopDongExecute(Sender: TObject);
    procedure CmdHopDongExecute(Sender: TObject);
    procedure QrDMHopDongBeforeOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterManvChange(Sender: TField);
    procedure CbHopDongGocDropDown(Sender: TObject);
    procedure QrMasterMaLoai_HopDongChange(Sender: TField);
    procedure QrMasterAfterDelete(DataSet: TDataSet);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, mType: Integer;
    mSQL, fStr, mEmpID, mEmpIDLabel, mEmpName: String;
    FORM_CODE, REPORT_NAME: String;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
    procedure ViewContract();
    procedure MostRecentContract();
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmHopdongLaodong: TFrmHopdongLaodong;

const
    TABLE_NAME = 'HR_LICHSU_HOPDONG';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmHopdong, HrData, HrExCommon; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.FormCreate(Sender: TObject);
begin
    AddAllFields(QrMaster, TABLE_NAME);
    FORM_CODE := 'HR_LICHSU_HOPDONG';
    REPORT_NAME := 'HR_RP_LICHSU_HOPDONG';
    mType := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);
 
    with HrDataMain do
        OpenDataSets([QrDMNV, HR_DM_HOPDONG, QrV_HR_LOAI_HOPDONG]);

    OpenDataSets([QrHopDongGoc]);

    if mEmpID <> '' then
    begin
        TMyForm(Self).Init1;
        Width := 1260;
        Height := 612;
        Caption := 'Hồ Sơ Nhân Viên - Hợp Đồng Lao Động';
        fStr := mEmpID;
        frD2D.EdFrom.Date := Date;
        frD2D.EdTo.Date := Date;
        frD2D.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
        FORM_CODE := 'HR_LICHSU_NHANVIEN_HOPDONG';
        REPORT_NAME := 'HR_RP_LICHSU_NHANVIEN_HOPDONG';
        mType := 1;
        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        PaThongTin.Height := PaThongTin.Height - PaNhanVien.Height;
    end
    else
    begin
        TMyForm(Self).Init2;
    end;

    SetDisplayFormat(QrMaster, ctCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['NgayHieuLuc'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        if mEmpID <> '' then
            Add(' and Manv =:Manv')
        else
        begin
           Add(' and [NgayKy] between :Tungay and :Denngay' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayHieuLuc], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp, QrHopDongGoc]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    if mEmpID <> '' then
        CbMaLoaiHopDong.SetFocus
    else
        CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
    begin
        if mEmpID <> '' then
            CbMaLoaiHopDong.SetFocus
        else
            CbNhanVien.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdPrint2Execute(Sender: TObject);
var
    n: Integer;
    filename, repname: String;
begin
    n := (Sender as TComponent).Tag;

    repname := 'HR_RP_HOPDONG_WORD';
    filename := 'DOC\' +'HR_RP_HOPDONG_WORD_' + IntToStr(n) + '.docx';

    with QrMaster do
        DataOffice.CreateReport2(filename ,
            [sysLogonUID, FieldByName('Idx').AsInteger], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    if mType <> 0 then
        fStr := mEmpID;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        mType, //0: Lich su, 1: HSNV/Lich Su
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;
            if mEmpID <> '' then
                Parameters[0].Value := mEmpID
            else
            begin
                sSQL := '1=1';

                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
                    end;


                SQL.Text := Format(mSQL, [sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'NgayHieuLuc';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdFileMinusExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdHopDongExecute(Sender: TObject);
begin
    ViewContract;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdLoaiHopDongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOPDONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmHopdong, FrmDmHopdong);
    FrmDmHopdong.Execute(r);
    HrDataMain.HR_DM_HOPDONG.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bViewFile, bHopDong: Boolean;
begin
    bViewFile := False;
    bHopDong := False;
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
        if Active then
        begin
            bViewFile := FieldByName('FileExt').AsString <> '';
            bHopDong := FieldByName('MaHopDong').AsString <> '';
        end;

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdFileMinus.Enabled := bViewFile and not bBrowse;

    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;
    CmdClear.Enabled := (not bEmpty) and mCanEdit;
    CmdHopDong.Enabled := bBrowse and bHopDong;
    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'MaLoai_HopDong', 'SoHopDong', 'MaHopDong', 'NgayKy', 'NgayHieuLuc']) then
            Abort;

        if (UpperCase(Trim(CbMaLoaiHopDong.Text)) = UpperCase('Phụ lục hợp đồng'))
        and (FieldByName('Idx_HopDong').IsNull) then
        begin
            CbHopDongGoc.SetFocus;
            ErrMsg('Phải nhập "' + FieldByName('Idx_HopDong').DisplayLabel + '"');
            Abort;
        end;

    end;
    SetAudit(DataSet);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterCalcFields(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrDMHopDongBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := QrMaster.FieldByName('MaHopDong').AsString;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        if mEmpID <> '' then
            FieldByName('Manv').AsString := mEmpID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterApprovalChange(Sender: TField);
begin
    exDotManv(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CbHopDongGocDropDown(Sender: TObject);
var
    s: string;
    Idx: Integer;
begin
    QrHopDongGoc.Filter := 'Manv=@';
    s := QrMaster.FieldByName('Manv').AsString;
    if s <> '' then
    begin
        Idx := QrMaster.FieldByName('Idx').AsInteger;
        QrHopDongGoc.Filter := Format('Manv=%s and Idx<>%d', [s, Idx]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdSwitchExecute(Sender: TObject);
begin
    if mEmpID <> '' then
    begin
        EdSoHopDong.SetFocus;
        GrList.SetFocus;
    end
    else
    begin
        if ActiveControl = GrList then
            CbNhanVien.SetFocus
        else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
            frD2D.EdFrom.SetFocus
        else
            GrList.SetFocus
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterMaHopDongChange(Sender: TField);
begin
    with QrMaster, spHR_TINH_HETHAN_HDLD do
    begin
        if FieldByName('NgayHieuLuc').IsNull then
            FieldByName('NgayHetHan').Clear
		else
        begin
            Prepared := True;
            Parameters[1].Value := FieldByName('NgayHieuLuc').AsDateTime;
            Parameters[2].Value := FieldByName('MaHopDong').AsString;
            Execute;

            if Parameters[0].Value = 1 then
                FieldByName('NgayHetHan').Clear
            else
	            FieldByName('NgayHetHan').AsDateTime := Parameters[3].Value;
        end;

        if Sender.AsString <> '' then
            FieldByName('SoThang').AsFloat := FieldByName('LK_SoThang').AsFloat
        else
            FieldByName('SoThang').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterMaLoai_HopDongChange(Sender: TField);
begin
    MostRecentContract();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterManvChange(Sender: TField);
begin
    with QrMaster do
    begin
        EdManvQL.Text := EdManvQL.Field.AsString;
        FieldByName('Idx_HopDong').Clear;
        MostRecentContract();
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.QrMasterNgayKyChange(Sender: TField);
begin
    with QrMaster do
    	if FieldByName('NgayHieuLuc').AsDateTime = 0.0 then
        	FieldByName('NgayHieuLuc').AsDateTime :=
            	FieldByName('NgayKy').AsDateTime;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 493;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmHopdongLaodong.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrMaster do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.Dettach();
begin
     with QrMaster do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.ViewAttachment();
var
	s, sn: String;
begin
	with QrMaster do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.ViewContract();
var
	s, sName, sExt, sManv: String;
    Idx: Integer;
begin
	with QrMaster do
    try
        sManv := FieldByName('Manv').AsString;
        Idx := FieldByName('Idx').AsInteger;

        with QrDMHopDong do
        begin
            if Active then
                Close;

            Open;

            if IsEmpty then
            begin
                ErrMsg(RS_FILE_NOTFOUND_FORMAT);
                Exit;
            end;

            sName := isStripToneMark(toNonAccentVietnamese(FieldByName('FileName').AsString + '-' + sManv));
            sExt := FieldByName('FileExt').AsString;
            if (not SameText(sExt, '.doc')) and (not SameText(sExt, '.docx')) then
            begin
                ErrMsg(RS_FILE_NOTFOUND_FORMAT);
                Exit;
            end;

            s := sysAppData + sName + sExt;
            if not FileExists(s) then
            begin
                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;

            DataOffice.CreateDoc3(s, 'HR_RP_HOPDONG_WORD', [sysLogonUID, sManv, Idx]);

        end;
    except
        ErrMsg(RS_INVAILD_CONTENT);
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHopdongLaodong.MostRecentContract();
begin
    with QrMaster do
    begin
        if(State in [dsInsert]) then
        begin
            FieldByName('Idx_HopDong').Clear;
            if((FieldByName('Manv').AsString <> '')
                and (FieldByName('MaLoai_HopDong').AsString <> '')
                and (FieldByName('LK_MaLoai_HopDong_Ma').AsString <> '01')) then
            begin
                with spHR_LICHSU_HOPDONG_Get_Last_By_Manv_Ngay do
                begin
                    Prepared := True;
                    Parameters.ParamByName('@Manv').Value := FieldByName('Manv').AsString;
                    Parameters.ParamByName('@Ngay').Value := Date;
                    Execute;

                    if (Parameters.ParamValues['@RETURN_VALUE'] = 0) then
                        if (Parameters.FindParam('@Idx') <> nil) then
                            FieldByName('Idx_HopDong').Value := Parameters.ParamValues['@Idx'];
                end;
            end;
        end;
    end;
end;

end.
