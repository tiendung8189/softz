﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmCalamviec;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus, AdvMenus,
  AppEvnts, ToolWin, Wwdbigrd, Wwdbgrid;

type
  TFrmDmCalamviec = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdSearch: TAction;
    ApplicationEvents1: TApplicationEvents;
    ToolButton2: TToolButton;
    CmdPrint: TAction;
    ToolButton9: TToolButton;
    CmdAudit: TAction;
    QrDanhmucMACA: TWideStringField;
    QrDanhmucTENCA: TWideStringField;
    QrDanhmucGIOVAO: TDateTimeField;
    QrDanhmucGIORA: TDateTimeField;
    QrDanhmucGIOVAO1: TDateTimeField;
    QrDanhmucGIORA1: TDateTimeField;
    QrDanhmucQUANGAY: TBooleanField;
    QrDanhmucSOGIO: TFloatField;
    QrDanhmucVAO_THELENH: TWideStringField;
    QrDanhmucRA_THELENH: TWideStringField;
    QrDanhmucTHOIHAN_DITRE: TDateTimeField;
    QrDanhmucTHOIHAN_VESOM: TDateTimeField;
    QrDanhmucNGHI_GIUACA_TU: TDateTimeField;
    QrDanhmucNGHI_GIUACA_DEN: TDateTimeField;
    QrDanhmucDOANCA: TBooleanField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucMAMAUChange(Sender: TField);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
  private
  	mCanEdit, fixCode: Boolean;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmCalamviec: TFrmDmCalamviec;

implementation

uses
	MainData, isDb, isMsg, RepEngine, ExCommon, Rights, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_CALAMVIEC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.FormShow(Sender: TObject);
begin
    SetDisplayFormat(QrDanhmuc, sysCurFmt);
    SetDisplayFormat(QrDanhmuc, ['GIOVAO', 'GIORA', 'GIOVAO1', 'GIORA1',
            'THOIHAN_DITRE', 'THOIHAN_VESOM', 'NGHI_GIUACA_TU', 'NGHI_GIUACA_DEN'], sysMinFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE);

    GrList.ReadOnly := not mCanEdit;
    QrDanhmuc.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, mCanEdit);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
		if BlankConfirm(QrDanhmuc, ['MACA']) then
    		Abort;

        if fixCode then
			if LengthConfirm(QrDanhmuc, ['MACA']) then
            	Abort;

		if BlankConfirm(QrDanhmuc, ['TENCA', 'GIOVAO', 'GIORA', 'SOGIO']) then
    		Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucMAMAUChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('QUANGAY').AsBoolean := False;
        FieldByName('DOANCA').AsBoolean := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if SameText(Field.FullName, 'MACA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCalamviec.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

end.
