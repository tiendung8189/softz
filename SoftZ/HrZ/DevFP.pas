﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevFP;

interface

uses
  Classes, Graphics, Controls, Forms,
  ExtCtrls, Buttons,
  ActnList, fcButton, fcShapeBtn, StdCtrls, fcImgBtn, pngimage;

type
  TFrmDevFP = class(TForm)
    Image1: TImage;
    SpeedButton1: TfcShapeBtn;
    SpeedButton2: TfcShapeBtn;
    SpeedButton3: TfcShapeBtn;
    SpeedButton4: TfcShapeBtn;
    SpeedButton5: TfcShapeBtn;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Bevel5: TBevel;
    SpeedButton6: TfcShapeBtn;
    SpeedButton7: TfcShapeBtn;
    SpeedButton8: TfcShapeBtn;
    SpeedButton9: TfcShapeBtn;
    SpeedButton10: TfcShapeBtn;
    Bevel6: TBevel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    Bevel9: TBevel;
    Bevel10: TBevel;
    Bevel11: TBevel;
    BtnCancel: TBitBtn;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    CmdRegister: TAction;
    CmdDelete: TAction;
    Image2: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
  	enroll: String;
    fpStatus: array[0..9] of Boolean;

    procedure SetButtonState;
    function  DeleteFP(finger: Integer): Boolean;
    function  RegisterFP(finger: Integer): Boolean;
  public
    procedure Execute(empId: String);
  end;

var
  FrmDevFP: TFrmDevFP;

implementation

uses
	zkem, isMsg, DevFP2;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP.Execute(empId: String);
begin
	enroll := empId;
	ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP.SetButtonState;
begin
	SpeedButton1.Down  := fpStatus[0];
	SpeedButton2.Down  := fpStatus[1];
	SpeedButton3.Down  := fpStatus[2];
	SpeedButton4.Down  := fpStatus[3];
	SpeedButton5.Down  := fpStatus[4];

	SpeedButton6.Down  := fpStatus[5];
	SpeedButton7.Down  := fpStatus[6];
	SpeedButton8.Down  := fpStatus[7];
	SpeedButton9.Down  := fpStatus[8];
	SpeedButton10.Down := fpStatus[9];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP.FormShow(Sender: TObject);
var
	i: Integer;
begin
	Screen.Cursor := crHourGlass;
	for i := 0 to 9 do
	    fpStatus[i] := zkGetUserTmp(enroll, i);
	SetButtonState;

	for i := 0 to ComponentCount - 1 do
    	if Components[i].ClassName = 'TfcShapeBtn' then
        	with Components[i] as TfcShapeBtn do
            begin
				Glyph := Image2.Picture.Bitmap;
                NumGlyphs := 3;
            end;
	Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDevFP.DeleteFP(finger: Integer): Boolean;
begin
	Result := False;
    if not YesNo('Xóa vân tay này. Tiếp tục?', 1) then
        Exit;
    Result := zkDelUserTmp(enroll, finger);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDevFP.RegisterFP(finger: Integer): Boolean;
begin
	Result := zkStartEnrollEx(enroll, finger, 1);
    if not Result then
    begin
    	ErrMsg('Lỗi đăng ký vân tay.');
        Exit;
    end;

    Application.CreateForm(TFrmDevFP2, FrmDevFP2);
    Result := FrmDevFP2.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP.SpeedButton1Click(Sender: TObject);
var
	i: Integer;
begin
	i := (Sender as TfcShapeBtn).Tag;
    if fpStatus[i] then
		DeleteFP(i)
    else
		RegisterFP(i);
	fpStatus[i] := zkGetUserTmp(enroll, i);
	SetButtonState;
end;

end.
