﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevFP2;

interface

uses
  Windows, Messages, Classes, Controls, Forms,
  ExtCtrls, zkem, pngimage;

type
  TFrmDevFP2 = class(TForm)
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image1: TImage;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);

    procedure WM_ON_FINGER(var uMsg: TMessage); message WM_FINGER;
    procedure WM_ON_ENROLL(var uMsg: TMessage); message WM_ENROLL_FINGER;
  private
  	cnt: Integer;
  public
  	function  Execute: Boolean;
  end;

var
  FrmDevFP2: TFrmDevFP2;

implementation

uses
	isMsg;

{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDevFP2.Execute: Boolean;
begin
	cnt := 0;
	ShowModal;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Modalresult := mrCancel
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP2.FormShow(Sender: TObject);
begin
    Image1.Visible := cnt = 0;
    Image2.Visible := cnt = 1;
    Image3.Visible := cnt = 2;
    Image4.Visible := cnt = 3;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP2.WM_ON_FINGER(var uMsg: TMessage);
begin
	Inc(cnt);
    Image1.Visible := cnt = 0;
    Image2.Visible := cnt = 1;
    Image3.Visible := cnt = 2;
    Image4.Visible := cnt = 3;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevFP2.WM_ON_ENROLL(var uMsg: TMessage);
begin
    case uMsg.WParam of
    0:
    	Msg('Đã đăng ký vân tay thành công.');
    3:
    	ErrMsg('Lỗi lưu dữ liệu.');
    4:
   		ErrMsg('Lỗi đăng ký vân tay.');
    5:
   		ErrMsg('Trùng mẫu vân tay.');
    6:
		ErrMsg('Thao tác bị hũy.');
    end;
	Close;
end;

end.
