(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmThietBiTaiSanLaoDong;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  wwdblook, AppEvnts, Menus, AdvMenus, wwfltdlg, wwFltDlg2, wwDBGrid2, wwDialog,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbigrd, ToolWin;

type
  TFrmDmThietBiTaiSanLaoDong = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsNhom: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    QrNhom: TADOQuery;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PgMain: TPageControl;
    tsBehaviour: TTabSheet;
    tsDep: TTabSheet;
    GrLydo: TwwDBGrid2;
    GrNhom: TwwDBGrid2;
    QrDm: TADOQuery;
    DsDm: TDataSource;
    Bevel1: TBevel;
    CbNhom: TwwDBLookupCombo;
    RefNhom: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Filter: TwwFilterDialog2;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrNhomCREATE_BY: TIntegerField;
    QrNhomUPDATE_BY: TIntegerField;
    QrNhomCREATE_DATE: TDateTimeField;
    QrNhomUPDATE_DATE: TDateTimeField;
    QrDmCREATE_BY: TIntegerField;
    QrDmUPDATE_BY: TIntegerField;
    QrDmCREATE_DATE: TDateTimeField;
    QrDmUPDATE_DATE: TDateTimeField;
    QrDmLK_NHOM: TWideStringField;
    CmdReload: TAction;
    QrDmMaThietBi: TWideStringField;
    QrDmMaThietBi_Nhom: TWideStringField;
    QrDmTenThietBi: TWideStringField;
    QrDmTenThietBi_TA: TWideStringField;
    QrDmDvt: TWideStringField;
    QrDmCo_HoanTra: TBooleanField;
    QrNhomMaThietBi_Nhom: TWideStringField;
    QrNhomTenThietBi_Nhom: TWideStringField;
    QrNhomTenThietBi_Nhom_TA: TWideStringField;
    QrDmHanBaoHanh: TFloatField;
    QrDmHanDuocSoHuu: TFloatField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrNhomBeforePost(DataSet: TDataSet);
    procedure QrNhomBeforeDelete(DataSet: TDataSet);
    procedure QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNhomBeforeInsert(DataSet: TDataSet);
    procedure GrLydoCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure GrNhomCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrDmBeforePost(DataSet: TDataSet);
    procedure CbNhomNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrNhomAfterPost(DataSet: TDataSet);
  private
  	mCanEdit, mTrigger, fixCodeLd, fixCodeNhom, mRefresh: Boolean;
    mQuery: TADOQuery;
    qtyCusFmt: string;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmThietBiTaiSanLaoDong: TFrmDmThietBiTaiSanLaoDong;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, isCommon, MainData;

{$R *.DFM}

const
    FORM_CODE = 'HR_DM_THIETBI';
    FORM_CODE2 = 'HR_DM_THIETBI_NHOM';

    TABLE_NAME = FORM_CODE;
    TABLE_NAME2 = FORM_CODE2;

    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrDm, TABLE_NAME);
    AddFields(QrNhom, TABLE_NAME2);

    mRefresh := False;
    qtyCusFmt := '#,##0.0;-#,##0.0;#';
    mTrigger := False;
    mQuery := QrDm;

    SetCustomGrid(
    	[FORM_CODE, FORM_CODE2], [GrLydo, GrNhom]);
    SetDictionary([QrDM, QrNhom], [FORM_CODE, FORM_CODE2], [Filter, nil]);

    // Ly do
    fixCodeLd := setCodeLength(FORM_CODE, QrDm.FieldByName('MaThietBi'));

    // Nhom
    fixCodeNhom := SetCodeLength(FORM_CODE2, QrNhom.FieldByName('MaThietBi_Nhom'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.FormShow(Sender: TObject);
begin
	GrLydo.ReadOnly := not mCanEdit;
    GrNhom.ReadOnly := not mCanEdit;
	OpenDataSets([QrNhom, QrDm]);

    SetDisplayFormat(QrDm, sysCurFmt);
    SetDisplayFormat(QrDm, ['HanBaoHanh', 'HanDuocSoHuu'], qtyCusFmt);

    ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrNhom, QrDm]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
    n: Integer;
begin
	b := mQuery.State in [dsBrowse];
    n := PgMain.ActivePageIndex;
    CmdNew.Enabled 	  := b and mCanEdit;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdDel.Enabled	  := b and mCanEdit and (not mQuery.IsEmpty);
    CmdSearch.Enabled := b;
    CmdClearFilter.Enabled := b and (Filter.FieldInfo.Count > 0);

    CmdFilter.Enabled := b and (n = 0);
    CmdFilter.Visible := n = 0;
    CmdClearFilter.Visible := n = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrDmBeforePost(DataSet: TDataSet);
begin
    begin
		if BlankConfirm(QrDm, ['MaThietBi']) then
    		Abort;

        if fixCodeLd then
            if LengthConfirm(QrDm, ['MaThietBi']) then
                Abort;

		if BlankConfirm(QrDm, ['TenThietBi', 'MaThietBi_Nhom']) then
    		Abort;

		SetNull(QrDm, ['MaThietBi_Nhom']);
    end;

    SetAudit(DataSet);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrNhomBeforePost(DataSet: TDataSet);
begin
    begin
	    if BlankConfirm(QrNhom, ['MaThietBi_Nhom']) then
    	    Abort;

	    if fixCodeNhom then
    	    if LengthConfirm(QrNhom, ['MaThietBi_Nhom']) then
        	    Abort;

	    if BlankConfirm(QrNhom, ['TenThietBi_Nhom']) then
    	    Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrNhomAfterPost(DataSet: TDataSet);
begin
    mRefresh := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrNhomBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
        exSearch(Name + '_0', DsDm)
    else
        exSearch(Name + '_1', DsNhom)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.QrNhomBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.GrLydoCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MaThietBi') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.GrNhomCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if (Field.FullName = 'MaThietBi_Nhom') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 0 then
    	Status.SimpleText := exRecordCount(mQuery, Filter)
    else
    	Status.SimpleText := RecordCount(mQuery);    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, REPORT_NAME, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := mQuery.State in [dsBrowse];
    if not AllowChange then
        exCompleteConfirm;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.PgMainChange(Sender: TObject);
begin
	case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrDm;
			GrLydo.SetFocus;
            RefNhom.Requery;

            if mRefresh then
            begin
                exReSyncRecord(mQuery);
                mRefresh := False;
            end;
	    end;
    1:
	    begin
    	    mQuery := QrNhom;
        	GrNhom.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CbNhomNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmThietBiTaiSanLaoDong.CmdReloadExecute(Sender: TObject);
begin
    QrDm.Requery;
    QrNhom.Requery;
    RefNhom.Requery;
end;

end.
