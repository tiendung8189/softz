﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvLichSuBieuMau;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdblook, AdvEdit, RzEdit, RzDBEdit, RzDBBnEd,
  Winapi.ShellAPI, Winapi.Windows, Vcl.Buttons, DBCtrlsEh, frameEmp;

type
  TFrmHoSonvLichSuBieuMau = class(TForm)
    ActionList: TActionList;
    CmdPrint: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    DsLichSuBieuMau: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    Panel1: TPanel;
    CmdRefresh: TAction;
    frEmp1: TfrEmp;
    BtnNew: TToolButton;
    CmdNew: TAction;
    BtnEdit: TToolButton;
    CmdEdit: TAction;
    CmdDel: TAction;
    BtnDel: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    spLichSuBieuMau: TADOStoredProc;
    PaList: TPanel;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    GrList: TwwDBGrid2;
    RzForm: TRzSizePanel;
    PD1: TisPanel;
    EdFileName: TDBEditEh;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    PaDinhKem: TisPanel;
    DBEditEh1: TDBEditEh;
    DBEdit6: TDBEditEh;
    QrThongTinBieuMau: TADOQuery;
    QrThongTinBieuMauManv: TWideStringField;
    QrThongTinBieuMauGhiChu: TWideMemoField;
    QrThongTinBieuMauCREATE_BY: TIntegerField;
    QrThongTinBieuMauUPDATE_BY: TIntegerField;
    QrThongTinBieuMauCREATE_DATE: TDateTimeField;
    QrThongTinBieuMauUPDATE_DATE: TDateTimeField;
    QrThongTinBieuMauGiayTo_Loai: TIntegerField;
    QrThongTinBieuMauFileIdx: TGuidField;
    QrThongTinBieuMauFileName: TWideStringField;
    QrThongTinBieuMauFileExt: TWideStringField;
    QrThongTinBieuMauLink: TWideStringField;
    QrThongTinBieuMauCalc_FileName: TWideStringField;
    QrThongTinBieuMauCacl_FileExt: TWideStringField;
    DsThongTinBieuMau: TDataSource;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    CmdOpenLink: TAction;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrThongTinBieuMauFileDesc: TWideStringField;
    CmdSave: TAction;
    CmdCancel: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    spLichSuBieuMauFileIdx: TGuidField;
    spLichSuBieuMauManv: TWideStringField;
    spLichSuBieuMauTenLoaiChucNang: TWideStringField;
    spLichSuBieuMauFileName: TWideStringField;
    spLichSuBieuMauFileExt: TWideStringField;
    spLichSuBieuMauCalc_FileName: TWideStringField;
    spLichSuBieuMauCacl_FileExt: TWideStringField;
    spLichSuBieuMauMaLoaiChucNang: TStringField;
    CmdReLoad: TAction;
    spLichSuBieuMauFileDesc: TWideStringField;
    spLichSuBieuMauLink: TWideStringField;
    BtView: TRzDBButtonEdit;
    spLichSuBieuMauCalc_FileView: TIntegerField;
    QrFileContentIdx: TAutoIncField;
    spLichSuBieuMauIdx: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdEditExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrThongTinBieuMauBeforeOpen(DataSet: TDataSet);
    procedure QrThongTinBieuMauBeforePost(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure CmdOpenLinkExecute(Sender: TObject);
    procedure QrThongTinBieuMauAfterInsert(DataSet: TDataSet);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrThongTinBieuMauAfterPost(DataSet: TDataSet);
    procedure QrThongTinBieuMauAfterDelete(DataSet: TDataSet);
    procedure QrThongTinBieuMauBeforeDelete(DataSet: TDataSet);
    procedure CmdReLoadExecute(Sender: TObject);
    procedure QrThongTinBieuMauLinkValidate(Sender: TField);
    procedure QrThongTinBieuMauCalcFields(DataSet: TDataSet);
    procedure spLichSuBieuMauCalcFields(DataSet: TDataSet);
    procedure spLichSuBieuMauAfterScroll(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure BtViewButtonClick(Sender: TObject);
    procedure GrListURLOpen(Sender: TObject; var URLLink: string; Field: TField;
      var UseDefault: Boolean);
  private
    r: WORD;
  	mCanEdit: Boolean;
    mFileIdx: TGUID;
    mSQL, mSearch, mEmpID, mEmpIDLabel, mEmpName: String;
    mIdx, mGridRow, mTotalRow: Integer;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmHoSonvLichSuBieuMau: TFrmHoSonvLichSuBieuMau;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    isStr, isFile, DmHotro_HR, GuidEx, OfficeData;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHANVIEN_LICHSU_BIEUMAU';
    REPORT_NAME = 'HR_DM_NHANVIEN_LICHSU_BIEUMAU';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
	DsLichSuBieuMau.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.FormShow(Sender: TObject);
begin

    frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
    SetDisplayFormat(spLichSuBieuMau, ctCurFmt);
    SetShortDateFormat(spLichSuBieuMau, ShortDateFormat);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(spLichSuBieuMau, FORM_CODE, Filter);

    CmdSave.Enabled := False;
    CmdCancel.Enabled := False;
    mSearch := '@';
    QrThongTinBieuMau.Open;
    // Loading
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.GrListURLOpen(Sender: TObject;
  var URLLink: string; Field: TField; var UseDefault: Boolean);
begin
    with spLichSuBieuMau do
    begin
        mIdx := FieldByName('Idx').AsInteger;
        if (mIdx > 0) and (FieldByName('Link').AsString <> '') then
        begin
            ShellExecute(Handle, 'Open', PChar(FieldByName('Link').AsString), Nil, Nil, SW_SHOWMAXIMIZED);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrThongTinBieuMau, spLichSuBieuMau]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrThongTinBieuMau, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdDelExecute(Sender: TObject);
begin
    mGridRow := GrList.GetActiveRow;
    mTotalRow := GrList.GetRowCount;
    with spLichSuBieuMau do
    begin
        mIdx := FieldByName('Idx').AsInteger;
        if mIdx > 0 then
        begin
            with QrThongTinBieuMau do
            begin
                if Active then
                    Close;

                Open;
                Delete;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdEditExecute(Sender: TObject);
begin
    with spLichSuBieuMau do
    begin
        mIdx := FieldByName('Idx').AsInteger;
        if mIdx > 0 then
        begin
            with QrThongTinBieuMau do
            begin
                if Active then
                    Close;

                Open;
                Edit;
            end;
            RzForm.Visible := True;
            EdFileName.SetFocus;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdRefreshExecute(Sender: TObject);
begin
   	if (EdSearch.Text <> mSearch) then
	begin
        mSearch := EdSearch.Text;
        Wait(PROCESSING);
        try
            with spLichSuBieuMau do
            begin
                Close;
                Prepared := True;
                Parameters[1].Value := mEmpID;
                Parameters[2].Value := DataMain.StripToneMark(mSearch);
                ExecProc;

                Active := True;
                if IsEmpty then
                begin
                    Active := False;
                    Exit;
                end;
            end;
            GrList.SetFocus;
        finally
            ClearWait;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdReLoadExecute(Sender: TObject);
var
    bm: TBytes;
begin
    with spLichSuBieuMau do
    begin
        bm := BookMark;
        DisableControls;
        Requery;
        BookMark := bm;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdSaveExecute(Sender: TObject);
begin
    QrThongTinBieuMau.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsLichSuBieuMau)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdFileMinusExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdNewExecute(Sender: TObject);
begin
    with QrThongTinBieuMau do
    begin
        Append;
    end;
    RzForm.Visible := True;
    EdFileName.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdOpenLinkExecute(Sender: TObject);
begin
    with QrThongTinBieuMau do
    if FieldByName('Link').AsString <> '' then
    begin
         ShellExecute(0, 'Open', PChar(FieldByName('Link').AsString), nil,  nil, SW_NORMAL);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdPrintExecute(Sender: TObject);
var
    d: TDateTime;
begin

    d := Date;
    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        1, //0: Lich su, 1: HSNV/Lich Su
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        mEmpID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bTTBMXem, bLSBMThaoTac: Boolean;
begin

    with QrThongTinBieuMau do
    begin
		bTTBMXem := State in [dsBrowse];
    end;

    with spLichSuBieuMau do
    begin
        // chỉ được chỉnh sửa hoặc xóa khi được thêm trên form này
        bLSBMThaoTac := (not IsEmpty) and (FieldByName('MaLoaiChucNang').AsString = 'HR_DM_NHANVIEN_LICHSU_BIEUMAU');
    end;

    PaList.Enabled := bTTBMXem;
    CmdDel.Enabled := bTTBMXem and bLSBMThaoTac and mCanEdit;
    CmdEdit.Enabled := bTTBMXem and bLSBMThaoTac and mCanEdit;
    CmdNew.Enabled := bTTBMXem and mCanEdit;
    CmdSave.Enabled := not bTTBMXem;
    CmdCancel.Enabled := not bTTBMXem;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
    spLichSuBieuMau.Requery;
    if mTotalRow > mGridRow then
        GrList.DataSource.DataSet.MoveBy(mGridRow);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauAfterInsert(DataSet: TDataSet);
begin
    with QrThongTinBieuMau do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Manv').AsString := mEmpID;
        FieldByName('GiayTo_Loai').AsInteger := 3;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauAfterPost(DataSet: TDataSet);
begin
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
	CmdReLoad.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrThongTinBieuMau.FieldByName('FileIdx')).AsGuid;
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        Parameters[0].Value := mEmpID;
        Parameters[1].Value := mIdx;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauBeforePost(DataSet: TDataSet);
begin
    with QrThongTinBieuMau do
    begin
	    if BlankConfirm(QrThongTinBieuMau, ['FileDesc']) then
   			Abort;

    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrThongTinBieuMau do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrThongTinBieuMauLinkValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
    begin
        if not validateUrl(Sender.AsString) then
        begin
            ErrMsg(Format(RS_INVAILD_VALIDATION, [Sender.DisplayLabel]));
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
    begin
        if (mFileIdx <> TGUID.Empty) then
            Parameters[0].Value :=  TGuidEx.ToString(mFileIdx)
        else
            Parameters[0].Value :=  TGuidEx.ToString(QrThongTinBieuMau.FieldByName('FileIdx'));
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.spLichSuBieuMauAfterScroll(DataSet: TDataSet);
begin
    RzForm.Visible := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.spLichSuBieuMauCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with spLichSuBieuMau do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdCancelExecute(Sender: TObject);
begin
    QrThongTinBieuMau.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;

    RzForm.Visible := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(spLichSuBieuMau, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsLichSuBieuMau);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;


    with QrThongTinBieuMau do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrThongTinBieuMau.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.BtViewButtonClick(Sender: TObject);
var
	s, sn: String;
begin
    with spLichSuBieuMau do
    begin
        sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;

        mFileIdx := TGuidField(FieldByName('FileIdx')).AsGuid;
        if (mFileIdx <> TGUID.Empty) and (FieldByName('FileExt').AsString <> '') then
        begin
            try
                with QrFileContent do
                begin
                    if Active then
                        Close;
                    Open;

                    if IsEmpty then
                    begin
                        ErrMsg(RS_INVAILD_NOTFOUND);
                        Exit;
                    end;

                    TBlobField(FieldByName('FileContent')).SaveToFile(s);
                end;

            except
                ErrMsg(RS_INVAILD_CONTENT);
                Exit;
            end;
            ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
            mFileIdx := TGUID.Empty;
        end;

    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.Dettach();
begin
     with QrThongTinBieuMau do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvLichSuBieuMau.ViewAttachment();
var
	s, sn: String;
begin
	with QrThongTinBieuMau do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;


end.
