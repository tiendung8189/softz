﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit KhenThuongKyLuatDanhSach;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, Vcl.Buttons, Vcl.Graphics,
  wwDialog, wwfltdlg, wwFltDlg2;

type
  TFrmKhenThuongKyLuatDanhSach = class(TForm)
    GrList: TwwDBGrid2;
    DsList: TDataSource;
    QrList: TADOQuery;
    ActionList: TActionList;
    CmdRegister: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Panel3: TPanel;
    BtnRegister: TBitBtn;
    BitBtn2: TBitBtn;
    spIMP_HR_LICHSU_KTKL_InsertData: TADOCommand;
    spIMP_HR_LICHSU_KTKL_DeleteList: TADOCommand;
    spIMP_HR_LICHSU_KTKL_Check: TADOCommand;
    QrListLK_TenLoaiKThuongKLuat: TWideStringField;
    Filter: TwwFilterDialog2;
    CmdDel: TAction;
    Xadng1: TMenuItem;
    CmdDelRecordError: TAction;
    Xaccdngangli1: TMenuItem;
    CHECK_RECORD_ERROR: TADOCommand;
    QrListLK_Tennv: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_TenKThuongKLuat: TWideStringField;
    QrListLK_Co_ThueTNCN: TBooleanField;
    QrListLK_KThuongKLuatLoai: TWideStringField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListKThuongKLuatLoai: TIntegerField;
    QrListManv: TWideStringField;
    QrListMaKThuongKLuat: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
  private
    mTransNo, countError: Integer;
    mCanEdit, mDelete: Boolean;
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	procedure Execute(r: WORD; pTransNo: Integer);
  end;

var
  FrmKhenThuongKyLuatDanhSach: TFrmKhenThuongKyLuatDanhSach;

const
    TABLE_NAME = 'IMP_HR_LICHSU_KTKL';
    FORM_CODE = 'IMP_HR_LICHSU_KTKL';
implementation

uses
	Rights, isDb, ExCommon, isLib, GuidEx, isCommon, isMsg;


{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';
procedure TFrmKhenThuongKyLuatDanhSach.Execute;
begin
    mCanEdit := rCanEdit(r);
   	mTransNo := pTransNo;
    ShowModal;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    CloseDataSets([QrList]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    AddAllFields(QrList, TABLE_NAME);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.FormShow(Sender: TObject);
begin
	OpenDataSets([QrList]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdDelRecordErrorExecute(
  Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_LICHSU_KTKL_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                mTransNo := 0;
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                Close;
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bIsError: Boolean;
begin
    with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdDel.Enabled := bBrowse and (not bEmpty);
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDanhSach.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
     if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKhenThuongKyLuatDanhSach.DeleteAllRecord;
begin
    with spIMP_HR_LICHSU_KTKL_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKhenThuongKyLuatDanhSach.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;


end.
