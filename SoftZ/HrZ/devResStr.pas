﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit devResStr;

interface
uses
    Messages;
type
    TEnrollData = array of Byte;
var
    mgDevCount: Integer;
const
    MY_MESSAGE              = WM_USER + 4000;
    MY_MESSAGE_DONE         = WM_USER + 4001;
    MY_MESSAGE_LOG          = WM_USER + 4002;

    MY_MESSAGE_DEV_STATUS   = WM_USER + 4010;
    DEV_STATUS_INACTIVE     = 0;
    DEV_STATUS_ACTIVE       = 1;
    DEV_STATUS_DISCONECTED  = 2;
    DEV_STATUS_CONECTED     = 3;

	    (*
        ** Time recorder
        *)
	// Events
	EF_ATTLOG		= 1;		// track the attendance log events / OnAttTransaction
    EF_FINGER		= 1 shl 1;
    EF_ENROLLUSER	= 1 shl 2;
    EF_ENROLLFINGER	= 1 shl 3;
    EF_BUTTON		= 1 shl 4;
    EF_UNLOCK		= 1 shl 5;	// unlock
    EF_STARTUP		= 1 shl 6;	// Starting System
    EF_VERIFY		= 1 shl 7;	// fingerprint verification
    EF_FPFTR		= 1 shl 8;	// Extract Fingerprint Feature Point
    EF_ALARM		= 1 shl 9;	// Alarm signals
    EF_HIDNUM		= 1 shl 10;	// RF card number
    EF_WRITECARD	= 1 shl 11;	// write card successfully
    EF_EMPTYCARD	= 1 shl 12;	// removals card successfully

    // Status
    SF_ADMIN			= 1;
    SF_USER				= 2;
    SF_FINGER			= 3;
    SF_PASSWORD			= 4;
    SF_MANAGE_RECORD	= 5;
    SF_INOUT_RECORD		= 6;
    SF_NOMINAL_FP		= 7;
    SF_NOMINAL_USER		= 8;
    SF_NOMINAL_INOUT	= 9;

    WM_FINGER = WM_USER + 1000;
    WM_ENROLL_FINGER = WM_USER + 1001;

resourcestring
    RS_DEV_NOT_SUPORT           = 'Máy không hổ trợ chức năng này.';
    RS_DEV_ERR_CONNECT          = 'Lỗi kết nối với máy chấm công %d.';
    RS_DEV_CLEAR_KEEPER_DATA    = 'Xóa toàn bộ dữ liệu trong máy. Tiếp tục?';
    RS_DEV_DEL_FINGER_DB        = 'Xóa vân tay của tất cả nhân viên trên màn hình khỏi CSDL. Tiếp tục?';
    RS_DEV_DEL_FINGER           = 'Xóa toàn bộ dữ liệu vân tay trên máy "%s". Tiếp tục?';
    RS_DEV_DEL_ENROLL           = 'Xóa danh sách nhân viên trên trên màn hình ra khỏi máy "%s". Tiếp tục?';
    RS_DEV_CLEAN_IO             = 'Xóa dữ liệu In/Out trong máy. Tiếp tục?';
    RS_DEV_UPLOAD_TMP           = 'Cập nhật vân tay của tất cả nhân viên trên màn hinh'#13'từ CSDL vào máy "%s". Tiếp tục?';
    RS_DEV_UPLOAD_ENROLL        = 'Cập nhật danh sách nhân viên trên màn hinh vào máy "%s". Tiếp tục?';
    RS_DEV_ENROLL_NOTFOUND      = 'Không có nhân viên "%s" trong máy.';
    RS_DEV_MSG_DONE             = 'Thao tác hoàn tất.';

    RS_DEV_READ_LOG             = 'Đọc dữ liệu chấm công.';
    RS_DEV_READ_LOG2            = 'Có %d mẫu tin chấm công.';
	RS_DEV_FULL_LOG           	= 'Dữ liệu chấm công %d/50000.';

    RS_DEV_CONNECTING            = 'Kết nối máy chấm công';
    RS_DEV_CONNECTED             = 'Kết nối thành công';
    RS_DEV_DISCONNECT            = 'Ngắt kết nối với máy chấm công.';
    RS_DEV_CONNECT_ERR           = 'Lỗi kết nối với máy chấm công.';	

implementation

end.
