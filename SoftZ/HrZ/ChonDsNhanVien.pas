﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsNhanVien;

interface

uses
  Classes, Controls, Forms, System.Variants,
  Wwdbdlg, StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh, ToolCtrlsEh, DynVarsEh,
  DbLookupComboboxEh2;

type
  TFrmChonDsNhanVien = class(TForm)
    QrSec: TADOQuery;
    QrDep: TADOQuery;
    DsDep: TDataSource;
    RgLoai: TRadioGroup;
    Panel1: TPanel;
    QrEmp: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    QrSite: TADOQuery;
    DsSite: TDataSource;
    DsSec: TDataSource;
    DsEmp: TDataSource;
    EdMaPhongBan: TDBEditEh;
    EdMaBoPhan: TDBEditEh;
    CbNhanVien: TDbLookupComboboxEh2;
    EdMaNhanVien: TDBEditEh;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CbPhongBanExit(Sender: TObject);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmChonDsNhanVien: TFrmChonDsNhanVien;

implementation

uses
	isDb, isLib, ExCommon, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrSite, QrDep, QrSec, QrEmp, DataMain.QrDMBOPHAN]);
	mTrigger := False;
    RgLoai.OnClick(Nil);

    if RgLoai.ItemIndex = 1 then
        CbPhongBanExit(CbPhongBan);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsNhanVien.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaPhongBan.Enabled := n <> 2;
    CbPhongBan.Enabled := n <> 2;

    EdMaBoPhan.Enabled := n = 1;
    CbBoPhan.Enabled := n = 1;

    EdMaNhanVien.Enabled :=  n = 2;
    CbNhanVien.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbPhongBan.SetFocus;
        1:
            CbBoPhan.SetFocus;
        2:
            CbNhanVien.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
            s1 := CbPhongBan.Value;
        	s2 := CbPhongBan.Text;
        end;
    1:
    	begin
            s1 := CbBoPhan.Value;
        	s2 := CbBoPhan.Text;
        end;
    2:
    	begin
            s1 := CbNhanVien.Value;
        	s2 := CbNhanVien.Text;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := EdMaPhongBan.Text;
    1:
       	s := EdMaBoPhan.Text;
    2:
       	s := EdMaNhanVien.Text;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
//	    CloseDataSets([QrSite, QrDep, QrSec, QrEmp]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsNhanVien.CbPhongBanExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;


	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrDep do
        if Locate('MaPhongBan', s, []) then
        begin
            EdMaPhongBan.Text := FieldByName('Ma').AsString;
            s1 := EdMaBoPhan.Text;
            with QrSec do
            if (s1 <> '') then
            begin
                if  not Locate('MaPhongBan;Ma', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaBoPhan.Text := FieldByName('Ma').AsString;
                CbBoPhan.Value := FieldByName('MaBoPhan').AsString;
            end
            else
            begin
                EdMaBoPhan.Text := '';
                CbBoPhan.Text := '';
            end;
        end
        else
        begin
            EdMaPhongBan.Text := '';
            EdMaBoPhan.Text := '';
            CbBoPhan.Text := '';
        end;
    1:		// bo phan
        with QrSec do
        if (s <> '') and (Locate('MaBoPhan', s, [])) then
        begin
            EdMaBoPhan.Text := FieldByName('Ma').AsString;
        end
        else
        begin
            EdMaBoPhan.Text := '';
        end;
    2:		// nhan vien
		EdMaNhanVien.Text := s;
	end;

    mTrigger := False;
end;

procedure TFrmChonDsNhanVien.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
