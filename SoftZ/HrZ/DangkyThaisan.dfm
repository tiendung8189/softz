object FrmDangkyThaisan: TFrmDangkyThaisan
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #208#259'ng K'#253' Thai S'#7843'n'
  ClientHeight = 573
  ClientWidth = 1022
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1022
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btnEdit: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton15: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 1022
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 1022
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 1022
    inherited Panel1: TPanel
      Width = 1022
      ExplicitWidth = 1022
      inherited CbOrg: TfcTreeCombo
        Width = 539
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitWidth = 539
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 540
    Top = 84
    Width = 482
    Height = 468
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 3
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 474
      Height = 468
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Panel1: TisPanel
        Left = 2
        Top = 2
        Width = 470
        Height = 253
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin '#273#259'ng k'#253
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GroupBox4: TGroupBox
          Left = 0
          Top = 192
          Width = 470
          Height = 52
          Align = alTop
          Caption = '  B'#7843'ng l'#432#417'ng/ Thu'#7871' TNCN  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          DesignSize = (
            470
            52)
          object Label5: TLabel
            Left = 27
            Top = 22
            Width = 107
            Height = 16
            Alignment = taRightJustify
            Caption = 'Thu'#7897'c th'#225'ng l'#432#417'ng'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object rDBCheckBox1: TrDBCheckBox
            Left = 338
            Top = 20
            Width = 124
            Height = 22
            Anchors = [akTop, akRight]
            Caption = 'C'#243' ch'#7883'u thu'#7871' TNCN'
            DataField = 'Co_ThueTNCN'
            DataSource = DsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            ShowFieldCaption = False
            UpdateAfterClick = True
          end
          object CbMon: TwwDBComboBox
            Left = 140
            Top = 20
            Width = 43
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Thang'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object CbYear: TwwDBComboBox
            Left = 186
            Top = 20
            Width = 55
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Nam'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 16
          Width = 470
          Height = 100
          Align = alTop
          Caption = '  Nh'#226'n vi'#234'n  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          DesignSize = (
            470
            100)
          object Label108: TLabel
            Left = 62
            Top = 48
            Width = 72
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ngh'#7881' t'#7915' ng'#224'y'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 301
            Top = 48
            Width = 54
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = #272#7871'n ng'#224'y'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 39
            Top = 72
            Width = 95
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y nh'#7853'n h'#7891' s'#417
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 276
            Top = 72
            Width = 79
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = 'Ng'#224'y sinh con'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object CbNhanVien: TDbLookupComboboxEh2
            Left = 140
            Top = 20
            Width = 242
            Height = 22
            ControlLabel.Width = 56
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akLeft, akTop, akRight]
            DynProps = <>
            DataField = 'Manv'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'Tennv'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 110
              end
              item
                FieldName = 'ManvQL'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 40
              end>
            DropDownBox.ListSource = DsEmp
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'Manv'
            ListField = 'Tennv'
            ListSource = HrDataMain.DsDMNV
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdManvQL: TDBEditEh
            Left = 385
            Top = 20
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_ManvQL'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object CbDate: TwwDBDateTimePicker
            Left = 140
            Top = 44
            Width = 101
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'TuNgay'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 2
          end
          object CbToDate: TwwDBDateTimePicker
            Left = 361
            Top = 44
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'DenNgay'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 3
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 140
            Top = 68
            Width = 101
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgayNhanHoSo'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 4
          end
          object dpNgaySinhCon: TwwDBDateTimePicker
            Left = 361
            Top = 68
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgaySinhCon'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 5
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 116
          Width = 470
          Height = 76
          Align = alTop
          Caption = '  Ph'#7909' c'#7845'p  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          DesignSize = (
            470
            76)
          object DBNumberEditEh1: TDBNumberEditEh
            Left = 140
            Top = 44
            Width = 101
            Height = 22
            ControlLabel.Width = 40
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' ti'#7873'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'PhuCapThaiSan'
            DataSource = DsMaster
            DecimalPlaces = 0
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object CbLoaiPhuCap: TDbLookupComboboxEh2
            Left = 140
            Top = 20
            Width = 218
            Height = 22
            ControlLabel.Width = 45
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ph'#7909' c'#7845'p'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akLeft, akTop, akRight]
            DynProps = <>
            DataField = 'MaPhuCap'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'TenPhuCap'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 200
              end>
            DropDownBox.ListSource = HrDataMain.DsDM_PHUCAP
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clWindow
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <
              item
                Action = CmdPhuCap
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MaPhuCap'
            ListField = 'TenPhuCap'
            ListSource = HrDataMain.DsDM_PHUCAP
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
            WordWrap = True
          end
          object EdPhuCapLoai: TDBEditEh
            Left = 361
            Top = 20
            Width = 101
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Width = 3
            ControlLabel.Height = 13
            ControlLabel.Caption = ' '
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_TenPhuCapLoai'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
      object PNTinhTrang: TisPanel
        Left = 2
        Top = 255
        Width = 470
        Height = 131
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' :: Th'#244'ng tin x'#233't duy'#7879't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          470
          131)
        object Label4: TLabel
          Left = 285
          Top = 74
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbNguoiDuyet: TDbLookupComboboxEh2
          Left = 140
          Top = 22
          Width = 242
          Height = 22
          ControlLabel.Width = 127
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#432#7901'i x'#233't duy'#7879't (BOD)'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          Anchors = [akLeft, akTop, akRight]
          DynProps = <>
          DataField = 'XetDuyet_Manv'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'Tennv'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 110
            end
            item
              FieldName = 'ManvQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 40
            end>
          DropDownBox.ListSource = DsEmpStatus
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'Manv'
          ListField = 'Tennv'
          ListSource = HrDataMain.DsDMNV
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 0
          Visible = True
        end
        object EdXetDuyet_ManvQL: TDBEditEh
          Left = 385
          Top = 22
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_XetDuyet_ManvQL'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object DBEditEh4: TDBEditEh
          Left = 140
          Top = 46
          Width = 322
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 42
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ghi ch'#250
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'XetDuyet_GhiChu'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object CbbXetDuyet_TinhTrang: TDbLookupComboboxEh2
          Left = 140
          Top = 70
          Width = 142
          Height = 22
          ControlLabel.Width = 59
          ControlLabel.Height = 16
          ControlLabel.Caption = 'T'#236'nh tr'#7841'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          Images = DataMain.ImageStatus
          Color = clBtnFace
          DataField = 'XetDuyet_TinhTrang'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'TEN_HOTRO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              ImageList = DataMain.ImageStatus
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 60
            end>
          DropDownBox.ListSource = DsTinhTrang_XetDuyet
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoFitRowHeightEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA_HOTRO'
          ListField = 'TEN_HOTRO'
          ListSource = DsTinhTrang_XetDuyet
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object wwDBDateTimePicker2: TwwDBDateTimePicker
          Left = 319
          Top = 70
          Width = 143
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'XetDuyet_Ngay'
          DataSource = DsMaster
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 4
        end
        object BtnApproveAccept: TBitBtn
          Left = 140
          Top = 96
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdApproveAccept
          Caption = 'Duy'#7879't'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object BtnApproveCancel: TBitBtn
          Left = 245
          Top = 96
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdApproveCancel
          Caption = 'T'#7915' ch'#7889'i'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
      end
      object PNGhiChu: TisPanel
        Left = 2
        Top = 386
        Width = 470
        Height = 80
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' :: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 0
          Top = 16
          Width = 470
          Height = 64
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 84
    Width = 540
    Height = 468
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 4
    object GrList: TwwDBGrid2
      Left = 1
      Top = 49
      Width = 538
      Height = 418
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'XetDuyet_TinhTrang;ImageIndex;Original Size')
      Selected.Strings = (
        'MANV'#9'12'#9'M'#227#9'F'
        'LK_TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
        'TUNGAY'#9'12'#9'T'#7915' ng'#224'y'#9'F'#9#272#259'ng k'#253' ngh'#7881
        'DENNGAY'#9'12'#9#272#7871'n ng'#224'y'#9'F'#9#272#259'ng k'#253' ngh'#7881
        'LK_TenVangMat'#9'30'#9'L'#253' do ngh'#7881#9'F'
        'GHICHU'#9'25'#9'Ghi ch'#250#9'F'
        'XetDuyet_TinhTrang'#9'3'#9'#'#9'F')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopCommon
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      OnEnter = CmdRefreshExecute
      ImageList = DataMain.ImageStatus
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 538
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object rgApproved: TRadioGroup
        Left = 0
        Top = 0
        Width = 515
        Height = 45
        Align = alCustom
        Caption = 'T'#236'nh tr'#7841'ng x'#233't duy'#7879't'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Ch'#7901' x'#233't duy'#7879't'
          #272#227' x'#233't duy'#7879't'
          'T'#7915' ch'#7889'i x'#233't duy'#7879't'
          'T'#7845't c'#7843)
        TabOrder = 0
      end
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      Visible = False
      OnExecute = CmdClearExecute
    end
    object CmdXacNhan: TAction
      Caption = 'X'#225'c nh'#7853'n'
    end
    object CmdTuChoi: TAction
      Caption = 'T'#7915' ch'#7889'i'
    end
    object CmdXacNhan1: TAction
      Caption = 'X'#225'c nh'#7853'n'
    end
    object CmdTuChoi1: TAction
      Caption = 'T'#7915' ch'#7889'i'
    end
    object CmdPhuCap: TAction
      Caption = 'CmdPhuCap'
      OnExecute = CmdPhuCapExecute
    end
    object CmdApproveAccept: TAction
      Caption = 'Duy'#7879't'
      OnExecute = CmdApproveAcceptExecute
    end
    object CmdApproveCancel: TAction
      Caption = 'T'#7915' ch'#7889'i'
      OnExecute = CmdApproveCancelExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrEmpBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1'
      
        'and a.GIOITINH = (select MA_HOTRO from V_HR_LOAI_GIOITINH where ' +
        'TEN_HOTRO like N'#39'%N'#7919'%'#39' or TEN_HOTRO like N'#39'%Female%'#39')')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <
      item
        Name = 'Tungay1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'Denngay1'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'Tungay2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'Denngay2'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'Tungay3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'Denngay3'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DANGKY_VANGMAT'
      ' where'#9
      '  ('
      '              [TuNgay] between :Tungay1 and :Denngay1'
      '         or [DenNgay] between :Tungay2 and :Denngay2'
      '         or :Tungay3 between [TuNgay] and [DenNgay]'
      '         or :Denngay3 between [TuNgay] and [DenNgay]'
      '   )'
      '  %s'
      '   and'#9'[MANV] in ('
      #9#9'select'#9'MANV'
      '              '#9#9'   from'#9'HR_DM_NHANVIEN'
      '           '#9#9'where'#9'%s'
      #9')'
      ' and  MaVangMat in ('
      #9#9'select'#9'MaVangMat'
      #9#9'  from'#9'V_HR_DM_LYDO_VANGMAT'
      #9#9'where'#9'isnull(MaLoai_VangMat_Ma, '#39#39') = '#39'ML'#39')')
    Left = 17
    Top = 172
    object QrMasterLK_TENNV: TWideStringField
      DisplayLabel = 'H'#7885' t'#234'n'
      FieldKind = fkLookup
      FieldName = 'LK_TenNV'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Size = 50
      Lookup = True
    end
    object QrMasterMA_LYDO: TWideStringField
      DisplayLabel = 'L'#253' do ngh'#7881
      FieldName = 'MaVangMat'
      Visible = False
      Size = 15
    end
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
    end
    object QrMasterDauCa: TIntegerField
      FieldName = 'DauCa'
    end
    object QrMasterSoNgay: TFloatField
      FieldName = 'SoNgay'
    end
    object QrMasterNgaySinhCon: TDateTimeField
      FieldName = 'NgaySinhCon'
    end
    object QrMasterNgayNhanHoSo: TDateTimeField
      FieldName = 'NgayNhanHoSo'
    end
    object QrMasterNgayThanhToan: TDateTimeField
      FieldName = 'NgayThanhToan'
    end
    object QrMasterTuNgay: TDateTimeField
      FieldName = 'TuNgay'
    end
    object QrMasterDenNgay: TDateTimeField
      FieldName = 'DenNgay'
    end
    object QrMasterPhuCapThaiSan: TFloatField
      FieldName = 'PhuCapThaiSan'
    end
    object QrMasterXacNhan_TinhTrang: TIntegerField
      FieldName = 'XacNhan_TinhTrang'
    end
    object QrMasterXacNhan_TinhTrangBy: TIntegerField
      FieldName = 'XacNhan_TinhTrangBy'
    end
    object QrMasterXacNhan_Manv: TWideStringField
      FieldName = 'XacNhan_Manv'
    end
    object QrMasterXacNhan_GhiChu: TWideStringField
      FieldName = 'XacNhan_GhiChu'
      Size = 200
    end
    object QrMasterXacNhan_Ngay: TDateTimeField
      FieldName = 'XacNhan_Ngay'
    end
    object QrMasterMaPhuCap: TWideStringField
      FieldName = 'MaPhuCap'
      OnChange = QrMasterMaPhuCapChange
    end
    object QrMasterThang: TIntegerField
      FieldName = 'Thang'
    end
    object QrMasterNam: TIntegerField
      FieldName = 'Nam'
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrMasterXetDuyet_TinhTrang: TIntegerField
      FieldName = 'XetDuyet_TinhTrang'
    end
    object QrMasterXetDuyet_TinhTrangBy: TIntegerField
      FieldName = 'XetDuyet_TinhTrangBy'
    end
    object QrMasterXetDuyet_Manv: TWideStringField
      FieldName = 'XetDuyet_Manv'
      OnChange = QrMasterXetDuyet_ManvChange
    end
    object QrMasterXetDuyet_GhiChu: TWideStringField
      FieldName = 'XetDuyet_GhiChu'
      Size = 200
    end
    object QrMasterXetDuyet_Ngay: TDateTimeField
      FieldName = 'XetDuyet_Ngay'
    end
    object QrMasterLK_PhuCapLoai: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_PhuCapLoai'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'PhuCapLoai'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrMasterLK_TenPhuCapLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhuCapLoai'
      LookupDataSet = HrDataMain.QrV_HR_PHUCAP_LOAI
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LK_PhuCapLoai'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_TenTinhTrang: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_TenTinhTrang'
      LookupDataSet = QrTinhTrang_XetDuyet
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'XetDuyet_TinhTrang'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_Tennv: TWideStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_XacNhan_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterLK_TenVangMat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenVangMat'
      LookupDataSet = QrListLeave
      LookupKeyFields = 'MaVangMat'
      LookupResultField = 'TenVangMat'
      KeyFields = 'MaVangMat'
      Lookup = True
    end
    object QrMasterLK_MaNhomLV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNhomLV'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'MaNhomLV'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterMaCa: TWideStringField
      FieldName = 'MaCa'
      OnChange = QrMasterMaCaChange
    end
    object QrMasterMaNhomLV: TWideStringField
      FieldName = 'MaNhomLV'
    end
    object QrMasterCREATE_SOURCE: TWideStringField
      FieldName = 'CREATE_SOURCE'
      Size = 5
    end
    object QrMasterLK_GioVao: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GioRa: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GioVao1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GioRa1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_SoGio: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_SoGio'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'SoGio'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterGioVao_Ca: TDateTimeField
      FieldName = 'GioVao_Ca'
    end
    object QrMasterGioRa_Ca: TDateTimeField
      FieldName = 'GioRa_Ca'
    end
    object QrMasterGioVao1_Ca: TDateTimeField
      FieldName = 'GioVao1_Ca'
    end
    object QrMasterGioRa1_Ca: TDateTimeField
      FieldName = 'GioRa1_Ca'
    end
    object QrMasterSoGio_Ca: TFloatField
      FieldName = 'SoGio_Ca'
    end
    object QrMasterCuoiCa: TIntegerField
      FieldName = 'CuoiCa'
    end
    object QrMasterIDX: TAutoIncField
      FieldName = 'IDX'
      ReadOnly = True
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object QrListLeave: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9' *'
      '  from'#9'V_HR_DM_LYDO_VANGMAT'
      ' where'#9'isnull(MaLoai_VangMat_Ma, '#39#39') = '#39'ML'#39
      'order by MaVangMat')
    Left = 76
    Top = 172
  end
  object QrDays: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'DAUCA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    SQL.Strings = (
      'select dbo.fnHR_Ngaynghi_Tinhngay(:MANV, :NGAYD, :NGAYC, :DAUCA)')
    Left = 108
    Top = 172
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 52
    Top = 200
  end
  object DsTinhTrang_XetDuyet: TDataSource
    AutoEdit = False
    DataSet = QrTinhTrang_XetDuyet
    Left = 236
    Top = 336
  end
  object QrTinhTrang_XetDuyet: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  * '
      '   from V_HR_TINHTRANG_XETDUYET'
      'order by MA_HOTRO asc')
    Left = 236
    Top = 292
  end
  object QrEmpStatus: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrEmpBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 320
    Top = 300
  end
  object DsEmpStatus: TDataSource
    AutoEdit = False
    DataSet = QrEmpStatus
    Left = 324
    Top = 328
  end
  object spHR_DANGKY_VANGMAT_Invalid: TADOCommand
    CommandText = 'spHR_DANGKY_VANGMAT_Invalid;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@IDX'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DAUCA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CUOICA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MA_LYDO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MACA'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 212
    Top = 212
  end
  object QrShift: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MANV'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'a.MACA, a.GIOVAO, a.GIORA, a.GIOVAO1, a.GIORA1, a.SOGIO'
      
        '  from '#9'HR_DM_CALAMVIEC a join HR_DM_NHOM_LAMVIEC b on a.MACA = ' +
        'b.CA1'
      #9#9'join HR_DM_NHANVIEN c on b.MANHOMLV = c.MANHOMLV'
      'where '#9'c.MANV = :MANV')
    Left = 170
    Top = 175
  end
end
