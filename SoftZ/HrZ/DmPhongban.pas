(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmPhongban;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, fcdbtreeview, Grids, ComCtrls,
  AppEvnts, Menus, AdvMenus, ActnList, Db, ADODB, wwDBGrid2,
  wwfltdlg, wwFltDlg2, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, RzPanel, ToolWin;

type
  TFrmDmPhongban = class(TForm)
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Panel2: TPanel;
    fcDBTreeView1: TfcDBTreeView;
    Bevel1: TBevel;
    QrDanhmuc2: TADOQuery;
    DsDanhmuc2: TDataSource;
    Status: TStatusBar;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdPrint: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    PopDetail: TAdvPopupMenu;
    Lctheomthng1: TMenuItem;
    Lcdliu1: TMenuItem;
    N1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    FilterDanhmuc: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PgMain: TPageControl;
    TsQuocgia: TTabSheet;
    GrDanhmuc: TwwDBGrid2;
    TsTinh: TTabSheet;
    GrDanhmuc2: TwwDBGrid2;
    CmdReload: TAction;
    FilterDanhmuc2: TwwFilterDialog2;
    TsHuyen: TTabSheet;
    GrDanhmuc3: TwwDBGrid2;
    QrDanhmuc3: TADOQuery;
    DsDanhmuc3: TDataSource;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmuc2CREATE_BY: TIntegerField;
    QrDanhmuc2UPDATE_BY: TIntegerField;
    QrDanhmuc2CREATE_DATE: TDateTimeField;
    QrDanhmuc2UPDATE_DATE: TDateTimeField;
    FilterDanhmuc3: TwwFilterDialog2;
    QrDanhmuc3CREATE_BY: TIntegerField;
    QrDanhmuc3UPDATE_BY: TIntegerField;
    QrDanhmuc3CREATE_DATE: TDateTimeField;
    QrDanhmuc3UPDATE_DATE: TDateTimeField;
    CmdAudit: TAction;
    QrDanhmucMaChiNhanh: TWideStringField;
    QrDanhmucTenChiNhanh: TWideStringField;
    QrDanhmucTenChiNhanh_TA: TWideStringField;
    QrDanhmucNgung_SuDung: TBooleanField;
    QrDanhmuc3MaBoPhan: TWideStringField;
    QrDanhmuc3Ma: TWideStringField;
    QrDanhmuc3MaPhongBan: TWideStringField;
    QrDanhmuc3TenBoPhan: TWideStringField;
    QrDanhmuc3TenBoPhan_TA: TWideStringField;
    QrDanhmuc2MaPhongBan: TWideStringField;
    QrDanhmuc2MaChiNhanh: TWideStringField;
    QrDanhmuc2Ma: TWideStringField;
    QrDanhmuc2TenPhongBan: TWideStringField;
    QrDanhmuc2TenPhongBan_TA: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDBError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrDanhmuc2CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDanhmucBeforeEdit(DataSet: TDataSet);
    procedure QrDanhmuc2AfterInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
  private
    mCanEdit, fixCode1, fixCode2, fixCode3: Boolean;
    mQuery: TADOQuery;
    mDs: TDataSource;
    mFilter: TwwFilterDialog2;
  public
    procedure Execute(r: WORD);
  end;

var
  FrmDmPhongban: TFrmDmPhongban;

implementation

uses
    exCommon, isCommon, isMsg, isDb, MainData, Rights, isLib, RepEngine;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.Execute(r: WORD);
begin
    mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
	FORM_CODE1: String = 'HR_DM_CHINHANH';
	FORM_CODE2: String = 'HR_DM_PHONGBAN';
    FORM_CODE3: String = 'HR_DM_BOPHAN';

    FIELD_CODE: String = 'Ma';

procedure TFrmDmPhongban.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrDanhmuc, FORM_CODE1);
    AddFields(QrDanhmuc2, FORM_CODE2);
    AddFields(QrDanhmuc3, FORM_CODE3);

    SetDictionary ([QrDanhmuc, QrDanhmuc2, QrDanhmuc3],
        [FORM_CODE1, FORM_CODE2, FORM_CODE3],
        [FilterDanhmuc, FilterDanhmuc2, FilterDanhmuc3]);
    SetCustomGrid([FORM_CODE1, FORM_CODE2, FORM_CODE3],
        [GrDanhmuc, GrDanhmuc2, GrDanhmuc3]);

    fixCode1 := SetCodeLength(FORM_CODE1, QrDanhmuc.FieldByName('MaChiNhanh'));
    fixCode2 := SetCodeLength(FORM_CODE2, QrDanhmuc2.FieldByName(FIELD_CODE));
    fixCode3 := SetCodeLength(FORM_CODE3, QrDanhmuc3.FieldByName(FIELD_CODE));

    mQuery := QrDanhmuc;
    mDs := DsDanhmuc;
    mFilter := FilterDanhmuc;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDanhmuc, QrDanhmuc2, QrDanhmuc3]);
	GrDanhmuc.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDanhmuc, QrDanhmuc2, QrDanhmuc3]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataset(mQuery, True);
end;

    (*
    **  PageTab
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrDanhmuc;
            mDs := DsDanhmuc;
            mFilter := FilterDanhmuc;
            GrDanhmuc.SetFocus;
	    end;
    1:
	    begin
    	    mQuery := QrDanhmuc2;
            mDs := DsDanhmuc2;
            mFilter := FilterDanhmuc2;
            GrDanhmuc2.SetFocus;
	    end;
    2:
	    begin
    	    mQuery := QrDanhmuc3;
            mDs := DsDanhmuc3;
            mFilter := FilterDanhmuc3;
            GrDanhmuc3.SetFocus;
	    end;
    end;

   	HideAudit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdNewExecute(Sender: TObject);
begin
    mQuery.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdSaveExecute(Sender: TObject);
begin
    mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdCancelExecute(Sender: TObject);
begin
    mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdDelExecute(Sender: TObject);
begin
    mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdSearchExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
        exSearch(Name + '0', DsDanhmuc);
    1:
        exSearch(Name + '1', DsDanhmuc2);
    2:
        exSearch(Name + '2', DsDanhmuc3);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, 'HR_DM_PHONGBAN', [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdFilterExecute(Sender: TObject);
begin
    mFilter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdClearFilterExecute(Sender: TObject);
begin
    with mFilter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdReloadExecute(Sender: TObject);
begin
    QrDanhmuc.Requery;
    QrDanhmuc2.Requery;
    QrDanhmuc3.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, mQuery, mFilter, mCanEdit);
end;

    (*
    **  Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    case PgMain.ActivePageIndex of
    0:
        Abort;
    1:
        if QrDanhmuc.FieldByName('MaChiNhanh').AsString = '' then
            Abort;
    2:
        if QrDanhmuc2.FieldByName('Ma').AsString = '' then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.QrDanhmucBeforeEdit(DataSet: TDataSet);
begin
//    Abort;
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.QrDanhmucBeforePost(DataSet: TDataSet);
var
    s: String;
begin
    case PgMain.ActivePageIndex of
    0:
    	with QrDanhmuc do
    	begin
            if BlankConfirm(QrDanhmuc, ['MaChiNhanh']) then
                Abort;

            if fixCode1 then
		        if LengthConfirm(QrDanhmuc, ['MaChiNhanh']) then
        	        Abort;

            if BlankConfirm(QrDanhmuc, ['TenChiNhanh']) then
                Abort;
        end;
    1:
    	with QrDanhmuc2 do
    	begin
            if BlankConfirm(QrDanhmuc2, [FIELD_CODE]) then
                Abort;

            if fixCode2 then
		        if LengthConfirm(QrDanhmuc2, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrDanhmuc2, ['TenPhongBan']) then
                Abort;

            s := QrDanhmuc.FieldByName('MaChiNhanh').AsString;
			FieldByName('MaChiNhanh').AsString := s;
            FieldByName('MaPhongBan').AsString := s + FieldByName('Ma').AsString;
        end;
    2:
    	with QrDanhmuc3 do
    	begin
            if BlankConfirm(QrDanhmuc3, [FIELD_CODE]) then
                Abort;

            if fixCode3 then
		        if LengthConfirm(QrDanhmuc3, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrDanhmuc3, ['TenBoPhan']) then
                Abort;

            s := QrDanhmuc3.FieldByName('MaPhongBan').AsString;
			FieldByName('MaPhongBan').AsString := s;
            FieldByName('MaBoPhan').AsString := s + FieldByName('Ma').AsString;
        end;
    end;
    SetAudit(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.OnDBError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    Action := DbeMsg;
end;

    (*
    **  Other
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(mQuery, mFilter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.GrDanhmuc2CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if SameText(Field.FullName, 'Ma') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.QrDanhmuc2AfterInsert(DataSet: TDataSet);
begin
    DataSet.FieldByName('Ma').Clear
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhongban.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, mDs);
end;

end.
