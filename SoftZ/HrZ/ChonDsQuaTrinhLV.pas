﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsQuaTrinhLV;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Db, ADODB, Buttons, Grids,
  Wwdbigrd, Wwdbgrid2, ActnList, wwidlg, Wwkeycb, Wwdbgrid;

type
  TFrmChonDsQuaTrinhLV = class(TForm)
    QrDuLieu: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClear: TAction;
    DsDuLieu: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GrList2: TStringGrid;
    GrList1: TwwDBGrid2;
    Label1: TLabel;
    EdMaSearch: TwwIncrementalSearch;
    QrDuLieuMaQTLV: TWideStringField;
    QrDuLieuTenQTLV: TWideStringField;
    QrDuLieuTenQTLV_TA: TWideStringField;
    QrDuLieuMaQTLV_Nhom: TWideStringField;
    QrDuLieuTenQTLV_Nhom: TWideStringField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure GrList1ColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure EdMaSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);
    procedure EdMaSearchKeyPress(Sender: TObject; var Key: Char);
  private
    mTrigger: Boolean;

  public
  	function  Get(var pLst: String): Boolean;
  end;

var
  FrmChonDsQuaTrinhLV: TFrmChonDsQuaTrinhLV;

implementation

uses
	isDb, isLib, SysUtils, ExCommon, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    OpenDataSets([QrDuLieu]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsQuaTrinhLV.Get;
var
	i: Integer;
begin
    Result := ShowModal = mrOK;
    if Result then
    begin
        pLst := '';
        with GrList2 do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.EdMaSearchKeyPress(Sender: TObject; var Key: Char);
var
    s: String;
begin
    if (Key = #13) or (Key = #8) then
    begin
        s := (Sender as TwwIncrementalSearch).Text;

        with (Sender as TwwIncrementalSearch).DataSource.DataSet do
        begin
            if s = '' then
                Filter := '';
            Filtered := s <> '';
            First;
        end;
    end;
end;

procedure TFrmChonDsQuaTrinhLV.EdMaSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
    with QrDuLieu do
    begin
        s1 := FieldByName('MaQTLV').AsString;
        s2 := FieldByName('TenQTLV').AsString
    end;

    with GrList2 do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList2 do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
    s := EdMaSearch.Text;
    CmdIns.Enabled := not QrDuLieu.IsEmpty;
    CmdDel.Enabled := GrList2.Cells[0, GrList2.Row] <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.FormCreate(Sender: TObject);
begin
    mTrigger := False;

    with GrList2 do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDuLieu]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList2 do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsQuaTrinhLV.GrList1ColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
    s: String;
begin
    s := GrList1.Columns[0].FieldName;
    EdMaSearch.SearchField := s;
    QrDuLieu.Sort := s;
end;

end.
