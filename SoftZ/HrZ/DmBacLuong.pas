﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmBacLuong;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  AppEvnts, Menus, AdvMenus, wwDBGrid2, StdCtrls, Mask, wwdbedit, Wwdotdot,
  Wwdbigrd, ToolWin, wwdblook;

type
  TFrmDmBacLuong = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsDanhmuc: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    QrDanhmuc: TADOQuery;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    CmdReload: TAction;
    QrDanhmucMaBacLuong: TWideStringField;
    QrDanhmucTenBacLuong: TWideStringField;
    QrDanhmucTenBacLuong_TA: TWideStringField;
    QrDanhmucBac1: TFloatField;
    QrDanhmucBac2: TFloatField;
    QrDanhmucBac3: TFloatField;
    QrDanhmucBac4: TFloatField;
    QrDanhmucBac5: TFloatField;
    QrDanhmucBac6: TFloatField;
    QrDanhmucBac7: TFloatField;
    QrDanhmucBac8: TFloatField;
    QrDanhmucBac9: TFloatField;
    QrDanhmucBac10: TFloatField;
    QrDanhmucBac11: TFloatField;
    QrDanhmucBac12: TFloatField;
    QrDanhmucBac13: TFloatField;
    QrDanhmucBac14: TFloatField;
    QrDanhmucBac15: TFloatField;
    QrDanhmucBac16: TFloatField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucDELETE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucDELETE_DATE: TDateTimeField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDanhmucBeforeEdit(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
  private
  	mCanEdit, fixCode, mChecked: Boolean;
    qtyFmt: String;
    qtyRound: Integer;
  public
  	procedure Execute (r : WORD);
  end;

var
  FrmDmBacLuong: TFrmDmBacLuong;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, MainData, isCommon;

{$R *.DFM}
const
    FORM_CODE = 'HR_DM_BACLUONG';
    TABLE_NAME = 'HR_DM_BACLUONG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
  var bEmpty : Boolean;
begin
    with QrDanhmuc do
    begin
      if not Active then
         Exit;
      bEmpty := IsEmpty ;
    end;

    exActionUpdate(ActionList, QrDanhmuc, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrDanhmuc]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.FormShow(Sender: TObject);
begin

    SetDisplayFormat(QrDanhmuc, ['Bac1', 'Bac2', 'Bac3', 'Bac4', 'Bac5', 'Bac6', 'Bac7','Bac8',
    'Bac9', 'Bac10', 'Bac11', 'Bac12', 'Bac13', 'Bac14', 'Bac15','Bac16'], qtyFmt);
    SetDictionary([QrDanhmuc], [FORM_CODE], []);
    SetCustomGrid( [FORM_CODE], [GrList]);
	GrList.ReadOnly := not mCanEdit;
	OpenDataSets([QrDanhmuc]);
    GrList.SetFocus;
end;

procedure TFrmDmBacLuong.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if (Field.FullName = 'MaBacLuong') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.QrDanhmucBeforePost(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
		if BlankConfirm(QrDanhmuc, ['MaBacLuong']) then
    		Abort;

        if fixCode then
            if LengthConfirm(QrDanhmuc, ['MaBacLuong']) then
                Abort;

		if BlankConfirm(QrDanhmuc, ['TenBacLuong']) then
    		Abort;

//		SetBool;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

procedure TFrmDmBacLuong.QrDanhmucBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    if not mCanEdit then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg; 
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1();
    qtyFmt := '#,##0.00;-#,##0.00;#';
    qtyRound := -2;
    fixCode := SetCodeLength('HR_DM_BACLUONG', QrDanhmuc.FieldByName('MaBacLuong'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBacLuong.CmdReloadExecute(Sender: TObject);
begin
    QrDanhmuc.Requery;
end;

end.
