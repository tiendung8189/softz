﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HosonvTaiKhoanTruyCap;

interface

uses
  Windows, Classes, Controls, Forms, ComCtrls, ExtCtrls,
  StdCtrls, Buttons, Mask, DBCtrls, ActnList, ADODb, Wwdbigrd,
  Wwdbgrid, Db, wwDBGrid2, SysUtils, Grids, wwdbedit, rDBComponents, DBCtrlsEh;

type
  TFrmHosonvTaiKhoanTruyCap = class(TForm)
    ActionList: TActionList;
    BtnOK: TBitBtn;
    BitBtn2: TBitBtn;
    CmdOK: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    EdName: TDBEditEh;
    EdPassword: TEdit;
    EdConfirmPas: TEdit;
    EdUser: TDBEditEh;
    GrList: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    QrList: TADOQuery;
    QrListIDX: TIntegerField;
    DsList: TDataSource;
    CmdRemove: TAction;
    CmdAdd: TAction;
    QrListDESC: TWideStringField;
    ChkDisable: TDBCheckBox;
    QrListIS_GROUP: TBooleanField;
    QrListGROUP: TIntegerField;
    QrListMEMBER: TIntegerField;
    QrListGROUP_NAME: TWideStringField;
    EdBrief: TDBEditEh;
    EdEmail: TDBEditEh;
    ChkPwdNerver: TDBCheckBox;
    ChkMustChgPwd: TDBCheckBox;
    ChkIsEmpoyee: TrDBCheckBox;
    BtnCapMoi: TBitBtn;
    CmdCapMoi: TAction;
    QrUser: TADOQuery;
    DsUser: TDataSource;
    QrUserUserName: TWideStringField;
    QrUserFullName: TWideStringField;
    QrUserBriefName: TWideStringField;
    QrUserEmail: TWideStringField;
    QrUserPassWD: TWideStringField;
    QrUserIs_Group: TBooleanField;
    QrUserDisabled: TBooleanField;
    QrUserMust_Chg_Pwd: TBooleanField;
    QrUserPwd_Never: TBooleanField;
    QrUserLast_Pwd_Chg: TDateTimeField;
    QrUserUID: TIntegerField;
    QrUserIs_Employee: TBooleanField;
    QrUserManv: TWideStringField;
    QrUserLOC: TWideStringField;
    BtnUpdate: TBitBtn;
    CmdUpdate: TAction;
    CmdCancel: TAction;
    BtnCancel: TBitBtn;
    CmdEdit: TAction;
    BtnEdit: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure CmdOKExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdRemoveExecute(Sender: TObject);
    procedure CmdAddExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure EdPasswordChange(Sender: TObject);
    procedure CmdCapMoiExecute(Sender: TObject);
    procedure QrUserBeforeOpen(DataSet: TDataSet);
    procedure QrUserAfterInsert(DataSet: TDataSet);
    procedure QrUserBeforePost(DataSet: TDataSet);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrUserAfterPost(DataSet: TDataSet);
  private
    mIsNew, mIsEncode: Boolean;
    mManv, mManvQL, mTennv, mEmail: string;
    function  IsValidInput: Boolean;
    procedure SetFakePass(const FakeText: String = '');
    procedure visibleAddNewEdit();
  public
    procedure Execute(EmpID, mEmpIDLabel, EmpName, EmpEmail: String; mEncode: Boolean = True);
  end;

var
  FrmHosonvTaiKhoanTruyCap: TFrmHosonvTaiKhoanTruyCap;

implementation

uses
	isMsg, isLib, ExCommon, isDb, isEnCoding, MainData, isType,
    ExAdminCommon, AddList, AdminData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_NEW		= 'Thêm';
    RS_ACCEPT	= 'Đồng ý';

procedure TFrmHosonvTaiKhoanTruyCap.CmdEditExecute(Sender: TObject);
begin
    QrUser.Edit;
    EdName.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.Execute;
var
    s: String;
begin

    mIsEncode := mEncode;
    mManv := EmpID;
    mManvQL := mEmpIDLabel;
    mTennv := EmpName;
    mEmail := EmpEmail;

//	if mIsNew then
//    	CmdOK.Caption := RS_NEW
//    else
//    	CmdOK.Caption := RS_ACCEPT;
//
//    s := QrUser.FieldByName('USERNAME').AsString;
//    PgMain.Pages[1].TabVisible := not mIsNew and
//        (s <> IS_USER_ADMIN) and
//        (s <> IS_GROUP_EVERYONE);
//
//    with EdUser do
//    begin
//    	ReadOnly := not mIsNew;
//        TabStop  := mIsNew;
//    end;

//    ChkDisable.Visible := mIsNew or (s <> IS_USER_ADMIN);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.SetFakePass(const FakeText: String);
begin
    with EdPassword do
    begin
        Tag := 1;   // Skip event OnChange
        Text := FakeText;
        Tag := 0;
        EdConfirmPas.Text := Text;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.FormShow(Sender: TObject);
begin
    QrUser.Open;
    
    if not mIsNew then
    begin
//        QrUser.Edit;
        QrList.Open;
        SetFakePass('!@#$%^&*()');
        EdName.SetFocus;
    end;
    visibleAddNewEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    QrUser.Cancel;
	CloseDataSets([QrUser, QrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_RETYPE = 'Mật khẩu lặp lại không đúng.';

procedure TFrmHosonvTaiKhoanTruyCap.CmdOKExecute(Sender: TObject);
begin
    if EdConfirmPas.Text <> EdPassword.Text Then
    begin
    	Msg(RS_RETYPE);
        Exit;
    end;
    // Post user info. and list of members
    QrUser.CheckBrowseMode;
    with QrList do
    if Active then
    begin
        CheckBrowseMode;
        UpdateBatch;
    end;

//	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.CmdRemoveExecute(Sender: TObject);
var
    i: Integer;
begin
    with GrList.SelectedList, QrList do
        for i := 0 to Count - 1 do
        begin
            try
                GotoBookmark(TBookmark(Items[i]));
//                if FieldByName('GROUP').AsInteger = sysAdminId then
                    Delete;
            except
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.CmdAddExecute(Sender: TObject);
var
    mUID, i: Integer;
    s: String;
begin
    mUID := QrUser.FieldByName('UID').AsInteger;
    Application.CreateForm(TFrmAddList, FrmAddList);
    with FrmAddList.GetList(mUID) do
    begin
        for i := 0 to Count - 1 do
        begin
            s := Strings[i];
            with QrList do
            if not Locate('GROUP', s, []) then
            begin
                begin
                    Append;
                    FieldByName('MEMBER').AsInteger := mUID;
                    FieldByName('GROUP').AsString := s;
                    Post;
                end;
            end;
        end;
        Free;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.CmdCancelExecute(Sender: TObject);
begin
    with QrList do
    if Active then
        CancelBatch;
    QrUser.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.CmdCapMoiExecute(Sender: TObject);
begin
    mIsNew := True;
    QrUser.Append;
    QrList.Open;
    SetFakePass;
    EdUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.CmdUpdateExecute(Sender: TObject);
begin
    with QrUser do
    begin
        FieldByName('UserName').AsString := mManvQL;
        FieldByName('FullName').AsString := mTennv;
        FieldByName('Email').AsString := mEmail;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvTaiKhoanTruyCap.IsValidInput: Boolean;
begin
	Result := (Trim(EdUser.Text) <> '') and (Trim(EdName.Text) <> '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    n: Integer;
    bEmpty, bBrowse, bInsert, bEdit: Boolean;
begin
    with QrUser do
    begin
        bEmpty := IsEmpty;
        bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bEdit := State in [dsEdit];
	end;
    mIsNew := bEmpty;
    CmdCapMoi.Enabled := not bInsert;
    CmdEdit.Enabled := not bEdit;
    CmdOK.Enabled := not bEmpty and not bBrowse and IsValidInput;
    CmdUpdate.Enabled := not bBrowse and not bEmpty;
    CmdCancel.Enabled := not bBrowse;
    n := PgMain.ActivePageIndex;
    CmdAdd.Enabled := n = 1;

	with QrList do
    begin
    	if not Active then
	    	Exit;
    	CmdRemove.Enabled := (not IsEmpty) and (n = 1);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrListBeforeOpen(DataSet: TDataSet);
begin
    QrList.Parameters[0].Value :=
        QrUser.FieldByName('UID').AsInteger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrListCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    if FieldByName('IS_GROUP').AsBoolean then
        FieldByName('IDX').AsInteger := 1
    else if FieldByName('GROUP').AsInteger = sysAdminId then
        FieldByName('IDX').AsInteger := 2
    else
        FieldByName('IDX').AsInteger := 0
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrUserAfterInsert(DataSet: TDataSet);
begin
    with QrUser do
    begin
        FieldByName('UID').AsInteger := NewUID;
        FieldByName('Manv').AsString := mManv;
        FieldByName('UserName').AsString := mManvQL;
        FieldByName('FullName').AsString := mTennv;
        FieldByName('Email').AsString := mEmail;
        FieldByName('Is_Employee').AsBoolean := True;
        FieldByName('Must_Chg_Pwd').AsBoolean := False;
        FieldByName('Pwd_Never').AsBoolean := False;
        FieldByName('Disabled').AsBoolean := False;
        FieldByName('Is_Group').AsBoolean := False;
        if mIsEncode then
            FieldByName('PASSWD').AsString := isEncodeMD5('');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrUserAfterPost(DataSet: TDataSet);
begin
    visibleAddNewEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrUserBeforeOpen(DataSet: TDataSet);
begin
    QrUser.Parameters[0].Value := mManv;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.QrUserBeforePost(DataSet: TDataSet);
begin
    with QrUser do
    begin
    	if BlankConfirm(DataSet, ['UserName', 'FullName']) then
            Abort;

        if mIsNew and BlankConfirm(DataSet, ['PASSWD']) then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := IsValidInput
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.EdPasswordChange(Sender: TObject);
begin
    if EdPassword.Tag <> 0  then
        Exit;

    SetEditState(QrUser);
    with QrUser do
    begin
        if mIsEncode then
            FieldByName('PASSWD').AsString := isEncodeMD5(EdPassword.Text)
        else
            FieldByName('PASSWD').AsString := EdPassword.Text;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvTaiKhoanTruyCap.visibleAddNewEdit();
begin
    with QrUser do
    begin
        BtnCapMoi.Visible := IsEmpty;
        BtnEdit.Visible := not IsEmpty;
    end;
end;

end.
