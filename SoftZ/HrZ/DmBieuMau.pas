﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmBieuMau;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdblook, AdvEdit, RzEdit, RzDBEdit, RzDBBnEd,
  Winapi.ShellAPI, Winapi.Windows, Vcl.Buttons, DBCtrlsEh;

type
  TFrmDmBieuMau = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMBieuMau: TADOQuery;
    DsDMBieuMau: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrDMBieuMauCREATE_BY: TIntegerField;
    QrDMBieuMauUPDATE_BY: TIntegerField;
    QrDMBieuMauCREATE_DATE: TDateTimeField;
    QrDMBieuMauUPDATE_DATE: TDateTimeField;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    Label7: TLabel;
    EdFileName: TDBEditEh;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    CmdDetail: TAction;
    ToolButton12: TToolButton;
    QrDMBieuMauFileName: TWideStringField;
    QrDMBieuMauFileExt: TWideStringField;
    QrDMBieuMauFileChecksum: TWideStringField;
    QrDMBieuMauFileGroup: TWideStringField;
    QrDMBieuMauFileDesc: TWideStringField;
    QrDMBieuMauComment: TWideMemoField;
    QrDMBieuMauCacl_FileExt: TWideStringField;
    CbNhom: TwwDBLookupCombo;
    RefNhom: TADOQuery;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    Panel1: TPanel;
    QrDMBieuMauLK_FileGroup: TWideStringField;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    CmdFilePlus: TAction;
    PaDinhKem: TisPanel;
    DBEditEh1: TDBEditEh;
    QrDMBieuMauCalc_FileName: TWideStringField;
    CmdOpenLink: TAction;
    QrDMBieuMauLink: TWideStringField;
    DBEdit6: TDBEditEh;
    QrDMBieuMauIdx: TAutoIncField;
    QrDMBieuMauFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBieuMauBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBieuMauBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBieuMauBeforeInsert(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure BtThelenhClick(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBieuMauCalcFields(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrDMBieuMauAfterInsert(DataSet: TDataSet);
    procedure QrDMBieuMauAfterPost(DataSet: TDataSet);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdOpenLinkExecute(Sender: TObject);
    procedure QrDMBieuMauLinkValidate(Sender: TField);
    procedure QrDMBieuMauAfterDelete(DataSet: TDataSet);
  private
    r: WORD;
  	mCanEdit: Boolean;
    mSQL, mSearch: String;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmBieuMau: TFrmDmBieuMau;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    isStr, isFile, DmHotro_HR, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_BIEUMAU';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDMBieuMau.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMBieuMau, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');
    mSQL := QrDMBieuMau.SQL.Text;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDMBieuMau, RefNhom]);
    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDMBieuMau]);
    finally
    end;

	// Save state panel
    RegWrite(Name, 'PD2', PD2.Collapsed);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMBieuMau, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdNewExecute(Sender: TObject);
begin
    QrDMBieuMau.Append;
    EdFileName.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdOpenLinkExecute(Sender: TObject);
begin
    with QrDMBieuMau do
    if FieldByName('Link').AsString <> '' then
    begin
         ShellExecute(0, 'Open', PChar(FieldByName('Link').AsString), nil,  nil, SW_NORMAL);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdRefreshExecute(Sender: TObject);
var
	sSQL, s1 : String;
begin
   	if (EdSearch.Text <> mSearch) then
	begin
        mSearch := EdSearch.Text;
        with QrDMBieuMau do
        begin
			Close;
            sSQL := mSQL;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSQL := sSQL + ' and (' +
                            'dbo.fnStripToneMark([FileName]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([FileDesc]) like ''%' + s1 + '%'''
                            + ')';
            end;

            sSQL := sSQL +' order by [FileGroup]';
            SQL.Text := sSQL;
            Open;
            First;
        end;

		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdSaveExecute(Sender: TObject);
begin
    QrDMBieuMau.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdCancelExecute(Sender: TObject);
begin
    QrDMBieuMau.Cancel;
    GrList.SetFocus;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdDelExecute(Sender: TObject);
begin
    QrDMBieuMau.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdDetailExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_NHOM_BIEUMAU');

    RefNhom.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMBieuMau)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdFileMinusExecute(Sender: TObject);
begin
   if not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
   bViewFile, bLink: Boolean;
begin
    bViewFile := False;
    bLink := False;
    exActionUpdate(ActionList, QrDMBieuMau, Filter, mCanEdit);
    with QrDMBieuMau do
    begin
        if Active then
        begin
           bViewFile := FieldByName('FileExt').AsString <> '';
           bLink := FieldByName('Link').AsString <> '';
        end;
    end;

    CmdOpenLink.Enabled := bLink;
    CmdFileMinus.Enabled := bViewFile;
    CmdFileView.Enabled := bViewFile;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauAfterInsert(DataSet: TDataSet);
begin
    with QrDMBieuMau do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauAfterPost(DataSet: TDataSet);
begin
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrDMBieuMau.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauBeforePost(DataSet: TDataSet);
begin
	with QrDMBieuMau do
    begin
	    if BlankConfirm(QrDMBieuMau, ['FileDesc', 'FileGroup', 'FileName']) then
   			Abort;

    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrDMBieuMau do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauLinkValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
    begin
        if not validateUrl(Sender.AsString) then
        begin
            ErrMsg(Format(RS_INVAILD_VALIDATION, [Sender.DisplayLabel]));
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrDMBieuMau.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.QrDMBieuMauBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMBieuMau, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.BtThelenhClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMBieuMau);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;


    with QrDMBieuMau do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrDMBieuMau.FieldByName('Idx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.Dettach();
begin
     with QrDMBieuMau do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmBieuMau.ViewAttachment();
var
	s, sn: String;
begin
	with QrDMBieuMau do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

end.
