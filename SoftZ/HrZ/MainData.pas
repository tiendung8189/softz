﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit MainData;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Db, ADODB, Wwintl, isEnv,
  AdvMenuStylers, AdvMenus, ImgList, isLogin, NativeXml;

type
  TDataMain = class(TDataModule)
    Conn: TADOConnection;
    QrUSER: TADOQuery;
    QrTEMP: TADOQuery;
    QrNGOAITE: TADOQuery;
    QrLABEL: TADOQuery;
    EXPORT_LABEL: TADOStoredProc;
    isEnv1: TisEnv;
    ImageMark: TImageList;
    ImageSort: TImageList;
    ImageSmall: TImageList;
    wwIntl: TwwIntl;
    ImageNavi: TImageList;
    ImageOrg: TImageList;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    SYS_SEQ_GUID: TADOCommand;
    QrDMTK: TADOQuery;
    QrLOC: TADOQuery;
    GETACCESS: TADOCommand;
    GETSEQ: TADOCommand;
    QrFUNCS: TADOQuery;
    isLogon: TisLogon;
    GET_LOGON: TADOStoredProc;
    SYS_LOGOUT: TADOStoredProc;
    SETPASS: TADOStoredProc;
    SETPARAM: TADOCommand;
    QrDMPHONGBAN: TADOQuery;
    QrDMNV: TADOQuery;
    QrSql: TADOQuery;
    SYS_SEQ_SHA1: TADOCommand;
    QrDMTK_NV: TADOQuery;
    QrDMTK_NB: TADOQuery;
    QrDM_CHUCVU: TADOQuery;
    QrDM_MAYCC: TADOQuery;
    QrSite: TADOQuery;
    QrDMBOPHAN: TADOQuery;
    QrLOAI_HDLD: TADOQuery;
    QrQuocgia: TADOQuery;
    QrTinh: TADOQuery;
    QrHuyen: TADOQuery;
    QrNganhang: TADOQuery;
    QrNganhangCN: TADOQuery;
    GEN_TIMESHEET: TADOCommand;
    QrDM_NHOMLV: TADOQuery;
    QrDM_CHUCDANH: TADOQuery;
    QrNHOM_PHUCAP: TADOQuery;
    HR_MANV_HOPLE: TADOCommand;
    QrDM_CALAMVIEC: TADOQuery;
    QrLOAI_CANGHI: TADOQuery;
    QrLOAI_GIOITINH: TADOQuery;
    QrDM_HDLD: TADOQuery;
    QrV_QUOCTICH: TADOQuery;
    QrV_DANTOC: TADOQuery;
    QrV_TONGIAO: TADOQuery;
    QrV_VANHOA: TADOQuery;
    QrLOAI_THOIVIEC: TADOQuery;
    QrDM_LYDO_VANGMAT: TADOQuery;
    procedure ConnWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    mEximPath: String;
  public
    procedure InitParams(const subsys: Integer = 0);
    function  Logon(const subsys: Integer = 0): Boolean;

    function  ExecSQL(const sqlStr: String): Integer;
    function  GetNewGuid: TGuid;
    function  GetNewSHA1: String;

    function  GetSysParam(param: String): Variant;
    procedure SetSysParam(const ParamName: String; const Value: Variant);
    function  GetSeqValue(seq: String): Integer;
	function  GetRights(func: String; denyshow: Boolean = True): Integer; overload;
	function  GetRights(user, func: String): Integer; overload;
    function  GetRights(uid: Integer; func: String): Integer; overload;
	function  GetFuncState(func: String): Boolean;

    function  GetUsername(uid: Integer): String;
    function  GetUID(username: String ): Integer;

    function  UpdatePayrollSheet(m, y: Integer; pDaily: Boolean): Integer;
    function  GetPeriodStatus(m, y: Integer; t: Integer): Boolean;

    //HR
    function  ManvValid(pMANV, pFunc: String; pDate: TDateTime): Integer;

    //Xml
    function  ListTables(pType: Integer; ls: TStrings): Boolean;
    procedure CopyData(qrSource, qrDest: TADOQuery);

    function CreateXml: TNativeXml;

    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer); overload;
    procedure xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer); overload;

    procedure XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
    procedure XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);

    procedure XmlCreateNode(pXml: TNativeXml; tabname: String);
    procedure ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String); overload;

    procedure ImportDataXml(pXml: TNativeXml; pTable: Integer); overload;
    procedure ImportDataXml(pXml: TNativeXml; pTable: string); overload;
  end;

var
  DataMain: TDataMain;

implementation

uses
	ExCommon, Rights, isMsg, isDb, Variants, isCommon, GuidEx, RepEngine,
    isOdbc, isStr, isType, isLib, AdminData, isEnCoding, isFile;

const
    EXCEPT_FIELDS: String = 'TABLE,';
    SELECT_SQL: String = 'select * from ';
    SELECT_SQL2: String = 'select FLEXIBLE from ';

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    NOT_CONFIG_YET = 'Chưa cấu hình số hiệu máy bán hàng.';
    NOT_CONFIG_SYSLOC = 'Chưa cấu hình Chinh nhánh. Kết thúc chương trình.';
    EXIST_HELD_INVOICE = 'Có hóa đơn chưa lưu. Tiếp tục?';

function TDataMain.Logon(const subsys: Integer): Boolean;
begin
	Result := False;
//    if not DbConnect then
//        Exit;

//    if not ServerLicenseCheck then
//    	Exit;

//    if CheckSlKho(True) then
//        Exit;

    DataMain.Conn.Close;

  // Logon
    with isLogon do
    begin
        if not Logon2(RegReadString('', Iif(subsys = 1, 'LastLogonUserHR' ,'LastLogonUser'), '')) then
        begin
            Exit;
        end;
    end;

    if not IsLicenseCheck then
        Exit;

    sysDbConnection := Conn;

    InitParams(subsys);
    sysIsAdmin := sysLogonUser = 'ADMIN';
    if sysIsAdmin then
        DataAdmin.Initial;

    if sysLoc = '' then
    begin
        ErrMsg(NOT_CONFIG_SYSLOC);
        Exit;
    end;
//    Screen.Cursor := crHourGlass;
//    CheckWebUpdate;
//    Screen.Cursor := crDefault;

    if not sysIsAdmin  then
        RegWrite('', Iif(subsys = 1, 'LastLogonUserHR' ,'LastLogonUser'), sysLogonUser);


    wwIntl.Connected := not sysEnglish;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.InitParams;
var
	s: String;
    yy, mm, dd: WORD;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select * from SYS_CONFIG';
        Open;

        sysDesc1 := FieldByName('HEADER1').AsString;
		sysDesc2 := FieldByName('HEADER2').AsString;
		sysDesc3 := FieldByName('HEADER3').AsString;

        sysCurFmt := FieldByName('FMT_GMS_CUR').AsString;
//        sysQtyFmt := FieldByName('FMT_GMS_QTY').AsString;
//        sysUsdFmt := FieldByName('FMT_GMS_USD').AsString;

        s := FieldByName('FMT_GMS_THOUSAND_SEP').AsString;
        if s <> '' then
            ThousandSeparator := s[1];

        s := FieldByName('FMT_GMS_DECIMAL_SEP').AsString;
        if s <> '' then
            DecimalSeparator := s[1];
        sysPerFmt := FieldByName('FMT_PER').AsString;
        sysTaxFmt := FieldByName('FMT_TAX').AsString;

        sysCurRound := FieldByName('ROUNDING_CUR').AsInteger;
        sysNumRound := FieldByName('ROUNDING_NUM').AsInteger;

        sysIntFmt := '#,##0;-#,##0;#';

        // Date format
        if subsys = 0 then
			s := FieldByName('FMT_GMS_DATE').AsString
        else
			s := FieldByName('FMT_POS_DATE').AsString;

        if s = '' then
		    s := 'dd/mm/yyyy';
	    ShortDateFormat := s;
        DateTimeFmt := s + ' hh:nn:ss';
        sysMinFmt := ' hh:nn';

        sysCloseHH := Trunc(FieldByName('KS_HH').AsDateTime);
        sysCloseCN := Trunc(FieldByName('KS_CN').AsDateTime);

        sysLoc  := FieldByName('DEFAULT_LOC').AsString;
        sysSite  := FieldByName('DEFAULT_CHINHANH').AsString;
        sysIsCentral := FieldByName('IS_CENTRAL').AsBoolean;
        sysRepPath := FieldByName('FOLDER_REPORT').AsString;

        sysLateDay := FieldByName('DEFAULT_LATE_DAY').AsInteger;

//        if FieldByName('SEARCH_MATCH_ANY').AsBoolean then
//            isSetSearchOption([CODE_ANY_WHERE])
//        else
//            exSetSearchOption([]);

        
        // Begin month year user Softz
        sysBegMon  := FieldByName('BEGIN_MONTH').AsInteger;
        sysBegYear := FieldByName('BEGIN_YEAR').AsInteger;
        sysIsDrc   := FieldByName('IS_DRC').AsBoolean;
        sysIsHrChinhanh := FieldByName('IS_HR_CHINHANH').AsBoolean;

        DecodeDate(Date, yy, mm, dd);
        sysMon  := mm;
        sysYear := yy;

        // Done
       	Close;
        Free;
    end;
    InitCommonVariables;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ConnWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
//	UserID := dbUser;
//    Password := dbPasswd;
//	ConnectionString := BuildConStr(ConnectionString);
    ConnectionString := GetConnectionString(isNameServer, isUserServer, isPasswordServer, isDatabaseServer);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleCreate(Sender: TObject);
begin
	CloseDataSets(Conn);
    Conn.Connected := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.DataModuleDestroy(Sender: TObject);
begin
//	FreeReportEngine;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ManvValid;
begin
	with HR_MANV_HOPLE do
    begin
    	Prepared := True;
        Parameters[1].Value := sysSite;
        Parameters[2].Value := pMANV;
        Parameters[3].Value := pDate;
        Parameters[4].Value := pFunc;
//        Parameters[5].Value := 1; // sysFemale;
        Execute;
        Result := Parameters[0].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ExecSQL(const sqlStr: String): Integer;
var
	n: Integer;
begin
	with Conn do
        Execute(sqlStr, n);
    Result := n;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewGuid: TGuid;
begin
    with SYS_SEQ_GUID do
    begin
        Prepared := True;
        Execute;
        Result := TGuidEx.FromString(Parameters[1].Value);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetNewSHA1: String;
begin
    with SYS_SEQ_SHA1 do
    begin
        Prepared := True;
        Execute;
        Result := Parameters[1].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetPeriodStatus(m, y, t: Integer): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format(
        	'select [KHOASO_CONG], [KHOASO_LUONG], [KHOASO_LICHLV], [DUYET_LUONG] from HR_BANG_THONGSO where [NAM]=%d and [THANG]=%d', [y, m]);
        Open;
        Result := Fields[t].AsBoolean;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetSysParam;
var
    x : Variant;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select %s from SYS_CONFIG', [param]);
        Open;

		x := Fields[0].Value;
        if x = Null then
        	case Fields[0].DataType of
			ftMemo, ftString, ftWideMemo, ftWideString:
				Result := '';
            ftSmallint, ftInteger, ftWord, ftBytes:
				Result := 0;
            ftBoolean:
				Result := False;
            ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime:
				Result := 0.0;
            else
				Result := x;
            end
        else
			Result := x;

        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUID(username: String): Integer;
begin
    with QrUSER do
    begin
        if Locate('USERNAME', username, []) then
            Result := FieldByName('UID').AsInteger
        else
            Result := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetUsername(uid: Integer): String;
begin
    with QrUSER do
    begin
        if Locate('UID', uid, []) then
            Result := FieldByName('USERNAME').AsString
        else
            Result := '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.SetSysParam;
begin
	with SETPARAM do
    begin
        CommandText := Format('update SYS_CONFIG set [%s]=:Value', [ParamName]);
        Parameters.Refresh;
        Parameters[0].Value := Value;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.UpdatePayrollSheet(m, y: Integer; pDaily: Boolean): Integer;
var
	s: String;
begin
//	Wait(PROCESSING);
    try
        with GEN_TIMESHEET do
        begin
            Parameters.Refresh;
            Parameters[1].Value := y;
            Parameters[2].Value := m;
            Parameters[3].Value := pDaily;
            Execute;
            Result := Parameters[0].Value
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.GetSeqValue;
begin
	with GETSEQ do
    begin
    	Prepared := True;
    	Parameters[1].Value := seq;
        Parameters[3].Value := True;
        Execute;
    	Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TGetRights(func: String; denyshow: Boolean = True): Integer;
begin
	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := sysLogonUID;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;

    if DenyShow and (Result = R_DENY) then
    	DenyMsg;
end;

function TGetRights(user, func: String): Integer;
var
	uid: Integer;
begin
	with TADOQuery.Create(Self) do
    begin
    	Connection := Conn;
        LockType := ltReadOnly;
        SQL.Text := Format('select UID from SYS_USER where USERNAME=''%s''', [user]);
        Open;
		uid := Fields[0].AsInteger;
        Close;
        Free;
	end;

	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := uid;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;
end;

function TGetRights(uid: Integer; func: String): Integer;
begin
	with GetAccess do
    begin
    	Prepared := True;
		Parameters[1].Value := uid;
		Parameters[2].Value := func;
        Execute;
       	Result := Parameters[0].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TGetFuncState(Func : String) : Boolean;
begin
	with QrFuncs do
    begin
    	if not Active then
        	Open;
        if Locate('FUNC_CODE', Func, []) then
        	Result := FieldByName('ENABLED').AsBoolean
        else
        	Result := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataMain.ListTables(pType: Integer; ls: TStrings): Boolean;
begin
    Result := False;

    // OtherX
    if (pType and 64) <> 0 then
    begin
        ls.Add('DM_KHACX')
    end;

    // Dbe
    if (pType and 32) <> 0 then
    begin
        ls.Add('SYS_DBE_OBJ');
        ls.Add('SYS_DBE_MSG');
    end;

    // Reports
    if (pType and 16) <> 0 then
        ls.Add('SYS_REPORT');

    // Functions
    if (pType and 8) <> 0 then
        ls.Add('SYS_FUNC');

    // Flexible
    if (pType and 4) <> 0 then
        ls.Add('SYS_CONFIG');

    // Dictionary
    if (pType and 2) <> 0 then
        ls.Add('SYS_DICTIONARY');

    // Grid
    if (pType and 1) <> 0 then
        ls.Add('SYS_GRID');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.CopyData(qrSource, qrDest: TADOQuery);
var
    i, n: Integer;
begin
    // Export
    with qrDest do
    begin
        n := qrSource.FieldCount - 1;
        while not qrSource.Eof do
        begin
            Append;
            for i := 0 to n do
                if Fields[i].DataType <> ftAutoInc then
                begin
                    FieldByName(qrSource.Fields[i].FullName).Value := qrSource.Fields[i].Value;
                end;
            Post;
            qrSource.Next;
        end;
        Close;
    end;
    qrSource.Close;
end;

function TDataMain.CreateXml: TNativeXml;
begin
	Result := TNativeXml.CreateName('ApplicationConfigs');
	with Result do
	begin
		EncodingString := 'Utf-8';
		Root.AttributeAdd('Exportby', ExtractFileName(Application.ExeName));
        Root.AttributeAdd('Version', VersionString);
        Root.AttributeAdd('Source', isNameServer + '.' + isDatabaseServer);
	end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlCreateNode(pXml: TNativeXml; tabname: String);
var
    i, n: Integer;
    sField: String;
    nodeR: TXmlNode;
begin
    // Table
    pXml.Root.NodeNew(tabname);

    // Column & Data
    with QrSql do
    begin
        SQL.Text := SELECT_SQL + tabname;
        Open;
        n := FieldCount - 1;
        while not Eof  do
        begin
            nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
            for i := 0 to n do
                if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                begin
                    sField := Fields[i].FullName;
                    nodeR.NodeNew(sField).ValueAsString :=FieldByName(sField).AsString;
                end;
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ExportDataXlm(pXml: TNativeXml; pTable: Integer; pFile: String);
var
    i: Integer;
    ls: TStrings;

    (*
    **
    *)
    function TableKey(pTabname: string): string;
    begin
        if (pTabname = 'SYS_GRID') then
            Result := 'NAME'
        else
        if (pTabname = 'SYS_DICTIONARY') then
            Result := 'TABLE;FIELD'
        else
        if (pTabname = 'SYS_CONFIG') then
            Result := 'HEADER1'
        else
        if (pTabname = 'SYS_FUNC') then
            Result := 'FUNC_CODE'
        else
        if (pTabname = 'SYS_REPORT') then
            Result := 'REP_NAME'
        else
        if (pTabname = 'SYS_DBE_MSG') then
            Result := 'ERROR'
        else
        if (pTabname = 'SYS_DBE_OBJ') then
            Result := 'KEY'
        else
        if (pTabname = 'DM_KHACX')then
            Result := 'LOAI'
        else
        if (pTabname =  'LIST_OTHER_TYPE')  then
            Result := 'Type'
        else
            Result := '';
    end;

    procedure CreateNode(pXml: TNativeXml; tabname: String);
    var
        i, n: Integer;
        sField: String;
        nodeR: TXmlNode;
    begin
        // Table
        pXml.Root.NodeNew(tabname);
        if (tabname = 'DM_KHACX') or (tabname = 'LIST_OTHER_TYPE') then
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 0)
        else
            pXml.Root.NodeByName(tabname).AttributeAdd('Full', 1);

        pXml.Root.NodeByName(tabname).AttributeAdd('Key', TableKey(tabname));

        // Column & Data
        with QrSql do
        begin
            SQL.Text := SELECT_SQL + tabname;
            Open;
            n := FieldCount - 1;
            if tabname = 'SYS_CONFIG' then
            begin
                nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
                nodeR.NodeNew('FLEXIBLE').ValueAsString := (FieldByName('FLEXIBLE').AsString);
//                nodeR.NodeNew('FLEXIBLE').ValueAsString := isEncode64(FieldByName('FLEXIBLE').AsString);
            end else
            while not Eof  do
            begin
                nodeR := pXml.Root.NodeByName(tabname).NodeNew('ROW');
                for i := 0 to n do
                    if (Fields[i].AsString <> '') and (not Fields[i].ReadOnly) then
                    begin
                        sField := Fields[i].FullName;
                        nodeR.NodeNew(sField).ValueAsString := (FieldByName(sField).AsString);
//                        nodeR.NodeNew(sField).ValueAsString := isEncode64(FieldByName(sField).AsString);
                    end;
                Next;
            end;
        end;
    end;

begin
    mEximPath := pFile;
    Screen.Cursor := crSQLWait;
    ls := TStringList.Create;

    ListTables(pTable, ls);
    for i := 0 to ls.Count - 1 do
        CreateNode(pXml, ls[i]);

    ls.Free;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pPos: Integer) overload;
var
    i: Integer;
    data: string;
begin
    with pData do
    begin
        Append;
        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.xmlNodeToDs(pNode: TXmlNode; pData: TDataSet; pKey: string; pPos: Integer) overload;
var
    i: Integer;
    data: string;
    sK1, sK2, sV1, sV2: string;
begin
    i := Pos(';', pKey);
    if i > 0 then
    begin
        sK1 := isLeft(pKey, i-1);
        sK2 := isRight(pKey, Length(pKey)- i);
    end else
        sK1 := pKey;

    sV1 := pNode.FindNode(sK1).ValueAsUnicodeString;
    if i > 0 then
        sV2 := pNode.FindNode(sK2).ValueAsUnicodeString;

    with pData do
    begin
        if i > 0 then
        begin
//            if Locate(pKey, VarArrayOf([isDecode64(sV1), isDecode64(sV2)]),[]) then
            if Locate(pKey, VarArrayOf([(sV1), (sV2)]),[]) then
                SetEditState(pData)
            else
                Append;
        end else
        begin
//            if Locate(pKey, isDecode64(sV1), []) then
            if Locate(pKey, (sV1), []) then
                SetEditState(pData)
            else
                Append;
        end;

        for i := pPos to FieldCount - 1 do
            XmlNodeToField2(pNode, Fields[i]);
        try
            Post;
        except
            on E: Exception do
            begin
                for i := pPos to FieldCount - 1 do
                    data := data + Fields[i].FullName + ' = ' + Fields[i].AsString + '; ';
                ErrMsg('Error: ' + e.Message + #13 + 'Data: ' + data);
                Cancel;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;

    NodeName := isStrReplace(pField.FieldName,' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := node.ValueAsString;
    ftWideString, ftWideMemo:
        pField.AsString := node.ValueAsUnicodeString;
    ftBoolean:
        pField.AsBoolean := node.ValueAsBool;
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := node.ValueAsInteger;
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := node.ValueAsFloat;
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(node.ValueAsString);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.XmlNodeToField2(const ParentNode: TXmlNode; pField: TField);
var
    b: Boolean;
    NodeName: String;
	node: TXmlNode;
    d: TDateTime;
    data: string;
begin
    if (not pField.Visible) or pField.ReadOnly or (not (pField.FieldKind = fkData)) then
        Exit;
    NodeName := isStrReplace(pField.FieldName, ' ', '_');
	node := ParentNode.NodeByName(NodeName);
    if node = Nil then
    	Exit;

    b := True;
//    data := isDecode64(node.ValueAsUnicodeString);
    data := (node.ValueAsUnicodeString);
    case pField.DataType of
    ftString, ftGuid, ftMemo:
        pField.AsAnsiString := data;
    ftWideString, ftWideMemo:
        pField.AsString := data;
    ftBoolean:
        pField.AsBoolean := StrToBoolDef(data, False);
    ftInteger, ftAutoInc, ftLargeint:
    	try
	        pField.AsInteger := StrToIntDef(data, 0)
        except
	        pField.Clear;
            b := False;
        end;
    ftFloat:
    	try
	        pField.AsFloat := StrToFloatDef(data, 0.0);
        except
	        pField.Clear;
            b := False;
        end;
    ftDateTime, ftDate, ftTime:
        begin
        	if node.ValueAsString = '' then
		        pField.Clear
            else
            begin
				try
                	d := StrToDateTime(data);
                    if d <= 0 then
				        pField.Clear
                	else
	                    pField.AsDateTime := d;
		        except
			        pField.Clear;
	        	end;
            end;
        end;
    end;

    if not b then
    	ErrMsg(Format('Lỗi lấy dữ liệu Xml (node: %s, field: %s).',
        	[NodeName, pField.FieldName]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: Integer);
var
    lsTB: TStrings;
    i, j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
    bFull: Boolean;
    sKey: string;
begin
    lsTB := TStringList.Create;
    LsNode := TXmlNodeList.Create;
    ListTables(pTable, lsTB);

    StartProgress(lsTB.Count, 'Import');

    //No check constraint
    Conn.Execute(
        'alter table SYS_RIGHT nocheck constraint all'#13 +
        'alter table SYS_RIGHT_REP nocheck constraint all');

    // Import
    for i := 0 to lsTB.Count - 1 do
    begin
        SetProgressDesc(lsTB[i]);
        with QrSql do
        begin
            SQL.Text := SELECT_SQL + lsTB[i];
            Open;
        end;
        node := pXml.Root.FindNode(lsTB[i]);
        if node <> nil then
        begin
            bFull := StrToBoolDef(node.AttributeByName['Full'], False);
            if SameText(lsTB[i], 'SYS_CONFIG') then
            begin
                if bFull then
                    with QrSql do
                    begin
                        node := pXml.Root.NodeByName(lsTB[i]).FindNode('ROW').FindNode('FLEXIBLE');
                        if node <> nil then
                        begin
                            Edit;
//                            FieldByName('FLEXIBLE').Value := isDecode64(node.ValueAsString);
                            FieldByName('FLEXIBLE').Value := (node.ValueAsString);
                            Post;
                        end;
                        Close;
                    end
                else
                    Continue;
            end
            else
            begin
                if bFull then
                begin
                    Conn.Execute('delete ' + lsTB[i]);
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, 0);
                        end;
                    end;
                end else
                begin
                    sKey := node.AttributeByName['Key'];
                    with QrSql do
                    begin
                        pXml.Root.NodeByName(lsTB[i]).FindNodes('ROW', LsNode);
                        for j := 0 to LsNode.Count - 1 do
                        begin
                            SetProgressDesc(lsTB[i] + ': Row: ' + IntToStr(j));
                            xmlNodeToDs(LsNode.Items[j], QrSql, sKey, 0);
                        end;
                    end;
                end;
            end;
        end;
    end;

    // Check constraint
    Conn.Execute(
        'alter table SYS_RIGHT check constraint all'#13 +
        'alter table SYS_RIGHT_REP check constraint all');

    // CLean up
    Conn.Execute('delete SYS_RIGHT where FUNC_CODE not in (select FUNC_CODE from SYS_FUNC)');
    Conn.Execute('delete SYS_RIGHT_REP where REP_CODE not in (select REP_CODE from SYS_REPORT)');

    lsTB.Free;
    pXml.Free;
	StopProgress;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataMain.ImportDataXml(pXml: TNativeXml; pTable: string);
var
    j: Integer;
    node: TXmlNode;
    LsNode: TXmlNodeList;
begin
    LsNode := TXmlNodeList.Create;
    // Import
    with QrSql do
    begin
        SQL.Text := SELECT_SQL + pTable;
        Open;
    end;

    node := pXml.Root.FindNode(pTable);
    if node <> nil then
    begin
        Conn.Execute('delete ' + pTable);
        with QrSql do
        begin
            pXml.Root.NodeByName(pTable).FindNodes('ROW', LsNode);
            for j := 0 to LsNode.Count - 1 do
                xmlNodeToDs(LsNode.Items[j], QrSql, 0);
        end;
    end;
    LsNode.Free;
end;

end.
