object FrmPayNet: TFrmPayNet
  Left = 239
  Top = 252
  BorderIcons = [biSystemMenu]
  Caption = 'G'#7917'i B'#7843'ng L'#432#417'ng Qua Ng'#226'n H'#224'ng'
  ClientHeight = 351
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 39
    Width = 778
    Height = 314
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object RemuWorking: TwwDataInspector
      Left = 2
      Top = 2
      Width = 774
      Height = 310
      DisableThemes = False
      Align = alClient
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      DataSource = DsList
      Items = <
        item
          DataSource = DsTemp
          DataField = 'LK_PNET_EMP_TYPE'
          Caption = ' Lo'#7841'i nh'#226'n vi'#234'n'
          CustomControl = CbEmpType2
          WordWrap = False
        end
        item
          DataSource = DsTemp
          DataField = 'PNET_BANK_CODE'
          Caption = ' T'#224'i kho'#7843'n thanh to'#225'n'
          CustomControl = CbBank
          WordWrap = False
        end
        item
          DataSource = DsParam
          DataField = 'PAYNET_DATE'
          Caption = ' Ng'#224'y '#273#227' g'#7917'i'
          ReadOnly = True
          WordWrap = False
        end
        item
          DataSource = DsParam
          DataField = 'PAYNET_VALUE'
          Caption = ' Ng'#224'y Duy'#7879't'
          WordWrap = False
        end
        item
          DataSource = DsList
          DataField = 'NO_OF_TRANS'
          Caption = ' S'#7889' l'#432#7907'ng chuy'#7875'n'
          WordWrap = False
        end
        item
          DataSource = DsList
          DataField = 'PNET_PAY'
          Caption = ' T'#7893'ng s'#7889' ti'#7873'n'
          WordWrap = False
        end
        item
          DataSource = DsList
          Caption = 'Th'#244'ng tin t'#224'i kho'#7843'n ng'#226'n h'#224'ng'
          Expanded = True
          Items = <
            item
              DataSource = DsList
              DataField = 'PNET_COMPANY_NAME'
              Caption = 'Company name'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_ADDRESS_1'
              Caption = 'Address 1'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_ADDRESS_2'
              Caption = 'Address 2'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_ABC_ID'
              Caption = 'ABC ID'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_CUSTOMER_ID'
              Caption = 'Customer ID'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_ACCOUNT'
              Caption = 'Sender'#39's Account'
              WordWrap = False
            end
            item
              DataSource = DsList
              DataField = 'PNET_PAYMENT_SET'
              Caption = 'Payment Set'
              WordWrap = False
            end
            item
              DataSource = DsTemp
              DataField = 'PNET_REFERENCE'
              Caption = 'Reference'
              WordWrap = False
            end>
          WordWrap = False
        end>
      DefaultRowHeight = 20
      CaptionWidth = 208
      Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
      PaintOptions.AlternatingRowColor = 15794175
      PaintOptions.BackgroundOptions = [coFillDataCells]
      CaptionFont.Charset = DEFAULT_CHARSET
      CaptionFont.Color = clBlack
      CaptionFont.Height = -13
      CaptionFont.Name = 'Tahoma'
      CaptionFont.Style = []
      LineStyleCaption = ovDottedLine
      LineStyleData = ovDottedLine
      object CbEmpType2: TwwDBLookupCombo
        Tag = 999
        Left = 536
        Top = 80
        Width = 560
        Height = 19
        TabStop = False
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'PNET_EMP_TYPE_NAME'#9'25'#9'PNET_EMP_TYPE_NAME'#9'F')
        DataField = 'LK_PNET_EMP_TYPE'
        DataSource = DsTemp
        LookupTable = DataMain.QrPNET_EMP_TYPE
        LookupField = 'PNET_EMP_TYPE'
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = False
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = True
        AllowClearKey = False
        ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
      end
      object CbBank: TwwDBLookupCombo
        Tag = 999
        Left = 326
        Top = 169
        Width = 560
        Height = 19
        TabStop = False
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Bank Code'#9'10'#9'Bank Code'#9'F'
          'PNET_COMPANY_NAME'#9'30'#9'PNET_COMPANY_NAME'#9'F'
          'PNET_PAYMENT_SET'#9'10'#9'PNET_PAYMENT_SET'#9'F')
        DataField = 'PNET_BANK_CODE'
        DataSource = DsTemp
        LookupTable = DataMain.QrBank2
        LookupField = 'Bank Code'
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Visible = False
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 784
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    DisabledImages = DataMain.ImageMed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageMed
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    Transparent = True
    Wrapable = False
    object ToolBtnRefresh: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdFillData
      ImageIndex = 9
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton1: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton4: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PS_PAYNET_MASTER: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterOpen = PS_PAYNET_MASTERAfterOpen
    CommandTimeout = 60
    ProcedureName = 'PS_PAYNET_MASTER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@YEAR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MONTH'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAYMENT_TYPE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EMP_TYPE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@BANK_CODE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@REPERENCE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 200
        Value = Null
      end>
    Left = 128
    Top = 72
  end
  object DsList: TDataSource
    AutoEdit = False
    DataSet = PS_PAYNET_MASTER
    Left = 128
    Top = 104
  end
  object PS_PAYNET_DETAIL: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandTimeout = 60
    ProcedureName = 'PS_PAYNET_DETAIL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@YEAR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MON'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAYMENT_TYPE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 200
        Value = Null
      end
      item
        Name = '@EMP_TYPE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@BANK_CODE'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end>
    Left = 168
    Top = 72
  end
  object ActionList1: TActionList
    Left = 152
    Top = 160
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdFillData: TAction
      Caption = 'Export'
      Hint = 'Xu'#7845't d'#7919' li'#7879'u'
      ShortCut = 114
      OnExecute = CmdFillDataExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      OnExecute = CmdPrintExecute
    end
  end
  object QrParam: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    MarshalOptions = moMarshalModifiedOnly
    Parameters = <
      item
        Name = 'Year'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Month'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select PAYNET_VALUE, PAYNET_DATE'
      'from PAYROLL_PERIOD a '
      '  where a.[Year] = :Year and a.[Month] = :Month')
    Left = 208
    Top = 72
    object QrParamPAYNET_VALUE: TDateTimeField
      DisplayLabel = 'Ng'#224'y duy'#7879't'
      FieldName = 'PAYNET_VALUE'
    end
    object QrParamPAYNET_DATE: TDateTimeField
      DisplayLabel = 'Ng'#224'y g'#7917'i'
      FieldName = 'PAYNET_DATE'
    end
  end
  object DsParam: TDataSource
    DataSet = QrParam
    Left = 208
    Top = 104
  end
  object QrTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'PNET_EMP_TYPE'
        DataType = ftWideString
        Size = 5
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 0
    LocaleID = 0
    Left = 520
    Top = 56
    object QrTempPNET_EMP_TYPE: TWideStringField
      FieldName = 'PNET_EMP_TYPE'
      OnChange = QrTempPNET_EMP_TYPEChange
      Size = 5
    end
    object QrTempLK_PNET_EMP_TYPE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PNET_EMP_TYPE'
      LookupDataSet = DataMain.QrPNET_EMP_TYPE
      LookupKeyFields = 'PNET_EMP_TYPE'
      LookupResultField = 'PNET_EMP_TYPE_NAME'
      KeyFields = 'PNET_EMP_TYPE'
      Size = 200
      Lookup = True
    end
    object QrTempPNET_BANK_CODE: TWideStringField
      FieldName = 'PNET_BANK_CODE'
      OnChange = QrTempPNET_EMP_TYPEChange
      Size = 15
    end
    object QrTempPNET_REFERENCE: TWideStringField
      FieldName = 'PNET_REFERENCE'
    end
  end
  object DsTemp: TDataSource
    DataSet = QrTemp
    Left = 520
    Top = 88
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.txt'
    Filter = 'Text document (*.txt)|*.txt'
    Left = 436
    Top = 65
  end
end
