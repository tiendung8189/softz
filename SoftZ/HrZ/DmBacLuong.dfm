object FrmDmBacLuong: TFrmDmBacLuong
  Left = 169
  Top = 135
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c B'#7853'c L'#432#417'ng'
  ClientHeight = 583
  ClientWidth = 871
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 562
    Width = 871
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitTop = 315
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 871
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 36
    Width = 871
    Height = 526
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitHeight = 279
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 871
      Height = 526
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'DONG_BH;CheckBox;True;False'
        'TINH_THUE;CheckBox;True;False'
        'LOAIPC;CustomEdit;CbType;F'
        'LK_TEN_NHOMPC;CustomEdit;CbNhom;F')
      PictureMasks.Strings = (
        'MAPC'#9'*!'#9'T'#9'T')
      Selected.Strings = (
        'Bac1'#9'10'#9'Bac1'#9#9
        'Bac2'#9'10'#9'Bac2'#9#9
        'Bac3'#9'10'#9'Bac3'#9#9
        'Bac4'#9'10'#9'Bac4'#9#9
        'Bac5'#9'10'#9'Bac5'#9#9
        'Bac6'#9'10'#9'Bac6'#9#9
        'Bac7'#9'10'#9'Bac7'#9#9
        'Bac8'#9'10'#9'Bac8'#9#9
        'Bac9'#9'10'#9'Bac9'#9#9
        'Bac10'#9'10'#9'Bac10'#9#9
        'Bac11'#9'10'#9'Bac11'#9#9
        'Bac12'#9'10'#9'Bac12'#9#9
        'Bac13'#9'10'#9'Bac13'#9#9
        'Bac14'#9'10'#9'Bac14'#9#9
        'Bac15'#9'10'#9'Bac15'#9#9
        'Bac16'#9'10'#9'Bac16'#9#9
        'CREATE_BY'#9'10'#9'CREATE_BY'#9#9
        'UPDATE_BY'#9'10'#9'UPDATE_BY'#9#9
        'DELETE_BY'#9'10'#9'DELETE_BY'#9#9
        'CREATE_DATE'#9'18'#9'CREATE_DATE'#9#9
        'UPDATE_DATE'#9'18'#9'UPDATE_DATE'#9#9
        'DELETE_DATE'#9'18'#9'DELETE_DATE'#9#9
        'ROWGUID'#9'38'#9'ROWGUID'#9#9)
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = DsDanhmuc
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopCommon
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      ExplicitHeight = 279
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 84
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDanhmucBeforeInsert
    BeforeEdit = QrDanhmucBeforeEdit
    BeforePost = QrDanhmucBeforePost
    BeforeDelete = QrDanhmucBeforeDelete
    OnDeleteError = QrDanhmucPostError
    OnPostError = QrDanhmucPostError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_BACLUONG'
      'order by'#9'MaBacLuong')
    Left = 20
    Top = 168
    object QrDanhmucMaBacLuong: TWideStringField
      FieldName = 'MaBacLuong'
    end
    object QrDanhmucTenBacLuong: TWideStringField
      FieldName = 'TenBacLuong'
      Size = 200
    end
    object QrDanhmucTenBacLuong_TA: TWideStringField
      FieldName = 'TenBacLuong_TA'
      Size = 200
    end
    object QrDanhmucBac1: TFloatField
      FieldName = 'Bac1'
    end
    object QrDanhmucBac2: TFloatField
      FieldName = 'Bac2'
    end
    object QrDanhmucBac3: TFloatField
      FieldName = 'Bac3'
    end
    object QrDanhmucBac4: TFloatField
      FieldName = 'Bac4'
    end
    object QrDanhmucBac5: TFloatField
      FieldName = 'Bac5'
    end
    object QrDanhmucBac6: TFloatField
      FieldName = 'Bac6'
    end
    object QrDanhmucBac7: TFloatField
      FieldName = 'Bac7'
    end
    object QrDanhmucBac8: TFloatField
      FieldName = 'Bac8'
    end
    object QrDanhmucBac9: TFloatField
      FieldName = 'Bac9'
    end
    object QrDanhmucBac10: TFloatField
      FieldName = 'Bac10'
    end
    object QrDanhmucBac11: TFloatField
      FieldName = 'Bac11'
    end
    object QrDanhmucBac12: TFloatField
      FieldName = 'Bac12'
    end
    object QrDanhmucBac13: TFloatField
      FieldName = 'Bac13'
    end
    object QrDanhmucBac14: TFloatField
      FieldName = 'Bac14'
    end
    object QrDanhmucBac15: TFloatField
      FieldName = 'Bac15'
    end
    object QrDanhmucBac16: TFloatField
      FieldName = 'Bac16'
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDanhmucDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDanhmucDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 20
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 52
    Top = 168
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 116
    Top = 168
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
  end
end
