﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyTangCa;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, isPanel, Vcl.Buttons, DBCtrlsEh, rImprovedComps, rHTMLLabel,
  rDBComponents, DBGridEh, DBLookupEh, RzPanel, RzSplit, rDBCtrlGridEx,
  DbLookupComboboxEh2, frameEmp, System.DateUtils;

type
  TFrmDangkyTangCa = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton2: TToolButton;
    SepImport: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    btnEdit: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    QrMasterLK_TENNV: TWideStringField;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdNewList: TAction;
    ToolButton13: TToolButton;
    ToolButton15: TToolButton;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    PaInfo: TisPanel;
    QrShift: TADOQuery;
    QrMasterLK_GIOVAO: TDateTimeField;
    QrMasterLK_GIORA: TDateTimeField;
    QrMasterLK_GIOVAO1: TDateTimeField;
    QrMasterLK_GIORA1: TDateTimeField;
    QrMasterLK_SOGIO: TFloatField;
    QrMasterLK_XacNhan_Tennv: TWideStringField;
    spHR_DANGKY_TANGCA_Invalid: TADOCommand;
    CmdImportExcel: TAction;
    QrCsv: TADOQuery;
    BtnImport: TToolButton;
    ToolButton18: TToolButton;
    QrMasterXacNhan_TinhTrang: TIntegerField;
    QrMasterXacNhan_Manv: TWideStringField;
    QrMasterXacNhan_GhiChu: TWideStringField;
    QrMasterXetDuyet_TinhTrang: TIntegerField;
    QrMasterXetDuyet_Manv: TWideStringField;
    QrMasterXetDuyet_GhiChu: TWideStringField;
    QrMasterManv: TWideStringField;
    QrMasterMaCa: TWideStringField;
    PNTinhTrang: TisPanel;
    PNTinhTrang_XacNhan: TisPanel;
    QrMasterLK_XetDuyet_Tennv: TWideStringField;
    QrTinhTrang_XacNhan: TADOQuery;
    QrMasterLK_XetDuyet_TenTinhTrang: TWideStringField;
    QrMasterLK_XacNhan_TenTinhTrang: TWideStringField;
    wwDBEdit7: TDBEditEh;
    QrMasterNgay: TDateTimeField;
    QrMasterXetDuyet_TinhTrangBy: TIntegerField;
    QrMasterXetDuyet_Ngay: TDateTimeField;
    QrMasterXacNhan_TinhTrangBy: TIntegerField;
    QrMasterXacNhan_Ngay: TDateTimeField;
    QrMasterCREATE_SOURCE: TWideStringField;
    QrMasterCREATE_NAME: TWideStringField;
    QrMasterCREATE_SOURCE_NAME: TWideStringField;
    CbbXetDuyet_TinhTrang: TDbLookupComboboxEh2;
    RzSizePanel1: TRzSizePanel;
    rScrollBoxEx1: TrScrollBoxEx;
    QrMasterLK_XetDuyet_TinhTrangFullName: TWideStringField;
    QrMasterLK_XacNhan_TinhTrangFullName: TWideStringField;
    QrMasterLK_XetDuyet_Manv: TWideStringField;
    QrMasterLK_XacNhan_Manv: TWideStringField;
    DBEditEh1: TDBEditEh;
    CbXacNhan_TinhTrang: TDbLookupComboboxEh2;
    QrMasterLK_MaNhomLV: TWideStringField;
    QrMasterMaNhomLV: TWideStringField;
    QrMasterCREATE_FUNC: TWideStringField;
    Panel1: TPanel;
    Panel3: TPanel;
    DsEmp: TDataSource;
    CbXetDuyet: TDbLookupComboboxEh2;
    EdXetDuyetManvQL: TDBEditEh;
    CbXacNhan: TDbLookupComboboxEh2;
    EdXacNhanManvQL: TDBEditEh;
    QrMasterGhiChu: TWideMemoField;
    gbNhanVien: TGroupBox;
    QrTinhTrang_XetDuyet: TADOQuery;
    DsTinhTrang_XacNhan: TDataSource;
    DsTinhTrang_XetDuyet: TDataSource;
    rgApproved: TRadioGroup;
    CmdLyDoVangMat: TAction;
    PNGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrEmpStatus: TADOQuery;
    DsEmpStatus: TDataSource;
    QrMasterLK_ManvQL: TWideStringField;
    QrMasterLK_XacNhan_ManvQL: TWideStringField;
    QrMasterLK_XetDuyet_ManvQL: TWideStringField;
    N3: TMenuItem;
    PopTinhTrangXacNhan: TMenuItem;
    PopTinhTrangXacNhan1: TMenuItem;
    PopTinhTrangXacNhan2: TMenuItem;
    PopTinhTrangXacNhan3: TMenuItem;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    QrMasterDangKyTuGio: TDateTimeField;
    QrMasterDangKyDenGio: TDateTimeField;
    QrMasterDangKySoGio: TFloatField;
    QrDays: TADOQuery;
    frEmp1: TfrEmp;
    QrMasterGioVao_Ca: TDateTimeField;
    QrMasterGioRa_Ca: TDateTimeField;
    QrMasterGioVao1_Ca: TDateTimeField;
    QrMasterGioRa1_Ca: TDateTimeField;
    QrMasterSoGio_Ca: TFloatField;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    Label4: TLabel;
    Label5: TLabel;
    BtnConfirmAccept: TBitBtn;
    BtnConfirmCancel: TBitBtn;
    CmdConfirmAccept: TAction;
    CmdConfirmCancel: TAction;
    CmdApproveAccept: TAction;
    CmdApproveCancel: TAction;
    BtnApproveAccept: TBitBtn;
    BtnApproveCancel: TBitBtn;
    PaNhanVien: TPanel;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    Panel2: TPanel;
    CbCaLamViec: TDbLookupComboboxEh2;
    Label13: TLabel;
    dpGioVao: TwwDBDateTimePicker;
    Label2: TLabel;
    dpGioRa1: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    Label1: TLabel;
    dpDangKyTuGio: TwwDBDateTimePicker;
    Label3: TLabel;
    dpDangKyDenGio: TwwDBDateTimePicker;
    DBNumberEditEh4: TDBNumberEditEh;
    QrMasterXetDuyet_SoGioHuongLuong: TFloatField;
    QrMasterXetDuyet_SoGioNghiBu: TFloatField;
    Label6: TLabel;
    DBNumberEditEh3: TDBNumberEditEh;
    Label7: TLabel;
    PopImport: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    CmdSampleExcel: TAction;
    spIMP_HR_DANGKY_TANGCA_DeleteList: TADOCommand;
    DBNumberEditEh2: TDBNumberEditEh;
    N4: TMenuItem;
    PopTangCaTatCa: TMenuItem;
    PopTangCaHuongLuong: TMenuItem;
    PopTangCaHuongNghiBu: TMenuItem;
    QrMasterIDX: TAutoIncField;
    QrMasterIDX_FUNC: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrMasterMANVValidate(Sender: TField);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrEmpBeforeOpen(DataSet: TDataSet);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure QrMasterMANVChange(Sender: TField);
    procedure QrMasterMACAChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure PopCommonPopup(Sender: TObject);
    procedure PopTinhTrangXacNhan1Click(Sender: TObject);
    procedure QrMasterXacNhan_ManvChange(Sender: TField);
    procedure QrMasterXetDuyet_ManvChange(Sender: TField);
    procedure QrMasterDangKyTuGioChange(Sender: TField);
    procedure CmdConfirmAcceptExecute(Sender: TObject);
    procedure CmdConfirmCancelExecute(Sender: TObject);
    procedure CmdApproveAcceptExecute(Sender: TObject);
    procedure CmdApproveCancelExecute(Sender: TObject);
    procedure QrMasterDangKySoGioChange(Sender: TField);
    procedure CmdNewListExecute(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure CmdSampleExcelExecute(Sender: TObject);
    procedure QrMasterXetDuyet_SoGioHuongLuongChange(Sender: TField);
    procedure PopTangCaHuongLuongClick(Sender: TObject);
  private
    r: WORD;
  	mCanEdit, mShowMsg, isFunctionConfirm, isFunctionApproved: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, fFilterConfirm, mFilterConfirm, mFilterApproved, fFilterCompensatory, mFilterCompensatory: Integer;
    mSQL, fStr, mManv, mManvQL, mName, mFunc: String;
    mIdxFunc: Integer;
    function calcOvertimes: Boolean; overload;
    function calcOvertimes(pEmpID, pSession: String; pDate, pDateFrom, pDateTo: TDateTime): Double; overload;
    function overTimeDateValid(pEmpID: String; pDate, pDateFrom, pDateTo: TDateTime;
		pSession: String; pIDX: Integer): Boolean;
    procedure XetDuyetChange(pTinhtrang: Integer; pShowMsg: Boolean = True);
    procedure XacNhanChange(pTinhtrang: Integer; pShowMsg: Boolean = True);
    function DeleteAllRecordByTransNo(pTransNo: Integer; pIsError: Boolean = false): Boolean;
  public
  	procedure Execute(r: WORD; pIdxFunc: Integer; pManv: string = ''; pFunc : string = ''; pManvQL: string = ''; pName: string = '');
  end;

var
  FrmDangkyTangCa: TFrmDangkyTangCa;

const
    TABLE_NAME = 'HR_DANGKY_TANGCA';
    FORM_CODE = 'HR_DANGKY_TANGCA';
    REPORT_NAME = 'HR_RP_DANGKY_TANGCA';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, isStr, GuidEx, RepEngine,
    isCommon, isFile, ImportExcel, OfficeData, DmVangmat, Phepnam,
    DangkyTangCaTools, DangkyTangCaDanhSach, ReceiptDesc,
    ImportExcelToTable, HrData, HrExCommon; // MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := pManv;
    mManvQL := pManvQL;
    mName := pName;
    mFunc := pFunc;
    mIdxFunc := pIdxFunc;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.FormCreate(Sender: TObject);
var
    bXacNhan: Boolean;
begin
	TMyForm(Self).Init2;
    AddAllFields(QrMaster, TABLE_NAME);

    BtnImport.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    SepImport.Visible := BtnImport.Visible;

    isFunctionConfirm := GetFuncState('HR_TANGCA_XACNHAN');
    isFunctionApproved :=  GetFuncState('HR_TANGCA_DUYET');

    PNTinhTrang_XacNhan.Visible := isFunctionConfirm;
    if isFunctionConfirm then
        PNTinhTrang_XacNhan.Enabled := GetRights('HR_TANGCA_XACNHAN', False) <> R_DENY;

    PNTinhTrang.Visible := isFunctionApproved;
    if isFunctionApproved then
        PNTinhTrang.Enabled := GetRights('HR_TANGCA_DUYET', False) <> R_DENY;

    mFilterCompensatory := -1;
    mFilterConfirm:= -1;
    mFilterApproved := -1;
    mShowMsg := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);

    with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_CALAMVIEC, QrLOAI_CANGHI]);

    OpenDataSets([QrTinhTrang_XacNhan, QrTinhTrang_XetDuyet]);


    SetDisplayFormat(QrMaster, ctCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['GioVao_Ca', 'GioRa_Ca', 'GioVao1_Ca', 'GioRa1_Ca', 'DangKyTuGio', 'DangKyDenGio'], sysMinFmt);
    SetDisplayFormat(QrMaster, ['DangKySoGio', 'XetDuyet_SoGioHuongLuong', 'XetDuyet_SoGioNghiBu'], sysFloatFmtTwo);
    SetDisplayFormat(QrMaster, ['XacNhan_Ngay', 'XetDuyet_Ngay'], DateTimeFmt);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    if not isFunctionConfirm then
        grRemoveFields(GrList, ['XacNhan_Manv', 'LK_XacNhan_ManvQL', 'LK_XacNhan_Tennv',
            'LK_XacNhan_Manv', 'LK_XacNhan_TinhTrangFullName' ,'XacNhan_TinhTrang',
            'LK_XacNhan_TenTinhTrang', 'XacNhan_TinhTrangBy', 'XacNhan_Ngay', 'XacNhan_GhiChu']);

    if not isFunctionApproved then
        grRemoveFields(GrList, ['XetDuyet_Manv', 'LK_XetDuyet_ManvQL', 'LK_XetDuyet_Tennv',
            'LK_XetDuyet_Manv', 'LK_XetDuyet_TinhTrangFullName',  'XetDuyet_TinhTrang',
            'LK_XetDuyet_TenTinhTrang', 'XetDuyet_TinhTrangBy', 'XetDuyet_Ngay', 'XetDuyet_GhiChu']);

    lookupComboboxEhShowImages(QrTinhTrang_XacNhan, CbXacNhan_TinhTrang, 'TEN_HOTRO', 0);
    lookupComboboxEhShowImages(QrTinhTrang_XetDuyet, CbbXetDuyet_TinhTrang, 'TEN_HOTRO', 0);

    fFilterConfirm := 3;
    fFilterCompensatory := 0;
    if mManv <> '' then
    begin
        frD2D.Visible := False;
//        Panel3.Visible := False;
//        PopTinhTrangXacNhan.Visible := False;
//        PopTinhTrangXacNhan1.Visible := False;
//        PopTinhTrangXacNhan2.Visible := False;
//        PopTinhTrangXacNhan3.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mManv, mManvQL, mName);
        rgApproved.ItemIndex := 3;

        if (mFunc = 'HR_BANGCONG') or (mFunc = 'HR_NGHIBU') then
        begin
            if mFunc = 'HR_BANGCONG' then
                fFilterCompensatory := 1;

            if mFunc = 'HR_NGHIBU' then
                fFilterCompensatory := 2;
        end;


        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        gbNhanVien.Height := gbNhanVien.Height - PaNhanVien.Height;
        PaInfo.Height := PaInfo.Height - PaNhanVien.Height;
    end;

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

         if mManv <> '' then
            SQL.Add(' and [Manv]=''' + mManv + '''');

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrEmpStatus do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        if mManv <> '' then
             Add(' and [Manv]=''' + mManv + '''')
        else
        begin
           Add(' and ([Ngay] between :Tungay and :Denngay)' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

         if mIdxFunc > 0 then
            Add(Format(' and IDX_FUNC=%d ', [mIdxFunc] ));

        Add('order by [Ngay], Manv');

        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrTinhTrang_XacNhan, QrTinhTrang_XetDuyet, QrEmp, QrEmpStatus]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    if mManv <> '' then
        CbCaLamViec.SetFocus
    else
        CbNhanVien.SetFocus;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdNewListExecute(Sender: TObject);
var
    mTransNo: Integer;
begin
    mTransNo := 0;
    Application.CreateForm(TFrmDangkyTangCaTools, FrmDangkyTangCaTools);
    if not FrmDangkyTangCaTools.Execute(r, FORM_CODE, mTransNo) then
        Exit;

    QrMaster.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if mManv <> '' then
        CbCaLamViec.SetFocus
    else
        CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdSampleExcelExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_HR_DANGKY_TANGCA_EXCEL');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdApproveAcceptExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_TANGCA_DUYET');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_APPROVE, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            if FieldByName('XacNhan_TinhTrang').AsInteger = 0 then
            begin
                FieldByName('XacNhan_TinhTrang').AsInteger := 1;
                FieldByName('XacNhan_GhiChu').AsString := 'Xác nhận bởi "Người xét duyệt (BOD)"';
                FieldByName('XacNhan_Ngay').AsDateTime := Now;
                FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
//                if FieldByName('XacNhan_Manv').AsString = '' then
//                    FieldByName('XacNhan_Manv').AsString := sysLogonManv;
            end;
            FieldByName('XetDuyet_TinhTrang').AsInteger := 1;
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XetDuyet_Manv').AsString = '' then
//               FieldByName('XetDuyet_Manv').AsString := sysLogonManv;
            Post;
        end;
    end;
end;

procedure TFrmDangkyTangCa.CmdApproveCancelExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_TANGCA_DUYET');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_CANCEL, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            if FieldByName('XacNhan_TinhTrang').AsInteger = 0 then
            begin
                FieldByName('XacNhan_TinhTrang').AsInteger := 2;
                FieldByName('XacNhan_GhiChu').AsString := 'Từ chối bởi "Người xét duyệt (BOD)"';
                FieldByName('XacNhan_Ngay').AsDateTime := Now;
                FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
//              if FieldByName('XacNhan_Manv').AsString = '' then
//                 FieldByName('XacNhan_Manv').AsString := sysLogonManv;
            end;

            FieldByName('XetDuyet_TinhTrang').AsInteger := 2;
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XetDuyet_Manv').AsString = '' then
//               FieldByName('XetDuyet_Manv').AsString := sysLogonManv;

            Post;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdDelExecute(Sender: TObject);
var
    pIdx: TGUID;
begin
    pIdx := TGuidField(QrMaster.FieldByName('Idx')).AsGuid;
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdConfirmAcceptExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_TANGCA_XACNHAN');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_CONFIRM, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            FieldByName('XacNhan_TinhTrang').AsInteger := 1;
            FieldByName('XacNhan_Ngay').AsDateTime := Now;
            FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XacNhan_Manv').AsString = '' then
//               FieldByName('XacNhan_Manv').AsString := sysLogonManv;
            Post;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdConfirmCancelExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_TANGCA_XACNHAN');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_CANCEL, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            if FieldByName('XetDuyet_TinhTrang').AsInteger = 0 then
            begin
                FieldByName('XetDuyet_TinhTrang').AsInteger := 2;
                FieldByName('XetDuyet_GhiChu').AsString := 'Từ chối bởi "Người quản lý"';
                FieldByName('XetDuyet_Ngay').AsDateTime := Now;
                FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
//                if FieldByName('XetDuyet_Manv').AsString = '' then
//                    FieldByName('XetDuyet_Manv').AsString := sysLogonManv;
            end;
            FieldByName('XacNhan_TinhTrang').AsInteger := 2;
            FieldByName('XacNhan_Ngay').AsDateTime := Now;
            FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XacNhan_Manv').AsString = '' then
//               FieldByName('XacNhan_Manv').AsString := sysLogonManv;

            Post;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        rgApproved.ItemIndex, //0: Chờ xét duyệt; 1: Đã xét duyệt; 2: Từ chối xét duyệt; 3: Tất cả
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdRefreshExecute(Sender: TObject);
var
    sSQL, sWhere, s: String;
    fFilterApproved : Integer;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

    fFilterApproved := rgApproved.ItemIndex;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) or
       (fFilterConfirm <> mFilterConfirm) or
       (fFilterApproved <> mFilterApproved) or
       (fFilterCompensatory <> mFilterCompensatory) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;
        mFilterConfirm := fFilterConfirm;
        mFilterApproved := fFilterApproved;
        mFilterCompensatory := fFilterCompensatory;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;
            sWhere := '';
            if mFilterConfirm <= 2 then
                sWhere := sWhere + ' and isnull(XacNhan_TinhTrang, 0) = '+ IntToStr(mFilterConfirm);

            if mFilterApproved <= 2 then
                sWhere := sWhere + ' and isnull(XetDuyet_TinhTrang, 0) = '+ IntToStr(mFilterApproved);

            if mFilterCompensatory  = 1 then
                sWhere := sWhere + ' and isnull(XetDuyet_SoGioHuongLuong, 0) > 0';

            if mFilterCompensatory  = 2 then
                sWhere := sWhere + ' and isnull(XetDuyet_SoGioNghiBu, 0) > 0';

            if mManv <> '' then
            begin
                SQL.Text := Format(mSQL, [sWhere]);
            end
            else
            begin
                sSQL := '1=1';
                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
    //                3:
    //                    sSQL := '[Group ID]=''' + fStr + '''';
                    end;


                SQL.Text := Format(mSQL, [sWhere, sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'Ngay';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdImportExcelExecute(Sender: TObject);
var
    mTransNo: Integer;
    mTableNameImport: String;
begin
    mTableNameImport := 'IMP_HR_DANGKY_TANGCA';
    mTransNo := DataMain.GetSeqValue(mTableNameImport);
    if not FrmImportExcelToTable.ExcelImportToTable(mTableNameImport, mTableNameImport, 'spIMP_HR_DANGKY_TANGCA_ImportListExcel;1',
    [mTransNo, FORM_CODE + '_EXCEL', sysAppSource, mTransNo, sysLogonUID, sysLang, ''], mTransNo) then
    begin
        //xóa dữ liệu excel đã tự động insert
        DeleteAllRecordByTransNo(mTransNo);
        mTransNo := 0;
        Exit;
    end;

    if mTransNo <> 0 then
    begin
        Application.CreateForm(TFrmDangkyTangCaDanhSach, FrmDangkyTangCaDanhSach);
        if not FrmDangkyTangCaDanhSach.Execute(r, mTransNo) then
            Exit;

        QrMaster.Requery;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bTinhTrang, bTinhTrangXacNhan, bEdit: Boolean;
begin
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEdit := State in [dsEdit];
        bEmpty := IsEmpty;
        bTinhTrangXacNhan :=  FieldByName('XacNhan_TinhTrang').AsInteger <> 0;
        bTinhTrang :=  FieldByName('XetDuyet_TinhTrang').AsInteger <> 0;
    end;

    CmdNewList.Enabled := bBrowse and mCanEdit;
    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    BtnConfirmAccept.Enabled := bBrowse and (not bTinhTrangXacNhan) and  (not bEmpty);
    BtnConfirmCancel.Enabled := bBrowse and (not bTinhTrangXacNhan) and (not bEmpty);
    BtnApproveAccept.Enabled := bBrowse and (not bTinhTrang) and (not bEmpty);
    BtnApproveCancel.Enabled := bBrowse and (not bTinhTrang) and (not bEmpty);

    btnEdit.Enabled := (not bEmpty) and (not bTinhTrangXacNhan)  and (not bTinhTrang) and bBrowse;

    BtnImport.Enabled := bBrowse;
    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
    try
        if GetRights('HR_TANGCA_XACNHAN', False) <> R_DENY then
            PNTinhTrang_XacNhan.Enabled := not bTinhTrang;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVAILD_HOURS = 'Giờ tăng ca không hợp lệ.';
    RS_INVAILD_HOURS_START = 'Từ giờ tăng ca không hợp lệ.';
    RS_INVAILD_HOURS_END = 'Đến giờ tăng ca không hợp lệ.';
    RS_INVAILD_HOURS_APPROVED = 'Duyệt giờ tăng ca không hợp lệ.';
procedure TFrmDangkyTangCa.QrMasterBeforePost(DataSet: TDataSet);
var
	d: TDateTime;
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'MaCa','Ngay', 'XacNhan_Manv', 'XetDuyet_Manv']) then
            Abort;

        if (FieldByName('DangKyTuGio').IsNull) then
        begin
            ErrMsg(RS_INVAILD_HOURS_START);
            Abort;
        end;

        if (FieldByName('DangKyDenGio').IsNull) then
        begin
            ErrMsg(RS_INVAILD_HOURS_END);
            Abort;
        end;

    	d := FieldByName('Ngay').AsDateTime;
		if exEmpValid(FieldByName('Manv').AsString, FORM_CODE, d) <> 0 then
    	begin
	    	ErrMsg(RS_EMP_ID);
            CbNhanVien.SetFocus;
	        Abort
    	end;

        if (FieldByName('DangKyTuGio').AsDateTime > 0) and
            (FieldByName('DangKyDenGio').AsDateTime > 0) and
            (
                ((HourOf(FieldByName('DangKyDenGio').AsDateTime) <> 0) and  (FieldByName('DangKyDenGio').AsDateTime <= FieldByName('DangKyTuGio').AsDateTime))
                or
                ((HourOf(FieldByName('DangKyDenGio').AsDateTime) = 0) and  (IncDay(FieldByName('DangKyDenGio').AsDateTime) <= FieldByName('DangKyTuGio').AsDateTime))
            )
            then
        begin
            ErrMsg(RS_INVAILD_HOURS);
            dpDangKyTuGio.SetFocus;
            Abort;
        end;


        if not overTimeDateValid(FieldByName('Manv').AsString,
            FieldByName('Ngay').AsDateTime,
            FieldByName('DangKyTuGio').AsDateTime,
            FieldByName('DangKyDenGio').AsDateTime,
            FieldByName('MaCa').AsString,
            FieldByName('IDX').AsInteger) then
        begin
            if CbDate.Focused then
               CbDate.SetFocus
            else
                dpDangKyTuGio.SetFocus;
            Abort;
        end;

        if FieldByName('XetDuyet_SoGioNghiBu').AsFloat < 0 then
        begin
            ErrMsg(RS_INVAILD_HOURS_APPROVED);
            Abort;
        end;

//        if (FieldByName('XacNhan_TinhTrang').AsInteger <> 0)
//            and (FieldByName('XetDuyet_TinhTrang').AsInteger = 0)
//            and BlankConfirm(QrMaster, ['XacNhan_Manv']) then
//            Abort;
//
//        if (FieldByName('XetDuyet_TinhTrang').AsInteger <> 0)
//            and BlankConfirm(QrMaster, ['XetDuyet_Manv']) then
//            Abort;
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterDangKySoGioChange(Sender: TField);
begin
    with QrMaster do
    begin
        if FieldByName('XetDuyet_SoGioHuongLuong').AsFloat = 0 then
            FieldByName('XetDuyet_SoGioHuongLuong').AsFloat :=
                FieldByName('DangKySoGio').AsFloat;

        QrMasterXetDuyet_SoGioHuongLuongChange(Sender);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterDangKyTuGioChange(Sender: TField);
begin
    calcOvertimes;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrEmpBeforeOpen(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.BtnImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.XetDuyetChange(pTinhtrang: Integer; pShowMsg: Boolean);
begin
    with QrMaster do
    begin
        if pTinhtrang = 0 then
        begin
            FieldByName('XetDuyet_Ngay').Clear;
            FieldByName('XetDuyet_TinhTrangBy').Clear;
        end else
        begin
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;

            if pShowMsg then
            case pTinhtrang of
                1: Msg(RS_REG_STATUS1);
                2: Msg(RS_REG_STATUS2);
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Date;
    with QrMaster do
    begin
        FieldByName('Ngay').AsDateTime := d;

        if not isFunctionConfirm then
        begin
           FieldByName('XacNhan_TinhTrang').AsInteger := 1;
           FieldByName('XacNhan_Ngay').AsDateTime := Now;
           FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
        end
        else
        begin
           FieldByName('XacNhan_TinhTrang').AsInteger := 0;
        end;

        if not isFunctionApproved then
        begin
           FieldByName('XetDuyet_TinhTrang').AsInteger := 1;
           FieldByName('XetDuyet_Ngay').AsDateTime := Now;
           FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
        end
        else
        begin
           FieldByName('XetDuyet_TinhTrang').AsInteger := 0;
        end;

        FieldByName('CREATE_SOURCE').AsString := sysAppSource;
        if mManv <> '' then
        begin
            FieldByName('Manv').AsString := mManv;
            FieldByName('CREATE_FUNC').AsString := mFunc;
            FieldByName('IDX_FUNC').AsInteger := mIdxFunc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
    QrEmpStatus.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrList then
    	CbNhanVien.SetFocus
    else if (ActiveControl = PaInfo) or (ActiveControl.Parent = PaInfo) then
    	frD2D.EdFrom.SetFocus
    else
    	GrList.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterMANVValidate(Sender: TField);
var
    s: String;
begin
	s := Sender.AsString;
    if (s = '') or (IsDotSelect(s) <> 0) then
    	Exit;

	if exEmpValid(s, FORM_CODE) <> 0 then
    begin
    	ErrMsg(RS_EMP_ID);
        Abort
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDangkyTangCa.QrMasterMACAChange(Sender: TField);
begin
    with QrMaster do
    begin
        FieldByName('GioVao_Ca').AsDateTime := FieldByName('LK_GioVao').AsDateTime;
        FieldByName('GioRa_Ca').AsDateTime := FieldByName('LK_GioRa').AsDateTime;
        FieldByName('GioVao1_Ca').AsDateTime := FieldByName('LK_GioVao1').AsDateTime;
        FieldByName('GioRa1_Ca').AsDateTime := FieldByName('LK_GioRa1').AsDateTime;
        FieldByName('SoGio_Ca').AsFloat := FieldByName('LK_SoGio').AsFloat;

        if (FieldByName('MaCa').AsString = '') or (FieldByName('MaCa').IsNull) then
        begin
            FieldByName('GioVao_Ca').Clear;
            FieldByName('GioRa1_Ca').Clear;
        end;
    end;
    calcOvertimes;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterMANVChange(Sender: TField);
begin
    calcOvertimes;

    with QrMaster do
    begin
        FieldByName('XetDuyet_Manv').AsString := FieldByName('LK_XetDuyet_Manv').AsString;
        FieldByName('XacNhan_Manv').AsString := FieldByName('LK_XacNhan_Manv').AsString;
        FieldByName('MaNhomLV').AsString := FieldByName('LK_MaNhomLV').AsString;
        EdXacNhanManvQL.Text := EdXacNhanManvQL.Field.AsString;
        EdXetDuyetManvQL.Text := EdXetDuyetManvQL.Field.AsString;
        EdManvQL.Text := EdManvQL.Field.AsString;
    end;

    with QrShift do
    begin
        Close;
        Parameters[0].Value := Sender.AsString;
        Open;

        QrMaster.FieldByName('MaCa').AsString := FieldByName('MACA').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCa.overTimeDateValid;
var
    s: String;
begin
    with spHR_DANGKY_TANGCA_Invalid do
    begin
        Prepared := True;
        Parameters.ParamByName('@Idx').Value := pIDX;
        Parameters.ParamByName('@Manv').Value := pEmpID;
        Parameters.ParamByName('@Ngay').Value := pDate;
        Parameters.ParamByName('@DangKyTuGio').Value := pDateFrom;
        Parameters.ParamByName('@DangKyDenGio').Value := pDateTo;
        Parameters.ParamByName('@MaCa').Value := pSession;
        try
            Execute;
        except
        end;

        if Parameters.FindParam('@STR') <> nil then
            s := Parameters.ParamValues['@STR'];


        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.PopCommonPopup(Sender: TObject);
begin
    PopTinhTrangXacNhan.Checked := fFilterConfirm = 0;
    PopTinhTrangXacNhan1.Checked := fFilterConfirm = 1;
    PopTinhTrangXacNhan2.Checked := fFilterConfirm = 2;
    PopTinhTrangXacNhan3.Checked := fFilterConfirm = 3;

    PopTangCaHuongLuong.Checked := mFilterCompensatory = 1;
    PopTangCaHuongNghiBu.Checked := mFilterCompensatory = 2;
    PopTangCaTatCa.Checked := mFilterCompensatory = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.PopTangCaHuongLuongClick(Sender: TObject);
begin
    fFilterCompensatory :=  (Sender as TComponent).Tag;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.PopTinhTrangXacNhan1Click(Sender: TObject);
begin
    fFilterConfirm :=  (Sender as TComponent).Tag;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterXacNhan_ManvChange(Sender: TField);
begin
    EdXacNhanManvQL.Text := EdXacNhanManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 518;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.XacNhanChange(pTinhtrang: Integer; pShowMsg: Boolean);
begin
    with QrMaster do
    begin
        if pTinhtrang = 0 then
        begin
            FieldByName('XacNhan_Ngay').Clear;
            FieldByName('XacNhan_TinhTrangBy').Clear;
        end else
        begin
            FieldByName('XacNhan_Ngay').AsDateTime := Now;
            FieldByName('XacNhan_TinhTrangBy').AsInteger := sysLogonUID;
            if pShowMsg then
            case pTinhtrang of
                1: Msg(RS_REG_STATUS1);
                2: Msg(RS_REG_STATUS2);
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCa.QrMasterXetDuyet_ManvChange(Sender: TField);
begin
     EdXetDuyetManvQL.Text := EdXetDuyetManvQL.Field.AsString;
end;

procedure TFrmDangkyTangCa.QrMasterXetDuyet_SoGioHuongLuongChange(
  Sender: TField);
begin
    try
        with QrMaster do
        begin
            FieldByName('XetDuyet_SoGioNghiBu').AsFloat :=
                        FieldByName('DangKySoGio').AsFloat - FieldByName('XetDuyet_SoGioHuongLuong').AsFloat;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCa.calcOvertimes: Boolean;
var
    f: Double;
begin
	try
        with QrMaster do
        begin
            if  (FieldByName('MaCa').AsString <> '')
            and (not FieldByName('DangKyTuGio').IsNull)
            and (not FieldByName('DangKyDenGio').IsNull) then
            begin
                f := calcOvertimes( FieldByName('Manv').AsString,
                            FieldByName('MaCa').AsString,
                            FieldByName('Ngay').AsDateTime,
                            FieldByName('DangKyTuGio').AsDateTime,
                            FieldByName('DangKyDenGio').AsDateTime);

                FieldByName('DangKySoGio').AsFloat := f;
            end
            else
            begin
                FieldByName('DangKySoGio').Clear;
            end;
        end;
		Result := True;
    except
		Result := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCa.calcOvertimes(pEmpID, pSession: String; pDate, pDateFrom, pDateTo: TDateTime): Double;
begin
  	with QrDays do
    begin
    	Parameters[0].Value := pEmpID;
    	Parameters[1].Value := pDate;
        Parameters[2].Value := pDateFrom;
        Parameters[3].Value := pDateTo;
        Parameters[4].Value := pSession;
        Open;
        Result := Fields[0].AsFloat;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCa.DeleteAllRecordByTransNo;
begin
    with spIMP_HR_DANGKY_TANGCA_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

end.
