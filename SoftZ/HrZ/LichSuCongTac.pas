﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit LichSuCongTac;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, Vcl.Buttons, rDBComponents, wwcheckbox, DBCtrlsEh,
  DBGridEh, DBLookupEh, frameEmp, DbLookupComboboxEh2;

type
  TFrmLichSuCongTac = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    PaThongTin: TisPanel;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterLK_TENNV: TWideStringField;
    QrMasterNoiDung: TWideStringField;
    QrMasterMaPhuCap: TWideStringField;
    QrMasterPhuCapCongTac: TFloatField;
    QrMasterNgayBatDau: TDateTimeField;
    QrMasterNgayKetThuc: TDateTimeField;
    QrMasterLK_TenPhuCap: TWideStringField;
    QrMasterLK_PhuCapLoai: TWideStringField;
    QrMasterLK_TenPhuCapLoai: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterThang: TIntegerField;
    QrMasterNam: TIntegerField;
    QrMasterGhiChu: TWideMemoField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    QrMasterUPDATE_NAME: TWideStringField;
    CmdLoaiPhuCap: TAction;
    QrMasterCo_ThueTNCN: TBooleanField;
    CmdNgoaiTe: TAction;
    QrMasterMaQuocGia: TWideStringField;
    QrMasterCo_TrongNuoc: TBooleanField;
    QrMasterNgoaiTe_Loai: TWideStringField;
    QrMasterNgoaiTe_TyGia: TFloatField;
    QrMasterNgoaiTe_SoTien: TFloatField;
    QrMasterLK_Co_TrongNuoc: TBooleanField;
    CmdQuocGia: TAction;
    DsEmp: TDataSource;
    QrMasterLK_TenQuocGia: TWideStringField;
    QrMasterLK_TenNgoaiTe_Loai: TWideStringField;
    gbThongTin: TGroupBox;
    GroupBox2: TGroupBox;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    EdLyDo: TDBEditEh;
    CbToDate: TwwDBDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    CbQuocGia: TDbLookupComboboxEh2;
    GroupBox3: TGroupBox;
    CbLoaiPhuCap: TDbLookupComboboxEh2;
    EdPhuCapLoai: TDBEditEh;
    EdNumSoTienNgoaiTe: TDBNumberEditEh;
    EdNumSoTien: TDBNumberEditEh;
    CbNgoaiTe: TDbLookupComboboxEh2;
    EdNumTyGia: TDBNumberEditEh;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    rDBCheckBox1: TrDBCheckBox;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    SpeedButton1: TSpeedButton;
    CmDangKyVangMat: TAction;
    QrMasterNgayBatDauVangMat: TDateTimeField;
    QrMasterNgayKetThucVangMat: TDateTimeField;
    QrMasterSoNgayVangMat: TFloatField;
    EdMaThietBi: TDBEditEh;
    QrMasterLK_ManvQL: TWideStringField;
    spHR_LICHSU_CONGTAC_Invalid_Ngay: TADOCommand;
    frEmp1: TfrEmp;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    PaNhanVien: TPanel;
    PaInfo: TPanel;
    Panel1: TPanel;
    QrMasterIdx: TAutoIncField;
    QrMasterFileIdx: TGuidField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure QrMasterApprovalChange(Sender: TField);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure ToolButton2Click(Sender: TObject);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrMasterMaPhuCapChange(Sender: TField);
    procedure CmdLoaiPhuCapExecute(Sender: TObject);
    procedure CmdNgoaiTeExecute(Sender: TObject);
    procedure QrMasterMaQuocGiaChange(Sender: TField);
    procedure QrMasterNgoaiTe_SoTienChange(Sender: TField);
    procedure CmdQuocGiaExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure CmDangKyVangMatExecute(Sender: TObject);
    procedure QrMasterManvChange(Sender: TField);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, mType: Integer;
    mSQL, fStr, mEmpID, mEmpIDLabel, mEmpName: String;
    FORM_CODE, REPORT_NAME: String;
    r: WORD;
    function LeaveDayValid(pEmpID: String; pDate: TDateTime; pDateTo: TDateTime;pIDX: Integer): Boolean;
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmLichSuCongTac: TFrmLichSuCongTac;

const
    TABLE_NAME = 'HR_LICHSU_CONGTAC';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmPhucap, DmHotro_HR, Dmdl,
    DangkyVangmat, HrData, HrExCommon; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.FormCreate(Sender: TObject);
begin
    AddAllFields(QrMaster, TABLE_NAME);
    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);
    FORM_CODE := 'HR_LICHSU_CONGTAC';
    REPORT_NAME := 'HR_RP_LICHSU_CONGTAC';
    mType := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);

    with DataMain do
        OpenDataSets([QrQuocgia, QrNGOAITE]);

    with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_HR_PHUCAP, QrV_HR_PHUCAP_LOAI]);

    if mEmpID <> '' then
    begin
        TMyForm(Self).Init1;
        Width := 1260;
        Height := 682;
        Caption := 'Hồ Sơ Nhân Viên - Lịch Sử Công Tác';
        fStr := mEmpID;
        frD2D.EdFrom.Date := Date;
        frD2D.EdTo.Date := Date;
        frD2D.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
        FORM_CODE := 'HR_LICHSU_NHANVIEN_CONGTAC';
        REPORT_NAME := 'HR_RP_LICHSU_NHANVIEN_CONGTAC';
        mType := 1;
        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        gbThongTin.Height := gbThongTin.Height - PaNhanVien.Height;
        PaThongTin.Height := PaThongTin.Height - PaNhanVien.Height;
    end
    else
    begin
        TMyForm(Self).Init2;
    end;

    SetDisplayFormat(QrMaster, sysCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['NgoaiTe_SoTien', 'NgoaiTe_TyGia'], sysFloatFmtOne);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        if mEmpID <> '' then
            Add(' and Manv =:Manv')
        else
        begin
           Add(' and ([NgayBatDau] between :Tungay1 and :Denngay1' +
           ' or [NgayKetThuc] between :Tungay2 and :Denngay2' +
           ' or :Tungay3 between [NgayBatDau] and [NgayKetThuc]' +
           ' or :Denngay3 between [NgayBatDau] and [NgayKetThuc])' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayBatDau], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    if mEmpID <> '' then
        EdLyDo.SetFocus
    else
        CbNhanVien.SetFocus;
end;

procedure TFrmLichSuCongTac.CmdNgoaiTeExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'LOAI_NGOAITE');

    DataMain.QrNGOAITE.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
    begin
        if mEmpID <> '' then
            EdLyDo.SetFocus
        else
            CbNhanVien.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmDangKyVangMatExecute(Sender: TObject);
var
    sManv, sManvQL, sName: String;
    pIdx: Integer;
begin
    r := GetRights('HR_VANGMAT');
    if r = R_DENY then
    	Exit;

    with QrMaster do
    begin
	    sManv := FieldByName('Manv').AsString;
        sManvQL := FieldByName('LK_ManvQL').AsString;
        sName := FieldByName('LK_Tennv').AsString;
        pIdx := FieldByName('Idx').AsInteger;
    end;

    Application.CreateForm(TFrmDangkyVangmat, FrmDangkyVangmat);
    FrmDangkyVangmat.Execute(r, pIdx, sManv, 'HR_CONGTAC', sManvQL, sName);
    exReSyncRecord(QrMaster);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdPrint2Execute(Sender: TObject);
var
    n: Integer;
    filename, repname: String;
begin
    n := (Sender as TComponent).Tag;

    repname := 'HR_RP_LICHSU_CONGTAC_WORD';
    filename := 'DOC\' +'HR_RP_LICHSU_CONGTAC_WORD_' + IntToStr(n) + '.docx';

    with QrMaster do
        DataOffice.CreateReport2(filename ,
            [sysLogonUID, FieldByName('Idx').AsInteger], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    if mType <> 0 then
        fStr := mEmpID;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        mType, //0: Lich su, 1: HSNV/Lich Su
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdQuocGiaExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r, True);
    DataMain.QrQuocgia.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;
            if mEmpID <> '' then
                Parameters[0].Value := mEmpID
            else
            begin
                sSQL := '1=1';

                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
                    end;


                SQL.Text := Format(mSQL, [sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
                Parameters[2].Value := fTungay;
                Parameters[3].Value := fDenngay;
                Parameters[4].Value := fTungay;
                Parameters[5].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'NgayBatDau';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

procedure TFrmLichSuCongTac.CmdLoaiPhuCapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
    HrDataMain.QrDM_HR_PHUCAP.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDangKyVangMat: Boolean;
begin
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDangKyVangMat :=  (bBrowse) and (FieldByName('Manv').AsString <> '');
    end;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;
    CmDangKyVangMat.Enabled := bDangKyVangMat;
    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_TUDEN = 'Từ ngày không hợp lệ.';
procedure TFrmLichSuCongTac.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'NoiDung', 'NgayBatDau', 'NgayKetThuc']) then
            Abort;

        if (FieldByName('NgayKetThuc').AsFloat > 10) and (FieldByName('NgayKetThuc').AsDateTime < FieldByName('NgayBatDau').AsDateTime) then
        begin
            ErrMsg(RS_INVALID_TUDEN);
            CbToDate.SelectAll;
            CbToDate.SetFocus;
            Abort;
        end;

        if not LeaveDayValid(FieldByName('Manv').AsString,
            FieldByName('NgayBatDau').AsDateTime,
            FieldByName('NgayKetThuc').AsDateTime,
            FieldByName('Idx').AsInteger) then
            Abort;
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('Idx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
        FieldByName('MaQuocGia').AsString := '084';
        FieldByName('NgoaiTe_Loai').AsString := 'LOAI_NGOAITE.VND';
        FieldByName('NgoaiTe_TyGia').AsFloat := 1.0;
        if mEmpID <> '' then
            FieldByName('Manv').AsString := mEmpID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterApprovalChange(Sender: TField);
begin
    exDotManv(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdSwitchExecute(Sender: TObject);
begin
    if mEmpID <> '' then
    begin
        if ActiveControl = GrList then
            EdLyDo.SetFocus;
        GrList.SetFocus;
    end
    else
    begin
        if ActiveControl = GrList then
            CbNhanVien.SetFocus
        else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
            frD2D.EdFrom.SetFocus
        else
            GrList.SetFocus
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterManvChange(Sender: TField);
begin
    EdManvQL.Text := EdManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterMaPhuCapChange(Sender: TField);
begin
    EdPhuCapLoai.Text := EdPhuCapLoai.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterMaQuocGiaChange(Sender: TField);
begin
    with QrMaster do
    begin
        FieldByName('Co_TrongNuoc').AsBoolean := FieldByName('LK_Co_TrongNuoc').AsBoolean;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.QrMasterNgoaiTe_SoTienChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    bTrigger := mTrigger;
    mTrigger := True;
    with QrMaster do
    begin
        if Sender.FieldName = 'PhuCapCongTac' then
        begin
           FieldByName('NgoaiTe_TyGia').AsFloat := exVNDRound(SafeDiv(FieldByName('PhuCapCongTac').AsFloat, FieldByName('NgoaiTe_SoTien').AsFloat), sysFloatRoundOne);
        end
        else
        begin
            FieldByName('PhuCapCongTac').AsFloat := exVNDRound(FieldByName('NgoaiTe_SoTien').AsFloat * FieldByName('NgoaiTe_TyGia').AsFloat, sysCurHrRound);
        end;
    end;
    mTrigger := bTrigger;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 508;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichSuCongTac.ToolButton2Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichSuCongTac.LeaveDayValid;
var
    s: String;
begin
    with spHR_LICHSU_CONGTAC_Invalid_Ngay do
    begin
        Prepared := True;
        Parameters.ParamByName('@IDX').Value := pIDX;
        Parameters.ParamByName('@MANV').Value := pEmpID;
        Parameters.ParamByName('@NGAYD').Value := pDate;
        Parameters.ParamByName('@NGAYC').Value := pDateTo;
        try
            Execute;
        except
        end;

         if (Parameters.FindParam('@STR') <> nil) then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;
end.
