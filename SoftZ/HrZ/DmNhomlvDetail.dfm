object FrmNhomlvDetail: TFrmNhomlvDetail
  Left = 279
  Top = 179
  BorderStyle = bsDialog
  Caption = 'Nh'#243'm L'#224'm Vi'#7879'c - Chi Ti'#7871't'
  ClientHeight = 366
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grThongTinLamViec: TDBVertGridEh
    Left = 0
    Top = 0
    Width = 379
    Height = 366
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    AllowedSelections = []
    RowCategories.Active = True
    RowCategories.CategoryProps = <
      item
        Name = 'NGAYLAMVIEC'
        DisplayText = 'Ng'#224'y l'#224'm vi'#7879'c trong tu'#7847'n'
        DefaultExpanded = True
      end
      item
        Name = 'CHAMCONG'
        DisplayText = 'X'#225'c nh'#7853'n ch'#7845'm c'#244'ng'
        DefaultExpanded = True
      end
      item
        Name = 'TANGCA'
        DisplayText = 'T'#259'ng ca'
        DefaultExpanded = True
      end
      item
        Name = 'KHAC'
        DisplayText = 'Kh'#225'c'
        DefaultExpanded = True
      end>
    RowCategories.Font.Charset = DEFAULT_CHARSET
    RowCategories.Font.Color = clWindow
    RowCategories.Font.Height = -13
    RowCategories.Font.Name = 'Tahoma'
    RowCategories.Font.Style = [fsBold]
    RowCategories.ParentFont = False
    LabelColParams.Font.Charset = DEFAULT_CHARSET
    LabelColParams.Font.Color = clWindowText
    LabelColParams.Font.Height = -13
    LabelColParams.Font.Name = 'Tahoma'
    LabelColParams.Font.Style = []
    LabelColParams.ParentFont = False
    PrintService.ColorSchema = pcsFullColorEh
    DataColParams.Font.Charset = DEFAULT_CHARSET
    DataColParams.Font.Color = 8404992
    DataColParams.Font.Height = -13
    DataColParams.Font.Name = 'Tahoma'
    DataColParams.Font.Style = [fsBold]
    DataColParams.MaxRowHeight = 2
    DataColParams.MaxRowLines = 10
    DataColParams.ParentFont = False
    DataColParams.RowHeight = 21
    DataSource = FrmDmNhomlv.DsDM
    DrawGraphicData = True
    DrawMemoText = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Flat = True
    GridLineParams.ColorScheme = glcsThemedEh
    Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
    OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
    ParentFont = False
    SearchPanel.Enabled = True
    SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
    TabOrder = 0
    LabelColWidth = 213
    Rows = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T2'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T3'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T4'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T5'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T6'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'T7'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'CN'
        CategoryName = 'NGAYLAMVIEC'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'Co_ChamCong'
        CategoryName = 'CHAMCONG'
      end
      item
        ButtonStyle = cbsDropDown
        DynProps = <>
        DropDownBox.ColumnDefValues.AutoDropDown = True
        DropDownBox.SortLocal = True
        EditButton.Visible = True
        EditButton.Width = 20
        EditButtons = <>
        FieldName = 'CA1'
        FitRowHeightToData = False
        FitRowHeightToTextLines = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        WordWrap = True
        CategoryName = 'CHAMCONG'
        LookupParams.KeyFieldNames = 'CA1'
        LookupParams.LookupDataSet = QrDMCA
        LookupParams.LookupDisplayFieldName = 'TenCa'
        LookupParams.LookupKeyFieldNames = 'MaCa'
      end
      item
        ButtonStyle = cbsDropDown
        DynProps = <>
        DropDownBox.ColumnDefValues.AutoDropDown = True
        DropDownBox.SortLocal = True
        EditButton.Visible = True
        EditButton.Width = 20
        EditButtons = <>
        FieldName = 'CA2'
        FitRowHeightToData = False
        FitRowHeightToTextLines = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        CaseInsensitiveTextSearch = False
        WordWrap = False
        CategoryName = 'CHAMCONG'
        LookupParams.KeyFieldNames = 'CA2'
        LookupParams.LookupDataSet = QrDMCA
        LookupParams.LookupDisplayFieldName = 'TenCa'
        LookupParams.LookupKeyFieldNames = 'MaCa'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'OT'
        CategoryName = 'TANGCA'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'HT'
        CategoryName = 'TANGCA'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'NS'
        CategoryName = 'TANGCA'
      end>
  end
  object ActionList1: TActionList
    Left = 108
    Top = 132
    object CmdClose: TAction
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
  end
  object QrDMCA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_CALAMVIEC')
    Left = 220
    Top = 158
  end
end
