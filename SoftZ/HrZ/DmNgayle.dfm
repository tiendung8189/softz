object FrmDmNgayle: TFrmDmNgayle
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch C'#225'c Ng'#224'y Ngh'#7881' Trong N'#259'm'
  ClientHeight = 409
  ClientWidth = 695
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 37
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 388
    Width = 695
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 85
    Width = 695
    Height = 303
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'Loai;CustomEdit;CbLoai;F'
      'LK_TenLoai;CustomEdit;CbLoai;F')
    Selected.Strings = (
      'NGAY'#9'15'#9'Ng'#224'y'#9'F'
      'THU'#9'8'#9'Th'#7913#9'T'
      'TenLeTet'#9'55'#9'Di'#7877'n gi'#7843'i'#9'F'
      'LK_TenLoai'#9'30'#9'Lo'#7841'i ng'#224'y ngh'#7881#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDM
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object Panel1: TPanel
    Left = 0
    Top = 37
    Width = 695
    Height = 48
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 217
      Top = 16
      Width = 26
      Height = 16
      Caption = '&N'#259'm'
      FocusControl = CbYear
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbYear: TComboBox
      Left = 253
      Top = 12
      Width = 65
      Height = 24
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = 'CbYear'
      OnChange = CbYearChange
    end
  end
  object CbLoai: TwwDBLookupCombo
    Left = 486
    Top = 199
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TEN_HOTRO'#9'25'#9'DGIAI'#9'F')
    DataField = 'Loai'
    DataSource = DsDM
    LookupTable = HrDataMain.QrV_HR_LOAI_NGAY_TINHCONG
    LookupField = 'MA_HOTRO'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdEveryYear: TAction
      Caption = 'M'#7863'c '#273#7883'nh nh'#432' n'#259'm tr'#432#7899'c'
      OnExecute = CmdEveryYearExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 206
    Top = 168
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = QrDMBeforeOpen
    BeforeInsert = QrDMBeforeInsert
    AfterInsert = QrDMAfterInsert
    BeforePost = QrDMBeforePost
    BeforeDelete = QrDMBeforeDelete
    OnCalcFields = QrDMCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'YEAR'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from HR_DM_NGAYLETET'
      ' where'#9'year([NGAY]) = :YEAR'
      'order by NGAY')
    Left = 388
    Top = 164
    object QrDMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMNGAY: TDateTimeField
      DisplayLabel = 'M'#227
      FieldName = 'NGAY'
    end
    object QrDMTHU: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'THU'
      Size = 15
      Calculated = True
    end
    object QrDMTenLeTet: TWideStringField
      FieldName = 'TenLeTet'
      Size = 200
    end
    object QrDMTenLeTet_TA: TWideStringField
      FieldName = 'TenLeTet_TA'
      Size = 200
    end
    object QrDMLoai: TIntegerField
      FieldName = 'Loai'
    end
    object QrDMLK_TenLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenLoai'
      LookupDataSet = HrDataMain.QrV_HR_LOAI_NGAY_TINHCONG
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'Loai'
      Lookup = True
    end
    object QrDMHeSoOT: TFloatField
      FieldName = 'HeSoOT'
    end
  end
  object DsDM: TDataSource
    DataSet = QrDM
    Left = 388
    Top = 192
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 177
    Top = 168
    object Khnglcdliu1: TMenuItem
      Action = CmdEveryYear
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 236
    Top = 168
  end
  object SET_DEFAULT: TADOCommand
    CommandText = 
      'insert into HR_DM_NGAYLETET ([NGAY], [TenLeTet], [TenLeTet_TA], ' +
      '[LOAI], [CREATE_BY], [UPDATE_BY], [CREATE_DATE], [UPDATE_DATE])'#13 +
      #10'select'#9'dateadd(yy, 1, [NGAY]), [TenLeTet], [TenLeTet_TA], [LOAI' +
      '], :UID, :UID, getdate(), getdate()'#13#10'  from'#9'HR_DM_NGAYLETET'#13#10' wh' +
      'ere'#9'year([NGAY]) = :YEAR - 1'#13#10'   and'#9'dateadd(yy, 1, [NGAY]) not ' +
      'in (select [NGAY] from HR_DM_NGAYLETET where year([NGAY]) = :YEA' +
      'R)'#13#10
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'UID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'UID'
        Size = -1
        Value = Null
      end
      item
        Name = 'YEAR'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'YEAR'
        Size = -1
        Value = Null
      end>
    Left = 45
    Top = 184
  end
end
