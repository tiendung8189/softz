﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevLog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, Buttons, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, wwdblook, OleCtrls, zkemkeeper_TLB;

type
  TFrmDevLog = class(TForm)
    Panel1: TPanel;
    Rb1: TRadioButton;
    Rb2: TRadioButton;
    EdDate: TwwDBDateTimePicker;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Label8: TLabel;
    CbMay: TwwDBLookupCombo;
    CZKEM1: TCZKEM;
    LbAni: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
  public
  	function  Execute: Boolean;
  end;

var
  FrmDevLog: TFrmDevLog;

implementation

uses
	isLib, ExCommon, IniFiles, MainData, zkem, ADODB, isMsg, DB, LogReader,
    WaitProgress;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevLog.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevLog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    RegWrite(Name, 'Default', Iif(Rb1.Checked, 0, 1));
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDevLog.Execute;
begin
	EdDate.Date := EncodeDate(sysYear, sysMon, 1);
	Result := ShowModal = mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevLog.FormShow(Sender: TObject);
begin
    if RegRead(Name, 'Default', 0) = 0 then
	    Rb1.Checked := True
    else
	    Rb2.Checked := True;

    with DataMain.QrDM_MAYCC do
    begin
    	Open;
        if not IsEmpty then
	    	CbMay.LookupValue := FieldByName('ID').Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevLog.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevLog.CmdReturnClick(Sender: TObject);
var
    i: Integer;
	xIo: Integer;
    mLogReader: TLogReader;
begin
    devLogAll := Rb1.Checked;
    devLogFromDate := EdDate.Date;

    with DataMain.QrDM_MAYCC do
    begin
        if FieldByName('IO_TYPE').AsString = 'I' then
        	xIo := 0
        else if FieldByName('IO_TYPE').AsString = 'O' then
        	xIo := 1
        else if FieldByName('IO_TYPE').AsString = 'F' then
        	xIo := 2
        else
        	xIo := -1;

        i := FieldByName('ID').AsInteger;
            FrmWaitProgress.Start(500, Format('Kết nối máy chấm công "%d"', [i]));
            Application.ProcessMessages;

            mLogReader := TLogReader.Create(
                FieldByName('IP4').AsString,
                i,
                FieldByName('SOCKET').AsInteger,
                xIo, RecNo, 10, 0, True);

            mLogReader.MaxLog := 5000;//mRecMaxLog;
			mLogReader.AutoClearLog := False;
            mLogReader.Active := True;
            mLogReader.Resume;
            mLogReader.WaitFor;
            Application.ProcessMessages;
            mLogReader.Free;
    end;
    {

	with DataMain.QrDM_MAYCC do
    begin
        if not zkConnect(CZKEM1,
            FieldByName('COM_TYPE').AsString,
            FieldByName('IP4').AsString,
            FieldByName('SOCKET').AsInteger,
            FieldByName('PORT').AsInteger,
            FieldByName('ID').AsInteger,
            FieldByName('BAUD_RATE').AsInteger) then
        	Exit;

        if FieldByName('IO_TYPE').AsString = 'I' then
        	xIo := 0
        else if FieldByName('IO_TYPE').AsString = 'O' then
        	xIo := 1
        else if FieldByName('IO_TYPE').AsString = 'F' then
        	xIo := 2
        else
        	xIo := -1;
	end;

	if not zkReadGeneralLog then
    begin
		error := zkGetLastError;
        if error = 0 then
		begin
        	Msg('Không có mẫu tin dữ liệu nào.');
            zkDisconnect;
            Exit;
		end;
        ErrMsg('Lỗi đọc dữ liệu từ máy chấm công.');
        zkDisconnect;
        Exit;
	end;

    bAll := Rb1.Checked;
    xDate := EdDate.Date;

    Screen.Cursor := crHourGlass;
	with DataMain.QrDM_MAYCC do
    begin
    	Close;
        Open;
		while zkGetGeneralLog(empId, inout, nYear, nMonth, nDay, nHour, nMin, nSec) do
		begin
        	LbAni.Caption := Format('%s,%d-%d-%d %d:%d', [empId, nYear, nMonth, nDay, nHour, nMin]);
            LbAni.Refresh;

        	if xIo < 2 then
            	inout := xIo;

            if bAll then
            else if xDate > EncodeDate(nYear, nMonth, nDay) then
            	Continue;
            Append;
            FieldByName('EmpID').AsString := empId;
            FieldByName('Year').AsInteger := nYear;
            FieldByName('Month').AsInteger := nMonth;
            FieldByName('Day').AsInteger := nDay;
            FieldByName('Hour').AsInteger := nHour;
            FieldByName('Minute').AsInteger := nMin;
            FieldByName('Second').AsInteger := nSec;
            FieldByName('Inout').AsInteger := inout;
            Post;
		end;
    end;
    }
	LbAni.Caption := '';
    Screen.Cursor := crDefault;
    MsgDone;
//    ModalResult := mrOK;
end;

end.
