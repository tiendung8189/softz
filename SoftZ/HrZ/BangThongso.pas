﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BangThongso;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid, ADODb,
  AppEvnts, Menus, AdvMenus, wwDBGrid2, DBCtrls, Grids, Wwdbigrd, ToolWin,
  Vcl.Mask, wwdbedit, Wwdotdot, Wwdbcomb;

type
  TFrmBangThongso = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    Ds1: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Qr1: TADOQuery;
    ToolButton8: TToolButton;
    Qr1NAM: TIntegerField;
    Qr1THANG: TIntegerField;
    Qr1CREATE_BY: TIntegerField;
    Qr1UPDATE_BY: TIntegerField;
    Qr1CREATE_DATE: TDateTimeField;
    Qr1UPDATE_DATE: TDateTimeField;
    GrList: TwwDBGrid2;
    PopCommon: TAdvPopupMenu;
    CmdCountWD: TAction;
    Tnhsngycngthct1: TMenuItem;
    COUNT_WD: TADOCommand;
    CmdReload: TAction;
    PaPsComment: TPanel;
    PDComment: TGroupBox;
    EdComment: TDBMemo;
    Qr1GHICHU: TWideStringField;
    Qr1KhoaSoLichLV: TBooleanField;
    Qr1KhoaSoCong: TBooleanField;
    Qr1KhoaSoLuong: TBooleanField;
    Qr1DuyetLuong: TBooleanField;
    Qr1ThuongTet: TBooleanField;
    Qr1GhiChuCong: TWideStringField;
    Qr1GhiChuLuong: TWideStringField;
    QrLast: TADOQuery;
    Qr1NgayCongChuan: TFloatField;
    Qr1NgayCongThang: TFloatField;
    CbYear: TwwDBComboBox;
    CbMon: TwwDBComboBox;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Qr1BeforePost(DataSet: TDataSet);
    procedure Qr1BeforeDelete(DataSet: TDataSet);
    procedure Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure Qr1BeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdCountWDExecute(Sender: TObject);
    procedure Qr1AfterInsert(DataSet: TDataSet);
    procedure Qr1NAMChange(Sender: TField);
    procedure CmdReloadExecute(Sender: TObject);
    procedure Qr1BeforeEdit(DataSet: TDataSet);
  private
    mMonth, mYear: Integer;
  	mCanEdit, mTrigger: Boolean;
  public
  	procedure Execute(r: WORD; pType: Integer; pMonth: Integer = 0; pYear: Integer = 0);
  end;

var
  FrmBangThongso: TFrmBangThongso;

const
    TABLE_NAME      = 'HR_BANG_THONGSO';
    FORM_CODE       = 'HR_BANG_THONGSO';
    FORM_CODE_CONG  = 'HR_BANG_THONGSO_CONG';
    FORM_CODE_LUONG = 'HR_BANG_THONGSO_LUONG';
    FORM_CODE_LICHLV = 'HR_BANG_THONGSO_LICHLV';
    REPORT_NAME     = 'HR_BANG_THONGSO';

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, Variants, MainData, isCommon;



{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Execute;
begin
	mCanEdit := rCanEdit(r);

    mMonth := pMonth;
    mYear  := pYear;

    case pType of
    1:
		SetCustomGrid(FORM_CODE_CONG, GrList);
	2:
    begin
        TMyForm(Self).Width := 1501;
        TMyForm(Self).Height := 747;
		SetCustomGrid(FORM_CODE_LUONG, GrList);
    end;
	3:
        begin
            Caption := 'Thông Số Lịch Làm Việc';
            SetCustomGrid(FORM_CODE_LICHLV, GrList);
        end;
    else
		SetCustomGrid(FORM_CODE, GrList);
    end;
    EdComment.ReadOnly := not mCanEdit;
//    EdComment2.ReadOnly := not mCanEdit;
    PaPsComment.Visible := pType = 2;
	ShowModal;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdNewExecute(Sender: TObject);
begin
	with Qr1 do
    begin
    	Append;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdSaveExecute(Sender: TObject);
begin
	Qr1.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdCancelExecute(Sender: TObject);
begin
	Qr1.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdDelExecute(Sender: TObject);
begin
	Qr1.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([Qr1]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    mTrigger := False;
    AddAllFields(Qr1, TABLE_NAME, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    OpenDataSets([Qr1]);

    with Qr1 do
    begin
	    SetDisplayFormat(Qr1, sysCurFmt);
    end;

    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);

    SetDictionary(Qr1, FORM_CODE);
    if not Qr1.Locate('NAM;THANG', VarArrayOf([mYear, mMonth]), []) then
        Qr1.Last;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, Qr1, mCanEdit);
    CmdCountWD.Enabled := (not Qr1.IsEmpty) and mCanEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(Qr1, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1BeforePost(DataSet: TDataSet);
begin
	if BlankConfirm(DataSet, ['THANG', 'NAM']) then
		Abort;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1BeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

//    if Qr1.FieldByName('Approved').AsBoolean then
//        Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1BeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

//    if Qr1.FieldByName('Approved').AsBoolean then
//        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1BeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    with Qr1 do
    begin
        if IsEmpty then
        begin
            mMonth := sysMon;
            mYear  := sysYear;
        end
        else
        begin
            Last;
	        mMonth := FieldByName('THANG').AsInteger;
    	    mYear := FieldByName('NAM').AsInteger;
		end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(Qr1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_INVALID_MONTH = 'Tháng không hợp lệ.';

procedure TFrmBangThongso.CmdCountWDExecute(Sender: TObject);
var
	m, y, n: Integer;
begin
	with Qr1 do
    begin
		m := FieldByName('THANG').AsInteger;
        if (m < 1) or (m > 12) then
        begin
        	Msg(RS_INVALID_MONTH);
        	Exit;
        end;
		y := FieldByName('NAM').AsInteger;
    end;

    with COUNT_WD do
    begin
    	Prepared := True;
        Parameters[1].Value := y;
        Parameters[2].Value := m;
        Execute;
        n := Parameters[0].Value;
    end;

	with Qr1 do
    begin
    	if State in [dsBrowse] then
        	Edit;
		FieldByName('NgayCongThang').AsInteger := n;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1AfterInsert(DataSet: TDataSet);
var
    sExceptField: String;
begin
    sExceptField := 'THANG;NAM;KhoaSoLichLV;KhoaSoCong;KhoaSoLuong;DuyetLuong;ThuongTet;GhiChuCong;GhiChuLuong;GhiChu;CREATE_BY;CREATE_DATE';
    with Qr1 do
    begin
        with QrLast do
        begin
            if Active then
                Close;

            Parameters[0].Value := mYear;
            Parameters[1].Value := mMonth;
            Open;

            exDbCopyFields(Qr1, QrLast, True, sExceptField);
        end;

        Inc(mMonth);
        if mMonth > 12 then
        begin
            mMonth := 1;
            Inc(mYear);
        end;

        FieldByName('THANG').AsInteger := mMonth;
        FieldByName('NAM').AsInteger := mYear;
        FieldByName('KhoaSoLichLV').AsBoolean := False;
        FieldByName('KhoaSoCong').AsBoolean := False;
        FieldByName('KhoaSoLuong').AsBoolean := False;
        FieldByName('DuyetLuong').AsBoolean := False;
        FieldByName('ThuongTet').AsBoolean := False;
    end;

    CmdCountWD.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.Qr1NAMChange(Sender: TField);
var
    dd, mm, yy: WORD;
begin
//    if mTrigger then
//        Exit;
//
//    with Qr1 do
//    begin
//		if FieldByName('Closing Date').IsNull then
//			Exit;
//
//        mTrigger := True;
//        DecodeDate(FieldByName('Closing Date').AsDateTime, yy, mm, dd);
//        mm := FieldByName('Month').AsInteger;
//        yy := FieldByName('Year').AsInteger;
//        if (mm > 0) and (yy > 0) then
//			FieldByName('Closing Date').AsDateTime := EncodeDate(yy, mm, dd);
//    end;
//    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangThongso.CmdReloadExecute(Sender: TObject);
begin
    Qr1.Requery;
end;

end.

