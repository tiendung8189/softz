﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ImportExcelRev;

interface

uses
  Classes, Controls, Forms, ActnList, Wwdbigrd, Wwdbgrid,
  wwDBGrid2, DB, ADODB, Grids;

type
  TFrmImportExcelRev = class(TForm)
    ActionList: TActionList;
    CmdClose: TAction;
    GrBrowse: TwwDBGrid2;
    DsExcel: TDataSource;
    QrExcel: TADOQuery;
    procedure TntFormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    //
  public
    procedure Execute(pDataSet: TDataSet; pCaption: String = 'Dữ Liệu Từ Excel');
  end;

var
  FrmImportExcelRev: TFrmImportExcelRev;

implementation

uses
    isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelRev.Execute;
begin
    Caption := pCaption;
    DsExcel.DataSet := pDataSet;
    ShowModal;
end;

procedure TFrmImportExcelRev.FormShow(Sender: TObject);
var
    i: Integer;
begin
    for i := 0 to GrBrowse.FieldCount - 1 do
        if GrBrowse.ColWidths[i] > 100 then
            GrBrowse.ColWidths[i] := 100;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelRev.TntFormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmImportExcelRev.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

end.
