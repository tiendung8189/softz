(*==============================================================================
**------------------------------------------------------------------------------
*)
unit WaitProgress;

interface

uses
  SysUtils, Classes, Controls, Forms, ComCtrls, StdCtrls, ExtCtrls;

type
  TPlayTimerProc = procedure of object;

  TMyTimerThread = class(TThread)
  private
    FProc: TPlayTimerProc;
  protected
    procedure Execute; override;
  public
    constructor Create(proc: TPlayTimerProc);
  end;

  TFrmWaitProgress = class(TForm)
    PaMain: TPanel;
    Desc: TLabel;
    LbTime: TLabel;
    ProgBar: TProgressBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    mPlay, mAutoInc: Boolean;
    mTime: TTime;
    mThread: TMyTimerThread;
    procedure PlayTimer;
  public
    procedure Execute(s: String; nMax: Integer = 0; showtime: Boolean = False;
        pOnTop: Boolean = True; pAutoInc: Boolean = False); overload;
    procedure Execute(n: Integer; nMax: Integer = 0; showtime: Boolean = False;
        pOnTop: Boolean = True; pAutoInc: Boolean = False); overload;

    procedure Start(nMax: Integer; sDesc: String = '');
    procedure Stop;

    procedure IncProgress(pInc: Integer = 1);
    procedure SetDesc(sDesc: String);
    procedure SetProgress(nPos: Integer);


  end;

var
	FrmWaitProgress: TFrmWaitProgress;

implementation

{$R *.DFM}

uses
	isCommon, Windows, ExCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TMyTimerThread.Create(proc: TPlayTimerProc);
begin
    FProc := proc;
	inherited Create(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TMyTimerThread.Execute;
begin
    try
        FProc();
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.PlayTimer;
var
	s: String;
begin
	while mPlay do
    begin
    	s := FormatDateTime('hh:nn:ss', Frac(Now()) - mTime);
        if (s[1] = '0') and (s[2] = '0')then
        	s := Copy(s, 4, 100);

        if s <> LbTime.Caption then
        begin
            LbTime.Caption := s;
            LbTime.Refresh;

            if mAutoInc then
            with ProgBar do
                if Max > Position then
                    StepIt
                else
                    Position := 0;
        end;
//        if mgCancelTThread then
//            mPlay := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.SetDesc(sDesc: String);
begin
    Desc.Caption := sDesc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.SetProgress(nPos: Integer);
begin
    ProgBar.Position := nPos;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.Start(nMax: Integer; sDesc: String);
begin
    with ProgBar do
    begin
        if nMax = 0 then
            Max := 100
        else
            Max := nMax;

        Position := 0;
    end;
    mTime := Frac(Now());
    if sDesc <> '' then
        Desc.Caption := sDesc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.Stop;
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.Execute(s: String; nMax: Integer; showtime, pOnTop, pAutoInc: Boolean);
//var
//	p: TPoint;
begin
//    if Screen.ActiveForm <> nil then
//    begin
//        p.x := round((Screen.ActiveForm.Width - Width)/2);
//        p.y := Screen.ActiveForm.Top + Screen.ActiveForm.Height - (Height + 82);
//        p := Screen.ActiveForm.ClientToScreen(p);
//
//        Left  := p.x;
//        Top   := p.y;
//    end;

    if showtime then
    begin
        mAutoInc := pAutoInc;
    	mPlay := True;
        mTime := Frac(Now());
        mThread := TMyTimerThread.Create(PlayTimer);
        mThread.FreeOnTerminate := True;
    	mThread.Priority := tpLowest;	// tpIdle;
    end;
    LbTime.Visible := showtime;
    Start(nMax, s);
    Desc.Caption := s;

    if pOnTop then
        FormStyle := fsStayOnTop
    else
        FormStyle := fsNormal;

    Show;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.Execute(n: Integer; nMax: Integer; showtime, pOnTop, pAutoInc: Boolean);
var
	s: String;
begin
    with mvlWaitDialog do
    begin
    	if TrippleDots then
        	s := '...'
        else
        	s := '';

	    case n of
		0:
			s := DatabaseConnectText + s;
	    1:
		    s := PrepareText + s;
	    2:
		    s := ProcessingText + s;
        3:
		    s := DataReadingText + s;
	    end;
    end;
	Execute(s, nMax, showtime, pOnTop, pAutoInc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	if LbTime.Visible then
    begin
        mThread.Terminate;
        mPlay := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.FormDestroy(Sender: TObject);
begin
//    if not mThread.Terminated then
    if mPlay then
    begin
        mPlay := False;
        if mThread <> nil then
            mThread.Terminate;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.FormShow(Sender: TObject);
begin
	// Font
    Desc.Font.Name := sysDialogFont.Name;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmWaitProgress.IncProgress(pInc: Integer);
begin
    with ProgBar do
    begin
        StepBy(pInc);
        StepIt;
    end;
end;

end.
