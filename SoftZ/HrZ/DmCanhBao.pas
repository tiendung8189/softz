(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmCanhBao;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin;

type
  TFrmDmCanhBao = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    CmdDetail: TAction;
    QrDMSoNgay: TFloatField;
    QrDMMaNhacViec: TWideStringField;
    QrDMTenNhacViec: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmCanhBao: TFrmDmCanhBao;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHACVIEC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDM.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDM, FORM_CODE);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDM, FORM_CODE, Filter);

    fixCode := SetCodeLength(FORM_CODE, QrDM.FieldByName('MaNhacViec'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.FormShow(Sender: TObject);
begin
    QrDM.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.QrDMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.QrDMBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDM, ['MaNhacViec', 'TenNhacViec']) then
   		Abort;

    if fixCode then
		if LengthConfirm(QrDM.FieldByName('MaNhacViec')) then
        	Abort;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaNhacViec' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmCanhBao.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

end.
