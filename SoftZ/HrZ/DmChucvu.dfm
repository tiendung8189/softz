object FrmDmChucvu: TFrmDmChucvu
  Left = 177
  Top = 132
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Ch'#7913'c Danh/ Ch'#7913'c V'#7909
  ClientHeight = 572
  ClientWidth = 939
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 939
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 720
  end
  object Status: TStatusBar
    Left = 0
    Top = 551
    Width = 939
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitTop = 612
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 939
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 939
    Height = 511
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitHeight = 572
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 939
    Height = 511
    Cursor = 1
    ActivePage = tsDep
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    HotTrack = True
    ParentFont = False
    TabOrder = 3
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    ExplicitHeight = 572
    object tsDep: TTabSheet
      Caption = 'Ch'#7913'c danh'
      ImageIndex = 1
      ExplicitHeight = 544
      object GrLydo: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 931
        Height = 483
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'LK_TenChucVu;CustomEdit;CbNhom;F')
        PictureMasks.Strings = (
          'MaChucVu'#9'*!'#9'T'#9'T')
        Selected.Strings = (
          'LK_TenChucVu'#9'20'#9'Ch'#7913'c v'#7909#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsDm
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopCommon
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrLydoCalcCellColors
        TitleImageList = DataMain.ImageSort
        GroupFieldName = 'LK_TenChucVu'
        ExplicitHeight = 544
      end
    end
    object tsBehaviour: TTabSheet
      Caption = 'Ch'#7913'c v'#7909
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GrNhom: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 931
        Height = 544
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'NHOM_DACBIET;CustomEdit;CbType;F')
        PictureMasks.Strings = (
          'MaChucVu'#9'*!'#9'T'#9'T')
        Selected.Strings = (
          'MaChucVu'#9'15'#9'M'#227#9'F'
          'TenChucVu'#9'50'#9'T'#234'n'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        Align = alClient
        DataSource = DsNhom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopCommon
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrNhomCalcCellColors
        TitleImageList = DataMain.ImageSort
      end
    end
  end
  object CbNhom: TwwDBLookupCombo
    Left = 294
    Top = 183
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TenChucVu'#9'20'#9'TenChucVu'#9'F')
    DataField = 'MaChucVu'
    DataSource = DsDm
    LookupTable = RefNhom
    LookupField = 'MaChucVu'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnNotInList = CbNhomNotInList
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 229
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
  end
  object QrNhom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrNhomBeforeInsert
    BeforePost = QrNhomBeforePost
    AfterPost = QrNhomAfterPost
    BeforeDelete = QrNhomBeforeDelete
    OnDeleteError = QrNhomPostError
    OnPostError = QrNhomPostError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_CHUCVU'
      'order by'#9'MaChucVu')
    Left = 61
    Top = 168
    object QrNhomCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNhomUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNhomCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNhomUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNhomMaChucVu: TWideStringField
      FieldName = 'MaChucVu'
    end
    object QrNhomTenChucVu: TWideStringField
      FieldName = 'TenChucVu'
      Size = 200
    end
    object QrNhomTenChucVu_TA: TWideStringField
      FieldName = 'TenChucVu_TA'
      Size = 200
    end
  end
  object DsNhom: TDataSource
    DataSet = QrNhom
    Left = 61
    Top = 196
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 197
    Top = 168
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 197
    Top = 228
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrDm: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrNhomBeforeInsert
    AfterInsert = QrDmAfterInsert
    BeforePost = QrDmBeforePost
    BeforeDelete = QrNhomBeforeDelete
    OnDeleteError = QrNhomPostError
    OnPostError = QrNhomPostError
    Parameters = <>
    SQL.Strings = (
      'select  *'
      '  from  HR_DM_CHUCDANH'
      'order by'#9'MaChucVu, MaChucDanh')
    Left = 33
    Top = 168
    object QrDmCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDmUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDmCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDmUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDmLK_NHOM: TWideStringField
      DisplayLabel = 'Ch'#7913'c v'#7909
      FieldKind = fkLookup
      FieldName = 'LK_TenChucVu'
      LookupDataSet = RefNhom
      LookupKeyFields = 'MaChucVu'
      LookupResultField = 'TenChucVu'
      KeyFields = 'MaChucVu'
      Size = 200
      Lookup = True
    end
    object QrDmMaChucDanh: TWideStringField
      FieldName = 'MaChucDanh'
    end
    object QrDmMaChucVu: TWideStringField
      FieldName = 'MaChucVu'
    end
    object QrDmTenChucDanh: TWideStringField
      FieldName = 'TenChucDanh'
      Size = 200
    end
    object QrDmTenChucDanh_TA: TWideStringField
      FieldName = 'TenChucDanh_TA'
      Size = 200
    end
    object QrDmPhuCap_ChucDanh: TFloatField
      FieldName = 'PhuCap_ChucDanh'
    end
    object QrDmPhepNam_LuatDinh: TFloatField
      FieldName = 'PhepNam_LuatDinh'
    end
    object QrDmPhepNam_ChucDanh: TFloatField
      FieldName = 'PhepNam_ChucDanh'
    end
    object QrDmPhepNam_ThamNien: TFloatField
      FieldName = 'PhepNam_ThamNien'
    end
    object QrDmCo_PhepNamMotThang: TBooleanField
      FieldName = 'Co_PhepNamMotThang'
    end
    object QrDmCo_PhepNamSoLe: TBooleanField
      FieldName = 'Co_PhepNamSoLe'
    end
  end
  object DsDm: TDataSource
    DataSet = QrDm
    Left = 33
    Top = 196
  end
  object RefNhom: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrDmBeforePost
    BeforeDelete = QrNhomBeforeDelete
    OnDeleteError = QrNhomPostError
    OnPostError = QrNhomPostError
    Parameters = <>
    SQL.Strings = (
      'select  *'
      '  from  HR_DM_CHUCVU')
    Left = 89
    Top = 168
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDm
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MaChucDanh'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MaChucDanh'
      'TenChucDanh'
      'MaChucVu')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdDayMonthYear
    SQLTables = <>
    Left = 197
    Top = 196
  end
end
