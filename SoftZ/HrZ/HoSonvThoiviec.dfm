object FrmHoSonvThoiviec: TFrmHoSonvThoiviec
  Left = 249
  Top = 197
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'H'#7891' S'#417' Nh'#226'n Vi'#234'n - Th'#244'i Vi'#7879'c'
  ClientHeight = 453
  ClientWidth = 899
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = TntFormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaTerminated: TisPanel
    Left = 0
    Top = 77
    Width = 899
    Height = 78
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    ParentBackground = False
    TabOrder = 2
    HeaderCaption = ' :: Th'#244'ng tin th'#244'i vi'#7879'c'
    HeaderColor = clHighlight
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWhite
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    DesignSize = (
      899
      78)
    object Label1: TLabel
      Left = 702
      Top = 25
      Width = 79
      Height = 16
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Ng'#224'y th'#244'i vi'#7879'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 653
      Top = 49
      Width = 128
      Height = 16
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Ng'#224'y quy'#7871't to'#225'n l'#432#417'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 10
      Top = 25
      Width = 127
      Height = 16
      Caption = 'Ng'#224'y '#273#259'ng k'#253' th'#244'i vi'#7879'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object DpNgayDangKyThoiViec: TwwDBDateTimePicker
      Left = 145
      Top = 22
      Width = 101
      Height = 22
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayDangKyThoiViec'
      DataSource = FrmHoSonv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
    object DpNgayThoiViec: TwwDBDateTimePicker
      Left = 787
      Top = 22
      Width = 101
      Height = 22
      Anchors = [akTop, akRight]
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayThoiViec'
      DataSource = FrmHoSonv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 2
    end
    object wwDBDateTimePicker4: TwwDBDateTimePicker
      Left = 787
      Top = 46
      Width = 101
      Height = 22
      Anchors = [akTop, akRight]
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayQuyetToan'
      DataSource = FrmHoSonv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 3
    end
    object CbLyDo: TDbLookupComboboxEh2
      Left = 145
      Top = 46
      Width = 502
      Height = 22
      ControlLabel.Width = 30
      ControlLabel.Height = 16
      ControlLabel.Caption = 'L'#253' do'
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      Anchors = [akLeft, akTop, akRight]
      DynProps = <>
      DataField = 'MaQTLV_ThoiViec'
      DataSource = FrmHoSonv.DsDMNV
      DropDownBox.Columns = <
        item
          FieldName = 'TenQTLV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'T'#234'n'
          Width = 200
        end>
      DropDownBox.ListSource = HrDataMain.DsLOAI_THOIVIEC
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      KeyField = 'MaQTLV'
      ListField = 'TenQTLV'
      ListSource = HrDataMain.DsLOAI_THOIVIEC
      ParentFont = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 1
      Visible = True
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 899
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton2: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 29
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  inline frEmp: TfrEmp
    Left = 0
    Top = 36
    Width = 899
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 899
    inherited Panel1: TPanel
      Width = 899
      ExplicitWidth = 899
    end
  end
  object Panel2: TisPanel
    Left = 0
    Top = 155
    Width = 899
    Height = 194
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    ParentBackground = False
    TabOrder = 3
    HeaderCaption = ' .: Th'#244'ng tin ho'#224'n tr'#7843' Thi'#7871't b'#7883' - T'#224'i s'#7843'n'
    HeaderColor = clHighlight
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = ANSI_CHARSET
    HeaderFont.Color = clWhite
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object PaLydo2: TPanel
      Left = 2
      Top = 18
      Width = 895
      Height = 31
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        895
        31)
      object Label2: TLabel
        Left = 631
        Top = 7
        Width = 148
        Height = 16
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#224'y tr'#7843' t'#224'i s'#7843'n cu'#7889'i c'#249'ng'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 531
      end
      object wwDBDateTimePicker1: TwwDBDateTimePicker
        Left = 785
        Top = 4
        Width = 101
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        AutoSize = False
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        Color = clBtnFace
        DataField = 'NgayTraTaiSan'
        DataSource = FrmHoSonv.DsDMNV
        Epoch = 1950
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        ShowButton = True
        TabOrder = 0
      end
      object EdMaThietBi: TDBEditEh
        Left = 8
        Top = 4
        Width = 236
        Height = 22
        TabStop = False
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clBtnFace
        ControlLabel.Caption = 'M'#7903' danh s'#225'ch c'#7845'p/tr'#7843' t'#224'i s'#7843'n'
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DynProps = <>
        EditButtons = <
          item
            Action = CmdThietBiTaiSan
            DefaultAction = False
            Style = ebsEllipsisEh
            Width = 20
            DrawBackTime = edbtWhenHotEh
          end>
        EmptyDataInfo.Text = 'M'#7903' danh s'#225'ch c'#7845'p/tr'#7843' t'#224'i s'#7843'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        Visible = True
      end
    end
    object GrList: TwwDBGrid2
      Left = 2
      Top = 49
      Width = 895
      Height = 143
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'XacNhan_TinhTrang;ImageIndex;Original Size'
        'BaoCao_TinhTrang;ImageIndex;Original Size')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      TabOrder = 2
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      ImageList = DataMain.ImageStatus
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
  end
  object isPanel1: TisPanel
    Left = 0
    Top = 349
    Width = 899
    Height = 104
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    ParentBackground = False
    TabOrder = 4
    HeaderCaption = ' .: Ghi ch'#250
    HeaderColor = clHighlight
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = ANSI_CHARSET
    HeaderFont.Color = clWhite
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object DBMemo1: TDBMemo
      Left = 2
      Top = 18
      Width = 895
      Height = 84
      Align = alClient
      BorderStyle = bsNone
      DataField = 'GhiChu_ThoiViec'
      DataSource = FrmHoSonv.DsDMNV
      TabOrder = 1
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 326
    Top = 48
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdChecked: TAction
      Caption = 'Duy'#7879't'
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      OnExecute = CmdEditExecute
    end
    object CmdThietBiTaiSan: TAction
      Caption = 'M'#7903' danh s'#225'ch c'#7845'p/tr'#7843' t'#224'i s'#7843'n'
      OnExecute = CmdThietBiTaiSanExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = spMaster
    Left = 316
    Top = 328
  end
  object spMaster: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterOpen = spMasterAfterOpen
    ProcedureName = 'spHR_LAY_LICHSU_THIETBI_TAISAN;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NgayDangKyThoiViec'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 312
    Top = 304
  end
end
