object FrmKhenThuongKyLuat: TFrmKhenThuongKyLuat
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Khen Th'#432#7903'ng - K'#7927' Lu'#7853't'
  ClientHeight = 565
  ClientWidth = 915
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 915
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnDangKy: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdNewList
      ImageIndex = 11
    end
    object ToolButton13: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton15: TToolButton
      Left = 418
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 426
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 544
    Width = 915
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 915
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
      DesignSize = (
        915
        48)
      inherited CbOrg: TfcTreeCombo
        Left = 470
        Width = 432
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitLeft = 470
        ExplicitWidth = 432
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 415
    Top = 125
    Width = 500
    Height = 419
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 3
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 492
      Height = 419
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PaThongTin: TisPanel
        Left = 2
        Top = 2
        Width = 488
        Height = 210
        HelpType = htKeyword
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin Khen th'#432#7903'ng - K'#7927' lu'#7853't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object gbViecLam: TGroupBox
          Left = 0
          Top = 16
          Width = 488
          Height = 140
          Align = alTop
          Caption = '  Th'#244'ng tin  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object PaNhanVien: TPanel
            Left = 2
            Top = 15
            Width = 484
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            DesignSize = (
              484
              24)
            object CbNhanVien: TDbLookupComboboxEh2
              Left = 122
              Top = 0
              Width = 275
              Height = 22
              ControlLabel.Width = 56
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'Manv'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'Tennv'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 130
                end
                item
                  FieldName = 'ManvQL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 42
                end>
              DropDownBox.ListSource = DsEmp
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Manv'
              ListField = 'Tennv'
              ListSource = HrDataMain.DsDMNV
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object EdManvQL: TDBEditEh
              Left = 400
              Top = 0
              Width = 77
              Height = 22
              TabStop = False
              Alignment = taLeftJustify
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_ManvQL'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
          end
          object PaInfo: TPanel
            Left = 2
            Top = 39
            Width = 484
            Height = 106
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            DesignSize = (
              484
              106)
            object Label2: TLabel
              Left = 39
              Top = 4
              Width = 77
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y hi'#7879'u l'#7921'c'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label108: TLabel
              Left = 72
              Top = 28
              Width = 44
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y k'#253
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object dpNgayHieuLuc: TwwDBDateTimePicker
              Left = 122
              Top = 0
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayHieuLuc'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ShowButton = True
              TabOrder = 0
            end
            object wwDBEdit5: TDBEditEh
              Left = 312
              Top = 0
              Width = 165
              Height = 22
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 78
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' quy'#7871't '#273#7883'nh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'SoQuyetDinh'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              EmptyDataInfo.Color = clInfoBk
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object CbDate: TwwDBDateTimePicker
              Left = 122
              Top = 24
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayKy'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 2
            end
            object DBNumberEditEh1: TDBNumberEditEh
              Left = 376
              Top = 24
              Width = 101
              Height = 22
              ControlLabel.Width = 40
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' ti'#7873'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'SoTien'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object EdNguoiKy_Manv: TDBEditEh
              Left = 122
              Top = 48
              Width = 355
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              AutoSelect = False
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 49
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i k'#253
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiKy'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              EmptyDataInfo.Color = clInfoBk
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object CbLyDo: TDbLookupComboboxEh2
              Left = 122
              Top = 72
              Width = 251
              Height = 22
              ControlLabel.Width = 30
              ControlLabel.Height = 16
              ControlLabel.Caption = 'L'#253' do'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaKThuongKLuat'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'TenKThuongKLuat'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsLYDO_KTKL
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdLyDoKThuongKLuat
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaKThuongKLuat'
              ListField = 'TenKThuongKLuat'
              ListSource = HrDataMain.DsLYDO_KTKL
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 5
              Visible = True
            end
            object EdKTKLLoai: TDBEditEh
              Left = 376
              Top = 72
              Width = 101
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabel.Width = 3
              ControlLabel.Height = 13
              ControlLabel.Caption = ' '
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_TenLoaiKThuongKLuat'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 6
              Visible = True
            end
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 156
          Width = 488
          Height = 48
          Align = alTop
          Caption = '  B'#7843'ng l'#432#417'ng/ Thu'#7871' TNCN  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          DesignSize = (
            488
            48)
          object Label1: TLabel
            Left = 11
            Top = 19
            Width = 107
            Height = 16
            Alignment = taRightJustify
            Caption = 'Thu'#7897'c th'#225'ng l'#432#417'ng'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object rDBCheckBox1: TrDBCheckBox
            Left = 355
            Top = 17
            Width = 124
            Height = 22
            Anchors = [akTop, akRight]
            Caption = 'C'#243' ch'#7883'u thu'#7871' TNCN'
            DataField = 'Co_ThueTNCN'
            DataSource = DsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            ShowFieldCaption = False
            UpdateAfterClick = True
          end
          object CbMon: TwwDBComboBox
            Left = 124
            Top = 17
            Width = 43
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Thang'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object CbYear: TwwDBComboBox
            Left = 170
            Top = 17
            Width = 55
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Nam'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
      end
      object PaGhiChu: TisPanel
        Left = 2
        Top = 266
        Width = 488
        Height = 151
        Align = alClient
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 484
          Height = 131
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
        end
      end
      object PaDinhKem: TisPanel
        Left = 2
        Top = 212
        Width = 488
        Height = 54
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: File '#273#237'nh k'#232'm'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          488
          54)
        object EdFileAttach: TDBEditEh
          Tag = 1
          Left = 124
          Top = 25
          Width = 355
          Height = 22
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Calc_FileName'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <
            item
              Action = CmdFilePlus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileMinus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 1
              Images.HotIndex = 1
              Images.PressedIndex = 1
              Images.DisabledIndex = 1
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileView
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 2
              Images.HotIndex = 2
              Images.PressedIndex = 2
              Images.DisabledIndex = 2
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 125
    Width = 415
    Height = 419
    Align = alClient
    TabOrder = 2
    object GrList: TwwDBGrid2
      Left = 1
      Top = 46
      Width = 413
      Height = 372
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'Manv'#9'12'#9'M'#227#9'F'
        'LK_TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
        'NgayKy'#9'12'#9'K'#253#9'F'#9'Ng'#224'y'
        'NgayHieuLuc'#9'12'#9'Hi'#7879'u l'#7921'c'#9'F'#9'Ng'#224'y'
        'NgayHetHan'#9'12'#9'H'#7871't h'#7841'n'#9'F'#9'Ng'#224'y'
        'GhiChu'#9'25'#9'Ghi ch'#250#9'F')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopCommon
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      OnEnter = CmdRefreshExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 413
      Height = 45
      Align = alTop
      TabOrder = 0
      object rgObsolete: TRadioGroup
        Left = 1
        Top = 1
        Width = 421
        Height = 43
        Align = alLeft
        Caption = 'Lo'#7841'i'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'T'#7845't c'#7843
          'Khen th'#432#7903'ng'
          'K'#7927' lu'#7853't')
        TabOrder = 0
      end
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 84
    Width = 915
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 84
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
      OnExecute = CmdPrint2Execute
    end
    object CmdLyDoKThuongKLuat: TAction
      OnExecute = CmdLyDoKThuongKLuatExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Caption = ' '
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
    object CmdNewList: TAction
      Caption = #272#259'ng k'#253
      Hint = #272#259'ng k'#253' h'#224'ng lo'#7841't'
      OnExecute = CmdNewListExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    AfterDelete = QrMasterAfterDelete
    OnCalcFields = QrMasterCalcFields
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_KTKL'
      'where'#9'1=1')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
    end
    object QrMasterNgayKy: TDateTimeField
      FieldName = 'NgayKy'
    end
    object QrMasterNgayHieuLuc: TDateTimeField
      FieldName = 'NgayHieuLuc'
      OnChange = QrMasterNgayHieuLucChange
    end
    object QrMasterNgayHetHan: TDateTimeField
      FieldName = 'NgayHetHan'
    end
    object QrMasterFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrMasterFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrMasterMaMaKThuongKLuat: TWideStringField
      FieldName = 'MaKThuongKLuat'
      OnChange = QrMasterMaMaKThuongKLuatChange
    end
    object QrMasterLK_TENNV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'MANV'
      LookupResultField = 'TENNV'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterSoQuyetDinh: TWideStringField
      FieldName = 'SoQuyetDinh'
      Size = 50
    end
    object QrMasterNguoiKy: TWideStringField
      FieldName = 'NguoiKy'
      Size = 50
    end
    object QrMasterKTKLLoai: TIntegerField
      FieldName = 'KThuongKLuatLoai'
    end
    object QrMasterNoiDung: TWideStringField
      FieldName = 'NoiDung'
      Size = 200
    end
    object QrMasterSoTien: TFloatField
      FieldName = 'SoTien'
    end
    object QrMasterLK_TEN_KThuongKLuat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenKThuongKLuat'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'TenKThuongKLuat'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrMasterLK_TEN_LOAI_KThuongKLuat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenLoaiKThuongKLuat'
      LookupDataSet = HrDataMain.QrLOAI_LYDO_KTKL
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'KThuongKLuatLoai'
      Lookup = True
    end
    object QrMasterCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrMasterThang: TIntegerField
      DisplayLabel = 'Thu'#7897'c th'#225'ng l'#432#417'ng'
      FieldName = 'Thang'
    end
    object QrMasterNam: TIntegerField
      DisplayLabel = 'Thu'#7897'c n'#259'm l'#432#417'ng'
      FieldName = 'Nam'
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterLK_KThuongKLuatLoai: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_KThuongKLuatLoai'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'KThuongKLuatLoai'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrMasterLK_Co_ThueTNCN: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_ThueTNCN'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'Co_ThueTNCN'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrMasterCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Calculated = True
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'FileIdx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :FileIdx')
    Left = 188
    Top = 348
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 50
    Top = 208
  end
  object CHECK_FILE_USED: TADOCommand
    CommandText = 'select 1 from HR_LICHSU_KTKL where FileIdx=:FileIdx'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'FileIdx'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    Left = 248
    Top = 268
  end
end
