﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HrData;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Db, ADODB, Wwintl, isEnv,
  AdvMenuStylers, AdvMenus, ImgList, isLogin, NativeXml;

type
  THrDataMain = class(TDataModule)
    QrDM_CHUCVU: TADOQuery;
    QrDM_NHOMLV: TADOQuery;
    QrDM_CHUCDANH: TADOQuery;
    QrDM_CALAMVIEC: TADOQuery;
    HR_DM_HOPDONG: TADOQuery;
    QrDM_LYDO_VANGMAT: TADOQuery;
    QrDMPHONGBAN: TADOQuery;
    QrDMBOPHAN: TADOQuery;
    spHR_Invalid_Manv_By_Function: TADOCommand;
    QrNHOM_PHUCAP: TADOQuery;
    QrLOAI_CANGHI: TADOQuery;
    QrLOAI_GIOITINH: TADOQuery;
    QrV_QUOCTICH: TADOQuery;
    QrV_DANTOC: TADOQuery;
    QrV_TONGIAO: TADOQuery;
    QrV_VANHOA: TADOQuery;
    QrLOAI_THOIVIEC: TADOQuery;
    spHR_CAPNHAT_BANG_CONGLUONG: TADOCommand;
    QrDMTK_HR: TADOQuery;
    QrDmNhacViec: TADOQuery;
    QrLYDO_KTKL: TADOQuery;
    QrLOAI_LYDO_KTKL: TADOQuery;
    QrDM_NOILV: TADOQuery;
    QrDM_NGUONTUYENDUNG: TADOQuery;
    QrDM_LYDO_TUYENDUNG: TADOQuery;
    QrDMNOIKHAMBENH: TADOQuery;
    QrV_HR_NHANVIEN_DOITUONG: TADOQuery;
    QrV_HONNHAN: TADOQuery;
    QrV_BHXH_NOICAP: TADOQuery;
    QrV_BHYT_NOICAP: TADOQuery;
    QrV_HR_CCCD_NOICAP: TADOQuery;
    QrV_HR_HOCHIEU_LOAI: TADOQuery;
    QrV_HR_DANG_TINHTRANG: TADOQuery;
    QrV_HR_DOAN_TINHTRANG: TADOQuery;
    QrV_HR_QUANHE: TADOQuery;
    QrV_HR_TAILIEU: TADOQuery;
    QrV_HR_CHUNGCHI: TADOQuery;
    QrDM_HR_PHUCAP: TADOQuery;
    QrV_HR_PHUCAP_LOAI: TADOQuery;
    QrV_HR_NHOMMAU: TADOQuery;
    QrV_HR_DANG_CHIBO: TADOQuery;
    spHr_DM_BACLUONG_Get_HeSoLuong: TADOStoredProc;
    QrDM_BACLUONG: TADOQuery;
    QrDM_THIETBI_TAISAN: TADOQuery;
    QrDM_THIETBI_TAISAN_NHOM: TADOQuery;
    spHR_LICHSU_QTLV_Sync: TADOCommand;
    QrV_HR_QUATRINH_LAMVIEC: TADOQuery;
    DsV_HR_QUATRINH_LAMVIEC: TDataSource;
    DsDM_CHUCDANH: TDataSource;
    DsDM_NOILV: TDataSource;
    DsDM_NHOMLV: TDataSource;
    DsDMPHONGBAN: TDataSource;
    DsDMBOPHAN: TDataSource;
    DsDM_BACLUONG: TDataSource;
    DsV_HR_THIETBI_TAISAN: TDataSource;
    QrTinhTrang_DangKy: TADOQuery;
    DsDM_LYDO_TUYENDUNG: TDataSource;
    DsDM_NGUONTUYENDUNG: TDataSource;
    DsV_HR_DOITUONG_NHANVIEN: TDataSource;
    DsV_HR_NHOMMAU: TDataSource;
    DsV_QUOCTICH: TDataSource;
    DsV_TONGIAO: TDataSource;
    DsV_VANHOA: TDataSource;
    DsV_DANTOC: TDataSource;
    DsLOAI_GIOITINH: TDataSource;
    DsV_HONNHAN: TDataSource;
    DsV_HR_DANG_CHIBO: TDataSource;
    DsV_HR_DANG_TINHTRANG: TDataSource;
    DsV_HR_DOAN_TINHTRANG: TDataSource;
    DsTinhTrang_DangKy: TDataSource;
    DsV_CCCD_NOICAP: TDataSource;
    DsV_BHXH_NOICAP: TDataSource;
    DsV_BHYT_NOICAP: TDataSource;
    DsDMNOIKHAMBENH: TDataSource;
    DsLOAI_THOIVIEC: TDataSource;
    DsV_HR_HOCHIEU_LOAI: TDataSource;
    DsQrDMTK_HR: TDataSource;
    DsV_HR_TAILIEU: TDataSource;
    DsV_HR_CHUNGCHI: TDataSource;
    DsHR_DM_HOPDONG: TDataSource;
    DsLYDO_KTKL: TDataSource;
    DsDM_PHUCAP: TDataSource;
    DsDM_CALAMVIEC: TDataSource;
    DsLOAI_CANGHI: TDataSource;
    spHR_DM_NHANVIEN_Set_NgayTraTaiSan: TADOCommand;
    spHR_Invalid_NgayThoiViec_By_Ngay: TADOCommand;
    spHR_LICHSU_QTLV_Invalid_NgayHieuLuc: TADOCommand;
    ALLOC_MANVQL: TADOCommand;
    QrV_HR_DM_NHANVIEN_CCCD: TADOQuery;
    DsQrV_HR_DM_NHANVIEN_CCCD: TDataSource;
    QrV_HR_DM_NHANVIEN_HOCHIEU: TADOQuery;
    DsQrV_HR_DM_NHANVIEN_HOCHIEU: TDataSource;
    QrV_HR_LOAI_HOPDONG: TADOQuery;
    DsV_HR_LOAI_HOPDONG: TDataSource;
    QrV_HR_CCCD_NHOM: TADOQuery;
    DsV_HR_CCCD_NHOM: TDataSource;
    DsV_HR_HOCHIEU_NHOM: TDataSource;
    QrV_HR_HOCHIEU_NHOM: TADOQuery;
    QrHRNamNgayDauNgayCuoi: TADOQuery;
    QrV_HR_LOAI_NGAY_TINHCONG: TADOQuery;
    SETPARAM: TADOCommand;
    QrDMNV: TADOQuery;
    DsDMNV: TDataSource;
    QrDMNV_DANGLAMVIEC: TADOQuery;
    DsDMNV_DANGLAMVIEC: TDataSource;
    QrV_HR_DAOTAO_NHOM: TADOQuery;
    DsV_HR_DAOTAO_NHOM: TDataSource;
    spHR_CAPNHAT_BANG_LICHLV: TADOCommand;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    mEximPath: String;
  public
//    procedure InitParams(const subsys: Integer = 0);
//    function  Logon(const subsys: Integer = 0): Boolean;
//
//    function  ExecSQL(const sqlStr: String): Integer;
//    function  GetNewGuid: TGuid;
//    function  GetNewSHA1: String;
//
//    function  GetSysParam(param: String): Variant;
    procedure SetSysParam(const ParamName: String; const Value: Variant);
//    function  GetSeqValue(seq: String): Integer;
//	function  GetRights(func: String; denyshow: Boolean = True): Integer; overload;
//	function  GetRights(user, func: String): Integer; overload;
//    function  GetRights(uid: Integer; func: String): Integer; overload;
//	function  GetFuncState(func: String): Boolean;
//
//    function  GetUsername(uid: Integer): String;
//    function  GetUID(username: String ): Integer;


    //HR
    function  GetPassCode(pMakho: String): String;
    function  UpdatePayrollSheet(m, y: Integer; pDaily: Boolean): Boolean;
    function  GetPeriodStatus(m, y: Integer; t: Integer): Boolean;
    function  GetPeriodDate(pYear, pMon: Integer; var pFromDate: TDateTime; var pToDate: TDateTime): Boolean;
    function  SyncQuaTrinh(pManv: String;
        pNhomDacBiet: Integer; //--1: Tuyển mới; 2: Nghỉ việc; 0: Thuyên chuyển
        pLoai: Integer = 0): Boolean; //--0: Hồ sơ --> Quá trình; --1: Quá trình  --> Hồ sơ

    function GetHeSoLuong(pSender: TField): Double;
    function  AllocManvQL(pMaNhomLv: String): String;
    function  ManvValid(pMANV, pFunc: String; pDate: TDateTime): Integer;
    function capNhatHoSoNhanSuNgayTraTaiSan(sManv: string): Boolean;
    function kiemTraNgayNghiChuaThoiViec(sManv: string; pDateTo: TDateTime): Boolean;
    function kiemTraNgayHieuLucHopLe(sManv: string; pNhomDatBiet: Integer; pNgayHieuLuc: TDateTime): Boolean;
    function fnHRNamNgayDauNgayCuoi(pNam: Integer; pChucNang: String; var pNgayDauNam, pNgayCuoiNam: TDateTime): Boolean;
    function  UpdateScheduleSheet(m, y: Integer; pDaily: Boolean): Boolean;
  end;

var
  HrDataMain: THrDataMain;

implementation

uses
	HrExCommon, Rights, isMsg, isDb, Variants, isCommon, GuidEx, RepEngine,
    isOdbc, isStr, isType, isLib, AdminData, isEnCoding, isFile, MainData, ExCommon;



{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure THrDataMain.DataModuleCreate(Sender: TObject);
begin
    DataMain.DataModuleCreate(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure THrDataMain.DataModuleDestroy(Sender: TObject);
begin
    DataMain.DataModuleDestroy(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure THrDataMain.SetSysParam;
begin
	with SETPARAM do
    begin
        CommandText := Format('update SYS_CONFIG set [%s]=:Value', [ParamName]);
        Parameters.Refresh;
        Parameters[0].Value := Value;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.GetPassCode(pMakho: String): String;
begin
    with TADOQuery.Create(Nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select THELENH from DM_LOCATION where LOC = ' + QuotedStr(pMakho);
        Open;
        Result := Fields[0].AsString;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.UpdatePayrollSheet(m, y: Integer; pDaily: Boolean): Boolean;
var
	_msg: String;
begin
//	Wait(PROCESSING);
    try
        with spHR_CAPNHAT_BANG_CONGLUONG do
        begin
            Parameters.Refresh;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := y;
            Parameters[3].Value := m;
            Parameters[4].Value := pDaily;
            Execute;
            Result := Parameters[0].Value = 0;

            if not Result then
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_ERROR_ACTION;

                ErrMsg(_msg);
            end;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.GetPeriodStatus(m, y, t: Integer): Boolean;
begin
    with TADOQuery.Create(Self) do
    begin
    	Connection :=  DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := Format(
        	'select [KhoaSoCong], [KhoaSoLuong], [KhoaSoLichLV], [DuyetLuong] from HR_BANG_THONGSO where [NAM]=%d and [THANG]=%d', [y, m]);
        Open;
        Result := Fields[t].AsBoolean;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.GetPeriodDate(pYear, pMon: Integer; var pFromDate: TDateTime; var pToDate: TDateTime): Boolean;
var
	_msg: String;
begin
    Result := False;
    try
        with TADOCommand.Create(nil) do
        begin
            Connection := DataMain.Conn;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spHR_THONGSO_CHUKY_CONGLUONG;1';

            Parameters.Refresh;
            Parameters[1].Value := pYear;
            Parameters[2].Value := pMon;
            Parameters[3].Value := date;
            Parameters[4].Value := date;
            Execute;

            Result := Parameters[0].Value = 0;
            if Result then
            begin
                pFromDate := Parameters[3].Value;
                pToDate := Parameters[4].Value;
            end else
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_ERROR_ACTION;

                ErrMsg(_msg);
            end;

            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.SyncQuaTrinh(pManv: String; pNhomDacBiet,
  pLoai: Integer): Boolean;
begin
    with spHR_LICHSU_QTLV_Sync do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := pManv;
        Parameters.ParamByName('@Nhom_DacBiet').Value := pNhomDacBiet;
        Parameters.ParamByName('@Loai').Value := pLoai;
        Execute;

        if Parameters.FindParam('@RETURN_VALUE') <> nil then
            Result := (Parameters.ParamValues['@RETURN_VALUE'] = 0)
        else
            Result := False;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.GetHeSoLuong(pSender: TField): Double;
var
    lDataSet: TDataSet;
    pHeSoLuong: Double;
begin
    lDataSet := pSender.DataSet;
    pHeSoLuong := 0;
    with lDataSet do
    begin
        if (FieldByName('MaBacLuong').AsString = '') or
            (FieldByName('BacLuong').AsInteger = 0) then
                Exit;
    end;

    with spHr_DM_BACLUONG_Get_HeSoLuong do
    begin
        Close;
        Parameters[1].Value := lDataSet.FieldByName('Manv').AsString;
        Parameters[2].Value := lDataSet.FieldByName('MaBacLuong').AsString;
        Parameters[3].Value := lDataSet.FieldByName('BacLuong').AsInteger;
        try
            Open;
            if not IsEmpty then
            pHeSoLuong := FieldByName('HeSoLuong').AsFloat;
        except
            on E: Exception do
                ErrMsg(E.Message);
        end;
        Close;
    end;

    Result := pHeSoLuong;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.AllocManvQL(pMaNhomLv: String): String;
begin
    with ALLOC_MANVQL do
    begin
        Prepared := True;
        Parameters[1].Value := pMaNhomLv;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.ManvValid;
begin
	with spHR_Invalid_Manv_By_Function do
    begin
    	Prepared := True;
        Parameters.ParamByName('@MACN').Value := sysSite;
        Parameters.ParamByName('@MANV').Value := pMANV;
        Parameters.ParamByName('@NGAY').Value := pDate;
        Parameters.ParamByName('@FUNC').Value := pFunc;
//        Parameters[5].Value := 1; // sysFemale;
        Execute;
        Result := Parameters.ParamValues['@RETURN_VALUE'];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.capNhatHoSoNhanSuNgayTraTaiSan(sManv: string): Boolean;
var
    s: String;
begin
    with spHR_DM_NHANVIEN_Set_NgayTraTaiSan do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := sManv;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.kiemTraNgayNghiChuaThoiViec(sManv: string; pDateTo: TDateTime): Boolean;
var
    s: String;
begin
    with spHR_Invalid_NgayThoiViec_By_Ngay do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := sManv;
        Parameters.ParamByName('@DenNgay').Value := pDateTo;
        Execute;

        Result := (Parameters.ParamValues['@RETURN_VALUE'] = 0);
        if not Result then
        begin
            if (Parameters.FindParam('@pMSG') <> nil) then
                s := getTranslateByResource(Parameters.ParamValues['@pMSG']);

            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.kiemTraNgayHieuLucHopLe(sManv: string; pNhomDatBiet: Integer; pNgayHieuLuc: TDateTime): Boolean;
var
    s: String;
begin
    with spHR_LICHSU_QTLV_Invalid_NgayHieuLuc do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := sManv;
        Parameters.ParamByName('@QTLV_Nhom_DacBiet').Value := pNhomDatBiet;
        Parameters.ParamByName('@NgayHieuLuc').Value := pNgayHieuLuc;
        Execute;
        Result := (Parameters.ParamValues['@RETURN_VALUE'] = 0);

        if not Result then
        begin
            if (Parameters.FindParam('@pMSG') <> nil) then
                s := getTranslateByResource(Parameters.ParamValues['@pMSG']);
            ErrMsg(s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.fnHRNamNgayDauNgayCuoi;
begin
  	with QrHRNamNgayDauNgayCuoi do
    begin
    	Parameters[0].Value := pNam;
    	Parameters[1].Value := pChucNang;
        Open;
        pNgayDauNam := Fields[1].AsDateTime;
        pNgayCuoiNam := Fields[2].AsDateTime;
        Result := True;
        Close;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function THrDataMain.UpdateScheduleSheet(m, y: Integer; pDaily: Boolean): Boolean;
var
	_msg: String;
begin
//	Wait(PROCESSING);
    try
        with spHR_CAPNHAT_BANG_LICHLV do
        begin
            Parameters.Refresh;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := y;
            Parameters[3].Value := m;
            Parameters[4].Value := pDaily;
            Execute;
            Result := Parameters[0].Value = 0;

            if not Result then
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_ERROR_ACTION;

                ErrMsg(_msg);
            end;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;


end.
