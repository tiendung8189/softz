﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyVangmatTools;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, rDBComponents, Vcl.Buttons, DBCtrlsEh, DBGridEh,
  DBLookupEh, frameEmp, DbLookupComboboxEh2, fcCombo, kbmMemTable;

type
  TFrmDangkyVangmatTools = class(TForm)
    ActionList: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdImport: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdClear: TAction;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    Panel1: TPanel;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    DsEmp: TDataSource;
    Panel3: TPanel;
    BtnContinute: TBitBtn;
    BitBtn2: TBitBtn;
    PaThongTinThem: TPanel;
    GrList: TStringGrid;
    PopList: TAdvPopupMenu;
    ImporttExcel1: TMenuItem;
    N1: TMenuItem;
    Xadanhsch1: TMenuItem;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    RgLoai: TRadioGroup;
    Panel2: TPanel;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaPhongBan: TDBEditEh;
    EdMaBoPhan: TDBEditEh;
    CbNhanVien: TDbLookupComboboxEh2;
    EdMaNhanVien: TDBEditEh;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSec: TDataSource;
    DsDep: TDataSource;
    DsSite: TDataSource;
    CmdContinue: TAction;
    CmdLyDoVangMat: TAction;
    spIMP_HR_DANGKY_VANGMAT_ImportList: TADOCommand;
    TbDummy: TkbmMemTable;
    DsDummy: TDataSource;
    TbDummyGhiChu: TWideMemoField;
    PaInfo: TisPanel;
    gbThongTin: TGroupBox;
    PaChiTiet: TPanel;
    Label108: TLabel;
    Label1: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    CbDate: TwwDBDateTimePicker;
    CbToDate: TwwDBDateTimePicker;
    CbCaLamViec: TDbLookupComboboxEh2;
    dpGioVao: TwwDBDateTimePicker;
    dpGioRa1: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    CbDauCa: TDbLookupComboboxEh2;
    CbLyDoNghiLan1: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    PNTinhTrang: TisPanel;
    Label5: TLabel;
    wwDBEdit7: TDBEditEh;
    CbbXetDuyet_TinhTrang: TDbLookupComboboxEh2;
    CbXetDuyet: TDbLookupComboboxEh2;
    EdXetDuyetManvQL: TDBEditEh;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    QrTinhTrang_XetDuyet: TADOQuery;
    DsTinhTrang_XetDuyet: TDataSource;
    TbDummyTuNgay: TDateTimeField;
    TbDummyDenNgay: TDateTimeField;
    TbDummyMaCa: TWideStringField;
    TbDummyDauCa: TIntegerField;
    TbDummyMaVangMat: TWideStringField;
    TbDummyGioVao_Ca: TDateTimeField;
    TbDummyGioRa_Ca: TDateTimeField;
    TbDummySoGio_Ca: TFloatField;
    TbDummyXetDuyet_Manv: TWideStringField;
    TbDummyXetDuyet_GhiChu: TWideStringField;
    TbDummyXetDuyet_TinhTrang: TIntegerField;
    TbDummyXetDuyet_Ngay: TDateTimeField;
    TbDummyLK_XetDuyet_ManvQL: TWideStringField;
    QrListLeave: TADOQuery;
    DsListLeave: TDataSource;
    DsEmpStatus: TDataSource;
    QrEmpStatus: TADOQuery;
    TbDummyNgay: TDateTimeField;
    TbDummyLK_GioVao: TDateTimeField;
    TbDummyLK_GioRa: TDateTimeField;
    TbDummyLK_SoGio: TFloatField;
    TbDummyXetDuyet_TinhTrangBy: TIntegerField;
    TbDummyIdxTransNo: TGuidField;
    CmdImportExcel: TAction;
    Panel7: TPanel;
    Panel8: TPanel;
    BitBtn4: TBitBtn;
    CmdRegister: TAction;
    Panel9: TPanel;
    CHECK_RECORD_ERROR: TADOCommand;
    spIMP_HR_DANGKY_VANGMAT_InsertData: TADOCommand;
    spIMP_HR_DANGKY_VANGMAT_DeleteList: TADOCommand;
    CmdDelRecordError: TAction;
    CmdDelRecordSelected: TAction;
    CmdRefresh: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Xadng1: TMenuItem;
    Xaccdngangli1: TMenuItem;
    GrListData: TwwDBGrid2;
    QrList: TADOQuery;
    QrListGhiChu: TWideMemoField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListXacNhan_TenTinhTrang: TWideStringField;
    QrListXetDuyet_TenTinhTrang: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_Tennv: TWideStringField;
    QrListManv: TWideStringField;
    QrListManvQL: TWideStringField;
    QrListTennv: TWideStringField;
    QrListTuNgay: TDateTimeField;
    QrListDenNgay: TDateTimeField;
    QrListSoNgay: TFloatField;
    QrListXetDuyet_Manv: TWideStringField;
    QrListXacNhan_Manv: TWideStringField;
    QrListTenVangMat: TWideStringField;
    QrListXacNhan_TinhTrang: TIntegerField;
    QrListXacNhan_Tennv: TWideStringField;
    QrListXetDuyet_TinhTrang: TIntegerField;
    QrListXetDuyet_Tennv: TWideStringField;
    QrListMaVangMat: TWideStringField;
    QrListXetDuyet_ManvQL: TWideStringField;
    DsList: TDataSource;
    Filter: TwwFilterDialog2;
    QrTinhTrang_XacNhan: TADOQuery;
    DsTinhTrang_XacNhan: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdClearExecute(Sender: TObject);
    procedure CmdLyDoVangMatExecute(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbPhongBanExit(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdContinueExecute(Sender: TObject);
    procedure TbDummyAfterInsert(DataSet: TDataSet);
    procedure TbDummyMaCaChange(Sender: TField);
    procedure TbDummyDauCaChange(Sender: TField);
    procedure TbDummyTuNgayChange(Sender: TField);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure CmdDelRecordSelectedExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrListDataCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
  private
  	mCanEdit, mIsFunction: Boolean;
    mTransNo, countError, countSubmit: Integer;
    r: WORD;
    mFileIdx: TGUID;
    mFORM_CODE: string;
    procedure SmartFocus;
    procedure DeleteRow(pRow: Integer);
    procedure AddRow(p1, p2: String);
    procedure RegisterData(pLst: String);
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; FORM_CODE: string; var pTransNo: Integer) : Boolean;
  end;

var
  FrmDangkyVangmatTools: TFrmDangkyVangmatTools;

const
    TABLE_NAME = 'IMP_HR_DANGKY_VANGMAT';
    FORM_CODE = 'IMP_HR_DANGKY_VANGMAT';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmVangmat, Clipbrd, HrData; //MassRegLeave;

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyVangmatTools.Execute;
begin
    mCanEdit := rCanEdit(r);
    mTransNo := 0;
    mFORM_CODE := FORM_CODE;
    Result := ShowModal = mrOK;
    if Result then
    begin
        pTransNo := mTransNo;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.FormCreate(Sender: TObject);
begin
    AddAllFields(QrList, TABLE_NAME);

    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
    mIsFunction := GetFuncState('HR_VANGMAT_DUYET');
    PNTinhTrang.Visible := mIsFunction;

    if mIsFunction then
        PNTinhTrang.Enabled := GetRights('HR_VANGMAT_DUYET', False) <> R_DENY
    else
    begin
         PaEmp.Height := PaEmp.Height - PNTinhTrang.Height;
         PaThongTinThem.Height :=  PaEmp.Height;
         Height := Height - PNTinhTrang.Height;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;

    with HrDataMain do
        OpenDataSets([QrDMBOPHAN, QrDMNV_DANGLAMVIEC, QrDM_LYDO_VANGMAT]);

	OpenDataSets([QrSite, QrDep, QrSec, QrEmp, QrTinhTrang_XetDuyet, QrList, QrTinhTrang_XacNhan, QrListLeave, QrEmpStatus]);


    SetDisplayFormat(TbDummy, ctCurFmt);
    SetShortDateFormat(TbDummy, ShortDateFormat);
    SetDisplayFormat(TbDummy, ['GioVao_Ca', 'GioRa_Ca'], sysMinFmt);
    SetDisplayFormat(TbDummy, ['XetDuyet_Ngay'], DateTimeFmt);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrListData);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;

    countSubmit:= 0;

    lookupComboboxEhShowImages(QrTinhTrang_XetDuyet, CbbXetDuyet_TinhTrang, 'TEN_HOTRO', 0);

    if not mIsFunction then
        grRemoveFields(GrListData, ['XetDuyet_Manv', 'XetDuyet_ManvQL', 'XetDuyet_Tennv',
        'XetDuyet_TinhTrang', 'XetDuyet_TenTinhTrang']);


    with TbDummy do
    begin
        Open;
    	Append;
    end;

    RgLoai.OnClick(Nil);
    CbDate.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.GrListDataCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    TbDummy.Cancel;

    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

	CloseDataSets([TbDummy, QrEmp, QrList, QrTinhTrang_XacNhan, QrTinhTrang_XetDuyet, QrListLeave, QrEmpStatus]);

    if countSubmit > 0 then
        ModalResult := mrOk;

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdImportExecute(Sender: TObject);
var
    ls: TStringList;
    i: Integer;
    s, sName: String;
begin
    ls := TStringList.Create;
    ls.Text := Clipboard.AsText;
    for i := 0 to ls.Count - 1 do
    begin
        s := Trim(ls[i]);
        if (s <> '') then
        begin
            with QrEmp do
            begin
                Filter := 'ManvQL=' + QuotedStr(s);
                sName := FieldByName('Tennv').AsString;
            end;

            AddRow(s, sName);
        end;
    end;
    QrEmp.Filter := '';
    ls.Free;
    Clipboard.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdInsExecute(Sender: TObject);
var
	s1, s2: String;
begin
    case RgLoai.ItemIndex of
    0:
    	begin
            s1 := CbPhongBan.Value;
        	s2 := CbPhongBan.Text;
        end;
    1:
    	begin
            s1 := CbBoPhan.Value;
        	s2 := CbBoPhan.Text;
        end;
    2:
    	begin
            s1 := CbNhanVien.Value;
        	s2 := CbNhanVien.Text;
        end;
    else
    	Exit;
    end;

    AddRow(s1, s2);
	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdDelExecute(Sender: TObject);
begin
	DeleteRow(GrList.Row);
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdDelRecordErrorExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdDelRecordSelectedExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdLyDoVangMatExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_VANGMAT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmVangmat, FrmDmVangmat);
    FrmDmVangmat.Execute(r);
    QrListLeave.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_DANGKY_VANGMAT_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                mTransNo := 0;
                CmdRefresh.Execute;
                Inc(countSubmit);
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdContinueExecute(Sender: TObject);
var
    pLst: String;
    i: Integer;
begin
    with TbDummy do
    begin
        if BlankConfirm(TbDummy, ['TuNgay', 'DenNgay', 'MaCa', 'DauCa',  'MaVangMat', 'XetDuyet_Manv']) then
            Abort;

//        if mIsFunction and BlankConfirm(TbDummy, ['XetDuyet_Manv', 'XetDuyet_Ngay']) then
//            Abort;

        if FieldByName('DenNgay').AsDateTime < FieldByName('TuNgay').AsDateTime then
        begin
            ErrMsg(RS_PERIOD);
            CbToDate.SetFocus;
            Abort;
        end;

    end;

    pLst := '';
    with GrList do
    for i := 0 to RowCount - 1 do
    begin
        if Cells[0, i] = '' then
            Break;
        if pLst <> '' then
            pLst := pLst + ',';
        pLst := pLst + '''' + Cells[0, i] + '''';
    end;

    RegisterData(pLst);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyList, bIsError: Boolean;
    s: String;
begin
    with TbDummy do
    begin

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    with QrList do
    begin
    	if not Active then
        	Exit;
        bEmptyList := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdSearch.Enabled := not bEmptyList;
    CmdDelRecordSelected.Enabled := not bEmptyList;
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmptyList);

    CmdContinue.Enabled := (GrList.Cells[0, 0] <> '');
    CmdClear.Enabled := (GrList.Cells[0, 0] <> '');
    CmdDel.Enabled := (GrList.Cells[0, GrList.Row] <> '');
	RgLoai.Enabled := (GrList.Cells[0, 0] = '');
    CmdImport.Enabled := (RgLoai.ItemIndex = 2);

    case RgLoai.ItemIndex of
    0:
       	s := EdMaPhongBan.Text;
    1:
       	s := EdMaBoPhan.Text;
    2:
       	s := EdMaNhanVien.Text;
    end;
    CmdIns.Enabled := s <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.QrListBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CbPhongBanExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;

	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrDep do
        if Locate('MaPhongBan', s, []) then
        begin
            EdMaPhongBan.Text := FieldByName('Ma').AsString;
            s1 := EdMaBoPhan.Text;
            with QrSec do
            if (s1 <> '') then
            begin
                if  not Locate('MaPhongBan;Ma', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaBoPhan.Text := FieldByName('Ma').AsString;
                CbBoPhan.Value := FieldByName('MaBoPhan').AsString;
            end
            else
            begin
                EdMaBoPhan.Text := '';
                CbBoPhan.Text := '';
            end;
        end
        else
        begin
            EdMaPhongBan.Text := '';
            EdMaBoPhan.Text := '';
            CbBoPhan.Text := '';
        end;
    1:		// bo phan
        with QrSec do
        if (s <> '') and (Locate('MaBoPhan', s, [])) then
        begin
            EdMaBoPhan.Text := FieldByName('Ma').AsString;
        end
        else
        begin
            EdMaBoPhan.Text := '';
        end;
    2:		// nhan vien
		EdMaNhanVien.Text := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaPhongBan.Enabled := n <> 2;
    CbPhongBan.Enabled := n <> 2;

    EdMaBoPhan.Enabled := n = 1;
    CbBoPhan.Enabled := n = 1;

    EdMaNhanVien.Enabled :=  n = 2;
    CbNhanVien.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.SmartFocus;
begin
	try
        case RgLoai.ItemIndex of
        0:
            CbPhongBan.SetFocus;
        1:
            CbBoPhan.SetFocus;
        2:
            CbNhanVien.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.TbDummyAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Date;
    with TbDummy do
    begin
        FieldByName('Ngay').AsDateTime := d;
        FieldByName('TuNgay').AsDateTime := d;
        FieldByName('DenNgay').AsDateTime := d;
        FieldByName('XetDuyet_TinhTrang').AsInteger := 1;
        FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
        FieldByName('XetDuyet_Ngay').AsDateTime := Now;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.TbDummyDauCaChange(Sender: TField);
begin
    with TbDummy do		// Proc: nghi ca ngay
    begin

        if (FieldByName('DauCa').AsInteger = 2) or
            (FieldByName('DauCa').AsInteger = 3) then
        begin
            FieldByName('DenNgay').AsDateTime := FieldByName('TuNgay').AsDateTime;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.TbDummyMaCaChange(Sender: TField);
begin
    with TbDummy do
    begin
        FieldByName('GioVao_Ca').AsDateTime := FieldByName('LK_GioVao').AsDateTime;
        FieldByName('GioRa_Ca').AsDateTime := FieldByName('LK_GioRa').AsDateTime;
        FieldByName('SoGio_Ca').AsFloat := FieldByName('LK_SoGio').AsFloat;

        if (FieldByName('MaCa').AsString = '') or (FieldByName('MaCa').IsNull) then
        begin
            FieldByName('GioVao_Ca').Clear;
        end;
    end;

    TbDummyDauCaChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.TbDummyTuNgayChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    bTrigger := mTrigger;
    mTrigger := True;
    with TbDummy do        // Proc: nghi ca ngay
    begin
        if (FieldByName('TuNgay').AsDateTime > FieldByName('DenNgay').AsDateTime) then
        begin
            FieldByName('DenNgay').AsDateTime := FieldByName('TuNgay').AsDateTime;
        end;
    end;
    TbDummyDauCaChange(Sender);
    mTrigger := bTrigger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.AddRow(p1, p2: String);
var
    i: Integer;
begin
    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := p1;
				Cells[1, i] := p2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = Copy(p1, 1, Length(Cells[0, i])) then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.DeleteRow(pRow: Integer);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, pRow] = '' then
			Exit;

		for i := pRow to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatTools.RegisterData;
var
    s: string;
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    mTransNo := DataMain.GetSeqValue(TABLE_NAME);
	with TbDummy, spIMP_HR_DANGKY_VANGMAT_ImportList do
    begin
        Prepared := True;
        Parameters.ParamByName('@pIdxTransNo').Value := mTransNo;
        Parameters.ParamByName('@pCreateFunc').Value := mFORM_CODE + '_GROUP';
        Parameters.ParamByName('@pCreateSource').Value := sysAppSource;
        Parameters.ParamByName('@pTransNo').Value := mTransNo;
        Parameters.ParamByName('@pLoai').Value := RgLoai.ItemIndex;
        Parameters.ParamByName('@pChuoi').Value := pLst;
        Parameters.ParamByName('@pNgay').Value := FieldByName('Ngay').AsDateTime;
        Parameters.ParamByName('@pTuNgay').Value := FieldByName('TuNgay').AsDateTime;
        Parameters.ParamByName('@pDenNgay').Value := FieldByName('DenNgay').AsDateTime;
        Parameters.ParamByName('@pMaCa').Value := FieldByName('MaCa').AsString;
        Parameters.ParamByName('@pDauCa').Value := FieldByName('DauCa').AsString;
        Parameters.ParamByName('@pMaVangMat').Value := FieldByName('MaVangMat').AsString;
        Parameters.ParamByName('@pXetDuyet_Manv').Value := FieldByName('XetDuyet_Manv').AsString;
        Parameters.ParamByName('@pXetDuyet_TinhTrangBy').Value := FieldByName('XetDuyet_TinhTrangBy').AsInteger;
        Parameters.ParamByName('@pXetDuyet_TinhTrang').Value := FieldByName('XetDuyet_TinhTrang').AsInteger;
        Parameters.ParamByName('@pXetDuyet_GhiChu').Value := FieldByName('XetDuyet_GhiChu').AsString;
        Parameters.ParamByName('@pXetDuyet_Ngay').Value := FieldByName('XetDuyet_Ngay').AsDateTime;
        Parameters.ParamByName('@pGhiChu').Value := FieldByName('GhiChu').AsString;
        Parameters.ParamByName('@pUID').Value := sysLogonUID;
        Parameters.ParamByName('@pLang').Value := sysLang;

        try
            Execute;
        except
        end;

        if Parameters[0].Value <> 0 then
        begin
            mTransNo := 0;
            s := Parameters.ParamValues['@returnCode'];
            ErrMsg(s);
        end
        else
        begin
            QrListBeforeOpen(QrList);
            CmdRefresh.Execute;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyVangmatTools.DeleteAllRecord;
begin
    with spIMP_HR_DANGKY_VANGMAT_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyVangmatTools.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;

end.
