object frEmp: TfrEmp
  Left = 0
  Top = 0
  Width = 520
  Height = 41
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 520
    Height = 41
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    ExplicitWidth = 420
    object Label4: TLabel
      Left = 160
      Top = 12
      Width = 81
      Height = 16
      Alignment = taRightJustify
      Caption = 'M'#227' nh'#226'n vi'#234'n:'
    end
    object LbMASO: TLabel
      Left = 250
      Top = 12
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 345
      Top = 12
      Width = 42
      Height = 16
      Alignment = taRightJustify
      Caption = 'H'#7885' t'#234'n:'
    end
    object LbHOTEN: TLabel
      Left = 394
      Top = 12
      Width = 40
      Height = 16
      Caption = 'HOTEN'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Image1: TImage
      Left = 4
      Top = 4
      Width = 32
      Height = 32
      AutoSize = True
      Transparent = True
    end
    object LbID: TLabel
      Left = 72
      Top = 12
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 48
      Top = 12
      Width = 17
      Height = 16
      Alignment = taRightJustify
      Caption = 'ID:'
    end
  end
end
