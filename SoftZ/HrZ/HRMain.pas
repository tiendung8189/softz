﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HRMain;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ActnList,
  Menus, ComCtrls, ExtCtrls, isEnv, AdvMenus, Jpeg, ExtDlgs, RzGroupBar, StdCtrls,
  RzSplit, fcStatusBar, ADODb, ImgList, fcImager, RzPanel, ToolWin, SystemCriticalU,
  AdvToolBar, NativeXml;

type
  TFrmHRMain = class(TForm)
    MyActionList: TActionList;
    CmdQuit: TAction;
    CmdSetpass: TAction;
    CmdAbout: TAction;
    CmdParams: TAction;
    CmdBc: TAction;
    CmdDmhotro: TAction;
    CmdForceDropDown: TAction;
    ImgLarge: TImageList;
    CmdHelp: TAction;
    CmdSetBkgr: TAction;
    CmdClearBkgr: TAction;
    PicDlg: TOpenPictureDialog;
    CmdSetDateTime: TAction;
    CmdLock: TAction;
    CmdFaq: TAction;
    CmdMyCustomer: TAction;
    PopBkGr: TAdvPopupMenu;
    PopFunc: TAdvPopupMenu;
    ItemCat: TMenuItem;
    Outlook2: TMenuItem;
    TaskList2: TMenuItem;
    N1: TMenuItem;
    ItemCloseAll: TMenuItem;
    PopDanhmuc: TAdvPopupMenu;
    N8: TMenuItem;
    N10: TMenuItem;
    Danhmckhc2: TMenuItem;
    CmdDmdialy: TAction;
    Vtral1: TMenuItem;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton3: TToolButton;
    ToolBar2: TToolBar;
    ToolButton10: TToolButton;
    ToolButton2: TToolButton;
    Panel1: TPanel;
    RzSizePanel1: TRzSizePanel;
    GbFunc: TRzGroupBar;
    GrDanhmuc: TRzGroup;
    GrHethong: TRzGroup;
    PaBkGr: TPanel;
    BkGr: TfcImager;
    PaDesc: TPanel;
    LbDesc2: TLabel;
    Bevel3: TBevel;
    LbDesc1: TLabel;
    Bevel2: TBevel;
    PopDulieu: TAdvPopupMenu;
    PopHethong: TAdvPopupMenu;
    PopTrogiup: TAdvPopupMenu;
    Ktthc2: TMenuItem;
    imtkhu2: TMenuItem;
    Hngdnsdng2: TMenuItem;
    N21: TMenuItem;
    Cchipthnggp1: TMenuItem;
    Danhschkhchhng1: TMenuItem;
    N31: TMenuItem;
    Giithiu2: TMenuItem;
    Status: TfcStatusBar;
    CmdTygia: TAction;
    CmdReconnect: TAction;
    BtBangcong: TToolButton;
    CmdExport: TAction;
    ChitkhutheoloithVIP1: TMenuItem;
    N24: TMenuItem;
    CmdDmTaikhoan: TAction;
    GrPhanQuyen: TRzGroup;
    CmdListUser: TAction;
    CmdListGroup: TAction;
    CmdFunctionRight: TAction;
    CmdReportRight: TAction;
    GrAdmin: TRzGroup;
    CmdAdmin: TAction;
    CmdDmPhongban: TAction;
    CmdProfile: TAction;
    LoithVIP1: TMenuItem;
    Phngban1: TMenuItem;
    Nhnvin1: TMenuItem;
    Khchhng1: TMenuItem;
    CmdConfig: TAction;
    Cuhnhhthng1: TMenuItem;
    N12: TMenuItem;
    RzGroup1: TRzGroup;
    CmdDmCalamviec: TAction;
    CmdDmVangmat: TAction;
    CmdDmChucvu: TAction;
    CmdDmNgayle: TAction;
    CmdDmMCC: TAction;
    CmdVantay: TAction;
    CmdCongtho: TAction;
    CmdBangcong: TAction;
    CmdBangluong: TAction;
    BtBangluong: TToolButton;
    CmdLaydulieucong: TAction;
    CmdDmPhucap: TAction;
    CmdDmQuatrinh: TAction;
    CmdDmNganhang: TAction;
    CmdDmHopdong: TAction;
    CmdDmNhomlv: TAction;
    Ngnhng1: TMenuItem;
    RzGroup2: TRzGroup;
    CmdQuatrinhLamViec: TAction;
    CmdHopDongLaoDong: TAction;
    CmdKhenthuongKyLuat: TAction;
    CmdGiadinh: TAction;
    CmdThietBiTaiSan: TAction;
    CmdTailieu: TAction;
    CmdNghiviec: TAction;
    CmdQuyettoan: TAction;
    CmdVangmat: TAction;
    CmdPhepnam: TAction;
    CmdLichlamviec: TAction;
    CmdNhansuREP: TAction;
    CmdTamung: TAction;
    CmdTinhluongREP: TAction;
    RzGroup3: TRzGroup;
    CmdDmDKKhambenh: TAction;
    Ningkkhmbnh1: TMenuItem;
    N2: TMenuItem;
    CmdThaisan: TAction;
    ToolButton1: TToolButton;
    CmdCongtac: TAction;
    CmdDmBacLuong: TAction;
    CmdDmLyDoKTKL: TAction;
    CmdDmDiaDiemLamViec: TAction;
    CmdDmNguonTuyenDung: TAction;
    CmdDmThietBiTaiSanLaoDong: TAction;
    CmdDmCanhBao: TAction;
    CmdDmBieuMau: TAction;
    CmdDmHotroNS: TAction;
    CmdTangCa: TAction;
    CmdMangThai: TAction;
    CmdThoiviec: TAction;
    CmdTuyendung: TAction;
    CmdDaotao: TAction;
    CmdBcNS: TAction;
    CmdTinhluong: TAction;
    CmdBcNSCong: TAction;
    Bclng1: TMenuItem;
    LdoKhenthngKlut1: TMenuItem;
    aimlmvic1: TMenuItem;
    Nguntuyndng1: TMenuItem;
    hitbTisnlaong1: TMenuItem;
    CmdNghiBu: TAction;
    ToolButton4: TToolButton;
    CmdDataRight: TAction;
    Ngnhng2: TMenuItem;
    aphng1: TMenuItem;
    N3: TMenuItem;
    procedure CmdQuitExecute(Sender: TObject);
    procedure CmdSetpassExecute(Sender: TObject);
    procedure CmdAboutExecute(Sender: TObject);
    procedure CmdParamsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdBcExecute(Sender: TObject);
    procedure CmdForceDropDownExecute(Sender: TObject);
    procedure CmdHelpExecute(Sender: TObject);
    procedure CmdSetBkgrExecute(Sender: TObject);
    procedure CmdClearBkgrExecute(Sender: TObject);
    procedure CmdSetDateTimeExecute(Sender: TObject);
    procedure PopBkGrPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdLockExecute(Sender: TObject);
    procedure CmdFaqExecute(Sender: TObject);
    procedure CmdMyCustomerExecute(Sender: TObject);
    procedure ItemCloseAllClick(Sender: TObject);
    procedure ItemCatClick(Sender: TObject);
    procedure PopFuncPopup(Sender: TObject);
    procedure CmdDmdialyExecute(Sender: TObject);
    procedure TntFormResize(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure CmdListUserExecute(Sender: TObject);
    procedure CmdListGroupExecute(Sender: TObject);
    procedure CmdFunctionRightExecute(Sender: TObject);
    procedure CmdReportRightExecute(Sender: TObject);
    procedure CmdAdminExecute(Sender: TObject);
    procedure CmdDmPhongbanExecute(Sender: TObject);
    procedure CmdConfigExecute(Sender: TObject);
    procedure CmdDmCalamviecExecute(Sender: TObject);
    procedure CmdDmVangmatExecute(Sender: TObject);
    procedure CmdDmChucvuExecute(Sender: TObject);
    procedure CmdDmNgayleExecute(Sender: TObject);
    procedure CmdDmMCCExecute(Sender: TObject);
    procedure CmdProfileExecute(Sender: TObject);
    procedure CmdVantayExecute(Sender: TObject);
    procedure CmdLaydulieucongExecute(Sender: TObject);
    procedure CmdCongthoExecute(Sender: TObject);
    procedure CmdDmNganhangExecute(Sender: TObject);
    procedure CmdDmNhomlvExecute(Sender: TObject);
    procedure CmdDmPhucapExecute(Sender: TObject);
    procedure CmdDmQuatrinhExecute(Sender: TObject);
    procedure CmdDmHopdongExecute(Sender: TObject);
    procedure CmdBangcongExecute(Sender: TObject);
    procedure CmdVangmatExecute(Sender: TObject);
    procedure CmdPhepnamExecute(Sender: TObject);
    procedure CmdLichlamviecExecute(Sender: TObject);
    procedure CmdTygiaExecute(Sender: TObject);
    procedure CmdQuatrinhLamViecExecute(Sender: TObject);
    procedure CmdHopDongLaoDongExecute(Sender: TObject);
    procedure CmdKhenthuongKyLuatExecute(Sender: TObject);
    procedure CmdGiadinhExecute(Sender: TObject);
    procedure CmdThietBiTaiSanExecute(Sender: TObject);
    procedure CmdTailieuExecute(Sender: TObject);
    procedure CmdNghiviecExecute(Sender: TObject);
    procedure CmdQuyettoanExecute(Sender: TObject);
    procedure CmdNhansuREPExecute(Sender: TObject);
    procedure CmdBangluongExecute(Sender: TObject);
    procedure CmdTamungExecute(Sender: TObject);
    procedure CmdTinhluongREPExecute(Sender: TObject);
    procedure CmdDmDKKhambenhExecute(Sender: TObject);
    procedure CmdRepNhansuExecute(Sender: TObject);
    procedure CmdChamcongREPExecute(Sender: TObject);
    procedure CmdThaisanExecute(Sender: TObject);
    procedure CmdDmBacLuongExecute(Sender: TObject);
    procedure CmdDmLyDoKTKLExecute(Sender: TObject);
    procedure CmdDmDiaDiemLamViecExecute(Sender: TObject);
    procedure CmdDmNguonTuyenDungExecute(Sender: TObject);
    procedure CmdDmThietBiTaiSanLaoDongExecute(Sender: TObject);
    procedure CmdDmCanhBaoExecute(Sender: TObject);
    procedure CmdDmBieuMauExecute(Sender: TObject);
    procedure CmdDmHotroNSExecute(Sender: TObject);
    procedure CmdCongtacExecute(Sender: TObject);
    procedure CmdBcNSExecute(Sender: TObject);
    procedure CmdTangCaExecute(Sender: TObject);
    procedure CmdMangThaiExecute(Sender: TObject);
    procedure CmdThoiviecExecute(Sender: TObject);
    procedure CmdTuyendungExecute(Sender: TObject);
    procedure CmdDaotaoExecute(Sender: TObject);
    procedure CmdBcNSCongExecute(Sender: TObject);
    procedure CmdNghiBuExecute(Sender: TObject);
    procedure CmdDataRightExecute(Sender: TObject);
  private
	r: WORD;
    mFixLogon: Boolean;
	procedure LoadBkgrImage;
    procedure SetBkgrText;
  public
  end;

var
  FrmHRMain: TFrmHRMain;

implementation

uses
	{$IFDEF _MULTILANG_}
	// Multilizer
    IvI18N,
    IvResDll,
    IvResLangD,
    {$ENDIF}

	MainData, ExCommon, Rights, GmsRep, isMsg, DmHotro_HR,isDb,
    isType, Scan, isStr, isDba, isLib, Dmdl, isCommon, isOdbc, isLogin,
    ListUser, ListGroup, FunctionAccess, ReportAccess, AdminData,
    isResourceString, GuidEx, Params,
    DmPhongban, Params2, DmVangmat, DmNgayle, Dmnv,
    DevList, DmCalamviec, DmChucvu, Vantay, LogReader, WaitProgress, DevRAW,
    DmNganhang, DmNhomlv, DmQTLV, DmPhucap, DmHopdong, DmNoichuabenh, Bangcong,
    Bangluong, DangkyVangmat, DangkyThaisan, Phepnam, HopdongLaodong, DevLog,
  DmPhongbanNotCN, QuaTrinhLamViec, DangkyTangCa, ThietBiTaiSan,
  DmNguonTuyenDung, HoSonv, DmLyDoKTKL, KhenThuongKyLuat, DmDiaDiemLamViec,
  LichSuCongTac, DmThietBiTaiSanLaoDong, DmBacLuong, DmBieuMau, DmCanhBao, NghiBu,
  DataAccess, DaoTao, LichLamViec, DangkyMangThai;

{$R *.DFM}
{$R exRes.res}

{$REGION '<< Some of reusable procedures >>'}
(*==============================================================================
** Load background image
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.LoadBkgrImage;
const
	cDrawStyle: array[0..5] of TfcImagerDrawStyle = (dsCenter, dsNormal,
        dsProportional, dsProportionalCenter, dsStretch, dsTile);
var
	s: String;
	n: Integer;
begin
    // Set background image
    s := RegRead('Background', 'FileName', '');
    if FileExists(s) then
    begin
        BkGr.Visible := True;
        try
            BkGr.Picture.LoadFromFile(s);
        except;
        end;
    end;
end;

(*==============================================================================
** Set background text
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.SetBkgrText;
begin
    if PaDesc.Visible then
    begin
	    LbDesc1.Caption := '';//UpperCase(isDoubleChr(sysDesc1), loUserLocale);
    	LbDesc2.Caption := '';//UpperCase(isDoubleChr(sysDesc2), loUserLocale);
    end;
end;
{$ENDREGION}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	Handled := MyActionList.Tag = 0;
//    if Handled then //NTD
//        TestDbConnection;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.FormActivate(Sender: TObject);
begin
	if Tag = 1 then
    begin
	    with GbFunc do
    	begin
	    	ScrollPosition := 0;
	        Groups[0].Reposition;
    	end;

		Tag := 2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
    i: Integer;
begin
    // Save GroupBar state
    with GbFunc do
        for i := 0 to GroupCount - 1 do
            RegWrite(Self.Name, Groups[i].Name, Groups[i].Opened);

    try
//        DataMain.isEnv1.Logoff; //NTD
    except
    end;
    FlexFinal;
    DbeFinal;

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.FormShow(Sender: TObject);
var
    i: Integer;
begin
	TMyForm(Self).Init2;
	LoadCustomIcon;
//    Status.Panels[4].Text := 'DB: ' + isNameServer + '.' + isDatabaseServer;
//    Status.Panels[6].Text := 'Build ' + GetModuleVersion;
    DbeInitial;
	FlexInitial;

	// Initial
    FrmScan := Nil;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);

    // Enable system functions
//    SetApplicationVersion;
    SetApplicationFunctions(Self);

    GrPhanQuyen.Visible := sysIsAdmin;
    GrAdmin.Visible := False;
    CmdBc.Visible := FlexConfigBool('FRMMAIN', 'Bao cao', False) and (not sysIsAdmin);

    CmdBangcong.Visible := (not sysIsAdmin) and (GetFuncState('HR_BANGCONG'));
    CmdBangluong.Visible := (not sysIsAdmin) and (GetFuncState('HR_BANGLUONG'));

    // Desktop
	SetBkgrText;
    LoadBkgrImage;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');


    // Load GroupBar state
    with GbFunc do
    begin
        Style := TRzGroupBarStyle(RegRead(Self.Name, 'RzGroupStyle', 0));
        for i := 0 to GroupCount - 1 do
            Groups[i].Opened := RegReadBool(Self.Name, Groups[i].Name, True);
	    ScrollPosition := 0;
        Groups[0].Reposition;
    end;
    MyActionList.Tag := 1;
    SystemCritical.IsCritical := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdQuatrinhLamViecExecute(Sender: TObject);
begin
    r := GetRights('HR_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmQuaTrinhLamViec, FrmQuaTrinhLamViec);
    FrmQuaTrinhLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

procedure TFrmHRMain.CmdQuyettoanExecute(Sender: TObject);
begin
    r := GetRights('HR_QUYETTOAN');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdSetpassExecute(Sender: TObject);
begin
    DataMain.isLogon.ResetPassword; //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTailieuExecute(Sender: TObject);
begin
    r := GetRights('HR_TAILIEU');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTamungExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGCONG');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTangCaExecute(Sender: TObject);
begin
    r := GetRights('HR_TANGCA');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyTangCa, FrmDangkyTangCa);
    FrmDangkyTangCa.Execute(r, TGuidEx.EmptyGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdThaisanExecute(Sender: TObject);
begin
    r := GetRights('HR_THAISAN');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyThaisan, FrmDangkyThaisan);
    FrmDangkyThaisan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdThietBiTaiSanExecute(Sender: TObject);
begin
    r := GetRights('HR_THIETBI_DUNGCU');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmThietBiTaiSan, FrmThietBiTaiSan);
    FrmThietBiTaiSan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdThoiviecExecute(Sender: TObject);
begin
    r := GetRights('HR_NGHIVIEC');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTinhluongREPExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('4') then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTuyendungExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdTygiaExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdVangmatExecute(Sender: TObject);
begin
    r := GetRights('HR_VANGMAT');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyVangmat, FrmDangkyVangmat);
    FrmDangkyVangmat.Execute(r, TGuidEx.EmptyGuid);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdVantayExecute(Sender: TObject);
begin
    r := GetRights('TB_VANTAY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmVantay, FrmVantay);
    FrmVantay.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdAboutExecute(Sender: TObject);
begin
//	Application.CreateForm(TFrmAbout, FrmAbout);
//    FrmAbout.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdParamsExecute(Sender: TObject);
var
	n: Integer;
begin
//	r := GetRights('SZ_TSO');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmParams, FrmParams);
    n := FrmParams.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdPhepnamExecute(Sender: TObject);
begin
    r := GetRights('HR_PHEPNAM');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmPhepnam, FrmPhepnam);
    FrmPhepnam.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmVangmatExecute(Sender: TObject);
begin
	r := GetRights('HR_DM_VANGMAT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmVangmat, FrmDmVangmat);
    FrmDmVangmat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmNganhangExecute(Sender: TObject);
begin
    r := GetRights('SZ_NGANHANG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNganhang, FrmDmNganhang);
    FrmDmNganhang.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmNgayleExecute(Sender: TObject);
begin
    r := GetRights('HR_NGAYLE');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNgayle, FrmDmNgayle);
    FrmDmNgayle.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmNguonTuyenDungExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NGUON_TUYENDUNG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNguonTuyenDung, FrmDmNguonTuyenDung);
    FrmDmNguonTuyenDung.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdProfileExecute(Sender: TObject);
begin
    r := GetRights('HR_PROFILE');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmHoSonv, FrmHoSonv);
    FrmHoSonv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmHotroNSExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmLyDoKTKLExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_LYDO_KTKL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLyDoKTKL, FrmDmLyDoKTKL);
    FrmDmLyDoKTKL.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdBangcongExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGCONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmBangcong, FrmBangcong);
    FrmBangcong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdBangluongExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGLUONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmBangluong, FrmBangluong);
    FrmBangluong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdBcExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('NhanSu') then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdBcNSCongExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('CongLuong') (*22*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdBcNSExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('NhanSu') (*20*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdAdminExecute(Sender: TObject);
begin
    if not mFixLogon then
    begin
		if not FixLogon then
			Exit;
        mFixLogon := True;
        GrAdmin.Visible := mFixLogon;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdForceDropDownExecute(Sender: TObject);
begin
   (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdFunctionRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFuncAccess, FrmFuncAccess);
    FrmFuncAccess.Execute;
end;

procedure TFrmHRMain.CmdGiadinhExecute(Sender: TObject);
begin
    r := GetRights('HR_GIADINH');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdKhenthuongKyLuatExecute(Sender: TObject);
begin
    r := GetRights('HR_KHENTHUONG_KL');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmKhenThuongKyLuat, FrmKhenThuongKyLuat);
    FrmKhenThuongKyLuat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdHelpExecute(Sender: TObject);
begin
	Application.HelpCommand(HH_DISPLAY_TOC, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdHopDongLaoDongExecute(Sender: TObject);
begin
    r := GetRights('HR_HOPDONG_LD');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmHopdongLaodong, FrmHopdongLaodong);
    FrmHopdongLaodong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdSetBkgrExecute(Sender: TObject);
begin
	// Get file name
	with PicDlg do
		if not Execute then
        	Exit;

    // Save to registry
    RegWrite('Background', 'FileName', PicDlg.FileName);

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdChamcongREPExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('3') then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

procedure TFrmHRMain.CmdClearBkgrExecute(Sender: TObject);
begin
	// Delete file name
    RegDelete('Background', 'FileName');

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdReconnectExecute(Sender: TObject);
begin
//     DbConnect; //NTD
    if not ConnectToServer(isNameServer, isUserServer, isPasswordServer, isDatabaseServer) then
    begin
      ErrMsg(StrFailedToConnectToDatabaseServer);
      Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdRepNhansuExecute(Sender: TObject);
begin
    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdReportRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmRepAccess, FrmRepAccess);
    FrmRepAccess.Execute(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdSetDateTimeExecute(Sender: TObject);
begin
	WinExec('control timedate.cpl', SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmMCCExecute(Sender: TObject);
begin
    r := GetRights('TB_MAYCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDevList, FrmDevList);
    FrmDevList.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDaotaoExecute(Sender: TObject);
begin
    r := GetRights('HR_DAOTAO');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDaoTao, FrmDaoTao);
    FrmDaoTao.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDataRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDataAccess, FrmDataAccess);
    FrmDataAccess.Execute(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmBacLuongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_BACLUONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmBacLuong, FrmDmBacLuong);
    FrmDmBacLuong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmBieuMauExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_BIEUMAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmBieuMau, FrmDmBieuMau);
    FrmDmBieuMau.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmCalamviecExecute(Sender: TObject);
begin
	r := GetRights('HR_CALAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmCalamviec, FrmDmCalamviec);
    FrmDmCalamviec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmCanhBaoExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NHACVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmCanhBao, FrmDmCanhBao);
    FrmDmCanhBao.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmNhomlvExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NHOMLV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmNhomlv, FrmDmNhomlv);
    FrmDmNhomlv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmChucvuExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_CHUCVU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChucvu, FrmDmChucvu);
    FrmDmChucvu.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmDiaDiemLamViecExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NOI_LAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmDiaDiemLamViec, FrmDmDiaDiemLamViec);
    FrmDmDiaDiemLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmdialyExecute(Sender: TObject);
begin
	r := GetRights('SZ_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmDKKhambenhExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_DK_KHAMBENH');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmNoichuabenh, FrmDmNoichuabenh);
    FrmDmNoichuabenh.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmHopdongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOPDONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmHopdong, FrmDmHopdong);
    FrmDmHopdong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.PopBkGrPopup(Sender: TObject);
begin
	PopBkGr.Items[0].Caption := CmdSetBkGr.Caption +
    	Format(' (%dx%d)', [PaBkGr.Width, PaBkGr.Height]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdConfigExecute(Sender: TObject);
var
    n: Integer;
begin
    Application.CreateForm(TFrmParams2, FrmParams2);
    n := FrmParams2.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdCongtacExecute(Sender: TObject);
begin
    r := GetRights('HR_CONGTAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmLichSuCongTac, FrmLichSuCongTac);
    FrmLichSuCongTac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdCongthoExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDevRAW, FrmDevRAW);
	FrmDevRAW.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdLaydulieucongExecute(Sender: TObject);
    var
    mLogReader: TLogReader;
    n, i: Integer;
begin
{
    with TADOQuery.Create(nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select a.ID, a.TENMAY, a.IP4, a.SOCKET, a.IO_TYPE from HR_DM_MAYCC a order by ID';
        Open;
        FrmWaitProgress.Execute('Kết nối máy chấm công', 500, true, true, true);
        while not Eof do
        begin
            if SameText(FieldByName('IO_TYPE').AsString, 'I') then
                n := 0
            else if SameText(FieldByName('IO_TYPE').AsString, 'O') then
                n := 1
            else
                n := -1;
            i := FieldByName('ID').AsInteger;
            FrmWaitProgress.Start(500, Format('Kết nối máy chấm công "%d"', [i]));
            Application.ProcessMessages;

            mLogReader := TLogReader.Create(
                FieldByName('IP4').AsString,
                i,
                FieldByName('SOCKET').AsInteger,
                n, RecNo, 10, 0, True);

            mLogReader.MaxLog := 5000;//mRecMaxLog;
			mLogReader.AutoClearLog := False;
            mLogReader.Active := True;
            mLogReader.Resume;
            mLogReader.WaitFor;
            Application.ProcessMessages;
            mLogReader.Free;

            Next;
        end;

        FrmWaitProgress.Stop;
        Close;
        Free;
    end;    }

    devLogAll := False;
    devLogFromDate := 0;

    Application.CreateForm(TFrmDevLog, FrmDevLog);
    if not FrmDevLog.Execute then
        Exit;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdLichlamviecExecute(Sender: TObject);
begin
    r := GetRights('HR_LICHLAMVIEC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmLichLamViec, FrmLichLamViec);
    FrmLichLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdListGroupExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListGroup, FrmListGroup);
    FrmListGroup.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdListUserExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListUser, FrmListUser);
    FrmListUser.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdLockExecute(Sender: TObject);
begin
//    DataMain.isEnv1.Lock //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdFaqExecute(Sender: TObject);
begin
	Faqs;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdMangThaiExecute(Sender: TObject);
begin
    r := GetRights('HR_MANGTHAI');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyMangThai, FrmDangkyMangThai);
    FrmDangkyMangThai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdMyCustomerExecute(Sender: TObject);
begin
	Customers;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdNghiBuExecute(Sender: TObject);
begin
    r := GetRights('HR_NGHIBU');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmNghiBu, FrmNghiBu);
    FrmNghiBu.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdNghiviecExecute(Sender: TObject);
begin
    r := GetRights('HR_NGHIVIEC');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdNhansuREPExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('3') then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.ItemCloseAllClick(Sender: TObject);
var
	i: Integer;
begin
	with GbFunc do
   		for i := 0 to GroupCount - 1 do
        	Groups[i].Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.ItemCatClick(Sender: TObject);
begin
	case (Sender as TComponent).Tag of
    0:
    	with GbFunc do
        	if Style <> gbsCategoryView then
	        	Style := gbsCategoryView;
    1:
    	with GbFunc do
        	if Style <> gbsOutlook then
	        	Style := gbsOutlook;
    2:
    	with GbFunc do
        	if Style <> gbsTaskList then
	        	Style := gbsTaskList;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.PopFuncPopup(Sender: TObject);
begin
	with PopFunc, GbFunc do
    begin
    	Items[0].Checked := Style = gbsCategoryView;
    	Items[1].Checked := Style = gbsOutlook;
    	Items[2].Checked := Style = gbsTasklist;
    	Items[4].Enabled := Items[0].Checked;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmPhongbanExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHONGBAN');
    if r = R_DENY then
    	Exit;

	if sysIsHrChinhanh then
    begin
        Application.CreateForm(TFrmDmPhongban, FrmDmPhongban);
        FrmDmPhongban.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmPhongbanNotCN, FrmDmPhongbanNotCN);
        FrmDmPhongbanNotCN.Execute(r);
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmPhucapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmQuatrinhExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmQTLV, FrmDmQTLV);
    FrmDmQTLV.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHRMain.CmdDmThietBiTaiSanLaoDongExecute(Sender: TObject);
begin
     r := GetRights('HR_DM_THIETBI_TAISAN_LAODONG');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmThietBiTaiSanLaoDong, FrmDmThietBiTaiSanLaoDong);
    FrmDmThietBiTaiSanLaoDong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
initialization
	{$IFDEF _MULTILANG_}
    sysEnglish := GetArg('E');
    if sysEnglish = False then
    else if not	SetNewResourceDll(LANG_ENGLISH) then
    {$ENDIF}
    	sysEnglish := False;
    if sysEnglish then
        DataMain.isEnv1.Language := ENGLISH;
end.

