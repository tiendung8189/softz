(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmNguonTuyenDung;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin;

type
  TFrmDmNguonTuyenDung = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    CmdDetail: TAction;
    QrDMMaNguonTD: TWideStringField;
    QrDMTenNguonTD: TWideStringField;
    QrDMTenNguonTD_TA: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmNguonTuyenDung: TFrmDmNguonTuyenDung;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NGUON_TUYENDUNG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDM.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDM, FORM_CODE);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDM, FORM_CODE, Filter);

    fixCode := SetCodeLength(FORM_CODE, QrDM.FieldByName('MaNguonTD'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.FormShow(Sender: TObject);
begin
    QrDM.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.QrDMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.QrDMBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDM, ['MaNguonTD', 'TenNguonTD']) then
   		Abort;

    if fixCode then
		if LengthConfirm(QrDM.FieldByName('MaNguonTD')) then
        	Abort;


    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaNguonTD' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNguonTuyenDung.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

end.
