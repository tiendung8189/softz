object FrmDangkyTangCa: TFrmDangkyTangCa
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #208#259'ng K'#253' T'#259'ng Ca'
  ClientHeight = 573
  ClientWidth = 1040
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1040
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 60
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btnEdit: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 128
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 136
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object SepImport: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'SepImport'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object BtnImport: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Caption = 'Import'
      DropdownMenu = PopImport
      ImageIndex = 26
      Style = tbsDropDown
      OnClick = BtnImportClick
    end
    object ToolButton18: TToolButton
      Left = 475
      Top = 0
      Width = 8
      Caption = 'ToolButton18'
      ImageIndex = 9
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 483
      Top = 0
      Cursor = 1
      Action = CmdNewList
      ImageIndex = 11
    end
    object ToolButton15: TToolButton
      Left = 543
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 551
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 1040
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 44
    Width = 1040
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 44
    ExplicitWidth = 1040
    inherited Panel1: TPanel
      Width = 1040
      ExplicitWidth = 1040
      inherited CbOrg: TfcTreeCombo
        Width = 557
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitWidth = 557
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 522
    Top = 133
    Width = 518
    Height = 419
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 3
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object rScrollBoxEx1: TrScrollBoxEx
      Left = 8
      Top = 0
      Width = 510
      Height = 419
      HorzScrollBar.Smooth = True
      HorzScrollBar.Style = ssHotTrack
      VertScrollBar.Smooth = True
      VertScrollBar.Size = 600
      VertScrollBar.Style = ssFlat
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object PaInfo: TisPanel
        Left = 0
        Top = 0
        Width = 510
        Height = 120
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin '#273#259'ng k'#253
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object gbNhanVien: TGroupBox
          Left = 0
          Top = 16
          Width = 510
          Height = 98
          Align = alTop
          Caption = '  Nh'#226'n vi'#234'n  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object PaNhanVien: TPanel
            Left = 2
            Top = 15
            Width = 506
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            DesignSize = (
              506
              24)
            object CbNhanVien: TDbLookupComboboxEh2
              Left = 138
              Top = 2
              Width = 282
              Height = 22
              ControlLabel.Width = 56
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'Manv'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'Tennv'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 120
                end
                item
                  FieldName = 'ManvQL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsEmp
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Manv'
              ListField = 'Tennv'
              ListSource = HrDataMain.DsDMNV
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object EdManvQL: TDBEditEh
              Left = 423
              Top = 2
              Width = 77
              Height = 22
              TabStop = False
              Alignment = taLeftJustify
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_ManvQL'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
          end
          object Panel2: TPanel
            Left = 2
            Top = 39
            Width = 506
            Height = 56
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            DesignSize = (
              506
              56)
            object Label13: TLabel
              Left = 292
              Top = 6
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'V'#224'o ra'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 254
            end
            object Label2: TLabel
              Left = 383
              Top = 6
              Width = 5
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = '-'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 345
            end
            object Label108: TLabel
              Left = 58
              Top = 30
              Width = 74
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y t'#259'ng ca'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label1: TLabel
              Left = 266
              Top = 30
              Width = 64
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Gi'#7901' t'#259'ng ca'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 228
            end
            object Label3: TLabel
              Left = 383
              Top = 30
              Width = 5
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = '-'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 345
            end
            object CbCaLamViec: TDbLookupComboboxEh2
              Left = 138
              Top = 2
              Width = 120
              Height = 22
              ControlLabel.Width = 66
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ca l'#224'm vi'#7879'c'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaCa'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'TenCa'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CALAMVIEC
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaCa'
              ListField = 'TenCa'
              ListSource = HrDataMain.DsDM_CALAMVIEC
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object dpGioVao: TwwDBDateTimePicker
              Left = 335
              Top = 2
              Width = 42
              Height = 22
              ParentCustomHint = False
              TabStop = False
              Anchors = [akTop, akRight]
              BiDiMode = bdLeftToRight
              AutoSize = False
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              Color = clBtnFace
              DataField = 'GioVao_Ca'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Time = 0.333333333333333300
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              ShowButton = False
              TabOrder = 1
            end
            object dpGioRa1: TwwDBDateTimePicker
              Left = 394
              Top = 2
              Width = 42
              Height = 22
              ParentCustomHint = False
              TabStop = False
              Anchors = [akTop, akRight]
              BiDiMode = bdLeftToRight
              AutoSize = False
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              Color = clBtnFace
              DataField = 'GioRa_Ca'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Time = 0.562500000000000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              ShowButton = False
              TabOrder = 2
            end
            object DBNumberEditEh1: TDBNumberEditEh
              Left = 439
              Top = 2
              Width = 35
              Height = 22
              ParentCustomHint = False
              TabStop = False
              ControlLabel.Width = 17
              ControlLabel.Height = 16
              ControlLabel.BiDiMode = bdLeftToRight
              ControlLabel.Caption = 'gi'#7901
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentBiDiMode = False
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpRightCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BiDiMode = bdLeftToRight
              BorderStyle = bsNone
              Color = clBtnFace
              Ctl3D = False
              DataField = 'SoGio_Ca'
              DataSource = DsMaster
              DecimalPlaces = 1
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentCtl3D = False
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object CbDate: TwwDBDateTimePicker
              Left = 138
              Top = 26
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'Ngay'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 4
            end
            object dpDangKyTuGio: TwwDBDateTimePicker
              Left = 335
              Top = 26
              Width = 42
              Height = 22
              ParentCustomHint = False
              Anchors = [akTop, akRight]
              BiDiMode = bdLeftToRight
              AutoSize = False
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'DangKyTuGio'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Time = 0.375000000000000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              ShowButton = False
              TabOrder = 5
            end
            object dpDangKyDenGio: TwwDBDateTimePicker
              Left = 394
              Top = 26
              Width = 42
              Height = 22
              ParentCustomHint = False
              Anchors = [akTop, akRight]
              BiDiMode = bdLeftToRight
              AutoSize = False
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'DangKyDenGio'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Time = 0.562500000000000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              ShowButton = False
              TabOrder = 6
            end
            object DBNumberEditEh4: TDBNumberEditEh
              Left = 439
              Top = 26
              Width = 35
              Height = 22
              ParentCustomHint = False
              TabStop = False
              ControlLabel.Width = 17
              ControlLabel.Height = 16
              ControlLabel.BiDiMode = bdLeftToRight
              ControlLabel.Caption = 'gi'#7901
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentBiDiMode = False
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpRightCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BiDiMode = bdLeftToRight
              BorderStyle = bsNone
              Color = clBtnFace
              Ctl3D = False
              DataField = 'DangKySoGio'
              DataSource = DsMaster
              DecimalPlaces = 1
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentBiDiMode = False
              ParentCtl3D = False
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 7
              Visible = True
            end
          end
        end
      end
      object PNTinhTrang_XacNhan: TisPanel
        Left = 0
        Top = 120
        Width = 510
        Height = 131
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' :: Th'#244'ng tin x'#225'c nh'#7853'n'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          510
          131)
        object Label4: TLabel
          Left = 321
          Top = 74
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 283
        end
        object DBEditEh1: TDBEditEh
          Left = 140
          Top = 46
          Width = 362
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 42
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ghi ch'#250
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'XacNhan_GhiChu'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object CbXacNhan_TinhTrang: TDbLookupComboboxEh2
          Left = 140
          Top = 70
          Width = 145
          Height = 22
          ControlLabel.Width = 59
          ControlLabel.Height = 16
          ControlLabel.Caption = 'T'#236'nh tr'#7841'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          Images = DataMain.ImageStatus
          Color = clBtnFace
          DataField = 'XacNhan_TinhTrang'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'TEN_HOTRO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 60
            end>
          DropDownBox.ListSource = DsTinhTrang_XacNhan
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoFitRowHeightEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA_HOTRO'
          ListField = 'TEN_HOTRO'
          ListSource = DsTinhTrang_XacNhan
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object CbXacNhan: TDbLookupComboboxEh2
          Left = 140
          Top = 22
          Width = 282
          Height = 22
          ControlLabel.Width = 78
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#432#7901'i qu'#7843'n l'#253
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          Anchors = [akLeft, akTop, akRight]
          DynProps = <>
          DataField = 'XacNhan_Manv'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'Tennv'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 120
            end
            item
              FieldName = 'ManvQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 40
            end>
          DropDownBox.ListSource = DsEmpStatus
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'Manv'
          ListField = 'Tennv'
          ListSource = HrDataMain.DsDMNV
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 0
          Visible = True
        end
        object EdXacNhanManvQL: TDBEditEh
          Left = 425
          Top = 22
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_XacNhan_ManvQL'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 355
          Top = 70
          Width = 147
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'XacNhan_Ngay'
          DataSource = DsMaster
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 4
        end
        object BtnConfirmAccept: TBitBtn
          Left = 140
          Top = 96
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdConfirmAccept
          Caption = 'X'#225'c nh'#7853'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object BtnConfirmCancel: TBitBtn
          Left = 245
          Top = 96
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdConfirmCancel
          Caption = 'T'#7915' ch'#7889'i'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
      end
      object PNTinhTrang: TisPanel
        Left = 0
        Top = 251
        Width = 510
        Height = 158
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' :: Th'#244'ng tin x'#233't duy'#7879't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 103
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          510
          158)
        object Label5: TLabel
          Left = 321
          Top = 98
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Ng'#224'y'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 283
        end
        object Label6: TLabel
          Left = 181
          Top = 72
          Width = 17
          Height = 16
          Alignment = taRightJustify
          Caption = 'gi'#7901
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 486
          Top = 72
          Width = 17
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'gi'#7901
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 448
        end
        object wwDBEdit7: TDBEditEh
          Left = 140
          Top = 46
          Width = 362
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 42
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ghi ch'#250
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'XetDuyet_GhiChu'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object CbbXetDuyet_TinhTrang: TDbLookupComboboxEh2
          Left = 140
          Top = 94
          Width = 145
          Height = 22
          ControlLabel.Width = 59
          ControlLabel.Height = 16
          ControlLabel.Caption = 'T'#236'nh tr'#7841'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          Images = DataMain.ImageStatus
          Color = clBtnFace
          DataField = 'XetDuyet_TinhTrang'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'TEN_HOTRO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 60
            end>
          DropDownBox.ListSource = DsTinhTrang_XetDuyet
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh, dlgAutoFitRowHeightEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA_HOTRO'
          ListField = 'TEN_HOTRO'
          ListSource = DsTinhTrang_XetDuyet
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
        object CbXetDuyet: TDbLookupComboboxEh2
          Left = 140
          Top = 22
          Width = 282
          Height = 22
          ControlLabel.Width = 127
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#432#7901'i x'#233't duy'#7879't (BOD)'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          Anchors = [akLeft, akTop, akRight]
          DynProps = <>
          DataField = 'XetDuyet_Manv'
          DataSource = DsMaster
          DropDownBox.Columns = <
            item
              FieldName = 'Tennv'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 120
            end
            item
              FieldName = 'ManvQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 40
            end>
          DropDownBox.ListSource = DsEmpStatus
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'Manv'
          ListField = 'Tennv'
          ListSource = HrDataMain.DsDMNV
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 0
          Visible = True
        end
        object EdXetDuyetManvQL: TDBEditEh
          Left = 425
          Top = 22
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_XetDuyet_ManvQL'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object wwDBDateTimePicker2: TwwDBDateTimePicker
          Left = 355
          Top = 94
          Width = 147
          Height = 22
          TabStop = False
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'XetDuyet_Ngay'
          DataSource = DsMaster
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ShowButton = True
          TabOrder = 6
        end
        object BtnApproveAccept: TBitBtn
          Left = 140
          Top = 120
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdApproveAccept
          Caption = 'Duy'#7879't'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
        end
        object BtnApproveCancel: TBitBtn
          Left = 245
          Top = 120
          Width = 100
          Height = 30
          Cursor = 1
          Action = CmdApproveCancel
          Caption = 'T'#7915' ch'#7889'i'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
        end
        object DBNumberEditEh3: TDBNumberEditEh
          Left = 445
          Top = 70
          Width = 35
          Height = 22
          ParentCustomHint = False
          TabStop = False
          ControlLabel.Width = 83
          ControlLabel.Height = 16
          ControlLabel.BiDiMode = bdLeftToRight
          ControlLabel.Caption = 'H'#432#7903'ng ngh'#7881' b'#249
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentBiDiMode = False
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BiDiMode = bdLeftToRight
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'XetDuyet_SoGioNghiBu'
          DataSource = DsMaster
          DecimalPlaces = 1
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object DBNumberEditEh2: TDBNumberEditEh
          Left = 140
          Top = 70
          Width = 35
          Height = 22
          ParentCustomHint = False
          ControlLabel.Width = 73
          ControlLabel.Height = 16
          ControlLabel.BiDiMode = bdLeftToRight
          ControlLabel.Caption = 'H'#432#7903'ng l'#432#417'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentBiDiMode = False
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          BevelKind = bkFlat
          BiDiMode = bdLeftToRight
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'XetDuyet_SoGioHuongLuong'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
      end
      object PNGhiChu: TisPanel
        Left = 0
        Top = 409
        Width = 510
        Height = 10
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 3
        HeaderCaption = ' :: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 0
          Top = 16
          Width = 510
          Height = 2
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 133
    Width = 522
    Height = 419
    Align = alClient
    TabOrder = 4
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 520
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object rgApproved: TRadioGroup
        Left = 0
        Top = 0
        Width = 515
        Height = 45
        Align = alCustom
        Caption = 'T'#236'nh tr'#7841'ng x'#233't duy'#7879't'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Ch'#7901' x'#233't duy'#7879't'
          #272#227' x'#233't duy'#7879't'
          'T'#7915' ch'#7889'i x'#233't duy'#7879't'
          'T'#7845't c'#7843)
        TabOrder = 0
      end
    end
    object GrList: TwwDBGrid2
      Left = 1
      Top = 49
      Width = 520
      Height = 369
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      ControlType.Strings = (
        'XacNhan_TinhTrang;ImageIndex;Original Size'
        'XetDuyet_TinhTrang;ImageIndex;Original Size')
      Selected.Strings = (
        'Manv'#9'20'#9'Manv'#9'F'
        'LK_TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
        'GhiChu'#9'200'#9'GhiChu'#9'F'
        'XetDuyet_TinhTrang'#9'4'#9'#'#9'F'
        'XacNhan_TinhTrang'#9'4'#9'#'#9'F')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopCommon
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      OnEnter = CmdRefreshExecute
      ImageList = DataMain.ImageStatus
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 92
    Width = 1040
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 92
    ExplicitWidth = 1040
    inherited Panel1: TPanel
      Width = 1040
      ExplicitWidth = 1040
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdNewList: TAction
      Caption = #208#259'ng k'#253
      Hint = #208#259'ng k'#253' h'#224'ng lo'#7841't'
      OnExecute = CmdNewListExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      Visible = False
      OnExecute = CmdClearExecute
    end
    object CmdImportExcel: TAction
      Caption = 'Ch'#7885'n file Excel ...'
      Hint = 'Import d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdLyDoVangMat: TAction
      Caption = 'L'#253' do v'#7855'ng m'#7863't'
    end
    object CmdConfirmAccept: TAction
      Caption = 'X'#225'c nh'#7853'n'
      OnExecute = CmdConfirmAcceptExecute
    end
    object CmdConfirmCancel: TAction
      Caption = 'T'#7915' ch'#7889'i'
      OnExecute = CmdConfirmCancelExecute
    end
    object CmdApproveAccept: TAction
      Caption = 'Duy'#7879't'
      OnExecute = CmdApproveAcceptExecute
    end
    object CmdApproveCancel: TAction
      Caption = 'T'#7915' ch'#7889'i'
      OnExecute = CmdApproveCancelExecute
    end
    object CmdSampleExcel: TAction
      Caption = 'T'#7843'i v'#7873' file Excel m'#7851'u'
      OnExecute = CmdSampleExcelExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopCommonPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PopTinhTrangXacNhan: TMenuItem
      Caption = 'Ch'#7901' x'#225'c nh'#7853'n'
      OnClick = PopTinhTrangXacNhan1Click
    end
    object PopTinhTrangXacNhan1: TMenuItem
      Tag = 1
      Caption = #272#227' x'#225'c nh'#7853'n'
      OnClick = PopTinhTrangXacNhan1Click
    end
    object PopTinhTrangXacNhan2: TMenuItem
      Tag = 2
      Caption = 'T'#7915' ch'#7889'i x'#225'c nh'#7853'n'
      OnClick = PopTinhTrangXacNhan1Click
    end
    object PopTinhTrangXacNhan3: TMenuItem
      Tag = 3
      Caption = 'T'#7845't c'#7843
      OnClick = PopTinhTrangXacNhan1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object PopTangCaHuongLuong: TMenuItem
      Tag = 1
      Caption = 'T'#259'ng ca h'#432#7903'ng l'#432#417'ng'
      OnClick = PopTangCaHuongLuongClick
    end
    object PopTangCaHuongNghiBu: TMenuItem
      Tag = 2
      Caption = 'T'#259'ng ca h'#432#7903'ng ngh'#7881' b'#249
      OnClick = PopTangCaHuongLuongClick
    end
    object PopTangCaTatCa: TMenuItem
      Caption = 'T'#7845't c'#7843
      OnClick = PopTangCaHuongLuongClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrEmpBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DANGKY_TANGCA'
      ' where 1=1  %s ')
    Left = 17
    Top = 172
    object QrMasterLK_TENNV: TWideStringField
      DisplayLabel = 'H'#7885' t'#234'n'
      FieldKind = fkLookup
      FieldName = 'LK_TenNV'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'MANV'
      LookupResultField = 'TENNV'
      KeyFields = 'Manv'
      Size = 50
      Lookup = True
    end
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrMasterLK_XacNhan_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XacNhan_Manv'
      Size = 200
      Lookup = True
    end
    object QrMasterLK_GIOVAO: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GIORA: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GIOVAO1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_GIORA1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterLK_SOGIO: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_SoGio'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'SoGio'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object QrMasterXacNhan_TinhTrang: TIntegerField
      FieldName = 'XacNhan_TinhTrang'
    end
    object QrMasterXacNhan_Manv: TWideStringField
      FieldName = 'XacNhan_Manv'
      OnChange = QrMasterXacNhan_ManvChange
    end
    object QrMasterXacNhan_GhiChu: TWideStringField
      FieldName = 'XacNhan_GhiChu'
      Size = 200
    end
    object QrMasterXetDuyet_TinhTrang: TIntegerField
      FieldName = 'XetDuyet_TinhTrang'
    end
    object QrMasterXetDuyet_Manv: TWideStringField
      FieldName = 'XetDuyet_Manv'
      OnChange = QrMasterXetDuyet_ManvChange
    end
    object QrMasterXetDuyet_GhiChu: TWideStringField
      FieldName = 'XetDuyet_GhiChu'
      Size = 200
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
      OnValidate = QrMasterMANVValidate
    end
    object QrMasterMaCa: TWideStringField
      FieldName = 'MaCa'
      OnChange = QrMasterMACAChange
    end
    object QrMasterLK_XetDuyet_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_TenTinhTrang: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_TenTinhTrang'
      LookupDataSet = QrTinhTrang_XetDuyet
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'XetDuyet_TinhTrang'
      Lookup = True
    end
    object QrMasterLK_XacNhan_TenTinhTrang: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_TenTinhTrang'
      LookupDataSet = QrTinhTrang_XacNhan
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'XacNhan_TinhTrang'
      Lookup = True
    end
    object QrMasterNgay: TDateTimeField
      FieldName = 'Ngay'
      OnChange = QrMasterDangKyTuGioChange
    end
    object QrMasterXetDuyet_TinhTrangBy: TIntegerField
      FieldName = 'XetDuyet_TinhTrangBy'
    end
    object QrMasterXetDuyet_Ngay: TDateTimeField
      FieldName = 'XetDuyet_Ngay'
    end
    object QrMasterXacNhan_TinhTrangBy: TIntegerField
      FieldName = 'XacNhan_TinhTrangBy'
    end
    object QrMasterXacNhan_Ngay: TDateTimeField
      FieldName = 'XacNhan_Ngay'
    end
    object QrMasterCREATE_SOURCE: TWideStringField
      FieldName = 'CREATE_SOURCE'
      Size = 5
    end
    object QrMasterCREATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'CREATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Size = 200
      Lookup = True
    end
    object QrMasterCREATE_SOURCE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'CREATE_SOURCE_NAME'
      LookupDataSet = DataMain.QrSYS_APP
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'CREATE_SOURCE'
      Size = 200
      Lookup = True
    end
    object QrMasterLK_XetDuyet_TinhTrangFullName: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_TinhTrangFullName'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'XetDuyet_TinhTrangBy'
      Size = 200
      Lookup = True
    end
    object QrMasterLK_XacNhan_TinhTrangFullName: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_TinhTrangFullName'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'XacNhan_TinhTrangBy'
      Size = 200
      Lookup = True
    end
    object QrMasterLK_XetDuyet_Manv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_Manv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'XetDuyet_Manv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_XacNhan_Manv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_Manv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'XacNhan_Manv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_MaNhomLV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNhomLV'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'MaNhomLV'
      KeyFields = 'Manv'
      Size = 200
      Lookup = True
    end
    object QrMasterMaNhomLV: TWideStringField
      FieldName = 'MaNhomLV'
    end
    object QrMasterCREATE_FUNC: TWideStringField
      FieldName = 'CREATE_FUNC'
      Size = 50
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_XacNhan_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterDangKyTuGio: TDateTimeField
      FieldName = 'DangKyTuGio'
      OnChange = QrMasterDangKyTuGioChange
    end
    object QrMasterDangKyDenGio: TDateTimeField
      FieldName = 'DangKyDenGio'
      OnChange = QrMasterDangKyTuGioChange
    end
    object QrMasterDangKySoGio: TFloatField
      FieldName = 'DangKySoGio'
      OnChange = QrMasterDangKySoGioChange
    end
    object QrMasterGioVao_Ca: TDateTimeField
      FieldName = 'GioVao_Ca'
    end
    object QrMasterGioRa_Ca: TDateTimeField
      FieldName = 'GioRa_Ca'
    end
    object QrMasterGioVao1_Ca: TDateTimeField
      FieldName = 'GioVao1_Ca'
    end
    object QrMasterGioRa1_Ca: TDateTimeField
      FieldName = 'GioRa1_Ca'
    end
    object QrMasterSoGio_Ca: TFloatField
      FieldName = 'SoGio_Ca'
    end
    object QrMasterXetDuyet_SoGioHuongLuong: TFloatField
      FieldName = 'XetDuyet_SoGioHuongLuong'
      OnChange = QrMasterXetDuyet_SoGioHuongLuongChange
    end
    object QrMasterXetDuyet_SoGioNghiBu: TFloatField
      FieldName = 'XetDuyet_SoGioNghiBu'
    end
    object QrMasterIDX: TAutoIncField
      FieldName = 'IDX'
      ReadOnly = True
    end
    object QrMasterIDX_FUNC: TIntegerField
      FieldName = 'IDX_FUNC'
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object QrShift: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MANV'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'a.MACA, a.GIOVAO, a.GIORA, a.GIOVAO1, a.GIORA1, a.SOGIO'
      
        '  from '#9'HR_DM_CALAMVIEC a join HR_DM_NHOM_LAMVIEC b on a.MACA = ' +
        'b.CA1'
      #9#9'join HR_DM_NHANVIEN c on b.MANHOMLV = c.MANHOMLV'
      'where '#9'c.MANV = :MANV')
    Left = 170
    Top = 143
  end
  object spHR_DANGKY_TANGCA_Invalid: TADOCommand
    CommandText = 'spHR_DANGKY_TANGCA_Invalid;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DangKyTuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DangKyDenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MaCa'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 172
    Top = 172
  end
  object QrCsv: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=SOFTZ' +
      '_HR_TEXT'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'"DangkyVangmat.csv"')
    Left = 208
    Top = 256
  end
  object QrTinhTrang_XacNhan: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  * '
      '   from V_HR_TINHTRANG_XACNHAN'
      'order by MA_HOTRO asc')
    Left = 116
    Top = 340
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 52
    Top = 200
  end
  object QrTinhTrang_XetDuyet: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  * '
      '   from V_HR_TINHTRANG_XETDUYET'
      'order by MA_HOTRO asc')
    Left = 228
    Top = 340
  end
  object DsTinhTrang_XacNhan: TDataSource
    AutoEdit = False
    DataSet = QrTinhTrang_XacNhan
    Left = 116
    Top = 384
  end
  object DsTinhTrang_XetDuyet: TDataSource
    AutoEdit = False
    DataSet = QrTinhTrang_XetDuyet
    Left = 228
    Top = 384
  end
  object QrEmpStatus: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrEmpBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 272
    Top = 276
  end
  object DsEmpStatus: TDataSource
    AutoEdit = False
    DataSet = QrEmpStatus
    Left = 276
    Top = 304
  end
  object QrDays: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'DangKyTuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'DangKyDenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'MaCa'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'select dbo.fnHR_Tangca_Tinhgio(:Manv, :Ngay, :DangKyTuGio, :Dang' +
        'KyDenGio, :MaCa)')
    Left = 132
    Top = 236
  end
  object PopImport: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopCommonPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 352
    Top = 400
    object MenuItem6: TMenuItem
      Action = CmdImportExcel
    end
    object MenuItem7: TMenuItem
      Tag = 1
      Action = CmdSampleExcel
    end
  end
  object spIMP_HR_DANGKY_TANGCA_DeleteList: TADOCommand
    CommandText = 'spIMP_HR_DANGKY_TANGCA_DeleteList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIsError'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 380
    Top = 356
  end
end
