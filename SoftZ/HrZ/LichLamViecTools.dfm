object FrmLichLamViecTools: TFrmLichLamViecTools
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = #208#259'ng K'#253' L'#7883'ch L'#224'm Vi'#7879'c'
  ClientHeight = 625
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 171
    Align = alTop
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 402
      Height = 169
      Align = alLeft
      BevelOuter = bvNone
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 0
      object RgLoai: TRadioGroup
        Left = 8
        Top = 4
        Width = 385
        Height = 47
        Columns = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 2
        Items.Strings = (
          'Theo ph'#242'ng ban'
          'Theo b'#7897' ph'#7853'n'
          'Theo nh'#226'n vi'#234'n')
        ParentFont = False
        TabOrder = 0
        OnClick = RgLoaiClick
      end
      object Panel2: TPanel
        Left = 8
        Top = 54
        Width = 385
        Height = 105
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object CbPhongBan: TDbLookupComboboxEh2
          Left = 69
          Top = 12
          Width = 222
          Height = 24
          ControlLabel.Width = 60
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ph'#242'ng ban'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Alignment = taLeftJustify
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'TenPhongBan'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'Ma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = DsDep
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MaPhongBan'
          ListField = 'TenPhongBan'
          ListSource = DsDep
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 0
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object CbBoPhan: TDbLookupComboboxEh2
          Tag = 1
          Left = 69
          Top = 40
          Width = 222
          Height = 24
          ControlLabel.Width = 46
          ControlLabel.Height = 16
          ControlLabel.Caption = 'B'#7897' ph'#7853'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'TenBoPhan'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'Ma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = DsSec
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MaBoPhan'
          ListField = 'TenBoPhan'
          ListSource = HrDataMain.DsDMBOPHAN
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object EdMaPhongBan: TDBEditEh
          Left = 295
          Top = 12
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object EdMaBoPhan: TDBEditEh
          Left = 295
          Top = 40
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object CbNhanVien: TDbLookupComboboxEh2
          Tag = 2
          Left = 69
          Top = 68
          Width = 222
          Height = 24
          ControlLabel.Width = 56
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'Tennv'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'ManvQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = HrDataMain.DsDMNV_DANGLAMVIEC
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'ManvQL'
          ListField = 'Tennv'
          ListSource = DsEmp
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object EdMaNhanVien: TDBEditEh
          Left = 295
          Top = 68
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
      end
    end
    object Panel5: TPanel
      Left = 403
      Top = 1
      Width = 62
      Height = 169
      Align = alLeft
      BevelOuter = bvNone
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object BitBtn1: TBitBtn
        Left = 0
        Top = 53
        Width = 53
        Height = 25
        Cursor = 1
        Action = CmdIns
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C30E0000C30E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874991D987494D0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AF9D885
          4856000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AFEDA87
          4AFBDA874A600000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AFEDA87
          4AFEDA874AFCDA8749690000000000000000000000000000000000000000DA86
          4965DA8649A9DA874AA9DA874AA9DA874AA9DA874AA9DA874AE1DA874AFEDA87
          4AFEDA874AFEDA874AFDDA87497400000000000000000000000000000000D986
          49A9DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFDDA874A7F000000000000000000000000DA87
          4AA9DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFEDA8749850000000000000000E999
          5BA9EA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFDE9985A7D0000000000000000E999
          5BA9EA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5BFDE9995B73000000000000000000000000E999
          5B63EA9A5BA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5CE1EA9A5CFEEA9A
          5CFEEA9A5CFEE9995BFBE9995A66000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5CA9EA9A5CFEEA9A
          5CFEE9995BF9E8985A5B00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5CA9EA9A5CFEE999
          5BF7E6975A4F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5BA9E9995BF3E395
          5845000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000E9995B84E093573B0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 0
      end
      object BitBtn3: TBitBtn
        Left = 0
        Top = 90
        Width = 53
        Height = 25
        Cursor = 1
        Action = CmdDel
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C30E0000C30E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000D786494DDA8749910000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000D7854756DA874AF9DA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000D9874A60DA874AFBDA874AFEDA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000DA874969DA874AFCDA874AFEDA874AFEDA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000DA874974DA874AFDDA874AFEDA874AFEDA874AFEDA874AE1DA87
          4AA9DA874AA9DA874AA9DA874AA9DA8649A9D986496500000000000000000000
          0000DA874A7FDA874AFDDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFED98649A90000000000000000DA87
          4985DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AA90000000000000000E998
          5A7DEA9A5BFDEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEE9995BA900000000000000000000
          0000E8995B73EA9A5BFDEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEE9995BA900000000000000000000
          000000000000E8985A66E9995BFBEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CE1EA9A
          5CA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5BA9E9995B6300000000000000000000
          00000000000000000000E697595BE9995BF9EA9A5CFEEA9A5CFEEA9A5CA90000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000E395584FE9995BF7EA9A5CFEEA9A5CA90000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000DE925745E9995BF3EA9A5BA90000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DC91553BE9995B840000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 1
      end
    end
    object Panel6: TPanel
      Left = 465
      Top = 1
      Width = 434
      Height = 169
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object GrList: TStringGrid
        Left = 0
        Top = 0
        Width = 434
        Height = 159
        Hint = 'X'#243'a danh s'#225'ch'
        Align = alTop
        BevelOuter = bvNone
        ColCount = 2
        Ctl3D = False
        DefaultColWidth = 76
        DefaultRowHeight = 19
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        ParentCtl3D = False
        ScrollBars = ssVertical
        TabOrder = 0
        OnDblClick = CmdDelExecute
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 584
    Width = 900
    Height = 41
    Align = alBottom
    TabOrder = 4
    DesignSize = (
      900
      41)
    object BtnContinute: TBitBtn
      Left = 336
      Top = 4
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdRegister
      Anchors = [akRight, akBottom]
      Caption = 'Th'#7921'c hi'#7879'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
        DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
        21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
        4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
        AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
        21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
        4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
        FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
        1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
        6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
        B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
        49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
        3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
        E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
        62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
        FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
        E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
        7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
        E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
        E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
        F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
        45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
        8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 467
      Top = 4
      Width = 96
      Height = 32
      Cursor = 1
      Hint = 'K'#7871't th'#250'c'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'K'#7871't th'#250'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
        00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
        78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
        F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
        A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
        7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
        16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
        C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
        7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
        210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
        B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
        82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
        6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
        C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
        85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
        FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
        CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
        88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
        240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
        DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
        78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
        FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
        B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      OnClick = CmdCloseExecute
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 171
    Width = 900
    Height = 170
    Align = alTop
    TabOrder = 1
    object PaEmp: TPanel
      Left = 1
      Top = 1
      Width = 898
      Height = 168
      Align = alClient
      BevelOuter = bvNone
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      object PgMain: TPageControl
        Left = 0
        Top = 0
        Width = 898
        Height = 168
        Cursor = 1
        ActivePage = TaDayOff
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrack = True
        ParentFont = False
        TabOrder = 0
        object TaDayOff: TTabSheet
          Caption = ' '#272#259'ng k'#253' '#273'i l'#224'm ng'#224'y th'#7913' 7'
          ImageIndex = 1
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 890
            Height = 140
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PaInfo: TisPanel
              Left = 0
              Top = 0
              Width = 890
              Height = 140
              Align = alClient
              BevelOuter = bvNone
              Color = 16119285
              ParentBackground = False
              TabOrder = 0
              HeaderCaption = ' :: Th'#244'ng tin '#273#259'ng k'#253
              HeaderColor = clHighlight
              ImageSet = 4
              RealHeight = 0
              ShowButton = False
              HeaderBevelInner = bvNone
              HeaderBevelOuter = bvNone
              HeaderFont.Charset = DEFAULT_CHARSET
              HeaderFont.Color = clWhite
              HeaderFont.Height = -11
              HeaderFont.Name = 'Tahoma'
              HeaderFont.Style = [fsBold]
              object Panel10: TPanel
                Left = 0
                Top = 16
                Width = 890
                Height = 31
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                DesignSize = (
                  890
                  31)
                object Label13: TLabel
                  Left = 518
                  Top = 9
                  Width = 38
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = 'V'#224'o ra'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label2: TLabel
                  Left = 609
                  Top = 8
                  Width = 5
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = '-'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object CbCaLamViec: TDbLookupComboboxEh2
                  Left = 191
                  Top = 6
                  Width = 286
                  Height = 22
                  ControlLabel.Width = 66
                  ControlLabel.Height = 16
                  ControlLabel.Caption = 'Ca l'#224'm vi'#7879'c'
                  ControlLabel.Font.Charset = DEFAULT_CHARSET
                  ControlLabel.Font.Color = clWindowText
                  ControlLabel.Font.Height = -13
                  ControlLabel.Font.Name = 'Tahoma'
                  ControlLabel.Font.Style = []
                  ControlLabel.ParentFont = False
                  ControlLabel.Visible = True
                  ControlLabelLocation.Spacing = 5
                  ControlLabelLocation.Position = lpLeftCenterEh
                  AlwaysShowBorder = True
                  AutoSize = False
                  BevelKind = bkFlat
                  Ctl3D = False
                  ParentCtl3D = False
                  BorderStyle = bsNone
                  Anchors = [akLeft, akTop, akRight]
                  DynProps = <>
                  DataField = 'MaCa'
                  DataSource = DsDummy
                  DropDownBox.Columns = <
                    item
                      FieldName = 'TenCa'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      SpecCell.Font.Charset = DEFAULT_CHARSET
                      SpecCell.Font.Color = clWindowText
                      SpecCell.Font.Height = -12
                      SpecCell.Font.Name = 'Tahoma'
                      SpecCell.Font.Style = []
                      Title.Alignment = taCenter
                      Title.Caption = 'T'#234'n'
                      Width = 200
                    end>
                  DropDownBox.ListSource = HrDataMain.DsDM_CALAMVIEC
                  DropDownBox.ListSourceAutoFilter = True
                  DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                  DropDownBox.ListSourceAutoFilterAllColumns = True
                  DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                  DropDownBox.AutoDrop = True
                  DropDownBox.Rows = 15
                  DropDownBox.Sizable = True
                  DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                  DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                  DropDownBox.SpecRow.Font.Color = clWindowText
                  DropDownBox.SpecRow.Font.Height = -12
                  DropDownBox.SpecRow.Font.Name = 'Tahoma'
                  DropDownBox.SpecRow.Font.Style = []
                  EmptyDataInfo.Color = clInfoBk
                  EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                  EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                  EmptyDataInfo.Font.Color = clSilver
                  EmptyDataInfo.Font.Height = -13
                  EmptyDataInfo.Font.Name = 'Tahoma'
                  EmptyDataInfo.Font.Style = [fsItalic]
                  EmptyDataInfo.ParentFont = False
                  EmptyDataInfo.Alignment = taLeftJustify
                  EditButton.DefaultAction = True
                  EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                  EditButton.Style = ebsAltDropDownEh
                  EditButton.Width = 20
                  EditButton.DrawBackTime = edbtWhenHotEh
                  EditButtons = <>
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Flat = True
                  KeyField = 'MaCa'
                  ListField = 'TenCa'
                  ListSource = HrDataMain.DsDM_CALAMVIEC
                  ParentFont = False
                  ShowHint = True
                  Style = csDropDownEh
                  TabOrder = 0
                  Visible = True
                end
                object DBNumberEditEh1: TDBNumberEditEh
                  Left = 665
                  Top = 6
                  Width = 35
                  Height = 22
                  TabStop = False
                  ControlLabel.Width = 17
                  ControlLabel.Height = 16
                  ControlLabel.BiDiMode = bdLeftToRight
                  ControlLabel.Caption = 'gi'#7901
                  ControlLabel.Font.Charset = DEFAULT_CHARSET
                  ControlLabel.Font.Color = clWindowText
                  ControlLabel.Font.Height = -13
                  ControlLabel.Font.Name = 'Tahoma'
                  ControlLabel.Font.Style = []
                  ControlLabel.ParentBiDiMode = False
                  ControlLabel.ParentFont = False
                  ControlLabel.Visible = True
                  ControlLabelLocation.Spacing = 5
                  ControlLabelLocation.Position = lpRightCenterEh
                  Anchors = [akTop, akRight]
                  BevelKind = bkFlat
                  BorderStyle = bsNone
                  Color = clBtnFace
                  Ctl3D = False
                  DataField = 'SoGio_Ca'
                  DataSource = DsDummy
                  DecimalPlaces = 1
                  DynProps = <>
                  EditButtons = <>
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentCtl3D = False
                  ParentFont = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 3
                  Visible = True
                end
                object dpGioRa1: TwwDBDateTimePicker
                  Left = 620
                  Top = 6
                  Width = 42
                  Height = 22
                  TabStop = False
                  Anchors = [akTop, akRight]
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  Color = clBtnFace
                  DataField = 'GioRa_Ca'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.729166666666666600
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  ShowButton = False
                  TabOrder = 2
                end
                object dpGioVao: TwwDBDateTimePicker
                  Left = 561
                  Top = 6
                  Width = 42
                  Height = 22
                  TabStop = False
                  Anchors = [akTop, akRight]
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  Color = clBtnFace
                  DataField = 'GioVao_Ca'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.312500000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  ShowButton = False
                  TabOrder = 1
                end
              end
              object GrDanhsach: TDBGridEh
                Left = 191
                Top = 46
                Width = 286
                Height = 93
                Align = alCustom
                BorderStyle = bsNone
                DataSource = DsGetAllSaturdayByMonth
                DynProps = <>
                FixedColor = 13360356
                IndicatorOptions = [gioShowRowIndicatorEh, gioShowRowselCheckboxesEh]
                Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                OptionsEh = [dghFixed3D, dghHighlightFocus, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
                RowDetailPanel.Color = clBtnFace
                SearchPanel.FilterEnabled = False
                STFilter.InstantApply = False
                TabOrder = 2
                TitleParams.Color = 13360356
                TitleParams.Font.Charset = DEFAULT_CHARSET
                TitleParams.Font.Color = 8404992
                TitleParams.Font.Height = -11
                TitleParams.Font.Name = 'Tahoma'
                TitleParams.Font.Style = [fsBold]
                TitleParams.MultiTitle = True
                TitleParams.ParentFont = False
                TitleParams.RowLines = 1
                Columns = <
                  item
                    CellButtons = <>
                    DynProps = <>
                    EditButtons = <>
                    FieldName = 'Ngay'
                    Footers = <>
                    Width = 100
                  end>
                object RowDetailData: TRowDetailPanelControlEh
                end
              end
            end
          end
        end
        object TaOvertime: TTabSheet
          Cursor = 1
          Caption = ' '#272#259'ng k'#253' ca, t'#259'ng ca'
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 890
            Height = 140
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object isPanel2: TisPanel
              Left = 0
              Top = 0
              Width = 890
              Height = 140
              Align = alClient
              BevelOuter = bvNone
              Color = 16119285
              ParentBackground = False
              TabOrder = 0
              HeaderCaption = ' :: Th'#244'ng tin '#273#259'ng k'#253
              HeaderColor = clHighlight
              ImageSet = 4
              RealHeight = 0
              ShowButton = False
              HeaderBevelInner = bvNone
              HeaderBevelOuter = bvNone
              HeaderFont.Charset = DEFAULT_CHARSET
              HeaderFont.Color = clWhite
              HeaderFont.Height = -11
              HeaderFont.Name = 'Tahoma'
              HeaderFont.Style = [fsBold]
              object Panel13: TPanel
                Left = 0
                Top = 16
                Width = 890
                Height = 124
                Align = alClient
                TabOrder = 1
                DesignSize = (
                  890
                  124)
                object Label1: TLabel
                  Left = 518
                  Top = 9
                  Width = 38
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = 'V'#224'o ra'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label3: TLabel
                  Left = 609
                  Top = 8
                  Width = 5
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = '-'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label108: TLabel
                  Left = 111
                  Top = 34
                  Width = 74
                  Height = 16
                  Alignment = taRightJustify
                  Caption = 'Ng'#224'y t'#259'ng ca'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label4: TLabel
                  Left = 125
                  Top = 58
                  Width = 61
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = 'OT '#272#7847'u ca'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label5: TLabel
                  Left = 239
                  Top = 58
                  Width = 5
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = '-'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label6: TLabel
                  Left = 307
                  Top = 58
                  Width = 64
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = 'OT Gi'#7919'a ca'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label9: TLabel
                  Left = 424
                  Top = 58
                  Width = 5
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = '-'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label10: TLabel
                  Left = 493
                  Top = 58
                  Width = 63
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = 'OT Cu'#7889'i ca'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label11: TLabel
                  Left = 609
                  Top = 58
                  Width = 5
                  Height = 16
                  Alignment = taRightJustify
                  Anchors = [akTop, akRight]
                  Caption = '-'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object CbCaLamViecTangCa: TDbLookupComboboxEh2
                  Left = 191
                  Top = 6
                  Width = 286
                  Height = 22
                  ControlLabel.Width = 66
                  ControlLabel.Height = 16
                  ControlLabel.Caption = 'Ca l'#224'm vi'#7879'c'
                  ControlLabel.Font.Charset = DEFAULT_CHARSET
                  ControlLabel.Font.Color = clWindowText
                  ControlLabel.Font.Height = -13
                  ControlLabel.Font.Name = 'Tahoma'
                  ControlLabel.Font.Style = []
                  ControlLabel.ParentFont = False
                  ControlLabel.Visible = True
                  ControlLabelLocation.Spacing = 5
                  ControlLabelLocation.Position = lpLeftCenterEh
                  AlwaysShowBorder = True
                  AutoSize = False
                  BevelKind = bkFlat
                  Ctl3D = False
                  ParentCtl3D = False
                  BorderStyle = bsNone
                  Anchors = [akLeft, akTop, akRight]
                  DynProps = <>
                  DataField = 'MaCa'
                  DataSource = DsDummy
                  DropDownBox.Columns = <
                    item
                      FieldName = 'TenCa'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      SpecCell.Font.Charset = DEFAULT_CHARSET
                      SpecCell.Font.Color = clWindowText
                      SpecCell.Font.Height = -12
                      SpecCell.Font.Name = 'Tahoma'
                      SpecCell.Font.Style = []
                      Title.Alignment = taCenter
                      Title.Caption = 'T'#234'n'
                      Width = 200
                    end>
                  DropDownBox.ListSource = HrDataMain.DsDM_CALAMVIEC
                  DropDownBox.ListSourceAutoFilter = True
                  DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                  DropDownBox.ListSourceAutoFilterAllColumns = True
                  DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                  DropDownBox.AutoDrop = True
                  DropDownBox.Rows = 15
                  DropDownBox.Sizable = True
                  DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                  DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                  DropDownBox.SpecRow.Font.Color = clWindowText
                  DropDownBox.SpecRow.Font.Height = -12
                  DropDownBox.SpecRow.Font.Name = 'Tahoma'
                  DropDownBox.SpecRow.Font.Style = []
                  EmptyDataInfo.Color = clInfoBk
                  EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                  EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                  EmptyDataInfo.Font.Color = clSilver
                  EmptyDataInfo.Font.Height = -13
                  EmptyDataInfo.Font.Name = 'Tahoma'
                  EmptyDataInfo.Font.Style = [fsItalic]
                  EmptyDataInfo.ParentFont = False
                  EmptyDataInfo.Alignment = taLeftJustify
                  EditButton.DefaultAction = True
                  EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                  EditButton.Style = ebsAltDropDownEh
                  EditButton.Width = 20
                  EditButton.DrawBackTime = edbtWhenHotEh
                  EditButtons = <>
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Flat = True
                  KeyField = 'MaCa'
                  ListField = 'TenCa'
                  ListSource = HrDataMain.DsDM_CALAMVIEC
                  ParentFont = False
                  ShowHint = True
                  Style = csDropDownEh
                  TabOrder = 0
                  Visible = True
                end
                object DBNumberEditEh3: TDBNumberEditEh
                  Left = 665
                  Top = 6
                  Width = 35
                  Height = 22
                  TabStop = False
                  ControlLabel.Width = 17
                  ControlLabel.Height = 16
                  ControlLabel.BiDiMode = bdLeftToRight
                  ControlLabel.Caption = 'gi'#7901
                  ControlLabel.Font.Charset = DEFAULT_CHARSET
                  ControlLabel.Font.Color = clWindowText
                  ControlLabel.Font.Height = -13
                  ControlLabel.Font.Name = 'Tahoma'
                  ControlLabel.Font.Style = []
                  ControlLabel.ParentBiDiMode = False
                  ControlLabel.ParentFont = False
                  ControlLabel.Visible = True
                  ControlLabelLocation.Spacing = 5
                  ControlLabelLocation.Position = lpRightCenterEh
                  Anchors = [akTop, akRight]
                  BevelKind = bkFlat
                  BorderStyle = bsNone
                  Color = clBtnFace
                  Ctl3D = False
                  DataField = 'SoGio_Ca'
                  DataSource = DsDummy
                  DecimalPlaces = 1
                  DynProps = <>
                  EditButtons = <>
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentCtl3D = False
                  ParentFont = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 3
                  Visible = True
                end
                object wwDBDateTimePicker1: TwwDBDateTimePicker
                  Left = 620
                  Top = 6
                  Width = 42
                  Height = 22
                  TabStop = False
                  Anchors = [akTop, akRight]
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  Color = clBtnFace
                  DataField = 'GioRa_Ca'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.729166666666666600
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  ShowButton = False
                  TabOrder = 2
                end
                object wwDBDateTimePicker2: TwwDBDateTimePicker
                  Left = 561
                  Top = 6
                  Width = 42
                  Height = 22
                  TabStop = False
                  Anchors = [akTop, akRight]
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  Color = clBtnFace
                  DataField = 'GioVao_Ca'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.312500000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  ShowButton = False
                  TabOrder = 1
                end
                object CbDate: TwwDBDateTimePicker
                  Left = 191
                  Top = 30
                  Width = 101
                  Height = 22
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'Ngay'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  ShowButton = True
                  TabOrder = 4
                end
                object dpDangKyTuGio: TwwDBDateTimePicker
                  Left = 191
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTDauCa_TuGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.375000000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 5
                end
                object dpDangKyDenGio: TwwDBDateTimePicker
                  Left = 250
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTDauCa_DenGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.562500000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 6
                end
                object wwDBDateTimePicker3: TwwDBDateTimePicker
                  Left = 376
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTGiuaCa_TuGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.375000000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 7
                end
                object wwDBDateTimePicker4: TwwDBDateTimePicker
                  Left = 435
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTGiuaCa_DenGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.562500000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 8
                end
                object wwDBDateTimePicker7: TwwDBDateTimePicker
                  Left = 561
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTCuoiCa_TuGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.375000000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 9
                end
                object wwDBDateTimePicker8: TwwDBDateTimePicker
                  Left = 620
                  Top = 54
                  Width = 42
                  Height = 22
                  ParentCustomHint = False
                  Anchors = [akTop, akRight]
                  BiDiMode = bdLeftToRight
                  AutoSize = False
                  BorderStyle = bsNone
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  DataField = 'OTCuoiCa_DenGio'
                  DataSource = DsDummy
                  Epoch = 1950
                  ButtonEffects.Transparent = True
                  ButtonEffects.Flat = True
                  Frame.Enabled = True
                  Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                  Time = 0.562500000000000000
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBiDiMode = False
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  ShowButton = False
                  TabOrder = 10
                end
              end
            end
          end
        end
        object TaAdditional: TTabSheet
          Caption = #272#259'ng k'#253' th'#234'm ca l'#224'm vi'#7879'c'
          ImageIndex = 2
          object isPanel1: TisPanel
            Left = 0
            Top = 0
            Width = 890
            Height = 140
            Align = alClient
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' :: Th'#244'ng tin '#273#259'ng k'#253
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 0
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = DEFAULT_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object Panel14: TPanel
              Left = 0
              Top = 16
              Width = 890
              Height = 124
              Align = alClient
              TabOrder = 1
              DesignSize = (
                890
                124)
              object Label7: TLabel
                Left = 518
                Top = 9
                Width = 38
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'V'#224'o ra'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label8: TLabel
                Left = 609
                Top = 8
                Width = 5
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = '-'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label12: TLabel
                Left = 157
                Top = 34
                Width = 28
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object CbCaLamViecDangKy: TDbLookupComboboxEh2
                Left = 191
                Top = 6
                Width = 286
                Height = 22
                ControlLabel.Width = 66
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ca l'#224'm vi'#7879'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaCa'
                DataSource = DsDummy
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenCa'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_CALAMVIEC
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaCa'
                ListField = 'TenCa'
                ListSource = HrDataMain.DsDM_CALAMVIEC
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object DBNumberEditEh2: TDBNumberEditEh
                Left = 665
                Top = 6
                Width = 35
                Height = 22
                TabStop = False
                ControlLabel.Width = 17
                ControlLabel.Height = 16
                ControlLabel.BiDiMode = bdLeftToRight
                ControlLabel.Caption = 'gi'#7901
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentBiDiMode = False
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpRightCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                Ctl3D = False
                DataField = 'SoGio_Ca'
                DataSource = DsDummy
                DecimalPlaces = 1
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object wwDBDateTimePicker5: TwwDBDateTimePicker
                Left = 620
                Top = 6
                Width = 42
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                AutoSize = False
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'GioRa_Ca'
                DataSource = DsDummy
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Time = 0.729166666666666600
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = False
                TabOrder = 2
              end
              object wwDBDateTimePicker6: TwwDBDateTimePicker
                Left = 561
                Top = 6
                Width = 42
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                AutoSize = False
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'GioVao_Ca'
                DataSource = DsDummy
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Time = 0.312500000000000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = False
                TabOrder = 1
              end
              object wwDBDateTimePicker9: TwwDBDateTimePicker
                Left = 191
                Top = 30
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'Ngay'
                DataSource = DsDummy
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 4
              end
            end
          end
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 341
    Width = 900
    Height = 41
    Align = alTop
    TabOrder = 2
    DesignSize = (
      900
      41)
    object BitBtn4: TBitBtn
      Left = 403
      Top = 4
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdContinue
      Anchors = [akRight, akBottom]
      Caption = 'Ti'#7871'p t'#7909'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
        DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
        21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
        4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
        AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
        21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
        4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
        FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
        1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
        6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
        B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
        49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
        3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
        E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
        62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
        FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
        E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
        7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
        E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
        E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
        F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
        45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
        8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 382
    Width = 900
    Height = 202
    Align = alClient
    TabOrder = 3
    object GrListData: TwwDBGrid2
      Left = 1
      Top = 1
      Width = 898
      Height = 200
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'ManvQL'#9'8'#9'M'#227#9'F'#9'Nh'#226'n vi'#234'n'
        'Tennv'#9'24'#9'H'#7885' t'#234'n'#9'F'#9'Nh'#226'n vi'#234'n'
        'Ngay'#9'10'#9#272#259'ng k'#253#9'F'#9'Ng'#224'y'
        'TenCa'#9'20'#9'Ca l'#224'm vi'#7879'c'#9'F'
        'SoGio_Ca'#9'6'#9'S'#7889' gi'#7901#9'F'#9'Ca l'#224'm vi'#7879'c'
        'GioVao_Ca'#9'6'#9'V'#224'o 1'#9'F'#9'Ca l'#224'm vi'#7879'c'
        'GioRa1_Ca'#9'6'#9'Ra 1'#9'F'#9'Ca l'#224'm vi'#7879'c'
        'GioVao1_Ca'#9'6'#9'V'#224'o 2'#9'F'#9'Ca l'#224'm vi'#7879'c'
        'GioRa_Ca'#9'6'#9'Ra 2'#9'F'#9'Ca l'#224'm vi'#7879'c'
        'OTDauCa_TuGio'#9'7'#9'T'#7915' gi'#7901#9'F'#9'OT '#272#7847'u ca'
        'OTDauCa_DenGio'#9'7'#9#272#7871'n gi'#7901#9'F'#9'OT '#272#7847'u ca'
        'OTGiuaCa_TuGio'#9'7'#9'T'#7915' gi'#7901#9'F'#9'OT Gi'#7919'a ca'
        'OTGiuaCa_DenGio'#9'7'#9#272#7871'n gi'#7901#9'F'#9'OT Gi'#7919'a ca'
        'OTCuoiCa_TuGio'#9'7'#9'T'#7915' gi'#7901#9'F'#9'OT Cu'#7889'i ca'
        'OTCuoiCa_DenGio'#9'7'#9#272#7871'n gi'#7901#9'F'#9'OT Cu'#7889'i ca'
        'ErrCode'#9'40'#9'ErrCode'#9'T')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick]
      Align = alClient
      DataSource = DsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListDataCalcCellColors
      ImageList = DataMain.ImageStatus
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 528
    Top = 104
    object CmdIns: TAction
      Hint = 'Th'#234'm v'#224'o danh s'#225'ch'
      ShortCut = 16429
      OnExecute = CmdInsExecute
    end
    object CmdDel: TAction
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdImport: TAction
      Caption = 'D'#225'n danh s'#225'ch nh'#226'n vi'#234'n t'#7915' file'
      OnExecute = CmdImportExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
    end
    object CmdFileMinus: TAction
      Caption = ' '
      Hint = 'X'#243'a file'
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
    end
    object CmdContinue: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      OnExecute = CmdContinueExecute
    end
    object CmdLyDoVangMat: TAction
    end
    object CmdImportExcel: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
    end
    object CmdRegister: TAction
      Caption = 'Th'#7921'c hi'#7879'n'
      OnExecute = CmdRegisterExecute
    end
    object CmdDelRecordError: TAction
      Caption = 'X'#243'a c'#225'c d'#242'ng '#273'ang l'#7895'i'
      OnExecute = CmdDelRecordErrorExecute
    end
    object CmdDelRecordSelected: TAction
      Caption = 'X'#243'a d'#242'ng '#273'ang ch'#7885'n'
      OnExecute = CmdDelRecordSelectedExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'order by a.Manv')
    Left = 744
    Top = 20
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 746
    Top = 64
  end
  object QrSite: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MaChiNhanh, TenChiNhanh'
      '  from HR_DM_CHINHANH'
      'order by MaChiNhanh')
    Left = 772
    Top = 16
  end
  object QrDep: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsSite
    Parameters = <
      item
        Name = 'MaChiNhanh'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'Ma, MaPhongBan, TenPhongBan, MaChiNhanh'
      '  from'#9'HR_DM_PHONGBAN'
      ' where  MaChiNhanh = :MaChiNhanh'
      'order by MaPhongBan')
    Left = 804
    Top = 16
  end
  object QrSec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsDep
    Parameters = <
      item
        Name = 'MaPhongBan'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'Ma, MaBoPhan, TenBoPhan, MaPhongBan'
      '  from'#9'HR_DM_BOPHAN'
      'where    MaPhongBan = :MaPhongBan'
      'order by MaBoPhan')
    Left = 840
    Top = 16
  end
  object DsSec: TDataSource
    DataSet = QrSec
    Left = 840
    Top = 56
  end
  object DsDep: TDataSource
    DataSet = QrDep
    Left = 808
    Top = 56
  end
  object DsSite: TDataSource
    DataSet = QrSite
    Left = 776
    Top = 56
  end
  object spIMP_HR_LICHLV_ImportList: TADOCommand
    CommandText = 'spIMP_HR_DANGKY_LICHLV_ImportList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIdxTransNo'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pCreateFunc'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pCreateSource'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pLoai'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pChuoi'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 2000
        Value = Null
      end
      item
        Name = '@pNgay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pMaCa'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@pPageIndex'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pOTDauCa_TuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTDauCa_DenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTDauCa_SoGio'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pOTGiuaCa_TuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTGiuaCa_DenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTGiuaCa_SoGio'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pOTCuoiCa_TuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTCuoiCa_DenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pOTCuoiCa_SoGio'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 716
    Top = 108
  end
  object TbDummy: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'GhiChu'
        DataType = ftWideMemo
      end
      item
        Name = 'MaCa'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'DauCa'
        DataType = ftInteger
      end
      item
        Name = 'GioVao_Ca'
        DataType = ftDateTime
      end
      item
        Name = 'GioRa_Ca'
        DataType = ftDateTime
      end
      item
        Name = 'SoGio_Ca'
        DataType = ftFloat
      end
      item
        Name = 'Ngay'
        DataType = ftDateTime
      end
      item
        Name = 'TuGio'
        DataType = ftDateTime
      end
      item
        Name = 'DenGio'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    AfterInsert = TbDummyAfterInsert
    Left = 664
    Top = 24
    object TbDummyMaCa: TWideStringField
      DisplayLabel = 'Ca l'#224'm vi'#7879'c'
      FieldName = 'MaCa'
      OnChange = TbDummyMaCaChange
    end
    object TbDummyGioVao_Ca: TDateTimeField
      FieldName = 'GioVao_Ca'
    end
    object TbDummyGioRa_Ca: TDateTimeField
      FieldName = 'GioRa_Ca'
    end
    object TbDummySoGio_Ca: TFloatField
      FieldName = 'SoGio_Ca'
    end
    object TbDummyNgay: TDateTimeField
      DisplayLabel = 'Ng'#224'y t'#259'ng ca'
      FieldName = 'Ngay'
    end
    object TbDummyLK_GioVao: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object TbDummyLK_GioRa: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object TbDummyLK_GioVao1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioVao1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioVao1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object TbDummyLK_GioRa1: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_GioRa1'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'GioRa1'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object TbDummyLK_SoGio: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_SoGio'
      LookupDataSet = HrDataMain.QrDM_CALAMVIEC
      LookupKeyFields = 'MaCa'
      LookupResultField = 'SoGio'
      KeyFields = 'MaCa'
      Lookup = True
    end
    object TbDummyOTDauCa_TuGio: TDateTimeField
      FieldName = 'OTDauCa_TuGio'
      OnChange = TbDummyOTDauCa_TuGioChange
    end
    object TbDummyOTDauCa_DenGio: TDateTimeField
      FieldName = 'OTDauCa_DenGio'
      OnChange = TbDummyOTDauCa_TuGioChange
    end
    object TbDummyOTGiuaCa_TuGio: TDateTimeField
      FieldName = 'OTGiuaCa_TuGio'
      OnChange = TbDummyOTGiuaCa_TuGioChange
    end
    object TbDummyOTGiuaCa_DenGio: TDateTimeField
      FieldName = 'OTGiuaCa_DenGio'
      OnChange = TbDummyOTGiuaCa_TuGioChange
    end
    object TbDummyOTCuoiCa_TuGio: TDateTimeField
      FieldName = 'OTCuoiCa_TuGio'
      OnChange = TbDummyOTCuoiCa_TuGioChange
    end
    object TbDummyOTCuoiCa_DenGio: TDateTimeField
      FieldName = 'OTCuoiCa_DenGio'
      OnChange = TbDummyOTCuoiCa_TuGioChange
    end
    object TbDummyOTDauCa_SoGio: TFloatField
      FieldName = 'OTDauCa_SoGio'
    end
    object TbDummyOTGiuaCa_SoGio: TFloatField
      FieldName = 'OTGiuaCa_SoGio'
    end
    object TbDummyOTCuoiCa_SoGio: TFloatField
      FieldName = 'OTCuoiCa_SoGio'
    end
    object TbDummyManv: TWideStringField
      FieldName = 'Manv'
    end
  end
  object DsDummy: TDataSource
    DataSet = TbDummy
    Left = 663
    Top = 65
  end
  object DsEmpStatus: TDataSource
    AutoEdit = False
    DataSet = QrEmpStatus
    Left = 564
    Top = 64
  end
  object QrEmpStatus: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 560
    Top = 36
  end
  object spIMP_HR_DANGKY_LICHLV_UpInsertData: TADOCommand
    CommandText = 'spIMP_HR_DANGKY_LICHLV_UpInsertData;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pPageIndex'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 764
    Top = 476
  end
  object spIMP_HR_LICHLV_DeleteList: TADOCommand
    CommandText = 'spIMP_HR_DANGKY_LICHLV_DeleteList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIsError'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 764
    Top = 508
  end
  object CHECK_RECORD_ERROR: TADOCommand
    CommandText = 
      'select 1 from IMP_HR_BANG_LICHLV where TransNo=:TransNo and isnu' +
      'll(ErrCode, '#39#39') <> '#39#39
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 616
    Top = 492
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'ManvQL'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 572
    Top = 472
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrListBeforeOpen
    BeforeDelete = QrListBeforeDelete
    AfterDelete = QrListAfterDelete
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'IMP_HR_BANG_LICHLV'
      'where TransNo=:TransNo')
    Left = 504
    Top = 476
    object QrListSheet: TWideStringField
      FieldName = 'Sheet'
      Size = 200
    end
    object QrListErrCode: TWideStringField
      FieldName = 'ErrCode'
      Size = 200
    end
    object QrListLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrListLK_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrListManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrListManvQL: TWideStringField
      FieldName = 'ManvQL'
    end
    object QrListTennv: TWideStringField
      FieldName = 'Tennv'
      Size = 200
    end
    object QrListNgay: TDateTimeField
      FieldName = 'Ngay'
    end
    object QrListMaCa: TWideStringField
      FieldName = 'MaCa'
    end
    object QrListTenCa: TWideStringField
      FieldName = 'TenCa'
      Size = 200
    end
    object QrListNam: TIntegerField
      FieldName = 'Nam'
    end
    object QrListThang: TIntegerField
      FieldName = 'Thang'
    end
    object QrListSoCa: TIntegerField
      FieldName = 'SoCa'
    end
    object QrListLoaiNgay: TIntegerField
      FieldName = 'LoaiNgay'
    end
    object QrListGioVao_Ca: TDateTimeField
      FieldName = 'GioVao_Ca'
    end
    object QrListGioRa_Ca: TDateTimeField
      FieldName = 'GioRa_Ca'
    end
    object QrListGioVao1_Ca: TDateTimeField
      FieldName = 'GioVao1_Ca'
    end
    object QrListGioRa1_Ca: TDateTimeField
      FieldName = 'GioRa1_Ca'
    end
    object QrListSoGio_Ca: TFloatField
      FieldName = 'SoGio_Ca'
    end
    object QrListDiTre_Ca: TDateTimeField
      FieldName = 'DiTre_Ca'
    end
    object QrListVeSom_Ca: TDateTimeField
      FieldName = 'VeSom_Ca'
    end
    object QrListOTDauCa_TuGio: TDateTimeField
      FieldName = 'OTDauCa_TuGio'
    end
    object QrListOTDauCa_DenGio: TDateTimeField
      FieldName = 'OTDauCa_DenGio'
    end
    object QrListOTGiuaCa_TuGio: TDateTimeField
      FieldName = 'OTGiuaCa_TuGio'
    end
    object QrListOTGiuaCa_DenGio: TDateTimeField
      FieldName = 'OTGiuaCa_DenGio'
    end
    object QrListOTCuoiCa_TuGio: TDateTimeField
      FieldName = 'OTCuoiCa_TuGio'
    end
    object QrListOTCuoiCa_DenGio: TDateTimeField
      FieldName = 'OTCuoiCa_DenGio'
    end
    object QrListOTDauCa_SoGio: TFloatField
      FieldName = 'OTDauCa_SoGio'
    end
    object QrListOTGiuaCa_SoGio: TFloatField
      FieldName = 'OTGiuaCa_SoGio'
    end
    object QrListOTCuoiCa_SoGio: TFloatField
      FieldName = 'OTCuoiCa_SoGio'
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 500
    Top = 516
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 484
    Top = 496
    object Tm1: TMenuItem
      Action = CmdSearch
      Visible = False
    end
    object Xadng1: TMenuItem
      Action = CmdDelRecordSelected
    end
    object Xaccdngangli1: TMenuItem
      Action = CmdDelRecordError
    end
  end
  object DsGetAllSaturdayByMonth: TDataSource
    DataSet = getAllSaturdayByMonth
    Left = 39
    Top = 257
  end
  object QrDays: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'DangKyTuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'DangKyDenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'MaCa'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'select dbo.fnHR_Tangca_Tinhgio(:Manv, :Ngay, :DangKyTuGio, :Dang' +
        'KyDenGio, :MaCa)')
    Left = 68
    Top = 252
  end
  object spHR_BANG_LICHLV_Invalid_GioTangCa: TADOCommand
    CommandText = 'spHR_BANG_LICHLV_Invalid_GioTangCa;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MaCa'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TuGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DenGio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@LoaiCa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 288
  end
  object getAllSaturdayByMonth: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    ProcedureName = 'getAllSaturdayByMonth;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 228
    object getAllSaturdayByMonthNgay: TDateTimeField
      DisplayWidth = 18
      FieldName = 'Ngay'
    end
  end
end
