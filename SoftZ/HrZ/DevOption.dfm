object FrmDevOption: TFrmDevOption
  Left = 298
  Top = 103
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'T'#249'y ch'#7885'n'
  ClientHeight = 419
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 8
    Top = 8
    Width = 406
    Height = 403
    DisableThemes = False
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 0
    Items = <
      item
        Caption = 'Registrable administrators number'
        WordWrap = False
      end
      item
        Caption = 'Device ID'
        WordWrap = False
      end
      item
        Caption = 'Languages'
        PickList.Items.Strings = (
          'English'#9'0'
          'Vietnamese / Chinese'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Auto power off time'
        WordWrap = False
      end
      item
        Caption = 'Lock control delay (20ms)'
        WordWrap = False
      end
      item
        Caption = 'In and out record warning'
        WordWrap = False
      end
      item
        Caption = 'Manage record warning'
        WordWrap = False
      end
      item
        Caption = 'Confirm interval time'
        WordWrap = False
      end
      item
        Caption = 'Baud Rate'
        PickList.Items.Strings = (
          '1200 bps'#9'0'
          '2400 bps'#9'1'
          '4800 bps'#9'2'
          '9600 bps'#9'3'
          '19200 bps'#9'4'
          '38400 bps'#9'5'
          '115200 bps'#9'6')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Even and Odd'
        PickList.Items.Strings = (
          'Nothing'#9'0'
          'Even'#9'1'
          'Odd'#9'2')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Stop bit'
        PickList.Items.Strings = (
          'One'#9'0'
          'Two'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Date list separator'
        PickList.Items.Strings = (
          '"/"'#9'0'
          '"-"'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Network'
        PickList.Items.Strings = (
          'Off'#9'0'
          'On'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'RS232'
        PickList.Items.Strings = (
          'Off'#9'0'
          'On'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'RS485'
        PickList.Items.Strings = (
          'Off'#9'0'
          'On'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Voice'
        PickList.Items.Strings = (
          'Off'#9'0'
          'On'#9'1')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Identification speed'
        PickList.Items.Strings = (
          'Low speed'#9'0'
          'High speed'#9'1'
          'Auto'#9'2')
        PickList.MapList = True
        PickList.Style = csDropDownList
        WordWrap = False
      end
      item
        Caption = 'Idle'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Shutdown time'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'PowerOn time'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Sleep time'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Auto Bell'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Comparing threshold'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Matching threshold'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = '1:1 threshold'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Show score'
        PickList.Style = csSimple
        WordWrap = False
      end
      item
        Caption = 'Unlock person count'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Only verify number card'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Net speed'
        PickList.Items.Strings = (
          'Auto'#9'8'
          '100M Full Duplex'#9'5'
          '10M Full Duplex'#9'4'
          '100M Half Duplex'#9'1'
          '10M Half Duplex'#9'0')
        PickList.MapList = True
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Must register card'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Time out of temp state keep'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Time out of input number'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Time out of menu keep'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Date format'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end
      item
        Caption = 'Only 1:1'
        PickList.Style = csOwnerDrawFixed
        WordWrap = False
      end>
    CaptionWidth = 225
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    PaintOptions.BackgroundOptions = [coFillDataCells]
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
  end
  object BitBtn3: TBitBtn
    Left = 424
    Top = 116
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdShutdown
    Caption = 'T'#7855't m'#225'y'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object BitBtn4: TBitBtn
    Left = 424
    Top = 386
    Width = 101
    Height = 25
    Cursor = 1
    Cancel = True
    Caption = 'K'#7871't th'#250'c'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 6
  end
  object BitBtn1: TBitBtn
    Left = 424
    Top = 8
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdLoadOption
    Caption = #272#7885'c th'#244'ng s'#7889
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 424
    Top = 36
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdSaveOption
    Caption = 'L'#432'u th'#244'ng s'#7889
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object BitBtn5: TBitBtn
    Left = 424
    Top = 84
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdRestart
    Caption = 'Kh'#7903'i '#273#7897'ng l'#7841'i'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object BitBtn6: TBitBtn
    Left = 424
    Top = 239
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdReset
    Caption = 'Kh'#7903'i t'#7841'o l'#7841'i'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
  object BitBtn7: TBitBtn
    Left = 424
    Top = 148
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdDeleteLog
    Caption = 'X'#243'a d'#7919' li'#7879'u c'#244'ng'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
  end
  object BitBtn8: TBitBtn
    Left = 424
    Top = 180
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdSynTime
    Caption = #272#7891'ng b'#7897' gi'#7901
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 173
    Top = 165
    object CmdLoadOption: TAction
      Caption = #272#7885'c th'#244'ng s'#7889
      OnExecute = CmdLoadOptionExecute
    end
    object CmdSaveOption: TAction
      Caption = 'L'#432'u th'#244'ng s'#7889
      OnExecute = CmdSaveOptionExecute
    end
    object CmdShutdown: TAction
      Caption = 'T'#7855't m'#225'y'
      Hint = 'T'#7855't m'#225'y v'#224' '#273#243'ng c'#7917'a s'#7893
      OnExecute = CmdShutdownExecute
    end
    object CmdRestart: TAction
      Caption = 'Kh'#7903'i '#273#7897'ng l'#7841'i'
      Hint = 'Sau khi kh'#7903'i '#273#7897'ng m'#225'y bu'#7897'c ph'#7843'i k'#7871't n'#7889'i l'#7841'i'
      OnExecute = CmdRestartExecute
    end
    object CmdReset: TAction
      Caption = 'Kh'#7903'i t'#7841'o l'#7841'i'
      Hint = 'X'#243'a to'#224'n b'#7897' d'#7919' li'#7879'u trong m'#225'y'
      OnExecute = CmdResetExecute
    end
    object CmdDeleteLog: TAction
      Caption = 'X'#243'a d'#7919' li'#7879'u c'#244'ng'
      OnExecute = CmdDeleteLogExecute
    end
    object CmdSynTime: TAction
      Caption = #272#7891'ng b'#7897' gi'#7901
      OnExecute = CmdSynTimeExecute
    end
  end
end
