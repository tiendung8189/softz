﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmnv;

interface

uses
  SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Grids, Wwdbigrd,
  Wwdbgrid, Mask, RzPanel, ToolWin, wwdblook, Buttons, wwDataInspector,
  DBCtrlsEh;

type
  TFrmDmnv = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMNV: TADOQuery;
    DsDMNV: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton10: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    EdTen: TDBEditEh;
    DBEdit2: TDBEditEh;
    DBEdit3: TDBEditEh;
    DBEdit5: TDBEditEh;
    EdMa: TDBEditEh;
    PD5: TisPanel;
    EdGhichu: TDBMemo;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    CmdReload: TAction;
    QrDMNVMANV: TWideStringField;
    QrDMNVTENNV: TWideStringField;
    QrDMNVDTHOAI: TWideStringField;
    QrDMNVEMAIL: TWideStringField;
    QrDMNVCREATE_BY: TIntegerField;
    QrDMNVUPDATE_BY: TIntegerField;
    QrDMNVCREATE_DATE: TDateTimeField;
    QrDMNVUPDATE_DATE: TDateTimeField;
    Label1: TLabel;
    ALLOC_MANV: TADOCommand;
    N1: TMenuItem;
    mnObsolete: TMenuItem;
    PD2: TisPanel;
    CbNGAY: TwwDBDateTimePicker;
    Label30: TLabel;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    CbChucvu: TwwDBLookupCombo;
    wwDBLookupCombo2: TwwDBLookupCombo;
    wwDBEdit1: TDBEditEh;
    wwDBEdit2: TDBEditEh;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label7: TLabel;
    PD4: TisPanel;
    SpeedButton2: TSpeedButton;
    DBEdit7: TDBEditEh;
    DBEdit8: TDBEditEh;
    DBEdit9: TDBEditEh;
    QrDMNVLK_MANH: TWideStringField;
    QrDMNVLK_MACN: TWideStringField;
    RemuInspect: TwwDataInspector;
    CmdDmtk: TAction;
    Label14: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    QrDMNVLK_TENNH: TWideStringField;
    QrDMNVLK_TENCN: TWideStringField;
    Label15: TLabel;
    Label16: TLabel;
    wwDBLookupCombo1: TwwDBLookupCombo;
    wwDBLookupCombo3: TwwDBLookupCombo;
    QrDMNVMACC: TWideStringField;
    QrDMNVPTTT: TWideStringField;
    Label9: TLabel;
    CbSession: TwwDBLookupCombo;
    PaPhepnam: TisPanel;
    Label17: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    EdPhepnam: TDBNumberEditEh;
    Label18: TLabel;
    wwDBEdit3: TDBEditEh;
    isPanel1: TisPanel;
    QrDMNVNAMSINH: TIntegerField;
    Label21: TLabel;
    wwDBLookupCombo4: TwwDBLookupCombo;
    Label22: TLabel;
    wwDBLookupCombo5: TwwDBLookupCombo;
    Label23: TLabel;
    wwDBLookupCombo6: TwwDBLookupCombo;
    Label24: TLabel;
    wwDBLookupCombo7: TwwDBLookupCombo;
    wwDBEdit4: TDBEditEh;
    wwDBEdit5: TDBEditEh;
    CmdEdit: TAction;
    ToolButton8: TToolButton;
    ToolButton12: TToolButton;
    PopMore: TAdvPopupMenu;
    CmdThoiviec: TAction;
    hivic1: TMenuItem;
    BtnChitiet: TToolButton;
    ToolButton14: TToolButton;
    QrDMNVMaChiNhanh: TWideStringField;
    QrDMNVMaPhongBan: TWideStringField;
    QrDMNVMaBoPhan: TWideStringField;
    QrDMNVNoiSinh_MaTinh: TWideStringField;
    QrDMNVNoiSinh_DiaChi: TWideStringField;
    QrDMNVCCCD: TWideStringField;
    QrDMNVCCCD_NgayCap: TDateTimeField;
    QrDMNVCCCD_NoiCap: TWideStringField;
    QrDMNVCCCD_GhiChu: TWideStringField;
    QrDMNVMaSoThue: TWideStringField;
    QrDMNVMaNoiLV: TWideStringField;
    QrDMNVCuTru_DiaChi: TWideStringField;
    QrDMNVThuongTru_DiaChi: TWideStringField;
    QrDMNVEmailCaNhan: TWideStringField;
    QrDMNVNgayVaoLam: TDateTimeField;
    QrDMNVNgayThamNien: TDateTimeField;
    QrDMNVLuongChinh: TFloatField;
    QrDMNVLuongBaoHiem: TFloatField;
    QrDMNVCo_BangCong: TBooleanField;
    QrDMNVCo_BangLuong: TBooleanField;
    QrDMNVCo_BHXH: TBooleanField;
    QrDMNVTinhTrang: TIntegerField;
    QrDMNVNgayThoiViec: TDateTimeField;
    QrDMNVNgayTraTaiSan: TDateTimeField;
    QrDMNVNgayQuyetToan: TDateTimeField;
    QrDMNVBiDanh: TWideStringField;
    QrDMNVNgaySinh: TDateTimeField;
    QrDMNVGioiTinh: TIntegerField;
    QrDMNVQuocTich: TWideStringField;
    QrDMNVDanToc: TWideStringField;
    QrDMNVTonGiao: TWideStringField;
    QrDMNVVanHoa: TWideStringField;
    QrDMNVMaChucDanh: TWideStringField;
    QrDMNVMaNhomLV: TWideStringField;
    QrDMNVSoNgay_PhepNam: TFloatField;
    QrDMNVMATK: TWideStringField;
    QrDMNVGhiChu: TWideMemoField;
    QrDMNVGhiChu_ThoiViec: TWideStringField;
    QrDMNVMaQTLV_ThoiViec: TWideStringField;
    QrDMNVNgayDangKyThoiViec: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMNVBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMNVBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMNVBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMNVAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDMNVAfterInsert(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure mnObsoleteClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure QrDMNVMANVChange(Sender: TField);
    procedure CmdDmtkExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMNVNGAY_VAOLAMChange(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure CmdThoiviecExecute(Sender: TObject);
    procedure BtnChitietClick(Sender: TObject);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mObsolete: Boolean;
    mSQL: String;
    fPhepnam: Double;

    function  AllocMANV(pLen: Integer): String;
  public
  	function Execute(r: WORD): String;
  end;

var

  FrmDmnv: TFrmDmnv;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, RepEngine, isLib, isCommon, DmTK_NV,
    DmnvThoiviec, HrData;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHANVIEN';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
function TFrmDmnv.Execute;
begin
	mCanEdit := rCanEdit(r);

	EdGhichu.ReadOnly := not mCanEdit;

    mRet := False;
    ShowModal;
    Result := QrDMNV.FieldByName('Manv').AsString;
    CloseDataSets([QrDMNV]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    AddFields(QrDMNV, FORM_CODE);

    mTrigger := False;
    PaDetail.VertScrollBar.Position := 0;

    PD5.Collapsed := RegReadBool(name, 'PD5');
    PD2.Collapsed := RegReadBool(name, 'PD2');
    mObsolete     := RegReadBool('Obsolete');

    PD2.Visible := GetRights('HR_NHANVIEN_LUONG', False) <> R_DENY;

    mAutoCode := FlexConfigBool(FORM_CODE, 'Auto Code');
    fPhepnam := FlexConfigInteger(FORM_CODE, 'Songay Phepnam', 12);
    fixCode := SetCodeLength(FORM_CODE, QrDMNV.FieldByName('Manv'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormShow(Sender: TObject);
begin
    EdMa.ReadOnly := mAutoCode;

    with DataMain do
        OpenDataSets([QrSite, QrNganhang, QrNganhangCN]);

    with HrDataMain do
        OpenDataSets([QrDMPHONGBAN, QrDM_CHUCDANH, QrDM_NHOMLV,
        QrLOAI_GIOITINH, QrV_QUOCTICH, QrV_DANTOC, QrV_TONGIAO, QrV_VANHOA]);

    SetShortDateFormat(QrDMNV);
    SetDisplayFormat(QrDMNV, sysCurFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMNV, FORM_CODE, Filter);

	mSQL := QrDMNV.SQL.Text;

    CmdReload.Execute;

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if (Field.FullName = 'Manv') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;

    if QrDMNV.FieldByName('NgayThoiViec').AsFloat > 10 then
    begin
	    AFont.Color := clRed;
        Exit;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.mnObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;

	//Save state panel
    RegWrite(Name, ['PD5','PD2', 'Obsolete'],
        [PD5.Collapsed,PD2.Collapsed, mObsolete]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMNV, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdNewExecute(Sender: TObject);
begin
    QrDMNV.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdSaveExecute(Sender: TObject);
begin
    QrDMNV.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdCancelExecute(Sender: TObject);
begin
    QrDMNV.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdDelExecute(Sender: TObject);
begin
    QrDMNV.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdDmtkExecute(Sender: TObject);
var
    r: WORD;
    pMadt: String;
begin
    r := GetRights('ACC_DM_TAIKHOAN_NH');
    if r = R_DENY then
    	Exit;

    with QrDMNV do
    begin
        pMadt := FieldByName('Manv').AsString;

        Application.CreateForm(TFrmDmTK_NV, FrmDmTK_NV);
        if FrmDmTK_NV.Execute(r, pMadt, False) then
        begin
            DataMain.QrDMTK.Requery();
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdEditExecute(Sender: TObject);
begin
	QrDMNV.Edit;
    EdMA.SetFocus;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMNV)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdThoiviecExecute(Sender: TObject);
var
	r: WORD;
    mEmpID, mName: String;
begin
	r := GetRights('HR_NV_NGHIVIEC');
    if r = R_DENY then
    	Exit;

	with QrDMNV do
    begin
	    mEmpID := FieldByName('Manv').AsString;
        mName := FieldByName('Tennv').AsString;
    end;

	Application.CreateForm(TFrmDmnvThoiviec, FrmDmnvThoiviec);
    if not FrmDmnvThoiviec.Execute (r, mEmpID, mEmpID, mName) then
    	Exit;

    exReSyncRecord(QrDMNV);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrDMNV, Filter, mCanEdit);

	with QrDMNV do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdReload.Enabled := bBrowse;
    CmdContact.Enabled := bBrowse and (not bEmpty);

    CmdThoiviec.Enabled := not bEmpty;
    CmdDmtk.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMNV);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.PopupMenu1Popup(Sender: TObject);
begin
    mnObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_PHEPNAM_INVALID = 'Số ngày tính phép năm phải >= %f';
procedure TFrmDmnv.QrDMNVBeforePost(DataSet: TDataSet);
begin
	with QrDMNV do
    begin
    	if not mAutoCode then
        begin
            if BlankConfirm(DataSet, ['Manv']) then
        	    Abort;

            if fixCode then
            	if LengthConfirm(FieldByName('Manv')) then
                	Abort;
        end
        else
    		if State in [dsInsert] then
            	FieldByName('Manv').AsString :=
                	AllocMANV(FieldByName('Manv').DisplayWidth);

        if BlankConfirm(DataSet, ['Tennv', 'NgayVaoLam', 'NgayThamNien',
            'MaPhongBan', 'MaChucDanh', 'MaNhomLV']) then
        	Abort;

        if FieldByName('SoNgayPhepNam').AsFloat < fPhepnam then
        begin
            ErrMsg(Format(RS_PHEPNAM_INVALID, [fPhepnam]));
            EdPhepnam.SetFocus;
            Abort;
        end;

        if FieldByName('NgayThoiViec').AsFloat > 100 then
        begin
            if BlankConfirm(DataSet, ['MaQTLV_ThoiViec']) then
                Abort;
        end
    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVMANVChange(Sender: TField);
var
    mMa, s: String;
    b: Boolean;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	with QrDMNV do
    begin
		b := FieldByName('MaCC').AsString = '';
    	if not b then
        	b := YesNo(RS_CODE_DEF);
		if b then
			FieldByName('MaCC').AsString := FieldByName('Manv').AsString;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVNGAY_VAOLAMChange(Sender: TField);
begin
    with QrDMNV do
    begin
        if FieldByName('NgayThamNien').AsFloat < 10 then
            FieldByName('NgayThamNien').AsDateTime := FieldByName('NgayVaoLam').AsDateTime;
    end;
end;

procedure TFrmDmnv.QrDMNVBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMNV, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.BtnChitietClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CbNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmnv.AllocMANV(pLen: Integer): String;
begin
    with ALLOC_MANV do
    begin
        Prepared := True;
        Parameters[1].Value := pLen;
        Execute;
        Result := Parameters[2].Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdRefreshExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.CmdReloadExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;
	with QrDMNV do
    begin
    	Close;
        if not mObsolete then
            sSQL := sSQL + ' where isnull(NgayThoiViec, 0) <= 0';
        SQL.Text := sSQL;
        SQL.Add(' order by 	Manv');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnv.QrDMNVAfterInsert(DataSet: TDataSet);
begin
	with QrDMNV do
	begin
        FieldByName('Co_BangCong').AsBoolean := True;
        FieldByName('Co_BangLuong').AsBoolean := True;
        FieldByName('Co_BaoHiem').AsBoolean := True;
        FieldByName('MaChiNhanh').AsString := sysSite;
        FieldByName('MaPhongBan').AsString := HrDataMain.QrDMPHONGBAN.FieldByName('MaPhongBan').AsString;
        FieldByName('MaNhomLV').AsString := HrDataMain.QrDM_NHOMLV.FieldByName('MaNhomLV').AsString;
        FieldByName('MaChucDanh').AsString := HrDataMain.QrDM_CHUCDANH.FieldByName('MaChucDanh').AsString;
        FieldByName('GioiTinh').AsInteger := HrDataMain.QrLOAI_GIOITINH.FieldByName('MA_HOTRO').AsInteger;
        FieldByName('SoNgayPhepNam').AsFloat := fPhepnam;
    end
end;
End.
