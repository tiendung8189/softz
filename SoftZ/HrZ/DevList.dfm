object FrmDevList: TFrmDevList
  Left = 231
  Top = 120
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'M'#225'y Ch'#7845'm C'#244'ng'
  ClientHeight = 409
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 388
    Width = 695
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 339
    Top = 36
    Width = 356
    Height = 352
    Align = alRight
    BorderOuter = fsLowered
    BorderSides = []
    HotSpotHighlight = 11855600
    HotSpotIgnoreMargins = True
    HotSpotVisible = True
    LockBar = True
    SizeBarWidth = 7
    TabOrder = 2
    object PD1: TisPanel
      Left = 8
      Top = 0
      Width = 348
      Height = 352
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = 14474460
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = ' :: Th'#244'ng tin'
      HeaderColor = 14474460
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object Label3: TLabel
        Left = 23
        Top = 80
        Width = 66
        Height = 16
        Alignment = taRightJustify
        Caption = 'Ki'#7875'u k'#7871't n'#7889'i'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 37
        Top = 56
        Width = 52
        Height = 16
        Alignment = taRightJustify
        Caption = 'Ph'#226'n lo'#7841'i'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LbRate: TLabel
        Left = 31
        Top = 128
        Width = 58
        Height = 16
        Alignment = taRightJustify
        Caption = 'Baud Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LbPort: TLabel
        Left = 216
        Top = 128
        Width = 23
        Height = 16
        Alignment = taRightJustify
        Caption = 'Port'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object BitBtn1: TSpeedButton
        Left = 12
        Top = 173
        Width = 121
        Height = 25
        Cursor = 1
        Action = CmdConnect
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BitBtn2: TSpeedButton
        Left = 12
        Top = 234
        Width = 121
        Height = 25
        Cursor = 1
        Action = CmdRestoreDevData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BitBtn3: TSpeedButton
        Left = 12
        Top = 203
        Width = 121
        Height = 25
        Cursor = 1
        Action = CmdBackupDevData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BitBtn4: TSpeedButton
        Left = 12
        Top = 265
        Width = 121
        Height = 25
        Cursor = 1
        Action = CmdOption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CbRate: TwwDBComboBox
        Left = 96
        Top = 124
        Width = 101
        Height = 22
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'BAUD_RATE'
        DataSource = DsListRec
        DropDownCount = 8
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ItemHeight = 0
        ParentCtl3D = False
        ParentFont = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object EdTENMAY: TDBEditEh
        Tag = 1
        Left = 96
        Top = 28
        Width = 225
        Height = 22
        AutoSize = False
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 50
        ControlLabel.Height = 16
        ControlLabel.Caption = 'T'#234'n m'#225'y'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'TENMAY'
        DataSource = DsListRec
        DynProps = <>
        EditButtons = <>
        EmptyDataInfo.Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 1
        Visible = True
      end
      object CbComType: TwwDBComboBox
        Left = 96
        Top = 76
        Width = 225
        Height = 22
        ShowButton = True
        Style = csDropDown
        MapList = True
        AllowClearKey = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'COM_TYPE'
        DataSource = DsListRec
        DropDownCount = 8
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ItemHeight = 0
        Items.Strings = (
          'Serial Port/RS485'#9'S'
          'Ethernet'#9'E'
          'Usb'#9'U')
        ParentCtl3D = False
        ParentFont = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object CbIO: TwwDBComboBox
        Left = 96
        Top = 52
        Width = 125
        Height = 22
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'IO_TYPE'
        DataSource = DsListRec
        DropDownCount = 8
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ItemHeight = 0
        Items.Strings = (
          'M'#225'y v'#224'o'#9'I'
          'M'#225'y ra'#9'O'
          'Kh'#244'ng x'#225'c '#273#7883'nh'#9'X')
        ParentCtl3D = False
        ParentFont = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object EdIP: TDBEditEh
        Left = 96
        Top = 100
        Width = 101
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 61
        ControlLabel.Height = 16
        ControlLabel.Caption = 'IP Address'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'IP4'
        DataSource = DsListRec
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 4
        Visible = True
      end
      object EdSocket: TDBEditEh
        Left = 248
        Top = 100
        Width = 73
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 23
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Port'
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'SOCKET'
        DataSource = DsListRec
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 5
        Visible = True
      end
      object CbPort: TwwDBComboBox
        Left = 248
        Top = 124
        Width = 73
        Height = 22
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'PORT'
        DataSource = DsListRec
        DropDownCount = 8
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ItemHeight = 0
        Items.Strings = (
          'COM1'#9'1'
          'COM2'#9'2'
          'COM3'#9'3'
          'COM4'#9'4')
        ParentCtl3D = False
        ParentFont = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object GroupBox1: TGroupBox
        Left = 140
        Top = 167
        Width = 197
        Height = 123
        Caption = ' Th'#244'ng s'#7889' '
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        object MeInfo: TMemo
          Left = 2
          Top = 19
          Width = 193
          Height = 102
          TabStop = False
          Align = alBottom
          Alignment = taCenter
          BorderStyle = bsNone
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8404992
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          ExplicitLeft = 3
        end
      end
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 339
    Height = 352
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'COM_TYPE;CustomEdit;CbKieuKetNoi;F')
    Selected.Strings = (
      'TENMAY'#9'30'#9'T'#234'n m'#225'y'#9'F'
      'CALC_IO_TYPE'#9'12'#9'Ph'#226'n lo'#7841'i'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsListRec
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnDblClick = CmdConnectExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 14474460
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdRestoreDevData: TAction
      Category = 'THAOTAC'
      Caption = 'Ph'#7909'c h'#7891'i d'#7919' li'#7879'u'
      OnExecute = CmdRestoreDevDataExecute
    end
    object CmdConnect: TAction
      Category = 'THAOTAC'
      Caption = 'K'#7871't n'#7889'i'
      OnExecute = CmdConnectExecute
    end
    object CmdOption: TAction
      Category = 'THAOTAC'
      Caption = 'T'#249'y ch'#7885'n'
      OnExecute = CmdOptionExecute
    end
    object CmdBackupDevData: TAction
      Category = 'THAOTAC'
      Caption = 'L'#432'u d'#7921' ph'#242'ng'
      OnExecute = CmdBackupDevDataExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsListRec
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 196
  end
  object QrListRec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrListRecBeforeInsert
    AfterInsert = QrListRecAfterInsert
    AfterEdit = QrListRecAfterEdit
    BeforePost = QrListRecBeforePost
    BeforeDelete = QrListRecBeforeDelete
    AfterScroll = QrListRecAfterScroll
    OnCalcFields = QrListRecCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_MAYCC'
      'order by Idx')
    Left = 148
    Top = 196
    object QrListRecTENMAY: TWideStringField
      FieldName = 'TENMAY'
      Size = 200
    end
    object QrListRecCOM_TYPE: TStringField
      FieldName = 'COM_TYPE'
      OnChange = QrListRecCOM_TYPEChange
      Size = 1
    end
    object QrListRecBAUD_RATE: TIntegerField
      FieldName = 'BAUD_RATE'
    end
    object QrListRecIP4: TStringField
      FieldName = 'IP4'
      Size = 15
    end
    object QrListRecCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrListRecUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrListRecCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrListRecUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrListRecIO_TYPE: TStringField
      FieldName = 'IO_TYPE'
      Size = 1
    end
    object QrListRecPORT: TIntegerField
      FieldName = 'PORT'
    end
    object QrListRecSOCKET: TIntegerField
      FieldName = 'SOCKET'
    end
    object QrListRecCALC_IO_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALC_IO_TYPE'
      Size = 50
      Calculated = True
    end
    object QrListRecIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsListRec: TDataSource
    DataSet = QrListRec
    Left = 148
    Top = 224
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 148
    Top = 140
  end
  object RestoreFrom: TOpenDialog
    FileName = 'D:\backup.nl'
    Left = 547
    Top = 291
  end
  object SaveTo: TSaveDialog
    DefaultExt = 'nl'
    FileName = 'D:\backup.nl'
    Left = 547
    Top = 263
  end
end
