(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmQTLV;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  wwdblook, AppEvnts, Menus, AdvMenus, wwfltdlg, wwFltDlg2, wwDBGrid2, wwDialog,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbigrd, ToolWin;

type
  TFrmDmQTLV = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsNhom: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    QrNhom: TADOQuery;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PgMain: TPageControl;
    tsBehaviour: TTabSheet;
    tsDep: TTabSheet;
    GrLydo: TwwDBGrid2;
    GrNhom: TwwDBGrid2;
    QrLydo: TADOQuery;
    DsLydo: TDataSource;
    Bevel1: TBevel;
    CbNhom: TwwDBLookupCombo;
    RefNhom: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Filter: TwwFilterDialog2;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrNhomMAQTLV_Nhom: TWideStringField;
    QrNhomTENQTLV_Nhom: TWideStringField;
    QrNhomTENQTLV_Nhom_TA: TWideStringField;
    QrNhomCREATE_BY: TIntegerField;
    QrNhomUPDATE_BY: TIntegerField;
    QrNhomCREATE_DATE: TDateTimeField;
    QrNhomUPDATE_DATE: TDateTimeField;
    QrLydoMAQTLV: TWideStringField;
    QrLydoMaQTLV_Nhom: TWideStringField;
    QrLydoTENQTLV: TWideStringField;
    QrLydoTENQTLV_TA: TWideStringField;
    QrLydoCREATE_BY: TIntegerField;
    QrLydoUPDATE_BY: TIntegerField;
    QrLydoCREATE_DATE: TDateTimeField;
    QrLydoUPDATE_DATE: TDateTimeField;
    QrLydoLK_NHOM: TWideStringField;
    CbType: TwwDBComboBox;
    QrNhomNHOM_DACBIET: TIntegerField;
    CmdReload: TAction;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrNhomBeforePost(DataSet: TDataSet);
    procedure QrNhomBeforeDelete(DataSet: TDataSet);
    procedure QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNhomBeforeInsert(DataSet: TDataSet);
    procedure GrLydoCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure GrNhomCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrLydoBeforePost(DataSet: TDataSet);
    procedure CbNhomNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrNhomAfterPost(DataSet: TDataSet);
  private
  	mCanEdit, mTrigger, fixCodeLd, fixCodeNhom, mRefresh: Boolean;
    mQuery: TADOQuery;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmQTLV: TFrmDmQTLV;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrLydo, 'HR_DM_QTLV');
    AddFields(QrNhom, 'HR_DM_QTLV_NHOM');

    mRefresh := False;
    mTrigger := False;
    mQuery := QrLydo;

    SetDictionary ([QrLydo, QrNhom],
        ['HR_DM_QTLV', 'HR_DM_QTLV_NHOM'],
        [Nil, Nil]);

    SetCustomGrid(
    	['HR_DM_QTLV', 'HR_DM_QTLV_NHOM'], [GrLydo, GrNhom]);

    // Ly do
    fixCodeLd := setCodeLength('HR_DM_QTLV', QrLydo.FieldByName('MaQTLV'));

    // Nhom
    fixCodeNhom := SetCodeLength('HR_DM_QTLV_NHOM', QrNhom.FieldByName('MaQTLV_Nhom'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.FormShow(Sender: TObject);
begin
	GrLydo.ReadOnly := not mCanEdit;
    GrNhom.ReadOnly := not mCanEdit;
	OpenDataSets([QrNhom, QrLydo]);
    ActiveSheet(PgMain, -1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrNhom, QrLydo]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
    n: Integer;
begin
	b := mQuery.State in [dsBrowse];
    n := PgMain.ActivePageIndex;
    CmdNew.Enabled 	  := b and mCanEdit;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdDel.Enabled	  := b and mCanEdit and (not mQuery.IsEmpty);
    CmdSearch.Enabled := b;
    CmdClearFilter.Enabled := b and (Filter.FieldInfo.Count > 0);

    CmdFilter.Enabled := b and (n = 0);
    CmdFilter.Visible := n = 0;
    CmdClearFilter.Visible := n = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrLydoBeforePost(DataSet: TDataSet);
begin
    begin
		if BlankConfirm(QrLydo, ['MaQTLV']) then
    		Abort;

        if fixCodeLd then
            if LengthConfirm(QrLydo, ['MaQTLV']) then
                Abort;

		if BlankConfirm(QrLydo, ['TenQTLV']) then
    		Abort;

		SetNull(QrLydo, ['MaQTLV_Nhom']);
    end;

    SetAudit(DataSet);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrNhomBeforePost(DataSet: TDataSet);
begin
    begin
	    if BlankConfirm(QrNhom, ['MaQTLV_Nhom']) then
    	    Abort;

	    if fixCodeNhom then
    	    if LengthConfirm(QrNhom, ['MaQTLV_Nhom']) then
        	    Abort;

	    if BlankConfirm(QrNhom, ['TenQTLV_Nhom']) then
    	    Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrNhomAfterPost(DataSet: TDataSet);
begin
    mRefresh := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrNhomBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
        exSearch(Name + '_0', DsLydo)
    else
        exSearch(Name + '_1', DsNhom)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.QrNhomBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.GrLydoCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MaQTLV') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.GrNhomCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if (Field.FullName = 'MaQTLV_Nhom') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 0 then
    	Status.SimpleText := exRecordCount(mQuery, Filter)
    else
    	Status.SimpleText := RecordCount(mQuery);    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, 'HR_DM_QTLV', [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := mQuery.State in [dsBrowse];
    if not AllowChange then
        exCompleteConfirm;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.PgMainChange(Sender: TObject);
begin
	case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrLydo;
			GrLydo.SetFocus;
            RefNhom.Requery;

            if mRefresh then
            begin
                exReSyncRecord(mQuery);
                mRefresh := False;
            end;
	    end;
    1:
	    begin
    	    mQuery := QrNhom;
        	GrNhom.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CbNhomNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmQTLV.CmdReloadExecute(Sender: TObject);
begin
    QrLydo.Requery;
    QrNhom.Requery;
    RefNhom.Requery;
end;

end.
