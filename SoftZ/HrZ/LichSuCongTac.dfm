object FrmLichSuCongTac: TFrmLichSuCongTac
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'L'#7883'ch S'#7917' '#272'i C'#244'ng T'#225'c'
  ClientHeight = 573
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    924
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object SpeedButton1: TSpeedButton
    Left = 2
    Top = 3
    Width = 180
    Height = 22
    Cursor = 1
    Anchors = [akTop, akRight]
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 924
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton13: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 924
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 125
    Width = 416
    Height = 427
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'LK_PhuCapLoai;CustomEdit;CbType;F')
    Selected.Strings = (
      'Manv'#9'12'#9'M'#227#9'F'
      'LK_Tennv'#9'30'#9'LK_Tennv'#9'F'
      'GhiChu'#9'20'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnEnter = CmdRefreshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 924
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 924
    inherited Panel1: TPanel
      Width = 924
      ExplicitWidth = 924
      DesignSize = (
        924
        48)
      inherited CbOrg: TfcTreeCombo
        Left = 469
        Width = 442
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitLeft = 469
        ExplicitWidth = 442
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 416
    Top = 125
    Width = 508
    Height = 427
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 4
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 500
      Height = 427
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PaThongTin: TisPanel
        Left = 2
        Top = 2
        Width = 496
        Height = 400
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin '#273'i c'#244'ng t'#225'c'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object gbThongTin: TGroupBox
          Left = 0
          Top = 16
          Width = 496
          Height = 124
          Align = alTop
          Caption = '  Th'#244'ng tin  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object PaNhanVien: TPanel
            Left = 2
            Top = 17
            Width = 492
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            DesignSize = (
              492
              24)
            object CbNhanVien: TDbLookupComboboxEh2
              Left = 134
              Top = 2
              Width = 272
              Height = 22
              ControlLabel.Width = 56
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'Manv'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'Tennv'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 120
                end
                item
                  FieldName = 'ManvQL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsEmp
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Manv'
              ListField = 'Tennv'
              ListSource = DsEmp
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object EdManvQL: TDBEditEh
              Left = 409
              Top = 2
              Width = 77
              Height = 22
              TabStop = False
              Alignment = taLeftJustify
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_ManvQL'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
          end
          object PaInfo: TPanel
            Left = 2
            Top = 41
            Width = 492
            Height = 80
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 2
            DesignSize = (
              492
              80)
            object Label1: TLabel
              Left = 9
              Top = 30
              Width = 121
              Height = 16
              Alignment = taRightJustify
              Caption = 'Th'#7901'i gian c'#244'ng t'#225'c t'#7915
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label3: TLabel
              Left = 354
              Top = 30
              Width = 23
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = #272#7871'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object CbQuocGia: TDbLookupComboboxEh2
              Left = 134
              Top = 50
              Width = 352
              Height = 22
              ControlLabel.Width = 75
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Qu'#7889'c gia '#273#7871'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaQuocGia'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = DataMain.DsQuocGia
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdQuocGia
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA'
              ListField = 'TEN'
              ListSource = DataMain.DsQuocGia
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 3
              Visible = True
            end
            object CbToDate: TwwDBDateTimePicker
              Left = 134
              Top = 26
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayBatDau'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 1
            end
            object EdLyDo: TDBEditEh
              Left = 134
              Top = 2
              Width = 352
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 85
              ControlLabel.Height = 16
              ControlLabel.Caption = 'N'#7897'i dung/L'#253' do'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NoiDung'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              EmptyDataInfo.Color = clInfoBk
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 0
              Visible = True
            end
            object wwDBDateTimePicker1: TwwDBDateTimePicker
              Left = 385
              Top = 26
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayKetThuc'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 2
            end
          end
          object Panel1: TPanel
            Left = 2
            Top = 15
            Width = 492
            Height = 2
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 292
          Width = 496
          Height = 100
          Align = alTop
          Caption = '  V'#7855'ng m'#7863't  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          DesignSize = (
            496
            100)
          object Label5: TLabel
            Left = 58
            Top = 48
            Width = 72
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ngh'#7881' t'#7915' ng'#224'y'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 327
            Top = 48
            Width = 54
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = #272#7871'n ng'#224'y'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object wwDBDateTimePicker2: TwwDBDateTimePicker
            Left = 134
            Top = 44
            Width = 101
            Height = 22
            TabStop = False
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = clBtnFace
            DataField = 'NgayBatDauVangMat'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = True
            TabOrder = 1
          end
          object wwDBDateTimePicker3: TwwDBDateTimePicker
            Left = 387
            Top = 44
            Width = 101
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = clBtnFace
            DataField = 'NgayKetThucVangMat'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = True
            TabOrder = 2
          end
          object DBNumberEditEh1: TDBNumberEditEh
            Left = 134
            Top = 68
            Width = 101
            Height = 22
            TabStop = False
            ControlLabel.Width = 74
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' ng'#224'y ngh'#7881
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoNgayVangMat'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            Visible = True
          end
          object EdMaThietBi: TDBEditEh
            Left = 134
            Top = 20
            Width = 101
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Caption = 'M'#7903' danh s'#225'ch c'#7845'p/tr'#7843' t'#224'i s'#7843'n'
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DynProps = <>
            EditButtons = <
              item
                Action = CmDangKyVangMat
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            EmptyDataInfo.Text = #272#259'ng k'#253' ngh'#7881
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clWindowText
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = []
            EmptyDataInfo.ParentFont = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 140
          Width = 496
          Height = 100
          Align = alTop
          Caption = '  Ph'#7909' c'#7845'p  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          DesignSize = (
            496
            100)
          object CbLoaiPhuCap: TDbLookupComboboxEh2
            Left = 134
            Top = 20
            Width = 226
            Height = 22
            ControlLabel.Width = 45
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ph'#7909' c'#7845'p'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akLeft, akTop, akRight]
            DynProps = <>
            DataField = 'MaPhuCap'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'TenPhuCap'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 200
              end>
            DropDownBox.ListSource = HrDataMain.DsDM_PHUCAP
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <
              item
                Action = CmdLoaiPhuCap
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MaPhuCap'
            ListField = 'TenPhuCap'
            ListSource = HrDataMain.DsDM_PHUCAP
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdPhuCapLoai: TDBEditEh
            Left = 363
            Top = 20
            Width = 125
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Width = 3
            ControlLabel.Height = 13
            ControlLabel.Caption = ' '
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_TenPhuCapLoai'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object EdNumSoTienNgoaiTe: TDBNumberEditEh
            Left = 134
            Top = 44
            Width = 101
            Height = 22
            ControlLabel.Width = 90
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' ti'#7873'n ngo'#7841'i t'#7879
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'NgoaiTe_SoTien'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object EdNumSoTien: TDBNumberEditEh
            Left = 134
            Top = 68
            Width = 101
            Height = 22
            ControlLabel.Width = 69
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' ti'#7873'n VN'#272
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'PhuCapCongTac'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 4
            Visible = True
          end
          object CbNgoaiTe: TDbLookupComboboxEh2
            Left = 363
            Top = 44
            Width = 125
            Height = 22
            ControlLabel.Width = 47
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ngo'#7841'i t'#7879
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akTop, akRight]
            DynProps = <>
            DataField = 'NgoaiTe_Loai'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 200
              end>
            DropDownBox.ListSource = DataMain.DsNGOAITE
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <
              item
                Action = CmdNgoaiTe
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = DataMain.DsNGOAITE
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 3
            Visible = True
          end
          object EdNumTyGia: TDBNumberEditEh
            Left = 387
            Top = 68
            Width = 101
            Height = 22
            ControlLabel.Width = 35
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#7927' gi'#225
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'NgoaiTe_TyGia'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 5
            Visible = True
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 240
          Width = 496
          Height = 52
          Align = alTop
          Caption = '  B'#7843'ng l'#432#417'ng/ Thu'#7871' TNCN  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          DesignSize = (
            496
            52)
          object Label2: TLabel
            Left = 21
            Top = 22
            Width = 107
            Height = 16
            Alignment = taRightJustify
            Caption = 'Thu'#7897'c th'#225'ng l'#432#417'ng'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object rDBCheckBox1: TrDBCheckBox
            Left = 364
            Top = 20
            Width = 124
            Height = 22
            Anchors = [akTop, akRight]
            Caption = 'C'#243' ch'#7883'u thu'#7871' TNCN'
            DataField = 'Co_ThueTNCN'
            DataSource = DsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            ShowFieldCaption = False
            UpdateAfterClick = True
          end
          object CbMon: TwwDBComboBox
            Left = 134
            Top = 20
            Width = 43
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Thang'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object CbYear: TwwDBComboBox
            Left = 180
            Top = 20
            Width = 55
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Nam'
            DataSource = DsMaster
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
      end
      object PaGhiChu: TisPanel
        Left = 2
        Top = 402
        Width = 496
        Height = 23
        Align = alClient
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 492
          Height = 3
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
        end
      end
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 84
    Width = 924
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 84
    ExplicitWidth = 924
    inherited Panel1: TPanel
      Width = 924
      ExplicitWidth = 924
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
      OnExecute = CmdPrint2Execute
    end
    object CmdLoaiPhuCap: TAction
      OnExecute = CmdLoaiPhuCapExecute
    end
    object CmdNgoaiTe: TAction
      OnExecute = CmdNgoaiTeExecute
    end
    object CmdQuocGia: TAction
      OnExecute = CmdQuocGiaExecute
    end
    object CmDangKyVangMat: TAction
      Caption = #272#259'ng k'#253' ngh'#7881
      OnExecute = CmDangKyVangMatExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL,a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.M' +
        'aBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_CONGTAC'
      ' where'#9'1=1')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
    end
    object QrMasterLK_TENNV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'MANV'
      LookupResultField = 'TENNV'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterNoiDung: TWideStringField
      FieldName = 'NoiDung'
      Size = 200
    end
    object QrMasterMaPhuCap: TWideStringField
      FieldName = 'MaPhuCap'
      OnChange = QrMasterMaPhuCapChange
    end
    object QrMasterPhuCapCongTac: TFloatField
      FieldName = 'PhuCapCongTac'
      OnChange = QrMasterNgoaiTe_SoTienChange
    end
    object QrMasterNgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
    end
    object QrMasterNgayKetThuc: TDateTimeField
      FieldName = 'NgayKetThuc'
    end
    object QrMasterLK_TenPhuCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhuCap'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'TenPhuCap'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrMasterLK_PhuCapLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PhuCapLoai'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'PhuCapLoai'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrMasterLK_TenPhuCapLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhuCapLoai'
      LookupDataSet = HrDataMain.QrV_HR_PHUCAP_LOAI
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LK_PhuCapLoai'
      Lookup = True
    end
    object QrMasterThang: TIntegerField
      FieldName = 'Thang'
    end
    object QrMasterNam: TIntegerField
      FieldName = 'Nam'
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterFileName: TWideStringField
      FieldName = 'FileName'
      Size = 200
    end
    object QrMasterFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrMasterMaQuocGia: TWideStringField
      FieldName = 'MaQuocGia'
      OnChange = QrMasterMaQuocGiaChange
      Size = 5
    end
    object QrMasterCo_TrongNuoc: TBooleanField
      FieldName = 'Co_TrongNuoc'
    end
    object QrMasterNgoaiTe_Loai: TWideStringField
      FieldName = 'NgoaiTe_Loai'
      Size = 70
    end
    object QrMasterNgoaiTe_TyGia: TFloatField
      FieldName = 'NgoaiTe_TyGia'
      OnChange = QrMasterNgoaiTe_SoTienChange
    end
    object QrMasterNgoaiTe_SoTien: TFloatField
      FieldName = 'NgoaiTe_SoTien'
      OnChange = QrMasterNgoaiTe_SoTienChange
    end
    object QrMasterLK_Co_TrongNuoc: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_TrongNuoc'
      LookupDataSet = DataMain.QrQuocgia
      LookupKeyFields = 'MA'
      LookupResultField = 'Co_TrongNuoc'
      KeyFields = 'MaQuocGia'
      Lookup = True
    end
    object QrMasterLK_TenQuocGia: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuocGia'
      LookupDataSet = DataMain.QrQuocgia
      LookupKeyFields = 'MA'
      LookupResultField = 'TEN'
      KeyFields = 'MaQuocGia'
      Size = 200
      Lookup = True
    end
    object QrMasterLK_TenNgoaiTe_Loai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNgoaiTe_Loai'
      LookupDataSet = DataMain.QrNGOAITE
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'NgoaiTe_Loai'
      Size = 200
      Lookup = True
    end
    object QrMasterNgayBatDauVangMat: TDateTimeField
      FieldName = 'NgayBatDauVangMat'
    end
    object QrMasterNgayKetThucVangMat: TDateTimeField
      FieldName = 'NgayKetThucVangMat'
    end
    object QrMasterSoNgayVangMat: TFloatField
      FieldName = 'SoNgayVangMat'
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
    object QrMasterFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 50
    Top = 200
  end
  object spHR_LICHSU_CONGTAC_Invalid_Ngay: TADOCommand
    CommandText = 'spHR_LICHSU_CONGTAC_Invalid_Ngay;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@STR'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@IDX'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 212
    Top = 212
  end
end
