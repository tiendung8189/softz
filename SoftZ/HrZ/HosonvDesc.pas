﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HosonvDesc;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Vcl.Buttons, Vcl.DBCtrls;

type
  TFrmHosonvDesc = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    EdGhichu: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Execute(var s: String): Boolean;
  end;

var
  FrmHosonvDesc: TFrmHosonvDesc;

implementation

{$R *.DFM}

uses
    isLib, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvDesc.CmdReturnClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

function TFrmHosonvDesc.Execute;
begin
    EdGhichu.Text := s;
    Result := ShowModal = mrOk;
    if Result then
        s := EdGhichu.Text;
    Free
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvDesc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvDesc.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    EdGhichu.Focused;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
