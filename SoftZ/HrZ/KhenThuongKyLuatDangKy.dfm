object FrmKhenThuongKyLuatDangKy: TFrmKhenThuongKyLuatDangKy
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = #272#259'ng K'#253' Khen Th'#432#7903'ng - K'#7927' Lu'#7853't'
  ClientHeight = 584
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 171
    Align = alTop
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 402
      Height = 169
      Align = alLeft
      BevelOuter = bvNone
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 0
      object RgLoai: TRadioGroup
        Left = 8
        Top = 4
        Width = 385
        Height = 47
        Columns = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 2
        Items.Strings = (
          'Theo ph'#242'ng ban'
          'Theo b'#7897' ph'#7853'n'
          'Theo nh'#226'n vi'#234'n')
        ParentFont = False
        TabOrder = 0
        OnClick = RgLoaiClick
      end
      object Panel2: TPanel
        Left = 8
        Top = 54
        Width = 385
        Height = 105
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object CbPhongBan: TDbLookupComboboxEh2
          Left = 69
          Top = 12
          Width = 222
          Height = 24
          ControlLabel.Width = 60
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ph'#242'ng ban'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Alignment = taLeftJustify
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'TenPhongBan'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'Ma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = DsDep
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MaPhongBan'
          ListField = 'TenPhongBan'
          ListSource = DsDep
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 0
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object CbBoPhan: TDbLookupComboboxEh2
          Tag = 1
          Left = 69
          Top = 40
          Width = 222
          Height = 24
          ControlLabel.Width = 46
          ControlLabel.Height = 16
          ControlLabel.Caption = 'B'#7897' ph'#7853'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'TenBoPhan'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'Ma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = DsSec
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MaBoPhan'
          ListField = 'TenBoPhan'
          ListSource = HrDataMain.DsDMBOPHAN
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object EdMaPhongBan: TDBEditEh
          Left = 295
          Top = 12
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object EdMaBoPhan: TDBEditEh
          Left = 295
          Top = 40
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object CbNhanVien: TDbLookupComboboxEh2
          Tag = 2
          Left = 69
          Top = 68
          Width = 222
          Height = 24
          ControlLabel.Width = 56
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DropDownBox.Columns = <
            item
              FieldName = 'Tennv'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 232
            end
            item
              FieldName = 'ManvQL'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 72
            end>
          DropDownBox.ListSource = DsEmp
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 304
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'ManvQL'
          ListField = 'Tennv'
          ListSource = DsEmp
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
          OnChange = CbPhongBanExit
          OnExit = CbPhongBanExit
        end
        object EdMaNhanVien: TDBEditEh
          Left = 295
          Top = 68
          Width = 77
          Height = 24
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
      end
    end
    object Panel5: TPanel
      Left = 403
      Top = 1
      Width = 62
      Height = 169
      Align = alLeft
      BevelOuter = bvNone
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object BitBtn1: TBitBtn
        Left = 0
        Top = 53
        Width = 53
        Height = 25
        Cursor = 1
        Action = CmdIns
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C30E0000C30E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874991D987494D0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AF9D885
          4856000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AFEDA87
          4AFBDA874A600000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DA874AA9DA874AFEDA87
          4AFEDA874AFCDA8749690000000000000000000000000000000000000000DA86
          4965DA8649A9DA874AA9DA874AA9DA874AA9DA874AA9DA874AE1DA874AFEDA87
          4AFEDA874AFEDA874AFDDA87497400000000000000000000000000000000D986
          49A9DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFDDA874A7F000000000000000000000000DA87
          4AA9DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFEDA8749850000000000000000E999
          5BA9EA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFDE9985A7D0000000000000000E999
          5BA9EA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5BFDE9995B73000000000000000000000000E999
          5B63EA9A5BA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5CE1EA9A5CFEEA9A
          5CFEEA9A5CFEE9995BFBE9995A66000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5CA9EA9A5CFEEA9A
          5CFEE9995BF9E8985A5B00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5CA9EA9A5CFEE999
          5BF7E6975A4F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000EA9A5BA9E9995BF3E395
          5845000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000E9995B84E093573B0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 0
      end
      object BitBtn3: TBitBtn
        Left = 0
        Top = 90
        Width = 53
        Height = 25
        Cursor = 1
        Action = CmdDel
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C30E0000C30E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000D786494DDA8749910000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000D7854756DA874AF9DA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000D9874A60DA874AFBDA874AFEDA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000DA874969DA874AFCDA874AFEDA874AFEDA874AA90000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000DA874974DA874AFDDA874AFEDA874AFEDA874AFEDA874AE1DA87
          4AA9DA874AA9DA874AA9DA874AA9DA8649A9D986496500000000000000000000
          0000DA874A7FDA874AFDDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFED98649A90000000000000000DA87
          4985DA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AFEDA87
          4AFEDA874AFEDA874AFEDA874AFEDA874AFEDA874AA90000000000000000E998
          5A7DEA9A5BFDEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEE9995BA900000000000000000000
          0000E8995B73EA9A5BFDEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A
          5CFEEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CFEE9995BA900000000000000000000
          000000000000E8985A66E9995BFBEA9A5CFEEA9A5CFEEA9A5CFEEA9A5CE1EA9A
          5CA9EA9A5CA9EA9A5CA9EA9A5CA9EA9A5BA9E9995B6300000000000000000000
          00000000000000000000E697595BE9995BF9EA9A5CFEEA9A5CFEEA9A5CA90000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000E395584FE9995BF7EA9A5CFEEA9A5CA90000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000DE925745E9995BF3EA9A5BA90000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DC91553BE9995B840000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 1
      end
    end
    object Panel6: TPanel
      Left = 465
      Top = 1
      Width = 434
      Height = 169
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object GrList: TStringGrid
        Left = 0
        Top = 0
        Width = 434
        Height = 159
        Hint = 'X'#243'a danh s'#225'ch'
        Align = alTop
        BevelOuter = bvNone
        ColCount = 2
        Ctl3D = False
        DefaultColWidth = 76
        DefaultRowHeight = 19
        FixedCols = 0
        RowCount = 100
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        ParentCtl3D = False
        PopupMenu = PopList
        ScrollBars = ssVertical
        TabOrder = 0
        OnDblClick = CmdDelExecute
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 543
    Width = 900
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 365
    DesignSize = (
      900
      41)
    object BtnContinute: TBitBtn
      Left = 326
      Top = 4
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdRegister
      Anchors = [akRight, akBottom]
      Caption = 'Th'#7921'c hi'#7879'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
        DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
        21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
        4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
        AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
        21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
        4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
        FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
        1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
        6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
        B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
        49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
        3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
        E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
        62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
        FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
        E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
        7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
        E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
        E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
        F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
        45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
        8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 462
      Top = 4
      Width = 96
      Height = 32
      Cursor = 1
      Hint = 'K'#7871't th'#250'c'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'K'#7871't th'#250'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
        00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
        78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
        F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
        A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
        7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
        16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
        C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
        7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
        210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
        B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
        82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
        6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
        C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
        85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
        FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
        CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
        88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
        240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
        DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
        78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
        FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
        B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      OnClick = CmdCloseExecute
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 171
    Width = 900
    Height = 194
    Align = alTop
    Caption = 'Panel7'
    TabOrder = 2
    ExplicitLeft = 104
    ExplicitTop = 184
    ExplicitWidth = 185
    object PaEmp: TPanel
      Left = 1
      Top = 1
      Width = 505
      Height = 192
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 365
      ExplicitHeight = 0
      object PaThongTin: TisPanel
        Left = 0
        Top = 0
        Width = 505
        Height = 192
        HelpType = htKeyword
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin Khen th'#432#7903'ng - K'#7927' lu'#7853't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitHeight = 0
        object gbViecLam: TGroupBox
          Left = 0
          Top = 16
          Width = 505
          Height = 116
          Align = alTop
          Caption = '  Th'#244'ng tin  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object PaInfo: TPanel
            Left = 2
            Top = 15
            Width = 501
            Height = 106
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            DesignSize = (
              501
              106)
            object Label2: TLabel
              Left = 39
              Top = 4
              Width = 77
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y hi'#7879'u l'#7921'c'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label108: TLabel
              Left = 72
              Top = 28
              Width = 44
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y k'#253
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object dpNgayHieuLuc: TwwDBDateTimePicker
              Left = 122
              Top = 0
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayHieuLuc'
              DataSource = DsDummy
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ShowButton = True
              TabOrder = 0
            end
            object wwDBEdit5: TDBEditEh
              Left = 329
              Top = 0
              Width = 165
              Height = 22
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 78
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' quy'#7871't '#273#7883'nh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'SoQuyetDinh'
              DataSource = DsDummy
              DynProps = <>
              EditButtons = <>
              EmptyDataInfo.Color = clInfoBk
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object CbDate: TwwDBDateTimePicker
              Left = 122
              Top = 24
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayKy'
              DataSource = DsDummy
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 2
            end
            object DBNumberEditEh1: TDBNumberEditEh
              Left = 393
              Top = 24
              Width = 101
              Height = 22
              ControlLabel.Width = 40
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' ti'#7873'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'SoTien'
              DataSource = DsDummy
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object EdNguoiKy_Manv: TDBEditEh
              Left = 122
              Top = 48
              Width = 372
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              AutoSelect = False
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 49
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i k'#253
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiKy'
              DataSource = DsDummy
              DynProps = <>
              EditButtons = <>
              EmptyDataInfo.Color = clInfoBk
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object CbLyDo: TDbLookupComboboxEh2
              Left = 122
              Top = 72
              Width = 268
              Height = 22
              ControlLabel.Width = 30
              ControlLabel.Height = 16
              ControlLabel.Caption = 'L'#253' do'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaKThuongKLuat'
              DataSource = DsDummy
              DropDownBox.Columns = <
                item
                  FieldName = 'TenKThuongKLuat'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsLYDO_KTKL
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdLyDoKThuongKLuat
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaKThuongKLuat'
              ListField = 'TenKThuongKLuat'
              ListSource = HrDataMain.DsLYDO_KTKL
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 5
              Visible = True
            end
            object EdKTKLLoai: TDBEditEh
              Left = 393
              Top = 72
              Width = 101
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabel.Width = 3
              ControlLabel.Height = 13
              ControlLabel.Caption = ' '
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_TenLoaiKThuongKLuat'
              DataSource = DsDummy
              DynProps = <>
              EditButtons = <>
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 6
              Visible = True
            end
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 132
          Width = 505
          Height = 48
          Align = alTop
          Caption = '  B'#7843'ng l'#432#417'ng/ Thu'#7871' TNCN  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          DesignSize = (
            505
            48)
          object Label1: TLabel
            Left = 11
            Top = 19
            Width = 107
            Height = 16
            Alignment = taRightJustify
            Caption = 'Thu'#7897'c th'#225'ng l'#432#417'ng'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object rDBCheckBox1: TrDBCheckBox
            Left = 372
            Top = 17
            Width = 124
            Height = 22
            Anchors = [akTop, akRight]
            Caption = 'C'#243' ch'#7883'u thu'#7871' TNCN'
            DataField = 'Co_ThueTNCN'
            DataSource = DsDummy
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            ShowFieldCaption = False
            UpdateAfterClick = True
          end
          object CbMon: TwwDBComboBox
            Left = 124
            Top = 17
            Width = 43
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Thang'
            DataSource = DsDummy
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object CbYear: TwwDBComboBox
            Left = 170
            Top = 17
            Width = 55
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'Nam'
            DataSource = DsDummy
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
      end
    end
    object PaThongTinThem: TPanel
      Left = 506
      Top = 1
      Width = 393
      Height = 192
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 0
      ExplicitTop = 365
      ExplicitWidth = 900
      ExplicitHeight = 0
      object PaDinhKem: TisPanel
        Left = 0
        Top = 0
        Width = 393
        Height = 54
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' .: File '#273#237'nh k'#232'm'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitWidth = 900
        DesignSize = (
          393
          54)
        object EdFileAttach: TDBEditEh
          Tag = 1
          Left = 124
          Top = 25
          Width = 260
          Height = 22
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Calc_FileName'
          DataSource = DsDummy
          DynProps = <>
          EditButtons = <
            item
              Action = CmdFilePlus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileMinus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 1
              Images.HotIndex = 1
              Images.PressedIndex = 1
              Images.DisabledIndex = 1
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileView
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 2
              Images.HotIndex = 2
              Images.PressedIndex = 2
              Images.DisabledIndex = 2
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
          ExplicitWidth = 767
        end
      end
      object PaGhiChu: TisPanel
        Left = 0
        Top = 54
        Width = 385
        Height = 125
        Align = alCustom
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 381
          Height = 105
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsDummy
          TabOrder = 1
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 365
    Width = 900
    Height = 41
    Align = alTop
    TabOrder = 3
    ExplicitLeft = 1
    ExplicitTop = 1
    ExplicitWidth = 898
    DesignSize = (
      900
      41)
    object BitBtn4: TBitBtn
      Left = 404
      Top = 3
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdContinue
      Anchors = [akRight, akBottom]
      Caption = 'Ti'#7871'p t'#7909'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
        DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
        21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
        4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
        AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
        21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
        4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
        FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
        1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
        6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
        B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
        49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
        3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
        E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
        62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
        FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
        E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
        7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
        E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
        E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
        F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
        45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
        8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 406
    Width = 900
    Height = 137
    Align = alTop
    Caption = 'Panel9'
    TabOrder = 4
    object wwDBGrid21: TwwDBGrid2
      Left = 1
      Top = 1
      Width = 898
      Height = 135
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'LK_TenLoaiKThuongKLuat'#9'15'#9'Lo'#7841'i h'#236'nh'#9'T'
        'Sheet'#9'200'#9'Sheet'#9'F'
        'ErrCode'#9'20'#9'ErrCode'#9'T')
      MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 2
      TitleButtons = True
      UseTFields = False
      ImageList = DataMain.ImageMark
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 576
    Top = 136
    object CmdIns: TAction
      Hint = 'Th'#234'm v'#224'o danh s'#225'ch'
      ShortCut = 16429
      OnExecute = CmdInsExecute
    end
    object CmdDel: TAction
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
    end
    object CmdImport: TAction
      Caption = 'D'#225'n danh s'#225'ch nh'#226'n vi'#234'n t'#7915' file'
      OnExecute = CmdImportExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Caption = ' '
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
    object CmdContinue: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      OnExecute = CmdContinueExecute
    end
    object CmdLyDoKThuongKLuat: TAction
      OnExecute = CmdLyDoKThuongKLuatExecute
    end
    object CmdUpdate: TAction
      Caption = 'C'#7853'p nh'#7853't'
    end
    object CmdRegister: TAction
      Caption = 'Th'#7921'c hi'#7879'n'
      OnExecute = CmdRegisterExecute
    end
    object CmdDelRecordError: TAction
      Caption = 'X'#243'a c'#225'c d'#242'ng '#273'ang l'#7895'i'
      OnExecute = CmdDelRecordErrorExecute
    end
    object CmdDelRecordSelected: TAction
      Caption = 'X'#243'a d'#242'ng '#273'ang ch'#7885'n'
      OnExecute = CmdDelRecordSelectedExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
    object CmdImportExcel: TAction
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'order by a.Manv')
    Left = 744
    Top = 20
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'FileIdx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :FileIdx')
    Left = 508
    Top = 132
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 746
    Top = 64
  end
  object PopList: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 812
    Top = 108
    object ImporttExcel1: TMenuItem
      Action = CmdImport
    end
    object N1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrSite: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MaChiNhanh, TenChiNhanh'
      '  from HR_DM_CHINHANH'
      'order by MaChiNhanh')
    Left = 772
    Top = 16
  end
  object QrDep: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsSite
    Parameters = <
      item
        Name = 'MaChiNhanh'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'Ma, MaPhongBan, TenPhongBan, MaChiNhanh'
      '  from'#9'HR_DM_PHONGBAN'
      ' where  MaChiNhanh = :MaChiNhanh'
      'order by MaPhongBan')
    Left = 804
    Top = 16
  end
  object QrSec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsDep
    Parameters = <
      item
        Name = 'MaPhongBan'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'Ma, MaBoPhan, TenBoPhan, MaPhongBan'
      '  from'#9'HR_DM_BOPHAN'
      'where    MaPhongBan = :MaPhongBan'
      'order by MaBoPhan')
    Left = 840
    Top = 16
  end
  object DsSec: TDataSource
    DataSet = QrSec
    Left = 840
    Top = 56
  end
  object DsDep: TDataSource
    DataSet = QrDep
    Left = 808
    Top = 56
  end
  object DsSite: TDataSource
    DataSet = QrSite
    Left = 776
    Top = 56
  end
  object spIMP_HR_LICHSU_KTKL_ImportList: TADOCommand
    CommandText = 'spIMP_HR_LICHSU_KTKL_ImportList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pLoai'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pChuoi'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 2000
        Value = Null
      end
      item
        Name = '@pNgayHieuLuc'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pSoQuyetDinh'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pNgayKy'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pNguoiKy'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pMaKThuongKLuat'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@pSoTien'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pCo_ThueTNCN'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@pFileIdx'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end
      item
        Name = '@pFileName'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pFileExt'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pGhiChu'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pLang'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 716
    Top = 108
  end
  object TbDummy: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'NgayHieuLuc'
        DataType = ftDateTime
      end
      item
        Name = 'SoQuyetDinh'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'NgayKy'
        DataType = ftDateTime
      end
      item
        Name = 'SoTien'
        DataType = ftFloat
      end
      item
        Name = 'NguoiKy'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'MaKThuongKLuat'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'Thang'
        DataType = ftInteger
      end
      item
        Name = 'Nam'
        DataType = ftInteger
      end
      item
        Name = 'Co_ThueTNCN'
        DataType = ftBoolean
      end
      item
        Name = 'FileIdx'
        DataType = ftGuid
        Size = 38
      end
      item
        Name = 'FileName'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'FileExt'
        DataType = ftWideString
        Size = 20
      end
      item
        Name = 'GhiChu'
        DataType = ftWideMemo
      end
      item
        Name = 'Manv'
        DataType = ftWideString
        Size = 20
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    AfterInsert = TbDummyAfterInsert
    OnCalcFields = TbDummyCalcFields
    Left = 664
    Top = 24
    object TbDummyNgayHieuLuc: TDateTimeField
      DisplayLabel = 'Ng'#224'y hi'#7879'u l'#7921'c'
      FieldName = 'NgayHieuLuc'
    end
    object TbDummySoQuyetDinh: TWideStringField
      DisplayLabel = 'S'#7889' quy'#7871't '#273#7883'nh'
      FieldName = 'SoQuyetDinh'
    end
    object TbDummyNgayKy: TDateTimeField
      DisplayLabel = 'Ng'#224'y k'#253
      FieldName = 'NgayKy'
    end
    object TbDummySoTien: TFloatField
      DisplayLabel = 'S'#7889' ti'#7873'n'
      FieldName = 'SoTien'
    end
    object TbDummyNguoiKy: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i k'#253
      FieldName = 'NguoiKy'
    end
    object TbDummyMaKThuongKLuat: TWideStringField
      DisplayLabel = 'L'#253' do Khen th'#432#7903'ng - K'#7927' lu'#7853't'
      FieldName = 'MaKThuongKLuat'
    end
    object TbDummyThang: TIntegerField
      DisplayLabel = 'Th'#225'ng'
      FieldName = 'Thang'
    end
    object TbDummyNam: TIntegerField
      DisplayLabel = 'N'#259'm'
      FieldName = 'Nam'
    end
    object TbDummyCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object TbDummyCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Calculated = True
    end
    object TbDummyFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
    object TbDummyFileName: TWideStringField
      FieldName = 'FileName'
    end
    object TbDummyFileExt: TWideStringField
      FieldName = 'FileExt'
    end
    object TbDummyGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object TbDummyManv: TWideStringField
      FieldName = 'Manv'
    end
    object TbDummyLK_KThuongKLuatLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_KThuongKLuatLoai'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'KThuongKLuatLoai'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object TbDummyLK_TenLoaiKThuongKLuat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenLoaiKThuongKLuat'
      LookupDataSet = HrDataMain.QrLOAI_LYDO_KTKL
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LK_KThuongKLuatLoai'
      Lookup = True
    end
  end
  object DsDummy: TDataSource
    DataSet = TbDummy
    Left = 663
    Top = 65
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDummy
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Manv'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 636
    Top = 88
  end
  object spIMP_HR_LICHSU_KTKL_InsertData: TADOCommand
    CommandText = 'spIMP_HR_LICHSU_KTKL_InsertData;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 812
    Top = 452
  end
  object spIMP_HR_LICHSU_KTKL_DeleteList: TADOCommand
    CommandText = 'spIMP_HR_LICHSU_KTKL_DeleteList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIsError'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 812
    Top = 492
  end
  object spIMP_HR_LICHSU_KTKL_Check: TADOCommand
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 660
    Top = 444
  end
  object CHECK_RECORD_ERROR: TADOCommand
    CommandText = 
      'select 1 from IMP_HR_LICHSU_KTKL where TransNo=:TransNo and isnu' +
      'll(ErrCode, '#39#39') <> '#39#39
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 672
    Top = 492
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 444
    Top = 492
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrListBeforeOpen
    BeforeDelete = QrListBeforeDelete
    AfterDelete = QrListAfterDelete
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'IMP_HR_LICHSU_KTKL'
      'where TransNo=:TransNo')
    Left = 432
    Top = 436
    object QrListLK_TenLoaiKThuongKLuat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenLoaiKThuongKLuat'
      LookupDataSet = HrDataMain.QrLOAI_LYDO_KTKL
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'KThuongKLuatLoai'
      Lookup = True
    end
    object QrListLK_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrListLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrListLK_TenKThuongKLuat: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenKThuongKLuat'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'TenKThuongKLuat'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrListLK_Co_ThueTNCN: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_ThueTNCN'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'Co_ThueTNCN'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrListLK_KThuongKLuatLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_KThuongKLuatLoai'
      LookupDataSet = HrDataMain.QrLYDO_KTKL
      LookupKeyFields = 'MaKThuongKLuat'
      LookupResultField = 'KThuongKLuatLoai'
      KeyFields = 'MaKThuongKLuat'
      Lookup = True
    end
    object QrListSheet: TWideStringField
      FieldName = 'Sheet'
      Size = 200
    end
    object QrListErrCode: TWideStringField
      FieldName = 'ErrCode'
      Size = 200
    end
    object QrListKThuongKLuatLoai: TIntegerField
      FieldName = 'KThuongKLuatLoai'
    end
    object QrListManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrListMaKThuongKLuat: TWideStringField
      FieldName = 'MaKThuongKLuat'
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 516
    Top = 456
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Xadng1: TMenuItem
      Action = CmdDelRecordSelected
    end
    object Xaccdngangli1: TMenuItem
      Action = CmdDelRecordError
    end
  end
end
