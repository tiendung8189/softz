(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvQuanHeGiaDinh;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin, Vcl.StdCtrls, wwdblook,
  frameEmp, Vcl.Mask, wwdbedit, Wwdotdot, Wwdbcomb, RzEdit, RzDBEdit, RzDBBnEd,
  DBCtrlsEh, wwdbdatetimepicker, Vcl.Buttons, Vcl.DBCtrls, isPanel,
  Vcl.ExtCtrls, RzPanel, RzSplit, rDBComponents;

type
  TFrmHoSonvQuanHeGiaDinh = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    QrDMManv: TWideStringField;
    QrDMHoTen: TWideStringField;
    QrDMQuanHe: TWideStringField;
    QrDMNgaySinh: TDateTimeField;
    QrDMCo_PhuThuoc: TBooleanField;
    QrDMNgayBatDau: TDateTimeField;
    QrDMNgayKetThuc: TDateTimeField;
    QrDMHonNhan: TWideStringField;
    QrDMCCCD: TWideStringField;
    QrDMMaSoThue: TWideStringField;
    QrDMQuocTich: TWideStringField;
    QrDMDienThoai: TWideStringField;
    QrDMDiaChi: TWideStringField;
    QrDMLK_TenQuocTich: TWideStringField;
    QrDMLK_TenQuanHe: TWideStringField;
    CbQuocTich: TwwDBLookupCombo;
    CbQuanHe: TwwDBLookupCombo;
    frEmp1: TfrEmp;
    QrDMSoThangBatDau: TFloatField;
    QrDMSoThangKetThuc: TFloatField;
    QrDMThangBatDau: TIntegerField;
    QrDMNamBatDau: TIntegerField;
    QrDMThangKetThuc: TIntegerField;
    QrDMNamKetThuc: TIntegerField;
    QrDMGhiChu: TWideMemoField;
    RzSizePanel1: TRzSizePanel;
    PaEmp: TPanel;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrDMUPDATE_NAME: TWideStringField;
    PaThongTin: TisPanel;
    Label1: TLabel;
    Label6: TLabel;
    CbToDate: TwwDBDateTimePicker;
    EdHoTen: TDBEditEh;
    wwDBEdit2: TDBEditEh;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    DBNumberEditEh1: TDBNumberEditEh;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBNumberEditEh2: TDBNumberEditEh;
    wwDBLookupCombo1: TwwDBLookupCombo;
    DBEditEh1: TDBEditEh;
    GroupBox1: TGroupBox;
    EdCMND: TDBEditEh;
    DBEditEh5: TDBEditEh;
    rDBCheckBox1: TrDBCheckBox;
    DBNumberEditEh4: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    CbMon: TwwDBComboBox;
    Label2: TLabel;
    Label3: TLabel;
    CbMonEnd: TwwDBComboBox;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    QrDMLK_CREATE_FULLNAME: TWideStringField;
    QrDMLK_UPDATE_FULLNAME: TWideStringField;
    QrDMSoKhaiSinh: TWideStringField;
    EdSoKhaiSinh: TDBEditEh;
    QrDMIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDMAfterInsert(DataSet: TDataSet);
    procedure QrDMNgaySinhChange(Sender: TField);
    procedure QrDMNgayKetThucChange(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
  private
  	mCanEdit: Boolean;
    mManv: string;
  public
  	procedure Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String);
  end;

var
  FrmHoSonvQuanHeGiaDinh: TFrmHoSonvQuanHeGiaDinh;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    HoSonv, HrData;

{$R *.DFM}

const
    TABLE_NAME = 'HR_LICHSU_GIADINH';
    FORM_CODE = 'HR_LICHSU_NHANVIEN_GIADINH';
    REPORT_NAME = 'HR_RP_LICHSU_NHANVIEN_GIADINH';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := EmpID;
    frEmp1.Initial(EmpID, EmpIDLabel, EmpName);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    AddFields(QrDM, TABLE_NAME);

    initMonthList(CbMon, 0);
    initMonthList(CbMonEnd, 0);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.FormShow(Sender: TObject);
begin
    with HrDataMain do
        OpenDataSets([QrV_QUOCTICH, QrV_HR_QUANHE]);

    SetDisplayFormat(QrDM, sysCurFmt);
    SetShortDateFormat(QrDM);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDM, FORM_CODE, Filter);

    QrDM.Open;
    SortDataSet(QrDM, 'NgayBatDau');
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
    EdHoTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdEditExecute(Sender: TObject);
begin
    QrDM.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
	    EdHoTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdPrintExecute(Sender: TObject);
var
    d: TDateTime;
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    d := Date;
    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        1, //0: Lich su, 1: HSNV/Lich Su
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        mManv]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);
    with QrDM do
    begin
    	if not Active then
        	Exit;

		bBrowse := State in [dsBrowse];
    end;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.QrDMAfterInsert(DataSet: TDataSet);
begin
    with QrDM do
    begin
        FieldByName('Manv').AsString := mManv;
        FieldByName('SoThangBatDau').AsFloat := 0;
        FieldByName('SoThangKetThuc').AsFloat := 0;
        FieldByName('Co_PhuThuoc').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.QrDMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.QrDMBeforePost(DataSet: TDataSet);
begin
    with QrDM do
    begin
        if BlankConfirm(QrDM, ['HoTen', 'QuanHe', 'NgaySinh']) then
            Abort;

        if FieldByName('Co_PhuThuoc').AsBoolean then
           if BlankConfirm(QrDM, ['ThangBatDau', 'NamBatDau']) then
                Abort;

        SetAudit(DataSet);
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.QrDMNgayKetThucChange(Sender: TField);
var
    mNgayKetThuc: TDateTime;
    nextYear, nextMonth, nextDay: Word;
begin
    with QrDM do
    begin
        if not FieldByName('NgayKetThuc').IsNull then
        begin
            mNgayKetThuc := FieldByName('NgayKetThuc').AsDateTime;
            DecodeDate(mNgayKetThuc, nextYear, nextMonth, nextDay);
            FieldByName('ThangKetThuc').AsInteger := nextMonth;
            FieldByName('NamKetThuc').AsInteger := nextYear;
        end
        else
        begin
            FieldByName('ThangKetThuc').Clear;
            FieldByName('NamKetThuc').Clear;
        end;
        GrList.InvalidateCurrentRow;
    end;
end;

procedure TFrmHoSonvQuanHeGiaDinh.QrDMNgaySinhChange(Sender: TField);
var
    fSoThangBatDau, fSoThangKetThuc: Integer;
    mNgaySinh, mNgayBatDau, mNgayKetThuc: TDateTime;
begin
    with QrDM do
    begin
        try
            if not (FieldByName('NgaySinh').IsNull) then
            begin
                mNgaySinh := FieldByName('NgaySinh').AsDateTime;
                if not (FieldByName('SoThangBatDau').IsNull) then
                begin
                    fSoThangBatDau := FieldByName('SoThangBatDau').AsInteger;
                    if fSoThangBatDau  = 0 then
                        FieldByName('NgayBatDau').Clear
                    else
                    begin
                        mNgayBatDau := IncMonth(mNgaySinh, fSoThangBatDau);
                        FieldByName('NgayBatDau').AsDateTime := mNgayBatDau;
                    end;
                end;

                if not (FieldByName('SoThangKetThuc').IsNull) then
                begin
                    fSoThangKetThuc := FieldByName('SoThangKetThuc').AsInteger;
                    if fSoThangKetThuc  = 0 then
                        FieldByName('NgayKetThuc').Clear
                    else
                    begin
                        mNgayKetThuc := IncMonth(mNgaySinh, fSoThangKetThuc);
                        FieldByName('NgayKetThuc').AsDateTime := mNgayKetThuc;
                    end;
                end;
                GrList.InvalidateCurrentRow;
            end;
        except
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 483;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if QrDM.Active then
		Status.Panels[0].Text := exRecordCount(QrDM, Filter);

    Status.Panels[2].Text := exStatusAudit(QrDM);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvQuanHeGiaDinh.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

end.
