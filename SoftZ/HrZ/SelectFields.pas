(*==============================================================================
**------------------------------------------------------------------------------
*)
unit SelectFields;

interface

uses
  Classes, Controls, Forms,
  CheckLst, Buttons, StdCtrls;

type
  TFrmSelectFields = class(TForm)
    ChkList: TCheckListBox;
    BtnCancel: TBitBtn;
    CmdReturn: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure TntFormCreate(Sender: TObject);
  private
  public
  	function  Get(pFields, pDescs, pTitle: String; pDef: Boolean = True): String;
  end;

var
  FrmSelectFields: TFrmSelectFields;

implementation

uses
	isLib;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmSelectFields.Get;
var
	i, n: Integer;
    ls: TStrings;
begin
	with ChkList do
    begin
    	Items.Text := pDescs;
    	ItemIndex := 0;
	    if pDef then
            for i := 0 to Items.Count - 1 do
            	Checked[i] := True;
    end;

    if pTitle <> '' then
    	Caption := pTitle;

    if ShowModal = mrOK then
    begin
    	ls := TStringList.Create;
        ls.Text := pFields;

    	with ChkList do
        begin
        	Result := '';
            n := Items.Count - 1;
            for i := 0 to n do
            	if Checked[i] then
	            	Result := Result + ls.Strings[i] + #13;
		end;

        ls.Free;
    end
    else
    	Result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSelectFields.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSelectFields.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSelectFields.TntFormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
