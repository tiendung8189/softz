object FrmHoSonv: TFrmHoSonv
  Left = 117
  Top = 93
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7891' S'#417' Nh'#226'n Vi'#234'n'
  ClientHeight = 691
  ClientWidth = 1008
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1008
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 79
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 79
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 87
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton12: TToolButton
      Left = 166
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 174
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 253
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 332
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 340
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 419
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object BtnPrint: TToolButton
      Left = 427
      Top = 0
      Cursor = 1
      Caption = 'In'
      DropdownMenu = PopIn
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnPrintClick
    end
    object ToolButton10: TToolButton
      Left = 521
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnChitiet: TToolButton
      Left = 529
      Top = 0
      Cursor = 1
      Hint = 'L'#7883'ch s'#7917
      Caption = 'L'#7883'ch s'#7917
      DropdownMenu = PopMore
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = BtnChitietClick
    end
    object ToolButton15: TToolButton
      Left = 623
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 631
      Top = 0
      Cursor = 1
      Action = CmdHinhAnh
      ImageIndex = 19
    end
    object ToolButton18: TToolButton
      Left = 710
      Top = 0
      Width = 8
      Caption = 'ToolButton18'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnImport: TToolButton
      Left = 718
      Top = 0
      Cursor = 1
      Caption = 'Import'
      DropdownMenu = PopImport
      ImageIndex = 26
      Style = tbsDropDown
      OnClick = BtnImportClick
    end
    object ToolButton17: TToolButton
      Left = 812
      Top = 0
      Width = 8
      Caption = 'ToolButton17'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton16: TToolButton
      Left = 820
      Top = 0
      Cursor = 1
      Action = CmdNewList
      ImageIndex = 11
    end
    object ToolButton14: TToolButton
      Left = 899
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 907
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 441
    Top = 44
    Width = 567
    Height = 626
    Align = alRight
    HotSpotHighlight = 11855600
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 2
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaThongTinChung: TPanel
      Left = 8
      Top = 0
      Width = 559
      Height = 190
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
      object paPhoto: TPanel
        Left = 473
        Top = 2
        Width = 84
        Height = 186
        Align = alRight
        TabOrder = 1
        Visible = False
        object Photo: TfcDBImager
          Left = 1
          Top = 1
          Width = 82
          Height = 184
          Cursor = 1
          Align = alClient
          AutoSize = True
          BitmapOptions.AlphaBlend.Amount = 0
          BitmapOptions.AlphaBlend.Transparent = False
          BitmapOptions.Color = clNone
          BitmapOptions.Contrast = 0
          BitmapOptions.Embossed = False
          BitmapOptions.TintColor = clNone
          BitmapOptions.GaussianBlur = 0
          BitmapOptions.GrayScale = False
          BitmapOptions.HorizontallyFlipped = False
          BitmapOptions.Inverted = False
          BitmapOptions.Lightness = 0
          BitmapOptions.Rotation.CenterX = -1
          BitmapOptions.Rotation.CenterY = -1
          BitmapOptions.Rotation.Angle = 0
          BitmapOptions.Saturation = -1
          BitmapOptions.Sharpen = 0
          BitmapOptions.Sponge = 0
          BitmapOptions.VerticallyFlipped = False
          BitmapOptions.Wave.XDiv = 0
          BitmapOptions.Wave.YDiv = 0
          BitmapOptions.Wave.Ratio = 0
          BitmapOptions.Wave.Wrap = False
          DrawStyle = dsProportional
          PreProcess = True
          SmoothStretching = False
          Transparent = False
          TransparentColor = clNone
          TabOrder = 0
        end
      end
      object PaProEmp: TPanel
        Left = 2
        Top = 2
        Width = 471
        Height = 186
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          471
          186)
        object Label2: TLabel
          Left = 99
          Top = 19
          Width = 4
          Height = 16
          Alignment = taRightJustify
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdMa: TDBEditEh
          Tag = 1
          Left = 147
          Top = 37
          Width = 100
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          CharCase = ecUpperCase
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' nh'#226'n vi'#234'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'ManvQL'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HighlightRequired = True
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object EdTen: TDBEditEh
          Tag = 1
          Left = 147
          Top = 61
          Width = 195
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 37
          ControlLabel.Height = 16
          ControlLabel.Caption = 'H'#7885' t'#234'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Ho'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object wwDBEdit1: TDBEditEh
          Left = 345
          Top = 37
          Width = 77
          Height = 22
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 83
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' ch'#7845'm c'#244'ng'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MaCC'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object wwDBEdit6: TDBEditEh
          Tag = 1
          Left = 345
          Top = 61
          Width = 77
          Height = 22
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 4
          ControlLabel.Height = 16
          ControlLabel.Caption = ' '
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Ten'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object wwDBEdit4: TDBEditEh
          Left = 147
          Top = 109
          Width = 275
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 58
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'i'#7879'n tho'#7841'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DienThoai'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
        object wwDBEdit5: TDBEditEh
          Left = 147
          Top = 133
          Width = 275
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Email c'#244'ng ty'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Email'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
        object wwDBEdit22: TDBEditEh
          Left = 147
          Top = 157
          Width = 275
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 63
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' s'#7889' thu'#7871
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MaSoThue'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 8
          Visible = True
        end
        object wwDBEdit23: TDBEditEh
          Left = 147
          Top = 85
          Width = 275
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 73
          ControlLabel.Height = 16
          ControlLabel.Caption = 'T'#234'n g'#7885'i kh'#225'c'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'BiDanh'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
        object DBEditEh16: TDBEditEh
          Tag = 1
          Left = 147
          Top = 13
          Width = 100
          Height = 22
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Color = clBtnFace
          ControlLabel.Width = 12
          ControlLabel.Height = 16
          ControlLabel.Caption = 'ID'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Manv'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HighlightRequired = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          Visible = True
        end
      end
    end
    object PgMain: TPageControl
      Left = 8
      Top = 190
      Width = 559
      Height = 436
      Cursor = 1
      ActivePage = TabSheet1
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      HotTrack = True
      Images = DataMain.ImageSmall
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'H'#7891' s'#417
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 58
        ParentFont = False
        object ScrollPro: TScrollBox
          Left = 0
          Top = 0
          Width = 551
          Height = 407
          Align = alClient
          TabOrder = 0
          object PaToChuc: TisPanel
            Left = 0
            Top = 0
            Width = 530
            Height = 199
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' :: Th'#244'ng tin t'#7893' ch'#7913'c'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 197
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              199)
            object wwDBEdit25: TDBEditEh
              Left = 143
              Top = 96
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 89
              ControlLabel.Height = 16
              ControlLabel.Caption = 'C'#244'ng vi'#7879'c ch'#237'nh'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'CongViecChinh'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 6
              Visible = True
            end
            object CbChucDanh: TDbLookupComboboxEh2
              Left = 143
              Top = 72
              Width = 298
              Height = 22
              ControlLabel.Width = 61
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c danh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaChucDanh'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TenChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end
                item
                  FieldName = 'MaChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaChucDanh'
              ListField = 'TenChucDanh'
              ListSource = HrDataMain.DsDM_CHUCDANH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 4
              Visible = True
            end
            object DBEditEh1: TDBEditEh
              Left = 444
              Top = 72
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'MaChucDanh'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdChucDanh
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object DBEditEh2: TDBEditEh
              Left = 444
              Top = 24
              Width = 77
              Height = 22
              TabStop = False
              Alignment = taLeftJustify
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_MaPhongBan'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdPhongBan
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object CbPhongBan: TDbLookupComboboxEh2
              Left = 143
              Top = 24
              Width = 298
              Height = 22
              ControlLabel.Width = 60
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ph'#242'ng ban'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaPhongBan'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TenPhongBan'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end
                item
                  FieldName = 'MaPhongBan'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsDep
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clInfoBk
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaPhongBan'
              ListField = 'TenPhongBan'
              ListSource = DsDep
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object CbBoPhan: TDbLookupComboboxEh2
              Left = 143
              Top = 48
              Width = 298
              Height = 22
              ControlLabel.Width = 46
              ControlLabel.Height = 16
              ControlLabel.Caption = 'B'#7897' ph'#7853'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaBoPhan'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TenBoPhan'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end
                item
                  FieldName = 'Ma'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsSec
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaBoPhan'
              ListField = 'TenBoPhan'
              ListSource = HrDataMain.DsDMBOPHAN
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 2
              Visible = True
              OnDropDown = CbBoPhanDropDown
            end
            object EdMaBoPhan_BanDau: TDBEditEh
              Left = 444
              Top = 48
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_MaBoPhan_BanDau'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdPhongBan
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object EdXacNhan_ManvQL: TDBEditEh
              Left = 444
              Top = 120
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_XacNhan_ManvQL'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 8
              Visible = True
            end
            object CbXacNhan_Manv: TDbLookupComboboxEh2
              Left = 143
              Top = 120
              Width = 298
              Height = 22
              ControlLabel.Width = 78
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i qu'#7843'n l'#253
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'XacNhan_Manv'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'Tennv'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 130
                end
                item
                  FieldName = 'ManvQL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsEmp
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clWindow
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Manv'
              ListField = 'Tennv'
              ListSource = HrDataMain.DsDMNV
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 7
              Visible = True
            end
            object EdXetDuyet_ManvQL: TDBEditEh
              Left = 444
              Top = 144
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_XetDuyet_ManvQL'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 10
              Visible = True
            end
            object CbXetDuyet_Manv: TDbLookupComboboxEh2
              Left = 143
              Top = 144
              Width = 298
              Height = 22
              ControlLabel.Width = 127
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i x'#233't duy'#7879't (BOD)'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'XetDuyet_Manv'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'Tennv'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 130
                end
                item
                  FieldName = 'ManvQL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = DsEmp
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Color = clWindow
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Manv'
              ListField = 'Tennv'
              ListSource = HrDataMain.DsDMNV
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 9
              Visible = True
            end
            object EdThongTinKhac: TDBMemoEh
              Left = 143
              Top = 168
              Width = 378
              Height = 22
              ControlLabel.Width = 130
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Th'#244'ng tin t'#7893' ch'#7913'c kh'#225'c'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Anchors = [akLeft, akTop, akRight]
              AutoSize = False
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'ThongTinKhac'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  DefaultAction = True
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 11
              Visible = True
              WantReturns = True
            end
          end
          object PaTuyenDung: TisPanel
            Left = 0
            Top = 199
            Width = 530
            Height = 367
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: Th'#244'ng tin tuy'#7875'n d'#7909'ng v'#224' vi'#7879'c l'#224'm'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 350
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object GroupBox9: TGroupBox
              Left = 1
              Top = 298
              Width = 528
              Height = 68
              Align = alClient
              Caption = '  Ph'#233'p n'#259'm   '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                528
                68)
              object DBNumberEditEh1: TDBNumberEditEh
                Left = 142
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 52
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Lu'#7853't '#273#7883'nh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_LuatDinh'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 0
                Visible = True
              end
              object DBNumberEditEh2: TDBNumberEditEh
                Left = 250
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 62
                ControlLabel.Height = 16
                ControlLabel.Caption = 'C'#7897'ng th'#234'm'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_ChucDanh'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object DBNumberEditEh3: TDBNumberEditEh
                Left = 357
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 61
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Th'#226'm ni'#234'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_ThamNien'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 2
                Visible = True
              end
              object DBNumberEditEh4: TDBNumberEditEh
                Left = 490
                Top = 12
                Width = 30
                Height = 22
                TabStop = False
                ControlLabel.Width = 86
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ph'#233'p n'#259'm t'#7893'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                Ctl3D = False
                DataField = 'CALC_TongPhepNam'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object rDBCheckBox1: TrDBCheckBox
                Left = 240
                Top = 40
                Width = 280
                Height = 22
                Anchors = [akTop, akRight]
                Caption = ' '#193'p d'#7909'ng lu'#7853't 1 ng'#224'y ph'#233'p / 1 th'#225'ng n'#259'm '#273#7847'u '
                DataField = 'Co_PhepNamMotThang'
                DataSource = DsDMNV
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                ValueChecked = 'True'
                ValueUnchecked = 'False'
                ShowFieldCaption = False
                UpdateAfterClick = True
              end
            end
            object GroupBox10: TGroupBox
              Left = 1
              Top = 110
              Width = 528
              Height = 188
              Align = alTop
              Caption = '  Vi'#7879'c l'#224'm  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                528
                188)
              object Label15: TLabel
                Left = 40
                Top = 66
                Width = 96
                Height = 16
                Alignment = taRightJustify
                Caption = 'Th'#7917' vi'#7879'c t'#7915' ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label18: TLabel
                Left = 359
                Top = 66
                Width = 54
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = #272#7871'n ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label86: TLabel
                Left = 45
                Top = 90
                Width = 91
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y ch'#237'nh th'#7913'c'
                Font.Charset = ANSI_CHARSET
                Font.Color = 8404992
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object lbThamNien: TLabel
                Left = 324
                Top = 90
                Width = 89
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Ng'#224'y th'#226'm ni'#234'n'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label4: TLabel
                Left = 59
                Top = 18
                Width = 77
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y v'#224'o l'#224'm'
                Font.Charset = ANSI_CHARSET
                Font.Color = 8404992
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label5: TLabel
                Left = 42
                Top = 42
                Width = 94
                Height = 16
                Alignment = taRightJustify
                Caption = 'H'#7885'c vi'#7879'c t'#7915' ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label6: TLabel
                Left = 359
                Top = 42
                Width = 54
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = #272#7871'n ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object DpTuNgay: TwwDBDateTimePicker
                Left = 142
                Top = 62
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayBatDauThuViec'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 3
              end
              object wwDBDateTimePicker20: TwwDBDateTimePicker
                Left = 419
                Top = 62
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayKetThucThuViec'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 4
              end
              object DpChinhThucTuNgay: TwwDBDateTimePicker
                Left = 142
                Top = 86
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayVaoLamChinhThuc'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = 8404992
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ShowButton = True
                TabOrder = 5
              end
              object CbThamNien: TwwDBDateTimePicker
                Left = 419
                Top = 86
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayThamNien'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = 8404992
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ShowButton = True
                TabOrder = 6
              end
              object CbNoiLamViec: TDbLookupComboboxEh2
                Left = 142
                Top = 110
                Width = 298
                Height = 22
                ControlLabel.Width = 69
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i l'#224'm vi'#7879'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaNoiLV'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenNoiLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaNoiLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_NOILV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaNoiLV'
                ListField = 'TenNoiLV'
                ListSource = HrDataMain.DsDM_NOILV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 7
                Visible = True
              end
              object DBEditEh6: TDBEditEh
                Left = 443
                Top = 110
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaNoiLV'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdNoiLamViec
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 8
                Visible = True
              end
              object CbNhomLamViec: TDbLookupComboboxEh2
                Left = 142
                Top = 134
                Width = 298
                Height = 22
                ControlLabel.Width = 84
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#243'm l'#224'm vi'#7879'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaNhomLV'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenNhomLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaNhomLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_NHOMLV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Flat = True
                KeyField = 'MaNhomLV'
                ListField = 'TenNhomLV'
                ListSource = HrDataMain.DsDM_NHOMLV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 9
                Visible = True
              end
              object DBEditEh7: TDBEditEh
                Left = 443
                Top = 134
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaNhomLV'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 10
                Visible = True
              end
              object CbDoiTuong: TDbLookupComboboxEh2
                Left = 142
                Top = 158
                Width = 378
                Height = 22
                ControlLabel.Width = 56
                ControlLabel.Height = 16
                ControlLabel.Caption = #272#7889'i t'#432#7907'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'DoiTuong'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_HR_DOITUONG_NHANVIEN
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_HR_DOITUONG_NHANVIEN
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 11
                Visible = True
              end
              object wwDBDateTimePicker3: TwwDBDateTimePicker
                Left = 142
                Top = 14
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayVaoLam'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = 8404992
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ShowButton = True
                TabOrder = 0
              end
              object DpHocViecTuNgay: TwwDBDateTimePicker
                Left = 142
                Top = 38
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayBatDauHocViec'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 1
              end
              object DpHocViecDenNgay: TwwDBDateTimePicker
                Left = 419
                Top = 38
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayKetThucHocViec'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 2
              end
            end
            object GroupBox11: TGroupBox
              Left = 1
              Top = 17
              Width = 528
              Height = 93
              Align = alTop
              Caption = '  Tuy'#7875'n d'#7909'ng   '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              DesignSize = (
                528
                93)
              object Label20: TLabel
                Left = 45
                Top = 67
                Width = 91
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y ph'#7887'ng v'#7845'n'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label32: TLabel
                Left = 286
                Top = 67
                Width = 127
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Ng'#224'y ki'#7875'm tra k'#7929' n'#259'ng'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object wwDBDateTimePicker19: TwwDBDateTimePicker
                Left = 142
                Top = 63
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayPhongVan'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 4
              end
              object wwDBDateTimePicker22: TwwDBDateTimePicker
                Left = 419
                Top = 63
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayKiemTraKyNang'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 5
              end
              object CbLyDoTuyen: TDbLookupComboboxEh2
                Left = 142
                Top = 15
                Width = 298
                Height = 22
                ControlLabel.Width = 65
                ControlLabel.Height = 16
                ControlLabel.Caption = 'L'#253' do tuy'#7875'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaQTLV_VaoLam'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenQTLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaQTLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_LYDO_TUYENDUNG
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaQTLV'
                ListField = 'TenQTLV'
                ListSource = HrDataMain.DsDM_LYDO_TUYENDUNG
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object DBEditEh4: TDBEditEh
                Left = 443
                Top = 15
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaQTLV_VaoLam'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdLoaiQuaTrinhLamViec
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object CbNguonTuyenDung: TDbLookupComboboxEh2
                Left = 142
                Top = 39
                Width = 298
                Height = 22
                ControlLabel.Width = 103
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ngu'#7891'n tuy'#7875'n d'#7909'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaNguonTD'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenNguonTD'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaNguonTD'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_NGUONTUYENDUNG
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaNguonTD'
                ListField = 'TenNguonTD'
                ListSource = HrDataMain.DsDM_NGUONTUYENDUNG
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
              end
              object DBEditEh5: TDBEditEh
                Left = 443
                Top = 39
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaNguonTD'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdNguonTuyenDung
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
            end
          end
          object PaChinhQuyen: TisPanel
            Left = 0
            Top = 566
            Width = 530
            Height = 176
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 2
            HeaderCaption = ' .: Th'#244'ng tin ch'#237'nh quy'#7873'n'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 127
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              176)
            object Label74: TLabel
              Left = 334
              Top = 51
              Width = 80
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y bi'#234'n ch'#7871
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label78: TLabel
              Left = 37
              Top = 99
              Width = 100
              Height = 16
              Alignment = taRightJustify
              Caption = 'B'#7893' nhi'#7879'm t'#7915' ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label80: TLabel
              Left = 41
              Top = 147
              Width = 96
              Height = 16
              Alignment = taRightJustify
              Caption = 'Bi'#7879't ph'#225'i t'#7915' ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label1: TLabel
              Left = 360
              Top = 99
              Width = 54
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = #272#7871'n ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label3: TLabel
              Left = 360
              Top = 147
              Width = 54
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = #272#7871'n ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object wwDBEdit24: TDBEditEh
              Left = 143
              Top = 25
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 118
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c v'#7909' ch'#237'nh quy'#7873'n'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'ChucVuChinhQuyen'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 0
              Visible = True
            end
            object wwDBDateTimePicker2: TwwDBDateTimePicker
              Left = 420
              Top = 49
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayBienChe'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 3
            end
            object DBNumberEditEh7: TDBNumberEditEh
              Left = 143
              Top = 49
              Width = 45
              Height = 22
              ControlLabel.Width = 89
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Quy ho'#7841'ch n'#259'm'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'NamQuyHoach'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object DBNumberEditEh8: TDBNumberEditEh
              Left = 278
              Top = 49
              Width = 45
              Height = 22
              ControlLabel.Width = 74
              ControlLabel.Height = 16
              ControlLabel.Caption = 'B'#7893' sung n'#259'm'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'NamQuyHoachBoSung'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object DpBoNhiemTuNgay: TwwDBDateTimePicker
              Left = 143
              Top = 97
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BoNhiem_NgayBatDau'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 6
            end
            object DpBietPhaiTuNgay: TwwDBDateTimePicker
              Left = 143
              Top = 145
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BietPhai_NgayBatDau'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 10
            end
            object CbBoNhiem_MaChucDanh: TDbLookupComboboxEh2
              Left = 143
              Top = 73
              Width = 298
              Height = 22
              ControlLabel.Width = 118
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c danh b'#7893' nhi'#7879'm'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'BoNhiem_MaChucDanh'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TenChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end
                item
                  FieldName = 'MaChucDanh'
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaChucDanh'
              ListField = 'TenChucDanh'
              ListSource = HrDataMain.DsDM_CHUCDANH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 4
              Visible = True
            end
            object CbBietPhai_MaChucDanh: TDbLookupComboboxEh2
              Left = 143
              Top = 121
              Width = 298
              Height = 22
              ControlLabel.Width = 114
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c danh bi'#7879't ph'#225'i'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'BietPhai_MaChucDanh'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TenChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end
                item
                  FieldName = 'MaChucDanh'
                  Title.Caption = 'M'#227
                  Width = 40
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaChucDanh'
              ListField = 'TenChucDanh'
              ListSource = HrDataMain.DsDM_CHUCDANH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 8
              Visible = True
            end
            object DBEditEh11: TDBEditEh
              Left = 444
              Top = 73
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'BoNhiem_MaChucDanh'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdChucDanh
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object DBEditEh12: TDBEditEh
              Left = 444
              Top = 121
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'BietPhai_MaChucDanh'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdChucDanh
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 9
              Visible = True
            end
            object wwDBDateTimePicker17: TwwDBDateTimePicker
              Left = 420
              Top = 97
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BoNhiem_NgayKetThuc'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 7
            end
            object wwDBDateTimePicker18: TwwDBDateTimePicker
              Left = 420
              Top = 145
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BietPhai_NgayKetThuc'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 11
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'L'#253' l'#7883'ch'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 60
        ParentFont = False
        object ScrollCV: TScrollBox
          Left = 0
          Top = 0
          Width = 551
          Height = 407
          Align = alClient
          TabOrder = 0
          object PaCaNhan: TisPanel
            Left = 0
            Top = 0
            Width = 530
            Height = 223
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' :: Th'#244'ng tin c'#225' nh'#226'n'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 0
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              223)
            object DBEdit2: TDBEditEh
              Left = 143
              Top = 144
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 71
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Chuy'#234'n m'#244'n'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'ChuyenMon'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 10
              Visible = True
            end
            object DBEdit5: TDBEditEh
              Left = 143
              Top = 192
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 108
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Trang m'#7841'ng x'#227' h'#7897'i'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'Website'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 12
              Visible = True
            end
            object wwDBEdit2: TDBEditEh
              Left = 143
              Top = 168
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 80
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Email c'#225' nh'#226'n'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'EmailCaNhan'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 11
              Visible = True
            end
            object DBNumberEditEh5: TDBNumberEditEh
              Left = 476
              Top = 24
              Width = 45
              Height = 22
              ControlLabel.Width = 81
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Chi'#7873'u cao (m)'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'ChieuCao'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object wwDBEdit29: TDBEditEh
              Left = 143
              Top = 48
              Width = 240
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 114
              ControlLabel.Height = 16
              ControlLabel.Caption = 'T'#236'nh tr'#7841'ng s'#7913'c kh'#7887'e'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'TinhTrangSucKhoe'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object DBNumberEditEh6: TDBNumberEditEh
              Left = 476
              Top = 48
              Width = 45
              Height = 22
              ControlLabel.Width = 82
              ControlLabel.Height = 16
              ControlLabel.Caption = 'C'#226'n n'#7863'ng (Kg)'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'CanNang'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object CbNhomMau: TDbLookupComboboxEh2
              Left = 143
              Top = 24
              Width = 125
              Height = 22
              ControlLabel.Width = 62
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Nh'#243'm m'#225'u'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'NhomMau'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_HR_NHOMMAU
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdNhomMau
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_HR_NHOMMAU
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
            object CbQuocTich: TDbLookupComboboxEh2
              Left = 143
              Top = 96
              Width = 125
              Height = 22
              ControlLabel.Width = 53
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Qu'#7889'c t'#7883'ch'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'QuocTich'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_QUOCTICH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdQuocTich
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_QUOCTICH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 6
              Visible = True
            end
            object CbTonGiao: TDbLookupComboboxEh2
              Left = 143
              Top = 120
              Width = 125
              Height = 22
              ControlLabel.Width = 50
              ControlLabel.Height = 16
              ControlLabel.Caption = 'T'#244'n gi'#225'o'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'TonGiao'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_TONGIAO
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdTonGiao
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_TONGIAO
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 8
              Visible = True
            end
            object CbDanToc: TDbLookupComboboxEh2
              Left = 396
              Top = 96
              Width = 125
              Height = 22
              ControlLabel.Width = 43
              ControlLabel.Height = 16
              ControlLabel.Caption = 'D'#226'n t'#7897'c'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akTop, akRight]
              DynProps = <>
              DataField = 'DanToc'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_DANTOC
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdDanToc
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_DANTOC
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 7
              Visible = True
            end
            object CbVanHoa: TDbLookupComboboxEh2
              Left = 396
              Top = 120
              Width = 125
              Height = 22
              ControlLabel.Width = 47
              ControlLabel.Height = 16
              ControlLabel.Caption = 'V'#259'n h'#243'a'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akTop, akRight]
              DynProps = <>
              DataField = 'VanHoa'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_VANHOA
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdVanHoa
                  DefaultAction = False
                  Style = ebsEllipsisEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_VANHOA
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 9
              Visible = True
            end
            object CbGioiTinh: TDbLookupComboboxEh2
              Left = 143
              Top = 72
              Width = 125
              Height = 22
              ControlLabel.Width = 46
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Gi'#7899'i t'#237'nh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              DynProps = <>
              DataField = 'GioiTinh'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsLOAI_GIOITINH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsLOAI_GIOITINH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 4
              Visible = True
            end
            object CbHonNhan: TDbLookupComboboxEh2
              Left = 396
              Top = 72
              Width = 125
              Height = 22
              ControlLabel.Width = 116
              ControlLabel.Height = 16
              ControlLabel.Caption = 'T'#236'nh tr'#7841'ng h'#244'n nh'#226'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akTop, akRight]
              DynProps = <>
              DataField = 'HonNhan'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TEN_HOTRO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsV_HONNHAN
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MA_HOTRO'
              ListField = 'TEN_HOTRO'
              ListSource = HrDataMain.DsV_HONNHAN
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 5
              Visible = True
            end
          end
          object PaNoiSinhHuuTri: TisPanel
            Left = 0
            Top = 223
            Width = 530
            Height = 101
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: Th'#244'ng tin n'#417'i sinh v'#224' h'#432'u tr'#237
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 126
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              101)
            object Label48: TLabel
              Left = 83
              Top = 27
              Width = 55
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ng'#224'y sinh'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label51: TLabel
              Left = 280
              Top = 51
              Width = 42
              Height = 16
              Alignment = taRightJustify
              Caption = '(th'#225'ng)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label52: TLabel
              Left = 332
              Top = 51
              Width = 82
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y ngh'#7881' h'#432'u'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object wwDBEdit12: TDBEditEh
              Left = 143
              Top = 71
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 45
              ControlLabel.Height = 16
              ControlLabel.Caption = 'N'#417'i sinh'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NoiSinh_DiaChi'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 6
              Visible = True
            end
            object wwDBDateTimePicker9: TwwDBDateTimePicker
              Left = 143
              Top = 23
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgaySinh'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = 8404992
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ShowButton = True
              TabOrder = 1
            end
            object DBNumberEditEh9: TDBNumberEditEh
              Left = 307
              Top = 23
              Width = 45
              Height = 22
              ControlLabel.Width = 53
              ControlLabel.Height = 16
              ControlLabel.Caption = 'N'#259'm sinh'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'NamSinh'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object DBNumberEditEh10: TDBNumberEditEh
              Left = 143
              Top = 47
              Width = 45
              Height = 22
              ControlLabel.Width = 67
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Tu'#7893'i h'#432'u tr'#237
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'HuuTriNam'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object DBNumberEditEh11: TDBNumberEditEh
              Left = 230
              Top = 47
              Width = 45
              Height = 22
              ControlLabel.Width = 35
              ControlLabel.Height = 16
              ControlLabel.Caption = '(n'#259'm)'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'HuuTriThang'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              ParentCtl3D = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object wwDBDateTimePicker10: TwwDBDateTimePicker
              Left = 420
              Top = 47
              Width = 101
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              Color = clBtnFace
              DataField = 'NgayHuuTri'
              DataSource = DsDMNV
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = 8404992
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              ShowButton = True
              TabOrder = 5
            end
          end
          object PaDangDoanThe: TisPanel
            Left = 0
            Top = 528
            Width = 530
            Height = 269
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 3
            HeaderCaption = ' .: '#272#7843'ng - '#272'o'#224'n th'#7875
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 126
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              269)
            object GroupBox7: TGroupBox
              Left = 1
              Top = 17
              Width = 528
              Height = 94
              Align = alTop
              Caption = '  '#272#7843'ng vi'#234'n  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                528
                94)
              object Label27: TLabel
                Left = 46
                Top = 19
                Width = 91
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y ch'#237'nh th'#7913'c'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label35: TLabel
                Left = 63
                Top = 41
                Width = 74
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y k'#7871't n'#7841'p'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object wwDBDateTimePicker12: TwwDBDateTimePicker
                Left = 142
                Top = 15
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'DangVien_NgayChinhThuc'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 0
              end
              object wwDBDateTimePicker13: TwwDBDateTimePicker
                Left = 142
                Top = 39
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'DangVien_NgayThamGia'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 1
              end
              object CbDangVien_TinhTrang: TDbLookupComboboxEh2
                Left = 385
                Top = 39
                Width = 135
                Height = 22
                ControlLabel.Width = 59
                ControlLabel.Height = 16
                ControlLabel.Caption = 'T'#236'nh tr'#7841'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akTop, akRight]
                DynProps = <>
                DataField = 'DangVien_TinhTrang'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_HR_DANG_TINHTRANG
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdDangTinhTrang
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_HR_DANG_TINHTRANG
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
              end
              object CbChiBoDang: TDbLookupComboboxEh2
                Left = 142
                Top = 63
                Width = 378
                Height = 22
                ControlLabel.Width = 70
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Chi b'#7897' '#272#7843'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'DangVien_ChiBo'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_HR_DANG_CHIBO
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdChiBoDang
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_HR_DANG_CHIBO
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 3
                Visible = True
              end
            end
            object GroupBox8: TGroupBox
              Left = 1
              Top = 111
              Width = 528
              Height = 73
              Align = alTop
              Caption = '  '#272'o'#224'n vi'#234'n - C'#244'ng '#273'o'#224'n  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                528
                73)
              object Label7: TLabel
                Left = 45
                Top = 21
                Width = 91
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y c'#244'ng '#273'o'#224'n'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label53: TLabel
                Left = 3
                Top = 43
                Width = 133
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y k'#7871't n'#7841'p '#273'o'#224'n vi'#234'n'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object wwDBDateTimePicker14: TwwDBDateTimePicker
                Left = 142
                Top = 17
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'CongDoan_NgayThamGia'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 0
              end
              object wwDBDateTimePicker15: TwwDBDateTimePicker
                Left = 142
                Top = 41
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'DoanVien_NgayThamGia'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 1
              end
              object CbDoanVien_TinhTrang: TDbLookupComboboxEh2
                Left = 385
                Top = 41
                Width = 135
                Height = 22
                ControlLabel.Width = 59
                ControlLabel.Height = 16
                ControlLabel.Caption = 'T'#236'nh tr'#7841'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akTop, akRight]
                DynProps = <>
                DataField = 'DoanVien_TinhTrang'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_HR_DOAN_TINHTRANG
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdDoanTinhTrang
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_HR_DOAN_TINHTRANG
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
              end
            end
            object wwDBEdit7: TDBEditEh
              Left = 143
              Top = 191
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 39
              ControlLabel.Height = 16
              ControlLabel.Caption = 'C'#7845'p '#7911'y'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'CapUy'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object wwDBEdit8: TDBEditEh
              Left = 143
              Top = 215
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 70
              ControlLabel.Height = 16
              ControlLabel.Caption = 'C'#7845'p '#7911'y ki'#234'm'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'CapUyKiem'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object wwDBEdit9: TDBEditEh
              Left = 143
              Top = 239
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 119
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c v'#7909' '#272'o'#224'n/ '#272#7843'ng'
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'ChucVuDangVien'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
          end
          object PaDiaChiLienLac: TisPanel
            Left = 0
            Top = 324
            Width = 530
            Height = 204
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 2
            HeaderCaption = ' .: '#272#7883'a ch'#7881' li'#234'n l'#7841'c'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 126
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object GroupBox3: TGroupBox
              Left = 1
              Top = 17
              Width = 528
              Height = 93
              Align = alTop
              Caption = '  N'#417'i th'#432#7901'ng tr'#250'  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                528
                93)
              object EdThuongTruDiaChi: TDBEditEh
                Tag = 999
                Left = 142
                Top = 63
                Width = 378
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 78
                ControlLabel.Height = 16
                ControlLabel.Caption = #272#7883'a ch'#7881' c'#7909' th'#7875
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'ThuongTru_DiaChi'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object CbThuongTruTinhThanhPho: TDbLookupComboboxEh2
                Left = 142
                Top = 15
                Width = 378
                Height = 22
                ControlLabel.Width = 95
                ControlLabel.Height = 16
                ControlLabel.Caption = 'T'#7881'nh/ Th'#224'nh ph'#7889
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'ThuongTru_MaTinh'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DataMain.DsTINH
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdViTriDiaLy
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MATINH'
                ListField = 'TEN'
                ListSource = DataMain.DsTINH
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object CbThuongTruQuanHuyen: TDbLookupComboboxEh2
                Left = 142
                Top = 39
                Width = 135
                Height = 22
                ControlLabel.Width = 74
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Qu'#7853'n/ Huy'#7879'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                DynProps = <>
                DataField = 'ThuongTru_MaHuyen'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DsHuyen
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 200
                DropDownBox.RowLines = 1
                EmptyDataInfo.Text = '-- Ch'#7885'n --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MAHUYEN'
                ListField = 'TEN'
                ListSource = DataMain.DsHUYEN
                ParentFont = False
                ShowHint = True
                TabOrder = 1
                Visible = True
                OnDropDown = CbThuongTruQuanHuyenDropDown
              end
              object CbThuongTruPhuongXa: TDbLookupComboboxEh2
                Left = 385
                Top = 39
                Width = 135
                Height = 22
                ControlLabel.Width = 67
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ph'#432#7901'ng/ X'#227
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akTop, akRight]
                DynProps = <>
                DataField = 'ThuongTru_MaXa'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenXa'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DsPhuongXa
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.Align = daRight
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 200
                DropDownBox.RowLines = 1
                EmptyDataInfo.Text = '-- Ch'#7885'n --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaXa'
                ListField = 'TenXa'
                ListSource = DataMain.DsPHUONGXA
                ParentFont = False
                ShowHint = True
                TabOrder = 2
                Visible = True
                OnDropDown = CbThuongTruPhuongXaDropDown
              end
            end
            object GroupBox4: TGroupBox
              Left = 1
              Top = 110
              Width = 528
              Height = 93
              Align = alClient
              Caption = '  N'#417'i c'#432' tr'#250'  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                528
                93)
              object EdTamTruDiaChi: TDBEditEh
                Tag = 999
                Left = 142
                Top = 63
                Width = 378
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 78
                ControlLabel.Height = 16
                ControlLabel.Caption = #272#7883'a ch'#7881' c'#7909' th'#7875
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'CuTru_DiaChi'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object CbCuTruTinhThanhPho: TDbLookupComboboxEh2
                Left = 142
                Top = 15
                Width = 378
                Height = 22
                ControlLabel.Width = 95
                ControlLabel.Height = 16
                ControlLabel.Caption = 'T'#7881'nh/ Th'#224'nh ph'#7889
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'CuTru_MaTinh'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DataMain.DsTINH
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdViTriDiaLy
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MATINH'
                ListField = 'TEN'
                ListSource = DataMain.DsTINH
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object CbCuTruQuanHuyen: TDbLookupComboboxEh2
                Left = 142
                Top = 39
                Width = 135
                Height = 22
                ControlLabel.Width = 74
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Qu'#7853'n/ Huy'#7879'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                DynProps = <>
                DataField = 'CuTru_MaHuyen'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DsHuyen
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 200
                DropDownBox.RowLines = 1
                EmptyDataInfo.Text = '-- Ch'#7885'n --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MAHUYEN'
                ListField = 'TEN'
                ListSource = DataMain.DsHUYEN
                ParentFont = False
                ShowHint = True
                TabOrder = 1
                Visible = True
                OnDropDown = CbCuTruQuanHuyenDropDown
              end
              object CbCuTruPhuongXa: TDbLookupComboboxEh2
                Left = 385
                Top = 39
                Width = 135
                Height = 22
                ControlLabel.Width = 67
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ph'#432#7901'ng/ X'#227
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Alignment = taLeftJustify
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akTop, akRight]
                DynProps = <>
                DataField = 'CuTru_MaXa'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenXa'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DsPhuongXa
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.Align = daRight
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 200
                DropDownBox.RowLines = 1
                EmptyDataInfo.Text = '-- Ch'#7885'n --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaXa'
                ListField = 'TenXa'
                ListSource = DataMain.DsPHUONGXA
                ParentFont = False
                ShowHint = True
                TabOrder = 2
                Visible = True
                OnDropDown = CbCuTruPhuongXaDropDown
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Gi'#7845'y t'#7901
        ImageIndex = 48
        object ScrollGiayTo: TScrollBox
          Left = 0
          Top = 0
          Width = 551
          Height = 407
          Align = alClient
          TabOrder = 0
          object PaBaoHiem: TisPanel
            Left = 0
            Top = 0
            Width = 530
            Height = 227
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' .: Th'#244'ng tin b'#7843'o hi'#7875'm'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 235
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object GroupBox1: TGroupBox
              Left = 1
              Top = 17
              Width = 528
              Height = 69
              Align = alTop
              Caption = '  B'#7843'o hi'#7875'm x'#227' h'#7897'i  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                528
                69)
              object Label39: TLabel
                Left = 361
                Top = 18
                Width = 52
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Ng'#224'y c'#7845'p'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object wwDBDateTimePicker6: TwwDBDateTimePicker
                Left = 419
                Top = 14
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'BHXH_NgayCap'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 1
              end
              object wwDBEdit10: TDBEditEh
                Left = 142
                Top = 14
                Width = 125
                Height = 22
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 32
                ControlLabel.Height = 16
                ControlLabel.Caption = 'S'#7889' s'#7893
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'BHXH_So'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 0
                Visible = True
              end
              object CbNoiCapBHXH: TDbLookupComboboxEh2
                Left = 142
                Top = 38
                Width = 378
                Height = 22
                ControlLabel.Width = 42
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i c'#7845'p'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'BHXH_NoiCap'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_BHXH_NOICAP
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdNoiCapBHXH
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_BHXH_NOICAP
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
              end
            end
            object GroupBox2: TGroupBox
              Left = 1
              Top = 86
              Width = 528
              Height = 140
              Align = alClient
              Caption = '  B'#7843'o hi'#7875'm y t'#7871'  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                528
                140)
              object Label42: TLabel
                Left = 359
                Top = 66
                Width = 54
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = #272#7871'n ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label44: TLabel
                Left = 39
                Top = 66
                Width = 97
                Height = 16
                Alignment = taRightJustify
                Caption = 'Th'#7901'i h'#7841'n t'#7915' ng'#224'y'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label65: TLabel
                Left = 253
                Top = 90
                Width = 160
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Th'#7901'i '#273'i'#7875'm '#273#7911' 5 n'#259'm li'#234'n t'#7909'c'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object wwDBDateTimePicker7: TwwDBDateTimePicker
                Left = 419
                Top = 62
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'BHYT_NgayKetThuc'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 4
              end
              object wwDBEdit11: TDBEditEh
                Left = 142
                Top = 14
                Width = 125
                Height = 22
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 34
                ControlLabel.Height = 16
                ControlLabel.Caption = 'M'#227' s'#7889
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'BHYT_So'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 0
                Visible = True
              end
              object wwDBDateTimePicker8: TwwDBDateTimePicker
                Left = 142
                Top = 62
                Width = 101
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'BHYT_NgayBatDau'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 3
              end
              object wwDBDateTimePicker1: TwwDBDateTimePicker
                Left = 419
                Top = 86
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'BHYT_Ngay5Nam'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 5
              end
              object CbNoiCapBHYT: TDbLookupComboboxEh2
                Left = 142
                Top = 110
                Width = 378
                Height = 22
                ControlLabel.Width = 90
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i c'#7845'p/ '#273#7893'i th'#7867
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'BHYT_NoiCap'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN_HOTRO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_BHYT_NOICAP
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdNoiCapBHYT
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA_HOTRO'
                ListField = 'TEN_HOTRO'
                ListSource = HrDataMain.DsV_BHYT_NOICAP
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 6
                Visible = True
              end
              object CbNoiDangKyKhamBenh: TDbLookupComboboxEh2
                Left = 142
                Top = 38
                Width = 298
                Height = 22
                ControlLabel.Width = 84
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i '#272'K KCB B'#272
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaKCB'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenKCB'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaKCB'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDMNOIKHAMBENH
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaKCB'
                ListField = 'TenKCB'
                ListSource = HrDataMain.DsDMNOIKHAMBENH
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 1
                Visible = True
              end
              object DBEditEh8: TDBEditEh
                Left = 443
                Top = 38
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaKCB'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
                Visible = True
              end
            end
          end
          object PaGiaoDich: TisPanel
            Left = 0
            Top = 454
            Width = 530
            Height = 100
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 110
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              530
              100)
            object EdTenNH: TDBEditEh
              Left = 143
              Top = 48
              Width = 378
              Height = 22
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabel.Width = 61
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_TenNH'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object EdTenNH_CN: TDBEditEh
              Left = 143
              Top = 72
              Width = 378
              Height = 22
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabel.Width = 57
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Chi nh'#225'nh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LK_TenNH_CN'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object CbSoTaiKhoan: TDbLookupComboboxEh2
              Left = 143
              Top = 24
              Width = 378
              Height = 22
              ControlLabel.Width = 71
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' t'#224'i kho'#7843'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'MaTK_Idx'
              DataSource = DsDMNV
              DropDownBox.Columns = <
                item
                  FieldName = 'TENNH'
                  Width = 200
                end
                item
                  FieldName = 'MaTK'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 52
                end>
              DropDownBox.ListSource = DsTaiKhoanNV_MANV
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <
                item
                  Action = CmdDmtk
                  DefaultAction = False
                  Glyph.Data = {
                    36040000424D3604000000000000360000002800000010000000100000000100
                    2000000000000004000000000000000000000000000000000000000000000000
                    00001220133149824CCC192F1A4B000000000000000000000000000000000000
                    00000D391D88186634F2196B37FF186634F20D391D8800000000000000000203
                    0205457548AF5FA564FD6BAE6FFF4B954FFB112012340A140B22000000000D37
                    1C84288C53FF64BA8DFF95D2B2FF64BA8DFF288C53FF0E3B1E8C00000000111B
                    12277BB97FFCB7DEBBFF67AC6CFF75B67AFF4E9751FE3C713FB82649287A1A69
                    37F762BA8BFF60BA87FFFFFFFFFF60B987FF67BC8FFF186835F700000000476F
                    4A9B9CCDA0FF6FB273FF8DC792FFAADCAFFF76B67BFF519B55FF77B77BFF317B
                    4CFF9CD4B6FFFFFFFFFFFFFFFFFFFFFFFFFF95D2B2FF196B37FF0C130D197BBB
                    80FF77B77CFF91CB97FFABDEB1FF9CD7A2FFAADDB0FF77B77CFF60AC65FF478A
                    60FF90D3B1FF92D6B1FFFFFFFFFF65BC8CFF67BC8FFF186835F7507753A07FBD
                    84FF97CE9CFFADDFB3FF6FB374FF96D59DFF9DD8A3FFAADDB0FF78B87CFF61A4
                    6FFF61AB81FF95D4B4FFBAE6D0FF6ABB8FFF2D8F57FF144824A37BBB80FF8EC8
                    93FFAFDFB5FFA1DAA7FF98D79FFF97D69EFF7EC083FF82C187FFABDDB0FF79B9
                    7DFF5EA16BFF5E9873FF4F8E66FF46895EFF3F894EFF050A05114563487D7DBB
                    82FF8FC894FFB0E0B6FFA2DAA8FF7FC185FFA4D0A7FFDDEEDFFF80B883FFABDE
                    B1FF7AB97FFF569F5AFFC4E7C8FF78B87CFF284F2A840000000000000000151D
                    15257BB87FFA90C995FFB0E0B6FF85C28AFFF7FCF8FF95C297FFDDEEDFFF82C2
                    87FFABDEB1FF7BBA7FFF58A05CFF59A15DFF0102010400000000000000000000
                    0000000000007AB67EF691CA96FFB1E0B6FFD9F3DDFFF7FCF8FFA4D0A7FF7EC0
                    84FF9FD9A5FFACDEB2FF7BBB80FF559D59FB0000000000000000000000000000
                    000000000000000000007AB780F592CB97FFB1E1B6FF85C38AFF80C185FF99D7
                    A0FF98D79FFF9FD9A5FFACDFB2FF7DBB81FF559A59F600000000000000000000
                    00000000000000000000000000007CB881F593CC98FFB1E1B7FFA3DBA9FF9BD8
                    A2FF73B477FFAFDFB4FF87C38CFF65AA69FF0509050E00000000000000000000
                    0000000000000000000000000000000000007DB882F594CC99FFB2E2B7FFA3DC
                    AAFFB0E0B6FF8CC692FF6EB173FF0609060E0000000000000000000000000000
                    000000000000000000000000000000000000000000007EB983F594CD9AFFB3E2
                    B7FF93CB98FF77B77CFF060A060E000000000000000000000000000000000000
                    00000000000000000000000000000000000000000000000000007FBA85F596CD
                    9BFF80BE85FF070A070E00000000000000000000000000000000000000000000
                    00000000000000000000000000000000000000000000000000000000000081BB
                    86F5070B070E0000000000000000000000000000000000000000}
                  Style = ebsGlyphEh
                  Width = 20
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'Idx'
              ListField = 'MaTK'
              ListSource = DsTaiKhoanNV
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 0
              Visible = True
            end
          end
          object PaGiayToTuyThan: TisPanel
            Left = 0
            Top = 227
            Width = 530
            Height = 227
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 2
            HeaderCaption = ' .: Gi'#7845'y t'#7901' t'#249'y th'#226'n'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 126
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object GroupBox5: TGroupBox
              Left = 1
              Top = 17
              Width = 528
              Height = 117
              Align = alTop
              Caption = '  CCCD/CMND  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                528
                117)
              object Label57: TLabel
                Left = 84
                Top = 42
                Width = 52
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y c'#7845'p'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object dpCCCDNgayCap: TwwDBDateTimePicker
                Left = 142
                Top = 38
                Width = 101
                Height = 22
                TabStop = False
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'LK_CCCD_NgayCap'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = True
                TabOrder = 2
              end
              object EdCCCDGhiChu: TDBEditEh
                Left = 142
                Top = 86
                Width = 378
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 42
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ghi ch'#250
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_CCCD_GhiChu'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 5
                Visible = True
              end
              object EdCCCDMaNoiCap: TDBEditEh
                Left = 443
                Top = 62
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_CCCD_MaNoiCap_Ma'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 4
                Visible = True
              end
              object EdCCCDNoiCap: TDBEditEh
                Left = 142
                Top = 62
                Width = 298
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 42
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i c'#7845'p'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_CCCD_TenNoiCap'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object CbCCCD: TDbLookupComboboxEh2
                Left = 142
                Top = 14
                Width = 235
                Height = 22
                ControlLabel.Width = 90
                ControlLabel.Height = 16
                ControlLabel.Caption = 'S'#7889' CCCD/CMND'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'CCCD_Idx'
                DataSource = DsDMNV
                DropDownBox.AutoFitColWidths = False
                DropDownBox.Columns = <
                  item
                    FieldName = 'CCCD'
                    Width = 90
                  end
                  item
                    FieldName = 'NoiCap'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 160
                  end>
                DropDownBox.ListSource = DsV_HR_DM_NHANVIEN_CCCD_MANV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 275
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdCCCD
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Idx'
                ListField = 'CCCD'
                ListSource = HrDataMain.DsQrV_HR_DM_NHANVIEN_CCCD
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object EdCCCDTenNhom: TDBEditEh
                Left = 380
                Top = 14
                Width = 140
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Caption = 'Nh'#243'm'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_CCCD_GiayTo_TenNhom'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
            end
            object GroupBox6: TGroupBox
              Left = 1
              Top = 134
              Width = 528
              Height = 92
              Align = alClient
              Caption = '  H'#7897' chi'#7871'u  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                528
                92)
              object Label61: TLabel
                Left = 84
                Top = 42
                Width = 52
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y c'#7845'p'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label66: TLabel
                Left = 338
                Top = 40
                Width = 75
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Ng'#224'y h'#7871't h'#7841'n'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object dpHoChieuNgayCap: TwwDBDateTimePicker
                Left = 142
                Top = 38
                Width = 101
                Height = 22
                TabStop = False
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'LK_HoChieu_NgayCap'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = True
                TabOrder = 2
              end
              object EdHoChieuNoiCap: TDBEditEh
                Left = 142
                Top = 62
                Width = 378
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 42
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i c'#7845'p'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_HoChieu_NoiCap'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 4
                Visible = True
              end
              object dpHoChieuNgayHetHan: TwwDBDateTimePicker
                Left = 419
                Top = 38
                Width = 101
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'LK_HoChieu_NgayHetHan'
                DataSource = DsDMNV
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = True
                TabOrder = 3
              end
              object CbHoChieu: TDbLookupComboboxEh2
                Left = 142
                Top = 14
                Width = 195
                Height = 22
                ControlLabel.Width = 67
                ControlLabel.Height = 16
                ControlLabel.Caption = 'S'#7889' h'#7897' chi'#7871'u'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'HoChieu_Idx'
                DataSource = DsDMNV
                DropDownBox.Columns = <
                  item
                    FieldName = 'HoChieu'
                  end
                  item
                    FieldName = 'GiayTo_TenNhom'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 150
                  end>
                DropDownBox.ListSource = DsV_HR_DM_NHANVIEN_HOCHIEU_MANV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdHoChieu
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Idx'
                ListField = 'HoChieu'
                ListSource = HrDataMain.DsQrV_HR_DM_NHANVIEN_HOCHIEU
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object EdHoChieuTenNhom: TDBEditEh
                Left = 380
                Top = 14
                Width = 140
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 33
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#243'm'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_HoChieu_GiayTo_TenNhom'
                DataSource = DsDMNV
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Thu nh'#7853'p'
        ImageIndex = 45
        object ScrollThuNhap: TScrollBox
          Left = 0
          Top = 0
          Width = 551
          Height = 407
          Align = alClient
          TabOrder = 0
          object PaPhuCap: TisPanel
            Left = 0
            Top = 476
            Width = 530
            Height = 320
            Align = alTop
            Color = 16119285
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 2
            HeaderCaption = ' .: Ph'#7909' c'#7845'p'
            HeaderColor = 1993170
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = DEFAULT_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object GrAllowance: TwwDBGrid2
              Left = 1
              Top = 58
              Width = 528
              Height = 261
              DittoAttributes.ShortCutDittoField = 16397
              DittoAttributes.Options = [wwdoSkipReadOnlyFields]
              ControlType.Strings = (
                'Name;CustomEdit;CbAllowanceType;F'
                'Approved;CheckBox;True;False'
                'LK_TenPhuCap;CustomEdit;CbPhuCap;F'
                'MaPhuCap;CustomEdit;CbMaPhuCap;F'
                'ThangBatDau;CustomEdit;CbMon;F'
                'NamBatDau;CustomEdit;CbYear;F')
              Selected.Strings = (
                'MaPhuCap'#9'8'#9'MaPhuCap'#9'F'
                'LK_TenPhuCap'#9'30'#9'LK_TenPhuCap'#9'F'
                'ThangBatDau'#9'10'#9'ThangBatDau'#9'F'
                'NamBatDau'#9'10'#9'NamBatDau'#9'F')
              MemoAttributes = [mSizeable, mWordWrap, mGridShow]
              IniAttributes.Delimiter = ';;'
              TitleColor = 13360356
              FixedCols = 0
              ShowHorzScrollBar = True
              EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
              Align = alClient
              BorderStyle = bsNone
              DataSource = DsAllowance
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
              ParentFont = False
              PopupMenu = PopDetail
              TabOrder = 1
              TitleAlignment = taCenter
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = 8404992
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = [fsBold]
              TitleLines = 2
              TitleButtons = False
              UseTFields = False
              TitleImageList = DataMain.ImageSort
              PadColumnStyle = pcsPadHeader
            end
            object Panel1: TPanel
              Left = 1
              Top = 17
              Width = 528
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object EdDmPhuCap: TDBEditEh
                Left = 14
                Top = 13
                Width = 139
                Height = 22
                TabStop = False
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Caption = 'M'#7903' danh s'#225'ch c'#7845'p/tr'#7843' t'#224'i s'#7843'n'
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdLoaiPhuCap
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                EmptyDataInfo.Text = 'Danh m'#7909'c ph'#7909' c'#7845'p'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clWindowText
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = []
                EmptyDataInfo.ParentFont = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                Visible = True
              end
            end
          end
          object PaThongTinLuong: TisPanel
            Left = 0
            Top = 0
            Width = 530
            Height = 321
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' .: Th'#244'ng tin l'#432#417'ng'
            HeaderColor = 1993170
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object grThongTinLuong: TDBVertGridEh
              Left = 1
              Top = 17
              Width = 528
              Height = 303
              Align = alClient
              AllowedOperations = [alopUpdateEh]
              AllowedSelections = []
              RowCategories.CategoryProps = <>
              PrintService.ColorSchema = pcsFullColorEh
              DataColParams.Font.Charset = DEFAULT_CHARSET
              DataColParams.Font.Color = 8404992
              DataColParams.Font.Height = -13
              DataColParams.Font.Name = 'Tahoma'
              DataColParams.Font.Style = [fsBold]
              DataColParams.MaxRowHeight = 2
              DataColParams.MaxRowLines = 10
              DataColParams.ParentFont = False
              DataColParams.RowHeight = 21
              DataSource = DsDMNV
              DrawGraphicData = True
              DrawMemoText = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              GridLineParams.ColorScheme = glcsThemedEh
              Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
              OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
              ParentFont = False
              RowsDefValues.FitRowHeightToData = True
              SearchPanel.Enabled = True
              SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
              TabOrder = 1
              LabelColWidth = 235
              Rows = <
                item
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'LuongChinh'
                end
                item
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'LuongBaoHiem'
                end>
            end
          end
          object PaHeSoLuong: TisPanel
            Left = 0
            Top = 321
            Width = 530
            Height = 155
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: Th'#244'ng tin h'#7879' s'#7889' l'#432#417'ng'
            HeaderColor = 1993170
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object grHeSoLuong: TDBVertGridEh
              Left = 1
              Top = 17
              Width = 528
              Height = 137
              Align = alClient
              AllowedOperations = [alopUpdateEh]
              AllowedSelections = []
              RowCategories.CategoryProps = <>
              PrintService.ColorSchema = pcsFullColorEh
              DataColParams.Font.Charset = DEFAULT_CHARSET
              DataColParams.Font.Color = 8404992
              DataColParams.Font.Height = -13
              DataColParams.Font.Name = 'Tahoma'
              DataColParams.Font.Style = [fsBold]
              DataColParams.MaxRowHeight = 2
              DataColParams.MaxRowLines = 10
              DataColParams.ParentFont = False
              DataColParams.RowHeight = 21
              DataSource = DsDMNV
              DrawGraphicData = True
              DrawMemoText = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              GridLineParams.ColorScheme = glcsThemedEh
              Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
              OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
              ParentFont = False
              RowsDefValues.FitRowHeightToData = True
              SearchPanel.Enabled = True
              SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
              TabOrder = 1
              LabelColWidth = 235
              Rows = <
                item
                  ButtonStyle = cbsDropDown
                  DynProps = <>
                  DropDownBox.Columns = <
                    item
                      FieldName = 'TenBacLuong'
                    end>
                  DropDownBox.ListSource = HrDataMain.DsDM_BACLUONG
                  DropDownBox.ListSourceAutoFilter = True
                  DropDownBox.ListSourceAutoFilterAllColumns = True
                  EditButton.Visible = True
                  EditButton.Width = 20
                  EditButtons = <>
                  FieldName = 'LK_TenBacLuong'
                end
                item
                  Color = clBtnFace
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'HeSoLuong'
                  ReadOnly = True
                end>
              object CbBacLuong: TDbLookupComboboxEh2
                Left = 294
                Top = 371
                Width = 279
                Height = 19
                TabStop = False
                ControlLabel.Width = 4
                ControlLabel.Height = 16
                ControlLabel.Caption = ' '
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                DynProps = <>
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenBacLuong'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 160
                  end
                  item
                    FieldName = 'MaBacLuong'
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ListField = 'TenBacLuong'
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = False
              end
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Th'#244'ng tin kh'#225'c'
        ImageIndex = 4
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 551
          Height = 407
          Align = alClient
          TabOrder = 0
          object PaLienLacKhan: TisPanel
            Left = 0
            Top = 0
            Width = 547
            Height = 182
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 0
            HeaderCaption = ' .: Li'#234'n l'#7841'c kh'#7849'n'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 126
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              547
              182)
            object DBEditEh13: TDBEditEh
              Tag = 999
              Left = 143
              Top = 24
              Width = 195
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 58
              ControlLabel.Height = 16
              ControlLabel.Caption = 'H'#7885' t'#234'n (1)'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe1_HoTen'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object DBEditEh14: TDBEditEh
              Tag = 999
              Left = 403
              Top = 24
              Width = 118
              Height = 22
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 48
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Quan h'#7879
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe1_QuanHe'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object DBEditEh15: TDBEditEh
              Tag = 999
              Left = 143
              Top = 48
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 58
              ControlLabel.Height = 16
              ControlLabel.Caption = #272'i'#7879'n tho'#7841'i'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe1_DienThoai'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
            object wwDBEdit16: TDBEditEh
              Tag = 999
              Left = 143
              Top = 72
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 39
              ControlLabel.Height = 16
              ControlLabel.Caption = #272#7883'a ch'#7881
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe1_DiaChi'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object wwDBEdit17: TDBEditEh
              Tag = 999
              Left = 143
              Top = 103
              Width = 195
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 58
              ControlLabel.Height = 16
              ControlLabel.Caption = 'H'#7885' t'#234'n (2)'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe2_HoTen'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object wwDBEdit18: TDBEditEh
              Tag = 999
              Left = 403
              Top = 103
              Width = 118
              Height = 22
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 48
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Quan h'#7879
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe2_QuanHe'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 6
              Visible = True
            end
            object wwDBEdit19: TDBEditEh
              Tag = 999
              Left = 143
              Top = 127
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 58
              ControlLabel.Height = 16
              ControlLabel.Caption = #272'i'#7879'n tho'#7841'i'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe2_DienThoai'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 7
              Visible = True
            end
            object wwDBEdit20: TDBEditEh
              Tag = 999
              Left = 143
              Top = 151
              Width = 378
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 39
              ControlLabel.Height = 16
              ControlLabel.Caption = #272#7883'a ch'#7881
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LienHe2_DiaChi'
              DataSource = DsDMNV
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 8
              Visible = True
            end
          end
          object PaGhiChu: TisPanel
            Left = 0
            Top = 182
            Width = 547
            Height = 221
            Align = alClient
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: Ghi ch'#250
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 100
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object EdGhichu: TDBMemo
              Left = 1
              Top = 17
              Width = 545
              Height = 203
              Align = alClient
              BorderStyle = bsNone
              DataField = 'GHICHU'
              DataSource = DsDMNV
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object PaList: TPanel
    Left = 0
    Top = 44
    Width = 441
    Height = 626
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GrList: TwwDBGrid2
      Left = 0
      Top = 102
      Width = 441
      Height = 524
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'Manv'#9'15'#9'M'#227' s'#7889#9'F'
        'Tennv'#9'30'#9'H'#7885' t'#234'n'#9'F'
        'GhiChu'#9'30'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsDMNV
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 2
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      OnEnter = CmdRefreshExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
    object PaSearch: TPanel
      Left = 0
      Top = 57
      Width = 441
      Height = 45
      Align = alTop
      TabOrder = 1
      object rgObsolete: TRadioGroup
        Left = 1
        Top = 1
        Width = 436
        Height = 43
        Align = alLeft
        Caption = 'T'#236'nh tr'#7841'ng nh'#226'n vi'#234'n'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          #272'ang l'#224'm vi'#7879'c'
          #272#227' th'#244'i vi'#7879'c'
          'T'#7845't c'#7843)
        TabOrder = 0
      end
    end
    object PaOrg: TisPanel
      Left = 0
      Top = 0
      Width = 441
      Height = 57
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = 16119285
      Padding.Left = 10
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = 'To'#224'n b'#7897
      HeaderColor = 16119285
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clBlack
      HeaderFont.Height = -13
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      DesignSize = (
        441
        57)
      object CbOrg: TfcTreeCombo
        Tag = 1
        Left = 12
        Top = 28
        Width = 413
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        AllowClearKey = True
        BorderStyle = bsNone
        ButtonStyle = cbsDownArrow
        Text = 'CbOrg'
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        DropDownCount = 12
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Images = DataMain.ImageOrg
        Items.StreamVersion = 1
        Items.Data = {00000000}
        Options = []
        ReadOnly = False
        ShowMatchText = True
        Sorted = False
        Style = csDropDownList
        TabOrder = 1
        OnChange = CbOrgChange
        OnExit = CbOrgChange
      end
    end
    object CbMaPhuCap: TwwDBLookupCombo
      Left = 223
      Top = 490
      Width = 130
      Height = 22
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MaPhuCap'#9'10'#9'MaPhuCap'#9'F'
        'TenPhuCap'#9'30'#9'TenPhuCap'#9'F')
      DataField = 'MaPhuCap'
      DataSource = DsAllowance
      LookupTable = HrDataMain.QrDM_HR_PHUCAP
      LookupField = 'MaPhuCap'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbPhuCap: TwwDBLookupCombo
      Left = 223
      Top = 518
      Width = 130
      Height = 22
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TenPhuCap'#9'30'#9'TenPhuCap'#9'F'
        'MaPhuCap'#9'10'#9'MaPhuCap'#9'F')
      DataField = 'MaPhuCap'
      DataSource = DsAllowance
      LookupTable = HrDataMain.QrDM_HR_PHUCAP
      LookupField = 'MaPhuCap'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 4
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbMon: TwwDBComboBox
      Left = 223
      Top = 460
      Width = 58
      Height = 24
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      ShowMatchText = True
      DataField = 'ThangBatDau'
      DataSource = DsAllowance
      DropDownCount = 6
      ItemHeight = 0
      Sorted = True
      TabOrder = 5
      UnboundDataType = wwDefault
    end
    object CbYear: TwwDBComboBox
      Left = 295
      Top = 460
      Width = 58
      Height = 24
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      ShowMatchText = True
      DataField = 'NamBatDau'
      DataSource = DsAllowance
      DropDownCount = 6
      ItemHeight = 0
      Sorted = True
      TabOrder = 6
      UnboundDataType = wwDefault
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 670
    Width = 1008
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 64
    Top = 272
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      OnExecute = CmdEditExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'Danh s'#225'ch nh'#226'n vi'#234'n'
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Hint = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdContact: TAction
      Caption = 'Li'#234'n l'#7841'c'
      Hint = 'Danh s'#225'ch ng'#432#7901'i li'#234'n l'#7841'c'
    end
    object CmdReload: TAction
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdDmtk: TAction
      Hint = 'Danh s'#225'ch t'#224'i kho'#7843'n'
      OnExecute = CmdDmtkExecute
    end
    object CmdThoiviec: TAction
      Category = 'ChiTiet'
      Caption = 'Th'#244'i vi'#7879'c'
      OnExecute = CmdThoiviecExecute
    end
    object CmdNoiCapBHXH: TAction
      Caption = 'N'#417'i c'#7845'p BHXH'
      OnExecute = CmdNoiCapBHXHExecute
    end
    object CmdNoiCapBHYT: TAction
      Caption = 'N'#417'i c'#7845'p BHYT'
      OnExecute = CmdNoiCapBHYTExecute
    end
    object CmdNoiCapCCCD: TAction
      Caption = 'N'#417'i c'#7845'p CCCD'
      OnExecute = CmdNoiCapCCCDExecute
    end
    object CmdHoChieuLoai: TAction
      Caption = 'Lo'#7841'i h'#7897' chi'#7871'u'
      OnExecute = CmdHoChieuLoaiExecute
    end
    object CmdDangTinhTrang: TAction
      Caption = 'T'#236'nh tr'#7841'ng'
      OnExecute = CmdDangTinhTrangExecute
    end
    object CmdDoanTinhTrang: TAction
      Caption = 'T'#236'nh tr'#7841'ng'
      OnExecute = CmdDoanTinhTrangExecute
    end
    object CmdQuaTrinhLamViec: TAction
      Category = 'ChiTiet'
      Caption = 'Qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      OnExecute = CmdQuaTrinhLamViecExecute
    end
    object CmdHopDong: TAction
      Category = 'ChiTiet'
      Caption = 'H'#7907'p '#273#7891'ng lao '#273#7897'ng'
      OnExecute = CmdHopDongExecute
    end
    object CmdKhenThuongKyLuat: TAction
      Category = 'ChiTiet'
      Caption = 'Khen th'#432#7903'ng - k'#7927' lu'#7853't'
      OnExecute = CmdKhenThuongKyLuatExecute
    end
    object CmdQuanHeGiaDinh: TAction
      Category = 'ChiTiet'
      Caption = 'Quan h'#7879' gia '#273#236'nh'
      OnExecute = CmdQuanHeGiaDinhExecute
    end
    object CmdTaiSanThietBi: TAction
      Category = 'ChiTiet'
      Caption = 'Thi'#7871't b'#7883' - T'#224'i s'#7843'n'
      OnExecute = CmdTaiSanThietBiExecute
    end
    object CmdDiCongTac: TAction
      Category = 'ChiTiet'
      Caption = #272'i c'#244'ng t'#225'c'
      OnExecute = CmdDiCongTacExecute
    end
    object CmdKienThucTaiLieu: TAction
      Category = 'ChiTiet'
      Caption = 'Ki'#7871'n th'#7913'c - t'#224'i li'#7879'u'
      OnExecute = CmdKienThucTaiLieuExecute
    end
    object CmdTruocVaoCongTy: TAction
      Category = 'ChiTiet'
      Caption = 'Tr'#432#7899'c khi v'#224'o c'#244'ng ty'
      OnExecute = CmdTruocVaoCongTyExecute
    end
    object CmdPhongBan: TAction
      Category = 'DanhMuc'
      Caption = 'T'#7893' ch'#7913'c ph'#242'ng ban'
      OnExecute = CmdPhongBanExecute
    end
    object CmdChucDanh: TAction
      Category = 'DanhMuc'
      Caption = 'Ch'#7913'c danh - ch'#7913'c v'#7909
      OnExecute = CmdChucDanhExecute
    end
    object CmdNoiLamViec: TAction
      Category = 'DanhMuc'
      Caption = #272#7883'a '#273'i'#7875'm l'#224'm vi'#7879'c'
      OnExecute = CmdNoiLamViecExecute
    end
    object CmdNguonTuyenDung: TAction
      Category = 'DanhMuc'
      Caption = 'Ngu'#7891'n tuy'#7875'n d'#7909'ng'
      OnExecute = CmdNguonTuyenDungExecute
    end
    object CmdNoiDangKyKhamBenh: TAction
      Category = 'DanhMuc'
      Caption = 'N'#417'i '#273#259'ng k'#253' kh'#225'm b'#7879'nh'
      OnExecute = CmdNoiDangKyKhamBenhExecute
    end
    object CmdDelDetail: TAction
      Caption = 'X'#243'a d'#242'ng d'#7919' li'#7879'u'
      OnExecute = CmdDelDetailExecute
    end
    object CmdTaiKhoanTruyCap: TAction
      Category = 'ChiTiet'
      Caption = 'T'#224'i kho'#7843'n truy c'#7853'p d'#7919' li'#7879'u'
      OnExecute = CmdTaiKhoanTruyCapExecute
    end
    object CmdQuocTich: TAction
      Caption = 'Qu'#7889'c t'#7883'ch'
      OnExecute = CmdQuocTichExecute
    end
    object CmdTonGiao: TAction
      Caption = 'T'#244'n gi'#225'o'
      OnExecute = CmdTonGiaoExecute
    end
    object CmdHonNhan: TAction
      Caption = 'H'#244'n nh'#226'n'
      OnExecute = CmdHonNhanExecute
    end
    object CmdDanToc: TAction
      Caption = 'D'#226'n t'#7897'c'
      OnExecute = CmdDanTocExecute
    end
    object CmdVanHoa: TAction
      Caption = 'V'#259'n h'#243'a'
      OnExecute = CmdVanHoaExecute
    end
    object CmdLoaiQuaTrinhLamViec: TAction
      Category = 'DanhMuc'
      Caption = 'Lo'#7841'i qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      OnExecute = CmdLoaiQuaTrinhLamViecExecute
    end
    object CmdLoaiPhuCap: TAction
      Category = 'DanhMuc'
      Caption = 'Lo'#7841'i ph'#7909' c'#7845'p'
      OnExecute = CmdLoaiPhuCapExecute
    end
    object CmdViTriDiaLy: TAction
      Category = 'DanhMuc'
      Caption = 'V'#7883' tr'#237' '#273#7883'a l'#253
      OnExecute = CmdViTriDiaLyExecute
    end
    object CmdThongTinKhac: TAction
      Caption = 'Th'#244'ng tin kh'#225'c'
      OnExecute = CmdThongTinKhacExecute
    end
    object CmdNhomMau: TAction
      OnExecute = CmdNhomMauExecute
    end
    object CmdLienLacKhan: TAction
      Caption = 'Li'#234'n l'#7841'c kh'#7849'n'
    end
    object CmdChiBoDang: TAction
      OnExecute = CmdChiBoDangExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdNgayKyNiem: TAction
      Category = 'ChiTiet'
      Caption = 'C'#225'c ng'#224'y k'#7927' ni'#7879'm'
      OnExecute = CmdNgayKyNiemExecute
    end
    object CmdHinhAnh: TAction
      Caption = 'H'#236'nh '#7843'nh'
      OnExecute = CmdHinhAnhExecute
    end
    object CmdCCCD: TAction
      Caption = 'S'#7889' CCCD/CMND'
      OnExecute = CmdCCCDExecute
    end
    object CmdHoChieu: TAction
      Caption = 'S'#7889' h'#7897' chi'#7871'u'
      OnExecute = CmdHoChieuExecute
    end
    object CmdNewList: TAction
      Caption = #272'.K Ph'#7909' c'#7845'p'
      Hint = 'C'#7853'p nh'#7853't ph'#7909' c'#7845'p h'#224'ng lo'#7841't'
      OnExecute = CmdNewListExecute
    end
    object CmdPrintForm: TAction
      Caption = 'Danh s'#225'ch bi'#7875'u m'#7851'u'
      OnExecute = CmdPrintFormExecute
    end
    object CmdImportExcel: TAction
      Caption = 'Ch'#7885'n file Excel ...'
      Hint = 'Import d'#7919' li'#7879'u t'#7915' file Excel'
      OnExecute = CmdImportExcelExecute
    end
    object CmdSampleExcel: TAction
      Caption = 'T'#7843'i v'#7873' file Excel m'#7851'u'
      OnExecute = CmdSampleExcelExecute
    end
    object CmdLichSuBieuMau: TAction
      Category = 'ChiTiet'
      Caption = 'Bi'#234'n b'#7843'n - H'#7891' S'#417' - Gi'#7845'y t'#7901
      OnExecute = CmdLichSuBieuMauExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDMNV
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MANV'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MADT'
      'TENDT'
      'MST'
      'DCHI'
      'DTHOAI'
      'FAX'
      'TENTK'
      'MATK'
      'TEN_NGANHANG'
      'DC_NGANHANG')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 116
    Top = 272
  end
  object QrDMNV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDMNVBeforeInsert
    AfterInsert = QrDMNVAfterInsert
    BeforePost = QrDMNVBeforePost
    AfterPost = QrDMNVAfterPost
    BeforeDelete = QrDMNVBeforeDelete
    AfterDelete = QrDMNVAfterPost
    AfterScroll = QrDMNVAfterScroll
    OnCalcFields = QrDMNVCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NHANVIEN')
    Left = 40
    Top = 208
    object QrDMNVMANV: TWideStringField
      DisplayLabel = 'ID'
      FieldName = 'Manv'
      OnChange = QrDMNVMANVChange
    end
    object QrDMNVTENNV: TWideStringField
      DisplayLabel = 'H'#7885' t'#234'n'
      FieldName = 'Tennv'
      Size = 200
    end
    object QrDMNVDTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'DienThoai'
      Size = 100
    end
    object QrDMNVDCHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881
      FieldName = 'CuTru_DiaChi'
      Size = 200
    end
    object QrDMNVEMAIL: TWideStringField
      FieldName = 'Email'
      OnValidate = QrDMNVEMAILValidate
      Size = 100
    end
    object QrDMNVCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMNVUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMNVCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMNVUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMNVGHICHU: TWideMemoField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrDMNVNgayThoiViec: TDateTimeField
      DisplayLabel = 'Ng'#224'y ngh'#7881' vi'#7879'c'
      FieldName = 'NgayThoiViec'
      Visible = False
      OnChange = QrDMNVNgayThoiViecChange
    end
    object QrDMNVMaPhongBan: TWideStringField
      DisplayLabel = 'Ph'#242'ng ban'
      FieldName = 'MaPhongBan'
      OnChange = QrDMNVMaPhongBanChange
      Size = 15
    end
    object QrDMNVNGAYSINH: TDateTimeField
      DisplayLabel = 'Ng'#224'y sinh'
      FieldName = 'NgaySinh'
      Visible = False
      OnChange = QrDMNVNGAYSINHChange
    end
    object QrDMNVMaChucDanh: TWideStringField
      DisplayLabel = 'Ch'#7913'c danh'
      FieldName = 'MaChucDanh'
      OnChange = QrDMNVMaChucDanhChange
      Size = 15
    end
    object QrDMNVLUONG_COBAN: TFloatField
      DisplayLabel = 'L'#432#417'ng ch'#237'nh'
      FieldName = 'LuongChinh'
    end
    object QrDMNVCOMAT_BANGCONG: TBooleanField
      FieldName = 'Co_BangCong'
    end
    object QrDMNVCOMAT_BANGLUONG: TBooleanField
      FieldName = 'Co_BangLuong'
    end
    object QrDMNVMACC: TWideStringField
      FieldName = 'MaCC'
      Size = 15
    end
    object QrDMNVNGAY_VAOLAM: TDateTimeField
      DisplayLabel = 'Ng'#224'y v'#224'o l'#224'm'
      FieldName = 'NgayVaoLam'
      OnChange = QrDMNVNGAY_VAOLAMChange
    end
    object QrDMNVMA_NHOMLV: TWideStringField
      FieldName = 'MaNhomLV'
      OnChange = QrDMNVMA_NHOMLVChange
      Size = 15
    end
    object QrDMNVMST: TWideStringField
      FieldName = 'MaSoThue'
    end
    object QrDMNVMABP: TWideStringField
      FieldName = 'MaBoPhan'
      OnChange = QrDMNVMABPChange
      Size = 60
    end
    object QrDMNVMA_NOI_LAMVIEC: TWideStringField
      FieldName = 'MaNoiLV'
    end
    object QrDMNVLUONG_BAOHIEM: TFloatField
      FieldName = 'LuongBaoHiem'
    end
    object QrDMNVPTTT: TWideStringField
      FieldName = 'PTTT'
    end
    object QrDMNVNGAY_LAMDON_THOIVIEC: TDateTimeField
      FieldName = 'NgayDangKyThoiViec'
    end
    object QrDMNVNGAY_TRATAISAN: TDateTimeField
      FieldName = 'NgayTraTaiSan'
    end
    object QrDMNVNGAY_QUYETTOAN_LUONG: TDateTimeField
      FieldName = 'NgayQuyetToan'
    end
    object QrDMNVGHICHU_THOIVIEC: TWideStringField
      FieldName = 'GhiChu_ThoiViec'
      Size = 200
    end
    object QrDMNVGIOITINH: TIntegerField
      FieldName = 'GioiTinh'
    end
    object QrDMNVNGAY_THAMNIEN: TDateTimeField
      DisplayLabel = 'Ng'#224'y t'#237'nh th'#226'm ni'#234'n'
      FieldName = 'NgayThamNien'
    end
    object QrDMNVBIDANH: TWideStringField
      FieldName = 'BiDanh'
      Size = 200
    end
    object QrDMNVNAMSINH: TIntegerField
      FieldName = 'NamSinh'
    end
    object QrDMNVNOISINH: TWideStringField
      FieldName = 'NoiSinh_DiaChi'
      Size = 200
    end
    object QrDMNVQUOCTICH: TWideStringField
      FieldName = 'QuocTich'
    end
    object QrDMNVDANTOC: TWideStringField
      FieldName = 'DanToc'
    end
    object QrDMNVTONGIAO: TWideStringField
      FieldName = 'TonGiao'
    end
    object QrDMNVVANHOA: TWideStringField
      FieldName = 'VanHoa'
    end
    object QrDMNVDCHI_THUONGTRU: TWideStringField
      FieldName = 'ThuongTru_DiaChi'
      Size = 200
    end
    object QrDMNVHO: TWideStringField
      DisplayLabel = 'H'#7885
      FieldName = 'Ho'
      OnChange = QrDMNVHOChange
      Size = 100
    end
    object QrDMNVTEN: TWideStringField
      DisplayLabel = 'T'#234'n'
      FieldName = 'Ten'
      OnChange = QrDMNVHOChange
      Size = 100
    end
    object QrDMNVHuuTriNam: TIntegerField
      FieldName = 'HuuTriNam'
      OnChange = QrDMNVHuuTriNamChange
    end
    object QrDMNVHuuTriThang: TIntegerField
      FieldName = 'HuuTriThang'
      OnChange = QrDMNVHuuTriNamChange
    end
    object QrDMNVNgayHuuTri: TDateTimeField
      FieldName = 'NgayHuuTri'
    end
    object QrDMNVNoiSinh_MaTinh: TWideStringField
      FieldName = 'NoiSinh_MaTinh'
      Size = 11
    end
    object QrDMNVHonNhan: TWideStringField
      FieldName = 'HonNhan'
      Size = 70
    end
    object QrDMNVChuyenMon: TWideStringField
      FieldName = 'ChuyenMon'
      Size = 200
    end
    object QrDMNVChuyenNganh: TWideStringField
      FieldName = 'ChuyenNganh'
      Size = 200
    end
    object QrDMNVCuTru_MaTinh: TWideStringField
      FieldName = 'CuTru_MaTinh'
      OnChange = QrDMNVCuTru_MaTinhChange
      Size = 11
    end
    object QrDMNVCuTru_MaHuyen: TWideStringField
      FieldName = 'CuTru_MaHuyen'
      OnChange = QrDMNVCuTru_MaHuyenChange
      Size = 17
    end
    object QrDMNVThuongTru_MaTinh: TWideStringField
      FieldName = 'ThuongTru_MaTinh'
      OnChange = QrDMNVThuongTru_MaTinhChange
      Size = 11
    end
    object QrDMNVThuongTru_MaHuyen: TWideStringField
      FieldName = 'ThuongTru_MaHuyen'
      OnChange = QrDMNVThuongTru_MaHuyenChange
      Size = 17
    end
    object QrDMNVEmailCaNhan: TWideStringField
      FieldName = 'EmailCaNhan'
      OnValidate = QrDMNVEMAILValidate
      Size = 100
    end
    object QrDMNVLienHe1_HoTen: TWideStringField
      FieldName = 'LienHe1_HoTen'
      Size = 100
    end
    object QrDMNVLienHe1_QuanHe: TWideStringField
      FieldName = 'LienHe1_QuanHe'
    end
    object QrDMNVLienHe1_DienThoai: TWideStringField
      FieldName = 'LienHe1_DienThoai'
      Size = 100
    end
    object QrDMNVLienHe1_DiaChi: TWideStringField
      FieldName = 'LienHe1_DiaChi'
      Size = 200
    end
    object QrDMNVLienHe2_HoTen: TWideStringField
      FieldName = 'LienHe2_HoTen'
      Size = 100
    end
    object QrDMNVLienHe2_QuanHe: TWideStringField
      FieldName = 'LienHe2_QuanHe'
    end
    object QrDMNVLienHe2_DienThoai: TWideStringField
      FieldName = 'LienHe2_DienThoai'
      Size = 100
    end
    object QrDMNVLienHe2_DiaChi: TWideStringField
      FieldName = 'LienHe2_DiaChi'
      Size = 200
    end
    object QrDMNVDangVien_NgayThamGia: TDateTimeField
      FieldName = 'DangVien_NgayThamGia'
    end
    object QrDMNVDangVien_NgayChinhThuc: TDateTimeField
      FieldName = 'DangVien_NgayChinhThuc'
    end
    object QrDMNVDangVien_TinhTrang: TWideStringField
      FieldName = 'DangVien_TinhTrang'
      Size = 70
    end
    object QrDMNVDangVien_GhiChu: TWideStringField
      FieldName = 'DangVien_GhiChu'
      Size = 200
    end
    object QrDMNVDoanVien_NgayThamGia: TDateTimeField
      FieldName = 'DoanVien_NgayThamGia'
    end
    object QrDMNVDoanVien_TinhTrang: TWideStringField
      FieldName = 'DoanVien_TinhTrang'
      Size = 70
    end
    object QrDMNVCongDoan_NgayThamGia: TDateTimeField
      FieldName = 'CongDoan_NgayThamGia'
    end
    object QrDMNVCongDoan_Phi: TFloatField
      FieldName = 'CongDoan_Phi'
    end
    object QrDMNVMaChiNhanh: TWideStringField
      FieldName = 'MaChiNhanh'
    end
    object QrDMNVMaNguonTD: TWideStringField
      FieldName = 'MaNguonTD'
    end
    object QrDMNVNgayBatDauThuViec: TDateTimeField
      FieldName = 'NgayBatDauThuViec'
    end
    object QrDMNVNgayKetThucThuViec: TDateTimeField
      FieldName = 'NgayKetThucThuViec'
    end
    object QrDMNVMaQTLV_VaoLam: TWideStringField
      FieldName = 'MaQTLV_VaoLam'
      OnChange = QrDMNVMaQTLV_VaoLamChange
    end
    object QrDMNVNgayBienChe: TDateTimeField
      FieldName = 'NgayBienChe'
    end
    object QrDMNVDoiTuong: TIntegerField
      DisplayLabel = #272#7889'i t'#432#7907'ng'
      FieldName = 'DoiTuong'
    end
    object QrDMNVMaBacLuong: TWideStringField
      FieldName = 'MaBacLuong'
      OnChange = QrDMNVMaBacLuongChange
    end
    object QrDMNVBacLuong: TIntegerField
      FieldName = 'BacLuong'
      OnChange = QrDMNVMaBacLuongChange
    end
    object QrDMNVNgayBacLuong: TDateTimeField
      FieldName = 'NgayBacLuong'
    end
    object QrDMNVBHXH_So: TWideStringField
      FieldName = 'BHXH_So'
    end
    object QrDMNVBHXH_NgayCap: TDateTimeField
      FieldName = 'BHXH_NgayCap'
    end
    object QrDMNVBHXH_NoiCap: TWideStringField
      FieldName = 'BHXH_NoiCap'
      Size = 70
    end
    object QrDMNVBHYT_So: TWideStringField
      FieldName = 'BHYT_So'
    end
    object QrDMNVBHYT_NgayBatDau: TDateTimeField
      FieldName = 'BHYT_NgayBatDau'
    end
    object QrDMNVBHYT_NgayKetThuc: TDateTimeField
      FieldName = 'BHYT_NgayKetThuc'
    end
    object QrDMNVBHYT_NoiCap: TWideStringField
      FieldName = 'BHYT_NoiCap'
      Size = 200
    end
    object QrDMNVMaKCB: TWideStringField
      FieldName = 'MaKCB'
    end
    object QrDMNVTinhTrang: TIntegerField
      FieldName = 'TinhTrang'
    end
    object QrDMNVMaQTLV_ThoiViec: TWideStringField
      FieldName = 'MaQTLV_ThoiViec'
      OnChange = QrDMNVMaQTLV_ThoiViecChange
    end
    object QrDMNVLK_XacNhan_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrDMNVLuongNgay: TFloatField
      FieldName = 'LuongNgay'
    end
    object QrDMNVLuongGio: TFloatField
      FieldName = 'LuongGio'
    end
    object QrDMNVCo_BHXH: TBooleanField
      FieldName = 'Co_BHXH'
    end
    object QrDMNVCo_BHYT: TBooleanField
      FieldName = 'Co_BHYT'
    end
    object QrDMNVCo_BHTN: TBooleanField
      FieldName = 'Co_BHTN'
    end
    object QrDMNVCo_ChuyenKhoan: TBooleanField
      FieldName = 'Co_ChuyenKhoan'
    end
    object QrDMNVLK_TenPhongBan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'TenPhongBan'
      KeyFields = 'MaPhongBan'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenBoPhan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBoPhan'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'TenBoPhan'
      KeyFields = 'MaBoPhan'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenChucDanh: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'TenChucDanh'
      KeyFields = 'MaChucDanh'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenNoiLV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNoiLV'
      LookupDataSet = HrDataMain.QrDM_NOILV
      LookupKeyFields = 'MaNoiLV'
      LookupResultField = 'TenNoiLV'
      KeyFields = 'MaNoiLV'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenNhomLV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhomLV'
      LookupDataSet = HrDataMain.QrDM_NHOMLV
      LookupKeyFields = 'MaNhomLV'
      LookupResultField = 'TenNhomLV'
      KeyFields = 'MaNhomLV'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenDoiTuong: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenDoiTuong'
      LookupDataSet = HrDataMain.QrV_HR_NHANVIEN_DOITUONG
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'DoiTuong'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenNguonTD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNguonTD'
      LookupDataSet = HrDataMain.QrDM_NGUONTUYENDUNG
      LookupKeyFields = 'MaNguonTD'
      LookupResultField = 'TenNguonTD'
      KeyFields = 'MaNguonTD'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_MaNH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNH'
      LookupDataSet = QrTaiKhoanNV
      LookupKeyFields = 'Idx'
      LookupResultField = 'MaNH'
      KeyFields = 'MaTK_Idx'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_MaNH_CN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNH_CN'
      LookupDataSet = QrTaiKhoanNV
      LookupKeyFields = 'Idx'
      LookupResultField = 'MaNH_CN'
      KeyFields = 'MaTK_Idx'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenNH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNH'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'LK_MaNH'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TenNH_CN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNH_CN'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MANH_CN'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'LK_MaNH_CN'
      Size = 200
      Lookup = True
    end
    object QrDMNVBHYT_Ngay5Nam: TDateTimeField
      FieldName = 'BHYT_Ngay5Nam'
    end
    object QrDMNVCuTru_MaXa: TWideStringField
      FieldName = 'CuTru_MaXa'
      Size = 23
    end
    object QrDMNVThuongTru_MaXa: TWideStringField
      FieldName = 'ThuongTru_MaXa'
      Size = 23
    end
    object QrDMNVMaTK: TWideStringField
      FieldName = 'MaTK'
      Size = 30
    end
    object QrDMNVNhomMau: TWideStringField
      FieldName = 'NhomMau'
      Size = 70
    end
    object QrDMNVTinhTrangSucKhoe: TWideStringField
      FieldName = 'TinhTrangSucKhoe'
      Size = 100
    end
    object QrDMNVCanNang: TFloatField
      FieldName = 'CanNang'
    end
    object QrDMNVDangVien_ChiBo: TWideStringField
      FieldName = 'DangVien_ChiBo'
      Size = 70
    end
    object QrDMNVCapUy: TWideStringField
      FieldName = 'CapUy'
      Size = 100
    end
    object QrDMNVCapUyKiem: TWideStringField
      FieldName = 'CapUyKiem'
      Size = 100
    end
    object QrDMNVChucVuDangVien: TWideStringField
      FieldName = 'ChucVuDangVien'
      Size = 100
    end
    object QrDMNVChucVuChinhQuyen: TWideStringField
      FieldName = 'ChucVuChinhQuyen'
      Size = 100
    end
    object QrDMNVNamQuyHoach: TIntegerField
      FieldName = 'NamQuyHoach'
    end
    object QrDMNVNamQuyHoachBoSung: TIntegerField
      FieldName = 'NamQuyHoachBoSung'
    end
    object QrDMNVBoNhiem_MaChucDanh: TWideStringField
      FieldName = 'BoNhiem_MaChucDanh'
    end
    object QrDMNVBoNhiem_NgayKetThuc: TDateTimeField
      FieldName = 'BoNhiem_NgayKetThuc'
    end
    object QrDMNVBietPhai_MaChucDanh: TWideStringField
      FieldName = 'BietPhai_MaChucDanh'
    end
    object QrDMNVBietPhai_NgayKetThuc: TDateTimeField
      FieldName = 'BietPhai_NgayKetThuc'
    end
    object QrDMNVPhepNam_LuatDinh: TFloatField
      FieldName = 'PhepNam_LuatDinh'
    end
    object QrDMNVPhepNam_ChucDanh: TFloatField
      FieldName = 'PhepNam_ChucDanh'
    end
    object QrDMNVPhepNam_ThamNien: TFloatField
      FieldName = 'PhepNam_ThamNien'
    end
    object QrDMNVXacNhan_Manv: TWideStringField
      FieldName = 'XacNhan_Manv'
      OnChange = QrDMNVXacNhan_ManvChange
    end
    object QrDMNVHeSoLuong: TFloatField
      FieldName = 'HeSoLuong'
    end
    object QrDMNVHeSoPhuCap: TFloatField
      FieldName = 'HeSoPhuCap'
    end
    object QrDMNVTyLeLuongThuViec: TFloatField
      FieldName = 'TyLeLuongThuViec'
    end
    object QrDMNVLK_XetDuyet_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NguoiDuyet_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrDMNVCongViecChinh: TWideStringField
      FieldName = 'CongViecChinh'
      Size = 100
    end
    object QrDMNVThongTinKhac: TWideMemoField
      FieldName = 'ThongTinKhac'
      BlobType = ftWideMemo
    end
    object QrDMNVWebsite: TWideStringField
      FieldName = 'Website'
      Size = 100
    end
    object QrDMNVLK_TenBacLuong: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBacLuong'
      LookupDataSet = HrDataMain.QrDM_BACLUONG
      LookupKeyFields = 'MaBacLuong'
      LookupResultField = 'TenBacLuong'
      KeyFields = 'MaBacLuong'
      Size = 200
      Lookup = True
    end
    object QrDMNVNgayPhongVan: TDateTimeField
      FieldName = 'NgayPhongVan'
    end
    object QrDMNVNgayKiemTraKyNang: TDateTimeField
      FieldName = 'NgayKiemTraKyNang'
    end
    object QrDMNVCALC_TongPhepNam: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TongPhepNam'
      Calculated = True
    end
    object QrDMNVCo_PhepNamMotThang: TBooleanField
      FieldName = 'Co_PhepNamMotThang'
    end
    object QrDMNVLK_PhepNam_LuatDinh: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_LuatDinh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_LuatDinh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrDMNVLK_PhepNam_ChucDanh: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ChucDanh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrDMNVLK_PhepNam_ThamNien: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ThamNien'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ThamNien'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrDMNVLK_Co_PhepNamMotThang: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_PhepNamMotThang'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'Co_PhepNamMotThang'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrDMNVChieuCao: TFloatField
      FieldName = 'ChieuCao'
    end
    object QrDMNVLK_MaBoPhan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaBoPhan_BanDau'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'Ma'
      KeyFields = 'MaBoPhan'
      Lookup = True
    end
    object QrDMNVLK_MaPhongBan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'Ma'
      KeyFields = 'MaPhongBan'
      Lookup = True
    end
    object QrDMNVBoNhiem_NgayBatDau: TDateTimeField
      FieldName = 'BoNhiem_NgayBatDau'
    end
    object QrDMNVBietPhai_NgayBatDau: TDateTimeField
      FieldName = 'BietPhai_NgayBatDau'
    end
    object QrDMNVXetDuyet_Manv: TWideStringField
      FieldName = 'XetDuyet_Manv'
      OnChange = QrDMNVXetDuyet_ManvChange
    end
    object QrDMNVNgayVaoLamChinhThuc: TDateTimeField
      FieldName = 'NgayVaoLamChinhThuc'
    end
    object QrDMNVCo_MacDinhCong: TBooleanField
      FieldName = 'Co_MacDinhCong'
    end
    object QrDMNVCo_CongDoan: TBooleanField
      FieldName = 'Co_CongDoan'
    end
    object QrDMNVManvQL: TWideStringField
      DisplayLabel = 'M'#227' nh'#226'n vi'#234'n'
      FieldName = 'ManvQL'
    end
    object QrDMNVNgayBatDauHocViec: TDateTimeField
      FieldName = 'NgayBatDauHocViec'
    end
    object QrDMNVNgayKetThucHocViec: TDateTimeField
      FieldName = 'NgayKetThucHocViec'
    end
    object QrDMNVLK_XetDuyet_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrDMNVLK_XacNhan_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrDMNVLK_UserName: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UserName'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'Manv'
      LookupResultField = 'UserName'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrDMNVLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrDMNVLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrDMNVLK_CCCD_GhiChu: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_GhiChu'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_CCCD
      LookupKeyFields = 'Idx'
      LookupResultField = 'GhiChu'
      KeyFields = 'CCCD_Idx'
      Lookup = True
    end
    object QrDMNVLK_CCCD_MaNoiCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_MaNoiCap'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_CCCD
      LookupKeyFields = 'Idx'
      LookupResultField = 'MaNoiCap'
      KeyFields = 'CCCD_Idx'
      Lookup = True
    end
    object QrDMNVLK_CCCD_NgayCap: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_NgayCap'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_CCCD
      LookupKeyFields = 'Idx'
      LookupResultField = 'NgayCap'
      KeyFields = 'CCCD_Idx'
      Lookup = True
    end
    object QrDMNVLK_CCCD_TenNoiCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_TenNoiCap'
      LookupDataSet = HrDataMain.QrV_HR_CCCD_NOICAP
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LK_CCCD_MaNoiCap'
      Lookup = True
    end
    object QrDMNVLK_CCCD_MaNoiCap_Ma: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_MaNoiCap_Ma'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_CCCD
      LookupKeyFields = 'Idx'
      LookupResultField = 'MaNoiCap_Ma'
      KeyFields = 'CCCD_Idx'
      Lookup = True
    end
    object QrDMNVLK_HoChieu_NoiCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HoChieu_NoiCap'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU
      LookupKeyFields = 'Idx'
      LookupResultField = 'NoiCap'
      KeyFields = 'HoChieu_Idx'
      Lookup = True
    end
    object QrDMNVLK_HoChieu_NgayCap: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HoChieu_NgayCap'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU
      LookupKeyFields = 'Idx'
      LookupResultField = 'NgayCap'
      KeyFields = 'HoChieu_Idx'
      Lookup = True
    end
    object QrDMNVLK_HoChieu_NgayHetHan: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HoChieu_NgayHetHan'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU
      LookupKeyFields = 'Idx'
      LookupResultField = 'NgayHetHan'
      KeyFields = 'HoChieu_Idx'
      Lookup = True
    end
    object QrDMNVLK_CCCD_GiayTo_TenNhom: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CCCD_GiayTo_TenNhom'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_CCCD
      LookupKeyFields = 'Idx'
      LookupResultField = 'GiayTo_TenNhom'
      KeyFields = 'CCCD_Idx'
      Lookup = True
    end
    object QrDMNVLK_HoChieu_GiayTo_TenNhom: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HoChieu_GiayTo_TenNhom'
      LookupDataSet = HrDataMain.QrV_HR_DM_NHANVIEN_HOCHIEU
      LookupKeyFields = 'Idx'
      LookupResultField = 'GiayTo_TenNhom'
      KeyFields = 'HoChieu_Idx'
      Lookup = True
    end
    object QrDMNVCCCD_Idx: TIntegerField
      FieldName = 'CCCD_Idx'
    end
    object QrDMNVHoChieu_Idx: TIntegerField
      FieldName = 'HoChieu_Idx'
    end
    object QrDMNVMaTK_Idx: TIntegerField
      FieldName = 'MaTK_Idx'
    end
    object QrDMNVIdxQTLV_VaoLam: TIntegerField
      FieldName = 'IdxQTLV_VaoLam'
    end
    object QrDMNVIdxQTLV_ThoiViec: TIntegerField
      FieldName = 'IdxQTLV_ThoiViec'
    end
    object QrDMNVIdxQTLV: TIntegerField
      FieldName = 'IdxQTLV'
    end
  end
  object DsDMNV: TDataSource
    AutoEdit = False
    DataSet = QrDMNV
    Left = 40
    Top = 228
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 92
    Top = 272
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 144
    Top = 272
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Khngspxp2: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ALLOC_MANV: TADOCommand
    CommandText = 'ALLOC_MANV;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LEN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CODE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 96
    Top = 200
  end
  object PopMore: TAdvPopupMenu
    AutoHotkeys = maManual
    MenuAnimation = [maRightToLeft]
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 228
    Top = 272
    object Qutrnhlmvic1: TMenuItem
      Action = CmdQuaTrinhLamViec
    end
    object Hpnglaong1: TMenuItem
      Action = CmdHopDong
    end
    object Khenthngklut1: TMenuItem
      Action = CmdKhenThuongKyLuat
    end
    object Gitisnthitb1: TMenuItem
      Action = CmdTaiSanThietBi
    end
    object icngtc1: TMenuItem
      Action = CmdDiCongTac
    end
    object Ccngyknim1: TMenuItem
      Action = CmdNgayKyNiem
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Linlc1: TMenuItem
      Action = CmdQuanHeGiaDinh
    end
    object Kinthctiliu1: TMenuItem
      Action = CmdKienThucTaiLieu
    end
    object rckhivocngty1: TMenuItem
      Action = CmdTruocVaoCongTy
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object hivic1: TMenuItem
      Action = CmdThoiviec
    end
    object BinbnHSGiyt1: TMenuItem
      Action = CmdLichSuBieuMau
    end
    object ikhontruycpdliu1: TMenuItem
      Action = CmdTaiKhoanTruyCap
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 144
    Top = 316
  end
  object PopDanhMuc: TAdvPopupMenu
    AutoHotkeys = maManual
    MenuAnimation = [maRightToLeft]
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 276
    Top = 272
    object Phngban1: TMenuItem
      Action = CmdPhongBan
    end
    object Chcdanhchcv1: TMenuItem
      Action = CmdChucDanh
    end
    object Loiqutrnhlmvic1: TMenuItem
      Action = CmdLoaiQuaTrinhLamViec
    end
    object Loiphcp1: TMenuItem
      Action = CmdLoaiPhuCap
    end
    object aimlmvic1: TMenuItem
      Action = CmdNoiLamViec
    end
    object Ningkkhmbnh1: TMenuItem
      Action = CmdNoiDangKyKhamBenh
    end
    object Nguntuyndng1: TMenuItem
      Action = CmdNguonTuyenDung
    end
    object Vtral1: TMenuItem
      Action = CmdViTriDiaLy
    end
  end
  object QrHuyen: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_DL_HUYEN')
    Left = 280
    Top = 424
  end
  object QrSite: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_CHINHANH'
      'order by MaChiNhanh')
    Left = 52
    Top = 396
  end
  object QrDep: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsSite
    Parameters = <
      item
        Name = 'MaChiNhanh'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DM_PHONGBAN'
      ' where  MaChiNhanh = :MaChiNhanh'
      'order by MaPhongBan')
    Left = 80
    Top = 396
  end
  object QrSec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DM_BOPHAN'
      'order by MaBoPhan')
    Left = 108
    Top = 396
  end
  object DsSite: TDataSource
    DataSet = QrSite
    Left = 52
    Top = 424
  end
  object DsDep: TDataSource
    DataSet = QrDep
    Left = 80
    Top = 424
  end
  object DsSec: TDataSource
    DataSet = QrSec
    Left = 108
    Top = 424
  end
  object QrAllowance: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrAllowanceBeforeOpen
    BeforeInsert = QrAllowanceBeforeInsert
    AfterInsert = QrAllowanceAfterInsert
    BeforeEdit = QrAllowanceBeforeEdit
    BeforePost = QrAllowanceBeforePost
    BeforeDelete = QrAllowanceBeforeDelete
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_PHUCAP'
      ' where'#9'Manv = :Manv')
    Left = 52
    Top = 468
    object QrAllowanceManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrAllowanceMaPhuCap: TWideStringField
      FieldName = 'MaPhuCap'
      OnChange = QrAllowanceMaPhuCapChange
    end
    object QrAllowanceCo_SuDung: TBooleanField
      FieldName = 'Co_SuDung'
    end
    object QrAllowanceNgayXetDuyet: TDateTimeField
      FieldName = 'NgayXetDuyet'
    end
    object QrAllowanceThangBatDau: TIntegerField
      FieldName = 'ThangBatDau'
      OnChange = QrAllowanceSoThangChange
    end
    object QrAllowanceNamBatDau: TIntegerField
      FieldName = 'NamBatDau'
      OnChange = QrAllowanceSoThangChange
    end
    object QrAllowanceThangKetThuc: TIntegerField
      FieldName = 'ThangKetThuc'
    end
    object QrAllowanceNamKetThuc: TIntegerField
      FieldName = 'NamKetThuc'
    end
    object QrAllowanceCo_GioiHan: TBooleanField
      FieldName = 'Co_GioiHan'
      OnChange = QrAllowanceSoThangChange
    end
    object QrAllowanceSoThang: TFloatField
      FieldName = 'SoThang'
      OnChange = QrAllowanceSoThangChange
    end
    object QrAllowanceSoTien: TFloatField
      FieldName = 'SoTien'
    end
    object QrAllowanceGhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrAllowanceCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrAllowanceUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrAllowanceCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrAllowanceUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrAllowanceLK_TenPhuCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhuCap'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'TenPhuCap'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrAllowanceLK_PhuCapLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PhuCapLoai'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'PhuCapLoai'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrAllowanceLK_TenPhanLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhanLoai'
      LookupDataSet = HrDataMain.QrV_HR_PHUCAP_LOAI
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LK_PhuCapLoai'
      Lookup = True
    end
    object QrAllowanceNgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
    end
    object QrAllowanceCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrAllowanceLK_Co_ThueTNCN: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_ThueTNCN'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'Co_ThueTNCN'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrAllowanceIDX: TAutoIncField
      FieldName = 'IDX'
      ReadOnly = True
    end
  end
  object DsAllowance: TDataSource
    DataSet = QrAllowance
    Left = 52
    Top = 496
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 264
    Top = 352
    object Xachitit1: TMenuItem
      Action = CmdDelDetail
    end
  end
  object QrTaiKhoanNV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.*, b.TENNH'
      '  from'#9'HR_DM_TAIKHOAN a'
      '  inner join DM_NGANHANG b on b.MANH = a.MaNH')
    Left = 100
    Top = 468
  end
  object DsTaiKhoanNV: TDataSource
    DataSet = QrTaiKhoanNV
    Left = 100
    Top = 496
  end
  object QrPhuongXa: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_DL_XA')
    Left = 324
    Top = 422
  end
  object DsHuyen: TDataSource
    DataSet = QrHuyen
    Left = 284
    Top = 464
  end
  object DsPhuongXa: TDataSource
    DataSet = QrPhuongXa
    Left = 324
    Top = 464
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 138
    Top = 352
  end
  object spHR_LICHSU_THIETBI_Invalid_NgayTraTaiSan: TADOCommand
    CommandText = 'spHR_LICHSU_THIETBI_Invalid_NgayTraTaiSan;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@pMSG'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 184
    Top = 208
  end
  object spHR_TAIKHOAN_NGANHANG_NGAYSUDUNGMAX: TADOCommand
    CommandText = 'spHR_TAIKHOAN_NGANHANG_NGAYSUDUNGMAX;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftGuid
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 192
    Top = 168
  end
  object QrV_HR_DM_NHANVIEN_CCCD_MANV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsDMNV
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DM_NHANVIEN_CCCD'
      'where Manv=:Manv')
    Left = 300
    Top = 566
  end
  object DsV_HR_DM_NHANVIEN_CCCD_MANV: TDataSource
    DataSet = QrV_HR_DM_NHANVIEN_CCCD_MANV
    Left = 300
    Top = 608
  end
  object QrV_HR_DM_NHANVIEN_HOCHIEU_MANV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsDMNV
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9' V_HR_DM_NHANVIEN_HOCHIEU'
      'where Manv=:Manv')
    Left = 132
    Top = 566
  end
  object DsV_HR_DM_NHANVIEN_HOCHIEU_MANV: TDataSource
    DataSet = QrV_HR_DM_NHANVIEN_HOCHIEU_MANV
    Left = 132
    Top = 608
  end
  object spHR_CCCD_NGAYCAPMAX: TADOCommand
    CommandText = 'spHR_CCCD_NGAYCAPMAX;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftGuid
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 200
    Top = 392
  end
  object QrTaiKhoanNV_MANV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsDMNV
    Parameters = <
      item
        Name = 'Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'a.*, b.TENNH'
      '  from'#9'HR_DM_TAIKHOAN a'
      '  inner join DM_NGANHANG b on b.MANH = a.MaNH'
      'where a.Manv=:Manv')
    Left = 164
    Top = 468
  end
  object DsTaiKhoanNV_MANV: TDataSource
    DataSet = QrTaiKhoanNV_MANV
    Left = 164
    Top = 496
  end
  object PopIn: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 328
    Top = 344
    object MenuItem1: TMenuItem
      Action = CmdPrint
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object MenuItem2: TMenuItem
      Action = CmdPrintForm
    end
  end
  object PopImport: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 336
    Top = 264
    object MenuItem6: TMenuItem
      Action = CmdImportExcel
    end
    object MenuItem7: TMenuItem
      Tag = 1
      Action = CmdSampleExcel
    end
  end
  object spIMP_HR_DM_NHANVIEN_DeleteList: TADOCommand
    CommandText = 'spIMP_HR_DM_NHANVIEN_DeleteList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIsError'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 220
    Top = 316
  end
end
