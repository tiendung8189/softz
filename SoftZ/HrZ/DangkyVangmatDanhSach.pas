﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyVangmatDanhSach;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, Vcl.Buttons, Vcl.Graphics,
  wwDialog, wwfltdlg, wwFltDlg2;

type
  TFrmDangkyVangmatDanhSach = class(TForm)
    GrList: TwwDBGrid2;
    DsList: TDataSource;
    QrList: TADOQuery;
    ActionList: TActionList;
    CmdRegister: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Panel3: TPanel;
    BtnRegister: TBitBtn;
    BitBtn2: TBitBtn;
    spIMP_HR_DANGKY_VANGMAT_InsertData: TADOCommand;
    spIMP_HR_DANGKY_VANGMAT_DeleteList: TADOCommand;
    spIMP_HR_DANGKY_VANGMAT_Check: TADOCommand;
    Filter: TwwFilterDialog2;
    CmdDel: TAction;
    Xadng1: TMenuItem;
    CmdDelRecordError: TAction;
    Xaccdngangli1: TMenuItem;
    CHECK_RECORD_ERROR: TADOCommand;
    QrListGhiChu: TWideMemoField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListXacNhan_TenTinhTrang: TWideStringField;
    QrListXetDuyet_TenTinhTrang: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_Tennv: TWideStringField;
    QrListLK_XacNhan_Tennv: TWideStringField;
    QrTinhTrang_XetDuyet: TADOQuery;
    DsTinhTrang_XetDuyet: TDataSource;
    QrTinhTrang_XacNhan: TADOQuery;
    DsTinhTrang_XacNhan: TDataSource;
    QrListLK_XacNhan_TenTinhTrang: TWideStringField;
    QrListManv: TWideStringField;
    QrListManvQL: TWideStringField;
    QrListTennv: TWideStringField;
    QrListTuNgay: TDateTimeField;
    QrListDenNgay: TDateTimeField;
    QrListSoNgay: TFloatField;
    QrListXetDuyet_Manv: TWideStringField;
    QrListXacNhan_Manv: TWideStringField;
    QrListTenVangMat: TWideStringField;
    QrListXacNhan_TinhTrang: TIntegerField;
    QrListXacNhan_Tennv: TWideStringField;
    QrListXetDuyet_TinhTrang: TIntegerField;
    QrListXetDuyet_Tennv: TWideStringField;
    QrListMaVangMat: TWideStringField;
    QrListXetDuyet_ManvQL: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
  private
    mTransNo, countError: Integer;
    mCanEdit, mDelete, isFunctionApproved: Boolean;
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; pTransNo: Integer) : Boolean;
  end;

var
  FrmDangkyVangmatDanhSach: TFrmDangkyVangmatDanhSach;

const
    TABLE_NAME = 'IMP_HR_DANGKY_VANGMAT';
    FORM_CODE = 'IMP_HR_DANGKY_VANGMAT';
implementation

uses
	Rights, isDb, ExCommon, isLib, GuidEx, isCommon, isMsg, MainData, HrData;


{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';

function TFrmDangkyVangmatDanhSach.Execute;
begin
    mCanEdit := rCanEdit(r);
   	mTransNo := pTransNo;
    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    CloseDataSets([QrList]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    AddAllFields(QrList, TABLE_NAME);

    isFunctionApproved :=  GetFuncState('HR_VANGMAT_DUYET');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.FormShow(Sender: TObject);
begin
  
    with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_LYDO_VANGMAT]);

	OpenDataSets([QrList, QrTinhTrang_XacNhan, QrTinhTrang_XetDuyet]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;

    if not isFunctionApproved then
        grRemoveFields(GrList, ['XetDuyet_Manv', 'XetDuyet_ManvQL', 'XetDuyet_Tennv',
        'XetDuyet_TinhTrang', 'XetDuyet_TenTinhTrang']);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdDelRecordErrorExecute(
  Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_DANGKY_VANGMAT_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                mTransNo := 0;
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                ModalResult := mrOk;
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bIsError: Boolean;
begin
    with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdDel.Enabled := bBrowse and (not bEmpty);
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyVangmatDanhSach.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
     if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyVangmatDanhSach.DeleteAllRecord;
begin
    with spIMP_HR_DANGKY_VANGMAT_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyVangmatDanhSach.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;


end.
