﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Bangcong;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, ComCtrls, ActnList,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, wwdblook, Db, ADODB, wwfltdlg,
  Menus, AdvMenus, AppEvnts, fctreecombo, wwFltDlg2, wwDBGrid2,
  frameMY, wwDialog, ToolWin, Mask, wwdbedit, Wwdotdot, Wwdbcomb, RzPanel,
  RzSplit;

type
  TFrmBangcong = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    ToolBar1: TToolBar;
    BtnXuly: TToolButton;
    ToolButton3: TToolButton;
    ToolButton11: TToolButton;
    QrPayroll: TADOQuery;
    CmdRefresh: TAction;
    DsPayroll: TDataSource;
    QrTimeSheet: TADOQuery;
    DsTimeSheet: TDataSource;
    FilterPayroll: TwwFilterDialog2;
    QrPayrollMANV: TWideStringField;
    QrPayrollMaPhongBan: TWideStringField;
    QrPayrollCREATE_BY: TIntegerField;
    QrPayrollUPDATE_BY: TIntegerField;
    QrPayrollCREATE_DATE: TDateTimeField;
    QrPayrollUPDATE_DATE: TDateTimeField;
    QrTimeSheetMANV: TWideStringField;
    QrTimeSheetUPDATE_BY: TIntegerField;
    QrTimeSheetUPDATE_DATE: TDateTimeField;
    CmdFilter: TAction;
    QrEmp: TADOQuery;
    QrPayrollName: TWideStringField;
    CmdCalc2: TAction;
    PopXuly: TAdvPopupMenu;
    N2: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    TS_RECORDER_IN: TADOCommand;
    TS_RECORDER_OUT: TADOCommand;
    CmdParams: TAction;
    Pop1: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    Lcdliu1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N5: TMenuItem;
    CmdClear1: TAction;
    Item3: TMenuItem;
    QrTimeSheetMACA: TWideStringField;
    QrShift: TADOQuery;
    QrTimeSheetLK_TENCA: TWideStringField;
    QrLeave: TADOQuery;
    CLEAR_DAILY_DATA: TADOCommand;
    CmdClear2: TAction;
    Xatonbdliucng1: TMenuItem;
    CmdRep: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdSwitch: TAction;
    TS_CALC: TADOCommand;
    CmdCalc1: TAction;
    ItemCalc1: TMenuItem;
    CmdDayCsv: TAction;
    Lydliucngngybsung1: TMenuItem;
    CmdAutoIO2: TAction;
    Tbsungdliuqunqutth1: TMenuItem;
    CmdAutoIO1: TAction;
    TS_AUTO_IO: TADOCommand;
    QrCsv: TADOQuery;
    TS_DAILY_CSV: TADOCommand;
    CmdDetail: TAction;
    CmdValidate: TAction;
    Kimtratnhngcadliu1: TMenuItem;
    CmdValidateNext: TAction;
    CmdReload: TAction;
    CmdUpdate: TAction;
    N10: TMenuItem;
    TS_DAILY_CHECK: TADOCommand;
    QrPayrollLK_TenPhongBan: TWideStringField;
    frMY: TfrMY;
    Status: TStatusBar;
    N4: TMenuItem;
    TS_RECORDER_DETECT: TADOCommand;
    CmdDayhst: TAction;
    Lydliuchmcngtfile1: TMenuItem;
    PgMain: TPageControl;
    TaPayroll: TTabSheet;
    GrPayroll: TwwDBGrid2;
    GrTime: TwwDBGrid2;
    CbShift: TwwDBLookupCombo;
    Splitter1: TSplitter;
    QrTimeSheetName: TWideStringField;
    Bevel1: TBevel;
    CmdGetRecLog: TAction;
    Cpnhtdliutdliucngth1: TMenuItem;
    TS_RECORDER_LOG: TADOCommand;
    TS_PAYROLL_PERIOD: TADOCommand;
    QrTimeSheetNGAY: TDateTimeField;
    QrTimeSheetNAM: TIntegerField;
    QrTimeSheetTHANG: TIntegerField;
    QrTimeSheetLOAINGAY: TIntegerField;
    QrTimeSheetMaVangMat: TWideStringField;
    QrTimeSheetMaVangMat1: TWideStringField;
    QrTimeSheetLK_TenVangMat1: TWideStringField;
    QrTimeSheetLK_TENVANGMAT: TWideStringField;
    QrPayrollTHANG: TIntegerField;
    QrPayrollNAM: TIntegerField;
    QrTimeSheetSoNgayVangMat: TFloatField;
    QrTimeSheetSoNgayVangMat1: TFloatField;
    QrTimeSheetTHU: TWideStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    spHR_XOACONG: TADOCommand;
    Item2: TMenuItem;
    CmdGetRecLog1NV: TAction;
    N1: TMenuItem;
    QrPayrollMaChiNhanh: TWideStringField;
    QrPayrollMaBoPhan: TWideStringField;
    QrPayrollMaNhomLV: TWideStringField;
    QrPayrollMaChucDanh: TWideStringField;
    QrTimeSheetCREATE_BY: TIntegerField;
    QrTimeSheetCREATE_DATE: TDateTimeField;
    QrTimeSheetSoGio_Ca: TFloatField;
    QrTimeSheetLK_TenPhongBan: TWideStringField;
    QrTimeSheetLK_TenBoPhan: TWideStringField;
    QrTimeSheetLK_TenChiNhanh: TWideStringField;
    QrPayrollLK_TenChiNhanh: TWideStringField;
    QrPayrollLK_TenChucDanh: TWideStringField;
    QrPayrollLK_TenNhomLV: TWideStringField;
    BtnDangKy: TToolButton;
    ToolButton12: TToolButton;
    PopDangKy: TAdvPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    CmdVangMat: TAction;
    CmdLichLamViec: TAction;
    CmdTangCa: TAction;
    QrTimeSheetGioVao_Ca: TDateTimeField;
    QrTimeSheetGioRa_Ca: TDateTimeField;
    QrTimeSheetGioVao1_Ca: TDateTimeField;
    QrTimeSheetGioRa1_Ca: TDateTimeField;
    QrTimeSheetDiTre_Ca: TDateTimeField;
    QrTimeSheetVeSom_Ca: TDateTimeField;
    QrTimeSheetLK_ManvQL: TWideStringField;
    QrTimeSheetManvQL: TWideStringField;
    TabSheet1: TTabSheet;
    GrDaily: TwwDBGrid2;
    GrTime2: TwwDBGrid2;
    QrDaily: TADOQuery;
    DsDaily: TDataSource;
    QrDailyNgay: TDateTimeField;
    QrDailyThang: TIntegerField;
    QrDailyNam: TIntegerField;
    QrDailyThu: TWideStringField;
    TabSheet2: TTabSheet;
    GrVangMat: TwwDBGrid2;
    QrVangMat: TADOQuery;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    WideStringField17: TWideStringField;
    DsVangMat: TDataSource;
    QrVangMatNgay: TDateTimeField;
    QrVangMatThang: TIntegerField;
    QrVangMatNam: TIntegerField;
    QrVangMatMaVangMat: TWideStringField;
    QrVangMatDauCa: TIntegerField;
    QrVangMatSoNgay: TFloatField;
    QrTangCa: TADOQuery;
    WideStringField3: TWideStringField;
    WideStringField4: TWideStringField;
    WideStringField5: TWideStringField;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DsTangCa: TDataSource;
    QrDailyVangMat: TADOQuery;
    DateTimeField2: TDateTimeField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    WideStringField7: TWideStringField;
    DsDaiLyVangMat: TDataSource;
    QrVangMatThu: TWideStringField;
    QrVangMatLK_TenVangMat: TWideStringField;
    QrVangMatLK_TenCaNghi: TWideStringField;
    QrTimeSheetLK_MaChiNhanh: TWideStringField;
    QrTimeSheetLK_MaPhongBan: TWideStringField;
    QrTimeSheetLK_MaBoPhan: TWideStringField;
    QrTimeSheetLK_MaNhomLV: TWideStringField;
    QrTimeSheetMaCa2: TWideStringField;
    QrTimeSheetSoGio_Ca2: TFloatField;
    QrTimeSheetGioVao_Ca2: TDateTimeField;
    QrTimeSheetGioRa_Ca2: TDateTimeField;
    QrTimeSheetGioVao1_Ca2: TDateTimeField;
    QrTimeSheetGioRa1_Ca2: TDateTimeField;
    CbShift2: TwwDBLookupCombo;
    QrTimeSheetLK_TenCa2: TWideStringField;
    Filter: TwwFilterDialog2;
    QrV_HR_LOAI_TINHLUONG: TADOQuery;
    QrTimeSheetLoaiNgayTinhLuong: TWideStringField;
    QrTimeSheetLK_TenLoaiNgayTinhLuong: TWideStringField;
    TabSheet3: TTabSheet;
    GrTangCa: TwwDBGrid2;
    QrTangCaMaCa: TWideStringField;
    QrTangCaSoGio_Ca: TFloatField;
    QrTangCaGioVao_Ca: TDateTimeField;
    QrTangCaGioRa_Ca: TDateTimeField;
    QrTangCaGioVao1_Ca: TDateTimeField;
    QrTangCaGioRa1_Ca: TDateTimeField;
    QrTangCaDangKyTuGio: TDateTimeField;
    QrTangCaDangKyDenGio: TDateTimeField;
    QrTangCaDangKySoGio: TFloatField;
    QrTangCaSoGioHuongLuong: TFloatField;
    QrTangCaSoGioNghiBu: TFloatField;
    QrTangCaLK_TenCa: TWideStringField;
    QrTangCaThu: TWideStringField;
    QrTimeSheetLK_MaLoai_VangMat_Ma: TWideStringField;
    QrTimeSheetLK_MaLoai_VangMat1_Ma: TWideStringField;
    QrVangMatLK_MaLoai_VangMat_Ma: TWideStringField;
    QrPayrollCalc_NgayVaoLam: TDateTimeField;
    QrPayrollNgayVaoLam: TDateTimeField;
    QrPayrollNgayBatDauHocViec: TDateTimeField;
    QrPayrollNgayKetThucHocViec: TDateTimeField;
    QrPayrollNgayBatDauThuViec: TDateTimeField;
    QrPayrollNgayKetThucThuViec: TDateTimeField;
    QrPayrollNgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollNgayThoiViec: TDateTimeField;
    QrPayrollCalc_NgayBatDauHocViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucHocViec: TDateTimeField;
    QrPayrollCalc_NgayBatDauThuViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucThuViec: TDateTimeField;
    QrPayrollCalc_NgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollCalc_NgayThoiViec: TDateTimeField;
    RzSizePaDaily: TRzSizePanel;
    RzSizePaPayroll: TRzSizePanel;
    PopVangMat: TAdvPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    PopNghiThaiSan: TMenuItem;
    PopTangCa: TAdvPopupMenu;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    FilterTangCa: TwwFilterDialog2;
    FilterVangMat: TwwFilterDialog2;
    CmdClearFilterVangMat: TAction;
    CmdClearFilterTangCa: TAction;
    PopTime2: TAdvPopupMenu;
    MenuItem10: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdCalc2Execute(Sender: TObject);
    procedure BtnXulyClick(Sender: TObject);
    procedure GrTimeCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrTimeSheetCalcFields(DataSet: TDataSet);
    procedure CmdParamsExecute(Sender: TObject);
    procedure CmdClear1Execute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdClear2Execute(Sender: TObject);
    procedure CmdRepExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdCalc1Execute(Sender: TObject);
    procedure CmdDayCsvExecute(Sender: TObject);
    procedure CmdAutoIO2Execute(Sender: TObject);
    procedure CmdAutoIO1Execute(Sender: TObject);
    procedure QrShiftAfterOpen(DataSet: TDataSet);
    procedure CmdDetailExecute(Sender: TObject);
    procedure CmdValidateExecute(Sender: TObject);
    procedure CmdValidateNextExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure Pop1Popup(Sender: TObject);
    procedure QrTimeSheetBeforePost(DataSet: TDataSet);
    procedure QrTimeSheetMACAChange(Sender: TField);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdDayhstExecute(Sender: TObject);
    procedure QrPayrollAfterScroll(DataSet: TDataSet);
    procedure QrDailyAfterScroll(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure GrTime2CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdGetRecLogExecute(Sender: TObject);
    procedure CmdGetRecLog1NVExecute(Sender: TObject);
    procedure BtnDangKyClick(Sender: TObject);
    procedure CmdVangMatExecute(Sender: TObject);
    procedure CmdTangCaExecute(Sender: TObject);
    procedure QrDailyCalcFields(DataSet: TDataSet);
    procedure GrVangMatCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrTimeSheetMaCa2Change(Sender: TField);
    procedure GrTangCaCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrPayrollCalcFields(DataSet: TDataSet);
    procedure PopVangMatPopup(Sender: TObject);
    procedure PopNghiThaiSanClick(Sender: TObject);
    procedure CmdClearFilterTangCaExecute(Sender: TObject);
    procedure CmdClearFilterVangMatExecute(Sender: TObject);
    procedure CmdLichLamViecExecute(Sender: TObject);
  private
    r: WORD;
  	mCanEdit, mTrigger, bClose: Boolean;
    fStr: String;
    fLevel, mMonth, mYear, fFilterThaiSan: Integer;
	sqlPayroll, sqlTimeSheet, sqlVangMat, sqlTangCa, mManv: String;

    fsDateFormat: TFormatSettings;

    procedure GetPeriod;
    procedure Clear1;
    procedure AutoIO1;
    procedure ValidationCheck(nx: Boolean);

    function DetectRec(pID: String; pDate, pTime: TDateTime): Boolean;
    function InRec(pID: String; pDate, pTime: TDateTime): Boolean;
    function OutRec(pID: String; pDate, pTime: TDateTime): Boolean;

	function StdConvert(pModel, pFile, pTemp: String): Boolean;

    procedure CapnhatDulieuCong(manv: String = '');
    function TinhCong(reOT: Boolean; manv: String = ''): Boolean;
    procedure XoaDulieuCong(manv: String = '');

    procedure OpenTimeSheet;
    procedure OpenVangMat;
    procedure OpenTangCa;
  public
  	procedure Execute(r: WORD; manv: String = ''; month: Integer = 0; year: Integer = 0);
  end;

var
  FrmBangcong: TFrmBangcong;

implementation

{$R *.DFM}

uses
	isLib, Rights, isDb, MainData, ExCommon, isStr, isMsg, Tuden,
  	BangThongso, BosungCong, Variants, isFile, isCommon, GmsRep, DangkyVangmat,
    GuidEx, DangkyTangCa, HrData, LichLamViec;

const
	FORM_CODE = 'HR_BANG_CONG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := manv;
    mMonth := month;
    mYear := year;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    frMY.Initial(sysMon, sysYear, CmdRefresh);
    mTrigger := False;
    sqlPayroll := QrPayroll.SQL.Text;
    sqlTimeSheet := QrTimeSheet.SQL.Text;
    sqlVangMat := QrVangMat.SQL.Text;
    sqlTangCa := QrTangCa.SQL.Text;
    AddAllFields(QrPayroll, 'HR_BANG_LUONG', 0);
    AddAllFields(QrTimesheet, 'HR_BANG_CONG', 0);
    AddAllFields(QrVangMat, 'HR_DANGKY_VANGMAT_CHITIET', 0);
    AddAllFields(QrTangCa, 'HR_BANG_CONG_TANGCA', 0);
    AddAllFields(QrDaily, 'HR_BANG_THONGSO_NGAY', 0);

    //Format Date.
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fsDateFormat);
//    fsDateFormat.ShortDateFormat := 'mm/dd/yyyy';
//    fsDateFormat.DateSeparator := '/';

    // Display format
//    SetShortDateFormat(PS_DAILY);

	SetDisplayFormat(QrPayroll, '0.0#;-0.0#;#');
    SetDisplayFormat(QrTimeSheet, ['NGAY'], 'dd');
//    SetDisplayFormat(QrDaily, ['NGAY'], 'dd');
    SetShortDateFormat(QrDaily);

    // Customize
	SetCustomGrid([FORM_CODE + '_DSNV', FORM_CODE + '_NV'],
        [GrPayroll, GrTime]);
    SetCustomGrid([FORM_CODE + '_DSNGAY', FORM_CODE + '_NGAY'],
        [GrDaily, GrTime2]);
    SetCustomGrid([FORM_CODE + '_VANGMAT', FORM_CODE + '_TANGCA'],
        [GrVangMat, GrTangCa]);

    SetDictionary([QrPayroll, QrTimeSheet], [FORM_CODE + '_DSNV', FORM_CODE + '_NV'], [FilterPayroll, Filter]);
    SetDictionary([QrVangMat, QrTangCa], [FORM_CODE + '_VANGMAT', FORM_CODE + '_TANGCA'], [FilterVangMat, FilterTangCa]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.FormShow(Sender: TObject);
begin
    // Open database
	OpenDataSets([QrShift, QrLeave, QrEmp, QrV_HR_LOAI_TINHLUONG]);
    if mManv <> '' then
    begin
        sysMon := mMonth;
        sysYear := mYear;
    end;

    fFilterThaiSan := 0;
    CmdReload.Execute;

    if mManv <> '' then
    begin
        QrPayroll.Locate('Manv', mManv, []);
    end;
    // Go ahead
	GrPayroll.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrShift, QrLeave, QrEmp, QrV_HR_LOAI_TINHLUONG]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.GetPeriod;
begin
	sysMon  := StrToInt(frMY.CbMon.Text);
    sysYear := StrToInt(frMY.CbYear.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdRefreshExecute(Sender: TObject);
var
    s: String;
	mSQL: String;
    mMon, mYear: Integer;
begin
    if frMY.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frMY.CbOrg.SelectedNode.StringData;
        fLevel := frMY.CbOrg.SelectedNode.Level;
	end;

    mMon := StrToInt(frMY.CbMon.Text);
    mYear := StrToInt(frMY.CbYear.Text);

	if	(fStr    = s) and
		(sysMon  = mMon)  and
        (sysYear = mYear) then
		Exit;

    Wait(DATAREADING);
    fStr := s;
//    if (mMon <> sysMon) or (mYear <> sysYear) or (PS_DAILY.IsEmpty) then
    if (mMon <> sysMon) or (mYear <> sysYear) then
    begin
        GetPeriod;

        with QrDaily do
        begin
            Close;
            Parameters[0].Value := sysMon;
            Parameters[1].Value := sysYear;
            Open;
        end;
    end;

    with QrPayroll do
    begin
		s := Sort;
    	Close;
        mSQL := ' 1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := sqlPayroll + ' and ' + mSQL;
        Parameters.Refresh;
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;

    end;
    if s = '' then
        s := 'ManvQL';
//
    SortDataSet(QrPayroll, s);

    OpenTimeSheet;
    OpenVangMat;
    OpenTangCa;

    ClearWait;

    PgMainChange(PgMain);
//
    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 0);
    if bClose then
    	Msg(RS_TIMESHEET_CFM_CLOSED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.OpenTangCa;
var
    s, mSQL: String;
begin
    with QrTangCa do
    begin
        s := Sort;
    	Close;
        mSQL := '1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := Format(sqlTangCa, [mSQL]);
        SQL.Add(' order by Manv, Ngay');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;
    end;
    if s = '' then
        s := 'Ngay';

    SortDataSet(QrTangCa, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.OpenTimeSheet;
var
    s, mSQL: String;
begin
    with QrTimeSheet do
    begin
        s := Sort;
    	Close;
        mSQL := '1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := Format(sqlTimeSheet, [mSQL]);
        SQL.Add(' Order by MANV, [NGAY]');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;
    end;
    if s = '' then
        s := 'ManvQL';

    SortDataSet(QrTimeSheet, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.OpenVangMat;
var
    s, mSQL: String;
begin
    with QrVangMat do
    begin
        s := Sort;
    	Close;
        mSQL := '1=1';

        if fStr <> '' then
            case fLevel of
            0:
                mSQL := ' [MaChiNhanh]=''' + fStr + '''';
            1:
                mSQL := ' [MaPhongBan]=''' + fStr + '''';
            2:
                mSQL := ' [MaBoPhan]=''' + fStr + '''';
            end;

        if sysIsDataAccess then
            mSQL := mSQL + Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4))', [sysLogonUID]);

        SQL.Text := Format(sqlVangMat, [mSQL]);

        if fFilterThaiSan > 0 then
             SQL.Add(' and [MaVangMat]=''ML'' ')
        else
            SQL.Add(' and [MaVangMat]<>''ML'' ');

        SQL.Add(' order by Manv, Ngay');
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;
    end;
    if s = '' then
        s := 'Ngay';

    SortDataSet(QrVangMat, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdSearchExecute(Sender: TObject);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        1: exSearch(Name + '_1', DsTimeSheet);
        2: exSearch(Name + '_2', DsVangMat);
        3: exSearch(Name + '_3', DsTangCa)
        else exSearch(Name, DsPayroll);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdFilterExecute(Sender: TObject);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        0: FilterPayroll.Execute;
        2: FilterVangMat.Execute;
        3: FilterTangCa.Execute;
//        else FilterPayroll.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdGetRecLog1NVExecute(Sender: TObject);
begin
    CapnhatDulieuCong(QrPayroll.FieldByName('MANV').AsString);
    exReSyncRecord(QrTimeSheet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdGetRecLogExecute(Sender: TObject);
begin
    CapnhatDulieuCong;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdLichLamViecExecute(Sender: TObject);
var
    sManv: String;
begin
    r := GetRights('HR_LICHLAMVIEC');
    if r = R_DENY then
    	Exit;

    with QrTimeSheet do
    begin
        sManv := FieldByName('Manv').AsString;
    end;
    Application.CreateForm(TFrmLichLamViec, FrmLichLamViec);
    FrmLichLamViec.Execute(r, sManv, sysMon, sysYear);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdUpdateExecute(Sender: TObject);
var
    bm: TBytes;
begin
    QrTimeSheet.CheckBrowseMode;
	GetPeriod;
    with QrPayroll do
    begin
        bm := BookMark;
        if HrDataMain.UpdatePayrollSheet(sysMon, sysYear, True) then
        begin
            MsgDone;
            CmdReload.Execute;
        end;
        BookMark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangcong.TinhCong(reOT: Boolean; manv: String): Boolean;
var
    _msg: string;
begin
    try
    	with TS_CALC do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := sysYear;
            Parameters[3].Value := sysMon;
            Parameters[4].Value := reOT;
            Parameters[5].Value := manv;
            Execute;
            Result := Parameters[0].Value = 0;
            if not Result then
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_TIMESHEET_CALC_ERROR;

                ErrMsg(_msg);
            end;
        end;
    except
        Result := False;
        ErrMsg(RS_TIMESHEET_CALC_ERROR);
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CapnhatDulieuCong(manv: String);
var
    ngayd, ngayc: TDateTime;
    b: Boolean;
begin
    with TS_PAYROLL_PERIOD do
    begin
        Prepared := True;
        Parameters[1].Value := sysYear;
        Parameters[2].Value := sysMon;
        Execute;
        if Parameters[0].Value <> 0 then
        begin
            MsgDone(False);
            Exit;
        end;
        ngayd := Parameters[3].Value;
        ngayc := Parameters[4].Value;
    end;
    if not FrmTuden.Execute(ngayd, ngayc) then
    	Exit;

    Wait(PROCESSING);
    try
        with TS_RECORDER_LOG do
        begin
            Prepared := True;
            Parameters[1].Value := ngayd;
            Parameters[2].Value := ngayc;
            Parameters[3].Value := manv;
            try
                Execute;
                b := Parameters[0].Value = 0;
                if b then
                begin
//                    exReSyncRecord(QrPayroll, True);
//                    exReSyncRecord(QrTimeSheet, True);
                    QrTimeSheet.Requery;
//                    CmdReload.Execute;
                end;
            except
                on E: Exception do
                    ErrMsg(E.Message);
            end;
        end;
    finally
        ClearWait;
        if b then
            MsgDone;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdCalc1Execute(Sender: TObject);
begin
	if not YesNo(Format(RS_TIMESHEET_CALC1, [sysMon, sysYear, QrPayroll.FieldByName('LK_TENNV').AsString])) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
    if TinhCong(True, QrPayroll.FieldByName('Manv').AsString) then
        MsgDone;

    exReSyncRecord(QrTimeSheet, True);
    exReSyncRecord(QrPayroll);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdCalc2Execute(Sender: TObject);
var
	b: Boolean;
    rs: Integer;
    bm: TBytes;
begin
	// Confirm
	if not YesNo(Format(RS_TIMESHEET_CALC2, [sysMon, sysYear]), 1) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
//	CmdRefresh.Execute;

//	rs := YesNoCancel(RS_TIMESHEET_RECALC1, 1);
//    if rs = mrCancel then
//        Exit;

    b := rs = mrYes;
    if TinhCong(b) then
        MsgDone;

    with QrPayroll do
    begin
        bm := BookMark;
        //QrPayroll.Requery;
        CmdReload.Execute;

        BookMark := bm;
    end;


end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.AutoIO1;
begin
	with TS_AUTO_IO do
    begin
    	Prepared := True;
        Parameters[1].Value := QrPayroll.FieldByName('MANV').AsString;
        Parameters[2].Value := sysYear;
        Parameters[3].Value := sysMon;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdAutoIO1Execute(Sender: TObject);
begin
	if not YesNo(Format(RS_TIMESHEET_AUTO1, [QrPayroll.FieldByName('LK_TENNV').AsString]), 1) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
	AutoIO1;
	QrTimeSheet.Requery;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
{$HINTS OFF}
procedure TFrmBangcong.CmdAutoIO2Execute(Sender: TObject);
begin
	if not YesNo(RS_TIMESHEET_AUTO2, 1) then
    	Exit;

    QrTimeSheet.CheckBrowseMode;
	CmdRefresh.Execute;

    with QrPayroll do
    begin
    	StartProgress(RecordCount);
    	DisableControls;
        First;
        Tag := 99;
        try
            while not Eof do
            begin
                SetProgressDesc(FieldByName('MANV').AsString);
                AutoIO1;
                Next;
                IncProgress;
            end;
            First;
        finally
            Tag := 0;
            EnableControls;
            StopProgress;
        end;
    end;

	QrTimeSheet.Requery;
    MsgDone;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdDayCsvExecute(Sender: TObject);
var
	b: Boolean;
    n: Integer;
    mIn, mOut, mDate: TDateTime;
	sFmt, sEmp, mFile: String;
	mLog: TStrings;
begin
	// Regional setting confirm
    SetLength(sFmt, 100);
    n := GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SSHORTDATE, @sFmt[1], 100);
    SetLength(sFmt, n - 1);

    if not YesNo(Format(RS_CFM_DATE_FORMAT, [sFmt])) then
    	Exit;

	// Get file name
	mFile := exGetImportFile;
    if mFile = '' then
    	Exit;

    // File preparing
//    if not CsvPrepareFile(mFile, 'Daily') then
//		Exit;
    Exit; //NTD

    QrTimeSheet.CheckBrowseMode;

    // Open data file
    with QrCsv do
    begin
    	b := True;
    	SQL.Text := 'select * from "Daily.csv"';
        try
	        Open;
        	sEmp := FieldByName('MANV').AsString;
		    mIn := FieldByName('IN').AsDateTime;
            mOut := FieldByName('OUT').AsDateTime;
            mDate := FieldByName('DATE').AsDateTime;
        except
        	ErrMsg(RS_INVALID_DATA);
            Close;
            b := False;
        end;

		if not b then
			Exit;

	    mLog := TStringList.Create;
        StartProgress(RecordCount);

        while not Eof do
        begin
        	sEmp := FieldByName('MANV').AsString;
		    mIn := Frac(FieldByName('IN').AsDateTime);
            mOut := Frac(FieldByName('OUT').AsDateTime);
            mDate := Trunc(FieldByName('DATE').AsDateTime);
        	SetProgressDesc(sEmp);

			with TS_DAILY_CSV do
            begin
            	Prepared := True;
                Parameters[1].Value := sEmp;
                Parameters[2].Value := mDate;
                if (mIn = 0.0) and (mOut = 0.0) then
                begin
	                Parameters[3].Value := NULL;
    	            Parameters[4].Value := NULL;
                end
                else
                begin
	                Parameters[3].Value := mIn;
    	            Parameters[4].Value := mOut;
                end;
                Execute;

                if Parameters[0].Value <> 0 then
                	mLog.Add(sEmp);
            end;

            IncProgress;
        	Next;
        end;
        Close;
        StopProgress;
    end;

    if mLog.Count > 0 then
    	exViewLog(RS_LOG_INVALID_EMP, mLog.Text);

    mLog.Free;
    CmdReload.Execute;
//    GrTime.SetFocus;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangcong.StdConvert(pModel, pFile, pTemp: String): Boolean;
var
	ls, rs, temp: TStrings;
    i: Integer;
begin
	Result := True;
    if not FileExists(pFile) then
    begin
    	Result := False;
        Exit;
    end;

	ls := TStringList.Create;		// Rows buffer
    rs := TStringList.Create;		// Fields buffer / row
    temp := TStringList.Create;		// Data
    ls.LoadFromFile(pFile);
    try
        for i := 0 to ls.Count - 1 do
        begin
            if Pos(',Granted,', ls[i]) > 0 then
            begin
                isStrBreak(ls[i], ',', rs);
                temp.Add('0,'+ rs[5] + ',X,' + rs[1] + ',' + rs[2])
            end;
        end;
    except
    	Result := False;
    end;
    temp.SaveToFile(pTemp);
    ls.Free;
    rs.Free;
    temp.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdDayhstExecute(Sender: TObject);
//var
//	mFile, mTempFile, mModel: String;
//    bDate: TDateTime;
//
//    b: Boolean;
//    i, cRecId, cId, cFunc, cDate, cTime: Integer;
//    lsIn, lsOut, lsX, lsIn1, lsOut1, log, ls, rs: TStrings;
//
//    s, mRecId, mId, mFunc, mDelim, mIO: String;
//    mDate, mTime: TDateTime;
//
//    fs: TFormatSettings;
begin
	// Get params
//    Application.CreateForm(TFrmTimeRecOption, FrmTimeRecOption);
//    if not FrmTimeRecOption.Get(mModel, mFile, bDate) then
//        Exit;
//    // Convert to specified format
//	mTempFile := vlGetTempDir + 'Hrms.hst';
//    if not StdConvert(mModel, mFile, mTempFile) then
//    	Exit;
//
//    // Init
//    ls := TStringList.Create;		// Rows buffer
//    rs := TStringList.Create;		// Fields buffer / row
//    log := TStringList.Create;		// Invalid Id
//
//    lsIn := TStringList.Create;		// List of In recorders
//    lsOut := TStringList.Create;	// List of Out recorders
//    lsX := TStringList.Create;		// List of Detect recorders
//
//    lsIn1 := TStringList.Create;	// List of In functions
//    lsOut1 := TStringList.Create;	// List of Out functions
//
//    // I/O settings
//    vlStrBreak(FlexReadString(mModel, 'In'), ',', lsIn);
//    vlStrBreak(FlexReadString('Out'), ',', lsOut);
//    vlStrBreak(FlexReadString('Detect'), ',', lsX);
//
//    vlStrBreak(FlexReadString('In1'), ',', lsIn1);
//    vlStrBreak(FlexReadString('Out1'), ',', lsOut1);
//
//    mDelim := FlexReadString(mModel, 'Delim', ' ');
//    if mDelim[1] = '#' then
//    	mDelim := Chr(StrToInt(vlSubStr(mDelim, 2)));
//
//	// Locale format settings
//	GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, fs);
//    fs.ShortDateFormat := FlexReadString(mModel, 'DateFormat', fs.ShortDateFormat);
//    fs.ShortTimeFormat := FlexReadString(mModel, 'TimeFormat', fs.ShortTimeFormat);
//    s := FlexReadString(mModel, 'DateSeparator', fs.DateSeparator);
//    if s <> '' then
//        fs.DateSeparator := s[1];
//    s := FlexReadString(mModel, 'TimeSeparator', fs.TimeSeparator);
//    if s <> '' then
//        fs.TimeSeparator := s[1];
//
//    // Data columns order
//    cRecId := FlexReadInteger(mModel, 'MacID', -1);		// Recorder
//    cId := FlexReadInteger('ID');							// Emp/Card ID
//    cFunc := FlexReadInteger('Function');					// Function
//    cDate := FlexReadInteger('Date');						// Date
//    cTime := FlexReadInteger('Time');						// Time
//
//    // Load data
//	ls.LoadFromFile(mTempFile);
//
//    // Proc
//    StartProgress(ls.Count, 'Analizing');
//    for i := 0 to ls.Count - 1 do
//    begin
//    	// Parse
//    	vlStrBreak(ls[i], mDelim, rs);
//		IncProgress;
//
//        b := False;
//        try
//        	// Recorder
//            if cRecId < 0 then
//            	mRecId := ''
//            else
//            	mRecId := rs[cRecId];
//
//        	// Emp/Card ID
//        	mId := rs[cId];
//
//            // Function
//            if cFunc < 0 then
//            	mFunc := ''
//            else
//            	mFunc := rs[cFunc];
//
//            if cTime < 0 then		// Date and time contigous
//            begin
//	            mDate := StrToDateTime(rs[cDate], fs);
//    	        mTime := Frac(mDate);
//                mDate := Trunc(mDate);
//            end
//            else
//            begin
//	            mDate := StrToDate(rs[cDate], fs);
//    	        mTime := StrToTime(rs[cTime]);
//            end;
//        except
//            b := True;
//        end;
//
//        if b then
//        begin
//        	ShowProgress(False);
//        	if not YesNo(Format(RS_IMPORT_ERROR_AT, [i])) then
//            	Break;
//        	ShowProgress;
//            Continue;
//        end;
//
//        // Proc
//	    SetProgressDesc(mId);
//
//        if mDate >= bDate then
//        begin
//        	mIO := '';
//        	if mRecId <> '' then
//            begin
//	        	if lsIn.IndexOf(mRecId) >= 0 then
//                	mIO := 'I'
//                else if lsOut.IndexOf(mRecId) >= 0 then
//                	mIO := 'O'
//                else if lsX.IndexOf(mRecId) >= 0 then
//                	mIO := 'X'
//            end;
//
//            if mIO = '' then
//                if mFunc <> '' then
//                begin
//                    if lsIn1.IndexOf(mFunc) >= 0 then
//                        mIO := 'I'
//                    else if lsOut1.IndexOf(mFunc) >= 0 then
//                        mIO := 'O'
//		            else
//                        mIO := 'X'
//                end
//                else
//					mIO := 'X';
//
//            if mIO = 'I' then
//            begin
//                if not InRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//            else if mIO = 'O' then
//            begin
//                if not OutRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//            else if mIO = 'X' then
//            begin
//                if not DetectRec(mID, mDate, mTime) then
//                    if log.IndexOf(mId) < 0 then
//                        log.Add(mId);
//			end
//		end;
//    end;
//    StopProgress;
//
//    if log.Count > 0 then
//    	exViewLog(RS_LOG_INVALID_CARD, log.Text);
//
//    ls.Free;
//    rs.Free;
//    log.Free;
//    lsIn.Free;
//    lsOut.Free;
//    lsX.Free;
//    lsIn1.Free;
//    lsOut1.Free;
//
//    // Done
//	// DeleteFile(mTempFile);
//    CmdReload.Execute;
//    GrTime.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.BtnDangKyClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.BtnXulyClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.GrTimeCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (UpperCase(s) = 'NGAY') or (UpperCase(s) = 'THU') then
        begin
            AFont.Size  := 10;
            AFont.Style := [];
            AFont.Color  := clBlack;

            case DataSource.DataSet.FieldByName('LoaiNgay').AsInteger of
            2:
            begin
                AFont.Size  := 9;
                AFont.Color := clBlue;
                AFont.Style := [fsBold];
            end;
            3:
            begin
                AFont.Size  := 9;
                AFont.Color := clRed;
                AFont.Style := [fsBold];
            end
            else
            end;
        end;

        if (UpperCase(s) = 'LK_TENVANGMAT') or (UpperCase(s) = 'SONGAYVANGMAT') then
        begin
            if DataSource.DataSet.FieldByName('LK_MaLoai_VangMat_Ma').AsString = 'AL' then
                AFont.Color := clPurple
        end;

        if (UpperCase(s) = 'LK_TENVANGMAT1') or (UpperCase(s) = 'SONGAYVANGMAT1') then
        begin
            if DataSource.DataSet.FieldByName('LK_MaLoai_VangMat1_Ma').AsString = 'AL' then
                AFont.Color := clPurple
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.GrTime2CalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(s).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (gdFixed in State) then
        begin
            AFont.Size  := 10;
            AFont.Style := [];
            AFont.Color  := clBlack;
            case DataSource.DataSet.FieldByName('LoaiNgay').AsInteger of
                2:
                    AFont.Color := clBlue;
                3:
                    AFont.Color := clRed;
            else
            end;
        end;

        if (UpperCase(s) = 'LK_TENVANGMAT') or (UpperCase(s) = 'SONGAYVANGMAT') then
        begin
            if DataSource.DataSet.FieldByName('LK_MaLoai_VangMat_Ma').AsString = 'AL' then
                AFont.Color := clPurple
        end;

        if (UpperCase(s) = 'LK_TENVANGMAT1') or (UpperCase(s) = 'SONGAYVANGMAT1') then
        begin
            if DataSource.DataSet.FieldByName('LK_MaLoai_VangMat1_Ma').AsString = 'AL' then
                AFont.Color := clPurple
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.GrVangMatCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (UpperCase(s) = 'LK_TENVANGMAT') or
            (UpperCase(s) = 'MAVANGMAT') or
            (UpperCase(s) = 'LK_TENCANGHI') or
            (UpperCase(s) = 'SONGAY') then
        begin
            if DataSource.DataSet.FieldByName('LK_MaLoai_VangMat_Ma').AsString = 'AL' then
                AFont.Color := clPurple
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.GrTangCaCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
	s: String;
begin
    if Highlight then
        Exit;

	s := Field.FullName;
    with (Sender as TwwDbGrid) do
    begin
        if ColumnByName(Field.FullName).ReadOnly and not (gdFocused in State) then
        begin
            ABrush.Color := clBtnFace;
            AFont.Color  := clBlack;
        end;

        if (UpperCase(s) = 'NGAY') or (UpperCase(s) = 'THU') or (UpperCase(s) = 'HESOOT') then
        begin
            case DataSource.DataSet.FieldByName('LoaiNgay').AsInteger of
            2:
                AFont.Color := clBlue;
            3:
                AFont.Color := clRed
            else
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
var
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    case n of
        1: Status.Panels[0].Text := exRecordCount(QrTimeSheet, Filter);
        2: Status.Panels[0].Text := exRecordCount(QrVangMat, Filter);
        3: Status.Panels[0].Text := exRecordCount(QrTangCa, Filter)
        else Status.Panels[0].Text := exRecordCount(QrPayroll, FilterPayroll);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
	WD_NAMES: array[1..14] of String = (
    	'CN',
    	'T2',
    	'T3',
    	'T4',
    	'T5',
    	'T6',
    	'T7',
        'Su',
        'Mo',
        'Tu',
        'We',
        'Th',
        'Fr',
        'Sa'
    );
procedure TFrmBangcong.QrTimeSheetCalcFields(DataSet: TDataSet);
var
	n: Integer;
begin
	n := Iif(sysEnglish, 7, 0);
	with DataSet do
       	FieldByName('THU').AsString := WD_NAMES[n + DayOfWeek(FieldByName('NGAY').AsDateTime)];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrDailyAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdParamsExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_BANG_THONGSO'); //NTD
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBangThongso, FrmBangThongso);
    FrmBangThongso.Execute(R_FULL, 1, StrToInt(frMY.cbMon.Text), StrToInt(frMY.CbYear.Text));
    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.Clear1;
begin
	with CLEAR_DAILY_DATA do
    begin
    	Prepared := True;
        Parameters[1].Value := QrPayroll.FieldByName('MANV').AsString;
        Parameters[2].Value := sysYear;
        Parameters[3].Value := sysMon;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdClear1Execute(Sender: TObject);
begin
	if not YesNo(Format(RS_TIMESHEET_CLEAR1, [sysMon, sysYear, QrPayroll.FieldByName('LK_TENNV').AsString]), 1) then
    	Exit;

    XoaDulieuCong(QrPayroll.FieldByName('MANV').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdClear2Execute(Sender: TObject);
begin
    if not YesNo(Format(RS_TIMESHEET_CLEAR2, [sysMon, sysYear]), 1) then
    	Exit;
    XoaDulieuCong;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdClearFilterExecute(Sender: TObject);
begin
    with FilterPayroll do
    begin
        FieldInfo.Clear;
		ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdClearFilterTangCaExecute(Sender: TObject);
begin
    with FilterTangCa do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdClearFilterVangMatExecute(Sender: TObject);
begin
    with FilterVangMat do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b, b2: Boolean;
    n: Integer;
    bBrowseTime: Boolean;
begin

    n := PgMain.ActivePageIndex;
    CmdClearFilter.Enabled := FilterPayroll.FieldInfo.Count > 0;
    CmdClearFilterVangMat.Enabled := FilterVangMat.FieldInfo.Count > 0;
    CmdClearFilterTangCa.Enabled := FilterTangCa.FieldInfo.Count > 0;

    b := mCanEdit and (not bClose);
    CmdUpdate.Enabled := b;
//    CmdTimeRec.Enabled := b;
    CmdDayCsv.Enabled := b;
    CmdDayhst.Enabled := b;

    CmdCalc1.Enabled := b and (n = 0);
    CmdClear1.Enabled := b and (n = 0);
    CmdAutoIO1.Enabled := b and (n = 0);

    CmdCalc2.Enabled := b;
    CmdClear2.Enabled := b;
    CmdAutoIO2.Enabled := b;

    CmdValidate.Enabled := (n = 0);
    CmdValidateNext.Enabled := (n = 0);
    CmdLichLamViec.Enabled :=  (n < 2);

    DsTimeSheet.AutoEdit := b;

    with QrTimeSheet do
    begin
        if not Active then
            Exit;
        b2 := IsEmpty;
        bBrowseTime := State in [dsBrowse];
    end;

    GrTime.ReadOnly := b2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdRepExecute(Sender: TObject);
var
	r: WORD;
begin
//	r := GetRights('COLEX_REPORT');
//    if r = R_DENY then
//    	Exit;
//
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('CongLuong') (*3*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrPayroll then
    	GrTime.SetFocus
	else if ActiveControl = GrTime then
    	GrPayroll.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdTangCaExecute(Sender: TObject);
var
    sManv, sManvQL, sName: string;
    n: Integer;
begin
    r := GetRights('HR_TANGCA');
    if r = R_DENY then
    	Exit;

    n := PgMain.ActivePageIndex;
    case n of
        3:
        begin
            with QrTangCa do
            begin
                sManv := FieldByName('Manv').AsString;
                sManvQL := FieldByName('LK_ManvQL').AsString;
                sName := FieldByName('LK_Tennv').AsString;
            end;
        end
        else
        begin
            with QrTimeSheet do
            begin
                sManv := FieldByName('Manv').AsString;
                sManvQL := FieldByName('LK_ManvQL').AsString;
                sName := FieldByName('LK_Tennv').AsString;
            end;
        end;
    end;

    Application.CreateForm(TFrmDangkyTangCa, FrmDangkyTangCa);
    FrmDangkyTangCa.Execute(r, 0, sManv, 'HR_BANGCONG', sManvQL, sName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrShiftAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrShift, ['GIOVAO', 'GIORA', 'GIOVAO1', 'GIORA1'], 'hh:mm');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdDetailExecute(Sender: TObject);
var
	p: TPoint;
    s: String;
begin
    if (Sender as TwwDBGrid2).GetActiveField.FieldName = 'VAORA_THO'  then
        Exit;

	// Field list
   	s := FlexConfigString(FORM_CODE, 'Editable');
    if s = '' then
    	Exit;

	p.x := GrTime.Left + GrTime.Width;
    p.y := GrTime.Top;
    p := ClientToScreen(p);

    GrTime.Enabled := False;
	Application.CreateForm(TFrmBosungCong, FrmBosungCong);
    with FrmBosungCong do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y + 2;
    	Execute(DsTimeSheet, s);
    end;
    GrTime.Enabled := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.ValidationCheck;
var
	b: Boolean;
    s: String;
    d: TDateTime;
begin
	with QrTimeSheet do
    begin
    	CheckBrowseMode;
    	DisableControls;
	end;

    if nx then
		QrTimeSheet.Next
    else
    	QrPayroll.First;

    b := False;
    while (not b) and (not QrPayroll.Eof) do
    begin
    	s := QrPayroll.FieldByName('MANV').AsString;

		with QrTimeSheet do
    	begin
            while (not b) and (not Eof) do
            begin
            	d := FieldByName('NGAY').AsDateTime;
                try
	            	with TS_DAILY_CHECK do
    	            begin
        	        	Parameters[1].Value := s;
            	    	Parameters[2].Value := d;
                	    Execute;
	                end;
					b := False;
                except
					s := DataMain.Conn.Errors[0].Description;
					b := True;
                end;

                if b then
                begin
					EnableControls;
                    Refresh;
					PopMsg(s);
                end
                else
	            	Next;
            end;
	    end;
        if not b then
	        QrPayroll.Next;
    end;

	QrTimeSheet.EnableControls;
    if b then
        GrTime.SetFocus
    else
	    Msg(RS_TIMESHEET_VALID);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.XoaDulieuCong(manv: String);
var
    ngayd, ngayc: TDateTime;
    b: Boolean;
begin
    Wait(PROCESSING);
    try
        with spHR_XOACONG do
        begin
            Prepared := True;
            Parameters[1].Value := sysYear;
            Parameters[2].Value := sysMon;
            Parameters[3].Value := manv;
            try
                Execute;
                b := Parameters[0].Value = 0;
                if b then
                begin
//                    exReSyncRecord(QrPayroll, True);
//                    exReSyncRecord(QrTimeSheet, True);
                    QrTimeSheet.Requery;
//                    CmdReload.Execute;
                end;
            except
                on E: Exception do
                    ErrMsg(E.Message);
            end;
        end;
    finally
        ClearWait;
        if b then
            MsgDone;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdValidateExecute(Sender: TObject);
begin
	ValidationCheck(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdValidateNextExecute(Sender: TObject);
begin
	ValidationCheck(True);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdVangMatExecute(Sender: TObject);
var
    sManv, sManvQL, sName: String;
    n: Integer;
begin
    r := GetRights('HR_VANGMAT');
    if r = R_DENY then
    	Exit;

    n := PgMain.ActivePageIndex;
    case n of
        2:
        begin
            with QrVangMat do
            begin
                sManv := FieldByName('Manv').AsString;
                sManvQL := FieldByName('LK_ManvQL').AsString;
                sName := FieldByName('LK_Tennv').AsString;
            end;
        end;
        3:
        begin
            with QrTangCa do
            begin
                sManv := FieldByName('Manv').AsString;
                sManvQL := FieldByName('LK_ManvQL').AsString;
                sName := FieldByName('LK_Tennv').AsString;
            end;
        end
        else
        begin
            with QrTimeSheet do
            begin
                sManv := FieldByName('Manv').AsString;
                sManvQL := FieldByName('LK_ManvQL').AsString;
                sName := FieldByName('LK_Tennv').AsString;
            end;
        end;
    end;
    Application.CreateForm(TFrmDangkyVangmat, FrmDangkyVangmat);
    FrmDangkyVangmat.Execute(r, 0, sManv, '', sManvQL, sName);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangcong.DetectRec;
begin
	with TS_RECORDER_DETECT do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangcong.InRec;
begin
	with TS_RECORDER_IN do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangcong.OutRec;
begin
	with TS_RECORDER_OUT do
    begin
        Parameters[1].Value := pId;
        Parameters[2].Value := pDate;
        Parameters[3].Value := pTime;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.CmdReloadExecute(Sender: TObject);
begin
    QrEmp.Requery;
    QrShift.Requery;
    QrLeave.Requery;

	sysMon := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.PgMainChange(Sender: TObject);
var
    s, manv: String;
    n: Integer;
begin
    if not QrPayroll.Active then
        Exit;

    n := PgMain.ActivePageIndex;
    manv := QrPayroll.FieldByName('MANV').AsString;
    Screen.Cursor := crSQLWait;
    case n of
        0:
        begin
            if QrTimeSheet.Active then
                QrTimeSheet.Filter := 'MANV=' + QuotedStr(manv);
        end;
        1:
        begin
            if QrTimeSheet.Active then
            begin
                s := '[Ngay]>=''' + DateToStr(QrDaily.FieldByName('Ngay').AsDateTime, fsDateFormat) + '''' +
                ' and [Ngay]<''' + DateToStr(QrDaily.FieldByName('Ngay').AsDateTime + 1, fsDateFormat) + '''';

                QrTimeSheet.Filter := s;
            end;
        end;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.Pop1Popup(Sender: TObject);
var
	s: String;
begin
	s := QrPayroll.FieldByName('LK_TENNV').AsString;
    if s <> '' then
    begin
		ItemCalc1.Caption := RS_TIMESHEET_POPUP + ' "' + s + '"';
        Item2.Caption := RS_TIMESHEET_POPUP2 + ' "' + s + '"';
        Item3.Caption := RS_TIMESHEET_POPUP3 + ' "' + s + '"'
    end
    else
    begin
		ItemCalc1.Caption := (ItemCalc1.Action as TAction).Caption;
        Item2.Caption := (Item2.Action as TAction).Caption;
        Item3.Caption := (Item3.Action as TAction).Caption;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.PopNghiThaiSanClick(Sender: TObject);
var
   fFilterTemp: Integer;
begin
    fFilterTemp := fFilterThaiSan;
    fFilterThaiSan :=  (Sender as TComponent).Tag;
    if fFilterTemp > 0 then
        fFilterThaiSan := 0;

    OpenVangMat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.PopVangMatPopup(Sender: TObject);
begin
     PopNghiThaiSan.Checked := fFilterThaiSan = 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrTimeSheetBeforePost(DataSet: TDataSet);
var
    s, s2: String;
    f, f2: Double;
begin
	with (QrTimeSheet) do
    begin
        s := FieldByName('MaVangMat').AsString;
        s2 := FieldByName('MaVangMat1').AsString;
        f := FieldByName('SoNgayVangMat').AsFloat;
        f2 := FieldByName('SoNgayVangMat1').AsFloat;
{NTD: Sử dụng tool đăng ký vắng mặt để duyệt nên không điều chỉnh tại Bảng Công}
//        if s <> '' then
//        begin
//            if BlankConfirm(QrTimeSheet, ['SoNgayVangMat']) then
//                Abort;

//            if s = s2 then
//            begin
//                ErrMsg(RS_LEAVE_TYPE_DUPLICATE);
//                Abort;
//            end;
//        end;

//        if f <> 0 then
//        begin
//            if BlankConfirm(QrTimeSheet, ['MaVangMat']) then
//                Abort;
//
//            if f + f2 > 1 then
//            begin
//                ErrMsg(RS_LEAVE_MAX_DAY);
//                Abort;
//            end;
//        end;
//
//        if s2 <> '' then
//        begin
//            if BlankConfirm(QrTimeSheet, ['SoNgayVangMat1']) then
//                Abort
//        end;
//
//        if f2 <> 0 then
//        begin
//            if BlankConfirm(QrTimeSheet, ['MaVangMat1']) then
//                Abort
//        end;

        FieldByName('UPDATE_BY').AsInteger := sysLogonUID;
        FieldByName('UPDATE_DATE').AsDateTime := Now;
		SetNull(QrTimeSheet, ['MaCa', 'MaCa2', 'MaVangMat', 'MaVangMat1']);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrTimeSheetMaCa2Change(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString = '' then
        begin
            FieldByName('Giovao_Ca2').Clear;
            FieldByName('GIOVAO1_CA2').Clear;
            FieldByName('GIORA1_CA2').Clear;
            FieldByName('GIORA_CA2').Clear;
            FieldByName('SOGIO_CA2').Clear;
        end
        else
        begin
            FieldByName('GIOVAO_CA2').Value  := QrShift.FieldByName('GIOVAO').Value;
            FieldByName('GIOVAO1_CA2').Value := QrShift.FieldByName('GIOVAO1').Value;
            FieldByName('GIORA1_CA2').Value := QrShift.FieldByName('GIORA1').Value;
            FieldByName('GIORA_CA2').Value := QrShift.FieldByName('GIORA').Value;
            FieldByName('SOGIO_CA2').Value := QrShift.FieldByName('SOGIO').Value;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrTimeSheetMACAChange(Sender: TField);
begin
    with QrTimeSheet do
    begin
        if Sender.AsString = '' then
        begin
            FieldByName('Giovao_Ca').Clear;
            FieldByName('GIOVAO1_CA').Clear;
            FieldByName('GIORA1_CA').Clear;
            FieldByName('GIORA_CA').Clear;
            FieldByName('SOGIO_CA').Clear;
        end
        else
        begin
            FieldByName('GIOVAO_CA').Value  := QrShift.FieldByName('GIOVAO').Value;
            FieldByName('GIOVAO1_CA').Value := QrShift.FieldByName('GIOVAO1').Value;
            FieldByName('GIORA1_CA').Value := QrShift.FieldByName('GIORA1').Value;
            FieldByName('GIORA_CA').Value := QrShift.FieldByName('GIORA').Value;
            FieldByName('SOGIO_CA').Value := QrShift.FieldByName('SOGIO').Value;
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrDailyCalcFields(DataSet: TDataSet);
var
	n: Integer;
begin
	n := Iif(sysEnglish, 7, 0);
	with DataSet do
       	FieldByName('Thu').AsString := WD_NAMES[n + DayOfWeek(FieldByName('Ngay').AsDateTime)];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrPayrollAfterScroll(DataSet: TDataSet);
begin
    if QrPayroll.Tag <> 99 then
        PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.QrPayrollCalcFields(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    with QrPayroll do
    begin
        DecodeDate(FieldByName('NgayVaoLam').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLam').AsDateTime := FieldByName('NgayVaoLam').AsDateTime
        else
            FieldByName('Calc_NgayVaoLam').Clear;

        DecodeDate(FieldByName('NgayBatDauHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauHocViec').AsDateTime := FieldByName('NgayBatDauHocViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauHocViec').Clear;

        DecodeDate(FieldByName('NgayKetThucHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucHocViec').AsDateTime := FieldByName('NgayKetThucHocViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucHocViec').Clear;

        DecodeDate(FieldByName('NgayBatDauThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauThuViec').AsDateTime := FieldByName('NgayBatDauThuViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauThuViec').Clear;

        DecodeDate(FieldByName('NgayKetThucThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucThuViec').AsDateTime := FieldByName('NgayKetThucThuViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucThuViec').Clear;

        DecodeDate(FieldByName('NgayVaoLamChinhThuc').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLamChinhThuc').AsDateTime := FieldByName('NgayVaoLamChinhThuc').AsDateTime
        else
            FieldByName('Calc_NgayVaoLamChinhThuc').Clear;

        DecodeDate(FieldByName('NgayThoiViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayThoiViec').AsDateTime := FieldByName('NgayThoiViec').AsDateTime
        else
            FieldByName('Calc_NgayThoiViec').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.ApplicationEvents1Hint(Sender: TObject);
begin
	if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangcong.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	try
		QrTimeSheet.CheckBrowseMode;
        CanClose := True;
    except
        CanClose := False;
    end;
end;

end.
