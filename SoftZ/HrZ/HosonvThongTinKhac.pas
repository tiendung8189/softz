﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HosonvThongTinKhac;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, wwdbdatetimepicker, ExtCtrls,
  ActnList, ComCtrls, db, wwdblook, DBCtrls, frameEmp, Buttons, isPanel, Mask,
  ToolWin, wwdbedit, DBCtrlsEh;

type
  TFrmHosonvThongTinKhac = class(TForm)
    PaTerminated: TPanel;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    frEmp: TfrEmp;
    CmdChecked: TAction;
    ToolButton1: TToolButton;
    CmdEdit: TAction;
    ToolButton2: TToolButton;
    PaGhiChu: TisPanel;
    EdThongTinKhac: TDBMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
    procedure CmdEditExecute(Sender: TObject);
  private
  	mCanEdit, mChanged: Boolean;
  public
  	function  Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String): Boolean;
  end;

var
  FrmHosonvThongTinKhac: TFrmHosonvThongTinKhac;

implementation

{$R *.DFM}

uses
    Rights, isLib, isMsg,  isDb, ExCommon, MainData, HoSonv;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvThongTinKhac.Execute;
begin
	mCanEdit := rCanEdit(r);
    mChanged := False;
    frEmp.Initial(EmpID, EmpIDLabel, EmpName);
    ShowModal;
    Result := mChanged;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.FormShow(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(FrmHoSonv.QrDMNV, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.CmdSaveExecute(Sender: TObject);
begin
	FrmHoSonv.QrDMNV.Post;
    mChanged := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.CmdCancelExecute(Sender: TObject);
begin
	FrmHoSonv.QrDMNV.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.CmdCloseExecute(Sender: TObject);
begin
    Close
end;

procedure TFrmHosonvThongTinKhac.CmdEditExecute(Sender: TObject);
begin
    FrmHoSonv.QrDMNV.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
begin
	b := FrmHoSonv.QrDMNV.State in [dsBrowse];
    CmdEdit.Enabled := b;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvThongTinKhac.TntFormCreate(Sender: TObject);
begin
    TMyForm(Self).Init();
end;
end.
