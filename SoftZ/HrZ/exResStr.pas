﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit exResStr;

interface

resourcestring
	// Database
    RS_DB_FAIL_CONNECT    	= 'Không kết nối được với cơ sở dữ liệu.';
    RS_CONNECT_OK           = 'Kết nối thành công.';
    RS_RECONNECT            = 'Kết nối lại với cơ sở dữ liệu. Tiếp tục?';
    RS_CONNECT_FAIL         = 'Lỗi kết nối.       ';

    // File
	RS_FILE_ERR_IO			= 'Lỗi truy xuất file.';
    RS_FILE_ERR_EXPORT		= 'Lỗi xuất dữ liệu ra file.';

	// Import
    RS_IMPORT_RECCOUNT		= 'Đã import %d mẫu tin.';
    RS_IMPORT_NONE			= 'Không có mẫu tin nào được import.';
    RS_IMPORT_ERR_RECCOUNT	= 'Có %d mẫu tin không hợp lệ.';

    // ePos
    RS_POS_ERROR_QTY 		= 'Số lượng không hợp lệ.';
    RS_POS_INVALID_PAID		= 'Số tiền thanh toán chưa đúng.';

    // Common
    RS_NOTE_CAP         	= 'Lưu ý';
    RS_ADMIN_CONTACT		= 'Xin liên hệ với người quản trị hệ thống.';

    RS_VAT_NOTSAME       	= 'Mặt hàng sai loại thuế.';
    RS_NO_FOLDEREXPORT		= 'Chưa chọn thư mục lưu dữ liệu export.';
    RS_EXPORTED_COMPLETE 	= 'Đã xuất xong dữ liệu vào thư mục "%s"';

    RS_DSN_CONFIG			= 'Chưa cấu hình text DSN.';
    RS_ERROR_COPY_FILE		= 'Lỗi khi copy file vào thư mục hệ thống.';
    RS_INVALID_DATA			= 'Dữ liệu không hợp lệ.';

	RS_ITEM_CODE_FAIL1		= 'Lỗi nhập liệu. Sai mã hàng hóa.';
	RS_ITEM_CODE_FAIL2		= 'Lỗi nhập liệu. Sai mã hàng hóa hoặc nhà cung cấp.';
    RS_BOOKCLOSED			= 'Đã khóa sổ đến ngày %s.';

	RS_INVALID_DISCOUNT 	= 'Chiết khấu không hợp lệ.';
	RS_POS_NOT_PRICE 		= 'Mặt hàng chưa có giá bán.';
    RS_SCT_CHANGE           = 'Ngày lập phiếu thay đổi. Số phiếu mới là: "%s".';

	RS_DA_THUCHI			= 'Đã phát sinh Thu / Chi nên không thể chỉnh sửa được.';
    RS_XOA_CHITIET			= 'Xóa toàn bộ chi tiết chứng từ?';

implementation

end.
