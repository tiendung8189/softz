﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmHopdong;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin, StdCtrls, wwdblook, Vcl.Mask,
  RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows, System.RegularExpressions;

type
  TFrmDmHopdong = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    CbNhom: TwwDBLookupCombo;
    QrDMLK_TENLOAI: TWideStringField;
    QrDMSORT: TIntegerField;
    QrDMMaHopDong: TWideStringField;
    QrDMTenHopDong: TWideStringField;
    QrDMTenHopDong_TA: TWideStringField;
    QrDMSoThang: TIntegerField;
    QrDMMaLoai_HopDong: TWideStringField;
    QrDMMaNhacViec: TWideStringField;
    QrDMLK_SoNgay: TWideStringField;
    QrDMLK_TenNhacViec: TWideStringField;
    CbCanhBao: TwwDBLookupCombo;
    BtClearFile: TRzDBButtonEdit;
    BtImport: TRzDBButtonEdit;
    QrDMFileName: TWideStringField;
    QrDMFileExt: TWideStringField;
    QrDMF1: TIntegerField;
    QrDMF2: TIntegerField;
    QrDMF3: TIntegerField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    BtView: TRzDBButtonEdit;
    QrDMCAL_Ext: TWideStringField;
    QrDMFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure BtImportButtonClick(Sender: TObject);
    procedure BtClearFileButtonClick(Sender: TObject);
    procedure BtViewButtonClick(Sender: TObject);
    procedure QrDMAfterInsert(DataSet: TDataSet);
    procedure QrDMCalcFields(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrDMAfterDelete(DataSet: TDataSet);
    procedure QrDMAfterPost(DataSet: TDataSet);
  private
  	mCanEdit, fixCode: Boolean;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmHopdong: TFrmDmHopdong;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    isFile, GuidEx, isStr, HrData;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_HOPDONG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDM.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDM, FORM_CODE);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDM, FORM_CODE, Filter);

    fixCode := SetCodeLength(FORM_CODE, QrDM.FieldByName('MaHopDong'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.FormShow(Sender: TObject);
begin
    OpenDataSets([HrDataMain.QrDmNhacViec]);

    QrDM.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMAfterInsert(DataSet: TDataSet);
begin
    with QrDM do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMAfterPost(DataSet: TDataSet);
begin
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrDM.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDM, ['MaHopDong', 'TenHopDong']) then
   		Abort;

    if fixCode then
		if LengthConfirm(QrDM.FieldByName('MaHopDong')) then
        	Abort;
    SetAudit(DataSet);
end;

procedure TFrmDmHopdong.QrDMCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrDM do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s <> '' then
        begin
            BtView.Enabled := True;
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('CAL_Ext').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
    end;
end;

procedure TFrmDmHopdong.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrDM.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaHopDong' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM, Filter);
end;

procedure TFrmDmHopdong.BtClearFileButtonClick(Sender: TObject);
begin
    with QrDM do
    if FieldByName('FileExt').AsString <> '' then
    begin
        if  not YesNo('Xoá file đính kèm. Tiếp tục?') then
           Exit;

        Dettach;
    end;
end;

procedure TFrmDmHopdong.BtImportButtonClick(Sender: TObject);
begin
    Attach;
end;

procedure TFrmDmHopdong.BtViewButtonClick(Sender: TObject);
begin
    with QrDM do
    if FieldByName('FileExt').AsString <> '' then
    begin
        ViewAttachment;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrDM do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrDM.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.Dettach();
begin
     with QrDM do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHopdong.ViewAttachment();
var
	s, sn, name: String;
begin
    with QrDM do
    begin
    	sn := isStripToneMark(toNonAccentVietnamese(FieldByName('FileName').AsString));
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg('Không tìm thấy nội dung file.');
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg('Lỗi trích xuất nội dung.');
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

end.
