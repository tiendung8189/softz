(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameMonthYear;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls;

type
  TfrMonthYear = class(TFrame)
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    CbMon: TComboBox;
    CbYear: TComboBox;
  private
  	
  public
  	procedure Initial(m, y: Integer); overload;
    procedure Initial; overload;
  end;

implementation

uses
	isStr;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMonthYear.Initial(m, y: Integer);
begin
    isMonthList(CbMon, m);
    isYearList(CbYear, y);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrMonthYear.Initial;
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    isMonthList(CbMon, mm);
    isYearList(CbYear, yy);
end;
end.
