﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Phepnam;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  Db, ADODB, AppEvnts, ActnList, Menus, AdvMenus, ComCtrls,
  Grids, StdCtrls, wwfltdlg,
  fcTreeView, fctreecombo, wwFltDlg2, wwDBGrid2,
  frameYearNow, wwDialog, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmPhepnam = class(TForm)
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Action: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    CmdRefresh: TAction;
    ApplicationEvents1: TApplicationEvents;
    DsLeaveBL: TDataSource;
    QrLeave: TADOQuery;
    CmdEntitlement: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    SepExport: TToolButton;
    ToolButton11: TToolButton;
    QrLeaveNAM: TIntegerField;
    QrLeaveMANV: TWideStringField;
    QrEmp: TADOQuery;
    ToolButton3: TToolButton;
    CmdImportExcel: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    Lcdliu1: TMenuItem;
    QrCsv: TADOQuery;
    UPDATE_BECF: TADOCommand;
    ToolButton5: TToolButton;
    HR_AL_TAKEN: TADOCommand;
    CmdReload: TAction;
    frYearNow: TfrYearNow;
    GrList: TwwDBGrid2;
    ToolButton7: TToolButton;
    QrLeaveNGAY_VAOLAM: TDateTimeField;
    QrLeaveNGAY_THOIVIEC: TDateTimeField;
    QrLeaveT1: TFloatField;
    QrLeaveT2: TFloatField;
    QrLeaveT3: TFloatField;
    QrLeaveT4: TFloatField;
    QrLeaveT5: TFloatField;
    QrLeaveT6: TFloatField;
    QrLeaveT7: TFloatField;
    QrLeaveT8: TFloatField;
    QrLeaveT9: TFloatField;
    QrLeaveT10: TFloatField;
    QrLeaveT11: TFloatField;
    QrLeaveT12: TFloatField;
    QrLeaveCREATE_BY: TIntegerField;
    QrLeaveUPDATE_BY: TIntegerField;
    QrLeaveCREATE_DATE: TDateTimeField;
    QrLeaveUPDATE_DATE: TDateTimeField;
    QrLeaveLK_TENNV: TWideStringField;
    CmdExportExcel: TAction;
    ToolButton1: TToolButton;
    ToolButton9: TToolButton;
    SepImport: TToolButton;
    QrLeaveMaPhongBan: TWideStringField;
    QrLeaveNgayThamNien: TDateTimeField;
    CmdParams: TAction;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    QrLeaveMaChucDanh: TWideStringField;
    QrLeaveMaNhomLV: TWideStringField;
    QrLeaveLK_TenChucDanh: TWideStringField;
    QrLeaveLK_TenNhomLV: TWideStringField;
    QrLeaveLK_TenPhongBan: TWideStringField;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    CmdNhanVien: TAction;
    ToolButton12: TToolButton;
    CmdNghiPhep: TAction;
    ToolButton13: TToolButton;
    QrLeaveLK_ManvQL: TWideStringField;
    QrLeaveManvQL: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdEntitlementExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrLeaveBeforeInsert(DataSet: TDataSet);
    procedure CmdReloadExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CmdExportExcelExecute(Sender: TObject);
    procedure CmdParamsExecute(Sender: TObject);
    procedure CmdNhanVienExecute(Sender: TObject);
    procedure CmdNghiPhepExecute(Sender: TObject);
    procedure frYearNowCbYearChange(Sender: TObject);
  private
    r: WORD;
  	mCanEdit: Boolean;
    mSQL: String;

    fYear, fLevel: Integer;
    fStr: String;
    dateNow: TDateTime;
    function  UpdateTobeCF(pEmp: String; pValue: Double): Integer;
    function  UpdatePhepNam: Boolean;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmPhepnam: TFrmPhepnam;

const
    FORM_CODE = 'HR_PHEPNAM';

implementation

{$R *.DFM}

uses
	isLib, isMsg, isDb, Rights, MainData, ExCommon, RepEngine, Variants, isCommon,
    OfficeData, GuidEx, isFile, ImportExcel, PhepnamThongbao, PhepnamBangThongso,
    HoSonv, DangkyVangmat, HrData;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    DsLeaveBL.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrLeave, QrEmp]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    SepImport.Visible := CmdImportExcel.Visible;

    CmdExportExcel.Visible := FlexConfigBool(FORM_CODE, 'Export Excel');
    SepExport.Visible := CmdExportExcel.Visible;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.FormShow(Sender: TObject);
begin
    frYearNow.Initial(sysYear, Date, CmdRefresh);

    dateNow := Date;

    with HrDataMain do
        OpenDataSets([QrDMNV]);

    AddAllFields(QrLeave, 'HR_PHEPNAM');

    SetDisplayFormat(QrLeave, sysFloatFmtOne);

	SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrLeave, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrLeave.SQL do
    begin
        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by Manv');

        mSQL := Text;
    end;

    CmdReload.Execute;
    GrList.SetFocus;

    // tinh toan lan dau tien
    UpdatePhepNam;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.frYearNowCbYearChange(Sender: TObject);
var
    ngayBatDau, ngayKetThuc: TDateTime;
begin
    fYear := StrToInt(frYearNow.CbYear.Text);
    HrDataMain.fnHRNamNgayDauNgayCuoi(fYear, 'PHEPNAM', ngayBatDau, ngayKetThuc);

    if (dateNow >= ngayBatDau) and (dateNow <= ngayKetThuc) then
        frYearNow.EdNow.Date := dateNow
    else if (dateNow > ngayBatDau) and (dateNow > ngayKetThuc) then
        frYearNow.EdNow.Date := ngayKetThuc
    else if (dateNow < ngayBatDau) and (dateNow < ngayKetThuc) then
        frYearNow.EdNow.Date := ngayBatDau;

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdRefreshExecute(Sender: TObject);
var
    s, sSQL: String;
begin
    if frYearNow.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frYearNow.CbOrg.SelectedNode.StringData;
        fLevel := frYearNow.CbOrg.SelectedNode.Level;
	end;

    if (StrToInt(frYearNow.CbYear.Text) <> fYear) or (fStr <> s) then
    begin
        fYear := StrToInt(frYearNow.CbYear.Text);
        fStr := s;

	    Wait(DATAREADING);
        with QrLeave do
        begin
            s := Sort;
            Close;
            sSQL := '1=1';

            if fStr <> '' then
                case fLevel of
                0:
                    sSQL := '[MaChiNhanh]=''' + fStr + '''';
                1:
                    sSQL := '[MaPhongBan]=''' + fStr + '''';
                2:
                    sSQL := '[MaBoPhan]=''' + fStr + '''';
//                3:
//                    sSQL := '[Group ID]=''' + fStr + '''';
                end;

            SQL.Text := Format(mSQL, [sSQL]);
	        Parameters.Refresh;
            Parameters[0].Value := fYear;
            Open;
        end;
        if s = '' then
            s := 'ManvQL';

        SortDataSet(QrLeave, s);
        ClearWait;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdEntitlementExecute(Sender: TObject);
begin
    CmdRefresh.Execute;
    if UpdatePhepNam then
        MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdParamsExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_PHEPNAM_THONGSO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPhepnamBangThongso, FrmPhepnamBangThongso);
    FrmPhepnamBangThongso.Execute(r, StrToInt(frYearNow.CbYear.Text));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdPrintExecute(Sender: TObject);
begin
    ShowReport(Caption, 'HR_RP_PHEPNAM', [sysLogonUID, fYear, fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdExportExcelExecute(Sender: TObject);
var
    filename, repname: String;
begin
    repname := 'HR_RP_PHEPNAM_EXCEL';
    filename := 'XLSX\HR_RP_PHEPNAM_EXCEL.xlsx';

    DataOffice.CreateReport2(filename, [sysLogonUID, fYear, fStr], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsLeaveBL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrLeave, '', False, 'Manv');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'Manv') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not HrDataMain.QrDMNV.Locate('Manv', s, []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã nhân viên');
                            Next;
                            Continue;
                        end;

                        if (not QrLeave.Locate('Manv', s, []))  then
                        begin
                            QrLeave.Append;
                            QrLeave.FieldByName('Manv').AsString := s;
                            QrLeave.FieldByName('NAM').AsInteger := StrToInt(frYearNow.CbYear.Text)
                        end
                        else
                            SetEditState(QrLeave);

                        for i := 0 to n do
                            if i <> k then
                                QrLeave.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrLeave.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdNghiPhepExecute(Sender: TObject);
var
    sManv, sManvQL, sName: String;
begin
    r := GetRights('HR_VANGMAT');
    if r = R_DENY then
    	Exit;

    with QrLeave do
    begin
	    sManv := FieldByName('Manv').AsString;
        sManvQL := FieldByName('LK_ManvQL').AsString;
        sName := FieldByName('LK_TENNV').AsString;
    end;

    Application.CreateForm(TFrmDangkyVangmat, FrmDangkyVangmat);
    FrmDangkyVangmat.Execute(r, 0, sManv, 'HR_PHEPNAM', sManvQL, sName);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdNhanVienExecute(Sender: TObject);
var
    mEmpID: String;
begin
    r := GetRights('HR_PROFILE');
    if r = R_DENY then
    	Exit;

    with QrLeave do
    begin
	    mEmpID := FieldByName('Manv').AsString;
    end;

    Application.CreateForm(TFrmHoSonv, FrmHoSonv);
    FrmHoSonv.Execute(r, mEmpID);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bIsEmpty: Boolean;
begin
    bIsEmpty := QrLeave.IsEmpty;
    CmdNghiPhep.Enabled := not bIsEmpty;
    CmdEntitlement.Enabled := mCanEdit;
    CmdImportExcel.Enabled := mCanEdit;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.QrLeaveBeforeInsert(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := exRecordCount(QrLeave, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPhepnam.UpdatePhepNam: Boolean;
var
	_msg: String;
begin
    Result := False;
    try
        with TADOCommand.Create(nil) do
        begin
            Connection := DataMain.Conn;
            CommandType := cmdStoredProc;
            CommandTimeout := 1200;
            CommandText := 'spHR_PHEPNAM_Generate;1';

            Parameters.Refresh;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := fYear;
            Parameters[3].Value := '';
            Parameters[4].Value := '';
            Execute;

            Result := Parameters[0].Value = 0;
            if Result then
            begin
                QrLeave.Requery;
            end else
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_ERROR_ACTION;

                ErrMsg(_msg);
            end;

            Free;
        end;
    except
        on E: Exception do
            ErrMsg(E.Message);
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPhepnam.UpdateTobeCF;
var
	n: Integer;
begin
    with UPDATE_BECF do
    begin
        Prepared := True;
        Execute(n, VarArrayOf([pValue, fYear, pEmp]));;
    end;
	Result := n;	// Number of affected records
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if Highlight then
    	Exit;

    with AFont do
    begin
        Size := 10;
        Color := clBlack;
        Style := [];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnam.CmdReloadExecute(Sender: TObject);
begin
    fYear := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

end.
