(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameYear;

interface

uses
  Windows, Classes, Controls, Forms,
  fctreecombo, StdCtrls, ExtCtrls,
  ActnList, fcCombo;

type
  TfrYear = class(TFrame)
    Panel1: TPanel;
    LbOrg: TLabel;
    CbOrg: TfcTreeCombo;
    Label5: TLabel;
    CbYear: TComboBox;
    procedure CbOrgChange(Sender: TObject);
    procedure CbOrgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CbOrgCloseUp(Sender: TObject; Select: Boolean);
  private
  	FAction: TAction;
  public
  	procedure Initial(y: Integer; ac: TAction);
  end;

implementation

uses
	ExCommon, isStr, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYear.Initial;
begin
    isYearList(CbYear, y);
    FAction := ac;
  	if sysIsDataAccess then
        FlexOrgComboDataAccess(CbOrg)
    else
      	FlexOrgCombo(CbOrg);
    CbOrg.OnChange(CbOrg);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYear.CbOrgChange(Sender: TObject);
begin
    LbOrg.Caption := exGetFlexOrgDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYear.CbOrgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearOrg}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrYear.CbOrgCloseUp(Sender: TObject; Select: Boolean);
begin
	if Assigned(FAction) then
	    if Select then
    	    FAction.Execute;
end;

end.
