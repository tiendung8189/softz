﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvTruocKhiVaoCongTy;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  frameEmp, RzPanel, RzSplit, isPanel, Vcl.Buttons, rDBComponents, DBCtrlsEh;

type
  TFrmHoSonvTruocKhiVaoCongTy = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    PaThongTin: TisPanel;
    EdCongTy: TDBEditEh;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterLK_TENNV: TWideStringField;
    frEmp1: TfrEmp;
    RzSizePanel1: TRzSizePanel;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterUPDATE_NAME: TWideStringField;
    CmdLoaiPhuCap: TAction;
    QrMasterCongTy: TWideStringField;
    QrMasterChucDanh: TWideStringField;
    QrMasterNgayBatDau: TDateTimeField;
    QrMasterNgayKetThuc: TDateTimeField;
    QrMasterKinhNghiem: TWideStringField;
    QrMasterGhiChu: TWideMemoField;
    Label4: TLabel;
    CbToDate: TwwDBDateTimePicker;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    wwDBEdit2: TDBEditEh;
    wwDBEdit1: TDBEditEh;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    QrMasterIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure ToolButton2Click(Sender: TObject);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdLoaiPhuCapExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel: Integer;
    mSQL, fStr, mManv: String;
    r: WORD;
  public
  	procedure Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String);
  end;

var
  FrmHoSonvTruocKhiVaoCongTy: TFrmHoSonvTruocKhiVaoCongTy;

const
    TABLE_NAME = 'HR_LICHSU_CONGTY';
    FORM_CODE = 'HR_LICHSU_NHANVIEN_CONGTY';
    REPORT_NAME = 'HR_RP_LICHSU_NHANVIEN_CONGTY';

implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmPhucap, HrData; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.Execute;
begin
	mCanEdit := rCanEdit(r);
    mManv := EmpID;
    frEmp1.Initial(EmpID, EmpIDLabel, EmpName);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrMaster, TABLE_NAME);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.FormShow(Sender: TObject);
begin
     with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_HR_PHUCAP, QrV_HR_PHUCAP_LOAI]);

    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, sysCurFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrMaster.SQL do
    begin
        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayBatDau], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    CbToDate.SetFocus;
    PaGhiChu.Collapsed := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
	    CbToDate.SetFocus;

    PaGhiChu.Collapsed := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdPrintExecute(Sender: TObject);
var
    d: TDateTime;
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    d := Date;
    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        1, //0: Lich su, 1: HSNV/Lich Su
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', d),
        mManv]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdRefreshExecute(Sender: TObject);
var
    s: String;
begin
    Wait(DATAREADING);
    with QrMaster do
    begin
        s := Sort;
        Close;
        Parameters[0].Value := mManv;
        Open;
    end;
    if s = '' then
        s := 'NgayBatDau';

    SortDataSet(QrMaster, s);
    ClearWait;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

procedure TFrmHoSonvTruocKhiVaoCongTy.CmdLoaiPhuCapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
    HrDataMain.QrDM_HR_PHUCAP.Requery;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.QrMasterBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_TUDEN = 'Từ ngày không hợp lệ.';
procedure TFrmHoSonvTruocKhiVaoCongTy.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'NgayBatDau', 'NgayKetThuc', 'CongTy']) then
            Abort;

        if (FieldByName('NgayKetThuc').AsFloat > 10) and (FieldByName('NgayKetThuc').AsDateTime < FieldByName('NgayBatDau').AsDateTime) then
        begin
            ErrMsg(RS_INVALID_TUDEN);
            CbToDate.SelectAll;
            CbToDate.SetFocus;
            Abort;
        end;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.RzSizePanel1ConstrainedResize(
  Sender: TObject; var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 490;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('Idx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Manv').AsString := mManv;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrList then
    	CbToDate.SetFocus;

    GrList.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvTruocKhiVaoCongTy.ToolButton2Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

end.
