﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmPhucap;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  AppEvnts, Menus, AdvMenus, wwDBGrid2, StdCtrls, Mask, wwdbedit, Wwdotdot,
  Wwdbigrd, ToolWin, wwdblook;

type
  TFrmDmPhucap = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsDanhmuc: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    QrDanhmuc: TADOQuery;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    CmdReload: TAction;
    CbNhom: TwwDBLookupCombo;
    QrDanhmucLK_TEN_NHOMPC: TWideStringField;
    QrDanhmucMaPhuCap: TWideStringField;
    QrDanhmucTenPhuCap: TWideStringField;
    QrDanhmucTenPhuCap_TA: TWideStringField;
    QrDanhmucMaNhom_PhuCap: TWideStringField;
    QrDanhmucPhuCapLoai: TIntegerField;
    QrDanhmucCo_BaoHiem: TBooleanField;
    QrDanhmucCo_ThueTNCN: TBooleanField;
    QrDanhmucSoTien: TFloatField;
    CbType: TwwDBLookupCombo;
    QrDanhmucLK_PhuCapLoai: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDanhmucBeforeEdit(DataSet: TDataSet);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
  private
  	mCanEdit, fixCode, mChecked: Boolean;
  public
  	procedure Execute (r : WORD);
  end;

var
  FrmDmPhucap: TFrmDmPhucap;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, MainData, isCommon, HrData;

{$R *.DFM}
const
    FORM_CODE = 'HR_DM_PHUCAP';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdNewExecute(Sender: TObject);
begin
	QrDanhmuc.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdDelExecute(Sender: TObject);
begin
	QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
  var bEmpty : Boolean;
begin
    with QrDanhmuc do
    begin
      if not Active then
         Exit;
      bEmpty := IsEmpty ;
    end;

    exActionUpdate(ActionList, QrDanhmuc, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrDanhmuc]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.FormShow(Sender: TObject);
begin
    SetDictionary(QrDanhmuc,FORM_CODE);
	GrList.ReadOnly := not mCanEdit;
    with HrDataMain do
        OpenDataSets([QrNHOM_PHUCAP]);
	OpenDataSets([QrDanhmuc]);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.QrDanhmucBeforePost(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
		if BlankConfirm(QrDanhmuc, ['MaPhuCap']) then
    		Abort;

        if fixCode then
            if LengthConfirm(QrDanhmuc, ['MaPhuCap']) then
                Abort;

		if BlankConfirm(QrDanhmuc, ['TenPhuCap', 'PhuCapLoai']) then
    		Abort;

//		SetBool;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('Co_BaoHiem').AsBoolean := False;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

procedure TFrmDmPhucap.QrDanhmucBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
        Exit;

    if not mCanEdit then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg; 
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MaPhuCap') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1();

    with HrDataMain do
        OpenDataSets([QrV_HR_PHUCAP_LOAI]);

    AddFields(QrDanhmuc, FORM_CODE);
	SetCustomGrid(FORM_CODE, GrList);

    fixCode := SetCodeLength('DM_PHUCAP', QrDanhmuc.FieldByName('MaPhuCap'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmPhucap.CmdReloadExecute(Sender: TObject);
begin
    QrDanhmuc.Requery;
end;

end.
