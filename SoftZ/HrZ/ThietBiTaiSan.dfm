object FrmThietBiTaiSan: TFrmThietBiTaiSan
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Thi'#7871't B'#7883' - T'#224'i S'#7843'n'
  ClientHeight = 673
  ClientWidth = 915
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 915
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton13: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 652
    Width = 915
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 125
    Width = 429
    Height = 527
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'Manv'#9'12'#9'M'#227#9'F'
      'LK_TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
      'GhiChu'#9'25'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnEnter = CmdRefreshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 915
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
      inherited CbOrg: TfcTreeCombo
        Left = 469
        Width = 432
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitLeft = 469
        ExplicitWidth = 432
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 429
    Top = 125
    Width = 486
    Height = 527
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 4
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 478
      Height = 527
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object ScrollInfo: TScrollBox
        Left = 2
        Top = 2
        Width = 474
        Height = 523
        Align = alClient
        TabOrder = 0
        object PaDinhKem: TisPanel
          Left = 0
          Top = 581
          Width = 453
          Height = 54
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvNone
          Color = 16119285
          ParentBackground = False
          TabOrder = 0
          HeaderCaption = ' .: File '#273#237'nh k'#232'm'
          HeaderColor = clHighlight
          ImageSet = 4
          RealHeight = 0
          ShowButton = True
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = ANSI_CHARSET
          HeaderFont.Color = clWhite
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = [fsBold]
          DesignSize = (
            453
            54)
          object EdFileAttach: TDBEditEh
            Tag = 1
            Left = 111
            Top = 22
            Width = 333
            Height = 22
            TabStop = False
            Anchors = [akLeft, akTop, akRight]
            AutoSize = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Width = 76
            ControlLabel.Height = 16
            ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'Calc_FileName'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <
              item
                Action = CmdFilePlus
                Images.NormalImages = DataMain.ImageEditButton
                Images.HotImages = DataMain.ImageEditButton
                Images.PressedImages = DataMain.ImageEditButton
                Images.DisabledImages = DataMain.ImageEditButton
                Style = ebsGlyphEh
                Width = 30
                DrawBackTime = edbtWhenHotEh
              end
              item
                Action = CmdFileMinus
                Images.NormalImages = DataMain.ImageEditButton
                Images.HotImages = DataMain.ImageEditButton
                Images.PressedImages = DataMain.ImageEditButton
                Images.DisabledImages = DataMain.ImageEditButton
                Images.NormalIndex = 1
                Images.HotIndex = 1
                Images.PressedIndex = 1
                Images.DisabledIndex = 1
                Style = ebsGlyphEh
                Width = 30
                DrawBackTime = edbtWhenHotEh
              end
              item
                Action = CmdFileView
                Images.NormalImages = DataMain.ImageEditButton
                Images.HotImages = DataMain.ImageEditButton
                Images.PressedImages = DataMain.ImageEditButton
                Images.DisabledImages = DataMain.ImageEditButton
                Images.NormalIndex = 2
                Images.HotIndex = 2
                Images.PressedIndex = 2
                Images.DisabledIndex = 2
                Style = ebsGlyphEh
                Width = 30
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaGhiChu: TisPanel
          Left = 0
          Top = 635
          Width = 453
          Height = 87
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvNone
          Color = 16119285
          ParentBackground = False
          TabOrder = 1
          HeaderCaption = ' .: Ghi ch'#250
          HeaderColor = clHighlight
          ImageSet = 4
          RealHeight = 0
          ShowButton = True
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = ANSI_CHARSET
          HeaderFont.Color = clWhite
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = [fsBold]
          object EdGHICHU: TDBMemo
            Left = 1
            Top = 17
            Width = 451
            Height = 69
            Align = alClient
            BorderStyle = bsNone
            DataField = 'GhiChu'
            DataSource = DsMaster
            TabOrder = 1
          end
        end
        object PaThongTin: TisPanel
          Left = 0
          Top = 0
          Width = 453
          Height = 581
          Align = alTop
          BevelOuter = bvNone
          Color = 16119285
          ParentBackground = False
          TabOrder = 2
          HeaderCaption = ' :: Th'#244'ng tin c'#7845'p thi'#7871't b'#7883' - t'#224'i s'#7843'n'
          HeaderColor = clHighlight
          ImageSet = 4
          RealHeight = 0
          ShowButton = False
          HeaderBevelInner = bvNone
          HeaderBevelOuter = bvNone
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWhite
          HeaderFont.Height = -11
          HeaderFont.Name = 'Tahoma'
          HeaderFont.Style = [fsBold]
          object gbViecLam: TGroupBox
            Left = 0
            Top = 16
            Width = 453
            Height = 217
            Align = alTop
            Caption = '  Th'#244'ng tin  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            object PaInfo: TPanel
              Left = 2
              Top = 39
              Width = 449
              Height = 175
              Align = alTop
              BevelOuter = bvNone
              Color = 16119285
              ParentBackground = False
              TabOrder = 1
              DesignSize = (
                449
                175)
              object CbTenThietBi: TDbLookupComboboxEh2
                Left = 111
                Top = 2
                Width = 253
                Height = 22
                ControlLabel.Width = 43
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Thi'#7871't b'#7883
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaThietBi'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenThietBi'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaThietBi'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsV_HR_THIETBI_TAISAN
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaThietBi'
                ListField = 'TenThietBi'
                ListSource = HrDataMain.DsV_HR_THIETBI_TAISAN
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object CbXuatXu: TDbLookupComboboxEh2
                Left = 111
                Top = 122
                Width = 135
                Height = 22
                ControlLabel.Width = 44
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Xu'#7845't x'#7913
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                DynProps = <>
                DataField = 'MaQuocGia'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TEN'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end>
                DropDownBox.ListSource = DataMain.DsV_QuocGia
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdQuocGia
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MA'
                ListField = 'TEN'
                ListSource = DataMain.DsV_QuocGia
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 7
                Visible = True
              end
              object DBEditEh1: TDBEditEh
                Left = 111
                Top = 74
                Width = 333
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 77
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#224' cung c'#7845'p'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'NhaCungCap'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 5
                Visible = True
              end
              object DBEditEh2: TDBEditEh
                Left = 111
                Top = 50
                Width = 253
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 76
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#243'm thi'#7871't b'#7883
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_TenThietBi_Nhom'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object DBEditEh3: TDBEditEh
                Left = 111
                Top = 146
                Width = 135
                Height = 22
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 34
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Model'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'Model'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 8
                Visible = True
              end
              object DBEditEh4: TDBEditEh
                Left = 309
                Top = 146
                Width = 135
                Height = 22
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 33
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Serial'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'Serial'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 9
                Visible = True
              end
              object DBEditEh5: TDBEditEh
                Left = 111
                Top = 98
                Width = 333
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 74
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#224' s'#7843'n xu'#7845't'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'NhaSanXuat'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 6
                Visible = True
              end
              object EdMaThietBi: TDBEditEh
                Left = 367
                Top = 2
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaThietBi'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdThietBi
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object EdTenThietBi_TA: TDBEditEh
                Left = 111
                Top = 26
                Width = 333
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 79
                ControlLabel.Height = 16
                ControlLabel.Caption = 'T'#234'n ti'#7871'ng anh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_TenThietBi_TA'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
                Visible = True
              end
              object EdThietBi_Dvt: TDBEditEh
                Left = 399
                Top = 50
                Width = 45
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 25
                ControlLabel.Height = 16
                ControlLabel.Caption = #272'VT'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_Dvt'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 4
                Visible = True
              end
            end
            object PaNhanVien: TPanel
              Left = 2
              Top = 15
              Width = 449
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              Color = 16119285
              ParentBackground = False
              TabOrder = 0
              DesignSize = (
                449
                24)
              object CbNhanVien: TDbLookupComboboxEh2
                Left = 111
                Top = 2
                Width = 253
                Height = 22
                ControlLabel.Width = 56
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'Manv'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'Tennv'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 110
                  end
                  item
                    FieldName = 'ManvQL'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = DsEmp
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Manv'
                ListField = 'Tennv'
                ListSource = HrDataMain.DsDMNV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object EdManvQL: TDBEditEh
                Left = 367
                Top = 2
                Width = 77
                Height = 22
                TabStop = False
                Alignment = taLeftJustify
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_ManvQL'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
            end
          end
          object GroupBox1: TGroupBox
            Left = 0
            Top = 233
            Width = 453
            Height = 124
            Align = alTop
            Caption = '  C'#7845'p ph'#225't  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            DesignSize = (
              453
              124)
            object Label13: TLabel
              Left = 170
              Top = 46
              Width = 42
              Height = 16
              Alignment = taRightJustify
              Caption = '(th'#225'ng)'
              FocusControl = DBNumberEditEh1
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label12: TLabel
              Left = 235
              Top = 48
              Width = 103
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y '#273#432#7907'c s'#7903' h'#7919'u'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 256
            end
            object Label2: TLabel
              Left = 285
              Top = 25
              Width = 52
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y c'#7845'p'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 306
            end
            object DBNumberEditEh1: TDBNumberEditEh
              Left = 111
              Top = 20
              Width = 54
              Height = 22
              ControlLabel.Width = 51
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' l'#432#7907'ng'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'SoLuong'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 0
              Visible = True
            end
            object DBNumberEditEh3: TDBNumberEditEh
              Left = 111
              Top = 44
              Width = 54
              Height = 22
              ControlLabel.Width = 97
              ControlLabel.Height = 16
              ControlLabel.Caption = 'H'#7841'n '#273#432#7907'c s'#7903' h'#7919'u'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'HanDuocSoHuu'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object wwDBDateTimePicker2: TwwDBDateTimePicker
              Left = 343
              Top = 44
              Width = 101
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              Color = clBtnFace
              DataField = 'NgaySoHuu'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ShowButton = True
              TabOrder = 3
            end
            object wwDBEdit7: TDBEditEh
              Left = 111
              Top = 68
              Width = 333
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 61
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i giao'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiGiao'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object wwDBEdit3: TDBEditEh
              Left = 111
              Top = 92
              Width = 333
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 65
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i nh'#7853'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiNhan'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object wwDBDateTimePicker3: TwwDBDateTimePicker
              Left = 343
              Top = 20
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayCapPhat'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ShowButton = True
              TabOrder = 1
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 357
            Width = 453
            Height = 76
            Align = alTop
            Caption = '  B'#7843'o h'#224'nh  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            DesignSize = (
              453
              76)
            object Label8: TLabel
              Left = 170
              Top = 50
              Width = 42
              Height = 16
              Alignment = taRightJustify
              Caption = '(th'#225'ng)'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label3: TLabel
              Left = 262
              Top = 47
              Width = 75
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y h'#7871't h'#7841'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 283
            end
            object Label1: TLabel
              Left = 262
              Top = 23
              Width = 75
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y b'#7855't '#273#7847'u'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 283
            end
            object DBNumberEditEh2: TDBNumberEditEh
              Left = 110
              Top = 48
              Width = 54
              Height = 22
              ControlLabel.Width = 79
              ControlLabel.Height = 16
              ControlLabel.Caption = 'H'#7841'n b'#7843'o h'#224'nh'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'HanBaoHanh'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 0
              Visible = True
            end
            object CbToDate: TwwDBDateTimePicker
              Left = 343
              Top = 20
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayBatDau'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 1
            end
            object wwDBDateTimePicker1: TwwDBDateTimePicker
              Left = 343
              Top = 44
              Width = 101
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              Color = clBtnFace
              DataField = 'NgayKetThuc'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ShowButton = True
              TabOrder = 2
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 433
            Width = 453
            Height = 148
            Align = alTop
            Caption = '  Ho'#224'n tr'#7843'  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            DesignSize = (
              453
              148)
            object Label6: TLabel
              Left = 247
              Top = 47
              Width = 90
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y tr'#7843' t'#224'i s'#7843'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object EdNSoLuongTraTaiSan: TDBNumberEditEh
              Left = 111
              Top = 44
              Width = 54
              Height = 22
              ControlLabel.Width = 51
              ControlLabel.Height = 16
              ControlLabel.Caption = 'S'#7889' l'#432#7907'ng'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'SoLuongTraTaiSan'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object dpNgayTraTaiSan: TwwDBDateTimePicker
              Left = 343
              Top = 44
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayTraTaiSan'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ShowButton = True
              TabOrder = 2
            end
            object rDBCheckBox1: TrDBCheckBox
              Left = 111
              Top = 20
              Width = 140
              Height = 22
              Caption = 'Ho'#224'n tr'#7843' khi th'#244'i vi'#7879'c '
              DataField = 'Co_HoanTra'
              DataSource = DsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              ValueChecked = 'True'
              ValueUnchecked = 'False'
              ShowFieldCaption = False
              UpdateAfterClick = True
            end
            object EdNguoiGiaoTraTaiSan: TDBEditEh
              Left = 111
              Top = 92
              Width = 333
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 61
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i giao'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiGiaoTraTaiSan'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 4
              Visible = True
            end
            object EdNguoiNhanTraTaiSan: TDBEditEh
              Left = 111
              Top = 116
              Width = 333
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 65
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ng'#432#7901'i nh'#7853'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'NguoiNhanTraTaiSan'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object EdLyDoTraTaiSan: TDBEditEh
              Left = 111
              Top = 68
              Width = 333
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 30
              ControlLabel.Height = 16
              ControlLabel.Caption = 'L'#253' do'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'LyDoTraTaiSan'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 3
              Visible = True
            end
          end
        end
      end
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 84
    Width = 915
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 84
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
      OnExecute = CmdPrint2Execute
    end
    object CmdLoaiHopDong: TAction
    end
    object CmdThietBi: TAction
      OnExecute = CmdThietBiExecute
    end
    object CmdQuocGia: TAction
      OnExecute = CmdQuocGiaExecute
    end
    object CmdFilePlus: TAction
      Caption = ' '
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Caption = ' '
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv,a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.M' +
        'aBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    AfterDelete = QrMasterAfterDelete
    OnCalcFields = QrMasterCalcFields
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_THIETBI'
      ' where'#9'1 = 1')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
    end
    object QrMasterFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrMasterFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterMaThietBi: TWideStringField
      FieldName = 'MaThietBi'
      OnChange = QrMasterMaThietBiChange
    end
    object QrMasterSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrMasterNgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
      OnChange = QrMasterNgayBatDauChange
    end
    object QrMasterNgayKetThuc: TDateTimeField
      FieldName = 'NgayKetThuc'
    end
    object QrMasterHanDuocSoHuu: TFloatField
      FieldName = 'HanDuocSoHuu'
      OnChange = QrMasterNgayCapPhatChange
    end
    object QrMasterNgaySoHuu: TDateTimeField
      FieldName = 'NgaySoHuu'
    end
    object QrMasterNguoiGiao: TWideStringField
      FieldName = 'NguoiGiao'
      Size = 200
    end
    object QrMasterNguoiNhan: TWideStringField
      FieldName = 'NguoiNhan'
      Size = 200
    end
    object QrMasterGhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrMasterLK_TenThietBi: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenThietBi'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'TenThietBi'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_Dvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Dvt'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'Dvt'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_MaThietBi_Nhom: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaThietBi_Nhom'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'MaThietBi_Nhom'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_TenThietBi_Nhom: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenThietBi_Nhom'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN_NHOM
      LookupKeyFields = 'MaThietBi_Nhom'
      LookupResultField = 'TenThietBi_Nhom'
      KeyFields = 'LK_MaThietBi_Nhom'
      Lookup = True
    end
    object QrMasterLK_SoThang_HanBaoHanh: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_SoThang_HanBaoHanh'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'HanBaoHanh'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_SoThang_HanDuocSoHuu: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_SoThang_HanDuocSoHuu'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'HanDuocSoHuu'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterNhaCungCap: TWideStringField
      FieldName = 'NhaCungCap'
      Size = 200
    end
    object QrMasterNhaSanXuat: TWideStringField
      FieldName = 'NhaSanXuat'
      Size = 200
    end
    object QrMasterMaQuocGia: TWideStringField
      FieldName = 'MaQuocGia'
      Size = 5
    end
    object QrMasterModel: TWideStringField
      FieldName = 'Model'
      Size = 50
    end
    object QrMasterSerial: TWideStringField
      FieldName = 'Serial'
      Size = 50
    end
    object QrMasterHanBaoHanh: TFloatField
      FieldName = 'HanBaoHanh'
      OnChange = QrMasterNgayBatDauChange
    end
    object QrMasterLK_TenThietBi_TA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenThietBi_TA'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'TenThietBi_TA'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_TenQuocGia: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuocGia'
      LookupDataSet = DataMain.QrQuocgia
      LookupKeyFields = 'MA'
      LookupResultField = 'TEN'
      KeyFields = 'MaQuocGia'
      Size = 200
      Lookup = True
    end
    object QrMasterCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Calculated = True
    end
    object QrMasterNgayCapPhat: TDateTimeField
      FieldName = 'NgayCapPhat'
      OnChange = QrMasterNgayCapPhatChange
    end
    object QrMasterCo_HoanTra: TBooleanField
      FieldName = 'Co_HoanTra'
    end
    object QrMasterNgayTraTaiSan: TDateTimeField
      FieldName = 'NgayTraTaiSan'
    end
    object QrMasterNguoiGiaoTraTaiSan: TWideStringField
      FieldName = 'NguoiGiaoTraTaiSan'
      Size = 200
    end
    object QrMasterNguoiNhanTraTaiSan: TWideStringField
      FieldName = 'NguoiNhanTraTaiSan'
      Size = 200
    end
    object QrMasterSoLuongTraTaiSan: TFloatField
      FieldName = 'SoLuongTraTaiSan'
    end
    object QrMasterLyDoTraTaiSan: TWideStringField
      FieldName = 'LyDoTraTaiSan'
      Size = 200
    end
    object QrMasterLK_Co_HoanTra: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_HoanTra'
      LookupDataSet = HrDataMain.QrDM_THIETBI_TAISAN
      LookupKeyFields = 'MaThietBi'
      LookupResultField = 'Co_HoanTra'
      KeyFields = 'MaThietBi'
      Lookup = True
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object spHR_TINH_HETHAN_HDLD: TADOCommand
    CommandText = 'spHR_TINH_HETHAN_HDLD;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NGAY_HIEULUC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MA_HDLD'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@NGAY_HETHAN'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 115
    Top = 173
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'FileIdx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :FileIdx')
    Left = 188
    Top = 348
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 52
    Top = 200
  end
end
