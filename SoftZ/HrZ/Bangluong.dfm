object FrmBangluong: TFrmBangluong
  Left = 175
  Top = 91
  Caption = 'B'#7843'ng L'#432#417'ng'
  ClientHeight = 573
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000000040000000000000000000000000000000000000000
    0000C57A2A4DC0782678BA73219FB46D1CC0AF6717DAAB6414EEAA6413FBAA64
    13FBAA6213EEA96013DAA85C13C0A758149FA5531478A44D144D000000000000
    0000AA6413FFBC7323FFC27828FFC4792BFFC5782CFFBF6927FFB55820FFB155
    1DFFAC501AFFA74E17FFA54D15FFA44F14FFA55413FFAA6413FF000000000000
    0000C073274DB1671BE5B86527FFBA6628FFC3702CFFBF6928FFE1BAA0FFFFFF
    FFFFAC511CFFA24519FF9F4117FFA24617FFA75A14E5A44D144D000000000000
    000000000000AB6414ECB45E24FFBC6629FFCB7E3DFFF7ECE3FFFFFFFFFFFAF5
    F1FFCD916BFFAB511EFFA24619FFA44818FFAA6213EC00000000000000000000
    000000000000AA6413FDC7702DFFC8752FFFCB7831FFD28F58FFC26F2CFFC87F
    46FFFFFFFFFFBF7341FFAF561FFFAB511CFFAA6413FD00000000000000000000
    000000000000AC6515ECCA7930FFCE7D34FFCD7D33FFCB7B33FFC87731FFD498
    65FFFFFFFFFFBF7136FFB76124FFB25C20FFAA6314EC00000000000000000000
    000000000000B1691ACDCA7D30FFD28438FFD18538FFD99C5FFFF5E6D7FFFFFF
    FFFFDCAC82FFC16F2CFFBD6828FFB66221FFAC6216CD00000000000000000000
    000000000000B87021A2C57C2CFFD68C3CFFD68D3DFFFCF7F2FFEDD0B0FFD18A
    44FFCB7C34FFC77630FFC26F2CFFB76720FFB0631AA200000000000000000000
    000000000000C27B2A6EBD7726FFD99340FFDA9441FFFFFFFFFFE1AD71FFD38A
    3BFFD59352FFCB7C33FFC7752FFFB4681DFFB5651E6E00000000000000000000
    000000000000CF873632B36E1CEBD99540FFDD9B45FFE8BB82FFFDF8F2FFFFFF
    FFFFF6E7D8FFCF8136FFC8772FFFAF6718EB12F7FEFF00000000000000000000
    00000000000000000000BF7A2893C98530FFE0A048FFDF9E46FFFFFFFFFFEFD1
    ACFFD58C3CFFD18438FFBE7225FF12F7FEFF0282BCFF00000000000000000000
    00000000000000000000D28F3A38B6711FCED7983FFFE0A048FFDD9B45FFDA94
    41FFD68D3DFFCB7F32FF12F7FEFF0282BCFF0000000000000000000000000000
    0000000000000000000000000000CD8A3557B87421D70282BCFF0282BCFF0282
    BCFF0282BCFF0282BCFF0282BCFF000000000000000000000000000000000000
    0000000000000000000000000000000000000282BCFF12F7FEFF12F7FEFF12F7
    FEFF12F7FEFF0282BCFF00000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000BE7827FFDCA464FFBD74
    22FFB56E1EF60000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000BD7625FFB56C1FFFB56C1FFFB56B
    1DFFB46A1CFFB46A1CF600000000000000000000000000000000000000008001
    00008001000080010000C0030000C0030000C0030000C0030000C0030000C003
    0000C0030000E0030000E0070000F00F0000F81F0000FC3F0000F81F0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 72
    Caption = 'ToolBar1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton8: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdUpdate
      ImageIndex = 30
    end
    object ToolButton5: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdCalc2
      ImageIndex = 36
    end
    object ToolButton6: TToolButton
      Left = 152
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 160
      Top = 0
      Cursor = 1
      Action = CmdParams
      ImageIndex = 34
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdBangCong
      ImageIndex = 31
    end
    object ToolButton12: TToolButton
      Left = 312
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXuly: TToolButton
      Left = 320
      Top = 0
      Cursor = 1
      Hint = 'X'#7917' l'#253' b'#7843'ng l'#432#417'ng'
      Caption = 'C'#244'ng c'#7909
      DropdownMenu = PopXuly
      ImageIndex = 35
      ParentShowHint = False
      ShowHint = True
      Style = tbsDropDown
      Visible = False
      OnClick = BtnXulyClick
    end
    object ToolButton9: TToolButton
      Left = 407
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 6
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton1: TToolButton
      Left = 415
      Top = 0
      Cursor = 1
      Action = CmdRep
      ImageIndex = 4
    end
    object ToolButton2: TToolButton
      Left = 487
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 495
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Gr1: TwwDBGrid2
    Left = 0
    Top = 44
    Width = 792
    Height = 508
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MANV'#9'12'#9'M'#227' s'#7889#9'F'
      'LK_TENNV'#9'25'#9'H'#7885' v'#224' t'#234'n '#9'F'
      'NgayCongChuan'#9'6'#9'Ng'#224'y c'#244'ng'#9'T'#9'Ng'#224'y c'#244'ng'
      'NgayCongThucTe'#9'6'#9'Th'#7921'c'#9'F'#9'Ng'#224'y c'#244'ng'
      'NgayNghiPhepNam'#9'7'#9'Ph'#233'p n'#259'm'#9'F'#9'Ng'#224'y ngh'#7881
      'NgayNghiCoLuong'#9'7'#9'C'#243' l'#432#417'ng'#9'F'#9'Ng'#224'y ngh'#7881
      'NgayNghiLeTet'#9'7'#9'L'#7877' t'#7871't'#9'F'#9'Ng'#224'y ngh'#7881
      'NgayNghiKhongLuong'#9'7'#9'Kh'#244'ng l'#432#417'ng'#9'F'#9'Ng'#224'y ngh'#7881
      'NgayCongHuongLuong'#9'7'#9'h'#432#7903'ng l'#432#417'ng'#9'F'#9'Ng'#224'y c'#244'ng'
      'LuongChinh'#9'12'#9'c'#417' b'#7843'n'#9'F'#9'L'#432#417'ng'
      'LuongBaoHiem'#9'12'#9'Ph'#7909' c'#7845'p'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 2
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsPayroll
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = Pop1
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = Gr1CalcCellColors
    OnDblClick = CmdDetailExecute
    OnEnter = CmdRefreshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  inline frMY: TfrMY
    Left = 0
    Top = 44
    Width = 792
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 44
    ExplicitWidth = 792
    inherited Panel1: TPanel
      Width = 792
      ExplicitWidth = 792
      inherited CbOrg: TfcTreeCombo
        Items.StreamVersion = 1
        Items.Data = {00000000}
      end
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end>
    UseSystemFont = False
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 205
    Top = 324
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdCalc2: TAction
      Category = 'XULY'
      Caption = 'T'#237'nh l'#432#417'ng'
      ShortCut = 118
      OnExecute = CmdCalc2Execute
    end
    object CmdParams: TAction
      Category = 'XULY'
      Caption = 'Th'#244'ng s'#7889
      OnExecute = CmdParamsExecute
    end
    object CmdRep: TAction
      Caption = 'B'#225'o c'#225'o'
      Hint = 'C'#225'c b'#225'o c'#225'o l'#432#417'ng'
      OnExecute = CmdRepExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdCalc1: TAction
      Category = '1'
      Caption = 'T'#237'nh l'#432#417'ng    '
      ShortCut = 16502
      OnExecute = CmdCalc1Execute
    end
    object CmdCsv: TAction
      Category = 'XULY'
      Caption = ':: L'#7845'y d'#7919' li'#7879'u b'#7893' sung'
      Visible = False
      OnExecute = CmdCsvExecute
    end
    object CmdDetail: TAction
      OnExecute = CmdDetailExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdUpdate: TAction
      Category = 'XULY'
      Caption = 'C'#7853'p nh'#7853't'
      Hint = 
        'C'#7853'p nh'#7853't danh s'#225'ch, '#273'i'#7873'n '#273#250'ng c'#7845'u tr'#250'c ph'#242'ng ban, l'#432#417'ng c'#417' b'#7843'n..' +
        '.'
      ShortCut = 120
      OnExecute = CmdUpdateExecute
    end
    object CmdErase: TAction
      Category = 'XULY'
      Caption = ':: X'#243'a d'#7919' li'#7879'u b'#7893' sung'
      Visible = False
      OnExecute = CmdEraseExecute
    end
    object CmdPaynet: TAction
      Caption = 'Paynet'
      Hint = 'Chuy'#7875'n l'#432#417'ng qua HSBC'
      Visible = False
      OnExecute = CmdPaynetExecute
    end
    object CmdApproved: TAction
      Caption = 'Duy'#7879't'
      Hint = 'Duy'#7879't b'#7843'ng l'#432#417'ng'
      OnExecute = CmdApprovedExecute
    end
    object CmdBangCong: TAction
      Caption = 'B'#7843'ng c'#244'ng'
      OnExecute = CmdBangCongExecute
    end
  end
  object QrPayroll: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    AfterPost = QrPayrollAfterPost
    OnCalcFields = QrPayrollCalcFields
    Parameters = <
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_BANG_LUONG'
      ' where'#9'[THANG] = :THANG'
      '   and'#9'[NAM] '#9'= :NAM'
      '   and'#9'%s'
      'order by [MaChiNhanh], [MaPhongBan], Manv')
    Left = 204
    Top = 260
    object QrPayrollNAM: TIntegerField
      FieldName = 'NAM'
      Visible = False
    end
    object QrPayrollTHANG: TIntegerField
      FieldName = 'THANG'
      Visible = False
    end
    object QrPayrollMANV: TWideStringField
      FieldName = 'Manv'
      Visible = False
      Size = 25
    end
    object QrPayrollMaPhongBan: TWideStringField
      FieldName = 'MaPhongBan'
      Visible = False
      Size = 41
    end
    object QrPayrollMaBoPhan: TWideStringField
      FieldName = 'MaBoPhan'
      Visible = False
      Size = 62
    end
    object QrPayrollMaNhomLV: TWideStringField
      FieldName = 'MaNhomLV'
      Visible = False
    end
    object QrPayrollMaChucDanh: TWideStringField
      FieldName = 'MaChucDanh'
      Visible = False
    end
    object QrPayrollGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Visible = False
      Size = 200
    end
    object QrPayrollCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrPayrollUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrPayrollCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrPayrollUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrPayrollLK_TENNV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNV'
      LookupDataSet = QrEmp
      LookupKeyFields = 'MANV'
      LookupResultField = 'TENNV'
      KeyFields = 'MANV'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrPayrollLK_TenPhongBan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'TenPhongBan'
      KeyFields = 'MaPhongBan'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrPayrollLK_TenBoPhan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBoPhan'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'TenBoPhan'
      KeyFields = 'MaBoPhan'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrPayrollMATK: TWideStringField
      FieldName = 'MATK'
      Visible = False
    end
    object QrPayrollMANH: TWideStringField
      FieldName = 'MANH'
      Visible = False
      Size = 10
    end
    object QrPayrollMACN: TWideStringField
      FieldName = 'MANH_CN'
      Visible = False
      Size = 26
    end
    object QrPayrollLK_MANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH'
      LookupDataSet = HrDataMain.QrDMTK_HR
      LookupKeyFields = 'MATK'
      LookupResultField = 'MaNH'
      KeyFields = 'MATK'
      Visible = False
      Size = 100
      Lookup = True
    end
    object QrPayrollLK_MACN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH_CN'
      LookupDataSet = HrDataMain.QrDMTK_HR
      LookupKeyFields = 'MATK'
      LookupResultField = 'MANH_CN'
      KeyFields = 'MATK'
      Visible = False
      Size = 100
      Lookup = True
    end
    object QrPayrollLK_TENNH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNH'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'LK_MANH'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrPayrollLK_TENNH_CN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNH_CN'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MANH_CN'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'LK_MANH_CN'
      Visible = False
      Size = 200
      Lookup = True
    end
    object QrPayrollMaChiNhanh: TWideStringField
      FieldName = 'MaChiNhanh'
    end
    object QrPayrollLK_TenChiNhanh: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenChiNhanh'
      LookupDataSet = DataMain.QrSite
      LookupKeyFields = 'MaChiNhanh'
      LookupResultField = 'TenChiNhanh'
      KeyFields = 'MaChiNhanh'
      Size = 200
      Lookup = True
    end
    object QrPayrollLK_TenChucDanh: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'TenChucDanh'
      KeyFields = 'MaChucDanh'
      Size = 200
      Lookup = True
    end
    object QrPayrollLK_TenNhomLV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhomLV'
      LookupDataSet = HrDataMain.QrDM_NHOMLV
      LookupKeyFields = 'MaNhomLV'
      LookupResultField = 'TenNhomLV'
      KeyFields = 'MaNhomLV'
      Size = 200
      Lookup = True
    end
    object QrPayrollManvQL: TWideStringField
      FieldName = 'ManvQL'
    end
    object QrPayrollNgayVaoLam: TDateTimeField
      FieldName = 'NgayVaoLam'
    end
    object QrPayrollNgayBatDauHocViec: TDateTimeField
      FieldName = 'NgayBatDauHocViec'
    end
    object QrPayrollNgayKetThucHocViec: TDateTimeField
      FieldName = 'NgayKetThucHocViec'
    end
    object QrPayrollNgayBatDauThuViec: TDateTimeField
      FieldName = 'NgayBatDauThuViec'
    end
    object QrPayrollNgayKetThucThuViec: TDateTimeField
      FieldName = 'NgayKetThucThuViec'
    end
    object QrPayrollNgayVaoLamChinhThuc: TDateTimeField
      FieldName = 'NgayVaoLamChinhThuc'
    end
    object QrPayrollNgayThoiViec: TDateTimeField
      FieldName = 'NgayThoiViec'
    end
    object QrPayrollCalc_NgayVaoLam: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayVaoLam'
      Calculated = True
    end
    object QrPayrollCalc_NgayBatDauHocViec: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayBatDauHocViec'
      Calculated = True
    end
    object QrPayrollCalc_NgayKetThucHocViec: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayKetThucHocViec'
      Calculated = True
    end
    object QrPayrollCalc_NgayBatDauThuViec: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayBatDauThuViec'
      Calculated = True
    end
    object QrPayrollCalc_NgayKetThucThuViec: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayKetThucThuViec'
      Calculated = True
    end
    object QrPayrollCalc_NgayVaoLamChinhThuc: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayVaoLamChinhThuc'
      Calculated = True
    end
    object QrPayrollCalc_NgayThoiViec: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'Calc_NgayThoiViec'
      Calculated = True
    end
  end
  object DsPayroll: TDataSource
    DataSet = QrPayroll
    Left = 204
    Top = 292
  end
  object Filter: TwwFilterDialog2
    DataSource = DsPayroll
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Name'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Base Salary')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdDayMonthYear
    SQLTables = <>
    Left = 236
    Top = 292
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'a.Manv, a.Tennv, a.NgayVaoLam'
      '  from'#9'HR_DM_NHANVIEN a'
      ' where'#9'1=1'
      'order by a.Manv')
    Left = 172
    Top = 260
  end
  object PopXuly: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 236
    Top = 324
    object Lydliubsung1: TMenuItem
      Action = CmdCsv
    end
    object Xadliubsung1: TMenuItem
      Action = CmdErase
    end
  end
  object Pop1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = Pop1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 268
    Top = 324
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object ItemCalc1: TMenuItem
      Action = CmdCalc1
      ImageIndex = 28
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 236
    Top = 356
  end
  object spHR_TINHLUONG: TADOCommand
    CommandText = 'spHR_TINHLUONG;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pManv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 172
    Top = 292
  end
  object QrCsv: TADOQuery
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=HRMS_' +
      'TEXT'
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'"Daily.csv"'
      ' ')
    Left = 172
    Top = 324
  end
  object CSV: TADOCommand
    CommandText = 'update HR_BANG_LUONG'#13#10'set MANV = '#39#39#13#10
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <>
    Left = 204
    Top = 356
  end
  object PAYROLL_ERASE: TADOCommand
    CommandText = 'update HR_BANG_LUONG'#13#10'set MANV = :MANV'#13#10
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'MANV'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    Left = 268
    Top = 356
  end
  object spUpdateApproved: TADOCommand
    CommandText = 
      'update'#9'HR_BANG_THONGSO'#13#10'set '#9'[DuyetLuong] = :DuyetLuong'#13#10'where '#9 +
      '[NAM] = :NAM'#13#10'and'#9'[THANG] = :THANG'#13#10
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'DuyetLuong'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'NAM'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'THANG'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 348
    Top = 348
  end
end
