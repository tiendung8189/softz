object FrmDmDiaDiemLamViec: TFrmDmDiaDiemLamViec
  Left = 177
  Top = 132
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c '#272#7883'a '#272'i'#7875'm L'#224'm Vi'#7879'c'
  ClientHeight = 409
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 695
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 720
  end
  object Status: TStatusBar
    Left = 0
    Top = 388
    Width = 695
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton8: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 695
    Height = 348
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 695
    Height = 348
    Cursor = 1
    ActivePage = tsDep
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object tsDep: TTabSheet
      Caption = #272#7883'a '#273'i'#7875'm'
      ImageIndex = 1
      ExplicitHeight = 483
      object GrDiaDiem: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 687
        Height = 320
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'LK_NHOM;CustomEdit;CbNhom;F'
          'LK_KhuVuc;CustomEdit;CbDiaDiemKhuVuc;F')
        Selected.Strings = (
          'LK_KhuVuc'#9'200'#9'Khu v'#7921'c l'#224'm vi'#7879'c'#9'F'
          'PhuCap_NoiLV'#9'10'#9'PhuCap_NoiLV'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsDiaDiem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopCommon
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrDiaDiemCalcCellColors
        TitleImageList = DataMain.ImageSort
        GroupFieldName = 'LK_KhuVuc'
        ExplicitHeight = 483
      end
    end
    object tsBehaviour: TTabSheet
      Caption = 'V'#249'ng/Mi'#7873'n'
      object GrKhuVuc: TwwDBGrid2
        Left = 0
        Top = 0
        Width = 687
        Height = 320
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'LK_TEN_LOAINHOM;CustomEdit;CbPLOAI;F')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        Align = alClient
        DataSource = DsKhuVuc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopCommon
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnCalcCellColors = GrKhuVucCalcCellColors
        TitleImageList = DataMain.ImageSort
      end
    end
  end
  object CbDiaDiemKhuVuc: TwwDBLookupCombo
    Left = 23
    Top = 127
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TenNoiLV_KhuVuc'#9'30'#9'TenNoiLV_KhuVuc'#9'F')
    DataField = 'MaNoiLV_KhuVuc'
    DataSource = DsDiaDiem
    LookupTable = RefKhuVuc
    LookupField = 'MaNoiLV_KhuVuc'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
    OnNotInList = CbDiaDiemKhuVucNotInList
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 229
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin'
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
  end
  object QrKhuVuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrKhuVucBeforeInsert
    BeforePost = QrKhuVucBeforePost
    AfterPost = QrKhuVucAfterPost
    BeforeDelete = QrKhuVucBeforeDelete
    OnDeleteError = QrKhuVucPostError
    OnPostError = QrKhuVucPostError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from'#9'HR_DM_NOI_LAMVIEC_KHUVUC'
      'order by'#9'MaNoiLV_KhuVuc')
    Left = 61
    Top = 168
    object QrKhuVucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrKhuVucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrKhuVucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrKhuVucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrKhuVucMaNoiLV_KhuVuc: TWideStringField
      FieldName = 'MaNoiLV_KhuVuc'
    end
    object QrKhuVucTenNoiLV_KhuVuc: TWideStringField
      FieldName = 'TenNoiLV_KhuVuc'
      Size = 200
    end
    object QrKhuVucTenNoiLV_KhuVuc_TA: TWideStringField
      FieldName = 'TenNoiLV_KhuVuc_TA'
      Size = 200
    end
  end
  object DsKhuVuc: TDataSource
    DataSet = QrKhuVuc
    Left = 61
    Top = 196
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 197
    Top = 168
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 197
    Top = 228
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrDiaDiem: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrKhuVucBeforeInsert
    BeforePost = QrDiaDiemBeforePost
    BeforeDelete = QrKhuVucBeforeDelete
    OnDeleteError = QrKhuVucPostError
    OnPostError = QrKhuVucPostError
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  HR_DM_NOI_LAMVIEC  '
      'order by MaNoiLV_KhuVuc, MaNoiLV')
    Left = 33
    Top = 168
    object QrDiaDiemCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDiaDiemUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDiaDiemCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDiaDiemUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDiaDiemLK_NHOM: TWideStringField
      DisplayLabel = 'Khu v'#7921'c l'#224'm vi'#7879'c'
      FieldKind = fkLookup
      FieldName = 'LK_KhuVuc'
      LookupDataSet = RefKhuVuc
      LookupKeyFields = 'MaNoiLV_KhuVuc'
      LookupResultField = 'TenNoiLV_KhuVuc'
      KeyFields = 'MaNoiLV_KhuVuc'
      Size = 200
      Lookup = True
    end
    object QrDiaDiemMaNoiLV: TWideStringField
      FieldName = 'MaNoiLV'
    end
    object QrDiaDiemMaNoiLV_KhuVuc: TWideStringField
      FieldName = 'MaNoiLV_KhuVuc'
    end
    object QrDiaDiemTenNoiLV: TWideStringField
      FieldName = 'TenNoiLV'
      Size = 200
    end
    object QrDiaDiemTenNoiLV_TA: TWideStringField
      FieldName = 'TenNoiLV_TA'
      Size = 200
    end
    object QrDiaDiemDiaChi: TWideStringField
      FieldName = 'DiaChi'
      Size = 200
    end
    object QrDiaDiemPhuCap_NoiLV: TFloatField
      FieldName = 'PhuCap_NoiLV'
    end
  end
  object DsDiaDiem: TDataSource
    DataSet = QrDiaDiem
    Left = 33
    Top = 196
  end
  object RefKhuVuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforePost = QrDiaDiemBeforePost
    BeforeDelete = QrKhuVucBeforeDelete
    OnDeleteError = QrKhuVucPostError
    OnPostError = QrKhuVucPostError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NOI_LAMVIEC_KHUVUC')
    Left = 89
    Top = 168
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDiaDiem
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MaNoiLV'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MaNoiLV')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdDayMonthYear
    SQLTables = <>
    Left = 197
    Top = 196
  end
end
