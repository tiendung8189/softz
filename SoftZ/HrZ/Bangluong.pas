﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Bangluong;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids,
  Wwdbgrid, StdCtrls, Db, ADODB, wwfltdlg,
  Menus, AdvMenus, AppEvnts, fctreecombo, wwDBGrid2,
  wwFltDlg2, frameMY, wwDialog, Wwdbigrd, ToolWin;

type
  TFrmBangluong = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdSearch: TAction;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    Gr1: TwwDBGrid2;
    CmdRefresh: TAction;
    QrPayroll: TADOQuery;
    DsPayroll: TDataSource;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    QrEmp: TADOQuery;
    CmdCalc2: TAction;
    BtnXuly: TToolButton;
    ToolButton5: TToolButton;
    PopXuly: TAdvPopupMenu;
    CmdParams: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdRep: TAction;
    Pop1: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    Lcdliu1: TMenuItem;
    Hinttc1: TMenuItem;
    N5: TMenuItem;
    CmdClearFilter: TAction;
    ApplicationEvents1: TApplicationEvents;
    spHR_TINHLUONG: TADOCommand;
    CmdCalc1: TAction;
    ItemCalc1: TMenuItem;
    CmdCsv: TAction;
    Lydliubsung1: TMenuItem;
    CmdDetail: TAction;
    CmdReload: TAction;
    QrCsv: TADOQuery;
    CSV: TADOCommand;
    CmdUpdate: TAction;
    N4: TMenuItem;
    CmdErase: TAction;
    Xadliubsung1: TMenuItem;
    PAYROLL_ERASE: TADOCommand;
    frMY: TfrMY;
    Status: TStatusBar;
    CmdPaynet: TAction;
    spUpdateApproved: TADOCommand;
    CmdApproved: TAction;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    QrPayrollNAM: TIntegerField;
    QrPayrollTHANG: TIntegerField;
    QrPayrollMANV: TWideStringField;
    QrPayrollMaPhongBan: TWideStringField;
    QrPayrollMaBoPhan: TWideStringField;
    QrPayrollMaNhomLV: TWideStringField;
    QrPayrollMaChucDanh: TWideStringField;
    QrPayrollGHICHU: TWideStringField;
    QrPayrollCREATE_BY: TIntegerField;
    QrPayrollUPDATE_BY: TIntegerField;
    QrPayrollCREATE_DATE: TDateTimeField;
    QrPayrollUPDATE_DATE: TDateTimeField;
    QrPayrollLK_TENNV: TWideStringField;
    QrPayrollLK_TenPhongBan: TWideStringField;
    QrPayrollLK_TenBoPhan: TWideStringField;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    QrPayrollMATK: TWideStringField;
    QrPayrollMANH: TWideStringField;
    QrPayrollMACN: TWideStringField;
    QrPayrollLK_MANH: TWideStringField;
    QrPayrollLK_MACN: TWideStringField;
    QrPayrollLK_TENNH: TWideStringField;
    QrPayrollLK_TENNH_CN: TWideStringField;
    QrPayrollMaChiNhanh: TWideStringField;
    QrPayrollLK_TenChiNhanh: TWideStringField;
    QrPayrollLK_TenChucDanh: TWideStringField;
    QrPayrollLK_TenNhomLV: TWideStringField;
    ToolButton10: TToolButton;
    CmdBangCong: TAction;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    QrPayrollManvQL: TWideStringField;
    QrPayrollNgayVaoLam: TDateTimeField;
    QrPayrollNgayBatDauHocViec: TDateTimeField;
    QrPayrollNgayKetThucHocViec: TDateTimeField;
    QrPayrollNgayBatDauThuViec: TDateTimeField;
    QrPayrollNgayKetThucThuViec: TDateTimeField;
    QrPayrollNgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollNgayThoiViec: TDateTimeField;
    QrPayrollCalc_NgayVaoLam: TDateTimeField;
    QrPayrollCalc_NgayBatDauHocViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucHocViec: TDateTimeField;
    QrPayrollCalc_NgayBatDauThuViec: TDateTimeField;
    QrPayrollCalc_NgayKetThucThuViec: TDateTimeField;
    QrPayrollCalc_NgayVaoLamChinhThuc: TDateTimeField;
    QrPayrollCalc_NgayThoiViec: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdCalc2Execute(Sender: TObject);
    procedure CmdParamsExecute(Sender: TObject);
    procedure BtnXulyClick(Sender: TObject);
    procedure Gr1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdRepExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdCalc1Execute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdCsvExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure CmdEraseExecute(Sender: TObject);
    procedure Pop1Popup(Sender: TObject);
    procedure QrPayrollAfterPost(DataSet: TDataSet);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CmdPaynetExecute(Sender: TObject);
    procedure CmdApprovedExecute(Sender: TObject);
    procedure CmdBangCongExecute(Sender: TObject);
    procedure QrPayrollCalcFields(DataSet: TDataSet);
  private
    r: WORD;
  	mCanEdit: Boolean;
	fStr, mSQL, mEditable, mRemoveFields: String;
    fLevel: Integer;
    bClose, bApproved: Boolean;

    procedure GetPeriod;
    function  TinhLuong(pManv: string = ''): Boolean;
  public
  	procedure Execute (r: WORD);
  end;

var
  FrmBangluong: TFrmBangluong;

implementation

{$R *.DFM}

uses
	isLib, Rights, isDb, MainData, ExCommon, isStr, BosungLuong, isMsg,
    BangThongso, SelectFields, isFile, ImportExcel, isCommon, GmsRep,
    Bangcong, HrData;

const
	FORM_CODE = 'HR_BANG_LUONG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.GetPeriod;
begin
	sysMon  := StrToInt(frMY.CbMon.Text);
    sysYear := StrToInt(frMY.CbYear.Text);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.FormCreate(Sender: TObject);
var
    b:Boolean;
begin
    TMyForm(Self).Init2;
	frMY.Initial(sysMon, sysYear, CmdRefresh);
    mSQL := QrPayroll.SQL.Text;

    AddAllFields(QrPayroll, 'HR_BANG_LUONG', 0);

    with QrPayroll do
    begin
	    SetDisplayFormat(QrPayroll, sysCurFmt);
//    	SetDisplayFormat(QrPayroll, [
//            'NGAYCONG_CHUAN', 'NGAYCONG_THANG', 'NGAYCONG_HUONGLUONG', 'NGAYCONG_THUCTE',
//        	'SOGIO_THUC'], sysDayFmt);
    end;

	SetCustomGrid(FORM_CODE, Gr1);
	SetDictionary(QrPayroll, FORM_CODE, Filter);
    mEditable := FlexConfigString(FORM_CODE, 'Editable');
    mRemoveFields := FlexConfigString(FORM_CODE, 'RightsLuong');

    b := GetRights('HR_NV_LUONG', False) <> R_DENY;
    if not b then
        grRemoveFields(Gr1, mRemoveFields, ';');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.FormShow(Sender: TObject);
begin
    CmdReload.Execute;
    with Gr1 do
    begin
    	SetFocus;
        ReadOnly := not mCanEdit;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdRefreshExecute(Sender: TObject);
var
	sSQL, s : String;
begin
    if frMY.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frMY.CbOrg.SelectedNode.StringData;
        fLevel := frMY.CbOrg.SelectedNode.Level;
	end;

	if  (s = fStr) and
		(sysMon  = StrToInt(frMY.CbMon.Text))  and
        (sysYear = StrToInt(frMY.CbYear.Text)) then
		Exit;

	GetPeriod;
    fStr := s;

    with QrPayroll do
    begin
        s := Sort;
    	Close;
    	sSQL := '1=1';

        if fStr <> '' then
            case fLevel of
            0:
                sSQL := '[MaChiNhanh]=''' + fStr + '''';
            1:
                sSQL := '[MaPhongBan]=''' + fStr + '''';
            2:
                sSQL := '[MaBoPhan]=''' + fStr + '''';
//            3:
//                sSQL := '[Group ID]=''' + fStr + '''';
            end;

		SQL.Text := Format(mSQL, [sSQL]);
        Parameters.Refresh;
        Parameters[0].Value := sysMon;
        Parameters[1].Value := sysYear;
        Open;

    end;

    if s = '' then
        s := 'ManvQL';

    SortDataSet(QrPayroll, s);

    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 1);
    bApproved := HrDataMain.GetPeriodStatus(sysMon, sysYear, 3);
    if bClose then
    	Msg(RS_PAYROLL_CFM_CLOSED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsPayroll);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBangluong.TinhLuong(pManv: string): Boolean;
var
    _msg: String;
begin
	Result := True;
    try
        with spHR_TINHLUONG do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := sysYear;
            Parameters[3].Value := sysMon;
            Parameters[4].Value := pManv;
            Execute;

            Result := Parameters[0].Value = 0;
            if not Result then
            begin
                if Parameters.FindParam('@returnCode') <> nil then
                    _msg := Parameters.ParamValues['@returnCode']
                else
                    _msg := RS_PAYROLL_ERROR_CALC;

                ErrMsg(_msg);
            end;
        end;
    except
        Result := False;
        ErrMsg(RS_PAYROLL_ERROR_CALC);
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdApprovedExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_BANGLUONG_DUYETLUONG');
    if r = R_DENY then
    	Exit;

    with spUpdateApproved do
    begin
        Prepared := True;
        Parameters[0].Value := not bApproved;
        Parameters[1].Value := sysYear;
        Parameters[2].Value := sysMon;
        Execute;
    end;

    bApproved := HrDataMain.GetPeriodStatus(sysMon, sysYear, 3);
end;

procedure TFrmBangluong.CmdBangCongExecute(Sender: TObject);
var
    mManv: string;
    mMonth, mYear: Integer;
begin
    r := GetRights('HR_BANGCONG');
    if r = R_DENY then
    	Exit;

    mMonth  := StrToInt(frMY.CbMon.Text);
    mYear := StrToInt(frMY.CbYear.Text);
    with QrPayroll do
    begin
        mManv := FieldByName('Manv').AsString;
    end;

    Application.CreateForm(TFrmBangcong, FrmBangcong);
    FrmBangcong.Execute(r, mManv, mMonth, mYear);
    SetProgressDesc(mManv);
    StopProgress;
    exReSyncRecord(QrPayroll);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdCalc1Execute(Sender: TObject);
var
	s: String;
begin
	if not YesNo(Format(RS_PAYROLL_CFM_CALC1, [QrPayroll.FieldByName('LK_Tennv').AsString]), 1) then
   		Exit;

    // Book mark
    s := QrPayroll.FieldByName('Manv').AsString;

    if TinhLuong(s) then
    begin
    	with QrPayroll do
        begin
        	Requery;
            Locate('Manv', s, []);
        end;
    	MsgDone;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdCalc2Execute(Sender: TObject);
var
    bm: TBytes;
begin
	if not YesNo(Format(RS_PAYROLL_CFM_CALC2, [sysMon, sysYear]), 1) then
    	Exit;

    with QrPayroll do
    begin
        if not TinhLuong() then
            Exit;
    end;
    with QrPayroll do
    begin
        bm := BookMark;
	    QrPayroll.Requery;
        MsgDone;
        BookMark := bm;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdParamsExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_BANG_THONGSO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBangThongso, FrmBangThongso);
    FrmBangThongso.Execute(r, 2, StrToInt(frMY.cbMon.Text), StrToInt(frMY.CbYear.Text));
    bClose := HrDataMain.GetPeriodStatus(sysMon, sysYear, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdPaynetExecute(Sender: TObject);
var
	r: WORD;
begin
	r := GetRights('HR_BANGLUONG_CHUYENLUONG');
    if r = R_DENY then
    	Exit;
    bApproved := HrDataMain.GetPeriodStatus(sysMon, sysYear, 3);
    if not bApproved then
    begin
        Exit;
    end;
//    Application.CreateForm(TFrmPayNet, FrmPayNet);
//    FrmPayNet.Execute(sysYear, sysMon);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.BtnXulyClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.Gr1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
var
	s: String;
begin
	s := Field.FullName;
	if (s = 'MANV') or (s = 'LK_TENNV') then
    begin
    	AFont.Style := [];
        AFont.Size  := 10;
        AFont.Color	:= clBlack;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdRepExecute(Sender: TObject);
var
	r: WORD;
begin
//	r := GetRights('BL_BAOCAO');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('CongLuong') (*3*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdCalc1.Enabled := not QrPayroll.IsEmpty and (not bClose);
    CmdCalc2.Enabled := not bClose;
    CmdCsv.Enabled := not bClose;
    CmdErase.Enabled := not bClose;
    CmdUpdate.Enabled := not bClose;
    DsPayroll.AutoEdit := not bClose;

    CmdApproved.Enabled := bClose;
    CmdPaynet.Enabled := bApproved;
//    CmdApproved.Caption := GetCheckedCaption(QrPayroll, bApproved);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.Panels[0].Text := exRecordCount(QrPayroll, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.QrPayrollAfterPost(DataSet: TDataSet);
var
	s: String;
begin
    // Book mark
    s := QrPayroll.FieldByName('MANV').AsString;

    if TinhLuong(s) then
    begin
    	with QrPayroll do
        begin
        	Requery;
            Locate('MANV', s, []);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.QrPayrollCalcFields(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    with QrPayroll do
    begin
        DecodeDate(FieldByName('NgayVaoLam').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLam').AsDateTime := FieldByName('NgayVaoLam').AsDateTime
        else
            FieldByName('Calc_NgayVaoLam').Clear;

        DecodeDate(FieldByName('NgayBatDauHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauHocViec').AsDateTime := FieldByName('NgayBatDauHocViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauHocViec').Clear;

        DecodeDate(FieldByName('NgayKetThucHocViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucHocViec').AsDateTime := FieldByName('NgayKetThucHocViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucHocViec').Clear;

        DecodeDate(FieldByName('NgayBatDauThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayBatDauThuViec').AsDateTime := FieldByName('NgayBatDauThuViec').AsDateTime
        else
            FieldByName('Calc_NgayBatDauThuViec').Clear;

        DecodeDate(FieldByName('NgayKetThucThuViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayKetThucThuViec').AsDateTime := FieldByName('NgayKetThucThuViec').AsDateTime
        else
            FieldByName('Calc_NgayKetThucThuViec').Clear;

        DecodeDate(FieldByName('NgayVaoLamChinhThuc').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayVaoLamChinhThuc').AsDateTime := FieldByName('NgayVaoLamChinhThuc').AsDateTime
        else
            FieldByName('Calc_NgayVaoLamChinhThuc').Clear;

        DecodeDate(FieldByName('NgayThoiViec').AsDateTime, yy, mm, dd);
        if (mm = sysMon) and (yy = sysYear)  then
            FieldByName('Calc_NgayThoiViec').AsDateTime := FieldByName('NgayThoiViec').AsDateTime
        else
            FieldByName('Calc_NgayThoiViec').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdCsvExecute(Sender: TObject);
var
    fld: TField;
	b, bSalary: Boolean;
    i, iParam, n, re: Integer;
	s, sEmp, sSalaryID, mFile, sErase: String;
	mLog: TStrings;
begin
	// Get file name
    mFile := isGetOpenFileName('XLS;XLSX;CSV;TXT;ALL', 1, GetSysParam('FOLDER_IMPORT'));;
    if mFile = '' then
    	Exit;

    // Only Update Erasable Field
	sErase := mEditable;
 {
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        //Application.CreateForm(TFrmImportExcel, FrmImportExcel);
        b := FrmImportExcel.Execute(mFile, 'IMP_BANG_LUONG', DsPayroll, 'MANV;' + sErase);
        try
            QrCsv := FrmImportExcel.QrExcel;
//            FrmImportExcel.Conn.Close;
            sEmp := QrCsv.FieldByName('MANV').AsString;
//            bSalary := QrCsv.FindField('SALARYID') <> nil;
        except
            ErrMsg(RS_INVALID_DATA);
            FrmImportExcel.QrExcel.Close;
            QrCsv.Close;
            b := False;
        end;
    end
    //file text with delimeter
    else
    begin
        // File preparing
//        if not CsvPrepareFile(mFile, 'Payroll') then
//            Exit;

        with QrCsv do
        begin
            b := True;
            SQL.Text := 'select * from "Payroll.csv"';
            try
                Open;
                sEmp := FieldByName('MANV').AsString;
//                bSalary := FindField('SALARYID') <> nil;
            except
                ErrMsg(RS_INVALID_DATA);
                Close;
                b := False;
            end;
        end;
    end;
    if not b then
        Exit;

	// Confirm
    if not YesNo(Format(RS_PAYROLL_CFM_IMPORT, [sysMon, sysYear]), 1) then
    	Exit;

    mLog := TStringList.Create;
    with QrCsv do
    if not IsEmpty then
    begin
        // Bat dau
        n := FieldCount - 1;
        iParam := 0;
        // Chuan bi cau query update
        s := 'update BANG_LUONG set ';
        for i := 0 to n do
            if (Pos(Fields[i].DisplayLabel, sErase) > 0) and (Fields[i].Tag <> 1) then
            begin
                fld := QrPayroll.FindField(Fields[i].DisplayLabel);
                if fld.FieldKind = fkData then
                    try
                        s := s + Format('[%s]=:"%s"', [Fields[i].DisplayLabel, Fields[i].FullName]);
                        s := s + ',';
                        Inc(iParam);
                    except
                        b := False;
                        Break;
                    end;
            end;

        b := iParam > 0;
        s := isLeft(s, Length(s) -1);
        s := s + Format(' where [NAM]=%d and [THANG]=%d and [MANV]=:MANV', [sysYear, sysMon]);


        if b then
        begin
            StartProgress(RecordCount, 'Importing');
            with CSV do
            begin
                Parameters.Clear;
                CommandType := cmdText;
                CommandText := s;
                Prepared := True;
                //Parameters.Refresh;
            end;

            while not Eof do
            begin
                sEmp := FieldByName('MANV').AsString;
//                if (sEmp = '') and bSalary then
//                begin
//                    sSalaryID := FieldByName('SalaryID').AsString;
//                    sEmp := VarToStrDef(QrEmp.Lookup('SalaryID', sSalaryID, 'EMPID'), '');
//                end;
//
//                if sEmp = '' then
//                begin
//                    if sSalaryID <> '' then
//                        mLog.Add(sSalaryID);
//                    Next;
//                    Continue;
//                end;

                SetProgressDesc(sEmp);
                with CSV do
                begin
                    try
                        for i := 0 to iParam - 1 do
                            Parameters[i].Value := QrCsv.FieldByName(Parameters[i].Name).Value;

                        Parameters[iParam].Value := sEmp;
                        Execute(re, Parameters[0].Value);
                    except
                        re := 0;
                    end;
                end;

                if re = 0 then
                    mLog.Add(sEmp);

                IncProgress;
                Next;
            end;
            Close;
            StopProgress;
        end;
    end;

    if mLog.Count > 0 then
    	exViewLog(RS_LOG_INVALID_EMP, mLog.Text);

    mLog.Free;
    CmdReload.Execute;
    Gr1.SetFocus;
    if b then
	    MsgDone;        }
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdDetailExecute(Sender: TObject);
var
	p: TPoint;
    s: String;
begin
	// Field list
   	s := FlexConfigString(FORM_CODE, 'Editable');
    if s = '' then
    	Exit;

	p.x := Gr1.Left + Gr1.Width;
    p.y := Gr1.Top;
    p := ClientToScreen(p);

	Application.CreateForm(TFrmBosungLuong, FrmBosungLuong);
    with FrmBosungLuong do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y + 2;
    	Execute(DsPayroll, s);
    end;
end;               

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdReloadExecute(Sender: TObject);
begin
	sysMon := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdUpdateExecute(Sender: TObject);
var
    bm: TBytes;
begin
	GetPeriod;
	if not HrDataMain.UpdatePayrollSheet(sysMon, sysYear, False) then
    begin
    	ErrMsg(Format(RS_TIMESHEET_GEN_ERROR, [sysMon, sysYear]));
		Exit;
    end;
     with QrPayroll do
    begin
        bm := BookMark;
        CmdReload.Execute;
        MsgDone;
        BookMark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.CmdEraseExecute(Sender: TObject);
var
	s: String;
begin
	// List of fields
   	s := mEditable;
    if s = '' then
    	Exit;
    s := isStrReplace(s, ';', #13);

    // Get selection
	Application.CreateForm(TFrmSelectFields, FrmSelectFields);
    s := FrmSelectFields.Get(s, TransFieldDesc(QrPayroll, s), '', False);
    if s = '' then
    	Exit;

    // Confirm
	if not YesNo(Format(RS_PAYROLL_CFM_ERASE, [sysMon, sysYear]), 1) then
        Exit;

	PAYROLL_ERASE.CommandText := MakeEraseSQL(QrPayroll, 'HR_BANG_LUONG', s) +
    	Format(' where [THANG]=%d and [NAM]=%d and MANV=:MANV', [sysMon, sysYear]);

    with QrPayroll do
    begin
    	StartProgress(RecordCount, 'Deleting');
    	DisableControls;
        First;
        while not Eof do
        begin
        	s := FieldByName('MANV').AsString;
        	SetProgressDesc(s);

        	with PAYROLL_ERASE do
            begin
            	Prepared := True;
                Parameters[0].Value := s;
                Execute;
            end;

        	Next;
            IncProgress;
        end;
    	EnableControls;
        StopProgress;
    end;

	QrPayroll.Requery;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.Pop1Popup(Sender: TObject);
var
	s: String;
begin
	s := QrPayroll.FieldByName('LK_TENNV').AsString;
    if s <> '' then
		ItemCalc1.Caption := RS_POPUP_RECALC + ' "' + s + '"    '
    else
		ItemCalc1.Caption := (ItemCalc1.Action as TAction).Caption;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBangluong.ApplicationEvents1Hint(Sender: TObject);
begin
	if Screen.ActiveForm = Self then
	    Status.Panels[1].Text := Application.Hint;
end;

end.
