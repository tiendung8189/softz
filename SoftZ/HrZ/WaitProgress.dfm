object FrmWaitProgress: TFrmWaitProgress
  Left = 305
  Top = 119
  BorderStyle = bsNone
  ClientHeight = 57
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PaMain: TPanel
    Left = 0
    Top = 0
    Width = 305
    Height = 57
    Align = alClient
    BevelInner = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Desc: TLabel
      Left = 14
      Top = 9
      Width = 227
      Height = 14
      AutoSize = False
      Caption = #272'ang th'#7921'c hi'#7879'n .....'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbTime: TLabel
      Left = 229
      Top = 22
      Width = 68
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '00:00:00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object ProgBar: TProgressBar
      Left = 2
      Top = 38
      Width = 301
      Height = 17
      Align = alBottom
      TabOrder = 0
    end
  end
end
