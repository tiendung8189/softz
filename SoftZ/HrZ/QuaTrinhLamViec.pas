﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit QuaTrinhLamViec;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  frameEmp, isPanel, Vcl.Buttons, RzPanel, RzSplit, wwDataInspector,
  rDBComponents, DBCtrlsEh, DBGridEh, DBLookupEh, ToolCtrlsEh, DynVarsEh,
  frameD2D, EhLibVCL, GridsEh, DBAxisGridsEh, DBVertGridsEh, DbLookupComboboxEh2;

type
  TFrmQuaTrinhLamViec = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    PaLyDo: TisPanel;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrMasterGhiChu: TWideMemoField;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    CmdQuaTrinhLamViec: TAction;
    QrMasterUPDATE_NAME: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaThongTinLuong: TisPanel;
    PaHeSoLuong: TisPanel;
    PaToChuc: TisPanel;
    Label6: TLabel;
    CdNgayHieuLuc: TwwDBDateTimePicker;
    EdSoQuyetDinh: TDBEditEh;
    QrMasterMaQTLV: TWideStringField;
    QrMasterQTLV_Nhom_DacBiet: TIntegerField;
    QrMasterNgayHieuLuc: TDateTimeField;
    QrMasterSoQuyetDinh: TWideStringField;
    QrMasterMaChiNhanh: TWideStringField;
    QrMasterMaPhongBan: TWideStringField;
    QrMasterMaBoPhan: TWideStringField;
    QrMasterMaNoiLV: TWideStringField;
    QrMasterMaChucDanh: TWideStringField;
    QrMasterPhepNam_LuatDinh: TFloatField;
    QrMasterPhepNam_ChucDanh: TFloatField;
    QrMasterPhepNam_ThamNien: TFloatField;
    QrMasterCo_PhepNamMotThang: TBooleanField;
    QrMasterMaNhomLV: TWideStringField;
    QrMasterBoNhiem_MaChucDanh: TWideStringField;
    QrMasterBoNhiem_NgayKetThuc: TDateTimeField;
    QrMasterBietPhai_MaChucDanh: TWideStringField;
    QrMasterBietPhai_NgayKetThuc: TDateTimeField;
    QrMasterChucVuChinhQuyen: TWideStringField;
    QrMasterCongViecChinh: TWideStringField;
    QrMasterXacNhan_Manv: TWideStringField;
    QrMasterMaBacLuong: TWideStringField;
    QrMasterBacLuong: TIntegerField;
    QrMasterNgayBacLuong: TDateTimeField;
    QrMasterHeSoLuong: TFloatField;
    QrMasterHeSoPhuCap: TFloatField;
    QrMasterLuongChinh: TFloatField;
    QrMasterLuongBaoHiem: TFloatField;
    QrMasterLuongNgay: TFloatField;
    QrMasterLuongGio: TFloatField;
    QrMasterTyLeLuongThuViec: TFloatField;
    QrMasterCo_BangCong: TBooleanField;
    QrMasterCo_BangLuong: TBooleanField;
    QrMasterCo_BHXH: TBooleanField;
    QrMasterCo_BHYT: TBooleanField;
    QrMasterCo_BHTN: TBooleanField;
    QrMasterCo_ChuyenKhoan: TBooleanField;
    ScrollToChuc: TScrollBox;
    ScrollThuNhap: TScrollBox;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    PaChinhQuyen: TisPanel;
    Label78: TLabel;
    Label80: TLabel;
    DpBoNhiemTuNgay: TwwDBDateTimePicker;
    DpBietPhaiTuNgay: TwwDBDateTimePicker;
    wwDBEdit24: TDBEditEh;
    gbPhepNam: TGroupBox;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    rDBCheckBox1: TrDBCheckBox;
    gbViecLam: TGroupBox;
    CmdNoiLamViec: TAction;
    gbToChuc: TGroupBox;
    wwDBEdit25: TDBEditEh;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSite: TDataSource;
    DsDep: TDataSource;
    DsSec: TDataSource;
    QrEmp: TADOQuery;
    QrMasterLK_TenQuaTrinhLamViec: TWideStringField;
    QrMasterLK_TenPhongBan: TWideStringField;
    QrMasterLK_TenChucDanh: TWideStringField;
    QrMasterLK_TenBoPhan: TWideStringField;
    QrMasterLK_: TWideStringField;
    QrMasterLK_TenNoiLamViec: TWideStringField;
    QrMasterLK_PhepNam_LuatDinh: TFloatField;
    QrMasterLK_PhepNam_ChucDanh: TFloatField;
    QrMasterLK_PhepNam_ThamNien: TFloatField;
    QrMasterLK_Co_PhepNamMotThang: TBooleanField;
    QrMasterCALC_TongPhepNam: TFloatField;
    QrMasterLK_QTLV_Nhom_DacBiet: TIntegerField;
    QrMasterLK_TenBacLuong: TWideStringField;
    QrV_HR_QUATRINH_LAMVIEC: TADOQuery;
    ToolButton14: TToolButton;
    CmdChucDanh: TAction;
    DsV_HR_QUATRINH_LAMVIEC: TDataSource;
    CbQTLV: TDbLookupComboboxEh2;
    wwDBEdit23: TDBEditEh;
    DBEditEh1: TDBEditEh;
    CbChucDanh: TDbLookupComboboxEh2;
    CmNoiLV: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    CbNhomLV: TDbLookupComboboxEh2;
    DBEditEh3: TDBEditEh;
    DBEditEh4: TDBEditEh;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaBoPhan_BanDau: TDBEditEh;
    QrMasterCalc_FileName: TWideStringField;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    QrMasterLK_BoPhan_Ma: TWideStringField;
    CbBietPhai_MaChucDanh: TDbLookupComboboxEh2;
    CbBoNhiem_MaChucDanh: TDbLookupComboboxEh2;
    CmdPhongBan: TAction;
    frD2D: TfrD2D;
    QrMasterLK_Tennv: TWideStringField;
    QrWorking: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    WideStringField3: TWideStringField;
    WideMemoField1: TWideMemoField;
    WideStringField4: TWideStringField;
    WideStringField5: TWideStringField;
    IntegerField3: TIntegerField;
    DateTimeField3: TDateTimeField;
    WideStringField6: TWideStringField;
    WideStringField7: TWideStringField;
    WideStringField8: TWideStringField;
    WideStringField9: TWideStringField;
    WideStringField10: TWideStringField;
    WideStringField11: TWideStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    BooleanField1: TBooleanField;
    WideStringField12: TWideStringField;
    WideStringField13: TWideStringField;
    DateTimeField4: TDateTimeField;
    WideStringField14: TWideStringField;
    DateTimeField5: TDateTimeField;
    WideStringField15: TWideStringField;
    WideStringField16: TWideStringField;
    WideStringField18: TWideStringField;
    WideStringField19: TWideStringField;
    IntegerField4: TIntegerField;
    DateTimeField6: TDateTimeField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    BooleanField5: TBooleanField;
    BooleanField6: TBooleanField;
    BooleanField7: TBooleanField;
    WideStringField20: TWideStringField;
    WideStringField21: TWideStringField;
    WideStringField22: TWideStringField;
    WideStringField23: TWideStringField;
    WideStringField24: TWideStringField;
    WideStringField25: TWideStringField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    BooleanField8: TBooleanField;
    WideStringField26: TWideStringField;
    WideStringField27: TWideStringField;
    FloatField14: TFloatField;
    IntegerField5: TIntegerField;
    WideStringField28: TWideStringField;
    WideStringField29: TWideStringField;
    WideStringField30: TWideStringField;
    WideStringField31: TWideStringField;
    QrMasterLK_MaPhongBan: TWideStringField;
    QrMasterNgayKy: TDateTimeField;
    QrMasterNguoiKy: TWideStringField;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    EdNguoiKy_Manv: TDBEditEh;
    EdXacNhan_ManvQL: TDBEditEh;
    CbXacNhan_Manv: TDbLookupComboboxEh2;
    DsEmp: TDataSource;
    EdXetDuyet_ManvQL: TDBEditEh;
    CbXetDuyet_Manv: TDbLookupComboboxEh2;
    DBNumberEditEh7: TDBNumberEditEh;
    DBNumberEditEh8: TDBNumberEditEh;
    Label74: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    DBEditEh11: TDBEditEh;
    DBEditEh12: TDBEditEh;
    QrMasterNamQuyHoach: TIntegerField;
    QrMasterNamQuyHoachBoSung: TIntegerField;
    QrMasterNgayBienChe: TDateTimeField;
    QrWorkingNgayKy: TDateTimeField;
    QrWorkingNguoiKy: TWideStringField;
    QrWorkingNamQuyHoach: TIntegerField;
    QrWorkingNamQuyHoachBoSung: TIntegerField;
    QrWorkingNgayBienChe: TDateTimeField;
    EdThongTinKhac: TDBMemoEh;
    Label1: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    wwDBDateTimePicker5: TwwDBDateTimePicker;
    Label2: TLabel;
    QrMasterBoNhiem_NgayBatDau: TDateTimeField;
    QrMasterBietPhai_NgayBatDau: TDateTimeField;
    QrMasterThongTinKhac: TWideMemoField;
    grThongTinLuong: TDBVertGridEh;
    grHeSoLuong: TDBVertGridEh;
    CbBacLuong: TDbLookupComboboxEh2;
    QrMasterXetDuyet_Manv: TWideStringField;
    QrWorkingBoNhiem_NgayBatDau: TDateTimeField;
    QrWorkingBietPhai_NgayBatDau: TDateTimeField;
    QrWorkingThongTinKhac: TWideMemoField;
    QrWorkingXetDuyet_Manv: TWideStringField;
    QrWorkingCo_MacDinhCong: TBooleanField;
    QrWorkingCo_CongDoan: TBooleanField;
    QrMasterCo_MacDinhCong: TBooleanField;
    QrMasterCo_CongDoan: TBooleanField;
    QrMasterLK_ManvQL: TWideStringField;
    QrMasterLK_XacNhan_ManvQL: TWideStringField;
    QrMasterLK_XetDuyet_ManvQL: TWideStringField;
    QrWorkingManvQL: TWideStringField;
    frEmp1: TfrEmp;
    QrMasterManvQL: TWideStringField;
    CmdCapManvQL: TAction;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    CbHopDong: TDbLookupComboboxEh2;
    QrHopDong: TADOQuery;
    DsHopDong: TDataSource;
    QrHopDongManv: TWideStringField;
    QrHopDongSoHopDong: TWideStringField;
    QrHopDongMaHopDong: TWideStringField;
    QrHopDongTenHopDong: TWideStringField;
    QrHopDongNgayHieuLuc: TDateTimeField;
    QrHopDongNgayKy: TDateTimeField;
    QrMasterLK_SoHopDong: TWideStringField;
    QrMasterLK_NgayKy: TDateTimeField;
    QrMasterLK_NgayHieuLuc: TDateTimeField;
    CmdHopDong: TAction;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    CbToDate: TwwDBDateTimePicker;
    DBEditEh5: TDBEditEh;
    PaTop: TPanel;
    PaNhanVien: TPanel;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    QrMasterFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    QrMasterIdx: TAutoIncField;
    QrMasterIdx_HopDong: TIntegerField;
    QrHopDongIdx: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdQuaTrinhLamViecExecute(Sender: TObject);
    procedure CmdNoiLamViecExecute(Sender: TObject);
    procedure QrMasterMaChucDanhChange(Sender: TField);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure QrMasterMaQTLVChange(Sender: TField);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdChucDanhExecute(Sender: TObject);
    procedure QrMasterMaPhongBanChange(Sender: TField);
    procedure QrMasterMaBacLuongChange(Sender: TField);
    procedure QrMasterAfterPost(DataSet: TDataSet);
    procedure QrMasterBeforeEdit(DataSet: TDataSet);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdPhongBanExecute(Sender: TObject);
    procedure QrMasterMaBoPhanChange(Sender: TField);
    procedure QrMasterManvValidate(Sender: TField);
    procedure QrWorkingBeforeOpen(DataSet: TDataSet);
    procedure QrMasterNgayHieuLucChange(Sender: TField);
    procedure CbQTLVDropDown(Sender: TObject);
    procedure CbBoPhanDropDown(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterManvChange(Sender: TField);
    procedure QrMasterXacNhan_ManvChange(Sender: TField);
    procedure QrMasterXetDuyet_ManvChange(Sender: TField);
    procedure CmdCapManvQLExecute(Sender: TObject);
    procedure CbHopDongDropDown(Sender: TObject);
    procedure CmdHopDongExecute(Sender: TObject);
    procedure QrMasterAfterDelete(DataSet: TDataSet);
  private
    mCanEdit, bCo_BangCong, bCo_BangLuong, bCo_BHXH, bCo_BHYT, bCo_BHTN,
    bCo_ChuyenKhoan, bCo_PhepNamMotThang, bCo_MacDinhCong, bCo_CongDoan, mAutoCode: Boolean;
    fTungay, fDenngay, dNgayBacLuong, dNgayBienChe: TDateTime;
    fLevel, mBacLuong, mNamQuyHoach, mNamQuyHoachBoSung, mType: Integer;
    mSQL, fStr, mEmpID, mEmpIDLabel, mEmpName: String;
    FORM_CODE, REPORT_NAME: String;
    sManvQL, sMaChiNhanh, sMaPhongBan, sMaBoPhan, sMaChucDanh, sCongViecChinh, sXetDuyet_Manv, sXacNhan_Manv,
    sMaNoiLV, sMaNhomLV, sMaBacLuong, sThongTinKhac : String;
    fHeSoLuong, fHeSoPhuCap, fLuongChinh, fLuongBaoHiem, fLuongNgay, fLuongGio,
    fTyLeLuongThuViec, fPhepNam_LuatDinh, fPhepNam_ChucDanh, fPhepNam_ThamNien  : Double;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
    procedure expendCollapsed(bExpend: Boolean);
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmQuaTrinhLamViec: TFrmQuaTrinhLamViec;

const
    TABLE_NAME = 'HR_LICHSU_QTLV';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmQTLV, DmNhomlv, DmDiaDiemLamViec,
    DmPhongban, DmPhongbanNotCN, DmChucvu, HopdongLaodong, HrData, HrExCommon; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.FormCreate(Sender: TObject);
var
    bThongTinLuong, bThongTinHeSoLuong: Boolean;
begin
    AddAllFields(QrMaster, TABLE_NAME);

    bThongTinLuong := GetRights('HR_NV_LUONG', False) <> R_DENY;
    PaThongTinLuong.Visible := bThongTinLuong;

    bThongTinHeSoLuong := FlexConfigBool('HR_DM_NHANVIEN', 'Panel_HeSoLuong');
    PaHeSoLuong.Visible := bThongTinLuong and bThongTinHeSoLuong;

    PaChinhQuyen.Visible := FlexConfigBool('HR_DM_NHANVIEN', 'Panel_ChinhQuyen');
    mAutoCode := FlexConfigBool('HR_DM_NHANVIEN', 'ManvQL_AutoCode');
    FORM_CODE := 'HR_LICHSU_QTLV';
    REPORT_NAME := 'HR_RP_LICHSU_QTLV';
    mType := 0;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.FormShow(Sender: TObject);
var
	s, s2: String;
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);

    with HrDataMain do
        OpenDataSets([QrDMNV, QrV_HR_QUATRINH_LAMVIEC, QrDM_CHUCDANH, QrDM_NOILV,
        QrDM_NHOMLV, QrDMPHONGBAN, QrDMBOPHAN, QrDM_BACLUONG]);

    OpenDataSets([QrEmp, QrSite, QrDep, QrSec, QrV_HR_QUATRINH_LAMVIEC, QrHopDong]);

    if mEmpID <> '' then
    begin
        TMyForm(Self).Init1;
        Width := 1260;
        Height := 714;
        Caption := 'Hồ Sơ Nhân Viên - Quá Trình Làm Việc';
        fStr := mEmpID;
        frD2D.EdFrom.Date := Date;
        frD2D.EdTo.Date := Date;
        frD2D.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
        FORM_CODE := 'HR_LICHSU_NHANVIEN_QTLV';
        REPORT_NAME := 'HR_RP_LICHSU_NHANVIEN_QTLV';
        mType := 1;
        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        PaLyDo.Height := PaLyDo.Height - PaNhanVien.Height;
    end
    else
    begin
        TMyForm(Self).Init2;
    end;

    SetTab0(Self);

    SetDisplayFormat(QrMaster, sysCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['NgayHieuLuc'], DateTimeFmt);
    SetDisplayFormat(QrMaster, ['HeSoLuong', 'HeSoPhuCap'], sysFloatFmtTwo);
    SetDisplayFormat(QrHopDong, ['NgayHieuLuc'], DateTimeFmt);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    s := FlexConfigString('HR_NHANVIEN_LUONG', 'Fields');
    exConfigVertGrid(grThongTinLuong, s);

    s2 := FlexConfigString('HR_NHANVIEN_LUONG', 'Fields_HeSoLuong');
    exConfigVertGrid(grHeSoLuong, s2);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        if mEmpID <> '' then
            Add(' and Manv =:Manv')
        else
        begin
           Add(' and [NgayHieuLuc] between :Tungay and :Denngay' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayHieuLuc], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

procedure TFrmQuaTrinhLamViec.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrMaster.FieldByName('QTLV_Nhom_DacBiet').AsInteger = 1 then
    begin
	    AFont.Color := clNavy;
        Exit;
    end
    else if QrMaster.FieldByName('QTLV_Nhom_DacBiet').AsInteger = 2 then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp, QrSite, QrDep, QrSec, QrV_HR_QUATRINH_LAMVIEC, QrHopDong]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    PgMain.ActivePageIndex := 0;
    PaGhiChu.Collapsed := False;
    expendCollapsed(False);
    if mEmpID <> '' then
        CbQTLV.SetFocus
    else
        CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdNoiLamViecExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NOI_LAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmDiaDiemLamViec, FrmDmDiaDiemLamViec);
    FrmDmDiaDiemLamViec.Execute(r);

    HrDataMain.QrDM_NOILV.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if mEmpID <> '' then
    begin
        if PgMain.ActivePageIndex = 0 then
            CbQTLV.SetFocus
    end
    else
        CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdCapManvQLExecute(Sender: TObject);
begin
    with QrMaster do
    begin
        FieldByName('ManvQL').AsString := HrDataMain.AllocManvQL(FieldByName('MaNhomLV').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdChucDanhExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_CHUCVU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChucvu, FrmDmChucvu);
    FrmDmChucvu.Execute(r);

    HrDataMain.QrDM_CHUCDANH.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
    with QrMaster do
    begin
        HrDataMain.SyncQuaTrinh(FieldByName('Manv').AsString, 0, 1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdPhongBanExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHONGBAN');
    if r = R_DENY then
    	Exit;

	if sysIsHrChinhanh then
    begin
        Application.CreateForm(TFrmDmPhongban, FrmDmPhongban);
        FrmDmPhongban.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmPhongbanNotCN, FrmDmPhongbanNotCN);
        FrmDmPhongbanNotCN.Execute(r);
    end;

    QrDep.Requery;
    QrSec.Requery;
    HrDataMain.QrDMBOPHAN.Requery;
end;

procedure TFrmQuaTrinhLamViec.CmdPrint2Execute(Sender: TObject);
var
    n: Integer;
    filename, repname: String;
begin
    n := (Sender as TComponent).Tag;

    repname := 'HR_RP_HDLD_WORD';
    filename := 'DOC\' +'HR_RP_HDLD_WORD_' + IntToStr(n) + '.docx';

    with QrMaster do
        DataOffice.CreateReport2(filename ,
            [sysLogonUID, FieldByName('Idx').AsInteger], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdPrintExecute(Sender: TObject);
begin
    if CmdSave.Enabled then
      CmdSave.Execute;

    if mType <> 0 then
        fStr := mEmpID;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        mType, //0: Lich su, 1: HSNV/Lich Su
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;
            if mEmpID <> '' then
                Parameters[0].Value := mEmpID
            else
            begin
                sSQL := '1=1';

                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
                    end;


                SQL.Text := Format(mSQL, [sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'NgayHieuLuc';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdFileMinusExecute(Sender: TObject);
begin
   if  not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdHopDongExecute(Sender: TObject);
begin
    r := GetRights('HR_HOPDONG_LD');
    if r = R_DENY then
    	Exit;

    with QrMaster do
    begin
          if BlankConfirm(QrMaster, ['Manv']) then
              Abort;

          mEmpID := FieldByName('Manv').AsString;
          mEmpIDLabel := FieldByName('ManvQL').AsString;
          mEmpName := FieldByName('LK_Tennv').AsString;
    end;

	Application.CreateForm(TFrmHopdongLaodong, FrmHopdongLaodong);
    FrmHopdongLaodong.Execute(r, mEmpID, mEmpIDLabel, mEmpName);
    QrHopDong.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdQuaTrinhLamViecExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmQTLV, FrmDmQTLV);
    FrmDmQTLV.Execute(r);
    QrV_HR_QUATRINH_LAMVIEC.Requery;
    HrDataMain.QrV_HR_QUATRINH_LAMVIEC.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bInsert, bEmpty, bViewFile, bVaoLam, bThoiViec, bCapManvQL: Boolean;
begin
    bViewFile := False;
    bCapManvQL := False;
	with QrMaster do
    begin
        if Active then
            bViewFile := FieldByName('FileExt').AsString <> '';

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bVaoLam := FieldByName('QTLV_Nhom_DacBiet').AsInteger = 1;
        bThoiViec := FieldByName('QTLV_Nhom_DacBiet').AsInteger = 2;
        bEmpty := IsEmpty;
        bCapManvQL := (FieldByName('MaNhomLV').AsString <> '') and (FieldByName('Manv').AsString <> '');
    end;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdEdit.Enabled := bBrowse and mCanEdit and (not bEmpty);
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bVaoLam) and (not bEmpty);
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFileMinus.Enabled := bViewFile and not bBrowse;
    EdManvQL.EditButtons[0].Enabled := bCapManvQL and bInsert;

    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;
    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;

    if bInsert and (not mAutoCode) then
    begin
        EdManvQL.ReadOnly := False;
        EdManvQL.Color := clWindow;
        EdManvQL.EmptyDataInfo.Color := clInfoBk;
    end
    else
    begin
        EdManvQL.ReadOnly := True;
        EdManvQL.Color := clBtnFace;
        EdManvQL.EmptyDataInfo.Color := clBtnFace;
    end;

    if not mAutoCode then
        EdManvQL.EditButtons[0].Visible := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

    with QrMaster do
    begin
        if RecNo <> RecordCount then
        begin
            if GetRights('HR_NV_QUATRINH_EDIT') = R_DENY then
                Abort;
        end;
    end;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterBeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    with QrMaster do
    begin
        if RecNo <> RecordCount then
        begin
            if GetRights('HR_NV_QUATRINH_EDIT') = R_DENY then
                Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_BONHIEM = 'Bổ nhiệm từ ngày không hợp lệ.';
    RS_INVALID_BIETPHAI = 'Biệt phái từ ngày không hợp lệ.';
procedure TFrmQuaTrinhLamViec.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'MaQTLV', 'NgayHieuLuc', 'NgayKy', 'NguoiKy',  'MaPhongBan', 'MaChucDanh', 'XacNhan_Manv', 'XetDuyet_Manv', 'MaNhomLV']) then
            Abort;

        if ((FieldByName('BoNhiem_NgayKetThuc').AsFloat > 10) and (FieldByName('BoNhiem_NgayKetThuc').AsDateTime < FieldByName('BoNhiem_NgayBatDau').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_BONHIEM);
            try
                DpBoNhiemTuNgay.SelectAll;
                DpBoNhiemTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if ((FieldByName('BietPhai_NgayKetThuc').AsFloat > 10) and (FieldByName('BietPhai_NgayKetThuc').AsDateTime < FieldByName('BietPhai_NgayBatDau').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_BIETPHAI);
            try
                DpBietPhaiTuNgay.SelectAll;
                DpBietPhaiTuNgay.SetFocus;
            except
            end;
            Abort;
        end;

        if (FieldByName('NgayHieuLuc').AsFloat > 10) and (not HrDataMain.kiemTraNgayHieuLucHopLe(FieldByName('Manv').AsString, FieldByName('QTLV_Nhom_DacBiet').AsInteger, FieldByName('NgayHieuLuc').AsDateTime)) then
            Abort;

    end;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterCalcFields(DataSet: TDataSet);
begin
   with QrMaster do
    begin
        FieldByName('CALC_TongPhepNam').AsFloat := exVNDRound(
            FieldByName('PhepNam_LuatDinh').AsFloat +
            FieldByName('PhepNam_ChucDanh').AsFloat +
            FieldByName('PhepNam_ThamNien').AsFloat, sysCurHrRound);
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterMaBacLuongChange(Sender: TField);
begin
    with QrMaster do
    begin
        FieldByName('HeSoLuong').AsFloat := HrDataMain.GetHeSoLuong(Sender);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterMaBoPhanChange(Sender: TField);
begin
    with QrMaster do
    begin
        EdMaBoPhan_BanDau.Text := EdMaBoPhan_BanDau.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterMaChucDanhChange(Sender: TField);
begin
    with QrMaster do
    begin
        if Sender.AsString <> '' then
        begin
           FieldByName('PhepNam_LuatDinh').AsFloat := FieldByName('LK_PhepNam_LuatDinh').AsFloat;
           FieldByName('PhepNam_ChucDanh').AsFloat := FieldByName('LK_PhepNam_ChucDanh').AsFloat;
           FieldByName('PhepNam_ThamNien').AsFloat := FieldByName('LK_PhepNam_ThamNien').AsFloat;
           FieldByName('Co_PhepNamMotThang').AsBoolean := FieldByName('LK_Co_PhepNamMotThang').AsBoolean;
        end
        else
        begin
           FieldByName('PhepNam_LuatDinh').Clear;
           FieldByName('PhepNam_ChucDanh').Clear;
           FieldByName('PhepNam_ThamNien').Clear;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterManvChange(Sender: TField);
begin
    EdManvQL.Text := EdManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterManvValidate(Sender: TField);
var
    s: String;
    i, j: Integer;
begin
	s := Sender.AsString;
    if (s = '') or ((s <> '') and (IsDotSelect(s) <> 0)) then
    	Exit;

	if exEmpValid(s, FORM_CODE) <> 0 then
    begin
    	ErrMsg(RS_EMP_ID);
        Abort
    end;

    // Fill Last Info
	if not mCanEdit then
    	Abort;

    with QrWorking do
    begin
        Close;
        Open;
        sManvQL := FieldByName('ManvQL').AsString;
        sMaChiNhanh := FieldByName('MaChiNhanh').AsString;
        sMaPhongBan := FieldByName('MaPhongBan').AsString;
        sMaBoPhan := FieldByName('MaBoPhan').AsString;
        sMaChucDanh := FieldByName('MaChucDanh').AsString;
        sCongViecChinh := FieldByName('CongViecChinh').AsString;
        sXacNhan_Manv := FieldByName('XacNhan_Manv').AsString;
        sXetDuyet_Manv := FieldByName('XetDuyet_Manv').AsString;
        sMaNoiLV := FieldByName('MaNoiLV').AsString;
        sMaNhomLV := FieldByName('MaNhomLV').AsString;
        sMaBacLuong := FieldByName('MaBacLuong').AsString;
        mBacLuong := FieldByName('BacLuong').AsInteger;
        dNgayBacLuong := FieldByName('NgayBacLuong').AsDateTime;
        fHeSoLuong := FieldByName('HeSoLuong').AsFloat;
        fHeSoPhuCap := FieldByName('HeSoPhuCap').AsFloat;
        fLuongChinh := FieldByName('LuongChinh').AsFloat;
        fLuongBaoHiem := FieldByName('LuongBaoHiem').AsFloat;
        fLuongNgay := FieldByName('LuongNgay').AsFloat;
        fLuongGio := FieldByName('LuongGio').AsFloat;
        fTyLeLuongThuViec := FieldByName('TyLeLuongThuViec').AsFloat;
        bCo_MacDinhCong := FieldByName('Co_MacDinhCong').AsBoolean;
        bCo_BHXH := FieldByName('Co_BHXH').AsBoolean;
        bCo_BHYT := FieldByName('Co_BHYT').AsBoolean;
        bCo_BHTN := FieldByName('Co_BHTN').AsBoolean;
        bCo_CongDoan := FieldByName('Co_CongDoan').AsBoolean;
        bCo_BangCong := FieldByName('Co_BangCong').AsBoolean;
        bCo_BangLuong := FieldByName('Co_BangLuong').AsBoolean;
        bCo_ChuyenKhoan := FieldByName('Co_ChuyenKhoan').AsBoolean;
        fPhepNam_LuatDinh := FieldByName('PhepNam_LuatDinh').AsFloat;
        fPhepNam_ChucDanh := FieldByName('PhepNam_ChucDanh').AsFloat;
        fPhepNam_ThamNien := FieldByName('PhepNam_ThamNien').AsFloat;
        bCo_PhepNamMotThang := FieldByName('Co_PhepNamMotThang').AsBoolean;
        mNamQuyHoach := FieldByName('NamQuyHoach').AsInteger;
        mNamQuyHoachBoSung := FieldByName('NamQuyHoachBoSung').AsInteger;
        dNgayBienChe := FieldByName('NgayBienChe').AsDateTime;
        sThongTinKhac := FieldByName('ThongTinKhac').AsString;
    end;

    with QrMaster  do
    begin
        FieldByName('ManvQL').AsString := sManvQL;
        FieldByName('MaChiNhanh').AsString := sMaChiNhanh;
        FieldByName('MaPhongBan').AsString := sMaPhongBan;
        FieldByName('MaBoPhan').AsString := sMaBoPhan;
        FieldByName('MaChucDanh').AsString := sMaChucDanh;
        FieldByName('CongViecChinh').AsString := sCongViecChinh;
        FieldByName('XacNhan_Manv').AsString := sXacNhan_Manv;
        FieldByName('XetDuyet_Manv').AsString := sXetDuyet_Manv;
        FieldByName('MaNoiLV').AsString := sMaNoiLV;
        FieldByName('MaNhomLV').AsString := sMaNhomLV;
        FieldByName('MaBacLuong').AsString := sMaBacLuong;
        FieldByName('BacLuong').AsInteger := mBacLuong;
        if dNgayBacLuong > 100 then
            FieldByName('NgayBacLuong').AsDateTime := dNgayBacLuong;
        FieldByName('HeSoPhuCap').AsFloat := fHeSoPhuCap;
        FieldByName('LuongChinh').AsFloat := fLuongChinh;
        FieldByName('LuongBaoHiem').AsFloat := fLuongBaoHiem;
        FieldByName('LuongNgay').AsFloat := fLuongNgay;
        FieldByName('LuongGio').AsFloat := fLuongGio;
        FieldByName('TyLeLuongThuViec').AsFloat := fTyLeLuongThuViec;
        FieldByName('Co_MacDinhCong').AsBoolean := bCo_MacDinhCong;
        FieldByName('Co_BHXH').AsBoolean := bCo_BHXH;
        FieldByName('Co_BHYT').AsBoolean := bCo_BHYT;
        FieldByName('Co_BHTN').AsBoolean := bCo_BHTN;
        FieldByName('Co_CongDoan').AsBoolean := bCo_CongDoan;
        FieldByName('Co_BangCong').AsBoolean := bCo_BangCong;
        FieldByName('Co_BangLuong').AsBoolean := bCo_BangLuong;
        FieldByName('Co_ChuyenKhoan').AsBoolean := bCo_ChuyenKhoan;
        FieldByName('PhepNam_LuatDinh').AsFloat := fPhepNam_LuatDinh;
        FieldByName('PhepNam_ChucDanh').AsFloat := fPhepNam_ChucDanh;
        FieldByName('PhepNam_ThamNien').AsFloat := fPhepNam_ThamNien;
        FieldByName('Co_PhepNamMotThang').AsBoolean := bCo_PhepNamMotThang;
        FieldByName('NamQuyHoach').AsInteger := mNamQuyHoach;
        FieldByName('NamQuyHoachBoSung').AsInteger := mNamQuyHoachBoSung;
        FieldByName('NgayBienChe').AsDateTime := dNgayBienChe;
        FieldByName('ThongTinKhac').AsString := sThongTinKhac;

        EdXacNhan_ManvQL.Text := EdXacNhan_ManvQL.Field.AsString;
        EdXetDuyet_ManvQL.Text := EdXetDuyet_ManvQL.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterMaPhongBanChange(Sender: TField);
begin
    with QrMaster do
    begin
         FieldByName('MaBoPhan').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterMaQTLVChange(Sender: TField);
begin
    with QrMaster do
    begin
        FieldByName('QTLV_Nhom_DacBiet').AsInteger := FieldByName('LK_QTLV_Nhom_DacBiet').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterNgayHieuLucChange(Sender: TField);
begin
    with QrMaster do
    begin
        if FieldByName('NgayKy').AsFloat < 10 then
            FieldByName('NgayKy').AsDateTime := FieldByName('NgayHieuLuc').AsDateTime;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterXacNhan_ManvChange(Sender: TField);
begin
    EdXacNhan_ManvQL.Text := EdXacNhan_ManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterXetDuyet_ManvChange(Sender: TField);
begin
     EdXetDuyet_ManvQL.Text := EdXetDuyet_ManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrWorkingBeforeOpen(DataSet: TDataSet);
begin
    QrWorking.Parameters[0].Value := QrMaster.FieldByName('Manv').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 550;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('FileIdx'));
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('QTLV_Nhom_DacBiet').AsInteger := 0;
        FieldByName('Co_PhepNamMotThang').AsBoolean := False;
        FieldByName('Co_MacDinhCong').AsBoolean := False;
        FieldByName('Co_BHXH').AsBoolean := False;
        FieldByName('Co_BHYT').AsBoolean := False;
        FieldByName('Co_BHTN').AsBoolean := False;
        FieldByName('Co_CongDoan').AsBoolean := False;
        FieldByName('Co_BangCong').AsBoolean := False;
        FieldByName('Co_BangLuong').AsBoolean := False;
        FieldByName('Co_ChuyenKhoan').AsBoolean := False;
        if mEmpID <> '' then
            FieldByName('Manv').AsString := mEmpID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.QrMasterAfterPost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        HrDataMain.SyncQuaTrinh(FieldByName('Manv').AsString,
            FieldByName('QTLV_Nhom_DacBiet').AsInteger, 1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CbBoPhanDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrMaster.FieldByName('MaPhongBan').AsString;
    QrSec.Filter := 'MaPhongBan=''' + s + '''';
end;

procedure TFrmQuaTrinhLamViec.CbHopDongDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrMaster.FieldByName('Manv').AsString;
    QrHopDong.Filter := 'Manv=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmQuaTrinhLamViec.CbQTLVDropDown(Sender: TObject);
var
    n: Integer;
begin
    n := QrMaster.FieldByName('QTLV_Nhom_DacBiet').AsInteger;
    QrV_HR_QUATRINH_LAMVIEC.Filter := 'Nhom_DacBiet=' + IntToStr(n) + '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmQuaTrinhLamViec.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrList then
        if mEmpID <> '' then
            CdNgayHieuLuc.SetFocus
        else
            CbNhanVien.SetFocus;
    GrList.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.ToolButton2Click(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrMaster do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.Dettach();
begin
     with QrMaster do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.ViewAttachment();
var
	s, sn: String;
begin
	with QrMaster do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmQuaTrinhLamViec.expendCollapsed(bExpend: Boolean);
begin
    PaLyDo.Collapsed := bExpend;
    PaToChuc.Collapsed := bExpend;
    PaChinhQuyen.Collapsed := bExpend;
    PaDinhKem.Collapsed := bExpend;
    PaGhiChu.Collapsed := bExpend;
    PaThongTinLuong.Collapsed := bExpend;
    PaHeSoLuong.Collapsed := bExpend;
end;

end.
