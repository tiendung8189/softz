object HrDataMain: THrDataMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 456
  Width = 907
  object QrDM_CHUCVU: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_CHUCVU')
    Left = 344
    Top = 41
  end
  object QrDM_NHOMLV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NHOM_LAMVIEC')
    Left = 496
    Top = 225
  end
  object QrDM_CHUCDANH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_CHUCDANH')
    Left = 328
    Top = 313
  end
  object QrDM_CALAMVIEC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_CALAMVIEC'
      'order by MaCa')
    Left = 832
    Top = 89
  end
  object HR_DM_HOPDONG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_HOPDONG'
      'order by SORT')
    Left = 472
    Top = 97
  end
  object QrDM_LYDO_VANGMAT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_LYDO_VANGMAT')
    Left = 392
    Top = 49
  end
  object QrDMPHONGBAN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_PHONGBAN')
    Left = 384
    Top = 313
  end
  object QrDMBOPHAN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_BOPHAN')
    Left = 432
    Top = 313
  end
  object spHR_Invalid_Manv_By_Function: TADOCommand
    CommandText = 'spHR_Invalid_Manv_By_Function;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MACN'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MANV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FUNC'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
    Left = 44
    Top = 52
  end
  object QrNHOM_PHUCAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_NHOM_PHUCAP_CODINH')
    Left = 408
    Top = 1
  end
  object QrLOAI_CANGHI: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_CANGHI')
    Left = 770
    Top = 93
  end
  object QrLOAI_GIOITINH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_GIOITINH')
    Left = 770
    Top = 229
  end
  object QrV_QUOCTICH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_QUOCTICH')
    Left = 488
    Top = 313
  end
  object QrV_DANTOC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DANTOC')
    Left = 664
    Top = 313
  end
  object QrV_TONGIAO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_TONGIAO')
    Left = 552
    Top = 313
  end
  object QrV_VANHOA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_VANHOA')
    Left = 608
    Top = 313
  end
  object QrLOAI_THOIVIEC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_THOIVIEC')
    Left = 266
    Top = 93
  end
  object spHR_CAPNHAT_BANG_CONGLUONG: TADOCommand
    CommandText = 'spHR_CAPNHAT_BANG_CONGLUONG;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 116
    Top = 148
  end
  object QrDMTK_HR: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_TAIKHOAN')
    Left = 828
    Top = 9
  end
  object QrDmNhacViec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MaNhacViec, TenNhacViec, TenNhacViec_TA, SoNgay'
      '  from HR_DM_NHACVIEC'
      'order by MaNhacViec')
    Left = 512
  end
  object QrLYDO_KTKL: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  *'
      '  from HR_DM_LYDO_KTKL')
    Left = 632
    Top = 92
  end
  object QrLOAI_LYDO_KTKL: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from V_HR_LOAI_KTKL')
    Left = 464
    Top = 4
  end
  object QrDM_NOILV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  HR_DM_NOI_LAMVIEC  ')
    Left = 440
    Top = 225
  end
  object QrDM_NGUONTUYENDUNG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_NGUON_TUYENDUNG ')
    Left = 568
    Top = 225
  end
  object QrDM_LYDO_TUYENDUNG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  *'
      '  from  HR_DM_QTLV'
      'where MaQTLV_Nhom in ('
      #9#9#9'select MaQTLV_Nhom'
      #9#9#9'   from HR_DM_QTLV_NHOM'
      #9#9#9'where Nhom_DacBiet = 1'
      #9#9'      )')
    Left = 640
    Top = 225
  end
  object QrDMNOIKHAMBENH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_NOI_KHAMBENH')
    Left = 768
    Top = 9
  end
  object QrV_HR_NHANVIEN_DOITUONG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_NHANVIEN_DOITUONG')
    Left = 584
    Top = 89
  end
  object QrV_HONNHAN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_HONNHAN')
    Left = 712
    Top = 305
  end
  object QrV_BHXH_NOICAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_BHXH_NOICAP')
    Left = 208
    Top = 313
  end
  object QrV_BHYT_NOICAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_BHYT_NOICAP')
    Left = 272
    Top = 305
  end
  object QrV_HR_CCCD_NOICAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_CCCD_NOICAP')
    Left = 80
    Top = 313
  end
  object QrV_HR_HOCHIEU_LOAI: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_HOCHIEU_LOAI')
    Left = 16
    Top = 225
  end
  object QrV_HR_DANG_TINHTRANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DANG_TINHTRANG')
    Left = 208
    Top = 225
  end
  object QrV_HR_DOAN_TINHTRANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DOAN_TINHTRANG')
    Left = 272
    Top = 225
  end
  object QrV_HR_QUANHE: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_QUANHE')
    Left = 568
    Top = 41
  end
  object QrV_HR_TAILIEU: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_KIENTHUC')
    Left = 328
    Top = 225
  end
  object QrV_HR_CHUNGCHI: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_CHUNGCHI')
    Left = 400
    Top = 97
  end
  object QrDM_HR_PHUCAP: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM HR_DM_PHUCAP')
    Left = 708
    Top = 8
  end
  object QrV_HR_PHUCAP_LOAI: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_PHUCAP_LOAI')
    Left = 520
    Top = 41
  end
  object QrV_HR_NHOMMAU: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_NHOMMAU')
    Left = 768
    Top = 305
  end
  object QrV_HR_DANG_CHIBO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DANG_CHIBO')
    Left = 832
    Top = 225
  end
  object spHr_DM_BACLUONG_Get_HeSoLuong: TADOStoredProc
    Connection = DataMain.Conn
    ProcedureName = 'spHr_DM_BACLUONG_Get_HeSoLuong;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@MaBacLuong'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@BacLuong'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 100
    Top = 48
  end
  object QrDM_BACLUONG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_BACLUONG')
    Left = 712
    Top = 225
  end
  object QrDM_THIETBI_TAISAN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_THIETBI')
    Left = 384
    Top = 225
  end
  object QrDM_THIETBI_TAISAN_NHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_THIETBI_NHOM')
    Left = 568
    Top = 1
  end
  object spHR_LICHSU_QTLV_Sync: TADOCommand
    CommandText = 'spHR_LICHSU_QTLV_Sync;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Nhom_DacBiet'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Loai'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdxEdit'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end>
    Left = 52
    Top = 8
  end
  object QrV_HR_QUATRINH_LAMVIEC: TADOQuery
    Active = False
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DM_QTLV')
    Left = 528
    Top = 92
  end
  object DsV_HR_QUATRINH_LAMVIEC: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_QUATRINH_LAMVIEC
    Left = 532
    Top = 136
  end
  object DsDM_CHUCDANH: TDataSource
    AutoEdit = False
    DataSet = QrDM_CHUCDANH
    Left = 332
    Top = 352
  end
  object DsDM_NOILV: TDataSource
    AutoEdit = False
    DataSet = QrDM_NOILV
    Left = 444
    Top = 264
  end
  object DsDM_NHOMLV: TDataSource
    AutoEdit = False
    DataSet = QrDM_NHOMLV
    Left = 500
    Top = 264
  end
  object DsDMPHONGBAN: TDataSource
    AutoEdit = False
    DataSet = QrDMPHONGBAN
    Left = 388
    Top = 352
  end
  object DsDMBOPHAN: TDataSource
    AutoEdit = False
    DataSet = QrDMBOPHAN
    Left = 436
    Top = 352
  end
  object DsDM_BACLUONG: TDataSource
    AutoEdit = False
    DataSet = QrDM_BACLUONG
    Left = 716
    Top = 264
  end
  object DsV_HR_THIETBI_TAISAN: TDataSource
    AutoEdit = False
    DataSet = QrDM_THIETBI_TAISAN
    Left = 388
    Top = 264
  end
  object QrTinhTrang_DangKy: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  * '
      '   from V_HR_TINHTRANG_DANGKY'
      'order by MA_HOTRO asc')
    Left = 708
    Top = 92
  end
  object DsDM_LYDO_TUYENDUNG: TDataSource
    AutoEdit = False
    DataSet = QrDM_LYDO_TUYENDUNG
    Left = 644
    Top = 272
  end
  object DsDM_NGUONTUYENDUNG: TDataSource
    AutoEdit = False
    DataSet = QrDM_NGUONTUYENDUNG
    Left = 572
    Top = 272
  end
  object DsV_HR_DOITUONG_NHANVIEN: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_NHANVIEN_DOITUONG
    Left = 588
    Top = 120
  end
  object DsV_HR_NHOMMAU: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_NHOMMAU
    Left = 772
    Top = 352
  end
  object DsV_QUOCTICH: TDataSource
    AutoEdit = False
    DataSet = QrV_QUOCTICH
    Left = 492
    Top = 352
  end
  object DsV_TONGIAO: TDataSource
    AutoEdit = False
    DataSet = QrV_TONGIAO
    Left = 548
    Top = 352
  end
  object DsV_VANHOA: TDataSource
    AutoEdit = False
    DataSet = QrV_VANHOA
    Left = 604
    Top = 352
  end
  object DsV_DANTOC: TDataSource
    AutoEdit = False
    DataSet = QrV_DANTOC
    Left = 668
    Top = 352
  end
  object DsLOAI_GIOITINH: TDataSource
    AutoEdit = False
    DataSet = QrLOAI_GIOITINH
    Left = 772
    Top = 264
  end
  object DsV_HONNHAN: TDataSource
    AutoEdit = False
    DataSet = QrV_HONNHAN
    Left = 716
    Top = 352
  end
  object DsV_HR_DANG_CHIBO: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DANG_CHIBO
    Left = 836
    Top = 272
  end
  object DsV_HR_DANG_TINHTRANG: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DANG_TINHTRANG
    Left = 212
    Top = 264
  end
  object DsV_HR_DOAN_TINHTRANG: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DOAN_TINHTRANG
    Left = 276
    Top = 264
  end
  object DsTinhTrang_DangKy: TDataSource
    AutoEdit = False
    DataSet = QrTinhTrang_DangKy
    Left = 708
    Top = 128
  end
  object DsV_CCCD_NOICAP: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_CCCD_NOICAP
    Left = 84
    Top = 352
  end
  object DsV_BHXH_NOICAP: TDataSource
    AutoEdit = False
    DataSet = QrV_BHXH_NOICAP
    Left = 212
    Top = 352
  end
  object DsV_BHYT_NOICAP: TDataSource
    AutoEdit = False
    DataSet = QrV_BHYT_NOICAP
    Left = 276
    Top = 352
  end
  object DsDMNOIKHAMBENH: TDataSource
    AutoEdit = False
    DataSet = QrDMNOIKHAMBENH
    Left = 772
    Top = 56
  end
  object DsLOAI_THOIVIEC: TDataSource
    AutoEdit = False
    DataSet = QrLOAI_THOIVIEC
    Left = 268
    Top = 136
  end
  object DsV_HR_HOCHIEU_LOAI: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_HOCHIEU_LOAI
    Left = 20
    Top = 264
  end
  object DsQrDMTK_HR: TDataSource
    AutoEdit = False
    DataSet = QrDMTK_HR
    Left = 828
    Top = 48
  end
  object DsV_HR_TAILIEU: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_TAILIEU
    Left = 332
    Top = 264
  end
  object DsV_HR_CHUNGCHI: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_CHUNGCHI
    Left = 404
    Top = 136
  end
  object DsHR_DM_HOPDONG: TDataSource
    AutoEdit = False
    DataSet = HR_DM_HOPDONG
    Left = 476
    Top = 128
  end
  object DsLYDO_KTKL: TDataSource
    AutoEdit = False
    DataSet = QrLYDO_KTKL
    Left = 636
    Top = 136
  end
  object DsDM_PHUCAP: TDataSource
    AutoEdit = False
    DataSet = QrDM_HR_PHUCAP
    Left = 708
    Top = 48
  end
  object DsDM_CALAMVIEC: TDataSource
    AutoEdit = False
    DataSet = QrDM_CALAMVIEC
    Left = 836
    Top = 136
  end
  object DsLOAI_CANGHI: TDataSource
    AutoEdit = False
    DataSet = QrLOAI_CANGHI
    Left = 772
    Top = 136
  end
  object spHR_DM_NHANVIEN_Set_NgayTraTaiSan: TADOCommand
    CommandText = 'spHR_DM_NHANVIEN_Set_NgayTraTaiSan;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end>
    Left = 40
    Top = 152
  end
  object spHR_Invalid_NgayThoiViec_By_Ngay: TADOCommand
    CommandText = 'spHR_Invalid_NgayThoiViec_By_Ngay;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@DenNgay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pMSG'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 108
    Top = 92
  end
  object spHR_LICHSU_QTLV_Invalid_NgayHieuLuc: TADOCommand
    CommandText = 'spHR_LICHSU_QTLV_Invalid_NgayHieuLuc;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@QTLV_Nhom_DacBiet'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NgayHieuLuc'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Lang'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@pMSG'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 44
    Top = 100
  end
  object ALLOC_MANVQL: TADOCommand
    CommandText = 'ALLOC_MANVQL;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaNhomLV'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@CODE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 96
    Top = 8
  end
  object QrV_HR_DM_NHANVIEN_CCCD: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DM_NHANVIEN_CCCD')
    Left = 136
    Top = 313
  end
  object DsQrV_HR_DM_NHANVIEN_CCCD: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DM_NHANVIEN_CCCD
    Left = 140
    Top = 352
  end
  object QrV_HR_DM_NHANVIEN_HOCHIEU: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DM_NHANVIEN_HOCHIEU')
    Left = 136
    Top = 225
  end
  object DsQrV_HR_DM_NHANVIEN_HOCHIEU: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DM_NHANVIEN_HOCHIEU
    Left = 140
    Top = 264
  end
  object QrV_HR_LOAI_HOPDONG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_HOPDONG')
    Left = 336
    Top = 97
  end
  object DsV_HR_LOAI_HOPDONG: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_LOAI_HOPDONG
    Left = 340
    Top = 136
  end
  object QrV_HR_CCCD_NHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_CCCD_NHOM')
    Left = 16
    Top = 313
  end
  object DsV_HR_CCCD_NHOM: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_CCCD_NHOM
    Left = 20
    Top = 352
  end
  object DsV_HR_HOCHIEU_NHOM: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_HOCHIEU_NHOM
    Left = 84
    Top = 264
  end
  object QrV_HR_HOCHIEU_NHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_HOCHIEU_NHOM')
    Left = 80
    Top = 225
  end
  object QrHRNamNgayDauNgayCuoi: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'NAM'
        Attributes = [paNullable]
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'LOAI_CHUCNANG'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      
        'select * from dbo.fnHr_Nam_NgayDau_NgayCuoi(:NAM, :LOAI_CHUCNANG' +
        ')')
    Left = 340
    Top = 4
  end
  object QrV_HR_LOAI_NGAY_TINHCONG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_LOAI_NGAY_TINHCONG')
    Left = 464
    Top = 41
  end
  object SETPARAM: TADOCommand
    Parameters = <>
    Left = 144
    Top = 16
  end
  object QrDMNV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NHANVIEN')
    Left = 208
    Top = 9
  end
  object DsDMNV: TDataSource
    AutoEdit = False
    DataSet = QrDMNV
    Left = 212
    Top = 40
  end
  object QrDMNV_DANGLAMVIEC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NHANVIEN'
      '  where isnull(NgayThoiViec,'#39#39') = '#39#39)
    Left = 256
    Top = 9
  end
  object DsDMNV_DANGLAMVIEC: TDataSource
    AutoEdit = False
    DataSet = QrDMNV_DANGLAMVIEC
    Left = 260
    Top = 40
  end
  object QrV_HR_DAOTAO_NHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DAOTAO_NHOM')
    Left = 840
    Top = 313
  end
  object DsV_HR_DAOTAO_NHOM: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_DAOTAO_NHOM
    Left = 844
    Top = 352
  end
  object spHR_CAPNHAT_BANG_LICHLV: TADOCommand
    CommandText = 'spHR_CAPNHAT_BANG_LICHLV;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pNam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pThang'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 188
    Top = 132
  end
end
