object FrmBosungCong: TFrmBosungCong
  Left = 246
  Top = 182
  BorderStyle = bsDialog
  Caption = 'C'#7853'p Nh'#7853't B'#7893' Sung'
  ClientHeight = 273
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = TntFormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 8
    Top = 33
    Width = 399
    Height = 232
    DisableThemes = False
    CaptionColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Items = <
      item
        DataField = 'LK_TENVANGMAT'
        Caption = 'L'#253' do ngh'#7881
        Alignment = taRightJustify
        CustomControl = CbLeave
        WordWrap = False
      end
      item
        DataField = 'SONGAY_VANGMAT'
        Caption = 'S'#7889' ng'#224'y ngh'#7881
        Alignment = taRightJustify
        CustomControl = CbLeaveAmout
        WordWrap = False
      end
      item
        DataField = 'LK_TENVANGMAT1'
        Caption = 'L'#253' do ngh'#7881' (2)'
        Alignment = taRightJustify
        CustomControl = CbLeave2
        WordWrap = False
      end
      item
        DataField = 'SONGAY_VANGMAT1'
        Caption = 'S'#7889' ng'#224'y ngh'#7881' (2)'
        Alignment = taRightJustify
        CustomControl = CbLeaveAmout2
        WordWrap = False
      end
      item
        DataField = 'OTN Hours'
        Caption = 'T'#7893'ng s'#7889' gi'#7901' t'#259'ng ca '#273#234'm'
        Alignment = taRightJustify
        WordWrap = False
      end
      item
        DataField = 'ST Hours'
        Caption = 'T'#7893'ng s'#7889' gi'#7901' t'#259'ng ca C.N'
        Alignment = taRightJustify
        WordWrap = False
      end
      item
        DataField = 'HT Hours'
        Caption = 'T'#7893'ng s'#7889' gi'#7901' t'#259'ng ca ng'#224'y l'#7877
        Alignment = taRightJustify
        WordWrap = False
      end
      item
        DataField = 'Advanced Percent'
        Caption = 'Advanced Percent'
        WordWrap = False
      end
      item
        DataField = 'Allowance'
        Caption = 'Allowance'
        WordWrap = False
      end
      item
        DataField = 'Allowance1'
        Caption = 'Allowance1'
        WordWrap = False
      end
      item
        DataField = 'Allowance2'
        Caption = 'Allowance2'
        WordWrap = False
      end
      item
        DataField = 'Allowance3'
        Caption = 'Allowance3'
        WordWrap = False
      end
      item
        DataField = 'Allowance4'
        Caption = 'Allowance4'
        WordWrap = False
      end
      item
        DataField = 'Annual Leaves'
        Caption = 'Annual Leaves'
        WordWrap = False
      end
      item
        DataField = 'Attendance Bonus'
        Caption = 'Attendance Bonus'
        WordWrap = False
      end
      item
        DataField = 'Base Salary'
        Caption = 'Base Salary'
        WordWrap = False
      end
      item
        DataField = 'Base Salary USD'
        Caption = 'Base Salary USD'
        WordWrap = False
      end
      item
        DataField = 'Basic Pay'
        Caption = 'Basic Pay'
        WordWrap = False
      end
      item
        DataField = 'CardID'
        Caption = 'M'#227' th'#7867
        WordWrap = False
      end
      item
        DataField = 'Comment'
        Caption = 'Comment'
        WordWrap = False
      end
      item
        DataField = 'CREATE_BY'
        Caption = 'CREATE_BY'
        WordWrap = False
      end
      item
        DataField = 'CREATE_DATE'
        Caption = 'CREATE_DATE'
        WordWrap = False
      end
      item
        DataField = 'Daily Allowance'
        Caption = 'Daily Allowance'
        WordWrap = False
      end
      item
        DataField = 'Daily Basic Pay'
        Caption = 'Daily Basic Pay'
        WordWrap = False
      end
      item
        DataField = 'Day Pay'
        Caption = 'Day Pay'
        WordWrap = False
      end
      item
        DataField = 'Days of Month'
        Caption = 'Days of Month'
        WordWrap = False
      end>
    CaptionWidth = 197
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
    object CbLeave: TwwDBLookupCombo
      Left = 189
      Top = 89
      Width = 179
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TenVangMat'#9'25'#9'TEN_LYDO'#9'F'
        'MaVangMat'#9'6'#9'MA_LYDO'#9'F')
      DataField = 'LK_TENVANGMAT'
      LookupTable = HrDataMain.QrDM_LYDO_VANGMAT
      LookupField = 'MaVangMat'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 0
      Visible = False
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
    end
    object CbLeaveAmout: TwwDBComboBox
      Left = 193
      Top = 114
      Width = 179
      Height = 22
      TabStop = False
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = True
      AutoDropDown = True
      ShowMatchText = True
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'SONGAY_VANGMAT'
      DropDownCount = 8
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ItemHeight = 0
      Items.Strings = (
        '0.5'#9'0.5'
        '1'#9'1')
      ParentCtl3D = False
      ParentFont = False
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
      Visible = False
    end
    object CbLeaveAmout2: TwwDBComboBox
      Left = 193
      Top = 115
      Width = 179
      Height = 22
      TabStop = False
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = True
      AutoDropDown = True
      ShowMatchText = True
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'SONGAY_VANGMAT1'
      DropDownCount = 8
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ItemHeight = 0
      Items.Strings = (
        '0.5'#9'0.5'
        '1'#9'1')
      ParentCtl3D = False
      ParentFont = False
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 2
      UnboundDataType = wwDefault
      Visible = False
    end
    object CbLeave2: TwwDBLookupCombo
      Left = 189
      Top = 88
      Width = 179
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TenVangMat'#9'25'#9'TEN_LYDO'#9'F'
        'MaVangMat'#9'6'#9'MA_LYDO'#9'F')
      DataField = 'LK_TENVANGMAT1'
      LookupTable = HrDataMain.QrDM_LYDO_VANGMAT
      LookupField = 'MaVangMat'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 3
      Visible = False
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
    end
  end
  object DBNavigator1: TRzDBNavigator
    Left = 142
    Top = 5
    Width = 264
    Height = 25
    Cursor = 1
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbPost, nbCancel]
    ImageIndexes.Post = 1
    ImageIndexes.Cancel = 2
    Images = DataMain.ImageNavi
    BorderOuter = fsNone
    TabOrder = 1
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 4
    Top = 4
    object CmdNext: TAction
      Caption = 'CmdNext'
      ShortCut = 34
      OnExecute = CmdNextExecute
    end
    object CmdPrior: TAction
      Caption = 'CmdPrior'
      ShortCut = 33
      OnExecute = CmdPriorExecute
    end
  end
end
