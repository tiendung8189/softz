﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit KhenThuongKyLuatDangKy;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, rDBComponents, Vcl.Buttons, DBCtrlsEh, DBGridEh,
  DBLookupEh, frameEmp, DbLookupComboboxEh2, fcCombo, kbmMemTable;

type
  TFrmKhenThuongKyLuatDangKy = class(TForm)
    ActionList: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdImport: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdClear: TAction;
    PaThongTin: TisPanel;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    Panel1: TPanel;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    DsEmp: TDataSource;
    gbViecLam: TGroupBox;
    GroupBox1: TGroupBox;
    rDBCheckBox1: TrDBCheckBox;
    Label1: TLabel;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    PaInfo: TPanel;
    Label2: TLabel;
    dpNgayHieuLuc: TwwDBDateTimePicker;
    wwDBEdit5: TDBEditEh;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    EdNguoiKy_Manv: TDBEditEh;
    CbLyDo: TDbLookupComboboxEh2;
    EdKTKLLoai: TDBEditEh;
    Panel3: TPanel;
    BtnContinute: TBitBtn;
    BitBtn2: TBitBtn;
    PaThongTinThem: TPanel;
    GrList: TStringGrid;
    PopList: TAdvPopupMenu;
    ImporttExcel1: TMenuItem;
    N1: TMenuItem;
    Xadanhsch1: TMenuItem;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    RgLoai: TRadioGroup;
    Panel2: TPanel;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaPhongBan: TDBEditEh;
    EdMaBoPhan: TDBEditEh;
    CbNhanVien: TDbLookupComboboxEh2;
    EdMaNhanVien: TDBEditEh;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSec: TDataSource;
    DsDep: TDataSource;
    DsSite: TDataSource;
    CmdContinue: TAction;
    CmdLyDoKThuongKLuat: TAction;
    spIMP_HR_LICHSU_KTKL_ImportList: TADOCommand;
    TbDummy: TkbmMemTable;
    DsDummy: TDataSource;
    TbDummyNgayHieuLuc: TDateTimeField;
    TbDummySoQuyetDinh: TWideStringField;
    TbDummyNgayKy: TDateTimeField;
    TbDummySoTien: TFloatField;
    TbDummyNguoiKy: TWideStringField;
    TbDummyMaKThuongKLuat: TWideStringField;
    TbDummyThang: TIntegerField;
    TbDummyNam: TIntegerField;
    TbDummyCo_ThueTNCN: TBooleanField;
    TbDummyCalc_FileName: TWideStringField;
    TbDummyFileIdx: TGuidField;
    TbDummyFileName: TWideStringField;
    TbDummyFileExt: TWideStringField;
    TbDummyGhiChu: TWideMemoField;
    Filter: TwwFilterDialog2;
    TbDummyManv: TWideStringField;
    TbDummyLK_KThuongKLuatLoai: TWideStringField;
    TbDummyLK_TenLoaiKThuongKLuat: TWideStringField;
    QrFileContentIdx: TAutoIncField;
    Panel7: TPanel;
    Panel8: TPanel;
    BitBtn4: TBitBtn;
    Panel9: TPanel;
    spIMP_HR_LICHSU_KTKL_InsertData: TADOCommand;
    spIMP_HR_LICHSU_KTKL_DeleteList: TADOCommand;
    spIMP_HR_LICHSU_KTKL_Check: TADOCommand;
    CHECK_RECORD_ERROR: TADOCommand;
    DsList: TDataSource;
    QrList: TADOQuery;
    QrListLK_TenLoaiKThuongKLuat: TWideStringField;
    QrListLK_Tennv: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_TenKThuongKLuat: TWideStringField;
    QrListLK_Co_ThueTNCN: TBooleanField;
    QrListLK_KThuongKLuatLoai: TWideStringField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListKThuongKLuatLoai: TIntegerField;
    QrListManv: TWideStringField;
    QrListMaKThuongKLuat: TWideStringField;
    GrListData: TwwDBGrid2;
    CmdUpdate: TAction;
    CmdRegister: TAction;
    CmdDelRecordError: TAction;
    CmdDelRecordSelected: TAction;
    CmdRefresh: TAction;
    CmdImportExcel: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Xadng1: TMenuItem;
    Xaccdngangli1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdClearExecute(Sender: TObject);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdLyDoKThuongKLuatExecute(Sender: TObject);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbPhongBanExit(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure TbDummyCalcFields(DataSet: TDataSet);
    procedure CmdContinueExecute(Sender: TObject);
    procedure TbDummyAfterInsert(DataSet: TDataSet);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure CmdDelRecordSelectedExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CmdRegisterExecute(Sender: TObject);
  private
  	mCanEdit: Boolean;
    mTransNo, countError, countSubmit: Integer;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
    procedure SmartFocus;
    procedure DeleteRow(pRow: Integer);
    procedure AddRow(p1, p2: String);
    procedure RegisterData(pLst: String);
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; var pTransNo: Integer) : Boolean;
  end;

var
  FrmKhenThuongKyLuatDangKy: TFrmKhenThuongKyLuatDangKy;

const
    TABLE_NAME = 'IMP_HR_LICHSU_KTKL';
    FORM_CODE = 'IMP_HR_LICHSU_KTKL';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmLyDoKTKL, Clipbrd, HrData; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKhenThuongKyLuatDangKy.Execute;
begin
    mTransNo := 0;
    mCanEdit := rCanEdit(r);
    Result := ShowModal = mrOK;
    if Result then
    begin
        pTransNo := mTransNo;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';

procedure TFrmKhenThuongKyLuatDangKy.FormCreate(Sender: TObject);
begin
    AddAllFields(QrList, TABLE_NAME);

    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);

    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;
	OpenDataSets([QrSite, QrDep, QrSec, QrEmp, QrList, HrDataMain.QrDMBOPHAN]);

    SetDisplayFormat(TbDummy, sysCurFmt);
    SetShortDateFormat(TbDummy, ShortDateFormat);
    SetDictionary(TbDummy, FORM_CODE, Filter);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrListData);
    SetDictionary(QrList, FORM_CODE, Filter);

    with TbDummy do
    begin
        Open;
    	Append;
    end;

    RgLoai.OnClick(Nil);
    dpNgayHieuLuc.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    TbDummy.Cancel;
    if QrFileContent.Active then
       QrFileContent.CancelBatch;

	CloseDataSets([TbDummy, QrEmp]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdImportExecute(Sender: TObject);
var
    ls: TStringList;
    i: Integer;
    s, sName: String;
begin
    ls := TStringList.Create;
    ls.Text := Clipboard.AsText;
    for i := 0 to ls.Count - 1 do
    begin
        s := Trim(ls[i]);
        if (s <> '') then
        begin
            with QrEmp do
            begin
                Filter := 'ManvQL=' + QuotedStr(s);
                sName := FieldByName('Tennv').AsString;
            end;

            AddRow(s, sName);
        end;
    end;
    QrEmp.Filter := '';
    ls.Free;
    Clipboard.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdInsExecute(Sender: TObject);
var
	s1, s2: String;
begin
    case RgLoai.ItemIndex of
    0:
    	begin
            s1 := CbPhongBan.Value;
        	s2 := CbPhongBan.Text;
        end;
    1:
    	begin
            s1 := CbBoPhan.Value;
        	s2 := CbBoPhan.Text;
        end;
    2:
    	begin
            s1 := CbNhanVien.Value;
        	s2 := CbNhanVien.Text;
        end;
    else
    	Exit;
    end;

    AddRow(s1, s2);
	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdDelExecute(Sender: TObject);
begin
	DeleteRow(GrList.Row);
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdDelRecordErrorExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdDelRecordSelectedExecute(
  Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdFileMinusExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdFilePlusExecute(Sender: TObject);
begin
     Attach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdLyDoKThuongKLuatExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_LYDO_KTKL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLyDoKTKL, FrmDmLyDoKTKL);
    FrmDmLyDoKTKL.Execute(r);

    HrDataMain.QrLYDO_KTKL.Requery;
    HrDataMain.QrLOAI_LYDO_KTKL.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_LICHSU_KTKL_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                mTransNo := 0;
                CmdRefresh.Execute;
                Inc(countSubmit);
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdContinueExecute(Sender: TObject);
var
    pLst: String;
    i: Integer;
begin

    with TbDummy do
    begin
        if BlankConfirm(TbDummy, ['NgayHieuLuc', 'SoQuyetDinh', 'NgayKy', 'NguoiKy', 'MaKThuongKLuat']) then
            Abort;

        if FieldByName('SoTien').AsFloat > 0 then
        begin
            if BlankConfirm(TbDummy, ['Thang', 'Nam']) then
                Abort;
        end;
    end;

    pLst := '';
    with GrList do
    for i := 0 to RowCount - 1 do
    begin
        if Cells[0, i] = '' then
            Break;
        if pLst <> '' then
            pLst := pLst + ',';
        pLst := pLst + '''' + Cells[0, i] + '''';
    end;

    RegisterData(pLst);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyList, bIsError, bViewFile: Boolean;
    s: String;
begin
    bViewFile := False;
    with TbDummy do
    begin
        if Active then
            bViewFile := FieldByName('FileExt').AsString <> '';

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdFileMinus.Enabled := bViewFile and (not bBrowse);
    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    with QrList do
    begin
    	if not Active then
        	Exit;
        bEmptyList := IsEmpty;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;

    CmdContinue.Enabled := (GrList.Cells[0, 0] <> '');
    CmdClear.Enabled := (GrList.Cells[0, 0] <> '');
    CmdDel.Enabled := (GrList.Cells[0, GrList.Row] <> '');
	RgLoai.Enabled := (GrList.Cells[0, 0] = '');
    CmdImport.Enabled := (RgLoai.ItemIndex = 2);

    bIsError := countError > 0;
    CmdSearch.Enabled := not bEmptyList;
    CmdDelRecordSelected.Enabled := not bEmptyList;
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmptyList);


    case RgLoai.ItemIndex of
    0:
       	s := EdMaPhongBan.Text;
    1:
       	s := EdMaBoPhan.Text;
    2:
       	s := EdMaNhanVien.Text;
    end;
    CmdIns.Enabled := s <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(TbDummy.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.QrListBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CbPhongBanExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;

	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrDep do
        if Locate('MaPhongBan', s, []) then
        begin
            EdMaPhongBan.Text := FieldByName('Ma').AsString;
            s1 := EdMaBoPhan.Text;
            with QrSec do
            if (s1 <> '') then
            begin
                if  not Locate('MaPhongBan;Ma', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaBoPhan.Text := FieldByName('Ma').AsString;
                CbBoPhan.Value := FieldByName('MaBoPhan').AsString;
            end
            else
            begin
                EdMaBoPhan.Text := '';
                CbBoPhan.Text := '';
            end;
        end
        else
        begin
            EdMaPhongBan.Text := '';
            EdMaBoPhan.Text := '';
            CbBoPhan.Text := '';
        end;
    1:		// bo phan
        with QrSec do
        if (s <> '') and (Locate('MaBoPhan', s, [])) then
        begin
            EdMaBoPhan.Text := FieldByName('Ma').AsString;
        end
        else
        begin
            EdMaBoPhan.Text := '';
        end;
    2:		// nhan vien
		EdMaNhanVien.Text := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaPhongBan.Enabled := n <> 2;
    CbPhongBan.Enabled := n <> 2;

    EdMaBoPhan.Enabled := n = 1;
    CbBoPhan.Enabled := n = 1;

    EdMaNhanVien.Enabled :=  n = 2;
    CbNhanVien.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with TbDummy do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(TbDummy.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.Dettach();
begin
     with TbDummy do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.ViewAttachment();
var
	s, sn: String;
begin

    with TbDummy do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
       
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.SmartFocus;
begin
	try
        case RgLoai.ItemIndex of
        0:
            CbPhongBan.SetFocus;
        1:
            CbBoPhan.SetFocus;
        2:
            CbNhanVien.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.TbDummyAfterInsert(DataSet: TDataSet);
begin
    with TbDummy do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.TbDummyCalcFields(DataSet: TDataSet);
begin
    with TbDummy do
    begin
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.AddRow(p1, p2: String);
var
    i: Integer;
begin
    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := p1;
				Cells[1, i] := p2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = Copy(p1, 1, Length(Cells[0, i])) then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.DeleteRow(pRow: Integer);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, pRow] = '' then
			Exit;

		for i := pRow to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuatDangKy.RegisterData;
var
    s: string;
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    mTransNo := DataMain.GetSeqValue(TABLE_NAME);
	with TbDummy, spIMP_HR_LICHSU_KTKL_ImportList do
    begin
        Prepared := True;
        Parameters.ParamByName('@pTransNo').Value := mTransNo;
        Parameters.ParamByName('@pLoai').Value := RgLoai.ItemIndex;
        Parameters.ParamByName('@pChuoi').Value := pLst;
        Parameters.ParamByName('@pNgayHieuLuc').Value := FieldByName('NgayHieuLuc').AsDateTime;
        Parameters.ParamByName('@pSoQuyetDinh').Value := FieldByName('SoQuyetDinh').AsString;
        Parameters.ParamByName('@pNgayKy').Value := FieldByName('NgayKy').AsDateTime;
        Parameters.ParamByName('@pNguoiKy').Value := FieldByName('NguoiKy').AsString;
        Parameters.ParamByName('@pMaKThuongKLuat').Value := FieldByName('MaKThuongKLuat').AsString;
        Parameters.ParamByName('@pSoTien').Value := FieldByName('SoTien').AsFloat;
        Parameters.ParamByName('@pThang').Value := FieldByName('Thang').AsInteger;
        Parameters.ParamByName('@pNam').Value := FieldByName('Nam').AsInteger;
        Parameters.ParamByName('@pCo_ThueTNCN').Value := FieldByName('Co_ThueTNCN').AsBoolean;
        Parameters.ParamByName('@pFileIdx').Value := TGuidEx.ToString(FieldByName('FileIdx'));
        Parameters.ParamByName('@pFileName').Value := FieldByName('FileName').AsString;
        Parameters.ParamByName('@pFileExt').Value := FieldByName('FileExt').AsString;
        Parameters.ParamByName('@pGhiChu').Value := FieldByName('GhiChu').AsString;
        Parameters.ParamByName('@pUID').Value := sysLogonUID;
        Parameters.ParamByName('@pLang').Value := sysLang;

        try
            Execute;
        except
        end;

        if Parameters[0].Value <> 0 then
        begin
            mTransNo := 0;
            s := Parameters.ParamValues['@returnCode'];
            ErrMsg(s);
        end
        else
        begin
            if QrFileContent.Active then
                QrFileContent.UpdateBatch;
            QrListBeforeOpen(QrList);
            CmdRefresh.Execute;
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKhenThuongKyLuatDangKy.DeleteAllRecord;
begin
    with spIMP_HR_LICHSU_KTKL_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKhenThuongKyLuatDangKy.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;

end.
