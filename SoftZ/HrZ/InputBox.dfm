object FrmInputBox: TFrmInputBox
  Left = 265
  Top = 224
  ActiveControl = EdGhichu
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = ' .: L'#253' do'
  ClientHeight = 69
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  DesignSize = (
    441
    69)
  PixelsPerInch = 96
  TextHeight = 13
  object lbPrompt: TLabel
    Left = 12
    Top = 7
    Width = 133
    Height = 16
    Caption = 'Nh'#7853'p l'#253' do kh'#244'ng duy'#7879't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object EdGhichu: TEdit
    Left = 12
    Top = 31
    Width = 417
    Height = 24
    Anchors = [akLeft, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
end
