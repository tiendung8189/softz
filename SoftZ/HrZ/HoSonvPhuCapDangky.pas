﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoSonvPhuCapDangky;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, rDBComponents, Vcl.Buttons, DBCtrlsEh, DBGridEh,
  DBLookupEh, frameEmp, DbLookupComboboxEh2, fcCombo, kbmMemTable;

type
  TFrmHoSonvPhuCapDangky = class(TForm)
    ActionList: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdClear: TAction;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    Panel1: TPanel;
    DsEmp: TDataSource;
    Panel3: TPanel;
    BtnContinute: TBitBtn;
    BitBtn2: TBitBtn;
    PaThongTinThem: TPanel;
    GrList: TStringGrid;
    PopList: TAdvPopupMenu;
    ImporttExcel1: TMenuItem;
    N1: TMenuItem;
    Xadanhsch1: TMenuItem;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    RgLoai: TRadioGroup;
    Panel2: TPanel;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaPhongBan: TDBEditEh;
    EdMaBoPhan: TDBEditEh;
    CbNhanVien: TDbLookupComboboxEh2;
    EdMaNhanVien: TDBEditEh;
    CmdContinue: TAction;
    CmdLoaiPhuCap: TAction;
    spIMP_HR_DANGKY_PHUCAP_ImportList: TADOCommand;
    TbDummy: TkbmMemTable;
    DsDummy: TDataSource;
    PaInfo: TisPanel;
    gbThongTin: TGroupBox;
    CmdImportExcel: TAction;
    CbPhuCap: TDbLookupComboboxEh2;
    EdLoaiPhuCap: TDBEditEh;
    Label1: TLabel;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    rDBCheckBox1: TrDBCheckBox;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    TbDummyMaPhuCap: TWideStringField;
    TbDummyLK_TenPhuCap: TWideStringField;
    TbDummySoTien: TFloatField;
    TbDummyThangBatDau: TIntegerField;
    TbDummyNamBatDau: TIntegerField;
    TbDummyCo_GioiHan: TBooleanField;
    TbDummySoThang: TFloatField;
    TbDummyThangKetThuc: TIntegerField;
    TbDummyNamKetThuc: TIntegerField;
    TbDummyLK_PhuCapLoai: TWideStringField;
    TbDummyLK_TenPhanLoai: TWideStringField;
    TbDummyGhiChu: TWideStringField;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSec: TDataSource;
    DsDep: TDataSource;
    DsSite: TDataSource;
    TbDummyNgayBatDau: TDateTimeField;
    Panel7: TPanel;
    Panel8: TPanel;
    BitBtn4: TBitBtn;
    Panel9: TPanel;
    spIMP_HR_DANGKY_PHUCAP_InsertData: TADOCommand;
    spIMP_HR_DANGKY_PHUCAP_DeleteList: TADOCommand;
    CHECK_RECORD_ERROR: TADOCommand;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Xadng1: TMenuItem;
    Xaccdngangli1: TMenuItem;
    wwDBGrid21: TwwDBGrid2;
    GrListData: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    DsList: TDataSource;
    QrList: TADOQuery;
    QrListGhiChu: TWideMemoField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListManv: TWideStringField;
    QrListManvQL: TWideStringField;
    QrListTennv: TWideStringField;
    QrListMaPhuCap: TWideStringField;
    QrListNgayBatDau: TDateTimeField;
    QrListThangBatDau: TIntegerField;
    QrListNamBatDau: TIntegerField;
    QrListThangKetThuc: TIntegerField;
    QrListNamKetThuc: TIntegerField;
    QrListCo_GioiHan: TBooleanField;
    QrListSoThang: TFloatField;
    QrListSoTien: TFloatField;
    QrListTenPhuCap: TWideStringField;
    QrListLK_TenPhuCap: TWideStringField;
    QrListLK_PhuCapLoai: TWideStringField;
    QrListLK_TenPhanLoai: TWideStringField;
    QrListMaNhom_PhuCap: TWideStringField;
    QrListTenNhom_PhuCap: TWideStringField;
    QrListMaNhomLV: TWideStringField;
    QrListTenNhomLV: TWideStringField;
    CmdRegister: TAction;
    CmdDelRecordError: TAction;
    CmdDelRecordSelected: TAction;
    CmdRefresh: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdClearExecute(Sender: TObject);
    procedure CmdLoaiPhuCapExecute(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbPhongBanExit(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdContinueExecute(Sender: TObject);
    procedure TbDummyAfterInsert(DataSet: TDataSet);
    procedure TbDummySoThangChange(Sender: TField);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure QrListAfterDelete(DataSet: TDataSet);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure GrListDataCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdDelRecordSelectedExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
  private
  	mCanEdit: Boolean;
    mTransNo, countError: Integer;
    r: WORD;
    mFORM_CODE: string;
    procedure SmartFocus;
    procedure DeleteRow(pRow: Integer);
    procedure AddRow(p1, p2: String);
    procedure RegisterData(pLst: String);
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; pFORM_CODE: string; var pTransNo: Integer) : Boolean;
  end;

var
  FrmHoSonvPhuCapDangky: TFrmHoSonvPhuCapDangky;

const
    TABLE_NAME = 'IMP_HR_LICHSU_PHUCAP';
    FORM_CODE = 'IMP_HR_LICHSU_PHUCAP';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmVangmat, Clipbrd, DmPhucap, HrData; //MassRegLeave;


resourcestring
    RS_INVAILD_APPLY_TIME           = '"Tháng/ Năm bắt đầu" phải > 0.';
    RS_INVAILD_APPLY_MONTH          = '"Số tháng áp dụng" phải > 0.';
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonvPhuCapDangky.Execute;
begin
    mCanEdit := rCanEdit(r);
    mFORM_CODE := pFORM_CODE;
    Result := ShowModal = mrOK;
    if Result then
    begin
        pTransNo := mTransNo;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.FormCreate(Sender: TObject);
begin
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;

    with HrDataMain do
        OpenDataSets([QrDMBOPHAN, QrDMNV_DANGLAMVIEC, QrDM_HR_PHUCAP, QrV_HR_PHUCAP_LOAI]);

	OpenDataSets([QrSite, QrDep, QrSec, QrEmp, QrList]);


    SetDisplayFormat(TbDummy, ctCurFmt);
    SetShortDateFormat(TbDummy, ShortDateFormat);
    SetDisplayFormat(TbDummy, ['SoThang'], '#,##0;-#,##0;0');


    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetDisplayFormat(QrList, ['SoThang'], '#,##0;-#,##0;0');

    SetCustomGrid(FORM_CODE, GrListData);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;


    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);

    with TbDummy do
    begin
        Open;
    	Append;
    end;

    RgLoai.OnClick(Nil);
    CbPhuCap.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.GrListDataCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    TbDummy.Cancel;

    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

	CloseDataSets([TbDummy, QrEmp, QrList]);

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdImportExecute(Sender: TObject);
var
    ls: TStringList;
    i: Integer;
    s, sName: String;
begin
    ls := TStringList.Create;
    ls.Text := Clipboard.AsText;
    for i := 0 to ls.Count - 1 do
    begin
        s := Trim(ls[i]);
        if (s <> '') then
        begin
            with QrEmp do
            begin
                Filter := 'ManvQL=' + QuotedStr(s);
                sName := FieldByName('Tennv').AsString;
            end;

            AddRow(s, sName);
        end;
    end;
    QrEmp.Filter := '';
    ls.Free;
    Clipboard.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdInsExecute(Sender: TObject);
var
	s1, s2: String;
begin
    case RgLoai.ItemIndex of
    0:
    	begin
            s1 := CbPhongBan.Value;
        	s2 := CbPhongBan.Text;
        end;
    1:
    	begin
            s1 := CbBoPhan.Value;
        	s2 := CbBoPhan.Text;
        end;
    2:
    	begin
            s1 := CbNhanVien.Value;
        	s2 := CbNhanVien.Text;
        end;
    else
    	Exit;
    end;

    AddRow(s1, s2);
	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdDelExecute(Sender: TObject);
begin
	DeleteRow(GrList.Row);
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdDelRecordErrorExecute(Sender: TObject);
begin
     if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdDelRecordSelectedExecute(Sender: TObject);
begin
     QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdLoaiPhuCapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
    HrDataMain.QrDM_HR_PHUCAP.Requery;
    HrDataMain.QrV_HR_PHUCAP_LOAI.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

procedure TFrmHoSonvPhuCapDangky.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_DANGKY_PHUCAP_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                mTransNo := 0;
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                mTransNo := 0;
                CmdRefresh.Execute;
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdContinueExecute(Sender: TObject);
var
    pLst: String;
    i, thangBatDau, namBatDau: Integer;
begin
    with TbDummy do
    begin
        if BlankConfirm(TbDummy, ['MaPhuCap']) then
            Abort;

        thangBatDau := FieldByName('ThangBatDau').AsInteger;
        namBatDau := FieldByName('NamBatDau').AsInteger;

        if (thangBatDau <= 0) or (namBatDau <= 0) then
        begin
            ErrMsg(RS_INVAILD_APPLY_TIME);
            Abort;
        end;

        if FieldByName('Co_GioiHan').AsBoolean then
        begin
            if FieldByName('SoThang').AsInteger <= 0 then
            begin
                ErrMsg(RS_INVAILD_APPLY_MONTH);
                Abort;
            end;
        end else
        begin
            FieldByName('SoThang').Clear;
            FieldByName('ThangKetThuc').Clear;
            FieldByName('NamKetThuc').Clear;
        end;
    end;


    pLst := '';
    with GrList do
    for i := 0 to RowCount - 1 do
    begin
        if Cells[0, i] = '' then
            Break;
        if pLst <> '' then
            pLst := pLst + ',';
        pLst := pLst + '''' + Cells[0, i] + '''';
    end;

    RegisterData(pLst);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyList, bIsError: Boolean;
    s: String;
begin
    with TbDummy do
    begin

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    with QrList do
    begin
    	if not Active then
        	Exit;
        bEmptyList := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdSearch.Enabled := not bEmptyList;
    CmdDelRecordSelected.Enabled := not bEmptyList;
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmptyList);

    CmdContinue.Enabled := (GrList.Cells[0, 0] <> '');
    CmdClear.Enabled := (GrList.Cells[0, 0] <> '');
    CmdDel.Enabled := (GrList.Cells[0, GrList.Row] <> '');
	RgLoai.Enabled := (GrList.Cells[0, 0] = '');

    case RgLoai.ItemIndex of
    0:
       	s := EdMaPhongBan.Text;
    1:
       	s := EdMaBoPhan.Text;
    2:
       	s := EdMaNhanVien.Text;
    end;
    CmdIns.Enabled := s <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.QrListBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CbPhongBanExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;

	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrDep do
        if Locate('MaPhongBan', s, []) then
        begin
            EdMaPhongBan.Text := FieldByName('Ma').AsString;
            s1 := EdMaBoPhan.Text;
            with QrSec do
            if (s1 <> '') then
            begin
                if  not Locate('MaPhongBan;Ma', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaBoPhan.Text := FieldByName('Ma').AsString;
                CbBoPhan.Value := FieldByName('MaBoPhan').AsString;
            end
            else
            begin
                EdMaBoPhan.Text := '';
                CbBoPhan.Text := '';
            end;
        end
        else
        begin
            EdMaPhongBan.Text := '';
            EdMaBoPhan.Text := '';
            CbBoPhan.Text := '';
        end;
    1:		// bo phan
        with QrSec do
        if (s <> '') and (Locate('MaBoPhan', s, [])) then
        begin
            EdMaBoPhan.Text := FieldByName('Ma').AsString;
        end
        else
        begin
            EdMaBoPhan.Text := '';
        end;
    2:		// nhan vien
		EdMaNhanVien.Text := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaPhongBan.Enabled := n <> 2;
    CbPhongBan.Enabled := n <> 2;

    EdMaBoPhan.Enabled := n = 1;
    CbBoPhan.Enabled := n = 1;

    EdMaNhanVien.Enabled :=  n = 2;
    CbNhanVien.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.SmartFocus;
begin
	try
        case RgLoai.ItemIndex of
        0:
            CbPhongBan.SetFocus;
        1:
            CbBoPhan.SetFocus;
        2:
            CbNhanVien.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.TbDummyAfterInsert(DataSet: TDataSet);
begin
    with TbDummy do
    begin
        FieldByName('Co_GioiHan').AsBoolean := False;
    end;
end;

procedure TFrmHoSonvPhuCapDangky.TbDummySoThangChange(Sender: TField);
var
    mDay,mThang, mNam, soThang: Integer;
    nextYear, nextMonth, nextDay: Word;
    mDate, mFromDate, mToDate: TDateTime;
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with TbDummy do
	begin
        mThang := FieldByName('ThangBatDau').AsInteger;
        mNam := FieldByName('NamBatDau').AsInteger;

        if FieldByName('Co_GioiHan').AsBoolean then
        begin
            if not (FieldByName('ThangBatDau').IsNull)
            and not (FieldByName('NamBatDau').IsNull) then
            begin
                soThang := FieldByName('SoThang').AsInteger;

                mDate := IncMonth(EncodeDate(mNam, mThang, 1), Iif(soThang<=0,1,soThang)) - 1;
                DecodeDate(mDate, nextYear, nextMonth, nextDay);
                FieldByName('ThangKetThuc').AsInteger := nextMonth;
                FieldByName('NamKetThuc').AsInteger := nextYear;
            end;
        end
        else
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            FieldByName('SoThang').Clear;
            FieldByName('ThangKetThuc').Clear;
            FieldByName('NamKetThuc').Clear;
            mTrigger := bTrigger;
        end;

        if (Sender.FieldName = 'ThangBatDau') or (Sender.FieldName = 'NamBatDau') then
        begin
            if (mNam <> 0) and (mThang <> 0) then
            begin
                if HrDataMain.GetPeriodDate(mNam, mThang, mFromDate, mToDate) then
                begin
                    FieldByName('NgayBatDau').AsDateTime := mFromDate;
                end;
            end;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.AddRow(p1, p2: String);
var
    i: Integer;
begin
    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := p1;
				Cells[1, i] := p2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = Copy(p1, 1, Length(Cells[0, i])) then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.DeleteRow(pRow: Integer);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, pRow] = '' then
			Exit;

		for i := pRow to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoSonvPhuCapDangky.RegisterData;
var
    s: string;
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    mTransNo := DataMain.GetSeqValue(TABLE_NAME);
	with TbDummy, spIMP_HR_DANGKY_PHUCAP_ImportList do
    begin
        Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := mTransNo;
        Parameters[3].Value := RgLoai.ItemIndex;
        Parameters[4].Value := pLst;
        Parameters[5].Value := FieldByName('MaPhuCap').AsString;
        Parameters[6].Value := FieldByName('SoTien').AsFloat;
        Parameters[7].Value := FieldByName('NgayBatDau').AsDateTime;
        Parameters[8].Value := FieldByName('ThangBatDau').AsInteger;
        Parameters[9].Value := FieldByName('NamBatDau').AsInteger;
        Parameters[10].Value := FieldByName('Co_GioiHan').AsBoolean;
        Parameters[11].Value := FieldByName('SoThang').AsInteger;
        Parameters[12].Value := FieldByName('ThangKetThuc').AsInteger;
        Parameters[13].Value := FieldByName('NamKetThuc').AsInteger;
        Parameters[14].Value := FieldByName('GhiChu').AsString;
        try
            Execute;
        except
        end;

        if Parameters[0].Value <> 0 then
        begin
            mTransNo := 0;
            s := Parameters[15].Value;
            ErrMsg(s);
        end
        else
        begin
            QrListBeforeOpen(QrList);
            CmdRefresh.Execute;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonvPhuCapDangky.DeleteAllRecord;
begin
    with spIMP_HR_DANGKY_PHUCAP_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoSonvPhuCapDangky.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;


end.
