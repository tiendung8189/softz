﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DaoTao;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,Variants,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Messages, Windows,
  frameNavi, isDb, isPanel, wwDialog, Grids, Wwdbgrid, ToolWin,
  Wwdotdot, Wwdbcomb, Buttons, AdvEdit, DBAdvEd, wwcheckbox, frameD2D, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, System.Win.ComObj,
  Winapi.ShellAPI;

type
  TFrmDaoTao = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    PaThongTinKhoaHoc: TisPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdFilterCom: TAction;
    PopDetail: TAdvPopupMenu;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    Xachitit1: TMenuItem;
    Bevel1: TBevel;
    N3: TMenuItem;
    frNavi: TfrNavi;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    TntLabel9: TLabel;
    DBMemo2: TDBMemo;
    PaChitiet: TisPanel;
    CmdSapthutu: TAction;
    CmdThamkhaoGia: TAction;
    SepChecked: TToolButton;
    BtnIn2: TToolButton;
    CmdImportExcel: TAction;
    CmdFromNhap: TAction;
    CmdCheckton: TAction;
    ExportraExceltlidliu2: TMenuItem;
    CmdReRead: TAction;
    QrNXMaKhoaHoc: TWideStringField;
    QrNXTenKhoaHoc: TWideStringField;
    QrNXNgayBatDau: TDateTimeField;
    QrNXNgayKetThuc: TDateTimeField;
    QrNXNgayHetHanPhucVu: TDateTimeField;
    QrNXMaQuocGia: TWideStringField;
    QrNXMaKhoaHoc_Loai: TWideStringField;
    QrNXDiaDiem: TWideStringField;
    QrNXTrungTam: TWideStringField;
    QrNXMucDich: TWideStringField;
    QrNXDonViToChuc: TWideStringField;
    QrNXGiangVien: TWideStringField;
    QrNXSoGioHoc: TFloatField;
    QrNXSoLuong: TFloatField;
    QrNXSoTienNhanVien: TFloatField;
    QrNXSoTienCongTy: TFloatField;
    QrNXSoTien: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    QrCTManv: TWideStringField;
    QrCTCo_ThamGia: TBooleanField;
    QrCTSoHopDong: TWideStringField;
    QrCTSoLuong: TFloatField;
    QrCTSoTienNhanVien: TFloatField;
    QrCTSoTienCongTy: TFloatField;
    QrCTSoTien: TFloatField;
    QrCTNgayCap: TDateTimeField;
    QrCTChungChi: TWideStringField;
    QrCTCo_NopChungChi: TBooleanField;
    QrCTMaPhuCap: TWideStringField;
    QrCTThang: TIntegerField;
    QrCTNam: TIntegerField;
    QrCTCo_ThueTNCN: TBooleanField;
    QrCTGhiChu: TWideStringField;
    frD2D: TfrD2D;
    ItemObsolete: TMenuItem;
    wwDBEdit25: TDBEditEh;
    Label2: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    CbNhomDaoTao: TDbLookupComboboxEh2;
    EdMa: TDBEditEh;
    EdSoQuyetDinh: TDBEditEh;
    DBEditEh1: TDBEditEh;
    CbQuocGia: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    DBEditEh3: TDBEditEh;
    DBEditEh4: TDBEditEh;
    DBNumberEditEh1: TDBNumberEditEh;
    Label4: TLabel;
    CmdNhomDaoTao: TAction;
    CmdQuocGia: TAction;
    QrCTSTT: TIntegerField;
    QrCTLK_Tennv: TWideStringField;
    QrCTLK_TenPhuCap: TWideStringField;
    N2: TMenuItem;
    QrNXLK_TenQuocGia: TWideStringField;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    QrNXLK_KhoaHocTenLoai: TWideStringField;
    QrCTLK_ManvQL: TWideStringField;
    CmdTotal: TAction;
    GrDetail: TwwDBGrid2;
    CbMaPhuCap: TwwDBLookupCombo;
    CbPhuCap: TwwDBLookupCombo;
    CbManvQL: TwwDBLookupCombo;
    CbTennv: TwwDBLookupCombo;
    vlTotal1: TisTotal;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    QrNXIdx: TAutoIncField;
    QrCTIdx: TAutoIncField;
    QrCTIdx_DaoTao: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrNXHINHTHUC_GIAValidate(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure CmdNhomDaoTaoExecute(Sender: TObject);
    procedure CmdQuocGiaExecute(Sender: TObject);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure QrCTSoTienNhanVienChange(Sender: TField);
    procedure QrCTManvValidate(Sender: TField);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTSoTienChange(Sender: TField);
    procedure QrCTLK_ManvQLChange(Sender: TField);
    procedure QrCTManvChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);

  private
	mCanEdit, b1Ncc, mLuugianhap, bDuplicate,
        bClSotien, bClThue, bCk, bCkHD, mObsolete: Boolean;
	mLydo, mMakho, mPrefix, mHTGia: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

	    (*
    	** Functions
	    *)
    procedure escapeKey(pSleepTime: Variant);
  public
	procedure Execute(r: WORD);
  end;

var
  FrmDaoTao: TFrmDaoTao;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    isLib, isFile, MasterData, OfficeData, HrData, DmHotro_HR, HrExCommon,
    Dmdl, exThread;

{$R *.DFM}

const
    TABLE_NAME = 'HR_DAOTAO';
	FORM_CODE = 'HR_DAOTAO';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.Execute;
begin
    // Audit setting
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.FormCreate(Sender: TObject);
begin
    frNavi.DataSet := QrNX;
  	mTrigger := False;
    mObsolete := False;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);
	// Open database

    with DataMain do
    begin
		OpenDataSets([QrQuocgia]);
    end;


    with HrDataMain do
    begin
		OpenDataSets([QrV_HR_DAOTAO_NHOM,  QrDMNV, QrDMNV_DANGLAMVIEC, QrDM_HR_PHUCAP]);
    end;



    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
        SetShortDateFormat(QrNX, ShortDateFormat);
        SetDisplayFormat(QrNX, ['SoGioHoc'], sysFloatFmtTwo);
    end;


    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
        SetShortDateFormat(QrCT, ShortDateFormat);
    end;


    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);


    with QrNX.SQL do
    begin
       Add(' and ([NgayBatDau] between :Tungay1 and :Denngay1' +
       ' or [NgayKetThuc] between :Tungay2 and :Denngay2' +
       ' or :Tungay3 between [NgayBatDau] and [NgayKetThuc]' +
       ' or :Denngay3 between [NgayBatDau] and [NgayKetThuc])');
        fSQL := Text;
    end;

    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);

    frD2D.CbOrg.Clear;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrNX, QrCT]);
    finally
    end;
    Action := caFree;
end;

	(*
	** Actions
	*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

    with HrDataMain do
    begin
		QrV_HR_DAOTAO_NHOM.Requery;
        QrV_QUOCTICH.Requery;
    end;
            
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdRefreshExecute(Sender: TObject);
var
	s, sAccess : String;
begin
     if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fType := frD2D.CbOrg.SelectedNode.Level;
	end;

    if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

		Screen.Cursor := crSQLWait;

        Wait(DATAREADING);
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            sAccess := '';
            if sysIsDataAccess then
                sAccess := Format(' and b.Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]);


//             Detail Filter
            if fStr <> '' then
	           	case fType of
                0:
                    SQL.Add('and Idx in (select a.Idx_DaoTao from HR_DAOTAO_CHITIET a, HR_DM_NHANVIEN b where a.Idx_DaoTao = HR_DAOTAO.Idx and a.Manv = b.Manv and b.[MaChiNhanh] in (' + fStr + ') ' + sAccess+ ' )' );
                1:
                    SQL.Add('and Idx in (select a.Idx_DaoTao from HR_DAOTAO_CHITIET a, HR_DM_NHANVIEN b where a.Idx_DaoTao = HR_DAOTAO.Idx and a.Manv = b.Manv and b.[MaPhongBan] in (' + fStr + ') ' + sAccess+ ' )');
                2:
                    SQL.Add('and Idx in (select a.Idx_DaoTao from HR_DAOTAO_CHITIET a, HR_DM_NHANVIEN b where a.Idx_DaoTao = HR_DAOTAO.Idx and a.Manv = b.Manv and b.[MaBoPhan] in (' + fStr + ') ' + sAccess+ ' ) ');

				end;

			SQL.Add(' order by NgayBatDau desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
            Parameters[2].Value := fTungay;
            Parameters[3].Value := fDenngay;
            Parameters[4].Value := fTungay;
            Parameters[5].Value := fDenngay;

            Open;
        end;

        if s = '' then
            s := 'NgayBatDau';

        SortDataSet(QrNX, s);
		ClearWait;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdNewExecute(Sender: TObject);
begin
    QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdNhomDaoTaoExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r, 'HR_DAOTAO_NHOM');
    HrDataMain.QrV_HR_DAOTAO_NHOM.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdDelExecute(Sender: TObject);
begin
	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdSwitchExecute(Sender: TObject);
begin
    EdMa.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdTotalExecute(Sender: TObject);
begin
    vlTotal1.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsNX)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
    else
    end;
end;



(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, QrNX.FieldByName('Idx').AsInteger]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdQuocGiaExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r, True);

    DataMain.QrQuocgia.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	// Master
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdPrint.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
	CmdRefresh.Enabled := bBrowse;
    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;


    CmdFilterCom.Checked := fStr <> '';

	// Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;

    CmdEmptyDetail.Enabled := not bEmptyCT;
end;

    (*
    **  Master DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
    mTrigger := False;
end;

resourcestring
    RS_INVALID_DATE = 'Ngày bắt đầu không hợp lệ.';
procedure TFrmDaoTao.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['MaKhoaHoc', 'TenKhoaHoc', 'NgayBatDau', 'NgayKetThuc', 'MaKhoaHoc_Loai']) then
        	Abort;

        if ((FieldByName('NgayBatDau').AsFloat > 10) and (FieldByName('NgayKetThuc').AsFloat > 10) and
             (FieldByName('NgayKetThuc').AsDateTime < FieldByName('NgayBatDau').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_DATE);
            try
                CbNGAY.SelectAll;
                CbNGAY.SetFocus;
            except
            end;
            Abort;
        end;
    end;
    CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrNXCalcFields(DataSet: TDataSet);
begin
//	{$I XCalc}
//    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrNXHINHTHUC_GIAValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if QrCT.IsEmpty then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDaoTao.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('Idx').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTBeforePost(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['Manv']) then
    		Abort;

        if State in [dsInsert] then
        begin
            FieldByName('Idx_DaoTao').Value := QrNX.FieldByName('Idx').Value;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTLK_ManvQLChange(Sender: TField);
var
    s: String;
begin
    exDotManv(HrDataMain.QrDMNV_DANGLAMVIEC, s);
    if s <> '' then
    begin
        QrCT.FieldByName('Manv').AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
   		Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTAfterCancel(DataSet: TDataSet);
begin
    vlTotal1.Reset;
end;

procedure TFrmDaoTao.QrCTAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

procedure TFrmDaoTao.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTAfterInsert(DataSet: TDataSet);
begin
    with QrCT do
    begin
        FieldByName('Co_ThamGia').AsBoolean := False;
        FieldByName('Co_NopChungChi').AsBoolean := False;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
        FieldByName('SoLuong').AsFloat := 1;
    end;
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTManvChange(Sender: TField);
begin
    GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTManvValidate(Sender: TField);
var
	s: String;
    bm: TBytes;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
	if s = '' then
    	Exit;

//    with QrCT do
//    begin
//        begin
//            try
//                GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
//                Abort;
//            finally
//                Cancel;
//                GotoBookmark(bm);
//                Edit;
//                mgMyThread := TExThread.Create(escapeKey, 50);
//            end;
//        end;
//    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTSoTienChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.QrCTSoTienNhanVienChange(Sender: TField);
begin
    with QrCT do
    begin
         FieldByName('SoTien').AsFloat := FieldByName('SoTienNhanVien').AsFloat + FieldByName('SoTienCongTy').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

procedure TFrmDaoTao.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SoTien').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SoTien').AsFloat);
        ColumnByName('SoTienNhanVien').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SoTienNhanVien').AsFloat);
        ColumnByName('SoTienCongty').FooterValue :=
        	FormatFloat(sysCurFmt, FieldByName('SoTienCongty').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CbMAKHOBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;
	    try
    	    EdMa.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDaoTao.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;


end.
