object FrmBosungLuong: TFrmBosungLuong
  Left = 291
  Top = 172
  BorderStyle = bsDialog
  Caption = 'C'#7853'p Nh'#7853't B'#7893' Sung'
  ClientHeight = 273
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = TntFormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 8
    Top = 32
    Width = 399
    Height = 232
    DisableThemes = False
    CaptionColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Items = <
      item
        DataField = 'Daily Allowance'
        Caption = 'Ph'#7909' c'#7845'p ng'#224'y'
        WordWrap = False
      end
      item
        DataField = 'Monthly Allowance'
        Caption = 'Ph'#7909' c'#7845'p th'#225'ng'
        WordWrap = False
      end
      item
        DataField = 'Allowance2'
        Caption = 'Ch'#234'nh l'#7879'ch t'#7927' gi'#225
        WordWrap = False
      end
      item
        DataField = 'Allowance3'
        Caption = 'Thu nh'#7853'p kh'#225'c ch'#7883'u thu'#7871
        WordWrap = False
      end
      item
        DataField = 'Allowance4'
        Caption = 'Thu nh'#7853'p kh'#225'c kh'#244'ng thu'#7871
        WordWrap = False
      end
      item
        DataField = 'Addition'
        Caption = #272'i'#7873'u ch'#7881'nh'
        WordWrap = False
      end
      item
        DataField = 'Comment'
        Caption = 'Ghi ch'#250
        WordWrap = False
      end
      item
        DataField = 'Advanced'
        Caption = 'T'#7841'm '#7913'ng'
        WordWrap = False
      end
      item
        DataField = 'Advanced Percent'
        Caption = 'Advanced Percent'
        WordWrap = False
      end
      item
        DataField = 'Allowance'
        Caption = 'Ph'#7909' c'#7845'p'
        WordWrap = False
      end
      item
        DataField = 'Allowance1'
        Caption = 'Ph'#7909' c'#7845'p hi'#7879'n di'#7879'n'
        WordWrap = False
      end
      item
        DataField = 'Annual Leaves'
        Caption = 'Annual Leaves'
        WordWrap = False
      end
      item
        DataField = 'Attendance Bonus'
        Caption = 'Attendance Bonus'
        WordWrap = False
      end
      item
        DataField = 'Base Salary'
        Caption = 'L'#432#417'ng th'#225'ng'
        WordWrap = False
      end>
    CaptionWidth = 197
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
  end
  object DBNavigator1: TRzDBNavigator
    Left = 142
    Top = 5
    Width = 264
    Height = 25
    Cursor = 1
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbPost, nbCancel]
    ImageIndexes.Post = 1
    ImageIndexes.Cancel = 2
    Images = DataMain.ImageNavi
    BorderOuter = fsNone
    TabOrder = 1
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 4
    Top = 4
    object CmdNext: TAction
      Caption = 'CmdNext'
      ShortCut = 34
      OnExecute = CmdNextExecute
    end
    object CmdPrior: TAction
      Caption = 'CmdPrior'
      ShortCut = 33
      OnExecute = CmdPriorExecute
    end
  end
end
