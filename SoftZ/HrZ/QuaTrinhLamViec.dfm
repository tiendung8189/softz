object FrmQuaTrinhLamViec: TFrmQuaTrinhLamViec
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Qu'#225' Tr'#236'nh L'#224'm Vi'#7879'c'
  ClientHeight = 675
  ClientWidth = 1244
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1244
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton14: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 654
    Width = 1244
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 125
    Width = 694
    Height = 529
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'Manv'#9'12'#9'M'#227#9'F'
      'LK_Tennv'#9'25'#9'H'#7885' t'#234'n'#9'F'
      'NgayHieuLuc'#9'12'#9'Hi'#7879'u l'#7921'c'#9'F'#9'Ng'#224'y'
      'GhiChu'#9'25'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    OnEnter = CmdRefreshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object RzSizePanel1: TRzSizePanel
    Left = 694
    Top = 125
    Width = 550
    Height = 529
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotSpotVisible = True
    ParentColor = True
    ParentFont = False
    ParentShowHint = False
    RealTimeDrag = True
    ShowHint = True
    SizeBarWidth = 7
    TabOrder = 3
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PgMain: TPageControl
      Left = 8
      Top = 0
      Width = 542
      Height = 529
      Cursor = 1
      ActivePage = TabSheet1
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      HotTrack = True
      Images = DataMain.ImageSmall
      ParentFont = False
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'T'#7893' ch'#7913'c'
        ImageIndex = 58
        object ScrollToChuc: TScrollBox
          Left = 0
          Top = 0
          Width = 534
          Height = 500
          Align = alClient
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
          object PaChinhQuyen: TisPanel
            Left = 0
            Top = 547
            Width = 513
            Height = 174
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 2
            HeaderCaption = ' .: Th'#244'ng tin ch'#237'nh quy'#7873'n'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 127
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              513
              174)
            object Label78: TLabel
              Left = 37
              Top = 97
              Width = 100
              Height = 16
              Alignment = taRightJustify
              Caption = 'B'#7893' nhi'#7879'm t'#7915' ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label80: TLabel
              Left = 41
              Top = 145
              Width = 96
              Height = 16
              Alignment = taRightJustify
              Caption = 'Bi'#7879't ph'#225'i t'#7915' ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label74: TLabel
              Left = 317
              Top = 49
              Width = 80
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Ng'#224'y bi'#234'n ch'#7871
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label1: TLabel
              Left = 343
              Top = 96
              Width = 54
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = #272#7871'n ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label2: TLabel
              Left = 343
              Top = 145
              Width = 54
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = #272#7871'n ng'#224'y'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object DpBoNhiemTuNgay: TwwDBDateTimePicker
              Left = 143
              Top = 95
              Width = 101
              Height = 22
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BoNhiem_NgayBatDau'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 6
            end
            object DpBietPhaiTuNgay: TwwDBDateTimePicker
              Left = 143
              Top = 143
              Width = 101
              Height = 22
              BiDiMode = bdLeftToRight
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BietPhai_NgayBatDau'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ShowButton = True
              TabOrder = 10
            end
            object wwDBEdit24: TDBEditEh
              Left = 143
              Top = 23
              Width = 361
              Height = 22
              Anchors = [akLeft, akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              ControlLabel.Width = 118
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c v'#7909' ch'#237'nh quy'#7873'n'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'ChucVuChinhQuyen'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 0
              Visible = True
            end
            object CbBietPhai_MaChucDanh: TDbLookupComboboxEh2
              Left = 143
              Top = 71
              Width = 281
              Height = 22
              ControlLabel.Width = 114
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c danh bi'#7879't ph'#225'i'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'BietPhai_MaChucDanh'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  FieldName = 'TenChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaChucDanh'
              ListField = 'TenChucDanh'
              ListSource = HrDataMain.DsDM_CHUCDANH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 4
              Visible = True
            end
            object CbBoNhiem_MaChucDanh: TDbLookupComboboxEh2
              Left = 143
              Top = 119
              Width = 281
              Height = 22
              ControlLabel.Width = 118
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Ch'#7913'c danh b'#7893' nhi'#7879'm'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              AlwaysShowBorder = True
              AutoSize = False
              BevelKind = bkFlat
              Ctl3D = False
              ParentCtl3D = False
              BorderStyle = bsNone
              Anchors = [akLeft, akTop, akRight]
              DynProps = <>
              DataField = 'BoNhiem_MaChucDanh'
              DataSource = DsMaster
              DropDownBox.Columns = <
                item
                  EndEllipsis = True
                  FieldName = 'TenChucDanh'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  SpecCell.Font.Charset = DEFAULT_CHARSET
                  SpecCell.Font.Color = clWindowText
                  SpecCell.Font.Height = -12
                  SpecCell.Font.Name = 'Tahoma'
                  SpecCell.Font.Style = []
                  Title.Alignment = taCenter
                  Title.Caption = 'T'#234'n'
                  Width = 200
                end>
              DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
              DropDownBox.ListSourceAutoFilter = True
              DropDownBox.ListSourceAutoFilterType = lsftContainsEh
              DropDownBox.ListSourceAutoFilterAllColumns = True
              DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
              DropDownBox.AutoDrop = True
              DropDownBox.Rows = 15
              DropDownBox.Sizable = True
              DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
              DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
              DropDownBox.SpecRow.Font.Color = clWindowText
              DropDownBox.SpecRow.Font.Height = -12
              DropDownBox.SpecRow.Font.Name = 'Tahoma'
              DropDownBox.SpecRow.Font.Style = []
              EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
              EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
              EmptyDataInfo.Font.Color = clSilver
              EmptyDataInfo.Font.Height = -13
              EmptyDataInfo.Font.Name = 'Tahoma'
              EmptyDataInfo.Font.Style = [fsItalic]
              EmptyDataInfo.ParentFont = False
              EmptyDataInfo.Alignment = taLeftJustify
              EditButton.DefaultAction = True
              EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
              EditButton.Style = ebsAltDropDownEh
              EditButton.Width = 20
              EditButton.DrawBackTime = edbtWhenHotEh
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              KeyField = 'MaChucDanh'
              ListField = 'TenChucDanh'
              ListSource = HrDataMain.DsDM_CHUCDANH
              ParentFont = False
              ShowHint = True
              Style = csDropDownEh
              TabOrder = 8
              Visible = True
            end
            object DBNumberEditEh7: TDBNumberEditEh
              Left = 143
              Top = 47
              Width = 45
              Height = 22
              ControlLabel.Width = 89
              ControlLabel.Height = 16
              ControlLabel.Caption = 'Quy ho'#7841'ch n'#259'm'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'NamQuyHoach'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
            object DBNumberEditEh8: TDBNumberEditEh
              Left = 270
              Top = 47
              Width = 45
              Height = 22
              ControlLabel.Width = 74
              ControlLabel.Height = 16
              ControlLabel.Caption = 'B'#7893' sung n'#259'm'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              BevelKind = bkFlat
              BorderStyle = bsNone
              Ctl3D = False
              DataField = 'NamQuyHoachBoSung'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ShowHint = True
              TabOrder = 2
              Visible = True
            end
            object wwDBDateTimePicker2: TwwDBDateTimePicker
              Left = 403
              Top = 47
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'NgayBienChe'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 3
            end
            object DBEditEh11: TDBEditEh
              Left = 427
              Top = 71
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'BoNhiem_MaChucDanh'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 5
              Visible = True
            end
            object DBEditEh12: TDBEditEh
              Left = 427
              Top = 119
              Width = 77
              Height = 22
              TabStop = False
              Anchors = [akTop, akRight]
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'BietPhai_MaChucDanh'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 9
              Visible = True
            end
            object wwDBDateTimePicker1: TwwDBDateTimePicker
              Left = 403
              Top = 95
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BoNhiem_NgayKetThuc'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ShowButton = True
              TabOrder = 7
            end
            object wwDBDateTimePicker5: TwwDBDateTimePicker
              Left = 403
              Top = 143
              Width = 101
              Height = 22
              Anchors = [akTop, akRight]
              BiDiMode = bdLeftToRight
              BorderStyle = bsNone
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              DataField = 'BietPhai_NgayKetThuc'
              DataSource = DsMaster
              Epoch = 1950
              ButtonEffects.Transparent = True
              ButtonEffects.Flat = True
              Frame.Enabled = True
              Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentBiDiMode = False
              ParentFont = False
              ShowButton = True
              TabOrder = 11
            end
          end
          object PaDinhKem: TisPanel
            Left = 0
            Top = 721
            Width = 513
            Height = 51
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 3
            HeaderCaption = ' .: File '#273#237'nh k'#232'm'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            DesignSize = (
              513
              51)
            object EdFileAttach: TDBEditEh
              Tag = 1
              Left = 143
              Top = 23
              Width = 361
              Height = 22
              TabStop = False
              Anchors = [akLeft, akTop, akRight]
              AutoSize = False
              BevelKind = bkFlat
              BorderStyle = bsNone
              Color = clBtnFace
              ControlLabel.Width = 76
              ControlLabel.Height = 16
              ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
              ControlLabel.Font.Charset = DEFAULT_CHARSET
              ControlLabel.Font.Color = clWindowText
              ControlLabel.Font.Height = -13
              ControlLabel.Font.Name = 'Tahoma'
              ControlLabel.Font.Style = []
              ControlLabel.ParentFont = False
              ControlLabel.Visible = True
              ControlLabelLocation.Spacing = 5
              ControlLabelLocation.Position = lpLeftCenterEh
              Ctl3D = False
              DataField = 'Calc_FileName'
              DataSource = DsMaster
              DynProps = <>
              EditButtons = <
                item
                  Action = CmdFilePlus
                  Images.NormalImages = DataMain.ImageEditButton
                  Images.HotImages = DataMain.ImageEditButton
                  Images.PressedImages = DataMain.ImageEditButton
                  Images.DisabledImages = DataMain.ImageEditButton
                  Style = ebsGlyphEh
                  Width = 30
                  DrawBackTime = edbtWhenHotEh
                end
                item
                  Action = CmdFileMinus
                  Images.NormalImages = DataMain.ImageEditButton
                  Images.HotImages = DataMain.ImageEditButton
                  Images.PressedImages = DataMain.ImageEditButton
                  Images.DisabledImages = DataMain.ImageEditButton
                  Images.NormalIndex = 1
                  Images.HotIndex = 1
                  Images.PressedIndex = 1
                  Images.DisabledIndex = 1
                  Style = ebsGlyphEh
                  Width = 30
                  DrawBackTime = edbtWhenHotEh
                end
                item
                  Action = CmdFileView
                  Images.NormalImages = DataMain.ImageEditButton
                  Images.HotImages = DataMain.ImageEditButton
                  Images.PressedImages = DataMain.ImageEditButton
                  Images.DisabledImages = DataMain.ImageEditButton
                  Images.NormalIndex = 2
                  Images.HotIndex = 2
                  Images.PressedIndex = 2
                  Images.DisabledIndex = 2
                  Style = ebsGlyphEh
                  Width = 30
                  DrawBackTime = edbtWhenHotEh
                end>
              Font.Charset = ANSI_CHARSET
              Font.Color = 8404992
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
              Visible = True
            end
          end
          object PaGhiChu: TisPanel
            Left = 0
            Top = 772
            Width = 513
            Height = 100
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            ParentBackground = False
            TabOrder = 4
            HeaderCaption = ' .: Ghi ch'#250
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object EdGHICHU: TDBMemo
              Left = 0
              Top = 16
              Width = 513
              Height = 84
              Align = alClient
              BorderStyle = bsNone
              DataField = 'GhiChu'
              DataSource = DsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
          end
          object PaLyDo: TisPanel
            Left = 0
            Top = 0
            Width = 513
            Height = 209
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 0
            HeaderCaption = ' :: Qu'#225' tr'#236'nh l'#224'm vi'#7879'c (QTLV)'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 0
            ShowButton = False
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object Panel1: TPanel
              Left = 0
              Top = 48
              Width = 513
              Height = 95
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 2
              DesignSize = (
                513
                95)
              object Label108: TLabel
                Left = 353
                Top = 26
                Width = 44
                Height = 16
                Alignment = taRightJustify
                Anchors = [akTop, akRight]
                Caption = 'Ng'#224'y k'#253
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label6: TLabel
                Left = 60
                Top = 26
                Width = 77
                Height = 16
                Alignment = taRightJustify
                Caption = 'Ng'#224'y hi'#7879'u l'#7921'c'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object CbDate: TwwDBDateTimePicker
                Left = 403
                Top = 24
                Width = 101
                Height = 22
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayKy'
                DataSource = DsMaster
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ShowButton = True
                TabOrder = 3
              end
              object CbQTLV: TDbLookupComboboxEh2
                Left = 143
                Top = 0
                Width = 281
                Height = 22
                ControlLabel.Width = 97
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Lo'#7841'i QTLV/ L'#253' do'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaQTLV'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenQTLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaQTLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = DsV_HR_QUATRINH_LAMVIEC
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaQTLV'
                ListField = 'TenQTLV'
                ListSource = HrDataMain.DsV_HR_QUATRINH_LAMVIEC
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
                OnDropDown = CbQTLVDropDown
              end
              object CdNgayHieuLuc: TwwDBDateTimePicker
                Left = 143
                Top = 24
                Width = 165
                Height = 22
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                DataField = 'NgayHieuLuc'
                DataSource = DsMaster
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                ShowButton = True
                TabOrder = 2
              end
              object EdNguoiKy_Manv: TDBEditEh
                Left = 143
                Top = 72
                Width = 361
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                AutoSelect = False
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 49
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ng'#432#7901'i k'#253
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'NguoiKy'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                EmptyDataInfo.Color = clInfoBk
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 5
                Visible = True
              end
              object EdSoQuyetDinh: TDBEditEh
                Left = 143
                Top = 48
                Width = 361
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 78
                ControlLabel.Height = 16
                ControlLabel.Caption = 'S'#7889' quy'#7871't '#273#7883'nh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'SoQuyetDinh'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 4
                Visible = True
              end
              object wwDBEdit23: TDBEditEh
                Left = 427
                Top = 0
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaQTLV'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdQuaTrinhLamViec
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
            end
            object GroupBox1: TGroupBox
              Left = 0
              Top = 143
              Width = 513
              Height = 66
              Align = alClient
              Caption = '  Th'#244'ng tin h'#7907'p '#273#7891'ng  '
              Ctl3D = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              DesignSize = (
                513
                66)
              object CbHopDong: TDbLookupComboboxEh2
                Left = 143
                Top = 13
                Width = 361
                Height = 22
                ControlLabel.Width = 54
                ControlLabel.Height = 16
                ControlLabel.Caption = 'H'#7907'p '#273#7891'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'Idx_HopDong'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'SoHopDong'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'H'#7907'p '#273#7891'ng/PLH'#272'|S'#7889
                    Width = 110
                  end
                  item
                    FieldName = 'TenHopDong'
                    Title.Alignment = taCenter
                    Title.Caption = 'H'#7907'p '#273#7891'ng/PLH'#272'|Lo'#7841'i'
                    Width = 210
                  end
                  item
                    FieldName = 'NgayHieuLuc'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'Ng'#224'y hi'#7879'u l'#7921'c'
                  end>
                DropDownBox.ListSource = DsHopDong
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.UseMultiTitle = True
                DropDownBox.Align = daRight
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.ShowTitles = True
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                DropDownBox.Width = 450
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <
                  item
                    Action = CmdHopDong
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Idx'
                ListField = 'TenHopDong'
                ListSource = DsHopDong
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
                OnDropDown = CbHopDongDropDown
              end
              object CbToDate: TwwDBDateTimePicker
                Left = 403
                Top = 37
                Width = 101
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BorderStyle = bsNone
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                Color = clBtnFace
                DataField = 'LK_NgayHieuLuc'
                DataSource = DsMaster
                Epoch = 1950
                ButtonEffects.Transparent = True
                ButtonEffects.Flat = True
                Frame.Enabled = True
                Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                ShowButton = True
                TabOrder = 1
              end
              object DBEditEh5: TDBEditEh
                Left = 143
                Top = 37
                Width = 257
                Height = 22
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabel.Width = 72
                ControlLabel.Height = 16
                ControlLabel.Caption = 'S'#7889' h'#7907'p '#273#7891'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_SoHopDong'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
                Visible = True
              end
            end
            object PaTop: TPanel
              Left = 0
              Top = 16
              Width = 513
              Height = 8
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
            end
            object PaNhanVien: TPanel
              Left = 0
              Top = 24
              Width = 513
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              DesignSize = (
                513
                24)
              object CbNhanVien: TDbLookupComboboxEh2
                Left = 143
                Top = 0
                Width = 281
                Height = 22
                ControlLabel.Width = 56
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'Manv'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'Tennv'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 130
                  end
                  item
                    FieldName = 'ManvQL'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = DsEmp
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Manv'
                ListField = 'Tennv'
                ListSource = HrDataMain.DsDMNV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object EdManvQL: TDBEditEh
                Left = 427
                Top = 0
                Width = 77
                Height = 22
                TabStop = False
                Alignment = taLeftJustify
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'ManvQL'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdCapManvQL
                    DefaultAction = False
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
            end
          end
          object PaToChuc: TisPanel
            Left = 0
            Top = 209
            Width = 513
            Height = 338
            Align = alTop
            BevelOuter = bvNone
            Color = 16119285
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 1
            HeaderCaption = ' .: Th'#244'ng tin t'#7893' ch'#7913'c v'#224' vi'#7879'c l'#224'm'
            HeaderColor = clHighlight
            ImageSet = 4
            RealHeight = 197
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWhite
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object gbPhepNam: TGroupBox
              Left = 0
              Top = 272
              Width = 513
              Height = 66
              Align = alClient
              Caption = '  Ph'#233'p n'#259'm   '
              Ctl3D = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              DesignSize = (
                513
                66)
              object DBNumberEditEh1: TDBNumberEditEh
                Left = 143
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 52
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Lu'#7853't '#273#7883'nh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_LuatDinh'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 0
                Visible = True
              end
              object DBNumberEditEh2: TDBNumberEditEh
                Left = 250
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 62
                ControlLabel.Height = 16
                ControlLabel.Caption = 'C'#7897'ng th'#234'm'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_ChucDanh'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object DBNumberEditEh3: TDBNumberEditEh
                Left = 349
                Top = 13
                Width = 30
                Height = 22
                ControlLabel.Width = 61
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Th'#226'm ni'#234'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'PhepNam_ThamNien'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 2
                Visible = True
              end
              object DBNumberEditEh4: TDBNumberEditEh
                Left = 474
                Top = 13
                Width = 30
                Height = 22
                TabStop = False
                ControlLabel.Width = 86
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ph'#233'p n'#259'm t'#7893'ng'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                Ctl3D = False
                DataField = 'CALC_TongPhepNam'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = ANSI_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object rDBCheckBox1: TrDBCheckBox
                Left = 224
                Top = 39
                Width = 280
                Height = 22
                Anchors = [akTop, akRight]
                Caption = ' '#193'p d'#7909'ng lu'#7853't 1 ng'#224'y ph'#233'p / 1 th'#225'ng n'#259'm '#273#7847'u '
                DataField = 'Co_PhepNamMotThang'
                DataSource = DsMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                ValueChecked = 'True'
                ValueUnchecked = 'False'
                ShowFieldCaption = False
                UpdateAfterClick = True
              end
            end
            object gbViecLam: TGroupBox
              Left = 0
              Top = 204
              Width = 513
              Height = 68
              Align = alTop
              Caption = '  Vi'#7879'c l'#224'm  '
              Ctl3D = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              DesignSize = (
                513
                68)
              object CmNoiLV: TDbLookupComboboxEh2
                Left = 143
                Top = 14
                Width = 281
                Height = 22
                ControlLabel.Width = 69
                ControlLabel.Height = 16
                ControlLabel.Caption = 'N'#417'i l'#224'm vi'#7879'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaNoiLV'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenNoiLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaNoiLV'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_NOILV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaNoiLV'
                ListField = 'TenNoiLV'
                ListSource = HrDataMain.DsDM_NOILV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object DBEditEh2: TDBEditEh
                Left = 427
                Top = 14
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaNoiLV'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdNoiLamViec
                    DefaultAction = True
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object CbNhomLV: TDbLookupComboboxEh2
                Left = 143
                Top = 38
                Width = 281
                Height = 22
                ControlLabel.Width = 84
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Nh'#243'm l'#224'm vi'#7879'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaNhomLV'
                DataSource = DsMaster
                DropDownBox.ListSource = HrDataMain.DsDM_NHOMLV
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Flat = True
                KeyField = 'MaNhomLV'
                ListField = 'TenNhomLV'
                ListSource = HrDataMain.DsDM_NHOMLV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
              end
              object DBEditEh3: TDBEditEh
                Left = 427
                Top = 38
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaNhomLV'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 804000
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
            end
            object gbToChuc: TGroupBox
              Left = 0
              Top = 16
              Width = 513
              Height = 188
              Align = alTop
              Caption = '  T'#7893' ch'#7913'c  '
              Ctl3D = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              DesignSize = (
                513
                188)
              object wwDBEdit25: TDBEditEh
                Left = 143
                Top = 86
                Width = 361
                Height = 22
                Anchors = [akLeft, akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                ControlLabel.Width = 89
                ControlLabel.Height = 16
                ControlLabel.Caption = 'C'#244'ng vi'#7879'c ch'#237'nh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'CongViecChinh'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 6
                Visible = True
              end
              object DBEditEh1: TDBEditEh
                Left = 427
                Top = 62
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'MaChucDanh'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdChucDanh
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 5
                Visible = True
              end
              object CbChucDanh: TDbLookupComboboxEh2
                Left = 143
                Top = 62
                Width = 281
                Height = 22
                ControlLabel.Width = 61
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ch'#7913'c danh'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaChucDanh'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenChucDanh'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 200
                  end
                  item
                    FieldName = 'MaChucDanh'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = HrDataMain.DsDM_CHUCDANH
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaChucDanh'
                ListField = 'TenChucDanh'
                ListSource = HrDataMain.DsDM_CHUCDANH
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 4
                Visible = True
              end
              object DBEditEh4: TDBEditEh
                Left = 427
                Top = 14
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_MaPhongBan'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdPhongBan
                    DefaultAction = True
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
                Visible = True
              end
              object CbPhongBan: TDbLookupComboboxEh2
                Left = 143
                Top = 14
                Width = 281
                Height = 22
                ControlLabel.Width = 60
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ph'#242'ng ban'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaPhongBan'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenPhongBan'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 195
                  end
                  item
                    FieldName = 'MaPhongBan'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 45
                  end>
                DropDownBox.ListSource = DsDep
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clInfoBk
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaPhongBan'
                ListField = 'TenPhongBan'
                ListSource = DsDep
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = True
              end
              object CbBoPhan: TDbLookupComboboxEh2
                Left = 143
                Top = 38
                Width = 281
                Height = 22
                ControlLabel.Width = 46
                ControlLabel.Height = 16
                ControlLabel.Caption = 'B'#7897' ph'#7853'n'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'MaBoPhan'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenBoPhan'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 177
                  end
                  item
                    FieldName = 'Ma'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 63
                  end>
                DropDownBox.ListSource = DsSec
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'MaBoPhan'
                ListField = 'TenBoPhan'
                ListSource = HrDataMain.DsDMBOPHAN
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 2
                Visible = True
                OnDropDown = CbBoPhanDropDown
              end
              object EdMaBoPhan_BanDau: TDBEditEh
                Left = 427
                Top = 38
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_MaBoPhan_BanDau'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    Action = CmdPhongBan
                    DefaultAction = True
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                Visible = True
              end
              object EdXacNhan_ManvQL: TDBEditEh
                Left = 427
                Top = 110
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_XacNhan_ManvQL'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 8
                Visible = True
              end
              object CbXacNhan_Manv: TDbLookupComboboxEh2
                Left = 143
                Top = 110
                Width = 281
                Height = 22
                ControlLabel.Width = 78
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ng'#432#7901'i qu'#7843'n l'#253
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'XacNhan_Manv'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'Tennv'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 130
                  end
                  item
                    FieldName = 'ManvQL'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = DsEmp
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clWindow
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Manv'
                ListField = 'Tennv'
                ListSource = HrDataMain.DsDMNV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 7
                Visible = True
              end
              object EdXetDuyet_ManvQL: TDBEditEh
                Left = 427
                Top = 134
                Width = 77
                Height = 22
                TabStop = False
                Anchors = [akTop, akRight]
                BevelKind = bkFlat
                BorderStyle = bsNone
                Color = clBtnFace
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Ctl3D = False
                DataField = 'LK_XetDuyet_ManvQL'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ParentCtl3D = False
                ParentFont = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 10
                Visible = True
              end
              object CbXetDuyet_Manv: TDbLookupComboboxEh2
                Left = 143
                Top = 134
                Width = 281
                Height = 22
                ControlLabel.Width = 127
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Ng'#432#7901'i x'#233't duy'#7879't (BOD)'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                Anchors = [akLeft, akTop, akRight]
                DynProps = <>
                DataField = 'XetDuyet_Manv'
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'Tennv'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 130
                  end
                  item
                    FieldName = 'ManvQL'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSource = DsEmp
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Color = clWindow
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                KeyField = 'Manv'
                ListField = 'Tennv'
                ListSource = HrDataMain.DsDMNV
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 9
                Visible = True
              end
              object EdThongTinKhac: TDBMemoEh
                Left = 143
                Top = 158
                Width = 361
                Height = 22
                ControlLabel.Width = 130
                ControlLabel.Height = 16
                ControlLabel.Caption = 'Th'#244'ng tin t'#7893' ch'#7913'c kh'#225'c'
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                Anchors = [akLeft, akTop, akRight]
                AutoSize = False
                BevelKind = bkFlat
                BorderStyle = bsNone
                Ctl3D = False
                DataField = 'ThongTinKhac'
                DataSource = DsMaster
                DynProps = <>
                EditButtons = <
                  item
                    DefaultAction = True
                    Style = ebsEllipsisEh
                    Width = 20
                    DrawBackTime = edbtWhenHotEh
                  end>
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                ShowHint = True
                TabOrder = 11
                Visible = True
                WantReturns = True
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Thu nh'#7853'p'
        ImageIndex = 45
        object ScrollThuNhap: TScrollBox
          Left = 0
          Top = 0
          Width = 534
          Height = 500
          Align = alClient
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
          object PaThongTinLuong: TisPanel
            Left = 0
            Top = 0
            Width = 530
            Height = 321
            Align = alTop
            Color = 16119285
            Ctl3D = True
            ParentBackground = False
            ParentCtl3D = False
            TabOrder = 0
            HeaderCaption = ' .: Th'#244'ng tin l'#432#417'ng'
            HeaderColor = 1993170
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object grThongTinLuong: TDBVertGridEh
              Left = 1
              Top = 17
              Width = 528
              Height = 303
              Align = alClient
              AllowedOperations = [alopUpdateEh]
              AllowedSelections = []
              RowCategories.CategoryProps = <>
              PrintService.ColorSchema = pcsFullColorEh
              DataColParams.Font.Charset = DEFAULT_CHARSET
              DataColParams.Font.Color = 8404992
              DataColParams.Font.Height = -13
              DataColParams.Font.Name = 'Tahoma'
              DataColParams.Font.Style = [fsBold]
              DataColParams.MaxRowHeight = 2
              DataColParams.MaxRowLines = 10
              DataColParams.ParentFont = False
              DataColParams.RowHeight = 21
              DataSource = DsMaster
              DrawGraphicData = True
              DrawMemoText = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              GridLineParams.ColorScheme = glcsThemedEh
              Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
              OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
              ParentFont = False
              RowsDefValues.FitRowHeightToData = True
              SearchPanel.Enabled = True
              SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
              TabOrder = 1
              LabelColWidth = 235
              Rows = <
                item
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'LuongChinh'
                end
                item
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'LuongBaoHiem'
                end>
            end
          end
          object PaHeSoLuong: TisPanel
            Left = 0
            Top = 321
            Width = 530
            Height = 155
            Align = alTop
            Color = 16119285
            ParentBackground = False
            TabOrder = 1
            HeaderCaption = ' .: Th'#244'ng tin h'#7879' s'#7889' l'#432#417'ng'
            HeaderColor = 1993170
            ImageSet = 4
            RealHeight = 0
            ShowButton = True
            HeaderBevelInner = bvNone
            HeaderBevelOuter = bvNone
            HeaderFont.Charset = ANSI_CHARSET
            HeaderFont.Color = clWindow
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = [fsBold]
            object grHeSoLuong: TDBVertGridEh
              Left = 1
              Top = 17
              Width = 528
              Height = 137
              Align = alClient
              AllowedOperations = [alopUpdateEh]
              AllowedSelections = []
              RowCategories.CategoryProps = <>
              PrintService.ColorSchema = pcsFullColorEh
              DataColParams.Font.Charset = DEFAULT_CHARSET
              DataColParams.Font.Color = 8404992
              DataColParams.Font.Height = -13
              DataColParams.Font.Name = 'Tahoma'
              DataColParams.Font.Style = [fsBold]
              DataColParams.MaxRowHeight = 2
              DataColParams.MaxRowLines = 10
              DataColParams.ParentFont = False
              DataColParams.RowHeight = 21
              DataSource = DsMaster
              DrawGraphicData = True
              DrawMemoText = True
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Flat = True
              GridLineParams.ColorScheme = glcsThemedEh
              Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
              OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
              ParentFont = False
              RowsDefValues.FitRowHeightToData = True
              SearchPanel.Enabled = True
              SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
              TabOrder = 1
              LabelColWidth = 235
              Rows = <
                item
                  ButtonStyle = cbsDropDown
                  DynProps = <>
                  DropDownSpecRow.Visible = True
                  EditButton.Visible = True
                  EditButton.Width = 20
                  EditButtons = <>
                  FieldName = 'LK_TenBacLuong'
                  FitRowHeightToData = False
                  FitRowHeightToTextLines = False
                  RowLabel.FitHeightToData = False
                end
                item
                  Color = clBtnFace
                  DynProps = <>
                  EditButtons = <>
                  FieldName = 'HeSoLuong'
                  ReadOnly = True
                end>
              object CbBacLuong: TDbLookupComboboxEh2
                Left = 294
                Top = 371
                Width = 279
                Height = 19
                TabStop = False
                ControlLabel.Width = 4
                ControlLabel.Height = 16
                ControlLabel.Caption = ' '
                ControlLabel.Font.Charset = DEFAULT_CHARSET
                ControlLabel.Font.Color = clWindowText
                ControlLabel.Font.Height = -13
                ControlLabel.Font.Name = 'Tahoma'
                ControlLabel.Font.Style = []
                ControlLabel.ParentFont = False
                ControlLabel.Visible = True
                ControlLabelLocation.Spacing = 5
                ControlLabelLocation.Position = lpLeftCenterEh
                AlwaysShowBorder = True
                AutoSize = False
                BevelKind = bkFlat
                Ctl3D = False
                ParentCtl3D = False
                BorderStyle = bsNone
                DynProps = <>
                DataSource = DsMaster
                DropDownBox.Columns = <
                  item
                    FieldName = 'TenBacLuong'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    SpecCell.Font.Charset = DEFAULT_CHARSET
                    SpecCell.Font.Color = clWindowText
                    SpecCell.Font.Height = -12
                    SpecCell.Font.Name = 'Tahoma'
                    SpecCell.Font.Style = []
                    Title.Alignment = taCenter
                    Title.Caption = 'T'#234'n'
                    Width = 160
                  end
                  item
                    FieldName = 'MaBacLuong'
                    Title.Caption = 'M'#227
                    Width = 40
                  end>
                DropDownBox.ListSourceAutoFilter = True
                DropDownBox.ListSourceAutoFilterType = lsftContainsEh
                DropDownBox.ListSourceAutoFilterAllColumns = True
                DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
                DropDownBox.AutoDrop = True
                DropDownBox.Rows = 15
                DropDownBox.Sizable = True
                DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
                DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
                DropDownBox.SpecRow.Font.Color = clWindowText
                DropDownBox.SpecRow.Font.Height = -12
                DropDownBox.SpecRow.Font.Name = 'Tahoma'
                DropDownBox.SpecRow.Font.Style = []
                EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
                EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
                EmptyDataInfo.Font.Color = clSilver
                EmptyDataInfo.Font.Height = -13
                EmptyDataInfo.Font.Name = 'Tahoma'
                EmptyDataInfo.Font.Style = [fsItalic]
                EmptyDataInfo.ParentFont = False
                EmptyDataInfo.Alignment = taLeftJustify
                EditButton.DefaultAction = True
                EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
                EditButton.Style = ebsAltDropDownEh
                EditButton.Width = 20
                EditButton.DrawBackTime = edbtWhenHotEh
                EditButtons = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                Flat = True
                ListField = 'TenBacLuong'
                ParentFont = False
                ShowHint = True
                Style = csDropDownEh
                TabOrder = 0
                Visible = False
              end
            end
          end
        end
      end
    end
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 1244
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    ExplicitTop = 36
    ExplicitWidth = 1244
    inherited Panel1: TPanel
      Width = 1244
      ExplicitWidth = 1244
      inherited CbOrg: TfcTreeCombo
        Left = 470
        Width = 761
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitLeft = 470
        ExplicitWidth = 761
      end
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 84
    Width = 1244
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 84
    ExplicitWidth = 1244
    inherited Panel1: TPanel
      Width = 1244
      ExplicitWidth = 1244
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
      OnExecute = CmdPrint2Execute
    end
    object CmdQuaTrinhLamViec: TAction
      Caption = 'Lo'#7841'i qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      OnExecute = CmdQuaTrinhLamViecExecute
    end
    object CmdNoiLamViec: TAction
      Caption = 'N'#417'i l'#224'm vi'#7879'c'
      OnExecute = CmdNoiLamViecExecute
    end
    object CmdChucDanh: TAction
      Caption = 'Ch'#7913'c danh - ch'#7913'c v'#7909
      OnExecute = CmdChucDanhExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Caption = ' '
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
    object CmdPhongBan: TAction
      Caption = 'T'#7893' ch'#7913'c ph'#242'ng ban'
      OnExecute = CmdPhongBanExecute
    end
    object CmdCapManvQL: TAction
      Hint = 'C'#7845'p m'#227' theo nh'#243'm l'#224'm vi'#7879'c'
      OnExecute = CmdCapManvQLExecute
    end
    object CmdHopDong: TAction
      Caption = 'CmdHopDong'
      OnExecute = CmdHopDongExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Manv'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforeEdit = QrMasterBeforeEdit
    BeforePost = QrMasterBeforePost
    AfterPost = QrMasterAfterPost
    BeforeDelete = QrMasterBeforeDelete
    AfterDelete = QrMasterAfterDelete
    OnCalcFields = QrMasterCalcFields
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_QTLV'
      ' where'#9'1=1')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
      OnValidate = QrMasterManvValidate
    end
    object QrMasterFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrMasterFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterMaQTLV: TWideStringField
      FieldName = 'MaQTLV'
      OnChange = QrMasterMaQTLVChange
    end
    object QrMasterQTLV_Nhom_DacBiet: TIntegerField
      FieldName = 'QTLV_Nhom_DacBiet'
    end
    object QrMasterNgayHieuLuc: TDateTimeField
      FieldName = 'NgayHieuLuc'
      OnChange = QrMasterNgayHieuLucChange
    end
    object QrMasterSoQuyetDinh: TWideStringField
      FieldName = 'SoQuyetDinh'
      Size = 100
    end
    object QrMasterMaChiNhanh: TWideStringField
      FieldName = 'MaChiNhanh'
    end
    object QrMasterMaPhongBan: TWideStringField
      FieldName = 'MaPhongBan'
      OnChange = QrMasterMaPhongBanChange
      Size = 40
    end
    object QrMasterMaBoPhan: TWideStringField
      FieldName = 'MaBoPhan'
      OnChange = QrMasterMaBoPhanChange
      Size = 60
    end
    object QrMasterMaNoiLV: TWideStringField
      FieldName = 'MaNoiLV'
    end
    object QrMasterMaChucDanh: TWideStringField
      FieldName = 'MaChucDanh'
      OnChange = QrMasterMaChucDanhChange
    end
    object QrMasterPhepNam_LuatDinh: TFloatField
      FieldName = 'PhepNam_LuatDinh'
    end
    object QrMasterPhepNam_ChucDanh: TFloatField
      FieldName = 'PhepNam_ChucDanh'
    end
    object QrMasterPhepNam_ThamNien: TFloatField
      FieldName = 'PhepNam_ThamNien'
    end
    object QrMasterCo_PhepNamMotThang: TBooleanField
      FieldName = 'Co_PhepNamMotThang'
    end
    object QrMasterMaNhomLV: TWideStringField
      FieldName = 'MaNhomLV'
    end
    object QrMasterBoNhiem_MaChucDanh: TWideStringField
      FieldName = 'BoNhiem_MaChucDanh'
    end
    object QrMasterBoNhiem_NgayKetThuc: TDateTimeField
      FieldName = 'BoNhiem_NgayKetThuc'
    end
    object QrMasterBietPhai_MaChucDanh: TWideStringField
      FieldName = 'BietPhai_MaChucDanh'
    end
    object QrMasterBietPhai_NgayKetThuc: TDateTimeField
      FieldName = 'BietPhai_NgayKetThuc'
    end
    object QrMasterChucVuChinhQuyen: TWideStringField
      FieldName = 'ChucVuChinhQuyen'
      Size = 100
    end
    object QrMasterCongViecChinh: TWideStringField
      FieldName = 'CongViecChinh'
      Size = 100
    end
    object QrMasterXacNhan_Manv: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i qu'#7843'n l'#253
      FieldName = 'XacNhan_Manv'
      OnChange = QrMasterXacNhan_ManvChange
    end
    object QrMasterMaBacLuong: TWideStringField
      FieldName = 'MaBacLuong'
      OnChange = QrMasterMaBacLuongChange
    end
    object QrMasterBacLuong: TIntegerField
      FieldName = 'BacLuong'
      OnChange = QrMasterMaBacLuongChange
    end
    object QrMasterNgayBacLuong: TDateTimeField
      FieldName = 'NgayBacLuong'
    end
    object QrMasterHeSoLuong: TFloatField
      FieldName = 'HeSoLuong'
    end
    object QrMasterHeSoPhuCap: TFloatField
      FieldName = 'HeSoPhuCap'
    end
    object QrMasterLuongChinh: TFloatField
      FieldName = 'LuongChinh'
    end
    object QrMasterLuongBaoHiem: TFloatField
      FieldName = 'LuongBaoHiem'
    end
    object QrMasterLuongNgay: TFloatField
      FieldName = 'LuongNgay'
    end
    object QrMasterLuongGio: TFloatField
      FieldName = 'LuongGio'
    end
    object QrMasterTyLeLuongThuViec: TFloatField
      FieldName = 'TyLeLuongThuViec'
    end
    object QrMasterCo_BangCong: TBooleanField
      FieldName = 'Co_BangCong'
    end
    object QrMasterCo_BangLuong: TBooleanField
      FieldName = 'Co_BangLuong'
    end
    object QrMasterCo_BHXH: TBooleanField
      FieldName = 'Co_BHXH'
    end
    object QrMasterCo_BHYT: TBooleanField
      FieldName = 'Co_BHYT'
    end
    object QrMasterCo_BHTN: TBooleanField
      FieldName = 'Co_BHTN'
    end
    object QrMasterCo_ChuyenKhoan: TBooleanField
      FieldName = 'Co_ChuyenKhoan'
    end
    object QrMasterLK_TenQuaTrinhLamViec: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuaTrinhLamViec'
      LookupDataSet = HrDataMain.QrV_HR_QUATRINH_LAMVIEC
      LookupKeyFields = 'MaQTLV'
      LookupResultField = 'TenQTLV'
      KeyFields = 'MaQTLV'
      Lookup = True
    end
    object QrMasterLK_TenPhongBan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'TenPhongBan'
      KeyFields = 'MaPhongBan'
      Lookup = True
    end
    object QrMasterLK_TenChucDanh: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'TenChucDanh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrMasterLK_TenBoPhan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBoPhan'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'TenBoPhan'
      KeyFields = 'MaBoPhan'
      Lookup = True
    end
    object QrMasterLK_: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhomLamViec'
      LookupDataSet = HrDataMain.QrDM_NHOMLV
      LookupKeyFields = 'MaNhomLV'
      LookupResultField = 'TenNhomLV'
      KeyFields = 'MaNhomLV'
      Lookup = True
    end
    object QrMasterLK_TenNoiLamViec: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNoiLamViec'
      LookupDataSet = HrDataMain.QrDM_NOILV
      LookupKeyFields = 'MaNoiLV'
      LookupResultField = 'TenNoiLV'
      KeyFields = 'MaNoiLV'
      Lookup = True
    end
    object QrMasterLK_PhepNam_LuatDinh: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_LuatDinh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_LuatDinh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrMasterLK_PhepNam_ChucDanh: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ChucDanh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrMasterLK_PhepNam_ThamNien: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ThamNien'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ThamNien'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrMasterLK_Co_PhepNamMotThang: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_PhepNamMotThang'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'Co_PhepNamMotThang'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterLK_XacNhan_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrMasterCALC_TongPhepNam: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TongPhepNam'
      Calculated = True
    end
    object QrMasterLK_QTLV_Nhom_DacBiet: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_QTLV_Nhom_DacBiet'
      LookupDataSet = QrV_HR_QUATRINH_LAMVIEC
      LookupKeyFields = 'MaQTLV'
      LookupResultField = 'Nhom_DacBiet'
      KeyFields = 'MaQTLV'
      Lookup = True
    end
    object QrMasterLK_TenBacLuong: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBacLuong'
      LookupDataSet = HrDataMain.QrDM_BACLUONG
      LookupKeyFields = 'MaBacLuong'
      LookupResultField = 'TenBacLuong'
      KeyFields = 'MaBacLuong'
      Lookup = True
    end
    object QrMasterCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Size = 0
      Calculated = True
    end
    object QrMasterLK_BoPhan_Ma: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaBoPhan_BanDau'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'Ma'
      KeyFields = 'MaBoPhan'
      Lookup = True
    end
    object QrMasterLK_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_MaPhongBan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'Ma'
      KeyFields = 'MaPhongBan'
      Lookup = True
    end
    object QrMasterNgayKy: TDateTimeField
      FieldName = 'NgayKy'
    end
    object QrMasterNguoiKy: TWideStringField
      FieldName = 'NguoiKy'
      Size = 50
    end
    object QrMasterNamQuyHoach: TIntegerField
      FieldName = 'NamQuyHoach'
    end
    object QrMasterNamQuyHoachBoSung: TIntegerField
      FieldName = 'NamQuyHoachBoSung'
    end
    object QrMasterNgayBienChe: TDateTimeField
      FieldName = 'NgayBienChe'
    end
    object QrMasterBoNhiem_NgayBatDau: TDateTimeField
      FieldName = 'BoNhiem_NgayBatDau'
    end
    object QrMasterBietPhai_NgayBatDau: TDateTimeField
      FieldName = 'BietPhai_NgayBatDau'
    end
    object QrMasterThongTinKhac: TWideMemoField
      FieldName = 'ThongTinKhac'
      BlobType = ftWideMemo
    end
    object QrMasterXetDuyet_Manv: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i x'#233't duy'#7879't (BOD)'
      FieldName = 'XetDuyet_Manv'
      OnChange = QrMasterXetDuyet_ManvChange
    end
    object QrMasterCo_MacDinhCong: TBooleanField
      FieldName = 'Co_MacDinhCong'
    end
    object QrMasterCo_CongDoan: TBooleanField
      FieldName = 'Co_CongDoan'
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_XacNhan_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object QrMasterLK_XetDuyet_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object QrMasterManvQL: TWideStringField
      FieldName = 'ManvQL'
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterLK_SoHopDong: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_SoHopDong'
      LookupDataSet = QrHopDong
      LookupKeyFields = 'Idx'
      LookupResultField = 'SoHopDong'
      KeyFields = 'Idx_HopDong'
      Lookup = True
    end
    object QrMasterLK_NgayKy: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_NgayKy'
      LookupDataSet = QrHopDong
      LookupKeyFields = 'Idx'
      LookupResultField = 'NgayKy'
      KeyFields = 'Idx_HopDong'
      Lookup = True
    end
    object QrMasterLK_NgayHieuLuc: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_NgayHieuLuc'
      LookupDataSet = QrHopDong
      LookupKeyFields = 'Idx'
      LookupResultField = 'NgayHieuLuc'
      KeyFields = 'Idx_HopDong'
      Lookup = True
    end
    object QrMasterFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
    object QrMasterIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
    object QrMasterIdx_HopDong: TIntegerField
      FieldName = 'Idx_HopDong'
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'FileIdx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :FileIdx')
    Left = 188
    Top = 348
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object QrSite: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_CHINHANH'
      'order by MaChiNhanh')
    Left = 52
    Top = 396
  end
  object QrDep: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    DataSource = DsSite
    Parameters = <
      item
        Name = 'MaChiNhanh'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DM_PHONGBAN'
      ' where  MaChiNhanh = :MaChiNhanh'
      'order by MaPhongBan')
    Left = 80
    Top = 396
  end
  object QrSec: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DM_BOPHAN'
      'order by MaBoPhan')
    Left = 108
    Top = 396
  end
  object DsSite: TDataSource
    DataSet = QrSite
    Left = 52
    Top = 424
  end
  object DsDep: TDataSource
    DataSet = QrDep
    Left = 80
    Top = 424
  end
  object DsSec: TDataSource
    DataSet = QrSec
    Left = 108
    Top = 424
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL, a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.' +
        'MaBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 136
    Top = 308
  end
  object QrV_HR_QUATRINH_LAMVIEC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'V_HR_DM_QTLV')
    Left = 256
    Top = 132
  end
  object DsV_HR_QUATRINH_LAMVIEC: TDataSource
    AutoEdit = False
    DataSet = QrV_HR_QUATRINH_LAMVIEC
    Left = 260
    Top = 184
  end
  object QrWorking: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrWorkingBeforeOpen
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'#9' top 1 *'
      '  from'#9'HR_LICHSU_QTLV'
      ' where'#9'Manv =:Manv'
      'order by  [NgayHieuLuc] Desc')
    Left = 185
    Top = 308
    object IntegerField1: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object IntegerField2: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object WideStringField1: TWideStringField
      FieldName = 'Manv'
      OnValidate = QrMasterManvValidate
    end
    object WideStringField2: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object WideStringField3: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object WideMemoField1: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object WideStringField4: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object WideStringField5: TWideStringField
      FieldName = 'MaQTLV'
      OnChange = QrMasterMaQTLVChange
    end
    object IntegerField3: TIntegerField
      FieldName = 'QTLV_Nhom_DacBiet'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'NgayHieuLuc'
    end
    object WideStringField6: TWideStringField
      FieldName = 'SoQuyetDinh'
      Size = 100
    end
    object WideStringField7: TWideStringField
      FieldName = 'MaChiNhanh'
    end
    object WideStringField8: TWideStringField
      FieldName = 'MaPhongBan'
      OnChange = QrMasterMaPhongBanChange
      Size = 40
    end
    object WideStringField9: TWideStringField
      FieldName = 'MaBoPhan'
      OnChange = QrMasterMaBoPhanChange
      Size = 60
    end
    object WideStringField10: TWideStringField
      FieldName = 'MaNoiLV'
    end
    object WideStringField11: TWideStringField
      FieldName = 'MaChucDanh'
      OnChange = QrMasterMaChucDanhChange
    end
    object FloatField1: TFloatField
      FieldName = 'PhepNam_LuatDinh'
    end
    object FloatField2: TFloatField
      FieldName = 'PhepNam_ChucDanh'
    end
    object FloatField3: TFloatField
      FieldName = 'PhepNam_ThamNien'
    end
    object BooleanField1: TBooleanField
      FieldName = 'Co_PhepNamMotThang'
    end
    object WideStringField12: TWideStringField
      FieldName = 'MaNhomLV'
    end
    object WideStringField13: TWideStringField
      FieldName = 'BoNhiem_MaChucDanh'
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'BoNhiem_NgayKetThuc'
    end
    object WideStringField14: TWideStringField
      FieldName = 'BietPhai_MaChucDanh'
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'BietPhai_NgayKetThuc'
    end
    object WideStringField15: TWideStringField
      FieldName = 'ChucVuChinhQuyen'
      Size = 100
    end
    object WideStringField16: TWideStringField
      FieldName = 'CongViecChinh'
      Size = 100
    end
    object WideStringField18: TWideStringField
      FieldName = 'XacNhan_Manv'
    end
    object WideStringField19: TWideStringField
      FieldName = 'MaBacLuong'
      OnChange = QrMasterMaBacLuongChange
    end
    object IntegerField4: TIntegerField
      FieldName = 'BacLuong'
      OnChange = QrMasterMaBacLuongChange
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'NgayBacLuong'
    end
    object FloatField4: TFloatField
      FieldName = 'HeSoLuong'
    end
    object FloatField5: TFloatField
      FieldName = 'HeSoPhuCap'
    end
    object FloatField6: TFloatField
      FieldName = 'LuongChinh'
    end
    object FloatField7: TFloatField
      FieldName = 'LuongBaoHiem'
    end
    object FloatField8: TFloatField
      FieldName = 'LuongNgay'
    end
    object FloatField9: TFloatField
      FieldName = 'LuongGio'
    end
    object FloatField10: TFloatField
      FieldName = 'TyLeLuongThuViec'
    end
    object BooleanField2: TBooleanField
      FieldName = 'Co_BangCong'
    end
    object BooleanField3: TBooleanField
      FieldName = 'Co_BangLuong'
    end
    object BooleanField4: TBooleanField
      FieldName = 'Co_BHXH'
    end
    object BooleanField5: TBooleanField
      FieldName = 'Co_BHYT'
    end
    object BooleanField6: TBooleanField
      FieldName = 'Co_BHTN'
    end
    object BooleanField7: TBooleanField
      FieldName = 'Co_ChuyenKhoan'
    end
    object WideStringField20: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuaTrinhLamViec'
      LookupDataSet = HrDataMain.QrV_HR_QUATRINH_LAMVIEC
      LookupKeyFields = 'MaQTLV'
      LookupResultField = 'TenQTLV'
      KeyFields = 'MaQTLV'
      Lookup = True
    end
    object WideStringField21: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhongBan'
      LookupDataSet = HrDataMain.QrDMPHONGBAN
      LookupKeyFields = 'MaPhongBan'
      LookupResultField = 'TenPhongBan'
      KeyFields = 'MaPhongBan'
      Lookup = True
    end
    object WideStringField22: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'TenChucDanh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object WideStringField23: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBoPhan'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'TenBoPhan'
      KeyFields = 'MaBoPhan'
      Lookup = True
    end
    object WideStringField24: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhomLamViec'
      LookupDataSet = HrDataMain.QrDM_NHOMLV
      LookupKeyFields = 'MaNhomLV'
      LookupResultField = 'TenNhomLV'
      KeyFields = 'MaNhomLV'
      Lookup = True
    end
    object WideStringField25: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNoiLamViec'
      LookupDataSet = HrDataMain.QrDM_NOILV
      LookupKeyFields = 'MaNoiLV'
      LookupResultField = 'TenNoiLV'
      KeyFields = 'MaNoiLV'
      Lookup = True
    end
    object FloatField11: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_LuatDinh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_LuatDinh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object FloatField12: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ChucDanh'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ChucDanh'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object FloatField13: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_PhepNam_ThamNien'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'PhepNam_ThamNien'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object BooleanField8: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_Co_PhepNamMotThang'
      LookupDataSet = HrDataMain.QrDM_CHUCDANH
      LookupKeyFields = 'MaChucDanh'
      LookupResultField = 'Co_PhepNamMotThang'
      KeyFields = 'MaChucDanh'
      Lookup = True
    end
    object WideStringField26: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XetDuyet_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XetDuyet_Manv'
      Lookup = True
    end
    object WideStringField27: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_XacNhan_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'XacNhan_Manv'
      Lookup = True
    end
    object FloatField14: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_TongPhepNam'
      Calculated = True
    end
    object IntegerField5: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_QTLV_Nhom_DacBiet'
      LookupDataSet = QrV_HR_QUATRINH_LAMVIEC
      LookupKeyFields = 'MaQTLV'
      LookupResultField = 'Nhom_DacBiet'
      KeyFields = 'MaQTLV'
      Lookup = True
    end
    object WideStringField28: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenBacLuong'
      LookupDataSet = HrDataMain.QrDM_BACLUONG
      LookupKeyFields = 'MaBacLuong'
      LookupResultField = 'TenBacLuong'
      KeyFields = 'MaBacLuong'
      Lookup = True
    end
    object WideStringField29: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Size = 0
      Calculated = True
    end
    object WideStringField30: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaBoPhan_BanDau'
      LookupDataSet = HrDataMain.QrDMBOPHAN
      LookupKeyFields = 'MaBoPhan'
      LookupResultField = 'Ma'
      KeyFields = 'MaBoPhan'
      Lookup = True
    end
    object WideStringField31: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrWorkingNgayKy: TDateTimeField
      FieldName = 'NgayKy'
    end
    object QrWorkingNguoiKy: TWideStringField
      FieldName = 'NguoiKy'
      Size = 50
    end
    object QrWorkingNamQuyHoach: TIntegerField
      FieldName = 'NamQuyHoach'
    end
    object QrWorkingNamQuyHoachBoSung: TIntegerField
      FieldName = 'NamQuyHoachBoSung'
    end
    object QrWorkingNgayBienChe: TDateTimeField
      FieldName = 'NgayBienChe'
    end
    object QrWorkingBoNhiem_NgayBatDau: TDateTimeField
      FieldName = 'BoNhiem_NgayBatDau'
    end
    object QrWorkingBietPhai_NgayBatDau: TDateTimeField
      FieldName = 'BietPhai_NgayBatDau'
    end
    object QrWorkingThongTinKhac: TWideMemoField
      FieldName = 'ThongTinKhac'
      BlobType = ftWideMemo
    end
    object QrWorkingXetDuyet_Manv: TWideStringField
      FieldName = 'XetDuyet_Manv'
    end
    object QrWorkingCo_MacDinhCong: TBooleanField
      FieldName = 'Co_MacDinhCong'
    end
    object QrWorkingCo_CongDoan: TBooleanField
      FieldName = 'Co_CongDoan'
    end
    object QrWorkingManvQL: TWideStringField
      FieldName = 'ManvQL'
    end
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 138
    Top = 352
  end
  object QrHopDong: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select a.Idx, a.Manv, a.SoHopDong, a.MaHopDong, b.TenHopDong, a.' +
        'NgayHieuLuc, a.NgayKy'
      '  from'#9'HR_LICHSU_HOPDONG a'
      'inner join HR_DM_HOPDONG b on a.MaHopDong = b.MaHopDong'
      'order by a.NgayHieuLuc Desc')
    Left = 289
    Top = 252
    object QrHopDongManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrHopDongSoHopDong: TWideStringField
      FieldName = 'SoHopDong'
      Size = 50
    end
    object QrHopDongMaHopDong: TWideStringField
      FieldName = 'MaHopDong'
    end
    object QrHopDongTenHopDong: TWideStringField
      FieldName = 'TenHopDong'
      Size = 200
    end
    object QrHopDongNgayHieuLuc: TDateTimeField
      FieldName = 'NgayHieuLuc'
    end
    object QrHopDongNgayKy: TDateTimeField
      FieldName = 'NgayKy'
    end
    object QrHopDongIdx: TIntegerField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsHopDong: TDataSource
    AutoEdit = False
    DataSet = QrHopDong
    Left = 292
    Top = 280
  end
end
