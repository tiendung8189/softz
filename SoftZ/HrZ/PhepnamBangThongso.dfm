object FrmPhepnamBangThongso: TFrmPhepnamBangThongso
  Left = 242
  Top = 114
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Th'#244'ng S'#7889' Ph'#233'p N'#259'm'
  ClientHeight = 349
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000000000000680500001600000028000000100000002000
    0000010008000000000040010000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000808080001C1C
    2400747C7C00A49CA4007C84AC00FC04FC003C546400A4B4CC008CA4B4006464
    6400C4C4CC0034444C0084848400645C84008CA4C4008C94AC00B4BCBC005C5C
    7C00647C9400647494002C2C4400A4CCDC00C4D4E400748C9C007C94BC00ACAC
    BC004C445400B4BCCC00646C840094B4C40024243400544C6400848C9C00747C
    94009CA4AC00C4D4D4007494AC007C748C00ACC4CC003C4454005C6C7C00646C
    8C0094A4BC002C2C2C00ACACAC00645C7C008C9CBC00B4B4BC009CB4D4009C9C
    9C007484A4004C4C6400A4B4D400C4CCDC009CACC400C4CCC400D4E4E4008494
    BC00A4ACC4005454540094B4CC005C646C008494AC00444454001C1C2C00CCC4
    D4008C8C94008C94B400C4BCC4005C648C00647C9C00747494003C3C5400748C
    A400B4C4D400343434008494A400747C9C00DCDCDC00646C7C00A4A4B400848C
    AC00A4BCCC0064648C008CACC4005C648400BCD4E400D4DCE400647484005454
    740094A4B400C4D4DC0064748C0094ACBC00A4B4C4001C242400A4A4AC00748C
    AC00444C6C0084A4BC00C4CCCC0064648400B4BCC40054647C006C749C003C2C
    4400C4DCE400749CBC004C445C00B4C4CC009CBCC400242C340054546C008484
    A4007C94B40034445C005C748C00ACACB4004C546C00849CC40054545C0094BC
    CC0074747400444444009494A4006C6C6C008C8C8C00243444005C5464007C84
    9400546C840024342C009CA4A400ACBCD400CCE4EC005C5C74008494B4004444
    5C0024242C00949494008C9CB4006C849C007C94A40074849C006C747C009CAC
    B400CCDCDC003C4C5C00B4B4B4005C5C5C007C7C7C00A4A4A400ACB4CC0094A4
    C4008C9CAC00BCBCBC006C7C94006C749400ACCCDC007C8C9C00ACB4BC00BCBC
    CC006C6C84009CB4C400CCD4D4007C94AC00ACC4D4006C6C8C009CA4BC006464
    7C00949CBC007C84A4004C5464009CACCC00849CBC00ACACC4009CB4CC00849C
    AC00CCCCD400948C9400C4C4C4006C7C9C0074749C003C3C5C007C8CA400BCC4
    D40034343C00ACBCCC0094ACC400BCDCE4006C7484009CA4B4006C748C009CAC
    BC00ACB4C400242424007C8CAC0044546C008CA4BC00CCCCCC006C6484005C64
    7C00CCDCE4007C9CBC004C4C5C002C2C3400848CA400444C5C00949CB4007C84
    9C00000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    000004FEFC04000000010CF901000000000004FEFC04000000010CF901000000
    000004BBFEFC000000015EBBF900000000000004040400000000010101000000
    00000000A2000000000013A20000000000000000A2000000000013A200000000
    00000000A2000000000013A20000000000000000A20000000000000000000013
    00009D9DA29D9D0000009D00000000BB9D00BBA2BBA2BB9D9D9DA200000000BB
    BB00A2BBA2BBA2A2BBA200A2000000BB9D00A2389EA2A2BBA200BBA29D000808
    00000808080808080808A2A21300000000000000000000000008BBA29D000000
    00000000000000000008A2A2130000000000000000000000000808080800F060
    0000F0600000F0600000F8710000F8F10000F8F10000F8F10000307100000001
    000000010000000100000000000010000000FFE00000FFE00000FFE00000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 328
    Width = 640
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitWidth = 792
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 640
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    ExplicitWidth = 792
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton8: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 400
    Height = 292
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'KHOASO_CONG;CheckBox;True;False'
      'KHOASO_LUONG;CheckBox;True;False'
      'THUONGTET;CheckBox;True;False'
      'KHOASO_LICHLV;CheckBox;True;False')
    Selected.Strings = (
      'NAM'#9'8'#9'N'#259'm'#9'F'#9'K'#7923' l'#432#417'ng')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = Ds1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = False
    UseTFields = False
    PadColumnStyle = pcsPadHeader
    ExplicitWidth = 552
  end
  object PaPsComment: TPanel
    Left = 400
    Top = 36
    Width = 240
    Height = 292
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitLeft = 552
    object PDComment: TGroupBox
      Left = 0
      Top = 0
      Width = 240
      Height = 292
      Align = alClient
      Caption = ' Ghi ch'#250' '
      TabOrder = 0
      ExplicitWidth = 360
      object EdComment: TDBMemo
        AlignWithMargins = True
        Left = 5
        Top = 21
        Width = 230
        Height = 266
        Align = alClient
        DataField = 'GhiChu'
        DataSource = Ds1
        TabOrder = 0
        ExplicitWidth = 350
      end
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 84
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdCountWD: TAction
      Category = 'PROC'
      Caption = 'T'#237'nh s'#7889' ng'#224'y c'#244'ng th'#7921'c t'#7871'    '
      ShortCut = 119
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
  end
  object Qr1: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = Qr1BeforeInsert
    AfterInsert = Qr1AfterInsert
    BeforeEdit = Qr1BeforeEdit
    BeforePost = Qr1BeforePost
    BeforeDelete = Qr1BeforeDelete
    OnDeleteError = Qr1PostError
    OnPostError = Qr1PostError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_PHEPNAM_THONGSO'
      'order by [Nam]')
    Left = 20
    Top = 169
    object Qr1Nam: TIntegerField
      FieldName = 'Nam'
    end
    object Qr1GhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object Qr1CREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object Qr1UPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object Qr1CREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object Qr1UPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object Qr1PhepNam_NamSau_ToiDa: TFloatField
      FieldName = 'PhepNam_NamSau_ToiDa'
    end
    object Qr1PhepNam_NamTruoc_ThoiHan: TFloatField
      FieldName = 'PhepNam_NamTruoc_ThoiHan'
    end
  end
  object Ds1: TDataSource
    DataSet = Qr1
    Left = 20
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 52
    Top = 168
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 116
    Top = 168
    object Tnhsngycngthct1: TMenuItem
      Action = CmdCountWD
    end
  end
  object COUNT_WD: TADOCommand
    CommandText = 'TS_COUNT_MWD;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@YEAR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MONTH'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 20
    Top = 232
  end
end
