object FrmDaoTao: TFrmDaoTao
  Left = 308
  Top = 68
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #272#224'o T'#7841'o'
  ClientHeight = 573
  ClientWidth = 1016
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1016
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 1016
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1016
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolMain'
    Color = 16119285
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentColor = False
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton9: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object SepChecked: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 1016
    Height = 533
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 1008
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 48
        Width = 1008
        Height = 435
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'LK_KhoaHocTenNhom'#9'20'#9'Lo'#7841'i/ Nh'#243'm '#273#224'o t'#7841'o'#9'F'
          'MaKhoaHoc'#9'8'#9'M'#227#9'F'#9'Kh'#243'a h'#7885'c'
          'TenKhoaHoc'#9'30'#9'T'#234'n'#9'F'#9'Kh'#243'a h'#7885'c'
          'NgayBatDau'#9'10'#9'B'#7855't '#273#7847'u'#9'F'#9'Ng'#224'y'
          'NgayKetThuc'#9'10'#9'K'#7871't th'#250'c'#9'F'#9'Ng'#224'y'
          'SoTien'#9'10'#9'T'#7893'ng chi ph'#237#9'F'
          'TrungTam'#9'30'#9'Trung t'#226'm/ Tr'#432#7901'ng'#9'F'
          'GhiChu'#9'30'#9'Ghi ch'#250#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frD2D: TfrD2D
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alTop
        AutoSize = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        ExplicitWidth = 1008
        inherited Panel1: TPanel
          Width = 1008
          ExplicitWidth = 1008
          inherited CbOrg: TfcTreeCombo
            Left = 470
            Width = 525
            Items.StreamVersion = 1
            Items.Data = {00000000}
            ExplicitLeft = 470
            ExplicitWidth = 525
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaThongTinKhoaHoc: TisPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 170
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        HeaderCaption = ' :: Th'#244'ng tin kh'#243'a h'#7885'c'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label1: TLabel
          Left = 47
          Top = 72
          Width = 75
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y b'#7855't '#273#7847'u'
        end
        object TntLabel9: TLabel
          Left = 78
          Top = 146
          Width = 42
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ghi ch'#250
        end
        object Label2: TLabel
          Left = 304
          Top = 72
          Width = 77
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y k'#7871't th'#250'c'
        end
        object Label4: TLabel
          Left = 667
          Top = 72
          Width = 17
          Height = 16
          Alignment = taRightJustify
          Caption = 'gi'#7901
        end
        object Label3: TLabel
          Left = 741
          Top = 72
          Width = 123
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y h'#7871't h'#7841'n ph'#7909'c v'#7909
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 127
          Top = 70
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayBatDau'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 4
        end
        object DBMemo2: TDBMemo
          Left = 127
          Top = 142
          Width = 360
          Height = 22
          Ctl3D = False
          DataField = 'GhiChu'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
        end
        object wwDBEdit25: TDBEditEh
          Left = 127
          Top = 46
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 77
          ControlLabel.Height = 16
          ControlLabel.Caption = 'T'#234'n kh'#243'a h'#7885'c'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'TenKhoaHoc'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object wwDBDateTimePicker2: TwwDBDateTimePicker
          Left = 386
          Top = 70
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayKetThuc'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object CbNhomDaoTao: TDbLookupComboboxEh2
          Left = 127
          Top = 94
          Width = 360
          Height = 22
          ControlLabel.Width = 112
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Lo'#7841'i/ Nh'#243'm '#273#224'o t'#7841'o'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MaKhoaHoc_Loai'
          DataSource = DsNX
          DropDownBox.Columns = <
            item
              FieldName = 'TEN_HOTRO'
              Title.Alignment = taCenter
              Width = 210
            end>
          DropDownBox.ListSource = HrDataMain.DsV_HR_DAOTAO_NHOM
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.UseMultiTitle = True
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <
            item
              Action = CmdNhomDaoTao
              DefaultAction = False
              Style = ebsEllipsisEh
              Width = 20
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA_HOTRO'
          ListField = 'TEN_HOTRO'
          ListSource = HrDataMain.DsV_HR_DAOTAO_NHOM
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 8
          Visible = True
        end
        object EdMa: TDBEditEh
          Tag = 1
          Left = 127
          Top = 22
          Width = 101
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          CharCase = ecUpperCase
          ControlLabel.Width = 72
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' kh'#243'a h'#7885'c'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MaKhoaHoc'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HighlightRequired = True
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 0
          Visible = True
        end
        object EdSoQuyetDinh: TDBEditEh
          Left = 610
          Top = 142
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 59
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Gi'#7843'ng vi'#234'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'GiangVien'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 13
          Visible = True
        end
        object DBEditEh1: TDBEditEh
          Left = 610
          Top = 118
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 82
          ControlLabel.Height = 16
          ControlLabel.Caption = #272#417'n v'#7883' t'#7893' ch'#7913'c'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DonViToChuc'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 11
          Visible = True
        end
        object CbQuocGia: TDbLookupComboboxEh2
          Left = 127
          Top = 118
          Width = 360
          Height = 22
          ControlLabel.Width = 97
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Qu'#7889'c gia '#273#224'o t'#7841'o'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MaQuocGia'
          DataSource = DsNX
          DropDownBox.Columns = <
            item
              FieldName = 'TEN'
              Title.Alignment = taCenter
              Width = 210
            end>
          DropDownBox.ListSource = DataMain.DsV_QuocGia
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.UseMultiTitle = True
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <
            item
              Action = CmdQuocGia
              DefaultAction = False
              Style = ebsEllipsisEh
              Width = 20
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'TEN'
          ListSource = DataMain.DsV_QuocGia
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 10
          Visible = True
        end
        object DBEditEh2: TDBEditEh
          Left = 610
          Top = 22
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 51
          ControlLabel.Height = 16
          ControlLabel.Caption = #272#7883'a '#273'i'#7875'm'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DiaDiem'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object DBEditEh3: TDBEditEh
          Left = 610
          Top = 46
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 111
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Trung t'#226'm/ Tr'#432#7901'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'TrungTam'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object DBEditEh4: TDBEditEh
          Left = 610
          Top = 94
          Width = 360
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 50
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#7909'c '#273#237'ch'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MucDich'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 9
          Visible = True
        end
        object DBNumberEditEh1: TDBNumberEditEh
          Left = 610
          Top = 70
          Width = 50
          Height = 22
          ControlLabel.Width = 77
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Th'#7901'i gian h'#7885'c'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'SoGioHoc'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 869
          Top = 70
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayHetHanPhucVu'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 7
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 170
        Width = 1008
        Height = 334
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' .: Chi ti'#7871't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 1008
          Height = 318
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False'
            'LK_TENTHUE;CustomEdit;CbLOAITHUE;F'
            'Co_NopChungChi;CheckBox;True;False'
            'Co_ThueTNCN;CheckBox;True;False'
            'Co_ThamGia;CheckBox;True;False'
            'MaPhuCap;CustomEdit;CbMaPhuCap;F'
            'LK_TenPhuCap;CustomEdit;CbPhuCap;F'
            'LK_ManvQL;CustomEdit;CbManvQL;F'
            'LK_Tennv;CustomEdit;CbTennv;F'
            'Thang;CustomEdit;CbMon;F'
            'Nam;CustomEdit;CbYear;F')
          Selected.Strings = (
            'STT'#9'3'#9'STT'#9'F'
            'Manv'#9'15'#9'ID'#9'T'#9'Nh'#226'n vi'#234'n'
            'LK_ManvQL'#9'15'#9'M'#227#9'F'#9'Nh'#226'n vi'#234'n'
            'LK_Tennv'#9'25'#9'T'#234'n'#9'F'#9'Nh'#226'n vi'#234'n'
            'Co_ThamGia'#9'5'#9'C'#243' m'#7863't'#9'F'
            'SoTienNhanVien'#9'11'#9'Nh'#226'n vi'#234'n tr'#7843#9'F'#9'Chi ph'#237
            'SoTienCongTy'#9'11'#9'C'#244'ng ty tr'#7843#9'F'#9'Chi ph'#237
            'SoTien'#9'11'#9'T'#7893'ng'#9'T'#9'Chi ph'#237
            'MaPhuCap'#9'11'#9'M'#227#9'F'#9'Ph'#7909' c'#7845'p'
            'LK_TenPhuCap'#9'25'#9'T'#234'n'#9'F'#9'Ph'#7909' c'#7845'p'
            'Co_ThueTNCN'#9'5'#9'Thu'#7871' TNCN'#9'F'#9'C'#243' ch'#7883'u'
            'Thang'#9'6'#9'Th'#225'ng'#9'F'#9'Thu'#7897'c k'#7923' l'#432#417'ng'
            'Nam'#9'8'#9'N'#259'm'#9'F'#9'Thu'#7897'c k'#7923' l'#432#417'ng'
            'NgayCap'#9'12'#9'Ng'#224'y c'#7845'p'#9'F'#9'Ch'#7913'ng ch'#7881
            'Co_NopChungChi'#9'5'#9'N'#7897'p cty'#9'F'#9'Ch'#7913'ng ch'#7881
            'ChungChi'#9'25'#9'Lo'#7841'i b'#7857'ng'#9'F'#9'Ch'#7913'ng ch'#7881
            'GhiChu'#9'30'#9'Ghi ch'#250#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
        object CbMaPhuCap: TwwDBLookupCombo
          Left = 439
          Top = 88
          Width = 130
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'MaPhuCap'#9'10'#9'MaPhuCap'#9'F'
            'TenPhuCap'#9'30'#9'TenPhuCap'#9'F')
          DataField = 'MaPhuCap'
          DataSource = DsCT
          LookupTable = HrDataMain.QrDM_HR_PHUCAP
          LookupField = 'MaPhuCap'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object CbPhuCap: TwwDBLookupCombo
          Left = 439
          Top = 116
          Width = 130
          Height = 22
          BiDiMode = bdLeftToRight
          ParentBiDiMode = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TenPhuCap'#9'30'#9'TenPhuCap'#9'F'
            'MaPhuCap'#9'10'#9'MaPhuCap'#9'F')
          DataField = 'MaPhuCap'
          DataSource = DsCT
          LookupTable = HrDataMain.QrDM_HR_PHUCAP
          LookupField = 'MaPhuCap'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object CbManvQL: TwwDBLookupCombo
          Left = 143
          Top = 216
          Width = 130
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'ManvQL'#9'10'#9'ManvQL'#9'F'
            'Tennv'#9'30'#9'Tennv'#9'F')
          DataField = 'Manv'
          DataSource = DsCT
          LookupTable = HrDataMain.QrDMNV
          LookupField = 'Manv'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object CbTennv: TwwDBLookupCombo
          Left = 276
          Top = 216
          Width = 130
          Height = 22
          BiDiMode = bdLeftToRight
          ParentBiDiMode = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Tennv'#9'30'#9'Tennv'#9'F'
            'ManvQL'#9'10'#9'ManvQL'#9'F')
          DataField = 'Manv'
          DataSource = DsCT
          LookupTable = HrDataMain.QrDMNV
          LookupField = 'Manv'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 5
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object CbMon: TwwDBComboBox
          Left = 511
          Top = 182
          Width = 58
          Height = 24
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          ShowMatchText = True
          DataField = 'ThangBatDau'
          DropDownCount = 6
          ItemHeight = 0
          Sorted = True
          TabOrder = 6
          UnboundDataType = wwDefault
        end
        object CbYear: TwwDBComboBox
          Left = 583
          Top = 182
          Width = 58
          Height = 24
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          ShowMatchText = True
          DataField = 'NamBatDau'
          DropDownCount = 6
          ItemHeight = 0
          Sorted = True
          TabOrder = 7
          UnboundDataType = wwDefault
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 874
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 874
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 354
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng...'
    end
    object CmdFromOrder: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' '#273#417'n '#273#7863't h'#224'ng'
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
    end
    object CmdThamkhaoGia: TAction
      Category = 'DETAIL'
      Caption = 'Tham kh'#7843'o gi'#225
    end
    object CmdImportExcel: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y d'#7919' li'#7879'u t'#7915' file Excel'
    end
    object CmdFromNhap: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' phi'#7871'u nh'#7853'p'
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
    end
    object CmdReRead: TAction
      Caption = 'CmdReRead'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdNhomDaoTao: TAction
      Caption = 'Lo'#7841'i/ Nh'#243'm '#273#224'o t'#7841'o'
      OnExecute = CmdNhomDaoTaoExecute
    end
    object CmdQuocGia: TAction
      Caption = 'Qu'#7889'c gia '#273#224'o t'#7841'o'
      OnExecute = CmdQuocGiaExecute
    end
    object CmdTotal: TAction
      Caption = 'CmdTotal'
      OnExecute = CmdTotalExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 124
    Top = 354
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    AfterInsert = QrNXAfterInsert
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_DAOTAO'
      ' where'#9'1=1')
    Left = 724
    Top = 372
    object QrNXMaKhoaHoc: TWideStringField
      DisplayLabel = 'M'#227' kh'#243'a h'#7885'c'
      FieldName = 'MaKhoaHoc'
    end
    object QrNXTenKhoaHoc: TWideStringField
      DisplayLabel = 'T'#234'n kh'#243'a h'#7885'c'
      FieldName = 'TenKhoaHoc'
      Size = 200
    end
    object QrNXNgayBatDau: TDateTimeField
      DisplayLabel = 'Ng'#224'y b'#7855't '#273#7847'u'
      FieldName = 'NgayBatDau'
    end
    object QrNXNgayKetThuc: TDateTimeField
      DisplayLabel = 'Ng'#224'y k'#7871't th'#250'c'
      FieldName = 'NgayKetThuc'
    end
    object QrNXNgayHetHanPhucVu: TDateTimeField
      FieldName = 'NgayHetHanPhucVu'
    end
    object QrNXMaQuocGia: TWideStringField
      DisplayLabel = 'Qu'#7889'c gia '#273#224'o t'#7841'o'
      FieldName = 'MaQuocGia'
      Size = 5
    end
    object QrNXMaKhoaHoc_Loai: TWideStringField
      DisplayLabel = 'Lo'#7841'i/ Nh'#243'm '#273#224'o t'#7841'o'
      FieldName = 'MaKhoaHoc_Loai'
      Size = 70
    end
    object QrNXDiaDiem: TWideStringField
      DisplayLabel = #272#7883'a '#273'i'#7875'm'
      FieldName = 'DiaDiem'
      Size = 200
    end
    object QrNXTrungTam: TWideStringField
      DisplayLabel = 'Trung t'#226'm/ Tr'#432#7901'ng'
      FieldName = 'TrungTam'
      Size = 200
    end
    object QrNXMucDich: TWideStringField
      DisplayLabel = 'M'#7909'c '#273#237'ch'
      FieldName = 'MucDich'
      Size = 200
    end
    object QrNXDonViToChuc: TWideStringField
      DisplayLabel = #272#417'n v'#7883' t'#7893' ch'#7913'c'
      FieldName = 'DonViToChuc'
      Size = 200
    end
    object QrNXGiangVien: TWideStringField
      DisplayLabel = 'Gi'#7843'ng vi'#234'n'
      FieldName = 'GiangVien'
      Size = 200
    end
    object QrNXSoGioHoc: TFloatField
      DisplayLabel = 'Th'#7901'i gian h'#7885'c'
      FieldName = 'SoGioHoc'
    end
    object QrNXSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrNXSoTienNhanVien: TFloatField
      FieldName = 'SoTienNhanVien'
    end
    object QrNXSoTienCongTy: TFloatField
      FieldName = 'SoTienCongTy'
    end
    object QrNXSoTien: TFloatField
      FieldName = 'SoTien'
    end
    object QrNXGhiChu: TWideMemoField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrNXCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrNXUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrNXLK_TenQuocGia: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuocGia'
      LookupDataSet = HrDataMain.QrV_QUOCTICH
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'MaQuocGia'
      Lookup = True
    end
    object QrNXLK_KhoaHocTenLoai: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_KhoaHocTenLoai'
      LookupDataSet = HrDataMain.QrV_HR_DAOTAO_NHOM
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'MaKhoaHoc_Loai'
      Lookup = True
    end
    object QrNXIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterInsert
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'Idx_DaoTao'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from HR_DAOTAO_CHITIET'
      ' where Idx_DaoTao= :Idx_DaoTao'
      'order by STT')
    Left = 756
    Top = 372
    object QrCTManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrCTManvChange
      OnValidate = QrCTManvValidate
    end
    object QrCTCo_ThamGia: TBooleanField
      FieldName = 'Co_ThamGia'
    end
    object QrCTSoHopDong: TWideStringField
      FieldName = 'SoHopDong'
      Size = 50
    end
    object QrCTSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrCTSoTienNhanVien: TFloatField
      FieldName = 'SoTienNhanVien'
      OnChange = QrCTSoTienNhanVienChange
      EditFormat = '###0.##;-###0.##;#'
    end
    object QrCTSoTienCongTy: TFloatField
      FieldName = 'SoTienCongTy'
      OnChange = QrCTSoTienNhanVienChange
      EditFormat = '###0.##;-###0.##;#'
    end
    object QrCTSoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrCTSoTienChange
    end
    object QrCTNgayCap: TDateTimeField
      FieldName = 'NgayCap'
    end
    object QrCTChungChi: TWideStringField
      FieldName = 'ChungChi'
      Size = 70
    end
    object QrCTCo_NopChungChi: TBooleanField
      FieldName = 'Co_NopChungChi'
    end
    object QrCTMaPhuCap: TWideStringField
      FieldName = 'MaPhuCap'
      Size = 70
    end
    object QrCTThang: TIntegerField
      FieldName = 'Thang'
    end
    object QrCTNam: TIntegerField
      FieldName = 'Nam'
    end
    object QrCTCo_ThueTNCN: TBooleanField
      FieldName = 'Co_ThueTNCN'
    end
    object QrCTGhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTLK_Tennv: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tennv'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'Tennv'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrCTLK_TenPhuCap: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPhuCap'
      LookupDataSet = HrDataMain.QrDM_HR_PHUCAP
      LookupKeyFields = 'MaPhuCap'
      LookupResultField = 'TenPhuCap'
      KeyFields = 'MaPhuCap'
      Lookup = True
    end
    object QrCTLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      OnChange = QrCTLK_ManvQLChange
      Lookup = True
    end
    object QrCTIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
    object QrCTIdx_DaoTao: TIntegerField
      FieldName = 'Idx_DaoTao'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 724
    Top = 424
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 756
    Top = 424
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 434
    Top = 400
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ExportraExceltlidliu2: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 370
    Top = 384
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 382
    Top = 428
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
      Caption = 'X'#243'a t'#7845't c'#7843' chi ti'#7871't'
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SoLuong'
      'SoTienCongTy'
      'SoTienNhanVien'
      'SoTien')
    DetailFields.Strings = (
      'SoLuong'
      'SoTienCongTy'
      'SoTienNhanVien'
      'SoTien')
    Left = 372
    Top = 286
  end
end
