object FrmHoSonvDanhSachImport: TFrmHoSonvDanhSachImport
  Left = 149
  Top = 197
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Danh S'#225'ch Nh'#226'n Vi'#234'n'
  ClientHeight = 386
  ClientWidth = 1038
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object GrList: TwwDBGrid2
    Left = 0
    Top = 0
    Width = 1038
    Height = 345
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'XetDuyet_TinhTrang;ImageIndex;Original Size')
    Selected.Strings = (
      'ManvQL'#9'8'#9'M'#227#9'F'#9'Nh'#226'n vi'#234'n'
      'Tennv'#9'24'#9'H'#7885' t'#234'n'#9'F'#9'Nh'#226'n vi'#234'n'
      'NgayVaoLam'#9'10'#9'V'#224'o l'#224'm'#9'F'#9'Ng'#224'y'
      'NgayVaoLamChinhThuc'#9'10'#9'V'#224'o l'#224'm C.Th'#7913'c'#9'F'#9'Ng'#224'y'
      'NgayThamNien'#9'10'#9'Th'#226'm ni'#234'n'#9'F'#9'Ng'#224'y'
      'NgayThoiViec'#9'10'#9'Th'#244'i vi'#7879'c'#9'F'#9'Ng'#224'y'
      'TenNhomLV'#9'12'#9'Nh'#243'm l'#224'm vi'#7879'c'#9'F'
      'TenPhongBan'#9'18'#9'Ph'#242'ng ban'#9'F'
      'TenChucDanh'#9'30'#9'Ch'#7913'c danh'#9'F'
      'MaSoThue'#9'12'#9'M'#227' s'#7889' thu'#7871#9'F'
      'NgaySinh'#9'10'#9'Ng'#224'y sinh'#9'F'
      'Email'#9'25'#9'C'#244'ng ty'#9'F'#9'Email'
      'EmailCaNhan'#9'25'#9'C'#225' nh'#226'n'#9'F'#9'Email'
      'DienThoai'#9'30'#9#272'i'#7879'n tho'#7841'i'#9'F'
      'CuTru_DiaChi'#9'30'#9'C'#432' tr'#250#9'F'#9#272#7883'a ch'#7881
      'ThuongTru_DiaChi'#9'30'#9'Th'#432#7901'ng tr'#250#9'F'#9#272#7883'a ch'#7881
      'LuongChinh'#9'10'#9'Ch'#237'nh'#9'F'#9'M'#7913'c l'#432#417'ng'
      'LuongBaoHiem'#9'10'#9'B'#7843'o hi'#7875'm'#9'F'#9'M'#7913'c l'#432#417'ng'
      'GhiChu'#9'20'#9'Ghi ch'#250#9'F'
      'ErrCode'#9'40'#9'ErrCode'#9'T')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    OnEnter = CmdRefreshExecute
    ImageList = DataMain.ImageStatus
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object Panel3: TPanel
    Left = 0
    Top = 345
    Width = 1038
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      1038
      41)
    object BtnRegister: TBitBtn
      Left = 403
      Top = 5
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdRegister
      Anchors = [akRight, akBottom]
      Caption = 'Th'#7921'c hi'#7879'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
        DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
        21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
        4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
        AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
        21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
        4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
        FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
        1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
        6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
        B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
        49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
        3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
        E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
        62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
        FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
        E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
        7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
        E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
        E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
        F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
        45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
        8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 539
      Top = 5
      Width = 96
      Height = 32
      Cursor = 1
      Action = CmdClose
      Anchors = [akRight, akBottom]
      Caption = 'K'#7871't th'#250'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
        00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
        78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
        F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
        A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
        7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
        16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
        C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
        7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
        210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
        B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
        82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
        6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
        C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
        85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
        FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
        CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
        88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
        240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
        DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
        78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
        FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
        B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 52
    Top = 196
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrListBeforeOpen
    BeforeDelete = QrListBeforeDelete
    AfterDelete = QrListAfterDelete
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'IMP_HR_DM_NHANVIEN'
      'where TransNo=:TransNo')
    Left = 56
    Top = 156
    object QrListManvQL: TWideStringField
      FieldName = 'ManvQL'
    end
    object QrListTennv: TWideStringField
      FieldName = 'Tennv'
      Size = 200
    end
    object QrListGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrListSheet: TWideStringField
      FieldName = 'Sheet'
      Size = 200
    end
    object QrListErrCode: TWideStringField
      FieldName = 'ErrCode'
      Size = 200
    end
    object QrListManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrListNgayVaoLam: TDateTimeField
      FieldName = 'NgayVaoLam'
    end
    object QrListNgayVaoLamChinhThuc: TDateTimeField
      FieldName = 'NgayVaoLamChinhThuc'
    end
    object QrListNgayThamNien: TDateTimeField
      FieldName = 'NgayThamNien'
    end
    object QrListNgayThoiViec: TDateTimeField
      FieldName = 'NgayThoiViec'
    end
    object QrListTenPhongBan: TWideStringField
      FieldName = 'TenPhongBan'
      Size = 200
    end
    object QrListTenBoPhan: TWideStringField
      FieldName = 'TenBoPhan'
      Size = 200
    end
    object QrListTenChucDanh: TWideStringField
      FieldName = 'TenChucDanh'
      Size = 200
    end
    object QrListTenNhomLV: TWideStringField
      FieldName = 'TenNhomLV'
      Size = 200
    end
    object QrListNgaySinh: TDateTimeField
      FieldName = 'NgaySinh'
    end
    object QrListMaSoThue: TWideStringField
      FieldName = 'MaSoThue'
    end
    object QrListDienThoai: TWideStringField
      FieldName = 'DienThoai'
      Size = 100
    end
    object QrListCuTru_DiaChi: TWideStringField
      FieldName = 'CuTru_DiaChi'
      Size = 200
    end
    object QrListThuongTru_DiaChi: TWideStringField
      FieldName = 'ThuongTru_DiaChi'
      Size = 200
    end
    object QrListEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrListEmailCaNhan: TWideStringField
      FieldName = 'EmailCaNhan'
      Size = 100
    end
    object QrListLuongChinh: TFloatField
      FieldName = 'LuongChinh'
    end
    object QrListLuongBaoHiem: TFloatField
      FieldName = 'LuongBaoHiem'
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 144
    Top = 224
    object CmdRegister: TAction
      Caption = 'Th'#7921'c hi'#7879'n'
      OnExecute = CmdRegisterExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin    '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      ShortCut = 116
      OnExecute = CmdRefreshExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a d'#242'ng'
      OnExecute = CmdDelExecute
    end
    object CmdDelRecordError: TAction
      Caption = 'X'#243'a c'#225'c d'#242'ng '#273'ang l'#7895'i'
      OnExecute = CmdDelRecordErrorExecute
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 172
    Top = 224
    object Tm1: TMenuItem
      Action = CmdSearch
      Visible = False
    end
    object Xadng1: TMenuItem
      Action = CmdDel
      Caption = 'X'#243'a d'#242'ng '#273'ang ch'#7885'n'
    end
    object Xaccdngangli1: TMenuItem
      Action = CmdDelRecordError
    end
  end
  object spIMP_HR_DM_NHANVIEN_InsertData: TADOCommand
    CommandText = 'spIMP_HR_DM_NHANVIEN_InsertData;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pUID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 396
    Top = 84
  end
  object spIMP_HR_DM_NHANVIEN_DeleteList: TADOCommand
    CommandText = 'spIMP_HR_DM_NHANVIEN_DeleteList;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@pTransNo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pIsError'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 404
    Top = 132
  end
  object spIMP_HR_DM_NHANVIEN_Check: TADOCommand
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 404
    Top = 196
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'ManvQL'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 92
    Top = 264
  end
  object CHECK_RECORD_ERROR: TADOCommand
    CommandText = 
      'select 1 from IMP_HR_DM_NHANVIEN where TransNo=:TransNo and isnu' +
      'll(ErrCode, '#39#39') <> '#39#39
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'TransNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 248
    Top = 220
  end
end
