﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit KhenThuongKyLuat;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, rDBComponents, Vcl.Buttons, DBCtrlsEh, DBGridEh,
  DBLookupEh, frameEmp, DbLookupComboboxEh2;

type
  TFrmKhenThuongKyLuat = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    ToolButton15: TToolButton;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    PaThongTin: TisPanel;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterNgayKy: TDateTimeField;
    QrMasterNgayHieuLuc: TDateTimeField;
    QrMasterNgayHetHan: TDateTimeField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    QrMasterLK_TENNV: TWideStringField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrMasterMaMaKThuongKLuat: TWideStringField;
    QrMasterSoQuyetDinh: TWideStringField;
    QrMasterNguoiKy: TWideStringField;
    QrMasterKTKLLoai: TIntegerField;
    QrMasterNoiDung: TWideStringField;
    QrMasterSoTien: TFloatField;
    QrMasterLK_TEN_KThuongKLuat: TWideStringField;
    QrMasterLK_TEN_LOAI_KThuongKLuat: TWideStringField;
    QrMasterCo_ThueTNCN: TBooleanField;
    QrMasterThang: TIntegerField;
    QrMasterNam: TIntegerField;
    QrMasterGhiChu: TWideMemoField;
    RzSizePanel1: TRzSizePanel;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterUPDATE_NAME: TWideStringField;
    CmdLyDoKThuongKLuat: TAction;
    rgObsolete: TRadioGroup;
    Panel1: TPanel;
    Panel2: TPanel;
    QrMasterLK_KThuongKLuatLoai: TIntegerField;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    QrMasterCalc_FileName: TWideStringField;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    QrMasterLK_Co_ThueTNCN: TBooleanField;
    DsEmp: TDataSource;
    gbViecLam: TGroupBox;
    GroupBox1: TGroupBox;
    rDBCheckBox1: TrDBCheckBox;
    Label1: TLabel;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    QrMasterLK_ManvQL: TWideStringField;
    frEmp1: TfrEmp;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    PaNhanVien: TPanel;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    PaInfo: TPanel;
    Label2: TLabel;
    dpNgayHieuLuc: TwwDBDateTimePicker;
    wwDBEdit5: TDBEditEh;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    EdNguoiKy_Manv: TDBEditEh;
    CbLyDo: TDbLookupComboboxEh2;
    EdKTKLLoai: TDBEditEh;
    QrMasterFileIdx: TGuidField;
    BtnDangKy: TToolButton;
    CmdNewList: TAction;
    ToolButton13: TToolButton;
    CHECK_FILE_USED: TADOCommand;
    QrFileContentIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure QrMasterApprovalChange(Sender: TField);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrMasterMaMaKThuongKLuatChange(Sender: TField);
    procedure CmdLyDoKThuongKLuatExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrMasterNgayHieuLucChange(Sender: TField);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterManvChange(Sender: TField);
    procedure QrMasterAfterDelete(DataSet: TDataSet);
    procedure CmdNewListExecute(Sender: TObject);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, mFilter, mType: Integer;
    mSQL, fStr, mEmpID, mEmpIDLabel, mEmpName: String;
    FORM_CODE, REPORT_NAME: String;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmKhenThuongKyLuat: TFrmKhenThuongKyLuat;

const
    TABLE_NAME = 'HR_LICHSU_KTKL';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmLyDoKTKL, KhenThuongKyLuatDangKy,
    KhenThuongKyLuatDanhSach, HrData, HrExCommon; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.FormCreate(Sender: TObject);
begin
    AddAllFields(QrMaster, TABLE_NAME);
    mFilter := 0;
    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);
    FORM_CODE := 'HR_LICHSU_KTKL';
    REPORT_NAME := 'HR_RP_LICHSU_KTKL';
    mType := 0;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);

 
    with HrDataMain do
        OpenDataSets([QrDMNV, QrLYDO_KTKL, QrLOAI_LYDO_KTKL]);

    if mEmpID <> '' then
    begin
        TMyForm(Self).Init1;
        Width := 1260;
        Height := 612;
        Caption := 'Hồ Sơ Nhân Sự - Khen Thưởng Kỷ Luật';
        fStr := mEmpID;
        frD2D.EdFrom.Date := Date;
        frD2D.EdTo.Date := Date;
        frD2D.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
        FORM_CODE := 'HR_LICHSU_NHANVIEN_KTKL';
        REPORT_NAME := 'HR_RP_LICHSU_NHANVIEN_KTKL';
        mType := 1;
        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        gbViecLam.Height := gbViecLam.Height - PaNhanVien.Height;
        PaThongTin.Height := PaThongTin.Height - PaNhanVien.Height;
        ToolButton12.Visible := False;
        BtnDangKy.Visible := False;
    end
    else
    begin
        TMyForm(Self).Init2;
    end;

    SetDisplayFormat(QrMaster, sysCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        Add(' %s ');
        if mEmpID <> '' then
            Add(' and Manv =:Manv')
        else
        begin
           Add(' and ([NgayHieuLuc] between :Tungay and :Denngay)' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayKy], Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

procedure TFrmKhenThuongKyLuat.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrMaster.FieldByName('KThuongKLuatLoai').AsInteger = 2 then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp]);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    if mEmpID <> '' then
        dpNgayHieuLuc.SetFocus
    else
        CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
    begin
        if mEmpID <> '' then
            dpNgayHieuLuc.SetFocus
        else
            CbNhanVien.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdPrint2Execute(Sender: TObject);
var
    n: Integer;
    filename, repname: String;
begin
    n := (Sender as TComponent).Tag;

    repname := 'HR_RP_LICHSU_KTKL_WORD';
    filename := 'DOC\' +'HR_RP_LICHSU_KTKL_WORD_' + IntToStr(n) + '.docx';

    with QrMaster do
        DataOffice.CreateReport2(filename ,
            [sysLogonUID, TGuidEx.ToString(FieldByName('Idx'))], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    if mType <> 0 then
        fStr := mEmpID;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        mType, //0: Lich su, 1: HSNV/Lich Su
        rgObsolete.ItemIndex, //0: Tất cả; 1: Khen thưởng; 2: Kỷ luật
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdRefreshExecute(Sender: TObject);
var
    sSQL, sWhere, s: String;
    fFilter: Integer;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

    fFilter := rgObsolete.ItemIndex;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) or
       (fFilter <> mFilter) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;
        mFilter := fFilter;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;

            sWhere := '';
            if mFilter > 0 then
                sWhere := sWhere + ' and isnull(KThuongKLuatLoai, 0) = '+ IntToStr(mFilter);

            if mEmpID <> '' then
            begin
                SQL.Text := Format(mSQL, [sWhere]);
                Parameters[0].Value := mEmpID;
            end
            else
            begin
                sSQL := '1=1';

                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
                    end;

                SQL.Text := Format(mSQL, [sWhere, sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'NgayHieuLuc';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdNewListExecute(Sender: TObject);
var
    mTransNo: Integer;
begin
    mTransNo := 0;
    Application.CreateForm(TFrmKhenThuongKyLuatDangKy, FrmKhenThuongKyLuatDangKy);
    if not FrmKhenThuongKyLuatDangKy.Execute(r, mTransNo) then
        Exit;

    QrMaster.Requery;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdFileMinusExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdFilePlusExecute(Sender: TObject);
begin
     Attach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdLyDoKThuongKLuatExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_LYDO_KTKL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLyDoKTKL, FrmDmLyDoKTKL);
    FrmDmLyDoKTKL.Execute(r);

    HrDataMain.QrLYDO_KTKL.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bViewFile: Boolean;
begin
    bViewFile := False;
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
        if Active then
            bViewFile := FieldByName('FileExt').AsString <> '';

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdFileMinus.Enabled := bViewFile and not bBrowse;

    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;
    BtnDangKy.Enabled := bBrowse;
    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'NgayHieuLuc', 'SoQuyetDinh', 'NgayKy', 'NguoiKy', 'MaKThuongKLuat']) then
            Abort;

        if FieldByName('SoTien').AsFloat > 0 then
        begin
            if BlankConfirm(QrMaster, ['Thang', 'Nam']) then
                Abort;
        end;
    end;

    SetAudit(DataSet);
end;
 (*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterCalcFields(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterAfterDelete(DataSet: TDataSet);
var
   count: Integer;
begin
    if mFileIdx <> TGUID.Empty then
    begin
        count := 0;
        with CHECK_FILE_USED do
        begin
            Prepared := True;
            Parameters[0].Value := TGuidEx.ToString(mFileIdx);
            count := Execute.RecordCount;
        end;

        if count < 1 then
        begin
            deleteImageByFileIdx(mFileIdx);
            mFileIdx := TGUID.Empty;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
        if mEmpID <> '' then
            FieldByName('Manv').AsString := mEmpID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterApprovalChange(Sender: TField);
begin
    exDotManv(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdSwitchExecute(Sender: TObject);
begin
    if mEmpID <> '' then
    begin
        if ActiveControl = GrList then
            dpNgayHieuLuc.SetFocus;
        GrList.SetFocus;
    end
    else
    begin
        if ActiveControl = GrList then
            CbNhanVien.SetFocus
        else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
            frD2D.EdFrom.SetFocus
        else
            GrList.SetFocus
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterMaMaKThuongKLuatChange(Sender: TField);
var
    s: string;
begin
    s := Sender.AsString;
    with QrMaster do
    begin
        FieldByName('KThuongKLuatLoai').AsInteger := FieldByName('LK_KThuongKLuatLoai').AsInteger;
        FieldByName('Co_ThueTNCN').AsBoolean := FieldByName('LK_Co_ThueTNCN').AsBoolean;
    end;
    EdKTKLLoai.Text := EdKTKLLoai.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterManvChange(Sender: TField);
begin
     EdManvQL.Text := EdManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.QrMasterNgayHieuLucChange(Sender: TField);
begin
    with QrMaster do
    begin
        if FieldByName('NgayKy').AsFloat < 10 then
            FieldByName('NgayKy').AsDateTime := FieldByName('NgayHieuLuc').AsDateTime;
        FieldByName('NgayHetHan').AsDateTime := FieldByName('NgayHieuLuc').AsDateTime;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 500;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrMaster do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.Dettach();
begin
     with QrMaster do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhenThuongKyLuat.ViewAttachment();
var
	s, sn: String;
begin
	with QrMaster do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

end.
