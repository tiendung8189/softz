﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmnvThoiviec;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, wwdbdatetimepicker, ExtCtrls,
  ActnList, ComCtrls, db, wwdblook, DBCtrls, frameEmp, Buttons, isPanel, Mask,
  ToolWin, wwdbedit, DBCtrlsEh;

type
  TFrmDmnvThoiviec = class(TForm)
    PaTerminated: TPanel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label2: TLabel;
    Action: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    CbReason: TwwDBLookupCombo;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    wwDBDateTimePicker4: TwwDBDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdHappening: TDBMemo;
    frEmp: TfrEmp;
    Panel2: TisPanel;
    TntSpeedButton1: TSpeedButton;
    DBEdit8: TDBEditEh;
    DBEdit12: TDBEditEh;
    CmdChecked: TAction;
    Label4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure TntFormCreate(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
  private
  	mCanEdit, mChanged: Boolean;
  public
  	function  Execute(r: WORD; EmpID, EmpIDLabel, EmpName: String): Boolean;
  end;

var
  FrmDmnvThoiviec: TFrmDmnvThoiviec;

implementation

{$R *.DFM}

uses
    Dmnv, Rights, isLib, isMsg,  isDb, ExCommon, MainData, HrData;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmnvThoiviec.Execute;
begin
	mCanEdit := rCanEdit(r);
    mChanged := False;
    frEmp.Initial(EmpID, EmpIDLabel, EmpName);
    ShowModal;
    Result := mChanged;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.FormShow(Sender: TObject);
begin
    // Open Database
    with HrDataMain do
        OpenDataSets([QrLOAI_THOIVIEC]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    FrmDmnv.DsDMNV.AutoEdit := False;
	CloseDataSets([HrDataMain.QrLOAI_THOIVIEC]);
    Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(FrmDmnv.QrDMNV, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.CmdSaveExecute(Sender: TObject);
begin
	FrmDmnv.QrDMNV.Post;
    mChanged := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.CmdCancelExecute(Sender: TObject);
begin
	FrmDmnv.QrDMNV.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.CmdCloseExecute(Sender: TObject);
begin
    Close
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
begin
	b := FrmDmnv.QrDMNV.State in [dsBrowse];
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
//    CmdChecked.Enabled := mCanEdit and FrmProfile.mCanCheck and (FrmProfile.QrEmp.FieldByName('CHECKED3_BY').AsInteger = 0);
	FrmDmnv.DsDMNV.AutoEdit := mCanEdit //and CmdChecked.Enabled;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnvThoiviec.TntFormCreate(Sender: TObject);
begin
    TMyForm(Self).Init();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CHECK_CONFIRM = 'Xác nhận đã kiểm tra dữ liệu cho nhân viên thôi việc. Tiếp tục?';

procedure TFrmDmnvThoiviec.CmdCheckedExecute(Sender: TObject);
begin
//    if not YesNo(RS_CHECK_CONFIRM, 1) then
//        Exit;
//
//    SetEditState(FrmProfile.QrEmp);
//    with FrmProfile.QrEmp do
//    Begin
//        FieldByName('CHECKED3_DATE').AsDateTime := Now;
//        FieldByName('CHECKED3_BY').AsInteger := sysUID;
//    end;
//    CmdSave.Execute;
end;

end.
