(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmNhomlvDetail;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, ActnList, wwdblook, Db, ADODB, StdCtrls, Grids, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBVertGridsEh;

type
  TFrmNhomlvDetail = class(TForm)
    ActionList1: TActionList;
    CmdClose: TAction;
    QrDMCA: TADOQuery;
    grThongTinLamViec: TDBVertGridEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  FrmNhomlvDetail: TFrmNhomlvDetail;

implementation



{$R *.DFM}


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhomlvDetail.FormShow(Sender: TObject);
begin
    QrDMCA.Open;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhomlvDetail.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    QrDMCA.Close;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhomlvDetail.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

end.
