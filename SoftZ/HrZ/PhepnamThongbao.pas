﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PhepnamThongbao;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, wwdblook, Wwdbdlg, wwidlg, wwcheckbox;

type
  TFrmPhepnamThongbao = class(TForm)
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    LbText: TLabel;
    CkbCheck: TwwCheckBox;
    procedure CmdReturnKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
  public
    function Execute(var pCheck: Boolean): Boolean;
  end;

var
  FrmPhepnamThongbao: TFrmPhepnamThongbao;

implementation

{$R *.DFM}

uses
	isDb, isLib, SysUtils, ExCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmPhepnamThongbao.Execute;

begin
    if ShowModal = mrOK then
    begin
    	pCheck := CkbCheck.Checked;
    	Result := True;
	end
    else
    	Result := False;

    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamThongbao.CmdReturnClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamThongbao.CmdReturnKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamThongbao.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
    CkbCheck.Checked := False;
end;

end.
