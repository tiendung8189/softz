(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameD2D;

interface

uses
  Windows, Classes, Controls, Forms,
  fctreecombo, StdCtrls, wwdbdatetimepicker, ExtCtrls,
  ActnList, fcCombo;

type
  TfrD2D = class(TFrame)
    Panel1: TPanel;
    Label65: TLabel;
    Label66: TLabel;
    LbOrg: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    CbOrg: TfcTreeCombo;
    procedure CbOrgChange(Sender: TObject);
    procedure CbOrgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CbOrgCloseUp(Sender: TObject; Select: Boolean);
    procedure Panel1Exit(Sender: TObject);
  private
  	FAction: TAction;
  public
  	procedure Initial(d1, d2: TDateTime; ac: TAction);
  end;

implementation

uses
	ExCommon, isCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrD2D.Initial;
begin
    EdTo.Date := d1;
    EdFrom.Date := d2;
    FAction := ac;
    if sysIsDataAccess then
        FlexOrgComboDataAccess(CbOrg)
    else
      	FlexOrgCombo(CbOrg);
    CbOrg.OnChange(CbOrg);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrD2D.Panel1Exit(Sender: TObject);
begin
    if EdTo.DateTime < EdFrom.DateTime then
    begin
        ErrMsg(RS_PERIOD);
        EdFrom.SelectAll;
        EdFrom.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrD2D.CbOrgChange(Sender: TObject);
begin
    LbOrg.Caption := exGetFlexOrgDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrD2D.CbOrgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearOrg}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrD2D.CbOrgCloseUp(Sender: TObject; Select: Boolean);
begin
	if Assigned(FAction) then
	    if Select then
    	    FAction.Execute;
end;

end.
