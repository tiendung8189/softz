﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevRAW;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls, Db,
  Wwdbgrid2, Wwdbcomb, ADODb, Menus, AdvMenus,
  AppEvnts, wwdblook,
  wwfltdlg, wwFltDlg2, wwDialog, Mask, wwdbedit, Wwdotdot, ToolWin, Grids,
  Wwdbigrd, Wwdbgrid, wwdbdatetimepicker;

type
  TFrmDevRAW = class(TForm)
    ActionList: TActionList;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton11: TToolButton;
    QrList: TADOQuery;
    DsDMList: TDataSource;
    Filter: TwwFilterDialog2;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    Hinttc1: TMenuItem;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Panel1: TPanel;
    LbKHO: TLabel;
    CbTenKho: TwwDBLookupCombo;
    CbMaKho: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    DsDMKHO: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    EdTu: TwwDBDateTimePicker;
    EdDen: TwwDBDateTimePicker;
    CmdRefesh: TAction;
    QrDMKHOLOC: TWideStringField;
    QrDMKHOTEN: TWideStringField;
    QrListMANV: TWideStringField;
    QrListTENNV: TWideStringField;
    QrListLOC: TWideStringField;
    QrListTENLOC: TWideStringField;
    QrListNGAYCC: TDateTimeField;
    QrListDULIEU_THO: TWideMemoField;
    GrList: TwwDBGrid2;
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrListPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure CbPrinterNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure GrListDblClick(Sender: TObject);
  private
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDevRAW: TFrmDevRAW;

implementation

uses
	MainData, isDb, isMsg, ExCommon, Rights, isLib, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.Execute(r: WORD);
begin
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.FormCreate(Sender: TObject);
begin
//	TMyForm(Self).Init1('_IDI_CASHIER');
    SetCustomGrid('HR_DULIEU_CONGTHO', GrList);
    SetDictionary(QrList, 'HR_DULIEU_CONGTHO');
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDMKHO]);
    CbMaKho.LookupValue := sysLoc;
    CbTenKho.LookupValue := sysLoc;

    EdDen.Date := Date;
    EdTu.Date := Date - 30;
    CmdRefesh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.GrListDblClick(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDMList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.CmdRefeshExecute(Sender: TObject);
var
    tu, den: TDateTime;
begin
    tu := EdTu.Date;
    den := EdDen.Date;

    with QrList do
    begin
        if Active then
            Close;

        Parameters[0].Value := tu;
        Parameters[1].Value := den;

        Open;

        Filter := Format('LOC=''%s''', [CbMaKho.Value]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrList, Filter, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrList, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.QrListPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDevRAW.CbPrinterNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevRAW.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := RecordCount(QrList);
end;

end.
