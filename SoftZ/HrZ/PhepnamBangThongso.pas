﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PhepnamBangThongso;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid, ADODb,
  AppEvnts, Menus, AdvMenus, wwDBGrid2, DBCtrls, Grids, Wwdbigrd, ToolWin;

type
  TFrmPhepnamBangThongso = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    Ds1: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Qr1: TADOQuery;
    ToolButton8: TToolButton;
    GrList: TwwDBGrid2;
    PopCommon: TAdvPopupMenu;
    CmdCountWD: TAction;
    Tnhsngycngthct1: TMenuItem;
    COUNT_WD: TADOCommand;
    CmdReload: TAction;
    PaPsComment: TPanel;
    PDComment: TGroupBox;
    EdComment: TDBMemo;
    Qr1Nam: TIntegerField;
    Qr1GhiChu: TWideStringField;
    Qr1CREATE_BY: TIntegerField;
    Qr1UPDATE_BY: TIntegerField;
    Qr1CREATE_DATE: TDateTimeField;
    Qr1UPDATE_DATE: TDateTimeField;
    Qr1PhepNam_NamSau_ToiDa: TFloatField;
    Qr1PhepNam_NamTruoc_ThoiHan: TFloatField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Qr1BeforePost(DataSet: TDataSet);
    procedure Qr1BeforeDelete(DataSet: TDataSet);
    procedure Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure Qr1BeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Qr1AfterInsert(DataSet: TDataSet);
    procedure CmdReloadExecute(Sender: TObject);
    procedure Qr1BeforeEdit(DataSet: TDataSet);
  private
    mYear: Integer;
  	mCanEdit, mTrigger: Boolean;
    sFormat: string;
    fPhepNamToiDa, fPhepNamThoiHan: Double;
  public
  	procedure Execute(r: WORD; pYear: Integer = 0);
  end;

var
  FrmPhepnamBangThongso: TFrmPhepnamBangThongso;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, Variants, MainData, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Execute;
begin
	mCanEdit := rCanEdit(r);
    mYear  := pYear;
    SetCustomGrid('HR_PHEPNAM_THONGSO', GrList);
    EdComment.ReadOnly := not mCanEdit;
	ShowModal;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdNewExecute(Sender: TObject);
begin
	with Qr1 do
    begin
    	Append;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdSaveExecute(Sender: TObject);
begin
	Qr1.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdCancelExecute(Sender: TObject);
begin
	Qr1.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdDelExecute(Sender: TObject);
begin
	Qr1.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([Qr1]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    mTrigger := False;
    AddAllFields(Qr1, 'HR_PHEPNAM_THONGSO', -1);
    sFormat := '#,##0;-#,##0;0';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    OpenDataSets([Qr1]);

    with Qr1 do
    begin
	    SetDisplayFormat(Qr1, sysCurFmt);
    	SetDisplayFormat(Qr1, ['PhepNam_NamSau_ToiDa', 'PhepNam_NamTruoc_ThoiHan'], sFormat);
    end;

    SetDictionary(Qr1, 'HR_PHEPNAM_THONGSO');
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, Qr1, mCanEdit);
    CmdCountWD.Enabled := (not Qr1.IsEmpty) and mCanEdit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(Qr1, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Qr1BeforePost(DataSet: TDataSet);
begin
    with Qr1 do
    begin
       if BlankConfirm(DataSet, ['Nam']) then
		    Abort;

       if (FieldByName('Nam').AsFloat < 1899)  then
       begin
           ErrMsg(RS_YEAR_INVALID);
           Abort;
       end;

       if (FieldByName('PhepNam_NamTruoc_ThoiHan').AsFloat < 0)
          or (FieldByName('PhepNam_NamTruoc_ThoiHan').AsFloat > 12)  then
       begin
           ErrMsg('Số tháng dùng phép năm của năm trước <= 12');
           Abort;
       end;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Qr1BeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;


	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Qr1BeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Qr1PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.Qr1BeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    with Qr1 do
    begin
        if IsEmpty then
        begin
            mYear  := sysYear;
        end
        else
        begin
            Last;
    	    mYear := FieldByName('Nam').AsInteger;
            fPhepNamToiDa := FieldByName('PhepNam_NamSau_ToiDa').AsFloat;
            fPhepNamThoiHan := FieldByName('PhepNam_NamTruoc_ThoiHan').AsFloat;
		end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(Qr1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmPhepnamBangThongso.Qr1AfterInsert(DataSet: TDataSet);
begin
    with Qr1 do
    begin
        FieldByName('Nam').AsInteger := mYear + 1;
        FieldByName('PhepNam_NamSau_ToiDa').AsFloat := fPhepNamToiDa;
        FieldByName('PhepNam_NamTruoc_ThoiHan').AsFloat := fPhepNamThoiHan;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhepnamBangThongso.CmdReloadExecute(Sender: TObject);
begin
    Qr1.Requery;
end;

end.

