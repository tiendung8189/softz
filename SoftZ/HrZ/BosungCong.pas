(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BosungCong;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, DBCtrls, ActnList, Db, ExtCtrls, Grids, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, StdCtrls, wwdblook, RzPanel, RzDBNav;

type
  TFrmBosungCong = class(TForm)
    Inspect: TwwDataInspector;
    Action: TActionList;
    CmdNext: TAction;
    CmdPrior: TAction;
    CbLeave: TwwDBLookupCombo;
    CbLeave2: TwwDBLookupCombo;
    CbLeaveAmout: TwwDBComboBox;
    CbLeaveAmout2: TwwDBComboBox;
    DBNavigator1: TRzDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdNextExecute(Sender: TObject);
    procedure CmdPriorExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure TntFormCreate(Sender: TObject);
  private
  	mDataSource: TDataSource;
  public
  	procedure Execute(pDataSource: TDataSource; pStr: String);
  end;

var
  FrmBosungCong: TFrmBosungCong;

implementation

uses
	ExCommon, isLib, MainData, HrData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	mDataSource.DataSet.CheckBrowseMode;
	Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.CmdNextExecute(Sender: TObject);
begin
	mDataSource.DataSet.Next;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.CmdPriorExecute(Sender: TObject);
begin
	mDataSource.DataSet.Prior;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	CmdNext.Enabled  := not mDataSource.DataSet.Eof;
	CmdPrior.Enabled := not mDataSource.DataSet.Bof;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.Execute;
var
	n, h: Integer;
begin
	mDataSource := pDataSource;
    Inspect.DataSource := pDataSource;
    DBNavigator1.DataSource := pDataSource;

    CbLeave.LookupTable := pDataSource.DataSet.FieldByName('LK_TENVANGMAT').LookupDataSet;
    CbLeave2.LookupTable := pDataSource.DataSet.FieldByName('LK_TENVANGMAT1').LookupDataSet;

	n := exConfigInspector(Inspect, pStr);
    if n < 12 then
    	with Inspect do
        begin
            h := Height;
            Height := Trunc(n * (h - 4) / 12.0) + 4;
            ApplySettings;
            Self.Height := Self.Height + Height - h;
        end;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBosungCong.TntFormCreate(Sender: TObject);
begin
	TMyForm(Self).Init
end;

end.
