object FrmDmnv: TFrmDmnv
  Left = 117
  Top = 93
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch Nh'#226'n Vi'#234'n'
  ClientHeight = 771
  ClientWidth = 1006
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1006
    Height = 44
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 60
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton12: TToolButton
      Left = 128
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 136
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton10: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnChitiet: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Caption = 'Chi ti'#7871't'
      DropdownMenu = PopMore
      ImageIndex = 8
      Style = tbsDropDown
      OnClick = BtnChitietClick
    end
    object ToolButton14: TToolButton
      Left = 475
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 483
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 750
    Width = 1006
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 560
    Top = 44
    Width = 446
    Height = 706
    Align = alRight
    HotSpotHighlight = 11855600
    HotSpotVisible = True
    LockBar = True
    SizeBarWidth = 7
    TabOrder = 2
    object PaDetail: TScrollBox
      Left = 8
      Top = 0
      Width = 438
      Height = 706
      Align = alClient
      TabOrder = 0
      object PD1: TisPanel
        Left = 0
        Top = 0
        Width = 434
        Height = 321
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin c'#225' nh'#226'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label7: TLabel
          Left = 39
          Top = 102
          Width = 55
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y sinh'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 257
          Top = 102
          Width = 46
          Height = 16
          Alignment = taRightJustify
          Caption = 'Gi'#7899'i t'#237'nh'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 41
          Top = 150
          Width = 53
          Height = 16
          Alignment = taRightJustify
          Caption = 'Qu'#7889'c t'#7883'ch'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 261
          Top = 150
          Width = 43
          Height = 16
          Alignment = taRightJustify
          Caption = 'D'#226'n t'#7897'c'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 44
          Top = 174
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'T'#244'n gi'#225'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 257
          Top = 174
          Width = 47
          Height = 16
          Alignment = taRightJustify
          Caption = 'V'#259'n h'#243'a'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdTen: TDBEditEh
          Tag = 1
          Left = 101
          Top = 50
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 37
          ControlLabel.Height = 16
          ControlLabel.Caption = 'H'#7885' t'#234'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Tennv'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object DBEdit2: TDBEditEh
          Left = 101
          Top = 194
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 86
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'/C th'#432#7901'ng tr'#250
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'ThuongTru_DiaChi'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 12
          Visible = True
        end
        object DBEdit3: TDBEditEh
          Left = 101
          Top = 122
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 45
          ControlLabel.Height = 16
          ControlLabel.Caption = 'N'#417'i sinh'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'NoiSinh_DiaChi'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
        object DBEdit5: TDBEditEh
          Left = 101
          Top = 242
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 34
          ControlLabel.Height = 16
          ControlLabel.Caption = 'CMND'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'CCCD'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 14
          Visible = True
        end
        object EdMa: TDBEditEh
          Tag = 1
          Left = 101
          Top = 26
          Width = 140
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          CharCase = ecUpperCase
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' nh'#226'n vi'#234'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Manv'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object wwDBEdit1: TDBEditEh
          Left = 337
          Top = 26
          Width = 73
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 83
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' ch'#7845'm c'#244'ng'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MaCC'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object wwDBEdit2: TDBEditEh
          Left = 101
          Top = 218
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 68
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'/C t'#7841'm tr'#250
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'CuTru_DiaChi'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 13
          Visible = True
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 101
          Top = 98
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgaySinh'
          DataSource = DsDMNV
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object CbSession: TwwDBLookupCombo
          Left = 309
          Top = 98
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
          DataField = 'GioiTinh'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrLOAI_GIOITINH
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 6
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
        end
        object wwDBEdit3: TDBEditEh
          Left = 101
          Top = 74
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 42
          ControlLabel.Height = 16
          ControlLabel.Caption = 'B'#237' danh'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'BiDanh'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object wwDBLookupCombo4: TwwDBLookupCombo
          Left = 100
          Top = 146
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
          DataField = 'QuocTich'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrV_QUOCTICH
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 8
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
        end
        object wwDBLookupCombo5: TwwDBLookupCombo
          Left = 309
          Top = 146
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
          DataField = 'DanToc'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrV_DANTOC
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 9
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
        end
        object wwDBLookupCombo6: TwwDBLookupCombo
          Left = 101
          Top = 170
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
          DataField = 'TonGiao'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrV_TONGIAO
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 10
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
        end
        object wwDBLookupCombo7: TwwDBLookupCombo
          Left = 309
          Top = 170
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
          DataField = 'VanHoa'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrV_VANHOA
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 11
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = False
          ShowMatchText = True
        end
        object wwDBEdit4: TDBEditEh
          Left = 101
          Top = 266
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 58
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'i'#7879'n tho'#7841'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DienThoai'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 15
          Visible = True
        end
        object wwDBEdit5: TDBEditEh
          Left = 101
          Top = 290
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 31
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Email'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Email'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 16
          Visible = True
        end
      end
      object PD5: TisPanel
        Left = 0
        Top = 704
        Width = 434
        Height = 2
        Align = alClient
        Color = 16119285
        ParentBackground = False
        TabOrder = 3
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGhichu: TDBMemo
          Left = 1
          Top = 17
          Width = 432
          Height = 6
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsDMNV
          TabOrder = 1
        end
      end
      object PD2: TisPanel
        Left = 0
        Top = 498
        Width = 434
        Height = 96
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: L'#432#417'ng'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object RemuInspect: TwwDataInspector
          Left = 1
          Top = 17
          Width = 432
          Height = 78
          DisableThemes = False
          Align = alClient
          BorderStyle = bsNone
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          DataSource = DsDMNV
          Items = <
            item
              DataSource = DsDMNV
              DataField = 'LuongChinh'
              Caption = 'L'#432#417'ng c'#417' b'#7843'n'
              WordWrap = False
            end
            item
              DataSource = DsDMNV
              DataField = 'Co_BHXH'
              Caption = 'Tham gia B'#7843'o hi'#7875'm'
              PickList.Items.Strings = (
                'True'
                'False')
              PickList.DisplayAsCheckbox = True
              WordWrap = False
            end>
          DefaultRowHeight = 20
          CaptionWidth = 227
          Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
          PaintOptions.AlternatingRowColor = 15794175
          PaintOptions.BackgroundOptions = [coFillDataCells]
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clBlack
          CaptionFont.Height = -13
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = []
          LineStyleCaption = ovDottedLine
          LineStyleData = ovDottedLine
        end
      end
      object PD4: TisPanel
        Left = 0
        Top = 594
        Width = 434
        Height = 110
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        Visible = False
        HeaderCaption = ' .: T'#224'i kho'#7843'n giao d'#7883'ch'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 126
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object SpeedButton2: TSpeedButton
          Left = 387
          Top = 24
          Width = 23
          Height = 22
          Cursor = 1
          Hint = 'T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            00001220133149824CCC192F1A4B000000000000000000000000000000000000
            00000D391D88186634F2196B37FF186634F20D391D8800000000000000000203
            0205457548AF5FA564FD6BAE6FFF4B954FFB112012340A140B22000000000D37
            1C84288C53FF64BA8DFF95D2B2FF64BA8DFF288C53FF0E3B1E8C00000000111B
            12277BB97FFCB7DEBBFF67AC6CFF75B67AFF4E9751FE3C713FB82649287A1A69
            37F762BA8BFF60BA87FFFFFFFFFF60B987FF67BC8FFF186835F700000000476F
            4A9B9CCDA0FF6FB273FF8DC792FFAADCAFFF76B67BFF519B55FF77B77BFF317B
            4CFF9CD4B6FFFFFFFFFFFFFFFFFFFFFFFFFF95D2B2FF196B37FF0C130D197BBB
            80FF77B77CFF91CB97FFABDEB1FF9CD7A2FFAADDB0FF77B77CFF60AC65FF478A
            60FF90D3B1FF92D6B1FFFFFFFFFF65BC8CFF67BC8FFF186835F7507753A07FBD
            84FF97CE9CFFADDFB3FF6FB374FF96D59DFF9DD8A3FFAADDB0FF78B87CFF61A4
            6FFF61AB81FF95D4B4FFBAE6D0FF6ABB8FFF2D8F57FF144824A37BBB80FF8EC8
            93FFAFDFB5FFA1DAA7FF98D79FFF97D69EFF7EC083FF82C187FFABDDB0FF79B9
            7DFF5EA16BFF5E9873FF4F8E66FF46895EFF3F894EFF050A05114563487D7DBB
            82FF8FC894FFB0E0B6FFA2DAA8FF7FC185FFA4D0A7FFDDEEDFFF80B883FFABDE
            B1FF7AB97FFF569F5AFFC4E7C8FF78B87CFF284F2A840000000000000000151D
            15257BB87FFA90C995FFB0E0B6FF85C28AFFF7FCF8FF95C297FFDDEEDFFF82C2
            87FFABDEB1FF7BBA7FFF58A05CFF59A15DFF0102010400000000000000000000
            0000000000007AB67EF691CA96FFB1E0B6FFD9F3DDFFF7FCF8FFA4D0A7FF7EC0
            84FF9FD9A5FFACDEB2FF7BBB80FF559D59FB0000000000000000000000000000
            000000000000000000007AB780F592CB97FFB1E1B6FF85C38AFF80C185FF99D7
            A0FF98D79FFF9FD9A5FFACDFB2FF7DBB81FF559A59F600000000000000000000
            00000000000000000000000000007CB881F593CC98FFB1E1B7FFA3DBA9FF9BD8
            A2FF73B477FFAFDFB4FF87C38CFF65AA69FF0509050E00000000000000000000
            0000000000000000000000000000000000007DB882F594CC99FFB2E2B7FFA3DC
            AAFFB0E0B6FF8CC692FF6EB173FF0609060E0000000000000000000000000000
            000000000000000000000000000000000000000000007EB983F594CD9AFFB3E2
            B7FF93CB98FF77B77CFF060A060E000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000007FBA85F596CD
            9BFF80BE85FF070A070E00000000000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000000000000081BB
            86F5070B070E0000000000000000000000000000000000000000}
          OnClick = CmdDmtkExecute
        end
        object DBEdit7: TDBEditEh
          Left = 101
          Top = 24
          Width = 280
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 71
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' t'#224'i kho'#7843'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MATK'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object DBEdit8: TDBEditEh
          Left = 101
          Top = 48
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 61
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_TENNH'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object DBEdit9: TDBEditEh
          Left = 101
          Top = 72
          Width = 309
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 57
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Chi nh'#225'nh'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_TENNH_CN'
          DataSource = DsDMNV
          DynProps = <>
          EditButtons = <>
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
      end
      object PaPhepnam: TisPanel
        Left = 0
        Top = 425
        Width = 434
        Height = 73
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 4
        HeaderCaption = ' .: Th'#244'ng tin vi'#7879'c l'#224'm'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 126
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label18: TLabel
          Left = 365
          Top = 20
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = '(ng'#224'y)'
          FocusControl = DBEdit3
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 17
          Top = 23
          Width = 77
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y v'#224'o l'#224'm'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 5
          Top = 47
          Width = 89
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y th'#226'm ni'#234'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 221
          Top = 47
          Width = 82
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y ngh'#7881' vi'#7879'c'
          FocusControl = DBEdit5
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdPhepnam: TDBNumberEditEh
          Left = 309
          Top = 17
          Width = 50
          Height = 22
          ControlLabel.Width = 57
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ph'#233'p n'#259'm'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'SoNgayPhepNam'
          DataSource = DsDMNV
          DecimalPlaces = 1
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object wwDBDateTimePicker2: TwwDBDateTimePicker
          Left = 100
          Top = 17
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayVaoLam'
          DataSource = DsDMNV
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 1
        end
        object wwDBDateTimePicker3: TwwDBDateTimePicker
          Left = 100
          Top = 41
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgayThamNien'
          DataSource = DsDMNV
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 3
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 309
          Top = 41
          Width = 101
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'NgayThoiViec'
          DataSource = DsDMNV
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 4
        end
      end
      object isPanel1: TisPanel
        Left = 0
        Top = 321
        Width = 434
        Height = 104
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 5
        HeaderCaption = ' .: Th'#244'ng tin t'#7893' ch'#7913'c'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 126
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label1: TLabel
          Left = 33
          Top = 50
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ch'#7913'c danh'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 10
          Top = 74
          Width = 84
          Height = 16
          Alignment = taRightJustify
          Caption = 'Nh'#243'm l'#224'm vi'#7879'c'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 34
          Top = 24
          Width = 60
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ph'#242'ng ban'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbChucvu: TwwDBLookupCombo
          Left = 101
          Top = 46
          Width = 220
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TenChucDanh'#9'30'#9'TenChucDanh'#9'F'
            'MaChucDanh'#9'8'#9'MaChucDanh'#9'F')
          DataField = 'MaChucDanh'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDM_CHUCDANH
          LookupField = 'MaChucDanh'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo1: TwwDBLookupCombo
          Left = 101
          Top = 70
          Width = 220
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TenNhomLV'#9'30'#9'TenNhomLV'#9'F'
            'MaNhomLV'#9'8'#9'MaNhomLV'#9'F')
          DataField = 'MaNhomLV'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDM_NHOMLV
          LookupField = 'MaNhomLV'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 5
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo11: TwwDBLookupCombo
          Left = 101
          Top = 22
          Width = 220
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TenPhongBan'#9'30'#9'TENNXB'#9'F'
            'MaPhongBan'#9'8'#9'MANXB'#9'F')
          DataField = 'MaPhongBan'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDMPHONGBAN
          LookupField = 'MaPhongBan'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo12: TwwDBLookupCombo
          Left = 324
          Top = 22
          Width = 86
          Height = 22
          TabStop = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'MaPhongBan'#9'8'#9'MANXB'#9'F'
            'TenPhongBan'#9'30'#9'TENNXB'#9'F')
          DataField = 'MaPhongBan'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDMPHONGBAN
          LookupField = 'MaPhongBan'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo2: TwwDBLookupCombo
          Left = 324
          Top = 46
          Width = 86
          Height = 22
          TabStop = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'MaChucDanh'#9'8'#9'MaChucDanh'#9'F'
            'TenChucDanh'#9'30'#9'TenChucDanh'#9'F')
          DataField = 'MaChucDanh'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDM_CHUCDANH
          LookupField = 'MaChucDanh'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo3: TwwDBLookupCombo
          Left = 324
          Top = 70
          Width = 86
          Height = 22
          TabStop = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'MaNhomLV'#9'8'#9'MaNhomLV'#9'F'
            'TenNhomLV'#9'30'#9'TenNhomLV'#9'F')
          DataField = 'MaNhomLV'
          DataSource = DsDMNV
          LookupTable = HrDataMain.QrDM_NHOMLV
          LookupField = 'MaNhomLV'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 6
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
      end
    end
  end
  object PaList: TPanel
    Left = 0
    Top = 44
    Width = 560
    Height = 706
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 560
      Height = 706
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'Manv'#9'15'#9'M'#227' s'#7889#9'F'
        'NoiSinh_DiaChi'#9'30'#9#272#7883'a ch'#7881#9'F'
        'GhiChu'#9'30'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsDMNV
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      PaintOptions.AlternatingRowColor = 16119285
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 64
    Top = 272
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      OnExecute = CmdEditExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Hint = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdContact: TAction
      Caption = 'Li'#234'n l'#7841'c'
      Hint = 'Danh s'#225'ch ng'#432#7901'i li'#234'n l'#7841'c'
    end
    object CmdReload: TAction
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdDmtk: TAction
      Caption = 'CmdDmtk'
      OnExecute = CmdDmtkExecute
    end
    object CmdThoiviec: TAction
      Caption = 'Th'#244'i vi'#7879'c'
      Hint = '@HR_NV_NGHIVIEC'
      OnExecute = CmdThoiviecExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDMNV
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'Manv'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MADT'
      'TENDT'
      'MST'
      'DCHI'
      'DTHOAI'
      'FAX'
      'TENTK'
      'MATK'
      'TEN_NGANHANG'
      'DC_NGANHANG')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 116
    Top = 272
  end
  object QrDMNV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDMNVBeforeInsert
    AfterInsert = QrDMNVAfterInsert
    BeforePost = QrDMNVBeforePost
    AfterPost = QrDMNVAfterPost
    BeforeDelete = QrDMNVBeforeDelete
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HR_DM_NHANVIEN')
    Left = 40
    Top = 112
    object QrDMNVMANV: TWideStringField
      DisplayLabel = 'M'#227' s'#7889
      FieldName = 'Manv'
      OnChange = QrDMNVMANVChange
    end
    object QrDMNVTENNV: TWideStringField
      DisplayLabel = 'H'#7885' t'#234'n'
      FieldName = 'Tennv'
      Size = 200
    end
    object QrDMNVDTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'DienThoai'
      Size = 100
    end
    object QrDMNVEMAIL: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrDMNVCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMNVUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMNVCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMNVUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMNVLK_MANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH'
      LookupDataSet = HrDataMain.QrDMTK_HR
      LookupKeyFields = 'MATK'
      LookupResultField = 'MANH'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_MACN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MANH_CN'
      LookupDataSet = HrDataMain.QrDMTK_HR
      LookupKeyFields = 'MATK'
      LookupResultField = 'MANH_CN'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TENNH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNH'
      LookupDataSet = DataMain.QrNganhang
      LookupKeyFields = 'MANH'
      LookupResultField = 'TENNH'
      KeyFields = 'LK_MANH'
      Size = 200
      Lookup = True
    end
    object QrDMNVLK_TENCN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNH_CN'
      LookupDataSet = DataMain.QrNganhangCN
      LookupKeyFields = 'MANH_CN'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'LK_MANH_CN'
      Size = 200
      Lookup = True
    end
    object QrDMNVMACC: TWideStringField
      FieldName = 'MaCC'
      Size = 15
    end
    object QrDMNVPTTT: TWideStringField
      FieldName = 'PTTT'
    end
    object QrDMNVNAMSINH: TIntegerField
      FieldName = 'NAMSINH'
    end
    object QrDMNVMaChiNhanh: TWideStringField
      FieldName = 'MaChiNhanh'
    end
    object QrDMNVMaPhongBan: TWideStringField
      FieldName = 'MaPhongBan'
      Size = 40
    end
    object QrDMNVMaBoPhan: TWideStringField
      FieldName = 'MaBoPhan'
      Size = 60
    end
    object QrDMNVNoiSinh_MaTinh: TWideStringField
      FieldName = 'NoiSinh_MaTinh'
      Size = 11
    end
    object QrDMNVNoiSinh_DiaChi: TWideStringField
      FieldName = 'NoiSinh_DiaChi'
      Size = 200
    end
    object QrDMNVCCCD: TWideStringField
      FieldName = 'CCCD'
    end
    object QrDMNVCCCD_NgayCap: TDateTimeField
      FieldName = 'CCCD_NgayCap'
    end
    object QrDMNVCCCD_NoiCap: TWideStringField
      FieldName = 'CCCD_NoiCap'
      Size = 70
    end
    object QrDMNVCCCD_GhiChu: TWideStringField
      FieldName = 'CCCD_GhiChu'
      Size = 200
    end
    object QrDMNVMaSoThue: TWideStringField
      FieldName = 'MaSoThue'
    end
    object QrDMNVMaNoiLV: TWideStringField
      FieldName = 'MaNoiLV'
    end
    object QrDMNVCuTru_DiaChi: TWideStringField
      FieldName = 'CuTru_DiaChi'
      Size = 200
    end
    object QrDMNVThuongTru_DiaChi: TWideStringField
      FieldName = 'ThuongTru_DiaChi'
      Size = 200
    end
    object QrDMNVEmailCaNhan: TWideStringField
      FieldName = 'EmailCaNhan'
      Size = 100
    end
    object QrDMNVNgayVaoLam: TDateTimeField
      FieldName = 'NgayVaoLam'
      OnChange = QrDMNVNGAY_VAOLAMChange
    end
    object QrDMNVNgayThamNien: TDateTimeField
      FieldName = 'NgayThamNien'
    end
    object QrDMNVLuongChinh: TFloatField
      FieldName = 'LuongChinh'
    end
    object QrDMNVLuongBaoHiem: TFloatField
      FieldName = 'LuongBaoHiem'
    end
    object QrDMNVCo_BangCong: TBooleanField
      FieldName = 'Co_BangCong'
    end
    object QrDMNVCo_BangLuong: TBooleanField
      FieldName = 'Co_BangLuong'
    end
    object QrDMNVCo_BHXH: TBooleanField
      FieldName = 'Co_BHXH'
    end
    object QrDMNVTinhTrang: TIntegerField
      FieldName = 'TinhTrang'
    end
    object QrDMNVNgayThoiViec: TDateTimeField
      FieldName = 'NgayThoiViec'
    end
    object QrDMNVNgayTraTaiSan: TDateTimeField
      FieldName = 'NgayTraTaiSan'
    end
    object QrDMNVNgayQuyetToan: TDateTimeField
      FieldName = 'NgayQuyetToan'
    end
    object QrDMNVBiDanh: TWideStringField
      FieldName = 'BiDanh'
      Size = 200
    end
    object QrDMNVNgaySinh: TDateTimeField
      FieldName = 'NgaySinh'
    end
    object QrDMNVGioiTinh: TIntegerField
      FieldName = 'GioiTinh'
    end
    object QrDMNVQuocTich: TWideStringField
      FieldName = 'QuocTich'
    end
    object QrDMNVDanToc: TWideStringField
      FieldName = 'DanToc'
    end
    object QrDMNVTonGiao: TWideStringField
      FieldName = 'TonGiao'
    end
    object QrDMNVVanHoa: TWideStringField
      FieldName = 'VanHoa'
    end
    object QrDMNVMaChucDanh: TWideStringField
      FieldName = 'MaChucDanh'
    end
    object QrDMNVMaNhomLV: TWideStringField
      FieldName = 'MaNhomLV'
    end
    object QrDMNVSoNgay_PhepNam: TFloatField
      FieldName = 'SoNgayPhepNam'
    end
    object QrDMNVMATK: TWideStringField
      FieldName = 'MATK'
    end
    object QrDMNVGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrDMNVGhiChu_ThoiViec: TWideStringField
      FieldName = 'GhiChu_ThoiViec'
      Size = 200
    end
    object QrDMNVMaQTLV_ThoiViec: TWideStringField
      FieldName = 'MaQTLV_ThoiViec'
    end
    object QrDMNVNgayDangKyThoiViec: TDateTimeField
      FieldName = 'NgayDangKyThoiViec'
    end
  end
  object DsDMNV: TDataSource
    AutoEdit = False
    DataSet = QrDMNV
    Left = 40
    Top = 140
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 272
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 144
    Top = 272
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Khngspxp2: TMenuItem
      Action = CmdClearFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mnObsolete: TMenuItem
      Caption = 'Ngh'#7881' vi'#7879'c'
      Checked = True
      OnClick = mnObsoleteClick
    end
  end
  object ALLOC_MANV: TADOCommand
    CommandText = 'ALLOC_MANV;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LEN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CODE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 80
    Top = 112
  end
  object PopMore: TAdvPopupMenu
    AutoHotkeys = maManual
    MenuAnimation = [maRightToLeft]
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 228
    Top = 272
    object hivic1: TMenuItem
      Action = CmdThoiviec
    end
  end
end
