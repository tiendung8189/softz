object FrmDmnvThoiviec: TFrmDmnvThoiviec
  Left = 249
  Top = 197
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Th'#244'i Vi'#7879'c'
  ClientHeight = 338
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = TntFormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaTerminated: TPanel
    Left = 0
    Top = 36
    Width = 537
    Height = 246
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 60
      Top = 40
      Width = 30
      Height = 16
      Caption = 'L'#253' do'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 287
      Top = 64
      Width = 129
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y l'#224'm '#273#417'n th'#244'i vi'#7879'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 326
      Top = 88
      Width = 90
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y tr'#7843' t'#224'i s'#7843'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 288
      Top = 112
      Width = 128
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y quy'#7871't to'#225'n l'#432#417'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 48
      Top = 132
      Width = 42
      Height = 16
      Caption = 'Ghi ch'#250
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 11
      Top = 16
      Width = 79
      Height = 16
      Caption = 'Ng'#224'y th'#244'i vi'#7879'c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object wwDBDateTimePicker1: TwwDBDateTimePicker
      Left = 97
      Top = 12
      Width = 101
      Height = 22
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayThoiViec'
      DataSource = FrmDmnv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
    object CbReason: TwwDBLookupCombo
      Left = 97
      Top = 36
      Width = 429
      Height = 22
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TenQTLV'#9'40'#9'TenQTLV'#9'F')
      DataField = 'MaQTLV_ThoiViec'
      DataSource = FrmDmnv.DsDMNV
      LookupTable = HrDataMain.QrLOAI_THOIVIEC
      LookupField = 'MaQTLV'
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
    end
    object wwDBDateTimePicker2: TwwDBDateTimePicker
      Left = 425
      Top = 60
      Width = 101
      Height = 22
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayDangKyThoiViec'
      DataSource = FrmDmnv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 2
    end
    object wwDBDateTimePicker3: TwwDBDateTimePicker
      Left = 425
      Top = 84
      Width = 101
      Height = 22
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayTraTaiSan'
      DataSource = FrmDmnv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 3
    end
    object wwDBDateTimePicker4: TwwDBDateTimePicker
      Left = 425
      Top = 108
      Width = 101
      Height = 22
      AutoSize = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'NgayQuyetToan'
      DataSource = FrmDmnv.DsDMNV
      Epoch = 1950
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 4
    end
    object EdHappening: TDBMemo
      Left = 97
      Top = 132
      Width = 429
      Height = 61
      Ctl3D = False
      DataField = 'GhiChu_ThoiViec'
      DataSource = FrmDmnv.DsDMNV
      ParentCtl3D = False
      TabOrder = 5
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 537
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton4: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 54
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 108
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel2: TisPanel
    Left = 0
    Top = 282
    Width = 537
    Height = 56
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 16119285
    ParentBackground = False
    TabOrder = 1
    Visible = False
    HeaderCaption = ' :: Ki'#7875'm tra'
    HeaderColor = clTeal
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = ANSI_CHARSET
    HeaderFont.Color = clWhite
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object TntSpeedButton1: TSpeedButton
      Left = 400
      Top = 25
      Width = 113
      Height = 22
      Cursor = 1
      Action = CmdChecked
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit8: TDBEditEh
      Left = 52
      Top = 25
      Width = 74
      Height = 22
      TabStop = False
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15794175
      ControlLabel.Width = 28
      ControlLabel.Height = 16
      ControlLabel.Caption = 'Ng'#224'y'
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      Visible = True
    end
    object DBEdit12: TDBEditEh
      Left = 172
      Top = 25
      Width = 225
      Height = 22
      TabStop = False
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15794175
      ControlLabel.Width = 17
      ControlLabel.Height = 16
      ControlLabel.Caption = 'B'#7903'i'
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      Visible = True
    end
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 294
    Top = 16
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdChecked: TAction
      Caption = 'Duy'#7879't'
      OnExecute = CmdCheckedExecute
    end
  end
end
