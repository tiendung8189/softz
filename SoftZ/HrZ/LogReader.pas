﻿unit LogReader;

interface

uses
  Classes, windows, SysUtils, zkemkeeper_TLB, Forms, ADODB, devResStr;

type
  TLogReader = class(TThread)
  private
    { Private declarations }
    FActive: boolean;
    FRunOune: boolean;
    FConnected: boolean;
    FDev: TCZKEM;
    FIsTFT: Boolean;
	FClearLog: Boolean;

    FDevID: integer;
    FInOut: Integer;
    IPAddress: string;
    PortNo: integer;
    TS_RECORDER_RAWIO: TADOCommand;
    FIndex: Integer;
    FLastState: Integer;
    FMaxLog: Integer;

    function UpdateLog(pCardID: String; pYY, pMM, pDD, pHH, pMI, pSS, pIO, pDevID: Integer;
        var ret: Integer): Boolean;
    procedure UpdateState;
    function Connect: boolean;
    procedure DisConnnect;
    function ReadLogs: Boolean;
    procedure ClearLogs;
    procedure SetActive(const Value: boolean);

    procedure SendLog(pMsg: String);
	procedure SendMsg(pMsg: String);
  protected
    procedure Execute; override;
  public
    constructor Create(AIPAddress: string; ADevID: integer=1;
        APortNumber: integer=4370; AInOut: Integer = -1; AIndex: Integer = 0;
        devFP_Max: Integer = 10; devFP_Type: Integer = 0; ARunOne: Boolean = False); overload;
    destructor Destroy; override;

    property Connected: boolean read FConnected;
    property Active: boolean read FActive write SetActive;
    property MaxLog: integer read FMaxLog write FMaxLog;
	property AutoClearLog: boolean read FClearLog write FClearLog;
    property DevID: integer read FDevID write FDevID;
  end;

implementation
uses
    MainData, ExCommon;

{ TLogReader }

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.ClearLogs;
var
    n: Integer;
begin
    n := -1;
    FDev.GetDeviceStatus(DevID, SF_INOUT_RECORD, n);
    if n >= MaxLog then
		if AutoClearLog then
			FDev.ClearGLog(DevID)
		else
			SendMsg(Format(RS_DEV_FULL_LOG, [n]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TLogReader.Connect: boolean;
begin
    if not Connected then
    begin
        SendLog(RS_DEV_CONNECTING);
        FConnected := FDev.Connect_Net(IPAddress, PortNo);

        if Connected then
            SendLog(RS_DEV_CONNECTED)
        else
            SendLog(RS_DEV_CONNECT_ERR);
    end;
    if FActive and not Connected then
        FActive := False;

    result := Connected;
    if Result and (FLastState = -1) then
        try
            FIsTFT := FDev.IsTFTMachine(DevID);
        except
            FIsTFT := False;
        end;

    if Result then
        FLastState := 1
    else
        FLastState := 0;
    Synchronize(UpdateState);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TLogReader.Create(AIPAddress: string; ADevID, APortNumber, AInOut,
    AIndex, devFP_Max, devFP_Type: integer; ARunOne: Boolean);
begin
  inherited Create(true);
  FIndex := AIndex;
  FLastState := -1;
  IPAddress := AIPAddress;
  PortNo := APortNumber;
  DevID := ADevID;
  FInOut := AInOut;
  FMaxLog := 100;
  FDev :=  TCZKEM.Create(nil);
  FDev.Width := 0;
  FRunOune := ARunOne;
  FDev.Parent := Application.MainForm;

  TS_RECORDER_RAWIO := TADOCommand.Create(nil);
  with TS_RECORDER_RAWIO do
  begin
    Connection := DataMain.Conn;
    CommandType := cmdStoredProc;
    CommandText := 'TS_RECORDER_RAWIO;1';
    Parameters.Refresh;
  end;

  //resume;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TLogReader.Destroy;
begin
  FDev.Free;
  TS_RECORDER_RAWIO.Free;

  inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.DisConnnect;
begin
  if Connected then
  begin
    FDev.Disconnect;
    FConnected := false;
    SendLog(RS_DEV_DISCONNECT);
    FLastState := 0;
  end;
  Synchronize(UpdateState);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.Execute;
begin
  while not Terminated do
  begin
    if FActive then
    begin
//      if mgCancelTThread then Break;
      if Connect then
      begin
        if ReadLogs then
          ClearLogs;
      end;
    end
    else
      DisConnnect;

    if FActive then
        DisConnnect;
    if FRunOune then
        Break
    else
        Suspend;
  end;

  if FActive then
    DisConnnect;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TLogReader.ReadLogs: Boolean;
var
    n: Integer;
    dwInOutMode, dwYear, dwMonth, dwDay, dwHour,dwMinute: Integer;

    function  GetGeneralLog: Boolean;
    var
        dwEnroll: Integer;
        x1, x2, x3: Integer;
    begin
        Result := True;
        while FDev.GetGeneralLogData(DevID, x1, dwEnroll, x2, x3, dwInOutMode,
            dwYear, dwMonth, dwDay, dwHour, dwMinute) do
        begin
//            if mgCancelTThread then
//                Break;

            if FInOut < 2 then
                dwInOutMode := FInOut;

            if devLogAll then
            else if devLogFromDate > EncodeDate(dwYear, dwMonth, dwDay) then
                Continue;

            if not UpdateLog(IntToStr(dwEnroll), dwYear, dwMonth, dwDay, dwHour, dwMinute,
                0, dwInOutMode, DevID, n) then
                Result := False;
        end;
    end;

    function  GetGeneralLogTFT: Boolean;
    var
        enroll: WideString;
        dwSecond, x1, x2: Integer;
    begin
        Result := True;
        while FDev.SSR_GetGeneralLogData(DevID, enroll, x1, dwInOutMode, dwYear, dwMonth, dwDay,
            dwHour, dwMinute, dwSecond, x2) do
        begin
            SendLog(enroll);
            Application.ProcessMessages;

//            if mgCancelTThread then
//                Break;

            if FInOut < 2 then
                dwInOutMode := FInOut;

            if devLogAll then
            else if devLogFromDate > EncodeDate(dwYear, dwMonth, dwDay) then
                Continue;

            if not UpdateLog(enroll, dwYear, dwMonth, dwDay, dwHour, dwMinute,
                dwSecond, dwInOutMode, DevID, n) then
                Result := False;
        end;
    end;
begin
    n := 0;
    SendLog(RS_DEV_READ_LOG);
    Result := FDev.ReadGeneralLogData(DevID);
    if Result then
        if FIsTFT then
            Result := GetGeneralLogTFT
        else
            Result := GetGeneralLog;

    SendLog(Format(RS_DEV_READ_LOG2, [n]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.SendLog(pMsg: String);
var
    mBuf: PChar;
    s: String;
begin
    s := Format('(%d) %s', [DevID, pMsg]);
    mBuf := StrAlloc(Length(s)+1 );
    StrCopy(mBuf, PChar(s));
    PostMessage(Application.MainFormHandle, MY_MESSAGE_LOG, 0, lparam(mBuf));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.SendMsg(pMsg: String);
var
    mBuf: PChar;
    s: String;
begin
    s := Format('(%d) %s', [DevID, pMsg]);
    mBuf := StrAlloc(Length(s)+1 );
    StrCopy(mBuf, PChar(s));
    PostMessage(Application.MainFormHandle, MY_MESSAGE, 0, lparam(mBuf));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TLogReader.UpdateLog(pCardID: String; pYY, pMM, pDD, pHH, pMI, pSS, pIO,
  pDevID: Integer; var ret: Integer): Boolean;
begin
    with TS_RECORDER_RAWIO do
    begin
        Parameters[1].Value := pCardID;
        Parameters[2].Value := pYY;
        Parameters[3].Value := pMM;
        Parameters[4].Value := pDD;
        Parameters[5].Value := pHH;
        Parameters[6].Value := pMI;
        Parameters[7].Value := pSS;
        Parameters[8].Value := pIO;
        Parameters[9].Value := pDevID;
        Execute;
        Result := Parameters[0].Value = 0;
        if Result then
            inc(ret, Integer(Parameters[10].Value));
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TLogReader.UpdateState;
var
    i: Integer;
begin
    if Connected then
        i := DEV_STATUS_CONECTED
    else if not FActive then
        i := DEV_STATUS_INACTIVE
    else
        i := DEV_STATUS_DISCONECTED;

    PostMessage(Application.MainFormHandle, MY_MESSAGE_DEV_STATUS, FDevID, LParam(i));
end;

end.
