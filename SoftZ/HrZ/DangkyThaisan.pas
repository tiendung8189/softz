﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyThaisan;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, isPanel, Vcl.Buttons, DBCtrlsEh, RzPanel, RzSplit, DBGridEh,
  DBLookupEh, rDBComponents, DbLookupComboboxEh2;

type
  TFrmDangkyThaisan = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton2: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    btnEdit: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    QrMasterMA_LYDO: TWideStringField;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    QrMasterLK_TENNV: TWideStringField;
    DsMaster: TDataSource;
    CmdReload: TAction;
    QrListLeave: TADOQuery;
    CmdSwitch: TAction;
    ToolButton15: TToolButton;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    QrDays: TADOQuery;
    Panel1: TisPanel;
    QrMasterManv: TWideStringField;
    QrMasterDauCa: TIntegerField;
    QrMasterSoNgay: TFloatField;
    QrMasterNgaySinhCon: TDateTimeField;
    QrMasterNgayNhanHoSo: TDateTimeField;
    QrMasterNgayThanhToan: TDateTimeField;
    PNTinhTrang: TisPanel;
    CmdXacNhan: TAction;
    CmdTuChoi: TAction;
    QrMasterTuNgay: TDateTimeField;
    QrMasterDenNgay: TDateTimeField;
    CmdXacNhan1: TAction;
    CmdTuChoi1: TAction;
    RzSizePanel1: TRzSizePanel;
    QrMasterPhuCapThaiSan: TFloatField;
    QrMasterXacNhan_TinhTrang: TIntegerField;
    QrMasterXacNhan_TinhTrangBy: TIntegerField;
    QrMasterXacNhan_Manv: TWideStringField;
    QrMasterXacNhan_GhiChu: TWideStringField;
    QrMasterXacNhan_Ngay: TDateTimeField;
    QrMasterMaPhuCap: TWideStringField;
    DsEmp: TDataSource;
    CbNguoiDuyet: TDbLookupComboboxEh2;
    EdXetDuyet_ManvQL: TDBEditEh;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    rDBCheckBox1: TrDBCheckBox;
    CbMon: TwwDBComboBox;
    CbYear: TwwDBComboBox;
    QrMasterThang: TIntegerField;
    QrMasterNam: TIntegerField;
    QrMasterGhiChu: TWideMemoField;
    QrMasterCo_ThueTNCN: TBooleanField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    Label1: TLabel;
    CbToDate: TwwDBDateTimePicker;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label2: TLabel;
    dpNgaySinhCon: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    CbLoaiPhuCap: TDbLookupComboboxEh2;
    EdPhuCapLoai: TDBEditEh;
    DBEditEh4: TDBEditEh;
    CbbXetDuyet_TinhTrang: TDbLookupComboboxEh2;
    CmdPhuCap: TAction;
    DsTinhTrang_XetDuyet: TDataSource;
    QrTinhTrang_XetDuyet: TADOQuery;
    QrMasterXetDuyet_TinhTrang: TIntegerField;
    QrMasterXetDuyet_TinhTrangBy: TIntegerField;
    QrMasterXetDuyet_Manv: TWideStringField;
    QrMasterXetDuyet_GhiChu: TWideStringField;
    QrMasterXetDuyet_Ngay: TDateTimeField;
    QrMasterLK_PhuCapLoai: TIntegerField;
    QrMasterLK_TenPhuCapLoai: TWideStringField;
    QrMasterLK_XetDuyet_TenTinhTrang: TWideStringField;
    QrMasterLK_XetDuyet_Tennv: TWideStringField;
    Panel2: TPanel;
    Panel3: TPanel;
    rgApproved: TRadioGroup;
    PNGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrEmpStatus: TADOQuery;
    DsEmpStatus: TDataSource;
    QrMasterLK_ManvQL: TWideStringField;
    QrMasterLK_XacNhan_ManvQL: TWideStringField;
    QrMasterLK_XetDuyet_ManvQL: TWideStringField;
    spHR_DANGKY_VANGMAT_Invalid: TADOCommand;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    Label4: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    CmdApproveAccept: TAction;
    CmdApproveCancel: TAction;
    BtnApproveAccept: TBitBtn;
    BtnApproveCancel: TBitBtn;
    QrMasterLK_TenVangMat: TWideStringField;
    QrMasterLK_MaNhomLV: TWideStringField;
    QrMasterMaCa: TWideStringField;
    QrMasterMaNhomLV: TWideStringField;
    QrMasterCREATE_SOURCE: TWideStringField;
    QrShift: TADOQuery;
    QrMasterLK_GioVao: TDateTimeField;
    QrMasterLK_GioRa: TDateTimeField;
    QrMasterLK_GioVao1: TDateTimeField;
    QrMasterLK_GioRa1: TDateTimeField;
    QrMasterLK_SoGio: TFloatField;
    QrMasterGioVao_Ca: TDateTimeField;
    QrMasterGioRa_Ca: TDateTimeField;
    QrMasterGioVao1_Ca: TDateTimeField;
    QrMasterGioRa1_Ca: TDateTimeField;
    QrMasterSoGio_Ca: TFloatField;
    QrMasterCuoiCa: TIntegerField;
    QrMasterIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrEmpBeforeOpen(DataSet: TDataSet);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure CmdPhuCapExecute(Sender: TObject);
    procedure QrMasterMaPhuCapChange(Sender: TField);
    procedure QrMasterXetDuyet_ManvChange(Sender: TField);
    procedure QrMasterManvChange(Sender: TField);
    procedure CmdApproveAcceptExecute(Sender: TObject);
    procedure CmdApproveCancelExecute(Sender: TObject);
    procedure QrMasterMaCaChange(Sender: TField);
  private
  	mCanEdit, mShowMsg, mIsFunction: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, mFilterApproved: Integer;
    mSQL, fStr: String;
    r: WORD;
    procedure XetDuyetChange(pTinhtrang: Integer; pShowMsg: Boolean = True);
    function LeaveDayValid(pEmpID, pLeaveType, pShiftCode: String; pDate: TDateTime; pDateTo: TDateTime;
		pSession, pSessionEnd: Integer; pIDX: Integer): Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDangkyThaisan: TFrmDangkyThaisan;

const
    TABLE_NAME = 'HR_DANGKY_VANGMAT';
    FORM_CODE = 'HR_DANGKY_THAISAN';
    REPORT_NAME = 'HR_RP_DANGKY_THAISAN';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, isCommon,
  DmPhucap, ReceiptDesc, GuidEx, HrData, HrExCommon; // MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    AddAllFields(QrMaster, TABLE_NAME);

    mIsFunction := GetFuncState('HR_THAISAN_DUYET');
    PNTinhTrang.Visible := mIsFunction;

    if mIsFunction then
        PNTinhTrang.Enabled := GetRights('HR_THAISAN_DUYET', False) <> R_DENY;

    initMonthList(CbMon, 0);
    initYearList(CbYear, 0);
    mFilterApproved := -1;
    mShowMsg := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);
  
    with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_HR_PHUCAP, QrV_HR_PHUCAP_LOAI]);

    OpenDataSets([QrTinhTrang_XetDuyet]);

    SetDisplayFormat(QrMaster, ctCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['XetDuyet_Ngay'], DateTimeFmt);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

     if not mIsFunction then
        grRemoveFields(GrList, ['XetDuyet_Manv', 'LK_XetDuyet_ManvQL', 'LK_XetDuyet_Tennv',
            'XetDuyet_TinhTrang', 'LK_XetDuyet_TenTinhTrang', 'XetDuyet_TinhTrangBy',
            'XetDuyet_Ngay', 'XetDuyet_GhiChu']);

    lookupComboboxEhShowImages(QrTinhTrang_XetDuyet, CbbXetDuyet_TinhTrang, 'TEN_HOTRO', 0);
    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrEmpStatus do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;


    with QrMaster.SQL do
    begin
        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [TuNgay], Manv');

        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrTinhTrang_XetDuyet, QrEmp, QrEmpStatus]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
	    CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdApproveAcceptExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_THAISAN_DUYET');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_APPROVE, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            FieldByName('XetDuyet_TinhTrang').AsInteger := 1;
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XetDuyet_Manv').AsString = '' then
//                FieldByName('XetDuyet_Manv').AsString := sysLogonManv;
            Post;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdApproveCancelExecute(Sender: TObject);
var
    r: Word;
begin
    r := GetRights('HR_THAISAN_DUYET');
    if (r = R_DENY) then
        Exit;

    if YesNo(RS_REG_CANCEL, 1) then
    begin
        with QrMaster do
        begin
            Edit;
            FieldByName('XetDuyet_TinhTrang').AsInteger := 2;
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
//            if FieldByName('XetDuyet_Manv').AsString = '' then
//                FieldByName('XetDuyet_Manv').AsString := sysLogonManv;
            Post;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdPhuCapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
    HrDataMain.QrDM_HR_PHUCAP.Requery;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        rgApproved.ItemIndex, //0: Chờ xét duyệt; 1: Đã xét duyệt; 2: Từ chối xét duyệt; 3: Tất cả
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdRefreshExecute(Sender: TObject);
var
    sSQL, sWhere, s: String;
    fFilterApproved : Integer;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

    fFilterApproved := rgApproved.ItemIndex;
   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) or
       (fFilterApproved <> mFilterApproved) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;
        mFilterApproved := fFilterApproved;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;

            sWhere := '';
            if mFilterApproved <= 2 then
                sWhere := sWhere + ' and isnull(XetDuyet_TinhTrang, 0) = '+ IntToStr(mFilterApproved);

            sSQL := '1=1';

            if fStr <> '' then
                case fLevel of
                0:
                    sSQL := '[MaChiNhanh]=''' + fStr + '''';
                1:
                    sSQL := '[MaPhongBan]=''' + fStr + '''';
                2:
                    sSQL := '[MaBoPhan]=''' + fStr + '''';
//                3:
//                    sSQL := '[Group ID]=''' + fStr + '''';
                end;

            SQL.Text := Format(mSQL, [sWhere, sSQL]);
            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
            Parameters[2].Value := fTungay;
            Parameters[3].Value := fDenngay;
            Parameters[4].Value := fTungay;
            Parameters[5].Value := fDenngay;
    	    Open;
        end;
        if s = '' then
            s := 'TuNgay';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bTinhTrang: Boolean;
begin
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bTinhTrang :=  FieldByName('XetDuyet_TinhTrang').AsInteger <> 0;
    end;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;
    BtnApproveAccept.Enabled := bBrowse and (not bTinhTrang) and (not bEmpty);
    BtnApproveCancel.Enabled := bBrowse and (not bTinhTrang) and (not bEmpty);

    btnEdit.Enabled := (not bEmpty) and (not bTinhTrang) and bBrowse;

    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDangkyThaisan.QrMasterBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterBeforePost(DataSet: TDataSet);
var
	d: TDateTime;
begin
    if BlankConfirm(QrMaster, ['Manv', 'TuNgay', 'DenNgay', 'XetDuyet_Manv']) then
        Abort;

    with QrMaster do
    begin
    	d := FieldByName('TuNgay').AsDateTime;
		if exEmpValid(FieldByName('Manv').AsString, FORM_CODE, d) <> 0 then
    	begin
	    	ErrMsg(RS_EMP_ID);
            CbNhanVien.SetFocus;
	        Abort
    	end;

        if FieldByName('DenNgay').AsDateTime < d then
        begin
            ErrMsg(RS_PERIOD);
            CbToDate.SetFocus;
            Abort;
        end;

        if (FieldByName('DenNgay').AsFloat > 10) and (not HrDataMain.kiemTraNgayNghiChuaThoiViec(FieldByName('Manv').AsString,
            FieldByName('DenNgay').AsDateTime)) then
            Abort;

        if (FieldByName('TuNgay').AsFloat > 10) and (FieldByName('NgaySinhCon').AsFloat > 10)
            and (FieldByName('TuNgay').AsDateTime > FieldByName('NgaySinhCon').AsDateTime) then
        begin
            ErrMsg(RS_PERIOD_BIRTHDAY);
            dpNgaySinhCon.SetFocus;
            Abort;
        end;

        if not LeaveDayValid(FieldByName('Manv').AsString,
            FieldByName('MaVangMat').AsString,
            FieldByName('MaCa').AsString,
            FieldByName('TuNgay').AsDateTime,
            FieldByName('DenNgay').AsDateTime,
            FieldByName('DauCa').AsInteger,
            FieldByName('CuoiCa').AsInteger,
            FieldByName('IDX').AsInteger) then
            Abort;

//        if (FieldByName('XetDuyet_TinhTrang').AsInteger <> 0)
//            and BlankConfirm(QrMaster, ['XetDuyet_Manv']) then
//            Abort;

    end;

    SetAudit(DataSet);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterMaCaChange(Sender: TField);
begin
    with QrMaster do
    begin
        FieldByName('GioVao_Ca').AsDateTime := FieldByName('LK_GioVao').AsDateTime;
        FieldByName('GioRa_Ca').AsDateTime := FieldByName('LK_GioRa').AsDateTime;
        FieldByName('GioVao1_Ca').AsDateTime := FieldByName('LK_GioVao1').AsDateTime;
        FieldByName('GioRa1_Ca').AsDateTime := FieldByName('LK_GioRa1').AsDateTime;
        FieldByName('SoGio_Ca').AsFloat := FieldByName('LK_SoGio').AsFloat;

        if (FieldByName('MaCa').AsString = '') or (FieldByName('MaCa').IsNull) then
        begin
            FieldByName('GioVao_Ca').Clear;
            FieldByName('GioRa1_Ca').Clear;
        end;
    end;
end;

procedure TFrmDangkyThaisan.QrMasterManvChange(Sender: TField);
begin
    with QrMaster do
    begin
        EdManvQL.Text := EdManvQL.Field.AsString;
        FieldByName('MaNhomLV').AsString :=  FieldByName('LK_MaNhomLV').AsString;
    end;
    with QrShift do
    begin
        Close;
        Parameters[0].Value := Sender.AsString;
        Open;

        QrMaster.FieldByName('MaCa').AsString := FieldByName('MACA').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterMaPhuCapChange(Sender: TField);
begin
    EdPhuCapLoai.Text := EdPhuCapLoai.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterXetDuyet_ManvChange(Sender: TField);
begin
    EdXetDuyet_ManvQL.Text := EdXetDuyet_ManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 482;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrEmpBeforeOpen(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('TuNgay').AsDateTime := Date;
        FieldByName('DenNgay').AsDateTime := Date;

        if not mIsFunction then
        begin
           FieldByName('XetDuyet_TinhTrang').AsInteger := 1;
           FieldByName('XetDuyet_Ngay').AsDateTime := Now;
           FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;
        end
        else
        begin
           FieldByName('XetDuyet_TinhTrang').AsInteger := 0;
        end;

        FieldByName('CREATE_SOURCE').AsString := sysAppSource;
        FieldByName('DauCa').AsInteger := 1;
        FieldByName('Co_ThueTNCN').AsBoolean := False;
        FieldByName('MaVangMat').AsString := QrListLeave.FieldByName('MaVangMat').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
    QrEmpStatus.Requery;
    QrListLeave.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrList then
    	CbNhanVien.SetFocus
    else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    	frD2D.EdFrom.SetFocus
    else
    	GrList.SetFocus
end;

procedure TFrmDangkyThaisan.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyThaisan.XetDuyetChange(pTinhtrang: Integer; pShowMsg: Boolean);
begin
    with QrMaster do
    begin
        if pTinhtrang = 0 then
        begin
            FieldByName('XetDuyet_Ngay').Clear;
            FieldByName('XetDuyet_TinhTrangBy').Clear;
        end else
        begin
            FieldByName('XetDuyet_Ngay').AsDateTime := Now;
            FieldByName('XetDuyet_TinhTrangBy').AsInteger := sysLogonUID;

            if pShowMsg then
            case pTinhtrang of
                1: Msg(RS_REG_STATUS1);
                2: Msg(RS_REG_STATUS2);
            end;
        end;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyThaisan.LeaveDayValid;
var
    s: String;
begin
    with spHR_DANGKY_VANGMAT_Invalid do
    begin
        Prepared := True;
        Parameters.ParamByName('@IDX').Value := pIDX;
        Parameters.ParamByName('@MANV').Value := pEmpID;
        Parameters.ParamByName('@NGAYD').Value := pDate;
        Parameters.ParamByName('@NGAYC').Value := pDateTo;
        Parameters.ParamByName('@DAUCA').Value := pSession;
        Parameters.ParamByName('@CUOICA').Value := pSessionEnd;
        Parameters.ParamByName('@MA_LYDO').Value := pLeaveType;
        Parameters.ParamByName('@MACA').Value := pShiftCode;

        try
            Execute;
        except
        end;

        if (Parameters.FindParam('@STR') <> nil) then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

end.
