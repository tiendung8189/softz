﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsChucDanh;

interface

uses
  Classes, Controls, Forms, System.Variants,
  Wwdbdlg, StdCtrls, wwdblook, ExtCtrls, Db, ADODB, Buttons, Grids,
  ActnList, wwidlg, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh, ToolCtrlsEh, DynVarsEh,
  DbLookupComboboxEh2;

type
  TFrmChonDsChucDanh = class(TForm)
    QrChucDanh: TADOQuery;
    DsChucDanh: TDataSource;
    RgLoai: TRadioGroup;
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CmdIns: TAction;
    CmdDel: TAction;
    GrList: TStringGrid;
    CmdClear: TAction;
    CbChucVu: TDbLookupComboboxEh2;
    CbChucDanh: TDbLookupComboboxEh2;
    QrChucVu: TADOQuery;
    DsChucVu: TDataSource;
    EdMaChucVu: TDBEditEh;
    EdMaChucDanh: TDBEditEh;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure CbChucVuExit(Sender: TObject);
  private
    mTrigger: Boolean;

    procedure SmartFocus;
  public
  	function  Get(var pType: Integer; var pLst: String): Boolean;
  end;

var
  FrmChonDsChucDanh: TFrmChonDsChucDanh;

implementation

uses
	isDb, isLib, ExCommon, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
	mTrigger := True;
	OpenDataSets([QrChucVu, QrChucDanh, DataMain.QrDM_CHUCDANH]);
	mTrigger := False;
    RgLoai.OnClick(Nil);

    if RgLoai.ItemIndex = 1 then
        CbChucVuExit(CbChucVu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsChucDanh.Get;
var
	i: Integer;
begin
	RgLoai.ItemIndex := pType;
    Result := ShowModal = mrOK;
    if Result then
    begin
    	pType := RgLoai.ItemIndex;
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaChucDanh.Enabled := n = 1;
    CbChucDanh.Enabled := n = 1;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.SmartFocus;
begin
    try
        case RgLoai.ItemIndex of
        0:
            CbChucVu.SetFocus;
        1:
            CbChucDanh.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
	case RgLoai.ItemIndex of
    0:
    	begin
            s1 := EdMaChucVu.Text;
        	s2 := CbChucVu.Text;
        end;
    1:
    	begin
            s1 := EdMaChucDanh.Text;
        	s2 := CbChucDanh.Text;
        end;
    else
    	Exit;
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;

	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
	case RgLoai.ItemIndex of
    0:
       	s := EdMaChucVu.Text;
    1:
       	s := EdMaChucDanh.Text;
    end;

    CmdIns.Enabled := s <> '';
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
	RgLoai.Enabled := GrList.Cells[0, 0] = '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.FormCreate(Sender: TObject);
begin
    mTrigger := False;
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
//	    CloseDataSets([QrChucVu, QrChucDanh]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsChucDanh.CbChucVuExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;


	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrChucVu do
        if Locate('MaChucVu', s, []) then
        begin
            EdMaChucVu.Text := FieldByName('MaChucVu').AsString;
            s1 := EdMaChucDanh.Text;
            with QrChucDanh do
            if (s1 <> '') then
            begin
                if  not Locate('MaChucVu;MaChucDanh', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaChucDanh.Text := FieldByName('MaChucDanh').AsString;
                CbChucDanh.Value := FieldByName('MaChucDanh').AsString;
            end
            else
            begin
                EdMaChucDanh.Text := '';
                CbChucDanh.Text := '';
            end;
        end
        else
        begin
            EdMaChucVu.Text := '';
            EdMaChucDanh.Text := '';
            CbChucDanh.Text := ''
        end;
    1:		// bo phan
        with QrChucDanh do
        if (s <> '') and (Locate('MaChucDanh', s, [])) then
        begin
            EdMaChucDanh.Text := FieldByName('MaChucDanh').AsString;
        end
        else
        begin
            EdMaChucDanh.Text := '';
        end;
	end;

    mTrigger := False;
end;

procedure TFrmChonDsChucDanh.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

end.
