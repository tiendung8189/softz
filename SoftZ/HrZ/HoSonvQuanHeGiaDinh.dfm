object FrmHoSonvQuanHeGiaDinh: TFrmHoSonvQuanHeGiaDinh
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7891' S'#417' Nh'#226'n S'#7921' - Quan H'#7879' Gia '#272#236'nh'
  ClientHeight = 573
  ClientWidth = 1244
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1244
    Height = 37
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
    end
    object ToolButton3: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton12: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 78
    Width = 761
    Height = 474
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'LK_TenQuocTich;CustomEdit;CbQuocTich;F'
      'LK_TenQuanHe;CustomEdit;CbQuanHe;F')
    Selected.Strings = (
      'LK_TenQuanHe'#9'10'#9'Quan h'#7879#9'F'
      'HoTen'#9'20'#9'H'#7885' t'#234'n'#9'F'
      'NgaySinh'#9'10'#9'Ng'#224'y sinh'#9'F'
      'SoThangBatDau'#9'12'#9'S'#7889' th'#225'ng b'#7855't '#273#7847'u'#9'F'#9'Quy '#273#7883'nh ph'#7909' thu'#7897'c'
      'NgayBatDau'#9'10'#9'Ng'#224'y b'#7855't '#273#7847'u'#9'F'#9'Quy '#273#7883'nh ph'#7909' thu'#7897'c'
      'SoThangKetThuc'#9'12'#9'S'#7889' th'#225'ng k'#7871't th'#250'c'#9'F'#9'Quy '#273#7883'nh ph'#7909' thu'#7897'c'
      'NgayKetThuc'#9'10'#9'Ng'#224'y k'#7871't th'#250'c'#9'F'#9'Quy '#273#7883'nh ph'#7909' thu'#7897'c'
      'Co_PhuThuoc'#9'5'#9'Ph'#7909' thu'#7897'c'#9'F'#9'Ph'#7909' thu'#7897'c th'#7921'c t'#7871
      'ThangBatDau'#9'12'#9'Th'#225'ng b'#7855't '#273#7847'u'#9'F'#9'Ph'#7909' thu'#7897'c th'#7921'c t'#7871
      'NamBatDau'#9'12'#9'N'#259'm b'#7855't '#273#7847'u'#9'F'#9'Ph'#7909' thu'#7897'c th'#7921'c t'#7871
      'ThangKetThuc'#9'12'#9'Th'#225'ng k'#7871't th'#250'c'#9'F'#9'Ph'#7909' thu'#7897'c th'#7921'c t'#7871
      'NamKetThuc'#9'12'#9'N'#259'm k'#7871't th'#250'c'#9'F'#9'Ph'#7909' thu'#7897'c th'#7921'c t'#7871
      'MaSoThue'#9'10'#9'M'#227' s'#7889' thu'#7871#9'F'
      'CCCD'#9'12'#9'CCCD'#9'F'
      'DienThoai'#9'10'#9#272'i'#7879'n tho'#7841'i'#9'F'
      'DiaChi'#9'15'#9#272#7883'a ch'#7881#9'F'
      'GhiChu'#9'20'#9'Ghi ch'#250#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDM
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object CbQuocTich: TwwDBLookupCombo
    Left = 201
    Top = 255
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TEN_HOTRO'#9'10'#9'TEN_HOTRO'#9'F')
    DataField = 'QuocTich'
    DataSource = DsDM
    LookupTable = HrDataMain.QrV_QUOCTICH
    LookupField = 'MA_HOTRO'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
  end
  object CbQuanHe: TwwDBLookupCombo
    Left = 201
    Top = 283
    Width = 93
    Height = 22
    Ctl3D = False
    BorderStyle = bsNone
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TEN_HOTRO'#9'10'#9'TEN_HOTRO'#9'F')
    DataField = 'QuanHe'
    DataSource = DsDM
    LookupTable = HrDataMain.QrV_HR_QUANHE
    LookupField = 'MA_HOTRO'
    Options = [loColLines]
    Style = csDropDownList
    ButtonEffects.Transparent = True
    ButtonEffects.Flat = True
    Frame.Enabled = True
    Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
    ParentCtl3D = False
    TabOrder = 3
    AutoDropDown = True
    ShowButton = True
    UseTFields = False
    PreciseEditRegion = False
    AllowClearKey = True
    ShowMatchText = True
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 37
    Width = 1244
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    ExplicitTop = 37
    ExplicitWidth = 1244
    inherited Panel1: TPanel
      Width = 1244
      ExplicitWidth = 1244
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 761
    Top = 78
    Width = 483
    Height = 474
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    HotSpotVisible = True
    ParentColor = True
    ParentFont = False
    ParentShowHint = False
    RealTimeDrag = True
    ShowHint = True
    SizeBarWidth = 7
    TabOrder = 5
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 475
      Height = 474
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PaGhiChu: TisPanel
        Left = 2
        Top = 322
        Width = 471
        Height = 150
        Align = alClient
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 17
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitTop = 298
        ExplicitHeight = 174
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 467
          Height = 130
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsDM
          TabOrder = 1
          ExplicitHeight = 154
        end
      end
      object PaThongTin: TisPanel
        Left = 2
        Top = 2
        Width = 471
        Height = 320
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' :: Th'#244'ng tin'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitLeft = 26
        ExplicitTop = -4
        DesignSize = (
          471
          320)
        object Label1: TLabel
          Left = 37
          Top = 50
          Width = 70
          Height = 16
          Alignment = taRightJustify
          Caption = 'M'#7889'i quan h'#7879
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 300
          Top = 26
          Width = 55
          Height = 16
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Ng'#224'y sinh'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbToDate: TwwDBDateTimePicker
          Left = 361
          Top = 22
          Width = 101
          Height = 22
          Anchors = [akTop, akRight]
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NgaySinh'
          DataSource = DsDM
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 1
        end
        object EdHoTen: TDBEditEh
          Left = 113
          Top = 22
          Width = 180
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 37
          ControlLabel.Height = 16
          ControlLabel.Caption = 'H'#7885' t'#234'n'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'HoTen'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          EmptyDataInfo.Color = clInfoBk
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 0
          Visible = True
        end
        object wwDBEdit2: TDBEditEh
          Left = 113
          Top = 70
          Width = 101
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 63
          ControlLabel.Height = 16
          ControlLabel.Caption = 'M'#227' s'#7889' thu'#7871
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MaSoThue'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 144
          Width = 471
          Height = 76
          Align = alBottom
          Caption = '  Quy '#273#7883'nh ph'#7909' thu'#7897'c  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          ExplicitTop = 120
          DesignSize = (
            471
            76)
          object Label5: TLabel
            Left = 280
            Top = 24
            Width = 75
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = 'Ng'#224'y b'#7855't '#273#7847'u'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 278
            Top = 48
            Width = 77
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = 'Ng'#224'y k'#7871't th'#250'c'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object DBNumberEditEh1: TDBNumberEditEh
            Tag = 999
            Left = 113
            Top = 20
            Width = 45
            Height = 22
            ControlLabel.Width = 98
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' th'#225'ng b'#7855't '#273#7847'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'SoThangBatDau'
            DataSource = DsDM
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object wwDBDateTimePicker2: TwwDBDateTimePicker
            Left = 361
            Top = 20
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgayBatDau'
            DataSource = DsDM
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 1
          end
          object wwDBDateTimePicker3: TwwDBDateTimePicker
            Left = 361
            Top = 44
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgayKetThuc'
            DataSource = DsDM
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 3
          end
          object DBNumberEditEh2: TDBNumberEditEh
            Tag = 999
            Left = 113
            Top = 44
            Width = 45
            Height = 22
            ControlLabel.Width = 100
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' th'#225'ng k'#7871't th'#250'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'SoThangKetThuc'
            DataSource = DsDM
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
        end
        object wwDBLookupCombo1: TwwDBLookupCombo
          Left = 113
          Top = 46
          Width = 101
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TEN_HOTRO'#9'10'#9'TEN_HOTRO'#9'F')
          DataField = 'QuanHe'
          DataSource = DsDM
          LookupTable = HrDataMain.QrV_HR_QUANHE
          LookupField = 'MA_HOTRO'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object DBEditEh1: TDBEditEh
          Left = 361
          Top = 46
          Width = 101
          Height = 22
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 58
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'i'#7879'n tho'#7841'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DienThoai'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 220
          Width = 471
          Height = 100
          Align = alBottom
          Caption = '  Ph'#7909' thu'#7897'c th'#7921'c t'#7871'  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
          ExplicitTop = 196
          DesignSize = (
            471
            100)
          object Label2: TLabel
            Left = 24
            Top = 48
            Width = 83
            Height = 16
            Alignment = taRightJustify
            Caption = 'Th'#225'ng b'#7855't '#273#7847'u'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 22
            Top = 72
            Width = 85
            Height = 16
            Alignment = taRightJustify
            Caption = 'Th'#225'ng k'#7871't th'#250'c'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object rDBCheckBox1: TrDBCheckBox
            Left = 113
            Top = 20
            Width = 85
            Height = 17
            BiDiMode = bdLeftToRight
            Caption = 'Ph'#7909' thu'#7897'c    '
            DataField = 'Co_PhuThuoc'
            DataSource = DsDM
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            TabOrder = 0
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            ShowFieldCaption = False
            UpdateAfterClick = True
          end
          object DBNumberEditEh4: TDBNumberEditEh
            Left = 417
            Top = 68
            Width = 45
            Height = 22
            TabStop = False
            ControlLabel.Width = 75
            ControlLabel.Height = 16
            ControlLabel.Caption = 'N'#259'm k'#7871't th'#250'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'NamKetThuc'
            DataSource = DsDM
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 4
            Visible = True
          end
          object DBNumberEditEh3: TDBNumberEditEh
            Tag = 999
            Left = 417
            Top = 44
            Width = 45
            Height = 22
            ControlLabel.Width = 73
            ControlLabel.Height = 16
            ControlLabel.Caption = 'N'#259'm b'#7855't '#273#7847'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'NamBatDau'
            DataSource = DsDM
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object CbMon: TwwDBComboBox
            Left = 113
            Top = 44
            Width = 45
            Height = 22
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'ThangBatDau'
            DataSource = DsDM
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            Sorted = True
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object CbMonEnd: TwwDBComboBox
            Left = 113
            Top = 68
            Width = 45
            Height = 22
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            ShowMatchText = True
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThangKetThuc'
            DataSource = DsDM
            DropDownCount = 6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            ItemHeight = 0
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            Sorted = True
            TabOrder = 3
            UnboundDataType = wwDefault
          end
        end
        object EdCMND: TDBEditEh
          Tag = 999
          Left = 361
          Top = 70
          Width = 101
          Height = 22
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 90
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' CCCD/CMND'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'CCCD'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
        object DBEditEh5: TDBEditEh
          Tag = 999
          Left = 113
          Top = 118
          Width = 349
          Height = 22
          Anchors = [akLeft, akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 39
          ControlLabel.Height = 16
          ControlLabel.Caption = #272#7883'a ch'#7881
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'DiaChi'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
        object EdSoKhaiSinh: TDBEditEh
          Tag = 999
          Left = 361
          Top = 94
          Width = 101
          Height = 22
          Anchors = [akTop, akRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 69
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' khai sinh'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'SoKhaiSinh'
          DataSource = DsDM
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
      end
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 1244
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ImageIndex = 28
      OnExecute = CmdEditExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 206
    Top = 168
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDMBeforeInsert
    AfterInsert = QrDMAfterInsert
    BeforePost = QrDMBeforePost
    BeforeDelete = QrDMBeforeDelete
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    DataSource = FrmHoSonv.DsDMNV
    Parameters = <
      item
        Name = 'Manv'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from HR_LICHSU_GIADINH'
      'where Manv =:Manv'
      'order by QuanHe')
    Left = 388
    Top = 164
    object QrDMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMManv: TWideStringField
      FieldName = 'Manv'
    end
    object QrDMHoTen: TWideStringField
      FieldName = 'HoTen'
      Size = 100
    end
    object QrDMQuanHe: TWideStringField
      FieldName = 'QuanHe'
      Size = 70
    end
    object QrDMNgaySinh: TDateTimeField
      FieldName = 'NgaySinh'
      OnChange = QrDMNgaySinhChange
    end
    object QrDMCo_PhuThuoc: TBooleanField
      FieldName = 'Co_PhuThuoc'
    end
    object QrDMNgayBatDau: TDateTimeField
      FieldName = 'NgayBatDau'
    end
    object QrDMNgayKetThuc: TDateTimeField
      FieldName = 'NgayKetThuc'
      OnChange = QrDMNgayKetThucChange
    end
    object QrDMHonNhan: TWideStringField
      FieldName = 'HonNhan'
      Size = 70
    end
    object QrDMCCCD: TWideStringField
      FieldName = 'CCCD'
    end
    object QrDMMaSoThue: TWideStringField
      FieldName = 'MaSoThue'
    end
    object QrDMQuocTich: TWideStringField
      FieldName = 'QuocTich'
      Size = 70
    end
    object QrDMDienThoai: TWideStringField
      FieldName = 'DienThoai'
      Size = 100
    end
    object QrDMDiaChi: TWideStringField
      FieldName = 'DiaChi'
      Size = 200
    end
    object QrDMLK_TenQuocTich: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuocTich'
      LookupDataSet = HrDataMain.QrV_QUOCTICH
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'QuocTich'
      Lookup = True
    end
    object QrDMLK_TenQuanHe: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenQuanHe'
      LookupDataSet = HrDataMain.QrV_HR_QUANHE
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'QuanHe'
      Lookup = True
    end
    object QrDMSoThangBatDau: TFloatField
      FieldName = 'SoThangBatDau'
      OnChange = QrDMNgaySinhChange
    end
    object QrDMSoThangKetThuc: TFloatField
      FieldName = 'SoThangKetThuc'
      OnChange = QrDMNgaySinhChange
    end
    object QrDMThangBatDau: TIntegerField
      FieldName = 'ThangBatDau'
    end
    object QrDMNamBatDau: TIntegerField
      FieldName = 'NamBatDau'
    end
    object QrDMThangKetThuc: TIntegerField
      FieldName = 'ThangKetThuc'
    end
    object QrDMNamKetThuc: TIntegerField
      FieldName = 'NamKetThuc'
    end
    object QrDMGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrDMUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrDMLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrDMLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrDMSoKhaiSinh: TWideStringField
      FieldName = 'SoKhaiSinh'
      Size = 100
    end
    object QrDMIDX: TAutoIncField
      FieldName = 'IDX'
      ReadOnly = True
    end
  end
  object DsDM: TDataSource
    AutoEdit = False
    DataSet = QrDM
    Left = 388
    Top = 192
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 177
    Top = 168
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 236
    Top = 168
  end
end
