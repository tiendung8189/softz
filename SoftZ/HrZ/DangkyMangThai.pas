﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyMangThai;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, isPanel, Vcl.Buttons, DBCtrlsEh, RzPanel, RzSplit, DBGridEh,
  DBLookupEh, rDBComponents, DbLookupComboboxEh2;

type
  TFrmDangkyMangThai = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton2: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    ToolButton15: TToolButton;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    Panel1: TisPanel;
    QrMasterManv: TWideStringField;
    CmdXacNhan: TAction;
    CmdTuChoi: TAction;
    CmdXacNhan1: TAction;
    CmdTuChoi1: TAction;
    RzSizePanel1: TRzSizePanel;
    DsEmp: TDataSource;
    QrMasterGhiChu: TWideMemoField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    Label108: TLabel;
    NgayMangThai: TwwDBDateTimePicker;
    Label1: TLabel;
    CbNgayDuSinh: TwwDBDateTimePicker;
    CmdPhuCap: TAction;
    Panel2: TPanel;
    PNGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterLK_ManvQL: TWideStringField;
    spHR_DANGKY_MANGTHAI_Invalid: TADOCommand;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    CmdApproveAccept: TAction;
    CmdApproveCancel: TAction;
    QrMasterLK_Tennv: TWideStringField;
    QrMasterNgayMangThai: TDateTimeField;
    QrMasterNgayDuSinh: TDateTimeField;
    QrMasterSoGio: TFloatField;
    QrMasterNgayBatDau: TDateTimeField;
    QrMasterNgayKetThuc: TDateTimeField;
    CbDate: TwwDBDateTimePicker;
    Label4: TLabel;
    Label6: TLabel;
    CbToDate: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    Label2: TLabel;
    QrMasterIDX: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrEmpBeforeOpen(DataSet: TDataSet);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterManvChange(Sender: TField);
  private
  	mCanEdit, mShowMsg: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel: Integer;
    mSQL, fStr: String;
    function LeaveDayValid(pEmpID: String; pDate: TDateTime; pIDX: Integer): Boolean;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDangkyMangThai: TFrmDangkyMangThai;

const
    TABLE_NAME = 'HR_DANGKY_MANGTHAI';
    FORM_CODE = 'HR_DANGKY_MANGTHAI';
    REPORT_NAME = 'HR_RP_DANGKY_MANGTHAI';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, isCommon,
  DmPhucap, ReceiptDesc, GuidEx, HrData, HrExCommon; // MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    AddAllFields(QrMaster, TABLE_NAME);
    mShowMsg := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);
  
    with HrDataMain do
        OpenDataSets([QrDMNV]);

    SetDisplayFormat(QrMaster, ctCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetDisplayFormat(QrMaster, ['SoGio'], sysFloatFmtOne);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;


    with QrMaster.SQL do
    begin
        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by [NgayMangThai], Manv');

        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
	    CbNhanVien.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;

            sSQL := '1=1';

            if fStr <> '' then
                case fLevel of
                0:
                    sSQL := '[MaChiNhanh]=''' + fStr + '''';
                1:
                    sSQL := '[MaPhongBan]=''' + fStr + '''';
                2:
                    sSQL := '[MaBoPhan]=''' + fStr + '''';
//                3:
//                    sSQL := '[Group ID]=''' + fStr + '''';
                end;

            SQL.Text := Format(mSQL, [sSQL]);
            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
    	    Open;
        end;
        if s = '' then
            s := 'NgayMangThai';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmDangkyMangThai.QrMasterBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_DATE = 'Ngày bắt đầu không hợp lệ.';
    RS_INVALID_DATE_DUE = 'Ngày dự sinh phải > ngày mang thai.';
    RS_INVALID_DATE_START = 'Ngày bắt đầu phải > ngày mang thai.';
procedure TFrmDangkyMangThai.QrMasterBeforePost(DataSet: TDataSet);
var
	d: TDateTime;
begin


    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'NgayMangThai']) then
            Abort;

        d := FieldByName('NgayBatDau').AsDateTime;
		if exEmpValid(FieldByName('Manv').AsString, FORM_CODE, d) <> 0 then
    	begin
	    	ErrMsg(RS_EMP_ID);
            CbNhanVien.SetFocus;
	        Abort;
    	end;

        if (FieldByName('NgayDuSinh').AsFloat > 0) and (FieldByName('NgayDuSinh').AsDateTime < FieldByName('NgayMangThai').AsDateTime) then
        begin
            ErrMsg(RS_INVALID_DATE_DUE);
            CbNgayDuSinh.SetFocus;
	        Abort;
        end;

        if (FieldByName('SoGio').AsFloat > 0) and BlankConfirm(QrMaster, ['NgayBatDau', 'NgayKetThuc']) then
	        Abort;

        if (d > 0) and (d < FieldByName('NgayMangThai').AsDateTime) then
        begin

            ErrMsg(RS_INVALID_DATE_START);
            CbDate.SetFocus;
	        Abort;
        end;

        if (d > 0) and (FieldByName('NgayKetThuc').AsFloat > 0) and
             (FieldByName('NgayKetThuc').AsDateTime < d) then
        begin
            ErrMsg(RS_PERIOD);
            CbToDate.SetFocus;
	        Abort;
        end;

        if (FieldByName('NgayKetThuc').AsFloat > 0) and (not HrDataMain.kiemTraNgayNghiChuaThoiViec(FieldByName('Manv').AsString,
            FieldByName('NgayKetThuc').AsDateTime)) then
            Abort;

        if not LeaveDayValid(FieldByName('Manv').AsString,
            FieldByName('NgayMangThai').AsDateTime,
            FieldByName('Idx').AsInteger) then
            Abort;

    end;

    SetAudit(DataSet);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.QrMasterManvChange(Sender: TField);
begin
    EdManvQL.Text := EdManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 482;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.QrEmpBeforeOpen(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyMangThai.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrList then
    	CbNhanVien.SetFocus
    else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    	frD2D.EdFrom.SetFocus
    else
    	GrList.SetFocus
end;

procedure TFrmDangkyMangThai.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

function TFrmDangkyMangThai.LeaveDayValid;
var
    s: String;
begin
    with spHR_DANGKY_MANGTHAI_Invalid do
    begin
        Prepared := True;
        Parameters.ParamByName('@Idx').Value := pIDX;
        Parameters.ParamByName('@Manv').Value := pEmpID;
        Parameters.ParamByName('@NgayMangThai').Value := pDate;
        Parameters.ParamByName('@Lang').Value := sysLang;
        try
            Execute;
        except
        end;

        if (Parameters.FindParam('@STR') <> nil) then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

end.
