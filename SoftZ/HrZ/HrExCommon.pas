﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HrExCommon;

interface

uses
	Classes, Db, Windows, Forms, ADODb, wwfltdlg, Graphics, ComCtrls, ActnList,
    fctreecombo, fctreeview, Controls, SysUtils, wwDBGrid2, Messages, ADOInt,
    ShellAPI, wwdblook, wwDataInspector, Dialogs, AdvEdit, wwdbedit, Wwdbcomb, wwdbdatetimepicker, DBCtrls, DBAdvEd,
    DBLookupEh, DBCtrlsEh, kbmMemTable, System.Variants, ExtCtrls, StdCtrls,  Wwfltdlg2, Menus, AdvMenus, AppEvnts,
    isPanel, RzSplit, wwDialog, Grids, Wwdbigrd, Wwdbgrid, Mask, RzPanel, ToolWin,Buttons, fcImager, rDBComponents,
    fcCombo,Wwdotdot, DBGridEh, ToolCtrlsEh, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBVertGridsEh,
    DbLookupComboboxEh2;

resourcestring
	{ Softz }
	// Import
    RS_IM_NUM_REC			= 'Đã import %d mẫu tin.';
    RS_IM_NONE				= 'Không có mẫu tin nào được import.';
    RS_IM_ERR_REC			= 'Có %d mẫu tin không hợp lệ.';

    // File
	RS_FILE_IO				= 'Lỗi truy xuất file.';
    RS_EXPORT_ERR			= 'Lỗi xuất dữ liệu.';

    // Common
    RS_CONNECT_FAIL     	= 'Không kết nối được với cơ sở dữ liệu.';
    RS_NOTE_CAP         	= 'Lưu ý';

    RS_NO_FOLDEREXPORT		= 'Chưa chọn thư mục lưu dữ liệu export.';
    RS_EXPORTED_COMPLETE 	= 'Đã xuất xong dữ liệu vào thư mục "%s"';

    RS_DSN_CONFIG			= 'Chưa cấu hình text DSN.';
    RS_ERROR_COPY_FILE		= 'Lỗi khi copy file vào thư mục hệ thống.';
    RS_INVALID_DATA			= 'Dữ liệu không hợp lệ.';
    RS_ITEM_CODE_DUPLICATE	= 'Trùng %s.';

    RS_ADMIN_CONTACT		= 'Xin liên hệ với người quản trị hệ thống.';

    // Cr Frame
    RS_THESAME_MONTH 		= 'Ngày báo cáo phải cùng tháng.';
    RS_INVALID_REPORTTIME	= 'Thời gian báo cáo không hợp lệ.';

    {Hrms}
	RS_SEPARATION_DATE		= 'Ngày thôi việc không hợp lệ. Xem lại quá trình làm việc.';
	RS_EMP_ID				= 'Mã nhân viên không hợp lệ.';
	RS_PERIOD				= 'Thời gian không hợp lệ.';
    RS_SESSION_VALIED   	= 'Buổi nghỉ không hợp lệ.';
    RS_SESSION2_VALIED   	= 'Buổi nghỉ thứ 2 chỉ được nhập "Nửa buổi cuối ngày".';

    // Leave Type
	RS_LEAVE_TYPE_INVALID	= 'Lỗi nhập liệu. Loại nghỉ không hợp lệ.';
    RS_LEAVE_TYPE_AND_AMOUNT	= 'Lỗi nhập liệu. Phải nhập đủ Lý do và Số ngày.';
    RS_LEAVE_TYPE_DUPLICATE	= 'Lỗi nhập liệu. Loại nghỉ không được trùng nhau.';
    RS_LEAVE_TYPE_2ML		= 'Lỗi nhập liệu. Trùng loại nghỉ thai sản.';
    RS_LEAVE_MAX_DAY	    = 'Lỗi nhập liệu. Số ngày nghỉ không được lớn hơn 1.';

    // Leave Balance
	RS_CFM_CARRY_FWD		= 'Mặc định cột "Chuyển năm sau"'#13 +
    						  'bằng cột "Còn lại". Tiếp tục?';

    // Organization
	RS_CONGTY				= '&Công ty';
	RS_CHINHANH				= '&Chi nhánh';

	RS_ORG0					= '&Toàn bộ';
	RS_ORG1					= '&Phòng ban';
	RS_ORG2					= '&Bộ phận';
	RS_ORG3					= '&Tổ';
	RS_ORGANIZATION			= 'Sai tổ chức phòng ban / bộ phận...';

    RS_CODE_DEF             = 'Mặc định Mã chấm công từ Mã nhân viên?';
    RS_LOG_INVALID_CARD		= 'Các Thẻ Không Hợp Lệ';
    RS_LOG_INVALID_EMP		= 'Các Mã Không Hợp Lệ';
	RS_LOG_INVALID			= 'Dữ Liệu Không Hợp Lệ';
    RS_LOG_DUP_CARD			= 'Các Mã Thẻ Bị Trùng';

    // Timesheet
	RS_TIMESHEET_CFM_CLOSED	= 'Bảng công tháng đã khóa sổ. Bạn không thể chỉnh sửa được.';
	RS_IMPORT_ERROR_AT		= 'Dữ liệu lỗi ở dòng: %d, Emp ID: %s - Date: %s. Tiếp tục?';
	RS_TIMESHEET_CALC1		= 'Tổng hợp công tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CALC2		= 'Tổng hợp công tháng %d/%d của tất cả'#13'nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_AUTO1		= 'Tự động bổ sung giờ vào, giờ ra bị thiếu của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_AUTO2		= 'Tự động bổ sung giờ vào, giờ ra bị thiếu của'#13'tất cả nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_CLEAR1		= 'Xóa dữ liệu công tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CLEAR2		= 'Xóa toàn bộ dữ liệu công tháng %d/%d của'#13'tất cả nhân viên. Tiếp tục?';
    RS_TIMESHEET_CLEAR_M1	= 'Xóa dữ liệu suất ăn tháng %d/%d của'#13'nhân viên "%s". Tiếp tục?';
	RS_TIMESHEET_CLEAR_M2	= 'Xóa toàn bộ dữ liệu suất ăn tháng %d/%d của'#13'tất cả nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_VALID		= 'Dữ liệu công đã hợp lệ.';
	RS_TIMESHEET_ERASE		= 'Xóa dữ liệu công bổ sung tháng %d/%d của tất cả'#13'nhân viên có trên màn hình. Tiếp tục?';
	RS_TIMESHEET_POPUP		= 'Tổng hợp công tháng cho';
    RS_TIMESHEET_POPUP2		= 'Cập nhật dữ liệu công cho';
    RS_TIMESHEET_POPUP3		= 'Xóa dữ liệu giờ công của';
	RS_TIMESHEET_RECALC1	= 'Tính lại OT đã duyệt.';
    RS_TIMESHEET_GEN_ERROR	= 'Chưa cập nhật các thông số chấm công,'#13'tính lương cho tháng %d/%d.';

    // Payroll Advanced
	RS_ADVANCED_CALC1		= 'Tính lại tạm ứng cho nhân viên "%s". Tiếp tục?';
	RS_ADVANCED_CALC		= 'Tính lại tạm ứng tháng %d/%d của tất cả'#13 +
							  'nhân viên có trên màn hình. Tiếp tục?';

    // Payroll
	RS_PAYROLL_CFM_CLOSED	= 'Bảng lương tháng đã khóa sổ. Bạn không thể chỉnh sửa được.';
	RS_PAYROLL_ERROR_CALC1	= 'Lỗi tính lương nhân viên "%s".';
    RS_PAYROLL_CFM_CALC1	= 'Tính lại lương cho nhân viên "%s". Tiếp tục?';
    RS_PAYROLL_CFM_CALC2	= 'Tính lại lương tháng %d/%d của tất cả'#13 +
							  'nhân viên có trên màn hình. Tiếp tục?';
	RS_PAYROLL_CFM_IMPORT	= 'Cập nhật dữ liệu vào bảng lương tháng %d/%d. Tiếp tục?';
	RS_PAYROLL_CFM_ERASE	= 'Xóa dữ liệu lương bổ sung tháng %d/%d của tất cả'#13 +
    						  'nhân viên có trên màn hình. Tiếp tục?';

    // Separation
	RS_CFM_REWORK			= 'Nhân viên này sẽ chuyển từ tình trạng'#13 +
			    			  'thôi việc sang làm việc. Tiếp tục?';
	RS_SEPARATED			= 'Nhân viên này đã thôi việc.';

    RS_ROSTER_CFM_CLOSED	= 'Lịch làm việc đã khóa sổ. Bạn không thể chỉnh sửa được.';

	RS_EDIT_NOT_ALLOWED		= 'Dữ liệu đã được xác nhận là đúng. Không được phép chỉnh sửa.';

    RS_CFM_DATE_FORMAT		= 'Định dạng ngày của Windows đang là "%s".'#13'Dữ liệu ngày cũng phải được định dạng đúng như vậy. Tiếp tục?';
	RS_MUST_NODOT			= 'Lỗi nhập liệu. Mã không được chứa dấu ".".';
	RS_NO_SKIN_DATA			= 'Không có dữ liệu hiệu ứng.';
	RS_POPUP_RECALC			= 'Tính lại cho';
	RS_RECALC_COLS			= 'Tính Lại Các Cột';


const
    REP_ENCODE_KEY: Word = 119;
    TXT_DSN: String 		= 'SOFTZ_HR_TEXT';

var
    //
    sysSite: String;

    (*
        May cham cong
    *)
     devLogFromDate: TDateTime;
     devLogAll: Boolean;

    (*
    ** License
    *)
	(*
    ** Utils
    *)

//procedure FlexGroupCombo(Sender: TfcTreeCombo);
//function  exGetFlexDesc(Sender: TObject): WideString;

function  exEmpValid(pMANV, pFunc: String; pDate: TDateTime = 0): Integer;

procedure exCompleteConfirm;
function  exClearListConfirm: Boolean;

(*
    **
    *)
procedure FlexOrgCombo(Sender: TfcTreeCombo; const pDep: String = '');
function  exGetFlexDesc(Sender: TObject): String;

procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo; s: String);

	(*
    ** Refs. functions
    *)
function exDotManv(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;
function  exDotManv(Sender: TField): Boolean; overload;
function  exDotManv(DataSet: TDataSet; var s: String): Boolean; overload;
function  exDotManv(var s: String): Boolean; overload;
function  exDotManvRoom(DataSet: TDataSet; Field: TField; CbOrg: TfcTreeCombo = Nil): Boolean; overload;
function  exDotManvRoom(DataSet: TDataSet; var Value: String; CbOrg: TfcTreeCombo = Nil; pCond: String = ''): Boolean; overload;

function  exDotMaPhuCap(DataSet: TDataSet; Sender: TField;
	exCond: String = ''): Boolean; overload;

function  changeCaptionByNameForm(formComponent: TForm): Boolean; overload;
function  getCaptionByNameForm(Name: String): String;

implementation

uses
	isMsg, isStr, Printers, MainData, HrData, IniFiles, isCommon, Math, isFile,
    Rights, isDb, GuidEx, isLib, isNameVal, RepEngine, isEnCoding, SetLicense, isType,
    ExCommon;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exEmpValid(pMANV, pFunc: String; pDate: TDateTime = 0): Integer;
begin
    Result := 0;
    if pMANV = '' then
        Exit;
    if (IsDotSelect(pMANV) > 0) and (isSubStr(pMANV, 2) = '') then
        Exit;
	Result := HrDataMain.ManvValid(pMANV, pFunc, pDate);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure FlexOrgCombo(Sender: TfcTreeCombo; const pDep: String = '');
var
    mId: String;
	Qr0, Qr1, Qr2, Qr3: TADOQuery;
    it0, it1, it2, it3: TfcTreeNode;
begin
	with Sender do
    begin
    	Items.Clear;
		TreeView.Indent := 15;
    end;

    Qr0 := TADOQuery.Create(Nil);
    Qr1 := TADOQuery.Create(Nil);
    Qr2 := TADOQuery.Create(Nil);
    Qr3 := TADOQuery.Create(Nil);

    Qr0.Connection := DataMain.Conn;
    Qr1.Connection := DataMain.Conn;
    Qr2.Connection := DataMain.Conn;
    Qr3.Connection := DataMain.Conn;

    Qr0.LockType := ltReadOnly;
    Qr1.LockType := ltReadOnly;
    Qr2.LockType := ltReadOnly;
    Qr3.LockType := ltReadOnly;

    // Site
    with Qr0 do
	begin
    	if pDep = '' then
			SQL.Text := 'select * from HR_DM_CHINHANH order by [MACN]'
        else
			SQL.Text := 'select * from HR_DM_CHINHANH where [MACN] in (select [MACN] from DM_PHONGBAN where [MAPB] in (' + pDep + ')) order by [MAPB]';
        Open;

        while not Eof do
        begin
            mId := FieldByName('MACN').AsString;
            it0 := Sender.Items.Add(Nil, Format('[%s] %s', [FieldByName('MACN').AsString, FieldByName('TENCN').AsString]));
            it0.StringData := mId;
            it0.ImageIndex := 0;
//            if not sysManySites then
//            	Sender.Tag := 1;

            // Department
            with Qr1 do
            begin
            	if pDep = '' then
					SQL.Text := 'select * from HR_DM_PHONGBAN where [MACN]=''' + mId + ''' order by [MAPB]'
                else
					SQL.Text := 'select * from HR_DM_PHONGBAN where [MACN]=''' + mId + ''' and [MAPB] in (' + pDep + ') order by [MAPB]';
                Open;

                while not Eof do
                begin
                    mId := FieldByName('MAPB').AsString;
                    it1 := Sender.Items.AddChild(it0, Format('[%s] %s', [FieldByName('MAPB').AsString, FieldByName('TENPB').AsString]));
                    it1.StringData := mId;
                    it1.ImageIndex := 1;

    	            Next;
        	    end;
            	Close;
        	end;
            Next;
    	end;
        Close;
    end;


    Qr0.Free;
    Qr1.Free;
    Qr2.Free;
    Qr3.Free;

    with Sender do
        if Items.Count = 0 then
        	Text := ''
        else
	    begin
        	SetSelectedNode(Items[0]);
	    	//Text := Items[0].Text;
            Text := '';
		end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exGetFlexDesc(Sender: TObject): String;
var
    s: String;
begin
    with Sender as TfcTreeCombo do
    begin
        if Text = '' then
            s := RS_ORG0
        else if SelectedNode <> Nil then
        begin
            case SelectedNode.Level of
            0:
            	if (Sender as TComponent).Tag = 1 then
	                s := RS_CONGTY
                else
	                s := RS_CHINHANH;
            1:
                s := RS_ORG1;
            2:
                s := RS_ORG2;
            3:
                s := RS_ORG3;
            end;
        end;
    end;
    Result := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure exComboBeforeDropDown(Sender: TwwDBLookupCombo);
begin
    Sender.LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

procedure exComboBeforeDropDownTK(Sender: TwwDBLookupCombo; s: String);
begin
    Sender.LookupTable.Filter := 'MADT='+QuotedStr(s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_MUST_COMPLETED = 'Bạn phải hoàn tất nhập liệu.';

procedure exCompleteConfirm;
begin
	Msg(RS_MUST_COMPLETED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CFM_CLEARLIST = 'Toàn bộ dữ liệu trên màn hình sẽ bị xóa. Tiếp tục?';

function exClearListConfirm: Boolean;
begin
	Result := YesNo(RS_CFM_CLEARLIST, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function CsvPrepareFile(pSource, pName: String): Boolean;
var
	dsnPath: String;
begin
//	Result := GetDsnDir(TXT_DSN, dsnPath);
//    if not Result then
//    begin
//    	ErrMsg(RS_DSN_CONFIG);
//    	Exit;
//    end;

//	if FileExists(sysAppPath + 'System\schema.ini') then
//    	CopyFile(PChar(sysAppPath + 'System\schema.ini'), PChar(dsnPath + 'schema.ini'), False);
//
//	if UpperCase(pSource) <> UpperCase(dsnPath + pName + '.csv') then
//    	if not CopyFile(PChar(pSource), PChar(dsnPath + pName + '.csv'), False) then
//        begin
//        	ErrMsg(RS_ERROR_COPY_FILE);
//			Result := False;
//        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function GetToggleMarkCaption(DataSet: TCustomADODataSet) : String;
begin
	Result := 'Xóa';
	with DataSet do
		if Active then
			if FieldByName('DELETE_BY').AsString = '' then
            else
        		Result := 'Phục hồi'
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure ToggleMarkDataSet(DataSet: TCustomADODataSet);
begin
    with DataSet do
    begin
        Edit;

        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            FieldByName('DELETE_BY').Clear;
            FieldByName('DELETE_DATE').Clear;
        end
        else
        begin
            FieldByName('DELETE_BY').AsInteger := sysLogonUID;
            FieldByName('DELETE_DATE').AsDateTime := Date;
        end;

        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TransFieldDesc(ds: TDataSet; flst: String): String;
var
	i: Integer;
    ls: TStrings;
begin
    ls := TStringList.Create;
    ls.Text := flst;

    Result := '';
	for i := 0 to ls.Count - 1 do
    	Result := Result + ds.FieldByName(ls.Strings[i]).DisplayName + #13;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function MakeEraseSQL(ds: TDataSet; tn, flst: String): String;
var
	i: Integer;
    ls: TStrings;

    (*
    **
    *)
    function SmartValue(fn: String): String;
    begin
    	case ds.FieldByName(fn).DataType of
        ftFloat, ftInteger:
        	Result := '0';
		else
        	Result := 'NULL';
        end;
    end;

begin
   	ls := TStringList.Create;
	ls.Text := flst;

    Result := Format('update %s set [%s]=%s', [tn, ls.Strings[0], SmartValue(ls.Strings[0])]);
    for i := 1 to ls.Count - 1 do
    	Result := Result + Format(', [%s]=%s', [ls.Strings[i], SmartValue(ls.Strings[i])]);
end;

(*==============================================================================
** Đánh lại STT cho chi tiết phiếu trước khi lưu
**------------------------------------------------------------------------------
*)
procedure exSaveDetails(DataSet: TCustomADODataSet);
var
	bm: TBytes;
    n: Integer;
begin
    with DataSet do
    begin
        bm := Bookmark;
        DisableControls;

        First;
        mTrigger := True;
        while not Eof do
        begin
        	n := RecNo;
            if FieldByName('STT').AsInteger <> n then
            begin
                Edit;
                FieldByName('STT').AsInteger := n;
            end;
            Next;
        end;

        Bookmark := bm;
        EnableControls;
        UpdateBatch;
        mTrigger := False;
    end;
end;

	(*
    ** Refs. functions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	DOT_SEARCH_MANV = 'MANV'#9'12'#9'Mã nhân viên'#13'TENNV'#9'35'#9'Họ tên nhân viên'#13'TenNhomLV'#9'25'#9'Nhóm làm việc';
function exDotManv(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect2(DataSet, s, DOT_SEARCH_MANV, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

function exDotManv(Sender: TField): Boolean;
begin
	Result := exDotManv(HrDataMain.QrDMNV, Sender);
end;

function exDotManv(DataSet: TDataSet; var s: String): Boolean;
begin
    Result := QuickSelect2(DataSet, s, DOT_SEARCH_MANV);
end;
function exDotManv(var s: String): Boolean;
begin
    Result := exDotManv(HrDataMain.QrDMNV, s);
end;

function  exDotManvRoom(DataSet: TDataSet; var Value: String; CbOrg: TfcTreeCombo; pCond: String): Boolean; overload;
var
    sCond: String;
    s: String;
    n: Integer;
begin
	n := -1;
    s := '';
	if Assigned(CbOrg) then
        with CbOrg do
            if Text <> '' then
            begin
                s := SelectedNode.StringData;
                n := SelectedNode.Level;
            end;

    case n of
    0:
        sCond := sCond + ' [MaChiNhanh]=''' + s + '''';
    1:
        sCond := sCond + ' [MaPhongBan]=''' + s + '''';
    2:
        sCond := sCond + ' [MaBoPhan]=''' + s + '''';
    end;

    Result := QuickSelect2(DataSet, Value, DOT_SEARCH_MANV, sCond, 'Manv');
end;

function exDotManvRoom(DataSet: TDataSet; Field: TField;CbOrg: TfcTreeCombo): Boolean;
var
    s: String;
begin
	s := Field.AsString;
	Result := exDotManvRoom(DataSet, s, CbOrg);
    if Result then
		Field.AsString := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	DOT_SEARCH_MAPHUCAP = 'MaPhuCap'#9'12'#9'Mã phụ cấp'#13'TenPhuCap'#9'35'#9'Tên phụ cấp';
function exDotMaPhuCap(DataSet: TDataSet; Sender: TField; exCond: String = ''): Boolean;
var
    s: String;
begin
    if mExDot then
        Exit;

    s := Sender.AsString;
    Result := QuickSelect2(DataSet, s, DOT_SEARCH_MAPHUCAP, exCond);
    if Result then
    begin
        mExDot := True;
        Sender.AsString := s;
        mExDot := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  changeCaptionByNameForm(formComponent: TForm): Boolean; overload;
var
    sCaption: String;
    i: Integer;
begin
    with formComponent do
    begin
        for i := 0 to ComponentCount - 1 do
        begin
            if Components[i].ClassNameIs('TLabel')  then
            begin
                with (Components[i] as TLabel) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBEdit') or
                Components[i].ClassNameIs('TwwDBEdit') then
            begin
                with (Components[i] as TwwDBEdit) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBAdvEdit') then
            begin
                with (Components[i] as TDBAdvEdit) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TwwDBLookupCombo') then
            begin
                with (Components[i] as TwwDBLookupCombo) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TwwDBDateTimePicker') then
            begin
                with (Components[i] as TwwDBDateTimePicker) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBMemo') then
            begin
                with (Components[i] as TDBMemo) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TwwDBCombobox') then
            begin
                with (Components[i] as TwwDBCombobox) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBEditEh') then
            begin
                with (Components[i] as TDBEditEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBNumberEditEh') then
            begin
                with (Components[i] as TDBNumberEditEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBDateTimeEditEh') then
            begin
                with (Components[i] as TDBDateTimeEditEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBLookupComboboxEh') then
            begin
                with (Components[i] as TDBLookupComboboxEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBMemoEh') then
            begin
                with (Components[i] as TDBMemoEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end else if Components[i].ClassNameIs('TDBLookupComboboxEh2') then
            begin
                with (Components[i] as TDBLookupComboboxEh) do
                begin
                    sCaption := getCaptionByNameForm(Name);
                    if sCaption <> '' then
                        ControlLabel.Caption := sCaption; 
                end;
            end;
            
        end;
    end;

    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function  getCaptionByNameForm(Name: String): String;
begin
    Name := Copy(Name, 4, Length(Name) - 3);
    Result := getTranslateByResource(Name);
end;

end.
