object FrmDmHotro_HR: TFrmDmHotro_HR
  Left = 583
  Top = 367
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c H'#7895' Tr'#7907
  ClientHeight = 409
  ClientWidth = 695
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 388
    Width = 695
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 695
    Height = 48
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 80
      Top = 16
      Width = 61
      Height = 16
      Alignment = taRightJustify
      Caption = 'Lo'#7841'i h'#7895' tr'#7907
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbKhac: TwwDBLookupCombo
      Left = 149
      Top = 12
      Width = 300
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENNHOM_HOTRO'#9'30'#9'T'#234'n'#9'F')
      LookupTable = QrDmkhacX
      LookupField = 'MANHOM_HOTRO'
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnNotInList = CbKhacNotInList
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 84
    Width = 695
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 695
      Height = 304
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'MA'#9'10'#9'M'#227#9'F'
        'TEN_HOTRO'#9'60'#9'T'#234'n'#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      Ctl3D = True
      DataSource = DsDmkhac
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint]
      ParentCtl3D = False
      ParentFont = False
      PopupMenu = PopSort
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnCalcCellColors = GrListCalcCellColors
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
      ExplicitHeight = 266
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 52
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdAdmin: TAction
      ShortCut = 16507
      Visible = False
      OnExecute = CmdAdminExecute
    end
  end
  object QrDmkhac: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrDmkhacBeforeInsert
    AfterInsert = QrDmkhacAfterInsert
    BeforePost = QrDmkhacBeforePost
    BeforeDelete = QrDmkhacBeforeDelete
    OnDeleteError = QrDmkhacPostError
    OnEditError = QrDmkhacPostError
    OnPostError = QrDmkhacPostError
    DataSource = DsDmkhacX
    Parameters = <
      item
        Name = 'MANHOM_HOTRO'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_HOTRO'
      'where MANHOM_HOTRO=:MANHOM_HOTRO'
      'order by MANHOM_HOTRO')
    Left = 20
    Top = 168
    object QrDmkhacCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDmkhacUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDmkhacCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDmkhacUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDmkhacMA_HOTRO: TWideStringField
      FieldName = 'MA_HOTRO'
      Size = 70
    end
    object QrDmkhacMANHOM_HOTRO: TWideStringField
      FieldName = 'MANHOM_HOTRO'
      Size = 50
    end
    object QrDmkhacMA: TWideStringField
      FieldName = 'MA'
    end
    object QrDmkhacTEN_HOTRO: TWideStringField
      FieldName = 'TEN_HOTRO'
      Size = 200
    end
    object QrDmkhacGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
  end
  object DsDmkhac: TDataSource
    DataSet = QrDmkhac
    Left = 20
    Top = 200
  end
  object QrDmkhacX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from V_HR_DM_HOTRO_NHOM')
    Left = 52
    Top = 168
  end
  object DsDmkhacX: TDataSource
    AutoEdit = False
    DataSet = QrDmkhacX
    Left = 52
    Top = 200
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 84
    Top = 168
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 84
    Top = 200
    object Tmmutin1: TMenuItem
      Action = CmdSearch
    end
  end
end
