﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmNhomlv;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  wwfltdlg, Wwdbgrid, wwDialog, Wwdbigrd, ToolWin, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBVertGridsEh, Vcl.ExtCtrls, RzPanel, RzSplit, Vcl.StdCtrls,
  Vcl.Mask, wwdbedit, isPanel, DBCtrlsEh, System.Types, wwdblook, Wwdotdot,
  Wwdbcomb, Vcl.Buttons, rDBComponents;

type
  TFrmDmNhomlv = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDM: TADOQuery;
    DsDM: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdAudit: TAction;
    GrList: TwwDBGrid2;
    ToolButton12: TToolButton;
    QrDMCREATE_BY: TIntegerField;
    QrDMUPDATE_BY: TIntegerField;
    QrDMCREATE_DATE: TDateTimeField;
    QrDMUPDATE_DATE: TDateTimeField;
    QrDMMaNhomLV: TWideStringField;
    QrDMTENNHOMLV: TWideStringField;
    QrDMTENNHOMLV_TA: TWideStringField;
    QrDMNGAYCONG_THANG: TFloatField;
    QrDMNGAYCONG_TUAN: TFloatField;
    QrDMT2: TBooleanField;
    QrDMT3: TBooleanField;
    QrDMT4: TBooleanField;
    QrDMT5: TBooleanField;
    QrDMT6: TBooleanField;
    QrDMT7: TBooleanField;
    QrDMCN: TBooleanField;
    QrDMCo_ChamCong: TBooleanField;
    QrDMCA1: TWideStringField;
    QrDMCA2: TWideStringField;
    CmdDetail: TAction;
    QrDMNGAYCONG_GIO: TFloatField;
    QrDMNGAYCONG_PHUT: TFloatField;
    QrDMOT: TBooleanField;
    QrDMHT: TBooleanField;
    QrDMNS: TBooleanField;
    RzSizePanel1: TRzSizePanel;
    QrDMCA: TADOQuery;
    PD1: TisPanel;
    isPanel1: TisPanel;
    grThongTinLamViec: TDBVertGridEh;
    EdiMaNhomLV: TDBEditEh;
    EdTenNhomLV: TDBEditEh;
    CmdEdit: TAction;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure QrDMAfterInsert(DataSet: TDataSet);
    procedure grThongTinLamViecGetCellParams(Sender: TObject; Row: TFieldRowEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrAllowanceBeforeDelete(DataSet: TDataSet);
    procedure QrAllowanceBeforeEdit(DataSet: TDataSet);
    procedure QrAllowanceBeforeOpen(DataSet: TDataSet);
    procedure CmdEditExecute(Sender: TObject);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmNhomlv: TFrmDmNhomlv;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    DmNhomlvDetail, HrExCommon;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHOM_LAMVIEC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
//	DsDM.AutoEdit := mCanEdit;
//    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDM, FORM_CODE);

    fixCode := SetCodeLength(FORM_CODE, QrDM.FieldByName('MaNhomLV'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDM, QrDMCA, DataMain.QrResource]);

    changeCaptionByNameForm(FrmDmNhomlv);

    SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary([QrDM], [FORM_CODE], [Filter]);


end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDM, QrDMCA]);
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDM, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdNewExecute(Sender: TObject);
begin
    QrDM.Append;
	EdiMaNhomLV.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdSaveExecute(Sender: TObject);
begin
    QrDM.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdCancelExecute(Sender: TObject);
begin
    QrDM.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdDelExecute(Sender: TObject);
begin
    QrDM.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdDetailExecute(Sender: TObject);
begin
//    Application.CreateForm(TFrmNhomlvDetail, FrmNhomlvDetail);
//    FrmNhomlvDetail.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdEditExecute(Sender: TObject);
begin
    QrDM.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDM)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bInsert: Boolean;
begin
    exActionUpdate(ActionList, QrDM, Filter, mCanEdit);

    with QrDM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bEmpty := IsEmpty;

    end;

    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrAllowanceBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
        Abort;

    SetEditState(QrDM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrAllowanceBeforeEdit(DataSet: TDataSet);
begin
    SetEditState(QrDM);
//    with QrDMNV do
//    if (State in [dsBrowse]) then
//        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrAllowanceBeforeOpen(DataSet: TDataSet);
begin
    //QrAllowance.Parameters[0].Value := QrDM.FieldByName('MaNhomLV').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrDMAfterInsert(DataSet: TDataSet);
begin
    with QrDM do
    begin
        FieldByName('T2').AsBoolean := True;
        FieldByName('T3').AsBoolean := True;
        FieldByName('T4').AsBoolean := True;
        FieldByName('T5').AsBoolean := True;
        FieldByName('T6').AsBoolean := True;
        FieldByName('T7').AsBoolean := False;
        FieldByName('CN').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrDMBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrDMBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrDM, ['MaNhomLV', 'TenNhomLV']) then
   		Abort;

    if fixCode then
		if LengthConfirm(QrDM.FieldByName('MaNhomLV')) then
        	Abort;

    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 370;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.QrDMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaNhomLV' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

procedure TFrmDmNhomlv.grThongTinLamViecGetCellParams(Sender: TObject;
  Row: TFieldRowEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
	s: string;
begin
    with Row do
    begin
        s := UpperCase(FieldName);
        RowLabel.Font.Size  := 10;
        if ((not QrDM.FieldByName(FieldName).AsBoolean) and ((s = 'T7') or (s = 'CN'))) then
        begin
            RowLabel.Font.Color := clBlue;
            RowLabel.Font.Style := [fsBold];
        end
        else
        begin
            RowLabel.Font.Color := clBlack;
            RowLabel.Font.Style := [];
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDM, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNhomlv.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDM);
end;

end.
