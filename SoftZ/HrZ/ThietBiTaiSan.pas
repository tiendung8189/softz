﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThietBiTaiSan;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  RzPanel, RzSplit, isPanel, Vcl.Buttons, DBCtrlsEh, DBGridEh, DBLookupEh,
  rDBComponents, frameEmp, DbLookupComboboxEh2;

type
  TFrmThietBiTaiSan = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton12: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdPrint: TAction;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdRefresh: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    CmdEdit: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrMaster: TADOQuery;
    DsMaster: TDataSource;
    CmdReload: TAction;
    CmdSwitch: TAction;
    CmdClear: TAction;
    N2: TMenuItem;
    Xadanhsch1: TMenuItem;
    GrList: TwwDBGrid2;
    frD2D: TfrD2D;
    PaThongTin: TisPanel;
    QrMasterCREATE_BY: TIntegerField;
    QrMasterUPDATE_BY: TIntegerField;
    QrMasterCREATE_DATE: TDateTimeField;
    QrMasterUPDATE_DATE: TDateTimeField;
    spHR_TINH_HETHAN_HDLD: TADOCommand;
    CmdPrint2: TAction;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    QrMasterManv: TWideStringField;
    QrMasterFileName: TWideStringField;
    QrMasterFileExt: TWideStringField;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    PaGhiChu: TisPanel;
    EdGHICHU: TDBMemo;
    QrMasterUPDATE_NAME: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    CmdLoaiHopDong: TAction;
    QrMasterMaThietBi: TWideStringField;
    QrMasterSoLuong: TFloatField;
    QrMasterNgayBatDau: TDateTimeField;
    QrMasterNgayKetThuc: TDateTimeField;
    QrMasterHanDuocSoHuu: TFloatField;
    QrMasterNgaySoHuu: TDateTimeField;
    QrMasterNguoiGiao: TWideStringField;
    QrMasterNguoiNhan: TWideStringField;
    QrMasterGhiChu: TWideStringField;
    QrMasterLK_TenThietBi: TWideStringField;
    QrMasterLK_Dvt: TWideStringField;
    QrMasterLK_MaThietBi_Nhom: TWideStringField;
    QrMasterLK_TenThietBi_Nhom: TWideStringField;
    QrMasterLK_SoThang_HanDuocSoHuu: TFloatField;
    QrMasterLK_Tennv: TWideStringField;
    CmdThietBi: TAction;
    QrMasterNhaCungCap: TWideStringField;
    QrMasterNhaSanXuat: TWideStringField;
    QrMasterMaQuocGia: TWideStringField;
    QrMasterModel: TWideStringField;
    QrMasterSerial: TWideStringField;
    QrMasterHanBaoHanh: TFloatField;
    QrMasterLK_TenThietBi_TA: TWideStringField;
    CmdQuocGia: TAction;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    QrMasterCalc_FileName: TWideStringField;
    QrMasterLK_TenQuocGia: TWideStringField;
    DsEmp: TDataSource;
    QrMasterNgayCapPhat: TDateTimeField;
    gbViecLam: TGroupBox;
    CbTenThietBi: TDbLookupComboboxEh2;
    EdMaThietBi: TDBEditEh;
    EdTenThietBi_TA: TDBEditEh;
    DBEditEh2: TDBEditEh;
    DBEditEh1: TDBEditEh;
    DBEditEh5: TDBEditEh;
    CbXuatXu: TDbLookupComboboxEh2;
    DBEditEh3: TDBEditEh;
    DBEditEh4: TDBEditEh;
    EdThietBi_Dvt: TDBEditEh;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DBNumberEditEh2: TDBNumberEditEh;
    Label8: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    CbToDate: TwwDBDateTimePicker;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    Label13: TLabel;
    Label12: TLabel;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    wwDBEdit7: TDBEditEh;
    wwDBEdit3: TDBEditEh;
    Label2: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    QrMasterCo_HoanTra: TBooleanField;
    QrMasterNgayTraTaiSan: TDateTimeField;
    QrMasterNguoiGiaoTraTaiSan: TWideStringField;
    QrMasterNguoiNhanTraTaiSan: TWideStringField;
    QrMasterSoLuongTraTaiSan: TFloatField;
    QrMasterLyDoTraTaiSan: TWideStringField;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    EdNSoLuongTraTaiSan: TDBNumberEditEh;
    dpNgayTraTaiSan: TwwDBDateTimePicker;
    ScrollInfo: TScrollBox;
    rDBCheckBox1: TrDBCheckBox;
    EdNguoiGiaoTraTaiSan: TDBEditEh;
    EdNguoiNhanTraTaiSan: TDBEditEh;
    EdLyDoTraTaiSan: TDBEditEh;
    QrMasterLK_Co_HoanTra: TBooleanField;
    QrMasterLK_ManvQL: TWideStringField;
    frEmp1: TfrEmp;
    QrMasterLK_CREATE_FULLNAME: TWideStringField;
    QrMasterLK_UPDATE_FULLNAME: TWideStringField;
    PaInfo: TPanel;
    PaNhanVien: TPanel;
    CbNhanVien: TDbLookupComboboxEh2;
    EdManvQL: TDBEditEh;
    QrMasterFileIdx: TGuidField;
    QrFileContentIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrMasterBeforeDelete(DataSet: TDataSet);
    procedure QrMasterBeforeInsert(DataSet: TDataSet);
    procedure QrMasterBeforePost(DataSet: TDataSet);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbReasonIDNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure QrMasterApprovalChange(Sender: TField);
    procedure CbSession2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure CmdPrint2Execute(Sender: TObject);
    procedure QrMasterMaHopDongChange(Sender: TField);
    procedure QrMasterAfterInsert(DataSet: TDataSet);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure QrMasterMaThietBiChange(Sender: TField);
    procedure QrMasterNgayBatDauChange(Sender: TField);
    procedure CmdThietBiExecute(Sender: TObject);
    procedure CmdQuocGiaExecute(Sender: TObject);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrMasterNgayCapPhatChange(Sender: TField);
    procedure QrMasterManvChange(Sender: TField);
    procedure QrMasterAfterDelete(DataSet: TDataSet);
  private
  	mCanEdit: Boolean;
    fTungay, fDenngay: TDateTime;
    fLevel, mType: Integer;
    mSQL, fStr, mEmpID, mEmpIDLabel, mEmpName: String;
    FORM_CODE, REPORT_NAME: String;
    r: WORD;
    mFileIdx: TGUID;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute(r: WORD; sEmpID : String = ''; sEmpIDLabel: String = ''; sEmpName: String = '');
  end;

var
  FrmThietBiTaiSan: TFrmThietBiTaiSan;

const
    TABLE_NAME = 'HR_LICHSU_THIETBI';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmHopdong, DmThietBiTaiSanLaoDong,
    Dmdl, HrData, HrExCommon; //MassRegLeave;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.Execute;
begin
	mCanEdit := rCanEdit(r);
    mEmpID := sEmpID;
    mEmpIDLabel := sEmpIDLabel;
    mEmpName := sEmpName;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.FormCreate(Sender: TObject);
begin
    AddAllFields(QrMaster, TABLE_NAME);
    FORM_CODE := 'HR_LICHSU_THIETBI';
    REPORT_NAME := 'HR_RP_LICHSU_THIETBI';
    mType := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.FormShow(Sender: TObject);
begin
    frD2D.Initial(Date, BeginMonth(Date), CmdRefresh);
    with DataMain do
        OpenDataSets([QrQuocgia]);

     with HrDataMain do
        OpenDataSets([QrDMNV, QrDM_THIETBI_TAISAN]);

    if mEmpID <> '' then
    begin
        TMyForm(Self).Init1;
        Width := 1260;
        Height := 712;
        Caption := 'Hồ Sơ Nhân Viên - Thiết Bị Tài Sản';
        fStr := mEmpID;
        frD2D.EdFrom.Date := Date;
        frD2D.EdTo.Date := Date;
        frD2D.Visible := False;
        frEmp1.Visible := True;
        frEmp1.Initial(mEmpID, mEmpIDLabel, mEmpName);
        FORM_CODE := 'HR_LICHSU_NHANVIEN_THIETBI';
        REPORT_NAME := 'HR_RP_LICHSU_NHANVIEN_THIETBI';
        mType := 1;
        CbNhanVien.ReadOnly := True;
        CbNhanVien.Color := clBtnFace;
        PaNhanVien.Visible := False;
        gbViecLam.Height := gbViecLam.Height - PaNhanVien.Height;
        PaThongTin.Height := PaThongTin.Height - PaNhanVien.Height;
    end
    else
    begin
        TMyForm(Self).Init2;
    end;

    SetDisplayFormat(QrMaster, sysCurFmt);
    SetShortDateFormat(QrMaster, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrMaster, FORM_CODE, Filter);

    with QrEmp do
    begin
        if sysIsDataAccess then
            SQL.Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        SQL.Add(' order by Manv');
        Open;
    end;

    with QrMaster.SQL do
    begin
        if mEmpID <> '' then
            Add(' and Manv =:Manv')
        else
        begin
           Add(' and [NgayCapPhat] between :Tungay and :Denngay' +
           ' and [Manv] in (select Manv from HR_DM_NHANVIEN where %s )');
        end;

        if sysIsDataAccess then
            Add(Format(' and Manv in (select Manv from dbo.fnDataRights2(%d, 4)) ', [sysLogonUID]));

        Add(' order by NgayBatDau, Manv');
        mSQL := Text;
    end;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrMaster, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrMaster, QrEmp]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdNewExecute(Sender: TObject);
begin
	QrMaster.Append;
    if mEmpID <> '' then
        CbTenThietBi.SetFocus
    else
        CbNhanVien.SetFocus;

    PaGhiChu.Collapsed := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdEditExecute(Sender: TObject);
begin
	QrMaster.Edit;
    if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
    else
    begin
        if mEmpID <> '' then
            CbTenThietBi.SetFocus
        else
            CbNhanVien.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdSaveExecute(Sender: TObject);
begin
	QrMaster.Post;
    with QrMaster do
        HrDataMain.capNhatHoSoNhanSuNgayTraTaiSan(FieldByName('Manv').AsString);
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdCancelExecute(Sender: TObject);
begin
	QrMaster.Cancel;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdDelExecute(Sender: TObject);
begin
	QrMaster.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsMaster);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdPrint2Execute(Sender: TObject);
var
    n: Integer;
    filename, repname: String;
begin
    n := (Sender as TComponent).Tag;

    repname := 'HR_RP_HDLD_WORD';
    filename := 'DOC\' +'HR_RP_HDLD_WORD_' + IntToStr(n) + '.docx';

    with QrMaster do
        DataOffice.CreateReport2(filename ,
            [sysLogonUID, TGuidEx.ToString(FieldByName('Idx'))], repname);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdPrintExecute(Sender: TObject);
begin
	if CmdSave.Enabled then
    	CmdSave.Execute;

    if mType <> 0 then
        fStr := mEmpID;

    ShowReport(Caption, REPORT_NAME, [sysLogonUID,
        mType, //0: Lich su, 1: HSNV/Lich Su
		FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdFrom.Date),
        FormatDateTime('yyyy,mm,dd hh:mm:ss', frD2D.EdTo.Date),
        fStr]);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdQuocGiaExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
     FrmDmdl.Execute(r, True);
    DataMain.QrQuocgia.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdRefreshExecute(Sender: TObject);
var
    sSQL, s: String;
begin
    if frD2D.CbOrg.Text = '' then
        s := ''
    else
    begin
        s := frD2D.CbOrg.SelectedNode.StringData;
        fLevel := frD2D.CbOrg.SelectedNode.Level;
	end;

   	if (fStr <> s) or
       (frD2D.EdFrom.Date <> fTungay) or
	   (frD2D.EdTo.Date   <> fDenngay) then
    begin
        fStr := s;
		fTungay  := frD2D.EdFrom.Date;
        fDenngay := frD2D.EdTo.Date;

	    Wait(DATAREADING);
		with QrMaster do
    	begin
        	s := Sort;
	    	Close;

            if mEmpID <> '' then
                Parameters[0].Value := mEmpID
            else
            begin
                sSQL := '1=1';

                if fStr <> '' then
                    case fLevel of
                    0:
                        sSQL := '[MaChiNhanh]=''' + fStr + '''';
                    1:
                        sSQL := '[MaPhongBan]=''' + fStr + '''';
                    2:
                        sSQL := '[MaBoPhan]=''' + fStr + '''';
                    end;


                SQL.Text := Format(mSQL, [sSQL]);
                Parameters[0].Value := fTungay;
                Parameters[1].Value := fDenngay;
            end;
    	    Open;
        end;
        if s = '' then
            s := 'NgayBatDau';

        SortDataSet(QrMaster, s);
		ClearWait;
    end;
    with GrList do
        if Enabled then
            SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdFileMinusExecute(Sender: TObject);
begin
    if  not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdFilePlusExecute(Sender: TObject);
begin
     Attach;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdThietBiExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_THIETBI_TAISAN_LAODONG');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmThietBiTaiSanLaoDong, FrmDmThietBiTaiSanLaoDong);
    FrmDmThietBiTaiSanLaoDong.Execute(r);

    HrDataMain.QrDM_THIETBI_TAISAN.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bViewFile: Boolean;
begin
    bViewFile := False;
    exActionUpdate(ActionList, QrMaster, Filter, mCanEdit);
	with QrMaster do
    begin
        if Active then
            bViewFile := FieldByName('FileExt').AsString <> '';

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdFileMinus.Enabled := bViewFile and not bBrowse;

    with QrFileContent do
    begin
        if Active then
        begin
            bViewFile := bViewFile and  (State in [dsBrowse]);
        end;
    end;

    CmdFileView.Enabled := bViewFile;
    CmdFilePlus.Enabled := not bBrowse;

    CmdClear.Enabled := (not bEmpty) and mCanEdit;

    frD2D.Enabled := bBrowse;
    GrList.Enabled := bBrowse;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_NGAY_TRATAISAN = 'Ngày trả tài sản không hợp lệ.';
procedure TFrmThietBiTaiSan.QrMasterBeforePost(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        if BlankConfirm(QrMaster, ['Manv', 'MaThietBi', 'NgayCapPhat']) then
            Abort;

        if (FieldByName('HanBaoHanh').AsFloat > 0) and
            BlankConfirm(QrMaster, ['NgayBatDau']) then
            Abort;

        if ((FieldByName('NgayTraTaiSan').AsFloat > 10) and (FieldByName('NgayTraTaiSan').AsDateTime < FieldByName('NgayCapPhat').AsDateTime)) then
        begin
            ErrMsg(RS_INVALID_NGAY_TRATAISAN);
            try
                dpNgayTraTaiSan.SelectAll;
                dpNgayTraTaiSan.SetFocus;
            except
            end;
            Abort;
        end;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterCalcFields(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrMaster.Active then
		Status.Panels[0].Text := exRecordCount(QrMaster, Filter);

    Status.Panels[2].Text := exStatusAudit(QrMaster);
    Status.Panels[1].Width := Width - (Status.Panels[0].Width + Status.Panels[2].Width);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.ApplicationEvents1Hint(Sender: TObject);
begin
    if Screen.ActiveForm = Self then
		Status.Panels[1].Text := Application.Hint;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrMaster.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterAfterInsert(DataSet: TDataSet);
begin
    with QrMaster do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Co_HoanTra').AsBoolean := False;
        if mEmpID <> '' then
            FieldByName('Manv').AsString := mEmpID;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterApprovalChange(Sender: TField);
begin
    exDotManv(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CbReasonIDNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CbSession2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdReloadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
    QrEmp.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdSwitchExecute(Sender: TObject);
begin
    if mEmpID <> '' then
    begin
        if ActiveControl = GrList then
            CbTenThietBi.SetFocus;
        GrList.SetFocus;
    end
    else
    begin
        if ActiveControl = GrList then
            CbNhanVien.SetFocus
        else if (ActiveControl = PaEmp) or (ActiveControl.Parent = PaEmp) then
            frD2D.EdFrom.SetFocus
        else
            GrList.SetFocus
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.CmdClearExecute(Sender: TObject);
begin
	if not exClearListConfirm then
    	Exit;

	Refresh;
	Wait(PROCESSING);
    DeleteConfirm(False);
	with QrMaster do
    begin
    	DisableControls;
    	if State in [dsBrowse] then
        else
        	Cancel;
    	while not Eof do
        	Delete;
    	EnableControls;
    end;
    DeleteConfirm(True);
	ClearWait;
    MsgDone;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterMaHopDongChange(Sender: TField);
begin
    with QrMaster, spHR_TINH_HETHAN_HDLD do
    begin
        if FieldByName('NgayHieuLuc').IsNull then
            FieldByName('NgayHetHan').Clear
		else
        begin
            Prepared := True;
            Parameters[1].Value := FieldByName('NgayHieuLuc').AsDateTime;
            Parameters[2].Value := FieldByName('MaHopDong').AsString;
            Execute;

            if Parameters[0].Value = 1 then
                FieldByName('NgayHetHan').Clear
            else
	            FieldByName('NgayHetHan').AsDateTime := Parameters[3].Value;
        end;

        if Sender.AsString <> '' then
            FieldByName('SoThang').AsFloat := FieldByName('LK_SoThang').AsFloat
        else
            FieldByName('SoThang').Clear;
    end;
end;

procedure TFrmThietBiTaiSan.QrMasterManvChange(Sender: TField);
begin
    EdManvQL.Text := EdManvQL.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterMaThietBiChange(Sender: TField);
begin
     with QrMaster do
     begin
        EdThietBi_Dvt.Text := EdThietBi_Dvt.Field.AsString;
        EdTenThietBi_TA.Text := EdTenThietBi_TA.Field.AsString;
        FieldByName('HanBaoHanh').AsFloat := FieldByName('LK_SoThang_HanBaoHanh').AsFloat;
        FieldByName('HanDuocSoHuu').AsFloat := FieldByName('LK_SoThang_HanDuocSoHuu').AsFloat;
        FieldByName('Co_HoanTra').AsBoolean := FieldByName('LK_Co_HoanTra').AsBoolean;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterNgayBatDauChange(Sender: TField);
var
    soThang, soThangSoHuu: Integer;
    ngayBatDau, ngayKetThuc: TDateTime;
begin
   with QrMaster do
   begin
        ngayBatDau := FieldByName('NgayBatDau').AsDateTime;
        soThang := FieldByName('HanBaoHanh').AsInteger;
        if (ngayBatDau = 0) or (soThang = 0) then
        begin
            FieldByName('NgayKetThuc').Clear;
        end else
        begin
            ngayKetThuc := IncMonth(ngayBatDau, soThang);
            FieldByName('NgayKetThuc').AsDateTime := ngayKetThuc;
        end;
   end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.QrMasterNgayCapPhatChange(Sender: TField);
var
    soThang, soThangSoHuu: Integer;
    ngayCapPhat, ngaySoHuu: TDateTime;
begin
   with QrMaster do
   begin
        ngayCapPhat := FieldByName('NgayCapPhat').AsDateTime;
        soThangSoHuu := FieldByName('HanDuocSoHuu').AsInteger;

        if (ngayCapPhat = 0) or (soThangSoHuu = 0) then
        begin
            FieldByName('NgaySoHuu').Clear;
        end else
        begin
            ngaySoHuu := IncMonth(ngayCapPhat, soThangSoHuu);
            FieldByName('NgaySoHuu').AsDateTime := ngaySoHuu;
        end;
   end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 486;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrMaster do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrMaster.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.Dettach();
begin
     with QrMaster do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThietBiTaiSan.ViewAttachment();
var
	s, sn: String;
begin
	with QrMaster do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

end.
