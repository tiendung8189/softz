(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmChucvu;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids,
  Db, Wwdbgrid, Wwdbcomb, ADODb,
  wwdblook, AppEvnts, Menus, AdvMenus, wwfltdlg, wwFltDlg2, wwDBGrid2, wwDialog,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbigrd, ToolWin;

type
  TFrmDmChucvu = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    DsNhom: TDataSource;
    CmdSearch: TAction;
    Panel2: TPanel;
    ApplicationEvents1: TApplicationEvents;
    QrNhom: TADOQuery;
    CmdPrint: TAction;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    PopCommon: TAdvPopupMenu;
    Tm1: TMenuItem;
    N1: TMenuItem;
    PgMain: TPageControl;
    tsBehaviour: TTabSheet;
    tsDep: TTabSheet;
    GrLydo: TwwDBGrid2;
    GrNhom: TwwDBGrid2;
    QrDm: TADOQuery;
    DsDm: TDataSource;
    Bevel1: TBevel;
    CbNhom: TwwDBLookupCombo;
    RefNhom: TADOQuery;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    Filter: TwwFilterDialog2;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrNhomCREATE_BY: TIntegerField;
    QrNhomUPDATE_BY: TIntegerField;
    QrNhomCREATE_DATE: TDateTimeField;
    QrNhomUPDATE_DATE: TDateTimeField;
    QrDmCREATE_BY: TIntegerField;
    QrDmUPDATE_BY: TIntegerField;
    QrDmCREATE_DATE: TDateTimeField;
    QrDmUPDATE_DATE: TDateTimeField;
    QrDmLK_NHOM: TWideStringField;
    CmdReload: TAction;
    QrNhomMaChucVu: TWideStringField;
    QrNhomTenChucVu: TWideStringField;
    QrNhomTenChucVu_TA: TWideStringField;
    QrDmMaChucDanh: TWideStringField;
    QrDmMaChucVu: TWideStringField;
    QrDmTenChucDanh: TWideStringField;
    QrDmTenChucDanh_TA: TWideStringField;
    QrDmPhuCap_ChucDanh: TFloatField;
    QrDmPhepNam_LuatDinh: TFloatField;
    QrDmPhepNam_ChucDanh: TFloatField;
    QrDmPhepNam_ThamNien: TFloatField;
    QrDmCo_PhepNamMotThang: TBooleanField;
    QrDmCo_PhepNamSoLe: TBooleanField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrNhomBeforePost(DataSet: TDataSet);
    procedure QrNhomBeforeDelete(DataSet: TDataSet);
    procedure QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNhomBeforeInsert(DataSet: TDataSet);
    procedure GrLydoCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure GrNhomCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrDmBeforePost(DataSet: TDataSet);
    procedure CbNhomNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrDmAfterInsert(DataSet: TDataSet);
    procedure QrNhomAfterPost(DataSet: TDataSet);
  private
  	mCanEdit, mTrigger, fixCodeLd, fixCodeNhom, mRefresh: Boolean;
    mQuery: TADOQuery;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmChucvu: TFrmDmChucvu;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, RepEngine, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

    AddFields(QrDm, 'HR_DM_CHUCDANH');
    AddFields(QrNhom, 'HR_DM_CHUCVU');

    mRefresh := False;
    mTrigger := False;
    mQuery := QrDm;

    SetCustomGrid(
    	['HR_DM_CHUCDANH', 'HR_DM_CHUCVU'], [GrLydo, GrNhom]);
    SetDictionary([QrDM, QrNhom], ['HR_DM_CHUCDANH', 'HR_DM_CHUCVU'], [Filter, nil]);

    // Ly do
    fixCodeLd := setCodeLength('HR_DM_CHUCDANH', QrDm.FieldByName('MaChucDanh'));

    // Nhom
    fixCodeNhom := SetCodeLength('HR_DM_CHUCVU', QrNhom.FieldByName('MaChucVu'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.FormShow(Sender: TObject);
begin
	GrLydo.ReadOnly := not mCanEdit;
    GrNhom.ReadOnly := not mCanEdit;
	OpenDataSets([QrNhom, QrDm]);

    SetDisplayFormat(QrDm, sysCurFmt);
    ActiveSheet(PgMain, -1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	CloseDataSets([QrNhom, QrDm]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(mQuery, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b : Boolean;
    n: Integer;
begin
	b := mQuery.State in [dsBrowse];
    n := PgMain.ActivePageIndex;
    CmdNew.Enabled 	  := b and mCanEdit;
    CmdSave.Enabled   := not b;
    CmdCancel.Enabled := not b;
    CmdDel.Enabled	  := b and mCanEdit and (not mQuery.IsEmpty);
    CmdSearch.Enabled := b;
    CmdClearFilter.Enabled := b and (Filter.FieldInfo.Count > 0);

    CmdFilter.Enabled := b and (n = 0);
    CmdFilter.Visible := n = 0;
    CmdClearFilter.Visible := n = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrDmAfterInsert(DataSet: TDataSet);
begin
    with QrDm do
    begin
        FieldByName('Co_PhepNamMotThang').AsBoolean := False;
        FieldByName('Co_PhepNamSoLe').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrDmBeforePost(DataSet: TDataSet);
begin
    begin
		if BlankConfirm(QrDm, ['MaChucDanh']) then
    		Abort;

        if fixCodeLd then
            if LengthConfirm(QrDm, ['MaChucDanh']) then
                Abort;

		if BlankConfirm(QrDm, ['TenChucDanh']) then
    		Abort;

		SetNull(QrDm, ['MaChucVu']);
    end;

    SetAudit(DataSet);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrNhomBeforePost(DataSet: TDataSet);
begin
    begin
	    if BlankConfirm(QrNhom, ['MaChucVu']) then
    	    Abort;

	    if fixCodeNhom then
    	    if LengthConfirm(QrNhom, ['MaChucVu']) then
        	    Abort;

	    if BlankConfirm(QrNhom, ['TenChucVu']) then
    	    Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrNhomAfterPost(DataSet: TDataSet);
begin
    mRefresh := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrNhomBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrNhomPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
        exSearch(Name + '_0', DsDm)
    else
        exSearch(Name + '_1', DsNhom)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.QrNhomBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.GrLydoCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MaChucDanh') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.GrNhomCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	if (Field.FullName = 'MaChucVu') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    if PgMain.ActivePageIndex = 0 then
    	Status.SimpleText := exRecordCount(mQuery, Filter)
    else
    	Status.SimpleText := RecordCount(mQuery);    
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, 'HR_DM_CHUCDANH', [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    AllowChange := mQuery.State in [dsBrowse];
    if not AllowChange then
        exCompleteConfirm;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.PgMainChange(Sender: TObject);
begin
	case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrDm;
			GrLydo.SetFocus;
            RefNhom.Requery;

            if mRefresh then
            begin
                exReSyncRecord(mQuery);
                mRefresh := False;
            end;
	    end;
    1:
	    begin
    	    mQuery := QrNhom;
        	GrNhom.SetFocus;
	    end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CbNhomNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmChucvu.CmdReloadExecute(Sender: TObject);
begin
    QrDm.Requery;
    QrNhom.Requery;
    RefNhom.Requery;
end;

end.
