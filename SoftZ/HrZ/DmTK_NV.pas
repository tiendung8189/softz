﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmTK_NV;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, System.Variants,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, Wwdbgrid, RzSplit, wwDialog, Wwdbigrd, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdbdatetimepicker, wwcheckbox, wwdblook, DBCtrlsEh,
  DBGridEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmDmTK_NV = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    EdMA: TDBEditEh;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    QrDanhmucMATK: TWideStringField;
    QrDanhmucGHICHU: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    QrDanhmucNGAY: TDateTimeField;
    QrDanhmucMANV: TWideStringField;
    QrDanhmucMANH: TWideStringField;
    QrDanhmucMANH_CN: TWideStringField;
    CbNganHang: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    CbChinhanh: TDbLookupComboboxEh2;
    EdChiNhanh: TDBEditEh;
    QrDanhmucLK_MANH_CN_MA: TWideStringField;
    DsNganhangCN: TDataSource;
    QrNganhangCN: TADOQuery;
    spHR_TAIKHOAN_NGANHANG_NGAYSUDUNG_HOPLE: TADOCommand;
    QrDanhmucIdx: TAutoIncField;
    QrDanhmucLK_NGANHANG: TWideStringField;
    QrDanhmucLK_CHINHANH: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucMATKChange(Sender: TField);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure CbChinhanhBeforeDropDown(Sender: TObject);
    procedure CbChinhanhDropDown(Sender: TObject);
    procedure QrDanhmucMANHChange(Sender: TField);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
  private
  	mCanEdit, fixCode, mClose, mRet: Boolean;
    mSql, mManv: String;
    function UsedDateValid(pManv, pMaNH: String; pNgay: TDateTime; pIdx: Integer): Boolean;
  public
  	function Execute(r: WORD; pManv: string = ''; pClose: Boolean = True): Boolean;
  end;

var
  FrmDmTK_NV: TFrmDmTK_NV;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_TAIKHOAN';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmTK_NV.Execute(r: WORD; pManv: string; pClose: Boolean): Boolean;
begin
	mCanEdit := rCanEdit(r);
	DsDanhmuc.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    mManv := pManv;
    mClose := pClose;
    ShowModal;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDanhmuc, 'HR_DM_TAIKHOAN');

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');
    mSql := QrDanhmuc.SQL.Text;

	fixCode := SetCodeLength(FORM_CODE, QrDanhmuc.FieldByName('MATK'));
    mTrigger := False;
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.FormShow(Sender: TObject);
begin
    with DataMain do
        OpenDataSets([QrNganhang, QrNganhangCN]);

    OpenDataSets([QrNganhangCN]);
    if mManv <> '' then
        mSql := mSql + ' and MANV = ''' + mManv + '''';

    mSql := mSql + ' order by NGAY';

    with QrDanhmuc do
    begin
        SQL.Text := mSql;
        Open;
    end;

    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
            CloseDataSets(DataMain.Conn)
        else
        begin
            CloseDataSets([QrDanhmuc]);
        end;
    finally
    end;

	// Save state panel
    RegWrite(Name, 'PD2', PD2.Collapsed);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

procedure TFrmDmTK_NV.CbChinhanhDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MANH').AsString;
    QrNganhangCN.Filter := 'MANH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Date;
    with QrDanhmuc do
    begin
        FieldByName('NGAY').AsDateTime := d;
        FieldByName('MANV').AsString := mManv;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
	    if BlankConfirm(QrDanhmuc, ['MATK', 'MANH']) then
   			Abort;

	    if fixCode then
			if LengthConfirm(QrDanhmuc, ['MATK']) then
        		Abort;

	    if BlankConfirm(QrDanhmuc, ['NGAY']) then
   			Abort;

        if not UsedDateValid(FieldByName('MANV').AsString,
            FieldByName('MANH').AsString,
            FieldByName('NGAY').AsDateTime,
            FieldByName('Idx').AsInteger) then
            Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucMANHChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        FieldByName('MANH_CN').Clear;
        EdChiNhanh.Text := EdChiNhanh.Field.AsString;
    end;
end;
procedure TFrmDmTK_NV.QrDanhmucMATKChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 467;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MATK' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CbChinhanhBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MANH').AsString;
    (Sender as TwwDBLookupCombo).LookupTable.Filter := Format('MANH=''%s''', [s]);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTK_NV.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmTK_NV.UsedDateValid;
var
    s: String;
begin
    with spHR_TAIKHOAN_NGANHANG_NGAYSUDUNG_HOPLE do
    begin
        Prepared := True;
        Parameters[2].Value := pIdx;
        Parameters[3].Value := pManv;
        Parameters[4].Value := pMaNH;
        Parameters[5].Value := pNgay;
        try
            Execute;
        except
        end;
        s := Parameters[1].Value;
        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

end.
