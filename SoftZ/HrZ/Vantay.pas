﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Vantay;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  Grids, Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook,
  wwfltdlg,
  fctreecombo, wwFltDlg2, wwDBGrid2,
  zkemkeeper_TLB, Buttons, wwDialog, fcCombo, OleCtrls, Wwdbigrd, ToolWin;

type
  TFrmVantay = class(TForm)
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ActionList: TActionList;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PaList: TPanel;
    PaFilter: TPanel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdReload: TAction;
    CmdSwitch: TAction;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopList: TAdvPopupMenu;
    Gnbiutng1: TMenuItem;
    Xabiutng1: TMenuItem;
    N8: TMenuItem;
    Canhchnh1: TMenuItem;
    N4: TMenuItem;
    ItemShowCurrent: TMenuItem;
    ItemShowTerminated: TMenuItem;
    ItemShowAll: TMenuItem;
    CmdConnect: TAction;
    CmdUploadUser: TAction;
    CmdClearUser: TAction;
    CmdFinger: TAction;
    CmdUpload1: TAction;
    CmdClear1: TAction;
    CmdDownloadTmp: TAction;
    CmdUploadTmp: TAction;
    CmdClearFP: TAction;
    CmdDeleteFP: TAction;
    CmdInit: TAction;
    Label1: TLabel;
    CbMay: TwwDBLookupCombo;
    SpeedButton1: TSpeedButton;
    GrList: TwwDBGrid2;
    PopRec: TAdvPopupMenu;
    Cpnhtdanhschnhnvinvomy1: TMenuItem;
    Xanhnvinkhimy1: TMenuItem;
    CpnhtmuvntaytCSDLvomy1: TMenuItem;
    Xanhnvinkhimy2: TMenuItem;
    LyttcmuvntaytmyvCSDL1: TMenuItem;
    XavntaytrongCSDL1: TMenuItem;
    Khitolimy1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    BtnProc: TToolButton;
    ToolButton1: TToolButton;
    QrFP1: TADOQuery;
    QrFP2: TADOQuery;
    N5: TMenuItem;
    ItemUpload1: TMenuItem;
    ItemClear1: TMenuItem;
    Lyvntay1: TMenuItem;
    N6: TMenuItem;
    QrDanhmucFP1: TBooleanField;
    QrDanhmucFP2: TBooleanField;
    QrDanhmucFP3: TBooleanField;
    QrDanhmucFP4: TBooleanField;
    QrDanhmucFP5: TBooleanField;
    QrDanhmucFP6: TBooleanField;
    QrDanhmucFP7: TBooleanField;
    QrDanhmucFP8: TBooleanField;
    QrDanhmucFP9: TBooleanField;
    QrDanhmucFP10: TBooleanField;
    QrDanhmucMANV: TWideStringField;
    QrDanhmucTENNV: TWideStringField;
    QrDanhmucMaPhongBan: TWideStringField;
    QrDanhmucDTHOAI: TWideStringField;
    QrDanhmucNGAY_THOIVIEC: TDateTimeField;
    QrDanhmucNGAY_VAOLAM: TDateTimeField;
    QrDanhmucLK_TenPhongBan: TWideStringField;
    QrDanhmucMACC: TWideStringField;
    CmdUploadTmp1NV: TAction;
    CmdDownloadTmp1NV: TAction;
    N7: TMenuItem;
    ItemDownload2: TMenuItem;
    ItemUpload2: TMenuItem;
    CZKEM1: TCZKEM;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure QrDanhmucDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CbOrgCloseUp(Sender: TObject; Select: Boolean);
    procedure CmdReloadExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure ItemShowCurrentClick(Sender: TObject);
    procedure PopListPopup(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrDanhmucBeforeEdit(DataSet: TDataSet);
    procedure CmdConnectExecute(Sender: TObject);
    procedure CmdUploadUserExecute(Sender: TObject);
    procedure CmdClearUserExecute(Sender: TObject);
    procedure BtnProcClick(Sender: TObject);
    procedure CmdDownloadTmpExecute(Sender: TObject);
    procedure CmdFingerExecute(Sender: TObject);
    procedure CmdUpload1Execute(Sender: TObject);
    procedure CmdClear1Execute(Sender: TObject);
    procedure CmdUploadTmpExecute(Sender: TObject);
    procedure CmdClearFPExecute(Sender: TObject);
    procedure CmdDeleteFPExecute(Sender: TObject);
    procedure CmdInitExecute(Sender: TObject);
    procedure CZKEM1HIDNum(ASender: TObject; CardNumber: Integer);
    procedure CZKEM1Finger(Sender: TObject);
    procedure CZKEM1EnrollFinger(ASender: TObject; EnrollNumber, FingerIndex,
      ActionResult, TemplateLength: Integer);
    procedure CmdUploadTmp1NVExecute(Sender: TObject);
    procedure CmdDownloadTmp1NVExecute(Sender: TObject);
  private
  	mCanEdit: Boolean;
    mShow: Integer;
    mEmpSQL, fStr: String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmVantay: TFrmVantay;

implementation

{$R *.DFM}



uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, zkem, isStr,
  DevFP, DevFP2, HrData;

const
	FORM_CODE = 'HR_DM_NHANVIEN_VANTAY';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    SetCustomGrid(FORM_CODE, GrList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.FormShow(Sender: TObject);
begin
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);

    mEmpSQL := QrDanhmuc.SQL.Text;
    fStr := '~';
    mShow := 0;
    with DataMain.QrDM_MAYCC do
    begin
    	Open;
        if not IsEmpty then
	    	CbMay.LookupValue := FieldByName('Idx').Value;
    end;

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrDanhmuc, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    CloseDataSets([QrDanhmuc]);
    zkDisconnect;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdSaveExecute(Sender: TObject);
begin
	QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdCancelExecute(Sender: TObject);
begin
	QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdConnectExecute(Sender: TObject);
begin
	if CmdConnect.Tag = 1 then
    begin
    	zkDisConnect;
        CmdConnect.Tag := 0;
        Exit;
    end;

    // Connecting
    with DataMain.QrDM_MAYCC do
    	if zkConnect(CZKEM1,
			FieldByName('COM_TYPE').AsString,
			FieldByName('IP4').AsString,
            FieldByName('SOCKET').AsInteger,
			FieldByName('PORT').AsInteger,
			FieldByName('Idx').AsInteger,
           	FieldByName('BAUD_RATE').AsInteger) then
		begin
    		CmdConnect.Tag := 1;
        	zkRegEvent(EF_HIDNUM);
		end
    	else
    	begin
    		CmdConnect.Tag := 0;
    	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdDeleteFPExecute(Sender: TObject);
var
    enroll, n1, n2, n: Integer;
    bm: TBookMark;
begin
	if not YesNo('Xóa vân tay của tất cả nhân viên trên màn hình khỏi CSDL. Tiếp tục?', 1) then
    	Exit;

    with QrDanhmuc do
    begin
		StartProgress(RecordCount);
    	DisableControls;
        bm := BookMark;
        First;

        n1 := 0;	// Enroll
        n2 := 0;	// Finger
        while not Eof do
        begin
        	enroll := StrToIntDef(FieldByName('MaCC').AsString, 0);
			DataMain.Conn.Execute(Format('delete HR_DM_NHANVIEN_VANTAY where cast(Manv as Int) = %d', [enroll]), n);
            Inc(n2, n);
            if n > 0 then
            	Inc(n1);
			IncProgress;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;
	StopProgress;
    Refresh;
    Msg(Format('Đã xóa %d vân tay / %d nhân viên khỏi CSDL.', [n2, n1]));
	if (n1 <> 0) or (n2 <> 0) then
    	CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdDownloadTmp1NVExecute(Sender: TObject);
var
	ls: TStrings;
    finger, n2, n3: Integer;
	enroll, tmp, tennv: WideString;
    sName: String;
    b: Boolean;
begin
	ls := TStringList.Create;
    Screen.Cursor := crHourGlass;
    zkGetUsers(ls);
    Screen.Cursor := crDefault;

    n2 := 0;	// new
    n3 := 0;	// update
    StartProgress(ls.Count);

    	enroll := QrDanhmuc.FieldByName('MANV').AsString;
        tennv := QrDanhmuc.FieldByName('TENNV').AsString;
        sName := ls.Values[enroll];

        b := False;
		for finger := 0 to 9 do
        begin
	    	SetProgressDesc(Format('%s [%d]', [sName, finger]));
	        if zkGetUserTmpStr(enroll, finger, tmp) then
			begin
            	// Save to database
                with QrFP1 do
                begin
                    Close;
                    Parameters[0].Value := enroll;
                    Parameters[1].Value := finger;
                    Open;
                    if IsEmpty then
                    begin
                    	Append;
                        FieldByName('MANV').Value := enroll;
                        FieldByName('Finger').Value := finger;
                        Inc(n2);
                    end
					else
                    begin
                    	Edit;
                        Inc(n3);
                    end;

					FieldByName('Template').Value := tmp;
					Post;
                    Close;
                end;
            end;
        end;
		IncProgress;

    ls.Free;
    StopProgress;
    Msg(Format('Đã thêm %d vân tay mới, cập nhật %d vân tay cũ của nhân viên "%s".', [n2, n3, tennv]));
	if (n2 <> 0) or (n3 <> 0) then
    begin
    	CmdReload.Execute;
        QrDanhmuc.Locate('MANV', enroll, []);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdDownloadTmpExecute(Sender: TObject);
var
	ls: TStrings;
    finger, i, n1, n2, n3: Integer;
	enroll, tmp: WideString;
    sName: String;
    b: Boolean;
begin
	ls := TStringList.Create;
    Screen.Cursor := crHourGlass;
    zkGetUsers(ls);
    Screen.Cursor := crDefault;

    n1 := 0;	// enroll
    n2 := 0;	// new
    n3 := 0;	// update
    StartProgress(ls.Count);
    for i := 0 to ls.Count - 1 do
    begin
    	enroll := ls.Names[i];
        sName := ls.ValueFromIndex[i];

        b := False;
		for finger := 0 to 9 do
        begin
	    	SetProgressDesc(Format('%s [%d]', [sName, finger]));
	        if zkGetUserTmpStr(enroll, finger, tmp) then
			begin
            	b := True;

            	// Save to database
                with QrFP1 do
                begin
                    Close;
                    Parameters[0].Value := enroll;
                    Parameters[1].Value := finger;
                    Open;
                    if IsEmpty then
                    begin
                    	Append;
                        FieldByName('MANV').Value := enroll;
                        FieldByName('Finger').Value := finger;
                        Inc(n2);
                    end
					else
                    begin
                    	Edit;
                        Inc(n3);
                    end;

					FieldByName('Template').Value := tmp;
					Post;
                    Close;
                end;
            end;
        end; {for}
        if b then
            Inc(n1);
		IncProgress;
    end; {scan list}

    ls.Free;
    StopProgress;
    Msg(Format('Đã thêm %d vân tay mới, cập nhật %d vân tay cũ của %d nhân viên.', [n2, n3, n1]));
	if (n1 <> 0) or (n2 <> 0) or (n3 <> 0) then
    	CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdRefreshExecute(Sender: TObject);
var
	mSQL, s: String;
    fLevel: Integer;
begin
    Wait(DATAREADING);
    with QrDanhmuc do
    begin
        case mShow of
        0:
         	mSQL := mEmpSQL + 'and isnull(NgayThoiViec,0)=0';
        1:
         	mSQL := mEmpSQL + 'and isnull(NgayThoiViec,0)>0';
        2:
         	mSQL := mEmpSQL + 'and 1=1';
        end;

    	Close;
		SQL.Text := mSQL + ' order by MANV';
        Open;
    end;
    ClearWait;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdFilterExecute(Sender: TObject);
begin
    Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdFingerExecute(Sender: TObject);
var
	empId, s: WideString;
    n: Integer;
    b: Boolean;
begin
	empId := QrDanhmuc.FieldByName('MANV').AsString;
    if not zkGetUser(empId, s, s, n, b) then
    begin
        ErrMsg('Nhân viên chưa có danh sách trong máy.');
        Exit;
    end;

	zkRegEvent(EF_HIDNUM + EF_ENROLLFINGER + EF_FINGER);
	Application.CreateForm(TFrmDevFP, FrmDevFP);
    FrmDevFP.Execute(empId);
	zkRegEvent(EF_HIDNUM);
    zkStartIdentify;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdInitExecute(Sender: TObject);
begin
	if not YesNo('Xóa toàn bộ dữ liệu trong máy. Tiếp tục?') then
    	Exit;
	GrList.SetFocus;
    MsgDone(zkClearKeeperData);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdClear1Execute(Sender: TObject);
begin
    if zkDeleteUser(QrDanhmuc.FieldByName('MANV').AsString) then
        MsgDone
    else
        ErrMsg('Lỗi hoặc không có nhân viên này trong máy.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdClearFPExecute(Sender: TObject);
var
    enroll: String;
    finger: Integer;
    bm: TBookMark;
    mBatchMode: Boolean;
begin
	if not YesNo('Xóa vân tay của tất cả nhân viên trên màn hình'#13'khỏi máy chấm công"' +
    	CbMay.DisplayValue + '". Tiếp tục?', 1) then
    	Exit;

    with QrDanhmuc do
    begin
    	mBatchMode := False; // zkBeginBatch(1);
    	StartProgress(RecordCount);
        bm := BookMark;
    	DisableControls;
        First;

        while not Eof do
        begin
        	enroll := FieldByName('MANV').AsString;
            for finger := 0 to 9 do		// It makes me banana :(
            begin
                SetProgressDesc(Format('%s [%d]', [FieldByName('TENNV').AsString, finger]));
                zkDelUserTmp(enroll, finger);
    		end;
			IncProgress;
            Next;
        end;

        if mBatchMode then
        begin
			zkBatchUpdate;
            zkRefreshData;
            zkReconnect;
        end;

		StopProgress;
        BookMark := bm;
        EnableControls;
    end;
    Msg('Đã xóa vân tay của danh sách nhân viên khỏi máy.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdClearUserExecute(Sender: TObject);
var
    bm: TBookMark;
    b: Boolean;
    sLog: String;
    empId, empName: WideString;
begin
	if not YesNo('Xóa danh sách nhân viên trên màn hình khỏi máy "' +
    	CbMay.DisplayValue + '". Tiếp tục?', 1) then
        Exit;

    with QrDanhmuc do
    begin
		b := False; // zkBeginBatch(1);
		StartProgress(RecordCount);
        bm := BookMark;
    	DisableControls;
        First;
        sLog := '';
        while not Eof do
        begin
        	empName := FieldByName('TENNV').AsString;
            empId := FieldByName('MANV').AsString;
            SetProgressDesc(empName);

            if zkDeleteUser(empId) then
            else
                sLog := sLog + empId + ': ' + empName + #13;

			IncProgress;
            Next;
        end;

        if b then
        begin
            zkBatchUpdate;
            zkRefreshData;
            zkReconnect;
        end;
		StopProgress;
        BookMark := bm;
        EnableControls;
    end;

    if sLog = '' then
	    Msg('Đã xóa danh sách nhân viên khỏi máy.')
    else
    	exViewLog('Lỗi', sLog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, b: Boolean;
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;
    PaFilter.Enabled := bBrowse;

    with CmdConnect do
    begin
	   	b := Tag = 1;
    	if b then
		    CmdConnect.Caption := ('Ngắt kết nối')
        else
		    CmdConnect.Caption := ('Kết nối');
        CbMay.Enabled := not b;
        CmdUpload1.Enabled := b;
        CmdClear1.Enabled := b;
        CmdFinger.Enabled := b;

		CmdUploadUser.Enabled := b;
        CmdClearUser.Enabled := b;
        CmdClearFP.Enabled := b;
        CmdDownloadTmp.Enabled := b;
        CmdUploadTmp.Enabled := b;
        CmdDownloadTmp1NV.Enabled := b;
        CmdUploadTmp1NV.Enabled := b;
        CmdInit.Enabled := b;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.QrDanhmucDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	with Sender as TwwDBGrid2 do
    begin
	    if State = [gdFixed] then
        begin
			ABrush.Color := TitleColor;
			AFont.Color := Font.Color;
            AFont.Size := Font.Size;
            AFont.Style := [];
	    end;
    end;

	if Highlight then
    	Exit;

    if QrDanhmuc.FieldByName('NgayThoiViec').AsDateTime > 0 then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if QrDanhmuc.Active then
		Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

procedure TFrmVantay.BtnProcClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CbOrgCloseUp(Sender: TObject; Select: Boolean);
begin
    if Select then
        CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdReloadExecute(Sender: TObject);
begin
    fStr := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdSwitchExecute(Sender: TObject);
begin
    GrList.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdUpload1Execute(Sender: TObject);
begin
    with QrDanhmuc do
		MsgDone(zkSetUser(
        	FieldByName('MANV').AsString,
        	FieldByName('MACC').AsString,
            UpperCase(isStripToneMark(FieldByName('TENNV').AsString))))
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdUploadTmp1NVExecute(Sender: TObject);
var
    finger, n: Integer;
    b, mBatchMode: Boolean;
    enroll, data: WideString;
    tennv: String;
begin
    enroll := QrDanhmuc.FieldByName('MANV').AsString;
    tennv := QrDanhmuc.FieldByName('TENNV').AsString;
    if not YesNo(Format('Cập nhật vân tay của nhân viên "%s" '#13'từ CSDL vào máy "', [tennv]) +
    	CbMay.DisplayValue + '". Tiếp tục?', 1) then
    	Exit;

    mBatchMode := zkBeginBatch(1);
    n := 0;
    with QrFP2 do
    begin
        Close;
        Parameters[0].Value := enroll;
        Open;

        First;
        while not Eof do
        begin
            data := FieldByName('Template').Value;
            finger := FieldByName('Finger').Value;
            if zkSetUserTmpStr(enroll, finger, data) then
                Inc(n);
            Next;
        end;
        Close;
    end;

    if mBatchMode then
    begin
        zkBatchUpdate;
        zkRefreshData;
        zkReconnect;
    end;

    Msg(Format('Đã cập nhật %d vân tay.', [n]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdUploadTmpExecute(Sender: TObject);
var
	bm: TBookMark;
    finger, n1, n2: Integer;
    b, mBatchMode: Boolean;
    enroll, data: WideString;
begin
	if not YesNo('Cập nhật vân tay của tất cả nhân viên trên màn hinh'#13'từ CSDL vào máy "' +
    	CbMay.DisplayValue + '". Tiếp tục?', 1) then
    	Exit;

    with QrDanhmuc do
    begin
    	mBatchMode := zkBeginBatch(1);
        bm := BookMark;
    	DisableControls;
        First;

        n1 := 0;
        n2 := 0;
	    StartProgress(RecordCount);
        while not Eof do
        begin
        	SetProgressDesc(FieldByName('TENNV').AsString);
            enroll := FieldByName('MANV').AsString;
            b := False;
            with QrFP2 do
            begin
                Close;
                Parameters[0].Value := enroll;
                Open;

                while not Eof do
                begin
                    data := FieldByName('Template').Value;
                    finger := FieldByName('Finger').Value;
                    if zkSetUserTmpStr(enroll, finger, data) then
                    begin
                        b := True;
                        Inc(n2);
                    end;
                    Next;
                end;
                Close;
            end;

            if b then
                Inc(n1);

        	IncProgress;
        	Next;
        end;
        if mBatchMode then
        begin
			zkBatchUpdate;
            zkRefreshData;
            zkReconnect;
        end;
        StopProgress;
        BookMark := bm;
    	EnableControls;
    end;
    Msg(Format('Đã cập nhật %d vân tay / %d nhân viên.', [n2, n1]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CmdUploadUserExecute(Sender: TObject);
var
	empId, cardId, empName: WideString;
    sLog: String;
    bm: TBookMark;
    b: Boolean;
begin
	if not YesNo('Cập nhật danh sách nhân viên trên màn hình vào máy "' +
    	CbMay.DisplayValue + '". Tiếp tục?', 1) then
    	Exit;

    sLog := '';
	with QrDanhmuc do
    begin
		b := False; // zkBeginBatch(1);
    	StartProgress(RecordCount);
        bm := BookMark;
    	DisableControls;
        First;
        while not Eof do
        begin
        	empId  := FieldByName('MANV').AsString;
        	cardId := FieldByName('MACC').AsString;
            empName := FieldByName('TENNV').AsString;
        	SetProgressDesc(empName);

            if zkSetUser(empId, cardId, UpperCase(isStripToneMark(empName))) then
            else
                sLog := sLog + empId + ': ' + empName + #13;

            IncProgress;
        	Next;
        end;
        if b then
        begin
			zkBatchUpdate;
            zkRefreshData;
            zkReconnect;
        end;
        StopProgress;
        BookMark := bm;
        EnableControls;
    end;

    if sLog = '' then
	    Msg('Đã cập nhật danh sách nhân viên vào máy.')
    else
    	exViewLog('Lỗi', sLog);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CZKEM1EnrollFinger(ASender: TObject; EnrollNumber,
  FingerIndex, ActionResult, TemplateLength: Integer);
begin
	if Assigned(FrmDevFP2) then
    	try
		    SendMessage(FrmDevFP2.Handle, WM_ENROLL_FINGER, ActionResult, 0);
        except
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CZKEM1Finger(Sender: TObject);
begin
	if Assigned(FrmDevFP2) then
    	try
		    SendMessage(FrmDevFP2.Handle, WM_FINGER, 0, 0);
        except
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.CZKEM1HIDNum(ASender: TObject; CardNumber: Integer);
begin
	with QrDanhmuc do
    begin
    	if Eof then
        	Exit;
		Edit;
        FieldByName('MACC').AsString := IntToStr(CardNumber);	// vlInt2Str(CardNumber, 10);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.ItemShowCurrentClick(Sender: TObject);
begin
	mShow := (Sender as TComponent).Tag;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.PopListPopup(Sender: TObject);
var
	b: Boolean;
    s: String;
begin
	ItemShowCurrent.Checked := mShow = 0;
	ItemShowTerminated.Checked := mShow = 1;
	ItemShowAll.Checked := mShow = 2;

    b := QrDanhmuc.State in [dsBrowse];
	ItemShowCurrent.Enabled := b;
	ItemShowTerminated.Enabled := b;
	ItemShowAll.Enabled := b;

    s := QrDanhmuc.FieldByName('TENNV').AsString;
    ItemUpload1.Caption := (Format('Cập nhật "%s" vào máy', [s]));
    ItemClear1.Caption := (Format('Xóa "%s" khỏi máy', [s]));

    ItemUpload2.Caption := (Format('Cập nhật mẫu vân tay "%s" từ CSDL vào máy', [s]));
    ItemDownload2.Caption := (Format('Lấy mẫu vân tay nhân viên "%s" về CSDL', [s]));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmVantay.QrDanhmucBeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;
end;
end.
