﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit LichLamViecTools;

interface

uses
  SysUtils, Classes, Controls, Forms, System.Variants,
  Wwdbgrid, ExtCtrls, Menus, AdvMenus, ActnList, AppEvnts,
  Db, ADODB, ComCtrls, StdCtrls, wwdblook, DBCtrls,
  wwdbdatetimepicker, wwfltdlg, wwdbedit,
  Wwdbcomb, fctreecombo, wwFltDlg2, wwDBGrid2, frameD2D, wwDialog, Grids,
  Wwdbigrd, Wwdotdot, Mask, Graphics, ToolWin, pngimage, AdvCombo, AdvDBComboBox,
  AdvEdit, DBAdvEd, RzEdit, RzDBEdit, RzDBBnEd, Winapi.ShellAPI, Winapi.Windows,
  isPanel, RzPanel, RzSplit, rDBComponents, Vcl.Buttons, DBCtrlsEh, DBGridEh,
  DBLookupEh, frameEmp, DbLookupComboboxEh2, fcCombo, kbmMemTable, DateUtils,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh;

type
  TFrmLichLamViecTools = class(TForm)
    ActionList: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PaEmp: TPanel;
    QrEmp: TADOQuery;
    CmdImport: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    CmdClear: TAction;
    Panel1: TPanel;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    DsEmp: TDataSource;
    Panel3: TPanel;
    BtnContinute: TBitBtn;
    BitBtn2: TBitBtn;
    GrList: TStringGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    RgLoai: TRadioGroup;
    Panel2: TPanel;
    CbPhongBan: TDbLookupComboboxEh2;
    CbBoPhan: TDbLookupComboboxEh2;
    EdMaPhongBan: TDBEditEh;
    EdMaBoPhan: TDBEditEh;
    CbNhanVien: TDbLookupComboboxEh2;
    EdMaNhanVien: TDBEditEh;
    QrSite: TADOQuery;
    QrDep: TADOQuery;
    QrSec: TADOQuery;
    DsSec: TDataSource;
    DsDep: TDataSource;
    DsSite: TDataSource;
    CmdContinue: TAction;
    CmdLyDoVangMat: TAction;
    spIMP_HR_LICHLV_ImportList: TADOCommand;
    TbDummy: TkbmMemTable;
    DsDummy: TDataSource;
    TbDummyMaCa: TWideStringField;
    TbDummyGioVao_Ca: TDateTimeField;
    TbDummyGioRa_Ca: TDateTimeField;
    TbDummySoGio_Ca: TFloatField;
    DsEmpStatus: TDataSource;
    QrEmpStatus: TADOQuery;
    TbDummyNgay: TDateTimeField;
    CmdImportExcel: TAction;
    Panel7: TPanel;
    Panel8: TPanel;
    BitBtn4: TBitBtn;
    Panel9: TPanel;
    GrListData: TwwDBGrid2;
    spIMP_HR_DANGKY_LICHLV_UpInsertData: TADOCommand;
    spIMP_HR_LICHLV_DeleteList: TADOCommand;
    CHECK_RECORD_ERROR: TADOCommand;
    Filter: TwwFilterDialog2;
    QrList: TADOQuery;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_Tennv: TWideStringField;
    QrListManv: TWideStringField;
    QrListManvQL: TWideStringField;
    QrListTennv: TWideStringField;
    QrListNgay: TDateTimeField;
    DsList: TDataSource;
    CmdRegister: TAction;
    CmdDelRecordError: TAction;
    CmdDelRecordSelected: TAction;
    CmdRefresh: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Xadng1: TMenuItem;
    Xaccdngangli1: TMenuItem;
    PgMain: TPageControl;
    TaDayOff: TTabSheet;
    TaOvertime: TTabSheet;
    Panel11: TPanel;
    PaInfo: TisPanel;
    CbCaLamViec: TDbLookupComboboxEh2;
    Label13: TLabel;
    dpGioVao: TwwDBDateTimePicker;
    Label2: TLabel;
    dpGioRa1: TwwDBDateTimePicker;
    DBNumberEditEh1: TDBNumberEditEh;
    Panel10: TPanel;
    QrListMaCa: TWideStringField;
    QrListTenCa: TWideStringField;
    QrListNam: TIntegerField;
    QrListThang: TIntegerField;
    QrListSoCa: TIntegerField;
    QrListLoaiNgay: TIntegerField;
    QrListGioVao_Ca: TDateTimeField;
    QrListGioRa_Ca: TDateTimeField;
    QrListGioVao1_Ca: TDateTimeField;
    QrListGioRa1_Ca: TDateTimeField;
    QrListSoGio_Ca: TFloatField;
    QrListDiTre_Ca: TDateTimeField;
    QrListVeSom_Ca: TDateTimeField;
    QrListOTDauCa_TuGio: TDateTimeField;
    QrListOTDauCa_DenGio: TDateTimeField;
    QrListOTGiuaCa_TuGio: TDateTimeField;
    QrListOTGiuaCa_DenGio: TDateTimeField;
    QrListOTCuoiCa_TuGio: TDateTimeField;
    QrListOTCuoiCa_DenGio: TDateTimeField;
    TbDummyLK_GioVao: TDateTimeField;
    TbDummyLK_GioRa: TDateTimeField;
    TbDummyLK_GioVao1: TDateTimeField;
    TbDummyLK_GioRa1: TDateTimeField;
    TbDummyLK_SoGio: TIntegerField;
    DsGetAllSaturdayByMonth: TDataSource;
    Panel12: TPanel;
    isPanel2: TisPanel;
    Panel13: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    CbCaLamViecTangCa: TDbLookupComboboxEh2;
    DBNumberEditEh3: TDBNumberEditEh;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    wwDBDateTimePicker2: TwwDBDateTimePicker;
    Label108: TLabel;
    CbDate: TwwDBDateTimePicker;
    Label4: TLabel;
    dpDangKyTuGio: TwwDBDateTimePicker;
    Label5: TLabel;
    dpDangKyDenGio: TwwDBDateTimePicker;
    TbDummyOTDauCa_TuGio: TDateTimeField;
    TbDummyOTDauCa_DenGio: TDateTimeField;
    TbDummyOTGiuaCa_TuGio: TDateTimeField;
    TbDummyOTGiuaCa_DenGio: TDateTimeField;
    TbDummyOTCuoiCa_TuGio: TDateTimeField;
    TbDummyOTCuoiCa_DenGio: TDateTimeField;
    TbDummyOTDauCa_SoGio: TFloatField;
    TbDummyOTGiuaCa_SoGio: TFloatField;
    TbDummyOTCuoiCa_SoGio: TFloatField;
    Label6: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    Label9: TLabel;
    wwDBDateTimePicker4: TwwDBDateTimePicker;
    Label10: TLabel;
    wwDBDateTimePicker7: TwwDBDateTimePicker;
    Label11: TLabel;
    wwDBDateTimePicker8: TwwDBDateTimePicker;
    QrDays: TADOQuery;
    TbDummyManv: TWideStringField;
    spHR_BANG_LICHLV_Invalid_GioTangCa: TADOCommand;
    getAllSaturdayByMonth: TADOStoredProc;
    getAllSaturdayByMonthNgay: TDateTimeField;
    GrDanhsach: TDBGridEh;
    QrListOTDauCa_SoGio: TFloatField;
    QrListOTGiuaCa_SoGio: TFloatField;
    QrListOTCuoiCa_SoGio: TFloatField;
    TaAdditional: TTabSheet;
    isPanel1: TisPanel;
    Panel14: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    CbCaLamViecDangKy: TDbLookupComboboxEh2;
    DBNumberEditEh2: TDBNumberEditEh;
    wwDBDateTimePicker5: TwwDBDateTimePicker;
    wwDBDateTimePicker6: TwwDBDateTimePicker;
    wwDBDateTimePicker9: TwwDBDateTimePicker;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrMasterDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdClearExecute(Sender: TObject);
    procedure RgLoaiClick(Sender: TObject);
    procedure CbPhongBanExit(Sender: TObject);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdContinueExecute(Sender: TObject);
    procedure TbDummyAfterInsert(DataSet: TDataSet);
    procedure TbDummyMaCaChange(Sender: TField);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDelRecordSelectedExecute(Sender: TObject);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure GrListDataCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure TbDummyOTDauCa_TuGioChange(Sender: TField);
    procedure TbDummyOTGiuaCa_TuGioChange(Sender: TField);
    procedure TbDummyOTCuoiCa_TuGioChange(Sender: TField);
  private
  	mCanEdit, mIsFunction: Boolean;
    mTransNo, countError, mMonth, mYear, countSubmit: Integer;
    r: WORD;
    mFileIdx: TGUID;
    mFORM_CODE: string;
    procedure SmartFocus;
    procedure DeleteRow(pRow: Integer);
    procedure AddRow(p1, p2: String);
    procedure getAllDateSaturdayOfMonth();
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
    function calcOvertimes(pEmpID, pSession: String; pDate, pDateFrom, pDateTo: TDateTime): Double; overload;
    function overTimeDateValid(pManv, pMaCa: String; pDate, pDateFrom, pDateTo: TDateTime; pLoaiCa: Integer): Boolean;
    function RegisterData(pLst: String; pDate: TDateTime; var pCount: Integer): Boolean;
  public
  	function Execute(r: WORD; FORM_CODE: string; var pTransNo: Integer; pMon, pYear: Integer) : Boolean;
  end;

var
  FrmLichLamViecTools: TFrmLichLamViecTools;

const
    TABLE_NAME = 'IMP_HR_BANG_LICHLV';
    FORM_CODE = 'IMP_HR_BANG_LICHLV';
implementation

{$R *.DFM}

uses
    Rights, Excommon, isLib, isMsg, isDb, MainData, RepEngine, OfficeData,
    isCommon, isStr, isFile, GuidEx, DmVangmat, Clipbrd, HrData; //MassRegLeave;

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.Execute;
begin
    mCanEdit := rCanEdit(r);
    mTransNo := 0;
    mMonth := pMon;
    mYear := pYear;
    mFORM_CODE := FORM_CODE;
    Result := ShowModal = mrOK;
    if Result then
    begin
        pTransNo := mTransNo;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.FormCreate(Sender: TObject);
begin
    AddAllFields(QrList, TABLE_NAME);
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;

    with HrDataMain do
        OpenDataSets([QrDMBOPHAN, QrLOAI_CANGHI, QrDMNV_DANGLAMVIEC]);

	OpenDataSets([QrSite, QrDep, QrSec, QrEmp, QrList, QrEmpStatus]);


    SetDisplayFormat(TbDummy, ctCurFmt);
    SetShortDateFormat(TbDummy, ShortDateFormat);
    SetDisplayFormat(TbDummy, ['GioVao_Ca', 'GioRa_Ca', 'OTDauCa_TuGio', 'OTDauCa_DenGio', 'OTGiuaCa_TuGio',
    'OTGiuaCa_DenGio', 'OTCuoiCa_TuGio', 'OTCuoiCa_DenGio'], sysMinFmt);
    SetDisplayFormat(TbDummy, ['OTDauCa_SoGio', 'OTGiuaCa_SoGio', 'OTCuoiCa_SoGio'], sysFloatFmtTwo);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetDisplayFormat(QrList, ['GioVao_Ca', 'GioRa_Ca', 'GioVao1_Ca', 'GioRa1_Ca', 'OTDauCa_TuGio', 'OTDauCa_DenGio',
    'OTGiuaCa_TuGio', 'OTGiuaCa_DenGio', 'OTCuoiCa_TuGio', 'OTCuoiCa_DenGio'], sysMinFmt);
    SetDisplayFormat(QrList, ['OTDauCa_SoGio', 'OTGiuaCa_SoGio', 'OTCuoiCa_SoGio'], sysFloatFmtTwo);


    SetDisplayFormat(getAllSaturdayByMonth, ctCurFmt);
    SetShortDateFormat(getAllSaturdayByMonth, ShortDateFormat);

    SetCustomGrid(FORM_CODE, GrListData);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;

    countSubmit := 0;

    with TbDummy do
    begin
        Open;
    	Append;
    end;
    RgLoai.OnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.GrListDataCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    TbDummy.Cancel;
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;
	CloseDataSets([TbDummy, QrEmp, QrList, QrEmpStatus]);

    if countSubmit > 0 then
        ModalResult := mrOk;

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdImportExecute(Sender: TObject);
var
    ls: TStringList;
    i: Integer;
    s, sName: String;
begin
    ls := TStringList.Create;
    ls.Text := Clipboard.AsText;
    for i := 0 to ls.Count - 1 do
    begin
        s := Trim(ls[i]);
        if (s <> '') then
        begin
            with QrEmp do
            begin
                Filter := 'ManvQL=' + QuotedStr(s);
                sName := FieldByName('Tennv').AsString;
            end;

            AddRow(s, sName);
        end;
    end;
    QrEmp.Filter := '';
    ls.Free;
    Clipboard.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdInsExecute(Sender: TObject);
var
	s1, s2: String;
begin
    case RgLoai.ItemIndex of
    0:
    	begin
            s1 := CbPhongBan.Value;
        	s2 := CbPhongBan.Text;
        end;
    1:
    	begin
            s1 := CbBoPhan.Value;
        	s2 := CbBoPhan.Text;
        end;
    2:
    	begin
            s1 := CbNhanVien.Value;
        	s2 := CbNhanVien.Text;
        end;
    else
    	Exit;
    end;

    AddRow(s1, s2);
	SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdDelExecute(Sender: TObject);
begin
	DeleteRow(GrList.Row);
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdDelRecordErrorExecute(Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdDelRecordSelectedExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVAILD_HOURS = 'Giờ tăng ca không hợp lệ.';
    RS_INVAILD_HOURS_START = 'Từ giờ tăng ca không hợp lệ.';
    RS_INVAILD_HOURS_END = 'Đến giờ tăng ca không hợp lệ.';
    RS_INVAILD_INPUT_DATE = 'Phải chọn ngày đi làm thứ 7.';
    RS_INVAILD_INPUT = 'Phải nhập giờ tăng ca.';
procedure TFrmLichLamViecTools.CmdContinueExecute(Sender: TObject);
var
    pLst: String;
    i, j, n, mCount: Integer;
begin
    with TbDummy do
    begin
        if BlankConfirm(TbDummy, ['MaCa']) then
            Abort;

        if (PgMain.ActivePageIndex = 0) then
        begin
            if GrDanhsach.SelectedRows.Count = 0 then
            begin
                GrDanhsach.Focused;
                ErrMsg(RS_INVAILD_INPUT_DATE);
                Abort;
            end;
        end
        else if (PgMain.ActivePageIndex = 1) then
        begin
            if BlankConfirm(TbDummy, ['Ngay']) then
                Abort;

            if  ((FieldByName('OTDauCa_TuGio').IsNull) and
                (FieldByName('OTDauCa_DenGio').IsNull)) and
                ((FieldByName('OTGiuaCa_TuGio').IsNull) and
                (FieldByName('OTGiuaCa_DenGio').IsNull)) and
                ((FieldByName('OTCuoiCa_TuGio').IsNull) and
                (FieldByName('OTCuoiCa_DenGio').IsNull)) then
            begin
                dpDangKyTuGio.Focused;
                ErrMsg(RS_INVAILD_INPUT);
                Abort;
            end;
        end
        else if (PgMain.ActivePageIndex = 2) then
        begin
            if BlankConfirm(TbDummy, ['Ngay']) then
                Abort;
        end;
    end;

    pLst := '';
    mCount := 0;
    with GrList do
    for i := 0 to RowCount - 1 do
    begin
        if Cells[0, i] = '' then
            Break;
        if pLst <> '' then
            pLst := pLst + ',';
        pLst := pLst + '''' + Cells[0, i] + '''';
    end;


    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;
    mTransNo := DataMain.GetSeqValue(TABLE_NAME);

    if PgMain.ActivePageIndex = 0 then
    begin
        with getAllSaturdayByMonth do
        begin
            DisableControls;
            with GrDanhsach.SelectedRows do
            begin
                n := Count - 1;
                for j := 0 to n do
                begin
                    GotoBookmark(Items[j]);
                    RegisterData(pLst, FieldByName('Ngay').AsDateTime, mCount);
                end;
            end;
            EnableControls;
            if mCount > 0 then
            begin
                QrListBeforeOpen(QrList);
                CmdRefresh.Execute;
            end;
        end;
    end
    else
    begin
        RegisterData(pLst, TbDummy.FieldByName('Ngay').AsDateTime, mCount);
        if mCount > 0 then
        begin
            QrListBeforeOpen(QrList);
            CmdRefresh.Execute;
        end;
    end;



end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyList, bIsError: Boolean;
    s: String;
begin
    with TbDummy do
    begin

    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    with QrList do
    begin
    	if not Active then
        	Exit;
        bEmptyList := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdSearch.Enabled := not bEmptyList;
    CmdDelRecordSelected.Enabled := not bEmptyList;
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmptyList);


    CmdContinue.Enabled := (GrList.Cells[0, 0] <> '');
    CmdClear.Enabled := (GrList.Cells[0, 0] <> '');
    CmdDel.Enabled := (GrList.Cells[0, GrList.Row] <> '');
	RgLoai.Enabled := (GrList.Cells[0, 0] = '');
    CmdImport.Enabled := (RgLoai.ItemIndex = 2);

    case RgLoai.ItemIndex of
    0:
       	s := EdMaPhongBan.Text;
    1:
       	s := EdMaBoPhan.Text;
    2:
       	s := EdMaNhanVien.Text;
    end;
    CmdIns.Enabled := s <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.QrListAfterDelete(DataSet: TDataSet);
begin
     CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.QrListBeforeDelete(DataSet: TDataSet);
begin
     if not DeleteConfirm then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

procedure TFrmLichLamViecTools.QrMasterDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CbPhongBanExit(Sender: TObject);
var
	s, s1: String;
    comboboxEh: TDBLookupComboboxEh;
begin
    if mTrigger then
    	Exit;
    mTrigger := True;

    comboboxEh := (Sender as TDBLookupComboboxEh);
    if (comboboxEh.Text = '') or (VarIsNull(comboboxEh.Value)) or (VarIsEmpty(comboboxEh.Value)) then
    	s := ''
    else
        s :=  comboboxEh.Value;

	case (Sender as TComponent).Tag of
    0:		// phong ban

        with QrDep do
        if Locate('MaPhongBan', s, []) then
        begin
            EdMaPhongBan.Text := FieldByName('Ma').AsString;
            s1 := EdMaBoPhan.Text;
            with QrSec do
            if (s1 <> '') then
            begin
                if  not Locate('MaPhongBan;Ma', VarArrayOf([s, s1]), [])  then
                   First;

                EdMaBoPhan.Text := FieldByName('Ma').AsString;
                CbBoPhan.Value := FieldByName('MaBoPhan').AsString;
            end
            else
            begin
                EdMaBoPhan.Text := '';
                CbBoPhan.Text := '';
            end;
        end
        else
        begin
            EdMaPhongBan.Text := '';
            EdMaBoPhan.Text := '';
            CbBoPhan.Text := '';
        end;
    1:		// bo phan
        with QrSec do
        if (s <> '') and (Locate('MaBoPhan', s, [])) then
        begin
            EdMaBoPhan.Text := FieldByName('Ma').AsString;
        end
        else
        begin
            EdMaBoPhan.Text := '';
        end;
    2:		// nhan vien
		EdMaNhanVien.Text := s;
	end;

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.RgLoaiClick(Sender: TObject);
var
	n: Integer;
begin
	n := RgLoai.ItemIndex;

    // UI
    EdMaPhongBan.Enabled := n <> 2;
    CbPhongBan.Enabled := n <> 2;

    EdMaBoPhan.Enabled := n = 1;
    CbBoPhan.Enabled := n = 1;

    EdMaNhanVien.Enabled :=  n = 2;
    CbNhanVien.Enabled :=  n = 2;
    SmartFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.SmartFocus;
begin
	try
        case RgLoai.ItemIndex of
        0:
            CbPhongBan.SetFocus;
        1:
            CbBoPhan.SetFocus;
        2:
            CbNhanVien.SetFocus;
        end;
    except
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.TbDummyAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Date;
    with TbDummy do
    begin
        FieldByName('Ngay').AsDateTime := d;
        FieldByName('Manv').AsString := sysLogonManv; // de ma temp do ham check thoi gian OT bat buoc co Manv
    end;
    getAllDateSaturdayOfMonth;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.TbDummyMaCaChange(Sender: TField);
begin
    with TbDummy do
    begin
        FieldByName('GioVao_Ca').AsDateTime := FieldByName('LK_GioVao').AsDateTime;
        FieldByName('GioRa_Ca').AsDateTime := FieldByName('LK_GioRa').AsDateTime;
        FieldByName('SoGio_Ca').AsFloat := FieldByName('LK_SoGio').AsFloat;

        if (FieldByName('MaCa').AsString = '') or (FieldByName('MaCa').IsNull) then
        begin
            FieldByName('GioVao_Ca').Clear;
        end;
    end;
    TbDummyOTDauCa_TuGioChange(Sender);
    TbDummyOTGiuaCa_TuGioChange(Sender);
    TbDummyOTCuoiCa_TuGioChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.TbDummyOTCuoiCa_TuGioChange(Sender: TField);
var
    f: Double;
begin
    with TbDummy do
    begin
        if Sender.AsString <> '' then
        begin
            if  (FieldByName('MaCa').AsString <> '') and
                (not FieldByName('OTCuoiCa_TuGio').IsNull)and
                (not FieldByName('OTCuoiCa_DenGio').IsNull) then
            begin
                if not overTimeDateValid(
                    FieldByName('Manv').AsString,
                    FieldByName('MaCa').AsString,
                    FieldByName('Ngay').AsDateTime,
                    FieldByName('OTCuoiCa_TuGio').AsDateTime,
                    FieldByName('OTCuoiCa_DenGio').AsDateTime,
                    3) then
                begin
                    Abort;
                end;

                f := calcOvertimes( FieldByName('Manv').AsString,
                            FieldByName('MaCa').AsString,
                            FieldByName('Ngay').AsDateTime,
                            FieldByName('OTCuoiCa_TuGio').AsDateTime,
                            FieldByName('OTCuoiCa_DenGio').AsDateTime);

                FieldByName('OTCuoiCa_SoGio').AsFloat := f;
            end
            else
                FieldByName('OTCuoiCa_SoGio').Clear;
        end
        else
            FieldByName('OTCuoiCa_SoGio').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.TbDummyOTDauCa_TuGioChange(Sender: TField);
var
    f: Double;
begin
    with TbDummy do
    begin
        if Sender.AsString <> '' then
        begin
            if  (FieldByName('MaCa').AsString <> '') and
                (not FieldByName('OTDauCa_TuGio').IsNull) and
                (not FieldByName('OTDauCa_DenGio').IsNull) then
            begin
                if  not overTimeDateValid(
                        FieldByName('Manv').AsString,
                        FieldByName('MaCa').AsString,
                        FieldByName('Ngay').AsDateTime,
                        FieldByName('OTDauCa_TuGio').AsDateTime,
                        FieldByName('OTDauCa_DenGio').AsDateTime,
                    1) then
                begin
                    Abort;
                end;

                f := calcOvertimes( FieldByName('Manv').AsString,
                            FieldByName('MaCa').AsString,
                            FieldByName('Ngay').AsDateTime,
                            FieldByName('OTDauCa_TuGio').AsDateTime,
                            FieldByName('OTDauCa_DenGio').AsDateTime);

                FieldByName('OTDauCa_SoGio').AsFloat := f;
            end
            else
                FieldByName('OTDauCa_SoGio').Clear;
        end
        else
            FieldByName('OTDauCa_SoGio').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.TbDummyOTGiuaCa_TuGioChange(Sender: TField);
var
    f: Double;
begin
    with TbDummy do
    begin
        if Sender.AsString <> '' then
        begin
            if  (FieldByName('MaCa').AsString <> '') and
                (not FieldByName('OTGiuaCa_TuGio').IsNull) and
                (not FieldByName('OTGiuaCa_DenGio').IsNull) then
            begin
                if  not overTimeDateValid(
                    FieldByName('Manv').AsString,
                    FieldByName('MaCa').AsString,
                    FieldByName('Ngay').AsDateTime,
                    FieldByName('OTGiuaCa_TuGio').AsDateTime,
                    FieldByName('OTGiuaCa_DenGio').AsDateTime,
                    2) then
                begin
                    Abort;
                end;

                f := calcOvertimes( FieldByName('Manv').AsString,
                            FieldByName('MaCa').AsString,
                            FieldByName('Ngay').AsDateTime,
                            FieldByName('OTGiuaCa_TuGio').AsDateTime,
                            FieldByName('OTGiuaCa_DenGio').AsDateTime);

                FieldByName('OTGiuaCa_SoGio').AsFloat := f;
            end
            else
                FieldByName('OTGiuaCa_SoGio').Clear;
        end
        else
            FieldByName('OTGiuaCa_SoGio').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.AddRow(p1, p2: String);
var
    i: Integer;
begin
    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := p1;
				Cells[1, i] := p2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = Copy(p1, 1, Length(Cells[0, i])) then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.DeleteRow(pRow: Integer);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, pRow] = '' then
			Exit;

		for i := pRow to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.RegisterData;
var
    s: string;
begin
    
	with TbDummy, spIMP_HR_LICHLV_ImportList do
    begin
        Prepared := True;
        Parameters[1].Value := sysLogonUID;
        Parameters[2].Value := TGuidEx.ToString(DataMain.GetNewGuid);
        Parameters[3].Value := mFORM_CODE + '_GROUP';
        Parameters[4].Value := sysAppSource;
        Parameters[5].Value := mTransNo;
        Parameters[6].Value := RgLoai.ItemIndex;
        Parameters[7].Value := pLst;
        Parameters[8].Value := pDate;
        Parameters[9].Value := mYear;
        Parameters[10].Value := mMonth;
        Parameters[11].Value := FieldByName('MaCa').AsString;
        Parameters[12].Value := PgMain.ActivePageIndex;
        Parameters[13].Value := FieldByName('OTDauCa_TuGio').AsDateTime;
        Parameters[14].Value := FieldByName('OTDauCa_DenGio').AsDateTime;
        Parameters[15].Value := FieldByName('OTDauCa_SoGio').AsFloat;
        Parameters[16].Value := FieldByName('OTGiuaCa_TuGio').AsDateTime;
        Parameters[17].Value := FieldByName('OTGiuaCa_DenGio').AsDateTime;
        Parameters[18].Value := FieldByName('OTGiuaCa_SoGio').AsFloat;
        Parameters[19].Value := FieldByName('OTCuoiCa_TuGio').AsDateTime;
        Parameters[20].Value := FieldByName('OTCuoiCa_DenGio').AsDateTime;
        Parameters[21].Value := FieldByName('OTCuoiCa_SoGio').AsFloat;
        try
            Execute;
        except
        end;

        if Parameters[0].Value <> 0 then
        begin
            mTransNo := 0;
            s := Parameters[22].Value;
            ErrMsg(s);
        end
        else
        begin
            Inc(pCount);
        end;
    end;
    Result := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.DeleteAllRecord;
begin
    with spIMP_HR_LICHLV_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_DANGKY_LICHLV_UpInsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            Parameters[3].Value := PgMain.ActivePageIndex;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                mTransNo := 0;
                CmdRefresh.Execute;
                Inc(countSubmit);
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmLichLamViecTools.getAllDateSaturdayOfMonth;
begin
    try
        with getAllSaturdayByMonth do
        begin
            Prepared := True;
            Parameters[1].Value := mYear;
            Parameters[2].Value := mMonth;
            ExecProc;

            Active := True;
            if IsEmpty then
            begin
                Active := False;
                Exit;
            end;
        end;
    except
        on E : Exception do
            ErrMsg('Exception message = '+E.Message);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.calcOvertimes(pEmpID, pSession: String; pDate, pDateFrom, pDateTo: TDateTime): Double;
begin
  	with QrDays do
    begin
    	Parameters[0].Value := pEmpID;
    	Parameters[1].Value := pDate;
        Parameters[2].Value := pDateFrom;
        Parameters[3].Value := pDateTo;
        Parameters[4].Value := pSession;
        Open;
        Result := Fields[0].AsFloat;
        Close;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmLichLamViecTools.overTimeDateValid;
var
    s: String;
begin
    with spHR_BANG_LICHLV_Invalid_GioTangCa do
    begin
        Prepared := True;
        Parameters.ParamByName('@Manv').Value := pManv;
        Parameters.ParamByName('@MaCa').Value := pMaCa;
        Parameters.ParamByName('@Ngay').Value := pDate;
        Parameters.ParamByName('@TuGio').Value := pDateFrom;
        Parameters.ParamByName('@DenGio').Value := pDateTo;
        Parameters.ParamByName('@LoaiCa').Value := pLoaiCa;
        try
            Execute;
        except
        end;

        if Parameters.FindParam('@STR') <> nil then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;



end.
