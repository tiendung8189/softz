object FrmHRMain: TFrmHRMain
  Left = 162
  Top = 129
  Caption = 'Softz Solutions - HRM'
  ClientHeight = 749
  ClientWidth = 1119
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CoolBar1: TCoolBar
    Left = 0
    Top = 0
    Width = 1119
    Height = 69
    AutoSize = True
    Bands = <
      item
        Control = ToolBar1
        ImageIndex = -1
        MinHeight = 21
        Width = 1117
      end
      item
        Control = ToolBar2
        ImageIndex = -1
        MinHeight = 46
        Width = 1117
      end>
    EdgeBorders = []
    object ToolBar1: TToolBar
      Left = 11
      Top = 0
      Width = 1108
      Height = 21
      AutoSize = True
      ButtonHeight = 21
      ButtonWidth = 51
      Caption = 'ToolBar1'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowCaptions = True
      TabOrder = 0
      object ToolButton3: TToolButton
        Left = 0
        Top = 0
        Caption = 'H'#7879' th'#7889'ng'
        DropdownMenu = PopHethong
        Grouped = True
      end
    end
    object ToolBar2: TToolBar
      Left = 11
      Top = 23
      Width = 1108
      Height = 46
      AutoSize = True
      ButtonHeight = 46
      ButtonWidth = 112
      Caption = 'ToolBar2'
      Color = 16119285
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Images = ImgLarge
      List = True
      ParentColor = False
      ParentFont = False
      ShowCaptions = True
      TabOrder = 1
      object ToolButton10: TToolButton
        Left = 0
        Top = 0
        Cursor = 1
        Hint = 
          'C'#225'c danh m'#7909'c|C'#7853'p nh'#7853't danh m'#7909'c h'#224'ng h'#243'a, kh'#225'ch h'#224'ng, nh'#224' cung c'#7845 +
          'p...'
        Caption = 'Danh m'#7909'c'
        DropdownMenu = PopDanhmuc
        ImageIndex = 13
        ParentShowHint = False
        ShowHint = True
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object ToolButton4: TToolButton
        Left = 127
        Top = 0
        Cursor = 1
        Action = CmdProfile
        Caption = 'Nh'#226'n vi'#234'n'
        ImageIndex = 14
      end
      object BtBangcong: TToolButton
        Left = 239
        Top = 0
        Cursor = 1
        Action = CmdBangcong
        Caption = 'B'#7843'ng c'#244'ng'
        ImageIndex = 12
      end
      object BtBangluong: TToolButton
        Left = 351
        Top = 0
        Cursor = 1
        Action = CmdBangluong
        Caption = 'B'#7843'ng l'#432#417'ng'
        ImageIndex = 5
      end
      object ToolButton1: TToolButton
        Left = 463
        Top = 0
        Cursor = 1
        Action = CmdBc
        ImageIndex = 3
      end
      object ToolButton2: TToolButton
        Left = 575
        Top = 0
        Cursor = 1
        Hint = '|K'#7871't th'#250'c ch'#432#417'ng tr'#236'nh'
        Action = CmdQuit
        ImageIndex = 4
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 69
    Width = 1119
    Height = 657
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object RzSizePanel1: TRzSizePanel
      Tag = 99
      Left = 1
      Top = 1
      Width = 315
      Height = 655
      Color = 16119285
      GradientColorStart = 16119285
      GradientColorStop = 16119285
      HotSpotHighlight = 11855600
      HotSpotVisible = True
      SizeBarWidth = 7
      TabOrder = 0
      object GbFunc: TRzGroupBar
        Tag = 99
        Left = 0
        Top = 0
        Width = 307
        Height = 655
        GradientColorStart = 16119285
        GradientColorStop = 16119285
        LargeImages = ImgLarge
        SmallImages = DataMain.ImageSmall
        GroupBorderSize = 8
        UniqueItemSelection = True
        Align = alClient
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        PopupMenu = PopFunc
        TabOrder = 0
        object RzGroup1: TRzGroup
          CaptionColor = clHighlightText
          CaptionColorDefault = False
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 14
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdProfile
            end
            item
              Caption = '-'
            end
            item
              Action = CmdQuatrinhLamViec
            end
            item
              Action = CmdHopDongLaoDong
            end
            item
              Action = CmdKhenthuongKyLuat
            end
            item
              Action = CmdThietBiTaiSan
            end
            item
              Action = CmdCongtac
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBcNS
            end>
          ItemStaticFont.Charset = ANSI_CHARSET
          ItemStaticFont.Color = clWindowText
          ItemStaticFont.Height = -13
          ItemStaticFont.Name = 'Tahoma'
          ItemStaticFont.Style = []
          Opened = False
          OpenedHeight = 212
          DividerVisible = False
          Caption = 'Nh'#226'n s'#7921
          ParentColor = False
        end
        object RzGroup3: TRzGroup
          CaptionColor = clHighlightText
          CaptionColorDefault = False
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 12
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdVangmat
            end
            item
              Action = CmdTangCa
            end
            item
              Action = CmdMangThai
            end
            item
              Action = CmdThaisan
            end
            item
              Action = CmdThoiviec
            end
            item
              Caption = '-'
            end
            item
              Action = CmdTuyendung
            end
            item
              Action = CmdDaotao
            end
            item
              Caption = '-'
            end
            item
              Action = CmdPhepnam
            end
            item
              Action = CmdNghiBu
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDmCalamviec
            end
            item
              Action = CmdDmNgayle
            end
            item
              Action = CmdLichlamviec
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBangcong
            end
            item
              Action = CmdBangluong
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBcNSCong
            end>
          Opened = False
          OpenedHeight = 428
          DividerVisible = False
          Caption = 'Ch'#7845'm c'#244'ng - T'#237'nh l'#432#417'ng'
          ParentColor = False
        end
        object GrDanhmuc: TRzGroup
          Tag = 99
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 13
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdDmPhongban
            end
            item
              Action = CmdDmChucvu
            end
            item
              Action = CmdDmQuatrinh
            end
            item
              Action = CmdDmNhomlv
            end
            item
              Action = CmdDmBacLuong
            end
            item
              Action = CmdDmPhucap
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDmHopdong
            end
            item
              Action = CmdDmLyDoKTKL
            end
            item
              Action = CmdDmDiaDiemLamViec
            end
            item
              Action = CmdDmVangmat
            end
            item
              Action = CmdDmDKKhambenh
            end
            item
              Action = CmdDmNguonTuyenDung
            end
            item
              Action = CmdDmThietBiTaiSanLaoDong
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDmdialy
            end
            item
              Action = CmdDmNganhang
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDmCanhBao
            end
            item
              Action = CmdDmBieuMau
            end
            item
              Action = CmdDmHotroNS
            end>
          Opened = False
          OpenedHeight = 408
          DividerVisible = False
          Caption = 'Danh m'#7909'c'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object RzGroup2: TRzGroup
          CaptionColor = clHighlightText
          CaptionColorDefault = False
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 15
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdDmMCC
            end
            item
              Action = CmdLaydulieucong
            end
            item
              Action = CmdVantay
            end
            item
              Action = CmdCongtho
            end>
          Opened = False
          OpenedHeight = 108
          DividerVisible = False
          Caption = 'M'#225'y ch'#7845'm c'#244'ng'
          ParentColor = False
        end
        object GrHethong: TRzGroup
          Tag = 99
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 8
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdConfig
            end
            item
              Caption = '-'
            end
            item
              Action = CmdSetpass
            end>
          Opened = False
          OpenedHeight = 88
          DividerVisible = False
          Caption = 'Th'#244'ng s'#7889
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrPhanQuyen: TRzGroup
          Tag = 99
          CaptionColorStop = 16119285
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 8
          CaptionStyle = csLarge
          Color = clBtnFace
          ColorDefault = False
          Items = <
            item
              Action = CmdListUser
            end
            item
              Action = CmdListGroup
            end
            item
              Caption = '-'
            end
            item
              Action = CmdFunctionRight
            end
            item
              Action = CmdReportRight
              ImageIndex = 60
            end
            item
              Action = CmdDataRight
            end>
          Opened = False
          OpenedHeight = 128
          DividerVisible = False
          Caption = 'Ph'#226'n Quy'#7873'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrAdmin: TRzGroup
          CaptionColorStop = 16119285
          CaptionImageIndex = 8
          CaptionStyle = csLarge
          Items = <
            item
              Action = CmdParams
            end>
          Opened = False
          OpenedHeight = 48
          DividerVisible = False
          Caption = 'System - Softz'
          ParentColor = False
        end
      end
    end
    object PaBkGr: TPanel
      Tag = 99
      Left = 316
      Top = 1
      Width = 802
      Height = 655
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object BkGr: TfcImager
        Left = 0
        Top = 285
        Width = 802
        Height = 370
        Align = alClient
        AutoSize = False
        BitmapOptions.AlphaBlend.Amount = 0
        BitmapOptions.AlphaBlend.Transparent = False
        BitmapOptions.Color = clNone
        BitmapOptions.Contrast = 0
        BitmapOptions.Embossed = False
        BitmapOptions.TintColor = clNone
        BitmapOptions.GaussianBlur = 0
        BitmapOptions.GrayScale = False
        BitmapOptions.HorizontallyFlipped = False
        BitmapOptions.Inverted = False
        BitmapOptions.Lightness = 0
        BitmapOptions.Rotation.CenterX = -1
        BitmapOptions.Rotation.CenterY = -1
        BitmapOptions.Rotation.Angle = 0
        BitmapOptions.Saturation = -1
        BitmapOptions.Sharpen = 0
        BitmapOptions.Sponge = 0
        BitmapOptions.VerticallyFlipped = False
        BitmapOptions.Wave.XDiv = 0
        BitmapOptions.Wave.YDiv = 0
        BitmapOptions.Wave.Ratio = 0
        BitmapOptions.Wave.Wrap = False
        DrawStyle = dsProportionalCenter
        PreProcess = True
        SmoothStretching = False
        Transparent = True
        TransparentColor = clNone
        Visible = False
        TabOrder = 0
        ExplicitLeft = -2
        ExplicitTop = 291
        ExplicitHeight = 196
      end
      object PaDesc: TPanel
        Tag = 99
        Left = 0
        Top = 0
        Width = 802
        Height = 285
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LbDesc2: TLabel
          Left = 0
          Top = 178
          Width = 802
          Height = 33
          Align = alTop
          Alignment = taCenter
          Caption = 'DESC2'
          Font.Charset = ANSI_CHARSET
          Font.Color = 13360356
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          WordWrap = True
          ExplicitWidth = 89
        end
        object Bevel3: TBevel
          Left = 0
          Top = 164
          Width = 802
          Height = 14
          Align = alTop
          Shape = bsSpacer
          ExplicitWidth = 554
        end
        object LbDesc1: TLabel
          Left = 0
          Top = 80
          Width = 802
          Height = 84
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = 'DESC1'
          Font.Charset = ANSI_CHARSET
          Font.Color = 13360356
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExplicitWidth = 554
        end
        object Bevel2: TBevel
          Left = 0
          Top = 0
          Width = 802
          Height = 80
          Align = alTop
          Shape = bsSpacer
          ExplicitWidth = 554
        end
      end
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 726
    Width = 1119
    Height = 23
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageSmall
    Panels = <
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Hint = 'Double click '#273#7875' ch'#7881'nh ng'#224'y, gi'#7901
        Name = 'Panel2'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '77'
        OnDblClick = CmdSetDateTimeExecute
      end
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Style = psHintContainerOnly
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = -1
        Name = 'Panel3'
        Style = psGlyph
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '24'
      end
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel4'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '240'
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = -1
        Name = 'Panel5'
        Style = psGlyph
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '24'
        OnClick = CmdReconnectExecute
      end
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel6'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '150'
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Hint = 'Double click '#273#7875' ch'#7881'nh ng'#224'y, gi'#7901
        Name = 'Panel1'
        Style = psDateTime
        Tag = 0
        Text = '10/12/2023 5:19 PM'
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '197'
        OnDblClick = CmdSetDateTimeExecute
      end>
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object MyActionList: TActionList
    Images = DataMain.ImageSmall
    OnExecute = MyActionListExecute
    Left = 380
    Top = 276
    object CmdDmPhongban: TAction
      Category = 'DANHMUC'
      Caption = 'T'#7893' ch'#7913'c ph'#242'ng ban'
      Hint = '@HR_DM_PHONGBAN'
      OnExecute = CmdDmPhongbanExecute
    end
    object CmdDmChucvu: TAction
      Category = 'DANHMUC'
      Caption = 'Ch'#7913'c danh - Ch'#7913'c v'#7909
      Hint = '@HR_DM_CHUCVU'
      OnExecute = CmdDmChucvuExecute
    end
    object CmdDmNhomlv: TAction
      Category = 'DANHMUC'
      Caption = 'Nh'#243'm l'#224'm vi'#7879'c'
      Hint = '@HR_DM_NHOMLV'
      OnExecute = CmdDmNhomlvExecute
    end
    object CmdQuit: TAction
      Category = 'HETHONG'
      Caption = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdDmVangmat: TAction
      Category = 'DANHMUC'
      Caption = 'L'#253' do v'#7855'ng m'#7863't'
      Hint = '@HR_DM_VANGMAT'
      OnExecute = CmdDmVangmatExecute
    end
    object CmdThietBiTaiSan: TAction
      Category = 'NHANSU'
      Caption = 'Thi'#7871't b'#7883' - T'#224'i s'#7843'n'
      Hint = '@HR_THIETBI_DUNGCU'
      OnExecute = CmdThietBiTaiSanExecute
    end
    object CmdDmPhucap: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i ph'#7909' c'#7845'p'
      Hint = '@HR_DM_PHUCAP'
      OnExecute = CmdDmPhucapExecute
    end
    object CmdDmQuatrinh: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      Hint = '@HR_DM_QUATRINH_LV'
      OnExecute = CmdDmQuatrinhExecute
    end
    object CmdSetpass: TAction
      Category = 'HETHONG'
      Caption = #208#7893'i m'#7853't kh'#7849'u'
      ImageIndex = 9
      OnExecute = CmdSetpassExecute
    end
    object CmdAbout: TAction
      Caption = 'Gi'#7899'i thi'#7879'u...'
      OnExecute = CmdAboutExecute
    end
    object CmdParams: TAction
      Category = 'SOFTZ'
      Caption = 'Config'
      ImageIndex = 8
      OnExecute = CmdParamsExecute
    end
    object CmdBc: TAction
      Caption = 'B'#225'o c'#225'o'
      Hint = 'X'#7917' l'#253', xem in b'#225'o c'#225'o, th'#7889'ng k'#234'...'
      OnExecute = CmdBcExecute
    end
    object CmdDmhotro: TAction
      Category = 'DUNGCHUNG'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907
      Hint = '@SZ_HOTRO'
      ImageIndex = 32
    end
    object CmdForceDropDown: TAction
      OnExecute = CmdForceDropDownExecute
    end
    object CmdHelp: TAction
      Caption = 'H'#432#7899'ng d'#7851'n s'#7917' d'#7909'ng'
      ShortCut = 112
      OnExecute = CmdHelpExecute
    end
    object CmdSetBkgr: TAction
      Category = 'HINHNEN'
      Caption = 'G'#7855'n h'#236'nh n'#7873'n'
      ImageIndex = 2
      OnExecute = CmdSetBkgrExecute
    end
    object CmdClearBkgr: TAction
      Category = 'HINHNEN'
      Caption = 'X'#243'a h'#236'nh n'#7873'n'
      OnExecute = CmdClearBkgrExecute
    end
    object CmdSetDateTime: TAction
      Hint = 'Ch'#7881'nh l'#7841'i ng'#224'y, gi'#7901
      OnExecute = CmdSetDateTimeExecute
    end
    object CmdLock: TAction
      Caption = 'Kh'#243'a ch'#432#417'ng tr'#236'nh'
      ShortCut = 49228
      OnExecute = CmdLockExecute
    end
    object CmdFaq: TAction
      Caption = 'C'#225'c h'#7887'i '#273#225'p th'#432#7901'ng g'#7863'p'
      OnExecute = CmdFaqExecute
    end
    object CmdMyCustomer: TAction
      Caption = 'Danh s'#225'ch kh'#225'ch h'#224'ng'
      OnExecute = CmdMyCustomerExecute
    end
    object CmdDmdialy: TAction
      Category = 'DUNGCHUNG'
      Caption = 'V'#7883' tr'#237' '#273#7883'a l'#253
      Hint = '@SZ_DIALY'
      ImageIndex = 35
      OnExecute = CmdDmdialyExecute
    end
    object CmdTygia: TAction
      Category = 'DUNGCHUNG'
      Caption = 'T'#7927' gi'#225
      Hint = '@SZ_TYGIA'
      ImageIndex = 45
      OnExecute = CmdTygiaExecute
    end
    object CmdReconnect: TAction
      Category = 'HETHONG'
      Caption = 'K'#7871't n'#7889'i c'#417' s'#7903' d'#7919' li'#7879'u'
      ImageIndex = 57
      OnExecute = CmdReconnectExecute
    end
    object CmdExport: TAction
      Caption = 'Xu'#7845't Excel'
      Hint = 'B'#225'o c'#225'o '#273#7897'ng, xu'#7845't ra Excel'
    end
    object CmdDmTaikhoan: TAction
      Category = 'DUNGCHUNG'
      Caption = 'T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
      Hint = '@SZ_TAIKHOAN'
    end
    object CmdListUser: TAction
      Category = 'SOFTZ'
      Caption = 'Danh s'#225'ch t'#224'i kho'#7843'n'
      OnExecute = CmdListUserExecute
    end
    object CmdListGroup: TAction
      Category = 'PHANQUYEN'
      Caption = 'Danh s'#225'ch nh'#243'm t'#224'i kho'#7843'n'
      OnExecute = CmdListGroupExecute
    end
    object CmdFunctionRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n ch'#7913'c n'#259'ng'
      ImageIndex = 43
      OnExecute = CmdFunctionRightExecute
    end
    object CmdReportRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n b'#225'o c'#225'o'
      OnExecute = CmdReportRightExecute
    end
    object CmdAdmin: TAction
      Category = 'SOFTZ'
      ShortCut = 16507
      Visible = False
      OnExecute = CmdAdminExecute
    end
    object CmdProfile: TAction
      Category = 'NHANSU'
      Caption = 'H'#7891' s'#417' nh'#226'n vi'#234'n'
      Hint = '@HR_PROFILE'
      OnExecute = CmdProfileExecute
    end
    object CmdConfig: TAction
      Category = 'SOFTZ'
      Caption = 'C'#7845'u h'#236'nh h'#7879' th'#7889'ng'
      ImageIndex = 8
      OnExecute = CmdConfigExecute
    end
    object CmdDmCalamviec: TAction
      Category = 'CHAMCONG'
      Caption = 'Ca l'#224'm vi'#7879'c'
      Hint = '@HR_CALAMVIEC'
      OnExecute = CmdDmCalamviecExecute
    end
    object CmdDmNgayle: TAction
      Category = 'CHAMCONG'
      Caption = 'C'#225'c ng'#224'y ngh'#7881' trong n'#259'm'
      Hint = '@HR_NGAYLE'
      OnExecute = CmdDmNgayleExecute
    end
    object CmdDmMCC: TAction
      Category = 'MAYCC'
      Caption = 'M'#225'y ch'#7845'm c'#244'ng'
      Hint = '@TB_MAYCC'
      OnExecute = CmdDmMCCExecute
    end
    object CmdVantay: TAction
      Category = 'MAYCC'
      Caption = 'Danh s'#225'ch m'#7851'u d'#7919' li'#7879'u ch'#7845'm c'#244'ng'
      Hint = '@TB_VANTAY'
      OnExecute = CmdVantayExecute
    end
    object CmdCongtho: TAction
      Category = 'MAYCC'
      Caption = 'D'#7919' li'#7879'u c'#244'ng th'#244
      Hint = '@TB_DULIEU_CONGTHO'
      OnExecute = CmdCongthoExecute
    end
    object CmdBangcong: TAction
      Category = 'CHAMCONG'
      Caption = 'B'#7843'ng ch'#7845'm c'#244'ng'
      Hint = '@HR_BANGCONG'
      OnExecute = CmdBangcongExecute
    end
    object CmdBangluong: TAction
      Category = 'TINHLUONG'
      Caption = 'B'#7843'ng t'#237'nh l'#432#417'ng'
      Hint = '@HR_BANGLUONG'
      OnExecute = CmdBangluongExecute
    end
    object CmdLaydulieucong: TAction
      Category = 'MAYCC'
      Caption = '.: L'#7845'y d'#7919' li'#7879'u t'#7915' m'#225'y ch'#7845'm c'#244'ng'
      Hint = '@TB_DULIEU_CONG'
      OnExecute = CmdLaydulieucongExecute
    end
    object CmdDmNganhang: TAction
      Category = 'DUNGCHUNG'
      Caption = 'Ng'#226'n h'#224'ng'
      Hint = '@SZ_NGANHANG'
      OnExecute = CmdDmNganhangExecute
    end
    object CmdDmHopdong: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i h'#7907'p '#273#7891'ng lao '#273#7897'ng'
      Hint = '@HR_DM_HOPDONG'
      OnExecute = CmdDmHopdongExecute
    end
    object CmdQuatrinhLamViec: TAction
      Category = 'NHANSU'
      Caption = 'Qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      Hint = '@HR_QUATRINH_LV'
      OnExecute = CmdQuatrinhLamViecExecute
    end
    object CmdHopDongLaoDong: TAction
      Category = 'NHANSU'
      Caption = 'H'#7907'p '#273#7891'ng lao '#273#7897'ng'
      Hint = '@HR_HOPDONG_LD'
      OnExecute = CmdHopDongLaoDongExecute
    end
    object CmdKhenthuongKyLuat: TAction
      Category = 'NHANSU'
      Caption = 'Khen th'#432#7903'ng - K'#7927' lu'#7853't'
      Hint = '@HR_KHENTHUONG_KL'
      OnExecute = CmdKhenthuongKyLuatExecute
    end
    object CmdGiadinh: TAction
      Category = 'NHANSU'
      Caption = 'Quan h'#7879' gia '#273#236'nh'
      Hint = '@HR_GIADINH'
      OnExecute = CmdGiadinhExecute
    end
    object CmdTailieu: TAction
      Category = 'NHANSU'
      Caption = 'Ch'#7913'ng t'#7915' - T'#224'i li'#7879'u'
      Hint = '@HR_TAILIEU'
      OnExecute = CmdTailieuExecute
    end
    object CmdNghiviec: TAction
      Category = 'NHANSU'
      Caption = 'Th'#244'i vi'#7879'c'
      Hint = '@HR_NGHIVIEC'
      OnExecute = CmdNghiviecExecute
    end
    object CmdQuyettoan: TAction
      Category = 'NHANSU'
      Caption = 'Quy'#7871't to'#225'n'
      Hint = '@HR_QUYETTOAN'
      OnExecute = CmdQuyettoanExecute
    end
    object CmdVangmat: TAction
      Category = 'CHAMCONG'
      Caption = #272#259'ng k'#253' v'#7855'ng m'#7863't'
      Hint = '@HR_VANGMAT'
      OnExecute = CmdVangmatExecute
    end
    object CmdPhepnam: TAction
      Category = 'CHAMCONG'
      Caption = 'Qu'#7843'n l'#253' ph'#233'p n'#259'm'
      Hint = '@HR_PHEPNAM'
      OnExecute = CmdPhepnamExecute
    end
    object CmdLichlamviec: TAction
      Category = 'CHAMCONG'
      Caption = 'L'#7883'ch l'#224'm vi'#7879'c'
      Hint = '@HR_LICHLAMVIEC'
      OnExecute = CmdLichlamviecExecute
    end
    object CmdNhansuREP: TAction
      Category = 'NHANSU'
      Caption = 'B'#225'o c'#225'o'
      Hint = '@HR_NHANSU_REP'
      OnExecute = CmdNhansuREPExecute
    end
    object CmdTamung: TAction
      Category = 'TINHLUONG'
      Caption = 'T'#7841'm '#7913'ng l'#432#417'ng'
      Hint = '@HR_TAMUNG'
      OnExecute = CmdTamungExecute
    end
    object CmdTinhluongREP: TAction
      Category = 'TINHLUONG'
      Caption = 'B'#225'o c'#225'o'
      Hint = '@HR_TINHLUONG_REP'
      Visible = False
      OnExecute = CmdTinhluongREPExecute
    end
    object CmdDmDKKhambenh: TAction
      Category = 'DANHMUC'
      Caption = 'N'#417'i '#273#259'ng k'#253' kh'#225'm b'#7879'nh'
      Hint = '@HR_DM_DK_KHAMBENH'
      OnExecute = CmdDmDKKhambenhExecute
    end
    object CmdThaisan: TAction
      Category = 'CHAMCONG'
      Caption = #272#259'ng k'#253' thai s'#7843'n'
      Hint = '@HR_THAISAN'
      OnExecute = CmdThaisanExecute
    end
    object CmdCongtac: TAction
      Category = 'NHANSU'
      Caption = #272'i c'#244'ng t'#225'c'
      Hint = '@HR_CONGTAC'
      OnExecute = CmdCongtacExecute
    end
    object CmdDmBacLuong: TAction
      Category = 'DANHMUC'
      Caption = 'B'#7853'c l'#432#417'ng'
      Hint = '@HR_DM_BACLUONG'
      OnExecute = CmdDmBacLuongExecute
    end
    object CmdDmLyDoKTKL: TAction
      Category = 'DANHMUC'
      Caption = 'L'#253' do Khen th'#432#7903'ng - K'#7927' lu'#7853't'
      Hint = '@HR_DM_LYDO_KTKL'
      OnExecute = CmdDmLyDoKTKLExecute
    end
    object CmdDmDiaDiemLamViec: TAction
      Category = 'DANHMUC'
      Caption = #272#7883'a '#273'i'#7875'm l'#224'm vi'#7879'c'
      Hint = '@HR_DM_NOI_LAMVIEC'
      OnExecute = CmdDmDiaDiemLamViecExecute
    end
    object CmdDmNguonTuyenDung: TAction
      Category = 'DANHMUC'
      Caption = 'Ngu'#7891'n tuy'#7875'n d'#7909'ng'
      Hint = '@HR_DM_NGUON_TUYENDUNG'
      OnExecute = CmdDmNguonTuyenDungExecute
    end
    object CmdDmThietBiTaiSanLaoDong: TAction
      Category = 'DANHMUC'
      Caption = 'Thi'#7871't b'#7883' - T'#224'i s'#7843'n lao '#273#7897'ng'
      Hint = '@HR_DM_THIETBI_TAISAN_LAODONG'
      OnExecute = CmdDmThietBiTaiSanLaoDongExecute
    end
    object CmdDmCanhBao: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i nh'#7855'c vi'#7879'c'
      Hint = '@HR_DM_NHACVIEC'
      OnExecute = CmdDmCanhBaoExecute
    end
    object CmdDmBieuMau: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c bi'#7875'u m'#7851'u'
      Hint = '@HR_DM_BIEUMAU'
      OnExecute = CmdDmBieuMauExecute
    end
    object CmdDmHotroNS: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' nh'#226'n s'#7921
      Hint = '@HR_DM_HOTRO'
      OnExecute = CmdDmHotroNSExecute
    end
    object CmdTangCa: TAction
      Category = 'CHAMCONG'
      Caption = #272#259'ng k'#253' t'#259'ng ca'
      Hint = '@HR_TANGCA'
      OnExecute = CmdTangCaExecute
    end
    object CmdMangThai: TAction
      Category = 'CHAMCONG'
      Caption = #272#259'ng k'#253' mang thai'
      Hint = '@HR_MANGTHAI'
      OnExecute = CmdMangThaiExecute
    end
    object CmdThoiviec: TAction
      Category = 'CHAMCONG'
      Caption = #272#259'ng k'#253' th'#244'i vi'#7879'c'
      Enabled = False
      Hint = '@HR_NGHIVIEC'
      OnExecute = CmdThoiviecExecute
    end
    object CmdTuyendung: TAction
      Category = 'CHAMCONG'
      Caption = 'Tuy'#7875'n d'#7909'ng'
      Enabled = False
      Hint = '@HR_TUYENDUNG'
      OnExecute = CmdTuyendungExecute
    end
    object CmdDaotao: TAction
      Category = 'CHAMCONG'
      Caption = #272#224'o t'#7841'o'
      Hint = '@HR_DAOTAO'
      OnExecute = CmdDaotaoExecute
    end
    object CmdBcNS: TAction
      Category = 'NHANSU'
      Caption = 'B'#225'o c'#225'o nh'#226'n s'#7921
      Hint = '@HR_NHANSU_REP'
      ImageIndex = 64
      OnExecute = CmdBcNSExecute
    end
    object CmdTinhluong: TAction
      Category = 'CHAMCONG'
      Caption = 'T'#237'nh l'#432#417'ng'
      Hint = '@HR_TINHLUONG'
    end
    object CmdBcNSCong: TAction
      Category = 'CHAMCONG'
      Caption = 'B'#225'o c'#225'o c'#244'ng l'#432#417'ng'
      Hint = '@HR_CHAMCONG_REP'
      ImageIndex = 64
      OnExecute = CmdBcNSCongExecute
    end
    object CmdNghiBu: TAction
      Category = 'CHAMCONG'
      Caption = 'Qu'#7843'n l'#253' ngh'#7881' b'#249
      Hint = '@HR_NGHIBU'
      OnExecute = CmdNghiBuExecute
    end
    object CmdDataRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n d'#7919' li'#7879'u'
      OnExecute = CmdDataRightExecute
    end
  end
  object ImgLarge: TImageList
    Height = 32
    Width = 32
    Left = 380
    Top = 308
    Bitmap = {
      494C0101100015001C0020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000080000000A000000001002000000000000040
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3DED10087A98500487C
      4600306B2D00195A1600306B2D004B7D480087A98500D5DFD300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECECE00A9A9
      A9008F8F8E007C7C7B0071717000717171007E7D7D0093939100ADADAC00D4D4
      D400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C9C9C900ABABAB00909090007A7A7A007676760086868600A0A0A000BEBE
      BE00D9D9D90000000000CACACA00A4A4A40085858500818181009F9F9F00C9C9
      C900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEE6DE00608D5E0017641300127A0E000F85
      0B000F870A000F890A000F870A000F850B00127A0E0017641300608D5E00DEE6
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D7D7D700D3D3D300D7D7D700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007E7E7C004F4F
      4E004645450040403F004B4B4B004B4B4B003E3E3F00424448004A4E50008889
      8D00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1D1D1008E8075006E51
      380067411D00693A0D006D360400703600006B35030067370B00623B18005B41
      28004D4137002D6F7D001492AD0007B4DA0000CAF60009B7DC001B98B4003779
      870083838300D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BBCEBA0024642100127D0D001591100048C5430074E7
      6F008BF687008CF788008BF6870074E76F0048C5430015911000127D0D002464
      2100BDCFBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDC00B6B6B6009595950080808000777777008080800095959500B6B6
      B600DCDCDC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C4C4
      C4009C9C9C001B1B1B001C1C1C001B1C1C0029272500B4ACA100D1CCC5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D8D5D500887461006C441E00713700007137
      0000713700007137000071370000FFFFFF0071370000713700006F360000593E
      1600207F88000BCEF80020D4FB0038D9FE004CDEFF0043DBFE002BD6FC0012CE
      F8001E8CA40060707400D6D6D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BACDB800186114000F860B003ABE35007FEF7B0076DA720095C8
      9200AFBDAE00BABCBA00AFBDAE0095C8920076DA72007FEF7B0038BD34000F86
      0B0018611400BFD0BE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D4D4
      D400B6987B00BC824B00D7863B00EB8E3600D5843800AD733C00836548006B6B
      6B009C9C9C00D4D4D40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C1C1C000C0C0C000C2C2C200C0C0C000ACACAB00B0B0
      B000AEAEAD0049494900494948004748480056545000CABDAB00C6BAA700C4B9
      A400C4C2C000C3BFC100C1C1C100C3C3C3000000000000000000000000000000
      00000000000000000000AFA39800714B27007137000071370000713700007137
      0000713700007137000071370000FFFFFF0071370000713700006A340000267F
      86000BCCF80020D4FC0033D9FD003DDBFD0044DCFE0047DDFE0045DDFF0034D7
      FD0014CEF8001F8AA30083838300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DFE7DF00246421000F880B004DD148006ADF6500A4BFA300D3D3D300EBEB
      EB00F8F8F80045454500F8F8F800EBEBEB00D3D3D300A4C0A3006BDF66004DD1
      48000F880B0024642100DFE7DF0000000000000000000000000000000000DADA
      DA00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CBCBCB00B7AEA700BB89
      5700F19F4F00F9BC7900FCD6A200FFE5BC00FCD5A100F9BC7900F2A14F009C69
      3800696159009C9C9C00DCDCDC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AFAFB000B2B3B300A1A1A200A4A5A500ABABAC00ABAB
      AB00ABABAC00AEAEAE00ADADAB00ACAAA800A9A8A700A9ACAF00ACAFB300B3B3
      BA00C3C6CB007BCC9700ABABAB00ABABAB000000000000000000000000000000
      0000000000009D8976006F3D0F0071370000FFFFFF0095694000713700007137
      0000713700007137000071370000FFFFFF00713700007137000045604D0005C5
      F20012D0FA001FD5FC002AD7FD0032D9FD0038DAFE003ADBFD0038DAFE0032D8
      FD0024D4FB000AC5F10037748300C9C9C9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000608C5E00117E0D0038C4330058D45300BAC5BA00F2F2F20045454500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0045454500F2F2F200BAC5BA0058D4
      530038C43300117E0D00618E5F00000000000000000000000000DADADA009393
      93006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F
      6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F0068686800A5724100F6A6
      5500FDC88700FFD8A300FFDEAD00FFE0B100FFDEAD00FFD9A300FDC88600F6A7
      55009C6938006B6B6B00B6B6B6000000000000000000E7D3CD00CB9E8D00B97C
      5D00AF674000AD633200B06A3000AE653100AA5E3200AB603800B26F4E00B371
      5300BD856E00CEAFA400C2C6CB00A2998B006F48140079551C007B561B007B56
      1C007D571B007C561900805A210085612E0086642F0087652F008B6730008E69
      31008C6B2E007D611800BCC0C800C3C6CB000000000000000000000000000000
      00009B846F006F380500713700007137000095694000FFFFFF00713700007137
      00007137000071370000713700007137000071370000713700002886900005C7
      F4000FD1FC0017D3FC001FD5FC0027D6FD002CD8FD002DD8FD002CD7FD0027D6
      FD0020D4FC000FC9F5001D8EAA009F9F9F000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D5DF
      D3001764140011A10B0056DA5100ACC6AB00FAFAFA00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FAFAFA00ABC5
      AA0056DA510011A00B0017641400D5E1D500000000000000000008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D700528F9400F29D4700FDBF
      7600FFCC8B00FFD19600FFD69E00461E0000FFD69E00FFD19600FFCC8C00FDBE
      7600F29C4600836548009595950000000000E4CFC700A7582700BB772A00CB93
      3400D4A33D00D8A74200D7A64200D6A43F00D5A13900D19A3100C6892500BC76
      1D00B1651700A9612800BDC1C800A2998B00B17B2000BF8C3900BD8B3600BC89
      3600B9853100BF904400D1B17F00DEC9A700DCC6A500D8BF9900D6BB9000D5B8
      8700DABA8600B78D4B00BCC0C800BEC3C900000000000000000000000000AD9A
      88006F380500713700007137000071370000713700009569400071370000773C
      06007C400C00804410008346140085481600834614008044100014A3C40003CA
      F70008CFFC00043D4900043D4900043D4900043D4900043D4900043D4900043D
      490015D3FC000DCCF8000CA4CA00818181000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000085A7
      8300117C0E003ACB350056BD5300F2F2F20045454500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0045454500F1F1
      F10055BD51003ACB3500117C0E0087A98500000000000000000008A0D70029D3
      FE002AD4FE002AD4FF002BD3FF002BD4FF002BD4FF002AD4FF002AD3FE0029D3
      FF0027D3FE0026D3FF0024D2FE0021D2FE001FD1FE009D9E7600F9AC5B00FFC2
      7A00FFC68100FFCA8800FFCD8D00461E0000FFCD8D00FFCA8800FFC68100FFC2
      7A00F9AC5B00AD733C0080808000D7D7D700BC856E00C88B2C00D39F3900D5A3
      4300D7A84B00D9AB4F00D9AB5000D7A94C00D5A34500D19B3B00CC943000C88C
      2400C5841800BF821F00BCC1C800A2998B00A7721C00B3813000B2802F00AE7D
      2C00B4874000CBAE8000D2BC9900C9B08700C2A57800BF9E6C00BF9B6400BF96
      5900CBA66B00B0884400BCC0C800BCC1C6000000000000000000D0C6BD007341
      13007137000071370000713700007137000071370000763B06007E420E008548
      160085481600854816008548160085481600854816008548160008B0DD0002CD
      FA0015D3FB002C5C66003B68710048717A004F777F0048717A003B6871002C5C
      66001DD4FC0008CFFB0005ADDB00858585000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000467A
      45000C94070030C62B00A5D1A300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A5D1A30032C72C000C940700487C4600000000000000000008A0D7002DD5
      FF002FD5FE002FD5FF0030D5FE0030D5FE0030D4FE002FD4FE002ED5FF002ED4
      FE002CD4FE002AD3FE0028D3FF0025D3FE0024D3FE00CF965000FCB86B00FFC1
      7B00FFC98A00FFD09800FFD5A000461E0000FFD39D00FFCD9100FFC78500FFC2
      7A00FCB86B00D584380077777700D3D3D300B5745900D0993500D4A04200D9A9
      4D00DCB15700DEB55D00DEB55E00DCB35900D9AD5100D5A34500D0993900CA8F
      2B00C5841E00BD802400BCC0C800A2998B0092651C003F4539006E4D19009E71
      2A00BE9D6A00C8AD8600BC9E7000B28F5D00AC854F00A87E4500A7793800AF82
      3E00CFB07E00B6925700BCC0C800BABEC3000000000000000000876240007137
      000071370000713700007137000071370000793E090082451300854816008548
      1600854816008548160085481600854816008548160085481600189BBC001ECE
      F60040DAFC0055DEFC005EE0FC0064E1FD0068E2FD0066E1FD0060E1FD0058DF
      FC0042DBFC001ECEF6000C97BF00A6A6A6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002D69
      2A0009A303001ABB1400DBEBDA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00577A55001F5E1C006E936C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DBEBDA001BBB150009A203002E6A2C00000000000000000008A0D70034D5
      FE0033D5FF0035D6FE0036D6FF0036D6FE0036D6FE0035D6FE0034D5FF0032D6
      FF0031D5FF002FD5FE002DD4FE002AD3FE0028D3FF00ED933B00FFC68200FFD2
      9C007F644F00917967009E897900A59283009E897900917967007F644F00FFCC
      8E00FFC37D00EB8E360080808000D7D7D700B3705200D19D3C00D6A64900DCB0
      5600E0B96100E3BE6900E3BE6A00E1BB6300DDB35900D7A94D00D19D3F00CC92
      2F00C6882200BD822600BDC1C800A2998B0097610F00043B61000E2D42005A44
      2400997F5900AB895900A37D4800996F3600916326008F5E1B009D6C2800C5A7
      7800EDE1CC00C1A67800BCC0C800B9BBC10000000000C4B4A60071370000FFFF
      FF00956940007137000071370000793E09008347140085481600854816008548
      160085481600854816008548160085481600854816008548160035808B0053D2
      F1006EE3FD006DE3FD006CE3FD006BE2FD006BE2FD006BE2FD006CE3FD006DE3
      FD006EE3FD0053D2F100197A9800CBCBCB00CBCBCB0094949400939393009292
      9200929292009191910090909000909090008F8F8F008E8E8E008E8E8E00195B
      160012B20C0011B60B00EFF2EF0045454500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E5D1B003DB639001B5C1800FFFFFF00FFFFFF00FFFFFF00FFFFFF004545
      4500EFF2EF0011B60B0012B10C001A5A1700000000000000000008A0D70041D8
      FF0039D7FF003AD6FE003BD6FF003BD7FE003BD7FF003AD7FF0039D7FF0037D7
      FF0036D6FE0033D5FE0031D5FF002ED4FE002BD3FE00D1995300FDCD9500FFDE
      B700FFDEB600FFDDB500FFDDB4009E897900FFDBAE00FFD9AB00FFD7A700FFD4
      A200FCC48400D7863B009595950000000000AB634400D39F3F00D8A94D00DDB4
      5A00E2BD6700E6C57100E7C67300E3BF6900DFB65D00D9AB5100D29F4000CC93
      3200C7892400BF832700BCC1C800A2998B008E5909002948550008426A000D32
      4D003B301E0071512500875B1E0087581A0093662A00A8814B00D2BB9900EFE5
      D600F1E9DA00BFA27300BCC0C800B6B9BF00000000008A633F00713700009569
      4000FFFFFF0095694000763B06008245130085481600FFFFFF00A07049008548
      16008548160085481600854816008548160085481600854816005C6452003DBD
      E20077E1FA0079E5FD0077E5FD0077E5FD0077E5FD0077E5FD0077E5FD0079E5
      FD0077E1F9003CBDE200316270000000000097979700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF002F6A
      2C001CB3160006B00000DBEBDA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00AAAA
      AA002E512C001D5C1A002D522B00BABABA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DBEBDA0006B000001CB31600245F2100000000000000000008A0D70054DC
      FE003FD7FE003FD8FE0040D8FE0040D8FF0040D8FE003FD8FE003ED7FE003DD7
      FF003AD7FE0037D7FE0035D5FF0033D5FE002FD5FE00A3A77F00FBC78F00FFE3
      C100FFE3C100FFE2C000FFE2BE00947D6C00FFDFB900FFDDB500FFDBB000FFD9
      AB00FBBD7C00BC824B00B6B6B60000000000A75C4100CD953B00D8A94C00DDB3
      5900E2BD6600E6C46F00E6C57200E3BF6800DFB65D00DAAB5000D19F4000CB93
      3200C7892400BF832700BCC1C700A2998B008B550600605433000C4D77000D4D
      790009365B0059432600AA845200BC9E7400CFB89400E1D1B700EFE7D800ECE1
      CF00EBDFCB00BFA27300BCC0C800B4B6BD00E2DBD40070360000713700007137
      000071370000713700007E420E008548160085481600A0704900FFFFFF00A070
      490085481600854816008548160085481600854816008548160085481600477B
      7F004FC6E8007BE1F90085E8FD0083E7FD0082E7FD0083E7FD0085E8FD007BE1
      F9004EC6E8002D6B75004D413700D9D9D90099999900FFFFFF00E4E4E400E5E5
      E500E6E6E600E8E8E800A4A4A400EAEAEA00ECECEC00EDEDED00EEEEEE004579
      420036BB310006B00000A5D1A300FFFFFF00FFFFFF00FFFFFF00A9A9A9004545
      45009B9B9B00FFFFFF00AAAAAA0046464600B8B8B800FFFFFF00FFFFFF00FFFF
      FF00A4D0A30006B0000036BB310030642D00000000000000000008A0D70069E0
      FE0056DDFF0044D9FE0045D9FF0045D9FF0045D9FF0044D9FE0043D9FF0041D8
      FF003FD8FE003CD7FE003AD7FF0036D6FE0033D6FF006DBDBC00F4AE6600FEDF
      BB00FFE7CA00FFE7C900FFE6C800947D6C00FFE3C200FFE2BE00FFDFB900FED5
      A600F3A55A00B6987B00DCDCDC0000000000B3715B00BD7C3200D7A84900DBAF
      5400E0B86000E2BD6600E2BD6800E0B96200DCB25800D7A74A00D09B3B00CA8F
      2D00C6872100BE822500BDC1C700A2998B00AC834300B89C6F002B648600125B
      8800054B7C0058504200CEB99800DCC9AA00DFCEB100E8DAC600E9DDCB00E4D6
      BE00EBDFCB00BFA27400BCC0C800B1B4BA00BDA7940071370000713700007137
      000071370000773C060085481600854816008548160085481600A0704900FFFF
      FF00A0704900854816008548160085481600854816008548160085481600794F
      260044777B0035B6DC0059CFEE0074DFF80083E7FD0074DFF80059CFEE0035B6
      DC002E6972005A3A12005B412800BEBEBE009B9B9B00FFFFFF00E4E4E400E5E5
      E500E6E6E600E7E7E700A4A4A400EAEAEA00EBEBEB00EDEDED00EEEEEE0081A3
      800041AE3D000AB2040050B94C00F2F2F20045454500FFFFFF00454545009F9F
      9F00FFFFFF00FFFFFF00FFFFFF00ACACAC0046464600B8B8B80096969600F1F1
      F10050B94C000AB2040041AE3D004E704C00000000000000000008A0D70074E3
      FF0072E2FF0059DDFE004ADAFE004ADBFE0049DAFF0049DAFE0048D9FE0046D9
      FF0043D9FF0041D8FE003ED7FE003AD7FE0037D6FE0033D5FE0098AD8E00F9BD
      8100FEE3C400FFEBD300FFEAD100FFE9CF00FFE7CC00FFE5C800FEDBB300F8B6
      7400BC895800D4D4D4000000000000000000CEA49800A6542600D5A24100D8A8
      4C00DBAF5500DDB35A00DDB45B00DBB15700D7AA4A00D7A64B00D29C4200CC92
      3300C3831B00BC802200BCC1C700A2998B00B48D4F00C8AB7F00C1AA83008292
      99009AA5AC0056514C0087796400D7C4A300E1D0B600E5D7C100E2D3B900E3D4
      BB00EBE0CC00BFA27300BCC0C800AEB3B7009B77570071370000713700007137
      0000713700007C400C0085481600854917008E552600956034009B6A4100B691
      7300FFFFFF00BD9B8000AC836100AE856400AC836100AA7F5C00A67A5500A172
      4C009B6A4100677264003A7E8A001B8DAE000C99C700198BAD002B717C004853
      41006A3400006F360000623B1800A0A0A000A2A2A200A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A4008C97
      8B002675220047D0420008B00200ACC6AB00FAFAFA00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00ACACAC0045454500FAFAFA00A8C4
      A70008B0020048D143002574210075807400000000000000000008A0D70081E5
      FE0081E6FF0076E3FE005DDEFE004EDBFF004EDBFF004EDBFF004DDBFF004ADA
      FF0048DAFE0045D9FE0041D9FF003ED8FF003BD7FF0037D6FE0043CEEC0098AD
      8E00F4B27000FCD2A700FEE4C800FFECD600FEE1C100FBCE9E00F4AD6600A773
      4300BAB1A900000000000000000000000000000000009D482C00B66E2C00D7A5
      4200D6A64800D8A94D00D8A94E00D5A64600D7A64F00D19D5D00F0C5A000FFE2
      CA00D59B5900B97A1D00BCC0C700A2998B00B48D4D00C8AB7D00C9AC7F00C8B2
      8C00BCBAB80077767600564E4200C1B09400DFCEB200E0CFB300E1D1B600E4D4
      BC00EBE0CC00BEA27300BCC0C800ACB0B4008355290071370000713700007137
      0000713700008B5526009B694000A6795400AF876600AF876600AF876600AF87
      6600C1A18800FFFFFF00C1A18800AF876600AF876600AF876600AF876600AF87
      6600AF876600AF876600AF876600A67954009B6940008B552600713700007137
      0000713700007137000067370B0086868600A0A0A000FFFFFF00E4E4E400E5E5
      E500E5E5E500E6E6E600A4A4A400E6E6E600E7E7E700E8E8E800E9E9E900E9E9
      E9004672440057C6530025C0200024B41F00BAC5BA00F2F2F20045454500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0096969600F2F2F200BAC5BA0024B4
      1F0027C1210057C65300639061008B8B8B00000000000000000008A0D7008CE7
      FF008BE7FF008CE8FF007FE5FF0065DFFE0054DCFE0052DCFF0050DBFF004EDB
      FF004CDAFF0049DAFE0046D9FF0041D8FF003ED8FE0039D7FE0036D6FE0031D5
      FF006BBDBC00A2A77F00D0985300ED933B00CF9650009B9E7600538B8D006D6D
      6D00CECECE0000000000000000000000000000000000CFA99C009C432100BC78
      2E00D39F3B00D19D3E00D29D3C00CB933500E8BB8C00FEE6D000A1ABC2005E7E
      AE00E3CCC700D29D5800BDC1C800A2998B00B58D4A00CAAC7B00CCAE7E00CFB4
      8600B2ACA100A5A3A30055524E0098897300D8C5A500E1CEAF00E3D1B600E5D4
      BB00ECE1CB00BEA17100BCC0C800ACAFB400763F0D007137000071370000824F
      1E0092653B00AE866500B0896800B0896800B0896800B0896800B0896800B089
      6800A6878600C2A28900FFFFFF00C2A28900A6878600A6878600A6878600A687
      8600A6878600A6878600A6878600A6878600A6878600AE86650092653B00824F
      1E0071370000713700006B35030076767600A2A2A200FFFFFF00E5E5E500E5E5
      E500E5E5E500E5E5E500A4A4A400E6E6E600E6E6E600E6E6E600E6E6E600E7E7
      E700919A91002669230072E36E0025C01F0025B32000A4BDA300D3D3D300EBEB
      EB00F8F8F80045454500F8F8F800EBEBEB00D3D3D300A2BDA10025B3200025C0
      1F0071E26D0026692300E0E9E0008D8D8D00000000000000000008A0D70096EA
      FF0096E9FF0096E9FF0094E9FE008AE7FF006FE2FF0057DCFE0054DCFE0053DC
      FF0050DBFF004DDAFE0049DAFF0045D9FE0041D8FF003DD7FF0038D6FE0033D6
      FE002ED4FF002AD4FE0026D3FF0021D2FE001CD1FF0018D0FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000C49383009941
      2000B46B2700CC922D00C98C2D00EEBA8400BEC4D7006193C8004A7EBB003E77
      B60062739900D5C4C300B8BCC200A2998B00AF7A2000BD893000BC893000BC89
      3100B98D4200B5B6B700887261004F381500976F2A00BE8B3500BE8C3500BF8D
      3600C3903700A7752000BCC0C800A5A9AE0070370100FFFFFF00FFFFFF00FFFF
      FF00A5805D00B18B6B00B18B6B00B18B6B00B18B6B00B18B6B00B18B6B00B18B
      6B003E3DE8003E3DE800DCCABB00FFFFFF00C5A890003E3DE8003E3DE8003E3D
      E8003E3DE8003E3DE8003E3DE8003E3DE8003E3DE8003E3DE800A5805D00FFFF
      FF00FFFFFF00FFFFFF00703600007A7A7A00A4A4A400FFFFFF00E8E8E800E9E9
      E900EAEAEA00ECECEC00A4A4A400ECECEC00EEEEEE00EFEFEF00EFEFEF00EFEF
      EF00A4A4A400B0C3AF00236F20007AE875004AD445000DB2070053B94F0093BC
      9100AFBDAE00BABCBA00AFBDAE0092BD900053B94F000DB207004AD445007AE8
      7500236F2000A9BBA900FFFFFF008F8F8F00000000000000000008A0D700A0EC
      FF00A1ECFF009FECFF009EEBFF009BEAFF0097EAFF007DE5FE0063DFFF0057DC
      FF0053DCFF0050DBFF004CDBFF0048D9FF0043D9FE003FD8FF003AD7FF0035D6
      FF0030D5FF002CD4FE0027D3FE0023D2FE001ED1FE001AD1FE000A99CC007474
      7400D1D1D100000000000000000000000000000000000000000000000000D1AA
      9F009C472D009E471600DA9A6500BDCBE2004F88C8005585C1006C9ACF0078A4
      D70078A5D60080A9D800A2998B00A2998B00A2998B00A2998B00A2998B00A299
      8B00A2998B00A17E5F00DC813C008D460E0049433A009A918400A2998B00A299
      8B00A2998B00A2998B00A2998B00A2998B0077410F00986E4600A6826000A682
      6000A6826000B28C6C00B38D6E00B38D6E00B38D6E00B38D6E00B38D6E00B38D
      6E00A98C8A00A98C8A00C6AA9200FFFFFF00C6AA9200A98C8A00A98C8A00A98C
      8A00A98C8A00A98C8A00A98C8A00A98C8A00A98C8A00B28C6C00A6826000A682
      6000A6826000986E46006D36040090909000A7A7A700A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A4007C8F7B00236520005CC9570089F4840054D94F0029C3
      240015B80F0006B0000015B80F0029C3240054D94F0089F484005CC957002365
      20007D907C00A4A4A400A4A4A40091919100000000000000000008A0D700AAEE
      FF00AAEEFF00AAEDFE00A8EDFF00A5ECFF00A1ECFE009DEBFF008DE8FF0075E2
      FF005DDDFE0052DCFF004EDBFE004ADAFE0046D9FE0041D9FE003CD7FF0037D6
      FE0032D6FF002ED4FE0029D3FF0024D3FE001FD2FE001BD0FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      000000000000CFA39500937D85005793D4005C8BC40076A1D5007DAADA007FAD
      DC0082AFDE0083B0DE0081AFDF00CEDFF1000000000000000000000000000000
      000000000000EBDED300DB945E00C15E12006D432100B1B1B100000000000000
      000000000000000000000000000000000000885A30009D754F00A8846200A884
      6200A8846200B18C6C00B48E7000B48E7000B48E7000B48E7000B48E7000B48E
      7000B48E7000B48E7000C7AB9300FFFFFF00C7AB9300B48E7000B48E7000B48E
      7000B48E7000B48E7000B48E7000B48E7000B48E7000B18C6C00A8846200A884
      6200A88462009D754F00693A0D00ABABAB00A9A9A900FFFFFF00F2F2F200F2F2
      F200F2F2F200F2F2F200A4A4A400F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100A4A4A400F1F1F100F0F0F000D2DCD2005D8A5B002677220052BD4E007AE8
      75008CF688009AFE96008CF688007AE8750052BD4E00257422005B885900C8D1
      C800DEDEDE00DEDEDE00FFFFFF0094949400000000000000000008A0D700B4F0
      FF00B3EFFF00B2EFFF00B1EFFF00AEEEFF00A9EEFE00A5ECFF009FEBFF0098EA
      FF0086E6FF0071E2FF005DDEFF004CDBFF0047DAFF0043D8FF003DD7FF0039D6
      FE0033D6FE002FD5FE002AD3FF0025D2FF0020D2FE001CD0FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      000000000000000000008CA5C1006292CD0075A2D60080ABDC0084B0DF0087B4
      E2008BB7E5008BB8E5008AB8E400A7C7E9000000000000000000000000000000
      00000000000000000000A54D0800F6B78700A14C070079797900000000000000
      000000000000000000000000000000000000A785670095694000A9856400A985
      6400A9856400AF8B6B00B5907100B5907100B5907100B5907100B5907100B590
      7100B5907100B5907100C8AC9500FFFFFF00C8AC9500B5907100B5907100B590
      7100B5907100B5907100B5907100B5907100B5907100AF8B6B00A9856400A985
      6400A98564009569400067411D00C9C9C900ABABAB00FFFFFF00F3F3F300F3F3
      F300F3F3F300F2F2F200A4A4A400F2F2F200F1F1F100F1F1F100F1F1F100F1F1
      F100A4A4A400F0F0F000F0F0F000F0F0F000F0F0F000C8D4C80082A38000386B
      3500306B2D001C5D1900306B2D00487C460081A380008B978A00EFEFEF00EEEE
      EE00EDEDED00E7E7E700FFFFFF0096969600000000000000000008A0D70099E4
      F900BCF1FF00BAF1FF00B9F1FF00B6F0FF00B2EFFF00ACEEFF00A6EDFF009FEB
      FF009CEBFF0099EAFF0092E9FF007FE5FF006AE1FF0053DCFF003ED7FF003AD7
      FE0035D6FF002FD4FE002BD4FF0025D2FF0021D2FE001CD1FF000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000009EABB90046678D0080AFE2007EAADB0084B0DF008AB7E40090BC
      E80094BFEB0095C2ED0094C0EC009BC3E9000000000000000000000000000000
      00000000000000000000E1CBB900C4661C00C6712E0062452C00ADADAD000000
      000000000000000000000000000000000000CDBBAA008A5A2D00AA886700AA88
      6700AA886700AE8B6B00B6927400B6927400B6927400B6927400B6927400B692
      7400B6927400B6927400C9AD9700FFFFFF00C9AD9700B6927400B6927400B692
      7400B6927400B6927400B6927400B6927400B6927400AE8B6B00AA886700AA88
      6700AA8867008A5A2D006B4F360000000000ABABAB00FFFFFF00F5F5F500F5F5
      F500F4F4F400F3F3F300A4A4A400F3F3F300F3F3F300F2F2F200F2F2F200F1F1
      F100A4A4A400F1F1F100F0F0F000F0F0F000F0F0F000F0F0F000EFEFEF00A4A4
      A400EFEFEF00EEEEEE00EFEFEF00EEEEEE00EEEEEE00A4A4A400EEEEEE00EEEE
      EE00EDEDED00EDEDED00FFFFFF0098989800000000000000000082CEE8000BA2
      D7007AD5F200C1F2FF00C1F3FF00BEF2FF00B9F1FF00B3EFFF00ADEEFF00A9ED
      FF00A5EDFF00A4EDFE00A3EDFE00A2ECFF00A1ECFE009FEBFE008DE8FE0079E4
      FE0065DFFE0052DBFE003FD8FE0030D5FF0024D3FF001DD1FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000BFC7D100364D6700486A910083B0E40082AEDE0089B6E30091BEE80097C4
      EE009BC8F2009DCAF3009BC8F20098C3EC000000000000000000000000000000
      0000000000000000000000000000C79D7C00E3975D00A3480000747372000000
      000000000000000000000000000000000000000000007C471500A8856300AB89
      6900AB896900AB896900B3907100B7937600B7937600B7937600B7937600B793
      7600B7937600B7937600C9AE9800FFFFFF00C9AE9800B7937600B7937600B793
      7600B7937600B7937600B7937600B7937600B3907100AB896900AB896900AB89
      6900A88563007D48160085796C0000000000ABABAB00A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A4009A9A9A0000000000000000000000000009A9
      DB0010A9DF0052C3EA00AFEBFC00C4F3FF00BFF2FF00B9F1FF00B5F0FF00B1EF
      FF00B0EEFF00AFEEFF00AEEFFF00AFEFFF00AFEEFF00B0EFFF00B0EFFF00AEEF
      FF00AAEEFF00A3ECFF0099EAFE008AE7FF0079E4FE0067E0FF000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000516681002C496B005C82AF0082AFE10085B1E1008DBAE70095C2EC009CC9
      F200A2CFF600A4D1F800A2CFF7009CC9F000CBD9EA0000000000000000000000
      000000000000000000000000000000000000B1530B00EAA7740076441D009898
      98000000000000000000000000000000000000000000A37F5F00966C4300C0A6
      8E00FFFFFF00C1A89000AF8D6E00B6937600B8957800B8957800B8957800B895
      7800B8957800B8957800CAAF9900FFFFFF00CAAF9900B8957800B8957800B895
      7800B8957800B8957800B8957800B6937600AF8D6E00C1A89000FFFFFF00C1A8
      9000966C43006C3B0C00C6C6C60000000000ABABAB00FFFFFF00FBFBFB00FBFB
      FB00FAFAFA00F9F9F900A4A4A400F8F8F800F8F8F800F7F7F700F7F7F700F6F6
      F600A4A4A400F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200A4A4
      A400F1F1F100F0F0F000F0F0F000F0F0F000F0F0F000A4A4A400EEEEEE00EEEE
      EE00EEEEEE00EEEEEE00FFFFFF009D9D9D0000000000000000000000000009AB
      DD0053D7FF0031C3FA0018AFEB002AB4E50098E3F800B3EDFE00ABEBFC00B9F1
      FF00B7F0FF00B7F1FF00B8F1FF00BAF1FF00BCF1FF00BEF2FF00C0F2FF00BFF2
      FF00BDF1FF00B6F0FF00ACEEFF009EECFF008CE8FE0079E4FE000B9BCE008B8B
      8B00DCDCDC00000000000000000000000000000000000000000000000000BAC4
      CF002E4969003D6088007CA9DD0081ADDE0086B2E10090BCE80098C4EE009FCD
      F400A6D3F900ABD8FD00A7D4FB009DCBF20086A2C60000000000000000000000
      000000000000000000000000000000000000C0906B00D77F3B00A5520F00594D
      4300BBBBBB0000000000000000000000000000000000DFD3C90083502000FBFA
      F800C2A99100AE8C6D00AE8C6D00B2907200B8967800B9967A00B9967A00B996
      7A00B9967A00B9967A00CBB19B00FFFFFF00CBB19B00B9967A00B9967A00B996
      7A00B9967A00B9967A00B8967800B2907200AE8C6D00AE8C6D00C2A99200FFFF
      FF00835020007E5D3F000000000000000000ABABAB00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00A4A4A400FCFCFC00FBFBFB00FBFBFB00FAFAFA00F9F9
      F900A4A4A400F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F5F5F500A4A4
      A400F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200A4A4A400F1F1F100F0F0
      F000F0F0F000EFEFEF00FFFFFF009F9F9F0000000000000000000000000009AC
      DD0076E1FF0068DBFF004CCFFF0038C3FF0023B4FA0013A9EF000EA8EA000BA5
      E10009A2D90008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D7000C9BCE00BABA
      BA00000000000000000000000000000000000000000000000000000000007D91
      A8002745690046699000739FD00074A0CF0089B4E30090BCE80098C5EE00A0CD
      F400A7D4FA00ABD9FD00A9D6FB00A0CDF500628FBD0000000000000000000000
      00000000000000000000000000000000000000000000B7622000D57C38008341
      0C00898989000000000000000000000000000000000000000000A3815F009367
      3D00AF8E6F00AF8E6F00AF8E6F00AF8E6F00B3927400B8967A00BA987B00BA98
      7B00BA987B00BA987B00CCB29C00FFFFFF00CCB29C00BA987B00BA987B00BA98
      7B00BA987B00B8967A00B3927400AF8E6F00AF8E6F00AF8E6F00AF8E6F009367
      3D006F3F1300D3D0CE000000000000000000ABABAB00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A4A4
      A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00A1A1A10000000000000000000000000009AC
      DD0093E9FF008FE8FF0081E5FF0070DEFF005FD8FF004ACEFF003AC8FE0030C3
      FF002AC3FD0024C4FD001EC2FD0018BEFD0012BDFF000CBBFD0008B8FD0005BA
      FE0003BBFE0003BBFE0003BCFE0003BEFE0004C3FE0008A8DB00A1A1A1000000
      000000000000000000000000000000000000000000000000000000000000637B
      9B0031517600436183003F648F00648DBA008AB7E6008EBAE70096C3ED009CCA
      F200A2D0F600A5D3F900A3D0F7009FCDF4004F7FAE00E0E6EF00000000000000
      00000000000000000000000000000000000000000000DFC8B600DE844000C665
      18006A5F59000000000000000000000000000000000000000000000000008B5F
      35009C744E00AF8F7000AF8F7000AF8F7000AF8F7000B2917300B7957800BB98
      7C00BB987C00BB987C00CCB29D00FFFFFF00CCB29D00BB987C00BB987C00BB98
      7C00B7957800B2917300AF8F7000AF8F7000AF8F7000AF8F70009C744E007848
      1B00AD9F9300000000000000000000000000A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A40000000000000000000000000009AC
      DD00A8EEFF00A5ECFF009FEBFF0097EAFF008CE8FE007EE4FF006ADEFF0008A6
      DA0021A4CF002DABD50030AED70030AFD70030B0D70030B0D70030B1D70030B1
      D70030B1D70030B1D70030B1D70030B1D90030B1D90030B2D900000000000000
      0000000000000000000000000000000000000000000000000000000000005B77
      990035567C0039567600294A75005C84B20088B5E6008BB8E60095C1EC009BC8
      F1009DC9F2009ECAF3009ECBF4008CB9E400446E9800CED9E800000000000000
      0000000000000000000000000000000000000000000000000000CEAD9000DC75
      2900844718008A8A8A000000000000000000000000000000000000000000DCCD
      C20085542600A07A5500B0907200B0907200B0907200C4AC9500B0907200B493
      7500B7957900B9987B00BA997D00FFFFFF00BA997D00B9987B00B7957900B493
      7500B0907200C4AC9500B0907200B0907200B0907200A07A55007C4B1D009C88
      750000000000000000000000000000000000AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD4518000000000000000000000000001BAF
      DC0062D1F100B7F0FF00B0EFFF00A7EDFF009DEBFF008CE6FF0034C1EA0057A7
      BF00DBDBDB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B2C0
      D1003356820030527B002B5180005E8ABA0088B5E40082AEDD007BA6D4007FAA
      D80097C3EE00A0CBF1008BB7E4004F81B400436FA400D7E0ED00000000000000
      000000000000000000000000000000000000000000000000000000000000D194
      6600C06017006253470000000000000000000000000000000000000000000000
      0000D3C3B400855426009E765000B2927400C5AD9600FFFFFF00B2927400B292
      7400B2927400B2927400B2927400B2927400B2927400B2927400B2927400B292
      7400B2927400F6F2EF00C5AD9600B29274009E7650007F4D1F009B836E000000
      000000000000000000000000000000000000AD451800F0B26C00DF842300E492
      3400E6973500F7C48000AD451800F8C98200F0B14700F3B84E00F5BF5800FCD1
      8800AD451800FDD48800FCCF5800FDD25800FFD65800FDD25800FED68800AD45
      1800FCD38800F6C35800F4BC4E00F2B54700F9CA8200AD451800F8C67F00E89C
      3500E6973400E2892300F2B66E00AD4518000000000000000000000000000000
      000044BCE10009ACDD0009ACDD0009ACDD0009ACDD0009ACDD0070BFD8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DBE1
      E900315787003E6492005883B2006598CF005D8CBF003B6EA3003C679C004067
      9B004873A8005582B6007AA2CB00729CC3004E7AB00000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F1E7
      DF00F08330007C52330000000000000000000000000000000000000000000000
      000000000000D9CCBF008C60360095694000F7F4F000C6AE9800B3947600B394
      7600B3947600B3947600B3947600FFFFFF00B3947600B3947600B3947600B394
      7600B3947600BB9F8500F3EDE8009569400079461700A7948200000000000000
      000000000000000000000000000000000000AD451800FFDAA700EAAB6C00EBAE
      6C00ECB16C00FFDAA700AD451800FFDAA700F1BC6C00F2BE6C00F3C06C00FFDA
      A700AD451800FFDAA700F7C86C00F7C96C00F7C96C00F7C96C00FFDAA700AD45
      1800FFDAA700F5C36C00F3C06C00F2BE6C00FFDAA700AD451800FFDAA700EDB3
      6C00ECB16C00EBAE6C00FFDAA700AD4518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000637FA40049719E0075A7DE005486BD0029528600285182004474A9005F8F
      C400739DCD0087ACD5008DB5DD00527FB500ADC0DA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EDCDB500B68B6A0000000000000000000000000000000000000000000000
      0000000000000000000000000000A5836300845223009A714A00AF8F7000B394
      7700B3947700B3947700B3947700FFFFFF00B3947700B3947700B3947700B394
      7700AF8F70009A714A00845223007C4D2100C8BBAF0000000000000000000000
      000000000000000000000000000000000000AD451800FCDAAE00F6DABE00F6DA
      BE00F6DBBE00FDDAAE00AD451800FDDBAE00F8DEBE00F8DEBE00F8DFBE00FDDC
      AE00AD451800FDDCAE00F9E1BE00F9E1BE00F9E1BE00F9E1BE00FDDCAE00AD45
      1800FDDCAE00F8E0BE00F8DFBE00F8DEBE00FDDBAE00AD451800FDDBAE00F7DB
      BE00F6DBBE00F6DABE00FCDAAE00AD4518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C7D2E100436997003F6D9E002F5A8D002A5080004672A2005A8CC40070A4
      DD007BAEE4005582B9004B79B3008EA9CE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCD1C5009A734F007F4A18008F61
      35009C734D00A7836100AF8E6F00FFFFFF00AF8E6F00A78361009C734D008F61
      35007F4A180089623D00C0B09F00000000000000000000000000000000000000
      000000000000000000000000000000000000D1998100AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800D29B84000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000758FB2002D5382003E699C003E6AA300406EA8004370
      AB004473AE00547CB300B1C4DC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CDBB
      AA00A7856700885A300077410F0070370100763F0D00835529009B775700BDA7
      9400E2DBD4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F1CEA700EEC2
      8E00F7E7D600F5DCBC00F0C18900EFCDA7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CAE7F90095CEF20071BCEB005CB3EA006CB9EB008ECA
      F100C2E3F8000000000000000000000000000000000000000000000000000000
      00000000000000000000DFE0E000DDDDDE00E6E6E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E3E4E400E6E6E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E99C4000EEA3
      4700DB7E1100E89D4400F0A34400E1871E00E3A15400F7E7D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CCE6F7005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E900BDDFF500000000000000000000000000000000000000
      0000BEBEBE006E69690097867300A391770082756A0076767800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848484007B736D00978774008F8070006B676700C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E9AC6400E69E4A00E9A85D00ED9A3600ED90
      2100EE922600ED8D1D00EB830A00EF942900EF973000F4D1A700E6AD6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2E2
      F6005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E900BBDEF4000000000000000000000000000000
      00008E878600C8985F00E2B65800E9C56400D8A65100AE8D7100BDBDBE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CACACB00A7907E00D5A45800E9C46500E5BD5F00C8995E0089817D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2D4B100EF9C3800EC881300ED8E1E00EB7F0200EB7F
      0200EB7F0200EB7F0200EB7F0200EB7F0200EF9B3900E9B06E00D7750400F4DD
      C200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000060B3
      E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E90061B3E700000000000000000000000000D7D7
      D900AD7E5F00C7752700BB9A6F00B6A78F00D0985500BE702D009E908A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A0999800BC743900CA8A4000B7A38100C6A87900CE823100AC785300D4D4
      D500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7E4CF00E9B37300EE932800EB7F0200EB7F0200EB7F0200EB7F
      0200EB7F0200EB7F0200EB7F0200EB800400EE9A3600F4D5B200E7AF6F000000
      00000000000000000000000000000000000096CCE70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000A4A2
      A400B6785400C67F5700B3AFB200AFB2BC00C19F8D00BB591F00A08272000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A18E8400C06C3900BA927C00B0B2BC00B0AEB300C97E4F00B16233009F9D
      9D00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ECBF8C00EC9B3A00ED993600EB810700EB800500EB800500EC8A1800EE91
      2400EE8F2200EB830C00EB972D00EBB96900ECC17900EDC47F00F6E2C000D97E
      1500F0D0AA0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000B2B1
      B200AF836B00DDA88F00D3C2BD00DAD6D600E0BEB000C68866009D887D00C3C3
      C300CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00C5C6
      C6009F8F8800C4896800D6B0A100D6D2D200D6CBC700E0AA9100B1795900ACAA
      AA000000000000000000000000000000000000000000FDE9D100FDE5C9000000
      0000FDE6CB000000000000000000FDE2C20000000000FDE9D100FDEEDD000000
      0000FDE2C20000000000FDECD900FDEAD50000000000FDE2C200000000000000
      0000FDE7CE0000000000FDE4C6000000000000000000FDE4C700000000000000
      000000000000FDE2C200FDE9D200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7AA6300EE932900EB800500EB800500EB800500F4BD7E00ECBB8200DE85
      1D00DF933900E79C4200EDBA6B00ECC07600EDC37C00F1D09900F2D2A900D777
      0800E8B3780000000000000000000000000051ABD70051ABD70051ABD700A4DD
      FD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DD
      FD00A4DDFD0051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000D5D6
      D600AE9C9400B88F7800ECC9B700EECEBE00E0BAA600AD8A7800A7A4A3007675
      7300747472007474720074747200747472007474720074747200777573007676
      7400A9A6A500A3806D00DFB8A300EECFC100F0CFBF00CBA48E00B1948700CFCE
      CF000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDCA8E00FDDCB600FB88020000000000FCA74200FCBD7200FDE9
      D200FF8B000000000000FCB56200FCAF520000000000FF8B0000FDE2C200FDC5
      8200FD9F320000000000FB921200FDD3A200FDD19E00FB931600000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EEC59500EF973100EB800500EB800500F4BD7E00E1963F00D9801A00F1D4
      B20000000000F1D0AA00EDAC5700ECC17900EDC47F00F6E2C100D8790C00E099
      48000000000000000000000000000000000051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      0000B5B3B300BDACA400A07F6D00AB877500A3887B009F999C00A4A4A700D8D8
      D80000000000000000000000000000000000000000000000000000000000C9C9
      CB00A7A7A900BDB7B8009E817200AC857100AD867200AF897C0084A9B8000000
      00000000000000000000000000000000000000000000FCA74200FB951E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFCB
      A200E8A04C00ED8F2000EB800500EE922800EBB67A00D6770A00F6E5D3000000
      00000000000000000000E79B3E00EDC37C00EEC58200F2D4A200FAECD700E199
      4700F5E1CB0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      000000000000BEBEBF00CACACC00AAAAAC0096949100998A6F00AC9C7C00827A
      7100C1C1C2000000000000000000000000000000000000000000828184009B8C
      7900AC9B7A00BCAE9C00D8D6D900BBB8B900A2A4A300579EA20000C5E900B6C8
      CD000000000000000000000000000000000000000000FCA74200FB951E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E596
      3800EC850E00EB800500EB800500F2AE5F00DE8D2F00E3A45D00000000000000
      00000000000000000000EBA64F00EDC47F00EEC78500EFC98B00F6E1BF00DF8D
      2F00E9B9800000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005BB1E90059B1E80057B0E80056B0E80057B0E80059B0
      E8005BB1E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      00000000000000000000857F7A0084797300B6804800DDAA4E00E2B75700D39A
      4F009F8C7C00E2E1E000000000000000000000000000B5B3AF00B9906700DAA2
      4A00E1B85900D69339008A8B7C0019C4EF000BD5E70000ECFD0000E0FF007AAE
      C3000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FDD6A900FDDBB30000000000FDD09A00FDD09A00FDD09A00000000000000
      0000FDE4C800FDDBB30000000000FDD9AE00FDD09A00FDDAB10000000000FDD0
      9A00000000000000000000000000FDECD800FDD09A00FDE4C700FDE8D000FDD7
      AB00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E4A0
      5100EE9B3900EB800500EB800500F2B06300DD882700E9B98000000000000000
      000000000000EBB97F00F0B96D00EEC68200EFC88800F7E1C500E9B37400D87C
      1200F3DBBF0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005CB3
      EB0054AFE80050ADE60055AEE60073BDEB0089C6ED0095CDF0008AC8ED0075BE
      EB0056B0E60050ACE60054AEE8005BB1E80000000000000000007C493C00883C
      13008E42170094491A0098481400A14A1200AB4B0B00B25A1200BB6B1A00C57D
      1F00D1952E00D1A83C00D3B03E00D3B13E00D0AB3C00D3A53700CA8B2600BF78
      2000B9651400AF5109003A89A70004D1FF000EE8FF0012E9FF0012DBFF0049A5
      CD000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EB963200EB800500EB800500EDA24200E4923200E5AB6700000000000000
      0000F1D3B200E9A04700EEC37D00EEC78500F1D09900EBB87D00D7750400F7EA
      DA000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD7000000000059B2
      EA00B6DCF400F5FBFD00F4FAFC00D8ECFA00C3E1F500B6DCF400C3E1F500D8EC
      FA00F5FBFC00F8FCFE00BADFF4005AB0E80000000000000000006F220A00A461
      3E00AE6E4900B26E4400B36C3E00B4683700B4643100B8692C00BF732A00C681
      2900D0942A00DBAB3100E1B73500E1B93600DFB43600D5A32E00C88B2700C375
      1600AF5F16003F7AAC0015B4FF0023CCFF0025E1FF0018E5FF0016CDF9002DA9
      E4000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7A9
      6200EE962F00EB800500E9932300E7B45B00EFAD5500DD872100EFCBA100E9B0
      6D00EBA34A00EEBF7600EEC68200EFC88800F0CB8E00F6DBB800D7790E00F8ED
      DF000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70000000000F3FB
      FE0096CDF00055AFE80050ADE60052AEE60053AEE80054AFE80053AEE80052AE
      E60050ACE60055AFE60095CDF000ECF5FC00000000000000000070260B00B37E
      6200BB896B00BD866400BF855F00C0825A00C17F5500C5835200CA8C5100D096
      4F00D5A14D00DBAE4A00DEB74400DDB53C00D9AD3300D09A2B00CE871A00AF6F
      27003B70C000259EFF0036B6FF0036CEFF0020E1FF00458684004970760017BB
      FD00CCCED40000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6E0C700EE9F
      4100EE902300EA860F00E7B05400E8B55E00E9B76300EEB56300EEB15E00EFB8
      6A00EDC17900EDC48000EEC78600F7E5C600F9EAD200E39C4A00D5750600F5E3
      CE000000000000000000000000000000000051ABD70051ABD70051ABD700A4DD
      FD00FFFFFF00FFFFFF00FFFFFF00A4DDFD00A4DDFD00FFFFFF00FFFFFF00FFFF
      FF00A4DDFD0051ABD70051ABD70051ABD70051ABD70051ABD7000000000067BA
      EC0054AFE8005BB1E8005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005BB1E90055AFE80062B4E90000000000F2F0EF0071250600B988
      6F00C0907400C18E6E00C38C6900C48A6400C5865F00C7895B00CC905A00D199
      5900D6A35A00DBAE5B00DFB75A00DFB85600D9AE4A00D99B2D00AD7D3B003774
      D300378FFF0048A4FF0047BDFF0029C8FF00527975009126000065422F0028BC
      FF008DA5BC0000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFCC
      A400E6A04D00EBA84A00E8B45B00E8B76100E9B96700EABC6C00EBBE7200ECC1
      7800EDC37D00EEC68300F4DAAF00E49D4B00D5770A00E8B67C00EBC090000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D700FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD700FFFFFF00FFFFFF00FFFF
      FF0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70000000000B1DA
      F4005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E900ABD6F20000000000EFEDEB0071250200C094
      7E00C59A8000C6977A00C7967500C8937100C98F6B00CA906700CE956500D29C
      6300D6A46200DAAC6100DCB16000DCB15C00E1AB4C00B49769005690E6005AA1
      FF005D9EFF0053ADFF0031A9F3006563590097320000883002007329080037A1
      E9004E8FC90000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBBB8500EDB15A00E8B55E00E9B86400EABA6A00EBBD6F00ECC07500EDC2
      7B00EFCA8C00F1D09900F3D8AC00E7AD6800E3A2590000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF00A4DDFD0051ABD70051ABD700A4DDFD00FFFFFF00A4DD
      FD0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD7009FD0E7000000
      000000000000BEE1F70095CEF1007AC0ED0067B6E8005BB0E70067B6E8007AC0
      EC0095CDF100BDE0F700000000000000000000000000EDEAE800772A0800C7A1
      8D00CAA38D00CBA18700CC9F8200CD9D7D00CD997800CD977300D09B7000D3A0
      6E00D6A56C00D9AA6A00DAAD6700E1AD5900B29E80006AA1F10082B8FF0089B8
      FF007AB5FF004994E700775341009E3800008E390800843105007C1F00004578
      A7002E8EED0000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000000000000000000000000000099C8EE008FC3EC00DCEBF80097D1
      F100BC925800F1C28400F4D9B500EDC47F00EABC6C00EBBE7200ECC17800F3D7
      A700EFC49100F0C99900EFC18B00D7770800E3A45D0000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70053AA
      D50096CBE500D7EBF40000000000000000000000000000000000000000000000
      000000000000D8EBF500000000000000000000000000EBE7E4007B301000CEAD
      9D00CFAD9900CFAA9300D0A88E00D1A68A00D1A38400D19F7F00D2A07B00D5A4
      7800D7A77600D9AA7300DFAB6600B0A395007FB1F8009DC7FF00A1C6FF008EC1
      FF006A94D900A06B5100B15B2200984614008A360500822E04007D1F0000524D
      5F003398FF00E0E7F300000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000000000000D2E5F7000000000055C5F10059CEF50048B8EC0064DB
      FE004EBAE900B5AE8600D89E5800F1D29E00F5DDB700ECBE8900F4DBB200F6DF
      BA00DA821B00E0974400F1D4B200EABC87000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E9E3E00081381900D5BA
      AD00D4B6A600D4B4A000D5B29B00D5AF9600D5AC9100D5A88C00D5A68700D6A8
      8300D8AA7F00DDAA7400AFA8A80095C0FD00B5D5FF00B7D4FF009ECAFF008099
      CC00B2785400BB723F00AF6D4300A7633A0094471C007E2A01007A210000612E
      24004C9EFF00A1B9DE00000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000096C8EE005BD0F90052C4F20059CDFE004FBEFF0049B6FF0043AD
      FF0049B5FF0087CDFF007CB0DC00C0C1BC00E6A75D00D7750400EFCDA700E4A8
      6200D7750400ECC3940000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E7DFDC0086402200DDC7
      BC00DAC0B200D9BDAC00D9BBA800D9B8A300D9B59D00D9B19800D8AE9200D9AD
      8D00DBAC8400B2AFBA00ABCFFF00CAE1FF00CAE0FF00AAD0FF00949FBF00C388
      5E00C3825500B97C5400B3744D00AB6C4600A36340008E462400782301006F2A
      1200568EE100598AD500000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000081C4EC0047B4FF003EA7FF003CA4FF003AA1FF0039A0FF0039A0
      FF0039A2FF00A3D5FF0067AEE800DBEBF80000000000F5E1CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E6DCD7008E4B2E00E6D6
      CF00E0CBC100DFC9BB00DFC6B700DEC3B100DEC0AC00DDBCA700DEB89F00DCB2
      9500B9BACB00C2DDFF00DDEBFF00DAEBFF00B4D5FF00A7A7B900CF976D00CA91
      6B00C38C6800BD856100B77D5A00AF755300A66A4B009D614600904B2D007C33
      1B006E8AB9005A9FFC00000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA9460000000000000000000000000000000000C2DD
      F5004EBEF10054C6FB003DAAFF0040B3FF0061C8FF0055D1FF004ECCFE004CCA
      FE0062D9FE006FDEFE0089E4FE0099C6EC00C8DEF40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E4D8D2008C472C00E6D7
      D300E3D0C700E2CCC100E1CABC00E1C7B700E0C4B200E0C1AC00D9BDAD00BDC9
      E400D1ECFF00E6F5FF00DEF1FF00B8D3FC00B6AAB000D5A17B00CF9D7C00CA98
      7800C6937200C08C6B00BA846400B27C5E00AA715600A0664D009D624B00934F
      2E0099A2B4006FB4FF00B1BACA000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000000000000098C7
      EE0055C6FC003AA3FF003DACFF009FD8FE0079B5EA00429ADF0071BCEB0066DD
      FB0068DDFE0068DDFE00A0E9FE004D9ADF00BDD8F20000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E6D8D10076210100914B
      230093513000985837009D5C3600A25F3600A6603400AD612E00746D84005F84
      C400718DC100708EC1006B8CBD00926F5B00B6652000AC602500A85B2200A356
      20009E501D00984A1B0091451800893D15007F331000813611008F4214008B50
      3300DEDFE2006CA8FB00819DC6000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000000000000087BD
      EC0053C3FE003AA3FF008CCDFF00549FE30074B0E600000000000000000053C1
      EF0068DDFE0068DDFE00AFEDFE0081B9EA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000EBE5E30084340000A459
      0C009B6741009F756100A2745800A5735300A6714E00A66D4900AA6F4500B075
      3E00B27D3D00B7863C00BC8D3A00BE8F3700B5863400AC742600A15E1700964B
      0D008C3E0800863807007E330500752B0200651B0000823D0900BC711A00A38F
      7C00000000007099D80079A5E8000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000008FC2ED0055C4
      FB003FA6FE003AA3FF00A1D1F8003089DA0000000000000000000000000058C3
      F00068DDFE006EDFFE0081E4FE0095C5EE00A7CCEE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000000000007E4225009F50
      0D009D6D4E00A6816F00A77E6500A97D6100AA7B5B00AB775500AD765000B07B
      4D00B4824C00B7884A00B98C4700B88B4200B3823B00AC753200A36427009750
      19008C400D00853A09007E340800742C0500661E00007E380800AE671D00789A
      A300000000008CA1C30060A2FE000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000009CC8EE0059CA
      F400379BFF003AA3FF0097C9F300509BE0000000000000000000C0DCF3006ECC
      F30072E0FE009FEBFE00A9D2F2004495DD00CFE2F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000947A73009743
      0700A1735900AE8B7A00AE877000B0856B00B1836500B2805F00B27C5A00B57F
      5600B8845300BA885100BB8A4D00BA874700B5804000AF743700A6652D009D56
      2300934A1A008A3E0E007F330700752A0400681E00007C2F0100A16B360050B2
      DF0000000000AD9F98009F835E000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000005EC0
      F000379BFF0041ABFE0072DFFD003C94DE0000000000D0E6F70068BDEC0075E0
      FC007FE5FE00BCF2FE004498E100BDD8F2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000C9C5C5008934
      0300B3846C00C4A08F00C39B8400C5997E00C6977800C6947200C68F6C00C78E
      6600C9916200CB945E00CB945900C9915300C5894C00BF7D4200B66F3800AD61
      2E00A55726009B4E1E008E3E10007E2C030073210000842A00007070650052CD
      FF00D9DDE000B17E4C00B79666000000000000000000FDC98B00FCBF75000000
      0000FCC17A00FDC88900FDE9D200FCB56200FCB56200FCB56200FDECD9000000
      0000FDD7AB00FDC8890000000000FDC48100FCB56200FDC6860000000000FCB5
      6200000000000000000000000000FDE2C300FCB56200FDD6A800FDDDB700FCC2
      7C00FDEEDE00FCB56200FDCA8D000000000000000000000000008EC7EE0055C4
      FD00399EFF0061D4FE0068DEFE0063DAFA0056BEEE006BCBF30077E4FD0083E7
      FE00A3EEFF00C7F5FF004198E10092C0EB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000000000007F42
      2500BE917D00CCAC9D00CBA59100CCA48B00CDA18500CD9E7F00CD997800CC95
      7200CD956C00CE966700CD956200CB915B00C7895400C17F4A00B9724100B168
      3800A95F3000A055270094481E0083340F0076230100822700008B91950057C8
      FD0089A3BA00C47D2F00BDB2A400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5CFF00060BB
      EC005FD2FE0068DDFE0068DDFE0069DDFE0071E0FE007CE4FE0086E8FE00A6EF
      FF009CCCF1006AAEE600579FE100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5E0FE0051AB
      D70051ABD70051ABD7000000000000000000000000000000000000000000987D
      7400C2907B00CEAD9F00CCA69300CDA58E00CFA38800CFA08200CF9D7C00CE98
      7500CE956F00CE936A00CD926400CA8E5E00C6875600C17D4E00BA744600B46D
      3F00AC643700A2592E00964B2400883C1A0081300E00772A0A000000000062B1
      EF00769BC500CE904200DEDCDA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098C7
      EE0071DFFC0076E0FE006BDEFE0075E1FE007FE5FE008EEAFE00BCF3FF00BBF4
      FF007DB8EA00A0C8ED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDDFF00051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF00FFFFFF00FFFFFF00A4DDFD0051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700A4DDFD00FFFFFF00A5E0
      FE0051ABD70051ABD7000000000000000000000000000000000000000000CFCB
      C90085442A0087492E008D4D2E0093543200995932009D5D3300A25F3300A45F
      3300A5603200A7603200A7613200A7613100A55F3000A35C2E009F5A2D009C57
      2B0095522A008E4C2600864423007E3C2000813C20008B6858000000000072A1
      CE00C6BDBC00C49A640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C1DC
      F60078BDEC00B3DAF50088E6FE00B0EFFE00A2EEFE00C9F0FD0063ACE7008BC1
      EB002C87D900C8DEF40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098CE
      E80051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD700C5F4FF00FFFFFF00FFFFFF00C1F1FF0051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700A4DDFD00FFFF
      FF00A5E0FE0051ABD70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A1B0
      C300D3BCAB00B69F840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A1CAEE00CBEFFD0055A0E300BCE2F7008FC7F0002985D9000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCEDF60077BEE10051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD7007FC9EE00FFFFFF00FFFFFF00E0FFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700A4DD
      FD00FFFFFF00A5E0FE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEDE
      E200A5A4AB00C4C2C40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009EC9ED00A0C8ED0000000000AACEEF00D3E6F6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BBDEEF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700D7FEFF00FFFFFF00F9FF
      FF0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4F7FB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008CB6C7005E9CB40074A2B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D5E3E900ABC9D500C5CFD400CECECE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E5E400D7D4D200CFCCC900C0BCB700BEBAB600C5C2BF00CFCCC9000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000077A9BD006BAEC5009BE2EF005A97B000CACA
      CA000000000000000000000000000000000000000000E9F0F300B5CFDA008CB6
      C700639CB3005D9CB60063A5C1005C95AC00829AA200ABB3B600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDDDC00D0C5B900B3825600AD713500A95F
      1000AD5E1300B0581600B1531800AB491700A4411500993B0F009F4D0F00B492
      7C00CCC8C4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E9F0F30077A9BD0068AFC8007FCDE3009CE3EF005A97B000C0C0
      C000CDCDCD0000000000C9DCE40096BCCC006DA2B8005C9BB40060A2BC0066AA
      C7006AB1CF006BB3D1006BB3D10069ABC40076B8CC005D97AE00D0D0D0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DDDBDA00CEBAA300AB6D2700AA611100B6611900BA601B00C05E
      1F00BD5B1F00B8541D00B54F1B00A54217009E3E14009835110095301000A04E
      0F00A86C3700D1CFCC00DFDDDC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEE9EE00639CB3006CB7D00076C9E10080CFE4009DE3F0005A97B000839A
      A4005C82910050869C005FA0BA0064A7C30069B0CD006BB4D2006BB4D2006CB5
      D3005AB0B00018A33000199E330078C1DB009CE2EF005A97B000D1D1D1000000
      000000000000000000000000000000000000000000000000000000000000D9C7
      AF00AE732A00DB9B3D00E09F4200D1823400CA752D00C4672300C3622000C160
      2000BE5D1F00B6531D00B3501B00A7491500A3481400A0431200A1411100A646
      1000A5401000973F0F00A6672C00D5D4D100E0DEDD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BAA59600624A3200604830006048300060483000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      300060483000604830000000000000000000000000000000000000000000CEE4
      EB0067A5BB006FBDD40078CCE30077CBE20080D0E5009EE4F0005A97B0004D81
      970051879E005A97B0006BB4D2006BB5D3006CB6D5006CB7D6006CB7D7005BB3
      B4001EAC3C0026BE4C00109F210079C4DF009CE3EF005A97B000000000000000
      000000000000000000000000000000000000000000000000000000000000AB67
      1700DC9F4000E5AB4800E2A14300D3873500CC792D00C5692400C4642100C262
      2000BD5B1F00B6521C00B9521B00AE531700AB521500A5481300A2431200A844
      1100AB4611008C2710008E2F1000C3A99C00D5D4D100E6E5E300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BAA59600DEB8A200E2B59900E6B18D00E7AF8800E9AC8300E9AC
      8300EBAA7F00EDA77B00EEA47500EEA47500F1A27000F1A06C00F39E6900F49C
      6500F49C65006048300000000000000000000000000000000000000000006DB9
      CE0076C9DF0079D0E60079CEE40078CDE40081D2E6009FE4F0005A97B000558E
      A600558FA8005B9AB4006CB7D6006CB8D8006DB9D9006DBADA005CB6B80021AF
      41002BC3570026BE4C00109F21007AC6E2009DE3F0005A97B000000000000000
      000000000000000000000000000000000000000000000000000000000000C392
      5700D3953700E5AA4800E2A44400D88E3800D07F3000C86C2500C4662300BE5C
      1E00BB591E00B9571D00B9571D00BB5E1D00B85B1B00B1521700AB4B1500AA40
      1100AB3E1100B2471300A94013008C2A1000A6641E00D8D6D400DEDDDB000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B4C0BC00ACBCB600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000037714B0037714B00356F
      4900356F4900316A4500316A45002F6742002C643F0029603D00275E3900255A
      360022583400205531001D512E001D512E00194D2B00FAE6D900FAE5D800FAE4
      D600FAE3D400FAE2D300F9E0D200F9E0CF00F9DECD00F9DDCC00F9DCCA00F9DB
      C800F49C650060483000000000000000000000000000000000000000000070BF
      D3007BD3E8007AD1E70079D0E60079CEE50082D3E700A0E5F1005A97B0005792
      AB005794AD005D9EBA006DBADA006DBBDC006EBCDD005DB8BB0022B2460030C8
      60002BC3570026BE4C00109F21007BC9E5009EE4F0005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C18A4A00DFA04200DD9D4100D4883600CD7B2F00C56A2400C2642100BE5D
      1F00BD5C1E00BC5B1E00BC5A1E00C15E1F00BE601D00B8571A00B2501700AA3E
      1100A93C1100AC401200AF4614008B271000A7590F00D4D3D000DAD8D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006DA595003BB992003BB992005FA690000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000039744E006AA3790068A2
      780065A17600619E73005C9D6F00589A6B00529868004D956300489260004290
      5C003E8E5900388C55003389520033895200194D2B00FBE8DC00FAE7DA00FAE6
      D900FAE5D800FAE4D600FAE2D300FAE1D300FAE0D100F9DFCF00F9DECD00F9DC
      CA00F39E690060483000000000000000000000000000000000000000000073C5
      D8007BD4E9007BD3E8007AD2E7007AD0E60083D5E800A1E6F1005A97B0005997
      B1005998B3005FA2BF006EBDDE006EBEE0005DBABE0024B4480032CB660030C8
      60002BC3570026BE4C00109F210052B79800A0E5F0005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CD813000CD7C3100C26B2900C36C2800F0D3B100C1622100D9BD
      9D00C3682000C1632000C0602000C0602000C5672200C15E1D00BC581B00AC3F
      1200A93B1100A5350F00A03A1300A35D1200C1B6AE00D1CFCC00D8D6D400E6E3
      E200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006DA6
      94003BBB93003BBB93003BBB93003BBB93005FA7920000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003D7851006EA67C00EBF3
      EB00E9F1E900E6F0E600E4EFE400E2EEE300E0ECE000DEECDE00DDEADD00DBEA
      DB00DAE8D900D8E7D800D7E7D70033895200194D2B00B68A6A00B68A6A00B68A
      6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00FADF
      CE00F39E690060483000000000000000000000000000000000000000000075C8
      DB007CD6EA007BD5E9007BD3E8007AD2E70084D7E900A3E7F1005A97B0005A9B
      B6005B9DB80060A6C4006FC0E2005EBCC00025B64A0033CB670032CB660030C8
      60002BC3570026BE4C0020B8400013A627001C9E2E0040947D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C3722600C86F2900C1692600C26A2600F7DBBA00C76B2200E3C7
      A500CD752200C7692100C3652100C3622100CA6B2300C4632000BF5B1C00AE42
      1300AB3D1200A6371000A43E1300A9714300CBC7C400D8D6D400DEDDDC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006DA795003BBD
      95003BBD95003BBD95003BBD95003BBD95003BBD95005FA89200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000427D520071A98100EEF4
      ED00EBF3EC00E9F1E900E6F0E600E4EFE400E2EEE300E0ECE000DEECDE00DDEA
      DD00DBE9DB00D9E9DA00D8E7D800388C55001D512E00FCF0E900D4AA8C00FEF8
      F500B68A6A00FCEDE300FCEBE100D1A68700FEF8F500B68A6A00FAE6D900FAE0
      D100F1A06C0060483000000000000000000000000000000000000000000076CA
      DD007DD8EB007CD6EA007CD5E9007BD4E90085D8EB00A4E7F2005A97B0005C9F
      BC005DA1BE0062AAC9005EBEC20026B84C0033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000898110062B565000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C16F1E00CC712500D5843100EFC18D00FDE1BF00F6D3A500EEC8
      9A00ECD1B000D2873C00CB702200C5652200CB6D2400CA692200C15F1E00B44A
      1500AF441300A93D1200AA431400DAC0B3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006DA997003BBF96003BBF
      96003BBF96003BBF96003BBF96003BBF96003BBF96003BBF96005FA992000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046815A0077AD8500F0F6
      EF00EDF4EE00EBF3EC00E9F2E900E6F0E600E4EFE40081AD88002B512F002B51
      2F002B512F002B512F00D9E9DA003E8E590020553100FDF3EE00D5AB8D00FEF8
      F500B68A6A00FCF0E900FCEFE700D1A68700FEF8F500B68A6A00FCEADF00FAE2
      D300F1A2700060483000000000000000000000000000000000000000000076CA
      DD007DD9EC007DD8EB007DD7EB007CD6EA0086DAEC00A5E8F2005A97B0005EA3
      C1005FA5C30054ACAD0026B94E0033CB670033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA315000495070060B3
      6200000000000000000000000000000000000000000000000000000000000000
      000000000000C2701D00D3772600F0C49200FFEDD800E19A3300E0952800E093
      2800E2A14D00E1B68600CE762300C8682200CD6F2500CC6C2400C2611F00B750
      1700B2491500AD421300AF481500ECDDD4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006EA997003BC197003BC197003BC1
      97003BC197003BC197003BC197003BC1970045C49C003DC198003BC197005FAB
      9300000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004B865E007DB38A00F3F8
      F30051A356002B512F002B512F002B512F002B512F00237927006CB475004798
      5C004A9D4B0026613900DBEADC0042905C0022583400FDF3EE00D6AC8F00FEF8
      F500B68A6A00FCF0E900FCEFE700D3A98A00FEF8F500B68A6A00FCEADF00FAE4
      D600EEA4750060483000000000000000000000000000000000000000000076CA
      DD007EDBED007EDAED007DD9EC007DD8EB0082DAEC00A2E7F2005C9BB4005C9C
      B6005FA6C40031BB530066D88D0033CB670033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000191
      0200AED7AF000000000000000000000000000000000000000000000000000000
      000000000000C1701B00D97F2800FFF3E600EDB66700E59F2C00E7A12E00E69E
      2B00EAAC5500EDC49500D0792400CA6B2300D2742600CB6D2400CA692100BF5A
      1A00BC561900B44E1600AF4E1600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006EA997003BC399003BC399003BC399003BC3
      99003BC399003BC399003BC399003BC39900DCF4ED0089DBC2003BC399003BC3
      990060AB94000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000538E670086B99300F4F9
      F500F2F7F20051A3560064B271004FA65600237927007EC1850047985C004A9D
      4B002661390081AD8800DDEADD0048926000255C3700FEF7F300D7AF9200FEF8
      F500B68A6A00D5AB8E00D4AA8C00D3A98A00FEF8F500B68A6A00FCEFE700FAE5
      D800EEA4750060483000000000000000000000000000000000000000000076CA
      DD007FDDEE007FDCEE007EDBED007EDAED007EDAEC007DD9EC0083D8E9008BCB
      DA005B98B2004ED07300E9FCEC0059D5840033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000099
      0000209521000000000000000000000000000000000000000000000000000000
      000000000000BE6E1A00D87E2800E3993800E59B2D00E5A02E00E8A22F00F3C7
      8600FDE4C700DD954500CF772300CA6A2300D1762500CB6C2400CC6E2400C360
      1D00C2601C00B8531800AF4F1500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007DAB9E003BC59A003BC59A003BC59A003BC59A003BC5
      9A003BC59A003BC59A003BC59A003BC59A00F6FCFA0086DBC1003BC59A003BC5
      9A003BC59A0060AD950000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000057926A0086B99300F7FA
      F700F4F9F500F3F8F30051A356002E83310086C18C0047985C004A9D4B002661
      390060A0610060A06100DFECE0004D95630028603B00FEF7F300D9B19400FEF8
      F500B68A6A00FDF4EE00FDF3ED00D5AB8D00FEF8F500B68A6A00FCEEE400FBE8
      DC00EDA77B0060483000000000000000000000000000000000000000000076CA
      DD0080DFEF007FDEEF007FDDEE007FDCEE007EDBED007EDBED0089DFEF00AAEB
      F4005A97B00051B9A80070DF9000E9FCEC0059D5830033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000099
      000001900100BEDEBF0000000000000000000000000000000000000000000000
      000000000000BA6C1900D8822B00E0922D00EEC07B00FFF6EB00FFF4E700EDBF
      8000D98C2A00CF782600CB6F2500CC6D2400D3742600CC6D2300D0732500CF71
      2200CE6F2100B9571A00AB521300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000040BE97003BC69B003BC69B003BC69B003BC69B0043C8
      9F0070D5B60051CCA6003BC69B0040C89E00FFFFFF0078D8BA003BC69B003BC6
      9B003BC69B003BC69B0060AD9500000000000000000000000000000000000000
      000000000000000000000000000000000000000000005D986F008FC19B00F9FC
      F900F7FAF700F4F9F500348C370098CB9D005CA9660052A35300337042000066
      0000E6EFE600E2EEE300E0ECE000529868002C633E00FEFAF800D9B29600D9B1
      9400B68A6A00FDF5F000FDF5F000D6AC8F00FEF8F500B68A6A00FDF3ED00FBE8
      DC00EBAA7F0060483000000000000000000000000000000000000000000076CA
      DD0081E0F10080DFF00080DFF00080DEEF007FDEEF007FDDEE008AE0F000ACEC
      F5005A97B00066B6D90053BFB00070DF9000E8FCEB0059D5830032CB660030C8
      60002BC3570026BE4C0054CA64002CBA420012AB24000AA31500029B05000099
      0000009501007FBF7F0000000000000000000000000000000000000000000000
      000000000000B66A1700D8812D00EBB97F00FFFAF500F2CC8A00E9B04C00DC93
      2D00D6892A00CD742700CA6E2600D2732700D4752600CC6E2300D2762500D376
      2400CE702200BA571A00A9551200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000065AD96003BC89C003BC89C003BC89C0043CAA000DFF6
      EF00FFFFFF00F9FDFC006BD5B4004FCEA600FFFFFF0069D5B3003BC89C006ED6
      B60086DDC20047CBA2003BC89C0060AE95000000000000000000000000000000
      00000000000000000000000000000000000000000000619E730094C5A000FBFC
      FB00F9FCF900429446009ACBA4006CB475005CA96600427D520051A35600348C
      370000660000E6F0E600E4EFE400589A6B002F674200FEFAF800FEFAF800FEFA
      F800FEFAF800FEF9F600FEF8F500D7AE9100D6AD9000B68A6A00FDF3ED00FBEA
      DF00E9AC830060483000000000000000000000000000000000000000000076CA
      DD0081E2F20081E1F10081E1F10080E0F00080E0F00080DFF0008BE2F100AEED
      F5005A97B00068BADD0068BBDF0057C6B8006FDF9000E5FCE90057D4810030C8
      60002BC3570026BE4C003BAF4400A5F3AE007ADA82000AA31500029B05000099
      0000009900004FA74F0000000000000000000000000000000000000000000000
      000000000000AE651300DB883200FFFEFE00F9E9D400E5A53500E5A53800DB93
      3400F2D2AD00DB955300D6812F00D67E2D00D4762900D3762500D87E2900D579
      2500D0722200B9561900A75D0F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000055B496003BCA9E003BCA9E0070D8B800FFFF
      FF0052D0A900D2F3E900B4EBDA005ED3AF00FFFFFF005BD3AE0090E1C800FEFF
      FF00F5FCFA00DFF6EF003FCBA0003BCA9E0062AF970000000000000000000000
      0000000000000000000000000000000000000000000069A47A009ACBA400FCFD
      FC0045974900A7D5B10074BA7B0074BA7B004F8D55005681590064B2710051A3
      5600348C370000660000E6F0E6005C9D6F00316A4500FEFAF800FEFAF800FEFA
      F800DBB49800FEFAF800FDF7F300FDF7F300FDF7F300FDF7F300FDF7F300FCEF
      E700E6B18D0060483000000000000000000000000000000000000000000076CA
      DD0082E4F30082E3F20081E2F20081E2F20081E1F10081E1F1008CE4F200AFEE
      F6005A97B00069BDE2006ABEE4006FC9EF005CD0C4006DDF8F00E0FCE50054D2
      7B002BC3570026BE4C000F991E0040C5720082F1A100A2EDA6000FA111000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      000000000000AA621100DC8B3500F6E3CA00FFFFFE00E9AF4600E8AE4800F4DA
      B500FFF8F000DA8A3500DB893400D57F2D00D0762900D77C2900DB812A00D67A
      2500D0722200B6571700B1712F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000055B497003BCC9F0053D2AB00FAFE
      FD00D0F3E800F8FDFC0090E2C8006DD9B700FFFFFF004DD1A800CBF1E500B4EC
      DA0061D6B200FFFFFF005BD4AF003BCC9F003BCC9F0062B09700000000000000
      000000000000000000000000000000000000000000006EA98000A1D0AC00FFFE
      FE0069AF6E0067AA6B0064A5680059975B00F6F9F600F3F8F300568159005681
      59005681590056815900E9F2E900619E7300336D4700DEBA9F00DEB99E00DDB7
      9C00DCB69B00DBB49800DAB39700D9B29500D9B19400D8AF9300FDF6F100FCF0
      E900E4B4950060483000000000000000000000000000000000000000000076CA
      DD0083E5F40082E5F40082E4F30082E4F30082E3F30082E3F2008DE6F300B1EF
      F6005A97B0006BC0E5006BC2E70070CBF20075D3FD005CD1C50069DF8D00D7FB
      DD004ECE710026BE4C00109F21005A97B0007EDD9E0063E1850066CC67000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      000000000000C1894900D1873000DF984000E2A44C00F6E0B800F3D8A800F9E9
      D300E19F4400DF973D00DD923A00D7843300D2792C000282BC00DF8A2E00D67B
      2500CE6F2200AA5C1100EAD3C300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000055B698003BCEA0006DDA
      B800B4ECDA0090E3C9003DCFA1007CDEBF00FFFFFF003FCFA2009AE6CE00FAFE
      FC00E9F9F400E8F9F40041D0A3003BCEA0003BCEA0003BCEA00079AE9E000000
      0000000000000000000000000000000000000000000075B08600A4D2AF00FFFE
      FE00FDFEFD00FDFEFD00FBFCFB00F9FCF900F8FBF800F6F9F600F3F8F300F2F7
      F200F0F6EF00EDF5ED00EBF3EB0065A17600356F4900FEFAF800FEFAF800FEFA
      F800DDB89C00FEFAF800FEF8F500FEFAF800FEF8F500D9B19400FEF8F500FDF1
      EA00E4B4950060483000000000000000000000000000000000000000000076CA
      DD0083E7F50083E6F50083E6F40083E5F40083E5F40082E5F4008EE7F400B3F0
      F7005A97B0006CC3E9006CC4EB0071CDF50075D4FE0075D5FF005CD1C60064DF
      8A00C9FBD00046CA6600109F21005A97B0000000000066D78B007DDF87000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      000000000000D6AB7A00C77F2900E19E4300E1A14600FFFFFF00E4AC4D00FFFE
      FE00E2A34700DE984000DC913B00D7863500D27C2E000282BC00E28E3000D67C
      2700CB6C2000AF6D270000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000055B899003BD0
      A2003BD0A2003BD0A2003BD0A2008AE3C800F4FCFA003BD0A2003CD0A20081E1
      C30099E6CE0050D5AC003BD0A2003BD0A2003BD0A2003BD0A2004EBC99000000
      0000000000000000000000000000000000000000000079B58A00A7D5B100FFFE
      FE00FFFEFE00FFFEFE00FDFEFD00FBFCFB00FAFCFA00F8FBF800F6F9F600F4F9
      F500F2F7F200EFF6EF00EEF4ED0068A2780037714B00FEFAF800FEFAF800FEFA
      F800DEBA9F00FEFAF800FEF8F500FEF8F500FEF8F500DAB39700FDF7F300FDF2
      EC00E2B5990060483000000000000000000000000000000000000000000076CA
      DD0084E9F60084E8F60084E7F50084E7F50083E7F50083E7F5008FE9F500B5F1
      F7005A97B0006DC6EC006EC7EE0072CEF70075D5FF0075D5FF0075D5FF005CD1
      C6005BDE8500B7FBC00025A732005A97B00000000000BDEDCD0070DB81000099
      00000098000060AF600000000000000000000000000000000000000000000000
      00000000000000000000A9631300E2A44A00DF9F4700E2A84E00E9B75700E5AC
      4F00DA974400D68D3D00DA933F00D5863600D68634000282BC00E09134000282
      BC00C4661E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000055B8
      99003BD2A3003BD2A3003BD2A3009AE8CF00E6F9F3003BD2A3003BD2A3003BD2
      A3003BD2A3003BD2A3003BD2A3003BD2A3003BD2A3003BD2A3004EBC9A000000
      000000000000000000000000000000000000000000007DB98F00A7D5B100A4D2
      AF00A1D0AC009DCCA8009ACBA40094C5A0008FC19B0086B9930086B993007DB3
      8A0079AF870074AB820071A981006EA67C0037714B00E2C0A600E1BEA400E1BE
      A400E0BCA200DFBAA000DEB99E00DDB89C00DCB69B00DBB49800FDF7F300FDF3
      ED00E1BA9E0060483000000000000000000000000000000000000000000076CA
      DD0085EAF70085EAF70084E9F70084E9F60084E9F60084E8F60091EBF600B7F2
      F8005A97B0006FC8EF006FC9F10072D0F80075D5FF0075D5FF0075D5FF0075D5
      FF005CD1C6004DDA7B004EBC6C005A97B00000000000CBF0D8005DCF6B000099
      0000009201008EC78E0000000000000000000000000000000000000000000000
      00000000000000000000D1A46D00E5A94D00E0A44B00DDA14B00E6B45600E6AF
      5200DB9B4600DD9A4500E09E4600D4873800D98C38000282BC00E094350012F7
      FE00B66316000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000055BA9A003BD4A4003BD4A4009AE9D000CDF4E8003BD4A4003BD4A4003BD4
      A4003BD4A4003BD4A4003BD4A4003BD4A4003BD4A4003BD4A40050BD9A000000
      000000000000000000000000000000000000000000007DB98F0079B58A0075B0
      860070AC81006BA67D0065A17600609B73005A956D00538E67004F8A62004B86
      5E0046815A00427D52003D78510039744E0037714B00FFFEFE00FEFAF800FFFE
      FE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FDF5F000FDF3
      EE00E1BA9E0060483000000000000000000000000000000000000000000076CA
      DD0086ECF80085EBF80085EBF80085EAF70085EAF70085EAF70092ECF700B9F4
      F9005A97B0006FCAF20070CCF30072D0F90075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0086DCFD00B7F3F8005A97B0000000000098E3B20051C355000099
      0000018C02000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B87E3600DAA44700E2AD5300DB9F4B00E7B6
      5700E1AA5000E2A74D00E0A34A00DA933F00E19D42000282BC0012F7FE00B06C
      2300F4E7DD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000055BC9B003BD6A6003CD6A60044D8AA003BD6A6003BD6A6003BD6
      A6003BD6A6003BD6A60055BB9C0081B2A30042CAA0003BD6A60054BC9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0BDAF00FFFEFE00FEFAF800FFFEFE00FFFEFD00FFFEFD00FFFD
      FC00FFFCFB00FFFCFB00FFFEFE00FFFCFB00816B57007A634E00725B45006A53
      3C006048300060483000000000000000000000000000000000000000000076CA
      DD0086EDF90086EDF90086ECF90086ECF80086ECF80085ECF80093EEF800BBF5
      F9005A97B00070CCF40070CDF50073D1FA0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0086DDFE00B9F4F9005A97B000000000006CCB79001BA71D000097
      010070B971000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C28E4B00EABC5C00C5A55F00C3A2
      5C00C19C5900C7A45C00C19A56000282BC003079990012F7FE00BF6E1A00F2E1
      D300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BD9C003BD8A7003BD8A7003BD8A7003BD8A7003BD8
      A7003BD8A7003BD8A700DEE0DF000000000093B5AB003BD8A70055BC9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A7522900000000000000000000000000000000000000
      000000000000D0BDAF00FFFEFE00EFD2BD00EFD2BD00EDD0BB00EDCFB900FFFE
      FD00FFFDFC00FFFCFB00FFFEFE00FFFCFB00C1AE9F00FBE8DC00F5DED000EED4
      C3006048300000000000000000000000000000000000000000000000000076CA
      DD0087EEFA0087EEFA0086EEFA0086EEF90086EDF90086EDF90094EFF900BDF6
      FA005A97B00071CEF70072CEF70074D3FC0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0087DDFE00BBF5FA005A97B0000000000009A11300039A050030A0
      3300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000041EDEF0012F7FE0012F7
      FE0012F7FE0012F7FE0012F7FE0012F7FF0023CAD600DEECF300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000055BE9D003BDAA8003BDAA8003BDAA8003BDA
      A8003BDAA8003BDAA800A5BCB600000000006BB59D003BDAA80058BC9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E68F6300A7522900000000000000000000000000000000000000
      000000000000D1BFB000FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFD00FFFE
      FD00FFFDFC00FFFCFB00FFFEFE00FFFCFB00C2B0A000FBE8DC00F5DED000654E
      36000000000000000000000000000000000000000000000000000000000076CA
      DD0087F0FB0087F0FB0087EFFB0087EFFA0087EFFA0087EEFA0095F0FA00BFF7
      FB005A97B00072D0F90074D2FC0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0087DDFE00BEF7FA005597A60033AB380009A11300219A2600CEE7
      D000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000012F7FE0074885A009976
      2F0098762E0096742C0094732C0083785C0046B4AD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000055BF9E003BDBA9003BDBA9003BDB
      A9003BDBA9003BDBA9003BDBA9003DD4A3003BDBA9003BDBA9005CBA9D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7976F00E89A7100BD6A4100A752290095482300954823008C4421000000
      000000000000D1BFB000FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFD00FFFD
      FC00FFFDFC00FFFCFB00FEFAF800FFFCFB00C4B2A300FBE8DC006C553E000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0088F1FC0088F1FC0088F1FB0088F0FB0087F0FB0087F0FB0096F2FB00C1F8
      FB005A97B00075D4FE0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0088DEFE00C0F8FB002F9A5C000D9E190044A94800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C1884600C1782600C37B2800BB6F
      1800B56A1500AB631100A8610F00C2692600A84F260098422800D7BAB3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000055BE9D003BDBA9003BDB
      A9003BDBA9003BDBA9003BDBA9003BDBA9003BDBA9003BDBA90094B6AC000000
      000000000000000000000000000000000000000000000000000000000000E89C
      7600F9C2A600F5AE8800EC9F7600DB8C6300D87D4F00BD6A4100954823000000
      000000000000D0BDAF00CFBDAF00CFBDAF00CCB9AB00CBB8AA00CAB7A900CAB7
      A900C9B6A700C9B6A700C9B6A700C6B4A400C4B2A300836E5900000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0088F2FD0088F2FC0088F2FC0088F2FC0088F1FC0088F1FC00A2F5FC00C3FA
      FC005B98B10075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0088DEFE00C2F9FC005A9EAF00A1D3A40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CC985C00C1762200D48F4300D3924800C077
      2200B86D1800AC641100A8610F00D07B3000AE5427009C4528009B4D37000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A6BDB60095B6
      AC0095B6AC0095B6AC0095B6AC0095B6AC0097B7AC00B9C6C200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EAA68400F9C2A600BD6A4100E79A7300E68F6300D87D4F00E27239000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0089F3FD0089F3FD0089F3FD0089F3FD009FF6FD00B6F9FD00C5FBFD009CDA
      E4005EA0B90075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF009DE8FE00C4FBFD0063A5C1000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F6E7D800D69F5000BF672700E8B47900E5B68100C27F
      3200BD742200AE661300B06D1C00D18C4C00BF6F3800A44A25009D442700DBC0
      BA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E89C7600B7633A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0089F4FE008DF5FE00A1F7FE00BFFBFD00C2F9FB009EE3ED007FCEDF006EBF
      D90078D1F7008ADFFF008ADFFF009EE9FE009EE9FE00B2F2FE00B2F2FE00C6FC
      FD00C6FCFD00C6FCFD00C0F7FA0066ABC8000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D1A26900D4A45800D69C5200E1A46000D3995700A65F
      0E00AE661300B26D1A00D7984500DBA15F00C9804600A84D2500A0452500B072
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E89C7600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD00AAF9FE00C6FDFE00BFF7FA009AE0EB007BCDDF0095DDE900B9F4FA00C9FD
      FE00C9FDFE00C9FDFE00C9FDFE00B1EBF300B1EBF3009AD8E8009AD8E80088CA
      DF0082C6DC0071B8D40072B6D200C5E0EB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AB671900B16E1D00C38C3E00C77A2400D2873500A65F
      0E00A65F0E00E4963A00EAAD5700E8B56B00D6904C00B1522200A7492200BE87
      7600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD00BBF5F90096DEEA0075C9DC0089D1E300A2E0EC009BD9E8009BD9E80083C6
      DC0083C6DC006BB3D1006BB3D1008EC4DB008EC4DB00B4D7E600B4D7E600D9EA
      F100D9EAF1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7C6A200D5812500BA6F1800B66B1500BE721B00AA62
      1000A65F0E00DC8A2C00E59D4200E5A24F00D47E3000B4542000A74B1F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008DD0
      E10074C6DB0070BDD60073B7D3008EC4DB0097C9DD00B4D7E600BCDCE900D9EA
      F100D9EAF1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1DAC100BF844100B3732900BB7D
      3800C993560000000000F1DAC100A7621300B5712B00F0E0D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D8D7D700D3D3D300D1D1D100D2D2D200D4D4D400DCDC
      DC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FFC6C6C600AAAAAA00DFDFDF00000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000414141002D2D2D00424242006767
      6700656565006565650065656500656565006565650065656500656565006565
      65006969690055555500323232002B2B2B00C8C8C80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E0DE
      DD00C8C5C500A8A8A80095979700919193009090920091919300939393009A9B
      9B00B5B5B500D8D8D80000000000000000000000000000000000000000000000
      00000000000000000000DBDBDB00D6D6D600D2D2D200D0D0D000D0D0D000D2D2
      D200D6D6D600DBDBDB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FFCBCBCB00ADAD
      AD00A0A0A000ABABAB00C5C5C500E0E0E000E0E0E000CFCFCF00000000FFD5D5
      D500CDCDCD00000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000003030300017171700303030005A5A
      5A00575757005757570057575700575757005757570057575700575757005757
      57005D5D5D00454545001E1E1E0019191900CACACA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B6B7BB0087B7
      E4005BB1F4003EAEF7002FAAF8002AA7F90025A8F9002AA8F80033ACF8003FAF
      F60052ACF2007FAEDE00A6AAB00000000000000000000000000000000000D0D0
      D000C1C1C1009395A500AD968A00F3B36C00F9BD7500FBC17D00FBC47F00FAC1
      8000F3BD7B00E3B47F00C6B4A300C2C2C200D0D0D00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FFD1D1D100B3B3B300A2A2A200AFAFAF00CFCFCF00F4F4F400F8F8
      F800F4F4F400F0F0F000EBEBEB00E6E6E600E1E1E100C0C0C000CDCBCB00E8E8
      E700CFCECD00000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000005F5F5F0061616100767676008989
      890095959500989898009A9A9A009C9C9C009C9C9C009C9C9C009B9B9B009898
      9800949494008A8A8A007272720053535300C0C0C00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A9C5E00056C4FC0031BB
      FF0056D1FF004FC6FF0074E7FF0060D4FF006DE2FF0075E3FF0059CDFF006AE1
      FF0049C7FF0045CDFF0053C9FB00B6CBE00000000000D2D2D200B9B9B9009DA1
      AB004C74B7006CBBF6005EB4F60085A8D800FEF3DB00FEF5DF00FEF5DF00FEF3
      D900FEECCC00FED9AA00FDC78600DCA87300ACA8A300BABABA00D4D4D4000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FFB5B5B500A1A1
      A000BABABA00E4E4E400F5F5F500F9F9F900FCFCFC00FEFEFE00FCFCFC00F9F9
      F900F5F5F500F0F0F000ECECEC00E6E6E500DDD3C900E3DDD800EAE9E800EAE9
      E900C9C8C700D5D5D500CACACA00000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000007B7A7A00B8B5B600B0B3B800C3C5
      C600CDCCCC00D9D9D900E5E5E500F0F0F000F1F1F100E6E6E600D8D8D800CBCB
      CB00BDBEBE00989CA0008C8A8C0073707000B5B5B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001E9EFD0056D0FF004FC0
      FF0071DEFF0062D0FF008DF6FF0072DEFF0084EAFF008AF1FF0066D4FF007FE6
      FF0053BFFF0051C2FF0052CAFF003DADFD00D6D6D600B2B2B2008993A500437B
      C5008DD2FB00B8F6FF00C7F6FF001C74E500F3E9C800FEF1C700FEF1C800FEF2
      CB00FEF4D000FEF5D900FEEED100FED6A700FDB16200BB9C7E00B2B2B200D7D7
      D700000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFDFDFDF00DEDEDE00E6E6
      E600EBEBEB00F0F0F000F4F4F400F8F8F800FCFCFC00FEFEFE00FCFCFC00F3EB
      E300E6CEB600DDB48A00D59A5F00D0853900D17E2A00DBC3AB00DEDDDC00E0DF
      DE00E7E6E500EAE9E800D1D0D000000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000000000000059676E00759EA300BE975F007967
      560036393B00525252006969690080808000848484006F6F6F00585858003B3D
      3E004E4340009E76400063766A003C5E6A00B7B6B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000128DFD0072DEFF0078E5
      FF008AF2FF008FF8FF0094FAFF0094FBFF0097FBFF0096FAFF008EF5FF0090F7
      FF007EE9FF0074DEFF005ECFFF0027A1FF00C8C8C800A0A0A0003A6EBD0099DA
      FD00A5F0FF007FE8FF00A8F0FF0087D0FC0084A5CF00FEEBBE00FEECBF00FEEC
      C000FEEABB00FEE7B800FEE9BC00FEEFCD00FEDEB500FCAD5E00BA977800C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FFF2F2F200E1E1E100E5E5
      E500EAEAEA00EFEFEF00EFEAE400E9D4BE00E2BB9400DAA26900D1883F00CF7D
      2900D07D2800D07E2800D17E2900D17E2900D07E2900D6BFA700D5D4D300D2D1
      D000E3E2E100EAEAE900D8D7D700B6B6B600B4B4B400D7D7D700000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000646E740087A0AD00B98E74007664
      5D002D3031004B4B4B00636363007A7B7B007F7F7F006A6A6A00515152003335
      3500483F3F008C6048005B616400425A6600BFBFBF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00C9C9C900C3C3C300B5B5B500A7A7A70094949400848484007B7B7B007D7D
      7D008989890099999900AAAAAA00B8B8B800BFBFBF001998F8007DE2F60063C4
      E40059B4D80053AFD40052ACD2004FABD3004DADD5004EB1DD0052BBEF0057C7
      FB005DC9FD005FD1FF006CD9FF003AAEFD00D1D1D1005B83BD007BC4FA009FE9
      FF0074DFFF0074DFFF0075DFFF00B9ECFF00207BE800E9D9B800FEE5B500FEE5
      B500FEE4B200FEE1AD00FEDEA900FEDBA500FEE6BB00FEDEB700FB994700A797
      8A00B4B4B400BBBBBB00BABABA00BCBCBC00BFBFBF00C0C0C000C1C1C100C1C1
      C100C2C2C200C3C3C300C5C5C500CFCFCF00000000FFF5F4F300DED9D400D9BB
      9D00D5A26E00D08A4400CE7C2900CF7C2800CF7C2700D17E2900D17E2900D07E
      2900D1802A00D1802A00D1802900D2812A00D2812A00D1BAA200DAD8D800E2E1
      E000E4E3E200DCDBDA00E6E5E500EAE9E800EAE9E800B3B3B300D5D5D5000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000E3E3E300A0A0A1008E888500C8C0BC00B6B5B500B5B2
      AF00ABA6A200B5B0AC00C0BCB800CCC7C300CCC8C400C0BCB800B3AFAB00A6A1
      9D00A09C98008A8A890087817E0088807B0081818200C2C3C300000000000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00C9C9C900C3C3C300B6B6B600A7A7A7000058DC000058DE000058DE000058
      DE000058DB008191A500ABABAB00B8B8B800B9B9B9002092DB0067B8C300488E
      A6003D7C960038779200357691003474920034769400347B9B003A8DB60047A8
      D90051BDF4005ECDFC0079E3FF0047B9FF00CED7E4002F85EB00A8E3FF006ED8
      FF0068D5FF0069D7FF0067D5FF008DE0FF008ACBFB00809DC800FEDCA600FEDC
      A700FEDBA400FED9A100FED69D00FED69A00FED69B00FEE4BB00FDD1A400BF7B
      47007E7E7E008181810081818100838383008383830082828200868686008787
      87008787870087878700818181009F9F9F00000000FFF6F3ED00E3B98600D284
      3400CF7C2700CF7C2800D07D2800D07E2800D17E2900D17F2900D2812A00D281
      2A00D2812A00D3822B00D3832C00D3832C00D4842C00D9C2A900D4D3D200DEDD
      DC00E1E0E000D6D5D500E1E0DF00EBEAE900EBEAE900DFDFDF00EDEDED000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000B2B2BA0031327500BBB4AA00D6CCC100D3C9BF00D5C9
      C000D7CBC300D6C9C100D5C7BF00D4C5BE00D4C5BE00D6C7C000D8CAC200DBCC
      C400DBCEC400DDD0C500E1D4C800D1C6B5004D4C830056577500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE005DA0F3008EBFF700529F
      F600045CE0000058DE000000000000000000ADADAD00ACB6BD00B9BBBB00B2B2
      B200ADADAD00A6A5A500AAAAAA00ACACAC00A6A5A500B2B3B300A6A5A500AABB
      BC006EA1A4008EEEF50077DDFF0097D7FD0084ADEB007ABFF9008ADBFF0059CA
      FF005BCDFF005CCDFF005ACBFF005FCCFF00A3DEFE00287EEA00E8C79A00FED2
      9500FED19300FED09100FECD8E00FECE8E00FECE8F00FED49A00FEE5C200F9A6
      68008C6B550066666600636363005E5E5E005B5B5B0057575700525252004E4E
      4E004C4C4C0048484800464646004D4D4D00000000FFF9F2E800E4B88300D284
      3500D07C2800D07E2800D17F2900D17F2900D2812A00D3822B00D2822B00D484
      2C00D5852E00D4862E00D5872F00D5872F00D5872F00DBC4AC00D6D6D500D7D7
      D600E1E1E000E1E0DF00E6E5E400D0CFCE00E5E4E300DFDFDF00F4F3F2000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000E0E0E00063616400ADACB400C4D1E600B8CBDF00B5CE
      DF00B2D1E000B0D4E000ADD7E100AEDAE300B0DDE500A9D9E300A6D5E200A3D1
      E100A0CAE0009DC2DE009DBDE200A6ADC000706D6E00ABABAC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE0092C0F7000669E900408C
      EE0060A9F8000058DE000259DD00BCBCBC00B5B5B500A6A5A500D2D2D200A6A5
      A500E4E4E400A6A5A500E1E1E100E8E7E700A6A5A500DDDDDD00A6A5A500BEBD
      BD00B7B8B80087A9BB00AFDBF80000000000186EE40095D4FE005EC6FF004EC1
      FF0050C4FF0050C3FF004FC1FF004DBFFF007CD0FF0076BAF900859BC000FDC5
      8100FEC48000FEC37E00FEC47E00FEC47E00FEC68000FEC88600FEDEB100FCCE
      A400F5A37400F1F1F100EAEAEA00E6E6E600E3E3E300E2E2E200E1E1E100E0E0
      E000E0E0E000E0E0E000DDDDDD004B4B4B00000000FFF9F1E600E2AE7200D385
      3500D17E2900D17F2900D2812A00D2812B00D3832C00D5862E00D5872F00D588
      2F00D78A3100D78A3200D78B3200D98C3400D88C3400D5BEA600DBDAD900E7E7
      E600E5E5E400DBDAD900E7E6E500DAD8D800E2E1E000D6B18900F4ECE3000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000092908D0094A7C6008E92A0009490840099A1
      8D009DB19300A1C19800A3C99B008BC4960075C19A0095C6980089B996007CA8
      93006F948E00647D8600375B89003770BE00A09D9900E1E1E100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE008FBFF800096CEA000061
      E5005199F1004292F2000058DE00A8A8A900D5D5D500A6A5A500EBEBEB00A6A5
      A500EFEEEE00B8B7B700F2F1F100FFFFFF00BCBCBC00F7F7F700B1B1B100C7C7
      C700D5D5D500A6A5A50000000000000000002B7DEA009BD7FF003FB5FF0043B7
      FF0044B9FF0045BAFF0044B7FF0040B6FF0043B4FF009ACBEE0084A1CA00DBB6
      9100FEB66B00FEB86C00FEB86A00FEB96C00FEBA6F00FEC07800FECC8E00FEDE
      BB00F27C4400D8C8BF00D2D2D200D1D1D100CDCDCD00C8C8C800BEBEBE00B1B1
      B100A2A2A20095959500E5E5E50059595900000000FFF9F1E600E2AE7200D486
      3600D17F2900D2812B00D3822C00D4852E00D6873000D6893100D78B3200D88D
      3400D98E3500DA903600DA913700DA913800DB923800D4BEA600D5D4D300E3E2
      E200DFDEDE00DAD9D800E4E3E200E4E3E200ECEBEA00D9A05D00F4E8DA000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000009F9793008593AC009A694800D9A87600E0B5
      7900EBC58000F3D58700FBDD8C00D1C26600AFB25800FBD16900EFC86900E5B4
      6100D9A05B00CC8F5600A35F3300546A9100ADA6A500E0DFDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000059DF009AC6F900026BEB000064
      E7000462E40081BDFB00146BE400A4A4A500EDEDED00A6A5A500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EEEEEE00ECEC
      EC00DDDDDD00A6A5A50000000000000000003887ED0093D1FF0031ABFF0037AE
      FF0039AFFF0038B0FF0037AEFF0033AAFF0036A6FA00C6CCD100D1D2D2009EA6
      B900FDAA5900FEAC5800FEAC5800FEAF5C00FEB26100FEB66900FEBD7400FEE3
      C000F17F5300F7DCCF00F9F9F900F9F9F900F9F9F900F8F8F800F8F8F800F7F7
      F700FBFBFB0093939300E0E0E00069696900000000FFF9F1E600E2AE7200D488
      3700D2812B00D4842D00D5872F00D6893100D78B3300D98E3500D98F3600DB92
      3900DC933A00DC943B00DD963D00DE983E00DE983E00E6D1B800F0F0EF00EEEE
      ED00E1E0E000E7E7E600E7E7E600D1D0D000E9E8E700DAA25F00F5E8DA000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000E8EAED0079A7B7007598B6008F522C00BA7C3E00C790
      4600D4A54C00DEB85300E8C75800CBBC5600B1B25900E9C34400DFB94400D19E
      3900BE802A00A9611C008B3A0800446B980061BEDD00ADC1CA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000093BCF300025BDF009FCAFA00006FEF000067
      EA000062E6003787ED0068ADF800A7A8AA00F7F7F700EBEBEB00E5E5E500E1E1
      E100E1E1E100DFDFDF00DBDBDB00DBDBDB00D9D9D900D9D9D900DDDDDD00DFDF
      DF00E7E7E700BFBFBF000000000000000000408EEE0084CAFF0026A1FF002AA4
      FF002CA5FF002BA5FF0029A3FF0026A1FF0060A4DC00E6E6E600ECECEC00AFB3
      BB00EDAE7000FD9F4200FDA14600FDA34A00FDA64E00FDAC5800FDB36300FEDF
      BA00F0815E00F3CDBF00F3F3F300F2F2F200F0F0F000EEEEEE00EDEDED00ECEC
      EC00FBFBFB0099999900DADADA0077777700000000FFF9F1E600E3AF7300D68A
      3800D4852E00D5872F00D78A3200D98D3500DA903700DB933900DD953C00DE98
      3F00DF9A4000E09C4200E09C4300E19E4400E19E4400DDC9B000E2E1E100E3E3
      E200E3E3E200E4E3E200E5E4E400DBDBDA00E3E2E100DCA76300F5E8DA000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000B1DBF3002ADCFF003F99CA00883A0D00A6581200BA76
      2400CB913500D8A74600E4BA5600CBB15500BAB46300F1CF6E00E5C26C00DBAF
      6300CD985600B7783F0094410F002A619B0036DFFF007DC0D500000000000000
      00000000000000000000000000000000000000000000000000000000000065A1
      F0000C66E200015BDF00005ADE00005ADF000E66E40097C7FB000074F300006E
      EE000067E9000263E60085BFFC00AEAFAF00E5E5E500EBEBEB00E5E5E500E1E1
      E100DDDDDD00DBDBDB00D9D9D900D7D7D700D3D3D300D5D5D500D7D7D700DFDF
      DF00F1F1F100CBCBCB0000000000000000003785EB0087C8FF001B99FF001D9B
      FF001F9CFF001E9CFF001D9AFF002298F900BEC2C700E9E9E900E8E8E800D1D1
      D100D2B89F00FB973700FD963400FD983900FD9D3E00FDA34800FDAA5300FEDA
      B100EB755C00F0C8BC00F2F2F200F0F0F000EFEFEF00EDEDED00ECECEC00EBEB
      EB00FAFAFA0095959500D6D6D6007D7D7D00000000FFF9F1E600E3B07500D78D
      3B00D6883000D88C3400DA8F3600DB923A00DD963C00DE983F00DF9A4100E09C
      4300E29F4600E2A14700E2A14800E3A34A00E3A34A00DBC7AF00E0DFDF00DDDC
      DC00E8E7E700EAEAE900E6E5E500E7E6E500EFEEEE00DFAB6600F6E9DA000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000BEDDF3006EDCFF00648CBF0082534500B0866B00BB96
      7400C6A77D00CDB48300D5BF8800B6AA6B00A5AD6E00E0D09300D6C39100CFB6
      8D00C6A98A00BE9F8A00A073620037609D007DDAFF0097BDD300000000000000
      00000000000000000000000000000000000000000000A6CBF7000162E6000461
      E400307CE60071A5ED0095BDF200A4C7F400A3CAF700067DF700017AF7000176
      F4000171EF00026CEC00046AEA00A7B1BD00B8B7B700E0E0E000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F1F1F100D5DBE20000000000000000002978E80086C6FF001191FF001292
      FF001393FF001292FF001291FF006998C300DFDFDF00E0E0E000DBDBDB00E3E3
      E300B3B2B000E7A36400FD8F2800FD912B00FD953200FD9B3C00FDA24700FED4
      A800E8635600F0CEC600F1F1F100EFEFEF00EEEEEE00ECECEC00EAEAEA00E9E9
      E900F9F9F9008F8F8F00D2D2D2007F7F7F00000000FFF9F1E700E4B27700D990
      3E00DA8D3600DA903800DC933B00DE973E00DE994000E09C4300E2A04700E2A2
      4900E4A44C00E5A64E00E5A85000E5A85000E6AA5100DAC7AE00D8D7D700E0DF
      DF00EAE9E900E7E7E600E8E7E700E3E2E200E6E6E500E1B16C00F6EADB000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000CEDDF00082B8FA003481F6004686ED004B8CF3005192
      EE0058A1EB005EB5EC0062CCEE0060D8EB005EDAEB0064D4F10060BFEE005BA9
      ED005497EE004C8DF2004A88EF002B7DF3007EB3F800ADBACD00000000000000
      000000000000000000000000000000000000AACFF900016AEA002278E9009ABF
      F00078B1F20056A8FB003296FA001C8BFA000680F9000280FB000280FA00037E
      F900047CF7000479F4000478F3000979F1005A93CF008AA6C300BFC3C700C4C4
      C500C4C4C500C8C9C900C9C9C900C9C9C900C8C9C900C3C4C500B9BCC000B4C0
      CC004988D6007CB3F30000000000000000001367E20079BBFC002297FF000B8C
      FF000B8DFF000B8CFF00198DF400B9BBBE00E1E1E100D3D3D300D1D1D100DEDE
      DE00CCCCCC00C6AF9B00FC892100FD8C2300FD8F2900FD943100FDA65200FBBF
      9700E1474800EEDBD600EEEEEE00EDEDED00EBEBEB00E9E9E900E8E8E800E5E5
      E500F7F7F7008D8D8D00D0D0D0007C7C7C00000000FFF9F2E700E5B47900DB93
      4100DA903800DD953D00DF994100E09C4500E2A04900E3A34B00E4A54D00E5A8
      5000E6AA5200E8AD5600E9AE5800E8AE5800E9AF5900E1CEB600DADAD900DEDD
      DD00E8E8E800D9D9D800E7E6E600E1E0E000E7E6E600E3B57200F6EBDC000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000008699B8004D95FF0073ADFF006DAAFF006FAB
      FF0075B2FE007ABCFC007DC6FC0080CEFE0081CFFE007EC9FC007CC0FC0078B6
      FD0072ADFF006FAAFF0073ADFF004D95FF00819DCB00DEDEDE00000000000000
      0000000000000000000000000000000000000672ED00257EEC009AC0EF00004D
      C1000056CB000181FC000281FC000283FD000384FD000384FD000585FC000685
      FC000684FB000783FB000884FB000985FB00026BEB00005BE000005AE000026A
      EB000B8BFE000A8BFE000989FD000886FB000681FA00047BF6000275F3007EBC
      FB004F9FF7002E8AF200000000000000000083ADED0064A7F5004AA9FF000689
      FF000689FF000889FF006F94B700D3D3D300D7D7D700CBCBCB00CBCBCB00D1D1
      D100D9D9D900AAA8A600E3A67000FD881D00FD8B2200FD902B00FEB56E00F39C
      8300E26B6A00ECECEC00EBEBEB00EAEAEA00E7E7E700E6E6E600E4E4E400E1E1
      E100F4F4F40089898900CFCFCF007B7B7B00000000FFF9F2E700E6B67B00DD96
      4400DC943C00DE984000E19D4600E3A14A00E4A54E00E6AA5400E7AD5800E8AF
      5900EAB05B00EAB25D00EAB25E00EBB46100EBB46100DBC8B100E0E0DF00E2E1
      E100E6E6E600E0DFDF00ECECEB00EDECEB00EFEEED00E5B97A00F6ECDD000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000B1ADA9005593DE002598ED0044C2F5005ECC
      F70078CDF70092D2F700ACDBF900C8E6FC00D7EEFD00C2E9FC00A4E5FC0086E9
      FD006AE4FD0052CAF70038A7EF00227DDF00B1B5BE00DDDDDC00000000000000
      0000000000000000000000000000000000000274F000A6CDF800014DC100004C
      C0001187F8001E93FF002295FF002899FF000F8DFF000D8CFF001992FF0038A2
      FF0044A8FF001379EF000165E7000165E7000061E5009FFFFF009FFFFF00005F
      E4000164E7000F79F00046AAFF0046A9FE0038A1FD002F99FB001988F700449E
      F90095CAFD000575F0000000000000000000D9E6F8002877E70073B9FD000C8B
      FF000387FF001989EF00B1B2B300DADADA00C4C4C400C4C4C400C4C4C400C2C2
      C200D4D4D400C4C4C400BDA69300FB8F2D00FD891F00FD953400FCBC8B00E257
      5B00E7B7B300E8E8E800E7E7E700E6E6E600E4E4E400E2E2E200E0E0E000DFDF
      DF00EFEFEF0086868600CECECE0079797900000000FFF9F2E800E7B77D00DE99
      4700DF984100E19D4500E2A14A00E5A65000E7AB5500E8AE5900EAB25F00EBB5
      6300EBB66700ECB86800ECB86700ECB86800EEBA6A00E7D5BE00EAEAEA00F1F0
      F000E7E6E600E0DFDF00ECEBEB00DFDEDE00E7E7E600E6BE8000F7ECDE000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000B1ACA900ABBDCE000991E20023C6F1004DD6
      F50071D6F50094DBF600B4E2F800D0EBFB00DCF3FD00CCF4FD00AFF5FD008FFE
      FF006DF9FD004BD7F500149FE90070A5D200CAC2BC00DBDBDD00000000000000
      0000000000000000000000000000000000000378F100C6DFFB001365D200238F
      F7002E9CFF00359FFF003AA1FF0044A8FF0046A9FF004CACFF0051AFFF0052B0
      FF004BABFE000C75ED00A6FFFF00A6FFFF00A5FFFF00A5FFFF00A5FFFF00A0FF
      FF0095F1FD002186ED00248AF50057B5FF005BB5FF0058B3FF0054AFFE003E9E
      FA00D2E9FF001180F4005EAAF700000000000000000084ADED00579DF20051AB
      FF000185FF007E94AD00CACACA00CACACA00BCBCBC00BDBDBD00BDBDBD00BCBC
      BC00C1C1C100D2D2D200A4A3A300E1A77200FD881C00FEB06800EF8A7500E06D
      6C00E6E6E600E5E5E500E4E4E400E3E3E300E1E1E100DFDFDF00DDDDDD00DADA
      DA00E9E9E90085858500CCCCCC0077777700000000FFF9F2E800E8B97F00E09D
      4B00DF9B4300E3A14A00E5A54F00E6AA5500E9AF5A00EAB36000ECB66600EDBA
      6B00EEBC7000F0BF7400F1C07600F1C07400F1C17400EFDEC800F6F5F500F6F5
      F500EAE9E900E0DFDE00E8E7E700DBDADA00EDECEC00E8C38800F7EDDF000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000AEAEAE00E8E1DD0071BBE30043C9F10065D8
      F4007DD6F30092D7F400A3D9F400B0DBF500B4E3F700AEE9F800A1EEFA008EF8
      FC0079F4FA005ED6F30049B0E500BFC6CC00BDBBBA00DBDBDB00000000000000
      000000000000000000000000000000000000057CF5003690F200BEDCFA009CCF
      FF002E9BFF0037A1FF003EA4FF00004CC0004BADFF004EAFFF0050B1FF0052B3
      FF0053B4FF001B88F6000473F0000473F0000470EF00B0FFFF00B0FFFF00278E
      F2009DF8FE0088E6FC000977F10056B7FF0057B6FF0059B6FF005AB5FF0055B1
      FF0090CAFE0089C5FD001787F6000000000000000000000000003266B5006AAC
      F700509FEA00A9A9AC00D3D3D300B4B4B400B6B6B600B6B6B600B7B7B700B6B6
      B600B4B4B400C9C9C900BCBCBC00AD9E9200F8AD6700F3977900DE5A5A00E4E2
      E200E4E4E400E2E2E200E2E2E200E0E0E000DFDFDF00DDDDDD00DADADA00D7D7
      D700E3E3E30083838300CBCBCB0077777700000000FFF9F3E800E8BB8100E2A0
      4E00E29F4800E4A44E00E7A95400EAAF5B00EBB56100EEB96900F0BD6F00F1C0
      7400F2C27800F3C47C00F3C77F00F4C88100F3C67D00E6D5C000E8E8E700EBEB
      EB00F0EFEF00F4F3F300EEEDED00DEDDDD00E8E7E700E9C78E00F7EEE0000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000B1B1B100ECECEB00E4E5E400D7E2E600DAE5
      E700DDE6E900E0E7EA00E1E8EB00E2E8EB00E2E8EB00E1E8EA00DEE7E900DBE6
      E600D6E3E300D0DCDF00D4D8DA00DAD8D700BDBEBE00DBDBDB00000000000000
      000000000000000000000000000000000000AFD6FB001084F5000C82F6006FB2
      F600A5D4FE0036A2FF003EA6FF0044ABFF004BB0FF004DB2FF0050B5FF0050B6
      FF0051B7FF0050B7FF0033A2FC00198AF700087BF400C0FFFF00C0FFFF002D97
      F700A5FBFF00A2FDFF000478F40053B9FF0056B9FF0059B9FF0058B7FF0055B4
      FF005AB4FF00D4EBFF000D84F80000000000000000000000000065738800045B
      DF00849BB700CCCCCD00D1D1D100B7B7B700B1B1B100B2B2B200B2B2B200B1B1
      B100B7B7B700C8C8C800D8D8D800A09E9C00EDBFB000DF625D00DDBBB600DBDB
      DB00DBDBDB00DBDBDB00DADADA00D9D9D900D7D7D700D4D4D400D1D1D100CECE
      CE00DBDBDB0083838300CACACA0076767600000000FFF9F3E900E9BC8300E3A3
      5100E5A44E00E8A95400EAAF5B00ECB56100EFB96800F0BD6F00F1C17500F2C3
      7A00F3C57E00F3C88200F3C98400F3CA8600F3CB8700E2D2BD00DADADA00E1E1
      E100F0F0F000F6F6F500F0F0EF00E2E2E100F1F0F000ECCB9500F8EFE2000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000ACACAC00EAEAEA00E7E6E600EAE7E700EBE8
      E800ECEAE900ECEAEA00ECEBEA00ECEBEA00EBEAEA00EBE9E900E9E7E700E7E4
      E400E5E2E200E2DFDE00DEDCDB00DADADA00BDBDBD00DBDBDB00000000000000
      000000000000000000000000000000000000000000000000000054AAF9001489
      F800B2D6F9006ABBFF003BA7FF0042ACFF0048B1FF004BB5FF004DB7FF004FB9
      FF004FBAFF002A9EFC00299AFA00A5DFFD00D0FAFF00D0FFFF00D0FFFF00A7FF
      FF00A5FFFF009DF9FF000983F90051BDFF0055BCFF0058BDFF0058BAFF0055B8
      FF004FB2FF00D0EAFF001089F90000000000000000000000000078787800B0B9
      C6004C73AF00A2A4A700BABABA00CBCBCB00D6D6D600D8D8D800D8D8D800D9D9
      D900D0D0D000C2C2C200ACACAC00B1837700E46A6600DECDCA00DDDDDD00DCDC
      DC00DCDCDC00DCDCDC00DBDBDB00DADADA00D9D9D900D7D7D700D4D4D400D2D2
      D200E0E0E00081818100C7C7C70076767600000000FFFAF3E900EABE8500E5A6
      5500E7A75100E9AD5800EBB25F00EDB76500EFBB6C00F0BF7200F0C27800F2C5
      7D00F4C98300F3CA8600F5CD8A00F5CF8D00F5CF8E00E1D2BD00E6E6E600DAD9
      D900E9E9E800E7E7E700F1F1F100F5F5F500F5F5F500EECE9C00F8EFE3000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000A9A9A900EAEAEA00E7E7E700E8E8E800E9E9
      E900E9E9E900EAEAEA00EAEAEA00EAEAEA00E9E9E900E8E8E800E7E7E700E5E5
      E500E2E2E200E0E0E000DCDCDC00D7D7D700B8B8B800DADADA00000000000000
      0000000000000000000000000000000000000000000000000000000000002493
      F9005EABF40070BDFD00015FE3001F8AF50047B4FF004BB8FF004CBAFF004EBD
      FF004FBEFF002CA1FC00E6F9FF00F7FFFF00D8F7FF00E4FFFF00E4FFFF00A9FF
      FF0097F4FF00299EFC0029A0FD0054C2FF0056C2FF0057C0FF0058BFFF0055BC
      FF0050B6FF00D6EEFF00118DFB0000000000000000000000000077777700C1C1
      C100808080009AB4DC0092A0B600A9A9A900A0A0A000A4A4A400A4A4A400A1A1
      A100A7A7A700B0B0B000D1A89D00E0A39300E0DEDE00E0E0E000E0E0E000E0E0
      E000E0E0E000E0E0E000E0E0E000DDDDDD00DDDDDD00DCDCDC00DADADA00D8D8
      D800EAEAEA0081818100C5C5C50076767600000000FFFAF3E900EABE8600E6A8
      5700E7A85300E8AD5900EAB36000EEB96900EFBD7000F0C17600F2C67D00F3C9
      8300F5CC8800F6CF8D00F6D09000F7D29200F7D39400E0D1BD00E2E2E200E9E9
      E900E8E8E800E4E4E300F1F1F000F4F4F400F6F6F500EFD3A200F8F0E4000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000A6A6A600EBEBEB00E6E6E600E9E9E900EAEA
      EA00EAEAEA00EAEAEA00EAEAEA00EAEAEA00E9E9E900E8E8E800E7E7E700E5E5
      E500E3E3E300E0E0E000DADADA00D4D4D400B3B3B300DADADA00000000000000
      00000000000000000000000000000000000000000000000000000000000081C2
      FB00289AFB00196FE5000158DE000262E4001087F90048BAFF0050C0FF0053C4
      FF0055C6FF003AAFFD00F3FFFF00E3F6FF0043B0FE00F7FFFF00F7FFFF0028A0
      FE00259FFD0033ABFE0058C9FF0058C9FF005AC8FF005CC7FF005CC5FF0059C2
      FF0078CCFF00C9E9FF001891FB0000000000000000000000000077777700BEBE
      BE0080808000EBEBEB00D7D7D700D9D9D900D8D8D800CFCFCF00D1D1D100D4D4
      D400D9D9D900DEDEDE00E0E0E000E1E1E100E0E0E000E1E1E100E0E0E000E1E1
      E100E0E0E000E0E0E000E1E1E100E1E1E100DFDFDF00DFDFDF00DEDEDE00DBDB
      DB00F0F0F00080808000C3C3C30075757500000000FFFAF3EA00EBBF8700E6A9
      5800E7AA5500EAB05D00EBB56400EEBB6C00F1C07400F1C37A00F2C78100F5CC
      8800F5CE8D00F6D19100F8D49600F7D59800F8D69A00E6D8C400DFDFDF00F7F7
      F600EEEEED00D9D9D800EBEBEB00DFDFDF00F7F7F600F0D7A800F9F0E5000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000A2A2A200EEEEEE00E7E7E700E9E9E900ECEC
      EC00ECECEC00ECECEC00ECECEC00ECECEC00EBEBEB00EAEAEA00E9E9E900E7E7
      E700E4E4E400DEDEDE00D7D7D700D3D3D300AFAFAF00D9D9D900000000000000
      0000000000000000000000000000000000000000000000000000000000002098
      FA001984F2000E62E1000C69E5001D87F400309EFD0033A2FF0056C6FF005DCC
      FF0060CEFE003EB3FE00E9FAFF00EFFBFF0084D0FE00FCFFFF00FCFFFF0036AC
      FE0033AAFF0035ACFF0062D1FF0061D0FF0061CFFF0062CFFF0063CEFF0061CB
      FF00C2EAFF007DC8FE0076BEFC0000000000000000000000000077777700BBBB
      BB0080808000F0F0F000DDDDDD00DEDEDE00DFDFDF00E1E1E100E1E1E100E2E2
      E200E2E2E200E2E2E200E4E4E400E4E4E400E4E4E400E4E4E400E3E3E300E4E4
      E400E4E4E400E4E4E400E4E4E400E3E3E300E1E1E100E1E1E100E0E0E000E0E0
      E000F4F4F40080808000C1C1C10075757500000000FFFAF3EA00EBC08900E7AB
      5B00E7AB5700EAB15F00EDB86800EFBC6F00F1C17600F2C67E00F4CA8500F5CE
      8B00F7D19100F7D39500F8D69A00F9D89E00F9D89E00E1D4C100E7E7E700DCDC
      DC00E7E7E700E9E9E900ECECEC00DEDEDD00EAEAEA00F2DAAD00F9F1E6000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000009C9C9C00EBEBEB00E2E2E200E3E3E300E4E4
      E400E6E6E600E7E7E700E8E8E800E8E8E800E6E6E600E5E5E500E3E3E300DFDF
      DF00DBDBDB00D5D5D500CFCFCF00CDCDCD00AAAAAA00D7D7D700000000000000
      000000000000000000000000000000000000000000000000000000000000259C
      FC002788F000318FF2004AA9FB009AD1FE00E4F3FF006FCFFE0065D4FF0068D6
      FF006AD8FF003EB5FF00A3DFFF00E2F7FF00FBFFFF00FEFFFF00FEFFFF00FCFF
      FF00F5FFFF004DB8FF0060D1FF006BD8FF006AD8FF006BD7FF006CD7FF008EE0
      FF00D4F1FF00269BFB000000000000000000000000000000000076767600B7B7
      B70080808000F2F2F200E1E1E100E3E3E300E4E4E400E4E4E400E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E4E4E400E4E4E400E4E4
      E400F7F7F70080808000C0C0C00075757500000000FFFAF3EA00EBC08A00E7AC
      5C00E8AD5A00EBB36100EDB86900F0BE7200F2C37900F3C78100F5CD8800F6CF
      8E00F7D39300F8D69900F8D89D00F9DAA100FADBA300E0D3C000E2E2E200EEEE
      EE00F0F0F000F8F8F800F3F3F200E5E5E500EFEEEE00F2DBB200F9F1E6000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000090909000A2A2A200A8A8A800B2B2B200BBBB
      BB00C2C2C200C7C7C700CACACA00CCCCCC00CBCBCB00CACACA00C6C6C600C1C1
      C100B9B9B900AEAEAE00A3A3A3009C9C9C008787870000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002BA0
      FC00A2D4FF00DAEFFF00EAF6FF00B2DFFF005DC0FD00DFF6FF0099E7FF0074E1
      FF0076E3FF0066D5FF0041B8FF0042B7FF0049BCFF00FEFEFF00F9FDFF0047BB
      FF0046BAFF003FB6FF0077E3FF0074E0FF0074E0FF0075E1FF00A2EBFF00E2F8
      FF0042ADFF00AED9FD000000000000000000000000000000000076767600B3B3
      B30080808000F3F3F300E5E5E500E6E6E600E6E6E600E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E8E8E800E8E8
      E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E7E7E700E7E7
      E700F8F8F80080808000BFBFBF0075757500000000FFFAF3EA00ECC18A00E7AD
      5E00E8AD5A00ECB46300EDB96B00F0BF7300F3C47B00F3C88200F5CD8900F7D1
      9000F8D49500F8D79B00FADAA000F9DBA400F4D7A600EADFD200EBEBEB00EAEA
      E900E1E1E100DCDCDC00E9E9E800E8E8E800EFEFEF00F3DEB500F9F2E7000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000A5A5A500D9D9D900DEDEDE00E0E0
      E000E2E2E200E2E2E200E3E3E300E2E2E200E2E2E200E1E1E100DFDFDF00DCDC
      DC00D9D9D900D3D3D300C9C9C900C0C0C000AFAFAF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002AA3
      FC0032AAFF0038AFFE003DB3FE0040B5FE0048B6FC0058C0FF00D5F4FF00E6FB
      FF00ABF2FF0082ECFF0081EBFF0082ECFF0042B8FF0049BCFF0047BBFF0044BA
      FF008BF3FF0086EFFF0081EBFF007EE7FF00ABEFFF00E8FBFF00D0F3FF0043B1
      FF009CD3FD00000000000000000000000000000000000000000076767600AFAF
      AF0080808000FAFAFA00F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500FAFAFA0080808000BEBEBE0075757500000000FFFAF3EA00ECC18B00E8AD
      5F00E9AE5C00EBB46300EFBA6C00F0BF7300F2C47B00F5CA8300F5CE8D00F2CE
      9600F4D8AE00F4E1C400F2E6D500E6E0D900E5E5E400E5E5E500E7E7E700E3E3
      E300E6E6E600EAEAEA00F2F2F200EDEDEC00F3F3F200F4DEB700F9F2E7000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      000000000000000000000000000000000000BBBBBB00E3E3E300EDEDED00EBEB
      EB00ECECEC00EDEDED00EDEDED00EDEDED00EDEDED00ECECEC00EAEAEA00E8E8
      E800E5E5E500E3E3E300E8E8E800BDBDBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A3DBFD0048BBFF0074CD
      FF00D2F5FF00EDFDFF00C6F8FF00ACF5FF00A1F5FF0092F7FF0093FAFF0099FC
      FF00A8FBFF00B1F7FF00C7F7FF00EDFCFF00C9F1FF0069C7FF0041B0FD00B9E0
      FD0000000000000000000000000000000000000000000000000076767600ADAD
      AD008A8A8A008A8A8A008B8B8B008D8D8D008F8F8F0091919100939393009494
      94009696960098989800999999009C9C9C009E9E9E00A1A1A100A2A2A200A4A4
      A400A4A4A400A4A4A400A4A4A400A5A5A500A4A4A400A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500BEBEBE0075757500000000FFFAF3EA00ECC18B00E8AD
      5F00E9AE5C00ECB56800EABB7700EECA9600F2D9B600F1E3CF00E7DFD700E6E6
      E500E7E7E700E3E3E300E4E4E400E8E8E800ECECEC00EEEDED00ECEBEB00F0F0
      F000EDEDED00EDEDED00EDEDED00E6E6E600F1F1F100F5DFB700F9F2E7000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000000000000000000000BABABA00F5F5F500F3F3
      F300F3F3F300F5F5F500F5F5F500F6F6F600F6F6F600F5F5F500F3F3F300F1F1
      F100EEEEEE00ECECEC00EAEAEA00C7C7C7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0EBFD0054BE
      FD0049BCFF006BCAFF00B2EAFF00D6F7FF00E6FBFF00FCFFFF00FFFFFF00FBFF
      FF00E7FCFF00D4F6FF00ADE8FF0062C6FF0042B7FF0075C6FD00000000000000
      000000000000000000000000000000000000000000000000000075757500AEAE
      AE008585850085858500878787008A8A8A008D8D8D008F8F8F00929292009393
      930096969600999999009A9A9A009D9D9D009E9E9E00A0A0A000A2A2A200A3A3
      A300A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400BFBFBF0076767600000000FFF7EFE500E5B78000E8C5
      9A00F0DBC200EDE4DA00F3F1F000F4F4F400F1F1F100EAEAEA00EFEFEF00F0F0
      F000EEEEEE00EBEBEB00E8E8E800E7E7E700E4E4E300E2E2E200E0E0DF00DFDE
      DE00DCDBDB00E0E0E000DCDBDB00E5E4E400F5F4F400F5E0B700F9F2E7000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      00000000000000000000000000000000000000000000CBCBCB00DEDEDE00F3F3
      F300F1F1F100F3F3F300F5F5F500F7F7F700F7F7F700F5F5F500F2F2F200EFEF
      EF00ECECEC00F2F2F200B5B5B500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6EDFD0094D5FD0059BFFD0045B9FF0045B9FF0045B9FF0045B9FF0046BA
      FF0047BAFF0046BAFF0076CAFD00ACDDFD000000000000000000000000000000
      000000000000000000000000000000000000000000000000000085858500A1A1
      A100AEAEAE00B1B1B100B4B4B400B9B9B900BEBEBE00C4C4C400C8C8C800CCCC
      CC00CFCFCF00D2D2D200D4D4D400D5D5D500D7D7D700D7D7D7008C8C8C00DBDB
      DB00F0F0F000F0F0F000FB922700FFEF8A00FFEF8A00118BFE006BB7FF006BB7
      FF00118BFE000D5FDC00B5B5B5008C8C8C00000000FFF4EBE200CA722700D1AA
      8900D5B49700DCBB9C00DDBD9E00DEBFA000DFC1A100E4C7A600E8CCAC00E9CE
      AE00EBD1B100ECD4B400EDD7B700EEDABA00EEDCBE00F0DEC200F1E1C500F2E2
      C800F2E4CB00F3E5CD00F4E6CF00F4E7D200F4E8D400F6DEB000F9F1E7000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F1F1F100F7F7
      F700F7F7F700F7F7F700F8F8F800F9F9F900F9F9F900F8F8F800F8F8F800F7F7
      F700F6F6F600F7F7F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDEDE008A8A
      8A00828282008383830085858500878787008B8B8B008E8E8E00929292009595
      9500989898009A9A9A009B9B9B009D9D9D009F9F9F009F9F9F00898989008888
      88008888880088888800FC7F1100FF780000FF7800000083FE000084FF000084
      FF000083FE000B5DDA009B9B9B00E1E1E100000000FF000000FFEACFB700DDAD
      7F00DCA67800DAA37100DAA47000DBA67200DDA87400DFAB7600E0AF7800E2B2
      7B00E3B57F00E5B88200E6BC8600E8C08A00E9C28E00EAC59300ECC89700ECCB
      9C00EDCC9F00EECEA100EFD0A400EFD1A700EFD2A800F0D3AB00000000FF0000
      00FF000000FF000000FF000000FF000000FF424D3E000000000000003E000000
      2800000080000000A00000000100010000000000000A00000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF803FFFFFFFFFFFFFC00FFFF0040F
      FFFE000FFFFFFE3FFFFFC00FFF800003FFFC0007FFFFF007FFFFE01FFE000001
      FFF80003FFFFE003FFFC0000FC000001FFF00001E0000001FFFC0000F8000000
      FFF00001C000000180000000F0000000FFE00000C000000100000000E0000000
      FFE00000C000000000000000C0000000FFE00000C000000000000000C0000000
      FFE00000C0000000000000008000000000000000C00000010000000080000001
      00000000C0000001000000000000000000000000C00000010000000000000000
      00000000C0000003000000000000000000000000C00000078000000000000000
      00000000C0000007800000000000000000000000C0000007C000000000000000
      00000000C0000007E00000000000000000000000C0000007F800F83F00000000
      00000000C0000007FC00FC3F0000000000000000C0000007F800FC1F00000001
      00000000C0000007F000FE1F8000000100000000E0000007F0007F0F80000001
      00000000E0000007E0007F078000000300000000E000000FE0007F87C0000003
      00000000E000001FE0003F87E000000700000000E000003FE0003FC3E000000F
      00000000E007FFFFE0003FE3F000001F00000000F01FFFFFE0007FE3F800003F
      00000000FFFFFFFFF0007FF3FE00007F00000000FFFFFFFFF000FFFFFF0001FF
      00000000FFFFFFFFFC01FFFFFFE007FFFFFFC0FFFFFFFC07FC7FFE7FFFFFFFFF
      FFFFC03FFFFFF001F03FF81FFFFFFFFFFFFE001FFFFFE000F01FF01FFFFFFFFF
      FFFC000FFFFFE000E01FF00FFFFFFFFFFFF8001F00002000E01FF00FFFFFFFFF
      FFF0000700002000E000000F9694B5B9FFF0000700002000E000000F90848431
      FFF0080F00002000F00FE01F9FFFFFF1FFE01C0700002000F807C00F9FFFFFF1
      FFE03C0700002000FC03800F92322E01FFE0380700002000C000000F90122601
      FFF0300F00002000C000000F90122601FFE0000F00002000C000000790122601
      FFC0000F000020008000000790122601FFE0001F000020008000000790122601
      FFF0007F000018038000000790122601FF00007F000003FB8000000390122601
      FD0000FF000000038000000390122601F80003FF000000038000000390122601
      F800BFFF000000038000000390122601E0007FFF000000038000000190122601
      E0007FFF000000038000000190122601E060FFFF000000038000000990122601
      C0E07FFF00000003C000000990122601C0C07FFF00000003C000000990122601
      E080FFFF00000003C000000190122E01C000FFFF00000003E0000001FFFFFFFF
      C001FFFF00000003E0000021FFFFFFFFE003FFFF80000003E0000023FFFFFFFF
      E003FFFFE0000003FFFFFFE3FFFFFFFFF81FFFFFF0000003FFFFFFE3FFFFFFFF
      FC9FFFFFFC000003FFFFFFF7FFFFFFFFFF1FF87FFFF01FFFFFFFFFFFFFFFFFFF
      FE0F803FFE0007FFFFFFFFFFFFFFFFFFF804001FF80001FFFFFFFFFFFFFFFFFF
      F000001FE000007FFFFFFFFFFFF80003E000003FE000003FFFFFFFFFFFF80003
      E000003FE000001FFFF9FFFF80000003E000003FF000001FFFF0FFFF80000003
      E000003FF800000FFFE07FFF80000003E000003FF800001FFFC03FFF80000003
      E000001FF80000FFFF801FFF80000003E000000FF80000FFFF000FFF80000003
      E0000007F80001FFFE0007FF80000003E0000007F80001FFFC0003FF80000003
      E0000003F80001FFFC0001FF80000003E0000003F80001FFFC0000FF80000003
      E0000003F80001FFFE00007F80000003E0000003F80001FFFF00003F80000003
      E0000003F80001FFFF80001F80000003E0000083F80003FFFFC0001F80000003
      E0000083FC0007FFFFE0001F80000003E0000083FC0007FFFFF0001F80000003
      E0000087FE0007FFFFF8001FFFF80003E0000087FF000FFFFFFC011FFDF80007
      E000008FFF803FFFFFFE011FF9F8000FE000000FFF807FFFFFFF001FF018001F
      E000003FFF001FFFFFFF801FE018003FE000007FFE001FFFFFFFC03FF01FFFFF
      E00000FFFC000FFFFFFFFFFFF9FFFFFFE00000FFFC000FFFFFFFFFFFFDFFFFFF
      E00000FFFC000FFFFFFFFFFFFFFFFFFFE00007FFFC001FFFFFFFFFFFFFFFFFFF
      E007FFFFFF043FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0FFFFFFFFFFFFE3FFF
      FF00007FFFFFE003FC03FFFFFFC027FFFF00007FFFFFC001E0007FFFF80007FF
      FF00007FFFFF800080001FFFC00001FFFF00007FFFFF800000000FFF800001FF
      FF00007FFFFF800000000FFF8000003FFF00007FE0000000000000008000001F
      FC00003FE0000000000000008000001FFC00003FFF030000000000008000001F
      FC00003FFF000001000000008000001FFE00003FFF000003000000008000001F
      FE00003FFF000003000000008000001FFC00003FFE000003000000008000001F
      FC00003FE0000003000000008000001FFC00003F80000003000000008000001F
      FC00003F00000003000000008000001FFE00003F00000003000000008000001F
      FE00003F00000003000000008000001FFE00003F00000001800000008000001F
      FE00003F00000001C00000008000001FFE00003F00000001C00000008000001F
      FE00003FC0000001C00000008000001FFE00003FE0000001C00000008000001F
      FE00003FE0000001C00000008000001FFE00003FE0000001C00000008000001F
      FE00003FE0000003C00000008000001FFE00007FE0000003C00000008000001F
      FF00007FE0000007C00000008000001FFF0000FFFF80000FC00000008000001F
      FF8000FFFFC0003FC00000008000001FFF8001FFFFF000FFC00000008000001F
      FFC003FFFFFFFFFFC0000000C000003F00000000000000000000000000000000
      000000000000}
  end
  object PicDlg: TOpenPictureDialog
    Left = 364
    Top = 460
  end
  object PopBkGr: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopBkGrPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 444
    Top = 268
  end
  object PopFunc: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopFuncPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 444
    Top = 300
    object ItemCat: TMenuItem
      Caption = 'D'#7841'ng th'#7875' lo'#7841'i'
      OnClick = ItemCatClick
    end
    object Outlook2: TMenuItem
      Tag = 1
      Caption = 'D'#7841'ng Outlook'
      OnClick = ItemCatClick
    end
    object TaskList2: TMenuItem
      Tag = 2
      Caption = 'D'#7841'ng danh s'#225'ch'
      OnClick = ItemCatClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ItemCloseAll: TMenuItem
      Caption = #272#243'ng t'#7845't c'#7843' th'#7875' lo'#7841'i'
      OnClick = ItemCloseAllClick
    end
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 396
    Top = 396
    object Phngban1: TMenuItem
      Action = CmdDmPhongban
    end
    object LoithVIP1: TMenuItem
      Action = CmdDmChucvu
    end
    object Nhnvin1: TMenuItem
      Action = CmdDmQuatrinh
    end
    object N8: TMenuItem
      Action = CmdDmNhomlv
    end
    object Bclng1: TMenuItem
      Action = CmdDmBacLuong
    end
    object ChitkhutheoloithVIP1: TMenuItem
      Action = CmdDmPhucap
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N24: TMenuItem
      Action = CmdDmHopdong
    end
    object LdoKhenthngKlut1: TMenuItem
      Action = CmdDmLyDoKTKL
    end
    object aimlmvic1: TMenuItem
      Action = CmdDmDiaDiemLamViec
    end
    object Ningkkhmbnh1: TMenuItem
      Action = CmdDmDKKhambenh
    end
    object Nguntuyndng1: TMenuItem
      Action = CmdDmNguonTuyenDung
    end
    object hitbTisnlaong1: TMenuItem
      Action = CmdDmThietBiTaiSanLaoDong
    end
    object Khchhng1: TMenuItem
      Action = CmdDmVangmat
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object aphng1: TMenuItem
      Action = CmdDmdialy
    end
    object Ngnhng2: TMenuItem
      Action = CmdDmNganhang
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Ngnhng1: TMenuItem
      Action = CmdDmCanhBao
    end
    object Vtral1: TMenuItem
      Action = CmdDmBieuMau
    end
    object Danhmckhc2: TMenuItem
      Action = CmdDmHotroNS
    end
  end
  object PopDulieu: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 364
    Top = 428
    object Ktthc2: TMenuItem
      Action = CmdQuit
      ImageIndex = 1
    end
  end
  object PopHethong: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 396
    Top = 428
    object Cuhnhhthng1: TMenuItem
      Action = CmdConfig
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object imtkhu2: TMenuItem
      Action = CmdSetpass
    end
  end
  object PopTrogiup: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 428
    Top = 428
    object Hngdnsdng2: TMenuItem
      Action = CmdHelp
      ImageIndex = 0
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object Cchipthnggp1: TMenuItem
      Action = CmdFaq
    end
    object Danhschkhchhng1: TMenuItem
      Action = CmdMyCustomer
    end
    object N31: TMenuItem
      Caption = '-'
    end
    object Giithiu2: TMenuItem
      Action = CmdAbout
      ImageIndex = 13
    end
  end
end
