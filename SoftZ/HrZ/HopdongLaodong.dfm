object FrmHopdongLaodong: TFrmHopdongLaodong
  Left = 116
  Top = 93
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7907'p '#272#7891'ng Lao '#272#7897'ng'
  ClientHeight = 573
  ClientWidth = 915
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 915
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton10: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton3: TToolButton
      Left = 128
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 136
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton12: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton13: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btnHopDong: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdHopDong
      ImageIndex = 39
    end
    object ToolButton15: TToolButton
      Left = 460
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 468
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 915
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    Panels = <
      item
        Alignment = taCenter
        Width = 150
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 650
      end>
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 125
    Width = 422
    Height = 427
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'Manv'#9'12'#9'M'#227#9'F'
      'LK_TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
      'SoHopDong'#9'25'#9'S'#7889' h'#7907'p '#273#7891'ng'#9'F'
      'LK_TEN_HDLD'#9'40'#9'Th'#7901'i h'#7841'n h'#7907'p '#273#7891'ng'#9'F'
      'NgayKy'#9'12'#9'K'#253#9'F'#9'Ng'#224'y'
      'NgayHieuLuc'#9'12'#9'Hi'#7879'u l'#7921'c'#9'F'#9'Ng'#224'y'
      'NgayHetHan'#9'12'#9'H'#7871't h'#7841'n'#9'F'#9'Ng'#224'y'
      'GhiChu'#9'25'#9'Ghi ch'#250#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopCommon
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnEnter = CmdRefreshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  inline frD2D: TfrD2D
    Left = 0
    Top = 36
    Width = 915
    Height = 48
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 36
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
      inherited CbOrg: TfcTreeCombo
        Left = 470
        Width = 431
        Items.StreamVersion = 1
        Items.Data = {00000000}
        ExplicitLeft = 470
        ExplicitWidth = 431
      end
    end
  end
  object RzSizePanel1: TRzSizePanel
    Left = 422
    Top = 125
    Width = 493
    Height = 427
    Align = alRight
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 4
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PaEmp: TPanel
      Left = 8
      Top = 0
      Width = 485
      Height = 427
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object PaThongTin: TisPanel
        Left = 2
        Top = 2
        Width = 481
        Height = 174
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PaNhanVien: TPanel
          Left = 0
          Top = 24
          Width = 481
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 22
          DesignSize = (
            481
            24)
          object CbNhanVien: TDbLookupComboboxEh2
            Left = 115
            Top = 0
            Width = 277
            Height = 22
            ControlLabel.Width = 56
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Nh'#226'n vi'#234'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akLeft, akTop, akRight]
            DynProps = <>
            DataField = 'Manv'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'Tennv'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 130
              end
              item
                FieldName = 'ManvQL'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 42
              end>
            DropDownBox.ListSource = DsEmp
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'Manv'
            ListField = 'Tennv'
            ListSource = HrDataMain.DsDMNV
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdManvQL: TDBEditEh
            Left = 395
            Top = 0
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_ManvQL'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaInfo: TPanel
          Left = 0
          Top = 48
          Width = 481
          Height = 128
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          DesignSize = (
            481
            128)
          object Label8: TLabel
            Left = 164
            Top = 52
            Width = 42
            Height = 16
            Alignment = taRightJustify
            Caption = '(th'#225'ng)'
            FocusControl = DBNumberEditEh1
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label108: TLabel
            Left = 322
            Top = 54
            Width = 44
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = 'Ng'#224'y k'#253
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 32
            Top = 76
            Width = 77
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y hi'#7879'u l'#7921'c'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 291
            Top = 78
            Width = 75
            Height = 16
            Alignment = taRightJustify
            Anchors = [akTop, akRight]
            Caption = 'Ng'#224'y h'#7871't h'#7841'n'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object CbMaLoaiHopDong: TDbLookupComboboxEh2
            Left = 115
            Top = 0
            Width = 165
            Height = 22
            ControlLabel.Width = 71
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#272'L'#272'/ PLH'#272
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MaLoai_HopDong'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 130
              end>
            DropDownBox.ListSource = HrDataMain.DsV_HR_LOAI_HOPDONG
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = HrDataMain.DsV_HR_LOAI_HOPDONG
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object EdSoHopDong: TDBEditEh
            Left = 310
            Top = 0
            Width = 162
            Height = 22
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            ControlLabel.Width = 15
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'SoHopDong'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <>
            EmptyDataInfo.Color = clInfoBk
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object CbLoaiHopDong: TDbLookupComboboxEh2
            Left = 115
            Top = 24
            Width = 277
            Height = 22
            ControlLabel.Width = 80
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Lo'#7841'i h'#7907'p '#273#7891'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            Anchors = [akLeft, akTop, akRight]
            DynProps = <>
            DataField = 'MaHopDong'
            DataSource = DsMaster
            DropDownBox.Columns = <
              item
                FieldName = 'TenHopDong'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 200
              end
              item
                FieldName = 'MaHopDong'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 40
              end>
            DropDownBox.ListSource = HrDataMain.DsHR_DM_HOPDONG
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MaHopDong'
            ListField = 'TenHopDong'
            ListSource = HrDataMain.DsHR_DM_HOPDONG
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 2
            Visible = True
          end
          object DBEditEh2: TDBEditEh
            Left = 395
            Top = 24
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MaHopDong'
            DataSource = DsMaster
            DynProps = <>
            EditButtons = <
              item
                Action = CmdLoaiHopDong
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            Visible = True
          end
          object DBNumberEditEh1: TDBNumberEditEh
            Left = 115
            Top = 48
            Width = 45
            Height = 22
            TabStop = False
            ControlLabel.Width = 50
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Th'#7901'i h'#7841'n'
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoThang'
            DataSource = DsMaster
            DecimalPlaces = 1
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 4
            Visible = True
          end
          object CbDate: TwwDBDateTimePicker
            Left = 371
            Top = 48
            Width = 101
            Height = 22
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgayKy'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 5
          end
          object CbToDate: TwwDBDateTimePicker
            Left = 115
            Top = 72
            Width = 165
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NgayHieuLuc'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ShowButton = True
            TabOrder = 6
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 371
            Top = 72
            Width = 101
            Height = 22
            TabStop = False
            Anchors = [akTop, akRight]
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = clBtnFace
            DataField = 'NgayHetHan'
            DataSource = DsMaster
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 7
          end
          object CbHopDongGoc: TDbLookupComboboxEh2
            Left = 115
            Top = 96
            Width = 165
            Height = 22
            ControlLabel.Width = 78
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#7907'p '#273#7891'ng g'#7889'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'Idx_HopDong'
            DataSource = DsMaster
            DropDownBox.AutoFitColWidths = False
            DropDownBox.ColumnDefValues.AutoDropDown = True
            DropDownBox.Columns = <
              item
                FieldName = 'SoHopDong'
                Width = 280
              end>
            DropDownBox.ListSource = DsHopDongGoc
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 300
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'Idx'
            ListField = 'SoHopDong'
            ListSource = DsHopDongGoc
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 8
            Visible = True
            OnDropDown = CbHopDongGocDropDown
          end
        end
        object PaTop: TPanel
          Left = 0
          Top = 16
          Width = 481
          Height = 8
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
        end
      end
      object PaGhiChu: TisPanel
        Left = 2
        Top = 230
        Width = 481
        Height = 195
        Align = alClient
        BevelInner = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGHICHU: TDBMemo
          Left = 2
          Top = 18
          Width = 477
          Height = 175
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GhiChu'
          DataSource = DsMaster
          TabOrder = 1
        end
      end
      object PaDinhKem: TisPanel
        Left = 2
        Top = 176
        Width = 481
        Height = 54
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: File '#273#237'nh k'#232'm'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        DesignSize = (
          481
          54)
        object EdFileAttach: TDBEditEh
          Tag = 1
          Left = 118
          Top = 24
          Width = 354
          Height = 22
          TabStop = False
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 76
          ControlLabel.Height = 16
          ControlLabel.Caption = 'File '#273#237'nh k'#232'm'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'Calc_FileName'
          DataSource = DsMaster
          DynProps = <>
          EditButtons = <
            item
              Action = CmdFilePlus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileMinus
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 1
              Images.HotIndex = 1
              Images.PressedIndex = 1
              Images.DisabledIndex = 1
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end
            item
              Action = CmdFileView
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 2
              Images.HotIndex = 2
              Images.PressedIndex = 2
              Images.DisabledIndex = 2
              Style = ebsGlyphEh
              Width = 30
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
      end
    end
  end
  inline frEmp1: TfrEmp
    Left = 0
    Top = 84
    Width = 915
    Height = 41
    Align = alTop
    AutoSize = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    ExplicitTop = 84
    ExplicitWidth = 915
    inherited Panel1: TPanel
      Width = 915
      ExplicitWidth = 915
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnHint = ApplicationEvents1Hint
    Left = 20
    Top = 232
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 48
    Top = 232
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh s'#225'ch'
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      ShortCut = 16453
      OnExecute = CmdEditExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      ShortCut = 16466
      Visible = False
      OnExecute = CmdReloadExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdClear: TAction
      Category = 'POPUP'
      Caption = 'X'#243'a danh s'#225'ch'
      OnExecute = CmdClearExecute
    end
    object CmdPrint2: TAction
      Caption = 'CmdPrint2'
      OnExecute = CmdPrint2Execute
    end
    object CmdLoaiHopDong: TAction
      OnExecute = CmdLoaiHopDongExecute
    end
    object CmdHopDong: TAction
      Caption = 'H'#7907'p '#273#7891'ng'
      Hint = 'In h'#7907'p '#273#7891'ng'
      OnExecute = CmdHopDongExecute
    end
    object CmdFilePlus: TAction
      Hint = 'Th'#234'm file'
      OnExecute = CmdFilePlusExecute
    end
    object CmdFileMinus: TAction
      Hint = 'X'#243'a file'
      OnExecute = CmdFileMinusExecute
    end
    object CmdFileView: TAction
      Hint = 'Xem file'
      OnExecute = CmdFileViewExecute
    end
  end
  object PopCommon: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 48
    Top = 264
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 19
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 20
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xadanhsch1: TMenuItem
      Action = CmdClear
    end
  end
  object QrEmp: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'select'#9'a.Manv, a.ManvQL,a.Tennv, a.MaChiNhanh, a.MaPhongBan, a.M' +
        'aBoPhan, a.NgayThoiViec, b.TenNhomLV '
      '  from'#9'HR_DM_NHANVIEN a '
      'left join HR_DM_NHOM_LAMVIEC  b on a.MaNhomLV = b.MaNhomLV '
      'where 1=1')
    Left = 48
    Top = 172
  end
  object Filter: TwwFilterDialog2
    DataSource = DsMaster
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#228'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'EmpID'
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'EmpID'
      'Date'
      'To Date'
      'Percent'
      'Leave Type'
      'Comment')
    FilterOptimization = fdUseAllIndexes
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 76
    Top = 232
  end
  object QrMaster: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrMasterBeforeInsert
    AfterInsert = QrMasterAfterInsert
    BeforePost = QrMasterBeforePost
    BeforeDelete = QrMasterBeforeDelete
    AfterDelete = QrMasterAfterDelete
    OnCalcFields = QrMasterCalcFields
    OnDeleteError = QrMasterDeleteError
    OnPostError = QrMasterDeleteError
    Parameters = <>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'HR_LICHSU_HOPDONG'
      ' where'#9'1=1')
    Left = 17
    Top = 172
    object QrMasterCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrMasterUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrMasterCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrMasterUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrMasterManv: TWideStringField
      FieldName = 'Manv'
      OnChange = QrMasterManvChange
    end
    object QrMasterSoHopDong: TWideStringField
      FieldName = 'SoHopDong'
      Size = 50
    end
    object QrMasterMaHopDong: TWideStringField
      FieldName = 'MaHopDong'
      OnChange = QrMasterMaHopDongChange
    end
    object QrMasterSoThang: TFloatField
      FieldName = 'SoThang'
    end
    object QrMasterSoHopDong_Goc: TWideStringField
      FieldName = 'SoHopDong_Goc'
      Size = 50
    end
    object QrMasterNgayKy: TDateTimeField
      FieldName = 'NgayKy'
      OnChange = QrMasterNgayKyChange
    end
    object QrMasterNgayHieuLuc: TDateTimeField
      FieldName = 'NgayHieuLuc'
      OnChange = QrMasterMaHopDongChange
    end
    object QrMasterNgayHetHan: TDateTimeField
      FieldName = 'NgayHetHan'
    end
    object QrMasterFileName: TWideStringField
      FieldName = 'FileName'
      Size = 50
    end
    object QrMasterFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrMasterLK_TENNV: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENNV'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'MANV'
      LookupResultField = 'TENNV'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_TEN_HDLD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TEN_HDLD'
      LookupDataSet = HrDataMain.HR_DM_HOPDONG
      LookupKeyFields = 'MaHopDong'
      LookupResultField = 'TenHopDong'
      KeyFields = 'MaHopDong'
      Lookup = True
    end
    object QrMasterLK_TEN_HDLD_TA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TEN_HDLD_TA'
      LookupDataSet = HrDataMain.HR_DM_HOPDONG
      LookupKeyFields = 'MaHopDong'
      LookupResultField = 'TenHopDong_TA'
      KeyFields = 'MaHopDong'
      Lookup = True
    end
    object QrMasterGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrMasterUPDATE_NAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'UPDATE_NAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterLK_SoThang: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_SoThang'
      LookupDataSet = HrDataMain.HR_DM_HOPDONG
      LookupKeyFields = 'MaHopDong'
      LookupResultField = 'SoThang'
      KeyFields = 'MaHopDong'
      Lookup = True
    end
    object QrMasterCalc_FileName: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Calc_FileName'
      Calculated = True
    end
    object QrMasterLK_ManvQL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_ManvQL'
      LookupDataSet = HrDataMain.QrDMNV
      LookupKeyFields = 'Manv'
      LookupResultField = 'ManvQL'
      KeyFields = 'Manv'
      Lookup = True
    end
    object QrMasterLK_CREATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CREATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'CREATE_BY'
      Lookup = True
    end
    object QrMasterLK_UPDATE_FULLNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UPDATE_FULLNAME'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'UPDATE_BY'
      Lookup = True
    end
    object QrMasterMaLoai_HopDong: TWideStringField
      FieldName = 'MaLoai_HopDong'
      OnChange = QrMasterMaLoai_HopDongChange
      Size = 70
    end
    object QrMasterLK_MaLoai_HopDong_Ma: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaLoai_HopDong_Ma'
      LookupDataSet = HrDataMain.QrV_HR_LOAI_HOPDONG
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'MA'
      KeyFields = 'MaLoai_HopDong'
      Lookup = True
    end
    object QrMasterFileIdx: TGuidField
      FieldName = 'FileIdx'
      FixedChar = True
      Size = 38
    end
    object QrMasterIdx_HopDong: TIntegerField
      FieldName = 'Idx_HopDong'
    end
    object QrMasterIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object DsMaster: TDataSource
    AutoEdit = False
    DataSet = QrMaster
    Left = 20
    Top = 200
  end
  object spHR_TINH_HETHAN_HDLD: TADOCommand
    CommandText = 'spHR_TINH_HETHAN_HDLD;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NGAY_HIEULUC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MA_HDLD'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@NGAY_HETHAN'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 115
    Top = 173
  end
  object PopIn: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 268
    Top = 324
    object Hpngcngtcvin1: TMenuItem
      Tag = 20
      Caption = 'H'#7907'p '#273#7891'ng c'#7897'ng t'#225'c vi'#234'n'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object Hpnghcvic1: TMenuItem
      Tag = 21
      Caption = 'H'#7907'p '#273#7891'ng h'#7885'c vi'#7879'c'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object Hpngthvic1: TMenuItem
      Tag = 22
      Caption = 'H'#7907'p '#273#7891'ng th'#7917' vi'#7879'c'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object Hpnglaong1: TMenuItem
      Tag = 23
      Caption = 'H'#7907'p '#273#7891'ng lao '#273#7897'ng'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Phlchpnghcvic1: TMenuItem
      Tag = 30
      Caption = 'Ph'#7909' l'#7909'c h'#7907'p '#273#7891'ng h'#7885'c vi'#7879'c'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object Phlchpngthvic1: TMenuItem
      Tag = 31
      Caption = 'Ph'#7909' l'#7909'c h'#7907'p '#273#7891'ng th'#7917' vi'#7879'c'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
    object Phlchpnglaong1: TMenuItem
      Tag = 32
      Caption = 'Ph'#7909' l'#7909'c h'#7907'p '#273#7891'ng lao '#273#7897'ng'
      ImageIndex = 64
      OnClick = CmdPrint2Execute
    end
  end
  object QrFileContent: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrFileContentBeforeOpen
    Parameters = <
      item
        Name = 'FileIdx'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from '#9'SYS_FILE_CONTENT'
      'where IdxKey = :FileIdx')
    Left = 188
    Top = 348
    object QrFileContentCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrFileContentUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrFileContentCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrFileContentUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrFileContentFileChecksum: TWideStringField
      FieldName = 'FileChecksum'
      Size = 50
    end
    object QrFileContentFileContent: TBlobField
      FieldName = 'FileContent'
    end
    object QrFileContentIdxKey: TGuidField
      FieldName = 'IdxKey'
      FixedChar = True
      Size = 38
    end
    object QrFileContentFunctionKey: TWideStringField
      FieldName = 'FunctionKey'
      Size = 50
    end
    object QrFileContentIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
  end
  object QrDMHopDong: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QrDMHopDongBeforeOpen
    Parameters = <
      item
        Name = 'MaHopDong'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select dm.FileName, dm.FileExt, fi.FileContent'
      '  from HR_DM_HOPDONG dm'
      '  inner join SYS_FILE_CONTENT fi on fi.IdxKey = dm.FileIdx'
      '  where dm.MaHopDong = :MaHopDong')
    Left = 196
    Top = 276
    object QrDMHopDongFileName: TWideStringField
      FieldName = 'FileName'
      Size = 200
    end
    object QrDMHopDongFileExt: TWideStringField
      FieldName = 'FileExt'
      Size = 50
    end
    object QrDMHopDongFileContent: TBlobField
      FieldName = 'FileContent'
    end
  end
  object DsEmp: TDataSource
    AutoEdit = False
    DataSet = QrEmp
    Left = 50
    Top = 208
  end
  object QrHopDongGoc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select Idx, Manv, SoHopDong, MaHopDong'
      '  from'#9'HR_LICHSU_HOPDONG'
      'order by NgayHieuLuc Desc')
    Left = 289
    Top = 220
  end
  object DsHopDongGoc: TDataSource
    AutoEdit = False
    DataSet = QrHopDongGoc
    Left = 292
    Top = 248
  end
  object spHR_LICHSU_HOPDONG_Get_Last_By_Manv_Ngay: TADOCommand
    CommandText = 'spHR_LICHSU_HOPDONG_Get_Last_By_Manv_Ngay;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Idx'
        Attributes = [paNullable]
        DataType = ftGuid
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@Manv'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Ngay'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 235
    Top = 173
  end
end
