﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DangkyTangCaDanhSach;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, Vcl.Buttons, Vcl.Graphics,
  wwDialog, wwfltdlg, wwFltDlg2;

type
  TFrmDangkyTangCaDanhSach = class(TForm)
    GrList: TwwDBGrid2;
    DsList: TDataSource;
    QrList: TADOQuery;
    ActionList: TActionList;
    CmdRegister: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Panel3: TPanel;
    BtnRegister: TBitBtn;
    BitBtn2: TBitBtn;
    spIMP_HR_DANGKY_TANGCA_InsertData: TADOCommand;
    spIMP_HR_DANGKY_TANGCA_DeleteList: TADOCommand;
    spIMP_HR_DANGKY_TANGCA_Check: TADOCommand;
    Filter: TwwFilterDialog2;
    CmdDel: TAction;
    Xadng1: TMenuItem;
    CmdDelRecordError: TAction;
    Xaccdngangli1: TMenuItem;
    CHECK_RECORD_ERROR: TADOCommand;
    QrListGhiChu: TWideMemoField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    QrListXacNhan_TenTinhTrang: TWideStringField;
    QrListXetDuyet_TenTinhTrang: TWideStringField;
    QrListLK_ManvQL: TWideStringField;
    QrListLK_Tennv: TWideStringField;
    QrTinhTrang_XetDuyet: TADOQuery;
    DsTinhTrang_XetDuyet: TDataSource;
    QrTinhTrang_XacNhan: TADOQuery;
    DsTinhTrang_XacNhan: TDataSource;
    QrListManv: TWideStringField;
    QrListXacNhan_Manv: TWideStringField;
    QrListXetDuyet_Manv: TWideStringField;
    QrListXacNhan_TinhTrang: TIntegerField;
    QrListXetDuyet_TinhTrang: TIntegerField;
    QrListManvQL: TWideStringField;
    QrListTennv: TWideStringField;
    QrListNgay: TDateTimeField;
    QrListDangKyTuGio: TDateTimeField;
    QrListDangKyDenGio: TDateTimeField;
    QrListDangKySoGio: TFloatField;
    QrListXacNhan_Tennv: TWideStringField;
    QrListXetDuyet_Tennv: TWideStringField;
    QrListXetDuyet_ManvQL: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
  private
    mTransNo, countError: Integer;
    mCanEdit, mDelete, isFunctionApproved: Boolean;
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; pTransNo: Integer) : Boolean;
  end;

var
  FrmDangkyTangCaDanhSach: TFrmDangkyTangCaDanhSach;

const
    TABLE_NAME = 'IMP_HR_DANGKY_TANGCA';
    FORM_CODE = 'IMP_HR_DANGKY_TANGCA';
implementation

uses
	Rights, isDb, ExCommon, isLib, GuidEx, isCommon, isMsg, MainData, HrData;


{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';

function TFrmDangkyTangCaDanhSach.Execute;
begin
    mCanEdit := rCanEdit(r);
   	mTransNo := pTransNo;
    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    CloseDataSets([QrList]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    AddAllFields(QrList, TABLE_NAME);
    isFunctionApproved :=  GetFuncState('HR_TANGCA_DUYET');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.FormShow(Sender: TObject);
begin
    with HrDataMain do
        OpenDataSets([QrDMNV]);

	OpenDataSets([QrList, QrTinhTrang_XacNhan, QrTinhTrang_XetDuyet]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetDisplayFormat(QrList, ['GioVao_Ca', 'GioRa_Ca', 'GioVao1_Ca', 'GioRa1_Ca', 'DangKyTuGio', 'DangKyDenGio'], sysMinFmt);
    SetDisplayFormat(QrList, ['DangKySoGio', 'XetDuyet_SoGioHuongLuong', 'XetDuyet_SoGioNghiBu'], sysFloatFmtTwo);
    SetDisplayFormat(QrList, ['XacNhan_Ngay', 'XetDuyet_Ngay'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;

    if not isFunctionApproved then
        grRemoveFields(GrList, ['XetDuyet_Manv', 'XetDuyet_ManvQL', 'XetDuyet_Tennv',
        'XetDuyet_TinhTrang', 'XetDuyet_TenTinhTrang']);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdDelRecordErrorExecute(
  Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_HR_DANGKY_TANGCA_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                mTransNo := 0;
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                ModalResult := mrOk;
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bIsError: Boolean;
begin
    with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdDel.Enabled := bBrowse and (not bEmpty);
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDangkyTangCaDanhSach.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
     if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCaDanhSach.DeleteAllRecord;
begin
    with spIMP_HR_DANGKY_TANGCA_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDangkyTangCaDanhSach.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;


end.
