﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit zkem;

interface

uses
	Classes, zkemkeeper_TLB, Messages, IniFiles;

const
    HexOfNum: array[0..15] of char=('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
    	(*
        ** Time recorder
        *)
	// Events
	EF_ATTLOG		= 1;		// track the attendance log events / OnAttTransaction
    EF_FINGER		= 1 shl 1;
    EF_ENROLLUSER	= 1 shl 2;
    EF_ENROLLFINGER	= 1 shl 3;
    EF_BUTTON		= 1 shl 4;
    EF_UNLOCK		= 1 shl 5;	// unlock
    EF_STARTUP		= 1 shl 6;	// Starting System
    EF_VERIFY		= 1 shl 7;	// fingerprint verification
    EF_FPFTR		= 1 shl 8;	// Extract Fingerprint Feature Point
    EF_ALARM		= 1 shl 9;	// Alarm signals
    EF_HIDNUM		= 1 shl 10;	// RF card number
    EF_WRITECARD	= 1 shl 11;	// write card successfully
    EF_EMPTYCARD	= 1 shl 12;	// removals card successfully

    // Status
    SF_ADMIN			= 1;
    SF_USER				= 2;
    SF_FINGER			= 3;
    SF_PASSWORD			= 4;
    SF_MANAGE_RECORD	= 5;
    SF_INOUT_RECORD		= 6;
    SF_NOMINAL_FP		= 7;
    SF_NOMINAL_USER		= 8;
    SF_NOMINAL_INOUT	= 9;

    WM_FINGER = WM_USER + 1000;
    WM_ENROLL_FINGER = WM_USER + 1001;

function zkConnect(
	CZKEM1: TCZKEM;
    devComType, devIP4: String;
    devSocket, devPort, devId, devBaudrate: Integer
    ): Boolean;
procedure zkDisconnect;
function  zkReconnect: Boolean;

procedure zkSetTime;
procedure zkClearGLog;
procedure zkRegEvent(EventMask: Integer);
procedure zkReadInfo(ls: TStrings);

function  zkReadAllUserID: Boolean;
function  zkGetAllUserInfo(const File2save: String):Boolean;
function  zkSetAllUserInfo(const File2save: String):Boolean;

function  zkGetStatus(n: Integer): Integer;
function  zkSetUser(empId, cardId, empName: WideString): Boolean;
function  zkGetUser(empId: WideString; var empName, empPass: WideString;
	var empPriv: Integer; var empEnable: Boolean): Boolean;
function  zkGetUsers(ls: TStrings): Boolean;
function  zkDeleteUser(empId: WideString): Boolean;
function  zkReadGeneralLog: Boolean;
function  zkGetGeneralLog(var enroll: WideString; var inout, nYear, nMonth,
	nDay, nHour, nMinute, nSecond: Integer): Boolean;
function  zkPowerOffDevice: Boolean;
function  zkReStartDevice: Boolean;
function  zkClearKeeperData: Boolean;
function  zkGetDeviceInfo(nInfo: Integer; var nValue: Integer): Boolean;
function  zkSetDeviceInfo(nInfo: Integer; var nValue: Integer): Boolean;

function  zkGetUserTmp(enroll: WideString; finger: Integer): Boolean; overload;
function  zkGetUserTmp(enroll: WideString; finger: Integer; var data: Byte; var len: Integer): Boolean; overload;
function  zkGetUserTmpStr(enroll: WideString; finger: Integer; var data: WideString): Boolean;
function  zkSetUserTmpStr(enroll: WideString; finger: Integer; data: WideString): Boolean;
function  zkDelUserTmp(enroll: WideString; finger: Integer): Boolean;

function  zkStartEnroll(enroll, finger: Integer): Boolean;
function  zkStartEnrollEx(enroll: WideString; finger, flag: Integer): Boolean;
function  zkStartVerify(enroll, finger: Integer): Boolean;
function  zkStartIdentify: Boolean;
function  zkGetLastError: Integer;
function  zkCancelOperation: Boolean;
function  zkEnableDevice(b: Boolean): Boolean;
function  zkBeginBatch(flag: Integer): Boolean;
function  zkBatchUpdate: Boolean;
function  zkRefreshData: Boolean;

var
    mDevId: Integer;
    mDevZKEM: TCZKEM;

implementation

uses
	SysUtils, isMsg, Forms, Controls, Windows;

var
    savComType, savIP4: String;
    savSocket, savPort, savBaudrate: Integer;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkConnect(
	CZKEM1: TCZKEM;
    devComType, devIP4: String;
    devSocket, devPort, devId, devBaudrate: Integer
    ): Boolean;
begin
	Result := False;
	mDevZKEM := CZKEM1;

    if devComType = 'E' then
    begin
        mDevId := 1;
        Result := CZKEM1.Connect_Net(devIP4, devSocket);
    end
    else if devComType = 'S' then
    begin
        mDevId := devId;
        Result := CZKEM1.Connect_Com(devPort, devId, devBaudrate);
    end
    else
        Exit;

	if Result then
    begin
	    savComType := devComType;
        savIP4 := devIP4;
    	savSocket := devSocket;
        savPort := devPort;
        savBaudrate := devBaudrate;
    end
    else
    	ErrMsg('Lỗi kết nối với máy chấm công.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkReconnect: Boolean;
var
	CZKEM1: TCZKEM;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
    	CZKEM1 := mDevZKEM;
    	zkDisconnect;
		Result := zkConnect(
			CZKEM1,
	    	savComType, savIP4,
	    	savSocket, savPort, mDevId, savBaudrate);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure zkSetTime;
begin
	if Assigned(mDevZKEM) then
		mDevZKEM.SetDeviceTime(mDevId);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure zkClearGLog;
begin
	if Assigned(mDevZKEM) then
		mDevZKEM.ClearGLog(mDevId);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure zkRegEvent(EventMask: Integer);
begin
	if Assigned(mDevZKEM) then
    begin
		mDevZKEM.RegEvent(mDevId, EventMask);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure zkReadInfo(ls: TStrings);
var
	s: WideString;
	m, n: Integer;
    dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond: Integer;
begin
	if not Assigned(mDevZKEM) then
    	Exit;

	ls.Clear;
    if mDevZKEM.GetFirmwareVersion(mDevId, s) then
        ls.Add('Firmware: ' + s);

    if mDevZKEM.GetSerialNumber(mDevId, s) then
        ls.Add('Serial Number: ' + s);

    {
    if mDevZKEM.GetProductCode(mDevId, s) then
        ls.Add('Product Code: ' + s);
    }

    if mDevZKEM.GetDeviceTime(mDevId, dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond) then
        ls.Add((Format('Giờ máy: %d-%d-%d %d:%d:%d',[dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond])));

    ls.Add(' ');
    m := zkGetStatus(SF_ADMIN);
    m := m + zkGetStatus(SF_USER);
    n := zkGetStatus(SF_NOMINAL_USER);
    ls.Add((Format('Số nhân viên: %d/%d', [m, n])));

    m := zkGetStatus(SF_FINGER);
    n := zkGetStatus(SF_NOMINAL_FP);
    ls.Add((Format('Số vân tay: %d/%d', [m, n])));

    m := zkGetStatus(SF_INOUT_RECORD);
    n := zkGetStatus(SF_NOMINAL_INOUT);
    ls.Add((Format('Số mẫu tin: %d/%d', [m, n])));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkReadAllUserID:Boolean;
begin
    Result := False;
	if Assigned(mDevZKEM) then
		mDevZKEM.ReadAllUserID(mDevId);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetAllUserInfo(const File2save: String):Boolean;
var
    filename : TInifile;
    enroll, privilege, i, lenght : Integer;
    name : WideString;
    password : WideString;
    enable : Wordbool;
    fingerdata : array[0..2047] of byte;
    s: String;
    (*
    **
    *)
    function encodedata(const BinData; size: integer): string;
    var
      i: integer;
      da: pbytearray;
    begin
      setlength(result, size*2);
      da:=@BinData;
      for i:= 0 to size-1 do
      begin
        result[i*2+1]:=HexOfNum[da[i] div 16];
        result[i*2+2]:=HexOfNum[da[i] mod 16];
      end;
    end;
    (*
    **
    *)
begin
    Result := false;
    filename := TIniFile.Create(File2Save);
    Screen.Cursor := crHourGlass;
    if Assigned(mDevZKEM) then
    begin
        try
            while mDevZKEM.GetAllUserInfo(mDevId, enroll, name, password, privilege, enable) do
            begin
                s := Format('Users_%d',[enroll]);
                filename.WriteInteger(s, 'privilege', privilege);
                filename.WriteBool(s, 'Enable', enable);
                filename.WriteString(s, 'Name', name);
                filename.WriteString(s, 'Password', password);
                for i:=0 to 9 do
                begin
                    ZeroMemory(@fingerdata[0],2048);
                    lenght := 2048;
                    if mDevZKEM.GetUserTmp(mDevId, enroll, i, fingerdata[0], lenght) then
                        begin
                            filename.WriteString(s, 'FingerTmp_'+ IntToStr(i), encodedata(fingerdata[0],lenght));
                        end;
                end;
            end;
        finally
        filename.Free;
        end;
    end;
    Screen.Cursor := crDefault;
    MsgDone;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkSetAllUserInfo(const File2save: String):Boolean;
var
    filename : TInifile;
    enroll, privilege, index, enable : Integer;
    name : WideString;
    s, password, fingertmp: String;
    Sections : TStrings;
    (*
    **
    *)
    function NumOfHex(c: Char): integer;
    begin
      result := 0;
      if c>='a' then result := ord(c)-ord('a')+10
      else if c>='A' then result := ord(c)-ord('A')+10
      else if c>='0' then result := ord(c)-ord('0');
      if result>16 then
        raise exception.Create('Error of Integer: '+c);
    end;

    function decodedata(const s: string): string;
    var
      i: integer;
    begin
        setlength(result, length(s) div 2);
        for i:= 1 to length(result) do
        begin
        result[i]:=char(NumOfHex(s[i*2-1])*16+NumOfHex(s[i*2]));
        end;
    end;
    (*
    **
    *)
begin
    Result := false;
    filename := TIniFile.Create(File2Save);
    Screen.Cursor := crHourGlass;
    try
        Sections := tstringlist.Create;
        try
            filename.ReadSections(Sections);
            for enroll := 1 to 65534 do
            begin
                s:=format('Users_%d',[enroll]);
                if Sections.IndexOf(s)>=0 then
                begin
                    privilege := filename.ReadInteger(s, 'Privilege', 0);
                    enable := filename.ReadInteger(s, 'Enabled', 1);
                    password := filename.ReadString(s, 'Password', '');
                    if password = '-1' then
                    password := '';
                    name := filename.ReadString(s, 'Name','');
                    mDevZKEM.SetUserInfo(mDevId, enroll, name, password, privilege, enable<>0);
                    for index := 0 to 9 do
                    begin
                        fingertmp := filename.ReadString(s, 'FingerTmp_'+IntToStr(index), '');
                        if fingertmp > '' then
                        begin
                            fingertmp := decodedata(fingertmp);
                            mDevZKEM.SetUserTmp(mDevId, enroll, index, pbyte(@fingertmp[1])^)
                        end;
                    end;
                end;
            end;
            finally
            sections.Free;
        end;
        finally
        filename.free;
    end;
    Screen.Cursor := crDefault;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetStatus(n: Integer): Integer;
var
	Value: Integer;
begin
	Result := -1;
	if Assigned(mDevZKEM) then
		if mDevZKEM.GetDeviceStatus(mDevId, n, Value) then
        	Result := Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure zkDisconnect;
begin
	if Assigned(mDevZKEM) then
    begin
		mDevZKEM.RefreshData(mDevId);
		mDevZKEM.EnableDevice(mDevId, True);
		mDevZKEM.Disconnect;
    end;
	mDevZKEM := Nil;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkSetUser(empId, cardId, empName: WideString): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
        mDevZKEM.SetStrCardNumber(cardId);
		Result := mDevZKEM.SSR_SetUserInfo(mDevId, empId, empName, '', 0, True);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetUser(empId: WideString; var empName, empPass: WideString;
	var empPriv: Integer; var empEnable: Boolean): Boolean;
var
	b: WordBool;
begin
	Result := False;
	if not Assigned(mDevZKEM) then
    	Exit;

	Result := mDevZKEM.SSR_GetUserInfo(mDevId, empId, empName, empPass, empPriv, b);

    if Result then
    	empEnable := b;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkDeleteUser(empId: WideString): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
        Result := mDevZKEM.SSR_DeleteEnrollData(mDevId, empId, 12);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetUsers(ls: TStrings): Boolean;
var
	x: Integer;
    enroll, sName, sPass: WideString;
    b: WordBool;
begin
	Result := mDevZKEM.ReadAllUserID(mDevId);
	if Result then
    begin
    	ls.Clear;
		while mDevZKEM.SSR_GetAllUserInfo(mDevID, enroll, sName, sPass, x, b) do
        	ls.Add(enroll + '=' + sName);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkReadGeneralLog: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.ReadGeneralLogData(mDevId)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetGeneralLog(var enroll: WideString;
	var inout, nYear, nMonth, nDay, nHour, nMinute, nSecond: Integer): Boolean;
var
	x1, x2: Integer;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.SSR_GetGeneralLogData(mDevId, enroll, x1, inout,
        	nYear, nMonth, nDay, nHour, nMinute, nSecond, x2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkPowerOffDevice: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.PowerOffDevice(mDevId)
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkReStartDevice: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.RestartDevice(mDevId)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkClearKeeperData: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.ClearKeeperData(mDevId)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetDeviceInfo(nInfo: Integer; var nValue: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.GetDeviceInfo(mDevId, nInfo, nValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkSetDeviceInfo(nInfo: Integer; var nValue: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
		Result := mDevZKEM.SetDeviceInfo(mDevId, nInfo, nValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetUserTmp(enroll: WideString; finger: Integer): Boolean;
var
	tmp: array[0..2047] of Byte;
    len: Integer;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.SSR_GetUserTmp(mDevId, enroll, finger, tmp[0], len);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetUserTmp(enroll: WideString; finger: Integer;
	var data: Byte; var len: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.SSR_GetUserTmp(mDevId, enroll, finger, data, len);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetUserTmpStr(enroll: WideString; finger: Integer; var data: WideString): Boolean;
var
	len: Integer;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
    	SetLength(data, 8*1024);
    	Result := mDevZKEM.SSR_GetUserTmpStr(mDevId, enroll, finger, data, len);
    	SetLength(data, len);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkSetUserTmpStr(enroll: WideString; finger: Integer; data: WideString): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.SetUserTmpExStr(mDevId, enroll, finger, 1, data);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkDelUserTmp(enroll: WideString; finger: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.SSR_DelUserTmp(mDevId, enroll, finger);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkStartEnroll(enroll, finger: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
		mDevZKEM.CancelOperation;
    	Result := mDevZKEM.StartEnroll(enroll, finger);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkStartEnrollEx(enroll: WideString; finger, flag: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
		mDevZKEM.CancelOperation;
    	Result := mDevZKEM.StartEnrollEx(enroll, finger, flag);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkStartVerify(enroll, finger: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.StartVerify(enroll, finger);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkStartIdentify: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.StartIdentify;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkCancelOperation: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.CancelOperation;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkEnableDevice(b: Boolean): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
		Result := mDevZKEM.EnableDevice(mDevId, b);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkGetLastError: Integer;
var
	n: Integer;
begin
	n := 0;
	if Assigned(mDevZKEM) then
		mDevZKEM.GetLastError(n);
	Result := n;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkBeginBatch(flag: Integer): Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
    	Screen.Cursor := crHourGlass;
    	Result := mDevZKEM.BeginBatchUpdate(mDevId, Flag);
    	Screen.Cursor := crDefault;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkBatchUpdate: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    begin
    	Screen.Cursor := crHourGlass;
    	Result := mDevZKEM.BatchUpdate(mDevId);
    	Screen.Cursor := crDefault;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkRefreshData: Boolean;
begin
	Result := False;
	if Assigned(mDevZKEM) then
    	Result := mDevZKEM.RefreshData(mDevId);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function zkBackupData(const DataFile: WideString): Boolean;
begin
	Result := False;
{	if Assigned(mDevZKEM) then
        Result := mDevZKEM.BackupData(DataFile); }
end;


begin
	mDevZKEM := Nil;
end.
