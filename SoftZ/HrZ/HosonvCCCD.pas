﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HosonvCCCD;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, System.Variants,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, Wwdbgrid, RzSplit, wwDialog, Wwdbigrd, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit, wwdbdatetimepicker, wwcheckbox, wwdblook, DBCtrlsEh,
  DBGridEh, DBLookupEh, DbLookupComboboxEh2, Winapi.ShellAPI, Winapi.Windows,
  frameEmp;

type
  TFrmHosonvCCCD = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    EdMA: TDBEditEh;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    DBEditEh4: TDBEditEh;
    Label57: TLabel;
    wwDBDateTimePicker16: TwwDBDateTimePicker;
    QrDanhmucManv: TWideStringField;
    QrDanhmucMaSo: TWideStringField;
    QrDanhmucMaNoiCap: TWideStringField;
    QrDanhmucNoiCap: TWideStringField;
    QrDanhmucNgayCap: TDateTimeField;
    QrDanhmucNgayHetHan: TDateTimeField;
    QrDanhmucGhiChu: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucLK_TenNoiCap: TWideStringField;
    CbNoiCap: TDbLookupComboboxEh2;
    QrDanhmucLK_NoiCap_Ma: TWideStringField;
    QrDanhmucGiayTo_Loai: TIntegerField;
    CbGiayToLoai: TDbLookupComboboxEh2;
    QrDanhmucGiayTo_Nhom: TWideStringField;
    QrDanhmucLK_GiayTo_TenNhom: TWideStringField;
    spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap: TADOCommand;
    PaDinhKem: TisPanel;
    EdFileAttach: TDBEditEh;
    DBEdit6: TDBEditEh;
    CmdOpenLink: TAction;
    QrFileContent: TADOQuery;
    QrFileContentCREATE_BY: TIntegerField;
    QrFileContentUPDATE_BY: TIntegerField;
    QrFileContentCREATE_DATE: TDateTimeField;
    QrFileContentUPDATE_DATE: TDateTimeField;
    QrFileContentFileChecksum: TWideStringField;
    QrFileContentFileContent: TBlobField;
    QrFileContentIdxKey: TGuidField;
    QrFileContentFunctionKey: TWideStringField;
    QrDanhmucFileIdx: TGuidField;
    QrDanhmucFileName: TWideStringField;
    QrDanhmucFileExt: TWideStringField;
    QrDanhmucLink: TWideStringField;
    CmdFilePlus: TAction;
    CmdFileMinus: TAction;
    CmdFileView: TAction;
    QrDanhmucCalc_FileName: TWideStringField;
    QrDanhmucCacl_FileExt: TWideStringField;
    frEmp1: TfrEmp;
    QrFileContentIdx: TAutoIncField;
    QrDanhmucFileDesc: TWideStringField;
    QrDanhmucIdx: TAutoIncField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure RzSizePanel1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
    procedure QrDanhmucBeforeOpen(DataSet: TDataSet);
    procedure CmdOpenLinkExecute(Sender: TObject);
    procedure QrFileContentBeforeOpen(DataSet: TDataSet);
    procedure CmdFilePlusExecute(Sender: TObject);
    procedure CmdFileViewExecute(Sender: TObject);
    procedure CmdFileMinusExecute(Sender: TObject);
    procedure QrDanhmucCalcFields(DataSet: TDataSet);
    procedure QrDanhmucAfterDelete(DataSet: TDataSet);
    procedure QrDanhmucLinkValidate(Sender: TField);
  private
  	mCanEdit, mClose, mRet: Boolean;
    mSql, mManv: String;
    mFileIdx: TGUID;
    function ReleaseDateValid(pManv: String; pGiayToLoai: Integer; pNgay: TDateTime; pIdx: Integer): Boolean;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	function Execute(r: WORD; pEmpID, pEmpIDLabel, pEmpName: string; pClose: Boolean = True): Boolean;
  end;

var
  FrmHosonvCCCD: TFrmHosonvCCCD;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon,
    GuidEx, HrData, isFile, isStr;

{$R *.DFM}

const
	FORM_CODE = 'HR_DM_NHANVIEN_CCCD';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvCCCD.Execute;
begin
	mCanEdit := rCanEdit(r);
	DsDanhmuc.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    mManv := pEmpID;
    mClose := pClose;
    frEmp1.Initial(pEmpID, pEmpIDLabel, pEmpName);
    ShowModal;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;

    AddFields(QrDanhmuc, 'HR_DM_NHANVIEN_GIAYTO');

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');
    mSql := QrDanhmuc.SQL.Text;

    mTrigger := False;
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.FormShow(Sender: TObject);
begin
    with HrDataMain do
        OpenDataSets([QrV_HR_CCCD_NOICAP, QrV_HR_CCCD_NHOM]);

    OpenDataSets([QrDanhmuc]);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
            CloseDataSets(DataMain.Conn)
        else
        begin
            CloseDataSets([QrDanhmuc]);
        end;
    finally
    end;

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
    EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdOpenLinkExecute(Sender: TObject);
begin
    with QrDanhmuc do
    if FieldByName('Link').AsString <> '' then
    begin
         ShellExecute(0, 'Open', PChar(FieldByName('Link').AsString), nil,  nil, SW_NORMAL);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
    if QrFileContent.Active then
        QrFileContent.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

procedure TFrmHosonvCCCD.CmdFileMinusExecute(Sender: TObject);
begin
    if  not YesNo(RS_CONFIRM_ATTACH) then
       Exit;

    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdFilePlusExecute(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdFileViewExecute(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucAfterDelete(DataSet: TDataSet);
begin
    deleteImageByFileIdx(mFileIdx);
    mFileIdx := TGUID.Empty;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        TGuidField(FieldByName('FileIdx')).AsGuid := DataMain.GetNewGuid;
        FieldByName('Manv').AsString := mManv;
        FieldByName('GiayTo_Loai').AsInteger := 1;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucAfterPost(DataSet: TDataSet);
begin
    if QrFileContent.Active then
        QrFileContent.UpdateBatch;
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
    mFileIdx := TGuidField(QrDanhmuc.FieldByName('FileIdx')).AsGuid;

	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
	    if BlankConfirm(QrDanhmuc, ['MaSo', 'NgayCap', 'GiayTo_Nhom', 'MaNoiCap']) then
   			Abort;

        if not ReleaseDateValid(FieldByName('Manv').AsString,
            FieldByName('GiayTo_Loai').AsInteger,
            FieldByName('NgayCap').AsDateTime,
            FieldByName('Idx').AsInteger) then
            Abort;

    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrDanhmuc do
    begin
        // File extension
        s := FieldByName('FileExt').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('Cacl_FileExt').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
        FieldByName('Calc_FileName').AsString :=
            FieldByName('FileName').AsString +
            FieldByName('FileExt').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucLinkValidate(Sender: TField);
begin
    if Sender.AsString <> '' then
    begin
        if not validateUrl(Sender.AsString) then
        begin
            ErrMsg(Format(RS_INVAILD_VALIDATION, [Sender.DisplayLabel]));
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrFileContentBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := TGuidEx.ToString(QrDanhmuc.FieldByName('FileIdx'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.RzSizePanel1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
    MinWidth := 467;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

procedure TFrmHosonvCCCD.QrDanhmucBeforeOpen(DataSet: TDataSet);
begin
      with DataSet as TADOQuery do
        Parameters[0].Value := mManv;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MaSo' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHosonvCCCD.ReleaseDateValid;
var
    s: String;
begin
    with spHR_DM_NHANVIEN_GIAYTO_Invalid_NgayCap do
    begin
        Prepared := True;
        Parameters.ParamByName('@Idx').Value := pIdx;
        Parameters.ParamByName('@Manv').Value := pManv;
        Parameters.ParamByName('@NgayCap').Value := pNgay;
        Parameters.ParamByName('@GiayTo_Loai').Value := pGiayToLoai;
        try
            Execute;
        except
        end;

        if Parameters.FindParam('@STR') <> nil then
            s := Parameters.ParamValues['@STR'];

        Result := s = '';
        if not Result then
            ErrMsg(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrDanhmuc do
    begin
    	Edit;
        FieldByName('FileName').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('FileExt').AsString := ExtractFileExt(s);
    end;

    with QrFileContent do
    begin
        if Active then
            Close;

        Open;

        if IsEmpty then
            Append
        else
            Edit;

        TGuidField(FieldByName('IdxKey')).AsGuid := TGuidField(QrDanhmuc.FieldByName('FileIdx')).AsGuid;
        FieldByName('FunctionKey').AsString := FORM_CODE;
        TBlobField(FieldByName('FileContent')).LoadFromFile(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.Dettach();
begin
     with QrDanhmuc do
     begin
        Edit;
        FieldByName('FileName').Clear;
        FieldByName('FileExt').Clear;
     end;

     with QrFileContent do
     begin
        if Active then
            Close;

        Open;

        if not IsEmpty then
            Delete;

     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHosonvCCCD.ViewAttachment();
var
	s, sn: String;
begin
	with QrDanhmuc do
    begin
    	sn := isStripToneMark(FieldByName('FileName').AsString);
        s := sysAppData + sn + FieldByName('FileExt').AsString;
        try
            with QrFileContent do
            begin
                if Active then
                    Close;
                Open;

                if IsEmpty then
                begin
                    ErrMsg(RS_INVAILD_NOTFOUND);
                    Exit;
                end;

                TBlobField(FieldByName('FileContent')).SaveToFile(s);
            end;
        except
        	ErrMsg(RS_INVAILD_CONTENT);
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;


end.
