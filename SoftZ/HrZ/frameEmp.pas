(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameEmp;

interface

uses
  Windows, Classes, Graphics, Controls, Forms,
  ExtCtrls, StdCtrls;

type
  TfrEmp = class(TFrame)
    Panel1: TPanel;
    Label4: TLabel;
    LbMASO: TLabel;
    Label5: TLabel;
    LbHOTEN: TLabel;
    Image1: TImage;
    LbID: TLabel;
    Label2: TLabel;
  private
  public
  	procedure Initial(pId, pCode, pName: String); overload;
  	procedure Initial(pId, pCode, pName, pRes: String); overload;
  end;

implementation



{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrEmp.Initial(pId, pCode, pName: String);
begin
    LbID.Caption := pId;
	LbMaso.Caption := pCode;
    LbHoten.Caption := pName;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrEmp.Initial(pId, pCode, pName, pRes: String);
begin
	Initial(pId, pCode, pName);
	with Image1.Picture.Icon do
    begin
    	ReleaseHandle;
		Handle := LoadIcon(hInstance, PChar(pRes));
    end;
end;

end.
