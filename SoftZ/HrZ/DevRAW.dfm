object FrmDevRAW: TFrmDevRAW
  Left = 175
  Top = 122
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch D'#7919' Li'#7879'u C'#244'ng Th'#244
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    object ToolButton11: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 36
    Width = 792
    Height = 77
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object LbKHO: TLabel
      Left = 118
      Top = 16
      Width = 113
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7883'a '#273'i'#7875'm/Chi nh'#225'nh'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 184
      Top = 43
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 368
      Top = 43
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbTenKho: TwwDBLookupCombo
      Left = 296
      Top = 12
      Width = 333
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN'#9'35'#9'TENKHO'#9'F'
        'LOC'#9'5'#9'MAKHO'#9'F')
      LookupTable = QrDMKHO
      LookupField = 'LOC'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnNotInList = CbPrinterNotInList
    end
    object CbMaKho: TwwDBLookupCombo
      Left = 240
      Top = 12
      Width = 53
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'LOC'#9'5'#9'MAKHO'#9'F'#9
        'TEN'#9'35'#9'TENKHO'#9'F'#9)
      LookupTable = QrDMKHO
      LookupField = 'LOC'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnNotInList = CbPrinterNotInList
    end
    object EdTu: TwwDBDateTimePicker
      Left = 240
      Top = 39
      Width = 105
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 2
    end
    object EdDen: TwwDBDateTimePicker
      Left = 432
      Top = 39
      Width = 105
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 3
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 113
    Width = 792
    Height = 439
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MANV'#9'16'#9'M'#227' nh'#226'n vi'#234'n'#9'F'
      'TENNV'#9'32'#9'T'#234'n nh'#226'n vi'#234'n'#9'F'
      'NGAYCC'#9'14'#9'Ng'#224'y'#9'F'
      'DULIEU_THO'#9'43'#9'#'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDMList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 3
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrListDblClick
    OnEnter = CmdRefeshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16119285
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 124
    Top = 136
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin     '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdRefesh: TAction
      OnExecute = CmdRefeshExecute
    end
  end
  object QrList: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'exec spTimeData :NGAYD, :NGAYC')
    Left = 92
    Top = 136
    object QrListMANV: TWideStringField
      FieldName = 'MANV'
    end
    object QrListTENNV: TWideStringField
      FieldName = 'TENNV'
      Size = 50
    end
    object QrListLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrListTENLOC: TWideStringField
      FieldName = 'TENLOC'
      Size = 100
    end
    object QrListNGAYCC: TDateTimeField
      FieldName = 'NGAYCC'
    end
    object QrListDULIEU_THO: TWideMemoField
      FieldName = 'DULIEU_THO'
      BlobType = ftWideMemo
    end
  end
  object DsDMList: TDataSource
    DataSet = QrList
    Left = 92
    Top = 168
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDMList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'TENMAY'
      'QUAY'
      'TENKHO'
      'PRINTER'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 309
    Top = 222
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 246
    Top = 222
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 278
    Top = 222
    object Tm1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lc1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DM_LOCATION'
      'order by LOC')
    Left = 56
    Top = 136
    object QrDMKHOLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrDMKHOTEN: TWideStringField
      FieldName = 'TEN'
      Size = 200
    end
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 56
    Top = 168
  end
end
