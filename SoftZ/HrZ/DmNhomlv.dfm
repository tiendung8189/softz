object FrmDmNhomlv: TFrmDmNhomlv
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c Nh'#243'm L'#224'm Vi'#7879'c'
  ClientHeight = 570
  ClientWidth = 1032
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1032
    Height = 37
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdEdit
      ImageIndex = 28
    end
    object ToolButton10: TToolButton
      Left = 116
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 124
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton12: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 549
    Width = 1032
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 37
    Width = 324
    Height = 512
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MaNhomLV'#9'15'#9'M'#227#9'F'
      'TenNhomLV'#9'25'#9'T'#234'n'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDM
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object RzSizePanel1: TRzSizePanel
    Left = 324
    Top = 37
    Width = 708
    Height = 512
    Align = alRight
    BorderOuter = fsLowered
    HotSpotVisible = True
    SizeBarWidth = 7
    TabOrder = 3
    OnConstrainedResize = RzSizePanel1ConstrainedResize
    object PD1: TisPanel
      Left = 10
      Top = 80
      Width = 696
      Height = 430
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = ' :: Chi ti'#7871't'
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWindow
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      ExplicitHeight = 249
      object grThongTinLamViec: TDBVertGridEh
        Left = 1
        Top = 17
        Width = 694
        Height = 412
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        AllowedSelections = []
        RowCategories.Active = True
        RowCategories.CategoryProps = <
          item
            Name = 'NGAYLAMVIEC'
            DisplayText = 'Ng'#224'y l'#224'm vi'#7879'c trong tu'#7847'n'
            DefaultExpanded = True
          end
          item
            Name = 'CHAMCONG'
            DisplayText = 'X'#225'c nh'#7853'n ch'#7845'm c'#244'ng'
            DefaultExpanded = True
          end
          item
            Name = 'TANGCA'
            DisplayText = 'T'#259'ng ca'
            DefaultExpanded = True
          end
          item
            Name = 'KHAC'
            DisplayText = 'Kh'#225'c'
            DefaultExpanded = True
          end>
        RowCategories.Font.Charset = DEFAULT_CHARSET
        RowCategories.Font.Color = clWindow
        RowCategories.Font.Height = -13
        RowCategories.Font.Name = 'Tahoma'
        RowCategories.Font.Style = [fsBold]
        RowCategories.ParentFont = False
        LabelColParams.Font.Charset = DEFAULT_CHARSET
        LabelColParams.Font.Color = clWindowText
        LabelColParams.Font.Height = -13
        LabelColParams.Font.Name = 'Tahoma'
        LabelColParams.Font.Style = []
        LabelColParams.ParentFont = False
        PrintService.ColorSchema = pcsFullColorEh
        DataColParams.Font.Charset = DEFAULT_CHARSET
        DataColParams.Font.Color = 8404992
        DataColParams.Font.Height = -13
        DataColParams.Font.Name = 'Tahoma'
        DataColParams.Font.Style = [fsBold]
        DataColParams.MaxRowHeight = 2
        DataColParams.MaxRowLines = 10
        DataColParams.ParentFont = False
        DataColParams.RowHeight = 21
        DataSource = DsDM
        DrawGraphicData = True
        DrawMemoText = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        GridLineParams.ColorScheme = glcsThemedEh
        Options = [dgvhEditing, dgvhLabelCol, dgvhColLines, dgvhRowLines, dgvhTabs, dgvhConfirmDelete, dgvhCancelOnExit]
        OptionsEh = [dgvhHighlightFocusEh, dgvhClearSelectionEh, dgvhEnterToNextRowEh, dgvhTabToNextRowEh, dgvhHotTrackEh]
        ParentFont = False
        SearchPanel.OptionsPopupMenuItems = [vgsmuSearchScopes]
        TabOrder = 1
        LabelColWidth = 213
        OnGetCellParams = grThongTinLamViecGetCellParams
        Rows = <
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T2'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T3'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T4'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T5'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T6'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'T7'
            RowLabel.Font.Charset = DEFAULT_CHARSET
            RowLabel.Font.Color = clDefault
            RowLabel.Font.Height = -13
            RowLabel.Font.Name = 'Tahoma'
            RowLabel.Font.Style = []
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'CN'
            CategoryName = 'NGAYLAMVIEC'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'Co_ChamCong'
            Visible = False
            CategoryName = 'CHAMCONG'
          end
          item
            ButtonStyle = cbsDropDown
            DynProps = <>
            DropDownBox.ColumnDefValues.AutoDropDown = True
            DropDownBox.SortLocal = True
            EditButton.Visible = True
            EditButton.Width = 20
            EditButtons = <>
            FieldName = 'CA1'
            FitRowHeightToData = False
            FitRowHeightToTextLines = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            WordWrap = True
            CategoryName = 'CHAMCONG'
            LookupParams.KeyFieldNames = 'CA1'
            LookupParams.LookupDataSet = QrDMCA
            LookupParams.LookupDisplayFieldName = 'TenCa'
            LookupParams.LookupKeyFieldNames = 'MaCa'
          end
          item
            ButtonStyle = cbsDropDown
            DynProps = <>
            DropDownBox.ColumnDefValues.AutoDropDown = True
            DropDownBox.SortLocal = True
            EditButton.Visible = True
            EditButton.Width = 20
            EditButtons = <>
            FieldName = 'CA2'
            FitRowHeightToData = False
            FitRowHeightToTextLines = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            CaseInsensitiveTextSearch = False
            Visible = False
            WordWrap = False
            CategoryName = 'CHAMCONG'
            LookupParams.KeyFieldNames = 'CA2'
            LookupParams.LookupDataSet = QrDMCA
            LookupParams.LookupDisplayFieldName = 'TenCa'
            LookupParams.LookupKeyFieldNames = 'MaCa'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'OT'
            Visible = False
            CategoryName = 'TANGCA'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'HT'
            Visible = False
            CategoryName = 'TANGCA'
          end
          item
            DblClickNextVal = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'NS'
            Visible = False
            CategoryName = 'TANGCA'
          end>
      end
    end
    object isPanel1: TisPanel
      Left = 10
      Top = 2
      Width = 696
      Height = 78
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 1
      HeaderCaption = ' :: Th'#244'ng tin'
      HeaderColor = clHighlight
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clWindow
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      DesignSize = (
        696
        78)
      object EdiMaNhomLV: TDBEditEh
        Left = 72
        Top = 22
        Width = 225
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 53
        ControlLabel.Height = 16
        ControlLabel.Caption = 'M'#227' nh'#243'm'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'MaNhomLV'
        DataSource = DsDM
        DynProps = <>
        EditButtons = <>
        EmptyDataInfo.Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 0
        Visible = True
      end
      object EdTenNhomLV: TDBEditEh
        Left = 72
        Top = 46
        Width = 615
        Height = 22
        Anchors = [akLeft, akTop, akRight]
        BevelKind = bkFlat
        BorderStyle = bsNone
        ControlLabel.Width = 58
        ControlLabel.Height = 16
        ControlLabel.Caption = 'T'#234'n nh'#243'm'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        Ctl3D = False
        DataField = 'TenNhomLV'
        DataSource = DsDM
        DynProps = <>
        EditButtons = <>
        EmptyDataInfo.Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ShowHint = True
        TabOrder = 1
        Visible = True
      end
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdDetail: TAction
      ShortCut = 117
      Visible = False
      OnExecute = CmdDetailExecute
    end
    object CmdEdit: TAction
      Caption = 'S'#7917'a'
      Hint = 'S'#7917'a m'#7851'u tin'
      OnExecute = CmdEditExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDM
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 206
    Top = 168
  end
  object QrDM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDMBeforeInsert
    AfterInsert = QrDMAfterInsert
    BeforePost = QrDMBeforePost
    BeforeDelete = QrDMBeforeDelete
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_NHOM_LAMVIEC'
      'order by MaNhomLV')
    Left = 76
    Top = 172
    object QrDMCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMMaNhomLV: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MaNhomLV'
    end
    object QrDMTENNHOMLV: TWideStringField
      DisplayLabel = 'T'#234'n'
      FieldName = 'TenNhomLV'
      Size = 200
    end
    object QrDMTENNHOMLV_TA: TWideStringField
      DisplayLabel = 'Description'
      FieldName = 'TenNhomLV_TA'
      Size = 200
    end
    object QrDMNGAYCONG_THANG: TFloatField
      FieldName = 'NgayCongThang'
    end
    object QrDMNGAYCONG_TUAN: TFloatField
      FieldName = 'NgayCongTuan'
    end
    object QrDMT2: TBooleanField
      FieldName = 'T2'
    end
    object QrDMT3: TBooleanField
      FieldName = 'T3'
    end
    object QrDMT4: TBooleanField
      FieldName = 'T4'
    end
    object QrDMT5: TBooleanField
      FieldName = 'T5'
    end
    object QrDMT6: TBooleanField
      FieldName = 'T6'
    end
    object QrDMT7: TBooleanField
      FieldName = 'T7'
    end
    object QrDMCN: TBooleanField
      FieldName = 'CN'
    end
    object QrDMCo_ChamCong: TBooleanField
      FieldName = 'Co_ChamCong'
    end
    object QrDMCA1: TWideStringField
      FieldName = 'CA1'
      Size = 15
    end
    object QrDMCA2: TWideStringField
      FieldName = 'CA2'
      Size = 15
    end
    object QrDMNGAYCONG_GIO: TFloatField
      FieldName = 'NgayCongGio'
    end
    object QrDMNGAYCONG_PHUT: TFloatField
      FieldName = 'NgayCongPhut'
    end
    object QrDMOT: TBooleanField
      FieldName = 'OT'
    end
    object QrDMHT: TBooleanField
      FieldName = 'HT'
    end
    object QrDMNS: TBooleanField
      FieldName = 'NS'
    end
  end
  object DsDM: TDataSource
    AutoEdit = False
    DataSet = QrDM
    Left = 76
    Top = 200
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 177
    Top = 168
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 236
    Top = 168
  end
  object QrDMCA: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from HR_DM_CALAMVIEC')
    Left = 268
    Top = 254
  end
end
