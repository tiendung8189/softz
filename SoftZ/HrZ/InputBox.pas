(*==============================================================================
**------------------------------------------------------------------------------
*)
unit InputBox;

interface

uses
  Classes, Controls,
  Forms, StdCtrls;

type
  TFrmInputBox = class(TForm)
    EdGhichu: TEdit;
    lbPrompt: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    function Execute(const APrompt, ADefault: string; const ACaption: String = ''): String;
  end;

var
  FrmInputBox: TFrmInputBox;

implementation

{$R *.DFM}

uses
    isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmInputBox.Execute;
begin
    if ACaption = '' then
        Caption := Application.Title
    else
        Caption := ACaption;
    lbPrompt.Caption := APrompt;
    Result := ADefault;
	EdGhichu.Text := ADefault;
	if ShowModal = mrOk then
        Result := EdGhichu.Text
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInputBox.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInputBox.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInputBox.FormKeyPress(Sender: TObject; var Key: Char);
begin
    case Key of
    #13:
        ModalResult := mrOk;
    #27:
        ModalResult := mrCancel;
    end;
end;

end.
