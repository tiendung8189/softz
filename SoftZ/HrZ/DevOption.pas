﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DevOption;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Buttons,
  ActnList, wwDataInspector, StdCtrls, Grids;

type
  TFrmDevOption = class(TForm)
    ActionList1: TActionList;
    CmdLoadOption: TAction;
    CmdSaveOption: TAction;
    CmdShutdown: TAction;
    Inspect: TwwDataInspector;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    CmdRestart: TAction;
    CmdReset: TAction;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    CmdDeleteLog: TAction;
    CmdSynTime: TAction;
    BitBtn8: TBitBtn;
    procedure CmdLoadOptionExecute(Sender: TObject);
    procedure CmdSaveOptionExecute(Sender: TObject);
    procedure CmdShutdownExecute(Sender: TObject);
    procedure CmdRestartExecute(Sender: TObject);
    procedure CmdResetExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdDeleteLogExecute(Sender: TObject);
    procedure CmdSynTimeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function  Execute: Boolean;
  end;

var
  FrmDevOption: TFrmDevOption;


implementation

{$R *.dfm}

uses
	zkem, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDevOption.Execute: Boolean;
begin
	Result := ShowModal = mrYes; 
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdLoadOptionExecute(Sender: TObject);
var
	Value, i: Integer;
    	(*
        **
        *)
	function BTimeEncode(MinuteSecond: Integer): String;
    var
	    m, s: Integer;
    begin
	    m := MinuteSecond div 256;
    	s := MinuteSecond mod 256;
		if (MinuteSecond<0) or (m>59) or (s>59) then
			Result := 'No'
	    else
    		Result := Format('%d:%d', [m, s]);
	end;

begin
    Screen.Cursor := crSQLWait;
	for i := 0 to Inspect.RowCount - 1 do
		if zkGetDeviceInfo(i + 1, value) then
		begin
            if (i >= 18) and (i <= 21) then
            	Inspect.Items[i].EditText := BTimeEncode(Value)
			else
            	Inspect.Items[i].EditText := IntToStr(Value);
		end;

	with Inspect do
    begin
        Refresh;
    	SetFocus;
    end;
    CmdLoadOption.Tag := 1; 
	Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdSaveOptionExecute(Sender: TObject);
var
    Value, i: Integer;

		(*
	    **
    	*)
	function BTimeDecode(const TimeStr: String): integer;
	var
		p, m, s: Integer;
	begin
    	p := Pos(':', TimeStr);
		m := StrToIntDef(Copy(TimeStr, 1, p - 1),   -1);
    	s := StrToIntDef(Copy(TimeStr, p + 1, 100), -1);
        if (m<0) or (s<0) or (m>255) or (s>255) then
        	Result := 255
        else
			Result := m * 256 + s;
	end;
begin
    Screen.Cursor := crSQLWait;
    for i := 0 to Inspect.RowCount-1 do
    begin
        if (i >= 18) and (i <= 21) then
            Value := BTimeDecode(Inspect.Items[i].EditText)
        else
            Value := StrToInt(Inspect.Items[i].EditText);
        try
	        zkSetDeviceInfo(i + 1, Value);
        except
        end;
    end;
	Screen.Cursor := crDefault;
    CmdLoadOption.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdShutdownExecute(Sender: TObject);
begin
    zkPowerOffDevice;
    ModalResult := mrYes;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdRestartExecute(Sender: TObject);
begin
    zkReStartDevice;
    ModalResult := mrYes;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdResetExecute(Sender: TObject);
begin
	if not YesNo('Xóa toàn bộ dữ liệu trong máy. Tiếp tục?') then
    	Exit;
	Inspect.SetFocus;
    MsgDone(zkClearKeeperData);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
	CmdSaveOption.Enabled :=CmdLoadOption.Tag = 1
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdDeleteLogExecute(Sender: TObject);
begin
	if not YesNo('Xóa dữ liệu chấm công trong máy. Tiếp tục?', 1) then
    	Exit;

	zkClearGLog;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDevOption.CmdSynTimeExecute(Sender: TObject);
begin
	zkSetTime;
end;

end.
