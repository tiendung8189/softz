﻿object FrmSoluong: TFrmSoluong
  Left = 387
  Top = 262
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'S'#7889' L'#432#7907'ng'
  ClientHeight = 66
  ClientWidth = 213
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 197
    Height = 49
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 16
      Width = 51
      Height = 16
      Alignment = taRightJustify
      Caption = 'S'#7889' l'#432#7907'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdSoluong: TEdit
      Left = 80
      Top = 12
      Width = 101
      Height = 24
      Color = 15794175
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
