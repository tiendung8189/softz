﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit NXKhac;

interface

uses
  SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, frameNgay, frameNavi,
  isDb, isPanel, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, Wwdotdot, Wwdbcomb,
  DBGridEh, DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh;

type
  TFrmNXKhac = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepBarcode: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTLK_TENVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_SO: TWideStringField;
    QrNHOADON_NGAY: TDateTimeField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    CmdBalance: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrCTGHICHU: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    Label5: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    Label8: TLabel;
    DBEdit2: TwwDBEdit;
    CmdAudit: TAction;
    QrCTB1: TBooleanField;
    BtnBarCode: TToolButton;
    BtnBarCode2: TToolButton;
    CmdListRefesh: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdSapthutu: TAction;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    QrCTQD1: TIntegerField;
    QrCTLK_QD1: TIntegerField;
    QrNXLK_LYDO_NHAP: TWideStringField;
    QrNXLK_LYDO_XUAT: TWideStringField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrCTLK_TINHTRANG: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCTLK_GIAVON: TFloatField;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    N4: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrCTLOC: TWideStringField;
    PopIn: TAdvPopupMenu;
    Phiuvnchuynnib1: TMenuItem;
    MenuItem2: TMenuItem;
    Phiucginhp1: TMenuItem;
    N5: TMenuItem;
    ItemObsolete: TMenuItem;
    QrNXLOCKED_BY: TIntegerField;
    QrNXLK_LOCKED_NAME: TWideStringField;
    QrNXLOCKED_DATE: TDateTimeField;
    CmdCheckton: TAction;
    N6: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    CmdExportDataGrid: TAction;
    N7: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    N8: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    N9: TMenuItem;
    heogibn1: TMenuItem;
    QrNXThanhTien: TFloatField;
    QrNXThanhTienSi: TFloatField;
    QrNXThanhTienLe: TFloatField;
    QrNXNguoiGiao: TWideStringField;
    QrNXNguoiNhan: TWideStringField;
    QrNXSoTienCKMH: TFloatField;
    QrNXSoLuong: TFloatField;
    QrNXThanhToan: TFloatField;
    QrCTLK_Dvt: TWideStringField;
    QrCTLK_DvtHop: TWideStringField;
    QrCTDonGiaThamKhao: TFloatField;
    QrCTDonGiaHop: TFloatField;
    QrCTDonGia: TFloatField;
    QrCTCALC_DonGiaThamKhaoHop: TFloatField;
    QrCTSoLuong: TFloatField;
    QrCTSoLuongHop: TFloatField;
    QrCTSoLuongLe: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTThanhTienSauCKMH: TFloatField;
    QrCTThueSuat: TFloatField;
    QrCTSoTienThue: TFloatField;
    QrCTDonGiaSi: TFloatField;
    QrCTDonGiaLe: TFloatField;
    QrCTThanhTienSi: TFloatField;
    QrCTThanhTienLe: TFloatField;
    QrCTTyLeLai: TFloatField;
    QrCTTyLeLaiSi: TFloatField;
    QrCTTyLeCKMH: TFloatField;
    QrCTSoTienCKMH: TFloatField;
    QrCTTyLeCKHD: TFloatField;
    QrCTSoTienCKHD: TFloatField;
    QrCTThanhTienSauCKHD: TFloatField;
    QrNXSoLuongHop: TFloatField;
    QrNXThanhTienSauCKMH: TFloatField;
    QrNXTyLeCKHD: TFloatField;
    QrNXSoTienCKHD: TFloatField;
    QrNXThanhTienSauCKHD: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrCTHanSuDung: TDateTimeField;
    EdMAKHO: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    CbLYDO: TDbLookupComboboxEh2;
    DBMemoEh1: TDBMemoEh;
    QrCTThanhToanTinhThue: TFloatField;
    QrCTThanhToanChuaThue: TFloatField;
    QrCTThanhToan: TFloatField;
    QrCTDonGiaCKMH: TFloatField;
    QrCTDonGiaCKHD: TFloatField;
    QrCTThanhToanChuaCL: TFloatField;
    QrNXThanhToanTinhThue: TFloatField;
    QrNXThanhToanChuaCL: TFloatField;
    QrNXThanhToanChuaThue: TFloatField;
    QrCTDonGiaTinhThue: TFloatField;
    QrCTLK_MaNoiBo: TWideStringField;
    N10: TMenuItem;
    Khngngi1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure QrNXAfterEdit(DataSet: TDataSet);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrCTThanhTienChange(Sender: TField);
    procedure QrCTThanhToanChange(Sender: TField);
    procedure CbKhoHangDropDown(Sender: TObject);
    procedure CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
  private
  	mLCT, mPrefix: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(kh: String; r: WORD);
  end;

var
  FrmNXKhac: TFrmNXKhac;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, Sapthutu, GuidEx,
    exThread, isCommon, isFile, ImportExcel, InlabelChungtu, CheckTonkho, OfficeData;

{$R *.DFM}

const
	FORM_CODE_N = 'PHIEU_NHAPKHAC';
    FORM_CODE_X = 'PHIEU_XUATKHAC';

    LCT_N = 'NKHAC';
    LCT_X = 'XKHAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	CAPTION_NHAP = 'Nhập Khác';
    CAPTION_XUAT = 'Xuất Khác';

procedure TFrmNXKhac.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;

    if kh = 'N' then
    begin
    	Caption := CAPTION_NHAP;
       	mLCT := LCT_N;
        CbLYDO.DropDownBox.ListSource := DataMain.DsLYDO_NK;
        CbLYDO.ListSource := DataMain.DsLYDO_NK;
	end
    else
    begin
    	Caption := CAPTION_XUAT;
       	mLCT := LCT_X;
        CbLYDO.DropDownBox.ListSource := DataMain.DsLYDO_XK;
        CbLYDO.ListSource := DataMain.DsLYDO_XK;
	end;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormCreate(Sender: TObject);
begin
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
	mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');
    CmdImportExcel.Visible := FlexConfigBool(Iif(mLCT=LCT_N,FORM_CODE_N, FORM_CODE_X), 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;
    
    mObsolete := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;

	Wait(PREPARING);
    InitFmtAndRoud(mLct);

    with  DataMain do
    begin
    	OpenDataSets([QrDMVT, QrDMKHO]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TyLeLai'], sysPerFmt);
    end;
    ClearWait;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['SoLuong'], ctQtyFmt);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

	with QrCT do
    begin
		SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SoLuong'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DonGia', 'DonGiaSi', 'DonGiaLe'], ctPriceFmt);
    end;

	if mLCT = LCT_N then
    begin
        TMyForm(Self).LoadIcon('_IDI_RECEIPT');
	    DataMain.QrLYDO_NK.Open;

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_N, FORM_CODE_N + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_N , FORM_CODE_N + '_CT'], [Filter, Nil]);
    end
    else
    begin
        TMyForm(Self).LoadIcon('_IDI_ISSUE');
	    DataMain.QrLYDO_XK.Open;

        // Invisible Barcode function
        BtnBarCode.Visible := False;
        BtnBarCode2.Visible := False;
        GrDetail.RemoveField('B1');

        // DicMap + Customize Grid
		SetCustomGrid([FORM_CODE_X, FORM_CODE_X + '_CT'], [GrBrowse, GrDetail]);
        SetDictionary([QrNX, QrCT], [FORM_CODE_X, FORM_CODE_X + '_CT'], [Filter, Nil]);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsBarcode then
    begin
        BtnBarCode.Visible := False;
        BtnBarCode2.Visible := False;

        grRemoveFields(GrDetail, ['B1']);
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
        QrDMVT.Requery;
        QrDMKHO.Requery;
        if mLCT = LCT_N then
        	QrLYDO_NK.Requery
        else
        	QrLYDO_XK.Requery;
    end;
        
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);

//    SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);

//    if QrNX.FieldByName('LOCKED_BY').AsInteger = sysLogonUID then
//        SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCheckedExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmCheckTonkho, FrmCheckTonkho);
    with QrCT do
	    if FrmCheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('LK_TENVT').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdDelExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdPrintExecute(Sender: TObject);
var
	sRep: String;
	n: Integer;
begin
	if mLCT = LCT_N then
    	sRep := FORM_CODE_N
    else
    	sRep := FORM_CODE_X;

	CmdSave.Execute;
    n := (Sender as TComponent).Tag;

	ShowReport(Caption, sRep + '_' + IntToStr(n), [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, Iif(mLCT=LCT_N,FORM_CODE_N, FORM_CODE_X),
                    QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdTotalExecute(Sender: TObject);
begin
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);

    BtnIn.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('MAKHO').AsString         := sysDefKho;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterEdit(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterPost(DataSet: TDataSet);
begin
	//
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO', 'LYDO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
	DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);

//    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(DataSet) then
//        Abort;
//    SetLockedBy(DataSet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('CALC_DonGiaThamKhaoHop').AsFloat :=
            FieldByName('DonGiaThamKhao').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE: Double;
    Qd1: Integer;
begin
   	exDotMavt(3, Sender);
    // Update referenced fields
    with QrCT do
    begin
        gia := 0; giaSI := 0; giaLE := 0;
        with DataMain.spCHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@MABH').Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                Qd1 := FieldByName('QD1').AsInteger;
                gia := FieldByName('DonGia').AsFloat;
                giaSI := FieldByName('DonGiaSi').AsFloat;
                giaLE := FieldByName('DonGiaLe').AsFloat;
            end;
        end;
        FieldByName('QD1').AsInteger := Qd1;

        FieldByName('DonGiaSi').AsFloat := giaSI;
        FieldByName('DonGiaLe').AsFloat := giaLE;
        FieldByName('DonGiaThamKhao').AsFloat := gia;
        FieldByName('DonGia').AsFloat := gia;

        if mLCT = LCT_N then
        begin
			if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
                FieldByName('B1').AsBoolean := True;
        end;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DonGia' then
                FieldByName('DonGiaHop').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DonGiaHop' then
                FieldByName('DonGia').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SoLuong' then
            begin
                sl  := FieldByName('SoLuong').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;
            end
            else if (Sender.FullName = 'SoLuongHop') or (Sender.FullName = 'SoLuongLe') then
            begin
                sl2 := FieldByName('SoLuongHop').AsFloat;
                sl2le := FieldByName('SoLuongLe').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SoLuong').AsFloat * FieldByName('DonGia').AsFloat, ctCurRound);
        FieldByName('ThanhTienSi').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaSi').AsFloat, ctCurRound);
        FieldByName('ThanhTienLe').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaLe').AsFloat, ctCurRound);
        FieldByName('ThanhTien').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTThanhTienChange(Sender: TField);
var
    dongia, thanhtien, thanhtoan: Double;
begin
    with QrCT do
    begin
        dongia := FieldByName('DonGia').AsFloat;
        thanhtien := FieldByName('ThanhTien').AsFloat;

        thanhtoan := exVNDRound(thanhtien, ctCurRound);

        FieldByName('DonGiaCKMH').AsFloat := dongia;
        FieldByName('DonGiaCKHD').AsFloat := dongia;
        FieldByName('DonGiaTinhThue').AsFloat := dongia;

        FieldByName('ThanhTienSauCKMH').AsFloat := thanhtoan;
        FieldByName('ThanhTienSauCKHD').AsFloat := thanhtoan;
        FieldByName('ThanhToanTinhThue').AsFloat := thanhtoan;
        FieldByName('ThanhToanChuaCL').AsFloat := thanhtoan;
        FieldByName('ThanhToanChuaThue').AsFloat := thanhtoan;
        FieldByName('ThanhToan').AsFloat := thanhtoan;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.QrCTThanhToanChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
begin
      (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbKhoHangDropDown(Sender: TObject);
begin
    (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SoLuong').AsFloat);
		ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat);
        ColumnByName('ThanhTienSi').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSi').AsFloat);
        ColumnByName('ThanhTienLe').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienLe').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNXKhac.CmdExBarcodeExecute(Sender: TObject);
begin
    CmdSave.Execute;
	with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
	procedure TFrmNXKhac.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*
    ** End: Others
    *)


end.
