object FrmNhaptraBanleTimPhieu: TFrmNhaptraBanleTimPhieu
  Left = 192
  Top = 107
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n H'#243'a '#208#417'n'
  ClientHeight = 384
  ClientWidth = 522
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    522
    384)
  PixelsPerInch = 96
  TextHeight = 16
  object PaKho: TPanel
    Left = 8
    Top = 56
    Width = 506
    Height = 99
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 262
      Top = 65
      Width = 65
      Height = 16
      Alignment = taRightJustify
      Caption = 'S'#7889' h'#243'a '#273#417'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 17
      Top = 68
      Width = 71
      Height = 16
      Alignment = taRightJustify
      Caption = 'Th'#225'ng/ N'#259'm'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbKHO: TLabel
      Left = 10
      Top = 13
      Width = 78
      Height = 16
      Alignment = taRightJustify
      Caption = 'Kho b'#225'n h'#224'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 5
      Top = 39
      Width = 83
      Height = 16
      Alignment = taRightJustify
      Caption = 'Qu'#7847'y thu ng'#226'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdThang: TComboBox
      Left = 94
      Top = 62
      Width = 53
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object EdNam: TComboBox
      Left = 150
      Top = 62
      Width = 90
      Height = 24
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object CbMaKho: TwwDBLookupCombo
      Tag = 2
      Left = 94
      Top = 10
      Width = 87
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'10'#9'MAKHO'#9'F'#9
        'TENKHO'#9'35'#9'TENKHO'#9'F'#9)
      DataField = 'MaKho'
      DataSource = DsTemp
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object CbTenKho: TwwDBLookupCombo
      Tag = 1
      Left = 184
      Top = 10
      Width = 295
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TENKHO'#9'35'#9'TENKHO'#9'F'
        'MAKHO'#9'5'#9'MAKHO'#9'F')
      DataField = 'MaKho'
      DataSource = DsTemp
      LookupTable = QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object wwDBLookupCombo1: TwwDBLookupCombo
      Tag = 2
      Left = 94
      Top = 36
      Width = 87
      Height = 24
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MaQuay'#9'10'#9'MaQuay'#9'F'#9
        'TenQuay'#9'35'#9'TenQuay'#9'F'#9)
      DataField = 'MaQuay'
      DataSource = DsTemp
      LookupTable = QrDMQUAY
      LookupField = 'MaQuay'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object wwDBLookupCombo2: TwwDBLookupCombo
      Tag = 1
      Left = 184
      Top = 36
      Width = 295
      Height = 24
      TabStop = False
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TenQuay'#9'35'#9'TenQuay'#9'F'
        'MaQuay'#9'5'#9'MaQuay'#9'F')
      DataField = 'MaQuay'
      DataSource = DsTemp
      LookupTable = QrDMQUAY
      LookupField = 'MaQuay'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Navigator = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object EdSct: TwwDBEdit
      Left = 333
      Top = 62
      Width = 146
      Height = 22
      CharCase = ecUpperCase
      Ctl3D = False
      DataField = 'Sct'
      DataSource = DsTemp
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object BitBtn1: TBitBtn
    Left = 317
    Top = 335
    Width = 97
    Height = 41
    Cursor = 1
    Anchors = [akRight, akBottom]
    Caption = 'Ti'#7871'p t'#7909'c'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 1
    ParentFont = False
    TabOrder = 2
  end
  object BitBtn2: TBitBtn
    Left = 417
    Top = 335
    Width = 97
    Height = 41
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Tho'#225't'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 3
  end
  object RgLoai: TRadioGroup
    Left = 8
    Top = 6
    Width = 542
    Height = 47
    Columns = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      'Theo s'#7889' h'#243'a '#273#417'n c'#7911'a th'#225'ng'
      'Theo ID tr'#234'n phi'#7871'u')
    ParentFont = False
    TabOrder = 4
  end
  object PaID: TPanel
    Left = 8
    Top = 161
    Width = 506
    Height = 57
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object Label4: TLabel
      Left = 84
      Top = 20
      Width = 74
      Height = 16
      Alignment = taRightJustify
      Caption = 'ID tr'#234'n phi'#7871'u'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object wwDBEdit1: TwwDBEdit
      Left = 164
      Top = 16
      Width = 146
      Height = 22
      CharCase = ecUpperCase
      Ctl3D = False
      DataField = '_id'
      DataSource = DsTemp
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object BitBtn3: TBitBtn
    Left = 216
    Top = 222
    Width = 97
    Height = 41
    Cursor = 1
    Caption = 'T'#236'm phi'#7871'u'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FF9D9995FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FF8C8C8C6363639E9E9EFF00FF4F4737FF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF746B61F6B564F6B564F5B4
      64646361FF00FFFF00FFFF00FF55D3FD28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF5D696DF6B564F6B564F6B564F6B564F5B4649C9D9EFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF616161F6B564F6B564F6B564F6B5
      64F6B564616363FF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF606263F6B564F8C98EF6B564F9D2A3F6B56478878DFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF30BBE8967F62F7BD75F9D09CF7B8
      6B746B61B5EAFCFF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF30BBE86062636161615D686C2AC6F9B5EBFDFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CA
      FF28CAFFB5EBFDFF00FFFF00FF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFFA3E4FDFF00FFFF00FF28CAFF
      28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CA
      FF28CAFF7CD5FDFF00FFFF00FF25C8FF28CAFF28CAFF28CAFF28CAFF28CAFF28
      CAFF28CAFF28CAFF28CAFF28CAFF28CAFF28CAFFDFF3FDFF00FFFF00FF00A0FF
      00A0FF00A0FF00A0FF00A0FF0AA2FDFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FF57BFFD54BEFD54BEFD54BEFDFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    TabOrder = 0
    OnClick = BitBtn3Click
  end
  object Panel3: TPanel
    Left = 8
    Top = 267
    Width = 506
    Height = 62
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 6
    object Label7: TLabel
      Left = 60
      Top = 22
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 262
      Top = 23
      Width = 65
      Height = 16
      Alignment = taRightJustify
      Caption = 'S'#7889' h'#243'a '#273#417'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object wwDBDateTimePicker1: TwwDBDateTimePicker
      Left = 94
      Top = 19
      Width = 146
      Height = 22
      TabStop = False
      BorderStyle = bsNone
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'Tahoma'
      CalendarAttributes.Font.Style = []
      Color = clBtnFace
      DataField = 'Ngay'
      DataSource = DsResult
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.FocusBorders = [efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
    end
    object wwDBEdit4: TwwDBEdit
      Left = 333
      Top = 19
      Width = 146
      Height = 22
      TabStop = False
      BorderStyle = bsNone
      Color = clBtnFace
      Ctl3D = False
      DataField = 'Sct'
      DataSource = DsResult
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Frame.Enabled = True
      Frame.FocusBorders = [efBottomBorder]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object kbmTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'MaKho'
        DataType = ftWideString
        Size = 200
      end
      item
        Name = 'MaQuay'
        DataType = ftWideString
        Size = 200
      end
      item
        Name = 'Sct'
        DataType = ftWideString
        Size = 200
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 0
    LocaleID = 0
    Left = 44
    Top = 199
    object kbmTempS1: TWideStringField
      FieldName = 'MaKho'
      OnChange = kbmTempS1Change
    end
    object kbmTempS2: TWideStringField
      FieldName = 'MaQuay'
    end
    object kbmTempS3: TWideStringField
      FieldName = 'Sct'
      Size = 200
    end
    object kbmTemp_id: TLargeintField
      FieldName = '_id'
    end
  end
  object DsTemp: TDataSource
    DataSet = kbmTemp
    Left = 43
    Top = 235
  end
  object QrDMQUAY: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_QUAYTN'
      'order by '#9'QUAY'
      ' ')
    Left = 130
    Top = 205
  end
  object spCHECK_BILL_BANLE: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'spCHECK_BILL_BANLE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@p_id'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@pMaKho'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@pMaQuay'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@pSCT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@returnCode'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 468
    Top = 196
    object spCHECK_BILL_BANLEKHOA: TGuidField
      FieldName = 'KHOA'
      ReadOnly = True
      FixedChar = True
      Size = 38
    end
    object spCHECK_BILL_BANLENGAY: TDateTimeField
      FieldName = 'NGAY'
      ReadOnly = True
    end
    object spCHECK_BILL_BANLEMaKho: TWideStringField
      FieldName = 'MaKho'
      ReadOnly = True
    end
    object spCHECK_BILL_BANLEMaQuay: TWideStringField
      FieldName = 'MaQuay'
      ReadOnly = True
    end
    object spCHECK_BILL_BANLESCT: TWideStringField
      FieldName = 'SCT'
      ReadOnly = True
    end
    object spCHECK_BILL_BANLEMAVIP: TWideStringField
      FieldName = 'MAVIP'
      ReadOnly = True
    end
    object spCHECK_BILL_BANLENhanVienThuNgan: TIntegerField
      FieldName = 'NhanVienThuNgan'
      ReadOnly = True
    end
  end
  object DsResult: TDataSource
    DataSet = spCHECK_BILL_BANLE
    Left = 467
    Top = 235
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO, LOC'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 96
    Top = 208
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 344
  end
end
