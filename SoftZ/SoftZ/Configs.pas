﻿(*==============================================================================
** System Preferences
**------------------------------------------------------------------------------
*)
unit Configs;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  Db, ADODB, wwDataInspector, Buttons,
  ActnList, wwdblook, StdCtrls, Grids, kbmMemTable;

type
  TFrmConfigs = class(TForm)
    ActionList: TActionList;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdCancel: TAction;
    Inspect: TwwDataInspector;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QrPARAMS: TkbmMemTable;
    DsPARAMS: TDataSource;
    QrPARAMSFOLDER_REPORT: TWideStringField;
    QrPARAMSFOLDER_BARCODE: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SelectFolder(Sender: TwwDataInspector; Item: TwwInspectorItem);
    procedure FormCreate(Sender: TObject);
  private
  	mCanEdit, mChanged, mSysChanged: Boolean;
  public
  	function Execute(r: WORD): Integer;
  end;

var
	FrmConfigs : TFrmConfigs;

implementation

uses
	MainData, isDb, Rights, ExCommon, isLib, isFile, isMsg, isStr,
    isCommon;

{$R *.DFM}

const
    FORM_CODE: String = 'SYS_CONFIG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmConfigs.Execute;
begin
	mChanged := False;
    mSysChanged := False;

	mCanEdit := rCanEdit(r);
    Inspect.ReadOnly := not mCanEdit;
    ShowModal;

	Result := 0;
    if mChanged then
	    if mSysChanged then
    		Result := 2
	    else
    		Result := 1
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.FormShow(Sender: TObject);
var
	i: Integer;
    s: String;
    b: Boolean;
    ls: TStrings;
    it: TwwInspectorItem;
begin
	with DataMain do
    begin
        with QrPARAMS do
        begin
            Append;

        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.CmdSaveExecute(Sender: TObject);
begin
	QrPARAMS.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.CmdCloseExecute(Sender: TObject);
begin
    QrPARAMS.Cancel;
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.CmdCancelExecute(Sender: TObject);
begin
    QrPARAMS.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrPARAMS, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmConfigs.SelectFolder(Sender: TwwDataInspector;
  Item: TwwInspectorItem);
var
	s: String;
begin
	s := ExpandUNCFileName(isGetDir('Chọn thư mục', ''));
    if s = '' then
    	Exit;
    with QrParams do
    begin
    	if State in [dsBrowse] then
        	Edit;
		Item.Field.AsString := s;
    end;
end;
end.
