﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieuKM;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, kbmMemTable,
  MemTableDataEh, MemTableEh;

type
  TFrmChonPhieuKM = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNX: TDataSource;
    QrPHIEUNX: TADOQuery;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNXSCT: TWideStringField;
    QrPHIEUNXTUNGAY: TDateTimeField;
    QrPHIEUNXDENNGAY: TDateTimeField;
    QrPHIEUNXTL_CK: TFloatField;
    QrPHIEUNXLYDO: TWideStringField;
    QrPHIEUNXNGAY: TDateTimeField;
    QrPHIEUNXLCT: TWideStringField;
    QrPHIEUNXDGIAI: TWideMemoField;
    QrPHIEUNXKHOA: TGuidField;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMaDVExit(Sender: TObject);
    procedure CbMaDVNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrPHIEUNXBeforeOpen(DataSet: TDataSet);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute: TGUID;
  end;

var
  FrmChonPhieuKM: TFrmChonPhieuKM;

implementation

uses
	isDb, ExCommon, isLib, GuidEx;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieuKM.Execute;
begin


	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUNX.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNX]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_PHIEU_KM', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.FormShow(Sender: TObject);

begin
   	OpenDataSets([QrPHIEUNX]);
	mSQL := QrPHIEUNX.SQL.Text;
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    GrBrowse.SetFocus;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.CmdRefreshExecute(Sender: TObject);
var
    Tungay, Denngay: TDateTime;
begin
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay)    then
	begin
        mTungay := Tungay;
        mDenngay := Denngay;

        with QrPHIEUNX do
        begin
			Close;
			Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.CbMaDVExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.CbMaDVNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.QrPHIEUNXBeforeOpen(DataSet: TDataSet);
begin
	with QrPHIEUNX do
    begin
        Parameters[0].Value := mTungay;
        Parameters[1].Value := mDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuKM.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

end.
