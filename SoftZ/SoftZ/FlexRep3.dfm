﻿object FrmFlexRep3: TFrmFlexRep3
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n Tr'#432#7901'ng'
  ClientHeight = 312
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrList: TwwDBGrid2
    Left = 0
    Top = 0
    Width = 502
    Height = 265
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'Visible;CheckBox;True;False')
    Selected.Strings = (
      'No'#9'5'#9'STT'#9'F'
      'Name'#9'50'#9'T'#234'n Tr'#432#7901'ng'#9'F'
      'Visible'#9'10'#9'Ch'#7885'n'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alTop
    DataSource = DsRep
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleAlignment = taCenter
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
PaintOptions.AlternatingRowColor=clNone
  end
  object BitBtn1: TBitBtn
    Left = 284
    Top = 276
    Width = 101
    Height = 25
    Cursor = 1
    Caption = 'Ti'#7871'p t'#7909'c'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    ModalResult = 1
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 388
    Top = 276
    Width = 101
    Height = 25
    Cursor = 1
    Caption = 'Tho'#225't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Kind = bkCancel
    ParentFont = False
    TabOrder = 2
  end
  object QrTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'No'
        DataType = ftInteger
      end
      item
        Name = 'Visible'
        DataType = ftBoolean
      end
      item
        Name = 'Name'
        DataType = ftWideString
        Size = 200
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '7.15.00 Professional Edition'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 104
    Top = 132
  end
  object DsRep: TDataSource
    DataSet = QrTemp
    Left = 104
    Top = 164
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 164
    Top = 132
    object Tm1: TMenuItem
      Action = CmdSelectAll
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Lc1: TMenuItem
      Action = CmdClearSelect
    end
  end
  object ActionList: TActionList
    Left = 164
    Top = 164
    object CmdSelectAll: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n t'#7845't c'#7843
      ShortCut = 16449
      OnExecute = CmdSelectAllExecute
    end
    object CmdClearSelect: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng ch'#7885'n t'#7845't c'#7843
      ShortCut = 16452
      OnExecute = CmdClearSelectExecute
    end
  end
end
