﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit UnlockPhieu;

interface

uses
  Classes, Controls, Forms, StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls,
  SysUtils, Data.DB, Data.Win.ADODB, wwdblook, Vcl.DBCtrls;

type
  TFrmUnlockPhieu = class(TForm)
    CmdAction: TBitBtn;
    BtnCancel: TBitBtn;
    Label15: TLabel;
    Label16: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    EdBarcode: TEdit;
    wwDBLookupCombo1: TwwDBLookupCombo;
    QrLCT: TADOQuery;
    EdUser1: TwwDBLookupCombo;
    EdUser2: TwwDBLookupCombo;
    EdDate1: TDBText;
    EdDate2: TDBText;
    Label1: TLabel;
    EdUser3: TwwDBLookupCombo;
    EdDate3: TDBText;
    CmdReturn: TBitBtn;
    QrList: TADOQuery;
    DsList: TDataSource;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
    procedure CmdActionClick(Sender: TObject);
  private
  public
  	function Execute : Boolean;
  end;

var
  FrmUnlockPhieu: TFrmUnlockPhieu;

implementation

{$R *.DFM}

uses
	MainData, MasterData, ExCommon, isStr, isMsg, isLib, Rights;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_ACTION  = 'Mở khóa Phiếu thì những dữ liệu đang nhập sẽ không được lưu. Tiếp tục?';


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmUnlockPhieu.Execute : Boolean;
begin
	Result := ShowModal = mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUnlockPhieu.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUnlockPhieu.FormShow(Sender: TObject);
var
    sSql: String;
begin
    TMyForm(Self).Init;
    DataMain.QrUSER.Open;

    with QrLCT do
    begin
        sSql := 'select * from SYS_LCT where 1 = 1 and isnull(TABLENAME, '''') <> '''' ';
        sSql := sSql + ' and LCT in (''CKHO'')';
        sSql := sSql + ' order by TABLENAME, PREFIX';
        SQL.Text := sSql;
        Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUnlockPhieu.CmdActionClick(Sender: TObject);
begin
    with QrList do
    begin
        if not Active then
            Exit;

        if (FindField('LOCKED_BY') = nil) or
            (FindField('LOCKED_DATE') = nil) then
            Exit;

        if FieldByName('LOCKED_BY').AsInteger <> 0 then
        begin
            if not YesNo('Mở khóa Phiếu sẽ bỏ qua dữ liệu đang chỉnh sửa. Tiếp tục?') then
                Exit;

            Edit;
            FieldByName('LOCKED_BY').Clear;
            FieldByName('LOCKED_DATE').Clear;
            Post;
            MsgDone;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUnlockPhieu.CmdReturnClick(Sender: TObject);
var
    sSql, lct, tbname, sct: string;
begin
    with QrLCT do
    begin
        lct := FieldByName('LCT').AsString;
        tbname := FieldByName('TABLENAME').AsString;
    end;
    sct := EdBarcode.Text;

    if (sct = '') or
        (tbname = '') then
    begin
        Msg('Phải nhập đủ thông tin tìm kiếm');
        Exit;
    end;

    with QrList do
    begin
        if Active then
            Close;

        sSql := 'select top 1 * from ' + tbname + ' where SCT = ''' + sct + '''';
        SQL.Text := sSql;
        Open;

        if IsEmpty then
        begin
            Msg('Không tìm thấy Số phiếu của Chứng từ');
            Exit;
        end;

        if (FindField('LOCKED_BY') = nil) or
            (FindField('LOCKED_DATE') = nil) then
        begin
            EdUser3.DataField := '';
            EdDate3.DataField := '';

            ErrMsg('Chứng từ không có chức năng "Khóa phiếu đang sửa". Liên hệ nhà cung cấp phần mềm.');
            Exit;
        end;

        EdUser3.DataField := 'LOCKED_BY';
        EdDate3.DataField := 'LOCKED_DATE';
    end;
end;

end.
