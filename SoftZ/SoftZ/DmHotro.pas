﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmHotro;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids, StdCtrls, Db,
  Wwdbgrid2, ADODb, wwdblook,
  AppEvnts, Menus, AdvMenus, DBCtrls, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmHotro = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    QrDmkhac: TADOQuery;
    DsDmkhac: TDataSource;
    CmdSearch: TAction;
    QrDmkhacX: TADOQuery;
    DsDmkhacX: TDataSource;
    Panel1: TPanel;
    CbKhac: TwwDBLookupCombo;
    Label1: TLabel;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    TntDBMemo1: TDBMemo;
    CmdAdmin: TAction;
    QrDmkhacMANHOM_HOTRO: TWideStringField;
    QrDmkhacMA_HOTRO: TWideStringField;
    QrDmkhacTEN_HOTRO: TWideStringField;
    QrDmkhacCREATE_BY: TIntegerField;
    QrDmkhacUPDATE_BY: TIntegerField;
    QrDmkhacCREATE_DATE: TDateTimeField;
    QrDmkhacUPDATE_DATE: TDateTimeField;
    QrDmkhacXMANHOM_HOTRO: TWideStringField;
    QrDmkhacXTENNHOM_HOTRO: TWideStringField;
    QrDmkhacXVISIBLE: TBooleanField;
    QrDmkhacXSYS: TIntegerField;
    QrDmkhacXSORT: TIntegerField;
    QrDmkhacXSZ: TBooleanField;
    QrDmkhacXHR: TBooleanField;
    QrDmkhacXOTHER: TBooleanField;
    QrDmkhacXGHICHU: TWideStringField;
    QrDmkhacXCREATE_BY: TIntegerField;
    QrDmkhacXUPDATE_BY: TIntegerField;
    QrDmkhacXCREATE_DATE: TDateTimeField;
    QrDmkhacXUPDATE_DATE: TDateTimeField;
    QrDmkhacXACC: TBooleanField;
    QrDmkhacXPOM: TBooleanField;
    QrDmkhacXASS: TBooleanField;
    QrDmkhacXFB: TBooleanField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDmkhacBeforePost(DataSet: TDataSet);
    procedure QrDmkhacBeforeDelete(DataSet: TDataSet);
    procedure QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrDmkhacBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbKhacNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrDmkhacAfterInsert(DataSet: TDataSet);
    procedure CmdAdminExecute(Sender: TObject);
    procedure TntDBMemo1Exit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  	mCanEdit, fixCode: Boolean;
    mFilter: Integer;
    mSql, mKey: String;
    procedure OpenQueries;
  public
  	procedure Execute(r: WORD; x: Integer = 0; pkey: String = '');//0: SZ; 1: HR; 2: KT; 3: SX; 4:ORTHER ; 5: TS
  end;

var
  FrmDmHotro: TFrmDmHotro;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib;

{$R *.DFM}

const
	FORM_CODE = 'DM_HOTRO';
    TABLE_NAME = FORM_CODE;
    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.Execute;
begin
	mCanEdit := rCanEdit (r);
    mFilter := x;
    mKey := pkey;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDmkhac, TABLE_NAME, 0);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDmkhac, FORM_CODE);
    fixCode := SetCodeLength(FORM_CODE, QrDmkhac.FieldByName('MA_HOTRO'));
    mSql := QrDmkhacX.SQL.Text;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdNewExecute(Sender: TObject);
begin
	QrDmKhac.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdSaveExecute(Sender: TObject);
begin
	QrDmKhac.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdCancelExecute(Sender: TObject);
begin
	QrDmKhac.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdDelExecute(Sender: TObject);
begin
	QrDmKhac.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDmKhac, QrDmKhacX]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.OpenQueries;
var
    sSql: String;
begin
    with QrDmkhacX do
    begin
        if Active then
            Close;
        sSql := mSql;

        case mFilter of
            0: sSql := sSql + ' and isnull(SZ, 0) = 1';
            1: sSql := sSql + ' and isnull(HR, 0) = 1';
            2: sSql := sSql + ' and isnull(ACC, 0) = 1';
            3: sSql := sSql + ' and isnull(POM, 0) = 1';
            5: sSql := sSql + ' and isnull(ASS, 0) = 1';
            6: sSql := sSql + ' and isnull(FB, 0) = 1';
        end;
        sSql := sSql + ' order by SORT';

        SQL.Text := sSql;
        Open;
    end;
    if mKey <> '' then
        CbKhac.LookupValue := mKey
    else
        CbKhac.LookupValue := QrDmKhacX.FieldByName('MANHOM_HOTRO').AsString;


    if mKey <> '' then
    begin
        Panel1.Visible := False;
        Caption := CbKhac.Text;
    end;

	with QrDmKhac do
    begin
    	Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormShow(Sender: TObject);
var
    i: Integer;
begin
	GrList.ReadOnly := not mCanEdit;
	OpenQueries;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkhac, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmKhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacAfterInsert(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
        FieldByName('MANHOM_HOTRO').AsString := QrDmkhacX.FieldByName('MANHOM_HOTRO').AsString;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacBeforePost(DataSet: TDataSet);
var
    s: string;
begin
	with QrDmKhac do
    begin
        if BlankConfirm(QrDmKhac, ['MA', 'TEN_HOTRO']) then
    		Abort;

        if fixCode then
    		if LengthConfirm(QrDmKhac, ['MA']) then
        		Abort;
        s := QrDmkhacX.FieldByName('MANHOM_HOTRO').AsString;
        FieldByName('MANHOM_HOTRO').AsString := s;
        FieldByName('MA_HOTRO').AsString := s + '.' + FieldByName('MA').AsString;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDmkhac);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.QrDmkhacBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MA_HOTRO') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDMKHAC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CbKhacNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.CmdAdminExecute(Sender: TObject);
begin
	if DsDmkhacx.AutoEdit then
    	Exit;
    DsDmkhacx.AutoEdit := FixLogon;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.TntDBMemo1Exit(Sender: TObject);
begin
	QrDmkhacx.CheckBrowseMode
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmHotro.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

end.
