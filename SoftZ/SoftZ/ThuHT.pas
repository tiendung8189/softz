﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThuHT;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook, System.Variants,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2, AdvMenus, AppEvnts, isPanel, wwfltdlg,
  frameNgay, frameNavi, wwDialog, Grids, Wwdbigrd, Wwdbgrid, ToolWin, Wwdotdot,
  Wwdbcomb, Buttons, AdvEdit, DBAdvEd, DBCtrlsEh, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmThuHT = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMADT: TWideStringField;
    QrTCLK_TENDT: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    PopupMenu2: TAdvPopupMenu;
    Bevel1: TBevel;
    PaMaster: TisPanel;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    HanbnlCVAT1: TMenuItem;
    N2: TMenuItem;
    Hanbnl1: TMenuItem;
    QrTCLCT: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    Panel1: TPanel;
    Label1: TLabel;
    DBText1: TDBText;
    CbNGAY: TwwDBDateTimePicker;
    PaTK: TPanel;
    Panel4: TPanel;
    QrTCMATK: TWideStringField;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    QrTCMALOC: TWideStringField;
    QrTCLK_TENLOC: TWideStringField;
    PaMALOC: TPanel;
    PaMADT: TPanel;
    N3: TMenuItem;
    ItemObsolete: TMenuItem;
    QrTCSCT2: TWideStringField;
    EdSoPhieu: TDBEditEh;
    CbHinhThuc: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    CbbMaLoc: TDbLookupComboboxEh2;
    EdMaLoc: TDBEditEh;
    EdNGUOIGIAO: TDBEditEh;
    EdNumSoTien: TDBNumberEditEh;
    DBMemoEh1: TDBMemoEh;
    CbTenTK: TDbLookupComboboxEh2;
    EdSTK: TDBEditEh;
    EdNganhang: TDBEditEh;
    EdChinhanh: TDBEditEh;
    CmdXemCongNo: TAction;
    EdMaDT: TDBEditEh;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    QrTCMaCP: TWideStringField;
    QrTCMATK2: TWideStringField;
    QrTCNhanVienThuNgan: TIntegerField;
    QrTCSoTienTraHang: TFloatField;
    QrTCSoGiaoDichNganHang: TWideStringField;
    QrTCGhiChu: TWideMemoField;
    QrTCSoLuong: TFloatField;
    QrTCSoDu: TFloatField;
    QrTCNguoi: TWideStringField;
    QrTCSoTien: TFloatField;
    QrTCThanhToan: TFloatField;
    QrTCLyDo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure CmdXemCongNoExecute(Sender: TObject);
    procedure QrTCMATKChange(Sender: TField);
  private
  	mMakho, mPTTT, mLCT: String;
	mCanEdit, mFuncTK, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;
  public
	procedure Execute (r : WORD);
  end;

var
  FrmThuHT: TFrmThuHT;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    isLib, CongnoNCC;

{$R *.DFM}

const
    FORM_CODE: String = 'PHIEU_THUHT';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsTC.AutoEdit := mCanEdit;

    mLCT := 'THUHT';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    mFuncTK := sysFuncTK;
    mPTTT := RegReadString(Name, 'Pttt', sysPTTT);
    mMakho := RegReadString(Name, 'Makho', sysDefKho);

    mObsolete := False;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormShow(Sender: TObject);
begin
    with DataMain do
    	OpenDataSets([QrPTTT, QrDMKHO, QrDM_KH_NCC, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);

    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsDrc then
    begin
        PaMALOC.Visible := False;
        grRemoveFields(GrBrowse, 'MALOC;LK_TENLOC', ';');
    end;

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
        grRemoveFields(GrBrowse, 'MATK;LK_TENTK;LK_NGANHANG', ';');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Pttt'], [mMakho, mPTTT]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
   	    CbNgay.SetFocus
	else
    	GrBrowse.SetFocus;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
		QrPTTT.Requery;
        QrDMKHO.Requery;
        QrDM_KH_NCC.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdNewExecute(Sender: TObject);
begin
	if not (QrTC.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
			QrTC.Cancel;

	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
        exSearch(Name, DsTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdXemCongNoExecute(Sender: TObject);
begin
    if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrTC do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdClearFilterExecute(Sender: TObject);
begin
    with  filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not bEmpty;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := d;
		FieldByName('LCT').AsString    := mLCT;
        FieldByName('MAKHO').AsString  := mMakho;
        FieldByName('MALOC').AsString  := sysLoc;
        FieldByName('LOC').AsString    := sysLoc;
        FieldByName('PTTT').AsString   := mPTTT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
		Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforePost(DataSet: TDataSet);
var
    pPTTT: string;
begin
	with QrTC do
    begin
        if BlankConfirm(QrTC, ['NGAY','PTTT']) then
	    	Abort;

        if BlankConfirm(QrTC, ['MALOC']) then
        begin
            CbbMaLoc.SetFocus;
            Abort;
        end;

        if BlankConfirm(QrTC, ['MADT']) then
        begin
            CbbNhaCungCap.SetFocus;
            Abort;
        end;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

        if BlankConfirm(QrTC, [ 'SoTien']) then
	    	Abort;




        if FieldByName('PTTT').AsString = '02' then
            pPTTT := 'THUCK'
        else if FieldByName('PTTT').AsString = '01' then
            pPTTT := 'THUTM'
        else
            pPTTT := '';

	    exValidClosing(FieldByName('NGAY').AsDateTime, 2);
    	SetNull(QrTC, ['PTTT', 'MAKHO', 'MADT']);
    end;
	DataMain.AllocSCT(mLCT, QrTC);
    DataMain.AllocSCT2(pPTTT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCMATKChange(Sender: TField);
begin
    EdChinhanh.Text := EdChinhanh.Field.AsString;
    EdNganhang.Text := EdNganhang.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCNGAYValidate(Sender: TField);
begin
    with QrTC do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('ThanhToan').AsFloat := FieldByName('SoTien').AsFloat
end;

(*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.QrTCAfterPost(DataSet: TDataSet);
begin
	with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
        mPTTT := FieldByName('PTTT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThuHT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;



end.
