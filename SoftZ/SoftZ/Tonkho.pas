﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Tonkho;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  StdCtrls, Wwdbigrd, Wwdbgrid2, ComCtrls, ExtCtrls, ActnList,
  Db, ADODB, wwdblook, Menus, Wwfltdlg2, AppEvnts,
  AdvMenus, wwfltdlg, wwDialog, Grids, Wwdbgrid, ToolWin, Wwkeycb, AdvEdit,
  Vcl.DBCtrls, wwdbdatetimepicker, Vcl.Mask, wwdbedit, DBAdvEd, DBGridEh,
  DBLookupEh, DbLookupComboboxEh2, DBCtrlsEh, Variants, MemTableDataEh,
  MemTableEh;

type
  TFrmTonkho = class(TForm)
    ToolBar1: TToolBar;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    Action: TActionList;
    CmdClose: TAction;
    Panel1: TPanel;
    Status: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    DsLIST: TDataSource;
    CmdRefresh: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Filter: TwwFilterDialog2;
    EdKy: TComboBox;
    EdNam: TComboBox;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    N2: TMenuItem;
    CmdReRead: TAction;
    CmdCalc: TAction;
    TntToolButton1: TToolButton;
    SepTinhton: TToolButton;
    CmdAudit: TAction;
    CmdPrint: TAction;
    TntToolButton4: TToolButton;
    Panel3: TPanel;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TaPayroll: TTabSheet;
    Panel4: TPanel;
    QrLIST: TADOStoredProc;
    Panel5: TPanel;
    GrList2: TwwDBGrid2;
    ckbTon: TCheckBox;
    Label16: TLabel;
    EdTonDen: TwwDBDateTimePicker;
    QrLIST2: TADOStoredProc;
    DsLIST2: TDataSource;
    Filter2: TwwFilterDialog2;
    Panel6: TPanel;
    Panel7: TPanel;
    QrNGANH: TADOQuery;
    QrNHOM: TADOQuery;
    EdMADT: TDBEditEh;
    CbMADT: TDbLookupComboboxEh2;
    EdSearch: TDBEditEh;
    CbMANGANH: TDbLookupComboboxEh2;
    EdMANGANH: TDBEditEh;
    DsNGANH: TDataSource;
    DsNHOM: TDataSource;
    CbMANHOM: TDbLookupComboboxEh2;
    EhMANHOM: TDBEditEh;
    CbMaKho: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdCalcExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure QrLISTAfterOpen(DataSet: TDataSet);
    procedure QrLISTFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QrLISTCalcFields(DataSet: TDataSet);
    procedure EdKyChange(Sender: TObject);
    procedure EdMaDTKeyPress(Sender: TObject; var Key: Char);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EdSearchChange(Sender: TObject);
    procedure CbMANHOMDropDown(Sender: TObject);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
  	mCanEdit, mBookClosed, mTonAm: Boolean;
  	mKy, mNam: Integer;
    mKho, mSearch, mNganh, mNhom, mNCC: String;
    mStockDate: TDateTime;

    function GetDate: TDateTime;
    function GetStockDate(d: TDateTime): TDateTime;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmTonkho: TFrmTonkho;

implementation

uses
	isStr, MainData, ExCommon, isDb, isMsg, RepEngine, Rights, isLib, isCommon,
  TudenTon, GmsRep;

{$R *.DFM}

const
	FORM_CODE = 'TONKHO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.EdKyChange(Sender: TObject);
begin
    GetDate;
end;

procedure TFrmTonkho.EdMaDTKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.EdSearchChange(Sender: TObject);
var
    s: string;
begin
    with (Sender as TDBEditEh) do
    begin
        s := Value;
        exPerformCustomSearch(QrLIST, 'TENVT', s);
        exPerformCustomSearch(QrLIST2, 'TENVT', s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.Execute;
begin
	mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrNGANH, QrNHOM]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;

	isMonthList(EdKy);
    isYearList(EdNam);
    ckbTon.Checked := False;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrLISTAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrLIST, sysQtyFmt);
    SetDisplayFormat(QrLIST2, sysQtyFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_TONG'], [GrList, GrList2]);
    SetDictionary([QrLIST, QrLIST2], [FORM_CODE, FORM_CODE + '_TONG'], [Filter, Filter2]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DATA_ARISE = 'Có dữ liệu tồn kho trước tháng bắt đầu sử dụng chương trình.' +
            	    #13 + 'Xin liên hệ với người quản trị hệ thống để hiệu chỉnh.';

procedure TFrmTonkho.FormShow(Sender: TObject);
begin
	// Open database
    OpenDataSets([DataMain.QrDMKHO, QrNGANH, QrNHOM, DataMain.QrDM_KH_NCC]);

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Start to use checking
    with TADOQuery.Create(Self) do
    begin
    	Connection := DataMain.Conn;
        LockType := ltReadOnly;

		SQL.Text := Format('select 1 from TONKHO where KY+12*NAM<%d',
        	[sysBegMon + 12 * sysBegYear]);
        Open;

        if not IsEmpty then
            Msg(RS_DATA_ARISE);

        Close;
        Free;
    end;

    mStockDate := GetSysParam('STOCK_DATE');
    GetStockDate(mStockDate);

    CmdCalc.Visible := FlexConfigBool('TONKHO', 'Tinhton', False);
    SepTinhton.Visible := CmdCalc.Visible;

    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTonkho.GetDate: TDateTime;
var
    yy, mm, dd: word;
begin
    mm := StrToInt(EdKy.Text);
    yy := StrToInt(EdNam.Text);

    Result := IncMonth(EncodeDate(yy, mm, 1)) - 1;
    if Result > mStockDate then
        Result := mStockDate;

    EdTonDen.DateTime := Result;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTonkho.GetStockDate(d: TDateTime): TDateTime;
var
    yy, mm, dd: word;

begin
    if d = 0 then
        if (sysMon = sysBegMon) and (sysYear = sysBegYear) then
            d := Date
        else
            d := IncMonth(sysBegDate) - 1;

    DecodeDate(d, yy, mm, dd);
    EdNam.ItemIndex := yy;
    EdKy.ItemIndex := mm - 1;
    EdTonDen.DateTime := d;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_MONTH = 'Tháng làm việc không hợp lệ. Tháng bắt đầu sử dụng chương trình là %d/%d.';

procedure TFrmTonkho.CmdRefreshExecute(Sender: TObject);
var
    s, sSearch, sNganh, sNhom, sNCC, sKho: String;
	ky, nam, n: Integer;
    b, bTonAm: Boolean;
    mDate: TDateTime;
begin
    ky := StrToInt(EdKy.Text);
    nam := StrToInt(EdNam.Text);
    bTonAm := ckbTon.Checked;
    n := PgMain.ActivePageIndex;

    sSearch := EdSearch.Text;
    sNganh := EdMANGANH.Text;
    sNhom := EhMANHOM.Text;
    sNCC := EdMaDT.Text;
    sKho := EdMaKho.Text;

	if (mKy <> ky) or (mNam <> nam)
        or (sKho <> mKho)
        or (sSearch <> mSearch)
        or (bTonAm <> mTonAm)
        or (sNganh <> mNganh)
        or (sNhom <> mNhom)
        or (sNCC <> mNCC)
    then
    begin
    	mKy := ky;
        mNam := nam;
        mSearch := sSearch;
        mTonAm := bTonAm;
        mNganh := sNganh;
        mNhom := sNhom;
        mNCC := sNCC;

        if n = 1 then
            mKho := ''
        else
            mKho := sKho;

    	with QrLIST do
        begin
        	s := Sort;
            if Active then
            	Close;

            Parameters[1].Value := nam;
            Parameters[2].Value := ky;
            Parameters[3].Value := False;
            Parameters[4].Value := mKho;
            Parameters[5].Value := sysLOC;
            Parameters[6].Value := mNganh;
            Parameters[7].Value := mNhom;
            Parameters[8].Value := mNCC;
            Open;
            if s <> '' then
            	Sort := s;
        end;

        with QrLIST2 do
        begin
        	s := Sort;
            if Active then
            	Close;

            Parameters[1].Value := nam;
            Parameters[2].Value := ky;
            Parameters[3].Value := False;
            Parameters[4].Value := sysLOC;
            Parameters[5].Value := mNganh;
            Parameters[6].Value := mNhom;
            Parameters[7].Value := mNCC;
            Open;
            if s <> '' then
            	Sort := s;
        end;

        // Check valid month
//        b := mKy + 12 * mNam >= sysBegMon + 12 * sysBegYear;
//
//        if not b then
//            Msg(Format(RS_INVALID_MONTH, [sysBegMon, sysBegYear]));
//
        // Check BookClose.
//		mDate := EncodeDate(mNam, mKy, 1);
//    	mBookClosed := mDate <= sysCloseHH;
    end;

    if n = 1 then
        GrList2.SetFocus
    else
        GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
	with QrLIST do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdReRead.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        exSearch(Name, DsLIST2)
    else
        exSearch(Name, DsLIST);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
        Filter2.Execute
    else
    	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
var
    s: String;
    n: Integer;
begin
    n := PgMain.ActivePageIndex;
    if n = 1 then
        s := exRecordCount(QrLIST2, Filter)
    else
        s := exRecordCount(QrLIST, Filter);

    Status.SimpleText := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdReReadExecute(Sender: TObject);
begin
	mKy := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Xác nhận tính tồn kho đến ngày %s. Tiếp tục?';

procedure TFrmTonkho.CmdCalcExecute(Sender: TObject);
var
    mDate: TDateTime;
begin
    if GetRights('SZ_TONTT') = R_DENY then
        Exit;

    // Make sure the params are correct
    CmdRefresh.Execute;
    mDate := EdTonDen.DateTime;

    Application.CreateForm(TFrmTudenTon, FrmTudenTon);
    if FrmTudenTon.Execute(mDate) then
    begin
        mStockDate := GetSysParam('STOCK_DATE');
        GetStockDate(mDate);
        //
        CmdReread.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsLIST);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CmdPrintExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('TonKho') (*19*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrLISTFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
    s: String;
    b: Boolean;
begin
//    s := UpperCase(DataMain.StripToneMark(EdSearch.Text));
//    if s <> '' then
//        Accept := (Pos(s, UpperCase(DataMain.StripToneMark(DataSet.FieldByName('TENVT').AsString))) > 0)
//                or (Pos(s, UpperCase(DataMain.StripToneMark(DataSet.FieldByName('MAVT').AsString))) > 0);
//
//    b := ckbTon.Checked;
//    if b then
//        Accept := Accept and (DataSet.FieldByName('LG_CUOI').AsFloat < 0)
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := sysDefKho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.QrLISTCalcFields(DataSet: TDataSet);
begin
//    with QrLIST do
//    begin
//        if not Active then
//            Exit;
//
//        FieldByName('RSTT').AsInteger := Abs(RecNo);
//    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTonkho.CbMANHOMDropDown(Sender: TObject);
var
    s: String;
begin
    if not VarIsNull(CbMANGANH.Value) then
        s := CbMANGANH.Value
    else
        s := '';

    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

end.
