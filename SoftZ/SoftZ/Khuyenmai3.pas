﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Khuyenmai3;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwdbgrid,
  frameNgay, frameNavi, isDb,
  isPanel, wwDialog, Mask, Grids, ToolWin, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmKhuyenmai3 = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrKM: TADOQuery;
    QrCT: TADOQuery;
    DsKM: TDataSource;
    DsCT: TDataSource;
    QrCTRSTT: TIntegerField;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrKMXOA: TWideStringField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrKMIMG: TIntegerField;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBText2: TDBText;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    CmdAudit: TAction;
    QrKMNGAY: TDateTimeField;
    QrKMLCT: TWideStringField;
    QrKMSCT: TWideStringField;
    QrKMTUNGAY: TDateTimeField;
    QrKMDENNGAY: TDateTimeField;
    QrKMLYDO: TWideStringField;
    QrKMDGIAI: TWideMemoField;
    QrKMCREATE_BY: TIntegerField;
    QrKMCREATE_DATE: TDateTimeField;
    QrKMUPDATE_BY: TIntegerField;
    QrKMUPDATE_DATE: TDateTimeField;
    QrKMDELETE_BY: TIntegerField;
    QrKMDELETE_DATE: TDateTimeField;
    QrCTSTT: TIntegerField;
    QrCTMAVT: TWideStringField;
    QrCTGIABAN: TFloatField;
    QrCTTL_LAI: TFloatField;
    TntLabel1: TLabel;
    EdLyDo: TwwDBEdit;
    Label5: TLabel;
    CbTungay: TwwDBDateTimePicker;
    TntLabel2: TLabel;
    CbDenngay: TwwDBDateTimePicker;
    Label10: TLabel;
    DBEdit1: TDBMemo;
    QrCTLK_TENVT: TWideStringField;
    QrCTLK_GIABAN: TFloatField;
    CmdUpdate: TAction;
    TntToolButton2: TToolButton;
    QrDMHH: TADOQuery;
    QrCTLK_DVT: TWideStringField;
    QrCTLK_TL_LAI: TFloatField;
    QrKMDENGIO: TDateTimeField;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    wwDBEdit1: TwwDBDateTimePicker;
    QrKMTUGIO: TDateTimeField;
    wwDBEdit2: TwwDBDateTimePicker;
    CmdDiemban: TAction;
    TntToolButton4: TToolButton;
    Status2: TStatusBar;
    Filter2: TwwFilterDialog2;
    CmdListRefesh: TAction;
    QrHinhthuc: TADOQuery;
    QrKMLK_HINHTHUC: TWideStringField;
    Label3: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    QrKMDENNGAY2: TDateTimeField;
    isPanel1: TisPanel;
    GrCTHD: TwwDBGrid2;
    Splitter1: TSplitter;
    QrCTHD: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField3: TIntegerField;
    DsCTHD: TDataSource;
    QrCTHDTU: TFloatField;
    QrCTHDDEN: TFloatField;
    QrCTHDTL_CK: TFloatField;
    QrCTHDGHICHU: TWideStringField;
    PaKM: TisPanel;
    GrCTKM: TwwDBGrid2;
    Bevel3: TSplitter;
    Splitter2: TSplitter;
    QrCT2: TADOQuery;
    IntegerField4: TIntegerField;
    QrCT2MAVT: TWideStringField;
    QrCT2GIABAN: TFloatField;
    QrCT2LK_TENVT: TWideStringField;
    QrCT2LK_GIABAN: TFloatField;
    QrCT2LK_DVT: TWideStringField;
    DsCT2: TDataSource;
    QrCT2TU: TFloatField;
    QrCT2DEN: TFloatField;
    QrCT2TL_CK: TFloatField;
    QrCT2SOLUONG: TFloatField;
    QrCT2DONGIA: TFloatField;
    QrCT2GHICHU: TWideStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    popDetail: TAdvPopupMenu;
    CmdEmpty: TAction;
    Xachitit1: TMenuItem;
    CmdDsNCC: TAction;
    CmdDsNhom: TAction;
    QrCTGHICHU: TWideStringField;
    Danhschnhcungcp1: TMenuItem;
    Danhschngnhnhmhng1: TMenuItem;
    N3: TMenuItem;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    CmdChecked: TAction;
    QrKMCHECKED: TBooleanField;
    QrKMKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrCTHDKHOACT: TGuidField;
    QrCTHDKHOA: TGuidField;
    QrCT2KHOACT2: TGuidField;
    QrCT2KHOACT: TGuidField;
    QrCT2KHOA: TGuidField;
    QrKMLOC: TWideStringField;
    QrKMCHECKED_BY: TIntegerField;
    QrKMCHECKED_DATE: TDateTimeField;
    QrKMCHECKED_DESC: TWideMemoField;
    DsHinhthuc: TDataSource;
    CbLCT: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrKMAfterInsert(DataSet: TDataSet);
    procedure QrKMBeforeOpen(DataSet: TDataSet);
    procedure QrKMBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrKMBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKMCalcFields(DataSet: TDataSet);
    procedure QrKMBeforeEdit(DataSet: TDataSet);
    procedure QrKMAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrKMAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdDiembanExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbLCTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure QrKMDENNGAY2Change(Sender: TField);
    procedure QrCTHDBeforePost(DataSet: TDataSet);
    procedure QrCT2BeforePost(DataSet: TDataSet);
    procedure QrCT2BeforeInsert(DataSet: TDataSet);
    procedure QrCTAfterScroll(DataSet: TDataSet);
    procedure QrCT2MAVTChange(Sender: TField);
    procedure QrCT2GIABANChange(Sender: TField);
    procedure QrCTGIABANChange(Sender: TField);
    procedure QrKMLCTValidate(Sender: TField);
    procedure QrCTHDBeforeDelete(DataSet: TDataSet);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure QrCTHDBeforeInsert(DataSet: TDataSet);
    procedure QrKMLCTChange(Sender: TField);
    procedure CmdDsNhomExecute(Sender: TObject);
    procedure CmdDsNCCExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
  private
	mCanEdit: Boolean;
   	fTungay, fDenngay: TDateTime;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
    procedure SetReadOnlyField();
  public
	procedure Execute(r: WORD);
  end;

var
  FrmKhuyenmai3: TFrmKhuyenmai3;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, Math, isCommon,
    ChonDsma, isLib, Khuyenmai2, ChonDsNCC, ChonDsNhom, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_KHUYENMAI_CK';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.Execute;
begin
	mCanEdit := rCanEdit(r);
    PaInFo.Enabled := mCanEdit;
    GrDetail.ReadOnly := not mCanEdit;
    GrCTHD.ReadOnly := not mCanEdit;
    GrCTKM.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrKM;

    // Display Format
    with QrKM do
    begin
	    SetShortDateFormat(QrKM);
    	SetDisplayFormat(QrKM, ['TUGIO', 'DENGIO'], 'hh:nn');
        SetDisplayFormat(QrKM, ['NGAY'], DateTimeFmt);
    end;

    SetDisplayFormat(QrCTHD, sysCurFmt);
    SetDisplayFormat(QrCTHD, ['TL_CK'], sysPerFmt);

    SetDisplayFormat(QrCT2, sysCurFmt);
    SetDisplayFormat(QrCT2, ['TU', 'DEN', 'SOLUONG'], ctQtyFmt);
    SetDisplayFormat(QrCT2, ['TL_CK'], sysPerFmt);

	SetDisplayFormat(QrCT, sysCurFmt);

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_HD', FORM_CODE + '_KM'], [GrBrowse, GrDetail, GrCTHD, GrCTKM]);
    SetDictionary([QrKM, QrCT2], [FORM_CODE, FORM_CODE + '_KM'], [Filter, nil]);

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrKM.SQL.Text;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2;

	Wait(PREPARING);
    QrDMHH.Open;
    SetDisplayFormat(QrDMHH, sysCurFmt);
    SetDisplayFormat(QrDMHH,['TyLeLai'], sysCurFmt);
    ClearWait;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrKM, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCT2GIABANChange(Sender: TField);
var
    s: String;
    dg: Double;
begin
    if Sender.Tag = 88 then
    begin
        Sender.Tag := 0;
        Exit;
    end;
    s := Sender.FieldName;
    dg := QrCT.FieldByName('GIABAN').AsFloat;
    with QrCT2 do
    begin
        if s = 'GIABAN' then
            with FieldByName('TL_CK') do
            begin
                Tag := 88;
                AsFloat := RoundTo(
                    100*(1 - FieldByName('GIABAN').AsFloat/dg), -4);
            end
        else
            with FieldByName('GIABAN') do
            begin
                Tag := 88;
                AsFloat := exVNDRound(
                    dg*(1 - FieldByName('TL_CK').AsFloat/100), ctCurNumRound);
            end;
    end;

    GrCTKM.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDMHH]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;

		Screen.Cursor := crSQLWait;
		with QrKM do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from KHUYENMAI_CT a, DM_HH b, DM_HH_CT c, DM_NHOM d where a.KHOA = KHUYENMAI.KHOA and b.MAVT = c.MAVT and a.MAVT = c.MABH and b.MANHOM = d.MANHOM and d.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from KHUYENMAI_CT a, DM_HH b, DM_HH_CT c where a.KHOA = KHUYENMAI.KHOA and b.MAVT = c.MAVT and a.MAVT = c.MABH and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select a.KHOA from KHUYENMAI_CT a where a.KHOA = KHUYENMAI.KHOA and a.MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY, SCT');
    	    Open;
            
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	QrDMHH.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdNewExecute(Sender: TObject);
begin
	if not (QrKM.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrKM.Cancel;
        end;

	QrKM.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdSaveExecute(Sender: TObject);
begin
    QrCTHD.CheckBrowseMode;
    QrCT.CheckBrowseMode;
    QrCT2.CheckBrowseMode;
    QrKM.Post;
    exSaveDetails(QrCTHD);
    exSaveDetails(QrCT);
    with QrCT2 do
    begin
        Filtered := False;
        UpdateBatch;
        Filtered := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdCancelExecute(Sender: TObject);
begin
    QrCTHD.CancelBatch;
    QrCT2.CancelBatch;
	QrCT.CancelBatch;
	QrKM.Cancel;

    if QrKM.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdCheckedExecute(Sender: TObject);
begin
	exChecked(QrKM, 'SZ_KHUYENMAI_DACHIEU');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrKM.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrCTKM then
    	try
    		CbNgay.SetFocus;
        except
            Perform(WM_NEXTDLGCTL, 0, 0)
        end
    else if ActiveControl = GrCTHD then
            Perform(WM_NEXTDLGCTL, 0, 0)
    else if ActiveControl = GrDetail then
        Perform(WM_NEXTDLGCTL, 0, 0)
    else
        try
            GrCTHD.SetFocus;
        except
            GrDetail.SetFocus;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsKM)
    else
        exSearch(Name + '_CT2', DsCT2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, b, bChecked: Boolean;
    n: Integer;
begin
	with QrKM do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
		bChecked := FieldByName('CHECKED').AsBoolean;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrKM);

    CmdChecked.Enabled := not bEmpty;
    CmdChecked.Caption := exGetCheckedCaption(QrKM);

    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;
    CmdUpdate.Enabled := mCanEdit and not bEmpty and bBrowse;
    CmdDiemban.Enabled := mCanEdit and not bEmpty and bBrowse;

    b := QrKM.FieldByName('LCT').AsString = 'CK';
    GrDetail.Enabled := not b;
    GrCTKM.Enabled := not b;
    GrCTHD.Enabled := b;

    CmdDsNCC.Enabled := not b and mCanEdit;
    CmdDsNhom.Enabled := not b and mCanEdit;
end;
	(*
    ** End: Actions
    *)

	(*
	**  Master DB
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrKM do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime    := d;
        FieldByName('TUNGAY').AsDateTime  := d;
        FieldByName('DENNGAY2').AsDateTime := d;
        FieldByName('TUGIO').AsDateTime   := EncodeTime(0,0,0,0);
        FieldByName('DENGIO').AsDateTime  := 1 + EncodeTime(0,0,0,0);

        mTrigger := True;
        FieldByName('LCT').AsString := QrHinhthuc.FieldByName('MA').AsString;
        mTrigger := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMDENNGAY2Change(Sender: TField);
begin
    with QrKM do
        FieldByName('DENNGAY').AsDateTime := FieldByName('DENNGAY2').AsDateTime;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMLCTChange(Sender: TField);
begin
    SetReadOnlyField;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMLCTValidate(Sender: TField);
begin
    if mTrigger then
        Exit;

    if not (QrCT.IsEmpty and QrCTHD.IsEmpty) then
    begin
        try
            Msg(Format('Không thể thay đổi %s', [Sender.DisplayLabel]));
            Abort;
        finally
            CbLCT.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
            CbLCT.Reset;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.SetReadOnlyField;
var
    sLCT: String;
begin
    sLCT := QrKM.FieldByName('LCT').AsString;

    with GrCTKM do
    begin
        ColumnByName('TL_CK').ReadOnly := sLCT <> 'KM';
        ColumnByName('GIABAN').ReadOnly := sLCT <> 'GG';
        ColumnByName('MAVT').ReadOnly := sLCT <> 'QT';
        ColumnByName('SOLUONG').ReadOnly := sLCT <> 'QT';

        RedrawGrid;
        SetColumnAttributes;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMBeforeOpen(DataSet: TDataSet);
begin
	with QrKM do
    begin
		Parameters[0].Value := fTungay;
		Parameters[1].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMBeforePost(DataSet: TDataSet);
var
	dengio: TDateTime;
begin
	with QrKM do
    begin
		if BlankConfirm(QrKM, ['NGAY', 'LCT', 'TUNGAY', 'DENNGAY']) then
	    	Abort;

	    exValidClosing(FieldByName('NGAY').AsDateTime);

	    // Adjust hours
        if FieldByName('TUGIO').IsNull then
            FieldByName('TUGIO').AsDateTime := 0;

	    dengio := Frac(FieldByName('DENGIO').AsDateTime);
        if dengio <> 0 then
        	FieldByName('DENGIO').AsDateTime := dengio
        else
        	FieldByName('DENGIO').AsDateTime := 1;

        if FieldByName('TUGIO').AsDateTime > FieldByName('DENGIO').AsDateTime then
        begin
        	ErrMsg('Giờ khuyến mãi không hợp lệ.');
        	Abort;
        end;
	end;
    DataMain.AllocSCT('KM', QrKM);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMBeforeEdit(DataSet: TDataSet);
begin
    if not mTrigger then
    	exIsChecked(DataSet);

	exValidClosing(QrKM.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrKMBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** End: Mestaer DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTBeforeOpen(DataSet: TDataSet);
begin
	(DataSet as TADOQuery).Parameters[0].Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTBeforePost(DataSet: TDataSet);
begin
	if not mTrigger then
        with QrCT do
        begin
            if BlankConfirm(QrCT, ['MAVT']) then
                Abort;

            if FieldByName('LK_TENVT').AsString = '' then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL1);
                Abort;
            end;

            if FieldByName('LK_GIABAN').AsFloat <= 0 then
            begin
                InvalidMsg(FieldByName('GIABAN'));
                Abort;
            end;
        end;

    with DataSet do
        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCT2BeforeInsert(DataSet: TDataSet);
begin
    if QrCT.IsEmpty then
        Abort;

    QrCT.CheckBrowseMode;
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCT2BeforePost(DataSet: TDataSet);
begin
    if not mTrigger then
        with DataSet do
        begin
            if QrKM.FieldByName('LCT').AsString = 'QT' then
            begin
                if BlankConfirm(DataSet, ['MAVT', 'SOLUONG']) then
                    Abort;

                if FieldByName('LK_TENVT').AsString = '' then
                begin
                    ErrMsg(RS_ITEM_CODE_FAIL1);
                    Abort;
                end;
            end;
        end;

    with DataSet do
        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrCT.FieldByName('KHOA').Value;
            FieldByName('KHOACT').Value := QrCT.FieldByName('KHOACT').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT2'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCT2MAVTChange(Sender: TField);
begin
    exDotMavt(4, QrDMHH, Sender);
    with QrCT2 do
    	FieldByName('DONGIA').AsFloat := FieldByName('LK_GIABAN').AsFloat;

	GrCTKM.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTAfterScroll(DataSet: TDataSet);
begin
    with QrCT2 do
    begin
        Filter := Format('KHOACT=%s', [TGuidEx.ToQuotedString(QrCT.FieldByName('KHOACT'))]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
    EmptyDataset(QrCT2);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTGIABANChange(Sender: TField);
var
    s: String;
begin
    if QrCT2.IsEmpty then
        Exit;

    s := QrKM.FieldByName('LCT').AsString;
    if not ((s = 'GG') or (s = 'KM')) then
        Exit;

    if QrKM.FieldByName('LCT').AsString = 'GG' then
        s := 'GIABAN'
    else
        s := 'TL_CK';
    with QrCT2 do
    begin
        CheckBrowseMode;
        First;
        while not eof do
        begin
            Edit;
            QrCT2GIABANChange(QrCT2.FieldByName(s));
            Next;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTHDBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;

    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTHDBeforeInsert(DataSet: TDataSet);
begin
    SetEditState(QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTHDBeforePost(DataSet: TDataSet);
begin
    if not mTrigger then
        with DataSet do
        begin
            if BlankConfirm(DataSet, ['TL_CK']) then
                Abort;
        end;

    with DataSet do
        if State in [dsInsert] then
            FieldByName('KHOA').Value := QrKM.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTMAVTChange(Sender: TField);
begin
    if IsDotSelect(Sender.AsString) = 0 then
        with QrCT do
        begin
            FieldByName('GIABAN').AsFloat := FieldByName('LK_GIABAN').AsFloat;
            FieldByName('TL_LAI').AsFloat := FieldByName('LK_TL_LAI').AsFloat;

            GrDetail.InvalidateCurrentRow;
        end
    else
        exDotMavt(4, QrDMHH, Sender, 'GIABAN>0');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.QrCTMAVTValidate(Sender: TField);
begin
    if IsDuplicateCode(QrCT, Sender) then
        Abort;
end;

    (*
    **  Others events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.GrBrowseDblClick(Sender: TObject);
begin
    if QrKM.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if PgMain.ActivePageIndex = 1 then
    	Status2.SimpleText := exRecordCount(QrCT,Filter2)
    else
   		Status.SimpleText := exRecordCount(QrKM, Filter);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        with QrCTHD do
        begin
            Close;
            Open;
        end;

        with QrCT2 do
        begin
            Close;
            Open;
        end;
	    Screen.Cursor := crDefault;
        SetReadOnlyField;
	    try
    	    CbNgay.SetFocus;
	    except
    		Perform(WM_NEXTDLGCTL, 0, 0);
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CbLCTNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsKM);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdDiembanExecute(Sender: TObject);
begin
	with QrKM do
    Begin
  		Application.CreateForm(TFrmKhuyenmai2, FrmKhuyenmai2);
        FrmKhuyenmai2.Execute(
            TGuidField(FieldByName('KHOA')).AsGuid,
            FieldByName('CHECKED').AsBoolean,
            FieldByName('SCT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdDsNCCExecute(Sender: TObject);
var
    s, sMAVT: String;
begin
    if not FrmChonDsNCC.Get(s) then
    	Exit;

    with TADOQuery.Create(Nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select MAVT from DM_VT_FULL where isnull(GIABAN, 0) > 0 and MADT in (select * from dbo.fnS_Table(:MADT))';
        Parameters[0].Value := s;
        Open;
        while not Eof do
        begin
            sMAVT := Fields[0].AsString;
            with QrCT do
            if not Locate('MAVT', sMAVT, []) then
            begin
                Append;
                FieldByName('MAVT').AsString := sMAVT;
                Post;
            end;
            Next;
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdDsNhomExecute(Sender: TObject);
var
    s, sMavt: String;
    n: Integer;
begin
    if not FrmChonDsNhom.Get(n, s) then
    	Exit;

    with TADOQuery.Create(Nil) do
    begin
        Connection := DataMain.Conn;
        LockType := ltReadOnly;
        SQL.Text := 'select MAVT from DM_VT where isnull(GIABAN, 0) > 0 ';
        case n of
        0:
            SQL.Add(' and MANGANH in (select * from dbo.fnS_Table(:DSMA))');
        1:
            SQL.Add(' and MANHOM in (select * from dbo.fnS_Table(:DSMA))');
        2:
            SQL.Add(' and MANHOM2 in (select * from dbo.fnS_Table(:DSMA))');
        end;
        Parameters[0].Value := s;
        Open;
        while not Eof do
        begin
            sMAVT := Fields[0].AsString;
            with QrCT do
            if not Locate('MAVT', sMAVT, []) then
            begin
                Append;
                FieldByName('MAVT').AsString := sMAVT;
                Post;
            end;
            Next;
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai3.CmdEmptyExecute(Sender: TObject);
begin
    if not YesNo(RS_XOA_CHITIET) then
        Exit;

    EmptyDataset(QrCT);
end;

    (*
    ** End: Others
    *)


end.
