﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Kiemke;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi,
  isPanel, isDb, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, RzLaunch, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmKiemke = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    BtImport: TToolButton;
    SepChecked: TToolButton;
    PopMaster: TAdvPopupMenu;
    QrDM_QUAYKE: TADOQuery;
    QrNXQUAYKE: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    N2: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    PaTotal: TPanel;
    ImgTotal: TImage;
    vlTotal1: TisTotal;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdRefresh: TAction;
    CmdSwitch: TAction;
    CmdFilter: TAction;
    CmdDel: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    CmdClearFilter: TAction;
    CmdImportExcel: TAction;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrNXNGAY2: TDateTimeField;
    Label4: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    CmdSapthutu: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdEmptyDetail: TAction;
    N3: TMenuItem;
    Xachititchngt1: TMenuItem;
    RzLauncher: TRzLauncher;
    CmdImport2: TAction;
    PopIm: TAdvPopupMenu;
    Lydliutthitbkimkho1: TMenuItem;
    QrCTQD1: TIntegerField;
    QrCTB1: TBooleanField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    PopIn: TAdvPopupMenu;
    Phiuvnchuynnib1: TMenuItem;
    MenuItem3: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXLOC: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCsv: TADOQuery;
    N4: TMenuItem;
    LydliutfileExcel1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    QrCTLOC: TWideStringField;
    CmdImportTxt: TAction;
    Lydliutfiletext1: TMenuItem;
    N5: TMenuItem;
    ItemObsolete: TMenuItem;
    CmdCheckton: TAction;
    N6: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    N7: TMenuItem;
    heogibnl1: TMenuItem;
    CmdExportDataGrid: TAction;
    XutdliutliraExcel1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    XutdliutliraExcel2: TMenuItem;
    CmdExBarcode: TAction;
    ToolButton5: TToolButton;
    ToolButton10: TToolButton;
    CmdImportExcelTemplate: TAction;
    N10: TMenuItem;
    LydliutfileExcel2: TMenuItem;
    ivfileExcelmu1: TMenuItem;
    QrLYDO_KKE: TADOQuery;
    QrNXLK_LYDO: TWideStringField;
    QrNXGhiChu: TWideMemoField;
    QrNXNguoiGiao: TWideStringField;
    QrNXNguoiNhan: TWideStringField;
    QrNXThanhTien: TFloatField;
    QrNXThanhTienSi: TFloatField;
    QrNXThanhTienLe: TFloatField;
    QrNXNguoiKiemKe: TWideStringField;
    QrNXSoLuong: TFloatField;
    QrCTCALC_DonGiaThamKhaoHop: TFloatField;
    QrNXThanhToan: TFloatField;
    QrCTSoLuongHop: TFloatField;
    QrCTSoLuongLe: TFloatField;
    QrCTDonGiaThamKhao: TFloatField;
    QrCTDonGiaHop: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTDonGiaSi: TFloatField;
    QrCTThanhTienSi: TFloatField;
    QrCTDonGiaLe: TFloatField;
    QrCTThanhTienLe: TFloatField;
    QrCTHanSuDung: TDateTimeField;
    QrCTSoLuong: TFloatField;
    QrCTDonGia: TFloatField;
    QrCTGhiChu: TWideStringField;
    QrCTLK_DvtHop: TWideStringField;
    QrCTLK_Dvt: TWideStringField;
    DBEdit2: TwwDBEdit;
    Label8: TLabel;
    QrCTLK_Tenvt: TWideStringField;
    CbLyDo: TDbLookupComboboxEh2;
    DsLYDO_KKE: TDataSource;
    EdMAKHO: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    CbbQuayKe: TDbLookupComboboxEh2;
    DsDM_QUAYKE: TDataSource;
    DBMemoEh1: TDBMemoEh;
    QrCTThanhToan: TFloatField;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    EdTriGiaTT: TDBNumberEditEh;
    QrCTLK_MaNoiBo: TWideStringField;
    N11: TMenuItem;
    Khngngi1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);

    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure BtImportClick(Sender: TObject);
    procedure CmdImport2Execute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure CmdImportTxtExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure CmdImportExcelTemplateExecute(Sender: TObject);
    procedure QrCTThanhTienChange(Sender: TField);
    procedure QrCTThanhToanChange(Sender: TField);
    procedure CbKhoHangDropDown(Sender: TObject);
    procedure CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
  private
  	mLCT: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;

    procedure OpenDetail;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmKiemke: TFrmKiemke;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, IniFiles, GuidEx, isCommon,
    ChonDsma, isLib, wwStr, ShellApi, isFile, Sapthutu, ImportExcel,
  CheckTonkho, OfficeData, InlabelChungtu;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_KIEMKE';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.Execute;
begin
	mLCT := 'KKE';
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    N4.Visible := CmdImportExcel.Visible;

    // Initial
  	mTrigger := False;
    mObsolete := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormShow(Sender: TObject);
begin
	Wait(PREPARING);
    InitFmtAndRoud(mLct);

    OpenDataSets([DataMain.QrDMVT, DataMain.QrDMKHO, QrDM_QUAYKE, QrLYDO_KKE]);
    SetDisplayFormat(DataMain.QrDMVT, sysCurFmt);
    SetDisplayFormat(DataMain.QrDMVT,['TyLeLai'], sysPerFmt);

    with QrNX do
    begin
		SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['SoLuong'], ctQtyFmt);
	    TFloatField(FieldByName('NGAY2')).DisplayFormat := ShortDateFormat + ' HH:SS';
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SoLuong'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DonGia', 'DonGiaSi', 'DonGiaLe'], ctPriceFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDM_QUAYKE, QrLYDO_KKE]);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from KIEMKE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from KIEMKE_CT a, DM_VT_FULL b where a.KHOA = KIEMKE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from KIEMKE_CT where KHOA = KIEMKE.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	DataMain.QrDMVT.Requery;
    DataMain.QrDMKHO.Requery;
    QrDM_QUAYKE.Requery;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmCheckTonkho, FrmCheckTonkho);
    with QrCT do
	    if FrmCheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('LK_Tenvt').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdExBarcodeExecute(Sender: TObject);
begin
    CmdSave.Execute;
	with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdPrintExecute(Sender: TObject);
var
	n: Integer;
begin
	CmdSave.Execute;

	n := (Sender as TComponent).Tag;
    ShowReport(Caption, FORM_CODE + '_' + IntToStr(n),
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdImport2Execute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	if not YesNo('Thiết bị phải đang ở tình trạng gởi dữ liệu. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'ConfigZ.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Remove current data file
    s := sysTempPath + 'upload.txt';
    DeleteFile(s);

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Up.exe';
		// Data_Read <File name>,<Upload via>,<COM Port>,<Baud rate>,<Save mode>,<Add CR character>,
		// <Add LF character>,<Show error>,<Show data>,<Show dialog>,<Keep online>,<Polling time>
        Parameters := s + ',2,' + IntToStr(n) + ',1,1,1,1,0,0,0,0,2';
        Launch;
    end;
    Screen.Cursor := crDefault;

	if not FileExists(s) then
		Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SoLuong').AsFloat := x;

            if FieldByName('LK_Tenvt').AsString = '' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
            //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
            //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;
    Refresh;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'Softz.log';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT', s, []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        QrCT.Append;
                        QrCT.FieldByName('MAVT').AsString := s;
                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';

procedure TFrmKiemke.CmdImportTxtExecute(Sender: TObject);
var
	i, n: Integer;
	s: String;
    x: Extended;
    ls, ps: TStrings;
begin
	// Format confirm
	if not YesNo(FORMAT_CONFIRM) then
        Exit;

    // Get file
    s := isGetOpenFileName('Txt;Csv;All');
	if s = '' then
    	Exit;

    // Load data
    ls := TStringList.Create;
    ps := TStringList.Create;
    ls.LoadFromFile(s);

    // Smart skip header
    if ls.Count > 0 then
    begin
    	strBreakApart(ls[0], ',', ps);
        if SameText(ps[0], 'MA') then
        	ls.Delete(0);
    end;

    StartProgress(ls.Count, 'Import');
    i := 0;
    n := 0;
    x := 0.0;
    while i < ls.Count do
    begin
    	strBreakApart(ls[i], ',', ps);
        if ps.Count < 2 then
        begin
        	Inc(i);
            Continue;
        end;

        try
	        s := ps[0];
    	    x := StrToFloat(ps[1]);
        except
            Inc(i);
            IncProgress;
            Continue;
        end;

        if TrimLeft(TrimRight(s)) = '' then
        begin
        	Inc(i);
            Continue;
        end;

        SetProgressDesc(s);
		IncProgress;

		with QrCT do
        begin
        	Append;
            FieldByName('MAVT').AsString := s;
            FieldByName('SoLuong').AsFloat := x;

            if FieldByName('LK_Tenvt').AsString = '' then
            begin
                Cancel;
                Inc(i);
                Continue;
            end else
            begin
    //            mTrigger := True;
                try
                    Post;
                    ls.Delete(i);
                    Inc(n);
                except
                    Cancel;
                    Inc(i);
                end;
    //            mTrigger := False;
            end;
		end;
    end;
	StopProgress;

	if n > 0 then
    	Msg(Format(RS_IM_NUM_REC, [n]))
	else
    	Msg(RS_IM_NONE);

	// Log process
    if ls.Count > 0 then
    begin
    	ErrMsg(Format(RS_IM_ERR_REC, [ls.Count]));
        s := sysTempPath + 'SoftzLog.txt';
        ls.SaveToFile(s);
		ShellExecute(Handle, Nil, PChar(s), Nil, Nil, SW_NORMAL);
    end;

    // Done
    ls.Free;
    ps.Free;

    with QrCT do
    begin
    	EnableControls;
    	CheckBrowseMode;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
	end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn.Enabled := (not bEmpty) and (not bDeleted);
    BtImport.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      	:= d;
		FieldByName('NGAY2').AsDateTime         := d;
		FieldByName('LCT').AsString           	:= mLCT;
		FieldByName('MAKHO').AsString       	:= sysDefKho;
        FieldByName('LOC').AsString             := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MAKHO', 'LYDO']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
    DataMain.AllocSCT(mLCT, QrNX);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Detail DB
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforePost(DataSet: TDataSet);
begin
	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_Tenvt').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;
        
    vlTotal1.Keep;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('CALC_DonGiaThamKhaoHop').AsFloat :=
            FieldByName('DonGiaThamKhao').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DonGia' then
                FieldByName('DonGiaHop').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DonGiaHop' then
                FieldByName('DonGia').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE : Double;
    Qd1: Integer;
begin
	exDotMavt(3, Sender);
    
    // Update referenced fields
    with QrCT do
    begin
        gia := 0; giaSI := 0; giaLE := 0;
        with DataMain.spCHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@MABH').Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DonGia').AsFloat;
                giaSI := FieldByName('DonGiaSi').AsFloat;
                giaLE := FieldByName('DonGiaLe').AsFloat;
                Qd1 := FieldByName('QD1').AsInteger;

            end;
        end;
        FieldByName('QD1').AsInteger := Qd1;

        FieldByName('DonGiaSi').AsFloat := giaSI;
        FieldByName('DonGiaLe').AsFloat := giaLE;
        FieldByName('DonGiaThamKhao').AsFloat := gia;
        FieldByName('DonGia').AsFloat := gia;
    end;

	GrDetail.InvalidateCurrentRow;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
        ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SoLuong').AsFloat);
		ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat);
        ColumnByName('ThanhTienSi').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSi').AsFloat);
        ColumnByName('ThanhTienLe').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienLe').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.BtImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
begin
   (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := '';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbKhoHangDropDown(Sender: TObject);
begin
    (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.OpenDetail;
begin
    Screen.Cursor := crSQLWait;
    with QrCT do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SoLuong' then
            begin
                sl  := FieldByName('SoLuong').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;
            end
            else if (Sender.FullName = 'SoLuongHop') or (Sender.FullName = 'SoLuongLe') then
            begin
                sl2 := FieldByName('SoLuongHop').AsFloat;
                sl2le := FieldByName('SoLuongLe').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SoLuong').AsFloat * FieldByName('DonGia').AsFloat, ctCurRound);

        FieldByName('ThanhTienSi').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaSi').AsFloat, ctCurRound);
        FieldByName('ThanhTienLe').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaLe').AsFloat, ctCurRound);
        FieldByName('ThanhTien').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTThanhTienChange(Sender: TField);
begin
    with QrCT do
    begin
         FieldByName('ThanhToan').AsFloat := FieldByName('ThanhTien').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTThanhToanChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterEdit(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.QrCTAfterInsert(DataSet: TDataSet);
begin
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKiemke.CmdImportExcelTemplateExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_CHUNGTU_EXCEL');
end;

end.
