﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DieuchinhXuat;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi, System.Variants,
  isDb, isPanel, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, Wwdotdot, Wwdbcomb,
  AdvEdit, DBAdvEd;

type
  TFrmDieuchinhXuat = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    StatusBar: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    PaTotal: TPanel;
    EdTriGiaTT: TwwDBEdit;
    EdTriGiaHH: TwwDBEdit;
    EdCK: TwwDBEdit;
    EdTienVAT: TwwDBEdit;
    EdTLCK: TwwDBEdit;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTSOLUONG: TFloatField;
    QrCTGIANHAP: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTSOTIEN: TFloatField;
    QrCTTL_CK: TFloatField;
    QrCTCHIETKHAU: TFloatField;
    QrCTTHUE_SUAT: TFloatField;
    QrCTTIEN_THUE: TFloatField;
    QrCTLK_TENVT: TWideStringField;
    QrCTDVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXSODDH: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_SO: TWideStringField;
    QrNXHOADON_NGAY: TDateTimeField;
    QrNXNG_GIAO: TWideStringField;
    QrNXTL_CK: TFloatField;
    QrNXCHIETKHAU: TFloatField;
    QrNXTHUE_SUAT: TFloatField;
    QrNXTHUE: TFloatField;
    QrNXCL_THUE: TFloatField;
    QrNXSOTIEN: TFloatField;
    QrNXCL_SOTIEN: TFloatField;
    QrNXTHANHTOAN: TFloatField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    QrCTGIATK: TFloatField;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXCHIETKHAU_MH: TFloatField;
    DBEdit1: TwwDBEdit;
    QrNXNG_NHAN: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXLK_TENDT: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXPTTT: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXTENKHO: TWideStringField;
    QrNXCO_HD: TBooleanField;
    QrNXCK_BY: TIntegerField;
    QrNXPRINTED: TBooleanField;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    ImgTotal: TImage;
    Bevel1: TBevel;
    Label24: TLabel;
    Label32: TLabel;
    Label18: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    Label19: TLabel;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    CmdEmptyDetail: TAction;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    vlTotal1: TisTotal;
    QrNXSOLUONG: TFloatField;
    QrCTGHICHU: TWideStringField;
    QrNXTC_SOTIEN: TFloatField;
    QrNXSCT2: TWideStringField;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label11: TLabel;
    EdSOHDON: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label31: TLabel;
    CbMAKHO: TwwDBLookupCombo;
    CbKHO: TwwDBLookupCombo;
    Label5: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    LbNHAPCUA: TLabel;
    CbMAKH: TDBAdvEdit;
    CbTENKH: TwwDBEdit;
    Label8: TLabel;
    DBEdit2: TwwDBEdit;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    TntLabel1: TLabel;
    EdSCT2: TwwDBEdit;
    QrCTSOLUONG_DC: TFloatField;
    QrCTDONGIA_DC: TFloatField;
    QrCTSOTIEN_DC: TFloatField;
    CmdCapnhatct: TAction;
    QrPHIEUXUAT: TADOQuery;
    QrNXLOAITHUE: TWideStringField;
    QrCTLOAITHUE: TWideStringField;
    QrNXDGIAI: TWideMemoField;
    QrNXLCT: TWideStringField;
    PaHoadon: TisPanel;
    PaHoadon2: TPanel;
    TntLabel2: TLabel;
    TntLabel3: TLabel;
    TntLabel4: TLabel;
    TntLabel7: TLabel;
    TntLabel8: TLabel;
    EdThueVAT: TwwDBEdit;
    CbLoaithue: TwwDBLookupCombo;
    EdSOHD: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLK_HOADON_SO: TWideStringField;
    QrNXLK_HOADON_SERI: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    Label7: TLabel;
    DBEdit6: TwwDBEdit;
    TntLabel9: TLabel;
    QrNXHAN_TTOAN: TIntegerField;
    QrPHIEUXUATHOADON_SO: TWideStringField;
    QrPHIEUXUATHOADON_SERI: TWideStringField;
    QrPHIEUXUATMAKH: TWideStringField;
    QrPHIEUXUATMAKHO: TWideStringField;
    QrPHIEUXUATLOAITHUE: TWideStringField;
    QrPHIEUXUATTHUE_SUAT: TFloatField;
    QrPHIEUXUATTL_CK_NX: TFloatField;
    QrPHIEUXUATHAN_TTOAN: TIntegerField;
    QrPHIEUXUATNG_GIAO: TWideStringField;
    QrPHIEUXUATNG_NHAN: TWideStringField;
    QrPHIEUXUATCO_HD: TBooleanField;
    QrPHIEUXUATDGIAI: TWideMemoField;
    QrPHIEUXUATMAVT: TWideStringField;
    QrPHIEUXUATTENVT: TWideStringField;
    QrPHIEUXUATDVT: TWideStringField;
    QrPHIEUXUATSOLUONG: TFloatField;
    QrPHIEUXUATDONGIA: TFloatField;
    QrPHIEUXUATSOTIEN: TFloatField;
    QrPHIEUXUATGIANHAP: TFloatField;
    QrPHIEUXUATTL_CK: TFloatField;
    QrPHIEUXUATGHICHU: TWideStringField;
    CmdListRefesh: TAction;
    QrPHIEUXUATGIABAN: TFloatField;
    QrPHIEUXUATGIASI: TFloatField;
    QrPHIEUXUATTL_LAI: TFloatField;
    QrPHIEUXUATTENDT: TWideStringField;
    CmdSapthutu: TAction;
    N4: TMenuItem;
    Splithtmthng1: TMenuItem;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrNXTL_CKChange(Sender: TField);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure QrCTTL_CKChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrNXSCT2Change(Sender: TField);
    procedure QrNXSCT2Validate(Sender: TField);
    procedure QrPHIEUXUATBeforeOpen(DataSet: TDataSet);
    procedure CmdCapnhatctExecute(Sender: TObject);
    procedure QrCTSOLUONG_DCChange(Sender: TField);
    procedure QrCTSOTIEN_DCChange(Sender: TField);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrNXAfterEdit(DataSet: TDataSet);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
  private
	mLCT: String;
	mCanEdit, giaCothue: Boolean;
    
    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

    function  OpenQueryRef: Boolean;
  public
	procedure Execute (r: WORD);
  end;

var
  FrmDieuchinhXuat: TFrmDieuchinhXuat;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    ChonDsma, isLib, Sapthutu;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_XUAT_DC';
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.Execute;
begin
	mLCT := 'XDC';

	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    ShowModal;
end;

	(*
    ** Form events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrNX;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

// Params
    giaCothue := FlexConfigBool('Price', 'BP Include Tax');

    // Initial
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init1;
    TMyForm(Self).LoadIcon('_IDI_ISSUE');

	// Open database
	Wait(PREPARING);
    InitFmtAndRoud(mLct);

    DataMain.QrDMLOAITHUE.Open;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetDisplayFormat(QrNX, ['THUE_SUAT', 'TL_CK'], sysPerFmt);
        SetDisplayFormat(QrNX, ['SOLUONG'], ctQtyFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG', 'SOLUONG_DC'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DONGIA', 'DONGIA_SI', 'DONGIA_LE'], ctPriceFmt);
	    SetDisplayFormat(QrCT, ['TL_CK'], sysPerFmt);
    end;

    with QrPHIEUXUAT do
    begin
	    SetDisplayFormat(QrPHIEUXUAT, sysCurFmt);
    	SetDisplayFormat(QrPHIEUXUAT, ['SOLUONG'], sysCurFmt);
	    SetDisplayFormat(QrPHIEUXUAT, ['TL_CK', 'TL_LAI'], sysPerFmt);
    end;

    // DicMap + Customize Grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
    ** Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        OpenQueryRef;

        Screen.Cursor := crDefault;
        
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

	(*
    ** Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
            SQL.Add('order by NGAY, SCT');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
    	QrDMLOAITHUE.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdSapthutuExecute(Sender: TObject);
begin
    CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
        	EdSCT2.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdTotalExecute(Sender: TObject);
var
	mTGHH, mCkmh : Double;
    mSt, mTl, mCk : Double;
    bm : TBytes;
begin
    with QrCT do
    begin
        mTGHH := 0;
        mCkmh := 0;

    	bm := BookMark;
        DisableControls;
        First;

        while not Eof do
        begin
	    	mSt := FieldByName('SOTIEN_DC').AsFloat;
    		mTl := FieldByName('TL_CK').AsFloat;
       		mCk := Round (mSt * mTl / 100);

            if FieldByName('CHIETKHAU').AsFloat <> mCk then
            begin
            	Edit;
		    	FieldByName('CHIETKHAU').AsFloat := mCk;
                Post;
            end;

        	mTGHH := mTGHH + mSt;
        	mCkmh := mCkmh + mCk;
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;

	with QrNX do
    begin
	    if State in [dsBrowse] then
            Edit;;

        // Chiet khau mat hang
        FieldByName('CHIETKHAU_MH').AsFloat := mCkmh;

        // Tri gia hang hoa
        FieldByName('SOTIEN').AsFloat := exVNDRound(mTGHH, sysCurRound);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdCapnhatctExecute(Sender: TObject);
begin
    if not YesNo(Format('Cập nhật lại danh sách mặt hàng từ phiếu "%s". Tiếp tục?',
        [QrNX.FieldByName('SCT2').AsString]), 1) then
        Exit;

    Wait(PROCESSING);

    // Clean up
    with QrCT do
    begin
        DisableControls;
        First;

        DeleteConfirm(False);
        while not Eof do
            Delete;
        DeleteConfirm(False);
    end;

    with TADOQuery.Create(Nil) do
    begin
        Connection  := DataMain.Conn;
        LockType    := ltReadOnly;
        SQL.Text    :=  'select b.MAVT, b.SOLUONG, b.DONGIA, b.SOTIEN, b.GIANHAP, b.TL_CK, b.LOAITHUE, b.THUE_SUAT, b.TIEN_THUE, b.GHICHU ' +
                        ' from T_CHUNGTU a, CHUNGTU_CT b ' +
                        ' where a.KHOA=b.KHOA and SCT=''' + QrNX.FieldByName('SCT2').AsString + '''' +
                        ' order by b.STT';
        Open;
        First;
        while not Eof do
        begin
            QrCT.Append;

    		QrCT.FieldByName('KHOA').Value          := QrNX.FieldByName('KHOA').Value;
            mTrigger := True;
            QrCT.FieldByName('MAVT').AsString       := FieldByName('MAVT').AsString;
            mTrigger := False;
            QrCT.FieldByName('SOLUONG').AsFloat     := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('DONGIA').AsFloat      := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('SOLUONG_DC').AsFloat  := FieldByName('SOLUONG').AsFloat;
            QrCT.FieldByName('DONGIA_DC').AsFloat   := FieldByName('DONGIA').AsFloat;
            QrCT.FieldByName('SOTIEN').AsFloat      := FieldByName('SOTIEN').AsFloat;
            QrCT.FieldByName('GIANHAP').AsFloat      := FieldByName('GIANHAP').AsFloat;
            QrCT.FieldByName('TL_CK').AsFloat       := FieldByName('TL_CK').AsFloat;
            QrCT.FieldByName('LOAITHUE').AsString   := FieldByName('LOAITHUE').AsString;
            QrCT.FieldByName('THUE_SUAT').AsFloat   := FieldByName('THUE_SUAT').AsFloat;
            QrCT.FieldByName('TIEN_THUE').AsFloat   := FieldByName('TIEN_THUE').AsFloat;
            QrCT.FieldByName('GHICHU').AsString     := FieldByName('GHICHU').AsString;

            QrCT.Post;
            Next;
        end;

        Close;
        Free;
    end;

    QrCT.EnableControls;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and exCheckLoc(QrNX, False);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdPrint.Enabled := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdFilterCom.Checked := fStr <> '';

    CmdEmptyDetail.Enabled := not bEmptyCT;
    CmdCapnhatct.Enabled := mCanEdit and (QrNX.FieldByName('SCT2').AsString <> '');
    EdSCT2.Enabled := bEmptyCT;

    CmdSwitch.Enabled := n = 1;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
    with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      := d;
		FieldByName('LCT').AsString         := mLCT;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('CO_HD').AsBoolean      := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXAfterEdit(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);

    if PgMain.ActivePageIndex = 1 then
        OpenQueryRef;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
	with DataSet do
    begin
		// Validate: da co phieu thu / chi
    	if FieldByName('TC_SOTIEN').AsFloat <> 0 then
	    begin
    	    ErrMsg(RS_DA_THUCHI);
        	Abort;
	    end;

    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['NGAY', 'SCT2']) then
    	    Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXTL_CKChange(Sender: TField);
begin
	with QrNX do
	    FieldByName('CHIETKHAU').AsFloat := exVNDRound(
        	(FieldByName('SOTIEN').AsFloat -
            FieldByName('CHIETKHAU_MH').AsFloat) *
        	FieldByName('TL_CK').AsFloat / 100, sysCurRound);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXSOTIENChange(Sender: TField);
var
	mSt, mCk, mCkmh, mThue, mTt, mTs: Double;
begin
	if mTrigger then
    	Exit;
    mTrigger := True;

	with QrNX do
    begin
    	mTs   := FieldByName('THUE_SUAT').AsFloat;
    	mSt   := FieldByName('SOTIEN').AsFloat;
    	mCkmh := FieldByName('CHIETKHAU_MH').AsFloat;
    	mCk   := exVNDRound((mSt - mCkmh) * FieldByName('TL_CK').AsFloat / 100, sysCurRound);
		mTt   := mSt - mCk - mCkmh;

        if giaCothue then
			mThue := exVNDRound((mTt * mTs) / (100 + mTs), sysCurRound)
        else
        begin
        	mThue := exVNDRound(mTt * mTs / 100, sysCurRound);
            mTt := mTt + mThue + FieldByName('CL_THUE').AsFloat;
        end;

    	FieldByName('CHIETKHAU').AsFloat := mCk;
    	FieldByName('THUE').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mTt;
	end;

    mTrigger := False;
end;

(*==============================================================================
**  Detail DB
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrNX, ['MAVT']) then
    		Abort;

		if FieldByName('TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;
        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    SetEditState(QrNX);

    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
//    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrPHIEUXUATBeforeOpen(DataSet: TDataSet);
begin
    QrPHIEUXUAT.Parameters[0].Value := QrNX.FieldByName('SCT2').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTSOLUONG_DCChange(Sender: TField);
begin
	with QrCT do
    	FieldByName('SOTIEN_DC').AsFloat :=
	    	FieldByName('SOLUONG_DC').AsFloat *
    		FieldByName('DONGIA_DC').AsFloat;

    vlTotal1.Update;
	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTSOTIEN_DCChange(Sender: TField);
begin
	QrCTTL_CKChange(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTMAVTValidate(Sender: TField);
var
    s: String;
begin
    if mTrigger then
        Exit;

    s := Sender.AsString;
    if s = '' then
        Exit;

    if IsDotSelect(s) > 0 then
        Exit;

    with QrPHIEUXUAT do
    begin
        Filter := '';
        if not Locate('MAVT', s, []) then
        begin
            ErrMsg(RS_ITEM_CODE_FAIL1);
            Abort;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTMAVTChange(Sender: TField);
begin
    if mTrigger then
        Exit;

    exDotMavt(1, QrPHIEUXUAT, Sender);

    // Update referenced fields
    with QrCT do
        if QrPHIEUXUAT.Locate('MAVT', Sender.AsString, []) then
        begin
            FieldByName('SOLUONG').AsFloat  := QrPHIEUXUAT.FieldByName('SOLUONG').AsFloat;
            FieldByName('DONGIA').AsFloat   := QrPHIEUXUAT.FieldByName('DONGIA').AsFloat;
            FieldByName('SOLUONG_DC').AsFloat := QrPHIEUXUAT.FieldByName('SOLUONG').AsFloat;
            FieldByName('DONGIA_DC').AsFloat  := QrPHIEUXUAT.FieldByName('DONGIA').AsFloat;
            FieldByName('SOTIEN').AsFloat   := QrPHIEUXUAT.FieldByName('SOTIEN').AsFloat;
            FieldByName('GIANHAP').AsFloat   := QrPHIEUXUAT.FieldByName('GIANHAP').AsFloat;
            FieldByName('TL_CK').AsFloat    := QrPHIEUXUAT.FieldByName('TL_CK').AsFloat;
            FieldByName('GHICHU').AsString  := QrPHIEUXUAT.FieldByName('GHICHU').AsString
        end
        else
        begin
            FieldByName('SOLUONG').Clear;
            FieldByName('DONGIA').Clear;
            FieldByName('SOLUONG_DC').Clear;
            FieldByName('DONGIA_DC').Clear;
            FieldByName('SOTIEN').Clear;
            FieldByName('GIANHAP').Clear;
            FieldByName('TL_CK').Clear;
            FieldByName('GHICHU').Clear;
        end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXSCT2Validate(Sender: TField);
var
    s: String;
begin
    s := Sender.AsString;

    if s = '' then
        Exit;

    if not OpenQueryRef then
    begin
        ErrMsg('Lỗi nhập liệu. Số phiếu không hợp lệ.');
        Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrNXSCT2Change(Sender: TField);
begin
    with QrNX do
        if QrNX.FieldByName('SCT2').AsString <> '' then
        begin
            FieldByName('LK_HOADON_SO').AsString     := QrPHIEUXUAT.FieldByName('HOADON_SO').AsString;
            FieldByName('LK_HOADON_SERI').AsString        := QrPHIEUXUAT.FieldByName('HOADON_SERI').AsString;
            FieldByName('MAKH').AsString        := QrPHIEUXUAT.FieldByName('MAKH').AsString;
            FieldByName('MAKHO').AsString       := QrPHIEUXUAT.FieldByName('MAKHO').AsString;
            FieldByName('LOAITHUE').AsString    := QrPHIEUXUAT.FieldByName('LOAITHUE').AsString;
            FieldByName('THUE_SUAT').AsFloat    := QrPHIEUXUAT.FieldByName('THUE_SUAT').AsFloat;
            FieldByName('TL_CK').AsFloat        := QrPHIEUXUAT.FieldByName('TL_CK_NX').AsFloat;
            FieldByName('NG_GIAO').AsString     := QrPHIEUXUAT.FieldByName('NG_GIAO').AsString;
            FieldByName('NG_NHAN').AsString     := QrPHIEUXUAT.FieldByName('NG_NHAN').AsString;
            FieldByName('CO_HD').AsBoolean      := QrPHIEUXUAT.FieldByName('CO_HD').AsBoolean;
            FieldByName('DGIAI').AsString       := QrPHIEUXUAT.FieldByName('DGIAI').AsString;
            FieldByName('HAN_TTOAN').AsInteger  := QrPHIEUXUAT.FieldByName('HAN_TTOAN').AsInteger;
        end
        else
        begin
            FieldByName('LK_HOADON_SO').Clear;
            FieldByName('LK_HOADON_SERI').Clear;
            FieldByName('MAKH').Clear;
            FieldByName('MAKHO').Clear;
            FieldByName('LOAITHUE').Clear;
            FieldByName('THUE_SUAT').Clear;
            FieldByName('TL_CK').Clear;
            FieldByName('NG_GIAO').Clear;
            FieldByName('NG_NHAN').Clear;
            FieldByName('CO_HD').Clear;
            FieldByName('DGIAI').Clear;
            FieldByName('HAN_TTOAN').Clear;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.QrCTTL_CKChange(Sender: TField);
begin
	with QrCT do
    	FieldByName('CHIETKHAU').AsFloat := Round (
        	FieldByName('SOTIEN_DC').AsFloat *
            FieldByName('TL_CK').AsFloat / 100);
    vlTotal1.Update;
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	StatusBar.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDieuchinhXuat.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDieuchinhXuat.OpenQueryRef: Boolean;
begin
    with QrPHIEUXUAT do
    begin
        Close;
        Open;                 
        Result := not IsEmpty;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)    
procedure TFrmDieuchinhXuat.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX);
end;
end.
