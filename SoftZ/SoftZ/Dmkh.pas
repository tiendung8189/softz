﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmKh;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Mask, ExtCtrls,
  RzPanel, Grids, Wwdbigrd, Wwdbgrid, ToolWin, wwdbedit, Wwkeycb, Buttons,
  AdvEdit, wwcheckbox, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmDmKh = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ToolButton12: TToolButton;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    QrDanhmucPLOAI: TWideStringField;
    QrDanhmucDCHI: TWideStringField;
    QrDanhmucDTHOAI: TWideStringField;
    QrDanhmucFAX: TWideStringField;
    QrDanhmucMST: TWideStringField;
    QrDanhmucTENTK: TWideStringField;
    QrDanhmucMATK: TWideStringField;
    QrDanhmucSODU: TFloatField;
    QrDanhmucGHICHU: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucHAN_TTOAN: TIntegerField;
    QrDanhmucLK_PLOAI: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    EdTen: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    EdMa: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    PD2: TisPanel;
    Label1: TLabel;
    DBEdit4: TwwDBEdit;
    PD5: TisPanel;
    DBMemo1: TDBMemo;
    PD4: TisPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit6: TwwDBEdit;
    DBEdit7: TwwDBEdit;
    DBEdit8: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    QrDanhmucLOAI: TIntegerField;
    QrDanhmucMADT: TWideStringField;
    QrDanhmucTENDT: TWideStringField;
    QrDanhmucMAQG: TWideStringField;
    QrDanhmucMATINH: TWideStringField;
    QrDanhmucMAHUYEN: TWideStringField;
    QrQuocgia: TADOQuery;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrTinh: TADOQuery;
    QrTinhMATINH: TWideStringField;
    QrTinhMAQG: TWideStringField;
    QrTinhMA: TWideStringField;
    QrTinhTEN: TWideStringField;
    QrHuyen: TADOQuery;
    QrHuyenMAHUYEN: TWideStringField;
    QrHuyenMATINH: TWideStringField;
    QrHuyenMA: TWideStringField;
    QrHuyenTEN: TWideStringField;
    QrDanhmucSODU_NGAY: TDateTimeField;
    QrDanhmucSODU_LIMIT: TFloatField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    PD3: TisPanel;
    TntLabel4: TLabel;
    DBEdit10: TwwDBEdit;
    TntLabel5: TLabel;
    DBEdit11: TwwDBEdit;
    Panel1: TPanel;
    PaVIP: TPanel;
    PaNV: TPanel;
    QrDanhmucMANV: TWideStringField;
    QrDanhmucLOAI_VIP: TWideStringField;
    PaChuquan: TPanel;
    QrDanhmucMADT2: TWideStringField;
    QrDanhmucLK_TENDT2: TWideStringField;
    QrDanhmucEMAIL: TWideStringField;
    Label17: TLabel;
    wwDBEdit1: TwwDBEdit;
    N1: TMenuItem;
    CmdRefesh: TAction;
    QrDanhmucNGUNG_LIENLAC: TBooleanField;
    ItemLienlac: TMenuItem;
    PaList: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    EdSearch: TAdvEdit;
    CHECK_DTHOAI: TADOCommand;
    SpeedButton2: TSpeedButton;
    CmdDmtk: TAction;
    QrDanhmucLK_TENTK: TWideStringField;
    QrDanhmucLK_NGANHANG: TWideStringField;
    QrDanhmucLK_CHINHANH: TWideStringField;
    CmdExportDataGrid: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    wwCheckBox1: TwwCheckBox;
    QrDanhmucIdx: TAutoIncField;
    DsQuocgia: TDataSource;
    DsTinh: TDataSource;
    DsHuyen: TDataSource;
    cbbMAQG: TDbLookupComboboxEh2;
    EdMAQG: TDBEditEh;
    EdMATINH: TDBEditEh;
    EdMAHUYEN: TDBEditEh;
    cbbMAHUYEN: TDbLookupComboboxEh2;
    cbbTINH: TDbLookupComboboxEh2;
    CbMANV: TDbLookupComboboxEh2;
    EdMANV: TDBEditEh;
    EdMADT2: TDBEditEh;
    CbMADT: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    CbLOAI_VIP: TDbLookupComboboxEh2;
    QrDanhmucLK_MATINHNGAN: TWideStringField;
    QrDanhmucLK_MAHUYENNGAN: TWideStringField;
    CmdDmct: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbPhanLoaiNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdContactExecute(Sender: TObject);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucMAQGChange(Sender: TField);
    procedure QrDanhmucMATINHChange(Sender: TField);
    procedure CbTentinhBeforeDropDown(Sender: TObject);
    procedure CbTenHuyenBeforeDropDown(Sender: TObject);
    procedure QrDanhmucMADTChange(Sender: TField);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ItemLienlacClick(Sender: TObject);
    procedure QrDanhmucDTHOAIValidate(Sender: TField);
    procedure CmdDmtkExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure CmdDmctExecute(Sender: TObject);
    procedure cbbTINHDropDown(Sender: TObject);
    procedure cbbMAHUYENDropDown(Sender: TObject);
    procedure QrDanhmucMAHUYENChange(Sender: TField);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mClose, mObsolete: Boolean;
    mCodeLength, mFilter: Integer;
    mSQL, mPrefix, mSearch: String;
  public
  	function Execute(r: WORD; pClose: Boolean = True): Boolean;
  end;

var

  FrmDmKh: TFrmDmKh;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, isLib, Lienhe, isCommon, DmTK, OfficeData, HrData,
    DmnccCty;

{$R *.DFM}

const
	FORM_CODE = 'DM_KH';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmKh.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsDanhmuc.AutoEdit := mCanEdit;

    mRet := False;
    mClose := pClose;
    ShowModal;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    mTrigger := False;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);

    PaDetail.VertScrollBar.Position := 0;

    PD2.Collapsed := RegReadBool(Name, 'PD2');
    PD3.Collapsed := RegReadBool('PD3');
  	PD4.Collapsed := RegRead('PD4', True);
    PD5.Collapsed := RegRead('PD5', True);

    mAutoCode := FlexConfigBool(FORM_CODE, 'AutoCode');
    fixCode := FlexConfigBool(FORM_CODE, 'Fixed');
    mCodeLength := FlexConfigInteger(FORM_CODE, 'CodeLength', 0);
    mObsolete     := RegReadBool('Obsolete');
    mPrefix := FlexConfigString(FORM_CODE, 'Prefix', '');
    mFilter := 9;
    mSearch := '';
    mSQL := QrDanhmuc.SQL.Text;

//    if mCodeLength > 0 then
//        with QrDanhmuc.FieldByName('MADT') do
//        begin
//            DisplayWidth := mCodeLength;
//            Size := mCodeLength;
//        end
//	else
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.FormShow(Sender: TObject);
var
    bVIP, bNV: Boolean;
begin
    EdMa.ReadOnly := mAutoCode;
//	PD2.Visible := GetSysParam('BEGIN_BALANCE');
    PD2.Enabled := GetRights('SZ_DM_KH_CONGNO', False) <> R_DENY;
    bVIP := GetFuncState('SZ_DM_LOAIVIP_KH');
    bNV := GetFuncState('HR_PROFILE');

    if not bVIP then
    begin
        PaVIP.Visible := False;
        PD1.Height := PD1.Height - PaVIP.Height;
    end;

    if not bNV then
    begin
        PaNV.Visible := False;
        PD1.Height := PD1.Height - PaNV.Height;
    end;

    with DataMain do
        OpenDataSets([QrLOAI_KH_NCC, QrLOAI_VIP_SI, QrDM_KH_NCC, QrDMTK]);
	OpenDataSets([QrQuocgia, QrTinh, QrHuyen, HrDataMain.QrDMNV_DANGLAMVIEC]);
    SetDisplayFormat(QrDanhmuc, sysCurFmt);

    CmdRefesh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.ItemLienlacClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrDanhmuc, QrQuocgia, QrTinh, QrHuyen]);
    finally
    end;

	//Save state panel
    RegWrite(Name, ['PD2', 'PD3', 'PD4', 'PD5', 'Obsolete'],
        [PD2.Collapsed, PD3.Collapsed, PD4.Collapsed, PD5.Collapsed, mObsolete]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdDmctExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_DM_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmnccCty, FrmDmnccCty);
    FrmDmnccCty.Execute(r);
    DataMain.QrDM_KH_NCC.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdDmtkExecute(Sender: TObject);
var
    r: WORD;
    pMatk, pMadt: String;
begin
    with QrDanhmuc do
    begin
        pMadt := FieldByName('MADT').AsString;

        Application.CreateForm(TFrmDmTK, FrmDmTK);
        if FrmDmTK.Execute(R_FULL, pMatk, 'DT', pMadt, '', False) then
        begin
            DataMain.QrDMTK.Requery();

            Edit;
            FieldByName('MATK').AsString := pMatk;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdRefeshExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;

    mSearch := DataMain.StripToneMark(EdSearch.Text);
	with QrDanhmuc do
    begin
    	Close;
        if mSearch <> '' then
            sSQL := sSQL + ' and (dbo.fnStripToneMark(TENDT) like ''%'+ mSearch + '%'' or dbo.fnStripToneMark(DTHOAI) like ''%' + mSearch + '%'')';

        if not mObsolete then
            sSQL := sSQL + ' and (isnull(NGUNG_LIENLAC, 0) = 0)';

        SQL.Text := sSQL;
        SQL.Add(' order by 	MADT');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);

	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdContact.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.PopupMenu1Popup(Sender: TObject);
begin
    ItemLienlac.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
        if State in [dsInsert] then
        begin
            if not mAutoCode then
            begin
                if BlankConfirm(QrDanhmuc, ['MADT']) then
                    Abort;
            end
            else
                FieldByName('MADT').AsString :=
                    DataMain.AllocMADT(mCodeLength, 0, mPrefix);
        end;

        if fixCode then
            if LengthConfirm(QrDanhmuc, ['MADT']) then
                Abort;

        if BlankConfirm(QrDanhmuc, ['TENDT']) then
        	Abort;

	    SetNull(QrDanhmuc, ['MAQG', 'MATINH', 'MAHUYEN', 'DTHOAI']);
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DTTHOAI_EXISTS = 'Số điện thoại đã được sử dụng.';
procedure TFrmDmKh.QrDanhmucDTHOAIValidate(Sender: TField);
var
	s: String;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
    if s = '' then
        Exit;

    with CHECK_DTHOAI do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrDanhmuc.FieldByName('MADT').AsString;
            if Execute.RecordCount > 0 then
            begin
                ErrMsg(RS_DTTHOAI_EXISTS);
                Abort;
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.cbbMAHUYENDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrTinh.FieldByName('MATINH').AsString;
    QrHuyen.Filter := 'MATINH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.cbbTINHDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrQuocgia.FieldByName('MA').AsString;
    QrTinh.Filter := 'MAQG=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CbPhanLoaiNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CmdContactExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmLienhe, FrmLienhe);
    with QrDanhmuc do
        FrmLienhe.Execute(
            mCanEdit,
        	FieldByName('KHOA').AsInteger,
            FieldByName('MADT').AsString,
            FieldByName('TENDT').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('LOAI').AsInteger := 0;
        FieldByName('NGUNG_LIENLAC').AsBoolean := False;
        FieldByName('MAQG').AsString := '084';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucMADTChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucMAHUYENChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        EdMAHUYEN.Text := EdMAHUYEN.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucMAQGChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        FieldByName('MATINH').Clear;
        FieldByName('MAHUYEN').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.QrDanhmucMATINHChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        FieldByName('MAHUYEN').Clear;
        EdMATINH.Text := EdMATINH.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CbTentinhBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MAQG').AsString;
    with QrTinh do
    begin
        Filter := Format('MAQG=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKh.CbTenHuyenBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MATINH').AsString;
    with QrHuyen do
    begin
        Filter := Format('MATINH=''%s''', [s]);
    end;
end;

end.
