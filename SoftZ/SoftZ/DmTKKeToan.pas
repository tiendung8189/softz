﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmTKKeToan;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Mask, ExtCtrls,
  RzPanel, Grids, Wwdbigrd, Wwdbgrid, ToolWin, wwdbedit, Wwkeycb, Buttons,
  AdvEdit, wwcheckbox, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmDmTKKeToan = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton10: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    EdTen: TwwDBEdit;
    EdMa: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    PD5: TisPanel;
    DBMemo1: TDBMemo;
    QrTaiKhoanCap1: TADOQuery;
    QrTaiKhoanCap2: TADOQuery;
    QrTinhChat: TADOQuery;
    Panel1: TPanel;
    N1: TMenuItem;
    CmdRefesh: TAction;
    ItemLienlac: TMenuItem;
    PaList: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    EdSearch: TAdvEdit;
    CHECK_DTHOAI: TADOCommand;
    CmdDmtk: TAction;
    CmdExportDataGrid: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    wwCheckBox1: TwwCheckBox;
    cbbMATK_CAP1: TDbLookupComboboxEh2;
    cbbMATK_CAP2: TDbLookupComboboxEh2;
    cbbTINHCHAT: TDbLookupComboboxEh2;
    QrDanhmucMATK: TWideStringField;
    QrDanhmucTENTK: TWideStringField;
    QrDanhmucTENTK_TA: TWideStringField;
    QrDanhmucTINHCHAT: TWideStringField;
    QrDanhmucMATK_CAP1: TWideStringField;
    QrDanhmucMATK_CAP2: TWideStringField;
    QrDanhmucTINHTRANG: TBooleanField;
    QrDanhmucGHICHU: TWideStringField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    DsTinhChat: TDataSource;
    DsTaiKhoanCap1: TDataSource;
    DsTaiKhoanCap2: TDataSource;
    QrDanhmucLK_TINHTRANG: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbPhanLoaiNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ItemLienlacClick(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrDanhmucMATKChange(Sender: TField);
    procedure cbbMATK_CAP2DropDown(Sender: TObject);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mClose, mObsolete: Boolean;
    mSQL, mPrefix, mSearch: String;
  public
  	function Execute(r: WORD; pClose: Boolean = True): Boolean;
  end;

var

  FrmDmTKKeToan: TFrmDmTKKeToan;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, isLib, Lienhe, isCommon, DmTK, OfficeData, HrData;

{$R *.DFM}

const
	FORM_CODE = 'ACC_DM_TAIKHOAN_KETOAN';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmTKKeToan.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsDanhmuc.AutoEdit := mCanEdit;

    mRet := False;
    mClose := pClose;
    ShowModal;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    mTrigger := False;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);
    PaDetail.VertScrollBar.Position := 0;
    mSearch := '';
    mSQL := QrDanhmuc.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.FormShow(Sender: TObject);
var
    bVIP, bNV: Boolean;
begin
    EdMa.ReadOnly := mAutoCode;;


	OpenDataSets([QrTaiKhoanCap1, QrTaiKhoanCap2, QrTinhChat]);
    SetDisplayFormat(QrDanhmuc, sysCurFmt);

    CmdRefesh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.ItemLienlacClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrDanhmuc, QrTaiKhoanCap1, QrTaiKhoanCap2, QrTinhChat]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdRefeshExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;

    mSearch := DataMain.StripToneMark(EdSearch.Text);
	with QrDanhmuc do
    begin
    	Close;
        if mSearch <> '' then
            sSQL := sSQL + ' and (dbo.fnStripToneMark(TENTK) like ''%'+ mSearch + '%'' or dbo.fnStripToneMark(TENTK_TA) like ''%' + mSearch + '%'')';

        if not mObsolete then
            sSQL := sSQL + ' and (isnull(TINHTRANG, 0) = 0)';

        SQL.Text := sSQL;
        SQL.Add(' order by 	MATK');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);

	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdContact.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.PopupMenu1Popup(Sender: TObject);
begin
    ItemLienlac.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
        if BlankConfirm(QrDanhmuc, ['MATK', 'TENTK', 'TINHCHAT']) then
        	Abort;

	    SetNull(QrDanhmuc, ['MATK_CAP1', 'MATK_CAP2']);
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.QrDanhmucAfterPost(DataSet: TDataSet);
begin
	mRet := True;
    QrTaiKhoanCap1.Requery;
    QrTaiKhoanCap2.Requery;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.cbbMATK_CAP2DropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDanhmuc.FieldByName('MATK_CAP1').AsString;
    QrTaiKhoanCap2.Filter := 'MATK_CAP1=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.CbPhanLoaiNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmTKKeToan.QrDanhmucMATKChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
