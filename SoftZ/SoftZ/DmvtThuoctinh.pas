﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtThuoctinh;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, DBCtrls, ActnList, Db, ExtCtrls, Grids, StdCtrls, wwdblook,
  RzPanel, RzDBNav;

type
  TFrmDmvtThuoctinh = class(TForm)
    Inspect: TwwDataInspector;
    Action: TActionList;
    CbMau: TwwDBLookupCombo;
    CbSize: TwwDBLookupCombo;
    Panel1: TPanel;
    RzDBNavigator1: TRzDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  public
  	procedure Execute(pStr: String);
  end;

var
  FrmDmvtThuoctinh: TFrmDmvtThuoctinh;

implementation

uses
	ExCommon, isLib, isDb, isMsg, Dmvt;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtThuoctinh.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtThuoctinh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtThuoctinh.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtThuoctinh.Execute;
var
	n, h: Integer;
begin
	n := exConfigInspector(Inspect, pStr);
    if n < 12 then
    	with Inspect do
        begin
            h := Height;
            Height := Trunc(n * (h - 4) / 12.0) + 4;
            ApplySettings;
            Self.Height := Self.Height + Height - h;
        end;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtThuoctinh.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1
end;

end.
